﻿using LinearNet.Matrices;
using LinearNet.Providers;
using LinearNet.Providers.LinearAlgebra;
using LinearNet.Structs;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace LinearNet.Benchmarks
{
    internal static class BLAS1Benchmarks
    {
        private static void AXPY<T>(IDenseBLAS1<T> provider, IDenseBLAS1<T> nativeProvider, T alpha, Func<T[], T[], bool> ApproxEquals)
        {
            // warm up
            T[] u = RectangularVector.Random<T>(10);
            T[] v = RectangularVector.Random<T>(10);

            T[] u1 = u.Copy();
            nativeProvider.AXPY(u, v, alpha, 0, u.Length);
            provider.AXPY(u1, v, alpha, 0, u.Length);
            Debug.Assert(ApproxEquals(u, u1), "AXPY for type " + typeof(T) + " failed.");

            // The actual test
            int size = 20000000;
            u = RectangularVector.Random<T>(size);
            v = RectangularVector.Random<T>(size);

            GC.Collect(GC.MaxGeneration);
            GC.WaitForPendingFinalizers();

            Stopwatch sw = new Stopwatch();
            sw.Restart();
            for (int i = 0; i < 1000; ++i)
            {
                nativeProvider.AXPY(u, v, alpha, 0, size);
            }
            sw.Stop();
            Debug.WriteLine($"Benchmark AXPY native C# implementation ({typeof(T)}):\t" + sw.ElapsedMilliseconds);

            sw.Restart();
            for (int i = 0; i < 1000; ++i)
            {
                provider.AXPY(u, v, alpha, 0, size);
            }
            sw.Stop();
            Debug.WriteLine($"Testing AXPY implementation ({typeof(T)}):\t" + sw.ElapsedMilliseconds);
        }
        public static void AXPY_float(IDenseBLAS1<float> provider)
        {
            float alpha = (float)(new Random().NextDouble() - .5);
            AXPY(provider, new NativeFloatProvider(), alpha, (a, b) => a.ApproximatelyEquals(b));
        }
        public static void AXPY_double(IDenseBLAS1<double> provider)
        {
            double alpha = new Random().NextDouble() - .5;
            AXPY(provider, new NativeDoubleProvider(), alpha, (a, b) => a.ApproximatelyEquals(b));
        }
        public static void AXPY_Complex(IDenseBLAS1<Complex> provider)
        {
            Random r = new Random();
            Complex alpha = new Complex(r.NextDouble() - .5, r.NextDouble() - .5);
            AXPY(provider, new NativeComplexProvider(), alpha, (a, b) => a.ApproximatelyEquals(b));
        }

        public static void DOT_Complex(IDenseBLAS1<Complex> provider)
        {
            // warm up
            Complex[] u = RectangularVector.Random<Complex>(99);
            Complex[] v = RectangularVector.Random<Complex>(99);

            IDenseBLAS1<Complex> nativeProvider = new NativeComplexProvider();
            Complex d1 = nativeProvider.DOT(u, v, 0, 99);
            Complex d2 = provider.DOT(u, v, 0, 99);
            Debug.Assert(d1.ApproximatelyEquals(d2), "Dot product:\t" + d1 + "\t" + d2);

            // The actual test
            int size = 20000000;
            u = RectangularVector.Random<Complex>(size);
            v = RectangularVector.Random<Complex>(size);

            GC.Collect(GC.MaxGeneration);
            GC.WaitForPendingFinalizers();

            Stopwatch sw = new Stopwatch();
            sw.Restart();
            for (int i = 0; i < 1000; ++i)
            {
                nativeProvider.DOT(u, v, 0, size);
            }
            sw.Stop();
            Debug.WriteLine("DOT native C# implementation:\t" + sw.ElapsedMilliseconds);

            sw.Restart();
            for (int i = 0; i < 1000; ++i)
            {
                provider.DOT(u, v, 0, size);
            }
            sw.Stop();
            Debug.WriteLine("DOT accelerated implementation:\t" + sw.ElapsedMilliseconds);
        }

        public static void GivensRotateRow<T>(IDenseBLAS1<T> provider, IDenseBLAS1<T> nativeProvider, T c, T s, Func<DenseMatrix<T>, DenseMatrix<T>, bool> ApproxEquals) where T : new()
        {
            Random r = new Random();
            // warmup
            int n = 10;
            DenseMatrix<T> matrix1 = DenseMatrix.Random<T>(n, n);
            DenseMatrix<T> matrix2 = matrix1.Copy();

            provider.ROTR(matrix1.Values, 3, 8, c, s, 0, n);
            nativeProvider.ROTR(matrix2.Values, 3, 8, c, s, 0, n);

            Debug.Assert(ApproxEquals(matrix1, matrix2), "Givens row rotation test for " + typeof(T) + " failed.");

            // The actual test
            int size = 20000000;
            matrix1 = DenseMatrix.Random<T>(2, size);
            matrix2 = matrix1.Copy();

            Stopwatch sw = new Stopwatch();
            sw.Restart();
            for (int i = 0; i < 100; ++i)
            {
                provider.ROTR(matrix1.Values, 0, 1, c, s, 0, size);
            }
            sw.Stop();
            Debug.WriteLine($"Givens rotate rows ({typeof(T)}[{size}]) on accelerated provider took: " + sw.ElapsedMilliseconds);

            sw.Restart();
            for (int i = 0; i < 100; ++i)
            {
                nativeProvider.ROTR(matrix2.Values, 0, 1, c, s, 0, size);
            }
            sw.Stop();
            Debug.WriteLine($"Givens rotate rows ({typeof(T)}[{size}]) on native provider took: " + sw.ElapsedMilliseconds);

            Debug.Assert(ApproxEquals(matrix1, matrix2), "Givens row rotation test (large) for " + typeof(T) + " failed.");
        }
        /// <summary>
        /// Early tests results indicate:
        /// ~ 10% performance gain on AVX2 
        /// 
        /// </summary>
        /// <param name="provider"></param>
        public static void GivensRotateRow(IDenseBLAS1<double> provider)
        {
            Random r = new Random();
            GivensRotateRow(provider, new NativeDoubleProvider(), r.NextDouble(), r.NextDouble(), (a, b) => a.ApproximatelyEquals(b));
        }
        /// <summary>
        /// Early tests results indicate:
        /// ~ 2x performance gain on AVX2
        /// No performance gain on SSE in safe mode
        /// ~ 2x performance gain on SSE in unsafe mode
        /// </summary>
        /// <param name="provider"></param>
        public static void GivensRotateRow(IDenseBLAS1<float> provider)
        {
            Random r = new Random();
            GivensRotateRow(provider, new NativeFloatProvider(), (float)r.NextDouble(), (float)r.NextDouble(), (a, b) => a.ApproximatelyEquals(b));
        }
        /// <summary>
        /// Early tests results indicate:
        /// ~ 2x performance gain on AVX2
        /// No performance gain on SSE in safe mode
        /// ~ 2x performance gain on SSE in unsafe mode
        /// </summary>
        /// <param name="provider"></param>
        public static void GivensRotateRow(IDenseBLAS1<int> provider)
        {
            Random r = new Random();
            GivensRotateRow(provider, new NativeInt32Provider(), r.Next(10), r.Next(10), (a, b) => a.ExactlyEquals(b));
        }
    }
}
