﻿using LinearNet.Matrices;
using LinearNet.Matrices.Band;
using LinearNet.Providers.LinearAlgebra;
using LinearNet.Structs;
using System.Diagnostics;

#if NETCOREAPP3_0
using LinearNet.Providers.AVX2;
#endif 

namespace LinearNet.Benchmarks
{
    internal static class BandMatrixBenchmarks
    {
        public static void Multiplication()
        {
            int[] sizes = { 256, 512, 1024, 2048, 4096, 8192, 16384, 32768 };
            double bandwidth = 0.1;
            Stopwatch sw = new Stopwatch();

            bool runMultiply = true,
                runParallelMultiply = true,
                runMultiply2 = false,
                runDense = false;
            foreach (int n in sizes)
            {
                int width = (int)(bandwidth * n);
                BandMatrix<double> A = BandMatrix.Random<double>(n, n, width, width), B = BandMatrix.Random<double>(n, n, width, width);

                BandMatrix<double> AB1 = null, AB2 = null;
                double btime = 0, pbtime = 0, b2time = 0, dtime = 0;

                if (runMultiply)
                {
                    sw.Restart();
                    AB1 = A.Multiply(B, BLAS.Double);
                    sw.Stop();
                    btime = sw.ElapsedMilliseconds;
                }

                if (runParallelMultiply)
                {
                    sw.Restart();
                    AB2 = A.MultiplyParallel(B, BLAS.Double);
                    sw.Stop();
                    pbtime = sw.ElapsedMilliseconds;
                }

                if (runMultiply2)
                {
                    sw.Restart();
                    A.Multiply2(B, BLAS.Double);
                    sw.Stop();
                    b2time = sw.ElapsedMilliseconds;
                }

                if (runDense)
                {
                    DenseMatrix<double> _A = A.ToDenseMatrix();
                    DenseMatrix<double> _B = B.ToDenseMatrix();
                    sw.Restart();
                    _A.Multiply(_B);
                    sw.Stop();
                    dtime = sw.ElapsedMilliseconds;
                }

                Debug.WriteLine(n + "\t" + btime + "\t" + pbtime + "\t" + b2time + "\t" + dtime);
                Debug.WriteLine("||AB1 - AB2||2 = " + (AB1 - AB2).FrobeniusNorm(DefaultProviders.Double, x => x));
            }
        }
        public static void ParallelMultiplication()
        {
            int[] sizes = { 256, 512, 1024, 2048, 4096, 8192, 16384, 32768 };
            double bandwidth = 0.1;
            Stopwatch sw = new Stopwatch();

            foreach (int n in sizes)
            {
                int width = (int)(bandwidth * n);
                BandMatrix<double> A = BandMatrix.Random<double>(n, n, width, width), B = BandMatrix.Random<double>(n, n, width, width);

                double time = 0.0, ptime = 0.0;

                sw.Restart();
                BandMatrix<double> AB = A.Multiply(B);
                sw.Stop();
                time = sw.ElapsedMilliseconds;

                sw.Restart();
                BandMatrix<double> _AB = A.MultiplyParallel(B);
                sw.Stop();
                ptime = sw.ElapsedMilliseconds;

                Debug.WriteLine(n + "\t" + time + "\t" + ptime);
                Debug.Assert(AB.ApproximatelyEquals(AB));
            }
        }
        public static void TriangularSolve()
        {
            int[] sizes = { 10000, 100000, 1000000 };

            foreach (int n in sizes)
            {
                BandMatrix<double> band = BandMatrix.Random<double>(n, n, 0, 500);
                DenseVector<double> b = DenseVector.Random<double>(n);
                double[] x = new double[n];

                Stopwatch sw = new Stopwatch();
                sw.Start();
                band.SolveTriangular(b.Values, x, DefaultProviders.Double, true, false);
                sw.Stop();
                Debug.WriteLine($"Band ({n} x {n}, bw = 500) triangular solve using provider took: " + sw.ElapsedMilliseconds);

#if NETCOREAPP3_0
                sw.Restart();
                band.SolveTriangular(b.Values, x, new AVX2DoubleProvider(), true, false);
                sw.Stop();
                Debug.WriteLine($"Band ({n} x {n}, bw = 500) triangular solve using BLAS took: " + sw.ElapsedMilliseconds);
#endif
            }
        }
        public static void Cholesky()
        {
            int[] sizes = { 256, 512, 1024, 2048, 4096, 8192, 16384, 32768 };
            double bandwidth = 0.05;
            Stopwatch sw = new Stopwatch();

            foreach (int n in sizes)
            {
                int width = (int)(bandwidth * n);
                BandMatrix<double> A = BandMatrix.Random<double>(n, n, width, width);
                A = A.Transpose() * A;

                BandMatrix<double> L1 = null, L2 = null;
                double btime = 0, ptime = 0;

                sw.Restart();
                L1 = A.CholeskyFactor(DefaultProviders.Double);
                sw.Stop();
                ptime = sw.ElapsedMilliseconds; 
                
                sw.Restart();
                L2 = A.CholeskyFactor(BLAS.Double);
                sw.Stop();
                btime = sw.ElapsedMilliseconds;

                Debug.WriteLine(n + "\t" + ptime + "\t" + btime);
                Debug.WriteLine("||L1 - L2||2 = " + (L1 - L2).FrobeniusNorm(DefaultProviders.Double, x => x));
            }
        }
    }
}
