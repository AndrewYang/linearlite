﻿using LinearNet.Matrices;
using LinearNet.Matrices.Multiplication;
using LinearNet.Providers.CUDA;
using LinearNet.Providers.LinearAlgebra;
using LinearNet.Structs;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace LinearNet.Benchmarks
{
    public static class CUDABenchmarks
    {
        internal static void AXPY_double()
        {
            BLAS1Benchmarks.AXPY_double(new CudaDoubleProvider());
        }

        internal static void MMult()
        {
            Stopwatch sw = new Stopwatch();
            int[] sizes = { 256, 512, 1024, 2048, 4096 };
            List<Type> types = new List<Type>() { 
                typeof(double), 
                //typeof(Complex) 
            };

            var provider = new CudaDoubleProvider();
            var defaultProvider = new NaiveMatrixMultiplication<double>(new NativeDoubleProvider());

            double dtime = 0, _dtime = 0, ctime = 0;
            foreach (int n in sizes)
            {
                if (types.Contains(typeof(double)))
                {
                    DenseMatrix<double> A = DenseMatrix.Random<double>(n, n), B = DenseMatrix.Random<double>(n, n);
                    sw.Restart();
                    DenseMatrix<double> C1 = A.Multiply(B, provider);
                    sw.Stop();
                    dtime = sw.ElapsedMilliseconds;

                    sw.Restart();
                    DenseMatrix<double> C2 = A.Multiply(B, defaultProvider);
                    sw.Stop();
                    _dtime = sw.ElapsedMilliseconds;

                    Debug.Assert(C1.ApproximatelyEquals(C2), $"CUDA matrix multiply != matrix multiply for double[{n}, {n}]");
                }
                if (types.Contains(typeof(Complex)))
                {
                    DenseMatrix<Complex> A = DenseMatrix.Random<Complex>(n, n), B = DenseMatrix.Random<Complex>(n, n);
                    sw.Restart();
                    A.MultiplyParallel(B);
                    sw.Stop();
                    ctime = sw.ElapsedMilliseconds;
                }

                Debug.WriteLine(n + "\t" + dtime + "\t" + _dtime + "\t" + ctime);
            }
        }
    }
}
