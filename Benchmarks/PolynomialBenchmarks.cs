﻿using LinearNet.Helpers;
using LinearNet.Structs;
using LinearNet.Structs.Fields;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace LinearNet.Benchmarks
{
    internal static class PolynomialBenchmarks
    {
        internal static void UnivaratePowers()
        {
            // x + 1
            Polynomial<BigRational> p = new Polynomial<BigRational>(1, 1);

            Stopwatch sw = new Stopwatch();
            sw.Restart();
            Polynomial<BigRational> result1 = p.Pow(5000);
            sw.Stop();
            Debug.WriteLine("Binomial expansion took: " + sw.ElapsedMilliseconds + " ms");

            DensePolynomial<BigRational> pd = new DensePolynomial<BigRational>(1, 1);
            sw.Restart();
            DensePolynomial<BigRational> result2 = pd.Pow(5000);
            sw.Stop();
            Debug.WriteLine("Recursive squaring took: " + sw.ElapsedMilliseconds + " ms");
        }
    }
}
