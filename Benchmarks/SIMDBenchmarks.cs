﻿#if NETCOREAPP3_0

using LinearNet.Matrices;
using LinearNet.Matrices.Multiplication;
using LinearNet.Providers;
using LinearNet.Providers.LinearAlgebra;
using LinearNet.Structs;
using System;
using System.Diagnostics;
using LinearNet.Providers.AVX2;

namespace LinearNet.Benchmarks
{
    public static class SIMDBenchmarks
    {
        public static void AXPY()
        {
            // warm up
            double[] u = RectangularVector.Random<double>(10);
            double[] v = RectangularVector.Random<double>(10);

            IDenseBLAS1<double> nativeProvider = new NativeDoubleProvider();
            IDenseBLAS1<double> simdProvider = new AVX2DoubleProvider();
            nativeProvider.AXPY(u, v, 0.5, 0, u.Length);
            simdProvider.AXPY(u, v, 0.5, 0, u.Length);

            // The actual test
            int size = 20000000;
            Random r = new Random();

            u = RectangularVector.Random<double>(size);
            v = RectangularVector.Random<double>(size);
            double alpha = r.NextDouble();

            GC.Collect(GC.MaxGeneration);
            GC.WaitForPendingFinalizers();

            Stopwatch sw = new Stopwatch();
            sw.Restart();
            for (int i = 0; i < 1000; ++i)
            {
                nativeProvider.AXPY(u, v, alpha, 0, size);
            }
            sw.Stop();
            Debug.WriteLine("Native C# implementation:\t" + sw.ElapsedMilliseconds);

            sw.Restart();
            for (int i = 0; i < 1000; ++i)
            {
                simdProvider.AXPY(u, v, alpha, 0, size);
            }
            sw.Stop();
            Debug.WriteLine("SIMD implementation:\t" + sw.ElapsedMilliseconds);
        }
        public static void AXPY_Complex()
        {
            BLAS1Benchmarks.AXPY_Complex(new AVX2ComplexProvider());
        }
        public static void DotProduct()
        {
            // warm up
            double[] u = RectangularVector.Random<double>(10);
            double[] v = RectangularVector.Random<double>(10);

            IDenseBLAS1<double> nativeProvider = new NativeDoubleProvider();
            IDenseBLAS1<double> simdProvider = new AVX2DoubleProvider();
            nativeProvider.DOT(u, v, 0, u.Length);
            simdProvider.DOT(u, v, 0, u.Length);

            // The actual test
            int size = 20000000;
            double nativeTime = 0, simdTime = 0;

            Stopwatch sw = new Stopwatch();
            for (int i = 0; i < 10; ++i)
            {
                u = RectangularVector.Random<double>(size);
                v = RectangularVector.Random<double>(size);

                GC.Collect(GC.MaxGeneration);
                GC.WaitForPendingFinalizers();

                sw.Restart();
                nativeProvider.DOT(u, v, 0, size);
                sw.Stop();
                nativeTime += sw.ElapsedMilliseconds;

                sw.Restart();
                simdProvider.DOT(u, v, 0, size);
                sw.Stop();
                simdTime += sw.ElapsedMilliseconds;

            }

            Debug.WriteLine("Native C# implementation:\t" + nativeTime);
            Debug.WriteLine("SIMD implementation:\t" + simdTime);
        }
        public static void DotProductComplex()
        {
            // warm up
            Complex[] u = RectangularVector.Random<Complex>(10);
            Complex[] v = RectangularVector.Random<Complex>(10);

            IDenseBLAS1<Complex> nativeProvider = new NativeComplexProvider();
            IDenseBLAS1<Complex> simdProvider = new AVX2ComplexProvider();
            nativeProvider.DOT(u, v, 0, u.Length);
            simdProvider.DOT(u, v, 0, u.Length);

            // The actual test
            int size = 20000000;
            double nativeTime = 0, simdTime = 0;

            Stopwatch sw = new Stopwatch();
            for (int i = 0; i < 10; ++i)
            {
                u = RectangularVector.Random<Complex>(size);
                v = RectangularVector.Random<Complex>(size);

                GC.Collect(GC.MaxGeneration);
                GC.WaitForPendingFinalizers();

                sw.Restart();
                nativeProvider.DOT(u, v, 0, size);
                sw.Stop();
                nativeTime += sw.ElapsedMilliseconds;

                sw.Restart();
                simdProvider.DOT(u, v, 0, size);
                sw.Stop();
                simdTime += sw.ElapsedMilliseconds;

            }

            Debug.WriteLine("Native C# implementation:\t" + nativeTime);
            Debug.WriteLine("SIMD implementation:\t" + simdTime);
        }

        public static void Multiplication()
        {
            // warm up
            double[][] u = DenseMatrix.Random<double>(10, 10).Values;
            double[][] v = DenseMatrix.Random<double>(10, 10).Values;
            double[][] result = MatrixInternalExtensions.JMatrix<double>(10, 10),
                        result1 = MatrixInternalExtensions.JMatrix<double>(10, 10);

            IMatrixMultiplication<double> nativeProvider = new NaiveMatrixMultiplication<double>(new NativeDoubleProvider());
            IMatrixMultiplication<double> simdProvider = new NaiveMatrixMultiplication<double>(new AVX2DoubleProvider());
            nativeProvider.Multiply(u, v, result, 2, 3, 4, 1, 3, 2, new Size3(5, 5, 5), false);
            simdProvider.Multiply(u, v, result1, 2, 3, 4, 1, 3, 2, new Size3(5, 5, 5), false);
            Debug.Assert(result.ToRectangular().ApproximatelyEquals(result1.ToRectangular()));

            // The actual test
            int size = 4000;
            u = DenseMatrix.Random<double>(size, size).Values;
            v = DenseMatrix.Random<double>(size, size).Values;
            DenseMatrix<double> ws = new DenseMatrix<double>(size, size);
            result = MatrixInternalExtensions.JMatrix<double>(size, size);

            GC.Collect(GC.MaxGeneration);
            GC.WaitForPendingFinalizers();

            // Need to re-create this with the proper workspaces...
            nativeProvider = new NaiveMatrixMultiplication<double>(new NativeDoubleProvider(), MultiplicationMode.OPTIMIZE_SPEED, ws.Values);
            simdProvider = new NaiveMatrixMultiplication<double>(new AVX2DoubleProvider(), MultiplicationMode.OPTIMIZE_SPEED, ws.Values);
            Stopwatch sw = new Stopwatch();
            sw.Restart(); 
            nativeProvider.Multiply(u, v, result, 0, 0, 0, 0, 0, 0, new Size3(size, size, size), false);
            sw.Stop();
            Debug.WriteLine("Native C# implementation:\t" + sw.ElapsedMilliseconds); // ~57.1s for 4,000, 888.6s for 10,000

            sw.Restart();
            simdProvider.Multiply(u, v, result, 0, 0, 0, 0, 0, 0, new Size3(size, size, size), false);
            sw.Stop();
            Debug.WriteLine("SIMD implementation:\t" + sw.ElapsedMilliseconds); // ~29.7s for 4,000, 445.6s for 10,000
        }
        public static void MultiplicationFloatMatrix4x4()
        {
            /// Benchmarks the float SIMD matrix multiplication implementation which uses 
            /// block-multiplication algorithm with the SIMD-enabled struct Matrix4x4.
            /// 

            float[][] A = DenseMatrix.Random<float>(13, 17).Values;
            float[][] B = DenseMatrix.Random<float>(17, 15).Values;

            float[][] result = MatrixInternalExtensions.JMatrix<float>(13, 15);
            float[][] result1 = MatrixInternalExtensions.JMatrix<float>(13, 15);
            float[][] result2 = MatrixInternalExtensions.JMatrix<float>(13, 15);

            IMatrixMultiplication<float> nativeProvider = new NaiveMatrixMultiplication<float>(new NativeFloatProvider());
            nativeProvider.Multiply(A, B, result, 0, 0, 0, 0, 0, 0, new Size3(13, 17, 15), false);
            result.Print();

            IMatrixMultiplication<float> simdProvider = new NaiveMatrixMultiplication<float>(new AVX2FloatProvider());
            simdProvider.Multiply(A, B, result1, 0, 0, 0, 0, 0, 0, new Size3(13, 17, 15), false);
            result1.Print();

            simdProvider.Multiply(A, B, result2, new Size3(13, 17, 15), false);
            result2.Print();

            // Test
            Debug.Assert(result.ToRectangular().ApproximatelyEquals(result1.ToRectangular(), 1e-3f));
            Debug.Assert(result.ToRectangular().ApproximatelyEquals(result2.ToRectangular(), 1e-3f));

            int size = 2004;
            float[][] u = DenseMatrix.Random<float>(size, size).Values;
            float[][] v = DenseMatrix.Random<float>(size, size).Values;
            result = MatrixInternalExtensions.JMatrix<float>(size, size);

            GC.Collect(GC.MaxGeneration);
            GC.WaitForPendingFinalizers();

            Stopwatch sw = new Stopwatch();
            sw.Restart();
            nativeProvider.Multiply(u, v, result, 0, 0, 0, 0, 0, 0, new Size3(size, size, size), false);
            sw.Stop();
            Debug.WriteLine("Native C# implementation:\t" + sw.ElapsedMilliseconds);

            sw.Restart();
            simdProvider.Multiply(u, v, result, 0, 0, 0, 0, 0, 0, new Size3(size, size, size), false);
            sw.Stop();
            Debug.WriteLine("SIMD implementation:\t" + sw.ElapsedMilliseconds);

            sw.Restart();
            simdProvider.Multiply(u, v, result, new Size3(size, size, size), false);
            sw.Stop();
            Debug.WriteLine("SIMD (1) implementation:\t" + sw.ElapsedMilliseconds);

            float[][] workspace = MatrixInternalExtensions.JMatrix<float>(size, size);
            simdProvider = new NaiveMatrixMultiplication<float>(new AVX2FloatProvider(), MultiplicationMode.OPTIMIZE_SPEED, workspace);
            sw.Restart();
            simdProvider.Multiply(u, v, result, 0, 0, 0, 0, 0, 0, new Size3(size, size, size), false);
            sw.Stop();
            Debug.WriteLine("SIMD^T implementation:\t" + sw.ElapsedMilliseconds);

            sw.Restart();
            simdProvider.Multiply(u, v, result, new Size3(size, size, size), false);
            sw.Stop();
            Debug.WriteLine("SIMD^T (1) implementation:\t" + sw.ElapsedMilliseconds);
        }
    }
}

#endif