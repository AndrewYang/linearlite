﻿#if NETCOREAPP3_0

using LinearNet.Providers.LinearAlgebra;
using LinearNet.Providers.SSE;
using System;
using System.Diagnostics;
using LinearNet.Providers;

namespace LinearNet.Benchmarks
{
    public static class SSEBenchmarks
    {
        public static void AXPY_double()
        {
            // warm up
            double[] u = RectangularVector.Random<double>(10);
            double[] v = RectangularVector.Random<double>(10);

            IDenseBLAS1<double> 
                nativeProvider = new NativeDoubleProvider(),
                simdProvider = new SSE2DoubleProvider(true);

            double[] u1 = u.Copy();
            nativeProvider.AXPY(u, v, 0.5, 0, u.Length);
            simdProvider.AXPY(u1, v, 0.5, 0, u.Length);
            Debug.Assert(u.ApproximatelyEquals(u1));

            // The actual test
            int size = 20000000;
            Random r = new Random();

            u = RectangularVector.Random<double>(size);
            v = RectangularVector.Random<double>(size);
            double alpha = r.NextDouble();

            GC.Collect(GC.MaxGeneration);
            GC.WaitForPendingFinalizers();

            Stopwatch sw = new Stopwatch();
            sw.Restart();
            for (int i = 0; i < 1000; ++i)
            {
                nativeProvider.AXPY(u, v, alpha, 0, size);
            }
            sw.Stop();
            Debug.WriteLine("Native C# implementation:\t" + sw.ElapsedMilliseconds);

            sw.Restart();
            for (int i = 0; i < 1000; ++i)
            {
                simdProvider.AXPY(u, v, alpha, 0, size);
            }
            sw.Stop();
            Debug.WriteLine("SSE implementation:\t" + sw.ElapsedMilliseconds);
        }
        public static void AXPY_float()
        {
            // warm up
            float[] u = RectangularVector.Random<float>(10);
            float[] v = RectangularVector.Random<float>(10);

            IDenseBLAS1<float>
                nativeProvider = new NativeFloatProvider(),
                simdProvider = new SSEFloatProvider(true);

            float[] u1 = u.Copy();
            nativeProvider.AXPY(u, v, 0.5f, 0, u.Length);
            simdProvider.AXPY(u1, v, 0.5f, 0, u.Length);
            Debug.Assert(u.ApproximatelyEquals(u1));

            // The actual test
            int size = 20000000;
            Random r = new Random();

            u = RectangularVector.Random<float>(size);
            v = RectangularVector.Random<float>(size);
            float alpha = (float)r.NextDouble();

            GC.Collect(GC.MaxGeneration);
            GC.WaitForPendingFinalizers();

            Stopwatch sw = new Stopwatch();
            sw.Restart();
            for (int i = 0; i < 1000; ++i)
            {
                nativeProvider.AXPY(u, v, alpha, 0, size);
            }
            sw.Stop();
            Debug.WriteLine("Native C# implementation:\t" + sw.ElapsedMilliseconds);

            sw.Restart();
            for (int i = 0; i < 1000; ++i)
            {
                simdProvider.AXPY(u, v, alpha, 0, size);
            }
            sw.Stop();
            Debug.WriteLine("SSE implementation:\t" + sw.ElapsedMilliseconds);
        }
        public static void DOT_double()
        {
            // warm up
            double[] u = RectangularVector.Random<double>(99);
            double[] v = RectangularVector.Random<double>(99);

            IDenseBLAS1<double> 
                nativeProvider = new NativeDoubleProvider(),
                simdProvider = new SSE2DoubleProvider(true);

            double d1 = nativeProvider.DOT(u, v, 0, 99);
            double d2 = simdProvider.DOT(u, v, 0, 99);
            Debug.Assert(Util.ApproximatelyEquals(d1, d2), "SSE dot product:\t" + d1 + "\t" + d2);

            // The actual test
            int size = 20000000;
            u = RectangularVector.Random<double>(size);
            v = RectangularVector.Random<double>(size);

            GC.Collect(GC.MaxGeneration);
            GC.WaitForPendingFinalizers();

            Stopwatch sw = new Stopwatch();
            sw.Restart();
            for (int i = 0; i < 1000; ++i)
            {
                nativeProvider.DOT(u, v, 0, size);
            }
            sw.Stop();
            Debug.WriteLine("Native C# implementation:\t" + sw.ElapsedMilliseconds);

            sw.Restart();
            for (int i = 0; i < 1000; ++i)
            {
                simdProvider.DOT(u, v, 0, size);
            }
            sw.Stop();
            Debug.WriteLine("SSE implementation:\t" + sw.ElapsedMilliseconds);
        }
        public static void DOT_float()
        {
            // warm up
            float[] u = RectangularVector.Random<float>(99);
            float[] v = RectangularVector.Random<float>(99);

            IDenseBLAS1<float> 
                nativeProvider = new NativeFloatProvider(),
                simdProvider = new SSEFloatProvider(true);
            if (!SSEFloatProvider.IsSupported)
            {
                throw new NotSupportedException();
            }

            float d1 = nativeProvider.DOT(u, v, 0, 99);
            float d2 = simdProvider.DOT(u, v, 0, 99);
            Debug.Assert(Util.ApproximatelyEquals(d1, d2), "SSE dot product:\t" + d1 + "\t" + d2);

            // The actual test
            int size = 20000000;
            u = RectangularVector.Random<float>(size);
            v = RectangularVector.Random<float>(size);

            GC.Collect(GC.MaxGeneration);
            GC.WaitForPendingFinalizers();

            Stopwatch sw = new Stopwatch();
            sw.Restart();
            for (int i = 0; i < 1000; ++i)
            {
                nativeProvider.DOT(u, v, 0, size);
            }
            sw.Stop();
            Debug.WriteLine("Native C# implementation:\t" + sw.ElapsedMilliseconds);

            sw.Restart();
            for (int i = 0; i < 1000; ++i)
            {
                simdProvider.DOT(u, v, 0, size);
            }
            sw.Stop();
            Debug.WriteLine("SSE implementation:\t" + sw.ElapsedMilliseconds);
        }
    }
}

#endif