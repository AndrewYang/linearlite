﻿using LinearNet.Global;
using LinearNet.Matrices.Cholesky.Kernels;
using LinearNet.Matrices.Sparse;
using LinearNet.Matrices.Sparse.Decompositions;
using LinearNet.Providers.LinearAlgebra;
using LinearNet.Structs;
using System;
using System.Diagnostics;
using System.Linq;

namespace LinearNet.Benchmarks
{
    public static class SparseMatrixBenchmarks
    {
        internal static void CSCMatrixMultiply()
        {
            // Warm up
            var discard = CSCSparseMatrix.Random<double>(10, 10, 0.5) * CSCSparseMatrix.Random<double>(10, 10, 0.5);

            int seed = 0;
            Random r = new Random(seed);
            double density = 0.02;
            int[] sizes = { 5000, 10000, 15000, 20000 };
            foreach (int n in sizes)
            {
                CSCSparseMatrix<double> A = CSCSparseMatrix.Random<double>(n, n, density, () => r.NextDouble() * 2 - 1, seed);
                CSCSparseMatrix<double> B = CSCSparseMatrix.Random<double>(n, n, density, () => r.NextDouble() * 2 - 1, seed);

                Stopwatch sw = new Stopwatch();
                sw.Start();
                CSCSparseMatrix<double> AB = A.Multiply(B, BLAS.Double, Matrices.Multiplication.MultiplicationMode.OPTIMIZE_SPEED);
                sw.Stop();
                double stime = sw.ElapsedMilliseconds;

                sw.Restart();
                //AB = A.Multiply(B, DefaultProviders.Double, 10);
                sw.Stop();
                double ptime = sw.ElapsedMilliseconds;

                Debug.WriteLine($"<{n}, {n}, {n}> @ {density} density with {AB.SymbolicNonZeroCount} nnz in product\t" + stime + "\t|| time:\t" + ptime);
            }
        }

        internal static void CSRAdd()
        {
            // Warm up
            var discard = CSRSparseMatrix.Random<double>(10, 10, 0.5) + CSRSparseMatrix.Random<double>(10, 10, 0.5);

            int[] sizes = { 10000, 50000 };
            foreach (int n in sizes)
            {
                CSRSparseMatrix<double> A = CSRSparseMatrix.Random<double>(n, n, 0.1);
                CSRSparseMatrix<double> B = CSRSparseMatrix.Random<double>(n, n, 0.1);

                Stopwatch sw = new Stopwatch();

                sw.Restart();
                CSRSparseMatrix<double> AB1 = A.Add(B, BLAS.Double);
                sw.Stop();
                double btime = sw.ElapsedMilliseconds;

                sw.Restart();
                CSRSparseMatrix<double> AB = A.Add(B, DefaultProviders.Double);
                sw.Stop();
                double stime = sw.ElapsedMilliseconds;

                Debug.WriteLine($"<{n}, {n}> addition with {AB.SymbolicNonZeroCount} nnz in sum\t" + stime + "\t" + btime);
            }
        }

        /// <summary>
        /// MCSR and CSR matrix multiply implementations are slightly different (in particular in the row-inclusion algorithm)
        /// This method tests which is more performant
        /// </summary>
        internal static void MCSRvsCSRMatrixMultiply()
        {
            // Warm up
            var discard = CSRSparseMatrix.Random<double>(10, 10, 0.5) * CSRSparseMatrix.Random<double>(10, 10, 0.5);
            var discard2 = MCSRSparseMatrix.Random<double>(10, 0.5, 0.5) * MCSRSparseMatrix.Random<double>(10, 0.5, 0.5);

            int[] sizes = { 10000, 100000 };
            foreach (int n in sizes)
            {
                CSRSparseMatrix<double> A = CSRSparseMatrix.Random<double>(n, n, 0.001), 
                    B = CSRSparseMatrix.Random<double>(n, n, 0.001);

                Stopwatch sw = new Stopwatch();
                sw.Start();
                CSRSparseMatrix<double> AB = A * B;
                sw.Stop();
                double stime = sw.ElapsedMilliseconds;

                MCSRSparseMatrix<double> _A = new MCSRSparseMatrix<double>(A);
                MCSRSparseMatrix<double> _B = new MCSRSparseMatrix<double>(B);

                sw.Restart();
                MCSRSparseMatrix<double> _AB = _A * _B;
                sw.Stop();
                double ptime = sw.ElapsedMilliseconds;

                Debug.WriteLine($"<{n}, {n}, {n}> with {AB.SymbolicNonZeroCount} nnz in product\t" + stime + "\t|| time:\t" + ptime);
            }
        }

        /// <summary>
        /// See which variant of Skyline sparse triangular solve is faster
        /// </summary>
        internal static void SKYSparseMatrixTriangularSolve()
        {
            int dim = 20000;
            DenseMatrix<double> dense = DenseMatrix.Random<double>(dim, dim);
            SKYSparseMatrix<double> A = new SKYSparseMatrix<double>(dense, SKYType.LOWER);
            DenseVector<double> b = DenseVector.Random<double>(dim);
            DenseVector<double> x = new DenseVector<double>(dim);

            Stopwatch sw = new Stopwatch();
            sw.Restart();
            A.SolveTriangular(b, x, DefaultProviders.Double);
            sw.Stop();
            Debug.WriteLine("Using provider took: " + sw.ElapsedMilliseconds + " ms");


            sw.Restart();
            A.SolveTriangular(b.Values, x.Values, BLAS.Double, true, false);
            sw.Stop();
            Debug.WriteLine("Using blas-1 took: " + sw.ElapsedMilliseconds + " ms");
        }
        internal static void SKYSparseMatrixCholesky()
        {
            int dim = 100000;
            BandMatrix<double> band = BandMatrix.Random<double>(dim, dim, 300, 300);
            SKYSparseMatrix<double> A = new SKYSparseMatrix<double>(band.Transpose() * band, SKYType.SYMMETRIC);

            Stopwatch sw = new Stopwatch();
            sw.Restart();
            A.CholeskyFactor(DefaultProviders.Double);
            sw.Stop();
            Debug.WriteLine("Using provider took: " + sw.ElapsedMilliseconds + " ms");

            sw.Restart();
            A.CholeskyFactor(BLAS.Double);
            sw.Stop();
            Debug.WriteLine("Using blas-1 took: " + sw.ElapsedMilliseconds + " ms");
        }

        internal static void CSCCholesky()
        {
            string[] files = System.IO.Directory.GetFiles(Constants.SparseMatrixDirectory);
            Stopwatch sw = new Stopwatch();

            foreach (string file in files)
            {
                if (!file.EndsWith(".mtx")) continue;

                string name = file.Split('\\').Last();
                double loadTime, symbTime;

                sw.Restart();
                CSCSparseMatrix<double> csc = CSCSparseMatrix.LoadTriplet(file, x => double.Parse(x), ' ', indexing: 1);
                sw.Stop();
                loadTime = sw.ElapsedMilliseconds;

                //csc.Print(x => x.ToString("n4"), maxRows: 50, maxCols: 50);

                csc = csc.ReflectLower();

                sw.Restart();
                SymbolicCholesky analysis = csc.CholeskyAnalysis();
                sw.Stop();
                symbTime = sw.ElapsedMilliseconds;

                // Set up all additional structures
                var chol = new CSCCholesky<double>();
                var kernel = new DoubleCholeskyTriangularSolveKernel();
                //var kernel = new AVX2DoubleCholeskyTriangularSolveKernel();
                //var kernel = new DefaultCSCCholeskyTriangularSolveKernel<double>(DefaultProviders.Double);
                //var kernel = new Blas1CSCCholeskyTriangularSolveKernel<double>(BLAS.Double);
                //var kernel = new Blas1CSCCholeskyTriangularSolveKernel2<double>(analysis, new AVX2DoubleProvider());
                var cholsuper = new CSCCholeskyLeftSuper<double>(csc, analysis, BLAS.Double);

                sw.Restart();
                //CSCSparseMatrix<double> L = csc.Cholesky(analysis, DefaultProviders.Double);
                CSCSparseMatrix<double> L = chol.CholUp2(csc, analysis, DefaultProviders.Double, kernel);
                sw.Stop();
                double time1 = sw.ElapsedMilliseconds;

                /*
                sw.Restart();
                CSRSparseMatrix<double> csr = (CSRSparseMatrix<double>)csc;
                csr.CholeskyFactor(DefaultProviders.Double);
                sw.Stop();
                */

                /*
                sw.Restart();
                CSCCholesky<double> chol = new CSCCholesky<double>();
                CSCSparseMatrix<double> L1 = chol.CholLeft(csc, analysis, BLAS.Double);
                sw.Stop();
                double time2 = sw.ElapsedMilliseconds;
                */

                sw.Restart();
                //CSCSparseMatrix<double> L = cholsuper.Decompose();
                sw.Stop();
                double time2 = sw.ElapsedMilliseconds;
                
                Debug.WriteLine($"{name}\tsize:\t{csc.Rows}\tnnz(A):\t{csc.SymbolicNonZeroCount}\tnnz(L):\t{L.SymbolicNonZeroCount}\tLoaded in:\t{loadTime}\tSymbolic in:\t{symbTime}" +
                    $"\tCompute time:\t{time1}\tCSR time:\t{time2}");
                //Debug.WriteLine("||LL' - A||2 / ||A||2 = " + csc.FrobeniusDistance(L * L.Transpose(), DefaultProviders.Double) / csc.FrobeniusNorm(DefaultProviders.Double));
            }
        }
        internal static void CSRMatrixAdd()
        {

        }

        internal static void LoadCSRSymmetric()
        {
            string[] files = System.IO.Directory.GetFiles(Constants.SparseMatrixDirectory);
            Stopwatch sw = new Stopwatch();

            foreach (string file in files)
            {
                sw.Restart();
                CSRSparseMatrix<double> csr = CSRSparseMatrix.LoadTriplet(file, x => double.Parse(x), ' ', indexing: 1);
                sw.Stop();

                Debug.WriteLine($"Loaded: {csr.Rows} x {csr.Columns} in {sw.ElapsedMilliseconds} ms. lower? {csr.IsLower()}");
            }
        }
    }
}
