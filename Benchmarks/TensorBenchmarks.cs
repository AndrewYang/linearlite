﻿using LinearNet.Providers.LinearAlgebra;
using LinearNet.Structs;
using LinearNet.Structs.Tensors;
using System;
using System.Diagnostics;

namespace LinearNet.Benchmarks
{
    public class TensorBenchmarks
    {
        internal static void Run()
        {
            //CSFMatrixMultiplyVsDense();
            //CSFUnfold();
            //CSFVectorMultiplyVsDense();
            //SetFibreTest();
            CSFSparseSumOver();
        }

        internal static void AccessBenchmarks()
        {
            int dimension = 4;

            Random r = new Random();
            Stopwatch sw = new Stopwatch();
            int[] orders = { 2, 3, 4, 5, 6, 8, 9 };
            foreach (int order in orders)
            {
                int[] dimensions = new int[order];
                for (int i = 0; i < order; ++i)
                {
                    dimensions[i] = dimension;
                }

                int[] index = new int[order];
                for (int i = 0; i < order; ++i)
                {
                    index[i] = r.Next(dimension);
                }

                DenseTensor<double> tensor = new DenseTensor<double>(dimensions);
                sw.Restart();
                for (int i = 0; i < 10000000; ++i)
                {
                    tensor.Set(Math.PI, index);
                    double val = tensor.Get(index);
                }
                sw.Stop();
                double time1 = sw.ElapsedMilliseconds;

                SparseTensor<double> st = new SparseTensor<double>(dimensions);
                sw.Restart();
                for (int i = 0; i < 10000000; ++i)
                {
                    st.Set(Math.PI, index);
                    double val = st.Get(index);
                }
                sw.Stop();
                double time2 = sw.ElapsedMilliseconds;

                Debug.WriteLine(order + "\t" + time1 + "\tms\t" + time2 + "\tms");
            }
        }

        internal static void DenseTensorContract()
        {
            int n = 4000;
            DenseMatrix<double> A = DenseMatrix.Random<double>(n, n),
                B = DenseMatrix.Random<double>(n, n);

            DenseTensor<double> _A = new DenseTensor<double>(A),
                _B = new DenseTensor<double>(B);

            Stopwatch sw = new Stopwatch();
            sw.Restart();
            DenseMatrix<double> C = A * B;
            sw.Stop();
            Debug.WriteLine($"Matrix multiplication <{n}, {n}, {n}> took:\t" + sw.ElapsedMilliseconds);

            sw.Restart();
            DenseTensor<double> _C = _A.Contract(_B, 1, 0, BLAS.Double);
            sw.Stop();
            Debug.WriteLine($"Tensor contraction <{n}, {n}, {n}> took:\t" + sw.ElapsedMilliseconds);
        }

        internal static void CSFSparseAdd()
        {
            int[] sizes = { 100, 200, 300, 400, 500, 600, 700 };
            double[] densities = { 0.1, 0.2, 0.3, 0.4, 0.5 };

            Stopwatch sw = new Stopwatch();
            foreach (int n in sizes)
            {
                foreach (double density in densities)
                {
                    CSFSparseTensor<double> A = CSFSparseTensor.Random<double>(n, n, n, density: density),
                        B = CSFSparseTensor.Random<double>(n, n, n, density:density);

                    sw.Restart();
                    A.Add(B, DefaultProviders.Double);
                    sw.Stop();
                    double time1 = sw.ElapsedMilliseconds;

                    sw.Restart();
                    A.Add(B, BLAS.Double);
                    sw.Stop();
                    double time3 = sw.ElapsedMilliseconds;

                    sw.Restart();
                    A.Add2(B, DefaultProviders.Double);
                    sw.Stop();
                    double time2 = sw.ElapsedMilliseconds;

                    Debug.WriteLine(n + "\t" + density + "\t" + time1 + "\t" + time2 + "\t" + time3);
                }
            }
        }

        internal static void CSFSparseTranspose()
        {
            int[] sizes = { 100, 200, 300, 400, 500, 600, 700, 800 };
            Stopwatch sw = new Stopwatch();
            foreach (int n in sizes)
            {
                CSFSparseTensor<double> tensor = CSFSparseTensor.Random<double>(n, n, n, density: 0.3);
                sw.Restart();
                tensor.Transpose(2, 1, 0);
                sw.Stop();
                double time1 = sw.ElapsedMilliseconds;

                sw.Restart();
                tensor.Transpose2(2, 1, 0);
                sw.Stop();
                double time2 = sw.ElapsedMilliseconds;

                Debug.WriteLine(n + "\t" + time1 + "\t" + time2);
            }
        }

        internal static void CSFMatrixMultiplyVsDense()
        {
            // Mode 0
            // Compare with dense matrix - find the breakeven densities where the sparse and dense variants take the same time
            // [100 x 100 x 100]: for density < 0.13 (0.15), sparse is faster than dense
            // [200 x 200 x 200]: for density < 0.15 (0.16), sparse is faster than dense
            // [300 x 300 x 300]: for density < 0.17 (0.17), sparse is faster than dense
            // ( . ) denotes the second implementation using linked lists

            int n = 300;
            Stopwatch sw = new Stopwatch();

            for (double density = 0.01; density < 0.5; density += 0.01)
            {
                CSFSparseTensor<double> stensor = CSFSparseTensor.Random<double>(n, n, n, density: density);
                DenseTensor<double> dtensor = new DenseTensor<double>(stensor);
                CSRSparseMatrix<double> smatrix = CSRSparseMatrix.Random<double>(n, n, density);
                DenseMatrix<double> dmatrix = new DenseMatrix<double>(smatrix);

                sw.Restart();
                stensor.Multiply(smatrix, 0, DefaultProviders.Double);
                sw.Stop();
                double stime = sw.ElapsedMilliseconds;

                sw.Restart();
                dtensor.Multiply(dmatrix, 0, BLAS.Double);
                sw.Stop();
                double dtime = sw.ElapsedMilliseconds;

                Debug.WriteLine(density + "\t" + stime + "\t" + dtime);
            }
        }

        internal static void CSFVectorMultiplyVsDense()
        {
            int[] sizes = { 100, 200, 300, 400, 500, 600, 700, 800 };
            double[] densities = { 0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5 };
            Stopwatch sw = new Stopwatch();
            
            /// Observation is mostly that increasing size has a small negative effect on sparse tensor 
            /// outperformance, however most important factor is density
            /// For mode 0: density < 0.2, sparse tensors outperform dense tensors for vector multiplication
            /// For mode 1: density < 0.5, ''
            /// For mode 2: density < 0.55 ''
            foreach (int n in sizes)
            {
                foreach (double d in densities)
                {
                    CSFSparseTensor<double> stensor = CSFSparseTensor.Random<double>(n, n, n, density: d);
                    DenseTensor<double> dtensor = new DenseTensor<double>(stensor);
                    DenseVector<double> vector = DenseVector.Random<double>(n);

                    double[] times = new double[6];
                    int k = 0;

                    for (int mode = 0; mode < 3; ++mode)
                    {
                        sw.Restart();
                        stensor.Multiply(vector, mode, DefaultProviders.Double);
                        sw.Stop();
                        times[k++] = sw.ElapsedMilliseconds;

                        sw.Restart();
                        dtensor.Multiply(vector, mode, BLAS.Double);
                        sw.Stop();
                        times[k++] = sw.ElapsedMilliseconds;
                    }

                    Debug.WriteLine(n + "\t" + d + "\t" + times[0] + "\t" + times[1] + "\t" + times[2] + "\t" + times[3] + "\t" + times[4] + "\t" + times[5]);
                }
            }
        }

        internal static void CSFUnfold()
        {
            int[] sizes = { 100, 200, 300, 400, 500, 600, 700, 800 };
            double[] densities = { 0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5, 0.55, 0.6 };

            Stopwatch sw = new Stopwatch();
            foreach (int n in sizes)
            {
                foreach (double density in densities)
                {
                    CSFSparseTensor<double> tensor = CSFSparseTensor.Random<double>(n, n, n, density: density);

                    double[] times = new double[3];
                    for (int mode = 0; mode < 3; ++mode)
                    {
                        sw.Restart();
                        tensor.Unfold(mode);
                        sw.Stop();
                        times[mode] = sw.ElapsedMilliseconds;
                    }
                    Debug.WriteLine(n + "\t" + density + "\t" + times[0] + "\t" + times[1] + "\t" + times[2]);
                }
            }
        }

        internal static void SetFibreTest()
        {
            int[] sizes = { 100, 200, 300, 400, 500 };
            double[] densities = { 0.1, 0.2, 0.3, 0.4, 0.5 };

            Stopwatch sw = new Stopwatch();
            
            foreach (int n in sizes)
            {
                foreach (double density in densities)
                {
                    CSFSparseTensor<double> tensor = CSFSparseTensor.Random<double>(n, n, n, density: density);
                    CSFSparseTensor<double> tensor2 = new CSFSparseTensor<double>(tensor);
                    DenseVector<double> vector = DenseVector.Random<double>(n);

                    sw.Restart();
                    tensor.SetRowFibre(n / 2, n / 2, vector);
                    sw.Stop();
                    double t1 = sw.ElapsedMilliseconds;
                    double t2 = sw.ElapsedMilliseconds;

                    Debug.WriteLine(n + "\t" + density + "\t" + t1 + "\t" + t2);
                }
            }
        }

        internal static void CSFSparseSumOver()
        {
            int[] sizes = { 100, 200, 300, 400, 500, 600, 700, 800 };
            double[] densities = { 0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5 };
            Stopwatch sw = new Stopwatch();

            foreach (int n in sizes)
            {
                foreach (double d in densities)
                {
                    CSFSparseTensor<double> stensor = CSFSparseTensor.Random<double>(n, n, n, density: d);

                    sw.Restart();
                    stensor.SumOver(0, DefaultProviders.Double);
                    sw.Stop();
                    double time1 = sw.ElapsedMilliseconds;

                    Debug.WriteLine(n + "\t" + d + "\t" + time1);
                }
            }
        }

    }
}
