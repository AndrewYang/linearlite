
// Response codes
constexpr unsigned int SUCCESS = 0;
constexpr unsigned int ERR_SET_DEVICE = 1;
constexpr unsigned int ERR_MALLOC = 2;
constexpr unsigned int ERR_MEMCPY = 3;
constexpr unsigned int ERR_HANDLE_CREATE = 4;
constexpr unsigned int ERR_KERNEL = 5;
constexpr unsigned int ERR_SYNC = 6;
constexpr unsigned int ERR_HANDLE_DESTROY = 7;