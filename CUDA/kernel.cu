
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <stdio.h>
#include "exitcodes.h"

template<class T> bool add(T *c, const T *a, const T *b, unsigned int size);

__global__ void addKernel(__int32* c, const __int32* a, const __int32* b, const int n) {
    int i = threadIdx.x;
    if (i < n) c[i] = a[i] + b[i];
}
__global__ void addKernel(__int64* c, const __int64* a, const __int64* b, const int n) {
    int i = threadIdx.x;
    if (i < n) c[i] = a[i] + b[i];
}
__global__ void addKernel(float* c, const float* a, const float* b, const int n) {
    int i = threadIdx.x;
    if (i < n) c[i] = a[i] + b[i];
}
__global__ void addKernel(double* c, const double* a, const double* b, const int n) {
    int i = threadIdx.x;
    if (i < n) c[i] = a[i] + b[i];
}

__global__ void axpyKernel(__int32* y, const __int32* x, const __int32 alpha, const unsigned int start, const unsigned int end) {
    int i = threadIdx.x;
    if (start <= i && i < end) y[i] += alpha * x[i];
}
__global__ void axpyKernel(__int64* y, const __int64* x, const __int64 alpha, const unsigned int start, const unsigned int end) {
    int i = threadIdx.x;
    if (start <= i && i < end) y[i] += alpha * x[i];
}
__global__ void axpyKernel(float* y, const float* x, const float alpha, const unsigned int start, const unsigned int end) {
    int i = threadIdx.x;
    if (start <= i && i < end) y[i] += alpha * x[i];
}
__global__ void axpyKernel(double* y, const double* x, const double alpha, const unsigned int start, const unsigned int end) {
    int i = threadIdx.x;
    if (start <= i && i < end) y[i] += alpha * x[i];
}

__global__ void scalKernel(__int32* x, const __int32 s, const unsigned int start, const unsigned int end) {
    int i = threadIdx.x;
    if (start <= i && i < end) x[i] *= s;
}
__global__ void scalKernel(__int64* x, const __int64 s, const unsigned int start, const unsigned int end) {
    int i = threadIdx.x;
    if (start <= i && i < end) x[i] *= s;
}
__global__ void scalKernel(float* x, const float s, const unsigned int start, const unsigned int end) {
    int i = threadIdx.x;
    if (start <= i && i < end) x[i] *= s;
}
__global__ void scalKernel(double* x, const double s, const unsigned int start, const unsigned int end) {
    int i = threadIdx.x;
    if (start <= i && i < end) x[i] *= s;
}

/*
int main()
{
    const unsigned int length = 5;

    const int a[length] = { 1, 2, 3, 4, 5 };
    const int b[length] = { 10, 20, 30, 40, 50 };
    int c[length] = { 0 };
    add<int>(c, a, b, length);
    printf("int32 sum = {%d,%d,%d,%d,%d}\n", c[0], c[1], c[2], c[3], c[4]);

    const __int64 la[length] = { 1, 2, 3, 4, 5 };
    const __int64 lb[length] = { 2, 3, 4, 5, 6 };
    __int64 lc[length] = { 0 };
    add<__int64>(lc, la, lb, length);
    printf("int64 sum = {%d,%d,%d,%d,%d}\n", lc[0], lc[1], lc[2], lc[3], lc[4]);

    const double da[length] = { 0.5, 0.6, 0.7, 0.8, 0.9 };
    const double db[length] = { 1.1, 1.2, 1.3, 1.4, 1.5 };
    double dc[length] = { 0 };
    add<double>(dc, da, db, length);
    printf("double sum = {%f,%f,%f,%f,%f}\n", dc[0], dc[1], dc[2], dc[3], dc[4]);

    
    // cudaDeviceReset must be called before exiting in order for profiling and
    // tracing tools such as Nsight and Visual Profiler to show complete traces.
    cudaError_t cudaStatus = cudaDeviceReset();
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaDeviceReset failed!");
        return 1;
    }

    return 0;
}*/

template<class T> T* CreateArrayInDeviceMemory(const unsigned int size, bool &success) {
    T* dev_array = 0;
    cudaError_t status = cudaMalloc((void**)&dev_array, size * sizeof(T));
    if (status != cudaSuccess) {
        success = false;
    }
    return dev_array;
}
template<class T> void CopyArray_HostToDevice(const T* host_array, T* device_array, const unsigned int length, bool &success) {
    cudaError_t status = cudaMemcpy(device_array, host_array, length * sizeof(T), cudaMemcpyHostToDevice);
    if (status != cudaSuccess) {
        success = false;
    }
}

template<class T> bool add(T *c, const T *a, const T *b, const unsigned int length)
{
    bool success = true;
    cudaError_t cudaStatus;

    // Choose which GPU to run on, change this on a multi-GPU system.
    cudaStatus = cudaSetDevice(0);
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaSetDevice failed!  Do you have a CUDA-capable GPU installed?");
        goto Error;
    }

    // Allocate GPU buffers for three vectors (two input, one output).
    T* dev_a = CreateArrayInDeviceMemory<T>(length, success);
    T* dev_b = CreateArrayInDeviceMemory<T>(length, success);
    T* dev_c = CreateArrayInDeviceMemory<T>(length, success);
    if (!success) {
        fprintf(stderr, "cudaMalloc failed.");
        goto Error;
    }

    // Copy input vectors from host memory to GPU buffers.
    CopyArray_HostToDevice<T>(a, dev_a, length, success);
    CopyArray_HostToDevice<T>(b, dev_b, length, success);
    if (!success) {
        fprintf(stderr, "cudaMemcpy failed.");
        goto Error;
    }

    // Launch a kernel on the GPU with one thread for each element.
    addKernel<<<1, length>>>(dev_c, dev_a, dev_b, length);

    // Check for any errors launching the kernel
    cudaStatus = cudaGetLastError();
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "addKernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
        goto Error;
    }
    
    // cudaDeviceSynchronize waits for the kernel to finish, and returns
    // any errors encountered during the launch.
    cudaStatus = cudaDeviceSynchronize();
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaDeviceSynchronize returned error code %d after launching addKernel!\n", cudaStatus);
        goto Error;
    }

    // Copy output vector from GPU buffer to host memory.
    cudaStatus = cudaMemcpy(c, dev_c, length * sizeof(T), cudaMemcpyDeviceToHost);
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaMemcpy failed!");
        goto Error;
    }

Error:
    cudaFree(dev_c);
    cudaFree(dev_a);
    cudaFree(dev_b);
    
    return success;
}

template<class T> int axpy(T* y, const T* x, const T alpha, const unsigned int start, const unsigned int end)
{
    bool success = true;
    int exitcode = SUCCESS;

    // Choose which GPU to run on, change this on a multi-GPU system.
    if (cudaSetDevice(0) != cudaSuccess) {
        return ERR_SET_DEVICE; // No cuda-compatible GPU device detected
    }

    // Allocate GPU buffers for three vectors (two input, one output).
    T* dev_y = CreateArrayInDeviceMemory<T>(end, success);
    T* dev_x = CreateArrayInDeviceMemory<T>(end, success);
    if (!success) {
        exitcode = ERR_MALLOC; // unable to allocate required memory on device.
        goto Error;
    }

    // Copy input vectors from host memory to GPU buffers.
    CopyArray_HostToDevice<T>(y, dev_y, end, success);
    CopyArray_HostToDevice<T>(x, dev_x, end, success);
    if (!success) {
        exitcode = ERR_MEMCPY;
        goto Error;
    }

    // Launch a kernel on the GPU with one thread for each element.
    axpyKernel<<<1, end>>>(dev_y, dev_x, alpha, start, end);

    // Check for any errors launching the kernel
    if (cudaGetLastError() != cudaSuccess) {
        exitcode = ERR_KERNEL;  // error encountered while launching the kernel ... cudaGetErrorString(cudaStatus);
        goto Error;
    }

    if (cudaDeviceSynchronize() != cudaSuccess) {
        exitcode = ERR_SYNC;  // error encountered after kernel is launched.
        goto Error;
    }

    // Copy output vector from GPU buffer to host memory.
    if (cudaMemcpy(y, dev_y, end * sizeof(T), cudaMemcpyDeviceToHost) != cudaSuccess) {
        exitcode = ERR_MEMCPY;  // error encountered while copying from device to host
        goto Error;
    }

Error:
    cudaFree(dev_y);
    cudaFree(dev_x);

    return exitcode;
}
template<class T> int scal(T* x, const T scalar, const unsigned int start, const unsigned int end)
{
    bool success = true;
    int exitcode = SUCCESS;

    // Choose which GPU to run on, change this on a multi-GPU system.
    if (cudaSetDevice(0) != cudaSuccess) {
        return ERR_SET_DEVICE; // No cuda-compatible GPU device detected
    }

    // Allocate GPU buffers for three vectors (two input, one output).
    T* dev_x = CreateArrayInDeviceMemory<T>(end, success);
    if (!success) {
        exitcode = ERR_MALLOC; // unable to allocate required memory on device.
        goto Error;
    }

    // Copy input vectors from host memory to GPU buffers.
    CopyArray_HostToDevice<T>(x, dev_x, end, success);
    if (!success) {
        exitcode = ERR_MEMCPY;
        goto Error;
    }

    // Launch a kernel on the GPU with one thread for each element.
    scalKernel<<<1, end>>>(dev_x, scalar, start, end);

    // Check for any errors launching the kernel
    if (cudaGetLastError() != cudaSuccess) {
        exitcode = ERR_KERNEL;  // error encountered while launching the kernel ... cudaGetErrorString(cudaStatus);
        goto Error;
    }

    if (cudaDeviceSynchronize() != cudaSuccess) {
        exitcode = ERR_SYNC;  // error encountered after kernel is launched.
        goto Error;
    }

    // Copy output vector from GPU buffer to host memory.
    if (cudaMemcpy(x, dev_x, end * sizeof(T), cudaMemcpyDeviceToHost) != cudaSuccess) {
        exitcode = ERR_MEMCPY;  // error encountered while copying from device to host
        goto Error;
    }

Error:
    cudaFree(dev_x);

    return exitcode;
}

extern "C" {
    int __declspec(dllexport) __stdcall addvect_int32(__int32* c, const __int32* a, const __int32* b, const unsigned int len) {
        return add<int>(c, a, b, len);
    }
    int __declspec(dllexport) __stdcall addvect_int64(__int64* c, const  __int64* a, const  __int64* b, const unsigned int len) {
        return add<__int64>(c, a, b, len);
    }
    int __declspec(dllexport) __stdcall addvect_float32(float* c, const float* a, const float* b, const unsigned int len) {
        return add<float>(c, a, b, len);
    }
    int __declspec(dllexport) __stdcall addvect_float64(double* c, const double* a, const double* b, const unsigned int len) {
        return add<double>(c, a, b, len);
    }

    /* AXPY exports */
    int __declspec(dllexport) __stdcall axpy_int32(__int32* y, const __int32* x, const __int32 alpha, const unsigned int start, const unsigned int end) {
        return axpy<__int32>(y, x, alpha, start, end);
    }
    int __declspec(dllexport) __stdcall axpy_int64(__int64* y, const __int64* x, const __int64 alpha, const unsigned int start, const unsigned int end) {
        return axpy<__int64>(y, x, alpha, start, end);
    }
    int __declspec(dllexport) __stdcall axpy_float32(float* y, const float* x, const float alpha, const unsigned int start, const unsigned int end) {
        return axpy<float>(y, x, alpha, start, end);
    }
    int __declspec(dllexport) __stdcall axpy_float64(double* y, const double* x, const double alpha, const unsigned int start, const unsigned int end) {
        return axpy<double>(y, x, alpha, start, end);
    }

    /* SCAL exports */
    int __declspec(dllexport) __stdcall scal_int32(__int32* x, const __int32 scalar, const unsigned int start, const unsigned int end) {
        return scal<__int32>(x, scalar, start, end);
    }
    int __declspec(dllexport) __stdcall scal_int64(__int64* x, const __int64 scalar, const unsigned int start, const unsigned int end) {
        return scal<__int64>(x, scalar, start, end);
    }
    int __declspec(dllexport) __stdcall scal_float32(float* x, const float scalar, const unsigned int start, const unsigned int end) {
        return scal<float>(x, scalar, start, end);
    }
    int __declspec(dllexport) __stdcall scal_float64(double* x, const double scalar, const unsigned int start, const unsigned int end) {
        return scal<double>(x, scalar, start, end);
    }
}