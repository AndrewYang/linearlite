

#include <cuda_runtime.h>
#include <cublas_v2.h>
#include <random>
#include <iostream>
#include <helper_cuda.h>

// Response codes
constexpr unsigned int SUCCESS = 0;
constexpr unsigned int ERR_SET_DEVICE = 1;
constexpr unsigned int ERR_MALLOC = 2;
constexpr unsigned int ERR_MEMCPY = 3;
constexpr unsigned int ERR_HANDLE_CREATE = 4;
constexpr unsigned int ERR_KERNEL = 5;
constexpr unsigned int ERR_SYNC = 6;
constexpr unsigned int ERR_HANDLE_DESTROY = 7;

#define cui const unsigned int

/* float wrapper */
cublasStatus_t gemm(
	cublasHandle_t handle,
	const float* alpha,
	const float* beta,
	const float* A,
	const float* B,
	float* C,
	const int m,
	const int n,
	const int p) {
	return cublasSgemm_v2(handle, CUBLAS_OP_N, CUBLAS_OP_N, p, m, n, alpha, B, p, A, n, beta, C, p);
}
/* double wrapper */
cublasStatus_t gemm(
	cublasHandle_t handle,
	const double* alpha,
	const double* beta,
	const double* A,
	const double* B,
	double* C,
	const int m,
	const int n,
	const int p) {
	return cublasDgemm_v2(handle, CUBLAS_OP_N, CUBLAS_OP_N, p, m, n, alpha, B, p, A, n, beta, C, p);
}
/* complex wrapper */
cublasStatus_t gemm(
	cublasHandle_t handle,
	const cuDoubleComplex* alpha,
	const cuDoubleComplex* beta,
	const cuDoubleComplex* A,
	const cuDoubleComplex* B,
	cuDoubleComplex* C,
	const int m,
	const int n,
	const int p) {
	return cublasZgemm_v2(handle, CUBLAS_OP_N, CUBLAS_OP_N, p, m, n, alpha, B, p, A, n, beta, C, p);
}


template<class T> int gemm(
	int devID,
	const T* A, const T* B, T* C, // C = A * B
	cui m, cui n, cui p,
	const T alpha, const T beta,
	cui block_size)
{
	if (devID < 0) {
		// find the device with the highest GFLOPs
		devID = gpuGetMaxGflopsDeviceId();
	}
	if (cudaSetDevice(devID) != cudaSuccess) return ERR_SET_DEVICE;

	// allocate device memory
	T* A_device = {}, * B_device = {}, * C_device = {};
	cui size_C = m * p;
	cui mem_size_A = sizeof(T) * m * n;
	cui mem_size_B = sizeof(T) * n * p;
	const unsigned int mem_size_C = sizeof(T) * size_C;

	if (cudaMalloc((void**)&A_device, mem_size_A) != cudaSuccess) {
		cudaFree(A_device);
		return ERR_MALLOC;
	}
	if (cudaMalloc((void**)&B_device, mem_size_B) != cudaSuccess) {
		cudaFree(A_device);
		cudaFree(B_device);
		return ERR_MALLOC;
	}
	if (cudaMemcpy(A_device, A, mem_size_A, cudaMemcpyHostToDevice) != cudaSuccess ||
		cudaMemcpy(B_device, B, mem_size_B, cudaMemcpyHostToDevice) != cudaSuccess) {
		cudaFree(A_device);
		cudaFree(B_device);
		return ERR_MEMCPY;
	}

	unsigned int exitcode = SUCCESS;
	if (cudaMalloc((void**)&C_device, mem_size_C) != cudaSuccess) {
		exitcode = ERR_MALLOC;
		goto Error;
	}

	const dim3 threads(block_size, block_size);
	const dim3 grid(p / threads.x, m / threads.y);

	// Create the handle
	cublasHandle_t handle;
	if (cublasCreate_v2(&handle) != cudaSuccess) {
		exitcode = ERR_HANDLE_CREATE;
		goto Error;
	}

	// Since CUBLAS accepts matrices in column-major order, we calculate C := AB = (B^T * A^T)^T.
	if (gemm(handle, &alpha, &beta, A_device, B_device, C_device, m, n, p) != cudaSuccess) {
		exitcode = ERR_KERNEL;
		goto Error;
	}

	// Copy device matrix -> host matrix.
	if (cudaMemcpy(C, C_device, mem_size_C, cudaMemcpyDeviceToHost) != cudaSuccess) {
		exitcode = ERR_MEMCPY;
		goto Error;
	}

	// Destroy the handle
	if (cublasDestroy_v2(handle) != cudaSuccess) {
		exitcode = ERR_HANDLE_DESTROY;
		goto Error;
	}

Error:
	cudaFree(A_device);
	cudaFree(B_device);
	cudaFree(C_device);

	return exitcode;
}

/*
// Allocates a matrix with random float entries.
void randomInit(float* data, int size)
{
	for (int i = 0; i < size; ++i)
		data[i] = rand() / (float)RAND_MAX;
}

int main(int argc, char** argv)
{
	// set seed for rand()
	srand(2006);

	const unsigned int m = 4096, n = 4096, p = 4096;

	// allocate host memory for matrices A and B
	const unsigned int size_A = m * n;
	float* h_A = new float[size_A];

	const unsigned int size_B = n * p;
	float* h_B = new float[size_B];

	const unsigned int size_C = m * p;
	float* h_C = new float[size_C];

	// set seed for rand()
	srand(2006);

	// initialize host memory
	randomInit(h_A, size_A);
	randomInit(h_B, size_B);

	int exitcode = gemm<float>(0, h_A, h_B, h_C, m, n, p, 1.0f, 0.0f, 32);
	std::cout << exitcode << std::endl;

	delete[] h_A, h_B, h_C;
}
*/

extern "C" {
	int __declspec(dllexport) __stdcall gemm_float32(int devID, const float* A, const float* B, float* C, cui m, cui n, cui p, const float alpha, const float beta, cui block_size) {
		return gemm<float>(devID, A, B, C, m, n, p, alpha, beta, block_size);
	}
	int __declspec(dllexport) __stdcall gemm_float64(int devID, const double* A, const double* B, double* C, cui m, cui n, cui p, const double alpha, const double beta, cui block_size) {
		return gemm<double>(devID, A, B, C, m, n, p, alpha, beta, block_size);
	}
}