#include <algorithm>
#include <iterator>

using namespace std;

class Matrix {
    public:
        // Constructors
        Matrix(const int rows, const int columns, double* values);

        // Static Methods
        static Matrix random(int rows, int columns);

        // Instance Methods
        int get_rows();
        int get_columns();
        double* get_values();
        void multiply_in_place(double scalar);
        void transpose_in_place();

    private:
        double* values;
        int rows;
        int columns;
};

/* Constructors */
Matrix::Matrix(const int rows, const int columns, double* values) {
    this->rows = rows;
    this->columns = columns;
    this->values = values;
}

/* Instance methods */

int Matrix::get_rows() { return this->rows; }
int Matrix::get_columns() { return this->columns; }
double* Matrix::get_values() { return this->values; }

/* Multiply this matrix by a scalar, in place */
void Matrix::multiply_in_place(double scalar) {
    int len = this->rows * this->columns, i;
    double* values = this->values;

    for (i = 0; i < len; ++i) {
        values[i] *= scalar;
    }
}
/* Transpose this matrix in place. */
void Matrix::transpose_in_place() {
    int rows = this->rows, cols = this->columns, i, j;
    double* values = this->values;

    // No workspace required if matrix is square
    if (rows == cols) {
        for (i = 0; i < rows; ++i) {
            for (j = 0; j < i; ++j) {
                int i1 = j * columns + i, i2 = i * columns + j;
                double tmp = values[i1];
                values[i1] = values[i2];
                values[i2] = tmp;
            }
        }
    }
    else {

        // Requires workspace or rectangular matrices
        double* transpose = new double[rows * cols];
        int k = 0;
        for (i = 0; i < rows; ++i) {
            for (j = 0; j < cols; ++j) {
                transpose[j * rows + i] = values[k++];
            }
        }
        // Copy back into values array
        memcpy(values, transpose, rows * cols * sizeof(double));
        delete [] transpose;
    }
    
}

