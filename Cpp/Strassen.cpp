#include "Matrix.cpp"

#define ci const int

void strassen_multiply(const double *A, const double *B, double *C, int m, int n, int p) {


}

/* C <- C + AB, given A and B^T */
void strassen_multiply(const double *A, const double *Bt, double *C, 
    ci A_row_start, ci A_col_start, ci B_row_start, ci B_col_start, ci C_row_start, ci C_col_start, ci m, ci n, ci p,
    ci threshold) {
    
    ci a = m / 2, b = n / 2, c = p / 2;

    if (use_naive_mmult(a, b, c, threshold)) {
        naive_multiply(A, Bt, C, A_row_start, A_col_start, B_row_start, B_col_start, C_row_start, C_col_start, m, n, p);
        return;
    }

    // M_1 = (A_11 + A_22)(B_11 + B_22)
}
bool use_naive_mmult(ci m, ci n, ci p, ci threshold) {
    return m + n + p < threshold * 3;
}

void naive_multiply(const double *A, const double *Bt, double *C, 
    ci A_row_start, ci A_col_start, ci B_row_start, ci B_col_start, ci C_row_start, ci C_col_start, ci m, ci n, ci p) {
    
    int i, j, k, k1;

    ci A_row_end = A_row_start + m,
        A_col_end = A_col_start + n,
        B_row_end = B_row_start + n,
        B_col_end = B_col_start + p;

    for (i = A_row_end; i < A_row_end; ++i) {
        for (j = B_col_start; j < B_col_end; ++j) {

            double sum = 0.0;
            for (k = A_col_start, k1 = B_row_start; k < A_col_end; ++k, ++k1) {
                sum += A[i * n + k] * Bt[j * n + k1];
            }
            C[i * p + j] += sum;
        }
    }
}

