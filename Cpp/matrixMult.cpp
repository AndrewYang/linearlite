#include <iostream>
#include <vector>
#include <string>
#include <random>
#include <chrono>
#include "Matrix.cpp"

using namespace std;

double* random_matrix(const int, const int);
void multiply(const double*, const double*, double*, const int, const int, const int);
void multiply_using_transpose(const double*, const double*, double*, double*, const int, const int, const int);
void multiply_using_block_transpose(const double*, const double*, double*, double*, const int, const int, const int, const int);
void print(const double*, const int, const int);
void transpose(const double*, double*, const int, const int);

int main()
{
    const int m = 2000, n = 2000, p = 2000;
    double* A = random_matrix(m, n);
    double* B = random_matrix(n, p);
    double* C = new double[m * p];
    double* Bt = new double[n * p];

    double* a = random_matrix(10, 10);
    double* at = new double[10 * 10];
    transpose(a, at, 10, 10);
    cout << "A" << endl;
    print(a, 10, 10);
    cout << "At" << endl;
    print(at, 10, 10);

    auto start = std::chrono::high_resolution_clock::now();
    transpose(B, Bt, n, p);
    auto finish = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> diff = finish - start;
    cout << "<" << n << "," << p << "> transposition in " << diff.count() << " s" << endl;

    start = std::chrono::high_resolution_clock::now();
    multiply(A, B, C, m, n, p);
    finish = std::chrono::high_resolution_clock::now();
    diff = finish - start;
    cout << "<" << m << "," << n << "," << p << "> multiplication in " << diff.count() << " s" << endl;

    start = std::chrono::high_resolution_clock::now();
    multiply_using_transpose(A, B, Bt, C, m, n, p);
    finish = std::chrono::high_resolution_clock::now();
    diff = finish - start;
    cout << "<" << m << "," << n << "," << p << "> transposed multiplication in " << diff.count() << " s" << endl;

    start = std::chrono::high_resolution_clock::now();
    multiply_using_block_transpose(A, B, Bt, C, m, n, p, 50);
    finish = std::chrono::high_resolution_clock::now();
    diff = finish - start;
    cout << "<" << m << "," << n << "," << p << "> block transposed multiplication (40) in " << diff.count() << " s" << endl;

    delete [] A, B, Bt, C;
}

double* random_matrix(const int rows, const int columns) {

    uniform_real_distribution<double> unif(-1.0, 1.0);
    default_random_engine re;

    int size = rows * columns, i;
    double* a = new double[size];
    for (i = 0; i < size; ++i) {
        a[i] = unif(re);
    }

    return a;
}

/*
Multiply C <- A * B
*/
void multiply(const double* A, const double* B, double* C, const int m, const int n, const int p) {
    int i, j, k, ai = 0, ci = 0;
    for (i = 0; i < m; ++i) {
        ai = i * n;
        for (j = 0; j < p; ++j) {
            double dot = 0.0;
            for (k = 0; k < n; ++k) {
                dot += A[ai + k] * B[k * p + j];
            }
            C[ci++] = dot;
        }
    }
}
/*
Multiply C <- A * B, by first transposing B and storing it in a workspace
*/
void multiply_using_transpose(const double* A, const double* B, double* Bt_workspace, double* C, const int m, const int n, const int p) {
    transpose(B, Bt_workspace, n, p);

    int i, j, k, ai, bi, ci = 0;
    for (i = 0; i < m; ++i) {
        ai = i * n;
        for (j = 0; j < p; ++j) {
            bi = j * n;
            double dot = 0.0;
            for (k = 0; k < n; ++k) {
                dot += *(A + ai + k) * *(Bt_workspace + bi + k);
            }
            C[ci++] = dot;
        }
    }
}
/*
Multiply C <- A * B, using block multiplication for greater cache efficiency, and by first transposing B and storing 
it in a workspace
*/
void multiply_using_block_transpose(const double* A, const double* B, double* Bt_ws, double* C, const int m, const int n, const int p, const int blockSize) {

    transpose(B, Bt_ws, n, p);

    if (m % blockSize != 0) throw "m is not divisible by the block size";
    if (n % blockSize != 0) throw "n is not divisible by the block size";
    if (p % blockSize != 0) throw "p is not divisible by the block size";

    int i, j, k, x_end, y_end, z_end, x, y, z, xn, yn, xp;
    for (i = 0; i < m; i += blockSize) {

        x_end = i + blockSize;
        for (j = 0; j < p; j += blockSize) {
            
            y_end = j + blockSize;
            for (k = 0; k < n; k += blockSize) {

                z_end = k + blockSize;

                // Process a single block
                for (x = i; x < x_end; ++x) {

                    xn = x * n;
                    xp = x * p;
                    for (y = j; y < y_end; ++y) {

                        yn = y * n;
                        double sum = 0.0;
                        for (z = k; z < z_end; ++z) {
                            sum += A[xn + z] * Bt_ws[yn + z];
                        }
                        C[xp + y] += sum;
                    }
                }
            }
        }
    }
}

void print(const double* A, const int rows, const int cols) {
    int i, j;
    for (i = 0; i < rows; ++i) {
        for (j = 0; j < cols; ++j) {
            cout << A[i * cols + j] << "\t";
        }
        cout << endl;
    }
}

void transpose(const double* A, double* At, const int rows, const int cols) {
    int i, j, k = 0;
    for (i = 0; i < rows; ++i) {
        for (j = 0; j < cols; ++j) {
            At[j * rows + i] = A[k++];
        }
    }
}