﻿using LinearNet.Global;
using LinearNet.Structs;
using LinearNet.Structs.Fields;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Functions
{
    /// <cat>numerics</cat>
    public static partial class MathFunctions
    {
        /// <summary>
        /// <para>
        /// Calculate the first $n$ Bernoulli numbers exactly, storing them in the first $n$ entries of the array <txt>bernoulli</txt>.
        /// </para>
        /// <para>
        /// Due to the fast growth of the numerator and denominators of Bernoulli numbers, 
        /// this method overflows for $n > 14$. For larger values of $n$, please use the equivalent method for <txt>BigRational</txt> or
        /// use <txt>double</txt> if a finite-precision approximation is sufficient.
        /// </para>
        /// </summary>
        /// <name>Bernoulli_Rational</name>
        /// <proto>void Bernoulli(Rational[] bernoulli, int n = -1, bool minus = true)</proto>
        /// <cat>functions</cat>
        /// <param name="bernoulli">
        /// The array in which to store the calculated Bernoulli numbers. Array indices in the range $0, 1, ..., n - 1$ will be altered; 
        /// the other indices (if any) will remain unchanged.
        /// </param>
        /// <param name="n">
        /// <b>Optional</b>, defaults to <txt>-1</txt>.
        /// The number of Bernoulli terms to calculate. If equal to <txt>-1</txt>, all entries of <txt>bernoulli</txt>
        /// will be filled. The length of the <txt>bernoulli</txt> array must be at least 
        /// <txt>n</txt>.
        /// </param>
        /// <param name="minus">
        /// <b>Optional</b>: defaults to <txt>true</txt>.<br/>
        /// If <txt>true</txt>, the sequence $B^-_n$ is calculated, otherwise $B^+_n$ is calculated.
        /// </param>
        public static void Bernoulli(Rational[] bernoulli, int n = -1, bool minus = true)
        {
            if (bernoulli == null)
            {
                throw new ArgumentNullException(nameof(bernoulli));
            }

            if (n < 0)
            {
                n = bernoulli.Length;
            }
            else if (n > bernoulli.Length)
            {
                throw new ArgumentOutOfRangeException(nameof(n));
            }

            if (n >= 1)
            {
                bernoulli[0] = Rational.One;
            }
            if (n >= 2)
            {
                bernoulli[1] = minus ? new Rational(-1, 2) : new Rational(1, 2);
            }

            for (int i = 2; i < n; ++i)
            {
                // All odd Bernoulli numbers above B(1) are 0
                if (i % 2 == 1)
                {
                    bernoulli[i] = Rational.Zero;
                }
                else
                {
                    int n1 = i + 1, n2 = i + 2;
                    int p = 1, q = 1;
                    Rational numerator = bernoulli[0];
                    for (int k = 1; k < i; ++k)
                    {
                        // Online generation of Choose(n + 1, k)
                        p *= (n2 - k);
                        q *= k;
                        numerator += (p / q) * bernoulli[k];
                        numerator = numerator.Simplify();
                    }
                    bernoulli[i] = -numerator / n1;
                    bernoulli[i] = bernoulli[i].Simplify();
                }
            }
        }

        /// <summary>
        /// <para>
        /// Calculate the first $n$ Bernoulli numbers in finite precision arithmetic, storing them as doubles in the first $n$ entries of the array <txt>bernoulli</txt>.
        /// </para>
        /// </summary>
        /// <name>Bernoulli_double</name>
        /// <proto>void Bernoulli(double[] bernoulli, int n = -1, bool minus = true)</proto>
        /// <cat>functions</cat>
        /// <param name="bernoulli">
        /// The array in which to store the calculated Bernoulli numbers. Array indices in the range $0, 1, ..., n - 1$ will be altered; 
        /// the other indices (if any) will remain unchanged.
        /// </param>
        /// <param name="n">
        /// The number of Bernoulli terms to calculate. If negative, all entries of the <txt>bernoulli</txt> array
        /// will be filled. If <txt>n</txt> is non-negative, the length of the <txt>bernoulli</txt> array must be at least <txt>n</txt>.
        /// </param>
        /// <param name="minus">
        /// If <txt>true</txt>, the sequence $B^-_n$ is calculated, otherwise $B^+_n$ is calculated.
        /// </param>
        public static void Bernoulli(double[] bernoulli, int n = -1, bool minus = true)
        {
            if (bernoulli == null)
            {
                throw new ArgumentNullException(nameof(bernoulli));
            }

            if (n < 0)
            {
                n = bernoulli.Length;
            }
            else if (n > bernoulli.Length)
            {
                throw new ArgumentOutOfRangeException(nameof(n));
            }

            if (n >= 1)
            {
                bernoulli[0] = 1;
            }
            if (n >= 2)
            {
                bernoulli[1] = minus ? -0.5 : 0.5;
            }

            for (int i = 2; i < n; ++i)
            {
                // All odd Bernoulli numbers above B(1) are 0
                if (i % 2 == 1)
                {
                    bernoulli[i] = 0.0;
                }
                else
                {
                    int n1 = i + 1, n2 = i + 2;
                    double p = 1.0, q = 1.0;
                    double numerator = bernoulli[0];
                    for (int k = 1; k < i; ++k)
                    {
                        // Online generation of Choose(n + 1, k)
                        p *= (n2 - k);
                        q *= k;
                        numerator += (p / q) * bernoulli[k];
                    }
                    bernoulli[i] = -numerator / n1;
                }
            }
        }

        /// <summary>
        /// Calculates the first $n$ Bernoulli numbers $B_0, ..., B_{n-1}$ exactly, for some positive integer $n$.
        /// <para>
        /// The Bernoulli numbers are stored in the indices $0, 1, ..., {n - 1}$ respectively in the array '<txt>bernoulli</txt>'.
        /// </para>
        /// <para>
        /// The first few terms of this sequence is $B_n^{±} = \{1, ±\frac{1}{2}, \frac{1}{6}, 0, -\frac{1}{30}, ...\}$</para>
        /// <para>
        /// Also see: <a href="#EvenBernoulli"><txt>EvenBernoulli</txt></a>.
        /// </para>
        /// <!--inputs-->
        /// </summary>
        /// <name>Bernoulli</name>
        /// <proto>void Bernoulli(BigRational[] bernoulli, int n = -1, bool minus = true)</proto>
        /// <cat>functions</cat>
        /// <param name="bernoulli">
        /// The array in which to store the calculated Bernoulli numbers. Array indices in the range $0, 1, ..., n - 1$ will be altered; 
        /// the other indices (if any) will remain unchanged.
        /// </param>
        /// <param name="n">
        /// <b>Optional</b>, defaults to <txt>-1</txt>.
        /// The number of Bernoulli terms to calculate. If equal to <txt>-1</txt>, all entries of <txt>bernoulli</txt>
        /// will be filled. The length of the <txt>bernoulli</txt> array must be at least 
        /// <txt>n</txt>.
        /// </param>
        /// <param name="minus">
        /// <b>Optional</b>: defaults to <txt>true</txt>.<br/>
        /// If <txt>true</txt>, the sequence $B^-_n$ is calculated, otherwise $B^+_n$ is calculated.
        /// </param>
        public static void Bernoulli(BigRational[] bernoulli, int n = -1, bool minus = true)
        {
            if (bernoulli == null)
            {
                throw new ArgumentNullException(nameof(bernoulli));
            }

            if (n < 0)
            {
                n = bernoulli.Length;
            }

            if (n > bernoulli.Length)
            {
                throw new ArgumentOutOfRangeException(nameof(n));
            }

            if (n >= 1)
            {
                bernoulli[0] = 1; 
            }
            if (n >= 2)
            {
                bernoulli[1] = minus ? new BigRational(-1, 2) : new BigRational(1, 2);
            }
            for (int i = 2; i < n; ++i)
            {
                // All odd Bernoulli numbers above B(1) are 0
                if (i % 2 == 1)
                {
                    bernoulli[i] = BigRational.Zero; 
                }
                else
                {
                    int n1 = i + 1, n2 = i + 2;
                    BigInteger p = 1, q = 1;
                    BigRational numerator = bernoulli[0];
                    for (int k = 1; k < i; ++k)
                    {
                        // Online generation of Choose(n + 1, k)
                        p *= (n2 - k);
                        q *= k;

                        numerator += (p / q) * bernoulli[k];
                    }
                    bernoulli[i] = -numerator / n1;
                }
            }
        }

        /// <summary>
        /// Calculates the first $n$ Bernoulli numbers approximately, accurate up to '<txt>sigFigs</txt>' significant figures.
        /// </summary>
        /// <name>Bernoulli_BigDecimal</name>
        /// <proto>void Bernoulli(BigDecimal[] bernoulli, int n = -1, bool minus = true)</proto>
        /// <cat>functions</cat>
        /// <param name="sigFigs"></param>
        /// <param name="bernoulli">
        /// The array in which to store the calculated Bernoulli numbers. Array indices in the range $0, 1, ..., n - 1$ will be altered; 
        /// the other indices (if any) will remain unchanged.
        /// </param>
        /// <param name="sigFigs">
        /// The number of significant figures to which the calculated Bernoulli numbers should be accurate to.
        /// </param>
        /// <param name="n">
        /// The number of Bernoulli terms to calculate. If equal to <txt>-1</txt>, all entries of <txt>bernoulli</txt>
        /// will be filled. The length of the <txt>bernoulli</txt> array must be at least <txt>n</txt>.
        /// </param>
        /// <param name="minus">
        /// If <txt>true</txt>, the sequence $B^-_n$ is calculated, otherwise $B^+_n$ is calculated.
        /// </param>
        public static void Bernoulli(BigDecimal[] bernoulli, int sigFigs, int n = -1, bool minus = true)
        {
            if (bernoulli == null)
            {
                throw new ArgumentNullException(nameof(bernoulli));
            }

            if (n < 0)
            {
                n = bernoulli.Length;
            }

            if (n > bernoulli.Length)
            {
                throw new ArgumentOutOfRangeException(nameof(n));
            }

            BigDecimal one = new BigDecimal(1, sigFigs);
            if (n >= 1)
            {
                bernoulli[0] = one;
            }
            if (n >= 2)
            {
                BigDecimal half = one / new BigDecimal(2, sigFigs);
                bernoulli[1] = minus ? -half : half;
            }
            for (int i = 2; i < n; ++i)
            {
                // All odd Bernoulli numbers above B(1) are 0
                if (i % 2 == 1)
                {
                    bernoulli[i] = new BigDecimal(0, sigFigs);
                }
                else
                {
                    int n1 = i + 1, n2 = i + 2;
                    BigInteger p = 1, q = 1;
                    BigDecimal numerator = bernoulli[0];
                    for (int k = 1; k < i; ++k)
                    {
                        // Online generation of Choose(n + 1, k)
                        p *= (n2 - k);
                        q *= k;

                        numerator += new BigDecimal(p / q, sigFigs) * bernoulli[k];
                    }
                    bernoulli[i] = -numerator / n1;
                }
            }
        }

        /// <summary>
        /// Calculate and returns the $n$-th Bernoulli number, $B_n$.
        /// <!--inputs-->
        /// </summary>
        /// <param name="n">The index of the Bernoulli number to return</param>
        /// <param name="minus">
        /// If true, the $n$-th term of $B^-$ will be returned, else $B^+$ will be returned.
        /// The two sequences differ only in their second term, $B_1$.
        /// </param>
        /// <returns>The $n$-th Bernoulli number $B^{\pm}_n$</returns>
        public static BigRational Bernoulli(int n, bool minus = true)
        {
            if (n < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(n), ExceptionMessages.NegativeNotAllowed);
            }

            if (n == 0)
            {
                return BigRational.One;
            }

            if (n % 2 == 1)
            {
                if (n == 1)
                {
                    return minus ? new BigRational(-1, 2) : new BigRational(1, 2);
                }
                return BigRational.Zero;
            }

            BigInteger[] powers = new BigInteger[n + 1];
            for (int k = 0; k <= n; ++k)
            {
                powers[k] = BigInteger.Pow(k, n);
            }

            BigRational Bn = BigRational.Zero;
            for (int k = 0; k <= n; ++k)
            {
                BigInteger num = k % 2 == 0 ? powers[k] : -powers[k];
                BigInteger p = 1, q = 1;
                for (int v = 1; v < k; ++v)
                {
                    p *= (k - v + 1);
                    q *= v;

                    // Even division is guaranteed
                    BigInteger term = p / q * powers[v];
                    num += (v % 2 == 0 ? term : -term);
                }

                Bn += new BigRational(num, k + 1);
            }
            return Bn;
        }

        /// <summary>
        /// Given an array of at least length $n$, containing the even Bernoulli numbers $B_0, ..., B_{2m - 2}$ in the indices $0, ..., m - 1$ respectively,
        /// calculates and stores the even Bernoulli numbers $B_{2m}, ..., B_{2n - 2}$ in the indices $m, ..., n - 1$ for some positive integers $m < n$.
        /// </summary>
        /// <name>EvenBernoulli</name>
        /// <proto>void EvenBernoulli(BigRational[] bernoulli, int m, int n)</proto>
        /// <param name="bernoulli">
        /// The array of at least length $n$, which holds the first $m$ even Bernoulli numbers in indices $0, ..., m - 1$, and 
        /// which will contain the first $n$ Bernoulli numbers upon the completion of this method, for $n > m$.
        /// </param>
        /// <param name="m">
        /// The number of even Bernoulli numbers that the array <txt>Bernoulli</txt> holds prior to the 
        /// execution of this method. Must be less than $n$.
        /// </param>
        /// <param name="n">
        /// The number of even Bernoulli numbers that the array <txt>Bernoulli</txt> will hold after the 
        /// completion of this method. Must be greater than $m$.
        /// </param>
        public static void EvenBernoulli(BigRational[] bernoulli, int m, int n)
        {
            if (bernoulli == null)
            {
                throw new ArgumentNullException(nameof(bernoulli));
            }
            if (n > bernoulli.Length)
            {
                throw new ArgumentOutOfRangeException(nameof(n));
            }
            if (m > n)
            {
                throw new ArgumentOutOfRangeException(nameof(m) + " > " + nameof(n));
            }

            if (n >= 1)
            {
                bernoulli[0] = 1;
            }

            BigRational half = new BigRational(-1, 2);
            for (int i = m; i < n; ++i)
            {
                int n1 = i * 2 + 1;
                BigInteger p = n1, q = 1;
                BigRational numerator = bernoulli[0] + n1 * half; // B(0) + (n + 1) * B(1)
                for (int k = 2; k < n1 - 1; ++k)
                {
                    // Online generation of Choose(n1, k)
                    p *= (n1 - k + 1);
                    q *= k;

                    if (k % 2 == 0)
                    {
                        numerator += (p / q) * bernoulli[k / 2];
                    }
                }
                bernoulli[i] = -numerator / n1;
            }
        }
    }
}
