﻿using LinearNet.Global;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Functions
{
    ///<cat>numerics</cat>
    public static partial class MathFunctions
    {
        /// <summary>
        /// Returns the order $\alpha$ Bessel function of the first kind evaluated at some real value $x$, $J_{\alpha}(x)$
        /// </summary>
        /// <param name="alpha">The order, $\alpha$, of the Bessel function.</param>
        /// <param name="x">The value at which to evaluate the Bessel function.</param>
        /// <param name="N">The number of terms to include in the summation.</param>
        /// <returns>$J_{\alpha}(x)$</returns>
        public static double BesselJ(double alpha, double x, int N = 100)
        {
            if (N < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(N), ExceptionMessages.NegativeNotAllowed);
            }
            if (x < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(x), ExceptionMessages.NegativeNotAllowed);
            }

            double sum = 0.0;
            for (int m = 0; m < N; ++m)
            {
                double term = Math.Exp((2 * m + alpha) * Math.Log(x / 2) - LogFactorial(m) - LogGamma(m + alpha + 1));
                sum += m % 2 == 0 ? term : -term;
            }
            return sum;
        }
    }
}
