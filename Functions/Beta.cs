﻿using LinearNet.Global;
using LinearNet.Structs;
using LinearNet.Structs.Fields;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Functions
{
    /// <cat>numerics</cat>
    public static partial class MathFunctions
    {
        /// <summary>
        /// Calculates the Euler beta function for two real numbers $p, q$,
        /// $$B(p, q) = \frac{\Gamma(p)\Gamma(q)}{\Gamma(p + q)}$$
        /// where $\Gamma(\cdot)$ is the <a href="#Gamma">Gamma</a> function.
        /// </summary>
        /// <name>Beta</name>
        /// <proto>double Beta(double p, double q)</proto>
        /// <cat>functions</cat>
        /// <param name="p">The first parameter of the beta function.</param>
        /// <param name="q">The second parameter of the beta function.</param>
        /// <returns>$B(p, q)$</returns>
        public static double Beta(double p, double q)
        {
            double logBeta = LogBeta(p, q);

            if (double.IsNaN(logBeta))
            {
                return double.NaN;
            }
            if (double.IsNegativeInfinity(logBeta))
            {
                return 0.0;
            }

            return Math.Exp(logBeta);
        }
        /// <summary>
        /// Calculates the Euler beta function for two <txt>BigDecimal</txt>
        /// </summary>
        /// <param name="p"></param>
        /// <param name="q"></param>
        /// <param name="sigFigs"></param>
        /// <returns></returns>
        public static BigDecimal Beta(BigDecimal p, BigDecimal q, int sigFigs = -1)
        {
            if (p.IsNegative)
            {
                throw new ArgumentOutOfRangeException(nameof(p), ExceptionMessages.NegativeNotAllowed);
            }
            if (q.IsNegative)
            {
                throw new ArgumentOutOfRangeException(nameof(q), ExceptionMessages.NegativeNotAllowed);
            }

            if (sigFigs < 0)
            {
                sigFigs = Math.Min(p.SignificantFigures, q.SignificantFigures);
            }

            int ch = 2;
            sigFigs += ch;

            BigDecimal beta = Gamma(p, sigFigs) * Gamma(q, sigFigs) / Gamma(p + q, sigFigs);
            beta.Round(sigFigs - ch);
            return beta;
        }

        public static double LogBeta(double p, double q)
        {
            if (p < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(p), ExceptionMessages.NegativeNotAllowed);
            }
            if (q < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(q), ExceptionMessages.NegativeNotAllowed);
            }
            if (double.IsNaN(p) || double.IsNaN(q))
            {
                return double.NaN;
            }

            // If p and q are integers (32-bit), we use permutation
            // methods instead of the gamma function
            // It is debatable whether this process offers any value in terms 
            // of numerical stability. TODO: benchmark this.
            if ((p + q).IsInt32() && p.IsInt32())
            {
                // Choose the maximum of p and q to increase cancellation - ensure that p is maximum
                if (p < q)
                {
                    Util.Swap(ref p, ref q);
                }

                // Gamma(p + q) / Gamma(p) = (p + q - 1)! / (p - 1)! = P(p + q + 1, q)
                return LogFactorial((q - 1).ToInt32()) - LogPermutations((p + q).ToInt32() - 1, q.ToInt32());
            }
            else
            {
                return LogGamma(p) + LogGamma(q) - LogGamma(p + q);
            }
        }

        /// <summary>
        /// Calculates the incomplete beta function 
        /// $$B(x; p, q) = \int_0^x t^{p-1}(1-t)^{q-1} dt, \enspace 0\le{x}\le{1} $$
        /// <para>
        /// This function reduces to the beta function $B(p, q)$ for $x = 1$.
        /// </para>
        /// </summary>
        /// <name>IncompleteBeta</name>
        /// <proto>double IncompleteBeta(double x, double p, double q)</proto>
        /// <cat>functions</cat>
        /// <param name="x"></param>
        /// <param name="p"></param>
        /// <param name="q"></param>
        /// <param name="N">
        /// <b>Optional</b>, defaults to 200.<br/>
        /// The maximum number of terms to include in the numerical approximation. If N < 0, the summation will 
        /// continue until other termination criteria are met. 
        /// </param>
        /// <param name="eps">
        /// <b>Optional</b>, defaults to 0. <br/>
        /// The summation will terminate if the last added term is less than <txt>eps</txt> in magnitude. 
        /// </param>
        /// <returns></returns>
        public static double IncompleteBeta(double x, double p, double q, int N = 100, double eps = 0.0)
        {
            if (x < 0 || x > 1)
            {
                throw new ArgumentOutOfRangeException(nameof(x), ExceptionMessages.NotBetween0And1);
            }
            if (N < 0)
            {
                N = int.MaxValue;
            }
            if (double.IsNaN(x))
            {
                return double.NaN;
            }

            // Poor convergence for large x, use the reflection formula
            // B(x, p, q) = B(p, q) - B(1 - x, q, p)
            if (x > 0.51)
            {
                return Beta(p, q) - IncompleteBeta(1.0 - x, q, p, N, eps);
            }

            if (x == 0.0)
            {
                return 0.0;
            }

            double logx = Math.Log(x);
            double coeff = Math.Exp(p * logx + q * Math.Log(1.0 - x) - Math.Log(p));
            double sum = 1.0;

            double logBetaRatio = LogBeta(p + 1, 1) - LogBeta(p + q, 1);
            for (int n = 0; n < N; ++n)
            {
                double term = Math.Exp(logBetaRatio + (n + 1) * logx);
                sum += term;

                if (Math.Abs(term) <= eps)
                {
                    break;
                }
                logBetaRatio += Math.Log(p + q + n + 2) - Math.Log(p + n + 3);
            }

            return coeff * sum;
        }

        /// <summary>
        /// Calculates the incomplete beta function $B(x; p, q)$ correct to a specified number of significant figures.
        /// <para>
        /// This implementation uses the series approximation
        /// $$B(x; p, q) \approx \frac{x^p(1-x)^q}{p} \Big\{ 1 + \sum_{n = 1}^{N} \frac{B(p + 1, n)}{B(p + q, n)} x^n \Big\} $$
        /// </para>
        /// <!--inputs-->
        /// </summary>
        /// <name>IncompleteBeta_BigDecimal</name>
        /// <proto>BigDecimal IncompleteBeta(BigDecimal x, BigDecimal p, BigDecimal q, int sigFigs = -1)</proto>
        /// <cat>functions</cat>
        /// <param name="x">The value at which to evaluate the incomplete beta function.</param>
        /// <param name="p">The $p$ parameter of the beta function.</param>
        /// <param name="q">The $q$ parameter of the beta function.</param>
        /// <param name="sigFigs">
        /// <b>Optional</b>, defaults to -1<br/>
        /// If negative, defaults to the minimum of the significant figures of <txt>x</txt>, <txt>p</txt> and <txt>q</txt>.
        /// </param>
        /// <returns></returns>
        public static BigDecimal IncompleteBeta(BigDecimal x, BigDecimal p, BigDecimal q, int sigFigs = -1)
        {
            // This also serves as a check for x > 1, because of the recursive reflection call
            if (x.IsNegative)
            {
                throw new ArgumentOutOfRangeException(nameof(x), ExceptionMessages.NotBetween0And1);
            }

            if (sigFigs < 0)
            {
                sigFigs = Util.Min(x.SignificantFigures, p.SignificantFigures, q.SignificantFigures);
            }

            // Account for rounding error
            sigFigs -= 2;

            // The return threshold is when the change in sum is lower than half a 
            // place value
            BigDecimal eps = new BigDecimal(5, -sigFigs, sigFigs);
            x.SignificantFigures = sigFigs;
            p.SignificantFigures = sigFigs;
            q.SignificantFigures = sigFigs;

            BigDecimal one = new BigDecimal(1, sigFigs);
            if (x > 0.51)
            {
                return Beta(p, q) - IncompleteBeta(one - x, q, p, sigFigs);
            }

            if (x.IsZero)
            {
                return new BigDecimal(0, sigFigs);
            }

            BigDecimal
                logx = BigDecimal.Log(x, sigFigs),
                log1x = BigDecimal.Log(one - x, sigFigs),
                xn = x,
                coeff = BigDecimal.Exp(p * logx + q * log1x, sigFigs) / p,
                sum = one,

                p_q_n = p + q + one,
                p_n = p + new BigDecimal(2, sigFigs),

                // Beta(p + 1, 1) / Beta(p + q, 1)
                betaRatio = Gamma(p + one, sigFigs) * Gamma(p_q_n, sigFigs) / (Gamma(p_n, sigFigs) * Gamma(p + q, sigFigs));

            int maxitr = sigFigs;
            for (int n = 0; n < maxitr; ++n)
            {
                BigDecimal term = betaRatio * xn;
                sum += term;
                if (BigDecimal.Abs(term) < eps)
                {
                    break;
                }

                p_q_n += one;
                p_n += one;
                betaRatio *= p_q_n / p_n;
                xn *= x;
            }

            BigDecimal result = coeff * sum;
            result.Round(sigFigs - 2);

            return result;
        }

        /// <summary>
        /// Calculates the beta function for two complex numbers $p, q$.
        /// </summary>
        /// <name>Beta_Complex</name>
        /// <proto>Complex Beta(Complex p, Complex q)</proto>
        /// <cat>functions</cat>
        public static Complex Beta(Complex p, Complex q)
        {
            if (p.Real <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(p));
            }
            if (q.Real <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(q));
            }

            return Complex.Exp(LogGamma(p) + LogGamma(q) - LogGamma(p + q));
        }

        /// <summary>
        /// Calculates and returns the multivariate beta function for a set of real numbers, 
        /// $$B(\alpha_1, \alpha_2, \dotsb, \alpha_n) = \frac{\Gamma(\alpha_1)\Gamma(\alpha_2)\dotsb\Gamma(\alpha_n)}{\Gamma(\alpha_1 + \dotsb + \alpha_n)}$$
        /// for a given vector representing the $\alpha_i$'s, where $\Gamma(\cdot)$ is the <a href="#Gamma">Gamma</a> function.
        /// </summary>
        /// <name>BetaMultinomial</name>
        /// <proto>double Beta(double[] alpha)</proto>
        /// <cat>functions</cat>
        /// <param name="alpha"></param>
        /// <returns></returns>
        public static double Beta(double[] alpha)
        {
            if (alpha == null)
            {
                throw new ArgumentNullException(nameof(alpha));
            }

            double ln_numerator = 0.0;
            double sum_alpha = 0.0;

            int len = alpha.Length, i;
            for (i = 0; i < len; ++i)
            {
                double a = alpha[i];
                if (a < 0)
                {
                    throw new ArgumentOutOfRangeException(nameof(alpha) + "[" + i + "]");
                }

                sum_alpha += a;
                ln_numerator += (a.IsInt32() ? LogFactorial(a.ToInt32() - 1) : LogGamma(a));
            }

            double ln_denominator = sum_alpha.IsInt32() ? LogFactorial(sum_alpha.ToInt32() - 1) : LogGamma(sum_alpha);
            return Math.Exp(ln_numerator - ln_denominator);
        }

        /// <summary>
        /// Calculates and returns the multivariate beta function for a set of complex numbers.
        /// </summary>
        /// <name>BetaMultinomial_Complex</name>
        /// <proto>Complex Beta(Complex[] alpha)</proto>
        /// <cat>functions</cat>
        public static Complex Beta(Complex[] alpha)
        {
            if (alpha == null)
            {
                throw new ArgumentNullException(nameof(alpha));
            }

            Complex ln_numerator = Complex.Zero;
            Complex sum_alpha = Complex.Zero;

            int len = alpha.Length, i;
            for (i = 0; i < len; ++i)
            {
                Complex a = alpha[i];
                if (a.Real <= 0)
                {
                    throw new ArgumentOutOfRangeException(nameof(alpha) + "[" + i + "]");
                }

                sum_alpha.IncrementBy(a);
                ln_numerator.IncrementBy(LogGamma(a));
            }

            return Complex.Exp(ln_numerator - LogGamma(sum_alpha));
        }

    }
}
