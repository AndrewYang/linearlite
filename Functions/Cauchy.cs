﻿using LinearNet.Global;
using LinearNet.Structs.Fields;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Functions
{
    /// <summary>
    /// Partial class for computation of Cauchy numbers of 1st and 2nd kind
    /// - Note: Cauchy numbers of the 1st kind are just Bernoulli numbers of the 2nd kind,
    /// or Gregory coefficients
    /// </summary>
    /// <cat>numerics</cat>
    public static partial class MathFunctions
    {
        /// <summary>
        /// Calculates the first $N$ Cauchy numbers of the second type, $C_0, ..., C_{N - 1}$, where
        /// $$C_n = n!\int_0^1 \dbinom{-x}{n} dx$$
        /// 
        /// <para>
        /// The numbers will be calculated exactly and stored in their simpliest form, in the indices $0, ..., N - 1$ of the array <txt>cauchy2</txt>. The rest of the 
        /// array is unaffected.
        /// </para>
        /// <para>
        /// For Cauchy numbers of the first type, please see <a href="#Gregory"><txt>Gregory</txt></a>.
        /// </para>
        /// </summary>
        /// <name>Cauchy2</name>
        /// <proto>void Cauchy2(BigRational[] cauchy2, int N)</proto>
        /// <cat>functions</cat>
        /// <param name="cauchy2"></param>
        /// <param name="N"></param>
        public static void Cauchy2(BigRational[] cauchy2, int N)
        {
            if (N < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(N), ExceptionMessages.NegativeNotAllowed);
            }

            if (N >= 1)
            {
                cauchy2[0] = BigRational.One;
            }

            for (int n = 1; n < N; ++n)
            {
                BigRational sum = new BigRational(n % 2 == 0 ? 1 : -1, n * (n + 1));
                BigInteger k_fact = BigInteger.One;
                for (int k = 1; k < n; ++k)
                {
                    int n_k = n - k;
                    k_fact *= k;
                    BigRational term = cauchy2[k] / (k_fact * n_k * (n_k + 1));
                    sum += (n_k % 2 == 0) ? term : -term;
                }
                cauchy2[n] = k_fact * n * sum;
            }
        }

        /// <summary>
        /// Calculates the first $N$ Cauchy numbers of the second type approximately, accurate up to '<txt>sigFigs</txt>' significant figures
        /// </summary>
        /// <param name="cauchy2"></param>
        /// <param name="N"></param>
        /// <param name="sigFigs"></param>
        public static void Cauchy2(BigDecimal[] cauchy2, int N, int sigFigs)
        {
            if (N < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(N), ExceptionMessages.NegativeNotAllowed);
            }

            if (N >= 1)
            {
                cauchy2[0] = new BigDecimal(1, sigFigs);
            }

            BigDecimal one = new BigDecimal(1, sigFigs);
            for (int n = 1; n < N; ++n)
            {
                BigDecimal sum = (n % 2 == 0 ? one : -one) / new BigDecimal(n * (n + 1), sigFigs);
                BigInteger k_fact = BigInteger.One;
                for (int k = 1; k < n; ++k)
                {
                    int n_k = n - k;
                    k_fact *= k;
                    BigDecimal term = cauchy2[k] / new BigDecimal(n_k * (n_k + 1) * k_fact, sigFigs);
                    sum += (n_k % 2 == 0) ? term : -term;
                }
                cauchy2[n] = sum * k_fact * n;
            }
        }
    }
}
