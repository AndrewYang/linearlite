﻿using LinearNet.Global;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Functions
{
    /// <cat>numerics</cat>
    public static partial class MathFunctions
    {
        /// <summary>
        /// Returns $n$ choose $k$, returning an object of type <txt>double</txt>.
        /// $$\dbinom{n}{k} = \frac{n!}{k!(n-k)!}$$
        /// </summary>
        /// <param name="n">The number of objects to choose from, $n$.</param>
        /// <param name="k">The number of objects to choose, $k$.</param>
        /// <returns>The number of ways to choose $k$ objects from $n$ (distinct) objects.</returns>
        public static double Combinations(int n, int k)
        {
            return Math.Round(Math.Exp(LogCombinations(n, k)));
        }
        /// <summary>
        /// Returns $n$ choose $k$ exactly as an 32-bit integer.
        /// <para>
        /// Due to the fast growth of combinations with respect to both $n$ and $k$, this method overflows 
        /// quite easily. Consider using: 
        /// </para>
        /// <ul>
        /// <li><a href="#Combinations"><txt>double Combinations(int n, int k)</txt></a> or <a href="#LogCombinations"><txt>double LogCombinations(int n, int k)</txt></a> for finite-precision evaluation of large combinations,</li>
        /// <li><a href="#BigCombinations"><txt>BigInteger BigCombinations(int n, int k)</txt></a> for exact evaluation of large combinations.</li>
        /// </ul>
        /// </summary>
        /// <param name="n">The number of objects to choose from, $n$.</param>
        /// <param name="k">The number of objects to choose, $k$.</param>
        /// <returns>The number of ways to choose $k$ objects from $n$ (distinct) objects.</returns>
        public static int IntCombinations(int n, int k)
        {
            if (n < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(n));
            }
            if (k < 0 || k > n)
            {
                throw new ArgumentOutOfRangeException(nameof(k));
            }

            if (k > n / 2)
            {
                return IntPermutations(n, n - k) / IntFactorial(n - k);
            }
            return IntPermutations(n, k) / IntFactorial(k);
        }
        /// <summary>
        /// Calculates $n$ choose $k$, $\dbinom{n}{k}$, exactly as a <txt>System.Numerics.BigInteger</txt>.
        /// <para>
        /// If performance is important, it is faster to instead use <a href="#Combinations"><txt>double Combinations(int n, int k)</txt></a> 
        /// or <a href="#IntCombinations"><txt>int IntCombinations(int n, int k)</txt></a>.
        /// </para>
        /// </summary>
        /// <name>BigCombinations</name>
        /// <proto>BigInteger BigCombinations(int n, int k)</proto>
        /// <cat>functions</cat>
        /// <param name="n">The number of objects to choose from, $n$.</param>
        /// <param name="k">The number of objects to choose, $k$.</param>
        /// <returns>The number of ways to choose $k$ objects from $n$ (distinct) objects.</returns>
        public static BigInteger BigCombinations(int n, int k)
        {
            if (n < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(n));
            }
            if (k < 0 || k > n)
            {
                throw new ArgumentOutOfRangeException(nameof(k));
            }

            if (k > n / 2)
            {
                return BigPermutations(n, n - k) / BigFactorial(n - k);
            }
            return BigPermutations(n, k) / BigFactorial(k);
        }

        public static BigInteger Combinations(BigInteger n, BigInteger k)
        {
            if (n < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(n));
            }
            if (k < 0 || k > n)
            {
                throw new ArgumentOutOfRangeException(nameof(k));
            }

            if (k > n / 2)
            {
                return Combinations(n, n - k);
            }

            // Do this to avoid blowups
            BigInteger comb = BigInteger.One;
            for (BigInteger i = 0; i < k; ++i)
            {
                comb *= (n - i);
                comb /= (i + 1);
            }

            return comb;
        }

        /// <summary>
        /// Calculates and returns the natural log (base $e$) of $C(n, k)$.
        /// </summary>
        /// <param name="n">The number of objects to choose from, $n$.</param>
        /// <param name="k">The number of objects to choose, $k$.</param>
        /// <returns>$\ln \dbinom{n}{k}$</returns>
        public static double LogCombinations(int n, int k)
        {
            if (n < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(n));
            }
            if (k < 0 || k > n)
            {
                throw new ArgumentOutOfRangeException(nameof(k));
            }

            if (k > n / 2)
            {
                return LogPermutations(n, n - k) - LogFactorial(n - k);
            }
            return LogPermutations(n, k) - LogFactorial(k);
        }

        /// <summary>
        /// Calculates and returns the multinomial coefficient, returning an object of type <txt>double</txt>.
        /// $$\dbinom{n}{k_1, k_2, ... , k_m} = \frac{n!}{k_1! k_2! ... k_n!}$$
        /// </summary>
        /// <param name="n">The total number of objects, $n$.</param>
        /// <param name="ks">A length-$m$ array containing the coefficients $k_1, ..., k_m$.</param>
        /// <returns>The multinomial coefficient $\dbinom{n}{k_1, k_2, ... , k_m}$.</returns>
        public static double Combinations(int n, int[] ks)
        {
            return Math.Round(Math.Exp(LogCombinations(n, ks)));
        }
        /// <summary>
        /// Calculates and returns the multinomial coefficient exactly, returning an object of type <txt>System.Numerics.BigInteger</txt>.
        /// </summary>
        /// <param name="n">The total number of objects, $n$.</param>
        /// <param name="ks">A length-$m$ array containing the coefficients $k_1, ..., k_m$.</param>
        /// <returns>The multinomial coefficient $\dbinom{n}{k_1, k_2, ... , k_m}$.</returns>
        public static BigInteger BigCombinations(int n, int[] ks) 
        { 
            if (n < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(n), ExceptionMessages.NegativeNotAllowed);
            }
            if (ks == null)
            {
                throw new ArgumentNullException(nameof(ks));
            }
            if (ks.Length == 0)
            {
                throw new ArgumentOutOfRangeException(nameof(ks), "Array has 0 length.");
            }

            int max = Util.Max(ks);
            BigInteger result = BigPermutations(n, n - max);

            bool found = false;
            foreach (int k in ks) 
            {
                if (!found && k != max)
                {
                    result /= BigFactorial(k);
                    found = true;
                }
            }
            return result;
        }
        /// <summary>
        /// Calculates and returns the matural log (base $e$) of the multinomial coefficient $(n; k_1, k_2, ... k_m)$
        /// </summary>
        /// <param name="n">The total number of objects, $n$.</param>
        /// <param name="ks">A length-$m$ array containing the coefficients $k_1, ..., k_m$.</param>
        /// <returns>$\ln \dbinom{n}{k_1, k_2, ... , k_m}$.</returns>
        public static double LogCombinations(int n, int[] ks)
        {
            if (n < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(n));
            }
            if (ks == null)
            {
                throw new ArgumentNullException(nameof(ks));
            }
            if (ks.Length == 0)
            {
                throw new ArgumentOutOfRangeException(nameof(ks));
            }

            double result = LogFactorial(n);
            int sum = 0;
            foreach (int k in ks)
            {
                if (k < 0 || k > n)
                {
                    throw new ArgumentOutOfRangeException(nameof(ks));
                }
                sum += k;
                result -= LogFactorial(k);
            }

            if (sum != n)
            {
                throw new InvalidOperationException(nameof(ks));
            }

            return result;
        }

        /// <summary>
        /// Calculates $P(n, k)$, the number of permutations of $n$ objects taken $k$ at a time. $P(n, k) = \frac{n!}{(n-k)!}$
        /// </summary>
        /// <param name="n">The total number of objects.</param>
        /// <param name="k">The number of objects to permute.</param>
        /// <returns>The number of permuations of $n$ objects taken $k$ at a time, as a <txt>double</txt>.</returns>
        public static double Permutations(int n, int k)
        {
            if (n < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(n));
            }
            if (k < 0 || k > n)
            {
                throw new ArgumentOutOfRangeException(nameof(k));
            }

            // This is more precise than using division of two log factorials
            double product = 1.0;
            for (int i = n - k + 1; i <= n; ++i)
            {
                product *= i;
            }
            return product;
        }
        /// <summary>
        /// Calculates $P(n, k)$, the number of permutations of $n$ objects taken $k$ at a time, returning the answer exactly as a 32-bit integer.
        /// <para>
        /// If this method overflows, please see <a href="#IntPermutations"><txt>int IntPermutations(int n, int k)</txt></a> or 
        /// <a href="#BigPermutations"><txt>BigInteger BigPermutations(int n, int k)</txt></a>.
        /// </para>
        /// </summary>
        /// <param name="n">The total number of objects.</param>
        /// <param name="k">The number of objects to permute.</param>
        /// <returns>The number of permuations of $n$ objects taken $k$ at a time, as an 32-bit integer.</returns>
        public static int IntPermutations(int n, int k)
        {
            if (n < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(n));
            }
            if (k < 0 || k > n)
            {
                throw new ArgumentOutOfRangeException(nameof(k));
            }

            // This is more precise than using division of two log factorials
            int product = 1;
            for (int i = n - k + 1; i <= n; ++i)
            {
                product *= i;
            }
            return product;
        }
        /// <summary>
        /// Calculates $P(n, k)$, the number of permutations of $n$ objects taken $k$ at a time, returning the answer exactly as a <txt>System.Numerics.BigInteger</txt>.
        /// </summary>
        /// <name>BigPermutations</name>
        /// <proto>BigInteger BigPermutations(int n, int k)</proto>
        /// <cat>functions</cat>
        /// <param name="n"></param>
        /// <param name="k"></param>
        /// <returns></returns>
        public static BigInteger BigPermutations(int n, int k)
        {
            if (n < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(n));
            }
            if (k < 0 || k > n)
            {
                throw new ArgumentOutOfRangeException(nameof(k));
            }

            // This is more precise than using division of two log factorials
            BigInteger product = BigInteger.One;
            for (int i = n - k + 1; i <= n; ++i)
            {
                product *= i;
            }
            return product;
        }
        public static BigInteger Permutations(BigInteger n, BigInteger k)
        {
            if (n < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(n));
            }
            if (k < 0 || k > n)
            {
                throw new ArgumentOutOfRangeException(nameof(k));
            }

            // This is more precise than using division of two log factorials
            BigInteger product = BigInteger.One;
            for (BigInteger i = n - k + 1; i <= n; ++i)
            {
                product *= i;
            }
            return product;
        }

        /// <summary>
        /// Calculates the natural log (base $e$) of $P(n, k)$.
        /// </summary>
        /// <name>LogPermutations</name>
        /// <proto>double LogPermutations(int n, int k)</proto>
        /// <cat>functions</cat>
        /// <param name="n"></param>
        /// <param name="k"></param>
        /// <returns></returns>
        public static double LogPermutations(int n, int k)
        {
            if (n < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(n));
            }
            if (k < 0 || k > n)
            {
                throw new ArgumentOutOfRangeException(nameof(k));
            }

            // This is more precise than using division of two log factorials
            double logProduct = 0.0;
            for (int i = n - k + 1; i <= n; ++i)
            {
                logProduct += Math.Log(i);
            }
            return logProduct;
        }

        /// <summary>
        /// Returns the Eulerian number $A(m, n)$ representing the number of 
        /// permutations of a ordered set of size $n$, in which $m$ are greater 
        /// than the previous element.
        /// </summary>
        /// <name>Eulerian</name>
        /// <proto>void Eulerian(int n, int m)</proto>
        /// <cat>functions</cat>
        /// <param name="n"></param>
        /// <param name="m"></param>
        /// <returns></returns>
        public static int Eulerian(int n, int m)
        {
            int sum = 0;
            for (int k = 1; k < m; ++k)
            {
                int term = IntCombinations(n + 1, k) * Pow(m + 1 - k, n);
                if (k % 2 == 0)
                {
                    sum += term;
                }
                else
                {
                    sum -= term;
                }
            }
            return sum;
        }
        private static int Pow(int n, int power)
        {
            if (n == 0) return 1;
            if (n == 1) return n;

            int half = Pow(n, power / 2);
            if (n % 2 == 0)
            {
                return half * half;
            }
            else
            {
                return half * half * n;
            }
        }
    }
}
