﻿using LinearNet.Structs;
using LinearNet.Structs.Fields;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Functions
{
    /// <cat>numerics</cat>
    public static partial class MathFunctions
    {

        /// <summary>
        /// The n-th entry in this array is B(2(n + 1)) / (2 * (n + 1)), where B(k) is the k-th 
        /// Bernoulli number
        /// </summary>
        private static readonly double[] _coeffs =
        {
            8.33333333333333E-02,
            -8.33333333333333E-03,
            3.96825396825397E-03,
            -4.16666666666667E-03,
            7.57575757575758E-03,
            -2.10927960927961E-02,
            8.33333333333333E-02,
            -4.43259803921569E-01,
            3.05395433027012E+00,
            -2.64562121212121E+01,
            2.81460144927536E+02,
            -3.60751054639805E+03,
            5.48275833333333E+04,
            -9.74936823850575E+05,
            2.00526957966881E+07,
            -4.72384867721630E+08,
            1.26357247959167E+10,
            -3.80879311252453E+11,
            1.28508504993050E+13,
            -4.82414483548501E+14
        };

        /// <summary>
        /// Calculates the digamma function $\psi(z)$ where 
        /// $$\psi(z) = \frac{d}{dz}\ln(\Gamma(z)).$$
        /// This implementation uses the asymptotic expansion
        /// $$\psi(z) \approx \log(z) - \frac{1}{2z} - \sum_{n=1}^{N} \frac{B_{2n}}{2nz^{2n}}$$
        /// where $B_{2n}$ is the $2n$-th Bernoulli number, for some large $N$. For small values of $z$, the recurrence $\psi(z+1)=\psi(z)+ \frac{1}{z}$ is 
        /// used to shift it to an acceptably large value.
        /// <!--inputs-->
        /// </summary>
        /// <name>Digamma</name>
        /// <proto>double Digamma(double x, int N)</proto>
        /// <cat>functions</cat>
        /// <param name="x">The real value $x$ at which to evaluate the digamma function. <txt>NaN</txt> is returned if $x$ is within machine precision of a non-positive integer.</param>
        /// <param name="N">The number of terms to use in the series approximation of the digamma function. If <txt>-1</txt>, the default (and maximum) of <txt>20</txt> will be used.</param>
        /// <returns></returns>
        public static double Digamma(double x, int N = -1)
        {
            if (double.IsPositiveInfinity(x)) return double.PositiveInfinity;
            if (double.IsNaN(x)) return double.NaN;
            
            if (x <= 0)
            {
                // Poles at all non-positive integers
                if (x.IsInt32())
                {
                    return double.NaN;
                }

                // Perform the flip into positive territory psi(x) = psi(1 - x) - pi * cot(pi * x)
                return Digamma(1.0 - x, N) - Math.PI / Math.Tan(Math.PI * x);
            }

            // The shift formula requires evaluation of 
            // 1 / x, so for numerical stability we use the reflection 
            // formula first. psi(x) = psi(1 - x) - pi * cot(pi * x)
            if (x < 0.5)
            {
                return Digamma(1.0 - x, N) - Math.PI / Math.Tan(Math.PI * x);
            }

            // Shift positively
            double sum = 0.0;
            while (x < 10.0)
            {
                sum -= 1.0 / x;
                x++;
            }

            if (N < 0 || N > _coeffs.Length)
            {
                N = _coeffs.Length;
            }

            sum += Math.Log(x) - 0.5 / x;
            for (int n = 1; n <= N; ++n)
            {
                sum -= _coeffs[n - 1] / (2 * n * Math.Pow(x, 2 * n));
            }
            return sum;
        }

        public static BigDecimal Digamma(BigDecimal x, int sigFigs = -1)
        {
            if (sigFigs < 0)
            {
                sigFigs = x.SignificantFigures;
            }
            if (sigFigs != x.SignificantFigures)
            {
                x.SignificantFigures = sigFigs;
            }

            BigDecimal one = new BigDecimal(1, sigFigs);

            if (x.IsNegative || x.IsZero)
            {
                x.Simplify();

                // If the simplified exponents are non-negative, then it is a negative integer, which is a pole
                if (x.Exponent >= 0)
                {
                    // TODO: implement NaN?!
                    return new BigDecimal(0, sigFigs);
                }

                BigDecimal pi = BigDecimal.Pi(sigFigs);
                return Digamma(one - x, sigFigs) - pi / BigDecimal.Tan(pi * x, sigFigs);
            }

            // The shift formula requires evaluation of 
            // 1 / x, so for numerical stability we use the reflection 
            // formula first. psi(x) = psi(1 - x) - pi * cot(pi * x)
            if (x < 0.5)
            {
                BigDecimal pi = BigDecimal.Pi(sigFigs);
                return Digamma(one - x, sigFigs) - pi / BigDecimal.Tan(pi * x, sigFigs);
            }

            // Shift positively
            BigDecimal sum = new BigDecimal(0, sigFigs);
            BigDecimal ten = new BigDecimal(10, sigFigs);
            while (x < ten)
            {
                sum -= one / x;
                x += one;
            }

            BigDecimal two = new BigDecimal(2, sigFigs);
            sum += (BigDecimal.Log(x, sigFigs) - one / (two * x));

            BigDecimal x2 = x * x, xPow = x2;
            int maxitr = sigFigs;
            for (int n = 1; n <= maxitr; ++n)
            {
                int two_n = 2 * n;
                sum -= Bernoulli(two_n).ToDecimal(sigFigs) / (xPow * two_n);
                sum.Round(sigFigs);
                xPow *= x2;
            }
            return sum;
        }

        public static Complex Digamma(Complex x, int N = -1)
        {
            if (x.Real == double.PositiveInfinity) return double.PositiveInfinity;
            if (Complex.IsNaN(x)) return double.NaN;

            if (x.Real <= 0)
            {
                // Poles at all non-positive integers along the real axis
                if (x.Real.IsInt32())
                {
                    return double.NaN;
                }

                // Perform the flip into positive territory psi(x) = psi(1 - x) - pi * cot(pi * x)
                return Digamma(Complex.One - x, N) - Math.PI / Complex.Tan(Math.PI * x);
            }

            // The shift formula requires evaluation of 
            // 1 / x, so for numerical stability we use the reflection 
            // formula first. psi(x) = psi(1 - x) - pi * cot(pi * x)
            if (x.Real < 0.5)
            {
                return Digamma(Complex.One - x, N) - Math.PI / Complex.Tan(Math.PI * x);
            }

            // Shift positively
            Complex sum = 0.0;
            while (x.Real < 10.0)
            {
                sum -= x.MultiplicativeInverse();
                x.Real += 1;
            }

            if (N < 0 || N > _coeffs.Length)
            {
                N = _coeffs.Length;
            }

            sum += Complex.Log(x) - 0.5 / x;
            for (int n = 1; n <= N; ++n)
            {
                sum -= _coeffs[n - 1] / (2 * n * Complex.Pow(x, 2 * n));
            }
            return sum;
        }
    }
}
