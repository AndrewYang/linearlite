﻿using LinearNet.Global;
using LinearNet.Structs;
using LinearNet.Structs.Fields;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Functions
{
    /// <cat>numerics</cat>
    public static partial class MathFunctions
    {
        private static readonly double[] _logErfCoeffs =
        {
            0.0,-0.405465108108164,-1.32175583998232,-2.57451880847769,-4.07859620525396,-5.78334429749239,-7.65514647439398,-9.67004949493624,-11.8101156584325,-14.061407457039,
            -16.4127827142025,-18.8551297495717,-21.3808583938799,-23.9835480793243,-26.6576967287509,-29.3985367526761,-32.2018971335826,-35.0640980145121,-37.9818687465963,-40.952283212166,
            -43.9727080983104,-47.040761033444,-50.1542763426544,-53.3112767638045,-56.5099498813552,-59.7486283335196,-63.0257730665118,-66.3399590711843,-69.6898631584589,-73.0742534218047,
            -76.491980105418,-79.9419676512496,-83.4232077405853,-86.9347531794163,-90.4757125034537,-94.045245199935,-97.6425574605235,-101.2668983935,-104.917556634794,-108.593857306701,
            -112.295159280813,-116.02085270805,-119.77035678398,-123.543117722075,-127.338606911247,-131.156319237204,-134.995771549797,-138.856501260838,-142.738065058781,-146.640037728356,
        };
        private static readonly double[] _logErfcCoeffs =
        {
            0.0, 1.09861228866811, 2.70805020110221, 4.65396035015752, 6.85118492749374, 9.24908020029211, 11.8140295577536, 14.5220797588559, 17.3552931029121, 20.2997320820785,
            23.3442545198019, 26.4797487357311, 29.6986245605993, 32.9944614266036, 36.3617572565901, 39.7957444610752, 43.2922520225417, 46.8476000840311, 50.4585179966754, 54.122079642805,
            57.8356517095093, 61.5968518252029, 65.4035143149732, 69.2536619166833, 73.1454822147939, 77.0773078475182, 81.0475997610703, 85.0549329463028, 89.0979842141374, 93.1755216580431,
            97.2863955222164, 101.429530248608, 105.603917518504, 109.808610137895, 114.042716642492, 118.305396519533, 122.595855960682, 126.913344074218, 131.257149496071, 135.626597348539,
            140.021046503211, 144.439887111008, 148.882538367498, 153.348446486152, 157.837082855885, 162.347942362401, 166.880541855555, 171.434418747155, 176.009129725659, 180.604249575793 
        };

        private static double CalculateLogErfCoefficient(int n)
        {
            double log_denominator = 0.0;
            for (int i = 0; i <= n; ++i)
            {
                log_denominator += Math.Log(2 * i + 1);
            }
            return n * Math.Log(2.0) - log_denominator;
        }

        /// <summary>
        /// Computing the log in BigDecimal is very expensive, so we provide a naive Erf coefficient calculation method here 
        /// instead of the log variant
        /// </summary>
        /// <param name="n"></param>
        /// <returns></returns>
        private static BigDecimal CalculateErfCoefficient(int n, int sigFigs)
        {
            BigDecimal denominator = new BigDecimal(1, sigFigs);
            for (int i = 0; i <= n; ++i)
            {
                denominator *= new BigDecimal(2 * i + 1, sigFigs);
            }
            BigDecimal two = new BigDecimal(2, sigFigs);
            return BigDecimal.Pow(two, n) / denominator;
        }

        private static double CalculateLogErfcCoefficient(int n)
        {
            double log_coeff = 0.0;
            for (int i = 0; i < n; ++i)
            {
                log_coeff += Math.Log(2 * i + 1);
            }
            return log_coeff;
        }

        /// <summary>
        /// Calculates and returns the error function $$\operatorname{erf}(x) = \frac{1}{\sqrt\pi} \int_{-\pi}^{\pi} e^{-t^2} dt .$$
        /// For $|x| \le 4$, the following approximation is used
        /// $$\operatorname{erf}(x) \approx \frac{2e^{-x^2}}{\sqrt{\pi}}\sum_{n=0}^N \frac{(2x^2)^n}{(2n+1)!!}$$
        /// and for $|x| > 4$, the following approximation is used
        /// $$\operatorname{erf}(x) \approx 1 - \frac{e^{-x^2}}{x\sqrt{\pi}}\sum_{n=0}^N(-1)^n\frac{(2n+1)!!}{(2x^2)^n}$$
        /// 
        /// <!--inputs-->
        /// </summary>
        /// <name>Erf</name>
        /// <proto>double Erf(double x, int N)</proto>
        /// <cat>functions</cat>
        /// <param name="x">The real value at which to evaluate the error function.</param>
        /// <param name="N">
        /// <b>Optional</b>, defaults to 50.<br/>
        /// The number of terms used to calculate the function. Using more terms increases execution time and precision.
        /// </param>
        /// <returns></returns>
        public static double Erf(double x, int N = 50)
        {
            if (x < 0.0)
            {
                return -Erf(-x, N);
            }

            if (double.IsPositiveInfinity(x)) return 1;
            if (double.IsNaN(x)) return double.NaN;

            // Use the Erfc function for large values 
            if (x > 4.0)
            {
                return 1.0 - Erfc(x, N);
            }

            double x2 = x * x, sum = 0.0;
            for (int n = 0; n < N; ++n)
            {
                double coeff = n < 50 ? _logErfCoeffs[n] : CalculateLogErfCoefficient(n);
                sum += Math.Exp((2.0 * n + 1.0) * Math.Log(x) + coeff - x2);
            }
            return sum * 2.0 / Constants.SqrtPi;
        }
        /// <summary>
        /// Calculates and returns the error function $\operatorname{erf}(z)$ for a complex value $z$.
        /// </summary>
        /// <name>Erf_Complex</name>
        /// <proto>Complex Erf(Complex x, int N)</proto>
        /// <cat>functions</cat>
        /// <param name="z"></param>
        /// <param name="N"></param>
        /// <returns></returns>
        public static Complex Erf(Complex z, int N = 50)
        {
            if (z.Real < 0.0)
            {
                return -Erf(-z, N);
            }

            if (double.IsPositiveInfinity(z.Real) || double.IsPositiveInfinity(z.Imaginary)) return 1;
            if (Complex.IsNaN(z)) return Complex.NaN;

            // Use the Erfc function for large values 
            if (z.Modulus() > 4.0)
            {
                return 1.0 - Erfc(z, N);
            }

            Complex x2 = z * z, sum = 0.0;
            for (int n = 0; n < N; ++n)
            {
                double coeff = n < 50 ? _logErfCoeffs[n] : CalculateLogErfCoefficient(n);
                sum += Complex.Exp((2.0 * n + 1.0) * Complex.Log(z) + coeff - x2);
            }
            return sum * 2.0 / Constants.SqrtPi;
        }

        /// <summary>
        /// Calculates and returns the error function $\operatorname{erf}(x)$ for a <txt>BigDecimal</txt> $x$, up to a specified number of significant figures.
        /// <!--inputs-->
        /// </summary>
        /// <name>Erf_BigDecimal</name>
        /// <proto>BigDecimal Erf(BigDecimal x, int sigFigs, int maxTerms = -1)</proto>
        /// <cat>functions</cat>
        /// <param name="x">The real value at which to evaluate the error function.</param>
        /// <param name="sigFigs">The number of significant figures in the returned value.</param>
        /// <param name="maxTerms">
        /// <b>Optional</b>, defaults to -1<br/>
        /// The max number of terms to include in the summation. If -1, the summation will continue until 
        /// the first <txt>sigFigs</txt> significant figures are calculated.
        /// </param>
        /// <returns></returns>
        public static BigDecimal Erf(BigDecimal x, int sigFigs, int maxTerms = -1)
        {
            if (sigFigs <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(x), ExceptionMessages.NonPositiveNotAllowed);
            }

            if (x.IsNegative)
            {
                return -Erf(-x, sigFigs);
            }

            // Use the Erfc function for large values 
            if (x > 4.0)
            {
                return new BigDecimal(1, sigFigs) - Erfc(x, sigFigs);
            }

            // Rough heuristic for the default number of max terms
            if (maxTerms < 0)
            {
                maxTerms = Math.Max(100, sigFigs * 2);
            }

            // set x to the correct number of significant figures
            x.SignificantFigures = sigFigs;

            System.Numerics.BigInteger two = 2, q = 1;

            BigDecimal x2 = x * x, 
                twox2 = x2 * two,
                xPow = x,
                sum = new BigDecimal(0, sigFigs),
                eps = new BigDecimal(5, -sigFigs, sigFigs);

            for (int n = 1; n < maxTerms; ++n)
            {
                BigDecimal next = sum + xPow / new BigDecimal(q, sigFigs);
                if (BigDecimal.Abs(next - sum) < eps)
                {
                    break;
                }
                sum = next;

                xPow *= twox2;
                q *= (2 * n + 1);
            }

            BigDecimal pi = sigFigs >= 1000 ? BigDecimal.Pi(sigFigs) : BigDecimal.PI;
            return BigDecimal.Exp(-x2, sigFigs) * sum * two / BigDecimal.Sqrt(pi, sigFigs);
        }

        /// <summary>
        /// Calculates and returns the complementary error function $$\operatorname{erfc}(x) = 1 - \operatorname{erf}(x).$$
        /// This implementation uses the asymptotic expansion of the $\operatorname{erfc}$ function for large values of $x$, and falls back to 
        /// the $\operatorname{erf}$ function for $x$ close to zero.
        /// <!--inputs-->
        /// </summary>
        /// <name>Erfc</name>
        /// <proto>double Erfc(double x, int N)</proto>
        /// <cat>functions</cat>
        /// <param name="x">The real value at which to evaluate the complementary error function.</param>
        /// <param name="N">
        /// <b>Optional</b>, defaults to 50.<br/>
        /// The number of terms used to calculate the function. Using more terms increases execution time and precision.
        /// </param>
        /// <returns></returns>
        public static double Erfc(double x, int N = 50)
        {
            if (x < 0)
            {
                return 2.0 - Erfc(-x, N);
            }

            if (double.IsPositiveInfinity(x)) return 0.0;
            if (double.IsNaN(x)) return double.NaN;

            // Use the Erf function for small values
            if (x < 4.0)
            {
                return 1.0 - Erf(x, N);
            }

            double x2 = x * x, sum = Math.Exp(-x2);
            for (int n = 1; n <= N; ++n)
            {
                double coeff = n <= 50 ? _logErfcCoeffs[n - 1] : CalculateLogErfcCoefficient(n);
                double term = Math.Exp(coeff - n * Math.Log(2.0 * x2) - x2);
                sum += n % 2 == 0 ? term : -term;
            }
            return 1.0 / Constants.SqrtPi / x * sum;
        }
        /// <summary>
        /// Calculates and returns the complementary error function $\operatorname{erfc}(z)$ evaluated at a complex value $z$.
        /// </summary>
        /// <name>Erfc_Complex</name>
        /// <proto>Complex Erfc(Complex z, int N)</proto>
        /// <cat>functions</cat>
        /// <param name="z"></param>
        /// <param name="N"></param>
        /// <returns></returns>
        public static Complex Erfc(Complex z, int N = 50)
        {
            if (z.Real < 0)
            {
                return 2.0 - Erfc(-z, N);
            }

            if (double.IsPositiveInfinity(z.Real) || double.IsPositiveInfinity(z.Imaginary)) return 0.0;
            if (Complex.IsNaN(z)) return Complex.NaN;

            if (z.Modulus() < 4.0)
            {
                return 1.0 - Erf(z, N);
            }

            Complex z2 = z.Multiply(z), sum = Complex.Exp(-z2);
            for (int n = 1; n <= N; ++n)
            {
                double coeff = n <= 50 ? _logErfcCoeffs[n - 1] : CalculateLogErfcCoefficient(n);
                Complex term = Complex.Exp(coeff - n * Complex.Log(2.0 * z2) - z2);
                sum += n % 2 == 0 ? term : -term;
            }
            return 1.0 / Constants.SqrtPi / z * sum;
        }
        /// <summary>
        /// Calculates and returns the complementary error function $\operatorname{erfc}(z)$ evaluated for a <txt>BigDecimal</txt> $x$, correct to a specified number of 
        /// significant figures.
        /// </summary>
        /// <name>Erfc_BigDecimal</name>
        /// <proto>BigDecimal Erfc(BigDecimal x, int sigFigs, int maxTerms = -1)</proto>
        /// <cat>functions</cat>
        /// <param name="z"></param>
        /// <param name="N"></param>
        /// <returns></returns>
        public static BigDecimal Erfc(BigDecimal x, int sigFigs, int maxTerms = -1)
        {
            if (x.IsNegative)
            {
                return new BigDecimal(2, sigFigs) - Erfc(-x, sigFigs, maxTerms);
            }

            // Use the Erf function for small values
            if (x < 4.0)
            {
                return new BigDecimal(1, sigFigs) - Erf(x, sigFigs, maxTerms);
            }

            // Rough heuristic for the default number of max terms
            if (maxTerms < 0)
            {
                maxTerms = Math.Max(100, sigFigs * 2);
            }

            // set x to the correct number of significant figures
            x.SignificantFigures = sigFigs;

            System.Numerics.BigInteger fact = 1;
            BigDecimal 
                x2 = x * x, 
                xPow = x2,
                sum = BigDecimal.Exp(-x2, sigFigs);

            for (int n = 1; n <= maxTerms; ++n)
            {
                BigDecimal term = new BigDecimal(fact, sigFigs) / xPow;
                sum += n % 2 == 0 ? term : -term;

                fact *= (2 * n + 1);
                xPow *= x2;
            }

            BigDecimal pi = sigFigs >= 1000 ? BigDecimal.Pi(sigFigs) : BigDecimal.PI;
            return BigDecimal.Exp(-x2, sigFigs) / BigDecimal.Sqrt(pi, sigFigs) / x * sum;
        }
    }
}
