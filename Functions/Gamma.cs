﻿using LinearNet.Global;
using LinearNet.Structs;
using LinearNet.Structs.Fields;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Functions
{
    /// <summary>
    /// 
    /// </summary>
    /// <cat>numerics</cat>
    public static partial class MathFunctions
    {
        private static readonly double[] _LanczosSeriesCoeffs =
        {
            0.99999999999980993, 676.5203681218851, -1259.1392167224028, 771.32342877765313, -176.61502916214059, 12.507343278686905, -0.13857109526572012, 9.9843695780195716E-06, 1.5056327351493116E-07
        };

        /// <summary>
        /// Calculate an approximation for the Gamma function $\Gamma(x)$ using Lanczos' algorithm.
        /// Most suited for complex values in the right half plane $(\Re(z) > 0)$.
        /// </summary>
        /// <name>LanczosApproximation</name>
        /// <proto>double LanczosApproximation(double x, int N)</proto>
        /// <cat>functions</cat>
        public static double LanczosApproximation(double z, int N = 7)
        {
            // Perform Euler's reflection for small Re(z)
            if (z < 0.5)
            {
                return Math.PI / (Math.Sin(Math.PI * z) * LanczosApproximation(1.0 - z));
            }
            z -= 1;

            double x = _LanczosSeriesCoeffs[0];
            int end = N + 2, i;
            for (i = 1; i < end; ++i)
            {
                x += _LanczosSeriesCoeffs[i] / (z + i);
            }

            double t = z + N + 0.5;
            return Constants.Sqrt2Pi * (Math.Pow(t, z + 0.5)) * Math.Exp(-t) * x;
        }
        public static Complex LanczosApproximation(Complex z, int N = 7)
        {
            // Perform Euler's reflection for small Re(z)
            if (z.Real < 0.5)
            {
                return Math.PI / (Complex.Sin(Math.PI * z) * LanczosApproximation(1.0 - z));
            }

            z.DecrementBy(Complex.One);

            Complex x = _LanczosSeriesCoeffs[0];
            int end = N + 2, i;
            for (i = 1; i < end; ++i)
            {
                x += _LanczosSeriesCoeffs[i] / (z + i);
            }

            Complex t = z + N + 0.5;
            return Constants.Sqrt2Pi * (Complex.Pow(t, z + 0.5)) * Complex.Exp(-t) * x;
        }

        /// <summary>
        /// Calculate an approximation for the Gamma function $\Gamma(x)$ using Spouge's approximation.
        /// </summary>
        /// <name>SpougeApproximation</name>
        /// <proto>double SpougeApproximation(double x, int N)</proto>
        /// <cat>functions</cat>
        public static double SpougeApproximation(double x, int N = 50)
        {
            double[] c = new double[N];

            double factor = 1.0;
            for (int k = 1; k < N; k++)
            {
                int N_k = N - k;
                c[k] = Math.Exp(N_k) * Math.Pow(N_k, k - 0.5) / factor;
                factor *= -k;
            }

            double product = Constants.Sqrt2Pi;
            for (int k = 1; k < N; k++)
            {
                double delta = c[k] / (x + k);
                product += delta;
            }

            product *= Math.Exp(-(x + N)) * Math.Pow(x + N, x + 0.5);
            return product / x;
        }
        public static Complex SpougeApproximation(Complex z, int N = 50)
        {
            Complex[] c = new Complex[N];

            double factor = 1.0;
            for (int k = 1; k < N; ++k)
            {
                int N_k = N - k;
                c[k] = Math.Exp(N_k) * Math.Pow(N_k, k - 0.5) / factor;
                factor *= -k;
            }

            
            Complex product = Constants.Sqrt2Pi;
            for (int k = 1; k < N; ++k)
            {
                product += c[k] / (z + k);
            }

            product *= Complex.Exp(-(z + N)) * Complex.Pow(z + N, z + 0.5);
            return product / z;
        }
        public static BigDecimal SpougeApproximation(BigDecimal x, int sigFigs = -1)
        {
            if (sigFigs < 0)
            {
                sigFigs = x.SignificantFigures;
            }

            // Account for rounding errors
            int extraSf = (sigFigs + 10) * 2;
            sigFigs += extraSf;

            x.SignificantFigures = sigFigs;

            // Shift to acceptable value 
            BigDecimal one = new BigDecimal(1, sigFigs);
            BigDecimal fact = one;
            while (x > 5)
            {
                x -= one;
                fact *= x;
            }

            // sqrt(2 * pi)
            BigDecimal product = BigDecimal.Sqrt(BigDecimal.Pi(sigFigs).Multiply(2, sigFigs));
            BigDecimal half = new BigDecimal(0.5, sigFigs);
            //BigDecimal e = BigDecimal.Euler(sigFigs);
            //BigDecimal exp_N_minus_k = new BigDecimal(1, sigFigs);

            System.Numerics.BigInteger factor = 1;
            int N = sigFigs;
            for (int k = 1; k < N; k++)
            {
                int N_k = N - k;
                //exp_N_minus_k *= e;
                //BigDecimal N_k_power = new BigDecimal(System.Numerics.BigInteger.Pow(N_k, k - 1), sigFigs) * BigDecimal.Sqrt(new BigDecimal(N - k, sigFigs), sigFigs);
                //BigDecimal power2 = BigDecimal.Pow(N_k, k - 0.5, sigFigs);
                BigDecimal log = BigDecimal.Log(N_k, sigFigs);
                BigDecimal a = new BigDecimal(k, sigFigs) - half;

                // Cant figure out why power2, though being more accurate, does not generate a reliable result?
                //Debug.WriteLine(N_k_power * N_k_power);
                //Debug.WriteLine(power2 * power2);
                //Debug.WriteLine(new BigDecimal(System.Numerics.BigInteger.Pow(N_k, k - 1), sigFigs));
                //Debug.WriteLine(BigDecimal.Sqrt(new BigDecimal(N - k, sigFigs), sigFigs) * BigDecimal.Sqrt(new BigDecimal(N - k, sigFigs), sigFigs));

                BigDecimal c_k = BigDecimal.Exp(N_k + a * log, sigFigs) / new BigDecimal(factor, sigFigs);
                factor *= -k;
                BigDecimal delta = c_k / (x + new BigDecimal(k, sigFigs));
                product += delta;
            }

            BigDecimal x_N = x + new BigDecimal(N, sigFigs);
            product *= BigDecimal.Exp(-x_N, sigFigs) * BigDecimal.Pow(x_N, x + half, sigFigs) * fact / x;
            product.Round(sigFigs - extraSf);

            return product;
        }

        /// <summary>
        /// Calculated from OEIS A001163 / OEIS A001164 (may be subject to double precision rounding errors)
        /// </summary>
        private static readonly double[] _StirlingSeriesCoeffs =
        {
            8.33333333333333E-02, 3.47222222222222E-03, -2.68132716049383E-03, -2.29472093621399E-04, 7.84039221720067E-04, 6.97281375836586E-05, -5.92166437353694E-04, -5.17179090826059E-05,
            8.39498720672087E-04, 7.20489541602000E-05, -1.91443849856548E-03, -1.62516262783915E-04, 6.40336283380811E-03, 5.40164767892602E-04, -2.95278809456991E-02
        };

        /// <summary>
        /// Calculate an approximation for the Gamma function $\Gamma(x)$ using Stirling's approximation, with 16 terms.
        /// Stirling's method is more effective for large values of $x$.
        /// </summary>
        /// <name>StirlingsApproximation</name>
        /// <proto>double StirlingsApproximation(double x)</proto>
        /// <cat>functions</cat>
        public static double StirlingsApproximation(double z)
        {
            // Perform Euler's reflection for small z
            if (z < 0.5)
            {
                return Math.PI / Math.Sin(Math.PI * z) * StirlingsApproximation(1.0 - z);
            }

            double series = 1.0, 
                one_over_z = 1.0 / z, 
                power = one_over_z;

            foreach (double c in _StirlingSeriesCoeffs)
            {
                series += c * power;
                power *= one_over_z;
            }
            return Math.Exp(-z) * Math.Pow(z, z - 0.5) * Constants.Sqrt2Pi * series;
        }
        public static Complex StirlingsApproximation(Complex z)
        {
            // Perform Euler's reflection for small z
            if (z.Real < 0.5)
            {
                return Math.PI / Complex.Sin(Math.PI * z) * StirlingsApproximation(Complex.One.Subtract(z));
            }

            Complex series = 1.0,
                one_over_z = z.MultiplicativeInverse(),
                power = one_over_z;

            foreach (double c in _StirlingSeriesCoeffs)
            {
                series.IncrementBy(c * power);
                power.MultiplyEquals(one_over_z);
            }
            return Complex.Exp(z.AdditiveInverse()) * Complex.Pow(z, z.Subtract(0.5)) * Constants.Sqrt2Pi * series;
        }

        /// <summary>
        /// Calculated from OEIS A046968 / OEISA 046969 (may be subject to double precision rounding errors)
        /// </summary>
        private static readonly double[] _StirlingLogSeriesCoeffs =
        {
            8.33333333333333E-02, -2.77777777777778E-03, 7.93650793650794E-04, -5.95238095238095E-04, 8.41750841750842E-04, -1.91752691752692E-03, 6.41025641025641E-03, -2.95506535947712E-02,
            1.79644372368831E-01, -1.39243221690590E+00, 1.34028640441684E+01, -1.56848284626002E+02, 2.19310333333333E+03, -3.61087712537250E+04, 6.91472268851313E+05
        };
        /// <summary>
        /// Calculate the natural log of the Gamma function evaluated at $x$, $\ln\Gamma(x)$.
        /// <para>Aso see: <a href="#Gamma"><txt>Gamma(double x)</txt></a></para>
        /// </summary>
        /// <name>LogGamma</name>
        /// <proto>double LogGamma(double x)</proto>
        /// <cat>functions</cat>
        public static double LogGamma(double x)
        {
            if (x < 0.5)
            {
                return Math.Log(Math.PI / Math.Sin(Math.PI * x)) + LogGamma(1.0 - x);
            }

            // Stirlings is very good for x > 10. For smaller values, default to a better method
            if (x < 10)
            {
                return Math.Log(LanczosApproximation(x));
            }

            double series = 1.0,
                one_over_x = 1.0 / x,
                power = one_over_x;

            foreach (double c in _StirlingLogSeriesCoeffs)
            {
                series += c * power;
                power *= one_over_x;
            }
            return 0.5 * Constants.Ln2Pi + (x + 0.5) * Math.Log(x) - x + series;
        }
        public static Complex LogGamma(Complex x)
        {
            if (x.Real < 0.5)
            {
                return Complex.Log(Math.PI / Complex.Sin(x.Multiply(Math.PI))) + LogGamma(Complex.One.Subtract(x));
            }

            // Stirlings is very good for x > 10. For smaller values, default to a better method
            if (x.Modulus() < 10)
            {
                return Complex.Log(LanczosApproximation(x));
            }

            Complex series = Complex.One,
                one_over_x = x.MultiplicativeInverse(),
                power = one_over_x;

            foreach (double c in _StirlingLogSeriesCoeffs)
            {
                series.IncrementBy(power.Multiply(c));
                power.MultiplyEquals(one_over_x);
            }
            return 0.5 * Constants.Ln2Pi + (x + 0.5) * Complex.Log(x) - x + series;
        }

        /// <summary>
        /// Returns gamma function evaluated for a real number $x$, 
        /// $$\Gamma(x) = \int^{\infty}_0 t^{x-1} e^{-t} dt$$
        /// There are 3 currently supported implementations - 
        /// <a href="https://en.wikipedia.org/wiki/Lanczos_approximation">Lanczos' approximation</a>,
        /// <a href="https://en.wikipedia.org/wiki/Spouge%27s_approximation">Spouge's approximation</a> and
        /// <a href="https://en.wikipedia.org/wiki/Stirling%27s_approximation">Stirling's approximation</a>.
        /// <para>The default method is Lanczos'.</para>
        /// <para>Also see: 
        /// <ul>
        /// <li><a href="#LanczosApproximation"><txt>LanczosApproximation(double x, int N)</txt></a>, </li>
        /// <li><a href="#SpougeApproximation"><txt>SpougeApproximation(double x, int N)</txt></a>, </li>
        /// <li><a href="#StirlingsApproximation"><txt>StirlingsApproximation(double x)</txt></a>, </li>
        /// <li><a href="#LogGamma"><txt>LogGamma(double x)</txt></a></li>
        /// </ul>
        /// </para>
        /// </summary>
        /// <name>Gamma</name>
        /// <proto>double Gamma(double x, GammaEstimationMethod method)</proto>
        /// <cat>functions</cat>
        /// <param name="z"></param>
        /// <returns></returns>
        public static double Gamma(double x, GammaEstimationMethod method = GammaEstimationMethod.Lanczos)
        {
            if (method == GammaEstimationMethod.Lanczos)
            {
                return LanczosApproximation(x);
            }
            if (method == GammaEstimationMethod.Spouge)
            {
                return SpougeApproximation(x);
            }
            if (method == GammaEstimationMethod.Stirling)
            {
                return StirlingsApproximation(x);
            }
            throw new NotSupportedException();
        }
        /// <summary>
        /// Returns the gamma function evaluated for a complex number $z\in\mathbb{C}$.
        /// </summary>
        /// <name>Gamma_Complex</name>
        /// <proto>Complex Gamma(Complex z, GammaEstimationMethod method)</proto>
        /// <cat>functions</cat>
        /// <param name="z"></param>
        /// <returns></returns>
        public static Complex Gamma(Complex z, GammaEstimationMethod method = GammaEstimationMethod.Lanczos)
        {
            if (method == GammaEstimationMethod.Lanczos)
            {
                return LanczosApproximation(z);
            }
            if (method == GammaEstimationMethod.Spouge)
            {
                return SpougeApproximation(z);
            }
            if (method == GammaEstimationMethod.Stirling)
            {
                return StirlingsApproximation(z);
            }
            throw new NotSupportedException();
        }
        public static BigDecimal Gamma(BigDecimal x, int sigFigs = -1)
        {
            return SpougeApproximation(x, sigFigs);
        }

        /// <summary>
        /// <para>
        /// Calculates the lower or upper incomplete gamma function.
        /// </para>
        /// The lower gamma function is defined as: 
        /// $$\gamma(s, x) := \int_{0}^{x} t^{s-1}e^{-t} dt$$
        /// The upper gamma function is defined as:
        /// $$\Gamma(s, x) := \int_{x}^{\infty} t^{s-1}e^{-t} dt$$
        /// <!--inputs-->
        /// </summary>
        /// <name>IncompleteGamma</name>
        /// <proto>Complex IncompleteGamma(BigDecimal s, BigDecimal x, bool lower = true, int sigFigs = -1)</proto>
        /// <cat>functions</cat>
        /// <param name="s">The parameter $s$ of the incomplete gamma function</param>
        /// <param name="x">The parameter $x$ of the incomplete gamma function. Must be at least 0.</param>
        /// <param name="lower">
        /// <b>Optional</b>, defaults to <txt>true</txt><br/>
        /// If <txt>true</txt>, the lower incomplete gamma function $\gamma(s, x)$ will be returned, else the upper incomplete gamma 
        /// function $\Gamma(s, x)$ will be returned.
        /// </param>
        /// <param name="sigFigs">The number of significant figures of the returned result.</param>
        /// <returns></returns>
        public static BigDecimal IncompleteGamma(BigDecimal s, BigDecimal x, bool lower = true, int sigFigs = -1)
        {
            if (!lower)
            {
                return Gamma(s, sigFigs) - IncompleteGamma(s, x, true, sigFigs);
            }

            if (sigFigs < 0)
            {
                sigFigs = Math.Min(s.SignificantFigures, x.SignificantFigures);
            }

            int extraSf = (sigFigs + 10) * 2;
            sigFigs += extraSf;

            if (s.SignificantFigures < sigFigs) s.SignificantFigures = sigFigs;
            if (x.SignificantFigures < sigFigs) x.SignificantFigures = sigFigs;

            BigDecimal term = BigDecimal.Pow(x, s, sigFigs) * BigDecimal.Exp(-x, sigFigs) / s;
            BigDecimal sum = term;
            BigDecimal eps = new BigDecimal(5, -sigFigs, sigFigs);

            int maxitr = sigFigs * 10;
            for (int k = 1; k < maxitr; ++k)
            {
                term *= x / (s + k);
                sum += term;
                sum.Round(sigFigs);

                if (BigDecimal.Abs(term) < eps)
                {
                    break;
                }
            }

            sum.Round(sigFigs - extraSf);
            return sum;
        }
    }
    public enum GammaEstimationMethod
    {
        Lanczos, Spouge, Stirling
    }
}
