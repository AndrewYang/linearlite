﻿using LinearNet.Global;
using LinearNet.Structs.Fields;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Functions
{
    /// <summary>
    /// 
    /// </summary>
    /// <cat>numerics</cat>
    public static partial class MathFunctions
    {
        /// <summary>
        /// <para>
        /// Calculate the first $n$ Gregory coefficients $G_1, ..., G_n$ exactly.
        /// </para>
        /// <para>
        /// The coefficients are stored as <txt>BigRational</txt> in the array <txt>gregory</txt>, in the indices $0, ..., n - 1$.
        /// </para>
        /// <para>
        /// The Gregory coefficients (<a href="https://oeis.org/A002206">OEIS A002206</a>, <a href="https://oeis.org/A002207">OEIS A002207</a>) 
        /// are also commonly known as: 
        /// <ul>
        /// <li>Reciprocal logarithmic numbers, or</li>
        /// <li>Bernoulli numbers of the second kind, or</li>
        /// <li>Cauchy numbers of the first kind.</li>
        /// </ul>
        /// The first few Gregory coefficients are:
        /// $$\frac{1}{2}, -\frac{1}{12}, \frac{1}{24}, -\frac{19}{720}, \frac{3}{160}, -\frac{863}{60480}, \frac{275}{24192}, ...$$
        /// </para>
        /// </summary>
        /// <name>Gregory</name>
        /// <proto>void Gregory(BigRational[] gregory, int n)</proto>
        /// <cat>functions</cat>
        /// <param name="gregory"></param>
        /// <param name="n"></param>
        public static void Gregory(BigRational[] gregory, int n)
        {
            if (n < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(n), ExceptionMessages.NegativeNotAllowed);
            }

            if (n >= 1)
            {
                gregory[0] = new BigRational(1, 2);
            }

            // Calculate the next terms using 
            // G_1/n - G_2/(n - 1) + ... + (-1)^(n - 1) * G_n/1 = 1/(n + 1)
            // i.e. G_n = (-1)^(n - 1) * [1/(n + 1) - G_1/n + G_2/(n - 1) - G_3/(n - 2) + ...]

            for (int i = 1; i < n; ++i)
            {
                BigRational sum = new BigRational(1, i + 2);

                int k = i + 1;
                for (int j = 0; j < i; ++j)
                {
                    if (j % 2 == 0)
                    {
                        sum -= gregory[j] / (k - j);
                    }
                    else
                    {
                        sum += gregory[j] / (k - j);
                    }
                }

                if (i % 2 == 0)
                {
                    gregory[i] = sum;
                }
                else
                {
                    gregory[i] = -sum;
                }
            }
        }

        /// <summary>
        /// Calculate the first $n$ Gregory coefficients $G_1, ..., G_n$ correct up to '<txt>sigFigs</txt>' significant figures, storing them as <txt>BigDecimal</txt> in the array 
        /// <txt>gregory</txt>, in indices $0, ..., n - 1$.
        /// </summary>
        /// <name>Gregory_BigDecimal</name>
        /// <proto>void Gregory(BigDecimal[] gregory, int n, int sigFigs)</proto>
        /// <cat>functions</cat>
        /// <param name="gregory"></param>
        /// <param name="n"></param>
        /// <param name="sigFigs"></param>
        public static void Gregory(BigDecimal[] gregory, int n, int sigFigs)
        {
            if (n < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(n), ExceptionMessages.NegativeNotAllowed);
            }

            BigDecimal one = new BigDecimal(1, sigFigs);
            if (n >= 1)
            {
                gregory[0] = one / new BigDecimal(2, sigFigs);
            }

            // Calculate the next terms using 
            // G_1/n - G_2/(n - 1) + ... + (-1)^(n - 1) * G_n/1 = 1/(n + 1)
            // i.e. G_n = (-1)^(n - 1) * [1/(n + 1) - G_1/n + G_2/(n - 1) - G_3/(n - 2) + ...]

            for (int i = 1; i < n; ++i)
            {
                BigDecimal sum = one / new BigDecimal(i + 2, sigFigs);

                int k = i + 1;
                for (int j = 0; j < i; ++j)
                {
                    BigDecimal k_minus_j = new BigDecimal(k - j, sigFigs);
                    if (j % 2 == 0)
                    {
                        sum -= gregory[j] / k_minus_j;
                    }
                    else
                    {
                        sum += gregory[j] / k_minus_j;
                    }
                }

                if (i % 2 == 0)
                {
                    gregory[i] = sum;
                }
                else
                {
                    gregory[i] = -sum;
                }
            }
        }
    }
}
