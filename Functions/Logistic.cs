﻿using LinearNet.Structs.Fields;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Functions
{
    /// <summary>
    /// 
    /// </summary>
    /// <cat>numerics</cat>
    public static partial class MathFunctions
    {
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        /// <summary>
        /// Returns the logistic function evaluated at $x$,
        /// $$\sigma(x) = \frac{1}{1 + e^{-x}}$$
        /// </summary>
        /// <name>Logistic</name>
        /// <proto>double Logistic(double x)</proto>
        /// <cat>functions</cat>
        /// <param name="x"></param>
        /// <returns></returns>
        public static double Logistic(double x)
        {
            return 1.0 / (1.0 + Math.Exp(-x));
        }

        /// <summary>
        /// Returns the logistic function evaluated at a real value $x$, a <txt>BigDecimal</txt>, up to a specified number of significant figures.
        /// <!--inputs-->
        /// <!--returns-->
        /// </summary>
        /// <name>Logistic_BigDecimal</name>
        /// <proto>BigDecimal Logistic(BigDecimal x, int sigFigs = -1)</proto>
        /// <cat>functions</cat>
        /// <param name="x">The value at which to evaluate the logistic function.</param>
        /// <param name="sigFigs">
        /// <b>Optional</b>, defaults to -1.<br/>
        /// The number of significant figures in the returned result. <br/> 
        /// If negative, will default to the same number of significant figures as <txt>x</txt>.
        /// </param>
        /// <returns>$\sigma(x)$ accurate up to "<txt>sigFigs</txt>" significant figures.</returns>
        public static BigDecimal Logistic(BigDecimal x, int sigFigs = -1)
        {
            if (sigFigs < 0)
            {
                sigFigs = x.SignificantFigures;
            }

            sigFigs += 2;

            BigDecimal one = new BigDecimal(1, sigFigs);
            BigDecimal logistic = one / (one + BigDecimal.Exp(x, sigFigs));
            logistic.Round(sigFigs - 2);

            return logistic;
        }

        /// <summary>
        /// Returns the generalised logistic function evaluated at $x$,  
        /// $$\sigma_\alpha(x) = \frac{1}{(1 + e^{-x})^\alpha}$$
        /// </summary>
        /// <name>GeneralizedLogistic</name>
        /// <proto>double GeneralizedLogistic(double x, double alpha)</proto>
        /// <cat>functions</cat>
        /// <param name="x"></param>
        /// <param name="alpha"></param>
        /// <returns></returns>
        public static double GeneralizedLogistic(double x, double alpha)
        {
            return Math.Pow(1.0 + Math.Exp(-x), -alpha);
        }

        /// Approximate fast logistic function using linear intrapolation, optimized using FastLogisticLerpOptimizer
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static double FastLogistic(double x)
        {
            if (x > 0)
            {
                if (x < 1.202325307)
                {
                    return 0.5 + 0.223681713 * x;
                }
                if (x < 2.418980757)
                {
                    return 0.621371889 + 0.122734084 * x;
                }
                if (x < 5.111244503)
                {
                    return 0.85020756 + 0.028134046 * x;
                }
                return 1.0;
            }

            if (x > -1.202325307)
            {
                return 0.5 + 0.223681713 * x;
            }
            if (x > -2.418980757)
            {
                return 0.378628111 + 0.122734084 * x;
            }
            if (x > -5.111244503)
            {
                return 0.14979244 + 0.028134046 * x;
            }
            return 0.0;
        }
        

        /// <summary>
        /// Returns an approximate logistic function of evaluated at x.
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static float FastLogistic(float x)
        {
            float x2 = x * x;
            if (x > 0)
            {
                float exp = 1.0f + x + x2 * (0.555f + x2 * 0.143f);
                return 1.0f / (1.0f + 1.0f / exp);
            }
            else
            {
                float exp = 1.0f - x + x2 * (0.555f + x2 * 0.143f);
                return 1.0f / (1.0f + exp);
            }
        }

        /// <summary>
        /// Returns the logit function of $p$, where $0 \le p \le 1$. The logit function is the inverse of the logistic function. 
        /// $$\operatorname{logit}(p) = -\ln\Big(\frac{1}{p} - 1\Big)$$
        /// </summary>
        /// <name>Logit</name>
        /// <proto>double Logit(double p)</proto>
        /// <cat>functions</cat>
        /// <param name="p"></param>
        /// <returns></returns>
        public static double Logit(double p)
        {
            if (p < 0.0 || p > 1.0)
            {
                throw new ArgumentOutOfRangeException(nameof(p));
            }

            return Math.Log(p / (1.0 - p));
        }

        public static BigDecimal Logit(BigDecimal p)
        {
            if (p.IsNegative || p > BigDecimal.One)
            {
                throw new ArgumentOutOfRangeException(nameof(p));
            }

            return BigDecimal.Log(p / (BigDecimal.One - p));
        }
    }
}
