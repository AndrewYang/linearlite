﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Functions
{
    /// <summary>
    /// <p>
    /// A static class containing methods for computing special mathematical functions and series. 
    /// </p>
    /// <p>
    /// This class supports both high-precision and infinite-precision computing, 
    /// via the structs <txt>BigRational</txt>, <txt>BigDecimal</txt> and <txt>BigComplex</txt>.
    /// </p>
    /// </summary>
    /// <cat>numerics</cat>
    public static partial class MathFunctions
    {
    }
}
