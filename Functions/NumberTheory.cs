﻿using LinearNet.Global;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Functions
{
    /// <summary>
    /// 
    /// </summary>
    /// <cat>numerics</cat>
    public static partial class MathFunctions
    {
        /// <summary>
        /// Returns the sum of the $d$-th powers of the positive divisors of $n$, 
        /// $$\sum_{x | n} x^d$$
        /// </summary>
        /// <name>DivisorFunction</name>
        /// <proto>int DivisorFunction(int n, int d)</proto>
        /// <cat>functions</cat>
        /// <param name="n"></param>
        /// <param name="d"></param>
        /// <returns></returns>
        public static int DivisorFunction(int n, int d = 0)
        {
            int divisor = 0, i;
            for (i = 1; i * i < n; ++i)
            {
                if (n % d == 0)
                {
                    divisor += (int)(Math.Pow(i, d) + 0.5) + (int)(Math.Pow(n / i, d) + 0.5);
                }
            }

            if (i * i == n)
            {
                divisor += (int)(Math.Pow(i, d));
            }

            return divisor;
        }
        /// <summary>
        /// Returns the sum of the $d$-th powers of the positive divisors of $n$, returning 
        /// an object of type <txt>BigInteger</txt>.
        /// </summary>
        /// <name>BigDivisorFunction</name>
        /// <proto>BigInteger BigDivisorFunction(int n, int d)</proto>
        /// <cat>functions</cat>
        /// <param name="n"></param>
        /// <param name="d"></param>
        /// <returns></returns>
        public static BigInteger BigDivisorFunction(int n, int d = 0)
        {
            BigInteger divisor = BigInteger.Zero;

            int i = 1;
            for (; i * i < n; ++i)
            {
                if (n % d == 0)
                {
                    divisor += BigInteger.Pow(i, d) + BigInteger.Pow(n / i, d);
                }
            }

            if (i * i == n)
            {
                divisor += BigInteger.Pow(i, d);
            }

            return divisor;
        }

        /// <summary>
        /// Returns the Euler's totient function, $\phi(n)$, equal to the number of positive integers less than $n$ that are coprime to $n$.
        /// </summary>
        /// <name>EulerTotient</name>
        /// <proto>int EulerTotient(int n)</proto>
        /// <cat>functions</cat>
        /// <param name="n"></param>
        /// <returns></returns>
        public static int EulerTotient(int n)
        {
            int phi = n, i;
            for (i = 2; i * i <= n; i++)
            {
                if (n % i == 0)
                {
                    while (n % i == 0)
                    {
                        n /= i;
                    }
                    phi -= phi / i;
                }
            }
            if (n > 1)
            {
                phi -= phi / n;
            }
            return phi;
        }

        /// <summary>
        /// Returns the number of primes less than or equal to $n$, i.e. the prime-counting function evaluated at $n$.
        /// </summary>
        /// <name>PrimeCount</name>
        /// <proto>int PrimeCount(int n)</proto>
        /// <cat>functions</cat>
        /// <param name="n"></param>
        /// <returns></returns>
        public static int PrimeCount(int n)
        {
            int count = 0;
            for (int i = 2; i < n; ++i)
            {
                if (IsPrime(n))
                {
                    count++;
                }
            }
            return count;
        }

        /// <summary>
        /// Returns whether the integer $n$ is a prime.
        /// </summary>
        /// <name>IsPrime</name>
        /// <proto>bool IsPrime(int n)</proto>
        /// <cat>functions</cat>
        public static bool IsPrime(int n)
        {
            for (int i = 2; i * i <= n; ++i)
            {
                if (n % i == 0) return false;
            }
            return false;
        }

        /// <summary>
        /// Calculate $p(n)$, the number of partitions of a natural number $n$ (<a href="https://oeis.org/A000041">OEIS A000041</a>), using the algorithm
        /// as presented in <a href="https://arxiv.org/pdf/1205.5991.pdf">Johansson (2012)</a>.
        /// <para>Due to the fast growth of $p(n)$, method overflows for $n > 121$. For larger $n$, please see <a href="#PartitionLongCount"><txt>PartitionLongCount(int n)</txt></a></para>
        /// </summary>
        /// <name>PartitionCount</name>
        /// <proto>int PartitionCount(int n)</proto>
        /// <cat>functions</cat>
        /// <param name="n"></param>
        /// <param name="k"></param>
        /// <returns></returns>
        public static int PartitionCount(int n)
        {
            return Util.ToInt32(PartitionApproximateCount(n));
        }

        /// <summary>
        /// Calculate $p(n)$, the number of partitions of a natural number $n$, returning the result as a <txt>long</txt>.
        /// <para>This method overflows for $n > 405$. For larger values of $n$, see <a href="#PartitionApproximateCount"><txt>PartitionApproximateCount(int n)</txt></a>.</para>
        /// </summary>
        /// <name>PartitionLongCount</name>
        /// <proto>long PartitionLongCount(int n)</proto>
        /// <cat>functions</cat>
        public static long PartitionLongCount(int n)
        {
            return (long)(PartitionApproximateCount(n) + 0.5);
        }

        /// <summary>
        /// Calculate the approximate number of partitions of a natural number $n$, returning the result as a <txt>double</txt>, using the algorithm
        /// as presented in <a href="https://arxiv.org/pdf/1205.5991.pdf">Johansson (2012)</a>.
        /// For most values of $n$, this approximation is within 0.5 of the true partition count.
        /// </summary>
        /// <name>PartitionApproximateCount</name>
        /// <proto>double PartitionApproximateCount(int n)</proto>
        /// <cat>functions</cat>
        /// <param name="n"></param>
        /// <param name="k"></param>
        /// <returns></returns>
        public static double PartitionApproximateCount(int n)
        {
            if (n <= 0)
            {
                return 0.0;
            }

            double t = 24.0 * n - 1.0,
                factor = 4.0 / t,
                C_n = Constants.PiOver6 * Math.Sqrt(t),
                sum = 0.0;

            // N needs to be chosed so as to ensure that the error term is less than 0.5
            int N = (int)Math.Ceiling(Math.Sqrt(n)), k;

            for (k = 1; k <= N; ++k)
            {
                double arg = C_n / k;
                sum += Math.Sqrt(3.0 / k) * Calculate_A_kn(n, k) * (Math.Cosh(arg) - Math.Sinh(arg) / arg);
            }
            return factor * sum;
        }

        /// This method can be improved, see <a href="https://arxiv.org/pdf/1205.5991.pdf">Johansson (2012)</a>
        private static double Calculate_A_kn(int n, int k)
        {
            if (k <= 1)
            {
                return k;
            }
            if (k == 2)
            {
                return n % 2 == 0 ? 1 : -1;
            }

            int r = 2, m = n % k, two_k = 2 * k;
            double s = 0.0;

            for (int l = 0; l < two_k; ++l)
            {
                if (m == 0)
                {
                    s += (l % 2 == 0 ? 1 : -1) * Math.Cos(Constants.PiOver6 * (6.0 * l + 1.0) / k);
                }
                m += r;
                if (m >= k)
                {
                    m -= k;
                }
                r += 3;
                if (r >= k)
                {
                    r -= k;
                }
            }
            return Math.Sqrt(k / 3.0) * s;
        }
    }
}
