﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Functions
{
    /// <summary>
    /// 
    /// </summary>
    /// <cat>numerics</cat>
    public static partial class MathFunctions
    {
        private static readonly double[] _B_2n =
        {
            1.0,
            1.66666666666667E-01,
            -3.33333333333333E-02,
            2.38095238095238E-02,
            -3.33333333333333E-02,
            7.57575757575758E-02,
            -2.53113553113553E-01,
            1.16666666666667E+00,
            -7.09215686274510E+00,
            5.49711779448622E+01,
            -5.29124242424242E+02,
            6.19212318840580E+03,
            -8.65802531135531E+04,
            1.42551716666667E+06,
            -2.72982310678161E+07,
            6.01580873900642E+08,
            -1.51163157670922E+10,
            4.29614643061167E+11,
            -1.37116552050883E+13,
            4.88332318973592E+14,
            -1.92965793419401E+16
        };

        public static double Polygamma(double x, int m, int N = 20)
        {
            if (m < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(m));
            }
            if (m == 0)
            {
                return Digamma(x, N);
            }

            if (double.IsNaN(x)) return double.NaN;
            if (x <= 0)
            {
                // Poles at all non-positive integers
                if (x.IsInt32())
                {
                    return double.NaN;
                }
                return Polygamma(1.0 - x, m, N) - 
                    Factorial(m) / Math.Tan(Math.PI * x);
            }

            int m_1 = m - 1;
            double sum = 0.0;
            for (int n = 0; n < N; ++n)
            {
                int k = 2 * n;
                sum += Math.Exp(LogPermutations(k + m_1, m_1) + _B_2n[n] - (k + m) * Math.Log(x));
            }

            return m % 2 == 0 ? -sum : sum;
        }
    }
}
