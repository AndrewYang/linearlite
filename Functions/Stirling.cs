﻿using LinearNet.Global;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Functions
{
    /// <summary>
    /// Class for calculation of Stirling numbers 
    /// </summary>
    /// <cat>numerics</cat>
    public static partial class MathFunctions
    {
        /// <summary>
        /// Calculate the Stirling numbers (Stirling numbers of the second kind, 
        /// also known as Stirling partition number),
        /// $$S(n, k) = \begin{Bmatrix}
        ///  n \\
        ///  k
        /// \end{Bmatrix} = \frac{1}{k!} \sum_{i=0}^{k} (-1)^i \dbinom{k}{i} (k - i)^n $$
        /// </summary>
        /// <name>BigStirling</name>
        /// <proto>BigInteger BigStirling(int n, int k)</proto>
        /// <cat>functions</cat>
        /// <param name="n"></param>
        /// <param name="k"></param>
        /// <returns></returns>
        public static BigInteger BigStirling(int n, int k)
        {
            if (n < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(n), ExceptionMessages.NegativeNotAllowed);
            }
            if (k < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(k), ExceptionMessages.NegativeNotAllowed);
            }
            if (k > n)
            {
                return BigInteger.Zero;
            }
            if (k == n)
            {
                return BigInteger.One;
            }
            if (k == 0)
            {
                return BigInteger.Zero;
            }

            // The priority is to ensure that we're working in the integers 
            BigInteger sum = BigInteger.Pow(k, n), p = 1, q = 1;
            int k1 = k + 1;
            for (int i = 1; i < k; ++i)
            {
                p *= k1 - i;
                q *= i;

                BigInteger term = p / q * BigInteger.Pow(k - i, n);
                sum += (i % 2 == 0) ? term : -term;
            }
            return sum / (q * k);
        }

        /// <summary>
        /// Calculates the natural log of Stirling numbers of the second kind, returning the result 
        /// as a <txt>double</txt>.
        /// $$\log S(n, k) = \log \begin{Bmatrix}
        ///  n \\
        ///  k
        /// \end{Bmatrix}$$
        /// </summary>
        /// <name>LogStirling</name>
        /// <proto>double LogStirling(int n, int k)</proto>
        /// <cat>functions</cat>
        /// <param name="n"></param>
        /// <param name="k"></param>
        /// <returns></returns>
        public static double LogStirling(int n, int k)
        {
            if (n < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(n), ExceptionMessages.NegativeNotAllowed);
            }
            if (k < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(k), ExceptionMessages.NegativeNotAllowed);
            }
            if (k > n)
            {
                return double.NegativeInfinity;
            }
            if (k == n)
            {
                return 0.0;
            }
            if (k == 0)
            {
                return double.NegativeInfinity;
            }

            // The priority is to ensure that we're working in the integers 
            double sum = Math.Pow(k, n), ln_p = 0.0, ln_q = 0.0;
            int k1 = k + 1;
            for (int i = 1; i < k; ++i)
            {
                ln_p += Math.Log(k1 - i);
                ln_q += Math.Log(i);

                double term = Math.Exp(ln_p - ln_q + n * Math.Log(k - i));
                sum += (i % 2 == 0) ? term : -term;
            }
            return Math.Log(sum) - ln_q - Math.Log(k);
        }
    }
}
