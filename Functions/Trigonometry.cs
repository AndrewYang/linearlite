﻿using LinearNet.Global;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Functions
{
    /// <summary>
    /// Trigonometric math functions
    /// </summary>
    /// <cat>numerics</cat>
    public static partial class MathFunctions
    {
        /// <summary>
        /// Returns the Gudermannian function evaluated at a real value $x$, $$\operatorname{gd}(x) = \int^{x}_0{\frac{1}{\cosh(t)} dt}$$
        /// </summary>
        /// <name>Gudermannian</name>
        /// <proto>double Gudermannian(double x)</proto>
        /// <cat>functions</cat>
        /// <param name="x"></param>
        /// <returns></returns>
        public static double Gudermannian(double x)
        {
            double e = Math.Exp(x);

            if (e % Math.PI == Constants.PiOver2)
            {
                return double.PositiveInfinity;
            }
            return 2 * Math.Atan(e) - Constants.PiOver2;
        }

        /// <summary>
        /// Returns the Gudermannian inverse function evaluated at a real value $x$, 
        /// $$\operatorname{gd}^{-1}(x) = \int^{x}_0{\frac{1}{\cos(t)} dt},$$ 
        /// where $x\in(-\frac{\pi}{2}, \frac{\pi}{2})$.
        /// </summary>
        /// <name>GudermannianInverse</name>
        /// <proto>double GudermannianInverse(double x)</proto>
        /// <cat>functions</cat>
        /// <param name="x"></param>
        /// <returns></returns>
        public static double GudermannianInverse(double x)
        {
            if (x <= -Constants.PiOver2 || x >= Constants.PiOver2)
            {
                throw new ArgumentOutOfRangeException(nameof(x));
            }

            return Math.Log(Math.Abs(1.0 + Math.Sin(x))) - Math.Log(Math.Cos(x));
        }
    }
}
