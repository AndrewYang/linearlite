﻿using LinearNet.Structs;
using LinearNet.Structs.Fields;
using System;
using System.Diagnostics;

namespace LinearNet.Functions
{
    /// <summary>
    /// Zeta functions partial class
    /// </summary>
    /// <cat>numerics</cat>
    public static partial class MathFunctions
    {
        /// <summary>
        /// Calculate and returns the Riemann zeta function for a real value $s$.
        /// </summary>
        /// <param name="s"></param>
        /// <param name="N"></param>
        /// <returns></returns>
        public static double Zeta(double s, int N = 100)
        {
            if (double.IsNaN(s))
            {
                return double.NaN;
            }
            if (double.IsPositiveInfinity(s))
            {
                return 1.0;
            }

            double zeta = 0.0;
            double log_d0 = Calculate_LogDk(N, 0);
            for (int k = 1; k <= N; ++k)
            {
                double log_Dk = Calculate_LogDk(N, k), 
                        exponent = log_Dk - s * Math.Log(k) - log_d0;

                // -infty means adding 0 to zeta, skip
                if (!double.IsNegativeInfinity(exponent))
                {
                    int sign = k % 2 == 0 ? -1 : 1;
                    zeta += sign * Math.Exp(exponent);
                }
            }
            return zeta / (1.0 - Math.Pow(2.0, 1 - s));
        }
        /// <summary>
        /// Calculates and returns the Riemann zeta function for a complex value $z$.
        /// </summary>
        /// <param name="z"></param>
        /// <param name="N"></param>
        /// <returns></returns>
        public static Complex Zeta(Complex z, int N = 100)
        {
            if (Complex.IsNaN(z))
            {
                return Complex.NaN;
            }
            if (double.IsPositiveInfinity(z.Real))
            {
                return Complex.One;
            }

            Complex zeta = Complex.Zero;
            double log_d0 = Calculate_LogDk(N, 0);
            for (int k = 1; k <= N; ++k)
            {
                double log_Dk = Calculate_LogDk(N, k);
                Complex exponent = log_Dk - z * Math.Log(k) - log_d0;

                int sign = k % 2 == 0 ? -1 : 1;
                zeta += sign * Complex.Exp(exponent);
            }
            return zeta / (1.0 - Complex.Pow(2.0, 1.0 - z));
        }
        private static double Calculate_LogDk(int N, int k)
        {
            double[] values = new double[N - k + 1];
            double log4 = Math.Log(4.0);
            for (int j = k; j <= N; ++j)
            {
                if (N - j > 2 * j)
                {
                    // P(N + j + 1, 2j + 1) / (2j)! * 4^j
                    values[j - k] = LogPermutations(N + j + 1, 2 * j + 1) - LogFactorial(2 * j) + j * log4;
                }
                else
                {
                    // P(N + j + 1, N - j + 1)  / (N - j)! * 4^j
                    values[j - k] = LogPermutations(N + j + 1, N - j + 1) - LogFactorial(N - j) + j * log4;
                }
            }

            // ln(D1 + D2 + ... + Dn) = ln(min(D1 / min + D2 / min + ... + Dn / min) = ln(min) + ln(D1 / min + D2 / min + ... + Dn / min)
            double min = double.MaxValue;
            for (int j = 0; j < values.Length; ++j)
            {
                if (values[j] < min)
                {
                    min = values[j];
                }
            }

            if (min <= 0)
            {
                return double.NegativeInfinity;
            }

            // Di / min = exp(ln(Di / min)) = exp(ln(Di) - ln(min))
            double ln_min = Math.Log(min), sum = 0.0;
            for (int j = 0; j < values.Length; ++j)
            {
                sum += Math.Exp(values[j] - ln_min);
            }
            return Math.Log(N) + ln_min + Math.Log(sum);
        }

        public static BigDecimal Zeta(BigDecimal s)
        {
            throw new NotImplementedException();
        }
    }
}
