﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Geometry.Curves
{
    /// <summary>
    /// Implements a space-filling function, which is a continuous map of a one-dimensional interval [a, b]
    /// into a n-dimensional space.
    /// <p>If n = 2, the function is also known as a plane-filling function.</p>
    /// </summary>
    /// <typeparam name="F"></typeparam>
    /// <typeparam name="T"></typeparam>
    /// <cat>numerics</cat>
    public interface ISpaceFillingCurve<F, T>
    {
        /// <summary>
        /// Maps the one-dimensional input into a vector in n-dimensional space.
        /// </summary>
        /// <param name="x">The one-dimensional value.</param>
        /// <returns>A vector in n-dimensional space.</returns>
        T Map(F x);

        /// <summary>
        /// Finds the pre-image of a n-dimensional vector.
        /// </summary>
        /// <param name="x">The n-dimensional vector.</param>
        /// <returns>A scalar.</returns>
        F Inverse(T x);
    }
}
