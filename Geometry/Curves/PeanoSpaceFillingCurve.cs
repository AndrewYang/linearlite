﻿using LinearNet.Optimisation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Geometry.Curves
{
    /// <summary>
    /// Implements a Peano space-fulling curve.
    /// </summary>
    /// <cat>numerics</cat>
    public class PeanoSpaceFillingCurve : 
        ISpaceFillingCurve<double, double[]>, 
        ISpaceFillingCurve<float, float[]>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public double[] Map(double x)
        {
            return null;
        }
        public double Inverse(double[] x)
        {
            return 0;
        }

        public float[] Map(float x)
        {
            return null;
        }
        public float Inverse(float[] x)
        {
            return 0;
        }
    }
}
