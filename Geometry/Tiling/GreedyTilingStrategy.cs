﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Geometry.Tiling
{
    public class GreedyTilingStrategy : ITilingStrategy
    {
        private int[] _tileSizes;

        public GreedyTilingStrategy(IList<int> tileSizes)
        {
            _tileSizes = tileSizes.ToArray();
        }
        public override SpatialPartition2D Partition(int x, int y, int xLength, int yLength)
        {
            //Debug.WriteLine("Generating tiling for: " + xLength + " x " + yLength);
            List<Tile2D> tiles = new List<Tile2D>();
            Fill(tiles, 0, xLength, 0, yLength, get_tile_size(Math.Min(xLength, yLength)));
            return new SpatialPartition2D(0, 0, xLength, yLength, tiles);
        }
        private void Fill(List<Tile2D> tiles, int row_start, int row_end, int col_start, int col_end, int tileSize)
        {
            if (tileSize < 0)
            {
                tiles.Add(new Tile2D(row_start, col_start, row_end - row_start, col_end - col_start, true));
                return;
            }

            int col_length = col_end - col_start,
                row_length = row_end - col_start,
                last_col = col_end - tileSize,
                last_row = row_end - tileSize,
                c, r;

            for (c = col_start; c <= last_col; c += tileSize)
            {
                for (r = row_start; r <= last_row; r += tileSize)
                {
                    tiles.Add(new Tile2D(r, c, tileSize, tileSize, false));
                }
            }

            // Check for column remainders
            int col_remainder = col_length % tileSize;
            if (col_remainder > 0)
            {
                int newSize = get_tile_size(Math.Min(row_length, col_remainder));
                Fill(tiles, row_start, row_end, col_end - col_remainder, col_end, newSize);
            }

            // Check for row remainders
            int row_remainder = (row_end - row_start) % tileSize;
            if (row_remainder > 0)
            {
                int newSize = get_tile_size(Math.Min(row_remainder, col_length - col_remainder));
                Fill(tiles, row_end - row_remainder, row_end, col_start, col_end - col_remainder, newSize);
            }
        }
        private int get_tile_size(int availableSize)
        {
            // If even the smallest tile is too big, return -1
            if (_tileSizes[0] > availableSize)
            {
                return -1;
            }

            int i = _tileSizes.Length - 1;
            while (i >= 0 && _tileSizes[i] > availableSize) --i;

            // unsafe
            return _tileSizes[i];
        }

        public override SpatialPartition3D Partition(int x, int y, int z, int xLength, int yLength, int zLength)
        {
            List<Tile3D> tiles = new List<Tile3D>();
            Fill(tiles, x, x + xLength, y, y + yLength, z, z + zLength, get_tile_size(Util.Min(xLength, yLength, zLength)));
            return new SpatialPartition3D(x, y, z, xLength, yLength, zLength, tiles);
        }
        private void Fill(List<Tile3D> tiles, int x_start, int x_end, int y_start, int y_end, int z_start, int z_end, int tileSize)
        {
            if (tileSize < 0)
            {
                tiles.Add(new Tile3D(x_start, y_start, z_start, x_end - x_start, y_end - y_start, z_end - z_start, true));
                return;
            }

            int x_length = x_end - x_start,
                y_length = y_end - y_start,
                z_length = z_end - z_start,

                last_x = x_end - tileSize,
                last_y = y_end - tileSize,
                last_z = z_end - tileSize,
                x, y, z;

            for (x = x_start; x <= last_x; x += tileSize)
            {
                for (y = y_start; y <= last_y; y += tileSize)
                {
                    for (z = z_start; z <= last_z; z += tileSize)
                    {
                        tiles.Add(new Tile3D(x, y, z, tileSize, tileSize, tileSize, false));
                    }
                }
            }

            int x_remainder = x_length % tileSize, 
                y_remainder = y_length % tileSize,
                z_remainder = z_length % tileSize;

            if (y_remainder > 0)
            {
                int new_size = get_tile_size(Util.Min(y_remainder, x_length, z_length));
                Fill(tiles, x_start, x_end, y_end - y_remainder, y_end, z_start, z_end, new_size);
            }
            if (x_remainder > 0)
            {
                int new_size = get_tile_size(Util.Min(x_remainder, y_length - y_remainder, z_length));
                Fill(tiles, x_end - x_remainder, x_end, y_start, y_end - y_remainder, z_start, z_end, new_size);
            }
            if (z_remainder > 0)
            {
                int new_size = get_tile_size(Util.Min(x_length - x_remainder, y_length - y_remainder, z_remainder));
                Fill(tiles, x_start, x_end - x_remainder, y_start, y_end - y_remainder, z_end - z_remainder, z_end, new_size);
            }
        }
    }
}
