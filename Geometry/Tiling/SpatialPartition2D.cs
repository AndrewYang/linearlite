﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Geometry.Tiling
{
    public class SpatialPartition2D
    {
        public int FirstRow { get; set; }
        public int FirstColumn { get; set; }
        public int Rows { get; set; }
        public int Columns { get; set; }

        public List<Tile2D> Tiles { get; set; }

        public SpatialPartition2D(int first_row, int first_col, int rows, int columns, List<Tile2D> tiles)
        {
            FirstRow = first_row;
            FirstColumn = first_col;
            Rows = rows;
            Columns = columns;
            Tiles = tiles;
        }

        public void Shift(int rows, int columns)
        {
            FirstRow += rows;
            FirstColumn += columns;

            foreach (Tile2D t in Tiles)
            {
                t.Shift(rows, columns);
            }
        }
        public void Transpose()
        {
            int temp = FirstRow;
            FirstRow = FirstColumn;
            FirstColumn = temp;

            temp = Rows;
            Rows = Columns;
            Columns = temp;

            foreach (Tile2D t in Tiles)
            {
                t.Transpose();
            }
        }
        public void ReverseRows()
        {
            int row_end = FirstRow + Rows;
            foreach (Tile2D t in Tiles)
            {
                t.RowStart = row_end - t.RowEnd;
            }
        }

        public SpatialPartition2D Copy()
        {
            List<Tile2D> copy = new List<Tile2D>();
            foreach (Tile2D t in Tiles)
            {
                copy.Add(t.Copy());
            }
            return new SpatialPartition2D(FirstRow, FirstColumn, Rows, Columns, copy);
        }
    }
}
