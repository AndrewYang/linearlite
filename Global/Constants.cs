﻿using LinearNet.Helpers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Global
{
    public static class Constants
    {
        /// <summary>
        /// Sqrt(2) in double precision
        /// </summary>
        public const double Sqrt2 = 1.414213562373095048801688724209698078569671875377;
        /// <summary>
        /// Sqrt(2) in float (single) precision
        /// </summary>
        public const float Sqrt2_f = 1.414213562373095048801688724209698078569671875377f;
        /// <summary>
        /// Sqrt(2) in decimal precision
        /// </summary>
        public const decimal Sqrt2_m = 1.414213562373095048801688724209698078569671875376948073176679737990732478462107039m;


        public static readonly double SqrtPi = Math.Sqrt(Math.PI);

        /// <summary>
        /// Sqrt(2 * pi)
        /// </summary>
        public const double Sqrt2Pi = 2.5066282746310005024157652848110452530069867406099;

        /// <summary>
        /// Ln(2 * pi)
        /// </summary>
        public const double Ln2Pi = 1.8378770664093454835606594728112352797227949472756;

        /// <summary>
        /// pi / 2
        /// </summary>
        public const double PiOver2 = 1.5707963267948966192313216916397514420985846996876;
        public const float PiOver2_f = 1.5707963267948966192313216916397514420985846996876f;

        public const double TwoPi = Math.PI * 2.0;

        /// <summary>
        /// pi / 6
        /// </summary>
        public const double PiOver6 = 0.52359877559829887307710723054658381403286156656252;

        public static readonly double Ln2 = Math.Log(2.0);

        internal const string SparseMatrixDirectory = @"C:\Users\yanga\Documents\Matrices\";
    }
}
