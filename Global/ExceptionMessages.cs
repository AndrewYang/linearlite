﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Global
{
    public static class ExceptionMessages
    {
        public const string NaNNotAllowed = "The variable's value is NaN.";
        public const string NonPositiveNotAllowed = "The variable's value is not positive.";
        public const string NegativeNotAllowed = "The variable's value is negative.";
        public const string InfinityNotAllowed = "The variable's value is infinity.";
        public const string NotBetween0And1 = "The variable's value is not between 0 and 1.";
        public const string NotBetweenPlusMinus1 = "The variable's value is not between -1 and 1.";
        public const string DistributionNotSupported = "The distribution is not supported.";
        public const string ZeroNotAllowed = "The variable's value is 0.";
        public const string TypeNotSupported = "This method is not supported for the specified type.";

        public const string MatrixSize = "The dimensions of the matrices do not match.";
        public const string MatrixColumnSize = "The number of columns do not match.";
        public const string MatrixRowSize = "The number of rows do not match.";
        public const string MatrixNotSquare = "The matrix is not square.";

        public const string VectorSizeMismatch = "The dimensions of the vectors don't match.";

        public const string TensorOrderMismatch = "The orders of the tensors do not match.";
        public const string CannotConnectToStorage = "Unable to connect to matrix storage.";
    }
}
