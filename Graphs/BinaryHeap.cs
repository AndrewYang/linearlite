﻿using LinearNet.Global;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace LinearNet.Graphs
{
    /// <summary>
    /// Implementation of a binary heap. For a heap of size n, the following operations are supported:
    /// 1. Adding a new node in O(log n) time
    /// 2. Removing the root node in O(log n) time
    /// 
    /// </summary>
    public class BinaryHeap<T> where T : IComparable<T>
    {
        private T[] _values;
        private int _count;


        /// <summary>
        /// Create a binary heap with the specified initial capacity. Defaults to 2^4 - 1
        /// </summary>
        /// <param name="capacity">The initial capacity</param>
        public BinaryHeap(int capacity = 15)
        {
            if (capacity <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(capacity), ExceptionMessages.NegativeNotAllowed);
            }
            _values = new T[capacity];
            _count = 0;
        }

        /// <summary>
        /// Create a binary heap from a collection of elements 
        /// </summary>
        /// <param name="elements">The elements to include in the heap</param>
        public BinaryHeap(IEnumerable<T> elements)
        {
            if (elements is null)
            {
                throw new ArgumentNullException(nameof(elements));
            }

            _values = new T[elements.Count()];
            _count = 0;

            foreach (T e in elements)
            {
                Add(e);
            }
        }


        /// <summary>
        /// Add an element to this heap. 
        /// Throws ArgumentOutOfRangeException if the heap if not large enough to store the additional element.
        /// </summary>
        /// <param name="e">The element to add to this heap.</param>
        public void Add(T e)
        {
            if (_count >= _values.Length)
            {
                Array.Resize(ref _values, _values.Length * 2);
            }

            _values[_count] = e;

            int index = _count, parent = (index - 1) / 2;

            // Percolate up the heap
            while (index > 0)
            {
                // Check if child < parent
                if (_values[index].CompareTo(_values[parent]) >= 0)
                {
                    break;
                }

                // Swap
                T temp = _values[index];
                _values[index] = _values[parent];
                _values[parent] = temp;

                // Recurse up the tree
                index = parent;
                parent = (index - 1) / 2;
            }

            ++_count;
        }

        /// <summary>
        /// Removes the root element from this heap, and returns it
        /// </summary>
        public T Pop()
        {
            if (_count <= 0)
            {
                throw new IndexOutOfRangeException();
            }

            T root = _values[0];

            // Remove the tip
            T tip = _values[--_count];

            int parentIndex = 0,
                child1Index,
                child2Index,
                lowestIndex;

            bool inRange1, 
                inRange2;

            // Percolate down
            while (parentIndex < _count)
            {
                child1Index = parentIndex * 2 + 1;
                child2Index = child1Index + 1;

                inRange1 = child1Index < _count;
                inRange2 = child2Index < _count;

                if (inRange1 && inRange2)
                {
                    if (_values[child1Index].CompareTo(_values[child2Index]) < 0)
                    {
                        lowestIndex = child1Index;
                    }
                    else
                    {
                        lowestIndex = child2Index;
                    }
                }
                else if (inRange1)
                {
                    lowestIndex = child1Index;
                }
                else if (inRange2)
                {
                    lowestIndex = child2Index;
                }
                else
                {
                    return root; // Reached the end 
                }

                // No further comparisons needed
                if (tip.CompareTo(_values[lowestIndex]) <= 0)
                {
                    return root;
                }

                // Swap
                T temp = _values[lowestIndex];
                _values[lowestIndex] = _values[parentIndex];
                _values[parentIndex] = temp;

                // Percolate down
                parentIndex = lowestIndex;
            }

            // Should never get here
            return root;
        }

        /// <summary>
        /// Returns the root element from this heap.
        /// </summary>
        /// <returns>The root element of this heap (the minimum element)</returns>
        public T Peek()
        {
            if (_count <= 0)
            {
                throw new ArgumentOutOfRangeException();
            }
            return _values[0];
        }
        

        /// <summary>
        /// Print out this tree's structure
        /// </summary>
        internal void Print()
        {
            int levelSize = 1;
            int indexWithinLevel = 0;

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < _count; ++i)
            {
                sb.Append(_values[i]);
                sb.Append('\t');

                if (++indexWithinLevel == levelSize)
                {
                    sb.Append(Environment.NewLine);
                    levelSize *= 2;
                    indexWithinLevel = 0;
                }
            }
            Debug.WriteLine(sb.ToString());
        }
    }
}
