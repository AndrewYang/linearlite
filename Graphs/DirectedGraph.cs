﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LinearNet.Graphs
{
    public class DirectedGraph<T>
    {
        private readonly Dictionary<T, Vertex<T>> _vertices;

        public DirectedGraph(IEnumerable<T> vertices, IEnumerable<Tuple<T, T>> edges)
        {
            Dictionary<T, Vertex<T>> vertexDict = new Dictionary<T, Vertex<T>>();
            foreach (T vertex in vertices)
            {
                vertexDict.Add(vertex, new Vertex<T>(vertex));
            }

            // Add all edges 
            foreach (Tuple<T, T> edge in edges)
            {
                if (edge is null)
                {
                    throw new ArgumentNullException(nameof(edges), $"'{nameof(edges)}' contains a null element.");
                }
                if (!vertexDict.ContainsKey(edge.Item1))
                {
                    throw new ArgumentOutOfRangeException(nameof(edges), $"'{nameof(edges)}' contains an edge that references the unknown vertex: '{edge.Item1.ToString()}'.");
                }
                if (!vertexDict.ContainsKey(edge.Item2))
                {
                    throw new ArgumentOutOfRangeException(nameof(edges), $"'{nameof(edges)}' contains an edge that references the unknown vertex: '{edge.Item2.ToString()}'.");
                }
                vertexDict[edge.Item1].Neighbours.Add(edge.Item2);
            }

            _vertices = vertexDict;
        }

        internal void ClearVisited()
        {
            foreach (Vertex<T> v in _vertices.Values)
            {
                v.Visited = false;
            }
        }

        /// <summary>
        /// Returns the reach of the set of vertices.
        /// <para>Any vertices not in the graph will be ignored.</para>
        /// </summary>
        /// <param name="vertices"></param>
        /// <returns></returns>
        public HashSet<T> Reach(IEnumerable<T> vertices)
        {
            ClearVisited();

            foreach (T v in vertices)
            {
                if (!_vertices.ContainsKey(v))
                {
                    continue;
                }

                VisitReach(_vertices[v]);
            }

            HashSet<T> reach = new HashSet<T>();
            foreach (Vertex<T> vertex in _vertices.Values)
            {
                if (vertex.Visited)
                {
                    reach.Add(vertex.Value);
                }
            }
            return reach;
        }

        /// <summary>
        /// Mark all vertices in the reach of a vertex as visited
        /// </summary>
        /// <param name="vertex"></param>
        internal void VisitReach(Vertex<T> vertex)
        {
            // Already visited
            if (vertex.Visited) return;

            // Set as visited
            vertex.Visited = true;

            foreach (T neighbour in vertex.Neighbours)
            {
                Vertex<T> n = _vertices[neighbour];
                if (!n.Visited)
                {
                    // Depth-first
                    VisitReach(n);
                }
            }
        }

    }
    public class Vertex<T>
    {
        public T Value { get; private set; }
        public HashSet<T> Neighbours { get; set; }

        internal bool Visited { get; set; }

        public Vertex(T value, HashSet<T> neighbours)
        {
            Value = value;
            Neighbours = neighbours;
        }
        public Vertex(T value, IEnumerable<T> neighbours)
        {
            Value = value;
            Neighbours = new HashSet<T>(neighbours);
        }
        public Vertex(T value)
        {
            Value = value;
            Neighbours = new HashSet<T>();
        }
    }
}
