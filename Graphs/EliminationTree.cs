﻿using LinearNet.Global;
using LinearNet.Structs;
using System;
using System.Runtime.CompilerServices;

namespace LinearNet.Graphs
{
    /// <summary>
    /// Implements the elimination tree for a sparse matrix 
    /// 
    /// This implementation is designed for nodes represented by a non-negative int32, and is well-suited 
    /// for nodes that are sequentially ordered.
    /// 
    /// It keeps track of the parent of each node.
    /// </summary>
    public sealed class EliminationTree
    {
        private readonly int _numNodes;
        private readonly int[] _parents;

        internal int[] Parents { get { return _parents; } }
        internal int NodeCount { get { return _numNodes; } }

        internal EliminationTree(int n, int[] parents)
        {
            _numNodes = n;
            _parents = parents;
        }

        /// <summary>
        /// Based on Davis, T. (2006) page 42.
        /// </summary>
        /// <param name="matrix"></param>
        /// <returns></returns>
        internal static EliminationTree Calculate<T>(CSCSparseMatrix<T> matrix) where T : new()
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            if (!matrix.IsSquare)
            {
                throw new ArgumentOutOfRangeException(ExceptionMessages.MatrixNotSquare);
            }

            return Calculate(matrix.Rows, matrix.ColumnIndices, matrix.RowIndices);
        }
        internal static EliminationTree Calculate<T>(CSRSparseMatrix<T> matrix) where T : new()
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            if (!matrix.IsSquare)
            {
                throw new ArgumentOutOfRangeException(ExceptionMessages.MatrixNotSquare);
            }

            return Calculate(matrix.Rows, matrix.RowIndices, matrix.ColumnIndices);
        }
        private static EliminationTree Calculate(int dim, int[] columnIndices, int[] rowIndices)
        {
            int[] parent = new int[dim], ancestor = new int[dim];

            // Iterate through the nodes column-wise
            for (int c = 0; c < dim; ++c)
            {
                // Node c does not have a parent (yet)
                parent[c] = -1;

                // Node c does not have an ancestor (yet) - since it has no parent
                ancestor[c] = -1;

                int col_end = columnIndices[c + 1];
                for (int i = columnIndices[c]; i < col_end; ++i)
                {
                    // Holds the next node in our path from r -> c
                    int rnext;

                    // Traverse the path r -> c, by passing through r's ancestors.
                    // The ancestor of r at the end of this iteration is c, and we 
                    // also update all ancestors of r that we come across to c.
                    for (int r = rowIndices[i]; r != -1 && r < c; r = rnext)
                    {
                        rnext = ancestor[r];
                        ancestor[r] = c;
                        if (rnext == -1)
                        {
                            parent[r] = c;
                        }
                    }
                }
            }
            return new EliminationTree(dim, parent);
        }

        /// <summary>
        /// Calculate a postordering of this elimination tree.
        /// This method uses a more readable but less efficient algorithm (although asymptotically
        /// the same complexity) as the one in Davis, T. (2006) pg 45.
        /// 
        /// In particular, the method uses the pointer/value array pattern for temporarily storing
        /// the children of each node instead of a linked list. This consumes 2n + 1 memory where n
        /// is the size of this tree. 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="postOrdering">n-array containing the postordering of the set of n nodes.</param>
        /// <param name="levels">n-array containing the levels of each node (in original order)</param>
        internal void CalculatePostOrdering(int[] postOrdering, int[] levels = null)
        {
            if (postOrdering is null)
            {
                throw new ArgumentNullException(nameof(postOrdering));
            }
            if (postOrdering.Length != _numNodes)
            {
                throw new ArgumentOutOfRangeException(nameof(postOrdering));
            }
            if (levels != null && levels.Length != _numNodes)
            {
                throw new ArgumentOutOfRangeException(nameof(levels));
            }

            int[] pointers = new int[_numNodes + 1];
            int[] children = new int[_numNodes];
            int[] stack = new int[_numNodes];

            // Populate the pointer/children arrays
            // Step 1: count the number of children that each node has, and temporarily 
            // store it inside pointers (offset by +1)
            for (int i = 0; i < _numNodes; ++i)
            {
                int parent = _parents[i];
                if (parent >= 0)
                {
                    pointers[parent + 1]++;
                }
            }

            // Step 2: Accumulate the pointers array
            for (int i = 1; i <= _numNodes; ++i)
            {
                pointers[i] += pointers[i - 1];
            }

            // Step 3: Populate the children array, using the stack as a workspace to 
            // hold the number of elements already added
            for (int i = 0; i < _numNodes; ++i)
            {
                int parent = _parents[i];
                if (parent >= 0)
                {
                    children[pointers[parent] + stack[parent]++] = i;
                }
            }

            // Set stack to -1
            for (int i = 0; i < _numNodes; ++i)
            {
                stack[i] = -1;
            }

            // Loop through the nodes, and begin DFS on all roots
            // The stack is assumed to be empty at the start of every dfs call
            // k keeps track of the index of 'postordering' that we should write 
            // to next.
            int k = 0;

            if (levels is null)
            {
                for (int i = 0; i < _numNodes; ++i)
                {
                    if (_parents[i] < 0)
                    {
                        DFS(i, ref k, pointers, children, stack, postOrdering);
                    }
                }
            }
            else
            {
                for (int i = 0; i < _numNodes; ++i)
                {
                    if (_parents[i] < 0)
                    {
                        DFS(i, ref k, pointers, children, stack, postOrdering, levels);
                    }
                }
            }
        }

        /// <summary>
        /// Start dfs at node i
        /// </summary>
        /// <param name="i"></param>
        /// <param name="k"></param>
        /// <param name="pointers"></param>
        /// <param name="children"></param>
        /// <param name="stack"></param>
        /// <param name="post"></param>
        private void DFS(int i, ref int k, int[] pointers, int[] children, int[] stack, int[] post, int[] levels)
        {
            int top = 0;
            stack[0] = i;
            int level = 0;

            while (top >= 0)
            {
                //stack.Print();

                int parent = stack[top];
                int children_start = pointers[parent],
                    children_end = pointers[parent + 1],
                    t1 = top + 1;

                // Check if parent is a leaf i.e. no children
                if (children_end == children_start)
                {
                    --top;
                    post[k++] = parent;

                    // Set the level - not moving up a level, so don't decrement 
                    levels[parent] = level;
                }

                // If parent contains children, push all children to the stack
                // in inverse order, let the loop take care of their iteration
                else
                {
                    // If the next element is a child of node 'parent', then we have 
                    // finished recursing over all the children of 'parent', and we 
                    // should simply add 'parent' to the postordering.
                    if (t1 < _numNodes && stack[t1] >= 0 && _parents[stack[t1]] == parent)
                    {
                        stack[t1] = -1; // clear stack at index
                        --top;
                        post[k++] = parent;

                        // Set the level - since we are moving up a level, the level is decremented
                        levels[parent] = --level;
                    }
                    else
                    {
                        // Push all children in inverse order into stack
                        for (int p = children_end - 1; p >= children_start; --p)
                        {
                            // DFS at the child of index p
                            stack[++top] = children[p];
                        }
                        ++level;
                    }
                }
            }
        }
        private void DFS(int i, ref int k, int[] pointers, int[] children, int[] stack, int[] post)
        {
            int top = 0;
            stack[0] = i;

            while (top >= 0)
            {
                int parent = stack[top];
                int children_start = pointers[parent],
                    children_end = pointers[parent + 1],
                    t1 = top + 1;

                if (children_end == children_start)
                {
                    --top;
                    post[k++] = parent;
                }
                else
                {
                    if (t1 < _numNodes && stack[t1] >= 0 && _parents[stack[t1]] == parent)
                    {
                        stack[t1] = -1; // clear stack at index
                        --top;
                        post[k++] = parent;
                    }
                    else
                    {
                        for (int p = children_end - 1; p >= children_start; --p)
                        {
                            stack[++top] = children[p];
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Returns the root of node k
        /// </summary>
        /// <param name="k">The node index to return the root of</param>
        /// <returns></returns>
        internal int GetRoot(int k)
        {
            while (true)
            {
                int pk = _parents[k];
                if (pk < 0)
                {
                    return k;
                }
                k = pk;
            }
        }

        /// <summary>
        /// Calculate the first descendant of each node of the tree (in original order).
        /// Values are written to the 'firstDesc' array
        /// </summary>
        /// <param name="firstDesc">The array holding the first descendants.</param>
        internal void CalculateFirstDescendant(int[] postOrdering, int[] firstDesc)
        {
            if (firstDesc is null)
            {
                throw new ArgumentNullException(nameof(firstDesc));
            }
            if (firstDesc.Length != _numNodes)
            {
                throw new ArgumentOutOfRangeException(nameof(firstDesc));
            }

            // Initialize firstDesc to -1
            for (int i = 0; i < _numNodes; ++i)
            {
                firstDesc[i] = -1;
            }

            // If a node is not -1, then its first descendant has already been calculated
            // If a node has a first descendant value, then its first descendant value is 
            // guaranteed to be lower than the current one, since we are traversing the nodes
            // in initial ascending order.
            for (int k = 0; k < _numNodes; ++k)
            {
                // postOrdering[k] is the k-th node in the post-ordered tree
                // So i is the node id 
                int i = postOrdering[k];

                // Iterate while first descendant has not been calculated
                int j = i;
                while (firstDesc[j] < 0)
                {
                    firstDesc[j] = k;
                    j = _parents[j];

                    // Reached the root
                    if (j < 0)
                    {
                        break;
                    }
                }
            }
        }

        internal bool IsRoot(int k)
        {
            return _parents[k] < 0;
        }

        /// <summary>
        /// Calculate the reach of column k of the upper triangular part of A, i.e. A[0:k, k].
        /// </summary>
        /// <param name="k">The column to calculate the reach of.</param>
        /// <param name="reach"></param>
        /// <param name="in_reach">Used to store whether a node is in the reach.</param>
        /// <param name="in_reach_threshold">
        /// Used to determine whether a node is already in the reach. If in_reach[i] is at least this 
        /// threshold, then it has been added to the reach. Otherwise, it has not yet been added.
        /// </param>
        internal int Reach<T>(CSCSparseMatrix<T> A, int k, int[] reach, int[] in_reach, int in_reach_threshold) where T : new()
        {
            // Mark node k
            Mark(k, in_reach, in_reach_threshold);

            int[] columnIndices = A.ColumnIndices, 
                rowIndices = A.RowIndices;

            // The top of the stack - the reach is stored at the end of the
            // 'reach' array, in the range [top, dim)
            int top = A.Rows;

            int col_end = columnIndices[k + 1];
            for (int i = columnIndices[k]; i < col_end; ++i)
            {
                // A(r, k) is non-zero
                int r = rowIndices[i];

                // Since A is in sorted order, terminate if 
                // in the lower-triangular half
                if (r > k)
                {
                    break;
                }

                int index = 0;
                while (!IsMarked(r, in_reach, in_reach_threshold))
                {
                    // Insert node r into reach, starting from the front
                    reach[index++] = r;

                    // Mark node r as visited
                    Mark(r, in_reach, in_reach_threshold);

                    // Move up the tree - there is no danger of falling off 
                    // the tree as we will eventually reach node k (the root 
                    // of the k-th subtree), and node k is already marked, 
                    // so the while-loop will terminate.
                    r = _parents[r];
                }

                // Need to place the reach in reverse order, at the end 
                // of the array
                while (index > 0)
                {
                    reach[--top] = reach[--index];
                }
            }

            // Nodes do not need to be unmarked, since the next iteration will 
            // have a new in_reach_threshold
            return top;
        }

        /// <summary>
        /// Mark node j as visited.
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void Mark(int j, int[] in_reach, int in_reach_threshold)
        {
            in_reach[j] = in_reach_threshold;
        }
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private bool IsMarked(int j, int[] in_reach, int in_reach_threshold)
        {
            return in_reach[j] >= in_reach_threshold;
        }
    }
}
