﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LinearNet.Graphs
{
    /// <summary>
    /// Generic graph over a set of vertices
    /// </summary>
    public class Graph<V>
    {
        public bool IsDirected { get; private set; }

        public List<V> Vertices { get; private set; }
        public List<Tuple<V, V>> Edges { get; private set; }

        public Graph(List<V> vertices, List<Tuple<V, V>> edges, bool isDirected)
        {
            if (vertices is null)
            {
                throw new ArgumentNullException(nameof(vertices));
            }
            if (edges is null)
            {
                throw new ArgumentNullException(nameof(edges));
            }

            Vertices = vertices;
            Edges = edges;
            IsDirected = isDirected;
        }
    }
}
