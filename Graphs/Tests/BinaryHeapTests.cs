﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace LinearNet.Graphs.Tests
{
    internal static class BinaryHeapTests
    {
        internal static void RunAll()
        {
            InsertionTest();
        }

        internal static void InsertionTest()
        {
            int n = 100;
            int[] values = new int[n];
            for (int i = 0; i < n; ++i)
            {
                values[i] = i;
            }

            Random rand = new Random();
            values.Shuffle(rand);

            BinaryHeap<int> heap = new BinaryHeap<int>(n);
            for (int i = 0; i < n; ++i)
            {
                heap.Add(values[i]);
            }

            Debug.WriteLine("heap");
            heap.Print();
        }

    }
}
