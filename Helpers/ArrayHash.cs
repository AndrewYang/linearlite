﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LinearNet.Helpers
{
    // Creates a hash of a length-n index of type T, as a fixed-length string
    // Used for storing n-way arrays in a sparse manner (e.g. multivariate polynomials and tensors)
    internal class ArrayHash<T> where T : unmanaged
    {
        internal int Length { get; private set; }

        private readonly int _bufferLength;
        private readonly byte[] _buffer;

        internal ArrayHash(int length, int size)
        {
            Length = length;
            _bufferLength = length * size;
            _buffer = new byte[_bufferLength];
        }

        internal string Hash(T[] array)
        {
            Buffer.BlockCopy(array, 0, _buffer, 0, _bufferLength);
            return Convert.ToBase64String(_buffer);
        }
        internal void Unhash(string hash, T[] array)
        {
            Buffer.BlockCopy(Convert.FromBase64String(hash), 0, array, 0, _bufferLength);
        }

        internal int CompareAsInt32(string hash1, string hash2)
        {
            byte[] buff1 = Convert.FromBase64String(hash1),
                buff2 = Convert.FromBase64String(hash2);

            int size = sizeof(int), i = 0;
            while (i < _bufferLength)
            {
                int comp = BitConverter.ToInt32(buff1, i).CompareTo(BitConverter.ToInt32(buff2, i));
                if (comp != 0)
                {
                    return comp;
                }
                i += size;
            }
            return 0;
        }
    }
}
