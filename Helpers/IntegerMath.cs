﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Helpers
{
    public static class IntegerMath
    {
        /// <summary>
        /// Returns an integer raised to a non-negative integer.
        /// </summary>
        /// <param name="bse">The base integer.</param>
        /// <param name="power">The power integer (must be non-negative).</param>
        /// <returns>The integer power.</returns>
        public static int Pow(int bse, int power)
        {
            if (power < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(power));
            }
            if (power == 0) return 1;
            if (power == 1) return bse;

            int pow = Pow(bse, power / 2);
            if (power % 2 == 1)
            {
                return pow * pow * bse;
            }
            return pow * pow;
        }

        /// <summary>
        /// Returns an integer raised to a non-negative integer.
        /// </summary>
        /// <param name="bse"></param>
        /// <param name="power"></param>
        /// <returns></returns>
        public static long Pow(long bse, long power)
        {
            if (power < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(power));
            }
            if (power == 0) return 1L;
            if (power == 1) return bse;

            long pow = Pow(bse, power / 2L);
            if (power % 2L == 1L)
            {
                return pow * pow * bse;
            }
            return pow * pow;
        }
    }
}
