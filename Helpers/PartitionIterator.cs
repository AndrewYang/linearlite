﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LinearNet.Helpers
{
    /// <summary>
    /// Iterate through all possible partitionings of N > 0 object into K > 0 categories
    /// 'Sizes' holds the sizes of each category
    /// </summary>
    internal class PartitionIterator
    {
        internal int N { get; private set; }
        internal int K { get; private set; }

        internal int[] Sizes { get; private set; }

        internal bool HasStarted { get; private set; }

        internal bool HasFinished { get; private set; }

        internal int Count { get; private set; }

        internal PartitionIterator(int n, int k)
        {
            if (n <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(n));
            }
            if (k <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(k));
            }
            N = n;
            K = k;
            Sizes = new int[k];

            HasStarted = false;
            HasFinished = false;

            Count = 0;
        }

        /// <summary>
        /// Note that the returned array should be read only.
        /// </summary>
        /// <returns></returns>
        internal int[] Next()
        {
            if (!HasStarted)
            {
                Sizes[0] = N;
                Count++;
                return Sizes;
            }

            if (HasFinished)
            {
                return null;
            }

            throw new NotImplementedException();
        }
    }
}
