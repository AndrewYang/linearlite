﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LinearNet.Helpers
{
    internal static class PartitionUtil
    {
        /// <summary>
        /// Split n objects into k categories
        /// </summary>
        internal static List<int[]> GetAllPartitions(int n, int k)
        {
            List<int[]> partitions = new List<int[]>();

            if (k < 0)
            {
                return partitions;
            }
            if (k == 1)
            {
                int[] p = { n };
                partitions.Add(p);
                return partitions;
            }

            // Iterate through the first
            for (int i = 0; i <= n; ++i)
            {
                List<int[]> subpartitions = GetAllPartitions(n - i, k - 1);
                foreach (int[] sp in subpartitions)
                {
                    int[] p = new int[k];
                    Array.Copy(sp, 0, p, 1, k - 1);
                    p[0] = i;
                    partitions.Add(p);
                }
            }

            return partitions;
        }
    }
}
