﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace LinearNet.Helpers
{
    /// <summary>
    /// Permutation iterator over a collection of type T
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class PermutationIterator<T>
    {
        private readonly T[] _items;
        private readonly int[] _c;
        private readonly int _n;

        public PermutationIterator(T[] items)
        {
            _items = items ?? throw new ArgumentNullException(nameof(items));
            _n = items.Length;
            _c = new int[_n];
        }

        public IEnumerable<T[]> GetEnumerator()
        {
            Array.Clear(_c, 0, _n);

            yield return _items;

            int i = 0;
            while (i < _n)
            {
                if (_c[i] < i)
                {
                    if (i % 2 == 0)
                    {
                        T tmp = _items[0];
                        _items[0] = _items[i];
                        _items[i] = tmp;
                    }
                    else
                    {
                        int ci = _c[i];
                        T tmp = _items[ci];
                        _items[ci] = _items[i];
                        _items[i] = tmp;
                    }

                    yield return _items;

                    _c[i]++;
                    i = 0;
                }
                else
                {
                    _c[i] = 0;
                    ++i;
                }
            }
        }
    }
}
