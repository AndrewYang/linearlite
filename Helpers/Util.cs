﻿using LinearNet.Global;
using LinearNet.Helpers;
using LinearNet.Structs;
using LinearNet.Structs.Fields;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Text;

namespace LinearNet
{
    public static class Util
    {
        public static void Print(object o)
        {
            Debug.WriteLine(o.ToString());
        }
        public static void Print(this float[,] matrix)
        {
            string format = "n" + 4;
            var sb = new StringBuilder();

            int rows = matrix.GetLength(0), cols = matrix.GetLength(1), r, c;
            for (r = 0; r < rows; r++)
            {
                for (c = 0; c < cols; c++)
                {
                    sb.Append(matrix[r, c].ToString(format));
                    if (c < cols - 1)
                    {
                        sb.Append('\t');
                    }
                }
                sb.AppendLine();
            }
            Debug.WriteLine(sb.ToString());
        }
        public static void Print(this double[,] matrix)
        {
            var sb = new StringBuilder();

            int rows = matrix.GetLength(0), cols = matrix.GetLength(1), r, c;
            for (r = 0; r < rows; r++)
            {
                for (c = 0; c < cols; c++)
                {
                    sb.Append(matrix[r, c].ToString("n6"));
                    if (c < cols - 1)
                    {
                        sb.Append('\t');
                    }
                }
                sb.AppendLine();
            }
            Debug.WriteLine(sb.ToString());
        }
        public static void Print<T>(this T[,] matrix)
        {
            var sb = new StringBuilder();

            int rows = matrix.GetLength(0), cols = matrix.GetLength(1), r, c;
            for (r = 0; r < rows; r++)
            {
                for (c = 0; c < cols; c++)
                {
                    sb.Append(matrix[r,c]);
                    if (c < cols - 1)
                    {
                        sb.Append('\t');
                    }
                }
                sb.AppendLine();
            }
            Debug.WriteLine(sb.ToString());
        }
        public static void Print<T>(this T[][] matrix, Func<T, string> ToString)
        {
            var sb = new StringBuilder();
            int rows = matrix.Length, r, c;
            for (r = 0; r < rows; r++)
            {
                int cols = matrix[r].Length;
                for (c = 0; c < cols; c++)
                {
                    sb.Append(ToString(matrix[r][c]));
                    if (c < cols - 1)
                    {
                        sb.Append('\t');
                    }
                }
                sb.AppendLine();
            }
            Debug.WriteLine(sb.ToString());
        }
        public static void Print(this double[][] matrix) => Print(matrix, x => x.ToString("n6"));
        public static void Print(this float[][] matrix) => Print(matrix, x => x.ToString("n4"));

        public static void Print(this Matrix<float> matrix)
        {
            matrix.Print(x => x.ToString("n4"));
        }
        public static void Print(this Matrix<double> matrix)
        {
            matrix.Print(x => x.ToString("n6"));
        }
        public static void Print<T>(this Matrix<T> matrix) where T : new()
        {
            matrix.Print(x => x.ToString());
        }
        
        public static void Print(this double[] vector)
        {
            var sb = new StringBuilder();
            foreach (double d in vector)
            {
                sb.Append(d.ToString("n6") + "\t");
            }
            Debug.WriteLine(sb.ToString());
        }
        public static void Print<T>(this T[] vector)
        {
            var sb = new StringBuilder();
            foreach (T d in vector)
            {
                sb.Append(d.ToString() + "\t");
            }
            Debug.WriteLine(sb.ToString());
        }

        public static void Print(this Vector<float> vect)
        {
            vect.Print(x => x.ToString("n4"));
        }
        public static void Print(this Vector<double> vect)
        {
            vect.Print(x => x.ToString("n6"));
        }
        public static void Print<T>(this Vector<T> vect) where T : new()
        {
            vect.Print(x => x.ToString());
        }

        internal static void Print(this Tensor<float> tensor)
        {
            tensor.Print(x => x.ToString("n4"));
        }
        internal static void Print(this Tensor<double> tensor)
        {
            tensor.Print(x => x.ToString("n6"));
        }
        internal static void Print<T>(this Tensor<T> tensor) where T : new()
        {
            tensor.Print(x => x.ToString());
        }
        internal static bool ApproximatelyEquals(this float a, float b, float eps = Precision.FLOAT_PRECISION)
        {
            if (Math.Abs(b) < eps)
            {
                return Math.Abs(a) < eps;
            }
            return Math.Abs(a / b - 1.0f) < eps;
        }
        internal static bool ApproximatelyEquals(this double a, double b, double eps = Precision.DOUBLE_PRECISION)
        {
            if (Math.Abs(b) < eps)
            {
                return Math.Abs(a) < eps;
            }
            return Math.Abs(a / b - 1.0) < eps;
        }
        internal static bool ApproximatelyEquals(this decimal a, decimal b, decimal eps = Precision.DECIMAL_PRECISION)
        {
            if (Math.Abs(b) < eps)
            {
                return Math.Abs(a) < eps;
            }
            return Math.Abs(a / b - 1m) < eps;
        }
        internal static bool ApproximatelyEquals(this BigDecimal a, BigDecimal b, BigDecimal eps)
        {
            if (BigDecimal.Abs(b) < eps)
            {
                return BigDecimal.Abs(a) < eps;
            }
            return BigDecimal.Abs(a / b - BigDecimal.One) < eps;
        }

        /// <summary>
        /// Checking if a number is approximately zero is used frequently in other methods
        /// </summary>
        /// <param name="a"></param>
        /// <param name="eps"></param>
        /// <returns></returns>
        internal static bool approx_zero(this float a, float eps = Precision.FLOAT_PRECISION) => Math.Abs(a) < eps;
        internal static bool approx_zero(this double a, double eps = Precision.DOUBLE_PRECISION) => Math.Abs(a) < eps;
        internal static bool approx_zero(this decimal a, decimal eps = Precision.DECIMAL_PRECISION) => Math.Abs(a) < eps;
        internal static bool approx_zero(this Complex a, double eps = Precision.DOUBLE_PRECISION) => Math.Abs(a.Real) < eps && Math.Abs(a.Imaginary) < eps;


        internal static bool approx_one(this float a, float eps = Precision.FLOAT_PRECISION) => Math.Abs(a - 1.0F) < eps;
        internal static bool approx_one(this double a, double eps = Precision.DOUBLE_PRECISION) => Math.Abs(a - 1.0) < eps;
        internal static bool approx_one(this decimal a, decimal eps = Precision.DECIMAL_PRECISION) => Math.Abs(a - 1.0M) < eps;


        /// <summary>
        /// Numerically stable implementation of c = sqrt(a^2 + b^2), the hypotenuse function
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        internal static double Hypot(double a, double b)
        {
            if (double.IsInfinity(a) || double.IsInfinity(b))
            {
                return double.PositiveInfinity;
            }
            if (double.IsNaN(a) || double.IsNaN(b))
            {
                return double.NaN;
            }

            if (a < 0.0) a = -a;
            if (b < 0.0) b = -b;

            if (a > b)
            {
                if (a == 0) return 0;
                double t = b / a;
                return a * Math.Sqrt(1.0 + t * t);
            }
            else
            {
                if (b == 0) return 0;
                double t = a / b;
                return b * Math.Sqrt(1.0 + t * t);
            }
        }
        internal static float Hypot(float a, float b)
        {
            if (float.IsInfinity(a) || float.IsInfinity(b))
            {
                return float.PositiveInfinity;
            }
            if (float.IsNaN(a) || float.IsNaN(b))
            {
                return float.NaN;
            }

            if (a < 0.0f) a = -a;
            if (b < 0.0f) b = -b;

            double min = a, max = b;
            if (a > b)
            {
                min = b;
                max = a;
            }

            if (max == 0.0f) return 0.0f;

            double t = min / max;
            return (float)(max * Math.Sqrt(1.0 + t * t));
        }
        internal static decimal Hypot(decimal a, decimal b)
        {
            if (a < 0.0m) a = -a;
            if (b < 0.0m) b = -b;

            decimal min = a, max = b;
            if (a > b)
            {
                min = b;
                max = a;
            }

            if (max == 0.0m) return 0.0m;

            decimal t = min / max;
            return max * DecimalMath.Sqrt(1.0m + t * t);
        }
        internal static BigDecimal Hypot(BigDecimal a, BigDecimal b)
        {
            if (a.IsNegative) a = -a;
            if (b.IsNegative) b = -b;

            if (a > b)
            {
                if (a.IsZero) return BigDecimal.Zero;
                BigDecimal t = b / a;
                return a * BigDecimal.Sqrt(BigDecimal.One + t * t);
            }
            else
            {
                if (b.IsZero) return BigDecimal.Zero;
                BigDecimal t = a / b;
                return b * BigDecimal.Sqrt(BigDecimal.One + t * t);
            }
        }

        /// <summary>
        /// Results are arrays of length 2
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="results"></param>
        internal static void Givens(float a, float b, float[] results)
        {
            if (Math.Abs(a) > Math.Abs(b))
            {
                if (a == 0)
                {
                    results[0] = 0.0f;
                    results[1] = 0.0f;
                }
                else
                {
                    float t = b / a;
                    float c = (float)(1.0 / Math.Sqrt(1.0 + t * t));
                    results[0] = c;
                    results[1] = c * t; // s
                }
            }
            else
            {
                if (b == 0)
                {
                    results[0] = 0.0f;
                    results[1] = 0.0f;
                }
                else
                {
                    float t = a / b;
                    float s = (float)(1.0 / Math.Sqrt(1.0 + t * t));
                    results[0] = s * t; // c
                    results[1] = s;
                }
            }
        }
        internal static void Givens(double a, double b, double[] results)
        {
            if (Math.Abs(a) > Math.Abs(b))
            {
                if (a == 0)
                {
                    results[0] = 0.0;
                    results[1] = 0.0;
                }
                else
                {
                    double t = b / a;
                    double c = 1.0 / Math.Sqrt(1.0 + t * t);
                    results[0] = c;
                    results[1] = c * t; // s
                }
            }
            else
            {
                if (b == 0)
                {
                    results[0] = 0.0;
                    results[1] = 0.0;
                }
                else
                {
                    double t = a / b;
                    double s = 1.0 / Math.Sqrt(1.0 + t * t);
                    results[0] = s * t; // c
                    results[1] = s;
                }
            }
        }
        internal static void Givens(decimal a, decimal b, decimal[] results)
        {
            if (Math.Abs(a) > Math.Abs(b))
            {
                if (a == 0)
                {
                    results[0] = 0.0m;
                    results[1] = 0.0m;
                }
                else
                {
                    decimal t = b / a;
                    decimal c = 1.0m / DecimalMath.Sqrt(1.0m + t * t);
                    results[0] = c;
                    results[1] = c * t; // s
                }
            }
            else
            {
                if (b == 0)
                {
                    results[0] = 0.0m;
                    results[1] = 0.0m;
                }
                else
                {
                    decimal t = a / b;
                    decimal s = 1.0m / DecimalMath.Sqrt(1.0m + t * t);
                    results[0] = s * t; // c
                    results[1] = s;
                }
            }
        }
        internal static void Givens(Complex a, Complex b, Complex[] results)
        {
            double mod_a = a.Modulus(), mod_b = b.Modulus();
            if (mod_a > mod_b)
            {
                if (mod_a == 0)
                {
                    results[0] = Complex.Zero;
                    results[1] = Complex.Zero;
                }
                else
                {
                    double t = mod_b / mod_a, c = 1.0 / Math.Sqrt(1.0 + t * t);
                    results[0] = new Complex(c, 0);
                    results[1] = Complex.FromPolar(c * t, a.Argument() - b.Argument());
                }
            }
            else
            {
                if (mod_b == 0)
                {
                    results[0] = Complex.Zero;
                    results[1] = Complex.Zero;
                }
                else
                {
                    double t = mod_a / mod_b, s = 1.0 / Math.Sqrt(1.0 + t * t);
                    results[0] = new Complex(s * t, 0); // c
                    results[1] = Complex.FromPolar(s, a.Argument() - b.Argument());
                }
            }
        }

        internal static bool IsPowerOf2(int n)
        {
            if (n <= 0) return false;
            return (int)(Math.Ceiling((Math.Log(n) / Math.Log(2)))) == (int)(Math.Floor(((Math.Log(n) / Math.Log(2)))));
        }

        internal static decimal Sqrt(decimal x, decimal epsilon = 0.0m)
        {
            if (x < 0) throw new OverflowException("Cannot calculate square root from a negative number.");

            // Initialise estimate to decimal equivalent of double Sqrt()
            decimal current = (decimal)Math.Sqrt((double)x), previous;
            do
            {
                previous = current;
                if (previous == 0.0M) return 0;
                current = (previous + x / previous) / 2;
            }
            while (Math.Abs(previous - current) > epsilon);
            return current;
        }

        internal static double Max(params double[] values)
        {
            double max = values[0];
            for (int i = 1; i < values.Length; ++i)
            {
                double d = values[i];
                if (d > max)
                {
                    max = d;
                }
            }
            return max;
        }
        internal static int Max(params int[] values)
        {
            int max = values[0];
            for (int i = 1; i < values.Length; ++i)
            {
                int d = values[i];
                if (d > max)
                {
                    max = d;
                }
            }
            return max;
        }
        internal static double Min(params double[] values)
        {
            double min = values[0];
            for (int i = 1; i < values.Length; ++i)
            {
                double d = values[i];
                if (d < min)
                {
                    min = d;
                }
            }
            return min;
        }
        internal static T Min<T>(params T[] values) where T : IComparable<T>
        {
            T min = values[0];
            for (int i = 1; i < values.Length; ++i)
            {
                T d = values[i];
                if (d.CompareTo(min) < 0)
                {
                    min = d;
                }
            }
            return min;
        }
        internal static T Max<T>(params T[] values) where T : IComparable<T>
        {
            if (values == null)
            {
                throw new ArgumentNullException(nameof(values));
            }

            T max = values[0];
            for (int i = 1; i < values.Length; ++i)
            {
                T d = values[i];
                if (d.CompareTo(max) > 0)
                {
                    max = d;
                }
            }
            return max;
        }

        internal static bool IsInt32(this double x, double eps = Precision.DOUBLE_PRECISION)
        {
            if (x > int.MaxValue - 0.5 || x < int.MinValue)
            {
                return false;
            }
            int closestInteger = (int)(x + 0.5);
            return Math.Abs(closestInteger - x) < eps;
        }
        internal static int ToInt32(this double x)
        {
            return (int)(x + 0.5);
        }

        internal static void Swap<T>(ref T a, ref T b)
        {
            T tmp = a;
            a = b;
            b = tmp;
        }

        internal static double Log2(double x)
        {
            if (x <= 0)
            {
                return double.NegativeInfinity;
            }
            return Math.Log(x) / Constants.Ln2;
        }

        internal static int GCD(int a, int b)
        {
            // Enforce a <= b
            if (a > b)
            {
                return GCD(b, a);
            }
            if (a == 0)
            {
                return b;
            }
            return GCD(b % a, a);
        }


        #region Collections methods 

        /// <summary>
        /// Shuffles the values of the array in-place.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <param name="random"></param>
        internal static void Shuffle<T>(this IList<T> list, Random random)
        {
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = random.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }

        /// <summary>
        /// Shifts the values of the array in-place
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="array"></param>
        /// <param name="startIndex"></param>
        /// <param name="length"></param>
        /// <param name="shift"></param>
        internal static void Shift<T>(this T[] array, int startIndex, int length, int shift)
        {
            // Shift to the right
            if (shift > 0)
            {
                for (int i = startIndex + length - 1; i >= startIndex; --i)
                {
                    array[i + shift] = array[i];
                }
            }

            // Shift to the left
            else if (shift < 0)
            {
                int end = startIndex + length;
                for (int i = startIndex; i < end; ++i)
                {
                    array[i + shift] = array[i];
                }
            }
        }

        /// <summary>
        /// Increments [startIndex, endIndex) by increment
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="array"></param>
        /// <param name="startIndex"></param>
        /// <param name="length"></param>
        /// <param name="increment"></param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        internal static void Increment(this int[] array, int startIndex, int endIndex, int increment)
        {
            for (int i = startIndex; i < endIndex; ++i)
            {
                array[i] += increment;
            }
        }
        #endregion


    }
}
