﻿using System;
using System.Collections.Generic;
using System.IO;

namespace LinearNet.IO
{
    internal class MatrixReader<T> where T : new()
    {
        private object _thisLock = new object();

        internal bool FoundMetaInfo { get; private set; }

        /// <summary>
        /// Load a matrix stored in a file in triplet form (see COOSparseMatrix).
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="path"></param>
        /// <param name=""></param>
        /// <returns></returns>
        internal IEnumerable<IMatrixData> LoadTriplet(string path, Func<string, T> parse, char delim = ',', bool raiseNonCriticalExceptions = true, int indexing = 0)
        {
            if (string.IsNullOrEmpty(path))
            {
                throw new ArgumentNullException(nameof(path));
            }
            if (parse is null)
            {
                throw new ArgumentNullException(nameof(parse));
            }
            if (!File.Exists(path))
            {
                throw new ArgumentException(nameof(path), "File does not exist.");
            }

            lock (_thisLock)
            {
                // First row is [row] [column] [nnz] (the meta info)
                int rows = -1,
                    columns = -1,
                    k = 0,
                    previousEntryRow = -1,
                    previousEntryColumn = -1;

                FoundMetaInfo = false;

                using var fs = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                using var sr = new StreamReader(fs);

                while (sr.Peek() > -1)
                {
                    string line = sr.ReadLine();
                    if (string.IsNullOrEmpty(line))
                    {
                        if (raiseNonCriticalExceptions)
                        {
                            throw new FormatException($"Null or empty line.");
                        }
                        continue;
                    }

                    // Ignore comments
                    char first = line[0];
                    if (first == '%' || first == '#' || line.StartsWith("//")) continue;

                    string[] split = line.Split(delim);
                    if (split.Length < 3)
                    {
                        if (raiseNonCriticalExceptions)
                        {
                            throw new FormatException($"Invalid number of entries in the line: {line}.");
                        }
                        continue; // fail silently
                    }

                    // Parse meta info
                    if (!FoundMetaInfo)
                    {
                        if (!int.TryParse(split[0], out rows) || !int.TryParse(split[1], out columns) || !int.TryParse(split[2], out int nnz))
                        {
                            throw new FormatException("Invalid meta info: expected 3 integers.");
                        }
                        FoundMetaInfo = true;

                        yield return new MatrixHeader(rows, columns, nnz);
                    }
                    else
                    {
                        int r, c = 0;
                        if (!int.TryParse(split[0], out r) || !int.TryParse(split[1], out c))
                        {
                            if (raiseNonCriticalExceptions)
                            {
                                throw new FormatException($"Row or column index is not an integer in the line '{line}'.");
                            }
                        }

                        r -= indexing;
                        c -= indexing;

                        if (r < 0 || r >= rows)
                        {
                            throw new ArgumentOutOfRangeException("Row index out of range.");
                        }
                        if (c < 0 || c >= columns)
                        {
                            throw new ArgumentOutOfRangeException("Column index out of range.");
                        }

                        previousEntryRow = r;
                        previousEntryColumn = c;

                        yield return new MatrixElement<T>(r, c, parse(split[2]));
                    }
                }
            }
        }
    }
    interface IMatrixData
    {

    }
    internal class MatrixHeader : IMatrixData
    {
        internal int Rows { get; private set; }
        internal int Columns { get; private set; }
        internal int NNZ { get; private set; }

        internal MatrixHeader(int rows, int columns, int nnz)
        {
            Rows = rows;
            Columns = columns;
            NNZ = nnz;
        }
        internal MatrixHeader(int rows, int columns)
        {
            Rows = rows;
            Columns = columns;
            NNZ = -1;
        }
    }

    /// <summary>
    /// Potentially can merge with MatrixEntry?
    /// </summary>
    /// <typeparam name="T"></typeparam>
    internal class MatrixElement<T> : IMatrixData where T : new()
    {
        internal int Row { get; private set; }
        internal int Column { get; private set; }
        internal T Value { get; private set; }

        internal MatrixElement(int row, int column, T value)
        {
            Row = row;
            Column = column;
            Value = value;
        }
    }
}
