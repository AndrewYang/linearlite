﻿using LinearNet.Structs;

namespace LinearNet.Matrices.Band
{
    public static class BandMatrixBooleanExtensions
    {
        public static bool ExactlyEquals(this BandMatrix<int> A, BandMatrix<int> B) => A.ApproximatelyEquals(B, (a, b) => a == b);
        public static bool ExactlyEquals(this BandMatrix<long> A, BandMatrix<long> B) => A.ApproximatelyEquals(B, (a, b) => a == b);
        public static bool ApproximatelyEquals(this BandMatrix<float> A, BandMatrix<float> B, float precision = Precision.FLOAT_PRECISION) => A.ApproximatelyEquals(B, (a, b) => Util.ApproximatelyEquals(a, b, precision));
        public static bool ApproximatelyEquals(this BandMatrix<double> A, BandMatrix<double> B, double precision = Precision.DOUBLE_PRECISION) => A.ApproximatelyEquals(B, (a, b) => Util.ApproximatelyEquals(a, b, precision));
        public static bool ApproximatelyEquals(this BandMatrix<decimal> A, BandMatrix<decimal> B, decimal precision = Precision.DECIMAL_PRECISION) => A.ApproximatelyEquals(B, (a, b) => Util.ApproximatelyEquals(a, b, precision));
        public static bool ApproximatelyEquals<T>(this BandMatrix<T> A, BandMatrix<T> B, double precision = Precision.DOUBLE_PRECISION) where T : IAlgebra<T>, new() => A.ApproximatelyEquals(B, (a, b) => a.ApproximatelyEquals(b, precision));
    }
}
