﻿using LinearNet.Providers;
using LinearNet.Providers.LinearAlgebra;
using LinearNet.Structs;
using System;

namespace LinearNet.Matrices.Band
{
    /// <summary>
    /// 
    /// </summary>
    public static class BandMatrixOperationsExtensions
    {
        /// <summary>
        /// Returns the sum of two band matrices as another band matrix, without changing the original matrices.
        /// </summary>
        /// <param name="A">The first matrix.</param>
        /// <param name="B">The second matrix.</param>
        /// <returns>The matrix sum.</returns>
        /// <cat>la</cat>
        public static BandMatrix<int> Add(this BandMatrix<int> A, BandMatrix<int> B) => A.Add(B, (u, v) => u.Add(v));
        public static BandMatrix<long> Add(this BandMatrix<long> A, BandMatrix<long> B) => A.Add(B, (u, v) => u.Add(v));
        public static BandMatrix<float> Add(this BandMatrix<float> A, BandMatrix<float> B) => A.Add(B, (u, v) => u.Add(v));
        public static BandMatrix<double> Add(this BandMatrix<double> A, BandMatrix<double> B) => A.Add(B, (u, v) => u.Add(v));
        public static BandMatrix<decimal> Add(this BandMatrix<decimal> A, BandMatrix<decimal> B) => A.Add(B, (u, v) => u.Add(v));
        public static BandMatrix<T> Add<T>(this BandMatrix<T> A, BandMatrix<T> B) where T : IAdditiveGroup<T>, new() => A.Add(B, (u, v) => u.Add(v));

        /// <summary>
        /// Returns the first band matrix subtract the second band matrix, without changing the original matrices.
        /// </summary>
        /// <param name="A">The first matrix.</param>
        /// <param name="B">The second matrix.</param>
        /// <returns>The matrix difference.</returns>
        /// <cat>la</cat>
        public static BandMatrix<int> Subtract(this BandMatrix<int> A, BandMatrix<int> B) => A.Subtract(B, (u, v) => u.Subtract(v), u => u.Negate());
        public static BandMatrix<long> Subtract(this BandMatrix<long> A, BandMatrix<long> B) => A.Subtract(B, (u, v) => u.Subtract(v), u => u.Negate());
        public static BandMatrix<float> Subtract(this BandMatrix<float> A, BandMatrix<float> B) => A.Subtract(B, (u, v) => u.Subtract(v), u => u.Negate());
        public static BandMatrix<double> Subtract(this BandMatrix<double> A, BandMatrix<double> B) => A.Subtract(B, (u, v) => u.Subtract(v), u => u.Negate());
        public static BandMatrix<decimal> Subtract(this BandMatrix<decimal> A, BandMatrix<decimal> B) => A.Subtract(B, (u, v) => u.Subtract(v), u => u.Negate());
        public static BandMatrix<T> Subtract<T>(this BandMatrix<T> A, BandMatrix<T> B) where T : IAdditiveGroup<T>, new() => A.Subtract(B, (u, v) => u.Subtract(v), u => u.Negate());

        /// <summary>
        /// Returns the product between two band matrices as another band matrix, without changing either of the original matrices.
        /// </summary>
        /// <param name="A">The first matrix.</param>
        /// <param name="B">The second matrix.</param>
        /// <returns>The matrix product.</returns>
        /// <cat>la</cat>
        public static BandMatrix<int> Multiply(this BandMatrix<int> A, BandMatrix<int> B) => A.Multiply(B, new NativeInt32Provider());
        public static BandMatrix<long> Multiply(this BandMatrix<long> A, BandMatrix<long> B) => A.Multiply(B, new NativeInt64Provider());
        public static BandMatrix<float> Multiply(this BandMatrix<float> A, BandMatrix<float> B) => A.Multiply(B, new NativeFloatProvider());
        public static BandMatrix<double> Multiply(this BandMatrix<double> A, BandMatrix<double> B) => A.Multiply(B, new NativeDoubleProvider());
        public static BandMatrix<decimal> Multiply(this BandMatrix<decimal> A, BandMatrix<decimal> B) => A.Multiply(B, new NativeDecimalProvider());
        public static BandMatrix<T> Multiply<T>(this BandMatrix<T> A, BandMatrix<T> B) where T : IRing<T>, new() => A.Multiply(B, new NativeRingProvider<T>());

        /// <summary>
        /// Returns the product between a band matrix and a dense vector, as a dense vector. Neither the matrix nor the original vector are changed.
        /// </summary>
        /// <param name="A">A band matrix.</param>
        /// <param name="v">A vector.</param>
        /// <returns>The matrix-vector product.</returns>
        /// <cat>la</cat>
        public static DenseVector<int> Multiply(this BandMatrix<int> A, DenseVector<int> v) => A.Multiply(v, new NativeInt32Provider());
        public static DenseVector<long> Multiply(this BandMatrix<long> A, DenseVector<long> v) => A.Multiply(v, new NativeInt64Provider());
        public static DenseVector<float> Multiply(this BandMatrix<float> A, DenseVector<float> v) => A.Multiply(v, new NativeFloatProvider());
        public static DenseVector<double> Multiply(this BandMatrix<double> A, DenseVector<double> v) => A.Multiply(v, new NativeDoubleProvider());
        public static DenseVector<decimal> Multiply(this BandMatrix<decimal> A, DenseVector<decimal> v) => A.Multiply(v, new NativeDecimalProvider());
        public static DenseVector<T> Multiply<T>(this BandMatrix<T> A, DenseVector<T> v) where T : IRing<T>, new() => A.Multiply(v, new NativeRingProvider<T>());

        /// <summary>
        /// Returns the product between a band matrix and a vector, as an array. Neither the matrix nor the original array are changed.
        /// </summary>
        /// <param name="A">A band matrix.</param>
        /// <param name="v">A vector.</param>
        /// <returns>The matrix-vector product.</returns>
        /// <cat>la</cat>
        public static int[] Multiply(this BandMatrix<int> A, int[] v) => A.Multiply(v, new NativeInt32Provider());
        public static long[] Multiply(this BandMatrix<long> A, long[] v) => A.Multiply(v, new NativeInt64Provider());
        public static float[] Multiply(this BandMatrix<float> A, float[] v) => A.Multiply(v, new NativeFloatProvider());
        public static double[] Multiply(this BandMatrix<double> A, double[] v) => A.Multiply(v, new NativeDoubleProvider());
        public static decimal[] Multiply(this BandMatrix<decimal> A, decimal[] v) => A.Multiply(v, new NativeDecimalProvider());
        public static T[] Multiply<T>(this BandMatrix<T> A, T[] v) where T : IRing<T>, new() => A.Multiply(v, new NativeRingProvider<T>());

        /// <summary>
        /// Multiply a band matrix with a scalar, without changing the original matrix.
        /// </summary>
        /// <param name="A">A band matrix.</param>
        /// <param name="scalar">A scalar.</param>
        /// <returns>The matrix-scalar product.</returns>
        /// <cat>la</cat>
        public static BandMatrix<int> Multiply(this BandMatrix<int> A, int scalar) => A.Multiply(scalar, new NativeInt32Provider());
        public static BandMatrix<long> Multiply(this BandMatrix<long> A, long scalar) => A.Multiply(scalar, new NativeInt64Provider());
        public static BandMatrix<float> Multiply(this BandMatrix<float> A, float scalar) => A.Multiply(scalar, new NativeFloatProvider());
        public static BandMatrix<double> Multiply(this BandMatrix<double> A, double scalar) => A.Multiply(scalar, new NativeDoubleProvider());
        public static BandMatrix<decimal> Multiply(this BandMatrix<decimal> A, decimal scalar) => A.Multiply(scalar, new NativeDecimalProvider());
        public static BandMatrix<T> Multiply<T>(this BandMatrix<T> A, T scalar) where T : IRing<T>, new() => A.Multiply(scalar, new NativeRingProvider<T>());

        public static BandMatrix<int> MultiplyParallel(this BandMatrix<int> A, BandMatrix<int> B) => A.MultiplyParallel(B, new NativeInt32Provider());
        public static BandMatrix<long> MultiplyParallel(this BandMatrix<long> A, BandMatrix<long> B) => A.MultiplyParallel(B, new NativeInt64Provider());
        public static BandMatrix<float> MultiplyParallel(this BandMatrix<float> A, BandMatrix<float> B) => A.MultiplyParallel(B, new NativeFloatProvider());
        public static BandMatrix<double> MultiplyParallel(this BandMatrix<double> A, BandMatrix<double> B) => A.MultiplyParallel(B, new NativeDoubleProvider());
        public static BandMatrix<decimal> MultiplyParallel(this BandMatrix<decimal> A, BandMatrix<decimal> B) => A.MultiplyParallel(B, new NativeDecimalProvider());
        public static BandMatrix<T> MultiplyParallel<T>(this BandMatrix<T> A, BandMatrix<T> B) where T : IRing<T>, new() => A.MultiplyParallel(B, new NativeRingProvider<T>());

        public static BandMatrix<T> Elementwise<F, T>(this BandMatrix<F> A, Func<F, T> Operation) where F : new() where T : new()
        {
            int upperBandWidth = A.UpperBandwidth, lowerBandwidth = A.LowerBandwidth, i;
            T[][] upper = new T[upperBandWidth][], lower = new T[lowerBandwidth][];
            for (i = 0; i < upperBandWidth; ++i) 
            {
                upper[i] = A.UpperBands[i].Elementwise(Operation);
            }
            for (i = 0; i < lowerBandwidth; ++i)
            {
                lower[i] = A.LowerBands[i].Elementwise(Operation);
            }
            T[] diagonal = A.Diagonal.Elementwise(Operation);
            return new BandMatrix<T>(A.Rows, A.Columns, upper, diagonal, lower);
        }
        public static BandMatrix<U> Elementwise<S, T, U>(this BandMatrix<S> A, BandMatrix<T> B, Func<S, T, U> Function) where S : new() where T : new() where U : new()
        {
            if (A == null)
            {
                throw new ArgumentNullException(nameof(A));
            }
            if (B == null)
            {
                throw new ArgumentNullException(nameof(B));
            }
            if (A.Rows != B.Rows || 
                A.Columns != B.Columns || 
                A.LowerBandwidth != B.LowerBandwidth || 
                A.UpperBandwidth != B.UpperBandwidth)
            {
                throw new InvalidOperationException();
            }

            BandMatrix<U> result = new BandMatrix<U>(A.Rows, A.Columns, A.UpperBandwidth, A.LowerBandwidth);

            int width = B.LowerBandwidth, i;
            for (i = 0; i < width; ++i)
            {
                result.LowerBands[i] = A.LowerBands[i].Elementwise(B.LowerBands[i], Function);
            }

            Array.Copy(A.Diagonal.Elementwise(B.Diagonal, Function), 0, result.Diagonal, 0, result.Diagonal.Length);

            width = B.UpperBandwidth;
            for (i = 0; i < width; ++i)
            {
                result.UpperBands[i] = A.UpperBands[i].Elementwise(B.UpperBands[i], Function);
            }

            return result;
        }

        /// <summary>
        /// Returns the trace of a band matrix.
        /// </summary>
        /// <param name="A">A band matrix.</param>
        /// <returns>The matrix trace.</returns>
        /// <cat>la</cat>
        public static int Trace(this BandMatrix<int> A) => A.Trace(new Int32Provider());
        public static long Trace(this BandMatrix<long> A) => A.Trace(new Int64Provider());
        public static float Trace(this BandMatrix<float> A) => A.Trace(new FloatProvider());
        public static double Trace(this BandMatrix<double> A) => A.Trace(new DoubleProvider());
        public static decimal Trace(this BandMatrix<decimal> A) => A.Trace(new DecimalProvider());
        public static T Trace<T>(this BandMatrix<T> A) where T : IAdditiveGroup<T>, new() => A.Trace(new AdditiveGroupProvider<T>());
    }
}
