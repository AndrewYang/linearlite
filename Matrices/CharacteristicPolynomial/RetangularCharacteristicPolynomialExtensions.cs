﻿using LinearNet.Providers;
using LinearNet.Structs;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Matrices
{
    public static class RectangularCharacteristicPolynomialExtensions
    {
        /// <summary>
        /// <para>
        /// Calculates the characteristic polynomial of a square matrix. 
        /// </para>
        /// <para>
        /// By default, this method uses Faddeev-LeVerrier's algorithm.
        /// </para>
        /// <!--inputs-->
        /// </summary>
        /// <name>CharacteristicPolynomial</name>
        /// <proto>Polynomial<T> CharacteristicPolynomial(this IMatrix<T> A, CharacteristicPolynomialAlgorithm algorithm)</proto>
        /// <cat>la</cat>
        /// <param name="A">
        /// The square matrix $A$ for which the characteristic polynomial is to be calculated.<br/>
        /// Throws <txt>InvalidOperationException</txt> if $A$ is not square.</param>
        /// <param name="algorithm">
        /// <b>Optional</b>, defaults to <txt>FADDEEV_LEVERRIER</txt>.<br/>
        /// The algorithm used for calculating the characteristic polynomial. Currently, only the Faddeev–LeVerrier algorithm is 
        /// implemented.
        /// </param>
        /// <returns></returns>
        public static DensePolynomial<float> CharacteristicPolynomial(this float[,] A, CharacteristicPolynomialAlgorithm algorithm = CharacteristicPolynomialAlgorithm.FADDEEV_LEVERRIER)
        {
            if (algorithm == CharacteristicPolynomialAlgorithm.FADDEEV_LEVERRIER)
            {
                return A.FaddeevLeVerrierAlgorithm();
            }
            throw new NotImplementedException();
        }
        public static DensePolynomial<double> CharacteristicPolynomial(this double[,] A, CharacteristicPolynomialAlgorithm algorithm = CharacteristicPolynomialAlgorithm.FADDEEV_LEVERRIER)
        {
            if (algorithm == CharacteristicPolynomialAlgorithm.FADDEEV_LEVERRIER)
            {
                return A.FaddeevLeVerrierAlgorithm();
            }
            throw new NotImplementedException();
        }
        public static DensePolynomial<Rational> CharacteristicPolynomial(this int[,] A, CharacteristicPolynomialAlgorithm algorithm = CharacteristicPolynomialAlgorithm.FADDEEV_LEVERRIER)
        {
            if (algorithm == CharacteristicPolynomialAlgorithm.FADDEEV_LEVERRIER)
            {
                return A.FaddeevLeVerrierAlgorithm();
            }
            throw new NotImplementedException();
        }
        public static DensePolynomial<Complex> CharacteristicPolynomial(this Complex[,] A, CharacteristicPolynomialAlgorithm algorithm = CharacteristicPolynomialAlgorithm.FADDEEV_LEVERRIER)
        {
            if (algorithm == CharacteristicPolynomialAlgorithm.FADDEEV_LEVERRIER)
            {
                return A.FaddeevLeVerrierAlgorithm();
            }
            throw new NotImplementedException();
        }

        /// <summary>
        /// Calculates the characteristic polynomial of matrix A using the Faddeev–LeVerrier algorithm
        /// 
        /// Note this method has only been shallow-optimised (in particular the use of Func's are very slow compared 
        /// to unmanaged computations. As the complexity is O(n^4) (!!) we defer 
        /// production-quality characteristic polynomial derivation to other methods. 
        /// </summary>
        /// <param name="A"></param>
        /// <returns>The characteristic polynomial (with real coefficients) of A</returns>
        internal static DensePolynomial<float> FaddeevLeVerrierAlgorithm(this float[,] A)
        {
            return FaddeevLeVerrierAlgorithm(A, new FloatProvider(), (M, N, result) => M.multiply_unsafe(N, result));
        }
        internal static DensePolynomial<double> FaddeevLeVerrierAlgorithm(this double[,] A)
        {
            return FaddeevLeVerrierAlgorithm(A, new DoubleProvider(), (M, N, result) => M.multiply_unsafe(N, result));
        }
        internal static DensePolynomial<decimal> FaddeevLeVerrierAlgorithm(this decimal[,] A)
        {
            return FaddeevLeVerrierAlgorithm(A, new DecimalProvider(), (M, N, result) => M.multiply_unsafe(N, result));
        }
        internal static DensePolynomial<Rational> FaddeevLeVerrierAlgorithm(this int[,] A)
        {
            Rational[,] B = A.Convert(i => new Rational(i));
            Action<Rational[][], Rational[][], Rational[][]> multiply = (M, N, result) =>
            {
                M.multiply_unsafe(N, result);

                int m = result.Length, n = result[0].Length, i, j;
                Rational[] row;
                for (i = 0; i < m; ++i)
                {
                    row = result[i];
                    for (j = 0; j < n; ++j)
                    {
                        row[j].Simplify();
                    }
                }
            };
            return FaddeevLeVerrierAlgorithm(B, new FieldProvider<Rational>(), multiply);
        }
        internal static DensePolynomial<Complex> FaddeevLeVerrierAlgorithm(this Complex[,] A)
        {
            return FaddeevLeVerrierAlgorithm(A, new ComplexProvider(), (M, N, result) => M.multiply_unsafe(N, result));
        }
        internal static DensePolynomial<T> FaddeevLeVerrierAlgorithm<T>(this T[,] A) where T : IField<T>, new()
        {
            return FaddeevLeVerrierAlgorithm(A, new FieldProvider<T>(), (M, N, result) => M.multiply_unsafe(N, result));
        }
        private static DensePolynomial<T> FaddeevLeVerrierAlgorithm<T>(this T[,] A, IProvider<T> blas, Action<T[][], T[][], T[][]> Multiply) where T : new()
        {
            MatrixChecks.CheckNotNull(A);
            MatrixChecks.CheckIsSquare(A);

            int n = A.GetLength(0), i, j, k;
            T[] c = new T[n + 1]; // store the coefficients
            c[0] = blas.One;

            T[][] _A = A.ToJagged(),
                        M = MatrixInternalExtensions.JMatrix<T>(n, n),
                        N = MatrixInternalExtensions.JMatrix<T>(n, n);

            T[] M_j, N_j;

            // Initialise N with zeroes
            for (i = 0; i < n; ++i)
            {
                N_j = N[i];
                for (j = 0; j < n; ++j)
                {
                    N_j[j] = blas.Zero;
                }
            }

            T K = blas.Zero;
            for (i = 1; i <= n; ++i)
            {
                // M <- N + c[i - 1] * I
                T c_i = c[i - 1];
                for (j = 0; j < n; ++j)
                {
                    M_j = M[j];
                    N_j = N[j];
                    for (k = 0; k < n; ++k)
                    {
                        M_j[k] = N_j[k];
                    }
                    M_j[j] = blas.Add(M_j[j], c_i);
                }

                // N <- AM
                Multiply(_A, M, N);

                // Calculate tr(N)
                T trace = blas.Zero;
                for (j = 0; j < n; ++j)
                {
                    trace = blas.Add(trace, N[j][j]);
                }

                // c[i] := -1/k * tr(AM)
                K = blas.Subtract(K, blas.One);
                c[i] = blas.Divide(trace, K);
            }

            return new DensePolynomial<T>(c);
        }
    }
}
