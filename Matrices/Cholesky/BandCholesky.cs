﻿using LinearNet.Global;
using LinearNet.Providers;
using LinearNet.Structs;
using System;

namespace LinearNet.Matrices.Sparse.Decompositions
{
    internal class BandCholesky<T> where T : new()
    {
        /// <summary>
        /// A is not checked for symmetry, rather only the upper triangular half of the matrix is 
        /// used (and the lower triangular part is assumed to be the transpose of this upper triangular 
        /// half).
        /// </summary>
        /// <param name="A"></param>
        /// <returns></returns>
        internal BandMatrix<T> CholUp(BandMatrix<T> A, IProvider<T> provider)
        {
            if (A is null)
            {
                throw new ArgumentNullException(nameof(A));
            }
            if (!A.IsSquare)
            {
                throw new ArgumentOutOfRangeException(nameof(A), ExceptionMessages.MatrixNotSquare);
            }

            int dim = A.Columns, bw = A.UpperBandwidth;

            // The non-zero pattern of L is simply the non-zero pattern of the transpose
            // of the upper triangular portion of A (since we are assuming A to be symmetric).
            BandMatrix<T> L = new BandMatrix<T>(dim, dim, 0, bw);
            L[0, 0] = provider.Sqrt(A[0, 0]);

            T[] workspace = new T[dim];
            for (int c = 1; c < dim; ++c)
            {
                // Solve L * r = b where L is the lower triangular factor so far (top c - 1 rows)
                // and r is the c-th row.
                SolveLowerTriangular(L, A, c, workspace, provider);

                // Append the L_22 term L_22 = sqrt(A_22 - L_12^T * L_12)
                T dot = provider.Zero;
                for (int i = Math.Max(0, c - bw); i < c; ++i)
                {
                    T v = L[c, i];
                    dot = provider.Add(dot, provider.Multiply(v, v));
                }
                T sq = provider.Subtract(A[c, c], dot);
                if (provider.IsRealAndNegative(sq))
                {
                    throw new InvalidOperationException("Matrix is not positive-definite.");
                }
                L[c, c] = provider.Sqrt(sq);
            }
            return L;
        }

        /// <summary>
        /// Solve the lower triangular subsystem L(0:k, 0:k) * x(0:k) = b(0:k, k).
        /// L is defined implicitly using the tuple (indices, values), in row-major order
        /// k > 0. This is used to completely solve the k-th row of the Cholesky factor
        /// 
        /// The solution is appended onto values (as a contingous row). Also, the (k + 1)-th entry of 
        /// indices is updated.
        /// </summary>
        private void SolveLowerTriangular(BandMatrix<T> L, BandMatrix<T> b, int k, T[] x_workspace, IProvider<T> provider)
        {
            T[] L_diagonal = L.Diagonal;
            T[][] L_lower = L.LowerBands;
            int k1 = k - 1;

            // The k-th column of b matrix
            int row_start = Math.Max(0, k - b.UpperBandwidth), r, c;
            for (r = row_start; r < k; ++r)
            {
                // b[r, k] = b.Upper[k - r - 1][r] since r < k
                T xr = b.UpperBands[k1 - r][r];

                int offset = r - 1;
                // Subtract all terms L(r, c) * x(c) for c < r from the sum
                for (c = row_start; c < r; ++c)
                {
                    // L[r, c] = L_lower[r - c - 1][c]
                    xr = provider.Subtract(xr, provider.Multiply(x_workspace[c], L_lower[offset - c][c]));
                }

                // Get the diagonal term from the implicit matrix L(k - 1, k - 1)
                // Since L is lower triangular, the diagonal entry will always be the last element 
                // in the matrix
                x_workspace[r] = provider.Divide(xr, L_diagonal[r]);
            }

            // Insert row into values
            for (r = row_start; r < k; ++r)
            {
                // L[k, r] = L_lower[k - r - 1][r]
                L_lower[k1 - r][r] = x_workspace[r];
            }
        }

        /// <summary>
        /// An equivalent implementation of up-looking Cholesky using dense BLAS
        /// </summary>
        /// <param name="A"></param>
        /// <param name="blas1"></param>
        /// <returns></returns>
        internal BandMatrix<T> CholUp(BandMatrix<T> A, IDenseBLAS1<T> blas1)
        {
            if (A is null)
            {
                throw new ArgumentNullException(nameof(A));
            }
            if (!A.IsSquare)
            {
                throw new ArgumentOutOfRangeException(nameof(A), ExceptionMessages.MatrixNotSquare);
            }

            IProvider<T> provider = blas1.Provider;
            int dim = A.Columns, bw = A.UpperBandwidth;

            // The non-zero pattern of L is simply the non-zero pattern of the transpose
            // of the upper triangular portion of A (since we are assuming A to be symmetric).
            BandMatrix<T> L = new BandMatrix<T>(dim, dim, 0, bw);
            L[0, 0] = provider.Sqrt(A[0, 0]);

            T[] x_workspace = new T[dim], 
                L_workspace = new T[dim];

            for (int c = 1; c < dim; ++c)
            {
                // Solve L * r = b where L is the lower triangular factor so far (top c - 1 rows)
                // and r is the c-th row.
                T e = SolveLowerTriangular(L, A, c, x_workspace, L_workspace, blas1);

                // Append the L_22 term L_22 = sqrt(A_22 - L_12^T * L_12)
                T sq = provider.Subtract(A[c, c], e);
                if (provider.IsRealAndNegative(sq))
                {
                    throw new InvalidOperationException("Matrix is not positive-definite.");
                }
                L[c, c] = provider.Sqrt(sq);
            }
            return L;
        }

        /// <summary>
        /// Equivalent implementation using dense BLAS
        /// </summary>
        private T SolveLowerTriangular(BandMatrix<T> L, BandMatrix<T> b, int k, T[] x_workspace, T[] L_workspace, IDenseBLAS1<T> blas1)
        {
            IProvider<T> _provider = blas1.Provider;

            T[] L_diagonal = L.Diagonal;
            T[][] L_lower = L.LowerBands;
            int k1 = k - 1;

            // The k-th column of b matrix
            int row_start = Math.Max(0, k - b.UpperBandwidth), r, c;
            for (r = row_start; r < k; ++r)
            {
                int offset = r - 1;

                // Store all terms L(r, c) * x(c) for c < r from the sum
                for (c = row_start; c < r; ++c)
                {
                    // L[r, c] = L_lower[r - c - 1][c]
                    L_workspace[c] = L_lower[offset - c][c];
                }

                // b[r, k] = b.Upper[k - r - 1][r] since r < k
                T xr = _provider.Subtract(b.UpperBands[k1 - r][r], blas1.DOT(x_workspace, L_workspace, row_start, r));

                // Get the diagonal term from the implicit matrix L(k - 1, k - 1)
                // Since L is lower triangular, the diagonal entry will always be the last element 
                // in the matrix
                x_workspace[r] = _provider.Divide(xr, L_diagonal[r]);
            }

            // Insert row into values
            for (r = row_start; r < k; ++r)
            {
                // L[k, r] = L_lower[k - r - 1][r]
                L_lower[k1 - r][r] = x_workspace[r];
            }

            return blas1.DOT(x_workspace, x_workspace, row_start, k);
        }
    }
}
