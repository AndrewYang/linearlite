﻿using LinearNet.Global;
using LinearNet.Matrices.TriangularSolve;
using LinearNet.Providers;
using LinearNet.Structs;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace LinearNet.Matrices.Cholesky
{
    /// <summary>
    /// Dense Cholesky factorization using Strassen's multiplication and left-looking 
    /// block (supernodal) Cholesky
    /// </summary>
    public class BlockDenseCholesky<T> : IDenseCholeskyAlgorithm<T> where T : new()
    {
        private readonly IMatrixMultiplication<T> _strassen;
        private readonly IDenseTriangularSolver<T> _tsolver;
        private readonly int _blockSize;
        private readonly IDenseBLAS1<T> _blas1;
        private readonly IDenseBLAS2<T> _blas2;

        public BlockDenseCholesky(IMatrixMultiplication<T> baseAlgo, int blockSize, int threshold = 32)
        {
            if (baseAlgo is null)
            {
                throw new ArgumentNullException(nameof(baseAlgo));
            }
            if (blockSize <= 0)
            {
                throw new ArgumentOutOfRangeException(ExceptionMessages.NonPositiveNotAllowed);
            }

            if (!ProviderFactory.TryGetDefaultBLAS1Provider(out IDenseBLAS1<T> b1) ||
                !ProviderFactory.TryGetDefaultBLAS2Provider(out IDenseBLAS2<T> b2))
            {
                throw new NotSupportedException(ExceptionMessages.TypeNotSupported);
            }

            _blas1 = b1;
            _blas2 = b2;

            // Set up Strassen's multiplication
            _strassen = new VariableSizeStrassenMultiplication<T>(b1, b2, baseAlgo, blockSize, blockSize, blockSize, true, threshold);
            _tsolver = new NaiveTriangularSolver<T>(_blas1);
            _blockSize = blockSize;
        }

        public Cholesky<T> Decompose(DenseMatrix<T> A)
        {
            if (A is null)
            {
                throw new ArgumentNullException(nameof(A));
            }
            if (!A.IsSquare)
            {
                throw new InvalidOperationException(ExceptionMessages.MatrixNotSquare);
            }

            // Workspace to hold L_21', which can be up to (n, b) in size
            T[][] L_21_ws = MatrixInternalExtensions.JMatrix<T>(A.Rows, _blockSize);
            T[][] values = A.Values;

            // The left and right boundaries of L[:2] respectively
            int left, right = 0;
            while (right < A.Rows)
            {
                left = right;
                right = Math.Min(right + _blockSize, A.Rows);
                int size = right - left;

                // Copy L_21' into the workspace
                for (int r = left, r1 = 0; r < right; ++r, ++r1)
                {
                    T[] row = values[r];
                    for (int c = 0; c < left; ++c)
                    {
                        L_21_ws[c][r1] = row[c];
                    }
                }

                // L_21' <- -L_21'
                _blas2.Negate(L_21_ws, L_21_ws, 0, 0, 0, 0, left, size);

                // A_22 <- A_22 + L_21 * (-L_21') and 
                // A_32 <- A_32 + L_31 * (-L_21') at the same time
                _strassen.Multiply(values, L_21_ws, values,
                    left, 0,    // L_21 + L_31 coordinates
                    0, 0,       // L_21' coordinates
                    left, left, // A_22 + A_32 coordinates
                    new Size3(A.Rows - left, left, size),
                    true);

                // L_22 <- chol(A_22) = chol(A_22 - L_21 * L_21')
                if (!DenseCholeskyInPlace(values, left, size, _blas1))
                {
                    throw new InvalidOperationException("Matrix is not positive definite.");
                }

                // LT_solve(L_32 * L_22' = A_32) = LT_solve(L_32 * L_22' = (A_32 - L_31 * L_21'))
                if (!SolveTriangular(values, values, left, left, right, left, A.Rows - right, size, _blas1))
                {
                    throw new InvalidOperationException("Matrix is not positive definite.");
                }

                /*
                if (!_tsolver.Solve(values, values, left, left, right, left, A.Rows - right, size, true, false, true))
                {
                    throw new InvalidOperationException("Matrix is not positive definite.");
                }*/
            }

            return new Cholesky<T>(A, _blas1);
        }

        /// <summary>
        /// For A[offset: offset + dim, offset: offset + dim] (dense lower triangular), 
        /// finds the dense lower triangular Cholesky factor L in place
        /// </summary>
        private static bool DenseCholeskyInPlace(T[][] A, int offset, int dim, IDenseBLAS1<T> blas)
        {
            int end = offset + dim, r, c;

            IProvider<T> provider = blas.Provider;
            T zero = provider.Zero;

            for (r = offset; r < end; ++r)
            {
                T[] A_r = A[r];
                T s = zero;
                for (c = offset; c < r; c++)
                {
                    // Off-diagonal term in row r, col c where c < r is 
                    // (A(r, c) - sum(L(r, i) * L(c, i), i in [0, c))) / L(c, c)

                    T[] L_c = A[c];
                    T L_rc = blas.DOT(A_r, L_c, offset, c);
                    L_rc = provider.Divide(provider.Subtract(A_r[c], L_rc), L_c[c]);
                    A_r[c] = L_rc;
                    s = provider.Add(s, provider.Multiply(L_rc, L_rc));
                }

                // Diagonal term is L(r, r) = sqrt(A(r, r) - sum(L(r, i)^2, i in [0, r))
                T sq = provider.Subtract(A_r[r], s);
                if (provider.IsRealAndNegative(sq))
                {
                    return false;
                }
                A_r[r] = provider.Sqrt(sq);
            }
            return true;
        }

        /// <summary>
        /// Solve X * L' = B, where B is overwritten with X.
        /// </summary>
        private bool SolveTriangular(T[][] L, T[][] B, int A_row_offset, int A_col_offset, int B_row_offset, int B_col_offset, int B_rows, int B_cols, 
            IDenseBLAS1<T> blas)
        {
            IProvider<T> provider = blas.Provider;
            T zero = provider.Zero;

            int BA_offset = B_col_offset - A_col_offset;
            for (int k = 0; k < B_rows; ++k)
            {
                T[] b = B[B_row_offset + k];

                for (int c = 0, ai = A_row_offset, bix = B_col_offset; c < B_cols; ++c, ++ai, ++bix)
                {
                    T dot = blas.DOT(L[ai], b, A_col_offset, A_col_offset + c, BA_offset);

                    T L_cc = L[ai][ai];
                    if (provider.Equals(zero, L_cc))
                    {
                        return false;
                    }
                    b[bix] = provider.Divide(provider.Subtract(b[bix], dot), L_cc);
                }
            }

            return true;
        }
    }
}
