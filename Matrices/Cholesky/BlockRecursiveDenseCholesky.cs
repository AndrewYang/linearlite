﻿using LinearNet.Global;
using LinearNet.Matrices.TriangularSolve;
using LinearNet.Providers;
using System;
using System.Collections.Generic;
using System.Text;

namespace LinearNet.Matrices.Cholesky
{
    public class BlockRecursiveDenseCholesky<T> where T : new()
    {
        private readonly IMatrixMultiplication<T> _strassen;
        private readonly IDenseTriangularSolver<T> _tsolve;
        private readonly int _threshold;
        private readonly IDenseBLAS1<T> _blas1;
        private readonly IDenseBLAS2<T> _blas2;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="baseAlgo"></param>
        /// <param name="size">The size of the matrix to decompose</param>
        /// <param name="threshold"></param>
        public BlockRecursiveDenseCholesky(IMatrixMultiplication<T> baseAlgo, int size, int threshold = 32)
        {
            if (baseAlgo is null)
            {
                throw new ArgumentNullException(nameof(baseAlgo));
            }

            if (!ProviderFactory.TryGetDefaultBLAS1Provider(out IDenseBLAS1<T> b1) ||
                !ProviderFactory.TryGetDefaultBLAS2Provider(out IDenseBLAS2<T> b2))
            {
                throw new NotSupportedException(ExceptionMessages.TypeNotSupported);
            }

            _blas1 = b1;
            _blas2 = b2;

            // Set up Strassen's multiplication
            int halfSize = size / 2;
            _strassen = new VariableSizeStrassenMultiplication<T>(b1, b2, baseAlgo, halfSize, halfSize, halfSize, true, threshold);

            // Set up the triangular solver
            IDenseTriangularSolver<T> tsolver_base = new NaiveTriangularSolver<T>(b1);
            //_tsolve = new StrassenTriangularSolver<T>(tsolver_base, _strassen, b1, b2, threshold * 2, threshold);
            _threshold = threshold;
        }
    }
}
