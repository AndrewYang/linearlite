﻿using LinearNet.Global;
using LinearNet.Graphs;
using LinearNet.Matrices.Cholesky.Kernels;
using LinearNet.Providers;
using LinearNet.Structs;
using System;
using System.Diagnostics;

namespace LinearNet.Matrices.Sparse.Decompositions
{
    internal class CSCCholesky<T> where T : new()
    {
        /// <summary>
        /// Up-looking naive Cholesky
        /// </summary>
        internal CSCSparseMatrix<T> CholUp(CSCSparseMatrix<T> A, SymbolicCholesky analysis, IProvider<T> provider)
        {
            if (A is null)
            {
                throw new ArgumentNullException(nameof(A));
            }
            if (!A.IsSquare)
            {
                throw new ArgumentOutOfRangeException(nameof(A), ExceptionMessages.MatrixNotSquare);
            }
            if (analysis is null)
            {
                throw new ArgumentNullException(nameof(analysis));
            }

            EliminationTree etree = analysis.ElimTree;
            T zero = provider.Zero;

            int[] columnIndices = A.ColumnIndices,
                rowIndices = A.RowIndices;
            T[] values = A.Values;

            int dim = A.Rows, nnz = analysis.NumNonZeroElements;
            int[] L_columnIndices = analysis.ColumnIndices.Copy();
            T[] L_values = new T[nnz];
            int[] L_rowIndices = new int[nnz];

            // Since we are traversing one row at a time, 
            // and inserting into the values column-wise,
            // the resulting structure will be sorted.
            // We also need a workspace to keep track of 
            // the index at which we should be adding the 
            // next entry in each column. This is stored in 
            // 'next'
            int[] next = new int[dim];
            for (int i = 0; i < dim; ++i)
            {
                // The first entry in each column c is stored 
                // in columnIndex[c]
                next[i] = L_columnIndices[i];
            }

            // Workspace for temporarily storing the solution
            // to a triangular solve of up to size (dim - 1)
            T[] x = new T[dim];

            // Two additional workspaces of size dim is needed
            // to compute the reach of each node. 
            // The first is a buffer storing all the nodes in 
            // the reach. The second is an array used to keep 
            // track of which nodes have already been added to
            // the buffer (since it is a set union). The array
            // holds the index of the largest node that has 
            // called the reach algorithm
            int[] reach = new int[dim];
            int[] in_reach = new int[dim];

            // Solve for the Cholesky factor one row at a time
            for (int k = 0; k < dim; ++k)
            {
                // Traverse the elimination tree to find the non-zero 
                // pattern of row r. This is just all the nodes in 
                // subtree r of the elimination tree. 
                int top = etree.Reach(A, k, reach, in_reach, k + 1);

                // Initialize x[r] = 0 (since x was never initialized)
                x[k] = provider.Zero;

                // The column A(0:k, k) is the 'b' vector in the triangular
                // solve Lx = b. This vector is not explicitly stored.
                // Instead it is directly copied into the 'x' solution
                // vector as part of the triangular solve routine.
                int col_end = columnIndices[k + 1];
                for (int i = columnIndices[k]; i < col_end; ++i)
                {
                    int r = rowIndices[i];
                    if (r > k)
                    {
                        break; // Only look at the upper half of A
                    }
                    // Copy into x solution vector
                    x[r] = values[i];
                }

                // Extract diagonal term, then set to zero for the next 
                // iteration, which is required to guarantee that all x 
                // will be zero for future iterations
                T diag = x[k];
                x[k] = zero;

                // Perform the triangular solve
                for (int i = top; i < dim; ++i)
                {
                    // L[k, c] is non-zero since it is in the reach
                    int c = reach[i];

                    // L[k, c] = x[c] / L[c, c], but we store it in xc 
                    // for now to keep naming consistent with other 
                    // triangular solves
                    T xc = provider.Divide(x[c], L_values[L_columnIndices[c]]);

                    // Clear for future iterations, x[c] is not required
                    // from here 
                    x[c] = zero;

                    // Iterate through the rest of column c in L
                    int L_col_end = next[c];
                    for (int p = L_columnIndices[c] + 1; p < L_col_end; ++p)
                    {
                        int r = L_rowIndices[p];
                        x[r] = provider.Subtract(x[r], provider.Multiply(L_values[p], xc)); 
                    }

                    // Subtract L[k, i]^2 from diagonal term to incrementally
                    // calculate A[k, k] - L[k, *]^2 = L[k, k]^2
                    diag = provider.Subtract(diag, provider.Multiply(xc, xc));

                    // Insert into column c of L matrix
                    next[c]++;
                    L_rowIndices[L_col_end] = k;
                    L_values[L_col_end] = xc;
                }

                // Calculate the diagonal term L[k, k]
                if (provider.IsRealAndNegative(diag))
                {
                    throw new InvalidOperationException("Matrix is not positive definite.");
                }

                int index = next[k];
                L_rowIndices[index] = k;
                L_values[index] = provider.Sqrt(diag);
                next[k]++;
            }

            return new CSCSparseMatrix<T>(dim, L_columnIndices, L_rowIndices, L_values);
        }

        /// <summary>
        /// Up-looking Cholesky using dense clustering in the triangular solve. ~5-7x performance improvement compared to CholUp
        /// when using an AVX2 kernel
        /// </summary>
        internal CSCSparseMatrix<T> CholUp2(CSCSparseMatrix<T> A, SymbolicCholesky analysis, IProvider<T> provider, ICSCCholeskyTriangularSolveKernel<T> kernel)
        {
            if (A is null)
            {
                throw new ArgumentNullException(nameof(A));
            }
            if (!A.IsSquare)
            {
                throw new ArgumentOutOfRangeException(nameof(A), ExceptionMessages.MatrixNotSquare);
            }
            if (analysis is null)
            {
                throw new ArgumentNullException(nameof(analysis));
            }

            int maxBlocks = kernel.MaxColumns;

            EliminationTree etree = analysis.ElimTree;
            T zero = provider.Zero;

            int[] columnIndices = A.ColumnIndices,
                rowIndices = A.RowIndices;
            T[] values = A.Values;

            int dim = A.Rows, nnz = analysis.NumNonZeroElements;
            int[] L_columnIndices = analysis.ColumnIndices.Copy();
            T[] L_values = new T[nnz];
            int[] L_rowIndices = new int[nnz];

            // Since we are traversing one row at a time, 
            // and inserting into the values column-wise,
            // the resulting structure will be sorted.
            // We also need a workspace to keep track of 
            // the index at which we should be adding the 
            // next entry in each column. This is stored in 
            // 'next'
            int[] next = new int[dim];
            for (int i = 0; i < dim; ++i)
            {
                // The first entry in each column c is stored 
                // in columnIndex[c]
                next[i] = L_columnIndices[i];
            }

            // Workspace for temporarily storing the solution
            // to a triangular solve of up to size (dim - 1)
            T[] x = new T[dim];

            // Two additional workspaces of size dim is needed
            // to compute the reach of each node. 
            // The first is a buffer storing all the nodes in 
            // the reach. The second is an array used to keep 
            // track of which nodes have already been added to
            // the buffer (since it is a set union). The array
            // holds the index of the largest node that has 
            // called the reach algorithm
            int[] reach = new int[dim];
            int[] in_reach = new int[dim];

            // Solve for the Cholesky factor one row at a time
            for (int k = 0; k < dim; ++k)
            {
                // Traverse the elimination tree to find the non-zero 
                // pattern of row r. This is just all the nodes in 
                // subtree r of the elimination tree. 
                int top = etree.Reach(A, k, reach, in_reach, k + 1);

                // Initialize x[r] = 0 (since x was never initialized)
                x[k] = provider.Zero;

                // The column A(0:k, k) is the 'b' vector in the triangular
                // solve Lx = b. This vector is not explicitly stored.
                // Instead it is directly copied into the 'x' solution
                // vector as part of the triangular solve routine.
                int col_end = columnIndices[k + 1];
                for (int i = columnIndices[k]; i < col_end; ++i)
                {
                    int r = rowIndices[i];
                    if (r > k)
                    {
                        break; // Only look at the upper half of A
                    }
                    // Copy into x solution vector
                    x[r] = values[i];
                }

                // Extract diagonal term, then set to zero for the next 
                // iteration, which is required to guarantee that all x 
                // will be zero for future iterations
                T diag = x[k];
                x[k] = zero;

                // Perform the triangular solve
                for (int i = top; i < dim; )
                {
                    // L[k, c] is non-zero since it is in the reach
                    int c = reach[i],
                        L_col_start = L_columnIndices[c],
                        L_col_end = next[c];

                    // Look ahead up to maxBlocks columns to group
                    // As we traverse the subtree, all nodes 
                    int prevNode = c, 
                        prevLen = L_col_end - L_col_start, 
                        j = 1;
                    while (j < maxBlocks && i + j < dim)
                    {
                        // The next node in the reach
                        int col = reach[i + j];

                        // Check to see if this node is the parent of the 
                        // previous node
                        if (etree.Parents[prevNode] != col) break;

                        // Get the number of non-zeroes in column 'col' of 
                        // L computed so far
                        int len = next[col] - L_columnIndices[col];

                        // Dissimilar non-zero pattern between this column
                        // and the previous column
                        if (len + 1 != prevLen) break;

                        prevNode = col;
                        prevLen = len;

                        ++j;
                    }

                    if (j == 2)
                    {
                        diag = kernel.Solve(L_columnIndices, L_rowIndices, L_values, x, k, c, reach[i + 1], next, diag);
                        i += 2;
                    }
                    else if (j == 3)
                    {
                        diag = kernel.Solve(L_columnIndices, L_rowIndices, L_values, x, k, c, reach[i + 1], reach[i + 2], next, diag);
                        i += 3;
                    }
                    else if (j == 4)
                    {
                        diag = kernel.Solve(L_columnIndices, L_rowIndices, L_values, x, k, c, reach[i + 1], reach[i + 2], reach[i + 3], next, diag);
                        i += 4;
                    }
                    else if (j > 4)
                    {
                        diag = kernel.Solve(L_columnIndices, L_rowIndices, L_values, x, k, reach, i, j, next, diag);
                        i += j;
                    }
                    // Proceed as normal
                    else
                    {
                        // L[k, c] = x[c] / L[c, c], but we store it in xc 
                        // for now to keep naming consistent with other 
                        // triangular solves
                        T xc = provider.Divide(x[c], L_values[L_col_start]);

                        // Clear for future iterations, x[c] is not required
                        // from here 
                        x[c] = zero;

                        // Iterate through the rest of column c in L
                        for (int p = L_col_start + 1; p < L_col_end; ++p)
                        {
                            int r = L_rowIndices[p];
                            x[r] = provider.Subtract(x[r], provider.Multiply(L_values[p], xc));
                        }

                        // Subtract L[k, i]^2 from diagonal term to incrementally
                        // calculate A[k, k] - L[k, *]^2 = L[k, k]^2
                        diag = provider.Subtract(diag, provider.Multiply(xc, xc));

                        // Insert into column c of L matrix
                        next[c]++;
                        L_rowIndices[L_col_end] = k;
                        L_values[L_col_end] = xc;

                        // Move to the next node
                        ++i;
                    }
                }

                // Calculate the diagonal term L[k, k]
                if (provider.IsRealAndNegative(diag))
                {
                    throw new InvalidOperationException("Matrix is not positive definite.");
                }

                int index = next[k];
                L_rowIndices[index] = k;
                L_values[index] = provider.Sqrt(diag);
                next[k]++;
            }

            return new CSCSparseMatrix<T>(dim, L_columnIndices, L_rowIndices, L_values);
        }

        /// <summary>
        /// Returns the index of the diagonal element in A column c, or throws if the diagonal element is 
        /// structurally zero.
        /// </summary>
        /// <param name="A"></param>
        /// <returns></returns>
        private int CalculateDiagonalIndex(CSCSparseMatrix<T> A, int c)
        {
            int col_start = A.ColumnIndices[c], col_end = A.ColumnIndices[c + 1];
            int index = Array.BinarySearch(A.RowIndices, col_start, col_end - col_start, c);
            if (index < 0)
            {
                throw new InvalidOperationException("Matrix is not positive definite.");
            }
            return index;
        }

        /// <summary>
        /// Left-looking supernodal Cholesky algorithm
        /// </summary>
        /// <param name="A"></param>
        /// <param name="analysis"></param>
        /// <param name="blas"></param>
        /// <returns></returns>
        internal CSCSparseMatrix<T> CholLeftSuper(CSCSparseMatrix<T> A, SymbolicCholesky analysis, IDenseBLAS1<T> blas)
        {
            if (A is null)
            {
                throw new ArgumentNullException(nameof(A));
            }
            if (!A.IsSquare)
            {
                throw new InvalidOperationException(ExceptionMessages.MatrixNotSquare);
            }
            if (blas is null)
            {
                throw new ArgumentNullException(nameof(blas));
            }

            if (analysis is null || analysis.ElimTree is null || analysis.ColumnIndices is null)
            {
                analysis = A.CholeskyAnalysis();
            }

            int dim = A.Rows, nnz = analysis.NumNonZeroElements;
            int[] columnIndices = analysis.ColumnIndices.Copy(),
                rowIndices = new int[nnz];
            T[] values = new T[nnz];

            int[] A_columnIndices = A.ColumnIndices,
                A_rowIndices = A.RowIndices;
            T[] A_values = A.Values;

            EliminationTree etree = analysis.ElimTree;
            IProvider<T> provider = blas.Provider;
            T zero = provider.Zero;

            // Determine the size of workspace required to hold the dense matrix A_32 - L_31 * L_21'
            // which will be the 'B' matrix in the dense triangular solve L_32 * L_22' = B.
            // This requires an iteration through the structure of L, stored in size2.
            // Also use this loop to determine the required workspace size to compute L_22 (the dense
            // Cholesky factor) which will be a square matrix of dimension equal to the width of the 
            // largest supernode (stored in size1)
            int size1 = 0, size2 = 0;
            for (int c = 0; c < dim; ++c)
            {
                int nnz_col = columnIndices[c + 1] - columnIndices[c];

                int j = c;
                while (etree.Parents[j] == j + 1 && nnz_col == columnIndices[j + 2] - columnIndices[j + 1] + 1) 
                {
                    --nnz_col;
                    ++j;
                }

                // [c, j] forms a supernode with nnz_col common columns
                int width = j - c + 1, s;
                checked
                {
                    s = width * nnz_col;
                }
                if (width > size1)
                {
                    size1 = width;
                }
                if (s > size2)
                {
                    size2 = s;
                }

                // Prevent re-iteration
                c = j;
            }

            // Initialize the workspace for solving L_22
            T[][] workspace1 = InitializeLowerTriangularWorkspace(size1);

            // Initialize the workspace for solving L_32, storing a matrix in column-major 
            // order
            T[] workspace2 = new T[size2];
            // If workspace2_index[r] = i then workspace2[i + c * height] represents L[r, c]
            int[] workspace2_index = new int[dim];

            // Workspace for keeping track of the index of the next non-zero entry
            // in each column. Used to keep track of the boundary between L_11 and L_21 
            int[] next = new int[dim];
            Array.Copy(columnIndices, next, dim);

            // Workspace for keeping track of the first term inside L_31 in each column
            // Used to keep track of the boundary between L_21 and L_31
            int[] floor = new int[dim];

            // Workspace for calculating the reach of each row
            int[] reach = new int[dim];
            if (dim > int.MaxValue / 2)
            {
                throw new ArgumentOutOfRangeException("The matrix size is too large for the supernodal Cholesky algorithm.");
            }
            int[] in_reach = new int[dim];
            int membership_threshold = 1;

            // Since this is a left-looking Cholesky, we need to precompute the entire non-zero
            // pattern of L in |L| time. This code we borrowed from the CholLeft implementation
            // The array next is borrowed to keep track of where each element should be placed
            for (int r = 0; r < dim; ++r)
            {
                // Calculate the reach of L[r, 0 : r - 1] - for the non-zero pattern of row r
                // excluding the diagonal
                int top = etree.Reach(A, r, reach, in_reach, membership_threshold++);
                for (int i = top; i < dim; ++i)
                {
                    int c = reach[i];
                    rowIndices[next[c]++] = r;
                }

                // Insert the diagonal term
                rowIndices[next[r]++] = r;
            }

            // Reset the required arrays
            Array.Copy(columnIndices, next, dim);

            // Iterate column-wise
            for (int c = 0; c < dim; ++c)
            {
                // Determine the size of the current supernode
                int L_col_start = columnIndices[c],
                    L_col_end = columnIndices[c + 1],
                    nnz_col = L_col_end - L_col_start,
                    j = c;

                while (etree.Parents[j] == j + 1 && nnz_col == columnIndices[j + 2] - columnIndices[j + 1] + 1)
                {
                    --nnz_col;
                    ++j;
                }

                // The width of the current supernode
                int width = j - c + 1;

                // TODO: handle width == 1 case differently...

                // Clear the (lower) workspace prior to loading with A_22
                ClearLowerTriangularWorkspace(workspace1, width, zero);

                // Iterate over the columns of the supernode, extracting 
                // A_22 and inserting into workspace 1 (lower)
                int A_22_lower_bound = c + width; // the row index of the lower boundary of A_22 block
                for (int sc = c; sc <= j; ++sc)
                {
                    // The column within the workspace corresponding to sc
                    int ws_col = sc - c;

                    // Assuming lower-diagonal or full structure, start at the diagonal and iterate down
                    int A_col_start = CalculateDiagonalIndex(A, sc), A_col_end = A_columnIndices[sc + 1];
                    for (int si = A_col_start; si < A_col_end; ++si)
                    {
                        // The row index
                        int sr = A_rowIndices[si];
                        if (sr >= A_22_lower_bound) break;

                        workspace1[sr - c][ws_col] = A_values[si];
                    }
                }

                // Subtract L_21 * L_21' from workspace1 to form A_22 - L_21 * L_21' (= L_22 * L_22')
                // The top boundary of L_21's columns are stored in next, update them so they point to
                // the first nz entry under L_21
                // This operation is actually a rank-1 downdate of A_22, so we proceed one column at a 
                // time 
                // ... wait a sec isn't this going to make our algorithm O(n^2) at least... unless if
                // there is a better way to compute L_21 * L_21' (perhaps if we have it also stored by 
                // row... which brings additional problems since we can't use a rank-1 update, has to 
                // be evaluated using dot products...)
                for (int lc = 0; lc < c; ++lc)
                {
                    int ri = next[lc], col_end = columnIndices[lc + 1];

                    // Perform the rank-1 downdate over non-zeroes in column lc
                    while (ri < col_end && rowIndices[ri] < A_22_lower_bound)
                    {
                        // A non-zero at L[lc, r], convert into workspace coordinates and subtract from 
                        int r = rowIndices[ri] - c;
                        if (r < 0) continue;

                        T e = values[ri];
                        T[] workspace_r = workspace1[r];

                        int si = next[lc];
                        // Since A_22 is only lower triangular, only iterate up to the diagonal
                        while (si <= ri)
                        {
                            int s = rowIndices[si] - c;
                            workspace_r[s] = provider.Subtract(workspace_r[s], provider.Multiply(e, values[si]));
                            ++si;
                        }
                        ++ri;
                    }

                    // Set the floor to the first term inside L_31 (and under L_21)
                    floor[lc] = ri;
                }

                // Perform a dense Cholesky factorization of workspace1, which now holds A_22 - L_21 * L_21'
                // Also check for positive definite-ness 
                if (!DenseCholeskyInPlace(workspace1, width, blas))
                {
                    throw new InvalidOperationException("Matrix is not positive definite.");
                }

                // Store the dense Cholesky factor from workspace1 to L, updating next[c]... next[j]
                for (int sc = c; sc <= j; ++sc)
                {
                    // workspace column index
                    int wc = sc - c;
                    int k = next[sc];
                    // Iterate down - cringe
                    for (int wr = wc; wr < width; ++wr)
                    {
                        //Debug.WriteLine($"values[{k}] = {workspace1[wr][wc]}");
                        values[k++] = workspace1[wr][wc];
                    }

                    // next[c], ... next[j] is now in L_32 (note that L_31 starting indices is in floor)
                    next[sc] = k;
                }

                // Compute A_32 - L_31 * L_21' (width x height, in row-major order)
                int height = nnz_col - 1;

                // Calculate the indices for prior to loading A_32 into workspace2
                // Since the nz row indices are identical for columns c ... j, we just use c to determine 
                // the non-zero pattern of the entire submatrix
                int ri_end = columnIndices[c + 1];
                for (int ri = next[c], k = 0; ri < ri_end; ++ri)
                {
                    // row 'rowIndices[ri]' will be stored in location k in the workspace
                    workspace2_index[rowIndices[ri]] = k++;
                }

                // Load A_32 into workspace2 - this requires A to have a explicitly stored lower half...
                Clear(workspace2, height * width, zero);
                for (int sc = c; sc <= j; ++sc)
                {
                    // Iterate down column sc within matrix A
                    int col_end = A_columnIndices[sc + 1];
                    for (int i = A_columnIndices[sc]; i < col_end; ++i)
                    {
                        // Skip over the lower triangular portion (already handled by the dense Cholesky
                        // decomposition)
                        if (A_rowIndices[i] <= j) continue;

                        // We are going to trust that workspace2_index is properly initialized, and this 
                        // is going to point to a valid index within workspace2
                        int r = workspace2_index[A_rowIndices[i]];

                        // Insert in column-major order
                        workspace2[r + (sc - c) * height] = A_values[i];
                    }
                }

                // Subtract L_31 * L_21' from workspace2, which will hold A_32 - L_31 * L_21' afterwards
                // For each row c ... j
                for (int r = c; r <= j; ++r)
                {
                    // Calculate the non-zero pattern for row r
                    int top = etree.Reach(A, r, reach, in_reach, membership_threshold++);
                    CheckMembershipThresholdOverflow(in_reach, ref membership_threshold);

                    // The column index inside workspace2
                    int wc = r - c;
                    // For each non-zero in row r, multiply down starting at floor[col], and perform
                    // a set union to store in column
                    for (int i = top; i < dim; ++i)
                    {
                        int col = reach[i];
                        if (col >= c) break;

                        // Get the value of the element at L[r, col]. Also serves as the place where 
                        // increment the next array, for future iterations
                        T e = values[next[col]++];

                        // Multiply down the column, starting at the row index of floor[col]
                        int end = columnIndices[col + 1];
                        for (int ri = floor[col]; ri < end; ++ri)
                        {
                            // The workspace row
                            int wr = workspace2_index[rowIndices[ri]];

                            // Calculate index within workspace, in column-major order
                            int index = wr + wc * height;
                            workspace2[index] = provider.Subtract(workspace2[index], provider.Multiply(values[ri], e));
                        }
                    }
                }
                
                // Solve the triangular system L_22 * L_32' = (A_32 - L_31 * L_21)'
                // with L_22 stored in workspace1, and RHS stored in workspace2
                // workspace2 is overwritten with the solution L_32'
                if (!DenseLTSolveInPlace(workspace1, workspace2, width, height, blas))
                {
                    throw new InvalidOperationException("Division by zero encountered in the Cholesky decomposition, unable to proceed.");
                }

                // There is no need for reverse lookup table for the row indices of L_32, since
                // the row indices are already precomputed! Beautiful!
                for (int lc = c; lc <= j; ++lc)
                {
                    // the workspace column
                    int wc = lc - c;

                    // Sequential copy directly into values 
                    Array.Copy(workspace2, wc * height, values, next[lc], height);
                }

                // Remember to reset c
                c = j;
            }

            return new CSCSparseMatrix<T>(dim, columnIndices, rowIndices, values);
        }

        /// <summary>
        /// Create a lower-triangular matrix of dimension dim x dim
        /// </summary>
        private static T[][] InitializeLowerTriangularWorkspace(int dim)
        {
            T[][] L = new T[dim][];
            for (int r = 0; r < dim; ++r)
            {
                L[r] = new T[r + 1];
            }
            return L;
        }

        private static void ClearLowerTriangularWorkspace(T[][] L, int dim, T zero)
        {
            if (zero.Equals(default))
            {
                for (int i = 0; i < dim; ++i)
                {
                    Array.Clear(L[i], 0, i + 1);
                }
            }
            else
            {
                for (int i = 0; i < dim; ++i)
                {
                    T[] row = L[i];
                    for (int j = 0; j <= i; ++j)
                    {
                        row[j] = zero;
                    }
                }
            }
        }

        private static void Clear(T[] workspace, int dim, T zero)
        {
            if (zero.Equals(default))
            {
                Array.Clear(workspace, 0, dim);
            }
            else
            {
                for (int i = 0; i < dim; ++i)
                {
                    workspace[i] = zero;
                }
            }
        }

        private static void CheckMembershipThresholdOverflow(int[] in_set, ref int threshold)
        {
            if (threshold == int.MaxValue)
            {
                Array.Clear(in_set, 0, in_set.Length);
                threshold = 1;
            }
        }

        /// <summary>
        /// For A dense lower triangular, finds the dense lower triangular Cholesky factor L 
        /// </summary>
        private static bool DenseCholeskyInPlace(T[][] A, int dim, IDenseBLAS1<T> blas)
        {
            int r, c;

            IProvider<T> provider = blas.Provider;
            T zero = provider.Zero;

            for (r = 0; r < dim; ++r)
            {
                T[] A_r = A[r];
                T s = zero;
                for (c = 0; c < r; c++)
                {
                    // Off-diagonal term in row r, col c where c < r is 
                    // (A(r, c) - sum(L(r, i) * L(c, i), i in [0, c))) / L(c, c)

                    T[] L_c = A[c];
                    T L_rc = blas.DOT(A_r, L_c, 0, c);
                    L_rc = provider.Divide(provider.Subtract(A_r[c], L_rc), L_c[c]);
                    A_r[c] = L_rc;
                    s = provider.Add(s, provider.Multiply(L_rc, L_rc));
                }

                // Diagonal term is L(r, r) = sqrt(A(r, r) - sum(L(r, i)^2, i in [0, r))
                T sq = provider.Subtract(A_r[r], s);
                if (provider.IsRealAndNegative(sq))
                {
                    Debug.WriteLine(sq + "\t" + A_r[r] + "\t" + s + "\t" + dim);
                    A.Print(x => x.ToString());
                    return false;
                }
                A_r[r] = provider.Sqrt(sq);
            }
            return true;
        }

        /// <summary>
        /// Solve the triangular system L'X = B, storing the result X in B.
        /// B is a matrix (width x height) stored in row-major order as a single vector.
        /// </summary>
        private static bool DenseLTSolveInPlace(T[][] L, T[] B, int dim, int m, IDenseBLAS1<T> blas)
        {
            IProvider<T> provider = blas.Provider;
            T zero = provider.Zero, one = provider.One;

            for (int r = 0; r < dim; ++r)
            {
                T[] row = L[r];
                int Xr_start = r * m, Xr_end = Xr_start + m;

                // X[r] -= L[r][c] * X[c]
                for (int c = 0; c < r; ++c)
                {
                    int Xc_start = c * m;
                    blas.AXPY(B, B, provider.Negate(row[c]), Xr_start, Xr_end, Xc_start - Xr_start);
                }

                T diag = row[r];
                if (provider.Equals(diag, zero))
                {
                    return false;
                }

                // X[r] /= diag
                // X[r] is stored in B[r * width, (r + 1) * width)
                blas.SCAL(B, provider.Divide(one, diag), Xr_start, Xr_end);
            }
            return true;
        }
    }
}
