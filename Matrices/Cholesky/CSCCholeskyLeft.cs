﻿using LinearNet.Global;
using LinearNet.Graphs;
using LinearNet.Matrices.Sparse;
using LinearNet.Providers;
using LinearNet.Structs;
using System;

namespace LinearNet.Matrices.Cholesky
{
    internal class CSCCholeskyLeft<T> where T : new()
    {
        /// <summary>
        /// Left-looking Cholesky - assumes that A is stored in lower triangular format.
        /// i.e. only the lower triangular portion of A is used.
        /// </summary>
        internal CSCSparseMatrix<T> CholLeft(CSCSparseMatrix<T> A, SymbolicCholesky analysis, IDenseBLAS1<T> blas1)
        {
            if (A is null)
            {
                throw new ArgumentNullException(nameof(A));
            }
            if (!A.IsSquare)
            {
                throw new InvalidOperationException(ExceptionMessages.MatrixNotSquare);
            }

            // We need the upper triangular representation to compute the reach, but also 
            // currently require the lower triangular portion for computing the matrix-vector
            // multiplication...
            A = A.ReflectLower();

            IProvider<T> provider = blas1.Provider;
            T neg_one = provider.Negate(provider.One);

            int dim = A.Columns, nnz = analysis.NumNonZeroElements;
            int[] columnIndices = analysis.ColumnIndices.Copy(),
                rowIndices = new int[nnz];
            T[] values = new T[nnz];

            int[] A_rowIndices = A.RowIndices, A_columnIndices = A.ColumnIndices;
            T[] A_values = A.Values;

            EliminationTree tree = analysis.ElimTree;

            // Workspace for keeping track of the index of the next non-zero entry
            // in each column
            int[] next = new int[dim];
            Array.Copy(columnIndices, next, dim);

            // Workspace for calculating the reach of each row
            int[] reach = new int[dim];
            // Workspace for determining the set union during the reach calculation
            // It is also used to store the set union of the temporary column being
            // computed. It achieves this double-storage by checking for membership
            // against 2 * c + 1 for the reach, and 2 * c + 2 for the column. This 
            // reduces the maximum size of the matrix to int.MaxValue / 2, which we 
            // check for here
            if (dim > int.MaxValue / 2)
            {
                throw new ArgumentOutOfRangeException("The matrix size is too large for the left-looking Cholesky algorithm.");
            }
            int[] in_reach = new int[dim];

            // Workspace for temporarily holding the values of a column
            T[] column = new T[dim];
            // Workspace for storing the non-zero pattern of column
            // Since the reach is never used concurrently with the column index, 
            // we share the same workspace.
            int[] column_index = reach;

            // Pre-compute the non-zero pattern of L in |L| time
            // borrow next to keep track of where each element should be placed
            for (int r = 0; r < dim; ++r)
            {
                // Calculate the reach of L[r, 0 : r - 1] - for the non-zero pattern of row r
                // excluding the diagonal
                int top = tree.Reach(A, r, reach, in_reach, r + 1);
                for (int i = top; i < dim; ++i)
                {
                    int c = reach[i];
                    rowIndices[next[c]++] = r;
                }

                // Insert the diagonal term
                rowIndices[next[r]++] = r;
            }

            // Reset the required arrays
            Array.Copy(columnIndices, next, dim);
            Array.Clear(in_reach, 0, dim);

            // Solve for the Cholesky factor one column at a time
            for (int c = 0; c < dim; ++c)
            {
                int A_col_diag = CalculateDiagonalIndex(A, c),
                    A_col_end = A_columnIndices[c + 1], k;

                // L_22 = A_22 - L_21' * L_21

                // Set to A_22
                T sq = A_values[A_col_diag];

                // Calculate the reach of L[c, 0 : c - 1]
                int top = tree.Reach(A, c, reach, in_reach, 2 * c + 1);
                for (int j = top; j < dim; ++j)
                {
                    int col = reach[j];
                    T e = values[next[col]]; // Save the incrementation for later
                    sq = provider.Subtract(sq, provider.Multiply(e, e));
                }

                // Check for positive definite-ness
                if (provider.IsRealAndNegative(sq))
                {
                    throw new InvalidOperationException("Matrix is not positive definite.");
                }

                // Insert L_22 into L storage
                int L_c_start = columnIndices[c];
                T L_22 = provider.Sqrt(sq);
                values[L_c_start] = L_22;

                // incrementation for this column (down from the diagonal)
                next[c]++;

                // Special case for first column - no need to calculate L_31 * L_21
                if (c == 0)
                {
                    int start = A_col_diag + 1,
                        L_start = L_c_start + 1;

                    Array.Copy(A.Values, start, values, L_start, A_col_end - start);
                    blas1.SCAL(values, provider.Divide(provider.One, L_22), L_start, columnIndices[c + 1]);
                    continue;
                }

                // Calculate L[c + 1: n, 0, c] * L[c, 0: c]' = L_31 * L_21
                k = 0;
                int membership = 2 * c + 2;
                for (int j = top; j < dim; ++j)
                {
                    int col = reach[j],
                        rowIndex = next[col]++, // Incrementation occurs here
                        end = columnIndices[col + 1];

                    T L_cj = values[rowIndex];
                    for (int i = rowIndex + 1; i < end; ++i)
                    {
                        int r = rowIndices[i];

                        // Already in column sum = increment
                        if (in_reach[r] >= membership)
                        {
                            column[r] = provider.Add(column[r], provider.Multiply(values[i], L_cj));
                        }
                        // Not already in column = set and insert index directly
                        // into L
                        else
                        {
                            column[r] = provider.Multiply(values[i], L_cj);
                            column_index[k++] = r; // record position
                            in_reach[r] = membership; // mark as visited in column c
                        }
                    }
                }

                // Subtract A_32 from the temporary column, column now stores
                // L_31 * L_21 - A_32
                for (int i = A_col_diag + 1; i < A_col_end; ++i)
                {
                    int r = A_rowIndices[i];
                    if (in_reach[r] >= membership)
                    {
                        column[r] = provider.Subtract(column[r], A_values[i]);
                    }
                    else
                    {
                        column[r] = provider.Negate(A_values[i]);
                        column_index[k++] = r;
                        in_reach[r] = membership;
                    }
                }

                // Sort the column indices - introduces a log factor - see if we can remove it
                Array.Sort(column_index, 0, k);

                // Gather operation
                int l = L_c_start + 1;
                for (int i = 0; i < k; ++i)
                {
                    values[l++] = column[column_index[i]];
                }

                // Single BLAS-1 divide operation for dividing by L_22
                blas1.SCAL(values, provider.Divide(neg_one, L_22), L_c_start + 1, l);
            }

            return new CSCSparseMatrix<T>(dim, columnIndices, rowIndices, values);
        }

        /// <summary>
        /// Returns the index of the diagonal element in A column c, or throws if the diagonal element is 
        /// structurally zero.
        /// </summary>
        /// <param name="A"></param>
        /// <returns></returns>
        private int CalculateDiagonalIndex(CSCSparseMatrix<T> A, int c)
        {
            int col_start = A.ColumnIndices[c], col_end = A.ColumnIndices[c + 1];
            int index = Array.BinarySearch(A.RowIndices, col_start, col_end - col_start, c);
            if (index < 0)
            {
                throw new InvalidOperationException("Matrix is not positive definite.");
            }
            return index;
        }

    }
}
