﻿using LinearNet.Global;
using LinearNet.Graphs;
using LinearNet.Providers;
using LinearNet.Structs;
using System;
using System.Collections;
using System.Diagnostics;

namespace LinearNet.Matrices.Sparse.Decompositions
{
    /// <summary>
    /// Implements a left-looking supernodal Cholesky decomposition algorithm
    /// </summary>
    /// <typeparam name="T"></typeparam>
    internal class CSCCholeskyLeftSuper<T> where T : new()
    {
        private readonly int 
            // The dimension of the matrix to decompose
            dim,
            // The minimum width of supernodes - under this we default to standard Chol-left
            min_width;
        private readonly int[] columnIndices, rowIndices, A_columnIndices, A_rowIndices;
        private readonly T[] values, A_values;
        private readonly EliminationTree etree;
        private readonly IProvider<T> provider;
        private readonly IDenseBLAS1<T> blas;
        private readonly T zero, neg_one;
        private readonly CSCSparseMatrix<T> A;

        #region Workspaces
        // stores the boundaries of the supernodes 
        private readonly int[] supernode_indices;

        // The lower triangular workspace for solving L_22
        private readonly T[][] workspace1;

        // The workspace for solving L_32, storing a matrix in column-major order
        // For the single column case, this also stores the temporary column, so 
        // the dimension is max(dim, L_32 size)
        private readonly T[] workspace2;
        // If workspace2_index[r] = i then workspace2[i + c * height] represents L[r, c]
        // Also used to store the column indexes in the single case
        private readonly int[] workspace2_index;

        // A workspace of size dim
        private readonly Workspace<T> column_ws;

        // A workspace of size of the largest supernode
        private readonly T[] supernode_ws, dim_ws;

        // Workspace for keeping track of the index of the next non-zero entry
        // in each column. Used to keep track of the boundary between L_11 and L_21 
        private readonly int[] next;

        // Workspace for keeping track of the first term inside L_31 in each column
        // Used to keep track of the boundary between L_21 and L_31
        private readonly int[] floor;
        // Workspace for keeping track of the location of the next non-zero index in each column,
        // under a temporary row during the calculation of L_21 * L_21'. This works alongside 
        // floor to form the dot products
        private readonly int[] floor2;
        // Workspace for storing the indices that were changed going from floor to floor2
        private readonly int[] floor_changed_indices;

        // Workspace for calculating the reach of each row - two are required because of the 
        // L_21 * L_21' calculation which requires co-iteration by row
        private readonly int[] reach, reach2;
        // Workspace for determining whether each index belongs to the reach
        private readonly int[] in_reach, in_reach2;

        // The membership threshold for both in_reach workspaces
        private int membership_threshold;
        #endregion

        internal CSCCholeskyLeftSuper(CSCSparseMatrix<T> A, SymbolicCholesky analysis, IDenseBLAS1<T> _blas, int minWidth = 5)
        {
            if (A is null)
            {
                throw new ArgumentNullException(nameof(A));
            }
            if (!A.IsSquare)
            {
                throw new InvalidOperationException(ExceptionMessages.MatrixNotSquare);
            }
            if (_blas is null)
            {
                throw new ArgumentNullException(nameof(_blas));
            }

            if (analysis is null || analysis.ElimTree is null || analysis.ColumnIndices is null)
            {
                analysis = A.CholeskyAnalysis();
            }

            dim = A.Rows;
            min_width = minWidth;

            int nnz = analysis.NumNonZeroElements;
            columnIndices = analysis.ColumnIndices.Copy();
            rowIndices = new int[nnz];
            values = new T[nnz];

            this.A = A;
            A_columnIndices = A.ColumnIndices;
            A_rowIndices = A.RowIndices;
            A_values = A.Values;

            etree = analysis.ElimTree;
            provider = _blas.Provider;
            blas = _blas;
            zero = provider.Zero;
            neg_one = provider.Negate(provider.One);

            // Determine the size of workspace required to hold the dense matrix A_32 - L_31 * L_21'
            // which will be the 'B' matrix in the dense triangular solve L_32 * L_22' = B.
            // This requires an iteration through the structure of L, stored in size2.
            // Also use this loop to determine the required workspace size to compute L_22 (the dense
            // Cholesky factor) which will be a square matrix of dimension equal to the width of the 
            // largest supernode (stored in size1)
            int size1 = 0, size2 = 0, nSupernodes = 0;
            for (int c = 0; c < dim; ++c)
            {
                int nnz_col = columnIndices[c + 1] - columnIndices[c];

                int j = c;
                while (etree.Parents[j] == j + 1 && nnz_col == columnIndices[j + 2] - columnIndices[j + 1] + 1)
                {
                    --nnz_col;
                    ++j;
                }

                // [c, j] forms a supernode with nnz_col common columns
                int width = j - c + 1, s;
                checked
                {
                    s = width * nnz_col;
                }
                if (width > size1)
                {
                    size1 = width;
                }
                if (s > size2)
                {
                    size2 = s;
                }

                // Prevent re-iteration
                c = j;
                ++nSupernodes;
            }

            // re-traverse to find the supernode boundaries
            supernode_indices = new int[nSupernodes + 1];
            for (int c = 0, k = 1; c < dim; ++c)
            {
                int nnz_col = columnIndices[c + 1] - columnIndices[c], j = c;
                while (etree.Parents[j] == j + 1 && nnz_col == columnIndices[j + 2] - columnIndices[j + 1] + 1)
                {
                    --nnz_col;
                    ++j;
                }
                supernode_indices[k++] = j + 1;
                // Prevent re-iteration
                c = j;
            }

            workspace1 = InitializeLowerTriangularWorkspace(size1);
            workspace2 = new T[Math.Max(dim, size2)];
            workspace2_index = new int[dim];
            column_ws = new Workspace<T>(dim, provider);
            supernode_ws = new T[size1];
            dim_ws = new T[dim];

            next = new int[dim];
            Array.Copy(columnIndices, next, dim);

            floor = new int[dim];
            floor2 = new int[dim];
            Array.Copy(columnIndices, floor, dim);
            Array.Copy(columnIndices, floor2, dim);
            floor_changed_indices = new int[dim];

            reach = new int[dim];
            reach2 = new int[dim];
            in_reach = new int[dim];
            in_reach2 = new int[dim];
            membership_threshold = 1;
        }

        internal CSCSparseMatrix<T> Decompose()
        {
            // Since this is a left-looking Cholesky, we need to precompute the entire non-zero
            // pattern of L in |L| time. This code we borrowed from the CholLeft implementation
            // The array next is borrowed to keep track of where each element should be placed
            CalculateFactorNonZeroPattern();
            
            // Iterate column-wise
            int nodeCount = supernode_indices.Length - 1;
            for (int i = 0; i < nodeCount; ++i)
            {
                int c = supernode_indices[i], 
                    j = supernode_indices[i + 1] - 1,
                    nnz_col = columnIndices[c + 1] - columnIndices[c] - (j - c);

                // The width of the current supernode
                int width = j - c + 1;
                if (width < min_width)
                {
                    for (int k = c; k <= j; ++k)
                    {
                        FactorSingle(k);
                    }
                }
                else
                {
                    FactorBlock(ref c, j, nnz_col);
                }
            }
            return new CSCSparseMatrix<T>(dim, columnIndices, rowIndices, values);
        }

        /// <summary>
        /// Calculates the non-zero pattern of L in |L| time, storing in rowIndices.
        /// </summary>
        private void CalculateFactorNonZeroPattern()
        {
            for (int r = 0; r < dim; ++r)
            {
                // Calculate the reach of L[r, 0 : r - 1] - for the non-zero pattern of row r
                // excluding the diagonal
                int top = etree.Reach(A, r, reach, in_reach, membership_threshold++);
                for (int i = top; i < dim; ++i)
                {
                    int c = reach[i];
                    rowIndices[next[c]++] = r;
                }

                // Insert the diagonal term
                rowIndices[next[r]++] = r;
            }

            // Reset the required arrays
            Array.Copy(columnIndices, next, dim);
        }


        /// <summary>
        /// Factor a single column c, storing the result in L storage
        /// </summary>
        /// <returns></returns>
        private void FactorSingle(int c)
        {
            int A_col_diag = CalculateDiagonalIndex(A, c),
                A_col_end = A_columnIndices[c + 1], k;

            // L_22 = A_22 - L_21' * L_21

            // Set to A_22
            T sq = A_values[A_col_diag];

            // Calculate the reach of L[c, 0 : c - 1]
            int top = etree.Reach(A, c, reach, in_reach, membership_threshold++);
            CheckMembershipThresholdOverflow(in_reach, ref membership_threshold);
            for (int ci = top; ci < dim; ++ci)
            {
                int col = reach[ci];
                if (col >= c) continue;
                T e = values[next[col]]; // Save the incrementation for later
                sq = provider.Subtract(sq, provider.Multiply(e, e));
            }

            // Check for positive definite-ness
            if (provider.IsRealAndNegative(sq))
            {
                throw new InvalidOperationException("Matrix is not positive definite.");
            }

            // Insert L_22 into L storage
            int L_c_start = columnIndices[c];
            T L_22 = provider.Sqrt(sq);
            values[L_c_start] = L_22;

            // incrementation for this column (down from the diagonal)
            next[c]++;
            // Maintain loop invariants
            floor[c]++;
            floor2[c]++;

            // Special case for first column - no need to calculate L_31 * L_21
            if (c == 0)
            {
                int start = A_col_diag + 1,
                    L_start = L_c_start + 1;

                Array.Copy(A.Values, start, values, L_start, A_col_end - start);
                blas.SCAL(values, provider.Divide(provider.One, L_22), L_start, columnIndices[c + 1]);
                return;
            }

            // Calculate L[c + 1: n, 0, c] * L[c, 0: c]' = L_31 * L_21
            k = 0;
            int membership = membership_threshold++;
            for (int w = top; w < dim; ++w)
            {
                int col = reach[w],
                    rowIndex = next[col]++, // Incrementation occurs here
                    end = columnIndices[col + 1];

                // Maintain loop invariants
                floor[col]++;
                floor2[col]++;

                T L_cj = values[rowIndex];
                for (int i = rowIndex + 1; i < end; ++i)
                {
                    int r = rowIndices[i];

                    // Already in column sum = increment
                    if (in_reach[r] >= membership)
                    {
                        workspace2[r] = provider.Add(workspace2[r], provider.Multiply(values[i], L_cj));
                    }
                    // Not already in column = set and insert index directly
                    // into L
                    else
                    {
                        workspace2[r] = provider.Multiply(values[i], L_cj);
                        workspace2_index[k++] = r; // record position
                        in_reach[r] = membership; // mark as visited in column c
                    }
                }
            }

            // Check for overflow
            CheckMembershipThresholdOverflow(in_reach, ref membership_threshold);

            // Subtract A_32 from the temporary column, column now stores
            // L_31 * L_21 - A_32
            for (int i = A_col_diag + 1; i < A_col_end; ++i)
            {
                int r = A_rowIndices[i];
                if (in_reach[r] >= membership)
                {
                    workspace2[r] = provider.Subtract(workspace2[r], A_values[i]);
                }
                else
                {
                    workspace2[r] = provider.Negate(A_values[i]);
                    workspace2_index[k++] = r;
                    in_reach[r] = membership;
                }
            }

            // Sort the column indices - introduces a log factor - see if we can remove it
            Array.Sort(workspace2_index, 0, k);

            // Gather operation
            int l = L_c_start + 1;
            for (int i = 0; i < k; ++i)
            {
                values[l++] = workspace2[workspace2_index[i]];
            }

            // Single BLAS-1 divide operation for dividing by L_22
            blas.SCAL(values, provider.Divide(neg_one, L_22), L_c_start + 1, l);
        }

        /// <summary>
        /// Factor a block of columns [c, j], given the number of non-zeros in the column j
        /// </summary>
        private void FactorBlock(ref int c, int j, int nnz_col)
        {
            int width = j - c + 1;

            // Clear the (lower) workspace prior to loading with A_22
            ClearLowerTriangularWorkspace(workspace1, width, zero);

            // Iterate over the columns of the supernode, extracting 
            // A_22 and inserting into workspace 1 (lower)
            int A_22_lower_bound = c + width; // the row index of the lower boundary of A_22 block
            for (int sc = c; sc <= j; ++sc)
            {
                // The column within the workspace corresponding to sc
                int ws_col = sc - c;

                // Assuming lower-diagonal or full structure, start at the diagonal and iterate down
                int A_col_start = CalculateDiagonalIndex(A, sc), A_col_end = A_columnIndices[sc + 1];
                for (int si = A_col_start; si < A_col_end; ++si)
                {
                    // The row index
                    int sr = A_rowIndices[si];
                    if (sr >= A_22_lower_bound) break;

                    workspace1[sr - c][ws_col] = A_values[si];
                }
            }

            // Subtract L_21 * L_21' from workspace1 to form A_22 - L_21 * L_21' (= L_22 * L_22')
            // The top boundary of L_21's columns are stored in next, update them so they point to
            // the first nz entry under L_21

            for (int lr = c; lr <= j; ++lr)
            {
                // Check for overflow before calculating the reach, since handling 
                // it afterwards will clear the in_reach array which is needed in 
                // the inner-loop
                CheckMembershipThresholdOverflow(in_reach, ref membership_threshold);

                // Store the membership threshold prior to the inner loop
                int t1 = membership_threshold;

                int top = etree.Reach(A, lr, reach, in_reach, membership_threshold++);

                // The workspace row
                int wr = lr - c;

                // The floor changes are treated like a stack, with indices allocated 
                // from 0 ... ceiling. by setting ceiling to 0, we are effectively 
                // clearing the stack of floor changes
                int ceiling = 0;

                // Iterate over a upper-triangular form
                for (int lc = lr; lc <= j; ++lc)
                {
                    int wc = lc - c;

                    int top2 = etree.Reach(A, lc, reach2, in_reach2, membership_threshold++);
                    CheckMembershipThresholdOverflow(in_reach2, ref membership_threshold);

                    // Perform the dot product, using floor and floor2 as the arrays holding the 
                    // indices of elements we are multiplying. 
                    // Update floor2 as we traverse, and insert the updated index into the floor_changed_index
                    // array, and update the ceiling of that stack
                    T dot = zero;
                    for (int k = top2; k < dim; ++k)
                    {
                        int col = reach2[k];
                        if (col >= c) continue;
                        if (in_reach[col] >= t1)
                        {
                            // Insert change into floor_changed_index, if not already changed
                            if (floor[col] == floor2[col])
                            {
                                floor_changed_indices[ceiling++] = col;
                            }

                            // Increment floor2[col] for the next iteration of this inner loop, add to dot product
                            dot = provider.Add(dot, provider.Multiply(values[floor[col]], values[floor2[col]++]));
                        }
                    }

                    // Insert in transposed fashion
                    workspace1[wc][wr] = provider.Subtract(workspace1[wc][wr], dot);
                }

                // Increment floor to move to the next row 
                for (int i = top; i < dim; ++i)
                {
                    int col = reach[i];
                    if (col >= c) continue;

                    floor[col]++;
                    floor2[col]++; // should be redundant if floor_changed_indices is implemented properly
                }

                // Reset floor2 to floor for next iteration
                for (int i = 0; i < ceiling; ++i)
                {
                    int col = floor_changed_indices[i];
                    floor2[col] = floor[col];
                }
            }

            // Perform a dense Cholesky factorization of workspace1, which now holds A_22 - L_21 * L_21'
            // Also check for positive definite-ness 
            if (!DenseCholeskyInPlace(workspace1, width, blas))
            {
                throw new InvalidOperationException("Matrix is not positive definite.");
            }

            // Store the dense Cholesky factor from workspace1 to L, updating next[c]... next[j]
            for (int sc = c; sc <= j; ++sc)
            {
                // workspace column index
                int wc = sc - c;
                int k = next[sc];
                // Iterate down - cringe
                for (int wr = wc; wr < width; ++wr)
                {
                    values[k++] = workspace1[wr][wc];
                }

                // next[c], ... next[j] is now in L_32 (note that L_31 starting indices is in floor)
                next[sc] = k;

                // Maintain the loop invariant
                floor[sc] = k;
                floor2[sc] = k;
            }

            // Compute A_32 - L_31 * L_21' (width x height, in row-major order)
            int height = nnz_col - 1;

            // Calculate the indices for prior to loading A_32 into workspace2
            // Since the nz row indices are identical for columns c ... j, we just use c to determine 
            // the non-zero pattern of the entire submatrix
            int ri_end = columnIndices[c + 1];
            for (int ri = next[c], k = 0; ri < ri_end; ++ri)
            {
                // row 'rowIndices[ri]' will be stored in location k in the workspace
                workspace2_index[rowIndices[ri]] = k++;
            }

            // Load A_32 into workspace2 - this requires A to have a explicitly stored lower half...
            Clear(workspace2, height * width, zero);
            for (int sc = c; sc <= j; ++sc)
            {
                // Iterate down column sc within matrix A
                int col_end = A_columnIndices[sc + 1], offset = (sc - c) * height;
                for (int i = A_columnIndices[sc]; i < col_end; ++i)
                {
                    // Skip over the lower triangular portion (already handled by the dense Cholesky
                    // decomposition)
                    if (A_rowIndices[i] <= j) continue;

                    // We are going to trust that workspace2_index is properly initialized, and this 
                    // is going to point to a valid index within workspace2
                    int r = workspace2_index[A_rowIndices[i]];

                    // Insert in column-major order
                    workspace2[r + offset] = A_values[i];
                }
            }

            // Subtract L_31 * L_21' from workspace2, which will hold A_32 - L_31 * L_21' afterwards
            // For each row c ... j
            Subtract_L31L21_from_A(c, j, height);
            //Subtract_L31L21_from_A1(c, j, height);
            //workspace2.Print();

            // Solve the triangular system L_22 * L_32' = (A_32 - L_31 * L_21)'
            // with L_22 stored in workspace1, and RHS stored in workspace2
            // workspace2 is overwritten with the solution L_32'
            if (!DenseLTSolveInPlace(workspace1, workspace2, width, height, blas))
            {
                throw new InvalidOperationException("Division by zero encountered in the Cholesky decomposition, unable to proceed.");
            }

            // There is no need for reverse lookup table for the row indices of L_32, since
            // the row indices are already precomputed! Beautiful!
            for (int lc = c; lc <= j; ++lc)
            {
                // the workspace column
                int wc = lc - c;

                // Sequential copy directly into values 
                Array.Copy(workspace2, wc * height, values, next[lc], height);
            }

            // Remember to reset c
            c = j;
        }

        private void Subtract_L31L21_from_A(int c, int j, int height)
        {
            // Subtract L_31 * L_21' from workspace2, which will hold A_32 - L_31 * L_21' afterwards
            // For each row c ... j
            for (int r = c; r <= j; ++r)
            {
                // Calculate the non-zero pattern for row r
                int top = etree.Reach(A, r, reach, in_reach, membership_threshold++);
                CheckMembershipThresholdOverflow(in_reach, ref membership_threshold);

                column_ws.Clear();

                // The column index inside workspace2
                int wc = r - c, offset = wc * height;

                // For each non-zero in row r, multiply down starting at floor[col], and perform
                // a set union to store in column
                for (int i = top; i < dim; ++i)
                {
                    int col = reach[i];
                    if (col >= c) continue;

                    // Get the value of the element at L[r, col]. Also serves as the place where 
                    // increment the next array, for future iterations
                    int end = columnIndices[col + 1];

                    //Debug.WriteLine($"{r}\t[{reach[i]}]\t{col}\t{floor[col]}\t{end}\t{values[next[col]]}");
                    column_ws.AXPY(rowIndices, values, floor[col], end, values[next[col]++]);
                }

                // subtract from workspace2 once
                for (int i = 0; i < column_ws._count; ++i)
                {
                    int col_index = column_ws._nzIndices[i];
                    int index = workspace2_index[col_index] + offset;
                    workspace2[index] = provider.Subtract(workspace2[index], column_ws._workspace[col_index]);
                }
            }
        }
        private void Subtract_L31L21_from_A1(int c, int j, int height)
        {
            // Subtract L_31 * L_21' from workspace2, which will hold A_32 - L_31 * L_21' afterwards
            // For each row c ... j
            for (int r = c; r <= j; ++r)
            {
                // Calculate the non-zero pattern for row r
                int top = etree.Reach(A, r, reach, in_reach, membership_threshold++);
                CheckMembershipThresholdOverflow(in_reach, ref membership_threshold);

                column_ws.Clear();

                // The column index inside workspace2
                int wc = r - c, offset = wc * height;

                // For each non-zero in row r, multiply down starting at floor[col], and perform
                // a set union to store in column
                for (int i = top; i < dim; ++i)
                {
                    int left_col = reach[i], 
                        end = columnIndices[left_col + 1],
                        nnz_col = end - columnIndices[left_col];

                    if (left_col >= c)
                    {
                        continue;
                    }

                    // store column[i] into supernode workspace 1
                    supernode_ws[0] = values[next[left_col]++];

                    // Continue traversing to collect all non-zeroes in the supernode
                    int k = i + 1;
                    for (; k < dim; ++k)
                    {
                        int col = reach[k];
                        if (col >= c) break;

                        if (nnz_col != columnIndices[col + 1] - columnIndices[col] + 1)
                        {
                            --nnz_col;
                        }

                        // store into supernode workspace 1
                        supernode_ws[k - i] = values[next[col]++];
                    }

                    // Check if we should use block multiply or single multiply
                    if (k - i > min_width)
                    {
                        // Copy first column into workspace
                        int start = floor[left_col], len = end - start;
                        Array.Copy(values, start, dim_ws, 0, len);
                        blas.SCAL(dim_ws, supernode_ws[0], 0, len);

                        // AXPY the rest of the columns
                        for (int ci = i + 1; ci < k; ++ci)
                        {
                            int col = reach[ci];
                            blas.AXPY(dim_ws, values, supernode_ws[ci - i], 0, len, floor[col]);
                        }

                        // Add to the column
                        for (int ri = 0; ri < len; ++ri)
                        {
                            int index = rowIndices[start + ri];
                            if (column_ws._inWorkspace[index])
                            {
                                column_ws._workspace[index] = provider.Add(column_ws._workspace[index], dim_ws[ri]);
                            }
                            else
                            {
                                column_ws._workspace[index] = dim_ws[ri];
                                column_ws._inWorkspace[index] = true;
                                column_ws._nzIndices[column_ws._count++] = index;
                            }
                        }
                    }
                    else
                    {
                        for (int ci = i; ci < k; ++ci)
                        {
                            int col = reach[ci];
                            column_ws.AXPY(rowIndices, values, floor[col], columnIndices[col + 1], supernode_ws[ci - i]);
                        }
                    }
                    i = k - 1;
                }

                // subtract from workspace2 once
                for (int i = 0; i < column_ws._count; ++i)
                {
                    int col_index = column_ws._nzIndices[i];
                    int index = workspace2_index[col_index] + offset;
                    workspace2[index] = provider.Subtract(workspace2[index], column_ws._workspace[col_index]);
                }
            }
        }

        /// <summary>
        /// Returns the index of the diagonal element in A column c, or throws if the diagonal element is 
        /// structurally zero.
        /// </summary>
        /// <param name="A"></param>
        /// <returns></returns>
        private int CalculateDiagonalIndex(CSCSparseMatrix<T> A, int c)
        {
            int col_start = A.ColumnIndices[c], col_end = A.ColumnIndices[c + 1];
            int index = Array.BinarySearch(A.RowIndices, col_start, col_end - col_start, c);
            if (index < 0)
            {
                throw new InvalidOperationException("Matrix is not positive definite.");
            }
            return index;
        }

        /// <summary>
        /// Create a lower-triangular matrix of dimension dim x dim
        /// </summary>
        private static T[][] InitializeLowerTriangularWorkspace(int dim)
        {
            T[][] L = new T[dim][];
            for (int r = 0; r < dim; ++r)
            {
                L[r] = new T[r + 1];
            }
            return L;
        }

        private static void ClearLowerTriangularWorkspace(T[][] L, int dim, T zero)
        {
            if (zero.Equals(default))
            {
                for (int i = 0; i < dim; ++i)
                {
                    Array.Clear(L[i], 0, i + 1);
                }
            }
            else
            {
                for (int i = 0; i < dim; ++i)
                {
                    T[] row = L[i];
                    for (int j = 0; j <= i; ++j)
                    {
                        row[j] = zero;
                    }
                }
            }
        }

        private static void Clear(T[] workspace, int dim, T zero)
        {
            if (zero.Equals(default))
            {
                Array.Clear(workspace, 0, dim);
            }
            else
            {
                for (int i = 0; i < dim; ++i)
                {
                    workspace[i] = zero;
                }
            }
        }

        private static void CheckMembershipThresholdOverflow(int[] in_set, ref int threshold)
        {
            if (threshold == int.MaxValue)
            {
                Debug.WriteLine("Clearing membership threshold");
                Array.Clear(in_set, 0, in_set.Length);
                threshold = 1;
            }
        }

        /// <summary>
        /// For A dense lower triangular, finds the dense lower triangular Cholesky factor L 
        /// </summary>
        private static bool DenseCholeskyInPlace(T[][] A, int dim, IDenseBLAS1<T> blas)
        {
            int r, c;

            IProvider<T> provider = blas.Provider;
            T zero = provider.Zero;

            for (r = 0; r < dim; ++r)
            {
                T[] A_r = A[r];
                T s = zero;
                for (c = 0; c < r; c++)
                {
                    // Off-diagonal term in row r, col c where c < r is 
                    // (A(r, c) - sum(L(r, i) * L(c, i), i in [0, c))) / L(c, c)

                    T[] L_c = A[c];
                    T L_rc = blas.DOT(A_r, L_c, 0, c);
                    L_rc = provider.Divide(provider.Subtract(A_r[c], L_rc), L_c[c]);
                    A_r[c] = L_rc;
                    s = provider.Add(s, provider.Multiply(L_rc, L_rc));
                }

                // Diagonal term is L(r, r) = sqrt(A(r, r) - sum(L(r, i)^2, i in [0, r))
                T sq = provider.Subtract(A_r[r], s);
                if (provider.IsRealAndNegative(sq))
                {
                    Debug.WriteLine(sq + "\t" + A_r[r] + "\t" + s + "\t" + dim);
                    for (int i = 0; i < dim; ++i)
                    {
                        for (int j = 0; j <= i; ++j)
                        {
                            Debug.Write(A[i][j] + "\t");
                        }
                        Debug.WriteLine("");
                    }

                    return false;
                }
                A_r[r] = provider.Sqrt(sq);
            }
            return true;
        }

        /// <summary>
        /// Solve the triangular system L'X = B, storing the result X in B.
        /// B is a matrix (width x height) stored in row-major order as a single vector.
        /// </summary>
        private static bool DenseLTSolveInPlace(T[][] L, T[] B, int dim, int m, IDenseBLAS1<T> blas)
        {
            IProvider<T> provider = blas.Provider;
            T zero = provider.Zero, one = provider.One;

            for (int r = 0; r < dim; ++r)
            {
                T[] row = L[r];
                int Xr_start = r * m, Xr_end = Xr_start + m;

                // X[r] -= L[r][c] * X[c]
                for (int c = 0; c < r; ++c)
                {
                    int Xc_start = c * m;
                    blas.AXPY(B, B, provider.Negate(row[c]), Xr_start, Xr_end, Xc_start - Xr_start);
                }

                T diag = row[r];
                if (provider.Equals(diag, zero))
                {
                    return false;
                }

                // X[r] /= diag
                // X[r] is stored in B[r * width, (r + 1) * width)
                blas.SCAL(B, provider.Divide(one, diag), Xr_start, Xr_end);
            }
            return true;
        }
    }

    internal class Workspace<T>
    {
        private readonly IProvider<T> _provider;
        internal readonly T[] _workspace;
        internal readonly int[] _nzIndices;
        internal readonly BitArray _inWorkspace;
        internal int _count;

        internal Workspace(int dimension, IProvider<T> provider)
        {
            _workspace = new T[dimension];
            _nzIndices = new int[dimension];
            _inWorkspace = new BitArray(dimension);
            _count = 0;
            _provider = provider;
        }

        internal void Clear()
        {
            for (int i = 0; i < _count; ++i)
            {
                _inWorkspace[_nzIndices[i]] = false;
            }
            _count = 0;
        }

        /// <summary>
        /// this <- this + alpha * v, with v a sparse vector 
        /// defined implicitly using 
        /// </summary>
        /// <param name="v_nzIndices"></param>
        /// <param name="v_values"></param>
        /// <param name="v_start"></param>
        /// <param name="v_end"></param>
        /// <param name="alpha"></param>
        internal void AXPY(int[] v_nzIndices, T[] v_values, int v_start, int v_end, T alpha)
        {
            for (int i = v_start; i < v_end; ++i)
            {
                int index = v_nzIndices[i];
                if (_inWorkspace[index])
                {
                    _workspace[index] = _provider.Add(_workspace[index], _provider.Multiply(alpha, v_values[i]));
                }
                else
                {
                    _workspace[index] = _provider.Multiply(alpha, v_values[i]);
                    _inWorkspace[index] = true;
                    _nzIndices[_count++] = index;
                }
            }
        }
    }
}
