﻿using LinearNet.Graphs;
using LinearNet.Matrices.Sparse.Symbolic;
using LinearNet.Providers;
using LinearNet.Structs;
using System;

namespace LinearNet.Matrices.Sparse.Decompositions
{
    internal class CSRCholesky<T> where T : new()
    {
        public SymbolicCholesky Analyse(CSRSparseMatrix<T> matrix)
        {
            EliminationTree tree = EliminationTree.Calculate(matrix);

            int[] post = new int[matrix.Rows], levels = new int[matrix.Rows], first = new int[matrix.Rows];
            tree.CalculatePostOrdering(post, levels);
            tree.CalculateFirstDescendant(post, first);

            // Calculate the row counts
            int[] rowIndices = CSCSymbolic<T>.CalculateRowIndices(matrix.RowIndices, matrix.ColumnIndices, tree, post, levels, first);
            return new SymbolicCholesky(tree, rowIndices, false);
        }

        /// <summary>
        /// Due to asymptotics, this method is 40 times less performant as the equivalent method for CSC matrices for some matrices
        /// </summary>
        /// <param name="matrix"></param>
        /// <param name="analysis"></param>
        /// <param name="provider"></param>
        /// <returns></returns>
        internal CSRSparseMatrix<T> CholLeft(CSRSparseMatrix<T> matrix, SymbolicCholesky analysis, IProvider<T> provider)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            if (analysis is null)
            {
                throw new ArgumentNullException(nameof(analysis));
            }

            // Set up Cholesky factor workspaces
            int[] rowIndices = analysis.RowIndices.Copy();
            int dim = matrix.Rows, len = rowIndices[dim];
            int[] columnIndices = new int[len];
            T[] values = new T[len];

            // The next column index in each row
            int[] next = new int[dim];
            Array.Copy(rowIndices, next, dim);

            for (int c = 0; c < dim; ++c)
            {
                // L_22 = sqrt(A_22 - L_21^T . L_21)
                T sq = matrix[c, c];
                int row_start = rowIndices[c], row_end = next[c];
                for (int i = row_start; i < row_end; ++i)
                {
                    int col = columnIndices[i];
                    if (col >= c) break;

                    T e = values[i];
                    sq = provider.Subtract(sq, provider.Multiply(e, e));
                }

                if (provider.IsRealAndNegative(sq))
                {
                    throw new InvalidOperationException("Matrix is not positive definite.");
                }

                // Save L_22 term
                int index = next[c]++;
                columnIndices[index] = c;
                T L_22 = provider.Sqrt(sq);
                values[index] = L_22;

                // L_32 = (A_32 - L_31 * L_21) / L_22
                // find the start of A_32^T (A[c, c:n])
                int j = matrix.RowIndices[c], A_32_end = matrix.RowIndices[c + 1];
                while (j < A_32_end && matrix.ColumnIndices[j] <= c) ++j;

                // calculate L_31 * L_21, co-iterating with A_32
                for (int r = c + 1; r < dim; ++r)
                {
                    // (L_31 * L_21)[r] is a dot product
                    T dot = provider.Zero;
                    bool any = false;
                    int i1 = rowIndices[r], end = rowIndices[r + 1],
                        i2 = row_start;

                    while (i1 < end && i2 < row_end)
                    {
                        int c1 = i1 < end ? columnIndices[i1] : int.MaxValue,
                            c2 = i2 < row_end ? columnIndices[i2] : int.MaxValue;

                        if (c1 < c2)
                        {
                            ++i1;
                        }
                        else if (c1 > c2)
                        {
                            ++i2;
                        }
                        else
                        {
                            dot = provider.Subtract(dot, provider.Multiply(values[i1], values[i2]));
                            any = true;
                            ++i1;
                            ++i2;
                        }
                    }

                    if (j < A_32_end && matrix.ColumnIndices[j] == r)
                    {
                        dot = provider.Add(dot, matrix.Values[j]);
                        any = true;
                        ++j;
                    }

                    if (any)
                    {
                        int ind = next[r]++;
                        columnIndices[ind] = c;
                        values[ind] = provider.Divide(dot, L_22);
                    }
                }
            }

            return new CSRSparseMatrix<T>(dim, rowIndices, columnIndices, values);
        }
    }
}
