﻿using LinearNet.Providers;
using LinearNet.Structs;
using System;

namespace LinearNet.Matrices
{
    /// <summary>
    /// Object storing the Cholesky decomposition of a matrix 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class Cholesky<T> where T : new()
    {
        private readonly IDenseBLAS1<T> _blas;
        private readonly IProvider<T> _provider;
        private readonly Matrix<T> _L;

        internal IDenseBLAS1<T> BLAS1;

        /// <summary>
        /// The lower-triangular factor $L$.
        /// </summary>
        public Matrix<T> Factor { get { return _L; } }

        internal Cholesky(Matrix<T> factor, IDenseBLAS1<T> blas)
        {
            _L = factor;
            _blas = blas;
            _provider = _blas.Provider;
        }

        /// <summary>
        /// Calculate the determinant of the original matrix $A$ using its Cholesky decomposition.
        /// </summary>
        /// <returns>The matrix determinant.</returns>
        public T Determinant()
        {
            T product = _provider.Product(_L.LeadingDiagonal());
            return _provider.Multiply(product, product);
        }

        /// <summary>
        /// Solve the system $Ax = b$ (with dense $b, x$) using this Cholesky factorization of $A$.
        /// </summary>
        /// <param name="b">The right-hand side vector.</param>
        /// <param name="x">The solution vector.</param>
        public void Solve(T[] b, T[] x)
        {
            if (b is null)
            {
                throw new ArgumentNullException(nameof(b));
            }
            if (x is null)
            {
                throw new ArgumentNullException(nameof(x));
            }

            int dim = _L.Rows;
            if (b.Length < dim)
            {
                throw new ArgumentOutOfRangeException(nameof(b));
            }
            if (x.Length < dim)
            {
                throw new ArgumentOutOfRangeException(nameof(x));
            }

            T[] y = new T[dim];
            _L.SolveTriangular(b, y, _blas, true, false);
            _L.SolveTriangular(y, x, _blas, true, true);
        }

        /// <summary>
        /// Solve the system $Ax = b$ using this Cholesky decomposition of $A$, and return the solution vector $x$
        /// as an array.
        /// </summary>
        /// <param name="b">The dense right-hand-side vector, $b$.</param>
        /// <returns>The solution vector as an array.</returns>
        public T[] Solve(T[] b)
        {
            T[] x = new T[_L.Columns];
            Solve(b, x);
            return x;
        }

        /// <summary>
        /// Solve the square system $Ax = b$ using this Cholesky decomposition of $A$, for dense vectors 
        /// $b$ and $x$. 
        /// </summary>
        /// <param name="b">The dense right-hand-side vector.</param>
        /// <param name="x">The dense solution vector.</param>
        public void Solve(DenseVector<T> b, DenseVector<T> x)
        {
            if (b is null)
            {
                throw new ArgumentNullException(nameof(b));
            }
            if (x is null)
            {
                throw new ArgumentNullException(nameof(x));
            }

            Solve(b.Values, x.Values);
        }

        /// <summary>
        /// Solve the square system $Ax = b$ using this Cholesky decomposition of $A$, returning the 
        /// dense solution vector as a <txt>DenseVector</txt>.
        /// </summary>
        /// <param name="b">The dense right-hand-side vector.</param>
        /// <returns>The solution vector.</returns>
        public DenseVector<T> Solve(DenseVector<T> b)
        {
            DenseVector<T> x = new DenseVector<T>(_L.Columns);
            Solve(b, x);
            return x;
        }
    }
}
