﻿using LinearNet.Global;
using LinearNet.Providers;
using LinearNet.Structs;
using System;
using System.Collections.Generic;
using System.Text;

namespace LinearNet.Matrices.Cholesky
{
    public sealed class DenseCholesky<T> : IDenseCholeskyAlgorithm<T> where T : new()
    {
        private readonly IDenseBLAS1<T> _blas;

        internal DenseCholesky(IDenseBLAS1<T> blas)
        {
            _blas = blas;
        }

        public Cholesky<T> Decompose(DenseMatrix<T> matrix)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            if (!matrix.IsSquare)
            {
                throw new InvalidOperationException(ExceptionMessages.MatrixNotSquare);
            }

            int dim = matrix.Rows, r, c;

            IProvider<T> provider = _blas.Provider;
            T zero = provider.Zero;
            T[][] A = matrix.Values;

            for (r = 0; r < dim; ++r)
            {
                T[] A_r = A[r];
                T s = zero;
                for (c = 0; c < r; c++)
                {
                    // Off-diagonal term in row r, col c where c < r is 
                    // (A(r, c) - sum(L(r, i) * L(c, i), i in [0, c))) / L(c, c)

                    T[] L_c = A[c];
                    T L_rc = _blas.DOT(A_r, L_c, 0, c);
                    L_rc = provider.Divide(provider.Subtract(A_r[c], L_rc), L_c[c]);

                    A_r[c] = L_rc;
                    s = provider.Add(s, provider.Multiply(L_rc, L_rc));
                }

                // Diagonal term is L(r, r) = sqrt(A(r, r) - sum(L(r, i)^2, i in [0, r))
                T sq = provider.Subtract(A_r[r], s);
                if (provider.IsRealAndNegative(sq))
                {
                    throw new InvalidOperationException("Matrix is not positive definite.");
                }
                A[r][r] = provider.Sqrt(sq);
            }

            return new Cholesky<T>(matrix, _blas);
        }
    }
}
