﻿using LinearNet.Structs;
using System;
using System.Collections.Generic;
using System.Text;

namespace LinearNet.Matrices.Cholesky
{
    public interface IDenseCholeskyAlgorithm<T> where T : new()
    {
        Cholesky<T> Decompose(DenseMatrix<T> matrix);
    }
}
