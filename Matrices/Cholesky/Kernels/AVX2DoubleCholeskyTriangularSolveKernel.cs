﻿#if NETCOREAPP3_0

using System;
using System.Numerics;

namespace LinearNet.Matrices.Cholesky.Kernels
{
    internal class AVX2DoubleCholeskyTriangularSolveKernel : ICSCCholeskyTriangularSolveKernel<double>
    {
        public int MaxColumns => 4;

        internal AVX2DoubleCholeskyTriangularSolveKernel()
        {
            if (Vector<double>.Count != 4)
            {
                throw new NotImplementedException();
            }
        }

        public double Solve(int[] L_columnIndices, int[] L_rowIndices, double[] L_values, double[] x, int k, int c1, int c2, int[] next, double diag)
        {
            // Determine the column indices
            int i1 = L_columnIndices[c1], end1 = next[c1];
            int i2 = L_columnIndices[c2];

            double xc1 = x[c1] / L_values[i1++];

            // Subtract the 1 x 1 triangular block
            // r > c, and r should be = reach[i + 1]
            x[L_rowIndices[i1]] -= L_values[i1] * xc1;
            ++i1;

            // x[c2]'s value is updated by the mini triangular solve... can only be 
            // fetched here
            double xc2 = x[c2] / L_values[i2++];
            x[c1] = 0.0;
            x[c2] = 0.0;

            // Subtract the n x 2 block 
            int last1 = (end1 - i1) / 4 * 4 + i1;
            for (int m1 = i1, m2 = i2; m1 < last1; m2 += 4)
            {
                Vector<double> 
                    l1 = new Vector<double>(L_values, m1),
                    l2 = new Vector<double>(L_values, m2);

                Vector<double> p = Vector.Add(Vector.Multiply(l1, xc1), Vector.Multiply(l2, xc2));

                int xi1 = L_rowIndices[m1++],
                    xi2 = L_rowIndices[m1++],
                    xi3 = L_rowIndices[m1++],
                    xi4 = L_rowIndices[m1++];

                x[xi1] -= p[0];
                x[xi2] -= p[1];
                x[xi3] -= p[2];
                x[xi4] -= p[3];
            }
            int count = last1 - i1;
            for (int m1 = i1 + count, m2 = i2 + count; m1 < end1;)
            {
                // Get the row index from the first column's values
                int r = L_rowIndices[m1];
                x[r] -= (L_values[m1++] * xc1 + L_values[m2++] * xc2);
            }

            diag -= (xc1 * xc1 + xc2 * xc2);

            // Insert into column c of L matrix
            next[c1]++;
            L_rowIndices[end1] = k;
            L_values[end1] = xc1;

            int end2 = next[c2]++;
            L_rowIndices[end2] = k;
            L_values[end2] = xc2;

            return diag;
        }

        public double Solve(int[] L_columnIndices, int[] L_rowIndices, double[] L_values, double[] x, int k, int c1, int c2, int c3, int[] next, double diag)
        {
            // Determine the column indices
            int end1 = next[c1],
                i1 = L_columnIndices[c1],
                i2 = L_columnIndices[c2],
                i3 = L_columnIndices[c3];

            double xc1 = x[c1] / L_values[i1++];
            x[L_rowIndices[i1]] -= L_values[i1] * xc1;
            ++i1;
            x[L_rowIndices[i1]] -= L_values[i1] * xc1;
            ++i1;

            // x[c2]'s value is updated by the mini triangular solve... can only be 
            // fetched here
            double xc2 = x[c2] / L_values[i2++];
            x[L_rowIndices[i2]] -= L_values[i2] * xc2;
            ++i2;

            double xc3 = x[c3] / L_values[i3++];

            x[c1] = 0.0;
            x[c2] = 0.0;
            x[c3] = 0.0;

            // Subtract the n x 2 block 
            for (int m1 = i1, m2 = i2, m3 = i3; m1 < end1; ++m1, ++m2, ++m3)
            {
                // Get the row index from the first column's values
                x[L_rowIndices[m1]] -= (L_values[m1] * xc1 + L_values[m2] * xc2 + L_values[m3] * xc3);
            }

            diag -= (xc1 * xc1 + xc2 * xc2 + xc3 * xc3);

            // Insert into column c of L matrix
            next[c1]++;
            L_rowIndices[end1] = k;
            L_values[end1] = xc1;

            int end2 = next[c2]++;
            L_rowIndices[end2] = k;
            L_values[end2] = xc2;

            int end3 = next[c3]++;
            L_rowIndices[end3] = k;
            L_values[end3] = xc3;

            return diag;
        }

        public double Solve(int[] L_columnIndices, int[] L_rowIndices, double[] L_values, double[] x, int k, int c1, int c2, int c3, int c4, int[] next, double diag)
        {
            // Determine the column indices
            int end1 = next[c1],
                i1 = L_columnIndices[c1],
                i2 = L_columnIndices[c2],
                i3 = L_columnIndices[c3],
                i4 = L_columnIndices[c4];

            // x[c1]
            double xc1 = x[c1];
            x[c1] = 0.0;
            xc1 /= L_values[i1++];
            x[L_rowIndices[i1]] -= L_values[i1] * xc1; ++i1;
            x[L_rowIndices[i1]] -= L_values[i1] * xc1; ++i1;
            x[L_rowIndices[i1]] -= L_values[i1] * xc1; ++i1;

            // x[c2]
            double xc2 = x[c2];
            x[c2] = 0.0;
            xc2 /= L_values[i2++];
            x[L_rowIndices[i2]] -= L_values[i2] * xc2; ++i2;
            x[L_rowIndices[i2]] -= L_values[i2] * xc2; ++i2;

            // x[c3]
            double xc3 = x[c3];
            x[c3] = 0.0;
            xc3 /= L_values[i3++];
            x[L_rowIndices[i3]] -= L_values[i3] * xc3; ++i3;

            // x[c4]
            double xc4 = x[c4];
            x[c4] = 0.0;
            xc4 /= L_values[i4++];

            // Subtract the n x 4 block in steps of 4
            int last1 = (end1 - i1) / 4 * 4 + i1;
            for (int m1 = i1, m2 = i2, m3 = i3, m4 = i4; m1 < last1; m2 += 4, m3 += 4, m4 += 4)
            {
                Vector<double> 
                    l1 = new Vector<double>(L_values, m1),
                    l2 = new Vector<double>(L_values, m2),
                    l3 = new Vector<double>(L_values, m3),
                    l4 = new Vector<double>(L_values, m4);

                Vector<double> p = Vector.Add(
                    Vector.Add(Vector.Multiply(l1, xc1), Vector.Multiply(l2, xc2)),
                    Vector.Add(Vector.Multiply(l3, xc3), Vector.Multiply(l4, xc4)));

                // Get the row index from the first column's values
                int xi1 = L_rowIndices[m1++],
                    xi2 = L_rowIndices[m1++],
                    xi3 = L_rowIndices[m1++],
                    xi4 = L_rowIndices[m1++];

                x[xi1] -= p[0];
                x[xi2] -= p[1];
                x[xi3] -= p[2];
                x[xi4] -= p[3];
            }
            int count = last1 - i1;
            for (int m1 = i1 + count, m2 = i2 + count, m3 = i3 + count, m4 = i4 + count; m1 < end1;)
            {
                // Get the row index from the first column's values
                int r = L_rowIndices[m1];
                x[r] -= (L_values[m1++] * xc1 + L_values[m2++] * xc2 + L_values[m3++] * xc3 + L_values[m4++] * xc4);
            }

            diag -= (xc1 * xc1 + xc2 * xc2 + xc3 * xc3 + xc4 * xc4);

            // Insert into column c of L matrix
            next[c1]++;
            L_rowIndices[end1] = k;
            L_values[end1] = xc1;

            int end2 = next[c2]++;
            L_rowIndices[end2] = k;
            L_values[end2] = xc2;

            int end3 = next[c3]++;
            L_rowIndices[end3] = k;
            L_values[end3] = xc3;

            int end4 = next[c4]++;
            L_rowIndices[end4] = k;
            L_values[end4] = xc4;

            return diag;
        }

        public double Solve(int[] L_columnIndices, int[] L_rowIndices, double[] L_values, double[] x, int k, int[] columns, int c_start, int c_count, int[] next, double diag)
        {
            throw new NotImplementedException();
        }
    }
}
#endif