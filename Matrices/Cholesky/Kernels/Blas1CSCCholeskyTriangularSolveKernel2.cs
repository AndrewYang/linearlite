﻿using LinearNet.Matrices.Sparse;
using LinearNet.Providers;
using System;

namespace LinearNet.Matrices.Cholesky.Kernels
{
    internal class Blas1CSCCholeskyTriangularSolveKernel2<T> : ICSCCholeskyTriangularSolveKernel<T> where T : new()
    {
        public int MaxColumns => _maxColumns;

        private readonly int _length, _maxColumns;
        private readonly T[] _workspace, _xc, _xr;
        private readonly IDenseBLAS1<T> _blas1;
        private readonly T _zero;
        private readonly int[] _column;

        internal Blas1CSCCholeskyTriangularSolveKernel2(SymbolicCholesky analysis, IDenseBLAS1<T> blas1, int maxColumns = 100)
        {
            _blas1 = blas1;
            _zero = blas1.Provider.Zero;
            _maxColumns = maxColumns;
            _length = analysis.MaxColumnCount;
            _workspace = new T[_length];
            _xc = new T[maxColumns];
            _xr = new T[maxColumns];
            _column = new int[maxColumns];
        }


        public T Solve(int[] L_columnIndices, int[] L_rowIndices, T[] L_values, T[] x, int k, int c1, int c2, int[] next, T diag)
        {
            IProvider<T> _provider = _blas1.Provider;

            // Determine the column indices
            int start1 = L_columnIndices[c1], end1 = next[c1];
            int start2 = L_columnIndices[c2];

            T xc1 = _provider.Divide(x[c1], L_values[start1]);

            // Subtract the 1 x 1 triangular block
            // r > c, and r should be = reach[i + 1]
            int r = L_rowIndices[start1 + 1];
            x[r] = _provider.Subtract(x[r], _provider.Multiply(L_values[start1 + 1], xc1));

            // x[c2]'s value is updated by the mini triangular solve... can only be 
            // fetched here
            T xc2 = _provider.Divide(x[c2], L_values[start2]);
            x[c1] = _zero;
            x[c2] = _zero;

            // Subtract the n x 2 block 
            for (int m1 = start1 + 2, m2 = start2 + 1; m1 < end1; ++m1, ++m2)
            {
                // Get the row index from the first column's values
                r = L_rowIndices[m1];

                T xr = x[r];

                xr = _provider.Subtract(xr, _provider.Multiply(L_values[m1], xc1));
                xr = _provider.Subtract(xr, _provider.Multiply(L_values[m2], xc2));

                x[r] = xr;
            }

            diag = _provider.Subtract(diag, _provider.Multiply(xc1, xc1));
            diag = _provider.Subtract(diag, _provider.Multiply(xc2, xc2));

            // Insert into column c of L matrix
            next[c1]++;
            L_rowIndices[end1] = k;
            L_values[end1] = xc1;

            int end2 = next[c2]++;
            L_rowIndices[end2] = k;
            L_values[end2] = xc2;

            return diag;
        }

        public T Solve(int[] L_columnIndices, int[] L_rowIndices, T[] L_values, T[] x, int k, int c1, int c2, int c3, int[] next, T diag)
        {
            IProvider<T> _provider = _blas1.Provider;

            // Determine the column indices
            int start1 = L_columnIndices[c1], end1 = next[c1],
                start2 = L_columnIndices[c2],
                start3 = L_columnIndices[c3],
                r,
                i1 = start1,
                i2 = start2,
                i3 = start3;

            T xc1 = _provider.Divide(x[c1], L_values[i1++]);
            r = L_rowIndices[i1];
            x[r] = _provider.Subtract(x[r], _provider.Multiply(L_values[i1], xc1));
            ++i1;
            r = L_rowIndices[i1];
            x[r] = _provider.Subtract(x[r], _provider.Multiply(L_values[i1], xc1));
            ++i1;

            // x[c2]'s value is updated by the mini triangular solve... can only be 
            // fetched here
            T xc2 = _provider.Divide(x[c2], L_values[i2++]);
            r = L_rowIndices[i2];
            x[r] = _provider.Subtract(x[r], _provider.Multiply(L_values[i2], xc2));
            ++i2;

            T xc3 = _provider.Divide(x[c3], L_values[i3++]);

            x[c1] = _zero;
            x[c2] = _zero;
            x[c3] = _zero;

            // Subtract the n x 2 block 
            for (int m1 = i1, m2 = i2, m3 = i3; m1 < end1; ++m1, ++m2, ++m3)
            {
                // Get the row index from the first column's values
                r = L_rowIndices[m1];

                T xr = x[r];

                xr = _provider.Subtract(xr, _provider.Multiply(L_values[m1], xc1));
                xr = _provider.Subtract(xr, _provider.Multiply(L_values[m2], xc2));
                xr = _provider.Subtract(xr, _provider.Multiply(L_values[m3], xc3));

                x[r] = xr;
            }

            diag = _provider.Subtract(diag, _provider.Multiply(xc1, xc1));
            diag = _provider.Subtract(diag, _provider.Multiply(xc2, xc2));
            diag = _provider.Subtract(diag, _provider.Multiply(xc3, xc3));

            // Insert into column c of L matrix
            next[c1]++;
            L_rowIndices[end1] = k;
            L_values[end1] = xc1;

            int end2 = next[c2]++;
            L_rowIndices[end2] = k;
            L_values[end2] = xc2;

            int end3 = next[c3]++;
            L_rowIndices[end3] = k;
            L_values[end3] = xc3;

            return diag;
        }

        public T Solve(int[] L_columnIndices, int[] L_rowIndices, T[] L_values, T[] x, int k, int c1, int c2, int c3, int c4, int[] next, T diag)
        {
            IProvider<T> _provider = _blas1.Provider;

            // Determine the column indices
            int end1 = next[c1],
                r,
                i1 = L_columnIndices[c1],
                i2 = L_columnIndices[c2],
                i3 = L_columnIndices[c3],
                i4 = L_columnIndices[c4];

            // x[c1]
            T xc1 = _provider.Divide(x[c1], L_values[i1++]);
            r = L_rowIndices[i1];
            x[r] = _provider.Subtract(x[r], _provider.Multiply(L_values[i1], xc1));
            ++i1;
            r = L_rowIndices[i1];
            x[r] = _provider.Subtract(x[r], _provider.Multiply(L_values[i1], xc1));
            ++i1;
            r = L_rowIndices[i1];
            x[r] = _provider.Subtract(x[r], _provider.Multiply(L_values[i1], xc1));
            ++i1;

            // x[c2]
            T xc2 = _provider.Divide(x[c2], L_values[i2++]);
            r = L_rowIndices[i2];
            x[r] = _provider.Subtract(x[r], _provider.Multiply(L_values[i2], xc2));
            ++i2;
            r = L_rowIndices[i2];
            x[r] = _provider.Subtract(x[r], _provider.Multiply(L_values[i2], xc2));
            ++i2;

            // x[c3]
            T xc3 = _provider.Divide(x[c3], L_values[i3++]);
            r = L_rowIndices[i3];
            x[r] = _provider.Subtract(x[r], _provider.Multiply(L_values[i3], xc3));
            ++i3;

            // x[c4]
            T xc4 = _provider.Divide(x[c4], L_values[i4++]);

            x[c1] = _zero;
            x[c2] = _zero;
            x[c3] = _zero;
            x[c4] = _zero;

            // Subtract the n x 4 block 
            int n = end1 - i1;
            Array.Copy(L_values, i1, _workspace, 0, n);
            _blas1.SCAL(_workspace, xc1, 0, n);
            _blas1.AXPY(_workspace, L_values, xc2, 0, n, i2);
            _blas1.AXPY(_workspace, L_values, xc3, 0, n, i3);
            _blas1.AXPY(_workspace, L_values, xc4, 0, n, i4);

            // Get the row index from the first column's values
            for (int j = 0; j < n; ++j)
            {
                int r1 = L_rowIndices[i1++];
                x[r1] = _provider.Subtract(x[r1], _workspace[j]);
            }

            diag = _provider.Subtract(diag, _provider.Multiply(xc1, xc1));
            diag = _provider.Subtract(diag, _provider.Multiply(xc2, xc2));
            diag = _provider.Subtract(diag, _provider.Multiply(xc3, xc3));
            diag = _provider.Subtract(diag, _provider.Multiply(xc4, xc4));

            // Insert into column c of L matrix
            next[c1]++;
            L_rowIndices[end1] = k;
            L_values[end1] = xc1;

            int end2 = next[c2]++;
            L_rowIndices[end2] = k;
            L_values[end2] = xc2;

            int end3 = next[c3]++;
            L_rowIndices[end3] = k;
            L_values[end3] = xc3;

            int end4 = next[c4]++;
            L_rowIndices[end4] = k;
            L_values[end4] = xc4;

            return diag;
        }

        /// <summary>
        /// All columns from c_start in the 'columns' array, total number is c_count
        /// </summary>
        public T Solve(int[] L_columnIndices, int[] L_rowIndices, T[] L_values, T[] x, int k, int[] columns, int c_start, int c_count, int[] next, T diag)
        {
            IProvider<T> _provider = _blas1.Provider;

            // Determine the column indices
            int end1 = next[columns[c_start]];
            for (int i = 0; i < c_count; ++i)
            {
                _column[i] = L_columnIndices[columns[c_start + i]];
            }

            // Gather the values of vector x into xr, so the top 
            // part of the triangular solve can be carried out as a 
            // single dense triangular solve
            int c0 = _column[0];
            for (int i = 0; i < c_count; ++i)
            {
                _xr[i] = x[L_rowIndices[c0 + i]];
            }

            // Solve the upper triangular portion as a single dense 
            // triangular solve with A defined using the columns of L
            for (int i = 0; i < c_count; ++i)
            {
                int c = columns[c_start + i], ij = _column[i], ij_i = ij - i;
                T xc = _provider.Divide(_xr[i], L_values[ij]);

                // Clear from main array
                x[c] = _zero;

                _blas1.AXPY(_xr, L_values, _provider.Negate(xc), i + 1, c_count, ij_i);
                _column[i] = ij_i + c_count;
                _xc[i] = xc;
            }

            // Subtract the n x c block 
            int n = end1 - _column[0];
            if (n > 0)
            {
                // Gather operation to store the non-zero values of x into _workspace
                int i1 = _column[0];
                for (int j = 0; j < n; ++j)
                {
                    _workspace[j] = x[L_rowIndices[i1 + j]];
                }

                // AXPY operations
                for (int i = 0; i < c_count; ++i)
                {
                    _blas1.AXPY(_workspace, L_values, _provider.Negate(_xc[i]), 0, n, _column[i]);
                }

                // Scatter operation to distribute the computed values into the correct
                // nz pattern of x
                for (int j = 0; j < n; ++j)
                {
                    x[L_rowIndices[i1 + j]] = _workspace[j];
                }
            }

            // Insert into column c of L matrix
            for (int i = 0; i < c_count; ++i)
            {
                int end = next[columns[i + c_start]]++;
                L_rowIndices[end] = k;
                L_values[end] = _xc[i];
            }

            return _provider.Subtract(diag, _blas1.DOT(_xc, _xc, 0, c_count));
        }
    }
}
