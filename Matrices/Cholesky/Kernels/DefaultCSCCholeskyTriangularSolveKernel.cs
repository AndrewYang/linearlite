﻿using LinearNet.Providers;
using System;
using System.Collections.Generic;
using System.Text;

namespace LinearNet.Matrices.Cholesky.Kernels
{
    internal class DefaultCSCCholeskyTriangularSolveKernel<T> : ICSCCholeskyTriangularSolveKernel<T> where T : new()
    {
        public int MaxColumns => 4;

        private readonly IProvider<T> _provider;
        private readonly T _zero;

        internal DefaultCSCCholeskyTriangularSolveKernel(IProvider<T> provider)
        {
            _provider = provider;
            _zero = provider.Zero;
        }

        public T Solve(int[] L_columnIndices, int[] L_rowIndices, T[] L_values, T[] x, int k, int c1, int c2, int[] next, T diag)
        {
            // Determine the column indices
            int start1 = L_columnIndices[c1], end1 = next[c1];
            int start2 = L_columnIndices[c2];

            T xc1 = _provider.Divide(x[c1], L_values[start1]);

            // Subtract the 1 x 1 triangular block
            // r > c, and r should be = reach[i + 1]
            int r = L_rowIndices[start1 + 1];
            x[r] = _provider.Subtract(x[r], _provider.Multiply(L_values[start1 + 1], xc1));

            // x[c2]'s value is updated by the mini triangular solve... can only be 
            // fetched here
            T xc2 = _provider.Divide(x[c2], L_values[start2]);
            x[c1] = _zero;
            x[c2] = _zero;

            // Subtract the n x 2 block 
            for (int m1 = start1 + 2, m2 = start2 + 1; m1 < end1; ++m1, ++m2)
            {
                // Get the row index from the first column's values
                r = L_rowIndices[m1];

                T xr = x[r];

                xr = _provider.Subtract(xr, _provider.Multiply(L_values[m1], xc1));
                xr = _provider.Subtract(xr, _provider.Multiply(L_values[m2], xc2));

                x[r] = xr;
            }

            diag = _provider.Subtract(diag, _provider.Multiply(xc1, xc1));
            diag = _provider.Subtract(diag, _provider.Multiply(xc2, xc2));

            // Insert into column c of L matrix
            next[c1]++;
            L_rowIndices[end1] = k;
            L_values[end1] = xc1;

            int end2 = next[c2]++;
            L_rowIndices[end2] = k;
            L_values[end2] = xc2;

            return diag;
        }

        public T Solve(int[] L_columnIndices, int[] L_rowIndices, T[] L_values, T[] x, int k, int c1, int c2, int c3, int[] next, T diag)
        {
            // Determine the column indices
            int start1 = L_columnIndices[c1], end1 = next[c1],
                start2 = L_columnIndices[c2],
                start3 = L_columnIndices[c3],
                r,
                i1 = start1,
                i2 = start2,
                i3 = start3;

            T xc1 = _provider.Divide(x[c1], L_values[i1++]);
            r = L_rowIndices[i1];
            x[r] = _provider.Subtract(x[r], _provider.Multiply(L_values[i1], xc1));
            ++i1;
            r = L_rowIndices[i1];
            x[r] = _provider.Subtract(x[r], _provider.Multiply(L_values[i1], xc1));
            ++i1;

            // x[c2]'s value is updated by the mini triangular solve... can only be 
            // fetched here
            T xc2 = _provider.Divide(x[c2], L_values[i2++]);
            r = L_rowIndices[i2];
            x[r] = _provider.Subtract(x[r], _provider.Multiply(L_values[i2], xc2));
            ++i2;

            T xc3 = _provider.Divide(x[c3], L_values[i3++]);

            x[c1] = _zero;
            x[c2] = _zero;
            x[c3] = _zero;

            // Subtract the n x 2 block 
            for (int m1 = i1, m2 = i2, m3 = i3; m1 < end1; ++m1, ++m2, ++m3)
            {
                // Get the row index from the first column's values
                r = L_rowIndices[m1];

                T xr = x[r];

                xr = _provider.Subtract(xr, _provider.Multiply(L_values[m1], xc1));
                xr = _provider.Subtract(xr, _provider.Multiply(L_values[m2], xc2));
                xr = _provider.Subtract(xr, _provider.Multiply(L_values[m3], xc3));

                x[r] = xr;
            }

            diag = _provider.Subtract(diag, _provider.Multiply(xc1, xc1));
            diag = _provider.Subtract(diag, _provider.Multiply(xc2, xc2));
            diag = _provider.Subtract(diag, _provider.Multiply(xc3, xc3));

            // Insert into column c of L matrix
            next[c1]++;
            L_rowIndices[end1] = k;
            L_values[end1] = xc1;

            int end2 = next[c2]++;
            L_rowIndices[end2] = k;
            L_values[end2] = xc2;

            int end3 = next[c3]++;
            L_rowIndices[end3] = k;
            L_values[end3] = xc3;

            return diag;
        }

        public T Solve(int[] L_columnIndices, int[] L_rowIndices, T[] L_values, T[] x, int k, int c1, int c2, int c3, int c4, int[] next, T diag)
        {
            // Determine the column indices
            int end1 = next[c1],
                r,
                i1 = L_columnIndices[c1],
                i2 = L_columnIndices[c2],
                i3 = L_columnIndices[c3],
                i4 = L_columnIndices[c4];

            // x[c1]
            T xc1 = _provider.Divide(x[c1], L_values[i1++]);
            r = L_rowIndices[i1];
            x[r] = _provider.Subtract(x[r], _provider.Multiply(L_values[i1], xc1));
            ++i1;
            r = L_rowIndices[i1];
            x[r] = _provider.Subtract(x[r], _provider.Multiply(L_values[i1], xc1));
            ++i1;
            r = L_rowIndices[i1];
            x[r] = _provider.Subtract(x[r], _provider.Multiply(L_values[i1], xc1));
            ++i1;

            // x[c2]
            T xc2 = _provider.Divide(x[c2], L_values[i2++]);
            r = L_rowIndices[i2];
            x[r] = _provider.Subtract(x[r], _provider.Multiply(L_values[i2], xc2));
            ++i2;
            r = L_rowIndices[i2];
            x[r] = _provider.Subtract(x[r], _provider.Multiply(L_values[i2], xc2));
            ++i2;

            // x[c3]
            T xc3 = _provider.Divide(x[c3], L_values[i3++]);
            r = L_rowIndices[i3];
            x[r] = _provider.Subtract(x[r], _provider.Multiply(L_values[i3], xc3));
            ++i3;

            // x[c4]
            T xc4 = _provider.Divide(x[c4], L_values[i4++]);

            x[c1] = _zero;
            x[c2] = _zero;
            x[c3] = _zero;
            x[c4] = _zero;

            // Subtract the n x 2 block 
            for (int m1 = i1, m2 = i2, m3 = i3, m4 = i4; m1 < end1;)
            {
                // Get the row index from the first column's values
                r = L_rowIndices[m1];

                T xr = x[r];

                xr = _provider.Subtract(xr, _provider.Multiply(L_values[m1++], xc1));
                xr = _provider.Subtract(xr, _provider.Multiply(L_values[m2++], xc2));
                xr = _provider.Subtract(xr, _provider.Multiply(L_values[m3++], xc3));
                xr = _provider.Subtract(xr, _provider.Multiply(L_values[m4++], xc4));

                x[r] = xr;
            }

            diag = _provider.Subtract(diag, _provider.Multiply(xc1, xc1));
            diag = _provider.Subtract(diag, _provider.Multiply(xc2, xc2));
            diag = _provider.Subtract(diag, _provider.Multiply(xc3, xc3));
            diag = _provider.Subtract(diag, _provider.Multiply(xc4, xc4));

            // Insert into column c of L matrix
            next[c1]++;
            L_rowIndices[end1] = k;
            L_values[end1] = xc1;

            int end2 = next[c2]++;
            L_rowIndices[end2] = k;
            L_values[end2] = xc2;

            int end3 = next[c3]++;
            L_rowIndices[end3] = k;
            L_values[end3] = xc3;

            int end4 = next[c4]++;
            L_rowIndices[end4] = k;
            L_values[end4] = xc4;

            return diag;
        }

        public T Solve(int[] L_columnIndices, int[] L_rowIndices, T[] L_values, T[] x, int k, int[] columns, int c_start, int c_count, int[] next, T diag)
        {
            throw new NotImplementedException();
        }
    }
}
