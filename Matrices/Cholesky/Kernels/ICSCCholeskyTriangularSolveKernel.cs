﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LinearNet.Matrices.Cholesky.Kernels
{
    interface ICSCCholeskyTriangularSolveKernel<T>
    {
        /// <summary>
        /// The maximum number of columns that can be executed at once
        /// </summary>
        int MaxColumns { get; }

        /// <summary>
        /// Perform a step of the triangular solve;
        /// with the triangular matrix L defined implicitly by the CSC tuple 
        /// (L_columnIndices, L_rowIndices, L_values).
        /// 
        /// x holds the solution and is initialized with the b vector in 
        /// Lx = b.
        /// 
        /// c1 and c2 represent the index of two columns that we will solve 
        /// at once.
        /// 
        /// next holds the end of each column
        /// </summary>
        /// <param name="L_columnIndices"></param>
        /// <param name="L_rowIndices"></param>
        /// <param name="L_values"></param>
        /// <param name="x"></param>
        /// <param name="k"></param>
        /// <param name="c1"></param>
        /// <param name="c2"></param>
        /// <param name="next"></param>
        /// <param name="diag"></param>
        /// <returns>The updated diagonal term.</returns>
        T Solve(int[] L_columnIndices, int[] L_rowIndices, T[] L_values, T[] x, int k, int c1, int c2, int[] next, T diag);

        /// <summary>
        /// Solve a group of 3 columns
        /// </summary>
        /// <param name="L_columnIndices"></param>
        /// <param name="L_rowIndices"></param>
        /// <param name="L_values"></param>
        /// <param name="x"></param>
        /// <param name="k"></param>
        /// <param name="c1"></param>
        /// <param name="c2"></param>
        /// <param name="c3"></param>
        /// <param name="next"></param>
        /// <param name="diag"></param>
        /// <returns></returns>
        T Solve(int[] L_columnIndices, int[] L_rowIndices, T[] L_values, T[] x, int k, int c1, int c2, int c3, int[] next, T diag);

        /// <summary>
        /// Solve a group of 4 columns
        /// </summary>
        /// <param name="L_columnIndices"></param>
        /// <param name="L_rowIndices"></param>
        /// <param name="L_values"></param>
        /// <param name="x"></param>
        /// <param name="k"></param>
        /// <param name="c1"></param>
        /// <param name="c2"></param>
        /// <param name="c3"></param>
        /// <param name="c4"></param>
        /// <param name="next"></param>
        /// <param name="diag"></param>
        /// <returns></returns>
        T Solve(int[] L_columnIndices, int[] L_rowIndices, T[] L_values, T[] x,
            int k, int c1, int c2, int c3, int c4, int[] next, T diag);

        T Solve(int[] L_columnIndices, int[] L_rowIndices, T[] L_values, T[] x, int k, int[] columns, int c_start, int c_count, int[] next, T diag);
    }
}