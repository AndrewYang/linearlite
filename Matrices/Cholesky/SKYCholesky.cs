﻿using LinearNet.Global;
using LinearNet.Providers;
using LinearNet.Structs;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace LinearNet.Matrices.Sparse.Decompositions
{
    internal class SKYCholesky<T> where T : new()
    {
        /// <summary>
        /// Returns the lower-triangular Cholesky factor of a symmetric sparse matrix A stored in Skyline format.  
        /// Matrix must be symmetric positive definite. No permutation is applied.
        /// 
        /// This method uses the upward-looking Cholesky algorithm. No symbolic precomputation is used to determine
        /// the matrix's non-zero pattern, to keep the algorithm simple. This means we end up using an extra workspace 
        /// of at least size n (however the additional time penalty is low).
        /// </summary>
        /// <param name="A"></param>
        /// <returns></returns>
        internal SKYSparseMatrix<T> CholUp(SKYSparseMatrix<T> A, IProvider<T> provider)
        {
            if (A is null)
            {
                throw new ArgumentNullException(nameof(A));
            }
            if (A.Type != SKYType.SYMMETRIC)
            {
                throw new InvalidOperationException("Matrix is not symmetric.");
            }

            int dim = A.Columns;

            // L is lower triangular, we store row by row
            T L_11 = provider.Sqrt(A[0, 0]);
            int[] indices = new int[dim + 1];
            indices[1] = 1;

            // All diagonal elements need to be stored, which is at least dim entries
            List<T> values = new List<T>(dim) { L_11 };

            T[] workspace = new T[dim];
            for (int c = 1; c < dim; ++c)
            {
                // Solve L * r = b where L is the lower triangular factor so far (top c - 1 rows)
                // and r is the c-th row.
                SolveLowerTriangular(indices, values, A, c, workspace, provider);

                // Append the L_22 term L_22 = sqrt(A_22 - L_12^T * L_12)
                T dot = provider.Zero;
                int row_start = indices[c], row_end = indices[c + 1];

                for (int i = row_start; i < row_end; ++i)
                {
                    T v = values[i];
                    dot = provider.Add(dot, provider.Multiply(v, v));
                }
                T sq = provider.Subtract(A[c, c], dot);
                if (provider.IsRealAndNegative(sq))
                {
                    throw new InvalidOperationException("Matrix is not positive-semidefinite.");
                }

                T L_22 = provider.Sqrt(sq);
                values.Add(L_22);
                indices[c + 1]++;
            }

            return new SKYSparseMatrix<T>(indices, values.ToArray(), SKYType.LOWER, true);
        }
        /// <summary>
        /// Solve the lower triangular subsystem L(0:k, 0:k) * x(0:k) = b(0:k, k).
        /// L is defined implicitly using the tuple (indices, values), in row-major order
        /// k > 0. This is used to completely solve the k-th row of the Cholesky factor
        /// 
        /// The solution is appended onto values (as a contingous row). Also, the (k + 1)-th entry of 
        /// indices is updated.
        /// </summary>
        private void SolveLowerTriangular(int[] indices, List<T> values, SKYSparseMatrix<T> b, int k, T[] x_workspace, IProvider<T> provider)
        {
            // The k-th column of b matrix
            // We only want to take the top (k - 1) entries (i.e. drop the last one), so the 
            // actual length of b-vector (blen) is 1 less than (end - start)
            int start = b.Indices[k], 
                end = b.Indices[k + 1], 
                blen = end - start - 1;

            // The first row containing a non-zero in b. 
            // row_end is exclusive, i.e. we iterate over the range [row_start, row_end)
            int row_start = k - blen, 
                row_end = k;

            for (int r = row_start, bi = start; r < row_end; ++r, ++bi)
            {
                T xr = b.Values[bi];

                int r_offset = -r + indices[r + 1] - 1, 
                    r_start = indices[r];

                // Subtract all terms L(r, c) * x(c) for c < r from the sum
                for (int c = row_start; c < r; ++c)
                {
                    // Calculate the index within L using the row/col coordinates (r, c)
                    //int L_index = c - r + indices[r + 1] - 1;
                    int L_index = c + r_offset;
                    //if (L_index >= indices[r])
                    if (L_index >= r_start)
                    {
                        xr = provider.Subtract(xr, provider.Multiply(x_workspace[c], values[L_index]));
                    }
                }

                // Get the diagonal term from the implicit matrix L(k - 1, k - 1)
                // Since L is lower triangular, the diagonal entry will always be the last element 
                // in the matrix
                T L_diag = values[indices[r + 1] - 1];
                x_workspace[r] = provider.Divide(xr, L_diag);
            }

            // Insert row into values
            for (int r = row_start; r < row_end; ++r)
            {
                values.Add(x_workspace[r]);
            }
            indices[k + 1] = values.Count;
        }

        internal SKYSparseMatrix<T> CholUp(SKYSparseMatrix<T> A, IDenseBLAS1<T> blas1)
        {
            if (A is null)
            {
                throw new ArgumentNullException(nameof(A));
            }
            if (A.Type != SKYType.SYMMETRIC)
            {
                throw new InvalidOperationException("Matrix is not symmetric.");
            }

            int dim = A.Columns;

            IProvider<T> provider = blas1.Provider;

            // L is lower triangular, we store row by row
            int[] indices = new int[dim + 1];
            indices[1] = 1;

            // All diagonal elements need to be stored, which is at least dim entries
            // We create our own list here
            FastList<T> values = new FastList<T>(dim);
            values.Add(provider.Sqrt(A[0, 0])); // Add L_11

            T[] workspace = new T[dim];
            for (int c = 1; c < dim; ++c)
            {
                // Solve L * r = b where L is the lower triangular factor so far (top c - 1 rows)
                // and r is the c-th row.
                SolveLowerTriangular(indices, values, A, c, workspace, blas1);

                // Append the L_22 term L_22 = sqrt(A_22 - L_12^T * L_12)
                T sq = provider.Subtract(A[c, c], blas1.DOT(values.Values, values.Values, indices[c], indices[c + 1]));
                if (provider.IsRealAndNegative(sq))
                {
                    throw new InvalidOperationException("Matrix is not positive-semidefinite.");
                }

                // Add L_22
                values.Add(provider.Sqrt(sq));
                indices[c + 1]++;
            }

            return new SKYSparseMatrix<T>(indices, values.ToArray(), SKYType.LOWER, true);
        }
        private void SolveLowerTriangular(int[] indices, FastList<T> values, SKYSparseMatrix<T> b, int k, T[] x_workspace, IDenseBLAS1<T> blas1)
        {
            int[] b_indices = b.Indices;
            T[] b_values = b.Values;
            IProvider<T> provider = blas1.Provider;

            // The k-th column of b matrix
            // We only want to take the top (k - 1) entries (i.e. drop the last one), so the 
            // actual length of b-vector (blen) is 1 less than (end - start)
            int start = b_indices[k],
                end = b_indices[k + 1],
                blen = end - start - 1;

            // The first row containing a non-zero in b. 
            // row_end is exclusive, i.e. we iterate over the range [row_start, row_end)
            int row_start = k - blen,
                row_end = k;

            for (int r = row_start, bi = start; r < row_end; ++r, ++bi)
            {
                int last = indices[r + 1] - 1, 
                    r_offset = last - r;

                // Subtract all terms L(r, c) * x(c) for c < r from the sum
                T xr = provider.Subtract(b_values[bi], blas1.DOT(x_workspace, values.Values, row_start, r, r_offset));

                // Get the diagonal term from the implicit matrix L(k - 1, k - 1)
                // Since L is lower triangular, the diagonal entry will always be the last element 
                // in the matrix
                T L_diag = values[last];
                x_workspace[r] = provider.Divide(xr, L_diag);
            }

            // Insert row into values
            values.AddRange(x_workspace, row_start, row_end);
            indices[k + 1] = values.Count;
        }
    }

    /// <summary>
    /// This class is used whenever dense level-1 operations need to be conducted, and direct access
    /// to the internal array structure is required (List<T> does not let us do this).
    /// </summary>
    /// <typeparam name="T"></typeparam>
    internal class FastList<T>
    {
        private int _length;
        private int _count;
        private T[] _values;

        internal int Count { get { return _count; } }
        internal T[] Values { get { return _values; } }

        internal T this[int index]
        {
            get
            {
                return _values[index];
            }
        }

        public FastList(int initialCapacity)
        {
            if (initialCapacity <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(initialCapacity), ExceptionMessages.NonPositiveNotAllowed);
            }
            _values = new T[initialCapacity];
            _count = 0;
            _length = initialCapacity;
        }

        internal void EnsureCapacity(int totalCapacity)
        {
            while (totalCapacity > _length)
            {
                _length *= 2;
            }
            Array.Resize(ref _values, _length);
        }
        internal void Add(T value)
        {
            if (_count >= _length)
            {
                _length *= 2;
                Array.Resize(ref _values, _length);
            }
            _values[_count++] = value;
        }
        internal void AddRange(T[] array, int start, int end)
        {
            int len = end - start;
            while (_count + len - 1 >= _length)
            {
                _length *= 2;
            }
            Array.Resize(ref _values, _length);
            for (int i = start; i < end; ++i)
            {
                _values[_count++] = array[i];
            }
        }
        internal T[] ToArray()
        {
            Array.Resize(ref _values, _count);
            return _values;
        }
        
        internal void Print()
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < _count; ++i)
            {
                sb.Append(_values[i] + "\t");
            }
            Debug.WriteLine(sb.ToString());
        }
    }
}
