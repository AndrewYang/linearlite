﻿using LinearNet.Providers;
using LinearNet.Helpers;
using LinearNet.Matrices;
using LinearNet.Structs;
using System;
using LinearNet.Providers.LinearAlgebra;

namespace LinearNet.Decomposition
{
    public static class CholeskyDecomposition
    {
        /// <summary>
        /// Internal method that operates on jagged matrices, using the Cholesky–Banachiewicz algorithm.
        /// Requirements:
        /// 1) Both A and L must be square and of the same dimensions (however this is not checked)
        /// 2) A must be positive definite
        /// If either requirement fails, the method returns false. Otherwise it returns true (success)
        /// </summary>
        /// <param name="A"></param>
        /// <param name="L"></param>
        internal static bool CholeskyDecompose<T>(T[][] A, T[][] L, IDenseBLAS1<T> blas, Func<T, T> Sqrt)
        {
            int dim = A.Length, r, c;

            IProvider<T> provider = blas.Provider;
            T zero = provider.Zero;

            for (r = 0; r < dim; ++r)
            {
                T[] L_r = L[r], A_r = A[r];
                T s = zero;
                for (c = 0; c < r; c++)
                {
                    // Off-diagonal term in row r, col c where c < r is 
                    // (A(r, c) - sum(L(r, i) * L(c, i), i in [0, c))) / L(c, c)

                    T[] L_c = L[c];
                    T L_rc = blas.DOT(L_r, L_c, 0, c);
                    L_rc = provider.Divide(provider.Subtract(A_r[c], L_rc), L_c[c]);

                    L_r[c] = L_rc;
                    s = provider.Add(s, provider.Multiply(L_rc, L_rc));
                }

                // Diagonal term is L(r, r) = sqrt(A(r, r) - sum(L(r, i)^2, i in [0, r))
                T sq = provider.Subtract(A_r[r], s);
                if (provider.IsRealAndNegative(sq))
                {
                    return false;
                }
                L[r][r] = Sqrt(sq);
            }
            return true;
        }

        private static void LDLTDecompose(float[][] A, float[][] L, float[] D)
        {
            int dim = A.Length, r, c, k;
            for (r = 0; r < dim; ++r)
            {
                float[] L_r = L[r], A_r = A[r];
                float D_r = A_r[r];
                for (c = 0; c < r; ++c)
                {
                    float[] L_c = L[c];
                    float L_rc = A_r[c];
                    for (k = 0; k < c; ++k)
                    {
                        L_rc -= L_r[k] * L_c[k] * D[k];
                    }
                    L_rc /= D[c];
                    L_r[c] = L_rc;

                    D_r -= L_rc * L_rc * D[c];
                }
                L[r][r] = 1.0f;
                D[r] = D_r;
            }
        }
        private static void LDLTDecompose(double[][] A, double[][] L, double[] D)
        {
            int dim = A.Length, r, c, k;
            for (r = 0; r < dim; ++r)
            {
                double[] L_r = L[r], A_r = A[r];
                double D_r = A_r[r];
                for (c = 0; c < r; ++c)
                {
                    double[] L_c = L[c];
                    double L_rc = A_r[c];
                    for (k = 0; k < c; ++k)
                    {
                        L_rc -= L_r[k] * L_c[k] * D[k];
                    }
                    L_rc /= D[c];
                    L_r[c] = L_rc;

                    D_r -= L_rc * L_rc * D[c];
                }
                L[r][r] = 1.0;
                D[r] = D_r;
            }
        }
        private static void LDLTDecompose(decimal[][] A, decimal[][] L, decimal[] D)
        {
            int dim = A.Length, r, c, k;
            for (r = 0; r < dim; ++r)
            {
                decimal[] L_r = L[r], A_r = A[r];
                decimal D_r = A_r[r];
                for (c = 0; c < r; ++c)
                {
                    decimal[] L_c = L[c];
                    decimal L_rc = A_r[c];
                    for (k = 0; k < c; ++k)
                    {
                        L_rc -= L_r[k] * L_c[k] * D[k];
                    }
                    L_rc /= D[c];
                    L_r[c] = L_rc;

                    D_r -= L_rc * L_rc * D[c];
                }
                L[r][r] = 1.0m;
                D[r] = D_r;
            }
        }
        private static void LDLTDecompose(Complex[][] A, Complex[][] L, Complex[] D)
        {
            int dim = A.Length, r, c, k;
            for (r = 0; r < dim; ++r)
            {
                Complex[] L_r = L[r], A_r = A[r];
                Complex D_r = A_r[r];
                for (c = 0; c < r; ++c)
                {
                    Complex[] L_c = L[c];
                    Complex L_rc = A_r[c];
                    for (k = 0; k < c; ++k)
                    {
                        L_rc -= L_r[k].Multiply(L_c[k].Conjugate()).Multiply(D[k]);
                    }
                    L_rc /= D[c];
                    L_r[c] = L_rc;

                    D_r -= L_rc.Multiply(L_rc.Conjugate()).Multiply(D[c]);
                }
                L[r][r] = Complex.One;
                D[r] = D_r;
            }
        }
        private static void LDLTDecompose<T>(T[][] A, T[][] L, T[] D) where T : IField<T>, new()
        {
            int dim = A.Length, r, c, k;
            T one = new T().AdditiveIdentity;
            for (r = 0; r < dim; ++r)
            {
                T[] L_r = L[r], A_r = A[r];
                T D_r = A_r[r];
                for (c = 0; c < r; ++c)
                {
                    T[] L_c = L[c];
                    T L_rc = A_r[c];
                    for (k = 0; k < c; ++k)
                    {
                        L_rc = L_rc.Subtract(L_r[k].Multiply(L_c[k]).Multiply(D[k]));
                    }
                    L_rc = L_rc.Divide(D[c]);
                    L_r[c] = L_rc;

                    D_r = D_r.Subtract(L_rc.Multiply(L_rc).Multiply(D[c]));
                }
                L[r][r] = one;
                D[r] = D_r;
            }
        }

        private static void LDLTDecompose<T>(T[][] A, T[][] L, T[][] D, Action<T[][], T[][], T[]> LDL)
        {
            int len = A.Length;
            T[] diagonal = new T[len];

            LDL(A, L, diagonal);
            for (int i = 0; i < len; ++i)
            {
                D[i][i] = diagonal[i];
            }
        }
        private static void LDLTDecompose(float[][] A, float[][] L, float[][] D) => LDLTDecompose(A, L, D, LDLTDecompose);
        private static void LDLTDecompose(double[][] A, double[][] L, double[][] D) => LDLTDecompose(A, L, D, LDLTDecompose);
        private static void LDLTDecompose(decimal[][] A, decimal[][] L, decimal[][] D) => LDLTDecompose(A, L, D, LDLTDecompose);
        private static void LDLTDecompose(Complex[][] A, Complex[][] L, Complex[][] D) => LDLTDecompose(A, L, D, LDLTDecompose);
        private static void LDLTDecompose<T>(T[][] A, T[][] L, T[][] D) where T : IField<T>, new() => LDLTDecompose(A, L, D, LDLTDecompose);

        /// <summary>
        /// <para>
        /// Calculate the Cholesky decomposition of a positive-definite square matrix $A\in\mathbb{F}^{n \times n}$ into the product 
        /// $LL^*$ where $L\in\mathbb{F}^{n\times n}$ is a lower triangular matrix and $L^*$ is the conjugate transpose of $L$.
        /// </para>
        /// <para>
        /// This implementation uses the Cholesky–Banachiewicz algorithm.
        /// </para>
        /// <para>
        /// Throws <txt>InvalidOperationException</txt> if $A$ is not square or positive semi-definite. 
        /// </para>
        /// <para>Also see: <a href="#LDLTDecompose"><txt>LDLTDecompose</txt></a>.</para>
        /// <!--inputs-->
        /// <h4>Example</h4>
        /// <pre><code class="cs">
        /// // Create a random PSD matrix 
        /// DenseMatrix&lt;double&gt; matrix = DenseMatrix.RandomPositiveDefinite&lt;double&gt;(10, 10); 
        /// matrix.CholeskyDecompose(out DenseMatrix&lt;double&gt; L);
        /// 
        /// Console.WriteLine(L.IsLowerTriangular()); // true
        /// Console.WriteLine(L.Multiply(L.Transpose()).ApproximatelyEquals(matrix)); // true
        /// 
        /// </code></pre>
        /// </summary>
        /// <name>CholeskyDecompose</name>
        /// <proto>void CholeskyDecompose(this DenseMatrix<T> A, out DenseMatrix<T> L)</proto>
        /// <cat>la</cat>
        /// <param name="A">The positive semidefinite square matrix $A$, to be decomposed.</param>
        /// <param name="L">
        /// <b>Out parameter</b><br/>
        /// The square lower triangular matrix $L$.</param>
        public static void CholeskyDecompose(this DenseMatrix<float> A, out DenseMatrix<float> L)
        {
            MatrixChecks.CheckNotNull(A);
            MatrixChecks.CheckIsSquare(A);

            if (!A.IsPositiveDefinite())
            {
                throw new ArgumentException("Matrix is not positive definite.");
            }

            L = new DenseMatrix<float>(A.Rows, A.Rows);
            CholeskyDecompose(A.Values, L.Values, new NativeFloatProvider(), a => (float)Math.Sqrt(Math.Max(0, a)));
        }
        public static void CholeskyDecompose(this DenseMatrix<double> A, out DenseMatrix<double> L)
        {
            MatrixChecks.CheckNotNull(A);
            MatrixChecks.CheckIsSquare(A);
            
            if (!A.IsPositiveDefinite())
            {
                throw new ArgumentException("Matrix is not positive definite.");
            }
            
            L = new DenseMatrix<double>(A.Rows, A.Rows);
            CholeskyDecompose(A.Values, L.Values, new NativeDoubleProvider(), x => Math.Sqrt(Math.Max(0, x)));
        }
        public static void CholeskyDecompose(this DenseMatrix<decimal> A, out DenseMatrix<decimal> L)
        {
            MatrixChecks.CheckNotNull(A);
            MatrixChecks.CheckIsSquare(A);

            if (!A.IsPositiveDefinite())
            {
                throw new ArgumentException("Matrix is not positive definite.");
            }

            L = new DenseMatrix<decimal>(A.Rows, A.Rows);
            CholeskyDecompose(A.Values, L.Values, new NativeDecimalProvider(), a => DecimalMath.Sqrt(Math.Max(0, a)));
        }
        /// <summary>
        /// TODO: fix this method
        /// </summary>
        /// <param name="A"></param>
        /// <param name="L"></param>
        public static void CholeskyDecompose(this DenseMatrix<Complex> A, out DenseMatrix<Complex> L)
        {
            MatrixChecks.CheckNotNull(A);
            MatrixChecks.CheckIsSquare(A);

            /*
            if (!A.IsPositiveDefinite())
            {
                throw new ArgumentException("Matrix is not positive definite.");
            }
            */

            L = new DenseMatrix<Complex>(A.Rows, A.Rows);
            CholeskyDecompose(A.Values, L.Values, new NativeComplexProvider(), a => Complex.Sqrt(a));
        }

        /// <summary>
        /// Decompose a square, positive-definite matrix A into a product of 2 square matrices, L(L^T),
        /// where L is a lower-triangular real matrix.
        /// Cholesky decomposition only applies for positive-definite matrices.
        /// Implementation uses the Cholesky–Banachiewicz algorithm.
        /// </summary>
        /// <param name="A">A positive definite real-valued matrix.</param>
        /// <param name="L">Real-valued square matrix L such that A = L(L^T).</param>
        public static void CholeskyDecompose(this double[,] A, out double[,] L)
        {
            MatrixChecks.CheckNotNull(A);
            MatrixChecks.CheckIsSquare(A);

            if (!A.IsPositiveDefinite(0))
            {
                throw new ArgumentException("Matrix is not positive definite.");
            }

            int dim = A.GetLength(0), r, c, i;
            L = new double[dim, dim];

            for (r = 0; r < dim; ++r)
            {
                double s = 0.0;
                for (c = 0; c < r; c++)
                {
                    // Off-diagonal term in row r, col c where c < r is 
                    // (A(r, c) - sum(L(r, i) * L(c, i), i in [0, c))) / L(c, c)

                    double L_rc = 0;
                    for (i = 0; i < c; ++i)
                    {
                        L_rc += L[r, i] * L[c, i];
                    }
                    L_rc = (A[r, c] - L_rc) / L[c, c];

                    L[r, c] = L_rc;
                    s += L_rc * L_rc;
                }

                // Diagonal term is L(r, r) = sqrt(A(r, r) - sum(L(r, i)^2, i in [0, r))
                L[r, r] = Math.Sqrt(Math.Max(0, A[r, r] - s));
            }
        }
        /// <summary>
        /// Decompose a square, positive-definite complex matrix A into a product of 2 square matrices, L(L^T),
        /// where L is a lower-triangular real matrix.
        /// Cholesky decomposition only applies for positive-definite matrices.
        /// Implementation uses the Cholesky–Banachiewicz algorithm.
        /// </summary>
        /// <param name="A">A positive definite real-valued matrix.</param>
        /// <param name="L">Real-valued square matrix L such that A = L(L^T).</param>
        public static void CholeskyDecompose(this Complex[,] A, out Complex[,] L)
        {
            MatrixChecks.CheckNotNull(A);
            MatrixChecks.CheckIsSquare(A);

            /*
            if (!A.IsPositiveDefinite(0))
            {
                throw new ArgumentException("Matrix is not positive definite.");
            }*/

            int dim = A.GetLength(0), r, c, i;
            L = new Complex[dim, dim];

            for (r = 0; r < dim; ++r)
            {
                Complex s = 0.0;
                for (c = 0; c < r; c++)
                {
                    // Off-diagonal term in row r, col c where c < r is 
                    // (A(r, c) - sum(L(r, i) * L(c, i), i in [0, c))) / L(c, c)

                    Complex L_rc = 0;
                    for (i = 0; i < c; ++i)
                    {
                        L_rc += L[r, i] * L[c, i];
                    }
                    L_rc = (A[r, c] - L_rc) / L[c, c];

                    L[r, c] = L_rc;
                    s += L_rc * L_rc;
                }

                // Diagonal term is L(r, r) = sqrt(A(r, r) - sum(L(r, i)^2, i in [0, r))
                L[r, r] = Complex.Sqrt((A[r, r] - s));
            }
        }

        /// <summary>
        /// <para>
        /// Calculate the LDLT decomposition of a square matrix $A$ into the product $LDL^*$, where $L$ is a lower triangular matrix 
        /// and $D$ is a diagonal matrix.
        /// </para>
        /// <para>
        /// Throws <txt>InvalidOperationException</txt> if $A$ is not square. 
        /// </para>
        /// <para>Supported for matrices over <txt>float</txt>, <txt>double</txt>, <txt>decimal</txt>, <txt>Complex</txt> and all types <txt>T</txt> implementing <txt>Field&lt;T&gt;</txt>.</para>
        /// <para>Also see: <a href="#LDLTDecomposeVector"><txt>LDLTDecompose</txt></a>, <a href="#CholeskyDecompose"><txt>CholeskyDecompose</txt></a>.</para>
        /// 
        /// <!--inputs-->
        /// </summary>
        /// <name>LDLTDecompose</name>
        /// <proto>void LDLTDecompose(this DenseMatrix<T> A, out DenseMatrix<T> L, out DenseMatrix<T> D)</proto>
        /// <cat>la</cat>
        /// <param name="A">The $m \times m$ square matrix to be decomposed.</param>
        /// <param name="L">
        /// <b>Out parameter</b><br/>
        /// A $m \times m$ lower triangular matrix, $L$.
        /// </param>
        /// <param name="D">
        /// <b>Out parameter</b><br/>
        /// A $m \times m$ diagonal matrix, $D$.
        /// </param>
        public static void LDLTDecompose(this DenseMatrix<float> A, out DenseMatrix<float> L, out DenseMatrix<float> D)
        {
            MatrixChecks.CheckNotNull(A);
            MatrixChecks.CheckIsSquare(A);

            int rows = A.Rows;
            L = new DenseMatrix<float>(rows, rows);
            D = new DenseMatrix<float>(rows, rows);

            LDLTDecompose(A.Values, L.Values, D.Values);
        }
        public static void LDLTDecompose(this DenseMatrix<double> A, out DenseMatrix<double> L, out DenseMatrix<double> D)
        {
            MatrixChecks.CheckNotNull(A);
            MatrixChecks.CheckIsSquare(A);

            int rows = A.Rows;
            L = new DenseMatrix<double>(rows, rows);
            D = new DenseMatrix<double>(rows, rows);

            //LDLDecomposition(A.Values, L.Values, D.Values, new DoubleBLAS());
            LDLTDecompose(A.Values, L.Values, D.Values);
        }
        public static void LDLTDecompose(this DenseMatrix<decimal> A, out DenseMatrix<decimal> L, out DenseMatrix<decimal> D)
        {
            MatrixChecks.CheckNotNull(A);
            MatrixChecks.CheckIsSquare(A);

            int rows = A.Rows;
            L = new DenseMatrix<decimal>(rows, rows);
            D = new DenseMatrix<decimal>(rows, rows);

            LDLTDecompose(A.Values, L.Values, D.Values);
        }
        public static void LDLTDecompose(this DenseMatrix<Complex> A, out DenseMatrix<Complex> L, out DenseMatrix<Complex> D)
        {
            MatrixChecks.CheckNotNull(A);
            MatrixChecks.CheckIsSquare(A);

            int rows = A.Rows;
            L = new DenseMatrix<Complex>(rows, rows);
            D = new DenseMatrix<Complex>(rows, rows);

            LDLTDecompose(A.Values, L.Values, D.Values);
        }
        public static void LDLTDecompose<T>(this DenseMatrix<T> A, out DenseMatrix<T> L, out DenseMatrix<T> D) where T : IField<T>, new()
        {
            MatrixChecks.CheckNotNull(A);
            MatrixChecks.CheckIsSquare(A);

            int rows = A.Rows;
            L = new DenseMatrix<T>(rows, rows);
            D = new DenseMatrix<T>(rows, rows);

            LDLTDecompose(A.Values, L.Values, D.Values);
        }

        /// <summary>
        /// <para>
        /// Calculate the LDLT decomposition of a square matrix $A$, returning the diagonal matrix as a vector of its diagonal elements.
        /// </para>
        /// <para>
        /// $A$ is factorized as $LDL^*$, where $L$ is a lower triangular matrix and $D$ is a diagonal matrix.
        /// </para>
        /// <para>Supported for matrices over <txt>float</txt>, <txt>double</txt>, <txt>decimal</txt>, <txt>Complex</txt> and all types <txt>T</txt> implementing <txt>Field&lt;T&gt;</txt>.</para>
        /// <para>Also see: <a href="#LDLTDecompose"><txt>LDLTDecompose</txt></a>, <a href="#CholeskyDecompose"><txt>CholeskyDecompose</txt></a>.</para>
        /// 
        /// <!--inputs-->
        /// </summary>
        /// <name>LDLTDecomposeVector</name>
        /// <proto>void LDLTDecompose(this DenseMatrix<T> A, out DenseMatrix<T> L, out T[] D)</proto>
        /// <cat>la</cat>
        /// <param name="A">The $m \times m$ square matrix to be decomposed.</param>
        /// <param name="L">
        /// <b>Out parameter</b><br/>
        /// A $m \times m$ lower triangular matrix, $L$.
        /// </param>
        /// <param name="D">
        /// <b>Out parameter</b><br/>
        /// A $m$-dimensional vector representing the diagonal elements of the $m \times m$ diagonal matrix $D$.
        /// </param>
        public static void LDLTDecompose(this DenseMatrix<float> A, out DenseMatrix<float> L, out float[] D)
        {
            MatrixChecks.CheckNotNull(A);
            MatrixChecks.CheckIsSquare(A);

            int rows = A.Rows;
            L = new DenseMatrix<float>(rows, rows);
            D = new float[rows];
            LDLTDecompose(A.Values, L.Values, D);
        }
        public static void LDLTDecompose(this DenseMatrix<double> A, out DenseMatrix<double> L, out double[] D)
        {
            MatrixChecks.CheckNotNull(A);
            MatrixChecks.CheckIsSquare(A);

            int rows = A.Rows;
            L = new DenseMatrix<double>(rows, rows);
            D = new double[rows];
            LDLTDecompose(A.Values, L.Values, D);
        }
        public static void LDLTDecompose(this DenseMatrix<decimal> A, out DenseMatrix<decimal> L, out decimal[] D)
        {
            MatrixChecks.CheckNotNull(A);
            MatrixChecks.CheckIsSquare(A);

            int rows = A.Rows;
            L = new DenseMatrix<decimal>(rows, rows);
            D = new decimal[rows];
            LDLTDecompose(A.Values, L.Values, D);
        }
        public static void LDLTDecompose(this DenseMatrix<Complex> A, out DenseMatrix<Complex> L, out Complex[] D)
        {
            MatrixChecks.CheckNotNull(A);
            MatrixChecks.CheckIsSquare(A);

            int rows = A.Rows;
            L = new DenseMatrix<Complex>(rows, rows);
            D = new Complex[rows];
            LDLTDecompose(A.Values, L.Values, D);
        }
        public static void LDLTDecompose<T>(this DenseMatrix<T> A, out DenseMatrix<T> L, out T[] D) where T : IField<T>, new()
        {
            MatrixChecks.CheckNotNull(A);
            MatrixChecks.CheckIsSquare(A);

            int rows = A.Rows;
            L = new DenseMatrix<T>(rows, rows);
            D = new T[rows];
            LDLTDecompose(A.Values, L.Values, D);
        }
    }
}
