﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Matrices
{
    public enum EigenvalueAlgorithm
    {
        NAIVE_QR_ALGORITHM,
        SHIFTED_QR_ALGORITHM,
        DOUBLE_SHIFTED_QR_ALGORITHM,
        FRANCIS_ALGORITHM
    }
}
