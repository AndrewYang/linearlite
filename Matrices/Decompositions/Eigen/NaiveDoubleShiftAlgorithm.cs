﻿using LinearNet.Decomposition;
using LinearNet.Matrices.Decompositions.QR;
using LinearNet.Structs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Matrices.Decompositions
{
    /// <summary>
    /// Calculates the eigenvalues of A using Wilkinson-shifted QR algorithm, with Hessenberg decomposition via
    /// Housholder reflections. 
    /// 
    /// This class does not store or return the eigenvectors. Since the eigenvectors will require a O(n^3) 
    /// step per iteration (more expensive than Hessenberg QR), it will be implemented in separate method.
    /// 
    /// </summary>
    internal class NaiveDoubleShiftAlgorithm : IEigenvalueAlgorithm
    {
        internal override void CalculateEigenvalues(double[][] H, Complex[] eigenvalues)
        {
            int max_iterations = 5;
            double termination_threshold = 1e-120;

            int m = H.Length, i, j, k, l;
            double[][] Q = MatrixInternalExtensions.JMatrix<double>(m, m), H_new;

            HessenbergExtensions.to_hessenberg_form(Q, H);

            IDenseQRDecompositionAlgorithm qr = new HouseholderQRDecompositionAlgorithm();

            List<Complex> evs = new List<Complex>();
            for (i = m - 1; i >= 1; i--)
            {
                // Start the iterations
                int prev = i - 1;
                for (j = 0; j < max_iterations; ++j)
                {
                    // Double shift method - construct M := H^2 - s * H + t * I
                    // where s := tr(H(2 x 2)), t := det(H(2 x 2))
                    double s = H[prev][prev] + H[i][i];
                    double t = H[prev][prev] * H[i][i] - H[prev][i] * H[i][prev];
                    double[][] M = H.Multiply(H);
                    // Subtract M <- M - s * H + t * I
                    for (k = 0; k <= i; ++k)
                    {
                        double[] M_k = M[k], H_k = H[k];
                        for (l = 0; l <= i; ++l)
                        {
                            M_k[l] -= H_k[l] * s;
                        }
                        M_k[k] += t;
                    }
                    
                    // Check for completeness in the M matrix
                    // Numerical problems tend to happen with i = 1
                    if (i == 1 && M[i].IsZero(termination_threshold) && M[prev].IsZero(termination_threshold))
                    {
                        break;
                    }

                    // QR decompose M instead of H - cannot use hessenberg decomp in this case 
                    // since M has 2 lower off-diagonals...
                    // This method runs into numerical issues when the number of rows/columns = 2
                    // So we terminate the program early. 
                    // QRDecomposition.qr_decompose_unsafe(Q, M, true);
                    qr.QRDecompose(null, Q, M, true, false, false);

                    // H := Q^T * M * Q
                    H_new = Q.Transpose().Multiply(H).Multiply(Q);
                    if (double.IsNaN(H_new[i][i]) || double.IsInfinity(H_new[i][i]) || double.IsNaN(H_new[prev][prev]) || double.IsInfinity(H_new[prev][prev]))
                    {
                        break;
                    }
                    H = H_new;

                    // Termination criteria
                    if (Math.Abs(H[i][prev]) < termination_threshold)
                    {
                        break;
                    }
                }

                if (Math.Abs(H[i][prev]) > Precision.DOUBLE_PRECISION)
                {
                    calc_eigenvalues(H[prev][prev], H[prev][i], H[i][prev], H[i][i], out Complex l1, out Complex l2);

                    evs.Add(l1);
                    evs.Add(l2);

                    H = H.Submatrix(0, 0, prev, prev);
                    Q = Q.Submatrix(0, 0, prev, prev);
                    i--;
                }
                else
                {
                    // Single eigenvalue case
                    evs.Add(H[i][i]);
                    if (i == 2)
                    {
                        evs.Add(H[0][0]);
                    }
                    else
                    {
                        H = H.Submatrix(0, 0, i, i);
                        Q = Q.Submatrix(0, 0, i, i);
                    }
                }
            }

            // Final eigenvalue
            if (evs.Count < m)
            {
                evs.Add(H[0][0]);
            }

            // copy over the eigenvalues
            for (i = 0; i < m; ++i)
            {
                eigenvalues[i] = evs[i];
            }
        }

        internal override void CalculateEigenvalues(float[][] A, Complex[] eigenvalues)
        {
            throw new NotImplementedException();
        }

        internal override void CalculateEigenvalues(decimal[][] A, Complex[] eigenvalues)
        {
            throw new NotImplementedException();
        }

        internal override void CalculateEigenvalues(Complex[][] A, Complex[] eigenvalues)
        {
            throw new NotImplementedException();
        }
    }
}
