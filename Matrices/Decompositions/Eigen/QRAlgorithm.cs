﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LinearNet.Decomposition;
using LinearNet.Matrices.Decompositions.QR;
using LinearNet.Structs;

namespace LinearNet.Matrices.Decompositions
{
    internal class QRAlgorithm : IEigenvalueAlgorithm
    {
        private int _iterations = 2000;

        internal override void CalculateEigenvalues(double[][] H, Complex[] eigenvalues)
        {
            int m = H.Length, n = H[0].Length, r, i;

            double[][] Q = MatrixInternalExtensions.JMatrix<double>(m, m),
                       temp = MatrixInternalExtensions.JMatrix<double>(m, n);

            HessenbergExtensions.to_hessenberg_form(Q, H);
            IDenseQRDecompositionAlgorithm qr = new HouseholderQRDecompositionAlgorithm();

            double[][] V = Q.Copy(), Vtemp = MatrixInternalExtensions.JMatrix<double>(m, m);

            for (i = 0; i < _iterations; i++)
            {
                //QRDecomposition.qr_decompose_unsafe(Q, H, true);
                qr.QRDecompose(null, Q, H, true, false, false);

                // H *= Q
                H.multiply_unsafe(Q, temp);
                H.CopyFrom(temp);

                if (V == null)
                {
                    V.CopyFrom(Q);
                }
                else
                {
                    V.multiply_unsafe(Q, Vtemp);
                    V.CopyFrom(Vtemp);
                }
            }

            // Fill in the eigenvalues from the Real-Schur form, checking for block diagonals
            int e = 0;
            for (r = 1; r <= m; ++r)
            {
                // A block diagonal exists (r, c, 1, 1)
                if (r < m && !Util.ApproximatelyEquals(H[r][r - 1], 0, 1e-8))
                {
                    calc_eigenvalues(H[r - 1][r - 1], H[r - 1][r], H[r][r - 1], H[r][r], out Complex l1, out Complex l2);

                    eigenvalues[e++] = l1;
                    eigenvalues[e++] = l2;
                    r++;

                    // Eigenvector extraction - not implemented in this pure eigenvalue method
                    //V.Column(r - 1, column); 
                    //V.Column(r, column);
                }

                else
                {
                    eigenvalues[e++] = new Complex(H[r - 1][r - 1]);

                    // Eigenvector extraction - not implemented in this pure eigenvalue method
                    //V.Column(r - 1, column);
                }
            }
        }

        internal override void CalculateEigenvalues(float[][] A, Complex[] eigenvalues)
        {
            throw new NotImplementedException();
        }

        internal override void CalculateEigenvalues(decimal[][] A, Complex[] eigenvalues)
        {
            throw new NotImplementedException();
        }

        internal override void CalculateEigenvalues(Complex[][] A, Complex[] eigenvalues)
        {
            throw new NotImplementedException();
        }
    }
}
