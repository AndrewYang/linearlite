﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Decomposition
{
    public static class JordanNormalDecomposition
    {
        /// <summary>
        /// Decomposes a real square matrix A into its Jordan normal form, i.e. A = PJP^-1 
        /// where P and J are square matrices, P is upper triangular and J is a Jordan 
        /// matrix.
        /// 
        /// Note that in general the computation of a Jordan structure is a ill 
        /// posed problem, in that there exist matrices for which the application
        /// of a small perturbation results in a non-smooth change in the computed
        /// Jordan normal form. 
        /// 
        /// For a similar but more numerically stable factorisation, please see Schur's 
        /// decomposition.
        /// 
        /// </summary>
        /// <param name="A">The matrix to be decomposed. Must be square and real.</param>
        /// <param name="J">The Jordan matrix</param>
        /// <param name="P">The upper triangular matrix</param>
        public static void JordanNormalDecompose(this double[,] A, out double[,] J, out double[,] P)
        {
            MatrixChecks.CheckNotNull(A);
            MatrixChecks.CheckIsSquare(A);

            throw new NotImplementedException();
        }
    }
}
