﻿using LinearNet.Providers;
using LinearNet.Matrices.Decompositions.LU;
using LinearNet.Structs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Decomposition
{
    public static class LUDecomposition
    {
        private const LUDecompositionMethod DEFAULT = LUDecompositionMethod.DOOLITTLE;

        private static IDenseLUDecompositionAlgorithm GetDefaultAlgorithm(LUDecompositionMethod method)
        {
            switch (method)
            {
                case LUDecompositionMethod.DOOLITTLE: return new DoolittleLUDecomposition();
                case LUDecompositionMethod.CAMARERO: return new CamareroBlockLUDecomposition();
            }
            throw new NotSupportedException();
        }
        /// <summary>
        /// <para>
        /// Calculate the LU decomposition using the Doolittle algorithm on matrix $A$.
        /// </para>
        /// <para>
        /// Factorises square matrix $A = LU$ where $L$ is a lower triangular matrix and $U$ is a upper triangular matrix.
        /// </para>
        /// 
        /// <!--inputs-->
        /// </summary>
        /// <name>LUDecompose</name>
        /// <proto>void LUDecompose(this IMatrix<T> A, out IMatrix<T> L, out IMatrix<T> U)</proto>
        /// <cat>la</cat>
        /// <param name="A">The $m \times m$ matrix $A$ to be decomposed.</param>
        /// <param name="L">
        /// <b>Out parameter</b><br/>
        /// The $m \times m$ lower triangular matrix factor $L$.</param>
        /// <param name="U">
        /// <b>Out parameter</b><br/>
        /// The $m \times m$ upper triangular matrix factor $U$.</param>
        public static void LUDecompose(this DenseMatrix<float> A, out DenseMatrix<float> L, out DenseMatrix<float> U, bool upperUnitDiagonal = false, LUDecompositionMethod method = DEFAULT)
        {
            var algo = GetDefaultAlgorithm(method);
            algo.LUDecompose(A, out L, out U, upperUnitDiagonal, new FloatProvider(), algo.LUDecompose);
        }
        public static void LUDecompose(this DenseMatrix<double> A, out DenseMatrix<double> L, out DenseMatrix<double> U, bool upperUnitDiagonal = false, LUDecompositionMethod method = DEFAULT)
        {
            var algo = GetDefaultAlgorithm(method);
            algo.LUDecompose(A, out L, out U, upperUnitDiagonal, new DoubleProvider(), algo.LUDecompose);
        }
        public static void LUDecompose(this DenseMatrix<decimal> A, out DenseMatrix<decimal> L, out DenseMatrix<decimal> U, bool upperUnitDiagonal = false, LUDecompositionMethod method = DEFAULT)
        {
            var algo = GetDefaultAlgorithm(method);
            algo.LUDecompose(A, out L, out U, upperUnitDiagonal, new DecimalProvider(), algo.LUDecompose);
        }
        public static void LUDecompose(this DenseMatrix<Complex> A, out DenseMatrix<Complex> L, out DenseMatrix<Complex> U, bool upperUnitDiagonal = false, LUDecompositionMethod method = DEFAULT)
        {
            /*
            MatrixChecks.CheckNotNull(A);
            MatrixChecks.CheckIsSquare(A);

            int dim = A.Rows, r, c, i;

            U = new DenseMatrix<Complex>(dim, dim);
            L = DenseMatrix.Identity<Complex>(dim);

            for (r = 0; r < dim; ++r)
            {
                Complex[] L_r = L[r], U_r = U[r], A_r = A[r];
                for (c = r; c < dim; ++c)
                {
                    // U(r, c) = A(i, k) - sum[L(r, i) * U(i, c), j from 0 to i]
                    double sum_re = 0.0, sum_im = 0.0;
                    for (i = 0; i < r; ++i)
                    {
                        Complex a = L_r[i], b = U[i][c];
                        sum_re += a.Real * b.Real - a.Imaginary * b.Imaginary;
                        sum_im += a.Imaginary * b.Real + a.Real * b.Imaginary;
                    }

                    U_r[c].Real = A_r[c].Real - sum_re;
                    U_r[c].Imaginary = A_r[c].Imaginary - sum_im;
                }

                // Lower is traversed in transposed mode
                Complex one_over_u = Complex.One.Divide(U_r[r]);
                for (c = r + 1; c < dim; ++c)
                {
                    Complex[] L_c = L[c];
                    // L(c, r) = A(k, r) - sum[L(c, i) * U(i, r), i from 0 to r] / U(r, r)
                    double sum_re = 0.0, sum_im = 0.0;
                    for (i = 0; i < r; ++i)
                    {
                        Complex a = L_c[i], b = U[i][r];
                        sum_re += a.Real * b.Real - a.Imaginary * b.Imaginary;
                        sum_im += a.Imaginary * b.Real + a.Real * b.Imaginary;
                    }
                        
                    L_c[r] = A[c][r].Subtract(new Complex(sum_re, sum_im)).Multiply(one_over_u);
                }
            }*/
            var algo = GetDefaultAlgorithm(method);
            algo.LUDecompose(A, out L, out U, upperUnitDiagonal, new ComplexProvider(), algo.LUDecompose);
        }

        /// <summary>
        /// <para>Calculate the LUP decomposition (with partial pivoting) of a $m \times m$ square matrix $A$, such that $$PA = LU,$$ where</para>
        /// <ul>
        /// <li>$L$ is a $m \times m$ lower triangular matrix,</li>
        /// <li>$U$ is a $m \times m$ upper triangular matrix,</li>
        /// <li>$P$ is a $m \times m$ row permutation matrix.</li>
        /// </ul>
        /// <para>$P$ is chosen so as to maximize the numerical stability of the calculated matrices $U$ and $V$.</para>
        /// <para>
        /// Also see:
        /// <ul>
        /// <li><a href="#LUDecompose"><txt>LUDecompose(this IMatrix&lt;T&gt; A, out IMatrix&lt;T&gt; L, out IMatrix&lt;T&gt; U)</txt></a></li>
        /// <li><a href="#LUPDecomposeLiteral"><txt>LUPDecompose(this IMatrix&lt;T&gt; A, out IMatrix&lt;T&gt; L, out IMatrix&lt;T&gt; U, out int[] P)</txt></a></li>
        /// </ul>
        /// </para>
        /// <!--inputs-->
        /// 
        /// </summary>
        /// <name>LUPDecompose</name>
        /// <proto>void LUPDecompose(this IMatrix<T> A, int[] P)</proto>
        /// <cat>la</cat>
        /// <param name="A">
        /// The square matrix to be decomposed, whose leading principal minors are all non-zero. <br/>
        /// The calculated matrices $L$ and $U$ will be stored in this matrix.
        /// <ul>
        /// <li>The entries in <txt>A</txt> along the main diagonal and above will be equal to the corresponding entries of $U$.</li>
        /// <li>The entries in <txt>A</txt> below the main diagonal will be equal to the corresponding entries of $L$. The diagonal elements of $L$ are assumed to all equal 1.</li>
        /// </ul>        
        /// </param>
        /// <param name="P">The row permutation matrix $P$ stored as a $m$-dimensional vector of permutations $v$, where $v_i = \pi (i)$.</param>
        public static void LUPDecompose(this DenseMatrix<float> A, int[] P, LUDecompositionMethod method = DEFAULT) => GetDefaultAlgorithm(method).LUPDecompose(A.Values, P);
        public static void LUPDecompose(this DenseMatrix<double> A, int[] P, LUDecompositionMethod method = DEFAULT) => GetDefaultAlgorithm(method).LUPDecompose(A.Values, P);
        public static void LUPDecompose(this DenseMatrix<decimal> A, int[] P, LUDecompositionMethod method = DEFAULT) => GetDefaultAlgorithm(method).LUPDecompose(A.Values, P);
        public static void LUPDecompose(this DenseMatrix<Complex> A, int[] P, LUDecompositionMethod method = DEFAULT) => GetDefaultAlgorithm(method).LUPDecompose(A.Values, P);

        /// <summary>
        /// Perform a LUP decomposition of a square matrix. 
        /// 
        /// <!--inputs-->
        /// 
        /// <h4>Example</h4>
        /// <pre><code class="cs">
        /// DenseMatrix&lt;Complex&gt; A = DenseMatrix.Random&lt;Complex&gt;(10, 10);
        /// A.LUPDecompose(out DenseMatrix&lt;Complex&gt; L, out DenseMatrix&lt;Complex&gt; U, out int[] P);
        /// 
        /// A.PermuteRows(P);
        /// Console.WriteLine(L.IsLowerTriangular()); // true
        /// Console.WriteLine(R.IsUpperTriangular()); // true
        /// Console.WriteLine(A.ApproximatelyEquals(L.Multiply(U)); // true
        /// 
        /// </code></pre>
        /// </summary>
        /// <name>LUPDecomposeLiteral</name>
        /// <proto>void LUPDecompose(this DenseMatrix<T> A, out DenseMatrix<T> L, out DenseMatrix<T> U, out int[] P, bool upperUnitDiagonal = false)</proto>
        /// <cat>la</cat>
        /// <param name="A">A $m \times m$ square matrix to be decomposed.</param>
        /// <param name="L">
        /// <b>Out parameter</b><br/>
        /// A $m \times m$ lower triangular matrix, $L$.<br/>
        /// If <txt>upperUnitDiagonal</txt> is <txt>false</txt>, the elements along its main diagonal will be 1.
        /// </param>
        /// <param name="U">
        /// <b>Out parameter</b><br/>
        /// A $m \times m$ upper triangular matrix, $R$.<br/>
        /// If <txt>upperUnitDiagonal</txt> is <txt>true</txt>, the elements along its main diagonal will be 1.
        /// </param>
        /// <param name="P">
        /// <b>Out parameter</b><br/>
        /// A $m$-dimensional vector representing the row permutations applied to matrix $A$.
        /// </param>
        /// <param name="upperUnitDiagonal">
        /// <b>Optional</b>, defaults to <txt>false</txt>.<br/>
        /// If <txt>true</txt>, the upper matrix $U$ will have a unitary main diagonal. Otherwise the lower matrix $L$ will have a unitary diagonal.</param>
        public static void LUPDecompose(this DenseMatrix<float> A, out DenseMatrix<float> L, out DenseMatrix<float> U, out int[] P, bool upperUnitDiagonal = false, LUDecompositionMethod method = DEFAULT)
        {
            var algo = GetDefaultAlgorithm(method);
            algo.LUPDecompose(A, out L, out U, out P, upperUnitDiagonal, new FloatProvider(), algo.LUPDecompose);
        }
        public static void LUPDecompose(this DenseMatrix<double> A, out DenseMatrix<double> L, out DenseMatrix<double> U, out int[] P, bool upperUnitDiagonal = false, LUDecompositionMethod method = DEFAULT)
        {
            var algo = GetDefaultAlgorithm(method);
            algo.LUPDecompose(A, out L, out U, out P, upperUnitDiagonal, new DoubleProvider(), algo.LUPDecompose);
        }
        public static void LUPDecompose(this DenseMatrix<decimal> A, out DenseMatrix<decimal> L, out DenseMatrix<decimal> U, out int[] P, bool upperUnitDiagonal = false, LUDecompositionMethod method = DEFAULT)
        {
            var algo = GetDefaultAlgorithm(method);
            algo.LUPDecompose(A, out L, out U, out P, upperUnitDiagonal, new DecimalProvider(), algo.LUPDecompose);
        }
        public static void LUPDecompose(this DenseMatrix<Complex> A, out DenseMatrix<Complex> L, out DenseMatrix<Complex> U, out int[] P, bool upperUnitDiagonal = false, LUDecompositionMethod method = DEFAULT)
        {
            var algo = GetDefaultAlgorithm(method);
            algo.LUPDecompose(A, out L, out U, out P, upperUnitDiagonal, new ComplexProvider(), algo.LUPDecompose);
        }

        public static void LUDecompose(this float[,] A, out float[,] L, out float[,] U, bool upperUnitDiagonal = false, LUDecompositionMethod method = DEFAULT)
        {
            var algo = GetDefaultAlgorithm(method);
            algo.LUDecompose(A, out L, out U, upperUnitDiagonal, new FloatProvider(), algo.LUDecompose);
        }
        public static void LUDecompose(this double[,] A, out double[,] L, out double[,] U, bool upperUnitDiagonal = false, LUDecompositionMethod method = DEFAULT)
        {
            /*
            MatrixChecks.CheckNotNull(A);
            MatrixChecks.CheckIsSquare(A);

            int dim = A.GetLength(0), r, c, i;

            U = new double[dim, dim];
            L = RectangularMatrix.Identity<double>(dim);

            for (r = 0; r < dim; ++r)
            {
                for (c = r; c < dim; ++c)
                {
                    // U(r, c) = A(i, k) - sum[L(r, i) * U(i, c), j from 0 to i]
                    double sum = 0;
                    for (i = 0; i < r; ++i)
                        sum += L[r, i] * U[i, c];
                    U[r, c] = A[r, c] - sum;
                }

                // Lower is traversed in transposed mode
                double u = U[r, r];
                for (c = r + 1; c < dim; ++c)
                {
                    // L(c, r) = A(k, r) - sum[L(c, i) * U(i, r), i from 0 to r] / U(r, r)
                    double sum = 0;
                    for (i = 0; i < r; ++i)
                        sum += L[c, i] * U[i, r];
                    L[c, r] = (A[c, r] - sum) / u;
                }
            }*/

            var algo = GetDefaultAlgorithm(method);
            algo.LUDecompose(A, out L, out U, upperUnitDiagonal, new DoubleProvider(), algo.LUDecompose);
        }
        public static void LUDecompose(this decimal[,] A, out decimal[,] L, out decimal[,] U, bool upperUnitDiagonal = false, LUDecompositionMethod method = DEFAULT)
        {
            var algo = GetDefaultAlgorithm(method);
            algo.LUDecompose(A, out L, out U, upperUnitDiagonal, new DecimalProvider(), algo.LUDecompose);
        }
        public static void LUDecompose(this Complex[,] A, out Complex[,] L, out Complex[,] U, bool upperUnitDiagonal = false, LUDecompositionMethod method = DEFAULT)
        {
            /*
            MatrixChecks.CheckNotNull(A);
            MatrixChecks.CheckIsSquare(A);

            int dim = A.GetLength(0), r, c, i;

            U = new Complex[dim, dim];
            L = RectangularMatrix.Identity<Complex>(dim);

            for (r = 0; r < dim; ++r)
            {
                for (c = r; c < dim; ++c)
                {
                    // U(r, c) = A(i, k) - sum[L(r, i) * U(i, c), j from 0 to i]
                    Complex sum = Complex.Zero;
                    for (i = 0; i < r; ++i)
                        sum += L[r, i] * U[i, c];
                    U[r, c] = A[r, c] - sum;
                }

                // Lower is traversed in transposed mode
                Complex u = U[r, r];
                for (c = r + 1; c < dim; ++c)
                {
                    // L(c, r) = A(k, r) - sum[L(c, i) * U(i, r), i from 0 to r] / U(r, r)
                    Complex sum = Complex.Zero;
                    for (i = 0; i < r; ++i)
                        sum += L[c, i] * U[i, r];
                    L[c, r] = (A[c, r] - sum) / u;
                }
            }*/

            var algo = GetDefaultAlgorithm(method);
            algo.LUDecompose(A, out L, out U, upperUnitDiagonal, new ComplexProvider(), algo.LUDecompose);
        }


        public static void LUDecomposeParallel(this DenseMatrix<float> A, out DenseMatrix<float> L, out DenseMatrix<float> U, bool upperUnitDiagonal = false, LUDecompositionMethod method = DEFAULT)
        {
            var algo = GetDefaultAlgorithm(method);
            algo.LUDecompose(A, out L, out U, upperUnitDiagonal, new FloatProvider(), algo.LUDecomposeParallel);
        }
        public static void LUDecomposeParallel(this DenseMatrix<double> A, out DenseMatrix<double> L, out DenseMatrix<double> U, bool upperUnitDiagonal = false, LUDecompositionMethod method = DEFAULT)
        {
            var algo = GetDefaultAlgorithm(method);
            algo.LUDecompose(A, out L, out U, upperUnitDiagonal, new DoubleProvider(), algo.LUDecomposeParallel);
        }
        public static void LUDecomposeParallel(this DenseMatrix<decimal> A, out DenseMatrix<decimal> L, out DenseMatrix<decimal> U, bool upperUnitDiagonal = false, LUDecompositionMethod method = DEFAULT)
        {
            var algo = GetDefaultAlgorithm(method);
            algo.LUDecompose(A, out L, out U, upperUnitDiagonal, new DecimalProvider(), algo.LUDecomposeParallel);
        }
        public static void LUDecomposeParallel(this DenseMatrix<Complex> A, out DenseMatrix<Complex> L, out DenseMatrix<Complex> U, bool upperUnitDiagonal = false, LUDecompositionMethod method = DEFAULT)
        {
            var algo = GetDefaultAlgorithm(method);
            algo.LUDecompose(A, out L, out U, upperUnitDiagonal, new ComplexProvider(), algo.LUDecomposeParallel);
        }
    }
    public enum LUDecompositionMethod
    {
        DOOLITTLE, CAMARERO
    }
}
