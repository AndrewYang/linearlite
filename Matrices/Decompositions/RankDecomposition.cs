﻿using LinearNet.Matrices;
using LinearNet.Structs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Decompositions
{
    public static class RankDecomposition
    {
        /// <summary>
        /// <para>
        /// Calculates the rank decomposition of rank-$r$ matrix $A\in\mathbb{F}^{m \times n}$ into $$A = CF,$$ 
        /// where $C\in\mathbb{F}^{m \times r}$ and $F\in\mathbb{F}^{r \times n}$.
        /// </para>
        /// <para><b>Example:</b></para>
        /// <pre><code class="cs">
        /// // Create a random 10 x 15 real matrix of rank 3
        /// DenseMatrix&lt;double&gt; matrix = DenseMatrix.RandomLowRankMatrix&lt;double&gt;(10, 15, 3); 
        /// DenseMatrix&lt;double&gt; C, F;
        /// matrix.RankDecompose(out C, out F); // Perform a low-rank decomposition
        /// 
        /// Console.WriteLine(C.Rows + ", " + C.Columns);   // 10, 3
        /// Console.WriteLine(F.Rows + ", " + F.Rows);      // 3, 15
        /// Console.WriteLine(C.Multiply(F).ApproximatelyEquals(matrix)); // true
        /// 
        /// </code></pre>
        /// </summary>
        /// <name>RankDecompose</name>
        /// <proto>void RankDecompose(this DenseMatrix<T> A, out DenseMatrix<T> C, out DenseMatrix<T> F)</proto>
        /// <cat>la</cat>
        /// <param name="A"></param>
        /// <param name="C"></param>
        /// <param name="F"></param>
        public static void RankDecompose(this DenseMatrix<double> A, out DenseMatrix<double> C, out DenseMatrix<double> F)
        {
            DenseMatrix<double> re = A.ToRowEchelonForm();
            List<double[]> _c = new List<double[]>(), _f = new List<double[]>();

            int rows = A.Rows, columns = A.Columns, i, j = 0;
            for (i = 0; i < rows; ++ i)
            {
                int j_start = j;
                while (j < columns && re[i][j].approx_zero())
                {
                    j++;
                }

                // Is a pivot column
                if (j_start + 1 >= j)
                {
                    _c.Add(A.Column(j).Values);
                }
                if (j == columns)
                {
                    break;
                }
                _f.Add(re[i]);
            }

            C = new DenseMatrix<double>(_c.ToArray().Transpose());
            F = new DenseMatrix<double>(_f.ToArray());
        }
    }
}
