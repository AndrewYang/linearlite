﻿using LinearNet.Providers;
using LinearNet.Matrices;
using LinearNet.Matrices.Decompositions.QR;
using LinearNet.Structs;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Decomposition
{
    public static class SVDecomposition
    {
        public static void SingularValueDecompose(this double[,] A, out double[,] U, out double[,] D, out double[,] V)
        {
            MatrixChecks.CheckNotNull(A);

            int m = A.GetLength(0), n = A.GetLength(1);

            A.ToBidiagonalForm(out double[,] UPrime, out double[,] B, out double[,] VPrime, false, true, BidiagonalizationMethod.HOUSEHOLDER_TRANSFORM);

            A.Print();

            U = RectangularMatrix.Identity<double>(m);
            D = new double[m, n];
            V = RectangularMatrix.Identity<double>(n);

            for (int i = 0; i < 2; ++i)
            {
                B.QRDecompose(out double[,] u, out double[,] R, true);
                R.LQDecompose(out double[,] d, out double[,] v, true);

                B = D;
                U = U.Multiply(u);
                D = d;
                V = v.Multiply(V);

                B.Print();
            }
        }

        /// <summary>
        /// Decompose a $m \times n$ matrix $A$ into $$A = UDV^*,$$ 
        /// where
        /// <ul>
        /// <li>$U$ is a $m \times m$ orthogonal/unitary matrix, the set of left singular vectors.</li>
        /// <li>$D$ is a $m \times n$ diagonal matrix with non-negative elements, the singular values of $A$.</li>
        /// <li>$V$ is a $n \times n$ orthogonal/unitary matrix, the set of right singular vectors.</li>
        /// </ul>
        /// and $V^*$ is the conjugate transpose of $V$.
        /// 
        /// <!--inputs-->
        /// 
        /// <para><b>Example</b></para>
        /// <pre><code class="cs">
        /// // Create a random 10 x 15 real matrix
        /// DenseMatrix&lt;double&gt; matrix = DenseMatrix.Random&lt;double&gt;(10, 15); 
        /// DenseMatrix&lt;double&gt; U, D, V;
        /// matrix.SingularValueDecompose(out U, out D, out V); // Perform a SVD
        /// 
        /// Console.WriteLine(U.IsOrthogonal()); // true
        /// Console.WriteLine(D.IsDiagonal());   // true
        /// Console.WriteLine(V.IsOrthogonal()); // true
        /// 
        /// </code></pre>
        /// </summary>
        /// <name>SingularValueDecompose</name>
        /// <proto>void SingularValueDecompose(this IMatrix<T> A, out IMatrix<T> U, out IMatrix<T> D, out IMatrix<T> V)</proto>
        /// <cat>la</cat>
        /// <param name="A">The $m \times n$ matrix $A$ to be decomposed.</param>
        /// <param name="U">
        /// <b>Out parameter</b><br/>
        /// The $m \times m$ orthogonal/unitary matrix $U$ of left singular vector matrix.
        /// </param>
        /// <param name="D">
        /// <b>Out parameter</b><br/>
        /// The $m \times n$ rectangular diagonal matrix $D$ of singular values.
        /// </param>
        /// <param name="V">
        /// <b>Out parameter</b><br/>
        /// The $n \times n$ orthogonal/unitary matrix $V$ of right singular vector matrix.
        /// </param>
        public static void SingularValueDecompose(this DenseMatrix<float> A, out DenseMatrix<float> U, out DenseMatrix<float> D, out DenseMatrix<float> V)
        {
            MatrixChecks.CheckNotNull(A);

            A.ToBidiagonalForm(out U, out D, out V, 
                upper:false, 
                thin:true, 
                preserveMatrix:true, 
                method:BidiagonalizationMethod.HOUSEHOLDER_TRANSFORM);

            float termination_norm = 1e-8f;
            U.Values.TransposeInPlace();
            V.Values.TransposeInPlace();

            IDenseQRDecompositionAlgorithm qr = new HouseholderQRDecompositionAlgorithm();
            float[][] Dt = MatrixInternalExtensions.JMatrix<float>(D.Columns, D.Rows);
            for (int i = 0; i < 1000; ++i)
            {
                qr.QRDecomposeHessenberg(U.Values, D.Values, false, false);
                CopyUpperBidiagonalToTranspose(D.Values, Dt, 0.0f);

                qr.QRDecomposeHessenberg(V.Values, Dt, false, false);
                CopyUpperBidiagonalToTranspose(Dt, D.Values, 0.0f);

                float norm = OffDiagonalNorm(D);
                if (norm <= termination_norm)
                {
                    Debug.WriteLine(i);
                    break;
                }
            }

            // Ensure diagonal is always positive
            EnforcePositiveDiagonals(U.Values, D.Values, new FloatProvider());
        }
        public static void SingularValueDecompose(this DenseMatrix<double> A, out DenseMatrix<double> U, out DenseMatrix<double> D, out DenseMatrix<double> V)
        {
            MatrixChecks.CheckNotNull(A);

            A.ToBidiagonalForm(out U, out D, out V, 
                upper:false, 
                thin:true,
                preserveMatrix:true, 
                method:BidiagonalizationMethod.HOUSEHOLDER_TRANSFORM);

            double termination_norm = 1e-16;
            U.Values.TransposeInPlace();
            V.Values.TransposeInPlace();

            IDenseQRDecompositionAlgorithm qr = new HouseholderQRDecompositionAlgorithm();
            double[][] Dt = MatrixInternalExtensions.JMatrix<double>(D.Columns, D.Rows);
            for (int i = 0; i < 1000; ++i)
            {
                qr.QRDecomposeHessenberg(U.Values, D.Values, false, false);
                CopyUpperBidiagonalToTranspose(D.Values, Dt, 0.0);

                qr.QRDecomposeHessenberg(V.Values, Dt, false, false);
                CopyUpperBidiagonalToTranspose(Dt, D.Values, 0.0);

                double norm = OffDiagonalNorm(D);
                if (norm <= termination_norm)
                {
                    Debug.WriteLine(i);
                    break;
                }
            }

            // Ensure diagonal is always positive
            EnforcePositiveDiagonals(U.Values, D.Values, new DoubleProvider());
        }
        public static void SingularValueDecompose(this DenseMatrix<decimal> A, out DenseMatrix<decimal> U, out DenseMatrix<decimal> D, out DenseMatrix<decimal> V)
        {
            MatrixChecks.CheckNotNull(A);

            A.ToBidiagonalForm(out U, out D, out V, 
                upper:false, 
                thin:true, 
                preserveMatrix:true, 
                method:BidiagonalizationMethod.HOUSEHOLDER_TRANSFORM);

            decimal termination_norm = 1e-60m;
            U.Values.TransposeInPlace();
            V.Values.TransposeInPlace();

            IDenseQRDecompositionAlgorithm qr = new HouseholderQRDecompositionAlgorithm();
            decimal[][] Dt = MatrixInternalExtensions.JMatrix<decimal>(D.Columns, D.Rows);
            for (int i = 0; i < 1000; ++i)
            {
                qr.QRDecomposeHessenberg(U.Values, D.Values, false, false);
                CopyUpperBidiagonalToTranspose(D.Values, Dt, 0.0m);

                qr.QRDecomposeHessenberg(V.Values, Dt, false, false);
                CopyUpperBidiagonalToTranspose(Dt, D.Values, 0.0m);

                decimal norm = OffDiagonalNorm(D);
                if (norm <= termination_norm)
                {
                    Debug.WriteLine(i);
                    break;
                }
            }

            // Ensure diagonal is always positive
            EnforcePositiveDiagonals(U.Values, D.Values, new DecimalProvider());
        }
        public static void SingularValueDecompose(this DenseMatrix<Complex> A, out DenseMatrix<Complex> U, out DenseMatrix<Complex> D, out DenseMatrix<Complex> V)
        {
            MatrixChecks.CheckNotNull(A);

            A.ToBidiagonalForm(out U, out D, out V, 
                upper:false, 
                thin:true, 
                preserveMatrix:true, 
                method:BidiagonalizationMethod.HOUSEHOLDER_TRANSFORM);

            double termination_norm = 1e-16;
            U.Values.SelfConjugateTranspose();
            V.Values.SelfConjugateTranspose();

            IDenseQRDecompositionAlgorithm qr = new HouseholderQRDecompositionAlgorithm();
            Complex[][] Dt = MatrixInternalExtensions.JMatrix<Complex>(D.Columns, D.Rows);
            for (int i = 0; i < 1000; ++i)
            {
                qr.QRDecomposeHessenberg(U.Values, D.Values, false, false);
                CopyUpperBidiagonalToConjugateTranspose(D.Values, Dt);

                qr.QRDecomposeHessenberg(V.Values, Dt, false, false);
                CopyUpperBidiagonalToConjugateTranspose(Dt, D.Values);

                double norm = OffDiagonalNorm(D);
                if (norm <= termination_norm)
                {
                    Debug.WriteLine(i);
                    break;
                }
            }

            // Ensure diagonal is real and positive
            // Set the diagonal = the modulus of the existing pseudo singular-value
            int m = D.Rows, n = D.Columns, min = Math.Min(m, n), d;
            for (d = 0; d < min; ++d)
            {
                Complex z = D[d][d];
                double modulus = z.Modulus();

                // The singular value is always real, set it to 
                // |z| which is equivalent to multiplying by |z| / z
                D[d][d] = new Complex(modulus, 0);

                // Correct for the multiplication of |z| / z by multiplying U 
                // terms by z / |z| = unit(z)
                z.MultiplyEquals(1.0 / modulus);
                for (int i = 0; i < m; ++i)
                {
                    U[i][d] = U[i][d].Multiply(z);
                }
            }
        }

        public static float[] SingularValues(this DenseMatrix<float> A, bool preserveMatrix = true, int maxIterations = 1000, float terminationNorm = 1e-8f)
            => SingularValues(A.Values, preserveMatrix, maxIterations, terminationNorm);
        public static double[] SingularValues(this DenseMatrix<double> A, bool preserveMatrix = true, int maxIterations = 1000, double terminationNorm = 1e-16)
        {
            return SingularValues(A.Values, preserveMatrix, maxIterations, terminationNorm);
            //return SingularValuesGivensRotations(A.Values, preserveMatrix, maxIterations, terminationNorm);
        }
        public static decimal[] SingularValues(this DenseMatrix<decimal> A, bool preserveMatrix = true, int maxIterations = 1000, decimal terminationNorm = 1e-60m)
            => SingularValues(A.Values, preserveMatrix, maxIterations, terminationNorm);
        public static Complex[] SingularValues(this DenseMatrix<Complex> A, bool preserveMatrix = true, int maxIterations = 1000, double terminationNorm = 1e-16)
            => SingularValues(A.Values, preserveMatrix, maxIterations, terminationNorm);

        internal static float[] SingularValues(float[][] A, bool preserveMatrix = true, int maxIterations = 1000, float terminationNorm = 1e-8f)
        {
            MatrixChecks.CheckNotNull(A);

            float[][] D;
            if (preserveMatrix)
            {
                D = MatrixInternalExtensions.JMatrix<float>(A.Length, A[0].Length);
                D.CopyFrom(A);
            }
            else
            {
                D = A;
            }

            D.Bidiagonalize(true);

            IDenseQRDecompositionAlgorithm qr = new HouseholderQRDecompositionAlgorithm();
            float[][] Dt = MatrixInternalExtensions.JMatrix<float>(A[0].Length, A.Length);

            for (int i = 0; i < maxIterations; ++i)
            {
                qr.QRDecomposeHessenberg(D, false);
                CopyUpperBidiagonalToTranspose(D, Dt, 0.0f);

                qr.QRDecomposeHessenberg(Dt, false);
                CopyUpperBidiagonalToTranspose(Dt, D, 0.0f);

                if (OffDiagonalNorm(D) <= terminationNorm)
                {
                    Debug.WriteLine(i);
                    break;
                }
            }

            float[] svs = Diagonal(D);
            for (int i = 0; i < svs.Length; ++i)
            {
                svs[i] = Math.Abs(svs[i]);
            }
            return svs;
        }
        internal static double[] SingularValues(double[][] A, bool preserveMatrix = true, int maxIterations = 1000, double terminationNorm = 1e-16)
        {
            MatrixChecks.CheckNotNull(A);

            double[][] D;
            if (preserveMatrix)
            {
                D = MatrixInternalExtensions.JMatrix<double>(A.Length, A[0].Length);
                D.CopyFrom(A);
            }
            else
            {
                D = A;
            }

            D.Bidiagonalize(true);

            IDenseQRDecompositionAlgorithm qr = new HouseholderQRDecompositionAlgorithm();
            double[][] Dt = MatrixInternalExtensions.JMatrix<double>(A[0].Length, A.Length);

            for (int i = 0; i < maxIterations; ++i)
            {
                qr.QRDecomposeHessenberg(D, false);
                CopyUpperBidiagonalToTranspose(D, Dt, 0.0);

                qr.QRDecomposeHessenberg(Dt, false);
                CopyUpperBidiagonalToTranspose(Dt, D, 0.0);

                if (OffDiagonalNorm(D) <= terminationNorm)
                {
                    Debug.WriteLine(i);
                    break;
                }
            }

            double[] svs = Diagonal(D);
            for (int i = 0; i < svs.Length; ++i)
            {
                svs[i] = Math.Abs(svs[i]);
            }
            return svs;
        }
        internal static decimal[] SingularValues(decimal[][] A, bool preserveMatrix = true, int maxIterations = 1000, decimal terminationNorm = 1e-60m)
        {
            MatrixChecks.CheckNotNull(A);

            decimal[][] D;
            if (preserveMatrix)
            {
                D = MatrixInternalExtensions.JMatrix<decimal>(A.Length, A[0].Length);
                D.CopyFrom(A);
            }
            else
            {
                D = A;
            }

            D.Bidiagonalize(true);

            IDenseQRDecompositionAlgorithm qr = new HouseholderQRDecompositionAlgorithm();
            decimal[][] Dt = MatrixInternalExtensions.JMatrix<decimal>(A[0].Length, A.Length);

            for (int i = 0; i < maxIterations; ++i)
            {
                qr.QRDecomposeHessenberg(D, false);
                CopyUpperBidiagonalToTranspose(D, Dt, 0.0m);

                qr.QRDecomposeHessenberg(Dt, false);
                CopyUpperBidiagonalToTranspose(Dt, D, 0.0m);

                if (OffDiagonalNorm(D) <= terminationNorm)
                {
                    Debug.WriteLine(i);
                    break;
                }
            }

            decimal[] svs = Diagonal(D);
            for (int i = 0; i < svs.Length; ++i)
            {
                svs[i] = Math.Abs(svs[i]);
            }
            return svs;
        }
        internal static Complex[] SingularValues(Complex[][] A, bool preserveMatrix = true, int maxIterations = 1000, double terminationNorm = 1e-16)
        {
            MatrixChecks.CheckNotNull(A);

            Complex[][] D;
            if (preserveMatrix)
            {
                D = MatrixInternalExtensions.JMatrix<Complex>(A.Length, A[0].Length);
                D.CopyFrom(A);
            }
            else
            {
                D = A;
            }

            D.Bidiagonalize(true);

            IDenseQRDecompositionAlgorithm qr = new HouseholderQRDecompositionAlgorithm();
            Complex[][] Dt = MatrixInternalExtensions.JMatrix<Complex>(A[0].Length, A.Length);

            Complex zero = Complex.Zero;
            for (int i = 0; i < maxIterations; ++i)
            {
                qr.QRDecomposeHessenberg(D, false);
                CopyUpperBidiagonalToTranspose(D, Dt, zero);

                qr.QRDecomposeHessenberg(Dt, false);
                CopyUpperBidiagonalToTranspose(Dt, D, zero);

                if (OffDiagonalNorm(D) <= terminationNorm)
                {
                    Debug.WriteLine(i);
                    break;
                }
            }

            Complex[] svs = new Complex[Math.Min(D.Length, D[0].Length)];
            for (int i = 0; i < svs.Length; ++i)
            {
                svs[i] = new Complex(D[i][i].Modulus(), 0);
            }
            return svs;
        }

        internal static double[] SingularValuesGivensRotations(double[][] A, bool preserveMatrix = true, int maxIterations = 1000, double terminationNorm = 1e-16)
        {
            MatrixChecks.CheckNotNull(A);

            int rows = A.Length, cols = A[0].Length, min = Math.Min(rows, cols);
            double[][] D;
            if (preserveMatrix)
            {
                D = MatrixInternalExtensions.JMatrix<double>(rows, cols);
                D.CopyFrom(A);
            }
            else
            {
                D = A;
            }

            D.Bidiagonalize(true);

            // Copy into vector
            double[] diagonal = new double[min], offDiagonal = new double[min];
            for (int i = 0; i < min; ++i)
            {
                diagonal[i] = D[i][i];
            }
            CopyUpperDiagonal(D, offDiagonal, rows, cols);

            Debug.WriteLine("D");
            D.ToRectangular().Print();
            Debug.WriteLine("diagonal");
            diagonal.Print();
            Debug.WriteLine("upper");
            offDiagonal.Print();
            
            // Store of the cos, sin givens rotation parameters
            double[] cs = new double[2];

            for (int i = 0; i < maxIterations; ++i)
            {
                // Always in the lower diagonal
                for (int d = 0; d < min; ++d)
                {
                    Util.Givens(diagonal[d], offDiagonal[d], cs);
                    double c = cs[0], s = cs[1];
                    double A_00 = diagonal[d], A_10 = offDiagonal[d];
                    diagonal[d] = c * A_00 + s * A_10;

                    if (d < min - 1)
                    {
                        double A_11 = diagonal[d + 1];
                        offDiagonal[d] = s * A_11;
                        diagonal[d + 1] = c * A_11;
                    }
                }

                // Virtual transposition check
                if (rows > cols && i % 2 == 1 || cols > rows && i % 2 == 0)
                {
                    int index = min - 1;
                    Util.Givens(diagonal[index], offDiagonal[index], cs);
                    double c = cs[0], s = cs[1];
                    diagonal[index] = c * diagonal[index] + s * offDiagonal[index];
                    
                }
                /*
                Debug.WriteLine("diagonal");
                diagonal.Print();
                Debug.WriteLine("upper");
                offDiagonal.Print();
                */

                if (OffDiagonalNorm(D) <= terminationNorm)
                {
                    Debug.WriteLine(i);
                    break;
                }
            }

            double[] svs = Diagonal(D);
            for (int i = 0; i < svs.Length; ++i)
            {
                svs[i] = Math.Abs(svs[i]);
            }
            return svs;
        }
        // index = 0: main diagonal, 1 = upper diagonal, -1 = lower diagonal
        private static void CopyUpperDiagonal<T>(T[][] matrix, T[] diagonal, int rows, int cols)
        {
            int end = cols > rows ? rows : cols - 1, i;
            for (i = 0; i < end; ++i)
            {
                diagonal[i] = matrix[i][i + 1];
            }
        }
        private static void CopyLowerDiagonal<T>(T[][] matrix, T[] diagonal, int rows, int cols)
        {
            int end = rows > cols ? cols : rows - 1, i;
            for (i = 0; i < end; ++i)
            {
                diagonal[i] = matrix[i + 1][i];
            }
        }

        private static void CopyUpperBidiagonalToTranspose<T>(T[][] D, T[][] Dt, T zero)
        {
            Dt[0][0] = D[0][0];

            int rows = D.Length, cols = D[0].Length, len = Math.Min(rows, cols), i;
            for (i = 1; i < len; ++i)
            {
                int _i = i - 1;
                T[] Dt_i = Dt[i];

                Dt[_i][i] = zero;
                Dt_i[i] = D[i][i];
                Dt_i[_i] = D[_i][i];
            }

            if (cols > rows)
            {
                Dt[len][len - 1] = D[len - 1][len];
            }
            if (rows > cols)
            {
                Dt[len - 1][len] = zero;
            }
        }
        private static void CopyUpperBidiagonalToConjugateTranspose(Complex[][] D, Complex[][] Dt)
        {
            Dt[0][0] = D[0][0].Conjugate();

            int rows = D.Length, cols = D[0].Length, len = Math.Min(rows, cols), i;
            for (i = 1; i < len; ++i)
            {
                int _i = i - 1;
                Complex[] Dt_i = Dt[i];

                Dt[_i][i] = Complex.Zero;
                Dt_i[i] = D[i][i].Conjugate();
                Dt_i[_i] = D[_i][i].Conjugate();
            }

            if (cols > rows)
            {
                Dt[len][len - 1] = D[len - 1][len].Conjugate();
            }
            if (rows > cols)
            {
                Dt[len - 1][len] = Complex.Zero;
            }
        }

        private static float OffDiagonalNorm(DenseMatrix<float> A)
        {
            int last = Math.Min(A.Columns, A.Rows - 1), i;
            float norm = 0.0f;
            for (i = 1; i <= last; ++i)
            {
                norm += Math.Abs(A[i][i - 1]);
            }
            return norm / last;
        }
        private static double OffDiagonalNorm(DenseMatrix<double> A)
        {
            int last = Math.Min(A.Columns, A.Rows - 1), i;
            double norm = 0.0;
            for (i = 1; i <= last; ++i)
            {
                norm += Math.Abs(A[i][i - 1]);
            }
            return norm / last;
        }
        private static decimal OffDiagonalNorm(DenseMatrix<decimal> A)
        {
            int last = Math.Min(A.Columns, A.Rows - 1), i;
            decimal norm = 0.0m;
            for (i = 1; i <= last; ++i)
            {
                norm += Math.Abs(A[i][i - 1]);
            }
            return norm / last;
        }
        private static double OffDiagonalNorm(DenseMatrix<Complex> A)
        {
            int last = Math.Min(A.Columns, A.Rows - 1), i;
            double norm = 0.0;
            for (i = 1; i <= last; ++i)
            {
                norm += A[i][i - 1].Modulus();
            }
            return norm / last;
        }

        private static T[] Diagonal<T>(T[][] A)
        {
            int min = Math.Min(A.Length, A[0].Length);
            T[] singularValues = new T[min];
            for (int i = 0; i < min; ++i)
            {
                singularValues[i] = A[i][i];
            }
            return singularValues;
        }
        private static void EnforcePositiveDiagonals<T>(T[][] U, T[][] D, IProvider<T> blas)
        {
            int m = D.Length, n = D[0].Length, min = Math.Min(m, n), d, i;
            for (d = 0; d < min; ++d)
            {
                if (blas.IsRealAndNegative(D[d][d]))
                {
                    D[d][d] = blas.Negate(D[d][d]);
                    for (i = 0; i < m; ++i)
                    {
                        U[i][d] = blas.Negate(U[i][d]);
                    }
                }
            }
        }
    }
}
