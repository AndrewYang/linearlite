﻿using LinearNet.Structs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Decompositions
{
    public static class SchurDecomposition
    {
        /// <summary>
        /// Calculates the Schur decomposition of matrix A = QU(Q^-1), where
        /// A is any real n x n matrix
        /// Q is a real n x n unitary matrix
        /// U is a real n x n upper diagonal matrix
        /// 
        /// Note that as Q is unitary and real, the Q^-1 = Q^T
        /// 
        /// The Schur decomposition is guaranteed to exist for all square matrices 
        /// (including defective matrices), however it is not guaranteed to be unique.
        /// 
        /// This implementation uses the QR algorithm
        /// </summary>
        /// <param name="A"></param>
        /// <param name="Q"></param>
        /// <param name="U"></param>
        public static void SchurDecompose(this double[,] A, out double[,] Q, out double[,] U)
        {
            MatrixChecks.CheckNotNull(A);
            MatrixChecks.CheckIsSquare(A);

            int n = A.GetLength(0);
            Q = new double[n, n];
            U = new double[n, n];
        }

        public static void SchurDecompose(this DenseMatrix<double> A, out DenseMatrix<double> Q, out DenseMatrix<double> U)
        {
            // Being lazy here - should try to stick to real decomposition?
            throw new NotImplementedException();
        }
    }
}
