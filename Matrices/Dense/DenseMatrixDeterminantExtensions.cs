﻿using LinearNet.Providers;
using LinearNet.Decomposition;
using LinearNet.Matrices.Decompositions.LU;
using LinearNet.Structs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LinearNet.Providers.LinearAlgebra;
using System.Diagnostics;

namespace LinearNet.Matrices
{
    public static class DenseMatrixDeterminantExtensions
    {
        #region Determinant calculation

        /// <summary>
        /// Calculates and returns the determinant of a square matrix A. The original matrix is unchanged.
        /// </summary>
        /// <param name="a">A non-degenerate, square matrix.</param>
        /// <returns>The determinant of the provided matrix.</returns>
        private static T calc_det_naive<T>(DenseMatrix<T> A, Func<DenseMatrix<T>, DenseMatrix<T>> ToRowEchelonForm, IProvider<T> blas) where T : new()
        {
            MatrixChecks.CheckNotNull(A);
            MatrixChecks.CheckIsSquare(A);

            DenseMatrix<T> copy = ToRowEchelonForm(A);

            T det = blas.One;
            int rows = copy.Rows, i;
            for (i = 0; i < rows; ++i)
            {
                det = blas.Multiply(det, copy[i][i]);
            }
            return det;
        }
        private static T calc_det_with_pivot<T>(DenseMatrix<T> A, IDenseBLAS1<T> blas, Func<T, double> Size) where T : new()
        {
            MatrixChecks.CheckNotNull(A);
            MatrixChecks.CheckIsSquare(A);

            IProvider<T> _p = blas.Provider;

            int m = A.Rows, product = 1, r, c;
            T[][] re = A.Copy().Values;

            for (c = 0; c < m; ++c)
            {
                T[] re_c = re[c];

                int largestRow = c;
                double largest = Size(re_c[c]);
                for (r = c + 1; r < m; ++r)
                {
                    double size = Size(re[r][c]);
                    if (size > largest)
                    {
                        largest = size;
                        largestRow = r;
                    }
                }

                if (largestRow != c)
                {
                    product *= -1;
                    re[c] = re[largestRow];
                    re[largestRow] = re_c;
                    re_c = re[c];
                }

                for (r = c + 1; r < m; ++r)
                {
                    T s = _p.Divide(re[r][c], re_c[c]);
                    blas.AXPY(re[r], re_c, _p.Negate(s), c, m);
                }
            }

            // If there are any extra rows - remove them
            T sum = _p.One;
            for (r = 0; r < m; ++r)
            {
                sum = _p.Multiply(sum, re[r][r]);
            }

            if (product < 0)
            {
                return _p.Negate(sum);
            }
            return sum;
        }

        /// <summary>
        /// Calculates and returns the determinant of a square matrix using implicit LU decomposition
        /// 
        /// TODO: this method has quite a high workspace requirement - how to implicitly compute 
        /// the diagonals of LU decomposition?
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="A"></param>
        /// <param name="blas"></param>
        /// <returns></returns>
        private static T calc_det_lud<T>(DenseMatrix<T> A, IDenseBLAS1<T> blas) where T : new()
        {
            MatrixChecks.CheckNotNull(A);
            MatrixChecks.CheckIsSquare(A);

            T[][] copy = A.Copy().Values;
            DoolittleLUDecomposition algo = new DoolittleLUDecomposition();
            algo.LUDecompose(copy, blas);

            // Here we evaluate the product using BLAS product that is stable for finite-precision arithmetic
            // O(n) additional time/workspace is a small price to pay in return for numerical stability
            T[] values = new T[A.Rows];
            for (int i = 0; i < A.Rows; ++i)
            {
                values[i] = copy[i][i];
            }
            return blas.Provider.Product(values);
        }

        private static T calc_det_finite_precision<T>(DenseMatrix<T> A, FinitePrecisionDeterminantMethod method, IDenseBLAS1<T> blas, Func<T, double> Size) where T : new()
        {
            if (method == FinitePrecisionDeterminantMethod.ECHELON_ROW_REDUCTION)
            {
                return calc_det_with_pivot(A, blas, Size);
            }
            else if (method == FinitePrecisionDeterminantMethod.LU_DECOMPOSITION)
            {
                return calc_det_lud(A, blas);
            }
            throw new NotImplementedException();
        }

        public static Rational ExactDeterminant(this DenseMatrix<int> A) => calc_det_with_pivot(A.ToRational(), new NativeFieldProvider<Rational>(), x => x.ToDouble());

        /// <summary>
        /// <para>
        /// Calculates the determinant of a square matrix, via row-echelon reduction or LU decomposition.
        /// <!--inputs-->
        /// 
        /// <h4>Example</h4>
        /// <pre><code class="cs">
        /// // Create a random PSD matrix 
        /// DenseMatrix&lt;double&gt; matrix = DenseMatrix.Random&lt;double&gt;(10, 10);
        /// 
        /// // Calculate the matrix determinant using pivoted row echelon reduction
        /// Console.WriteLine(matrix.Determinant());
        /// 
        /// // Calculate the matrix determinant using LU decomposition
        /// Console.WriteLine(matrix.Determinant(FinitePrecisionDeterminantMethod.LU_DECOMPOSITION));
        /// 
        /// </code></pre>
        /// </summary>
        /// <name>Determinant</name>
        /// <proto>T Determinant(this IMatrix<T> A, FinitePrecisionDeterminantMethod method)</proto>
        /// <cat>la</cat>
        /// <param name="A">A square matrix for which the determinant is to be calculated.</param>
        /// <param name="method">
        /// <b>Optional</b>, defaults to <txt>ECHELON_ROW_REDUCTION</txt>.<br/>
        /// The determinant calculation method. The current implementations are:
        /// <ul>
        /// <li><txt>ECHELON_ROW_REDUCTION</txt> - calculate the determinant by first reducing the matrix into its row-echelon form.</li>
        /// <li><txt>LU_DECOMPOSITION</txt> - calculate the determinant by calculating the matrix's LU decomposition.</li>
        /// </ul>
        /// </param>
        /// <returns></returns>
        public static double Determinant(this DenseMatrix<int> A, FinitePrecisionDeterminantMethod method = FinitePrecisionDeterminantMethod.ECHELON_ROW_REDUCTION)
            => calc_det_finite_precision(A.ToDouble(), method, new NativeDoubleProvider(), x => Math.Abs(x));

        public static double Determinant(this DenseMatrix<long> A, FinitePrecisionDeterminantMethod method = FinitePrecisionDeterminantMethod.ECHELON_ROW_REDUCTION)
            => calc_det_finite_precision(A.ToDouble(), method, new NativeDoubleProvider(), x => Math.Abs(x));

        public static float Determinant(this DenseMatrix<float> A, FinitePrecisionDeterminantMethod method = FinitePrecisionDeterminantMethod.ECHELON_ROW_REDUCTION)
            => calc_det_finite_precision(A, method, new NativeFloatProvider(), x => Math.Abs(x));

        public static double Determinant(this DenseMatrix<double> A, FinitePrecisionDeterminantMethod method = FinitePrecisionDeterminantMethod.ECHELON_ROW_REDUCTION)
            => calc_det_finite_precision(A, method, new NativeDoubleProvider(), x => Math.Abs(x));

        public static decimal Determinant(this DenseMatrix<decimal> A, FinitePrecisionDeterminantMethod method = FinitePrecisionDeterminantMethod.ECHELON_ROW_REDUCTION)
            => calc_det_finite_precision(A, method, new NativeDecimalProvider(), x => (double)Math.Abs(x));

        public static Complex Determinant(this DenseMatrix<Complex> A, FinitePrecisionDeterminantMethod method = FinitePrecisionDeterminantMethod.ECHELON_ROW_REDUCTION)
            => calc_det_finite_precision(A, method, new NativeComplexProvider(), x => x.Modulus());

        public static T Determinant<T>(this DenseMatrix<T> A) where T : IField<T>, new() => calc_det_naive(A, _A => _A.ToRowEchelonForm(), new FieldProvider<T>());

        #endregion

        #region Log determinant calculation

        #endregion
    }
}
