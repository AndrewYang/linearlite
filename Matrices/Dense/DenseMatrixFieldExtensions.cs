﻿using LinearNet.Providers;
using LinearNet.Structs;
using System;
using LinearNet.Providers.LinearAlgebra;

namespace LinearNet.Matrices
{
    /// <summary>
    /// Dense matrix methods which require the matrix to be over a field.
    /// </summary>
    public static class DenseMatrixFieldExtensions
    {
        #region Rank calculation

        private static int rank_inner<T>(this DenseMatrix<T> A, Func<DenseMatrix<T>, DenseMatrix<T>> ToRowEchelonForm, Func<T, bool> ApproximatelyZero) where T : new()
        {
            MatrixChecks.CheckNotNull(A);

            DenseMatrix<T> re = ToRowEchelonForm(A);
            int rows = re.Rows, cols = re.Columns, n = Math.Min(rows, cols), r, c, rank = 0;
            for (r = 0; r < n; ++r)
            {
                T[] re_r = re[r];
                for (c = 0; c < cols; ++c)
                {
                    if (!ApproximatelyZero(re_r[c]))
                    {
                        rank++;
                        break;
                    }
                }
            }
            return rank;
        }

        /// <summary>
        /// <para>Returns the rank of a matrix $A$, or 0 if $A$ is null.</para>
        /// <para>This method uses row-echelon reduction by default.</para>
        /// <para><b>Example:</b></para>
        /// <pre><code class="cs">
        /// // Create two random vectors 
        /// DenseVector&lt;double&gt; u = DenseVector.Random&lt;double&gt;(10), v = DenseVector.Random&lt;double&gt;(10); 
        /// DenseMatrix&lt;double&gt; A = u.OuterProduct(v);
        /// int rank = A.Rank();
        /// 
        /// Console.WriteLine("rank = " + rank); // rank = 1
        /// 
        /// </code></pre>
        /// </summary>
        /// <name>Rank</name>
        /// <proto>int Rank(this IMatrix<T> A)</proto>
        /// <cat>la</cat>
        /// <param name="eps"></param>
        /// <returns></returns>
        public static int Rank(this DenseMatrix<int> A) 
        {
            Rational zero = Rational.Zero;
            return rank_inner(A.ToRational(), _A => _A.ToRowEchelonForm(), a => a.Equals(zero));
        }
        public static int Rank(this DenseMatrix<float> A, float tolerance = Precision.FLOAT_PRECISION) => rank_inner(A, _A => _A.ToRowEchelonForm(), a => Util.ApproximatelyEquals(a, 0.0F, tolerance));
        public static int Rank(this DenseMatrix<double> A, double tolerance = Precision.DOUBLE_PRECISION) => rank_inner(A, _A => _A.ToRowEchelonForm(), a => Util.ApproximatelyEquals(a, 0.0, tolerance));
        public static int Rank(this DenseMatrix<decimal> A, decimal tolerance = Precision.DECIMAL_PRECISION) => rank_inner(A, _A => _A.ToRowEchelonForm(), a => Util.ApproximatelyEquals(a, 0.0M, tolerance));
        public static int Rank(this DenseMatrix<Complex> A, double tolerance = Precision.DOUBLE_PRECISION) => rank_inner(A, _A => _A.ToRowEchelonForm(), a => a.ApproximatelyEquals(Complex.Zero, tolerance));
        public static int Rank<T>(this DenseMatrix<T> A, double tolerance = Precision.DOUBLE_PRECISION) where T : IField<T>, new()
        {
            T zero = new T().AdditiveIdentity;
            return rank_inner(A, _A => _A.ToRowEchelonForm(), a => a.ApproximatelyEquals(zero, tolerance));
        }

        #endregion

        #region Rayleigh quotient

        private static T rayleigh_quotient_inner<T>(this DenseMatrix<T> A, DenseVector<T> x, IDenseBLAS1<T> blas) where T : new()
        {
            MatrixChecks.CheckNotNull(A);
            MatrixChecks.CheckIsSquare(A);

            int m = A.Rows, i;
            if (m != x.Dimension)
            {
                throw new InvalidOperationException();
            }

            T[][] _A = A.Values;
            T[] _x = x.Values, Ax = new T[m];
            for (i = 0; i < m; ++i)
            {
                Ax[i] = blas.DOT(_A[i], _x, 0, m);
            }

            return blas.Provider.Divide(blas.DOT(_x, Ax, 0, m), blas.DOT(_x, _x, 0, m));
        }

        /// <summary>
        /// <para>
        /// Returns the Rayleigh quotient of a symmetric (Hermitian) matrix $A\in\mathbb{F}^{n \times n}$ and a vector $x\in\mathbb{F}^n$, defined as 
        /// $$\frac{x^*Ax}{x^*x}$$
        /// </para>
        /// </summary>
        /// <name>RayleighQuotient</name>
        /// <proto>T RayleighQuotient(this IMatrix<T> A, IVector<T> x)</proto>
        /// <cat>la</cat>
        /// <param name="eps"></param>
        /// <returns></returns>
        public static double RayleighQuotient(this DenseMatrix<int> A, DenseVector<int> x) => RayleighQuotient(A.ToDouble(), x.ToDouble());
        public static double RayleighQuotient(this DenseMatrix<long> A, DenseVector<long> x) => RayleighQuotient(A.ToDouble(), x.ToDouble());
        public static float RayleighQuotient(this DenseMatrix<float> A, DenseVector<float> x) => rayleigh_quotient_inner(A, x, new NativeFloatProvider());
        public static double RayleighQuotient(this DenseMatrix<double> A, DenseVector<double> x) => rayleigh_quotient_inner(A, x, new NativeDoubleProvider());
        public static decimal RayleighQuotient(this DenseMatrix<decimal> A, DenseVector<decimal> x) => rayleigh_quotient_inner(A, x, new NativeDecimalProvider());
        public static Complex RayleighQuotient(this DenseMatrix<Complex> A, DenseVector<Complex> x) => rayleigh_quotient_inner(A, x, new NativeComplexProvider());
        public static T RayleighQuotient<T>(this DenseMatrix<T> A, DenseVector<T> x) where T : IField<T>, new() => rayleigh_quotient_inner(A, x, new NativeFieldProvider<T>());

        public static Rational ExactRayleighQuotient(this DenseMatrix<int> A, DenseVector<int> x) => RayleighQuotient(A.ToRational(), x.ToRational());

        #endregion
    }
    public enum FinitePrecisionDeterminantMethod
    {
        ECHELON_ROW_REDUCTION,
        LU_DECOMPOSITION
    }
}
