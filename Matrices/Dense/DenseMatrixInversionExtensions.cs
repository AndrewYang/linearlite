﻿using LinearNet.Providers;
using LinearNet.Decomposition;
using LinearNet.Matrices;
using LinearNet.Structs;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LinearNet.Providers.LinearAlgebra;
using LinearNet.Matrices.Inversion;

namespace LinearNet
{
    /// <summary>
    /// Extension methods to invert matrices of type DenseMatrix<T> where T is either:
    /// - a primitive of type int, long, float, double or decimal, or
    /// - a custom algebra implementing the IField<T> interface
    /// 
    /// Note that even though Complex implements Field<Complex>, the implementation here 
    /// is more efficient and more numerically stable, so it is maintained separately 
    /// to the more general Field method. 
    /// </summary>
    public static class DenseMatrixInversionExtensions
    {
        #region General matrix inversion 

        #endregion

        #region Gaussian elimination inversion algorithms

        /// <summary>
        /// <para>
        /// Calculates and returns the inverse of a square matrix $A$. The original matrix is unchanged.
        /// </para>
        /// <para>If $A$ is not square, an <txt>InvalidOperationException</txt> will be thrown.</para>
        /// </summary>
        /// <name>Invert</name>
        /// <proto>IMatrix<T> Invert(this IMatrix<T> A)</proto>
        /// <cat>la</cat>
        /// <param name="x">The matrix to be inverted</param>
        /// <returns>The inverted matrix (the original matrix is unchanged)</returns>
        public static DenseMatrix<double> Invert(this DenseMatrix<double> A)
        {
            return A.Invert(new PivotedGaussianEliminationMatrixInversion());
        }
        public static DenseMatrix<float> Invert(this DenseMatrix<float> A)
        {
            return A.Invert(new PivotedGaussianEliminationMatrixInversion());
        }
        public static DenseMatrix<decimal> Invert(this DenseMatrix<decimal> A)
        {
            return A.Invert(new PivotedGaussianEliminationMatrixInversion());
        }
        public static DenseMatrix<Complex> Invert(this DenseMatrix<Complex> A)
        {
            return A.Invert(new PivotedGaussianEliminationMatrixInversion());
        }

        /// <summary>
        /// Calculates the inverse of a square matrix over any IField<T>
        /// This implementation uses Gaussian elimination and has complexity O(n^3)
        /// </summary>
        /// <typeparam name="T">Any field</typeparam>
        /// <param name="A"></param>
        /// <returns></returns>
        public static DenseMatrix<T> Invert<T>(this DenseMatrix<T> A) where T : IField<T>, new()
        {
            if (A == null) throw new ArgumentNullException();
            if (!A.IsSquare) throw new InvalidOperationException();

            int n = A.Rows, i, j, k;
            T[][] cpy = MatrixInternalExtensions.JMatrix<T>(n, n);
            cpy.CopyFrom(A.Values);

            T e = cpy[0][0];
            T[][] inv = MatrixInternalExtensions.JIdentity(e, n);

            for (k = 0; k < n - 1; k++)
            {
                T[] cpy_k = cpy[k], inv_k = inv[k];

                // for each row below k, i.e. for i in [k + 1, n)
                T factor = cpy_k[k].MultiplicativeInverse();
                for (i = k + 1; i < n; i++)
                {
                    T[] cpy_i = cpy[i], inv_i = inv[i];
                    T alpha = cpy_i[k].Multiply(factor);

                    for (j = k + 1; j < n; j++)
                    {
                        cpy_i[j] = cpy_i[j].Subtract(cpy_k[j].Multiply(alpha));
                    }
                    for (j = 0; j < n; j++)
                    {
                        inv_i[j] = inv_i[j].Subtract(inv_k[j].Multiply(alpha));
                    }
                    cpy_i[k] = e.AdditiveIdentity;
                }
            }

            // going back upwards
            for (k = n - 1; k >= 0; k--)
            {
                T[] cpy_k = cpy[k], inv_k = inv[k];
                T factor = cpy_k[k].MultiplicativeInverse();
                for (i = k - 1; i >= 0; i--)
                {
                    T[] cpy_i = cpy[i], inv_i = inv[i];
                    T alpha = cpy_i[k].Multiply(factor);

                    for (j = i + 1; j < n; j++)
                    {
                        cpy_i[j] = cpy_i[j].Subtract(cpy_k[j].Multiply(alpha));
                    }
                    for (j = 0; j < n; j++)
                    {
                        inv_i[j] = inv_i[j].Subtract(inv_k[j].Multiply(alpha));
                    }
                }
            }

            // normalise - with order
            for (k = 0; k < n; k++)
            {
                T alpha = cpy[k][k];
                T[] inv_k = inv[k];
                for (i = 0; i < n; i++)
                {
                    inv_k[i] = inv_k[i].Divide(alpha);
                }
            }

            return new DenseMatrix<T>(inv);
        }

        #endregion

        #region Moore-Penrose Inverse

        /// <summary>
        /// <para>
        /// Calculate the <a href="https://en.wikipedia.org/wiki/Moore%E2%80%93Penrose_inverse">Moore-Penrose pseudo-inverse</a>, $A^+$, of a matrix 
        /// $A$ using the <txt>'geninv'</txt> function as documented in 'Fast Computation of Moore-Penrose Inverse Matrices', Pierre Courrieu (2005).
        /// </para>
        /// $A^+$ is a unique matrix that satisfies the following conditions:
        /// <ul>
        /// <li>$AA^+A=A$</li>
        /// <li>$A^+AA^+=A^+$</li>
        /// <li>$AA^+$ and $A^+A$ are Hermitian</li>
        /// </ul>
        /// </summary>
        /// <name>PseudoInvert</name>
        /// <proto>IMatrix<T> PseudoInvert(this IMatrix<T> A)</proto>
        /// <cat>la</cat>
        /// <param name="G"></param>
        /// <returns></returns>
        public static DenseMatrix<double> PseudoInvert(this DenseMatrix<double> G)
        {
            return PseudoInvert(G, (A, B) => A.Multiply(B), A => { A.CholeskyDecompose(out DenseMatrix<double> L); return L; }, A => A.Invert());
        }
        public static DenseMatrix<float> PseudoInvert(this DenseMatrix<float> G)
        {
            return PseudoInvert(G, (A, B) => A.Multiply(B), A => { A.CholeskyDecompose(out DenseMatrix<float> L); return L; }, A => A.Invert());
        }
        public static DenseMatrix<decimal> PseudoInvert(this DenseMatrix<decimal> G)
        {
            return PseudoInvert(G, (A, B) => A.Multiply(B), A => { A.CholeskyDecompose(out DenseMatrix<decimal> L); return L; }, A => A.Invert());
        }
        public static DenseMatrix<Complex> PseudoInvert(this DenseMatrix<Complex> G)
        {
            return PseudoInvert(G, (A, B) => A.Multiply(B), A => { A.CholeskyDecompose(out DenseMatrix<Complex> L); return L; }, A => A.Invert());
        }
        private static DenseMatrix<T> PseudoInvert<T>(this DenseMatrix<T> G, 
            Func<DenseMatrix<T>, DenseMatrix<T>, DenseMatrix<T>> Multiply,
            Func<DenseMatrix<T>, DenseMatrix<T>> CholeskyDecompose,
            Func<DenseMatrix<T>, DenseMatrix<T>> Invert) where T : new()
        {
            MatrixChecks.CheckNotNull(G);
            MatrixChecks.CheckIsSquare(G);

            DenseMatrix<T> Gt = G.Transpose(), A;

            bool transposed = G.Rows < G.Columns;
            if (transposed)
            {
                A = Multiply(G, Gt);
            }
            else
            {
                A = Multiply(Gt, G);
            }

            DenseMatrix<T> L = CholeskyDecompose(A);
            DenseMatrix<T> Lt = L.Transpose();
            DenseMatrix<T> M = Invert(Multiply(Lt, L));

            if (transposed)
            {
                return Multiply(Multiply(Multiply(Multiply(Gt, L), M), M), Lt);
            }
            else
            {
                return Multiply(Multiply(Multiply(Multiply(L, M), M), Lt), Gt);
            }
        }

        #endregion

        #region Left-inverse 

        /// <summary>
        /// Calculate and return the left-inverse of a matrix $A\in\mathbb{F}^{m \times n}$, where $m \ge n$
        /// </summary>
        /// <name>LeftInverse</name>
        /// <proto>IMatrix<T> LeftInverse(this IMatrix<T> A)</proto>
        /// <cat>la</cat>
        /// <param name="A"></param>
        /// <returns></returns>
        public static DenseMatrix<double> LeftInverse(this DenseMatrix<double> A)
        {
            MatrixChecks.CheckNotNull(A);
            if (A.Rows < A.Columns)
            {
                throw new InvalidOperationException();
            }

            DenseMatrix<double> At = A.Transpose();
            return At.Multiply(A).Invert().Multiply(At);
        }

        /// <summary>
        /// Calculate and return the right-inverse of a matrix $A\in\mathbb{F}^{m \times n}$, where $m \le n$
        /// </summary>
        /// <name>RightInverse</name>
        /// <proto>IMatrix<T> RightInverse(this IMatrix<T> A)</proto>
        /// <cat>la</cat>
        /// <param name="A"></param>
        /// <returns></returns>
        public static DenseMatrix<double> RightInverse(this DenseMatrix<double> A)
        {
            MatrixChecks.CheckNotNull(A);
            if (A.Rows > A.Columns)
            {
                throw new InvalidOperationException();
            }

            DenseMatrix<double> At = A.Transpose();
            return At.Multiply(A.Multiply(At).Invert());
        }

        #endregion
    }
}
