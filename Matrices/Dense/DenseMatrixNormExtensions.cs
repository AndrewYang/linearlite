﻿using LinearNet.Providers;
using LinearNet.Helpers;
using LinearNet.Matrices.InducedNorm;
using LinearNet.Structs;
using System;
using System.Diagnostics;

namespace LinearNet
{
    public static class DenseMatrixNormExtensions
    {
        /// <summary>
        /// <para>Calculates the $L_{p, q}$ norm of matrix $A$, for any positive integers $p, q$.</para>
        /// <para>The <a href="#FrobeniusNorm">Frobenius norm</a> is a special case for $p = q = 2$.</para>
        /// <para>The <a href="#MaxNorm">maximum norm</a> is a special case for $p = q = \infty$.</para>
        /// </summary>
        /// <name>ElementwiseNorm</name>
        /// <proto>T ElementwiseNorm(this IMatrix<T> A, int p, int q)</proto>
        /// <cat>la</cat>
        /// <param name="A"></param>
        /// <param name="p"></param>
        /// <param name="q"></param>
        /// <returns></returns>
        public static double ElementwiseNorm(this DenseMatrix<int> A, int p, int q) => ElementwiseNorm(A, p, q, 0.0, x => Math.Abs(x), (a, b) => Math.Pow(a, b), (a, b) => a + b);
        public static double ElementwiseNorm(this DenseMatrix<long> A, int p, int q) => ElementwiseNorm(A, p, q, 0.0, x => Math.Abs(x), (a, b) => Math.Pow(a, b), (a, b) => a + b);
        public static float ElementwiseNorm(this DenseMatrix<float> A, int p, int q) => ElementwiseNorm(A, p, q, 0.0f, x => Math.Abs(x), (a, b) => (float)Math.Pow(a, b), (a, b) => a + b);
        public static double ElementwiseNorm(this DenseMatrix<double> A, int p, int q) => ElementwiseNorm(A, p, q, 0.0, x => Math.Abs(x), (a, b) => Math.Pow(a, b), (a, b) => a + b);
        public static decimal ElementwiseNorm(this DenseMatrix<decimal> A, int p, int q) => ElementwiseNorm(A, p, q, 0.0m, x => Math.Abs(x), (a, b) => DecimalMath.Pow(a, (decimal)b), (a, b) => a + b);
        public static double ElementwiseNorm(this DenseMatrix<Complex> A, int p, int q) => ElementwiseNorm(A, p, q, 0.0, x => x.Modulus(), (a, b) => Math.Pow(a, b), (a, b) => a + b);
        private static F ElementwiseNorm<T, F>(this DenseMatrix<T> A, int p, int q, F zero, Func<T, F> Norm, Func<F, double, F> Pow, Func<F, F, F> Add) where T : new()
        {
            MatrixChecks.CheckNotNull(A);
            if (p < 1) throw new ArgumentOutOfRangeException();
            if (q < 1) throw new ArgumentOutOfRangeException();

            int m = A.Rows, n = A.Columns, i, j;
            F norm = zero;

            if (p == 1 && q == 1)
            {
                for (i = 0; i < m; ++i)
                {
                    T[] A_i = A[i];
                    for (j = 0; j < n; ++j)
                    {
                        norm = Add(norm, Norm(A_i[j]));
                    }
                }
                return norm;
            }
            else
            {
                double p_q = q / (double)p;
                for (j = 0; j < n; ++j)
                {
                    F sum = zero;
                    for (i = 0; i < m; ++i)
                    {
                        sum = Add(sum, Pow(Norm(A[i][j]), p));
                    }
                    norm = Add(norm, Pow(sum, p_q));
                }
                return Pow(norm, 1.0 / q);
            }
        }

        /// <summary>
        /// Calculates the max norm of a matrix $A$, defined as elementwise $L_{p, q}$ norm with $p = q = \infty$. $$||A||_{\max} = \max_{i, j} |A_{ij}|$$
        /// <para>Also see: <a href="#ElementwiseNorm"><txt>ElementwiseNorm</txt></a>.</para>
        /// </summary>
        /// <name>MaxNorm</name>
        /// <proto>T MaxNorm(this IMatrix<T> A)</proto>
        /// <cat>la</cat>
        /// <param name="A"></param>
        /// <returns></returns>
        public static int MaxNorm(this DenseMatrix<int> A) => MaxNorm(A, 0, x => Math.Abs(x));
        public static long MaxNorm(this DenseMatrix<long> A) => MaxNorm(A, 0L, x => Math.Abs(x));
        public static float MaxNorm(this DenseMatrix<float> A) => MaxNorm(A, 0.0f, x => Math.Abs(x));
        public static double MaxNorm(this DenseMatrix<double> A) => MaxNorm(A, 0.0, x => Math.Abs(x));
        public static decimal MaxNorm(this DenseMatrix<decimal> A) => MaxNorm(A, 0.0m, x => Math.Abs(x));
        public static double MaxNorm(this DenseMatrix<Complex> A) => MaxNorm(A, 0.0, x => x.Modulus());
        private static F MaxNorm<T, F>(this DenseMatrix<T> A, F zero, Func<T, F> Norm) where T : new() where F : IComparable<F>
        {
            MatrixChecks.CheckNotNull(A);

            F max = zero;
            int rows = A.Rows, cols = A.Columns, i, j;
            for (i = 0; i < rows; ++i)
            {
                T[] row = A[i];
                for (j = 0; j < cols; ++j)
                {
                    F norm = Norm(row[j]);
                    if (norm.CompareTo(max) > 0)
                    {
                        max = norm;
                    }
                }
            }
            return max;
        }

        /// <summary>
        /// Calculates the Frobenius norm, or Hilbert-Schmidt norm, of a matrix $A$, $||A||_F$. The Frobenius norm is equal to the $L_{2, 2}$ elementwise norm.
        /// <para>Also see: <a href="#ElementwiseNorm"><txt>ElementwiseNorm</txt></a>.</para>
        /// </summary>
        /// <name>FrobeniusNorm</name>
        /// <proto>T FrobeniusNorm(this IMatrix<T> A)</proto>
        /// <cat>la</cat>
        /// <param name="A"></param>
        /// <returns></returns>
        public static double FrobeniusNorm(this DenseMatrix<int> A) => ElementwiseNorm(A, 2, 2);
        public static double FrobeniusNorm(this DenseMatrix<long> A) => ElementwiseNorm(A, 2, 2);
        public static float FrobeniusNorm(this DenseMatrix<float> A) => ElementwiseNorm(A, 2, 2);
        public static double FrobeniusNorm(this DenseMatrix<double> A) => ElementwiseNorm(A, 2, 2);
        public static decimal FrobeniusNorm(this DenseMatrix<decimal> A) => ElementwiseNorm(A, 2, 2);
        public static double FrobeniusNorm(this DenseMatrix<Complex> A) => ElementwiseNorm(A, 2, 2);

        /// <summary>
        /// <para>
        /// Calculates the induced $p$-norm of matrix $A$, for any positive integer $p$, defined as
        /// $$||A||_p = \sup_{x \not= 0}{\frac{||Ax||_p}{||x||_p}}$$
        /// </para>
        /// <para>
        /// This method is experiences numerical instability for some matrices for $p > O(100)$. Consider using 
        /// <a href="#InducedInfinityNorm">induced infinity-norm</a> as an approximation for large $p$</para>
        /// </summary>
        /// <name>InducedNorm</name>
        /// <proto>T InducedNorm(this IMatrix<T> A, int p)</proto>
        /// <cat>la</cat>
        /// <param name="A"></param>
        /// <param name="p"></param>
        /// <returns></returns>
        public static double InducedNorm(this DenseMatrix<double> A, int p)
        {
            if (p < 1)
            {
                throw new InvalidOperationException("The induced p-norm of a matrix is only defined for p >= 1");
            }

            // For p = 1, the induced norm is the max absolute column sum of the matrix A
            if (p == 1)
            {
                int m = A.Rows, n = A.Columns, i, j;
                double max = double.NegativeInfinity;
                for (j = 0; j < n; ++j)
                {
                    double sum = 0.0;
                    for (i = 0; i < m; ++i)
                    {
                        sum += A[i][j];
                    }
                    if (sum > max)
                    {
                        max = sum;
                    }
                }
                return max;
            }

            /// TODO: implement SVD method for p = 2, currently the process is ill-conditioned 
            /// for O(100) < p < inf. 
            // For p = 2, the induced norm is the largest singular value of A
            // TODO - implement k-largest singular values method and link to this
            if (p == 2)
            {
                // Currently borrowing the iterative method...
            }

            // For sufficiently large values of p, the induced norm is numerically indistinguishable from the 
            // infinite-norm, which is the maximum absolute row sum of A
            if (p > 100000)
            {
                return InducedInfinityNorm(A);
            }

            // For all other values of p, use an iterative method to estimate the p-norm. 
            // Note that in general this problem is NP-hard however it is often possible to 
            // obtain a sufficiently good numerical approximation using gradient ascent. 

            IterativeInducedNormCalculator calculator = new IterativeInducedNormCalculator(A.Values, p);
            return calculator.Calculate();
        }

        /// <summary>
        /// <para>Calculates and returns the induced infinity-norm, $||A||_\infty$, of a matrix $A$.</para>
        /// </summary>
        /// <name>InducedInfinityNorm</name>
        /// <proto>T InducedInfinityNorm(this IMatrix<T> A)</proto>
        /// <cat>la</cat>
        /// <param name="A"></param>
        /// <returns></returns>
        public static int InducedInfinityNorm(this DenseMatrix<int> A) => InducedInfinityNorm(A, 0, x => Math.Abs(x), (a, b) => a + b);
        public static long InducedInfinityNorm(this DenseMatrix<long> A) => InducedInfinityNorm(A, 0L, x => Math.Abs(x), (a, b) => a + b);
        public static float InducedInfinityNorm(this DenseMatrix<float> A) => InducedInfinityNorm(A, 0.0f, x => Math.Abs(x), (a, b) => a + b);
        public static double InducedInfinityNorm(this DenseMatrix<double> A)
        {
            int m = A.Rows, n = A.Columns, i, j;
            double max = 0.0;
            for (i = 0; i < m; ++i)
            {
                double[] A_i = A[i];
                double sum = 0.0;
                for (j = 0; j < n; ++j)
                {
                    sum += Math.Abs(A_i[j]);
                }
                if (sum > max)
                {
                    max = sum;
                }
            }
            return max;
        }
        public static decimal InducedInfinityNorm(this DenseMatrix<decimal> A) => InducedInfinityNorm(A, 0.0m, x => Math.Abs(x), (a, b) => a + b);
        public static double InducedInfinityNorm(this DenseMatrix<Complex> A) => InducedInfinityNorm(A, 0.0, x => x.Modulus(), (a, b) => a + b);
        private static F InducedInfinityNorm<T, F>(this DenseMatrix<T> A, F zero, Func<T, F> Norm, Func<F, F, F> Add) where F : IComparable<F> where T : new()
        {
            int m = A.Rows, n = A.Columns, i, j;
            F max = zero;
            for (i = 0; i < m; ++i)
            {
                T[] A_i = A[i];
                F sum = zero;
                for (j = 0; j < n; ++j)
                {
                    sum = Add(sum, Norm(A_i[j]));
                }
                if (sum.CompareTo(max) > 0)
                {
                    max = sum;
                }
            }
            return max;
        }

    }
}
