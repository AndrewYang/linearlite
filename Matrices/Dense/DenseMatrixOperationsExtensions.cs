﻿using LinearNet.Providers;
using LinearNet.Helpers;
using LinearNet.Structs;
using LinearNet.Vectors;
using System;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using LinearNet.Providers.LinearAlgebra;
using LinearNet.Matrices.Multiplication;

namespace LinearNet.Matrices
{
    /// <summary>
    /// Implements elementary operations for matrices of type DenseMatrix<T>
    /// Operations currently supported: 
    /// - Addition
    /// - Direct Sum
    /// - Kronecker Sum
    /// - Subtraction
    /// - Multiplication
    /// - Kronecker Product
    /// - Khatri-Rao Product
    /// - Hadamard Product/Power
    /// </summary>
    public static class DenseMatrixOperationsExtensions
    {
        #region Addition

        /// <summary>
        /// <para>For two matrices $A$ and $B$, returns $A+B$. Matrices $A$ and $B$ are unchanged.</para>
        /// 
        /// <para>
        /// Supported for all matrix types, and for the data types <txt>int</txt>, <txt>long</txt>, <txt>float</txt>, <txt>double</txt>, <txt>decimal</txt> and any type <txt>T : IAdditiveGroup&lt;T&gt;</txt>
        /// </para>
        /// 
        /// Throws <txt>InvalidOperationException</txt> if $A$ and $B$ are not the same size.
        /// <para>Parallel version: <a href="#AddParallel"><txt><b>IMatrix&lt;T&gt; AddParallel(this IMatrix&lt;T&gt; A, IMatrix&lt;T&gt; B)</b></txt></a></para>
        /// <para>Also see: <a href="#Increment"><txt><b>IMatrix&lt;T&gt; Increment(this IMatrix&lt;T&gt; A, IMatrix&lt;T&gt; B)</b></txt></a></para>
        /// 
        /// <h4>Example</h4>
        /// <pre><code class="cs">
        /// // Randomly create two 10 by 10 matrices
        /// DenseMatrix&lt;float&gt; A = DenseMatrix.Random&lt;float&gt;(10, 10), B = DenseMatrix.Random&lt;float&gt;(10, 10);
        /// DenseMatrix&lt;float&gt; sum = A.Add(B);
        /// 
        /// </code></pre>
        /// </summary>
        /// <name>Add</name>
        /// <proto>DenseMatrix<T> Add(this DenseMatrix<T> A, DenseMatrix<T> B)</proto>
        /// <cat>la</cat>
        /// <param name="A">The first dense matrix to add.</param>
        /// <param name="B">The second dense matrix to add (must be of the same dimension as the first matrix).</param>
        /// <returns>The matrix sum.</returns>
        public static DenseMatrix<int> Add(this DenseMatrix<int> A, DenseMatrix<int> B)
        {
            MatrixChecks.CheckNotNull(A, B);
            MatrixChecks.AssertMatrixDimensionsEqual(A, B);

            int rows = A.Rows, cols = A.Columns, i, j;
            int[][] result = MatrixInternalExtensions.JMatrix<int>(rows, cols);

            for (i = 0; i < rows; i++)
            {
                int[] A_i = A[i], B_i = B[i], result_i = result[i];
                for (j = 0; j < cols; j++)
                {
                    result_i[j] = A_i[j] + B_i[j];
                }
            }
            return new DenseMatrix<int>(result);
        }
        public static DenseMatrix<long> Add(this DenseMatrix<long> A, DenseMatrix<long> B)
        {
            MatrixChecks.CheckNotNull(A, B);
            MatrixChecks.AssertMatrixDimensionsEqual(A, B);

            int rows = A.Rows, cols = A.Columns, i, j;
            long[][] result = MatrixInternalExtensions.JMatrix<long>(rows, cols);

            for (i = 0; i < rows; i++)
            {
                long[] A_i = A[i], B_i = B[i], result_i = result[i];
                for (j = 0; j < cols; j++)
                {
                    result_i[j] = A_i[j] + B_i[j];
                }
            }
            return new DenseMatrix<long>(result);
        }
        public static DenseMatrix<float> Add(this DenseMatrix<float> A, DenseMatrix<float> B)
        {
            MatrixChecks.CheckNotNull(A, B);
            MatrixChecks.AssertMatrixDimensionsEqual(A, B);

            int rows = A.Rows, cols = A.Columns, i, j;
            float[][] result = MatrixInternalExtensions.JMatrix<float>(rows, cols);

            for (i = 0; i < rows; i++)
            {
                float[] A_i = A[i], B_i = B[i], result_i = result[i];
                for (j = 0; j < cols; j++)
                {
                    result_i[j] = A_i[j] + B_i[j];
                }
            }
            return new DenseMatrix<float>(result);
        }
        public static DenseMatrix<double> Add(this DenseMatrix<double> A, DenseMatrix<double> B)
        {
            MatrixChecks.CheckNotNull(A, B);
            MatrixChecks.AssertMatrixDimensionsEqual(A, B);

            int rows = A.Rows, cols = A.Columns, i, j;
            double[][] result = MatrixInternalExtensions.JMatrix<double>(rows, cols);

            for (i = 0; i < rows; i++)
            {
                double[] A_i = A[i], B_i = B[i], result_i = result[i];
                for (j = 0; j < cols; j++)
                {
                    result_i[j] = A_i[j] + B_i[j];
                }
            }
            return new DenseMatrix<double>(result);
        }
        public static DenseMatrix<decimal> Add(this DenseMatrix<decimal> A, DenseMatrix<decimal> B)
        {
            MatrixChecks.CheckNotNull(A, B);
            MatrixChecks.AssertMatrixDimensionsEqual(A, B);

            int rows = A.Rows, cols = A.Columns, i, j;
            decimal[][] result = MatrixInternalExtensions.JMatrix<decimal>(rows, cols);

            for (i = 0; i < rows; i++)
            {
                decimal[] A_i = A[i], B_i = B[i], result_i = result[i];
                for (j = 0; j < cols; j++)
                {
                    result_i[j] = A_i[j] + B_i[j];
                }
            }
            return new DenseMatrix<decimal>(result);
        }
        public static DenseMatrix<T> Add<T>(this DenseMatrix<T> A, DenseMatrix<T> B) where T : IAdditiveGroup<T>, new()
        {
            MatrixChecks.CheckNotNull(A, B);
            MatrixChecks.AssertMatrixDimensionsEqual(A, B);

            int rows = A.Rows, cols = A.Columns, i, j;
            T[][] result = MatrixInternalExtensions.JMatrix<T>(rows, cols);

            for (i = 0; i < rows; i++)
            {
                T[] A_i = A[i], B_i = B[i], result_i = result[i];
                for (j = 0; j < cols; j++)
                {
                    result_i[j] = A_i[j].Add(B_i[j]);
                }
            }
            return new DenseMatrix<T>(result);
        }

        #endregion

        #region Increment
        /// <summary>
        /// <para>Given two matrices $A$ and $B$ of equal dimensions, set $A$ to $A + B$.</para>
        /// </summary>
        /// <name>Increment</name>
        /// <proto>void Increment(this IMatrix<T> A, IMatrix<T> B)</proto>
        /// <cat>la</cat>
        /// <param name="A"></param>
        /// <param name="B"></param>
        public static void Increment(this DenseMatrix<int> A, DenseMatrix<int> B) => Operation(A, B, (a, b) => a.increment_unsafe(b));
        public static void Increment(this DenseMatrix<long> A, DenseMatrix<long> B) => Operation(A, B, (a, b) => a.increment_unsafe(b));
        public static void Increment(this DenseMatrix<float> A, DenseMatrix<float> B) => Operation(A, B, (a, b) => a.increment_unsafe(b));
        public static void Increment(this DenseMatrix<double> A, DenseMatrix<double> B) => Operation(A, B, (a, b) => a.increment_unsafe(b));
        public static void Increment(this DenseMatrix<decimal> A, DenseMatrix<decimal> B) => Operation(A, B, (a, b) => a.increment_unsafe(b));
        public static void Increment(this DenseMatrix<Complex> A, DenseMatrix<Complex> B) => Operation(A, B, (a, b) => a.increment_unsafe(b));
        public static void Increment<T>(this DenseMatrix<T> A, DenseMatrix<T> B) where T : IAdditiveGroup<T>, new() => Operation(A, B, (a, b) => a.increment_unsafe(b));
        private static void Operation<T>(this DenseMatrix<T> A, DenseMatrix<T> B, Action<T[][], T[][]> Increment) where T : new()
        {
            MatrixChecks.CheckNotNull(A, B);
            MatrixChecks.AssertMatrixDimensionsEqual(A, B);
            Increment(A.Values, B.Values);
        }
        #endregion

        #region Decrement
        public static void Decrement(this DenseMatrix<int> A, DenseMatrix<int> B) => Operation(A, B, (a, b) => a.decrement_unsafe(b));
        public static void Decrement(this DenseMatrix<long> A, DenseMatrix<long> B) => Operation(A, B, (a, b) => a.decrement_unsafe(b));
        public static void Decrement(this DenseMatrix<float> A, DenseMatrix<float> B) => Operation(A, B, (a, b) => a.decrement_unsafe(b));
        public static void Decrement(this DenseMatrix<double> A, DenseMatrix<double> B) => Operation(A, B, (a, b) => a.decrement_unsafe(b));
        public static void Decrement(this DenseMatrix<decimal> A, DenseMatrix<decimal> B) => Operation(A, B, (a, b) => a.decrement_unsafe(b));
        public static void Decrement(this DenseMatrix<Complex> A, DenseMatrix<Complex> B) => Operation(A, B, (a, b) => a.decrement_unsafe(b));
        public static void Decrement<T>(this DenseMatrix<T> A, DenseMatrix<T> B) where T : IAdditiveGroup<T>, new() => Operation(A, B, (a, b) => a.decrement_unsafe(b));
        #endregion

        #region Subtraction 
        /// <summary>
        /// <para>Given two matrices $A$ and $B$ of equal dimensions, calculate and return $A-B$. Matrices $A$ and $B$ are unchanged.</para>
        /// <para>Parallel version: <a href="#SubtractParallel"><txt><b>SubtractParallel(IMatrix&lt;T&gt; A, IMatrix&lt;T&gt; B)</b></txt></a></para>
        /// </summary>
        /// <name>Subtract</name>
        /// <proto>IMatrix<T> Subtract(this IMatrix<T> A, IMatrix<T> B)</proto>
        /// <cat>la</cat>
        /// <param name="A"></param>
        /// <param name="B"></param>
        /// <returns></returns>
        public static DenseMatrix<int> Subtract(this DenseMatrix<int> A, DenseMatrix<int> B)
        {
            MatrixChecks.CheckNotNull(A, B);
            MatrixChecks.AssertMatrixDimensionsEqual(A, B);

            int rows = A.Rows, cols = A.Columns, i, j;
            int[][] result = MatrixInternalExtensions.JMatrix<int>(rows, cols);

            for (i = 0; i < rows; i++)
            {
                int[] A_i = A[i], B_i = B[i], result_i = result[i];
                for (j = 0; j < cols; j++)
                {
                    result_i[j] = A_i[j] - B_i[j];
                }
            }
            return new DenseMatrix<int>(result);
        }
        public static DenseMatrix<long> Subtract(this DenseMatrix<long> A, DenseMatrix<long> B)
        {
            MatrixChecks.CheckNotNull(A, B);
            MatrixChecks.AssertMatrixDimensionsEqual(A, B);

            int rows = A.Rows, cols = A.Columns, i, j;
            long[][] result = MatrixInternalExtensions.JMatrix<long>(rows, cols);

            for (i = 0; i < rows; i++)
            {
                long[] A_i = A[i], B_i = B[i], result_i = result[i];
                for (j = 0; j < cols; j++)
                {
                    result_i[j] = A_i[j] - B_i[j];
                }
            }
            return new DenseMatrix<long>(result);
        }
        public static DenseMatrix<float> Subtract(this DenseMatrix<float> A, DenseMatrix<float> B)
        {
            MatrixChecks.CheckNotNull(A, B);
            MatrixChecks.AssertMatrixDimensionsEqual(A, B);

            int rows = A.Rows, cols = A.Columns, i, j;
            float[][] result = MatrixInternalExtensions.JMatrix<float>(rows, cols);

            for (i = 0; i < rows; i++)
            {
                float[] A_i = A[i], B_i = B[i], result_i = result[i];
                for (j = 0; j < cols; j++)
                {
                    result_i[j] = A_i[j] - B_i[j];
                }
            }
            return new DenseMatrix<float>(result);
        }
        public static DenseMatrix<double> Subtract(this DenseMatrix<double> A, DenseMatrix<double> B)
        {
            MatrixChecks.CheckNotNull(A, B);
            MatrixChecks.AssertMatrixDimensionsEqual(A, B);

            int rows = A.Rows, cols = A.Columns, i, j;
            double[][] result = MatrixInternalExtensions.JMatrix<double>(rows, cols);

            for (i = 0; i < rows; i++)
            {
                double[] A_i = A[i], B_i = B[i], result_i = result[i];
                for (j = 0; j < cols; j++)
                {
                    result_i[j] = A_i[j] - B_i[j];
                }
            }
            return new DenseMatrix<double>(result);
        }
        public static DenseMatrix<decimal> Subtract(this DenseMatrix<decimal> A, DenseMatrix<decimal> B)
        {
            MatrixChecks.CheckNotNull(A, B);
            MatrixChecks.AssertMatrixDimensionsEqual(A, B);

            int rows = A.Rows, cols = A.Columns, i, j;
            decimal[][] result = MatrixInternalExtensions.JMatrix<decimal>(rows, cols);

            for (i = 0; i < rows; i++)
            {
                decimal[] A_i = A[i], B_i = B[i], result_i = result[i];
                for (j = 0; j < cols; j++)
                {
                    result_i[j] = A_i[j] - B_i[j];
                }
            }
            return new DenseMatrix<decimal>(result);
        }
        public static DenseMatrix<T> Subtract<T>(this DenseMatrix<T> A, DenseMatrix<T> B) where T : IAdditiveGroup<T>, new()
        {
            MatrixChecks.CheckNotNull(A, B);
            MatrixChecks.AssertMatrixDimensionsEqual(A, B);

            int rows = A.Rows, cols = A.Columns, i, j;
            T[][] result = MatrixInternalExtensions.JMatrix<T>(rows, cols);

            for (i = 0; i < rows; i++)
            {
                T[] A_i = A[i], B_i = B[i], result_i = result[i];
                for (j = 0; j < cols; j++)
                {
                    result_i[j] = A_i[j].Subtract(B_i[j]);
                }
            }
            return new DenseMatrix<T>(result);
        }
        #endregion

        #region Parallel addition
        private static DenseMatrix<T> OperationParallel<T>(this DenseMatrix<T> A, DenseMatrix<T> B, Func<T[], T[], T[]> Operation) where T : new()
        {
            MatrixChecks.CheckNotNull(A, B);
            MatrixChecks.AssertMatrixDimensionsEqual(A, B);

            int rows = A.Rows, cols = A.Columns;

            T[][] result = new T[rows][];
            Parallel.ForEach(Partitioner.Create(0, rows), range =>
            {
                int start = range.Item1, end = range.Item2, i;
                for (i = start; i < end; ++i)
                {
                    result[i] = Operation(A[i], B[i]);
                }
            });
            return result;
        }
        /// <summary>
        /// <para>
        /// Calculate $A + B$ in parallel, returning a new matrix without changing $A$ or $B$. 
        /// </para>
        /// <para>Implemented for all matrix types.</para>
        /// <para>Implemented for all matrices over data types <txt>int</txt>, <txt>long</txt>, <txt>float</txt>, <txt>double</txt>, <txt>decimal</txt>, 
        /// and all types <txt>T</txt> implementing <txt>AdditiveGroup&lt;T&gt;</txt>.</para>
        /// <para>Throws <txt>InvalidOperationException</txt> if $A$ and $B$ are not of the same size.</para>
        /// </summary>
        /// <name>AddParallel</name>
        /// <proto>IMatrix<T> AddParallel(this IMatrix<T> A, IMatrix<T> B)</proto>
        /// <cat>la</cat>
        /// <param name="A"></param>
        /// <param name="B"></param>
        /// <returns></returns>
        public static DenseMatrix<int> AddParallel(this DenseMatrix<int> A, DenseMatrix<int> B) => OperationParallel(A, B, (u, v) => u.Add(v));
        public static DenseMatrix<long> AddParallel(this DenseMatrix<long> A, DenseMatrix<long> B) => OperationParallel(A, B, (u, v) => u.Add(v));
        public static DenseMatrix<float> AddParallel(this DenseMatrix<float> A, DenseMatrix<float> B) => OperationParallel(A, B, (u, v) => u.Add(v));
        public static DenseMatrix<double> AddParallel(this DenseMatrix<double> A, DenseMatrix<double> B) => OperationParallel(A, B, (u, v) => u.Add(v));
        public static DenseMatrix<decimal> AddParallel(this DenseMatrix<decimal> A, DenseMatrix<decimal> B) => OperationParallel(A, B, (u, v) => u.Add(v));
        public static DenseMatrix<T> AddParallel<T>(this DenseMatrix<T> A, DenseMatrix<T> B) where T : IAdditiveGroup<T>, new() => OperationParallel(A, B, (u, v) => u.Add(v));
        #endregion

        #region Parallel subtract 
        public static DenseMatrix<int> SubtractParallel(this DenseMatrix<int> A, DenseMatrix<int> B) => OperationParallel(A, B, (u, v) => u.Subtract(v));
        public static DenseMatrix<long> SubtractParallel(this DenseMatrix<long> A, DenseMatrix<long> B) => OperationParallel(A, B, (u, v) => u.Subtract(v));
        public static DenseMatrix<float> SubtractParallel(this DenseMatrix<float> A, DenseMatrix<float> B) => OperationParallel(A, B, (u, v) => u.Subtract(v));
        public static DenseMatrix<double> SubtractParallel(this DenseMatrix<double> A, DenseMatrix<double> B) => OperationParallel(A, B, (u, v) => u.Subtract(v));
        public static DenseMatrix<decimal> SubtractParallel(this DenseMatrix<decimal> A, DenseMatrix<decimal> B) => OperationParallel(A, B, (u, v) => u.Subtract(v));
        public static DenseMatrix<T> SubtractParallel<T>(this DenseMatrix<T> A, DenseMatrix<T> B) where T : IAdditiveGroup<T>, new() => OperationParallel(A, B, (u, v) => u.Subtract(v));
        #endregion

        #region Multiplication
        private static DenseMatrix<T> Multiply<T>(this DenseMatrix<T> A, DenseMatrix<T> B, Action<T[][], T[][], T[][]> Multiply) where T : new()
        {
            MatrixChecks.CheckNotNull(A, B);
            if (A.Columns != B.Rows) throw new InvalidOperationException();

            T[][] result = MatrixInternalExtensions.JMatrix<T>(A.Rows, B.Columns);
            Multiply(A.Values, B.Values, result);
            return new DenseMatrix<T>(result);
        }
        private static DenseMatrix<T> MultiplyInner<T>(DenseMatrix<T> A, DenseMatrix<T> B, IMatrixMultiplication<T> algorithm) where T : new()
        {
            MatrixChecks.CheckNotNull(A, B);
            if (A.Columns != B.Rows) throw new InvalidOperationException();

            T[][] result = MatrixInternalExtensions.JMatrix<T>(A.Rows, B.Columns);

            // In general, it is faster to overwrite than increment, although this will depend on the algorithm used...
            // In the future, we can use a more intelligent method than defaulting to overwriting all the time.
            algorithm.Multiply(A.Values, B.Values, result, new Size3(A.Rows, A.Columns, B.Columns), false);

            return new DenseMatrix<T>(result);
        }
        private static void MultiplyInner<T>(DenseMatrix<T> A, DenseMatrix<T> B, DenseMatrix<T> result, bool increment, IMatrixMultiplication<T> algorithm) where T : new()
        {
            if (A == null) throw new ArgumentNullException(nameof(A));
            if (B == null) throw new ArgumentNullException(nameof(B));
            if (result == null) throw new ArgumentNullException(nameof(result));

            if (A.Rows > result.Rows) throw new ArgumentOutOfRangeException(nameof(A));
            if (A.Columns != B.Rows) throw new InvalidOperationException(nameof(A) + ", " + nameof(B));
            if (B.Columns > result.Columns) throw new ArgumentOutOfRangeException(nameof(B));

            algorithm.Multiply(A.Values, B.Values, result.Values, new Size3(A.Rows, A.Columns, B.Columns), increment);
        }

        /// <summary>
        /// <para>Given two matrices $A$ and $B$ of compatible dimensions, calculate and return the product $AB$. Matrices $A$ and $B$ are unchanged.</para>
        /// <para>
        /// This method supports a variety of multiplication algorithms and implementations, including parallel, SIMD and Strassen's algorithm. 
        /// The multiplication algorithm can be specified using the <txt>algo</txt> parameter.
        /// </para>
        /// <para>Parallel version: <a href=\"#MultiplyParallel\"><txt><b>MultiplyParallel(IMatrix&lt;T&gt; A, IMatrix&lt;T&gt; B)</b></txt></a></para>
        /// <para>Also see:</para>
        /// <ul>
        ///     <li><a href=\"#MultiplyMatrixVector\"><txt><b>Multiply(IMatrix &lt;T&gt; A, T[] vector)</b></txt></a>, </li>
        ///     <li><a href=\"#MultiplyMatrixScalar\"><txt><b>Multiply(IMatrix &lt;T&gt; A, T scalar)</b></txt></a>, </li>
        ///     <li><a href=\"#MultiplyOverwrite\"><txt><b>MultiplyOverwrite(IMatrix &lt;T&gt; A, IMatrix&lt;T&gt; B, bool overwriteFirst)</b></txt></a>, </li>
        /// </ul>
        /// <b>Examples:</b>
        /// <pre><code class="cs">
        /// DenseMatrix<double> A = DenseMatrix.Random<double>(100, 100), B = DenseMatrix.Random<double>(100, 100);
        /// 
        /// // Multiply AB using (the default) single threaded pure-C# algorithm
        /// DenseMatrix<double> C1 = A.Multiply(B);
        /// 
        /// // Multiply AB using SIMD implementation
        /// DenseMatrix<double> C2 = A.Multiply(B, new DoubleSIMDLinearAlgebraProvider());
        /// 
        /// // Multiply AB using Strassen's algorithm on a single thread
        /// IStrassenInstructionSet<double> instructions = new DoubleStrassenInstructionSet();
        /// IStrassenMultiplicationAlgorithm<double> algo = new DefiniteSizeRectStrassenMultiplication<double>(
        ///                     instructions, 100, 100, 100, useWinogradVariant: true, threshold: 32);
        /// DenseMatrix<double> C3 = A.Multiply(B, algo);
        /// </code></pre>
        /// </summary>
        /// <name>Multiply_MatrixMatrix</name>
        /// <proto>IMatrix<T> Multiply(this IMatrix<T> A, IMatrix<T> B, IMultiplicationAlgorithm<int> algo = null)</proto>
        /// <cat>la</cat>
        /// <param name="A"></param>
        /// <param name="B"></param>
        /// <returns></returns>
        public static DenseMatrix<int> Multiply(this DenseMatrix<int> A, DenseMatrix<int> B, IMatrixMultiplication<int> algo = null) => MultiplyInner(A, B, algo ?? new NaiveMatrixMultiplication<int>(BLAS.Int32));
        public static DenseMatrix<long> Multiply(this DenseMatrix<long> A, DenseMatrix<long> B, IMatrixMultiplication<long> algo = null) => MultiplyInner(A, B, algo ?? new NaiveMatrixMultiplication<long>(BLAS.Int64));
        public static DenseMatrix<float> Multiply(this DenseMatrix<float> A, DenseMatrix<float> B, IMatrixMultiplication<float> algo = null) => MultiplyInner(A, B, algo ?? new NaiveMatrixMultiplication<float>(BLAS.Float));
        public static DenseMatrix<double> Multiply(this DenseMatrix<double> A, DenseMatrix<double> B, IMatrixMultiplication<double> algo = null) => MultiplyInner(A, B, algo ?? new NaiveMatrixMultiplication<double>(BLAS.Double));
        public static DenseMatrix<decimal> Multiply(this DenseMatrix<decimal> A, DenseMatrix<decimal> B, IMatrixMultiplication<decimal> algo = null) => MultiplyInner(A, B, algo ?? new NaiveMatrixMultiplication<decimal>(BLAS.Decimal));
        public static DenseMatrix<Complex> Multiply(this DenseMatrix<Complex> A, DenseMatrix<Complex> B, IMatrixMultiplication<Complex> algo = null) => MultiplyInner(A, B, algo ?? new NaiveMatrixMultiplication<Complex>(BLAS.Complex));
        public static DenseMatrix<T> Multiply<T>(this DenseMatrix<T> A, DenseMatrix<T> B, IMatrixMultiplication<T> algo = null) where T : IRing<T>, new() => MultiplyInner(A, B, algo ?? new NaiveMatrixMultiplication<T>(new NativeRingProvider<T>()));

        public static void Multiply(this DenseMatrix<int> A, DenseMatrix<int> B, DenseMatrix<int> result, bool increment = false, IMatrixMultiplication<int> algo = null)
        {
            MultiplyInner(A, B, result, increment, algo ?? new NaiveMatrixMultiplication<int>(BLAS.Int32));
        }
        public static void Multiply(this DenseMatrix<long> A, DenseMatrix<long> B, DenseMatrix<long> result, bool increment = false, IMatrixMultiplication<long> algo = null)
        {
            MultiplyInner(A, B, result, increment, algo ?? new NaiveMatrixMultiplication<long>(BLAS.Int64));
        }
        public static void Multiply(this DenseMatrix<float> A, DenseMatrix<float> B, DenseMatrix<float> result, bool increment = false, IMatrixMultiplication<float> algo = null)
        {
            MultiplyInner(A, B, result, increment, algo ?? new NaiveMatrixMultiplication<float>(BLAS.Float));
        }
        public static void Multiply(this DenseMatrix<double> A, DenseMatrix<double> B, DenseMatrix<double> result, bool increment = false, IMatrixMultiplication<double> algo = null)
        {
            MultiplyInner(A, B, result, increment, algo ?? new NaiveMatrixMultiplication<double>(BLAS.Double));
        }
        public static void Multiply(this DenseMatrix<decimal> A, DenseMatrix<decimal> B, DenseMatrix<decimal> result, bool increment = false, IMatrixMultiplication<decimal> algo = null)
        {
            MultiplyInner(A, B, result, increment, algo ?? new NaiveMatrixMultiplication<decimal>(BLAS.Decimal));
        }
        public static void Multiply(this DenseMatrix<Complex> A, DenseMatrix<Complex> B, DenseMatrix<Complex> result, bool increment = false, IMatrixMultiplication<Complex> algo = null)
        {
            MultiplyInner(A, B, result, increment, algo ?? new NaiveMatrixMultiplication<Complex>(BLAS.Complex));
        }
        public static void Multiply<T>(this DenseMatrix<T> A, DenseMatrix<T> B, DenseMatrix<T> result, bool increment = false, IMatrixMultiplication<T> algo = null) where T : IRing<T>, new()
        {
            MultiplyInner(A, B, result, increment, algo ?? new NaiveMatrixMultiplication<T>(new NativeRingProvider<T>()));
        }
        #endregion

        #region Parallel naive-multiplication 
        public static DenseMatrix<int> MultiplyParallel(this DenseMatrix<int> A, DenseMatrix<int> B) => Multiply(A, B, MatrixOperationsExtensions.multiply_parallel_unsafe);
        public static DenseMatrix<long> MultiplyParallel(this DenseMatrix<long> A, DenseMatrix<long> B) => Multiply(A, B, MatrixOperationsExtensions.multiply_parallel_unsafe);
        public static DenseMatrix<float> MultiplyParallel(this DenseMatrix<float> A, DenseMatrix<float> B) => Multiply(A, B, MatrixOperationsExtensions.multiply_parallel_unsafe);
        public static DenseMatrix<double> MultiplyParallel(this DenseMatrix<double> A, DenseMatrix<double> B) => Multiply(A, B, MatrixOperationsExtensions.multiply_parallel_unsafe);
        public static DenseMatrix<decimal> MultiplyParallel(this DenseMatrix<decimal> A, DenseMatrix<decimal> B) => Multiply(A, B, MatrixOperationsExtensions.multiply_parallel_unsafe);
        public static DenseMatrix<Complex> MultiplyParallel(this DenseMatrix<Complex> A, DenseMatrix<Complex> B) => Multiply(A, B, MatrixOperationsExtensions.multiply_parallel_unsafe);
        #endregion

        #region Multiplication overwrite 
        /// <summary>
        /// <para>
        /// Given square matrices $A, B\in\mathbb{F}^{n \times n}$, calculate the product $AB\in\mathbb{F}^{n \times n}$ and store it in either $A$ or $B$. This is the most 
        /// memory-efficient of the matrix multiplication algorithms, with $O(n)$ intermediary memory required.
        /// </para>
        /// </summary>
        /// <name>MultiplyOverwrite</name>
        /// <proto>void MultiplyOverwrite(this IMatrix<T> A, IMatrix<T> B, bool overwriteFirst = true)</proto>
        /// <param name="A"></param>
        /// <param name="B"></param>
        /// <param name="overwriteFirst"></param>
        public static void MultiplyOverwrite(this DenseMatrix<int> A, DenseMatrix<int> B, bool overwriteFirst = true) => MultiplyOverwrite(A, B, overwriteFirst, MatrixOperationsExtensions.multiply_overwrite_unsafe);
        public static void MultiplyOverwrite(this DenseMatrix<long> A, DenseMatrix<long> B, bool overwriteFirst = true) => MultiplyOverwrite(A, B, overwriteFirst, MatrixOperationsExtensions.multiply_overwrite_unsafe);
        public static void MultiplyOverwrite(this DenseMatrix<float> A, DenseMatrix<float> B, bool overwriteFirst = true) => MultiplyOverwrite(A, B, overwriteFirst, MatrixOperationsExtensions.multiply_overwrite_unsafe);
        public static void MultiplyOverwrite(this DenseMatrix<double> A, DenseMatrix<double> B, bool overwriteFirst = true) => MultiplyOverwrite(A, B, overwriteFirst, MatrixOperationsExtensions.multiply_overwrite_unsafe);
        public static void MultiplyOverwrite(this DenseMatrix<decimal> A, DenseMatrix<decimal> B, bool overwriteFirst = true) => MultiplyOverwrite(A, B, overwriteFirst, MatrixOperationsExtensions.multiply_overwrite_unsafe);
        public static void MultiplyOverwrite(this DenseMatrix<Complex> A, DenseMatrix<Complex> B, bool overwriteFirst = true) => MultiplyOverwrite(A, B, overwriteFirst, MatrixOperationsExtensions.multiply_overwrite_unsafe);
        public static void MultiplyOverwrite<T>(this DenseMatrix<T> A, DenseMatrix<T> B, bool overwriteFirst = true) where T : IRing<T>, new() => MultiplyOverwrite(A, B, overwriteFirst, MatrixOperationsExtensions.multiply_overwrite_unsafe);
        private static void MultiplyOverwrite<T>(this DenseMatrix<T> A, DenseMatrix<T> B, bool overwriteFirst, Action<T[][], T[][], bool> MultiplyOverwrite) where T : new()
        {
            MatrixChecks.CheckNotNull(A, B);
            if (A.Columns != B.Rows) throw new InvalidOperationException();
            MultiplyOverwrite(A.Values, B.Values, overwriteFirst);
        }

        public static void MultiplyOverwriteParallel(this DenseMatrix<int> A, DenseMatrix<int> B, bool overwriteFirst = true) => MultiplyOverwrite(A, B, overwriteFirst, MatrixOperationsExtensions.multiply_overwrite_parallel_unsafe);
        public static void MultiplyOverwriteParallel(this DenseMatrix<long> A, DenseMatrix<long> B, bool overwriteFirst = true) => MultiplyOverwrite(A, B, overwriteFirst, MatrixOperationsExtensions.multiply_overwrite_parallel_unsafe);
        public static void MultiplyOverwriteParallel(this DenseMatrix<float> A, DenseMatrix<float> B, bool overwriteFirst = true) => MultiplyOverwrite(A, B, overwriteFirst, MatrixOperationsExtensions.multiply_overwrite_parallel_unsafe);
        public static void MultiplyOverwriteParallel(this DenseMatrix<double> A, DenseMatrix<double> B, bool overwriteFirst = true) => MultiplyOverwrite(A, B, overwriteFirst, MatrixOperationsExtensions.multiply_overwrite_parallel_unsafe);
        public static void MultiplyOverwriteParallel(this DenseMatrix<decimal> A, DenseMatrix<decimal> B, bool overwriteFirst = true) => MultiplyOverwrite(A, B, overwriteFirst, MatrixOperationsExtensions.multiply_overwrite_parallel_unsafe);
        public static void MultiplyOverwriteParallel(this DenseMatrix<Complex> A, DenseMatrix<Complex> B, bool overwriteFirst = true) => MultiplyOverwrite(A, B, overwriteFirst, MatrixOperationsExtensions.multiply_overwrite_parallel_unsafe);
        public static void MultiplyOverwriteParallel<T>(this DenseMatrix<T> A, DenseMatrix<T> B, bool overwriteFirst = true) where T : IRing<T>, new() => MultiplyOverwrite(A, B, overwriteFirst, MatrixOperationsExtensions.multiply_overwrite_parallel_unsafe);
        #endregion

        #region Matrix-vector multiplication 

        /// <summary>
        /// <para>
        /// Given a matrix $A\in\mathbb{F}^{m \times n}$ and vector $x\in\mathbb{F}^n$ of compatible dimensions, 
        /// calculate and return the matrix-vector product $Ax\in\mathbb{F}^m$ as a vector. Matrix $A$ and vector $x$ are unchanged.
        /// </para>
        /// <para>
        /// The number of columns of $A$ must match the dimensionality of $x$, or a <txt>InvalidOperationException</txt> will be thrown.
        /// </para>
        /// <!--inputs-->
        /// </summary>
        /// <name>MultiplyMatrixVector</name>
        /// <proto>T[] Multiply(IMatrix<T> A, T[] x)</proto>
        /// <cat>la</cat>
        /// <param name="A">A matrix of size $m \times n$.</param>
        /// <param name="x">A vector of dimension $n$.</param>
        /// <returns>A vector representing the product Ax, of length m</returns>
        public static int[] Multiply(this DenseMatrix<int> A, int[] x) => Multiply(A, x, new NativeInt32Provider());
        public static long[] Multiply(this DenseMatrix<long> A, long[] x) => Multiply(A, x, new NativeInt64Provider());
        public static float[] Multiply(this DenseMatrix<float> A, float[] x) => Multiply(A, x, new NativeFloatProvider());
        public static double[] Multiply(this DenseMatrix<double> A, double[] x) => Multiply(A, x, new NativeDoubleProvider());
        public static decimal[] Multiply(this DenseMatrix<decimal> A, decimal[] x) => Multiply(A, x, new NativeDecimalProvider());
        public static Complex[] Multiply(this DenseMatrix<Complex> A, Complex[] x) => Multiply(A, x, new NativeComplexProvider());
        public static T[] Multiply<T>(this DenseMatrix<T> A, T[] x) where T : IRing<T>, new() => Multiply(A, x, new NativeRingProvider<T>());
        private static T[] Multiply<T>(this DenseMatrix<T> A, T[] x, IDenseBLAS1<T> blas) where T : new()
        {
            MatrixChecks.CheckNotNull(A);
            MatrixChecks.CheckNotNull(x);

            int rows = A.Rows, cols = A.Columns;
            MatrixChecks.CheckDimensions(cols, x.Length);

            T[] result = new T[rows];
            for (int i = 0; i < rows; i++)
            {
                result[i] = blas.DOT(A[i], x, 0, cols);
            }
            return result;
        }

        /// <summary>
        /// <para>
        /// Given a matrix $A\in\mathbb{F}^{m \times n}$ and vector $x\in\mathbb{F}^n$ of compatible dimensions, 
        /// calculate and return the matrix-vector product $Ax\in\mathbb{F}^m$ as a vector. Matrix $A$ and vector $x$ are unchanged.
        /// </para>
        /// <para>
        /// The number of columns of $A$ must match the dimensionality of $x$, or a <txt>InvalidOperationException</txt> will be thrown.
        /// </para>
        /// <para>Also see: <a href="#MultiplyMatrixVector"><txt>Multiply&lt;T&gt;(this DenseMatrix&lt;T&gt; A, DenseVector&lt;T&gt; x)</txt></a></para>
        /// <!--inputs-->
        /// </summary>
        /// <name>MultiplyMatrixVectorDense</name>
        /// <proto>IVector<T> Multiply(IMatrix<T> A, IVector<T> x)</proto>
        /// <cat>la</cat>
        /// <param name="A">A matrix of size $m \times n$.</param>
        /// <param name="x">A vector of dimension $n$.</param>
        /// <returns>A vector representing the product Ax, of length m</returns>
        public static DenseVector<int> Multiply(this DenseMatrix<int> A, DenseVector<int> x) => Multiply(A, x, new NativeInt32Provider());
        public static DenseVector<long> Multiply(this DenseMatrix<long> A, DenseVector<long> x) => Multiply(A, x, new NativeInt64Provider());
        public static DenseVector<float> Multiply(this DenseMatrix<float> A, DenseVector<float> x) => Multiply(A, x, new NativeFloatProvider());
        public static DenseVector<double> Multiply(this DenseMatrix<double> A, DenseVector<double> x) => Multiply(A, x, new NativeDoubleProvider());
        public static DenseVector<decimal> Multiply(this DenseMatrix<decimal> A, DenseVector<decimal> x) => Multiply(A, x, new NativeDecimalProvider());
        public static DenseVector<Complex> Multiply(this DenseMatrix<Complex> A, DenseVector<Complex> x) => Multiply(A, x, new NativeComplexProvider());
        public static DenseVector<T> Multiply<T>(this DenseMatrix<T> A, DenseVector<T> x) where T : IRing<T>, new() => Multiply(A, x, new NativeRingProvider<T>());
        private static DenseVector<T> Multiply<T>(this DenseMatrix<T> A, DenseVector<T> x, IDenseBLAS1<T> blas) where T : new()
        {
            VectorChecks.CheckNotNull(x);
            return new DenseVector<T>(Multiply(A, x.Values, blas));
        }

        #endregion

        #region Matrix-scalar multiplication

        /// <summary>
        /// Given a matrix $A\in\mathbb{F}^{m \times n}$ and $s\in\mathbb{F}$, calculate and return the matrix-scalar product $sA\in\mathbb{F}^{m \times n}$. Matrix $A$ will be unchanged.
        /// </summary>
        /// <name>MultiplyMatrixScalar</name>
        /// <proto>IMatrix<T> Multiply(this IMatrix<T> A, T scalar)</proto>
        /// <cat>la</cat>
        /// <param name="A"></param>
        /// <param name="s"></param>
        /// <returns></returns>
        public static DenseMatrix<int> Multiply(this DenseMatrix<int> A, int s) => Multiply(A, s, (a, b) => a * b);
        public static DenseMatrix<long> Multiply(this DenseMatrix<long> A, long s) => Multiply(A, s, (a, b) => a * b);
        public static DenseMatrix<float> Multiply(this DenseMatrix<float> A, float s) => Multiply(A, s, (a, b) => a * b);
        public static DenseMatrix<double> Multiply(this DenseMatrix<double> A, double s) => Multiply(A, s, (a, b) => a * b);
        public static DenseMatrix<decimal> Multiply(this DenseMatrix<decimal> A, decimal s) => Multiply(A, s, (a, b) => a * b);
        public static DenseMatrix<T> Multiply<T>(this DenseMatrix<T> A, T s) where T : IRing<T>, new() => Multiply(A, s, (a, b) => a.Multiply(b));
        private static DenseMatrix<T> Multiply<T>(this DenseMatrix<T> A, T s, Func<T, T, T> Multiply) where T : new()
        {
            MatrixChecks.CheckNotNull(A);

            int rows = A.Rows, cols = A.Columns, r, c;

            T[][] result = new T[rows][];
            for (r = 0; r < rows; ++r)
            {
                T[] row = new T[cols], A_r = A[r];
                for (c = 0; c < cols; ++c)
                {
                    row[c] = Multiply(A_r[c], s);
                }
                result[r] = row;
            }
            return result;
        }

        #endregion

        #region Matrix powers
        /// <summary>
        /// <para>For a matrix $A$, returns $A^k$ for integer $k$.</para>
        /// 
        /// <para>If $k = 0$, then the identity matrix will be returned.</para>
        /// 
        /// <p>If $k \le 0$, the matrix $A$ must be square.</p>
        /// 
        /// <para>
        /// Negative powers are not supported for integer matrices 
        /// since the inverse of a matrix over the integers is not (necessarily)
        /// another matrix over the integers.
        /// </para>
        /// 
        /// <para>
        /// This method uses recursive squaring, and has complexity $O(n^3 \log k)$
        /// </para>
        /// 
        /// </summary>
        /// <proto>DenseMatrix<T> Pow(this DenseMatrix<T> A, int power)</proto>
        /// <cat>la</cat>
        /// <param name="A">The matrix</param>
        /// <param name="power">The power to raise to.</param>
        /// <returns></returns>
        public static DenseMatrix<int> Pow(this DenseMatrix<int> A, int power) => RecursiveSquaringAlgorithm(A, power, (M, N) => M.Multiply(N));
        public static DenseMatrix<long> Pow(this DenseMatrix<long> A, int power) => RecursiveSquaringAlgorithm(A, power, (M, N) => M.Multiply(N));

        /// <summary>
        /// Successive squaring matrix power algorithm
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="A"></param>
        /// <param name="power"></param>
        /// <returns></returns>
        private static DenseMatrix<T> RecursiveSquaringAlgorithm<T>(DenseMatrix<T> A, int power, Func<DenseMatrix<T>, DenseMatrix<T>, DenseMatrix<T>> Multiply) where T : new()
        {
            if (power < 0)
            {
                throw new NotImplementedException();
            }
            if (power == 0)
            {
                MatrixChecks.CheckIsSquare(A);
                return DenseMatrix.Identity<T>(A.Rows);
            }
            if (power == 1)
            {
                return A.Copy();
            }

            // For powers > 1
            DenseMatrix<T> Apow = RecursiveSquaringAlgorithm(A, power / 2, Multiply);
            if (power % 2 == 0)
            {
                return Multiply(Apow, Apow);
            }
            else
            {
                return Multiply(A, Multiply(Apow, Apow));
            }
        }
        #endregion

        #region Kronecker sum

        /// <summary>
        /// <para>Given two square matrices $A\in\mathbb{F}^{n \times n}$, $B\in\mathbb{F}^{m \times m}$, calculates and returns the Kronecker sum, 
        /// $A \oplus B$, defined as $$A \oplus B := (A \otimes I_m) + (I_n \otimes B)$$ where 
        /// $\otimes$ denotes the Kronecker product, and $I_k$ is the identity matrix of size $k \times k$.</para>
        /// <para>The matrix returned will be of size $mn \times mn$.</para>
        /// </summary>
        /// <name>KroneckerSum</name>
        /// <proto>IMatrix<T> KroneckerSum(this IMatrix<T> A, IMatrix<T> B)</proto>
        /// <cat>la</cat>
        /// <typeparam name="T"></typeparam>
        /// <param name="A"></param>
        /// <param name="B"></param>
        /// <returns></returns>
        public static DenseMatrix<int> KroneckerSum(this DenseMatrix<int> A, DenseMatrix<int> B) => KroneckerSum(A, B, (a, b) => a + b);
        public static DenseMatrix<long> KroneckerSum(this DenseMatrix<long> A, DenseMatrix<long> B) => KroneckerSum(A, B, (a, b) => a + b);
        public static DenseMatrix<float> KroneckerSum(this DenseMatrix<float> A, DenseMatrix<float> B) => KroneckerSum(A, B, (a, b) => a + b);
        public static DenseMatrix<double> KroneckerSum(this DenseMatrix<double> A, DenseMatrix<double> B) => KroneckerSum(A, B, (a, b) => a + b);
        public static DenseMatrix<decimal> KroneckerSum(this DenseMatrix<decimal> A, DenseMatrix<decimal> B) => KroneckerSum(A, B, (a, b) => a + b);
        public static DenseMatrix<T> KroneckerSum<T>(this DenseMatrix<T> A, DenseMatrix<T> B) where T : IAdditiveGroup<T>, new() => KroneckerSum(A, B, (a, b) => a.Add(b));
        private static DenseMatrix<T> KroneckerSum<T>(this DenseMatrix<T> A, DenseMatrix<T> B, Func<T, T, T> Add) where T : new()
        {
            MatrixChecks.CheckNotNull(A, B);
            if (!A.IsSquare || !B.IsSquare) throw new InvalidOperationException("Matrix is not square");

            int n = A.Rows, m = B.Rows, d = m * n, i, j, k;

            DenseMatrix<T> matrix = DenseMatrix.Zeroes<T>(d, d);
            T[][] sum = matrix.Values;
            for (i = 0; i < n; ++i)
            {
                T[] A_i = A[i];
                int im = i * m;
                for (j = 0; j < n; ++j)
                {
                    T A_ij = A_i[j];
                    int jm = j * m;
                    for (k = 0; k < n; ++k)
                    {
                        sum[im + k][jm + k] = A_ij;
                    }
                }
            }

            for (k = 0; k < n; ++k)
            {
                int x = k * n;
                for (i = 0; i < m; ++i)
                {
                    T[] B_i = B[i], row = sum[x + i];
                    for (j = 0; j < m; ++j)
                    {
                        int b = x + j;
                        row[b] = Add(row[b], B_i[j]);
                    }
                }
            }

            return matrix;
        }

        #endregion

        #region Direct sum

        /// <summary>
        /// Calculates the direct sum, $A \oplus B$, where $A$ and $B$ are matrices of arbitrary size over the same algebra.
        /// Returns a matrix with dimensions equal to the sum of the dimensions of $A$ and $B$.
        /// </summary>
        /// <name>DirectSum</name>
        /// <proto>IMatrix<T> DirectSum(this IMatrix<T> A, IMatrix<T> B)</proto>
        /// <cat>la</cat>
        /// <typeparam name="T"></typeparam>
        /// <param name="A"></param>
        /// <param name="B"></param>
        /// <returns></returns>
        public static DenseMatrix<T> DirectSum<T>(this DenseMatrix<T> A, DenseMatrix<T> B) where T : new()
        {
            MatrixChecks.CheckNotNull(A, B);

            int m1 = A.Rows, n1 = A.Columns, m2 = B.Rows, n2 = B.Columns, i, j;

            DenseMatrix<T> matrix = DenseMatrix.Zeroes<T>(m1 + m2, n1 + n2);
            T[][] sum = matrix.Values;
            for (i = 0; i < m1; ++i)
            {
                T[] sum_i = sum[i], A_i = A[i];
                for (j = 0; j < n1; ++j)
                {
                    sum_i[j] = A_i[j];
                }
            }
            for (i = 0; i < m2; ++i)
            {
                T[] sum_i = sum[i + m1], B_i = B[i];
                for (j = 0; j < n2; ++j)
                {
                    sum_i[j + n1] = B_i[j];
                }
            }
            return matrix;
        }

        #endregion


        #region Kronecker Product

        /// <summary>
        /// <para>Given two matrices $A$ and $B$, calculates and returns the Kronecker Product $A\otimes B$.</para>
        /// <para>If $A\in\mathbb{F}^{m\times n}$ and $B\in\mathbb{F}^{p\times q}$, the matrix $(A \otimes B)\in\mathbb{F}^{mp\times nq}$ will be returned.</para>
        /// <!--inputs-->
        /// <!--returns-->
        /// </summary>
        /// <name>KroneckerProduct</name>
        /// <proto>IMatrix<T> KroneckerProduct(this IMatrix<T> A, IMatrix<T> B)</proto>
        /// <cat>la</cat>
        /// <param name="A">A matrix of dimension $m\times n$.</param>
        /// <param name="B">A matrix of dimension $p\times q$.</param>
        /// <returns>A matrix of dimension $mp\times nq$.</returns>
        public static DenseMatrix<int> KroneckerProduct(this DenseMatrix<int> A, DenseMatrix<int> B) => KroneckerProduct(A, B, (a, b) => a * b);
        public static DenseMatrix<long> KroneckerProduct(this DenseMatrix<long> A, DenseMatrix<long> B) => KroneckerProduct(A, B, (a, b) => a * b);
        public static DenseMatrix<float> KroneckerProduct(this DenseMatrix<float> A, DenseMatrix<float> B) => KroneckerProduct(A, B, (a, b) => a * b);
        public static DenseMatrix<double> KroneckerProduct(this DenseMatrix<double> A, DenseMatrix<double> B) => KroneckerProduct(A, B, (a, b) => a * b);
        public static DenseMatrix<decimal> KroneckerProduct(this DenseMatrix<decimal> A, DenseMatrix<decimal> B) => KroneckerProduct(A, B, (a, b) => a * b);
        public static DenseMatrix<T> KroneckerProduct<T>(this DenseMatrix<T> A, DenseMatrix<T> B) where T : IRing<T>, new() => KroneckerProduct(A, B, (a, b) => a.Multiply(b));
        private static DenseMatrix<T> KroneckerProduct<T>(this DenseMatrix<T> A, DenseMatrix<T> B, Func<T, T, T> Multiply) where T : new()
        {
            if (A == null || B == null)
            {
                throw new ArgumentNullException();
            }

            int m = A.Rows, n = A.Columns, p = B.Rows, q = B.Columns, i, j, k, l, row = 0;

            DenseMatrix<T> matrix = new DenseMatrix<T>(m * p, n * q);
            T[][] result = matrix.Values;
            for (i = 0; i < m; ++i)
            {
                T[] A_i = A[i];
                for (k = 0; k < p; ++k)
                {
                    T[] result_row = result[row++], B_k = B[k];
                    int col = 0;
                    for (j = 0; j < n; ++j)
                    {
                        T A_ij = A_i[j];
                        for (l = 0; l < q; ++l)
                        {
                            result_row[col++] = Multiply(A_ij, B_k[l]);
                        }
                    }
                }
            }
            return matrix;
        }

        #endregion

        #region Khatri-Rao Product

        private static DenseMatrix<T> KhatriRaoProduct<T>(DenseMatrix<T> A, DenseMatrix<T> B, Func<T, T, T> Multiply) where T : new()
        {
            MatrixChecks.CheckNotNull(A, B);
            if (A.Rows != B.Columns) throw new InvalidOperationException();

            int m = A.Rows, n = A.Columns, p = B.Rows, i, j, k, row;
            DenseMatrix<T> matrix = new DenseMatrix<T>(m * p, n);
            T[][] result = matrix.Values;

            for (j = 0; j < n; ++j)
            {
                row = 0;
                for (i = 0; i < m; ++i)
                {
                    T A_ij = A[i][j];
                    for (k = 0; k < p; ++k)
                    {
                        result[row++][j] = Multiply(A_ij, B[k][j]);
                    }
                }
            }
            return matrix;
        }
        /// <summary>
        /// <para>Given two matrices $A$ and $B$, calculates and returns the Khatri-Rao Product $A*B$.</para>
        /// <para>If $A\in\mathbb{F}^{m\times n}$ and $B\in\mathbb{F}^{p\times m}$, a matrix of dimensions ($mp\times n$) will be returned.</para>
        /// </summary>
        /// <name>KhatriRaoProduct</name>
        /// <proto>IMatrix<T> KhatriRaoProduct(this IMatrix<T> A,  IMatrix<T> B)</proto>
        /// <cat>la</cat>
        /// <param name="A"></param>
        /// <param name="B"></param>
        /// <returns></returns>
        public static DenseMatrix<int> KhatriRaoProduct(this DenseMatrix<int> A, DenseMatrix<int> B) => KhatriRaoProduct(A, B, (a, b) => a * b);
        public static DenseMatrix<long> KhatriRaoProduct(this DenseMatrix<long> A, DenseMatrix<long> B) => KhatriRaoProduct(A, B, (a, b) => a * b);
        public static DenseMatrix<float> KhatriRaoProduct(this DenseMatrix<float> A, DenseMatrix<float> B) => KhatriRaoProduct(A, B, (a, b) => a * b);
        public static DenseMatrix<double> KhatriRaoProduct(this DenseMatrix<double> A, DenseMatrix<double> B) => KhatriRaoProduct(A, B, (a, b) => a * b);
        public static DenseMatrix<decimal> KhatriRaoProduct(this DenseMatrix<decimal> A, DenseMatrix<decimal> B) => KhatriRaoProduct(A, B, (a, b) => a * b);
        public static DenseMatrix<T> KhatriRaoProduct<T>(this DenseMatrix<T> A, DenseMatrix<T> B) where T : IRing<T>, new() => KhatriRaoProduct(A, B, (a, b) => a.Multiply(b));

        #endregion

        #region Hadamard product

        /// <summary>
        /// <para>Given two matrices $A$ and $B$ of the same dimensions, calculates and returns the Hadamard Product $A \circ B$, defined elementwise by:
        /// $$(A \circ B)_{ij} := A_{ij}B_{ij}$$
        /// </para>
        /// Also see:
        /// <ul>
        /// <li><a href="#Elementwise"><txt><b>Elementwise(this DenseMatrix<T> A, DenseMatrix<T> B)</b></txt></a></li>
        /// <li><a href="#HadamardPower"><txt><b>HadamardPower(this DenseMatrix<T> A, T power)</b></txt></a></li>
        /// </ul>
        /// </summary>
        /// <name>HadamardProduct</name>
        /// <proto>IMatrix<T> HadamardProduct(this IMatrix<T> A,  IMatrix<T> B)</proto>
        /// <cat>la</cat>
        /// <param name="A"></param>
        /// <param name="B"></param>
        /// <returns></returns>
        public static DenseMatrix<int> HadamardProduct(this DenseMatrix<int> A, DenseMatrix<int> B) => HadamardProduct(A, B, (a, b) => a * b);
        public static DenseMatrix<long> HadamardProduct(this DenseMatrix<long> A, DenseMatrix<long> B) => HadamardProduct(A, B, (a, b) => a * b);
        public static DenseMatrix<float> HadamardProduct(this DenseMatrix<float> A, DenseMatrix<float> B) => HadamardProduct(A, B, (a, b) => a * b);
        public static DenseMatrix<double> HadamardProduct(this DenseMatrix<double> A, DenseMatrix<double> B) => HadamardProduct(A, B, (a, b) => a * b);
        public static DenseMatrix<decimal> HadamardProduct(this DenseMatrix<decimal> A, DenseMatrix<decimal> B) => HadamardProduct(A, B, (a, b) => a * b);
        public static DenseMatrix<T> HadamardProduct<T>(this DenseMatrix<T> A, DenseMatrix<T> B) where T : IRing<T>, new() => HadamardProduct(A, B, (a, b) => a.Multiply(b));
        private static DenseMatrix<T> HadamardProduct<T>(this DenseMatrix<T> A, DenseMatrix<T> B, Func<T, T, T> Multiply) where T : new()
        {
            MatrixChecks.CheckNotNull(A, B);
            MatrixChecks.AssertMatrixDimensionsEqual(A, B);

            int m = A.Rows, n = A.Columns, i, j;

            DenseMatrix<T> matrix = new DenseMatrix<T>(m, n);
            T[][] result = matrix.Values;
            for (i = 0; i < m; ++i)
            {
                T[] A_i = A[i], B_i = B[i], result_i = result[i];
                for (j = 0; j < n; ++j)
                {
                    result_i[j] = Multiply(A_i[j], B_i[j]);
                }
            }
            return matrix;
        }

        #endregion

        #region Hadamard power

        /// <summary>
        /// <para>Given a matrix $A$, calculates and returns the Hadamard power $A^{\circ n}$, defined elementwise by:
        /// $$(A^{\circ n})_{ij} := A_{ij}^n$$
        /// </para>
        /// Also see:
        /// <ul>
        /// <li><a href="#Elementwise"><txt><b>Elementwise(this DenseMatrix&lt;T&gt; A, DenseMatrix&lt;T&gt; B)</b></txt></a></li>
        /// <li><a href="#HadamardProduct"><txt><b>HadamardProduct(this DenseMatrix&lt;T&gt; A, DenseMatrix&lt;T&gt; B)</b></txt></a></li>
        /// </ul>
        /// </summary>
        /// <name>HadamardPower</name>
        /// <proto>IMatrix<T> HadamardPower(this IMatrix<T> A,  IMatrix<T> B)</proto>
        /// <cat>la</cat>
        /// <param name="A"></param>
        /// <param name="B"></param>
        /// <returns></returns>
        public static DenseMatrix<int> HadamardPower(this DenseMatrix<int> A, int power) => HadamardPower(A, power, (a, e) => IntegerMath.Pow(a, e));
        public static DenseMatrix<long> HadamardPower(this DenseMatrix<long> A, int power) => HadamardPower(A, power, (a, e) => IntegerMath.Pow(a, e));
        public static DenseMatrix<float> HadamardPower(this DenseMatrix<float> A, float power) => HadamardPower(A, power, (a, e) => (float)Math.Pow(a, e));
        public static DenseMatrix<double> HadamardPower(this DenseMatrix<double> A, double power) => HadamardPower(A, power, (a, e) => Math.Pow(a, e));
        public static DenseMatrix<decimal> HadamardPower(this DenseMatrix<decimal> A, decimal power) => HadamardPower(A, power, (a, e) => DecimalMath.Pow(a, e));
        public static DenseMatrix<Complex> HadamardPower(this DenseMatrix<Complex> A, double power) => HadamardPower(A, power, (a, e) => Complex.Pow(a, e.Real));
        private static DenseMatrix<T> HadamardPower<T>(this DenseMatrix<T> A, T exponent, Func<T, T, T> Exp) where T : new()
        {
            MatrixChecks.CheckNotNull(A);

            int m = A.Rows, n = A.Columns, i, j;

            DenseMatrix<T> matrix = new DenseMatrix<T>(m, n);
            T[][] result = matrix.Values;
            for (i = 0; i < m; ++i)
            {
                T[] result_i = result[i], A_i = A[i];
                for (j = 0; j < m; ++j)
                {
                    result_i[j] = Exp(A_i[j], exponent);
                }
            }
            return matrix;
        }

        #endregion

        #region Elementwise-operations 
        /// <summary>
        /// Apply the function $f:F\to T$ on every element of a matrix $A\in F^{m \times n}$, returning a matrix $B\in T^{m \times n}$, where
        /// $$B_{ij} = f(A_{ij})$$ for each $1 \le {i} \le {m}, 1 \le {j} \le {n}.$
        /// <!--inputs-->
        /// 
        /// <h4>Example</h4>
        /// <pre><code class="cs">
        /// DenseMatrix&lt;double&gt; A = DenseMatrix.Random&lt;double&gt;(10, 10);
        /// 
        /// // Convert from a DenseMatrix&lt;double&gt; to DenseMatrix&lt;float&gt;
        /// DenseMatrix&lt;float&gt; Af = A.Elementwise(x => (float)x);
        /// 
        /// // Elementwise square root
        /// DenseMatrix&lt;double&gt; sqrtA = A.Elementwise(x => Math.Sqrt(x));
        /// 
        /// </code></pre>
        /// </summary>
        /// <name>Elementwise</name>
        /// <proto>IMatrix<T> Elementwise(this IMatrix<T> A, Func<F, T> Operation)</proto>
        /// <cat>la</cat>
        /// <typeparam name="F"></typeparam>
        /// <typeparam name="T"></typeparam>
        /// <param name="A">Any matrix over the type <txt>F</txt>.</param>
        /// <param name="Operation">
        /// A function $f:$ <txt>F</txt> $\to$ <txt>T</txt>. The function to evaluate on each element of the matrix.
        /// </param>
        /// <returns></returns>
        public static DenseMatrix<T> Elementwise<F, T>(this DenseMatrix<F> A, Func<F, T> Operation) where T : new() where F : new()
        {
            MatrixChecks.CheckNotNull(A);

            int m = A.Rows, n = A.Columns, i, j;

            DenseMatrix<T> matrix = new DenseMatrix<T>(m, n);
            T[][] result = matrix.Values;
            for (i = 0; i < m; ++i)
            {
                F[] A_i = A[i];
                T[] result_i = result[i];
                for (j = 0; j < n; ++j)
                {
                    result_i[j] = Operation(A_i[j]);
                }
            }
            return matrix;
        }

        /// <summary>
        /// Apply a binary function $f: S \times T \to U$ elementwise to a pair of matrices $A\in S^{m\times n}$ and $B\in T^{m\times n}$, returning a new matrix 
        /// $C\in U^{m\times n}$ where 
        /// $$C_{ij} = f(A_{ij}, B_{ij}).$$
        /// </summary>
        /// <name>ElementwiseBinary</name>
        /// <proto>IMatrix<U> Elementwise(this IMatrix<S> A, IMatrix<T> B, Func<S, T, U> Function)</proto>
        /// <cat>la</cat>
        /// <typeparam name="S"></typeparam>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="U"></typeparam>
        /// <param name="A"></param>
        /// <param name="B"></param>
        /// <param name="Function"></param>
        /// <returns></returns>
        public static DenseMatrix<U> Elementwise<S, T, U>(this DenseMatrix<S> A, DenseMatrix<T> B, Func<S, T, U> Function) where S : new() where T : new() where U : new()
        {
            if (A == null)
            {
                throw new ArgumentNullException(nameof(A));
            }
            if (B == null)
            {
                throw new ArgumentNullException(nameof(B));
            }
            if (A.Rows != B.Rows || A.Columns != B.Columns)
            {
                throw new InvalidOperationException();
            }

            int m = A.Rows, n = A.Columns, i;
            U[][] result = new U[m][];
            for (i = 0; i < m; ++i)
            {
                result[i] = A[i].Elementwise(B[i], Function);
            }
            return new DenseMatrix<U>(m, n, result);
        }
        public static DenseMatrix<U> ElementwiseParallel<S, T, U>(this DenseMatrix<S> A, DenseMatrix<T> B, Func<S, T, U> Function) where S : new() where T : new() where U : new()
        {
            if (A == null)
            {
                throw new ArgumentNullException(nameof(A));
            }
            if (B == null)
            {
                throw new ArgumentNullException(nameof(B));
            }
            if (A.Rows != B.Rows || A.Columns != B.Columns)
            {
                throw new InvalidOperationException();
            }

            U[][] result = new U[A.Rows][];
            Parallel.ForEach(Partitioner.Create(0, A.Rows), range =>
            {
                int min = range.Item1, max = range.Item2, i;
                for (i = min; i < max; ++i)
                {
                    result[i] = A[i].Elementwise(B[i], Function);
                }
            });
            return new DenseMatrix<U>(A.Rows, A.Columns, result);
        }
        #endregion

        #region Summations across a dimension 

        /// <summary>
        /// Returns a vector $v\in\mathbb{F}^{m}$ containing the row sums of a matrix $A\in\mathbb{F}^{m\times n}$, where 
        /// $$v_i = \sum_{1\le{j}\le{n}}{A_{ij}}$$
        /// <para>Also see: <a href="#ColumnSums"><txt>ColumnSums</txt></a>.</para>
        /// </summary>
        /// <name>RowSums</name>
        /// <proto>DenseVector<T> RowSums(this DenseMatrix<T> A)</proto>
        /// <cat>la</cat>
        /// <param name="A"></param>
        /// <returns></returns>
        public static DenseVector<int> RowSums(this DenseMatrix<int> A) => RowSums(A, new Int32Provider());
        public static DenseVector<long> RowSums(this DenseMatrix<long> A) => RowSums(A, new Int64Provider());
        public static DenseVector<float> RowSums(this DenseMatrix<float> A) => RowSums(A, new FloatProvider());
        public static DenseVector<double> RowSums(this DenseMatrix<double> A) => RowSums(A, new DoubleProvider());
        public static DenseVector<decimal> RowSums(this DenseMatrix<decimal> A) => RowSums(A, new DecimalProvider());
        public static DenseVector<Complex> RowSums(this DenseMatrix<Complex> A) => RowSums(A, new ComplexProvider());
        private static DenseVector<T> RowSums<T>(this DenseMatrix<T> A, IProvider<T> blas) where T : new()
        {
            int rows = A.Rows, cols = A.Columns, i, j;
            T[] row_sums = new T[rows];
            for (i = 0; i < rows; ++i)
            {
                T[] row = A[i];
                T sum = blas.Zero;
                for (j = 0; j < cols; ++j)
                {
                    sum = blas.Add(sum, row[j]);
                }
                row_sums[i] = sum;
            }
            return new DenseVector<T>(row_sums);
        }

        /// <summary>
        /// Returns the <a href="#RowSums">row sum</a> of a submatrix from a matrix $A$.
        /// <para>Also see: <a href="#RowSums"><txt>RowSums</txt></a>.</para>
        /// <!--inputs-->
        /// </summary>
        /// <name>RowSumsSubmatrix</name>
        /// <proto>DenseVector<T> RowSums(this DenseMatrix<T> A, int rowStart, int columnStart, int rows, int columns)</proto>
        /// <cat>la</cat>
        /// <param name="A">The matrix $A$ whose row sums are to be calculated.</param>
        /// <param name="rowStart">The first row to sum over.</param>
        /// <param name="columnStart">The column index to begin summing over (for each row).</param>
        /// <param name="rows">The number of rows, will be equal to the dimension of the returned vector.</param>
        /// <param name="columns">The number of columns to sum over for each row.</param>
        /// <returns></returns>
        public static DenseVector<int> RowSums(this DenseMatrix<int> A, int rowStart, int columnStart, int rows, int columns) => RowSums(A, rowStart, columnStart, rows, columns, new Int32Provider());
        public static DenseVector<long> RowSums(this DenseMatrix<long> A, int rowStart, int columnStart, int rows, int columns) => RowSums(A, rowStart, columnStart, rows, columns, new Int64Provider());
        public static DenseVector<float> RowSums(this DenseMatrix<float> A, int rowStart, int columnStart, int rows, int columns) => RowSums(A, rowStart, columnStart, rows, columns, new FloatProvider());
        public static DenseVector<double> RowSums(this DenseMatrix<double> A, int rowStart, int columnStart, int rows, int columns) => RowSums(A, rowStart, columnStart, rows, columns, new DoubleProvider());
        public static DenseVector<decimal> RowSums(this DenseMatrix<decimal> A, int rowStart, int columnStart, int rows, int columns) => RowSums(A, rowStart, columnStart, rows, columns, new DecimalProvider());
        public static DenseVector<Complex> RowSums(this DenseMatrix<Complex> A, int rowStart, int columnStart, int rows, int columns) => RowSums(A, rowStart, columnStart, rows, columns, new ComplexProvider());
        private static DenseVector<T> RowSums<T>(this DenseMatrix<T> A, int rowStart, int columnStart, int rows, int columns, IProvider<T> blas) where T : new()
        {
            if (rowStart < 0 || rowStart + rows > A.Rows)
            {
                throw new ArgumentOutOfRangeException();
            }
            if (columnStart < 0 || columnStart + columns > A.Columns)
            {
                throw new ArgumentOutOfRangeException();
            }

            int columnEnd = columnStart + columns, i, j;
            T[] row_sums = new T[rows];
            for (i = 0; i < rows; ++i)
            {
                T[] row = A[i + rowStart];
                T sum = blas.Zero;
                for (j = columnStart; j < columnEnd; ++j)
                {
                    sum = blas.Add(sum, row[j]);
                }
                row_sums[i] = sum;
            }
            return new DenseVector<T>(row_sums);
        }

        /// <summary>
        /// Returns a vector $v\in\mathbb{F}^{n}$ containing the row sums of a matrix $A\in\mathbb{F}^{m\times n}$, where 
        /// $$v_j = \sum_{1\le{i}\le{m}}{A_{ij}}$$
        /// <para>Also see: <a href="#RowSums"><txt>RowSums</txt></a>.</para>
        /// </summary>
        /// <name>ColumnSums</name>
        /// <proto>DenseVector<T> ColumnSums(this DenseMatrix<T> A)</proto>
        /// <cat>la</cat>
        /// <param name="A"></param>
        /// <returns></returns>
        public static DenseVector<int> ColumnSums(this DenseMatrix<int> A) => ColumnSums(A, new Int32Provider());
        public static DenseVector<long> ColumnSums(this DenseMatrix<long> A) => ColumnSums(A, new Int64Provider());
        public static DenseVector<float> ColumnSums(this DenseMatrix<float> A) => ColumnSums(A, new FloatProvider());
        public static DenseVector<double> ColumnSums(this DenseMatrix<double> A) => ColumnSums(A, new DoubleProvider());
        public static DenseVector<decimal> ColumnSums(this DenseMatrix<decimal> A) => ColumnSums(A, new DecimalProvider());
        public static DenseVector<Complex> ColumnSums(this DenseMatrix<Complex> A) => ColumnSums(A, new ComplexProvider());
        private static DenseVector<T> ColumnSums<T>(this DenseMatrix<T> A, IProvider<T> blas) where T : new()
        {
            MatrixChecks.CheckNotNull(A);

            int rows = A.Rows, cols = A.Columns, i, j;
            T[] col_sums = new T[cols];

            for (i = 0; i < rows; ++i)
            {
                T[] row = A[i];
                for (j = 0; j < cols; ++j)
                {
                    col_sums[j] = blas.Add(col_sums[j], row[j]);
                }
            }
            return new DenseVector<T>(col_sums);
        }

        /// <summary>
        /// Returns the <a href="#ColumnSums">column sum</a> of a submatrix from a matrix $A$.
        /// <para>Also see: <a href="#ColumnSums"><txt>ColumnSums</txt></a>.</para>
        /// <!--inputs-->
        /// </summary>
        /// <name>ColumnSumsSubmatrix</name>
        /// <proto>DenseVector<T> ColumnSums(this DenseMatrix<T> A, int rowStart, int columnStart, int rows, int columns)</proto>
        /// <cat>la</cat>
        /// <param name="A">The matrix $A$ whose row sums are to be calculated.</param>
        /// <param name="rowStart">The row index to begin summing over (for each column).</param>
        /// <param name="columnStart">The first column to sum over.</param>
        /// <param name="rows">The number of rows to sum over for each column.</param>
        /// <param name="columns">The number of columns, will be equal to the dimension of the returned vector.</param>
        /// <returns></returns>
        public static DenseVector<int> ColumnSums(this DenseMatrix<int> A, int rowStart, int columnStart, int rows, int columns) => ColumnSums(A, rowStart, columnStart, rows, columns, new Int32Provider());
        public static DenseVector<long> ColumnSums(this DenseMatrix<long> A, int rowStart, int columnStart, int rows, int columns) => ColumnSums(A, rowStart, columnStart, rows, columns, new Int64Provider());
        public static DenseVector<float> ColumnSums(this DenseMatrix<float> A, int rowStart, int columnStart, int rows, int columns) => ColumnSums(A, rowStart, columnStart, rows, columns, new FloatProvider());
        public static DenseVector<double> ColumnSums(this DenseMatrix<double> A, int rowStart, int columnStart, int rows, int columns) => ColumnSums(A, rowStart, columnStart, rows, columns, new DoubleProvider());
        public static DenseVector<decimal> ColumnSums(this DenseMatrix<decimal> A, int rowStart, int columnStart, int rows, int columns) => ColumnSums(A, rowStart, columnStart, rows, columns, new DecimalProvider());
        public static DenseVector<Complex> ColumnSums(this DenseMatrix<Complex> A, int rowStart, int columnStart, int rows, int columns) => ColumnSums(A, rowStart, columnStart, rows, columns, new ComplexProvider());
        private static DenseVector<T> ColumnSums<T>(this DenseMatrix<T> A, int rowStart, int columnStart, int rows, int columns, IProvider<T> blas) where T : new()
        {
            MatrixChecks.CheckNotNull(A);

            if (rowStart < 0 || rowStart + rows > A.Rows)
            {
                throw new ArgumentOutOfRangeException();
            }
            if (columnStart < 0 || columnStart + columns > A.Columns)
            {
                throw new ArgumentOutOfRangeException();
            }

            T[] col_sums = new T[columns];

            int rowEnd = rowStart + rows, i, j;
            for (i = rowStart; i < rowEnd; ++i)
            {
                T[] row = A[i];
                for (j = 0; j < columns; ++j)
                {
                    col_sums[j] = blas.Add(col_sums[j], row[j + columnStart]);
                }
            }
            return new DenseVector<T>(col_sums);
        }
        #endregion

        #region Frobenius inner product
        /// <summary>
        /// <para>Given two matrices $A, B\in\mathbb{C}^{m \times n}$, calculates and returns the Frobenius inner product, $\lang A, B \rang_F$, defined as
        /// $$\lang A, B \rang_F := \sum_{\substack{0\le i\le{m}\\0\le j\le{n}}} {\bar{A_{ij}}B_{ij}}$$
        /// </para>
        /// </summary>
        /// <name>FrobeniusInnerProduct</name>
        /// <proto>Complex FrobeniusInnerProduct(this IMatrix<Complex> A, IMatrix<Complex> B)</proto>
        /// <cat>la</cat>
        /// <param name="A"></param>
        /// <param name="B"></param>
        /// <returns></returns>
        public static Complex FrobeniusInnerProduct(this DenseMatrix<Complex> A, DenseMatrix<Complex> B)
        {
            MatrixChecks.CheckNotNull(A, B);
            MatrixChecks.AssertMatrixDimensionsEqual(A, B);

            //ComplexBLAS blas = new ComplexBLAS();
            double sum_re = 0.0, sum_im = 0.0;

            Complex[][] _A = A.Values, _B = B.Values;
            int m = A.Rows, n = A.Columns, i, j;
            for (i = 0; i < m; ++i)
            {
                Complex[] A_i = _A[i], B_i = _B[i];
                for (j = 0; j < n; ++j)
                {
                    Complex a = A_i[j], b = B_i[j];
                    sum_re += a.Real * b.Real + a.Imaginary * b.Imaginary;
                    sum_im += a.Real * b.Imaginary - a.Imaginary * b.Real;
                }
            }
            return new Complex(sum_re, sum_im);
        }
        #endregion
    }
}
