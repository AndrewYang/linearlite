﻿using LinearNet.Providers;
using LinearNet.Structs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Matrices
{
    public static class DenseMatrixRingExtensions
    {
        /// <summary>
        /// <para>Returns the trace (sum of diagonal elements) for a given square matrix $A$. </para>
        /// <para>Implemented for matrices over <txt>int</txt>, <txt>long</txt>, <txt>float</txt>, <txt>double</txt>, <txt>decimal</txt> and all <txt>T : IAdditiveGroup&lt;T&gt;</txt></para>
        /// <para>If $A$ is not square, <txt>InvalidOperationException</txt> will be thrown.</para>
        /// </summary>
        /// <name>Trace</name>
        /// <proto>T Trace(this IMatrix<T> A)</proto>
        /// <cat>la</cat>
        /// <param name="a">A square matrix.</param>
        /// <returns>The trace of the matrix.</returns>
        public static int Trace(this DenseMatrix<int> A) => Trace(A, new Int32Provider());
        public static long Trace(this DenseMatrix<long> A) => Trace(A, new Int64Provider());
        public static float Trace(this DenseMatrix<float> A) => Trace(A, new FloatProvider());
        public static double Trace(this DenseMatrix<double> A) => Trace(A, new DoubleProvider());
        public static decimal Trace(this DenseMatrix<decimal> A) => Trace(A, new DecimalProvider());
        public static T Trace<T>(this DenseMatrix<T> A) where T : IRing<T>, new() => Trace(A, new RingProvider<T>());
        internal static T Trace<T>(DenseMatrix<T> A, IProvider<T> blas) where T : new()
        {
            MatrixChecks.CheckNotNull(A);
            MatrixChecks.CheckIsSquare(A);

            int n = A.Rows, i;
            T tr = blas.Zero;
            for (i = 0; i < n; ++i) tr = blas.Add(tr, A[i][i]);
            return tr;
        }
    }
}
