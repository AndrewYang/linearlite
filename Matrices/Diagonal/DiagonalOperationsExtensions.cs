﻿using LinearNet.Providers;
using LinearNet.Helpers;
using LinearNet.Structs;
using System;
using LinearNet.Providers.LinearAlgebra;

namespace LinearNet.Matrices
{
    public static class DiagonalOperationsExtensions
    {
        /// <summary>
        /// Returns the first diagonal matrix subtract the second diagonal matrix, without changing the original matrices.
        /// </summary>
        /// <param name="A">The first matrix.</param>
        /// <param name="B">The second matrix.</param>
        /// <returns>The matrix difference.</returns>
        /// <cat>la</cat>
        public static DiagonalMatrix<int> Subtract(this DiagonalMatrix<int> A, DiagonalMatrix<int> B) => A.Subtract(B, (a, b) => a - b);
        public static DiagonalMatrix<long> Subtract(this DiagonalMatrix<long> A, DiagonalMatrix<long> B) => A.Subtract(B, (a, b) => a - b);
        public static DiagonalMatrix<float> Subtract(this DiagonalMatrix<float> A, DiagonalMatrix<float> B) => A.Subtract(B, (a, b) => a - b);
        public static DiagonalMatrix<double> Subtract(this DiagonalMatrix<double> A, DiagonalMatrix<double> B) => A.Subtract(B, (a, b) => a - b);
        public static DiagonalMatrix<decimal> Subtract(this DiagonalMatrix<decimal> A, DiagonalMatrix<decimal> B) => A.Subtract(B, (a, b) => a - b);
        public static DiagonalMatrix<T> Subtract<T>(this DiagonalMatrix<T> A, DiagonalMatrix<T> B) where T : IAdditiveGroup<T>, new() => A.Subtract(B, (a, b) => a.Subtract(b));

        /// <summary>
        /// Returns the product between two diagonal matrices as another diagoanl matrix, without changing either of the original matrices.
        /// </summary>
        /// <param name="A">The first matrix.</param>
        /// <param name="B">The second matrix.</param>
        /// <returns>The matrix product.</returns>
        /// <cat>la</cat>
        public static DiagonalMatrix<int> Multiply(this DiagonalMatrix<int> A, DiagonalMatrix<int> B) => A.Multiply(B, (a, b) => a * b);
        public static DiagonalMatrix<long> Multiply(this DiagonalMatrix<long> A, DiagonalMatrix<long> B) => A.Multiply(B, (a, b) => a * b);
        public static DiagonalMatrix<float> Multiply(this DiagonalMatrix<float> A, DiagonalMatrix<float> B) => A.Multiply(B, (a, b) => a * b);
        public static DiagonalMatrix<double> Multiply(this DiagonalMatrix<double> A, DiagonalMatrix<double> B) => A.Multiply(B, (a, b) => a * b);
        public static DiagonalMatrix<decimal> Multiply(this DiagonalMatrix<decimal> A, DiagonalMatrix<decimal> B) => A.Multiply(B, (a, b) => a * b);
        public static DiagonalMatrix<T> Multiply<T>(this DiagonalMatrix<T> A, DiagonalMatrix<T> B) where T : IRing<T>, new() => A.Multiply(B, (a, b) => a.Multiply(b));

        /// <summary>
        /// Returns the product between a band matrix and a dense vector, as another dense vector. Neither the matrix nor the original vector are changed.
        /// </summary>
        /// <param name="A">A diagonal matrix.</param>
        /// <param name="v">A vector.</param>
        /// <returns>The matrix-vector product.</returns>
        /// <cat>la</cat>
        public static int[] Multiply(this DiagonalMatrix<int> A, int[] B) => A.Multiply(B, DefaultProviders.Int32);
        public static long[] Multiply(this DiagonalMatrix<long> A, long[] B) => A.Multiply(B, DefaultProviders.Int64);
        public static float[] Multiply(this DiagonalMatrix<float> A, float[] B) => A.Multiply(B, DefaultProviders.Float);
        public static double[] Multiply(this DiagonalMatrix<double> A, double[] B) => A.Multiply(B, DefaultProviders.Double);
        public static decimal[] Multiply(this DiagonalMatrix<decimal> A, decimal[] B) => A.Multiply(B, DefaultProviders.Decimal);
        public static T[] Multiply<T>(this DiagonalMatrix<T> A, T[] B) where T : IRing<T>, new() => A.Multiply(B, new RingProvider<T>());

        /// <summary>
        /// Returns the product between a band matrix and a vector, as an array. Neither the matrix nor the original array are changed.
        /// </summary>
        /// <param name="A">A diagonal matrix.</param>
        /// <param name="v">A vector.</param>
        /// <returns>The matrix-vector product.</returns>
        /// <cat>la</cat>
        public static DiagonalMatrix<int> Multiply(this DiagonalMatrix<int> A, int s) => A.Multiply(s, new NativeInt32Provider());
        public static DiagonalMatrix<long> Multiply(this DiagonalMatrix<long> A, long s) => A.Multiply(s, new NativeInt64Provider());
        public static DiagonalMatrix<float> Multiply(this DiagonalMatrix<float> A, float s) => A.Multiply(s, new NativeFloatProvider());
        public static DiagonalMatrix<double> Multiply(this DiagonalMatrix<double> A, double s) => A.Multiply(s, new NativeDoubleProvider());
        public static DiagonalMatrix<decimal> Multiply(this DiagonalMatrix<decimal> A, decimal s) => A.Multiply(s, new NativeDecimalProvider());
        public static DiagonalMatrix<T> Multiply<T>(this DiagonalMatrix<T> A, T s) where T : IRing<T>, new() => A.Multiply(s, new NativeRingProvider<T>());

        /// <summary>
        /// Raises a square diagonal matrix to a non-negative integer power, $A^n$, as another square diagonal matrix, without altering the original matrix.
        /// </summary>
        /// <param name="A">A diagonal matrix.</param>
        /// <param name="power">The non-negative integer power to raise the matrix to.</param>
        /// <returns>The matrix-power.</returns>
        /// <cat>la</cat>
        public static DiagonalMatrix<int> Pow(this DiagonalMatrix<int> A, int power) => A.Pow(power, (a, b) => IntegerMath.Pow(a, b));
        public static DiagonalMatrix<long> Pow(this DiagonalMatrix<long> A, int power) => A.Pow(power, (a, b) => IntegerMath.Pow(a, b));
        public static DiagonalMatrix<float> Pow(this DiagonalMatrix<float> A, int power) => A.Pow(power, (a, b) => (float)Math.Pow(a, b));
        public static DiagonalMatrix<double> Pow(this DiagonalMatrix<double> A, int power) => A.Pow(power, (a, b) => Math.Pow(a, b));
        public static DiagonalMatrix<decimal> Pow(this DiagonalMatrix<decimal> A, int power) => A.Pow(power, (a, b) => DecimalMath.Pow(a, b));
        public static DiagonalMatrix<Complex> Pow(this DiagonalMatrix<Complex> A, int power) => A.Pow(power, (a, b) => Complex.Pow(a, b));

        /// <summary>
        /// Returns the matrix exponential of a square diagonal matrix, $\exp(A)$, as another square diagonal matrix, without altering the original matrix.
        /// </summary>
        /// <param name="A">A diagonal matrix.</param>
        /// <returns>The matrix exponential.</returns>
        /// <cat>la</cat>
        public static DiagonalMatrix<double> Exp(this DiagonalMatrix<int> A) => A.Exp(1.0, x => Math.Exp(x));
        public static DiagonalMatrix<double> Exp(this DiagonalMatrix<long> A) => A.Exp(1.0, x => Math.Exp(x));
        public static DiagonalMatrix<float> Exp(this DiagonalMatrix<float> A) => A.Exp(1.0f, x => (float)Math.Exp(x));
        public static DiagonalMatrix<double> Exp(this DiagonalMatrix<double> A) => A.Exp(1.0, x => Math.Exp(x));
        public static DiagonalMatrix<decimal> Exp(this DiagonalMatrix<decimal> A) => A.Exp(1.0m, x => DecimalMath.Exp(x));
        public static DiagonalMatrix<Complex> Exp(this DiagonalMatrix<Complex> A) => A.Exp(Complex.One, x => Complex.Exp(x));
    }
}
