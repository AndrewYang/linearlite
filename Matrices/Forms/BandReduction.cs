﻿using LinearNet.Providers;
using LinearNet.Providers.LinearAlgebra;
using LinearNet.Structs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Matrices
{
    public static class BandReduction
    {
        /// <summary>
        /// <para>Factor an arbitary matrix $A\in\mathbb{F}^{m \times n}$ as $$A = U^*BV,$$ where:</para>
        /// <ul>
        /// <li>$U\in\mathbb{F}^{m \times m}$ is an orthogonal/unitary matrix,</li>
        /// <li>$B\in\mathbb{F}^{m \times n}$ is a band matrix with a upper bandwidth of <txt>upperBandwidth</txt> and a lower bandwidth of <txt>lowerBandwidth</txt>,</li>
        /// <li>$V\in\mathbb{F}^{n \times n}$ is an orthogonal/unitary matrix</li>
        /// </ul>
        /// <para>This implementation use Householder transformations.</para>
        /// <para>Also see: <a href="#ToTridiagonalForm"><txt>ToBidiagonalForm</txt></a>, <a href="#ToTridiagonalForm"><txt>ToTridiagonalForm</txt></a></para>
        /// <!--inputs-->
        /// </summary>
        /// <name>BandReduce</name>
        /// <proto>void BandReduce(this DenseMatrix<T> A, out DenseMatrix<T> U, out DenseMatrix<T> B, out DenseMatrix<T> V, int lowerBandwidth, int upperBandwidth, bool overwrite = false)</proto>
        /// <cat>la</cat>
        /// <param name="A">The $m \times n$ matrix to be reduced.</param>
        /// <param name="U">
        /// <b>Out parameter</b><br/>
        /// A $m \times m$ left-orthogonal/unitary matrix, $U$.
        /// </param>
        /// <param name="B">
        /// <b>Out parameter</b><br/>
        /// A $m \times n$ band matrix, $B$, with <txt>lowerBandwidth</txt> lower bands and <txt>upperBandwidth</txt> upper bands.
        /// </param>
        /// <param name="V">
        /// <b>Out parameter</b><br/>
        /// A $n \times n$ right-orthogonal/unitary matrix, $V$.
        /// </param>
        /// <param name="lowerBandwidth">The lower bandwidth of $B$.</param>
        /// <param name="upperBandwidth">The upper bandwidth of $B$.</param>
        /// <param name="overwrite">
        /// <b>Optional parameter</b>, defaults to <txt>false</txt>.<br/>
        /// If <txt>true</txt>, the matrix A will be overwritten with the produced band matrix (the algorithm is slightly more performant).
        /// </param>
        public static void BandReduce(this DenseMatrix<float> A, out DenseMatrix<float> U, out DenseMatrix<float> B, out DenseMatrix<float> V, int lowerBandwidth, int upperBandwidth, bool overwrite = false)
            => BandReduce(A, out U, out B, out V, lowerBandwidth, upperBandwidth, overwrite, BLAS.Float, x => x.TransposeInPlace());
        public static void BandReduce(this DenseMatrix<double> A, out DenseMatrix<double> U, out DenseMatrix<double> B, out DenseMatrix<double> V, int lowerBandwidth, int upperBandwidth, bool overwrite = false)
            => BandReduce(A, out U, out B, out V, lowerBandwidth, upperBandwidth, overwrite, BLAS.Double, x => x.TransposeInPlace());
        public static void BandReduce(this DenseMatrix<decimal> A, out DenseMatrix<decimal> U, out DenseMatrix<decimal> B, out DenseMatrix<decimal> V, int lowerBandwidth, int upperBandwidth, bool overwrite = false)
            => BandReduce(A, out U, out B, out V, lowerBandwidth, upperBandwidth, overwrite, BLAS.Decimal, x => x.TransposeInPlace());
        public static void BandReduce(this DenseMatrix<Complex> A, out DenseMatrix<Complex> U, out DenseMatrix<Complex> B, out DenseMatrix<Complex> V, int lowerBandwidth, int upperBandwidth, bool overwrite = false)
            => BandReduce(A, out U, out B, out V, lowerBandwidth, upperBandwidth, overwrite, BLAS.Complex, x => x.SelfConjugateTranspose());

        internal static void BandReduce<T>(this DenseMatrix<T> A, out DenseMatrix<T> U, out DenseMatrix<T> B, out DenseMatrix<T> V, 
            int lowerBandwidth, int upperBandwidth, bool overwriteA, IDenseHouseholder<T> blas, Action<T[][]> SelfTranspose) where T : new()
        {
            MatrixChecks.CheckNotNull(A);

            if (overwriteA)
            {
                B = A;
            }
            else
            {
                B = A.Copy();
            }

            BandReduce(B.Values, out T[][] _U, out T[][] _V, lowerBandwidth, upperBandwidth, blas, SelfTranspose);

            U = new DenseMatrix<T>(A.Rows, A.Rows, _U);
            V = new DenseMatrix<T>(A.Columns, A.Columns, _V);
        }
        internal static void BandReduce<T>(this T[][] A, out T[][] U, out T[][] V, int lowerBandwidth, int upperBandwidth, IDenseHouseholder<T> blas, Action<T[][]> SelfTranspose)
        {
            MatrixChecks.CheckNotNull(A);

            if (lowerBandwidth < 0 || upperBandwidth < 0 || lowerBandwidth + upperBandwidth < 1)
            {
                throw new ArgumentOutOfRangeException();
            }

            int m = A.Length, n = A[0].Length, rank = Math.Min(m, n), max = Math.Max(m, n), c;

            U = MatrixInternalExtensions.JIdentity(blas.Provider.Zero, blas.Provider.One, m);
            V = MatrixInternalExtensions.JIdentity(blas.Provider.Zero, blas.Provider.One, n);
            T[] v = new T[max], w = new T[max];

            for (c = 0; c < rank; ++c)
            {
                int row_begin = c + lowerBandwidth;
                if (row_begin < m)
                {
                    blas.HouseholderTransform(A, U, v, w, c, n, row_begin, m, true);
                }

                int col_begin = c + upperBandwidth;
                if (col_begin < n)
                {
                    blas.HouseholderTransform(A, V, v, w, c, n, col_begin, m, false);
                }
            }

            SelfTranspose(U);
        }
    }
}
