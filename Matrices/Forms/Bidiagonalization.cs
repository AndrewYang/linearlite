﻿using LinearNet.Providers;
using LinearNet.Helpers;
using LinearNet.Matrices.Forms;
using LinearNet.Structs;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LinearNet.Providers.LinearAlgebra;

namespace LinearNet.Matrices
{
    public static class Bidiagonalization
    {
        #region Public methods 

        /// <summary>
        /// Convert a matrix to its bidiagonal form.
        /// The method decomposes matrix $A\in\mathbb{F}^{m \times n}$ as
        /// $$A = U^*BV$$
        /// <ul>
        /// <li>$U\in\mathbb{F}^{m \times m}$, a orthogonal/unitary matrix</li>
        /// <li>
        /// $B\in\mathbb{F}^{m \times n}$, a bidiagonal matrix, with non zero items in the leading and one minor diagonal. 
        /// If <txt>upper</txt> is <txt>true</txt>, then the upper minor diagonal will be non-zero, otherwise the lower minor diagonal will be the non-zero minor diagonal.
        /// </li>
        /// <li>$V\in\mathbb{F}^{n \times n}$, a orthogonal/unitary matrix</li>
        /// </ul>
        /// 
        /// The method currently supports the following bidiagonalization algorithms: 
        /// <ul>
        /// <li>Golub-Kahan-Lanczos algorithm, </li>
        /// <li>Givens rotations, and</li>
        /// <li>Householder reflections</li>
        /// </ul>
        /// 
        /// <para>
        /// The householder transformation method is selected by default. For small matrices, 
        /// the Golub-Kahan-Lanczos bidiagonalization procedure may offer a slight performance advantage. 
        /// </para>
        /// <para>
        /// However, for moderately sized ($n > 50$) and larger matrices, GKL suffers from loss of 
        /// orthogonality in the $U$ and $V$ matrices due to numerical errors. Householder 
        /// transformations are more numerically stable in this domain. 
        /// </para>
        /// <para>Also see: <a href="#ToBidiagonalFormParallel"><txt>ToBidiagonalFormParallel</txt></a>, <a href="#BandReduce"><txt>BandReduce</txt></a></para>
        /// 
        /// <!--inputs-->
        /// 
        /// <para><b>Example</b></para>
        /// <pre><code class="cs">
        /// // Create a random 15 x 10 real matrix
        /// DenseMatrix&lt;Complex&gt; matrix = DenseMatrix.Random&lt;Complex&gt;(15, 10); 
        /// 
        /// // Perform the default upper bidiagonalization (using Householder transformations)
        /// matrix.ToBidiagonalForm(out DenseMatrix&lt;Complex&gt; U, out DenseMatrix&lt;Complex&gt; B, out DenseMatrix&lt;Complex&gt; V);
        /// 
        /// // Perform a lower bidiagonalization (using Golub-Kahan-Lanczos algorithm)
        /// matrix.ToBidiagonalForm(out DenseMatrix&lt;Complex&gt; U1, out DenseMatrix&lt;Complex&gt; B1, out DenseMatrix&lt;Complex&gt; V1, 
        /// false, BidiagonalizationMethod.GOLUB_KAHAN_LANCZOS);
        /// 
        /// Console.WriteLine(U.IsOrthogonal());    // true
        /// Console.WriteLine(B.IsBidiagonal());    // true
        /// Console.WriteLine(V.IsOrthogonal());    // true
        /// 
        /// </code></pre>
        /// </summary>
        /// <name>ToBidiagonalForm</name>
        /// <proto>void ToBidiagonalForm(this IMatrix<T> A, out IMatrix<T> U, out IMatrix<T> B, out IMatrix<T> V, bool upper, bool thin, bool preserveMatrix, BidiagonalizationMethod method)</proto>
        /// <cat>la</cat>
        /// <param name="A">The $m \times n$ matrix matrix to be decomposed.</param>
        /// <param name="U">
        /// <b>Out parameter</b><br/>
        /// The $m \times m$ left-orthogonal/unitary matrix $U$, with the property $UU^* = I$
        /// </param>
        /// <param name="B">
        /// <b>Out parameter</b><br/>
        /// The $m \times n$ bidiagonal matrix $B$. <br/>
        /// If <txt>upper</txt> is <txt>true</txt>, $B$ will be a upper-bidiagonal matrix, i.e. $B_{ii}$ and $B_{i,i+1}$ are the only non-zero entries.<br/>
        /// If <txt>upper</txt> is <txt>false</txt>, $B$ will be a lower-bidiagonal matrix, i.e. $B_{ii}$ and $B_{i,i-1}$ are the only non-zero entries.
        /// </param>
        /// <param name="V">
        /// <b>Out parameter</b><br/>
        /// The $n \times n$ right-orthogonal/unitary matrix $V$, with the property $VV^* = I$
        /// </param>
        /// <param name="upper">
        /// <b>Optional</b>, defaults to <txt>true</txt>.<br/>
        /// If <txt>true</txt>, the matrix will be decomposed into a upper bidiagonal matrix. If <txt>false</txt>, it will be 
        /// decomposed into a lower bidiagonal matrix (with non-zero entires in the lower minor diagonal and leading diagonal).
        /// </param>
        /// <param name="thin">
        /// <b>Optional</b>, defaults to <txt>true</txt>.<br/>
        /// If <txt>true</txt>, the returned orthogonal matrices <txt>U</txt> and <txt>V</txt> will be thin and the bidiagonal matrix <txt>B</txt> will be square.<br/>
        /// If <txt>false</txt>, <txt>U</txt> and <txt>V</txt> will be square and <txt>B</txt> will have the same dimensionality as <txt>A</txt>.
        /// </param>
        /// <param name="preserveMatrix">
        /// <b>Optional</b>, defaults to <txt>true</txt>.<br/>
        /// If <txt>true</txt>, the matrix <txt>A</txt> will be unchanged. The method is more efficient if set to <txt>false</txt>.
        /// </param>
        /// <param name="method">
        /// <b>Optional</b>, defaults to the Householder transformation method (<txt>HOUSEHOLDER_TRANSFORM</txt>).<br/>
        /// Enumerated type, representing the choice of bidiagonalisation algorithm.<br/>
        /// Choices: <txt>GOLUB_KAHAN_LANCZOS</txt>, <txt>GIVENS_ROTATIONS</txt>, <txt>HOUSEHOLDER_TRANSFORM</txt>
        /// </param>
        public static void ToBidiagonalForm(this float[,] A, out float[,] U, out float[,] B, out float[,] V, bool upper = true, bool thin = true, BidiagonalizationMethod method = BidiagonalizationMethod.HOUSEHOLDER_TRANSFORM)
        {
            if (!upper)
            {
                A.Transpose().ToBidiagonalForm(out V, out float[,] _B, out U, true, thin, method);
                B = _B.Transpose();
                return;
            }

            if (method == BidiagonalizationMethod.HOUSEHOLDER_TRANSFORM)
            {
                HouseholderBidiagonalization(A, out U, out B, out V, thin, BLAS.Float, x => x.TransposeInPlace(), false);
            }
            else if (method == BidiagonalizationMethod.GOLUB_KAHAN_LANCZOS)
            {
                GolubKahanLanczosAlgorithm.Decompose(A, out U, out B, out V);
            }
            else
            {
                throw new NotImplementedException();
            }
        }
        public static void ToBidiagonalForm(this double[,] A, out double[,] U, out double[,] B, out double[,] V, bool upper = true, bool thin = true, BidiagonalizationMethod method = BidiagonalizationMethod.HOUSEHOLDER_TRANSFORM)
        {
            if (!upper)
            {
                A.Transpose().ToBidiagonalForm(out V, out double[,] _B, out U, true, thin, method);
                B = _B.Transpose();
                return;
            }

            if (method == BidiagonalizationMethod.HOUSEHOLDER_TRANSFORM)
            {
                HouseholderBidiagonalization(A, out U, out B, out V, thin, BLAS.Double, x => x.TransposeInPlace(), false);
            }
            else if (method == BidiagonalizationMethod.GOLUB_KAHAN_LANCZOS)
            {
                GolubKahanLanczosAlgorithm.Decompose(A, out U, out B, out V);
            }
            else
            {
                throw new NotImplementedException();
            }
        }
        public static void ToBidiagonalForm(this decimal[,] A, out decimal[,] U, out decimal[,] B, out decimal[,] V, bool upper = true, bool thin = true, BidiagonalizationMethod method = BidiagonalizationMethod.HOUSEHOLDER_TRANSFORM)
        {
            if (!upper)
            {
                A.Transpose().ToBidiagonalForm(out V, out decimal[,] _B, out U, true, thin, method);
                B = _B.Transpose();
                return;
            }

            if (method == BidiagonalizationMethod.HOUSEHOLDER_TRANSFORM)
            {
                HouseholderBidiagonalization(A, out U, out B, out V, thin, BLAS.Decimal, x => x.TransposeInPlace(), false);
            }
            else if (method == BidiagonalizationMethod.GOLUB_KAHAN_LANCZOS)
            {
                GolubKahanLanczosAlgorithm.Decompose(A, out U, out B, out V);
            }
            else
            {
                throw new NotImplementedException();
            }
        }
        public static void ToBidiagonalForm(this Complex[,] A, out Complex[,] U, out Complex[,] B, out Complex[,] V, bool upper = true, bool thin = true, BidiagonalizationMethod method = BidiagonalizationMethod.HOUSEHOLDER_TRANSFORM)
        {
            if (!upper)
            {
                A.ConjugateTranspose().ToBidiagonalForm(out V, out Complex[,] _B, out U, true, thin, method);
                B = _B.ConjugateTranspose();
                return;
            }

            if (method == BidiagonalizationMethod.HOUSEHOLDER_TRANSFORM)
            {
                HouseholderBidiagonalization(A, out U, out B, out V, thin, BLAS.Complex, x => x.SelfConjugateTranspose(), false);
            }
            else if (method == BidiagonalizationMethod.GOLUB_KAHAN_LANCZOS)
            {
                GolubKahanLanczosAlgorithm.Decompose(A, out U, out B, out V);
            }
            else
            {
                throw new NotImplementedException();
            }
        }

        /// <summary>
        /// <para>Parallel bidiagonalization of a matrix, with same the prototype as the sequential implementation, <a href="#ToBidiagonalForm"><txt>ToBidiagonalForm</txt></a>.</para>
        /// <!--inputs-->
        /// </summary>
        /// <name>ToBidiagonalFormParallel</name>
        /// <proto>void ToBidiagonalFormParallel(this IMatrix<T> A, out IMatrix<T> U, out IMatrix<T> B, out IMatrix<T> V, bool upper, BidiagonalizationMethod method)</proto>
        /// <cat>la</cat>
        /// <param name="A">The $m \times n$ matrix matrix to be decomposed.</param>
        /// <param name="U">
        /// <b>Out parameter</b><br/>
        /// The $m \times m$ left-orthogonal/unitary matrix $U$, with the property $UU^* = I$
        /// </param>
        /// <param name="B">
        /// <b>Out parameter</b><br/>
        /// The $m \times n$ bidiagonal matrix $B$. <br/>
        /// If <txt>upper</txt> is <txt>true</txt>, $B$ will be a upper-bidiagonal matrix, i.e. $B_{ii}$ and $B_{i,i+1}$ are the only non-zero entries.<br/>
        /// If <txt>upper</txt> is <txt>false</txt>, $B$ will be a lower-bidiagonal matrix, i.e. $B_{ii}$ and $B_{i,i-1}$ are the only non-zero entries.
        /// </param>
        /// <param name="V">
        /// <b>Out parameter</b><br/>
        /// The $n \times n$ right-orthogonal/unitary matrix $V$, with the property $VV^* = I$
        /// </param>
        /// <param name="upper">
        /// <b>Optional</b>, defaults to <txt>true</txt>.<br/>
        /// If <txt>true</txt>, the matrix will be decomposed into a upper bidiagonal matrix. If <txt>false</txt>, it will be 
        /// decomposed into a lower bidiagonal matrix (with non-zero entires in the lower minor diagonal and leading diagonal).
        /// </param>
        /// <param name="method">
        /// <b>Optional</b>, defaults to the Householder transformation method (<txt>HOUSEHOLDER_TRANSFORM</txt>).<br/>
        /// Enumerated type, representing the choice of bidiagonalisation algorithm.
        /// </param>
        public static void ToBidiagonalFormParallel(this float[,] A, out float[,] U, out float[,] B, out float[,] V, bool upper = true, bool thin = true, BidiagonalizationMethod method = BidiagonalizationMethod.HOUSEHOLDER_TRANSFORM)
        {
            if (!upper)
            {
                A.ParallelTranspose().ToBidiagonalFormParallel(out V, out float[,] _B, out U, true, thin, method);
                B = _B.ParallelTranspose();
                return;
            }

            if (method == BidiagonalizationMethod.HOUSEHOLDER_TRANSFORM)
            {
                HouseholderBidiagonalization(A, out U, out B, out V, thin, BLAS.Float, x => x.TransposeInPlaceParallel(), true);
            }
            else if (method == BidiagonalizationMethod.GOLUB_KAHAN_LANCZOS)
            {
                GolubKahanLanczosAlgorithm.Decompose(A, out U, out B, out V);
            }
            else
            {
                throw new NotImplementedException();
            }
        }
        public static void ToBidiagonalFormParallel(this double[,] A, out double[,] U, out double[,] B, out double[,] V, bool upper = true, bool thin = true, BidiagonalizationMethod method = BidiagonalizationMethod.HOUSEHOLDER_TRANSFORM)
        {
            if (!upper)
            {
                A.ParallelTranspose().ToBidiagonalFormParallel(out V, out double[,] _B, out U, true, thin, method);
                B = _B.ParallelTranspose();
                return;
            }

            if (method == BidiagonalizationMethod.HOUSEHOLDER_TRANSFORM)
            {
                HouseholderBidiagonalization(A, out U, out B, out V, thin, BLAS.Double, x => x.TransposeInPlaceParallel(), true);
            }
            else if (method == BidiagonalizationMethod.GOLUB_KAHAN_LANCZOS)
            {
                GolubKahanLanczosAlgorithm.Decompose(A, out U, out B, out V);
            }
            else
            {
                throw new NotImplementedException();
            }
        }
        public static void ToBidiagonalFormParallel(this decimal[,] A, out decimal[,] U, out decimal[,] B, out decimal[,] V, bool upper = true, bool thin = true, BidiagonalizationMethod method = BidiagonalizationMethod.HOUSEHOLDER_TRANSFORM)
        {
            if (!upper)
            {
                A.ParallelTranspose().ToBidiagonalFormParallel(out V, out decimal[,] _B, out U, true, thin, method);
                B = _B.ParallelTranspose();
                return;
            }

            if (method == BidiagonalizationMethod.HOUSEHOLDER_TRANSFORM)
            {
                HouseholderBidiagonalization(A, out U, out B, out V, thin, BLAS.Decimal, x => x.TransposeInPlaceParallel(), true);
            }
            else if (method == BidiagonalizationMethod.GOLUB_KAHAN_LANCZOS)
            {
                GolubKahanLanczosAlgorithm.Decompose(A, out U, out B, out V);
            }
            else
            {
                throw new NotImplementedException();
            }
        }
        public static void ToBidiagonalFormParallel(this Complex[,] A, out Complex[,] U, out Complex[,] B, out Complex[,] V, bool upper = true, bool thin = true, BidiagonalizationMethod method = BidiagonalizationMethod.HOUSEHOLDER_TRANSFORM)
        {
            if (!upper)
            {
                A.ParallelTranspose().ToBidiagonalFormParallel(out V, out Complex[,] _B, out U, true, thin, method);
                B = _B.ParallelTranspose();
                return;
            }

            if (method == BidiagonalizationMethod.HOUSEHOLDER_TRANSFORM)
            {
                HouseholderBidiagonalization(A, out U, out B, out V, thin, BLAS.Complex, x => x.SelfConjugateTransposeParallel(), true);
            }
            else if (method == BidiagonalizationMethod.GOLUB_KAHAN_LANCZOS)
            {
                GolubKahanLanczosAlgorithm.Decompose(A, out U, out B, out V);
            }
            else
            {
                throw new NotImplementedException();
            }
        }

        public static void ToBidiagonalForm(this DenseMatrix<float> A, out DenseMatrix<float> U, out DenseMatrix<float> B, out DenseMatrix<float> V,
            bool upper = true, bool thin = true, bool preserveMatrix = true, BidiagonalizationMethod method = BidiagonalizationMethod.HOUSEHOLDER_TRANSFORM)
        {
            if (!upper)
            {
                A.Transpose().ToBidiagonalForm(out V, out DenseMatrix<float> _B, out U, true, thin, preserveMatrix, method);
                B = _B.Transpose();
                return;
            }

            if (method == BidiagonalizationMethod.HOUSEHOLDER_TRANSFORM)
            {
                HouseholderBidiagonalization(A, out U, out B, out V, thin, preserveMatrix, BLAS.Float, x => x.TransposeInPlace(), false);
            }
            else if (method == BidiagonalizationMethod.GIVENS_ROTATIONS)
            {
                GivensRotationBidiagonalization(A, out U, out B, out V, preserveMatrix, BLAS.Float, Util.Givens, x => x.TransposeInPlace());
            }
            else if (method == BidiagonalizationMethod.GOLUB_KAHAN_LANCZOS)
            {
                GolubKahanLanczosAlgorithm.Decompose(A, out U, out B, out V);
            }
            else
            {
                throw new NotImplementedException();
            }
        }
        public static void ToBidiagonalForm(this DenseMatrix<double> A, out DenseMatrix<double> U, out DenseMatrix<double> B, out DenseMatrix<double> V, 
            bool upper = true, bool thin = true, bool preserveMatrix = true, BidiagonalizationMethod method = BidiagonalizationMethod.HOUSEHOLDER_TRANSFORM)
        {
            if (!upper)
            {
                A.Transpose().ToBidiagonalForm(out V, out DenseMatrix<double> _B, out U, true, thin, preserveMatrix, method);
                B = _B.Transpose();
                return;
            }

            if (method == BidiagonalizationMethod.HOUSEHOLDER_TRANSFORM)
            {
                HouseholderBidiagonalization(A, out U, out B, out V, thin, preserveMatrix, BLAS.Double, x => x.TransposeInPlace(), false);
            }
            else if (method == BidiagonalizationMethod.GIVENS_ROTATIONS)
            {
                GivensRotationBidiagonalization(A, out U, out B, out V, preserveMatrix, BLAS.Double, Util.Givens, x => x.TransposeInPlace());
            }
            else if (method == BidiagonalizationMethod.GOLUB_KAHAN_LANCZOS)
            {
                GolubKahanLanczosAlgorithm.Decompose(A, out U, out B, out V);
            }
            else
            {
                throw new NotImplementedException();
            }
        }
        public static void ToBidiagonalForm(this DenseMatrix<decimal> A, out DenseMatrix<decimal> U, out DenseMatrix<decimal> B, out DenseMatrix<decimal> V,
            bool upper = true, bool thin = true, bool preserveMatrix = true, BidiagonalizationMethod method = BidiagonalizationMethod.HOUSEHOLDER_TRANSFORM)
        {
            if (!upper)
            {
                A.Transpose().ToBidiagonalForm(out V, out DenseMatrix<decimal> _B, out U, true, thin, preserveMatrix, method);
                B = _B.Transpose();
                return;
            }

            if (method == BidiagonalizationMethod.HOUSEHOLDER_TRANSFORM)
            {
                HouseholderBidiagonalization(A, out U, out B, out V, thin, preserveMatrix, BLAS.Decimal, x => x.TransposeInPlace(), false);
            }
            else if (method == BidiagonalizationMethod.GIVENS_ROTATIONS)
            {
                GivensRotationBidiagonalization(A, out U, out B, out V, preserveMatrix, BLAS.Decimal, Util.Givens, x => x.TransposeInPlace());
            }
            else if (method == BidiagonalizationMethod.GOLUB_KAHAN_LANCZOS)
            {
                GolubKahanLanczosAlgorithm.Decompose(A, out U, out B, out V);
            }
            else
            {
                throw new NotImplementedException();
            }
        }
        public static void ToBidiagonalForm(this DenseMatrix<Complex> A, out DenseMatrix<Complex> U, out DenseMatrix<Complex> B, out DenseMatrix<Complex> V,
            bool upper = true, bool thin = true, bool preserveMatrix = true, BidiagonalizationMethod method = BidiagonalizationMethod.HOUSEHOLDER_TRANSFORM)
        {
            if (!upper)
            {
                A.ConjugateTranspose().ToBidiagonalForm(out V, out DenseMatrix<Complex> _B, out U, true, thin, preserveMatrix, method);
                B = _B.ConjugateTranspose();
                return;
            }

            if (method == BidiagonalizationMethod.HOUSEHOLDER_TRANSFORM)
            {
                HouseholderBidiagonalization(A, out U, out B, out V, thin, preserveMatrix, BLAS.Complex, x => x.SelfConjugateTranspose(), false);
            }
            else if (method == BidiagonalizationMethod.GIVENS_ROTATIONS)
            {
                MatrixChecks.CheckNotNull(A);

                Complex[][] _B = preserveMatrix ? A.Copy().Values : A.Values;
                GivensRotationBidiagonalization(_B, out Complex[][] _U, out Complex[][] _V, BLAS.Complex);

                U = new DenseMatrix<Complex>(_U);
                B = new DenseMatrix<Complex>(_B);
                V = new DenseMatrix<Complex>(_V);
            }
            else if (method == BidiagonalizationMethod.GOLUB_KAHAN_LANCZOS)
            {
                GolubKahanLanczosAlgorithm.Decompose(A, out U, out B, out V);
            }
            else
            {
                throw new NotImplementedException();
            }
        }

        public static void ToBidiagonalFormParallel(this DenseMatrix<float> A, out DenseMatrix<float> U, out DenseMatrix<float> B, out DenseMatrix<float> V,
            bool upper = true, bool thin = true, bool preserveMatrix = true, BidiagonalizationMethod method = BidiagonalizationMethod.HOUSEHOLDER_TRANSFORM)
        {
            if (!upper)
            {
                A.TransposeParallel().ToBidiagonalFormParallel(out V, out DenseMatrix<float> _B, out U, true, thin, preserveMatrix, method);
                B = _B.TransposeParallel();
                return;
            }

            if (method == BidiagonalizationMethod.HOUSEHOLDER_TRANSFORM)
            {
                HouseholderBidiagonalization(A, out U, out B, out V, thin, preserveMatrix, BLAS.Float, x => x.TransposeInPlaceParallel(), true);
            }
            else if (method == BidiagonalizationMethod.GOLUB_KAHAN_LANCZOS)
            {
                GolubKahanLanczosAlgorithm.Decompose(A, out U, out B, out V);
            }
            else
            {
                throw new NotImplementedException();
            }
        }
        public static void ToBidiagonalFormParallel(this DenseMatrix<double> A, out DenseMatrix<double> U, out DenseMatrix<double> B, out DenseMatrix<double> V, 
            bool upper = true, bool thin = true, bool preserveMatrix = true, BidiagonalizationMethod method = BidiagonalizationMethod.HOUSEHOLDER_TRANSFORM)
        {
            if (!upper)
            {
                A.TransposeParallel().ToBidiagonalFormParallel(out V, out DenseMatrix<double> _B, out U, true, thin, preserveMatrix, method);
                B = _B.TransposeParallel();
                return;
            }
            
            if (method == BidiagonalizationMethod.HOUSEHOLDER_TRANSFORM)
            {
                HouseholderBidiagonalization(A, out U, out B, out V, thin, preserveMatrix, BLAS.Double, x => x.TransposeInPlaceParallel(), true);
            }
            else if (method == BidiagonalizationMethod.GOLUB_KAHAN_LANCZOS)
            {
                GolubKahanLanczosAlgorithm.Decompose(A, out U, out B, out V);
            }
            else
            {
                throw new NotImplementedException();
            }
        }
        public static void ToBidiagonalFormParallel(this DenseMatrix<decimal> A, out DenseMatrix<decimal> U, out DenseMatrix<decimal> B, out DenseMatrix<decimal> V,
            bool upper = true, bool thin = true, bool preserveMatrix = true, BidiagonalizationMethod method = BidiagonalizationMethod.HOUSEHOLDER_TRANSFORM)
        {
            if (!upper)
            {
                A.TransposeParallel().ToBidiagonalFormParallel(out V, out DenseMatrix<decimal> _B, out U, true, thin, preserveMatrix, method);
                B = _B.TransposeParallel();
                return;
            }

            if (method == BidiagonalizationMethod.HOUSEHOLDER_TRANSFORM)
            {
                HouseholderBidiagonalization(A, out U, out B, out V, thin, preserveMatrix, BLAS.Decimal, x => x.TransposeInPlaceParallel(), true);
            }
            else if (method == BidiagonalizationMethod.GOLUB_KAHAN_LANCZOS)
            {
                GolubKahanLanczosAlgorithm.Decompose(A, out U, out B, out V);
            }
            else
            {
                throw new NotImplementedException();
            }
        }
        public static void ToBidiagonalFormParallel(this DenseMatrix<Complex> A, out DenseMatrix<Complex> U, out DenseMatrix<Complex> B, out DenseMatrix<Complex> V,
            bool upper = true, bool thin = true, bool preserveMatrix = true, BidiagonalizationMethod method = BidiagonalizationMethod.HOUSEHOLDER_TRANSFORM)
        {
            if (!upper)
            {
                A.ConjugateTransposeParallel().ToBidiagonalFormParallel(out V, out DenseMatrix<Complex> _B, out U, true, thin, preserveMatrix, method);
                B = _B.ConjugateTransposeParallel();
                return;
            }

            if (method == BidiagonalizationMethod.HOUSEHOLDER_TRANSFORM)
            {
                HouseholderBidiagonalization(A, out U, out B, out V, thin, preserveMatrix, BLAS.Complex, x => x.SelfConjugateTransposeParallel(), true);
            }
            else if (method == BidiagonalizationMethod.GOLUB_KAHAN_LANCZOS)
            {
                GolubKahanLanczosAlgorithm.Decompose(A, out U, out B, out V);
            }
            else
            {
                throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Convert a matrix $A$ to its upper or lower bidiagonal form, without calculating the left and right orthogonal (unitary) matrices. 
        /// <para>Also see: <a href="ToBidiagonalForm"><txt>ToBidiagonalForm</txt></a>.</para>
        /// </summary>
        /// <name>Bidiagonalize</name>
        /// <proto>void Bidiagonalize(this IMatrix<T> A)</proto>
        /// <cat>la</cat>
        /// <param name="A"></param>
        public static void Bidiagonalize(this DenseMatrix<float> A, bool upper = true) => Bidiagonalize(A.Values, upper);
        public static void Bidiagonalize(this DenseMatrix<double> A, bool upper = true) => Bidiagonalize(A.Values, upper);
        public static void Bidiagonalize(this DenseMatrix<decimal> A, bool upper = true) => Bidiagonalize(A.Values, upper);
        public static void Bidiagonalize(this DenseMatrix<Complex> A, bool upper = true) => Bidiagonalize(A.Values, upper);

        #endregion

        internal static void Bidiagonalize<T>(this T[][] A, Action<T[][], T[], T[], int, int, int, int, bool> HouseholderTransform, bool upper)
        {
            if (!upper)
            {
                T[][] At = A.Transpose();
                At.Bidiagonalize(HouseholderTransform, true);
                A.CopyTransposeFrom(A, 0, 0, A[0].Length, A.Length);
                return;
            }

            // Upper bidiagonalization
            int m = A.Length, n = A[0].Length, rank = Math.Min(m, n), max = Math.Max(m, n), c;
            T[] v = new T[max], w = new T[max];
            for (c = 0; c < rank; ++c)
            {
                HouseholderTransform(A, v, w, c, n, c, m, true);
                if (c < n - 1)
                {
                    HouseholderTransform(A, v, w, c, n, c + 1, m, false);
                }
            }
        }
        internal static void Bidiagonalize(this float[][] A, bool upper = true) => Bidiagonalize(A, Householder.Transform, upper);
        internal static void Bidiagonalize(this double[][] A, bool upper = true) => Bidiagonalize(A, Householder.Transform, upper);
        internal static void Bidiagonalize(this decimal[][] A, bool upper = true) => Bidiagonalize(A, Householder.Transform, upper);
        internal static void Bidiagonalize(this Complex[][] A, bool upper = true) => Bidiagonalize(A, Householder.Transform, upper);

        /// <summary>
        /// Reduce matrix A into bidiagonal form, also computing the orthogonal (unitary) matrices U and V.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="A"></param>
        /// <param name="U"></param>
        /// <param name="V"></param>
        /// <param name="blas"></param>
        /// <param name="ConjugateTranspose"></param>
        internal static void HouseholderBidiagonalization<T>(this T[][] A, out T[][] U, out T[][] V, T zero, T one, Action<T[][], T[][], T[], T[], int, int, int, int, bool> HouseholderTransform, bool thin, Action<T[][]> ConjugateTranspose = null)
        {
            if (A == null)
            {
                throw new ArgumentNullException();
            }

            // The householder transformation alternate between zero-ing a column and a row. 
            // This is the only way to guarantee that only 2 diagonal columns remain. 
            // otherwise, the effect of a Householder transformation applied to a row vector will undo
            // the previous Householder transformation applied to a column vector. 

            int m = A.Length, n = A[0].Length, rank = Math.Min(m, n), max = Math.Max(m, n), c;

            // Iterate through the columns, zero-ing each one from left to right, recording the householder 
            // transformation as we go
            U = MatrixInternalExtensions.JIdentity(zero, one, m);
            V = MatrixInternalExtensions.JIdentity(zero, one, n);

            T[] v = new T[max], w = new T[max];
            for (c = 0; c < rank; ++c)
            {
                HouseholderTransform(A, U, v, w, c, n, c, m, true);
                if (c < n - 1)
                {
                    HouseholderTransform(A, V, v, w, c, n, c + 1, m, false);
                }
            }
            
            /*
            if (typeof(T) == typeof(double))
            {
                DenseMatrix<double> _U = (double[][])(dynamic)U;
                DenseMatrix<double> _A = (double[][])(dynamic)A;
                DenseMatrix<double> _V = (double[][])(dynamic)V;
                Debug.WriteLine("Before U");
                _U.Transpose().Print();
                Debug.WriteLine("Before A");
                _A.Print();
                Debug.WriteLine("Before V");
                _V.Print();
                Debug.WriteLine("Before AV");
                _A.Multiply(_V).Print();
            }

            if (thin && n > m)
            {
                HouseholderTransform(A, V, v, w, m - 1, n, m - 1, m, false);

                if (typeof(T) == typeof(double))
                {
                    DenseMatrix<double> _U = (double[][])(dynamic)U;
                    DenseMatrix<double> _A = (double[][])(dynamic)A;
                    DenseMatrix<double> _V = (double[][])(dynamic)V;
                    Debug.WriteLine("After U");
                    _U.Transpose().Print();
                    Debug.WriteLine("After A");
                    _A.Print();
                    Debug.WriteLine("After V");
                    _V.Print();
                    Debug.WriteLine("After AV");
                    _A.Multiply(_V).Print();
                }
            }
            */

            ConjugateTranspose?.Invoke(U);
        }
        private static void HouseholderBidiagonalization<T>(this T[,] A, out T[,] U, out T[,] B, out T[,] V, bool thin, IDenseHouseholder<T> blas, Action<T[][]> ConjugateTranspose, bool parallel)
        {
            if (A == null)
            {
                throw new ArgumentNullException();
            }

            T[][] _B = parallel ? A.ToJaggedParallel() : A.ToJagged(), _U, _V;
            T zero = blas.Provider.Zero, one = blas.Provider.One;

            if (parallel)
            {
                HouseholderBidiagonalization(_B, out _U, out _V, zero, one, blas.HouseholderTransformParallel, thin, ConjugateTranspose);
            }
            else
            {
                HouseholderBidiagonalization(_B, out _U, out _V, zero, one, blas.HouseholderTransform, thin, ConjugateTranspose);
            }

            if (parallel)
            {
                U = _U.ToRectangularParallel();
                B = _B.ToRectangularParallel();
                V = _V.ToRectangularParallel();
            }
            else
            {
                U = _U.ToRectangular();
                B = _B.ToRectangular();
                V = _V.ToRectangular();
            }
        }
        private static void HouseholderBidiagonalization<T>(this DenseMatrix<T> A, out DenseMatrix<T> U, out DenseMatrix<T> B, out DenseMatrix<T> V, bool thin, bool preserveMatrix, IDenseHouseholder<T> blas, Action<T[][]> ConjugateTranspose, bool parallel) where T : new()
        {
            MatrixChecks.CheckNotNull(A);

            T zero = blas.Provider.Zero, one = blas.Provider.One;
            T[][] _B, _U, _V;
            if (parallel)
            {
                _B = preserveMatrix ? A.CopyParallel().Values : A.Values;
                HouseholderBidiagonalization(_B, out _U, out _V, zero, one, blas.HouseholderTransformParallel, thin, ConjugateTranspose);
            }
            else
            {
                _B = preserveMatrix ? A.Copy().Values : A.Values;
                HouseholderBidiagonalization(_B, out _U, out _V, zero, one, blas.HouseholderTransform, thin, ConjugateTranspose);
            }

            U = new DenseMatrix<T>(_U);
            B = new DenseMatrix<T>(_B);
            V = new DenseMatrix<T>(_V);
        }
        
        private static void GivensRotationBidiagonalization<T>(this T[][] A, out T[][] U, out T[][] V, IDenseBLAS1<T> blas, Action<T, T, T[]> Givens, Action<T[][]> ConjugateTranspose)
        {
            MatrixChecks.CheckNotNull(A);

            int m = A.Length, n = A[0].Length, min = Math.Min(m, n), i, j;

            T zero = blas.Provider.Zero, one = blas.Provider.One;
            U = MatrixInternalExtensions.JIdentity(zero, one, m);
            V = MatrixInternalExtensions.JIdentity(zero, one, n);

            T[] cs = new T[2];
            for (j = 0; j < min; ++j)
            {
                T[] A_j = A[j];

                // zero column j under A(j, j)
                for (i = j + 1; i < m; ++i)
                {
                    Givens(A_j[j], A[i][j], cs);
                    T c = cs[0], s = cs[1];
                    blas.ROTR(A, j, i, c, s, j, n);
                    blas.ROTC(U, j, i, c, s, 0, m);
                }

                // This borrows from QL decomposition
                // the lower left entry by default
                // zero row j to the right of A(j, j + 1)
                int end = Math.Min(n, j + 2), j_plus_1 = j + 1;
                for (i = n - 1; i >= end; --i)
                {
                    Givens(A_j[i], A_j[j_plus_1], cs);
                    T s = cs[0], c = cs[1];
                    blas.ROTC(A, j_plus_1, i, c, s, 0, m);
                    blas.ROTR(V, j_plus_1, i, c, s, 0, n);
                }
            }

            // clean
            for (i = 0; i < m; ++i)
            {
                int end = Math.Min(i, n);
                T[] A_i = A[i];
                for (j = 0; j < end; ++j)
                {
                    A_i[j] = zero;
                }
                for (j = i + 2; j < n; ++j)
                {
                    A_i[j] = zero;
                }
            }

            ConjugateTranspose(U);
        }

        private static void GivensRotationBidiagonalization(this Complex[][] A, out Complex[][] U, out Complex[][] V, IDenseBLAS1<Complex> blas)
        {
            MatrixChecks.CheckNotNull(A);

            // TODO: Some problems with the orthogonalization of V
            // Fix it before deployment
            throw new NotImplementedException();

            int m = A.Length, n = A[0].Length, min = Math.Min(m, n), i, j;

            Complex zero = blas.Provider.Zero, one = blas.Provider.One;
            U = MatrixInternalExtensions.JIdentity(zero, one, m);
            V = MatrixInternalExtensions.JIdentity(zero, one, n);

            Complex[] cs = new Complex[2];
            for (j = 0; j < min; ++j)
            {
                Complex[] A_j = A[j];

                // zero column j under A(j, j)
                for (i = j + 1; i < m; ++i)
                {
                    Util.Givens(A_j[j], A[i][j], cs);
                    Complex c = cs[0], s = cs[1];
                    blas.ROTR(A, j, i, c, s, j, n);
                    blas.ROTC(U, j, i, c, s, 0, m);
                }

                // This borrows from QL decomposition
                // the lower left entry by default
                // zero row j to the right of A(j, j + 1)
                int end = Math.Min(n, j + 2), j_plus_1 = j + 1;
                for (i = n - 1; i >= end; --i)
                {
                    Util.Givens(A_j[i], A_j[j_plus_1], cs);
                    Complex c = cs[1], s = cs[0];
                    blas.ROTC(A, j_plus_1, i, c, s , 0, m);
                    blas.ROTR(V, j_plus_1, i, c, s, 0, n);
                }
            }

            // clean
            for (i = 0; i < m; ++i)
            {
                int end = Math.Min(i, n);
                Complex[] A_i = A[i];
                for (j = 0; j < end; ++j)
                {
                    A_i[j] = zero;
                }
                for (j = i + 2; j < n; ++j)
                {
                    A_i[j] = zero;
                }
            }

            U.SelfConjugateTranspose();
        }
        private static void GivensRotationBidiagonalization<T>(this DenseMatrix<T> A, out DenseMatrix<T> U, out DenseMatrix<T> B, out DenseMatrix<T> V, 
            bool preserveMatrix, IDenseBLAS1<T> blas, Action<T, T, T[]> Givens, Action<T[][]> ConjugateTranspose) where T : new()
        {
            MatrixChecks.CheckNotNull(A);

            T[][] _B = preserveMatrix ? A.Copy().Values : A.Values;
            GivensRotationBidiagonalization(_B, out T[][] _U, out T[][] _V, blas, Givens, ConjugateTranspose);

            U = new DenseMatrix<T>(_U);
            B = new DenseMatrix<T>(_B);
            V = new DenseMatrix<T>(_V);
        }
    }
    public enum BidiagonalizationMethod
    {
        GOLUB_KAHAN_LANCZOS,
        GIVENS_ROTATIONS,
        HOUSEHOLDER_TRANSFORM
    }
}
