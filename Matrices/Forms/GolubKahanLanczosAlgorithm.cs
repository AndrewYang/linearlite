﻿using LinearNet.Providers;
using LinearNet.Helpers;
using LinearNet.Structs;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LinearNet.Providers.LinearAlgebra;

namespace LinearNet.Matrices.Forms
{
    internal static class GolubKahanLanczosAlgorithm
    {
        #region Golub-Jahan-Lanczos algorithm for T[,]

        private static void Decompose<T>(T[,] A, out T[,] U, out T[,] B, out T[,] V, T zero, Func<int, T> Sqrt1OverN,
            Func<T[], T> Norm2, Func<T, T, T> Add, Func<T, T, T> Subtract, Func<T, T, T> Multiply, Func<T, T, T> Divide)
        {
            if (A == null) throw new ArgumentNullException();

            int m = A.GetLength(0), n = A.GetLength(1), i, j, k, rank = Math.Min(m, n);
            T[][] _U = new T[m][], _V = new T[n][];
            B = new T[m, n];

            // set v1 to unit 2-norm vector
            T v_value = Sqrt1OverN(n);
            T[] v_0 = new T[n];
            for (i = 0; i < n; ++i) v_0[i] = v_value;
            _V[0] = v_0;

            for (k = 0; k < rank; ++k)
            {
                // Calculate u_k := A * v_k - beta_(k - 1) * u_(k - 1)
                T[] v_k = _V[k];
                T[] u = new T[m];
                _U[k] = u;
                for (i = 0; i < m; ++i)
                {
                    T sum = zero;
                    for (j = 0; j < n; ++j)
                    {
                        sum = Add(sum, Multiply(A[i, j], v_k[j]));
                    }
                    u[i] = sum;
                }

                if (k > 0)
                {
                    T[] u_prev = _U[k - 1];
                    T beta = B[k - 1, k];
                    for (i = 0; i < m; ++i)
                    {
                        u[i] = Subtract(u[i], Multiply(beta, u_prev[i]));
                    }
                }

                // Calculate alpha, and set as diagonal term in bidiagonal matrix
                T alpha = Norm2(u);
                B[k, k] = alpha;

                // u_k := u_k / alpha
                for (i = 0; i < m; ++i)
                {
                    u[i] = Divide(u[i], alpha);
                }

                // Calculate v_(k + 1) := A^T * u_k - A[k, k] * v_k,  if k < rank - 1
                // For complex numbers A^T is replaced with A*, the conjugate transpose
                if (k < rank - 1)
                {
                    T[] vNext = new T[n];
                    for (j = 0; j < n; ++j)
                    {
                        T sum = zero;
                        for (i = 0; i < m; ++i)
                        {
                            sum = Add(sum, Multiply(A[i, j], u[i]));
                        }
                        vNext[j] = Subtract(sum, Multiply(alpha, v_k[j]));
                    }
                    _V[k + 1] = vNext;

                    // Calculate beta, the off diagonal terms
                    T beta = Norm2(vNext);
                    for (j = 0; j < n; ++j)
                    {
                        vNext[j] = Divide(vNext[j], beta);
                    }
                    B[k, k + 1] = beta;
                }
            }

            // Wrap into rectangular array
            U = new T[m, m];
            V = new T[n, n];
            for (j = 0; j < rank; ++j)
            {
                T[] _v = _V[j];
                for (i = 0; i < n; ++i) V[j, i] = _v[i];

                T[] _u = _U[j];
                for (i = 0; i < m; ++i) U[j, i] = _u[i];
            }
        }
        /// <summary>
        /// Convert a real matrix into bidiagonal form using the Golub-Kahan-Lanczos Bidiagonalization Procedure.
        /// The method creates 3 matrices from a matrix A (m x n)
        /// 1. U (m x m), an orthogonal matrix
        /// 2. B (m x n), a upper bidiagonal matrix, with non zero items in A(i, i) and A(i, i + 1) only
        /// 3. V (n x n), an orthogonal matrix
        /// such that A = U^TBV
        /// 
        /// Unfortunately, this method is prone to numerical error blowup - unusable for most matrices with dimensions 
        /// > ~ 50 x 50.
        /// </summary>
        /// <param name="A">The matrix to be decomposed into bidiagonal form</param>
        internal static void Decompose(double[,] A, out double[,] U, out double[,] B, out double[,] V)
        {
            if (A == null) throw new ArgumentNullException();

            int m = A.GetLength(0), n = A.GetLength(1), i, j, k, rank = Math.Min(m, n);
            double[][] _U = new double[m][], _V = new double[n][];
            B = new double[m, n];

            // set v1 to unit 2-norm vector
            double v_value = Math.Sqrt(1.0 / n);
            double[] v_0 = new double[n];
            for (i = 0; i < n; ++i) v_0[i] = v_value;
            _V[0] = v_0;

            for (k = 0; k < rank; ++k)
            {
                // Calculate u_k := A * v_k - beta_(k - 1) * u_(k - 1)
                double[] v_k = _V[k];
                double[] u = new double[m];
                _U[k] = u;
                for (i = 0; i < m; ++i)
                {
                    double sum = 0.0;
                    for (j = 0; j < n; ++j)
                    {
                        sum += A[i, j] * v_k[j];
                    }
                    u[i] = sum;
                }

                if (k > 0)
                {
                    double[] u_prev = _U[k - 1];
                    double beta = B[k - 1, k];
                    for (i = 0; i < m; ++i)
                    {
                        u[i] -= beta * u_prev[i];
                    }
                }

                // Calculate alpha, and set as diagonal term in bidiagonal matrix
                double alpha = u.Norm(2);
                B[k, k] = alpha;

                // u_k := u_k / alpha
                for (i = 0; i < m; ++i)
                {
                    u[i] /= alpha;
                }

                // Calculate v_(k + 1) := A^T * u_k - A[k, k] * v_k,  if k < rank - 1
                // For complex numbers A^T is replaced with A*, the conjugate transpose
                if (k < rank - 1)
                {
                    double[] vNext = new double[n];
                    for (j = 0; j < n; ++j)
                    {
                        double sum = 0.0;
                        for (i = 0; i < m; ++i)
                        {
                            sum += A[i, j] * u[i];
                        }
                        vNext[j] = sum - alpha * v_k[j];
                    }
                    _V[k + 1] = vNext;

                    // Calculate beta, the off diagonal terms
                    double beta = vNext.Norm(2);
                    for (j = 0; j < n; ++j)
                    {
                        vNext[j] /= beta;
                    }
                    B[k, k + 1] = beta;
                }
            }

            // Wrap into rectangular array
            U = new double[m, m];
            V = new double[n, n];
            for (j = 0; j < rank; ++j)
            {
                double[] _v = _V[j];
                for (i = 0; i < n; ++i) V[j, i] = _v[i];

                double[] _u = _U[j];
                for (i = 0; i < m; ++i) U[j, i] = _u[i];
            }
        }
        internal static void Decompose(float[,] A, out float[,] U, out float[,] B, out float[,] V)
        {
            Decompose(A, out U, out B, out V,
                0.0f, n => (float)Math.Sqrt(1.0f / n), x => x.Norm(2),
                (x, y) => x + y, (x, y) => x - y, (x, y) => x * y, (x, y) => x / y);
        }
        internal static void Decompose(decimal[,] A, out decimal[,] U, out decimal[,] B, out decimal[,] V)
        {
            Decompose(A, out U, out B, out V,
                0.0m, n => DecimalMath.Sqrt(1.0m / n), x => x.Norm(2),
                (x, y) => x + y, (x, y) => x - y, (x, y) => x * y, (x, y) => x / y);
        }
        /// <summary>
        /// Convert a complex matrix into bidiagonal form using the Golub-Kahan-Lanczos Bidiagonalization Procedure.
        /// The method creates 3 matrices from a matrix A (m x n)
        /// 1. U (m x m), an unitary matrix
        /// 2. B (m x n), a upper bidiagonal matrix, with non zero items in A(i, i) and A(i, i + 1) only
        /// 3. V (n x n), an unitary matrix
        /// such that A = U*BV where U* is the conjugate tranpose of U
        /// </summary>
        /// <param name="A">The matrix to be decomposed into bidiagonal form</param>
        internal static void Decompose(Complex[,] A, out Complex[,] U, out Complex[,] B, out Complex[,] V)
        {
            if (A == null) throw new ArgumentNullException();

            int m = A.GetLength(0), n = A.GetLength(1), i, j, k, rank = Math.Min(m, n);

            Complex zero = Complex.Zero, sum;
            Complex[] v, v_next, u, u_prev;
            Complex[][] _U = new Complex[m][],
                        _V = new Complex[n][];
            B = new Complex[m, n];

            // set v1 to unit 2-norm vector
            double val = Math.Sqrt(1.0 / n);
            Complex[] v_0 = new Complex[n];
            for (i = 0; i < n; ++i) v_0[i] = val;
            _V[0] = v_0;

            for (k = 0; k < rank; ++k)
            {
                // Calculate u_k := A * v_k - beta_(k - 1) * u_(k - 1)
                v = _V[k];
                u = new Complex[m];
                _U[k] = u;
                for (i = 0; i < m; ++i)
                {
                    sum = zero;
                    for (j = 0; j < n; ++j)
                    {
                        sum.IncrementBy(A[i, j].Multiply(v[j]));
                    }
                    u[i] = sum;
                }

                if (k > 0)
                {
                    u_prev = _U[k - 1];
                    Complex beta = B[k - 1, k];
                    for (i = 0; i < m; ++i)
                    {
                        u[i].DecrementBy(beta.Multiply(u_prev[i]));
                    }
                }

                // Calculate alpha, and set as diagonal term in bidiagonal matrix
                double alpha = u.Norm();
                B[k, k] = alpha;

                // u_k := u_k / alpha
                double factor = 1 / alpha;
                for (i = 0; i < m; ++i)
                {
                    u[i].MultiplyEquals(factor);
                }

                // Calculate v_(k + 1) := A* * u_k - A[k, k] * v_k,  if k < rank - 1
                // For complex numbers A^T is replaced with A*, the conjugate transpose
                if (k < rank - 1)
                {
                    v_next = new Complex[n];
                    for (j = 0; j < n; ++j)
                    {
                        sum = zero;
                        for (i = 0; i < m; ++i)
                        {
                            sum.IncrementBy(A[i, j].Conjugate().Multiply(u[i]));
                        }
                        v_next[j] = sum.Subtract(alpha * v[j]);
                    }
                    _V[k + 1] = v_next;

                    // Calculate beta, the off diagonal terms
                    double beta = v_next.Norm(), div = 1 / beta;
                    for (j = 0; j < n; ++j)
                    {
                        v_next[j].MultiplyEquals(div);
                    }
                    B[k, k + 1] = beta;
                }
            }

            // Wrap into rectangular array
            U = new Complex[m, m];
            V = new Complex[n, n];
            for (j = 0; j < rank; ++j)
            {
                Complex[] _v = _V[j];
                for (i = 0; i < n; ++i) V[j, i] = _v[i].Conjugate();

                Complex[] _u = _U[j];
                for (i = 0; i < m; ++i) U[j, i] = _u[i].Conjugate();
            }
        }

        #endregion


        #region Golub-Jahan-Lanczos algorithm for DenseMatrix<T>

        /// <summary>
        /// Convert a real or complex matrix into bidiagonal form using the Golub-Kahan-Lanczos Bidiagonalization Procedure.
        /// The method creates 3 matrices from a matrix A (m x n)
        /// 1. U (m x m), an orthogonal/unitary matrix
        /// 2. B (m x n), a upper bidiagonal matrix, with non zero items in A(i, i) and A(i, i + 1) only
        /// 3. V (n x n), an orthogonal/unitary matrix
        /// such that A = U^TBV
        /// 
        /// Unfortunately, this method is prone to numerical error blowup - unusable for most matrices with dimensions 
        /// greater than ~ 50 x 50.
        /// </summary>
        /// <param name="A">The matrix to be decomposed into bidiagonal form</param>
        private static void Decompose<T>(this DenseMatrix<T> matrix, out DenseMatrix<T> U, out DenseMatrix<T> B, out DenseMatrix<T> V, IDenseBLAS1<T> blas, Func<int, T> Sqrt1OverN, Func<T[], T> Norm2) where T : new()
        {
            MatrixChecks.CheckNotNull(matrix);

            IProvider<T> provider = blas.Provider;
            T one = provider.One;
            int m = matrix.Rows, n = matrix.Columns, i, j, k, rank = Math.Min(m, n);
            T[][] _U = new T[m][], _V = new T[n][];
            B = new DenseMatrix<T>(m, n);

            // set v1 to unit 2-norm vector
            T v_value = Sqrt1OverN(n);
            T[] v_0 = new T[n];
            for (i = 0; i < n; ++i) v_0[i] = v_value;
            _V[0] = v_0;

            T[][] A = matrix.Values;
            for (k = 0; k < rank; ++k)
            {
                // Calculate u_k := A * v_k - beta_(k - 1) * u_(k - 1)
                T[] v_k = _V[k];
                T[] u = new T[m];
                _U[k] = u;
                for (i = 0; i < m; ++i)
                {
                    u[i] = blas.DOT(A[i], v_k, 0, n);
                }

                if (k > 0)
                {
                    blas.AXPY(u, _U[k - 1], provider.Negate(B[k - 1, k]), 0, m);
                }

                // Calculate alpha, and set as diagonal term in bidiagonal matrix
                T alpha = Norm2(u);
                B[k, k] = alpha;
                blas.SCAL(u, provider.Divide(one, alpha), 0, m);

                // Calculate v_(k + 1) := A^T * u_k - A[k, k] * v_k,  if k < rank - 1
                // For complex numbers A^T is replaced with A*, the conjugate transpose
                if (k < rank - 1)
                {
                    T[] vNext = new T[n];
                    for (j = 0; j < n; ++j)
                    {
                        T sum = blas.XTA(u, A, j, 0, m);
                        vNext[j] = provider.Subtract(sum, provider.Multiply(alpha, v_k[j]));
                    }
                    _V[k + 1] = vNext;

                    // Calculate beta, the off diagonal terms
                    T beta = Norm2(vNext);
                    blas.SCAL(vNext, provider.Divide(one, beta), 0, n);
                    B[k, k + 1] = beta;
                }
            }

            // Fill in the remaining rows
            for (k = rank; k < m; ++k)
            {
                _U[k] = new T[m];
            }

            // Wrap into rectangular array
            U = new DenseMatrix<T>(_U);
            V = new DenseMatrix<T>(_V);
        }
        internal static void Decompose(DenseMatrix<float> A, out DenseMatrix<float> U, out DenseMatrix<float> B, out DenseMatrix<float> V)
        {
            Decompose(A, out U, out B, out V, new NativeFloatProvider(), n => (float)Math.Sqrt(1.0f / n), x => x.Norm(2));
        }
        internal static void Decompose(DenseMatrix<double> A, out DenseMatrix<double> U, out DenseMatrix<double> B, out DenseMatrix<double> V)
        {
            Decompose(A, out U, out B, out V, new NativeDoubleProvider(), n => Math.Sqrt(1.0 / n), x => x.Norm(2));
        }
        internal static void Decompose(DenseMatrix<decimal> A, out DenseMatrix<decimal> U, out DenseMatrix<decimal> B, out DenseMatrix<decimal> V)
        {
            Decompose(A, out U, out B, out V, new NativeDecimalProvider(), n => DecimalMath.Sqrt(1.0m / n), x => x.Norm(2));
        }
        /// <summary>
        /// Convert a complex matrix into bidiagonal form using the Golub-Kahan-Lanczos Bidiagonalization Procedure.
        /// The method creates 3 matrices from a matrix A (m x n)
        /// 1. U (m x m), an unitary matrix
        /// 2. B (m x n), a upper bidiagonal matrix, with non zero items in A(i, i) and A(i, i + 1) only
        /// 3. V (n x n), an unitary matrix
        /// such that A = U*BV where U* is the conjugate tranpose of U
        /// </summary>
        /// <param name="A">The matrix to be decomposed into bidiagonal form</param>
        internal static void Decompose(DenseMatrix<Complex> A, out DenseMatrix<Complex> U, out DenseMatrix<Complex> B, out DenseMatrix<Complex> V)
        {
            if (A == null) throw new ArgumentNullException();

            int m = A.Rows, n = A.Columns, i, j, k, rank = Math.Min(m, n);

            Complex zero = Complex.Zero, sum;
            Complex[] v, v_next, u, u_prev;
            Complex[][] _U = new Complex[m][],
                        _V = new Complex[n][];
            B = new Complex[m, n];

            // set v1 to unit 2-norm vector
            double val = Math.Sqrt(1.0 / n);
            Complex[] v_0 = new Complex[n];
            for (i = 0; i < n; ++i) v_0[i] = val;
            _V[0] = v_0;

            for (k = 0; k < rank; ++k)
            {
                // Calculate u_k := A * v_k - beta_(k - 1) * u_(k - 1)
                v = _V[k];
                u = new Complex[m];
                _U[k] = u;
                for (i = 0; i < m; ++i)
                {
                    sum = zero;
                    for (j = 0; j < n; ++j)
                    {
                        sum.IncrementBy(A[i, j].Multiply(v[j]));
                    }
                    u[i] = sum;
                }

                if (k > 0)
                {
                    u_prev = _U[k - 1];
                    Complex beta = B[k - 1, k];
                    for (i = 0; i < m; ++i)
                    {
                        u[i].DecrementBy(beta.Multiply(u_prev[i]));
                    }
                }

                // Calculate alpha, and set as diagonal term in bidiagonal matrix
                double alpha = u.Norm();
                B[k, k] = alpha;

                // u_k := u_k / alpha
                double factor = 1 / alpha;
                for (i = 0; i < m; ++i)
                {
                    u[i].MultiplyEquals(factor);
                }

                // Calculate v_(k + 1) := A* * u_k - A[k, k] * v_k,  if k < rank - 1
                // For complex numbers A^T is replaced with A*, the conjugate transpose
                if (k < rank - 1)
                {
                    v_next = new Complex[n];
                    for (j = 0; j < n; ++j)
                    {
                        sum = zero;
                        for (i = 0; i < m; ++i)
                        {
                            sum.IncrementBy(A[i, j].Conjugate().Multiply(u[i]));
                        }
                        v_next[j] = sum.Subtract(alpha * v[j]);
                    }
                    _V[k + 1] = v_next;

                    // Calculate beta, the off diagonal terms
                    double beta = v_next.Norm(), div = 1 / beta;
                    for (j = 0; j < n; ++j)
                    {
                        v_next[j].MultiplyEquals(div);
                    }
                    B[k, k + 1] = beta;
                }
            }

            // Wrap into rectangular array
            U = new Complex[m, m];
            V = new Complex[n, n];
            for (j = 0; j < rank; ++j)
            {
                Complex[] _v = _V[j];
                for (i = 0; i < n; ++i) V[j, i] = _v[i].Conjugate();

                Complex[] _u = _U[j];
                for (i = 0; i < m; ++i) U[j, i] = _u[i].Conjugate();
            }
        }
        #endregion

    }
}
