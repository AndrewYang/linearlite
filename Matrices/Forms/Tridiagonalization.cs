﻿using LinearNet.Providers;
using LinearNet.Providers.LinearAlgebra;
using LinearNet.Structs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Matrices
{
    public static class Tridiagonalization
    {
        #region Private methods 
        /// <summary>
        /// U and V must be initialised as identity matrices
        /// If A is of dimensions (m x n), then 
        /// - U must be of (m x m)
        /// - D must be of (m x n)
        /// - V must be of (n x n)
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="A"></param>
        /// <param name="U"></param>
        /// <param name="D"></param>
        /// <param name="V"></param>
        /// <param name="HouseholderTransform"></param>
        private static void to_tridiagonal_form_unsafe<T>(this T[][] A, T[][] U, T[][] V, Action<T[][], T[][], T[], T[], int, int, int, int, bool> HouseholderTransform, Action<T[][]> ConjugateTranspose)
        {
            MatrixChecks.CheckNotNull(A);

            int m = A.Length, n = A[0].Length, min = Math.Min(m, n), max = Math.Max(m, n), c;

            T[] v = new T[max], w = new T[max];
            for (c = 0; c < min; ++c)
            {
                int begin = c + 1;
                if (begin < m)
                {
                    HouseholderTransform(A, U, v, w, c, n, begin, m, true);
                }

                if (begin < n)
                {
                    HouseholderTransform(A, V, v, w, c, n, begin, m, false);
                }
            }
            ConjugateTranspose(U);
        }
        private static void to_tridiagonal_form<T>(this T[,] A, out T[,] U, out T[,] B, out T[,] V, IDenseHouseholder<T> blas, Action<T[][]> ConjugateTranspose, bool parallel)
        {
            MatrixChecks.CheckNotNull(A);

            int m = A.GetLength(0), n = A.GetLength(1);
            T zero = blas.Provider.Zero, one = blas.Provider.One;
            T[][] _B = parallel ? A.ToJaggedParallel() : A.ToJagged();
            T[][] _U = MatrixInternalExtensions.JIdentity(zero, one, m);
            T[][] _V = MatrixInternalExtensions.JIdentity(zero, one, n);

            if (parallel)
            {
                to_tridiagonal_form_unsafe(_B, _U, _V, blas.HouseholderTransformParallel, ConjugateTranspose);
                U = _U.ToRectangularParallel();
                B = _B.ToRectangularParallel();
                V = _V.ToRectangularParallel();
            }
            else
            {
                to_tridiagonal_form_unsafe(_B, _U, _V, blas.HouseholderTransform, ConjugateTranspose);
                U = _U.ToRectangular();
                B = _B.ToRectangular();
                V = _V.ToRectangular();
            }
        }
        private static void to_tridiagonal_form<T>(this DenseMatrix<T> A, out DenseMatrix<T> U, out DenseMatrix<T> B, out DenseMatrix<T> V, IDenseHouseholder<T> blas, Action<T[][]> ConjugateTranspose, bool parallel) where T : new()
        {
            MatrixChecks.CheckNotNull(A);

            int m = A.Rows, n = A.Columns;
            T zero = blas.Provider.Zero, one = blas.Provider.One;
            T[][] _B = parallel ? A.CopyParallel().Values : A.Copy().Values;
            T[][] _U = MatrixInternalExtensions.JIdentity(zero, one, m);
            T[][] _V = MatrixInternalExtensions.JIdentity(zero, one, n);

            if (parallel)
            {
                to_tridiagonal_form_unsafe(_B, _U, _V, blas.HouseholderTransformParallel, ConjugateTranspose);
            }
            else
            {
                to_tridiagonal_form_unsafe(_B, _U, _V, blas.HouseholderTransform, ConjugateTranspose);
            }

            U = new DenseMatrix<T>(_U);
            B = new DenseMatrix<T>(_B);
            V = new DenseMatrix<T>(_V);
        }
        #endregion

        #region Sequential tridiagonal form methods for T[,]
        /// <summary>
        /// <para>Tridiagonalise an arbitary matrix $A\in\mathbb{F}^{m \times n}$ as $A = U^*MV$.</para>
        /// <para>This implementation use Householder transformations.</para>
        /// <!--inputs-->
        /// </summary>
        /// <name>ToTridiagonalForm</name>
        /// <proto>void ToTridiagonalForm(this IMatrix<T> A, out IMatrix<T> U, out IMatrix<T> M, out IMatrix<T> V)</proto>
        /// <cat>la</cat>
        /// <param name="A">The $m \times n$ matrix to be tridiagonalized.</param>
        /// <param name="U">
        /// <b>Out parameter</b><br/>
        /// A $m \times m$ left-orthogonal/unitary matrix, $U$.
        /// </param>
        /// <param name="D">
        /// <b>Out parameter</b><br/>
        /// A $m \times n$ tridiagonal matrix, $M$.
        /// </param>
        /// <param name="V">
        /// <b>Out parameter</b><br/>
        /// A $n \times n$ right-orthogonal/unitary matrix, $V$.
        /// </param>
        public static void ToTridiagonalForm(this float[,] A, out float[,] U, out float[,] D, out float[,] V)
        {
            to_tridiagonal_form(A, out U, out D, out V, BLAS.Float, M => M.TransposeInPlace(), false);
        }
        public static void ToTridiagonalForm(this double[,] A, out double[,] U, out double[,] D, out double[,] V)
        {
            to_tridiagonal_form(A, out U, out D, out V, BLAS.Double, M => M.TransposeInPlace(), false);
        }
        public static void ToTridiagonalForm(this decimal[,] A, out decimal[,] U, out decimal[,] D, out decimal[,] V)
        {
            to_tridiagonal_form(A, out U, out D, out V, BLAS.Decimal, M => M.TransposeInPlace(), false);
        }
        public static void ToTridiagonalForm(this Complex[,] A, out Complex[,] U, out Complex[,] D, out Complex[,] V)
        {
            to_tridiagonal_form(A, out U, out D, out V, BLAS.Complex, M => M.SelfConjugateTranspose(), false);
        }
        #endregion

        #region Sequential form methods for DenseMatrix matrices
        public static void ToTridiagonalForm(this DenseMatrix<float> A, out DenseMatrix<float> U, out DenseMatrix<float> D, out DenseMatrix<float> V)
        {
            to_tridiagonal_form(A, out U, out D, out V, BLAS.Float, M => M.TransposeInPlace(), false);
        }
        public static void ToTridiagonalForm(this DenseMatrix<double> A, out DenseMatrix<double> U, out DenseMatrix<double> D, out DenseMatrix<double> V)
        {
            to_tridiagonal_form(A, out U, out D, out V, BLAS.Double, M => M.TransposeInPlace(), false);
        }
        public static void ToTridiagonalForm(this DenseMatrix<decimal> A, out DenseMatrix<decimal> U, out DenseMatrix<decimal> D, out DenseMatrix<decimal> V)
        {
            to_tridiagonal_form(A, out U, out D, out V, BLAS.Decimal, M => M.TransposeInPlace(), false);
        }
        public static void ToTridiagonalForm(this DenseMatrix<Complex> A, out DenseMatrix<Complex> U, out DenseMatrix<Complex> D, out DenseMatrix<Complex> V)
        {
            to_tridiagonal_form(A, out U, out D, out V, BLAS.Complex, M => M.SelfConjugateTranspose(), false);
        }
        #endregion

        #region Parallel tridiagonal form methods for T[,]
        public static void ToTridiagonalFormParallel(this float[,] A, out float[,] U, out float[,] D, out float[,] V)
        {
            to_tridiagonal_form(A, out U, out D, out V, BLAS.Float, M => M.TransposeInPlaceParallel(), true);
        }
        public static void ToTridiagonalFormParallel(this double[,] A, out double[,] U, out double[,] D, out double[,] V)
        {
            to_tridiagonal_form(A, out U, out D, out V, BLAS.Double, M => M.TransposeInPlaceParallel(), true);
        }
        public static void ToTridiagonalFormParallel(this decimal[,] A, out decimal[,] U, out decimal[,] D, out decimal[,] V)
        {
            to_tridiagonal_form(A, out U, out D, out V, BLAS.Decimal, M => M.TransposeInPlaceParallel(), true);
        }
        public static void ToTridiagonalFormParallel(this Complex[,] A, out Complex[,] U, out Complex[,] D, out Complex[,] V)
        {
            to_tridiagonal_form(A, out U, out D, out V, BLAS.Complex, M => M.SelfConjugateTransposeParallel(), true);
        }
        #endregion

        #region Parallel tridiagonal form methods for DenseMatrix
        public static void ToTridiagonalFormParallel(this DenseMatrix<float> A, out DenseMatrix<float> U, out DenseMatrix<float> D, out DenseMatrix<float> V)
        {
            to_tridiagonal_form(A, out U, out D, out V, BLAS.Float, M => M.TransposeInPlaceParallel(), true);
        }
        public static void ToTridiagonalFormParallel(this DenseMatrix<double> A, out DenseMatrix<double> U, out DenseMatrix<double> D, out DenseMatrix<double> V)
        {
            to_tridiagonal_form(A, out U, out D, out V, BLAS.Double, M => M.TransposeInPlaceParallel(), true);
        }
        public static void ToTridiagonalFormParallel(this DenseMatrix<decimal> A, out DenseMatrix<decimal> U, out DenseMatrix<decimal> D, out DenseMatrix<decimal> V)
        {
            to_tridiagonal_form(A, out U, out D, out V, BLAS.Decimal, M => M.TransposeInPlaceParallel(), true);
        }
        public static void ToTridiagonalFormParallel(this DenseMatrix<Complex> A, out DenseMatrix<Complex> U, out DenseMatrix<Complex> D, out DenseMatrix<Complex> V)
        {
            to_tridiagonal_form(A, out U, out D, out V, BLAS.Complex, M => M.SelfConjugateTransposeParallel(), true);
        }
        #endregion
    }
}
