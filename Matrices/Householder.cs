﻿using LinearNet.Providers;
using LinearNet.Global;
using LinearNet.Structs;
using System;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using LinearNet.Providers.LinearAlgebra;

namespace LinearNet.Matrices
{
    internal static class Householder
    {
        #region Sequential Householder reflection methods 

        /// <summary>
        /// TODO: simplify the zeroes row
        /// </summary>
        private static void ZeroColumnDown(double[][] A, double[][] H, double[] v, double[] w, int col_start, int col_end, int row_start, int row_end)
        {
            NativeDoubleProvider blas = new NativeDoubleProvider();
            int i;

            // Calculate the vertical projection vector
            for (i = row_start; i < row_end; ++i)
            {
                v[i] = A[i][col_start];
            }
            v[row_start] += Math.Sign(v[row_start]) * v.Norm(row_start, row_end);
            blas.SCAL(v, Constants.Sqrt2 / v.Norm(row_start, row_end), row_start, row_end);

            // HH reflect on column c using vector v
            Array.Clear(w, col_start, col_end - col_start);
            for (i = row_start; i < row_end; ++i)
            {
                blas.AXPY(w, A[i], v[i], col_start, col_end);
            }
            for (i = row_start; i < row_end; ++i)
            {
                blas.AXPY(A[i], w, -v[i], col_start, col_end);
            }

            // update the left orthogonal matrix, _U
            for (i = 0; i < row_end; ++i)
            {
                double q_i = blas.DOT(v, H[i], row_start, row_end);
                blas.AXPY(H[i], v, -q_i, row_start, row_end);
            }
        }
        /// <summary>
        /// Zero everything using (row_end, col_end) as the pivot, the column of index col_end will have its elements in range [row_start, row_end] zeroed
        /// </summary>
        internal static void ZeroColumnUpLeft(double[][] A, double[][] Q, double[] v, double[] w, int col_start, int col_end, int row_start, int row_end)
        {
            NativeDoubleProvider blas = new NativeDoubleProvider();
            int row_end_incl = row_end + 1, 
                col_end_incl = col_end + 1, i;

            // Calculate the vertical projection vector
            for (i = row_start; i < row_end_incl; ++i)
            {
                v[i] = A[i][col_end];
            }
            v[row_end] += Math.Sign(v[row_end]) * v.Norm(row_start, row_end_incl);
            blas.SCAL(v, Constants.Sqrt2 / v.Norm(row_start, row_end_incl), row_start, row_end_incl);

            // HH reflect on column c using vector v
            Array.Clear(w, col_start, col_end_incl - col_start);
            for (i = row_start; i < row_end_incl; ++i)
            {
                blas.AXPY(w, A[i], v[i], col_start, col_end_incl);
            }
            for (i = row_start; i < row_end_incl; ++i)
            {
                blas.AXPY(A[i], w, -v[i], col_start, col_end_incl);
            }

            // update the left orthogonal matrix, H
            for (i = 0; i < row_end_incl; ++i)
            {
                double q_i = blas.DOT(v, Q[i], row_start, row_end_incl);
                blas.AXPY(Q[i], v, -q_i, row_start, row_end_incl);
            }
        }
        internal static void TransformColumn(double[][] A, double[] v, double[] w, int colStart, int colEnd, int rowStart, int rowEnd)
        {
            NativeDoubleProvider blas = new NativeDoubleProvider();
            int i;

            // Calculate the vertical projection vector
            for (i = rowStart; i < rowEnd; ++i) v[i] = A[i][colStart];
            v[rowStart] += Math.Sign(v[rowStart]) * v.Norm(rowStart, rowEnd);
            blas.SCAL(v, Constants.Sqrt2 / v.Norm(rowStart, rowEnd), rowStart, rowEnd);

            // HH reflect on column c using vector v
            Array.Clear(w, colStart, colEnd - colStart);
            for (i = rowStart; i < rowEnd; ++i)
            {
                blas.AXPY(w, A[i], v[i], colStart, colEnd);
            }
            for (i = rowStart; i < rowEnd; ++i)
            {
                blas.AXPY(A[i], w, -v[i], colStart, colEnd);
            }
        }
        private static void TransformRow(double[][] A, double[][] H, double[] v, double[] w, int colStart, int colEnd, int rowStart, int rowEnd)
        {
            double[] row;
            double v_i, w_i, w_j;

            int i, j;

            for (j = rowStart; j < colEnd; ++j) v[j] = A[colStart][j];
            int sign = v[rowStart] >= 0 ? 1 : -1;
            v[rowStart] += sign * v.Norm(rowStart, colEnd);
            v.MultiplyOverwrite(Constants.Sqrt2 / v.Norm(rowStart, colEnd), rowStart, colEnd);

            // HH reflect on row c using vector v
            for (i = colStart; i < rowEnd; ++i)
            {
                row = A[i];
                w_i = 0.0;
                for (j = rowStart; j < colEnd; ++j)
                {
                    w_i += v[j] * row[j];
                }
                for (j = rowStart; j < colEnd; ++j)
                {
                    row[j] -= v[j] * w_i;
                }
            }
            // update the left orthogonal matrix, _V
            for (j = 0; j < colEnd; ++j)
            {
                w_j = 0.0;
                for (i = rowStart; i < colEnd; ++i)
                {
                    w_j += v[i] * H[i][j];
                }
                w[j] = w_j;
            }
            for (i = rowStart; i < colEnd; ++i)
            {
                row = H[i];
                v_i = v[i];
                for (j = 0; j < colEnd; ++j)
                {
                    row[j] -= w[j] * v_i;
                }
            }
        }
        
        /// <summary>
        /// Perform a  Holderholder transformation to matrix A which is presumed to be in upper Hessenberg form. 
        /// A is not checked to sure that it is a Hessenberg matrix, rather all entires below the lower minor-diagonal 
        /// will be treated as though its identically 0, even if its not.
        /// 
        /// If 'clean' is set to true, then the elements which should be 0 will be set to 0 (in the colum being transformed)
        /// </summary>
        /// <param name="A"></param>
        /// <param name="H"></param>
        /// <param name="v"></param>
        /// <param name="w"></param>
        /// <param name="colStart"></param>
        /// <param name="colEnd"></param>
        /// <param name="rowStart"></param>
        /// <param name="rowEnd"></param>
        /// <summary>
        /// TODO: simplify the zeroes row
        /// </summary>
        private static void TransformColumn(Complex[][] A, Complex[][] H, Complex[] v, Complex[] w, int colStart, int colEnd, int rowStart, int rowEnd)
        {
            Complex[] row;
            Complex zero = new Complex(0, 0), v_i, a, b;
            int r, i, j;

            // Calculate the vertical projection vector
            for (r = rowStart; r < rowEnd; ++r) v[r] = A[r][colStart];
            Complex sign = Complex.Exp(Complex.I.Multiply(v[rowStart].Argument()));
            v[rowStart].IncrementBy(sign.Multiply(v.Norm(rowStart, rowEnd)));
            v.MultiplyOverwrite(Constants.Sqrt2 / v.Norm(rowStart, rowEnd), rowStart, rowEnd);

            // HH reflect on column c using vector v
            for (j = colStart; j < colEnd; ++j)
            {
                double w_j_re = 0.0, w_j_im = 0.0;
                for (i = rowStart; i < rowEnd; ++i)
                {
                    a = v[i];
                    b = A[i][j];

                    // w_j += conj(a) * b
                    w_j_re += (a.Real * b.Real + a.Imaginary * b.Imaginary);
                    w_j_im += (-a.Imaginary * b.Real + a.Real * b.Imaginary);
                }
                w[j] = new Complex(w_j_re, w_j_im);
            }

            A[rowStart][colStart].DecrementBy(w[colStart].Multiply(v[rowStart]));
            for (i = rowStart + 1; i < rowEnd; ++i)
            {
                A[i][colStart] = zero;
            }

            int col_first = colStart + 1;
            for (i = rowStart; i < rowEnd; ++i)
            {
                row = A[i];
                v_i = v[i];
                for (j = col_first; j < colEnd; ++j)
                {
                    b = w[j];
                    row[j].Real -= (v_i.Real * b.Real - v_i.Imaginary * b.Imaginary);
                    row[j].Imaginary -= (v_i.Imaginary * b.Real + v_i.Real * b.Imaginary);
                }
            }

            // update the left orthogonal matrix, _U
            for (i = 0; i < rowEnd; ++i)
            {
                row = H[i];
                double q_i_re = 0.0, q_i_im = 0.0;
                for (j = rowStart; j < rowEnd; ++j)
                {
                    a = v[j];
                    b = row[j];
                    q_i_re += (a.Real * b.Real - a.Imaginary * b.Imaginary);
                    q_i_im += (a.Imaginary * b.Real + a.Real * b.Imaginary);
                }
                for (j = rowStart; j < rowEnd; ++j)
                {
                    b = v[j];

                    // row[j] += q_i * cong(v[j])
                    row[j].Real -= (q_i_re * b.Real + q_i_im * b.Imaginary);
                    row[j].Imaginary -= (q_i_im * b.Real - q_i_re * b.Imaginary);
                }
            }
        }
        private static void TransformRow(Complex[][] A, Complex[][] H, Complex[] v, Complex[] w, int colStart, int colEnd, int rowStart, int rowEnd)
        {
            Complex[] row = A[colStart];
            Complex a, b;
            int i, j;

            // Calculate the horizontal projection vector
            for (j = rowStart; j < colEnd; ++j) v[j] = row[j];
            Complex sign = Complex.Exp(Complex.I.Multiply(v[rowStart].Argument()));
            v[rowStart].IncrementBy(sign.Multiply(v.Norm(rowStart, colEnd)));
            v.MultiplyOverwrite(Constants.Sqrt2 / v.Norm(rowStart, colEnd), rowStart, colEnd);

            // HH reflect on row c using vector v
            for (i = colStart; i < rowEnd; ++i)
            {
                row = A[i];
                double w_i_re = 0.0, w_i_im = 0.0;
                for (j = rowStart; j < colEnd; ++j)
                {
                    a = v[j];
                    b = row[j];
                    w_i_re += (a.Real * b.Real + a.Imaginary * b.Imaginary);
                    w_i_im += (-a.Imaginary * b.Real + a.Real * b.Imaginary);
                }
                for (j = rowStart; j < colEnd; ++j)
                {
                    a = v[j];
                    row[j].Real -= (a.Real * w_i_re - a.Imaginary * w_i_im);
                    row[j].Imaginary -= (a.Imaginary * w_i_re + a.Real * w_i_im);
                }
            }
            // update the left orthogonal matrix, _V
            for (j = 0; j < colEnd; ++j)
            {
                double w_j_re = 0.0, w_j_im = 0.0;
                for (i = rowStart; i < colEnd; ++i)
                {
                    a = v[i];
                    b = H[i][j];
                    w_j_re += (a.Real * b.Real - a.Imaginary * b.Imaginary);
                    w_j_im += (a.Imaginary * b.Real + a.Real * b.Imaginary);
                }
                w[j] = new Complex(w_j_re, w_j_im);
            }
            for (i = rowStart; i < colEnd; ++i)
            {
                row = H[i];
                double v_i_re = v[i].Real, v_i_im = -v[i].Imaginary;
                for (j = 0; j < colEnd; ++j)
                {
                    a = w[j];
                    row[j].Real -= (a.Real * v_i_re - a.Imaginary * v_i_im);
                    row[j].Imaginary -= (a.Imaginary * v_i_re + a.Real * v_i_im);
                    //row[j].Decrement(w[j].Multiply(v_i));
                }
            }
        }
        /// <summary>
        /// The base method for performing a Householder transformation.
        /// 
        /// This method is used by all matrix types except for double[,] and Complex[,] because those are commonly used and have their 
        /// own specialised optimisations. 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="A">The matrix (m x n) from which a row or column will be extracted to be used as the Householder vector.</param>
        /// <param name="H">This matrix (m x m) will be postmultiplied by the Householder reflection matrix. If a row reflector is used instead, then 
        /// this matrix will be premultiplied by the Householder reflector matrix. 
        /// </param>
        /// <param name="v">A vector buffer of length >= m if a column reflector is used, or of length >= n if a row reflector is used.</param>
        /// <param name="w">A vector buffer of length >= m if a column reflector is used, or of length >= n if a row reflector is used.</param>
        /// <param name="col_start">The first column of the submatrix of A for which the Householder reflection will be applied.</param>
        /// <param name="col_end">The number of columns of A (= m)</param>
        /// <param name="row_start">The first row of the submatrix of A for which the Householder reflection will be applied.</param>
        /// <param name="row_end">The number of rows of A</param>
        /// <param name="isColumn">If true, performs a Householder reflection of a column vector, otherwise a row vector is used.</param>
        /// <param name="Sign">(T x) => returns the sign of x</param>
        /// <param name="Norm">(T[] x, int start, int end) => returns the norm of vector x taken in the range [start, end)</param>
        /// <param name="Add">(T x, double y) => x + y</param>
        /// <param name="Negate">(T x) => -x</param>
        /// <param name="SCAL">The xSCAL BLAS 1 operation, (x, alpha, start, end) => x[i] <- alpha * x[i] for i in [start, end)</param>
        /// <param name="AMULT">(T[] x, T[][] A, int i, int start, int end) => Calculate (x^T * A)[i] with sum taken from k in [start, end)</param>
        /// <param name="AXPY">The xAXPY BLAS 1 operation, (u, v, alpha, start, end) => u[i] <- u[i] + alpha * v[i] for i in [start, end)</param>
        /// <param name="DOT">The xDOT BLAS 1 operation, (u, b, start, end) => return sum(u[i] * v[i]) over i in [start, end)</param>
        private static void Transform<T>(T[][] A, T[][] H, T[] v, T[] w, int col_start, int col_end, int row_start, int row_end, bool isColumn, IDenseBLAS1<T> blas, Func<T[], int, int, T> SignedNorm, Func<T[], int, int, T> Factor)
        {
            IProvider<T> provider = blas.Provider;
            int i;
            if (isColumn)
            {
                // Calculate the vertical projection vector
                for (i = row_start; i < row_end; ++i) v[i] = A[i][col_start];
                v[row_start] = blas.Provider.Add(v[row_start], SignedNorm(v, row_start, row_end));
                blas.SCAL(v, Factor(v, row_start, row_end), row_start, row_end);

                // HH reflect on column c using vector v
                for (i = col_start; i < col_end; ++i)
                {
                    w[i] = blas.XTA(v, A, i, row_start, row_end);
                }
                for (i = row_start; i < row_end; ++i)
                {
                    blas.AXPY(A[i], w, provider.Negate(v[i]), col_start, col_end);
                }

                // update the left orthogonal matrix, _U
                for (i = 0; i < row_end; ++i)
                {
                    T[] row = H[i];
                    T q_i = blas.DOT(v, row, row_start, row_end);
                    blas.AXPY(row, v, provider.Negate(q_i), row_start, row_end);
                }
            }
            else
            {
                for (i = row_start; i < col_end; ++i) v[i] = A[col_start][i];
                v[row_start] = blas.Provider.Add(v[row_start], SignedNorm(v, row_start, col_end));
                blas.SCAL(v, Factor(v, row_start, col_end), row_start, col_end);

                // HH reflect on row c using vector v
                for (i = col_start; i < row_end; ++i)
                {
                    T[] row = A[i];
                    T w_i = blas.DOT(v, row, row_start, col_end);
                    blas.AXPY(row, v, provider.Negate(w_i), row_start, col_end);
                }
                // update the left orthogonal matrix, _V
                for (i = 0; i < col_end; ++i)
                {
                    w[i] = blas.XTA(v, H, i, row_start, col_end);
                }
                for (i = row_start; i < col_end; ++i)
                {
                    blas.AXPY(H[i], w, provider.Negate(v[i]), 0, col_end);
                }
            }
        }

        /// <summary>
        /// Performs a row or vector Householder transformation on a submatrix of matrix A (m x n), storing the reflection matrix in H. 
        /// The submatrix is defined as taking the rows of A in the range [rowIndex, rows), and the columns in the range [colIndex, cols).
        /// If H is initialised to I, the identity matrix, then H will become the actual reflection matrix used. Otherwise it will be equal 
        /// to be matrix product of the initial value of H, with the reflector matrix used. 
        /// 
        /// The actual reflection matrix will be generated from column vector at 'colIndex' if isColumn = true, otherwise the row vector 
        /// at 'rowIndex' will be used. The Householder matrix will be a defined as I - 2vv^T where v is the vector. 
        /// 
        /// The vectors v and w serve no purpose other than to act as buffers in which the intermediary results will be stored. In particular
        /// their contents are not reliable in that it may vary between implementations. 
        /// </summary>
        /// <param name="A">m x n matrix of type float[,]</param>
        /// <param name="H">m x m matrix if isColumn = true, or a n x n matrix otherwise.</param>
        /// <param name="v">A vector buffer of length >= m if isColumn = true, or of length >= n if otherwise.</param>
        /// <param name="w">A vector buffer of length >= m if isColumn = true, or of length >= n if otherwise.</param>
        /// <param name="col_start">The first column of the submatrix of A for which the Householder reflection will be applied.</param>
        /// <param name="col_end">The number of columns of matrix A (= m)</param>
        /// <param name="row_start">The first row of the submatrix of A for which the Householder reflection will be applied.</param>
        /// <param name="row_end">The number of rows of matrix A (= n)</param>
        /// <param name="isColumn">If true, a column 'colIndex' A will be used to generate the reflection matrix, otherwise the row 'rowIndex' 
        /// will be used to generate the reflection matrix.</param>
        internal static void Transform(double[][] A, double[][] H, double[] v, double[] w, int col_start, int col_end, int row_start, int row_end, bool isColumn)
        {
            if (isColumn)
            {
                ZeroColumnDown(A, H, v, w, col_start, col_end, row_start, row_end);
            }
            else
            {
                TransformRow(A, H, v, w, col_start, col_end, row_start, row_end);
            }
        }
        internal static void Transform(float[][] A, float[][] H, float[] v, float[] w, int col_start, int col_end, int row_start, int row_end, bool isColumn)
        {
            Transform(A, H, v, w, col_start, col_end, row_start, row_end, isColumn,
                new NativeFloatProvider(),
                (x, start, end) => Math.Sign(x[start]) * x.Norm(start, end),
                (x, start, end) => Constants.Sqrt2_f / x.Norm(start, end));
        }
        internal static void Transform(decimal[][] A, decimal[][] H, decimal[] v, decimal[] w, int col_start, int col_end, int row_start, int row_end, bool isColumn)
        {
            Transform(A, H, v, w, col_start, col_end, row_start, row_end, isColumn,
                new NativeDecimalProvider(),
                (x, start, end) => Math.Sign(x[start]) * x.Norm(start, end),
                (x, start, end) => Constants.Sqrt2_m / x.Norm(start, end));
        }
        internal static void Transform(Complex[][] A, Complex[][] H, Complex[] v, Complex[] w, int col_start, int col_end, int row_start, int row_end, bool isColumn)
        {
            if (isColumn)
            {
                TransformColumn(A, H, v, w, col_start, col_end, row_start, row_end);
            }
            else
            {
                TransformRow(A, H, v, w, col_start, col_end, row_start, row_end);
            }
        }

        #endregion

        #region Sequential Householder reflection methods that don't store the orthogonal matrix 

        /// <summary>
        /// The base method for performing a Householder transformation.
        /// 
        /// This method is used by all matrix types except for double[,] and Complex[,] because those are commonly used and have their 
        /// own specialised optimisations. 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="A">The matrix (m x n) from which a row or column will be extracted to be used as the Householder vector.</param>
        /// <param name="H">This matrix (m x m) will be postmultiplied by the Householder reflection matrix. If a row reflector is used instead, then 
        /// this matrix will be premultiplied by the Householder reflector matrix. 
        /// </param>
        /// <param name="v">A vector buffer of length >= m if a column reflector is used, or of length >= n if a row reflector is used.</param>
        /// <param name="w">A vector buffer of length >= m if a column reflector is used, or of length >= n if a row reflector is used.</param>
        /// <param name="colIndex">The first column of the submatrix of A for which the Householder reflection will be applied.</param>
        /// <param name="columns">The number of columns of A (= m)</param>
        /// <param name="rowIndex">The first row of the submatrix of A for which the Householder reflection will be applied.</param>
        /// <param name="rows">The number of rows of A</param>
        /// <param name="isColumn">If true, performs a Householder reflection of a column vector, otherwise a row vector is used.</param>
        /// <param name="Sign">(T x) => returns the sign of x</param>
        /// <param name="Norm">(T[] x, int start, int end) => returns the norm of vector x taken in the range [start, end)</param>
        /// <param name="Add">(T x, double y) => x + y</param>
        /// <param name="Negate">(T x) => -x</param>
        /// <param name="SCAL">The xSCAL BLAS 1 operation, (x, alpha, start, end) => x[i] <- alpha * x[i] for i in [start, end)</param>
        /// <param name="AMULT">(T[] x, T[][] A, int i, int start, int end) => Calculate (x^T * A)[i] with sum taken from k in [start, end)</param>
        /// <param name="AXPY">The xAXPY BLAS 1 operation, (u, v, alpha, start, end) => u[i] <- u[i] + alpha * v[i] for i in [start, end)</param>
        /// <param name="DOT">The xDOT BLAS 1 operation, (u, b, start, end) => return sum(u[i] * v[i]) over i in [start, end)</param>
        private static void Transform<T>(T[][] A, T[] v, T[] w, int colIndex, int columns, int rowIndex, int rows, bool isColumn, IDenseBLAS1<T> blas, Func<T[], int, int, T> SignedNorm, Func<T[], int, int, T> Factor)
        {
            IProvider<T> prov = blas.Provider;
            int i;
            if (isColumn)
            {
                // Calculate the vertical projection vector
                for (i = rowIndex; i < rows; ++i) v[i] = A[i][colIndex];
                v[rowIndex] = prov.Add(v[rowIndex], SignedNorm(v, rowIndex, rows));
                blas.SCAL(v, Factor(v, rowIndex, rows), rowIndex, rows);

                // HH reflect on column c using vector v
                for (i = colIndex; i < columns; ++i)
                {
                    w[i] = blas.XTA(v, A, i, rowIndex, rows);
                }
                for (i = rowIndex; i < rows; ++i)
                {
                    blas.AXPY(A[i], w, prov.Negate(v[i]), colIndex, columns);
                }
            }
            else
            {
                for (i = rowIndex; i < columns; ++i) v[i] = A[colIndex][i];
                v[rowIndex] = blas.Provider.Add(v[rowIndex], SignedNorm(v, rowIndex, columns));
                blas.SCAL(v, Factor(v, rowIndex, columns), rowIndex, columns);

                // HH reflect on row c using vector v
                for (i = colIndex; i < rows; ++i)
                {
                    T[] row = A[i];
                    T w_i = blas.DOT(v, row, rowIndex, columns);
                    blas.AXPY(row, v, prov.Negate(w_i), rowIndex, columns);
                }
            }
        }
        internal static void Transform(float[][] A, float[] v, float[] w, int colIndex, int columns, int rowIndex, int rows, bool isColumn)
        {
            Transform(A, v, w, colIndex, columns, rowIndex, rows, isColumn,
                   new NativeFloatProvider(),
                   (x, start, end) => Math.Sign(x[start]) * x.Norm(start, end),
                   (x, start, end) => Constants.Sqrt2_f / x.Norm(start, end));
        }
        internal static void Transform(double[][] A, double[] v, double[] w, int colIndex, int columns, int rowIndex, int rows, bool isColumn)
        {
            Transform(A, v, w, colIndex, columns, rowIndex, rows, isColumn,
                   new NativeDoubleProvider(),
                   (x, start, end) => Math.Sign(x[start]) * x.Norm(start, end),
                   (x, start, end) => Constants.Sqrt2 / x.Norm(start, end));
        }
        internal static void Transform(decimal[][] A, decimal[] v, decimal[] w, int colIndex, int columns, int rowIndex, int rows, bool isColumn)
        {
            Transform(A, v, w, colIndex, columns, rowIndex, rows, isColumn,
                new NativeDecimalProvider(),
                (x, start, end) => Math.Sign(x[start]) * x.Norm(start, end),
                (x, start, end) => Constants.Sqrt2_m / x.Norm(start, end));
        }
        internal static void Transform(Complex[][] A, Complex[] v, Complex[] w, int colIndex, int columns, int rowIndex, int rows, bool isColumn)
        {
            Complex v_i, a, b;
            int r, i, j;

            if (isColumn)
            {
                Complex[] row;

                // Calculate the vertical projection vector
                for (r = rowIndex; r < rows; ++r) v[r] = A[r][colIndex];
                Complex sign = Complex.Exp(Complex.I.Multiply(v[rowIndex].Argument()));
                v[rowIndex].IncrementBy(sign.Multiply(v.Norm(rowIndex, rows)));
                v.MultiplyOverwrite(Constants.Sqrt2 / v.Norm(rowIndex, rows), rowIndex, rows);

                // HH reflect on column c using vector v
                for (j = colIndex; j < columns; ++j)
                {
                    double w_j_re = 0.0, w_j_im = 0.0;
                    for (i = rowIndex; i < rows; ++i)
                    {
                        a = v[i];
                        b = A[i][j];
                        w_j_re += (a.Real * b.Real + a.Imaginary * b.Imaginary);
                        w_j_im += (-a.Imaginary * b.Real + a.Real * b.Imaginary);
                    }
                    w[j] = new Complex(w_j_re, w_j_im);
                }

                A[rowIndex][colIndex].DecrementBy(w[colIndex].Multiply(v[rowIndex]));
                for (i = rowIndex + 1; i < rows; ++i)
                {
                    A[i][colIndex].Real = 0.0;
                    A[i][colIndex].Imaginary = 0.0;
                }

                int col_first = colIndex + 1;
                for (i = rowIndex; i < rows; ++i)
                {
                    row = A[i];
                    v_i = v[i];
                    for (j = col_first; j < columns; ++j)
                    {
                        b = w[j];
                        row[j].Real -= (v_i.Real * b.Real - v_i.Imaginary * b.Imaginary);
                        row[j].Imaginary -= (v_i.Imaginary * b.Real + v_i.Real * b.Imaginary);
                    }
                }
            }
            else
            {
                Complex[] row = A[colIndex];

                // Calculate the horizontal projection vector
                for (j = rowIndex; j < columns; ++j) v[j] = row[j];
                Complex sign = Complex.Exp(Complex.I.Multiply(v[rowIndex].Argument()));
                v[rowIndex].IncrementBy(sign.Multiply(v.Norm(rowIndex, columns)));
                v.MultiplyOverwrite(Constants.Sqrt2 / v.Norm(rowIndex, columns), rowIndex, columns);

                // HH reflect on row c using vector v
                for (i = colIndex; i < rows; ++i)
                {
                    row = A[i];
                    double w_i_re = 0.0, w_i_im = 0.0;
                    for (j = rowIndex; j < columns; ++j)
                    {
                        a = v[j];
                        b = row[j];
                        w_i_re += (a.Real * b.Real + a.Imaginary * b.Imaginary);
                        w_i_im += (-a.Imaginary * b.Real + a.Real * b.Imaginary);
                    }
                    for (j = rowIndex; j < columns; ++j)
                    {
                        a = v[j];
                        row[j].Real -= (a.Real * w_i_re - a.Imaginary * w_i_im);
                        row[j].Imaginary -= (a.Imaginary * w_i_re + a.Real * w_i_im);
                    }
                }
            }
        }
        #endregion


        #region Parallel Householder reflection methods

        internal static void TransformColumnParallel(double[][] A, double[] v, double[] w, int col_start, int col_end, int row_start, int row_end)
        {
            NativeDoubleProvider blas = new NativeDoubleProvider();
            int i, batch = 1;

            // Calculate the vertical projection vector
            for (i = row_start; i < row_end; ++i) v[i] = A[i][col_start];
            v[row_start] += Math.Sign(v[row_start]) * v.Norm(row_start, row_end);
            blas.SCAL(v, Constants.Sqrt2 / v.Norm(row_start, row_end), row_start, row_end);

            // HH reflect on column c using vector v
            Array.Clear(w, col_start, col_end - col_start);
            for (i = row_start; i < row_end; ++i)
            {
                double[] A_i = A[i];
                double v_i = v[i];

                Parallel.ForEach(Partitioner.Create(col_start, col_end, batch), range =>
                {
                    int min = range.Item1, max = range.Item2, j;
                    for (j = min; j < max; ++j)
                    {
                        w[j] += A_i[j] * v_i;
                    }
                });
            }
            for (i = row_start; i < row_end; ++i)
            {
                double[] A_i = A[i];
                double v_i = v[i];

                Parallel.ForEach(Partitioner.Create(col_start, col_end, batch), range =>
                {
                    int min = range.Item1, max = range.Item2, j;
                    for (j = min; j < max; ++j)
                    {
                        A_i[j] -= w[j] * v_i;
                    }
                });
            }
        }

        /// <summary>
        /// Perform a Householder reflection using a column from A, in parallel.
        /// </summary>
        private static void TransformColumnParallel(double[][] A, double[][] H, double[] v, double[] w, int colIndex, int columns, int rowIndex, int rows)
        {
            // Calculate the vertical projection vector
            for (int r = rowIndex; r < rows; ++r) v[r] = A[r][colIndex];
            v[rowIndex] += Math.Sign(v[rowIndex]) * v.Norm(rowIndex, rows);
            v.MultiplyOverwrite(Constants.Sqrt2 / v.Norm(rowIndex, rows), rowIndex, rows);

            // HH reflect on column c using vector v
            Parallel.ForEach(Partitioner.Create(colIndex, columns), range =>
            {
                int min = range.Item1, max = range.Item2, i, j;
                for (j = min; j < max; ++j)
                {
                    double w_j = 0.0;
                    for (i = rowIndex; i < rows; ++i)
                    {
                        w_j += v[i] * A[i][j];
                    }
                    w[j] = w_j;
                }
            });

            Parallel.ForEach(Partitioner.Create(rowIndex, rows), range =>
            {
                int min = range.Item1, max = range.Item2, i, j;
                double[] row;
                for (i = min; i < max; ++i)
                {
                    row = A[i];
                    double v_i = v[i];
                    for (j = colIndex; j < columns; ++j)
                    {
                        row[j] -= v_i * w[j];
                    }
                }
            });

            // update the left orthogonal matrix, _U
            Parallel.ForEach(Partitioner.Create(0, rows), range =>
            {
                int min = range.Item1, max = range.Item2, i, j;
                double[] row;
                for (i = min; i < max; ++i)
                {
                    row = H[i];
                    double q_i = 0.0;
                    for (j = rowIndex; j < rows; ++j)
                    {
                        q_i += v[j] * row[j];
                    }
                    for (j = rowIndex; j < rows; ++j)
                    {
                        row[j] -= q_i * v[j];
                    }
                }
            });
        }
        /// <summary>
        /// TODO: simplify the zeroes row
        /// </summary>
        private static void TransformRowParallel(double[][] A, double[][] H, double[] v, double[] w, int colIndex, int columns, int rowIndex, int rows)
        {
            for (int j = rowIndex; j < columns; ++j) v[j] = A[colIndex][j];
            int sign = v[rowIndex] >= 0 ? 1 : -1;
            v[rowIndex] += sign * v.Norm(rowIndex, columns);
            v.MultiplyOverwrite(Constants.Sqrt2 / v.Norm(rowIndex, columns), rowIndex, columns);

            // HH reflect on row c using vector v
            //for (i = colIndex; i < rows; ++i)
            Parallel.ForEach(Partitioner.Create(colIndex, rows), range =>
            {
                int min = range.Item1, max = range.Item2, i, j;
                double[] row;
                for (i = min; i < max; ++i)
                {
                    row = A[i];
                    double w_i = 0.0;
                    for (j = rowIndex; j < columns; ++j)
                    {
                        w_i += v[j] * row[j];
                    }
                    for (j = rowIndex; j < columns; ++j)
                    {
                        row[j] -= v[j] * w_i;
                    }
                }
            });

            // update the left orthogonal matrix, _V
            Parallel.ForEach(Partitioner.Create(0, columns), range =>
            {
                int min = range.Item1, max = range.Item2, i, j;
                for (j = min; j < max; ++j)
                {
                    double w_j = 0.0;
                    for (i = rowIndex; i < columns; ++i)
                    {
                        w_j += v[i] * H[i][j];
                    }
                    w[j] = w_j;
                }
            });

            Parallel.ForEach(Partitioner.Create(rowIndex, columns), range =>
            {
                int min = range.Item1, max = range.Item2, i, j;
                double[] row;
                for (i = min; i < max; ++i)
                {
                    row = H[i];
                    double v_i = v[i];
                    for (j = 0; j < columns; ++j)
                    {
                        row[j] -= w[j] * v_i;
                    }
                }
            });
        }
        private static void TransformParallel<T>(T[][] A, T[][] H, T[] v, T[] w, int colIndex, int columns, int rowIndex, int rows, bool isColumn, IDenseBLAS1<T> blas, Func<T[], int, int, T> SignedNorm, Func<T[], int, int, T> Factor)
        {
            IProvider<T> prov = blas.Provider;
            if (isColumn)
            {
                // Calculate the vertical projection vector
                for (int i = rowIndex; i < rows; ++i) v[i] = A[i][colIndex];
                v[rowIndex] = blas.Provider.Add(v[rowIndex], SignedNorm(v, rowIndex, rows));
                blas.SCAL(v, Factor(v, rowIndex, rows), rowIndex, rows);

                // HH reflect on column c using vector v
                Parallel.ForEach(Partitioner.Create(colIndex, columns), range =>
                {
                    int min = range.Item1, max = range.Item2;
                    for (int i = min; i < max; ++i)
                    {
                        w[i] = blas.XTA(v, A, i, rowIndex, rows);
                    }
                });
                Parallel.ForEach(Partitioner.Create(rowIndex, rows), range =>
                {
                    int min = range.Item1, max = range.Item2;
                    for (int i = min; i < max; ++i)
                    {
                        blas.AXPY(A[i], w, prov.Negate(v[i]), colIndex, columns);
                    }
                });

                // update the left orthogonal matrix, _U
                Parallel.ForEach(Partitioner.Create(0, rows), range =>
                {
                    int min = range.Item1, max = range.Item2;
                    for (int i = min; i < max; ++i)
                    {
                        T[] row = H[i];
                        T q_i = blas.DOT(v, row, rowIndex, rows);
                        blas.AXPY(row, v, prov.Negate(q_i), rowIndex, rows);
                    }
                });
            }
            else
            {
                for (int i = rowIndex; i < columns; ++i) v[i] = A[colIndex][i];
                v[rowIndex] = blas.Provider.Add(v[rowIndex], SignedNorm(v, rowIndex, columns));
                blas.SCAL(v, Factor(v, rowIndex, columns), rowIndex, columns);

                // HH reflect on row c using vector v
                Parallel.ForEach(Partitioner.Create(colIndex, rows), range =>
                {
                    int min = range.Item1, max = range.Item2;
                    for (int i = min; i < max; ++i)
                    {
                        T[] row = A[i];
                        T w_i = blas.DOT(v, row, rowIndex, columns);
                        blas.AXPY(row, v, prov.Negate(w_i), rowIndex, columns);
                    }
                });

                // update the left orthogonal matrix, _V
                Parallel.ForEach(Partitioner.Create(0, columns), range =>
                {
                    int min = range.Item1, max = range.Item2;
                    for (int i = min; i < max; ++i)
                    {
                        w[i] = blas.XTA(v, H, i, rowIndex, columns);
                    }
                });
                Parallel.ForEach(Partitioner.Create(rowIndex, columns), range =>
                {
                    int min = range.Item1, max = range.Item2;
                    for (int i = min; i < max; ++i)
                    {
                        blas.AXPY(H[i], w, prov.Negate(v[i]), 0, columns);
                    }
                });
            }
        }

        /// <summary>
        /// This method for double type has its own set of optimisations, which is why its implemented differently to the other 
        /// data types. However the exposed interface is identical to other that of other data types.
        /// </summary>
        /// <param name="A"></param>
        /// <param name="H"></param>
        /// <param name="v"></param>
        /// <param name="w"></param>
        /// <param name="colIndex"></param>
        /// <param name="columns"></param>
        /// <param name="rowIndex"></param>
        /// <param name="rows"></param>
        /// <param name="isColumn"></param>
        internal static void TransformParallel(double[][] A, double[][] H, double[] v, double[] w, int colIndex, int columns, int rowIndex, int rows, bool isColumn)
        {
            if (isColumn)
            {
                TransformColumnParallel(A, H, v, w, colIndex, columns, rowIndex, rows);
            }
            else
            {
                TransformRowParallel(A, H, v, w, colIndex, columns, rowIndex, rows);
            }
        }
        internal static void TransformParallel(float[][] A, float[][] H, float[] v, float[] w, int colIndex, int columns, int rowIndex, int rows, bool isColumn)
        {
            TransformParallel(A, H, v, w, colIndex, columns, rowIndex, rows, isColumn,
                new NativeFloatProvider(),
                (x, start, end) => Math.Sign(x[start]) * x.Norm(start, end),
                (x, start, end) => Constants.Sqrt2_f / x.Norm(start, end));
        }
        internal static void TransformParallel(decimal[][] A, decimal[][] H, decimal[] v, decimal[] w, int colIndex, int columns, int rowIndex, int rows, bool isColumn)
        {
            TransformParallel(A, H, v, w, colIndex, columns, rowIndex, rows, isColumn,
                new NativeDecimalProvider(),
                (x, start, end) => Math.Sign(x[start]) * x.Norm(start, end),
                (x, start, end) => Constants.Sqrt2_m / x.Norm(start, end));
        }
        internal static void TransformParallel(Complex[][] A, Complex[][] H, Complex[] v, Complex[] w, int colIndex, int columns, int rowIndex, int rows, bool isColumn)
        {
            if (isColumn)
            {
                // Calculate the vertical projection vector
                for (int r = rowIndex; r < rows; ++r) v[r] = A[r][colIndex];

                Complex sign = Complex.Exp(Complex.I.Multiply(v[rowIndex].Argument()));
                v[rowIndex].IncrementBy(sign.Multiply(v.Norm(rowIndex, rows)));
                v.MultiplyOverwrite(Constants.Sqrt2 / v.Norm(rowIndex, rows), rowIndex, rows);

                // HH reflect on column c using vector v
                Parallel.ForEach(Partitioner.Create(colIndex, columns), range =>
                {
                    int min = range.Item1, max = range.Item2, i, j;
                    Complex a, b;
                    double w_j_re, w_j_im;

                    for (j = min; j < max; ++j)
                    {
                        w_j_re = 0.0;
                        w_j_im = 0.0;
                        for (i = rowIndex; i < rows; ++i)
                        {
                            a = v[i];
                            b = A[i][j];
                            w_j_re += (a.Real * b.Real + a.Imaginary * b.Imaginary);
                            w_j_im += (-a.Imaginary * b.Real + a.Real * b.Imaginary);
                        }
                        w[j] = new Complex(w_j_re, w_j_im);
                    }
                });


                A[rowIndex][colIndex].DecrementBy(w[colIndex].Multiply(v[rowIndex]));
                for (int i = rowIndex + 1; i < rows; ++i)
                {
                    A[i][colIndex].Real = 0.0;
                    A[i][colIndex].Imaginary = 0.0;
                }

                int col_first = colIndex + 1;
                Parallel.ForEach(Partitioner.Create(rowIndex, rows), range =>
                {
                    int min = range.Item1, max = range.Item2, i, j;
                    Complex[] row;
                    Complex v_i, b;

                    for (i = min; i < max; ++i)
                    {
                        row = A[i];
                        v_i = v[i];
                        for (j = col_first; j < columns; ++j)
                        {
                            b = w[j];
                            row[j].Real -= (v_i.Real * b.Real - v_i.Imaginary * b.Imaginary);
                            row[j].Imaginary -= (v_i.Imaginary * b.Real + v_i.Real * b.Imaginary);
                        }
                    }
                });

                // update the left orthogonal matrix, _U
                Parallel.ForEach(Partitioner.Create(0, rows), range =>
                {
                    int min = range.Item1, max = range.Item2, i, j;
                    Complex[] row;
                    Complex a, b;

                    for (i = min; i < max; ++i)
                    {
                        row = H[i];
                        double q_i_re = 0.0, q_i_im = 0.0;
                        for (j = rowIndex; j < rows; ++j)
                        {
                            a = v[j];
                            b = row[j];
                            q_i_re += (a.Real * b.Real - a.Imaginary * b.Imaginary);
                            q_i_im += (a.Imaginary * b.Real + a.Real * b.Imaginary);
                        }
                        for (j = rowIndex; j < rows; ++j)
                        {
                            b = v[j];
                            row[j].Real -= (q_i_re * b.Real + q_i_im * b.Imaginary);
                            row[j].Imaginary -= (q_i_im * b.Real - q_i_re * b.Imaginary);
                        }
                    }
                });
            }
            else
            {
                // Calculate the horizontal projection vector
                Complex[] rho = A[colIndex];
                for (int r = rowIndex; r < columns; ++r) v[r] = rho[r];

                Complex sign = Complex.Exp(Complex.I.Multiply(v[rowIndex].Argument()));
                v[rowIndex].IncrementBy(sign.Multiply(v.Norm(rowIndex, columns)));
                v.MultiplyOverwrite(Constants.Sqrt2 / v.Norm(rowIndex, columns), rowIndex, columns);

                // HH reflect on row c using vector v
                Parallel.ForEach(Partitioner.Create(colIndex, rows), range =>
                {
                    int min = range.Item1, max = range.Item2, i, j;
                    Complex a, b;
                    Complex[] row;
                    double w_i_re = 0.0, w_i_im = 0.0;

                    for (i = min; i < max; ++i)
                    {
                        row = A[i];
                        w_i_re = 0.0;
                        w_i_im = 0.0;
                        for (j = rowIndex; j < columns; ++j)
                        {
                            a = v[j];
                            b = row[j];
                            w_i_re += (a.Real * b.Real + a.Imaginary * b.Imaginary);
                            w_i_im += (-a.Imaginary * b.Real + a.Real * b.Imaginary);
                        }
                        for (j = rowIndex; j < columns; ++j)
                        {
                            a = v[j];
                            row[j].Real -= (a.Real * w_i_re - a.Imaginary * w_i_im);
                            row[j].Imaginary -= (a.Imaginary * w_i_re + a.Real * w_i_im);
                        }
                    }
                });

                // update the left orthogonal matrix, _V
                Parallel.ForEach(Partitioner.Create(0, columns), range =>
                {
                    int min = range.Item1, max = range.Item2, i, j;
                    double w_j_re, w_j_im;
                    Complex a, b;

                    for (j = min; j < max; ++j)
                    {
                        w_j_re = 0.0;
                        w_j_im = 0.0;

                        for (i = rowIndex; i < columns; ++i)
                        {
                            a = v[i];
                            b = H[i][j];
                            w_j_re += (a.Real * b.Real - a.Imaginary * b.Imaginary);
                            w_j_im += (a.Imaginary * b.Real + a.Real * b.Imaginary);
                        }
                        w[j] = new Complex(w_j_re, w_j_im);
                    }
                });

                Parallel.ForEach(Partitioner.Create(rowIndex, columns), range =>
                {
                    int min = range.Item1, max = range.Item2, i, j;
                    Complex a;
                    Complex[] row;

                    for (i = min; i < max; ++i)
                    {
                        row = H[i];
                        double v_i_re = v[i].Real, v_i_im = -v[i].Imaginary;
                        for (j = 0; j < columns; ++j)
                        {
                            a = w[j];
                            row[j].Real -= (a.Real * v_i_re - a.Imaginary * v_i_im);
                            row[j].Imaginary -= (a.Imaginary * v_i_re + a.Real * v_i_im);
                        }
                    }
                });
            }
        }

        #endregion

        #region Sequential Householder reflections on Hessenberg matrices

        /// <summary>
        /// Apply Householder transformation to a matrix A, in Hessenberg form, using the column 'colIndex' taken from A.
        /// The Householder transformation matrix is stored in H.
        /// </summary>
        /// <param name="A"></param>
        /// <param name="H"></param>
        /// <param name="v"></param>
        /// <param name="colIndex"></param>
        /// <param name="columns"></param>
        /// <param name="rowIndex"></param>
        /// <param name="rows"></param>
        /// <param name="clean">If true, the matrix A will have its column 'colIndex' overwritten with 0's everywhere there should be 0</param>
        internal static void TransformHessenbergColumn(float[][] A, float[][] H, float[] v, int colIndex, int columns, int rowIndex, int rows, bool clean = false)
        {
            int r, i, j, next = rowIndex + 1;
            bool isLastRow = next >= rows;

            float[] row, row_next;
            float v_i, v_next, w_j;

            if (clean)
            {
                for (r = rowIndex + 2; r < rows; ++r)
                {
                    A[r][colIndex] = 0.0f;
                }
            }

            // Calculate the vertical projection vector, only the first 2 terms should be non-zero
            if (isLastRow)
            {
                v[rowIndex] = Constants.Sqrt2_f;
            }
            else
            {
                int end = next + 1;
                v[rowIndex] = A[rowIndex][colIndex];
                v[next] = A[next][colIndex];
                v[rowIndex] += Math.Sign(v[rowIndex]) * v.Norm(rowIndex, end);
                v.MultiplyOverwrite(Constants.Sqrt2_f / v.Norm(rowIndex, end), rowIndex, end);
            }

            // HH reflect on column c using vector v
            // Since there are only 2 rows, we fully unroll the inner loop for performance
            if (isLastRow)
            {
                row = A[rowIndex];
                v_i = v[rowIndex];
                v_i *= v_i;
                for (j = colIndex; j < columns; ++j)
                {
                    row[j] -= v_i * row[j];
                }
            }
            else
            {
                row = A[rowIndex];
                row_next = A[next];

                v_i = v[rowIndex];
                v_next = v[next];
                for (j = colIndex; j < columns; ++j)
                {
                    w_j = v_i * row[j] + v_next * row_next[j];
                    row[j] -= v_i * w_j;
                    row_next[j] -= v_next * w_j;
                }

                // The first term in row_next should be identically 0 as a result of the 
                // Householder reflection
                if (clean)
                {
                    row_next[colIndex] = 0.0f;
                }
            }

            // update the left orthogonal matrix, _U
            // Since there are only 2 rows, we fully unroll the inner loop for performance
            for (i = 0; i < rows; ++i)
            {
                row = H[i];
                if (isLastRow)
                {
                    v_i = v[rowIndex];
                    row[rowIndex] -= v_i * v_i * row[rowIndex];
                }
                else
                {
                    v_i = v[rowIndex];
                    v_next = v[next];

                    float q_i = v_i * row[rowIndex] + v_next * row[next];
                    row[rowIndex] -= q_i * v_i;
                    row[next] -= q_i * v_next;
                }
            }
        }
        internal static void TransformHessenbergColumn(double[][] A, double[][] H, double[] v, int colIndex, int columns, int rowIndex, int rows, bool clean = false)
        {
            int r, i, j, next = rowIndex + 1;
            bool isLastRow = next >= rows;

            double[] row, row_next;
            double v_i, v_next, w_j;

            if (clean)
            {
                for (r = rowIndex + 2; r < rows; ++r)
                {
                    A[r][colIndex] = 0.0;
                }
            }

            // Calculate the vertical projection vector, only the first 2 terms should be non-zero
            if (isLastRow)
            {
                v[rowIndex] = Constants.Sqrt2;
            }
            else
            {
                int end = next + 1;
                v[rowIndex] = A[rowIndex][colIndex];
                v[next] = A[next][colIndex];
                v[rowIndex] += Math.Sign(v[rowIndex]) * v.Norm(rowIndex, end);
                v.MultiplyOverwrite(Constants.Sqrt2 / v.Norm(rowIndex, end), rowIndex, end);
            }

            // HH reflect on column c using vector v
            // Since there are only 2 rows, we fully unroll the inner loop for performance
            if (isLastRow)
            {
                row = A[rowIndex];
                v_i = v[rowIndex];
                v_i *= v_i;
                for (j = colIndex; j < columns; ++j)
                {
                    row[j] -= v_i * row[j];
                }
            }
            else
            {
                row = A[rowIndex];
                row_next = A[next];

                v_i = v[rowIndex];
                v_next = v[next];
                for (j = colIndex; j < columns; ++j)
                {
                    w_j = v_i * row[j] + v_next * row_next[j];
                    row[j] -= v_i * w_j;
                    row_next[j] -= v_next * w_j;
                }

                // The first term in row_next should be identically 0 as a result of the 
                // Householder reflection
                if (clean)
                {
                    row_next[colIndex] = 0.0;
                }
            }

            // update the left orthogonal matrix, _U
            // Since there are only 2 rows, we fully unroll the inner loop for performance
            for (i = 0; i < rows; ++i)
            {
                row = H[i];
                if (isLastRow)
                {
                    v_i = v[rowIndex];
                    row[rowIndex] -= v_i * v_i * row[rowIndex];
                }
                else
                {
                    v_i = v[rowIndex];
                    v_next = v[next];

                    double q_i = v_i * row[rowIndex] + v_next * row[next];
                    row[rowIndex] -= q_i * v_i;
                    row[next] -= q_i * v_next;
                }
            }
        }
        internal static void TransformHessenbergColumn(decimal[][] A, decimal[][] H, decimal[] v, int colIndex, int columns, int rowIndex, int rows, bool clean = false)
        {
            int r, i, j, next = rowIndex + 1;
            bool isLastRow = next >= rows;

            decimal[] row, row_next;
            decimal v_i, v_next, w_j;

            if (clean)
            {
                for (r = rowIndex + 2; r < rows; ++r)
                {
                    A[r][colIndex] = 0.0m;
                }
            }

            // Calculate the vertical projection vector, only the first 2 terms should be non-zero
            if (isLastRow)
            {
                v[rowIndex] = Constants.Sqrt2_m;
            }
            else
            {
                int end = next + 1;
                v[rowIndex] = A[rowIndex][colIndex];
                v[next] = A[next][colIndex];
                v[rowIndex] += Math.Sign(v[rowIndex]) * v.Norm(rowIndex, end);
                v.MultiplyOverwrite(Constants.Sqrt2_m / v.Norm(rowIndex, end), rowIndex, end);
            }

            // HH reflect on column c using vector v
            // Since there are only 2 rows, we fully unroll the inner loop for performance
            if (isLastRow)
            {
                row = A[rowIndex];
                v_i = v[rowIndex];
                v_i *= v_i;
                for (j = colIndex; j < columns; ++j)
                {
                    row[j] -= v_i * row[j];
                }
            }
            else
            {
                row = A[rowIndex];
                row_next = A[next];

                v_i = v[rowIndex];
                v_next = v[next];
                for (j = colIndex; j < columns; ++j)
                {
                    w_j = v_i * row[j] + v_next * row_next[j];
                    row[j] -= v_i * w_j;
                    row_next[j] -= v_next * w_j;
                }

                // The first term in row_next should be identically 0 as a result of the 
                // Householder reflection
                if (clean)
                {
                    row_next[colIndex] = 0.0m;
                }
            }

            // update the left orthogonal matrix, _U
            // Since there are only 2 rows, we fully unroll the inner loop for performance
            for (i = 0; i < rows; ++i)
            {
                row = H[i];
                if (isLastRow)
                {
                    v_i = v[rowIndex];
                    row[rowIndex] -= v_i * v_i * row[rowIndex];
                }
                else
                {
                    v_i = v[rowIndex];
                    v_next = v[next];

                    decimal q_i = v_i * row[rowIndex] + v_next * row[next];
                    row[rowIndex] -= q_i * v_i;
                    row[next] -= q_i * v_next;
                }
            }
        }
        internal static void TransformHessenbergColumn(Complex[][] A, Complex[][] H, Complex[] v, int colIndex, int columns, int rowIndex, int rows, bool clean = false)
        {
            int next = rowIndex + 1, r, i, j;
            bool isLastRow = next == rows;
            Complex[] row, row_next;
            Complex v_i, v_next, a, b;

            if (clean)
            {
                for (r = rowIndex + 2; r < rows; ++r)
                {
                    A[r][colIndex].Real = 0.0;
                    A[r][colIndex].Imaginary = 0.0;
                }
            }

            // Calculate the vertical projection vector
            // This can likely be optimised further
            if (isLastRow)
            {
                v[rowIndex] = A[rowIndex][colIndex];
                Complex sign = Complex.Exp(Complex.I.Multiply(v[rowIndex].Argument()));
                v[rowIndex].IncrementBy(sign.Multiply(v[rowIndex].Modulus()));
                v[rowIndex].MultiplyEquals(Constants.Sqrt2 / v[rowIndex].Modulus());
            }
            else
            {
                v[rowIndex] = A[rowIndex][colIndex];
                v[next] = A[next][colIndex];
                Complex sign = Complex.Exp(Complex.I.Multiply(v[rowIndex].Argument()));
                v[rowIndex].IncrementBy(sign.Multiply(v.Norm(rowIndex, rows)));
                v.MultiplyOverwrite(Constants.Sqrt2 / v.Norm(rowIndex, rows), rowIndex, rows);
            }

            if (isLastRow)
            {
                row = A[rowIndex];

                double w_i_re = v[rowIndex].Real, w_i_im = v[rowIndex].Imaginary;
                double factor = 1.0 - (w_i_re * w_i_re + w_i_im * w_i_im);
                for (j = colIndex; j < columns; ++j)
                {
                    row[j].MultiplyEquals(factor);
                }
            }
            else
            {
                row = A[rowIndex];
                row_next = A[next];

                double v_i_re = v[rowIndex].Real, v_i_im = v[rowIndex].Imaginary;
                double v_next_re = v[next].Real, v_next_im = v[next].Imaginary;
                for (j = colIndex; j < columns; ++j)
                {
                    a = row[j];
                    b = row_next[j];
                    double w_j_re = v_i_re * a.Real + v_i_im * a.Imaginary + v_next_re * b.Real + v_next_im * b.Imaginary;
                    double w_j_im = -v_i_im * a.Real + v_i_re * a.Imaginary - v_next_im * b.Real + v_next_re * b.Imaginary;

                    row[j].Real -= (v_i_re * w_j_re - v_i_im * w_j_im);
                    row[j].Imaginary -= (v_i_im * w_j_re + v_i_re * w_j_im);

                    row_next[j].Real -= (v_next_re * w_j_re - v_next_im * w_j_im);
                    row_next[j].Imaginary -= (v_next_im * w_j_re + v_next_re * w_j_im);
                }

                // The first term in row_next should be identically 0 as a result of the 
                // Householder reflection
                if (clean)
                {
                    row_next[colIndex].Real = 0.0;
                    row_next[colIndex].Imaginary = 0.0;
                }
            }

            // update the left orthogonal matrix, _U
            // Since there are only 2 rows, we fully unroll the inner loop for performance
            for (i = 0; i < rows; ++i)
            {
                row = H[i];
                if (isLastRow)
                {
                    v_i = v[rowIndex];
                    double factor = 1.0 - (v_i.Real * v_i.Real + v_i.Imaginary * v_i.Imaginary);
                    row[rowIndex].MultiplyEquals(factor);
                }
                else
                {
                    v_i = v[rowIndex];
                    v_next = v[next];
                    a = row[rowIndex];
                    b = row[next];

                    double q_i_re = v_i.Real * a.Real - v_i.Imaginary * a.Imaginary + v_next.Real * b.Real - v_next.Imaginary * b.Imaginary;
                    double q_i_im = v_i.Imaginary * a.Real + v_i.Real * a.Imaginary + v_next.Imaginary * b.Real + v_next.Real * b.Imaginary;

                    row[rowIndex].Real -= (q_i_re * v_i.Real + q_i_im * v_i.Imaginary);
                    row[rowIndex].Imaginary -= (q_i_im * v_i.Real - q_i_re * v_i.Imaginary);

                    row[next].Real -= (q_i_re * v_next.Real + q_i_im * v_next.Imaginary);
                    row[next].Imaginary -= (q_i_im * v_next.Real - q_i_re * v_next.Imaginary);
                }
            }
        }

        /// <summary>
        /// Apply Householder transformation to a matrix A in Hessenberg form, using the column 'colIndex' taken from A.
        /// This method differs from the other TransformHessenbergColumn overload in that it does not waste time computing 
        /// and storing the Householder transofmration matrix. 
        /// </summary>
        /// <param name="A"></param>
        /// <param name="v"></param>
        /// <param name="colIndex"></param>
        /// <param name="columns"></param>
        /// <param name="rowIndex"></param>
        /// <param name="rows"></param>
        /// <param name="clean"></param>
        internal static void TransformHessenbergColumn(float[][] A, float[] v, int colIndex, int columns, int rowIndex, int rows, bool clean = false)
        {
            int r, j, next = rowIndex + 1;
            bool isLastRow = next >= rows;

            float[] row, row_next;
            float v_i, v_next, w_j;

            if (clean)
            {
                for (r = rowIndex + 2; r < rows; ++r)
                {
                    A[r][colIndex] = 0.0f;
                }
            }

            // Calculate the vertical projection vector, only the first 2 terms should be non-zero
            if (isLastRow)
            {
                v[rowIndex] = Constants.Sqrt2_f;
            }
            else
            {
                int end = next + 1;
                v[rowIndex] = A[rowIndex][colIndex];
                v[next] = A[next][colIndex];
                v[rowIndex] += Math.Sign(v[rowIndex]) * v.Norm(rowIndex, end);
                v.MultiplyOverwrite(Constants.Sqrt2_f / v.Norm(rowIndex, end), rowIndex, end);
            }

            // HH reflect on column c using vector v
            // Since there are only 2 rows, we fully unroll the inner loop for performance
            if (isLastRow)
            {
                row = A[rowIndex];
                v_i = v[rowIndex];
                v_i *= v_i;
                for (j = colIndex; j < columns; ++j)
                {
                    row[j] -= v_i * row[j];
                }
            }
            else
            {
                row = A[rowIndex];
                row_next = A[next];

                v_i = v[rowIndex];
                v_next = v[next];
                for (j = colIndex; j < columns; ++j)
                {
                    w_j = v_i * row[j] + v_next * row_next[j];
                    row[j] -= v_i * w_j;
                    row_next[j] -= v_next * w_j;
                }

                // The first term in row_next should be identically 0 as a result of the 
                // Householder reflection
                row_next[colIndex] = 0.0f;
            }
        }
        internal static void TransformHessenbergColumn(double[][] A, double[] v, int colIndex, int columns, int rowIndex, int rows, bool clean = false)
        {
            int r, j, next = rowIndex + 1;
            bool isLastRow = next >= rows;

            double[] row, row_next;
            double v_i, v_next, w_j;

            if (clean)
            {
                for (r = rowIndex + 2; r < rows; ++r)
                {
                    A[r][colIndex] = 0.0;
                }
            }

            // Calculate the vertical projection vector, only the first 2 terms should be non-zero
            if (isLastRow)
            {
                v[rowIndex] = Constants.Sqrt2;
            }
            else
            {
                int end = next + 1;
                v[rowIndex] = A[rowIndex][colIndex];
                v[next] = A[next][colIndex];
                v[rowIndex] += Math.Sign(v[rowIndex]) * v.Norm(rowIndex, end);
                v.MultiplyOverwrite(Constants.Sqrt2 / v.Norm(rowIndex, end), rowIndex, end);
            }

            // HH reflect on column c using vector v
            // Since there are only 2 rows, we fully unroll the inner loop for performance
            if (isLastRow)
            {
                row = A[rowIndex];
                v_i = v[rowIndex];
                v_i *= v_i;
                for (j = colIndex; j < columns; ++j)
                {
                    row[j] -= v_i * row[j];
                }
            }
            else
            {
                row = A[rowIndex];
                row_next = A[next];

                v_i = v[rowIndex];
                v_next = v[next];
                for (j = colIndex; j < columns; ++j)
                {
                    w_j = v_i * row[j] + v_next * row_next[j];
                    row[j] -= v_i * w_j;
                    row_next[j] -= v_next * w_j;
                }

                // The first term in row_next should be identically 0 as a result of the 
                // Householder reflection
                row_next[colIndex] = 0.0;
            }
        }
        internal static void TransformHessenbergColumn(decimal[][] A, decimal[] v, int colIndex, int columns, int rowIndex, int rows, bool clean = false)
        {
            int r, j, next = rowIndex + 1;
            bool isLastRow = next >= rows;

            decimal[] row, row_next;
            decimal v_i, v_next, w_j;

            if (clean)
            {
                for (r = rowIndex + 2; r < rows; ++r)
                {
                    A[r][colIndex] = 0.0m;
                }
            }

            // Calculate the vertical projection vector, only the first 2 terms should be non-zero
            if (isLastRow)
            {
                v[rowIndex] = Constants.Sqrt2_m;
            }
            else
            {
                int end = next + 1;
                v[rowIndex] = A[rowIndex][colIndex];
                v[next] = A[next][colIndex];
                v[rowIndex] += Math.Sign(v[rowIndex]) * v.Norm(rowIndex, end);
                v.MultiplyOverwrite(Constants.Sqrt2_m / v.Norm(rowIndex, end), rowIndex, end);
            }

            // HH reflect on column c using vector v
            // Since there are only 2 rows, we fully unroll the inner loop for performance
            if (isLastRow)
            {
                row = A[rowIndex];
                v_i = v[rowIndex];
                v_i *= v_i;
                for (j = colIndex; j < columns; ++j)
                {
                    row[j] -= v_i * row[j];
                }
            }
            else
            {
                row = A[rowIndex];
                row_next = A[next];

                v_i = v[rowIndex];
                v_next = v[next];
                for (j = colIndex; j < columns; ++j)
                {
                    w_j = v_i * row[j] + v_next * row_next[j];
                    row[j] -= v_i * w_j;
                    row_next[j] -= v_next * w_j;
                }

                // The first term in row_next should be identically 0 as a result of the 
                // Householder reflection
                row_next[colIndex] = 0.0m;
            }
        }
        internal static void TransformHessenbergColumn(Complex[][] A, Complex[] v, int colIndex, int columns, int rowIndex, int rows, bool clean = false)
        {
            int next = rowIndex + 1, r, j;
            bool isLastRow = next == rows;
            Complex[] row, row_next;
            Complex a, b;

            if (clean)
            {
                for (r = rowIndex + 2; r < rows; ++r)
                {
                    A[r][colIndex].Real = 0.0;
                    A[r][colIndex].Imaginary = 0.0;
                }
            }

            // Calculate the vertical projection vector
            // This can likely be optimised further
            if (isLastRow)
            {
                v[rowIndex] = A[rowIndex][colIndex];
                Complex sign = Complex.Exp(Complex.I.Multiply(v[rowIndex].Argument()));
                v[rowIndex].IncrementBy(sign.Multiply(v[rowIndex].Modulus()));
                v[rowIndex].MultiplyEquals(Constants.Sqrt2 / v[rowIndex].Modulus());
            }
            else
            {
                v[rowIndex] = A[rowIndex][colIndex];
                v[next] = A[next][colIndex];
                Complex sign = Complex.Exp(Complex.I.Multiply(v[rowIndex].Argument()));
                v[rowIndex].IncrementBy(sign.Multiply(v.Norm(rowIndex, rows)));
                v.MultiplyOverwrite(Constants.Sqrt2 / v.Norm(rowIndex, rows), rowIndex, rows);
            }

            if (isLastRow)
            {
                row = A[rowIndex];

                double w_i_re = v[rowIndex].Real, w_i_im = v[rowIndex].Imaginary;
                double factor = 1.0 - (w_i_re * w_i_re + w_i_im * w_i_im);
                for (j = colIndex; j < columns; ++j)
                {
                    row[j].MultiplyEquals(factor);
                }
            }
            else
            {
                row = A[rowIndex];
                row_next = A[next];

                double v_i_re = v[rowIndex].Real, v_i_im = v[rowIndex].Imaginary;
                double v_next_re = v[next].Real, v_next_im = v[next].Imaginary;
                for (j = colIndex; j < columns; ++j)
                {
                    a = row[j];
                    b = row_next[j];
                    double w_j_re = v_i_re * a.Real + v_i_im * a.Imaginary + v_next_re * b.Real + v_next_im * b.Imaginary;
                    double w_j_im = -v_i_im * a.Real + v_i_re * a.Imaginary - v_next_im * b.Real + v_next_re * b.Imaginary;

                    row[j].Real -= (v_i_re * w_j_re - v_i_im * w_j_im);
                    row[j].Imaginary -= (v_i_im * w_j_re + v_i_re * w_j_im);

                    row_next[j].Real -= (v_next_re * w_j_re - v_next_im * w_j_im);
                    row_next[j].Imaginary -= (v_next_im * w_j_re + v_next_re * w_j_im);
                }

                // The first term in row_next should be identically 0 as a result of the 
                // Householder reflection
                row_next[colIndex].Real = 0.0;
                row_next[colIndex].Imaginary = 0.0;
            }
        }

        #endregion 
    }
}
