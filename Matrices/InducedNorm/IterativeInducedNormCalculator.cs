﻿using LinearNet.Providers;
using LinearNet.Providers.LinearAlgebra;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Matrices.InducedNorm
{
    public class IterativeInducedNormCalculator
    {
        private readonly double _stepSize = 1.0;

        private readonly double[][] _A;
        private readonly int _p;

        private readonly double[] _x, _dx, _Ax;
        private readonly int _rows, _columns;

        private readonly NativeDoubleProvider _blas;

        public IterativeInducedNormCalculator(double[][] A, int p)
        {
            _A = A;
            _p = p;

            _rows = A.Length;
            _columns = A[0].Length;

            _x = new double[_columns];
            _dx = new double[_columns]; // derivative
            _Ax = new double[_rows];

            _blas = new NativeDoubleProvider();
        }

        public double Calculate()
        {
            Initialise();

            // Termination conditions
            double precision = Precision.DOUBLE_PRECISION / 1e2;
            int maxIterations = 1000;

            double norm = 0.0;
            for (int i = 0; i < maxIterations; ++i)
            {
                Iteration();
                Normalise();

                double newNorm = EvaluateNorm();
                if (Math.Abs(newNorm - norm) < precision)
                {
                    return newNorm;
                }
                norm = newNorm;
                //Debug.WriteLine(i + "\t" + norm);
            }
            return norm;
        }
        private void Initialise()
        {
            double x = Math.Pow(1.0 / _columns, 1.0 / _p);
            for (int i = 0; i < _columns; ++i)
            {
                _x[i] = x;
            }
        }
        private void Iteration()
        {
            CalculateDerivative();
            _blas.AXPY(_x, _dx, _stepSize, 0, _columns);
        }
        private void Normalise()
        {
            double norm = _x.Norm(_p);
            if (norm != 0)
            {
                _blas.SCAL(_x, 1.0 / norm, 0, _columns);
            }
        }
        private double EvaluateNorm()
        {
            MultiplyAx();
            return _Ax.Norm(_p) / _x.Norm(_p);
        }
        private void MultiplyAx()
        {
            for (int i = 0; i < _rows; ++i)
            {
                _Ax[i] = _blas.DOT(_A[i], _x, 0, _columns);
            }
        }

        private void CalculatePseudoDerivative()
        {
            double e = 1e-8;

            for (int i = 0; i < _columns; ++i)
            {
                double x_i = _x[i];

                _x[i] = x_i + e;
                double f_plus = Math.Log(EvaluateNorm());

                _x[i] = x_i - e;
                double f_minus = Math.Log(EvaluateNorm());

                _x[i] = x_i;

                _dx[i] = (f_plus - f_minus) / (2 * e);
            }
        }
        private void CalculateDerivative()
        {
            MultiplyAx();

            double norm_Ax = Math.Pow(_Ax.Norm(_p), _p);
            double norm_x = Math.Pow(_x.Norm(_p), _p);

            //Debug.WriteLine("norm_Ax:\t" + norm_Ax);

            int i, j;
            for (j = 0; j < _columns; ++j)
            {
                double dN_dx = 0.0;

                // Part 1 - derivative of ln(norm(Ax)^p) ~ d/dx(norm(Ax)^p) / norm(Ax)^p
                for (i = 0; i < _rows; ++i)
                {
                    if (_p % 2 == 0 || _Ax[i] >= 0)
                    {
                        dN_dx += Math.Pow(_Ax[i], _p - 1) * _A[i][j];
                    }
                    else
                    {
                        dN_dx -= Math.Pow(_Ax[i], _p - 1) * _A[i][j];
                    }
                }
                //Debug.WriteLine("dN_dx before division[" + i + "]\t" + dN_dx);
                dN_dx /= norm_Ax;
                //Debug.WriteLine("dN_dx after division[" + i + "]\t" + dN_dx);


                // Part 2 - derivative of ln(norm(x)^p) ~ d/dx(norm(x)^p) / norm(x)^p
                if (_p % 2 == 0 || _x[j] >= 0)
                {
                    dN_dx -= Math.Pow(_x[j], _p - 1) / norm_x;
                }
                else
                {
                    dN_dx += Math.Pow(_x[j], _p - 1) / norm_x;
                }

                _dx[j] = dN_dx;
            }
        }
    }
}
