﻿using LinearNet.Providers;
using System;
using System.Collections.Generic;
using System.Text;

namespace LinearNet.Matrices.Inversion
{
    internal static class GaussianEliminationFunctions
    {
        internal static void Invert<T>(T[][] A, T[][] inv, IDenseBLAS1<T> blas, int n, Func<T, T> Inverse, Func<T, double> Norm, Action<T[], int> ShiftBitsLeft, Func<T, int, T> ShiftBitRight) where T : new()
        {
            int i_max, i, j, k;
            IProvider<T> prov = blas.Provider;
            T alpha, zero = prov.Zero, one = prov.One;

            // shape into correct order
            double order = 0;
            for (i = 0; i < n; ++i)
            {
                T[] A_i = A[i];
                for (j = 0; j < n; ++j)
                {
                    order += Math.Log(Norm(A_i[j]) + 0.01);
                }
            }

            int shift = (int)Math.Round((order / (n * n)) / Math.Log(2));

            // x_norm stores the values of x after order adjustment
            T[][] A_norm = new T[n][];
            for (i = 0; i < n; i++)
            {
                T[] A_norm_i = A[i].Copy();
                ShiftBitsLeft(A_norm_i, shift);
                A_norm[i] = A_norm_i;
            }

            // Set the inverse to identity
            for (i = 0; i < n; ++i)
            {
                T[] row = inv[i];
                for (j = 0; j < n; ++j)
                {
                    row[j] = zero;
                }
                row[i] = one;
            }

            double max;
            for (k = 0; k < n - 1; k++)
            {
                // find i_max = argmax(x[i,k] for i = k, ..., n) - for numerical stability
                i_max = k + 1;
                max = Norm(A_norm[i_max][k]);
                for (i = k + 2; i < n; i++)
                {
                    double norm = Norm(A_norm[i][k]);
                    if (norm > max)
                    {
                        i_max = i;
                        max = norm;
                    }
                }

                // swap row k with row i_max
                T[] temp = A_norm[i_max];
                A_norm[i_max] = A_norm[k];
                A_norm[k] = temp;

                temp = inv[i_max];
                inv[i_max] = inv[k];
                inv[k] = temp;

                // for each row below k, i.e. for i = k + 1 : n
                T[] A_norm_k = A_norm[k], inv_k = inv[k];
                T factor = Inverse(A_norm_k[k]);
                for (i = k + 1; i < n; i++)
                {
                    T[] A_norm_i = A_norm[i];
                    alpha = prov.Negate(prov.Multiply(A_norm_i[k], factor));

                    blas.AXPY(A_norm_i, A_norm_k, alpha, k + 1, n);
                    blas.AXPY(inv[i], inv_k, alpha, 0, n);
                    A_norm_i[k] = zero;
                }
            }

            // going back upwards
            for (k = n - 1; k >= 0; k--)
            {
                T[] A_norm_k = A_norm[k], inv_k = inv[k];
                T factor = Inverse(A_norm_k[k]);
                for (i = k - 1; i >= 0; i--)
                {
                    alpha = prov.Negate(prov.Multiply(A_norm[i][k], factor));
                    blas.AXPY(A_norm[i], A_norm_k, alpha, i + 1, n);
                    blas.AXPY(inv[i], inv_k, alpha, 0, n);
                }
            }

            // normalise - with order
            for (k = 0; k < n; k++)
            {
                alpha = Inverse(ShiftBitRight(A_norm[k][k], shift));
                blas.SCAL(inv[k], alpha, 0, n);
            }
        }
    }
}
