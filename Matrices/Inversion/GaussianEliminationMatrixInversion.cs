﻿using LinearNet.Providers;
using System;
using System.Collections.Generic;
using System.Text;

namespace LinearNet.Matrices.Inversion
{
    public class GaussianEliminationMatrixInversion<T> : IMatrixInversion<T>
    {
        private readonly IDenseBLAS1<T> _blas1;

        internal GaussianEliminationMatrixInversion(IDenseBLAS1<T> blas)
        {
            _blas1 = blas;
        }

        public void Invert(T[][] matrix, T[][] inverse, int n)
        {
            int /*i_max, */ i, k;
            IProvider<T> prov = _blas1.Provider;
            T alpha, zero = prov.Zero;

            T[][] inv = MatrixInternalExtensions.JIdentity(zero, prov.One, n);
            //double max;
            for (k = 0; k < n - 1; k++)
            {
                // find i_max = argmax(x[i,k] for i = k, ..., n) - for numerical stability
                /*
                i_max = k + 1;
                max = Norm(matrix[i_max][k]);
                for (i = k + 2; i < n; i++)
                {
                    double norm = Norm(matrix[i][k]);
                    if (norm > max)
                    {
                        i_max = i;
                        max = norm;
                    }
                }

                // swap row k with row i_max
                T[] temp = A_norm[i_max];
                A_norm[i_max] = A_norm[k];
                A_norm[k] = temp;

                temp = inv[i_max];
                inv[i_max] = inv[k];
                inv[k] = temp;
                */


                // for each row below k, i.e. for i = k + 1 : n
                T[] mat_k = matrix[k], inv_k = inv[k];
                T factor = prov.Divide(prov.One, mat_k[k]);
                for (i = k + 1; i < n; i++)
                {
                    T[] mat_i = matrix[i];
                    alpha = prov.Negate(prov.Multiply(mat_i[k], factor));

                    _blas1.AXPY(mat_i, mat_k, alpha, k + 1, n);
                    _blas1.AXPY(inv[i], inv_k, alpha, 0, n);
                    mat_i[k] = zero;
                }
            }

            // going back upwards
            for (k = n - 1; k >= 0; k--)
            {
                T[] mat_k = matrix[k], inv_k = inv[k];
                T factor = prov.Divide(prov.One, mat_k[k]);
                for (i = k - 1; i >= 0; i--)
                {
                    alpha = prov.Negate(prov.Multiply(matrix[i][k], factor));
                    _blas1.AXPY(matrix[i], mat_k, alpha, i + 1, n);
                    _blas1.AXPY(inv[i], inv_k, alpha, 0, n);
                }
            }

            // normalise - with order
            for (k = 0; k < n; k++)
            {
                alpha = prov.Divide(prov.One, matrix[k][k]);
                _blas1.SCAL(inv[k], alpha, 0, n);
            }
        }
    }
}
