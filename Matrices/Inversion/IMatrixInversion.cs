﻿using LinearNet.Structs;
using System;
using System.Collections.Generic;
using System.Text;

namespace LinearNet.Matrices.Inversion
{
    public interface IMatrixInversion<T>
    {
        void Invert(T[][] matrix, T[][] inverse, int n);
    }
}
