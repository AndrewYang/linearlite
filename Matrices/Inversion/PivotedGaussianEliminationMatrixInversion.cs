﻿using LinearNet.Providers;
using LinearNet.Providers.LinearAlgebra;
using LinearNet.Structs;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace LinearNet.Matrices.Inversion
{
    public class PivotedGaussianEliminationMatrixInversion : 
        IMatrixInversion<float>,
        IMatrixInversion<double>,
        IMatrixInversion<decimal>,
        IMatrixInversion<Complex>
    {
        public void Invert(float[][] matrix, float[][] inverse, int n)
        {
            var blas1 = new NativeFloatProvider();
            GaussianEliminationFunctions.Invert(
                           matrix,
                           inverse,
                           blas1,
                           n,
                           x => 1.0f / x,
                           x => Math.Abs(x),
                           (x, s) =>
                           {
                               if (s > 0) FloatProvider.dSCAL(x, 1.0 / (1 << s), 0, x.Length);
                               else if (s < 0) FloatProvider.dSCAL(x, 1 << Math.Abs(s), 0, x.Length);
                           },
                           (x, s) =>
                           {
                               if (s > 0) return (1 << s) * x;
                               if (s < 0) return 1.0F / (1 << Math.Abs(s)) * x;
                               return x;
                           });
        }

        public void Invert(double[][] matrix, double[][] inv, int n)
        {
            int i_max, i, j, k;
            double order, max, alpha;

            // shape into correct order
            order = 0;
            for (i = 0; i < n; i++)
            {
                double[] A_i = matrix[i];
                for (j = 0; j < n; j++)
                {
                    order += Math.Log(Math.Abs(A_i[j]) + 0.01);
                }
            }
            order = Math.Exp(order / (n * n));

            // x_norm stores the values of x after order adjustment
            double[][] A_norm = MatrixInternalExtensions.JZero(n, n);
            for (i = 0; i < n; i++)
            {
                double[] A_norm_i = A_norm[i], A_i = matrix[i];
                for (j = 0; j < n; j++)
                {
                    A_norm_i[j] = A_i[j] / order;
                }
            }

            // Set inv to identity
            for (i = 0; i < n; i++)
            {
                double[] inv_i = inv[i];
                for (j = 0; j < n; j++)
                {
                    inv_i[j] = 0.0;
                }
                inv_i[i] = 1.0;
            }

            for (k = 0; k < n - 1; k++)
            {
                // find i_max = argmax(x[i,k] for i = k, ..., n) - for numerical stability
                max = Math.Abs(A_norm[k][k]);
                i_max = k;
                for (i = k + 1; i < n; i++)
                {
                    double abs = Math.Abs(A_norm[i][k]);
                    if (abs > max)
                    {
                        i_max = i;
                        max = abs;
                    }
                }

                // swap row k with row i_max
                double[] temp = A_norm[i_max];
                A_norm[i_max] = A_norm[k];
                A_norm[k] = temp;

                temp = inv[i_max];
                inv[i_max] = inv[k];
                inv[k] = temp;

                // for each row below k, i.e. for i = k + 1 : n
                double[] A_norm_k = A_norm[k], inv_k = inv[k];
                for (i = k + 1; i < n; i++)
                {
                    double[] A_norm_i = A_norm[i], inv_i = inv[i];
                    alpha = A_norm_i[k] / A_norm_k[k];

                    for (j = k + 1; j < n; j++)
                    {
                        A_norm_i[j] -= A_norm_k[j] * alpha;
                    }
                    for (j = 0; j < n; j++)
                    {
                        inv_i[j] -= inv_k[j] * alpha;
                    }
                    A_norm_i[k] = 0;
                }
            }

            // going back upwards
            for (k = n - 1; k >= 0; k--)
            {
                double[] A_norm_k = A_norm[k], inv_k = inv[k];
                for (i = k - 1; i >= 0; i--)
                {
                    double[] A_norm_i = A_norm[i], inv_i = inv[i];
                    alpha = A_norm_i[k] / A_norm_k[k];
                    for (j = i + 1; j < n; j++)
                    {
                        A_norm_i[j] -= A_norm_k[j] * alpha;
                    }
                    for (j = 0; j < n; j++)
                    {
                        inv_i[j] -= inv_k[j] * alpha;
                    }
                }
            }

            // normalise - with order
            for (k = 0; k < n; k++)
            {
                alpha = A_norm[k][k] * order;

                double[] inv_k = inv[k];
                for (i = 0; i < n; i++)
                    inv_k[i] /= alpha;
            }
        }

        public void Invert(decimal[][] A, decimal[][] inverse, int n)
        {
            IDenseBLAS1<decimal> blas = new NativeDecimalProvider();
            GaussianEliminationFunctions.Invert(
                        A,
                        inverse,
                        blas,
                        n,
                        x => 1.0m / x,
                        x => (double)Math.Abs(x),
                        (x, s) =>
                        {
                            if (s > 0) blas.SCAL(x, 1.0M / (1 << s), 0, x.Length);
                            else if (s < 0) blas.SCAL(x, 1 << Math.Abs(s), 0, x.Length);  // Shift bits left
                        },
                        (x, s) =>
                        {
                            if (s > 0) return (1 << s) * x;
                            if (s < 0) return 1.0M / (1 << Math.Abs(s)) * x;
                            return x;
                        });
        }

        public void Invert(Complex[][] A, Complex[][] inverse, int n)
        {
            var blas = new NativeComplexProvider();
            GaussianEliminationFunctions.Invert(
                A,
                inverse,
                blas,
                n,
                x => x.MultiplicativeInverse(),
                x => x.Modulus(),
                (x, s) => // Shift bits left
                {
                    if (s > 0) ComplexProvider.dSCAL(x, 1.0 / (1 << s), 0, x.Length);
                    else if (s < 0) ComplexProvider.dSCAL(x, 1 << Math.Abs(s), 0, x.Length);
                },
                (x, s) =>
                {
                    if (s > 0) return x.Multiply(1 << s);
                    if (s < 0) return x.Multiply(1.0 / (1 << Math.Abs(s)));
                    return x;
                });
        }
    }
}
