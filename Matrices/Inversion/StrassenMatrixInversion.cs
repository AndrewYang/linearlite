﻿using LinearNet.Helpers;
using LinearNet.Matrices.Multiplication;
using LinearNet.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Matrices.Inversion
{
    public class StrassenMatrixInversion<T> : IMatrixInversion<T> where T : new()
    {
        private readonly int _threshold;
        private readonly VariableSizeStrassenMultiplication<T> _strassenMultiply;
        private readonly IDenseBLAS2<T> _blas2;
        private readonly IMatrixInversion<T> _baseAlgo;

        private readonly Dictionary<int, T[][]> _temps, _M_1s, _M_2s, _M_3s, _M_6s;

        public StrassenMatrixInversion(VariableSizeStrassenMultiplication<T> multiplyAlgo, IMatrixInversion<T> baseAlgo, int size, int threshold = 32)
        {
            _strassenMultiply = multiplyAlgo;
            _blas2 = multiplyAlgo.BLAS2;
            _baseAlgo = baseAlgo;
            _threshold = threshold;

            _temps = new Dictionary<int, T[][]>();
            _M_1s = new Dictionary<int, T[][]>();
            _M_2s = new Dictionary<int, T[][]>();
            _M_3s = new Dictionary<int, T[][]>();
            _M_6s = new Dictionary<int, T[][]>();

            InitWorkspaces(size);
        }
        internal StrassenMatrixInversion(IDenseBLAS1<T> blas1, IDenseBLAS2<T> blas2, IMatrixInversion<T> baseAlgo, int size, int threshold = 32)
        {
            var baseMatrixMultiply = new NaiveMatrixMultiplication<T>(blas1, MultiplicationMode.OPTIMIZE_SPEED);
            _strassenMultiply = new VariableSizeStrassenMultiplication<T>(blas1, blas2, baseMatrixMultiply, size, size, size, true, threshold);
            _blas2 = blas2;
            _baseAlgo = baseAlgo;
            _threshold = threshold;

            _temps = new Dictionary<int, T[][]>();
            _M_1s = new Dictionary<int, T[][]>();
            _M_2s = new Dictionary<int, T[][]>();
            _M_3s = new Dictionary<int, T[][]>();
            _M_6s = new Dictionary<int, T[][]>();

            InitWorkspaces(size);
        }
        public StrassenMatrixInversion(int size, int threshold = 32)
        {
            if (!ProviderFactory.TryGetDefaultBLAS1Provider(out IDenseBLAS1<T> blas1))
            {
                throw new NotImplementedException("There are no default providers specified for type " + typeof(T) + ". Please use the literal constructor.");
            }
            if (!ProviderFactory.TryGetDefaultBLAS2Provider(out IDenseBLAS2<T> blas2))
            {
                throw new NotImplementedException("There are no default providers specified for type " + typeof(T) + ". Please use the literal constructor.");
            }
            if (!ProviderFactory.TryGetDefaultMatrixInversion(out IMatrixInversion<T> baseAlgo))
            {
                throw new NotImplementedException("There are no default providers specified for type " + typeof(T) + ". Please use the literal constructor.");
            }

            var baseMatrixMultiply = new NaiveMatrixMultiplication<T>(blas1, MultiplicationMode.OPTIMIZE_SPEED);
            _strassenMultiply = new VariableSizeStrassenMultiplication<T>(blas1, blas2, baseMatrixMultiply, size, size, size, true, threshold);
            _blas2 = blas2;
            _baseAlgo = baseAlgo;
            _threshold = threshold;

            _temps = new Dictionary<int, T[][]>();
            _M_1s = new Dictionary<int, T[][]>();
            _M_2s = new Dictionary<int, T[][]>();
            _M_3s = new Dictionary<int, T[][]>();
            _M_6s = new Dictionary<int, T[][]>();

            InitWorkspaces(size);
        }

        private void InitWorkspaces(int size)
        {
            while (!use_naive_algorithm(size))
            {
                size /= 2;
                _temps[size] = MatrixInternalExtensions.JMatrix<T>(size, size);
                _M_1s[size] = MatrixInternalExtensions.JMatrix<T>(size, size);
                _M_2s[size] = MatrixInternalExtensions.JMatrix<T>(size, size);
                _M_3s[size] = MatrixInternalExtensions.JMatrix<T>(size, size);
                _M_6s[size] = MatrixInternalExtensions.JMatrix<T>(size, size);
            }
        }
        /// <summary>
        /// Use Strassen's algorithm to invert a square matrix A, as per Strassen's original paper, 
        /// available at https://doi.org/10.1007/BF02165411.
        /// 
        /// Algorithm overview: 
        /// M_1 := inv(A_11)
        /// M_2 := A_21 * M_1
        /// M_3 := M_1 * A_12
        /// M_4 := A_21 * M_3
        /// M_5 := M_4 - A_22
        /// M_6 := inv(M_5)
        /// 
        /// C_12 := M_3 * M_6
        /// C_21 := M_6 * M_2
        /// M_7 := M_3 * C_21
        /// C_11 := M_2 - M_7
        /// C_22 := -M_6
        /// 
        /// To save space, only M_2, M_3, M_6 are kept, the other matrices are discarded at least step
        /// </summary>
        /// <param name="A"></param>
        /// <param name="result"></param>
        public void Invert(T[][] A, T[][] result, int n)
        {
            if (A == null || result == null)
            {
                throw new ArgumentNullException();
            }
            if (A.Length < n || A[0].Length < n || result.Length < n || result[0].Length < n)
            {
                throw new InvalidOperationException();
            }

            invert_unsafe(A, result, n);
        }

        private bool use_naive_algorithm(int n)
        {
            return n < _threshold;
        }
        private void invert_unsafe(T[][] A, T[][] result, int n)
        {
            // Revert to naive inversion algorithm for small matrices
            if (use_naive_algorithm(n))
            {
                _baseAlgo.Invert(A, result, n);
                return;
            }

            int half_n = n / 2;
            Size3 size = new Size3(half_n, half_n, half_n);

            T[][] temp = _temps[half_n];
            T[][] M_1 = _M_1s[half_n];
            T[][] M_2 = _M_2s[half_n];
            T[][] M_3 = _M_3s[half_n];
            T[][] M_6 = _M_6s[half_n];

            // M_1 := inv(A_11)
            _blas2.Copy(temp, A, half_n, half_n);
            invert_unsafe(temp, M_1, half_n);

            // M_2 := A_21 * M_1
            _blas2.Copy(temp, A, half_n, 0, half_n, half_n);
            _strassenMultiply.Multiply(temp, M_1, M_2, size, false);

            // M_3 := M_1 * A_12
            _blas2.Copy(temp, A, 0, half_n, half_n, half_n);
            _strassenMultiply.Multiply(M_1, temp, M_3, size, false);

            // M_4 := A_21 * M_3 (stored in M_6)
            _blas2.Copy(temp, A, half_n, 0, half_n, half_n);
            _strassenMultiply.Multiply(temp, M_3, M_6, size, false);

            // M_5 := M_4 - A_22 (stored in M_6)
            _blas2.Copy(temp, A, half_n, half_n, half_n, half_n);
            _blas2.Decrement(M_6, temp, 0, 0, half_n, half_n);

            // M_6 := inv(M_5), store in M_6 (overwrite and discard M_4, M_5) 
            _blas2.Copy(temp, M_6, half_n, half_n);
            invert_unsafe(temp, M_6, half_n);

            // C_12 := M_3 * M_6
            _strassenMultiply.Multiply(M_3, M_6, temp, size, false);
            _blas2.Copy(result, temp, 0, half_n, 0, 0, half_n, half_n);

            // C_21 := M_6 * M_2
            _strassenMultiply.Multiply(M_6, M_2, temp, size, false);
            _blas2.Copy(result, temp, half_n, 0, 0, 0, half_n, half_n);

            // M_7 := M_3 * C_21 (store in M_2, which is no longer needed)
            _strassenMultiply.Multiply(M_3, temp, M_2, size, false);

            // C_11 := M_1 - M_7 (which is stored in M_2)
            _blas2.Subtract(M_1, M_2, result, 0, 0, 0, 0, half_n, half_n);

            // C_22 := -M_6
            _blas2.Negate(M_6, result, 0, 0, half_n, half_n, half_n, half_n);
        }
    }
}
