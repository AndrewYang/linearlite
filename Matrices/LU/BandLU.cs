﻿using LinearNet.Providers;
using LinearNet.Structs;

namespace LinearNet.Matrices.LU
{
    public class BandLU<T> : LU<T> where T : new()
    {
        private readonly int _dim;
        private readonly BandMatrix<T> _LU;
        private readonly IProvider<T> _provider;
        private readonly IDenseBLAS1<T> _blas1;

        public override Matrix<T> L
        {
            get
            {
                T[] diagonal = RectangularVector.Repeat(_provider.One, _dim);
                return new BandMatrix<T>(_dim, _dim, new T[0][], diagonal, _LU.LowerBands);
            }
        }

        public override Matrix<T> U
        {
            get
            {
                return new BandMatrix<T>(_dim, _dim, _LU.UpperBands, _LU.Diagonal, new T[0][]);
            }
        }

        internal BandLU(BandMatrix<T> LU, IDenseBLAS1<T> blas)
        {
            _LU = LU;
            _blas1 = blas;
            _provider = blas.Provider;
            _dim = _LU.Rows;
        }

        public override T Determinant()
        {
            return _provider.Product(_LU.Diagonal);
        }

        public override T LogDeterminant()
        {
            return _provider.LogProduct(_LU.Diagonal);
        }

        public override void Solve(T[] b, T[] x)
        {
            BandMatrix<T> _L = L as BandMatrix<T>, _U = U as BandMatrix<T>;

            _L.SolveTriangular(b, x, _blas1, true, false);
            _U.SolveTriangular(x, x, _blas1, false, false);
        }
    }
}
