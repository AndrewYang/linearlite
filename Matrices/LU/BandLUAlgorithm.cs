﻿using LinearNet.Global;
using LinearNet.Providers;
using LinearNet.Structs;
using System;

namespace LinearNet.Matrices.LU
{
    public class BandLUAlgorithm<T> : LUAlgorithm<T> where T : new()
    {
        private readonly IProvider<T> _provider;
        private readonly IDenseBLAS1<T> _blas1;

        internal BandLUAlgorithm(IDenseBLAS1<T> blas)
        {
            _provider = blas.Provider;
            _blas1 = blas;
        }

        internal LU<T> Decompose(BandMatrix<T> A)
        {
            if (A is null)
            {
                throw new ArgumentNullException(nameof(A));
            }
            if (!A.IsSquare)
            {
                throw new ArgumentOutOfRangeException(nameof(A), ExceptionMessages.MatrixNotSquare);
            }

            T[][] lower = A.LowerBands, 
                upper = A.UpperBands;
            T[] diag = A.Diagonal;
            int dim = A.Rows, 
                lowerBandwidth = A.LowerBandwidth, 
                upperBandwidth = A.UpperBandwidth;

            // Initialize workspaces for storing a row and column within the band matrix, respectively
            T[] row_ws = new T[dim], col_ws = new T[dim];

            int i, j;
            for (i = 0; i < dim; i++)
            {
                // Calculate a row of the upper triangular factor U
                // Only go up to i + upperBandwidth + 1
                int max = Math.Min(dim, i + upperBandwidth + 1);
                for (j = i; j < max; j++)
                {
                    // Compute A[i, 0:i - 1] * A[0:i - 1, j]
                    // However since the matrix is banded, this is 
                    // equivalent to A[i, k:i - 1] * A[k:i - 1, j] where 
                    // k = i - min(upperBandwidth, lowerBandwidth)
                    
                    int start = Math.Max(0, i - Math.Min(upperBandwidth - (j - i), lowerBandwidth));

                    // Store A[i, k:i] into row workspace, A[k:i, j] into 
                    // column workspace
                    for (int k = start; k < i; ++k)
                    {
                        row_ws[k] = lower[i - k - 1][k];
                        col_ws[k] = upper[j - k - 1][k];
                    }

                    // Store the entry from U factor into the upper
                    // or diagonal band, based on (i, j)
                    T dot = _blas1.DOT(row_ws, col_ws, start, i);
                    if (j == i)
                    {
                        diag[i] = _provider.Subtract(diag[i], dot);
                    }
                    else
                    {
                        T[] band = upper[j - i - 1];
                        band[i] = _provider.Subtract(band[i], dot);
                    }
                }

                // Calculate a column of the lower triangular factor L
                max = Math.Min(dim, i + lowerBandwidth + 1);
                for (j = i + 1; j < max; j++)
                {
                    // Calculate A[j, 0:i] * A[0:i, i] which is 
                    // equivalent to A[j, k:i] * A[k:i, i] where 
                    // k = i - min(lowerBandwidth, upperBandwidth)

                    int start = Math.Max(0, i - Math.Min(upperBandwidth, lowerBandwidth - (j - i)));
                    for (int k = start; k < i; ++k)
                    {
                        row_ws[k] = lower[j - k - 1][k];
                        col_ws[k] = upper[i - k - 1][k];
                    }
                    T dot = _blas1.DOT(row_ws, col_ws, start, i);
                    T[] band = lower[j - i - 1];
                    band[i] = _provider.Divide(_provider.Subtract(band[i], dot), diag[i]);
                }
            }

            return new BandLU<T>(A, _blas1);
        }
    }
}
