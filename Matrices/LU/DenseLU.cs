﻿using LinearNet.Providers;
using LinearNet.Structs;
using System;

namespace LinearNet.Matrices.LU
{
    public sealed class DenseLU<T> : LU<T> where T : new()
    {
        private readonly int _dim;
        private readonly DenseMatrix<T> _LU;
        private readonly IDenseBLAS1<T> _blas1;
        private readonly IProvider<T> _provider;

        public override Matrix<T> L
        {
            get
            {
                DenseMatrix<T> _L = _LU.Lower();
                T one = _provider.One;
                T[][] values = _L.Values;
                for (int i = 0; i < _dim; ++i)
                {
                    values[i][i] = one;
                }
                return _L;
            }
        }
        public override Matrix<T> U
        {
            get
            {
                return _LU.Upper();
            }
        }

        internal DenseLU(DenseMatrix<T> LU, IDenseBLAS1<T> blas1)
        {
            _dim = LU.Rows;
            _LU = LU;
            _blas1 = blas1;
            _provider = _blas1.Provider;
        }

        public override T Determinant()
        {
            return _provider.Product(_LU.LeadingDiagonal());
        }

        public override T LogDeterminant()
        {
            return _provider.LogProduct(_LU.LeadingDiagonal());
        }

        public override void Solve(T[] b, T[] x)
        {
            throw new NotImplementedException();
        }
    }
}
