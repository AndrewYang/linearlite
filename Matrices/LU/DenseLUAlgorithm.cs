﻿using LinearNet.Global;
using LinearNet.Providers;
using LinearNet.Structs;
using System;
using System.Collections.Generic;
using System.Text;

namespace LinearNet.Matrices.LU
{
    public class DenseLUAlgorithm<T> : LUAlgorithm<T>, IDenseLUAlgorithm<T> where T : new()
    {
        private readonly IDenseBLAS1<T> _blas;

        internal DenseLUAlgorithm(IDenseBLAS1<T> blas)
        {
            _blas = blas;
        }

        public LU<T> Decompose(DenseMatrix<T> matrix)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            if (!matrix.IsSquare)
            {
                throw new ArgumentOutOfRangeException(nameof(matrix), ExceptionMessages.MatrixNotSquare);
            }

            IProvider<T> provider = _blas.Provider;
            T[][] A = matrix.Values;
            T one = provider.One;

            int dim = A.Length, i, j, c;
            T[] column = new T[dim];
            for (i = 0; i < dim; i++)
            {
                // The upper triangular matrix U is stored in the upper triangular portion of A.
                T[] A_i = A[i];
                for (c = 0; c < i; ++c)
                {
                    _blas.AXPY(A_i, A[c], provider.Negate(A_i[c]), i, dim);
                }

                for (j = 0; j < i; ++j)
                {
                    column[j] = A[j][i];
                }

                // The lower triangular matrix L is stored in the lower triangular portion of A.
                //T A_ii = A_i[i];
                T factor = provider.Divide(one, A_i[i]);
                for (j = i + 1; j < dim; ++j)
                {
                    T[] A_j = A[j];
                    T sum = _blas.DOT(A_j, column, 0, i);
                    A_j[i] = provider.Multiply(provider.Subtract(A_j[i], sum), factor);
                }
            }

            return new DenseLU<T>(A, _blas);
        }
    }
}
