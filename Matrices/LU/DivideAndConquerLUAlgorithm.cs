﻿using LinearNet.Global;
using LinearNet.Matrices.TriangularSolve;
using LinearNet.Providers;
using LinearNet.Structs;
using System;
using System.Collections.Generic;
using System.Text;

namespace LinearNet.Matrices.LU
{
    public class DivideAndConquerLUAlgorithm<T> : IDenseLUAlgorithm<T> where T : new()
    {
        private readonly StrassenRecursiveTriangularSolver<T> _tsolve;
        private readonly VariableSizeStrassenMultiplication<T> _strassen;
        private readonly DenseLUAlgorithm<T> _baseAlgorithm;
        private readonly int _threshold;
        private readonly IDenseBLAS1<T> _blas1;

        internal DivideAndConquerLUAlgorithm(StrassenRecursiveTriangularSolver<T> solver, VariableSizeStrassenMultiplication<T> strassen)
        {
            _tsolve = solver;
            _strassen = strassen;
        }

        public LU<T> Decompose(DenseMatrix<T> matrix)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            if (!matrix.IsSquare)
            {
                throw new InvalidOperationException(ExceptionMessages.MatrixNotSquare);
            }

            DecomposeInner(matrix, 0, matrix.Rows);

            return new DenseLU<T>(matrix, _blas1);
        }

        private void DecomposeInner(DenseMatrix<T> matrix, int offset, int dim)
        {
            throw new NotImplementedException(); // TODO
        }
    }
}
