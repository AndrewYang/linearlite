﻿using LinearNet.Structs;
using System;
using System.Collections.Generic;
using System.Text;

namespace LinearNet.Matrices.LU
{
    public interface IDenseLUAlgorithm<T> where T : new()
    {
        LU<T> Decompose(DenseMatrix<T> matrix);
    }
}
