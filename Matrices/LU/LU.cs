﻿using LinearNet.Structs;

namespace LinearNet.Matrices.LU
{
    public abstract class LU<T> where T : new()
    {
        /// <summary>
        /// The lower triangular factor $L$ in the LU decomposition
        /// </summary>
        public abstract Matrix<T> L { get; }

        /// <summary>
        /// The upper triangular factor $U$ in the LU decomposition
        /// </summary>
        public abstract Matrix<T> U { get; }

        /// <summary>
        /// Solve the square system $Ax = b$ where $A$ is the original 
        /// matrix, $x$ and $b$ are dense vectors, using the LU decomposition
        /// of $A$.
        /// </summary>
        /// <param name="b"></param>
        /// <param name="x"></param>
        public abstract void Solve(T[] b, T[] x);

        /// <summary>
        /// Returns the determinant of the original matrix.
        /// </summary>
        /// <returns>The matrix determinant.</returns>
        public abstract T Determinant();

        /// <summary>
        /// Returns the natural logarithm of the original matrix.
        /// </summary>
        /// <returns>The logarithm of the matrix determinant</returns>
        public abstract T LogDeterminant();
    }
}
