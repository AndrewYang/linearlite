﻿using LinearNet.Providers;
using LinearNet.Structs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Matrices.Decompositions.LU
{
    internal abstract class IDenseLUDecompositionAlgorithm
    {
        /// <summary>
        /// LU decomposition with no pivoting, storing result in matrix A.
        /// </summary>
        /// <param name="A"></param>
        internal abstract void LUDecompose(float[][] A);
        internal abstract void LUDecompose(double[][] A);
        internal abstract void LUDecompose(decimal[][] A);
        internal abstract void LUDecompose(Complex[][] A);

        internal abstract void LUDecomposeParallel(float[][] A);
        internal abstract void LUDecomposeParallel(double[][] A);
        internal abstract void LUDecomposeParallel(decimal[][] A);
        internal abstract void LUDecomposeParallel(Complex[][] A);

        /// <summary>
        /// LU decomposition with row pivoting, storing the result in A.
        /// </summary>
        /// <param name="A"></param>
        /// <param name="permutations"></param>
        internal abstract void LUPDecompose(float[][] A, int[] permutations);
        internal abstract void LUPDecompose(double[][] A, int[] permutations);
        internal abstract void LUPDecompose(decimal[][] A, int[] permutations);
        internal abstract void LUPDecompose(Complex[][] A, int[] permutations);

        internal abstract void LUPDecomposeParallel(float[][] A, int[] permutations);
        internal abstract void LUPDecomposeParallel(double[][] A, int[] permutations);
        internal abstract void LUPDecomposeParallel(decimal[][] A, int[] permutations);
        internal abstract void LUPDecomposeParallel(Complex[][] A, int[] permutations);


        internal void LUDecompose<T>(T[,] A, out T[,] L, out T[,] U, bool upperUnitDiagonal, IProvider<T> blas, Action<T[][]> LU) where T : new()
        {
            MatrixChecks.CheckNotNull(A);
            MatrixChecks.CheckIsSquare(A);

            T[][] _U = A.ToJagged();
            LU(_U);
            SplitMatrices(_U, out T[][] _L, blas, upperUnitDiagonal);

            U = _U.ToRectangular();
            L = _L.ToRectangular();
        }
        internal void LUDecompose<T>(DenseMatrix<T> A, out DenseMatrix<T> L, out DenseMatrix<T> U, bool upperUnitDiagonal, IProvider<T> blas, Action<T[][]> LU) where T : new()
        {
            MatrixChecks.CheckNotNull(A);
            MatrixChecks.CheckIsSquare(A);

            DenseMatrix<T> _U = A.Copy();
            LU(_U.Values);
            SplitMatrices(_U.Values, out T[][] _L, blas, upperUnitDiagonal);

            U = _U;
            L = new DenseMatrix<T>(A.Rows, A.Columns, _L);
        }
        internal void LUPDecompose<T>(DenseMatrix<T> A, out DenseMatrix<T> L, out DenseMatrix<T> U, out int[] P, bool upperUnitDiagonal, IProvider<T> blas, Action<T[][], int[]> LUP) where T : new()
        {
            MatrixChecks.CheckNotNull(A);
            MatrixChecks.CheckIsSquare(A);

            int[] permutation = new int[A.Rows];
            DenseMatrix<T> _U = A.Copy();
            LUP(_U.Values, permutation);
            SplitMatrices(_U.Values, out T[][] _L, blas, upperUnitDiagonal);

            L = _L;
            U = _U;
            P = permutation;
        }

        /// <summary>
        /// For a completed LUP decomposition whose L and U matrices are both stored in U, copy over the L matrix into L and zero them in U.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="U"></param>
        /// <param name="L"></param>
        /// <param name="blas"></param>
        /// <param name="upperUnitDiagonal"></param>
        private void SplitMatrices<T>(T[][] U, out T[][] L, IProvider<T> blas, bool upperUnitDiagonal) where T : new()
        {
            int dim = U.Length, i, j;
            T zero = blas.Zero, one = blas.One;

            // Copy over the lower-triangular values from U into L
            L = MatrixInternalExtensions.JMatrix<T>(dim, dim);
            for (i = 0; i < dim; ++i)
            {
                T[] L_i = L[i], U_i = U[i];
                for (j = 0; j < i; ++j)
                {
                    L_i[j] = U_i[j];
                    U_i[j] = zero;
                }

                if (upperUnitDiagonal)
                {
                    L_i[i] = U_i[i];
                    U_i[i] = one;
                }
                else
                {
                    L_i[i] = one;
                }
            }
        }
    }
}
