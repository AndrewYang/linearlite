﻿using LinearNet.Structs;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet
{
    public static class MatrixCommonExtensions
    {
        internal static T[][] Transpose<T>(this T[][] A)
        {
            MatrixChecks.CheckNotNull(A);

            int rows = A.Length, cols = A[0].Length, r, c;
            T[][] transpose = MatrixInternalExtensions.JMatrix<T>(cols, rows);
            for (r = 0; r < rows; r++)
            {
                T[] row = A[r];
                for (c = 0; c < cols; c++)
                {
                    transpose[c][r] = row[c];
                }
            }
            return transpose;
        }
        internal static T[][] TransposeParallel<T>(this T[][] A)
        {
            int rows = A.Length, cols = A[0].Length;
            T[][] transpose = new T[cols][];

            Parallel.ForEach(Partitioner.Create(0, cols), range =>
            {
                int r, c, min = range.Item1, max = range.Item2;
                for (c = min; c < max; c++)
                {
                    T[] row = new T[rows];
                    for (r = 0; r < cols; r++)
                    {
                        row[r] = A[r][c];
                    }
                    transpose[c] = row;
                }
            });

            return transpose;
        }

        /// <summary>
        /// Transpose A, storing it in A itself (overwrites the original matrix). 
        /// Only applies for square matrices. 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="A"></param>
        internal static void TransposeInPlace<T>(this T[][] A)
        {
            MatrixChecks.CheckNotNull(A);
            MatrixChecks.CheckIsSquare(A);

            int rows = A.Length, r, c;
            for (r = 0; r < rows; ++r)
            {
                T[] A_r = A[r];
                for (c = 0; c < r; ++c)
                {
                    T[] A_c = A[c];
                    T tmp = A_r[c];
                    A_r[c] = A_c[r];
                    A_c[r] = tmp;
                }
            }
        }
        internal static void TransposeInPlaceParallel<T>(this T[][] A)
        {
            MatrixChecks.CheckNotNull(A);
            MatrixChecks.CheckIsSquare(A);

            int rows = A.Length;
            Parallel.ForEach(Partitioner.Create(0, rows), range =>
            {
                int min = range.Item1, max = range.Item2, r, c;
                T[] A_r, A_c;
                T tmp;

                for (r = min; r < max; ++r)
                {
                    A_r = A[r];
                    for (c = 0; c < r; ++c)
                    {
                        A_c = A[c];

                        tmp = A_r[c];
                        A_r[c] = A_c[r];
                        A_c[r] = tmp;
                    }
                }
            });
        }
        internal static void SelfConjugateTranspose(this Complex[][] A)
        {
            MatrixChecks.CheckNotNull(A);
            MatrixChecks.CheckIsSquare(A);

            int rows = A.Length, r, c;
            for (r = 0; r < rows; r++)
            {
                Complex[] A_r = A[r];
                for (c = 0; c < r; c++)
                {
                    Complex[] A_c = A[c];
                    double A_rc_re = A_r[c].Real, A_rc_im = A_r[c].Imaginary,
                        A_cr_re = A_c[r].Real, A_cr_im = A_c[r].Imaginary;

                    // Perform swap
                    A_r[c].Real = A_cr_re;
                    A_r[c].Imaginary = -A_cr_im;

                    A_c[r].Real = A_rc_re;
                    A_c[r].Imaginary = -A_rc_im;
                }

                A_r[r].Imaginary = -A_r[r].Imaginary;
            }
        }
        internal static void SelfConjugateTransposeParallel(this Complex[][] A)
        {
            MatrixChecks.CheckNotNull(A);
            MatrixChecks.CheckIsSquare(A);

            int rows = A.Length;

            Parallel.ForEach(Partitioner.Create(0, rows), range =>
            {
                int min = range.Item1, max = range.Item2, r, c;
                Complex[] A_r, A_c;

                for (r = min; r < max; r++)
                {
                    A_r = A[r];
                    for (c = 0; c < r; c++)
                    {
                        A_c = A[c];
                        double A_rc_re = A_r[c].Real, A_rc_im = A_r[c].Imaginary,
                            A_cr_re = A_c[r].Real, A_cr_im = A_c[r].Imaginary;

                        // Perform swap
                        A_r[c].Real = A_cr_re;
                        A_r[c].Imaginary = -A_cr_im;

                        A_c[r].Real = A_rc_re;
                        A_c[r].Imaginary = -A_rc_im;
                    }

                    A_r[r].Imaginary = -A_r[r].Imaginary;
                }
            });
            
        }

        internal static void Column<T>(this T[][] A, int columnIndex, T[] column)
        {
            MatrixChecks.CheckNotNull(A);

            if (A[0].Length <= columnIndex)
            {
                throw new ArgumentOutOfRangeException($"Column index out of bounds. Matrix width = {A.GetLength(1)}, columnIndex = {columnIndex}.");
            }
            if (A.Length != column.Length)
            {
                throw new ArgumentOutOfRangeException($"Matrix rows does not match column vector length. {A.GetLength(0)} vs {column.Length}");
            }

            int n = column.Length, i;
            for (i = 0; i < n; i++)
            {
                column[i] = A[i][columnIndex];
            }
        }
        internal static T[] Column<T>(this T[][] A, int columnIndex)
        {
            MatrixChecks.CheckNotNull(A);

            if (A[0].Length <= columnIndex)
            {
                throw new ArgumentOutOfRangeException($"Column index out of bounds. Matrix width = {A.GetLength(1)}, columnIndex = {columnIndex}.");
            }

            int n = A.Length, i;
            T[] column = new T[n];
            for (i = 0; i < n; i++)
            {
                column[i] = A[i][columnIndex];
            }
            return column;
        }

        internal static T[][] Copy<T>(T[][] A)
        {
            MatrixChecks.CheckNotNull(A);

            int rows = A.Length, i, j;
            T[][] copy = new T[rows][];
            for (i = 0; i < rows; ++i)
            {
                T[] A_i = A[i];
                int cols = A_i.Length;
                T[] copy_i = new T[cols];
                for (j = 0; j < cols; ++j)
                {
                    copy_i[j] = A_i[j];
                }
                copy[i] = copy_i;
            }
            return copy;
        }
        internal static void CopyFrom<T>(this T[][] dest, T[][] src, int srcFirstRow, int srcFirstCol, int destFirstRow, int destFirstCol, int rows, int cols)
        {
            MatrixChecks.CheckNotNull(dest);
            MatrixChecks.CheckNotNull(src);

            if (srcFirstRow < 0 || srcFirstCol < 0 || rows < 0 || cols < 0 || destFirstRow < 0 || destFirstCol < 0)
            {
                throw new ArgumentOutOfRangeException("Index is less than 0.");
            }
            if (srcFirstRow + rows > src.Length || srcFirstCol + cols > src[0].Length)
            {
                throw new ArgumentOutOfRangeException("Index refers to a location outside the source matrix.");
            }
            if (destFirstRow + rows > dest.Length || destFirstCol + cols > dest[0].Length)
            {
                throw new ArgumentOutOfRangeException("Index refers to a location outside the destination matrix.");
            }

            int r, c;
            for (r = 0; r < rows; ++r)
            {
                //Array.Copy(src[r], srcFirstCol, dest[r], destFirstCol, cols);

                T[] d = dest[destFirstRow + r], s = src[srcFirstRow + r];

                for (c = 0; c < cols; ++c)
                {
                    d[destFirstCol + c] = s[srcFirstCol + c];
                }
            }
        }
        internal static void CopyFrom<T>(this T[][] dest, T[][] src, int srcFirstRow, int srcFirstCol, int destFirstRow, int destFirstCol)
        {
            CopyFrom(dest, src, srcFirstRow, srcFirstCol, destFirstRow, destFirstCol, src.Length, src[0].Length);
        }
        internal static void CopyFrom<T>(this T[][] dest, T[][] src)
        {
            int rows = src.Length, cols = src[0].Length, r, c;
            for (r = 0; r < rows; ++r)
            {
                T[] d = dest[r], s = src[r];
                for (c = 0; c < cols; ++c)
                {
                    d[c] = s[c];
                }
            }
        }
        internal static void CopyTransposeFrom<T>(this T[][] dest, T[][] src, int src_row_start, int src_col_start, int src_row_end, int src_col_end)
        {
            int i, j;
            for (j = src_col_start; j < src_col_end; ++j)
            {
                T[] dest_j = dest[j];
                for (i = src_row_start; i < src_row_end; ++i)
                {
                    dest_j[i] = src[i][j];
                }
            }
        }
        // Cache exploitation algorithm
        internal static void CopyTransposeFrom<T>(this T[][] dest, T[][] src, int src_row_start, int src_col_start, int src_row_end, int src_col_end, int blocksize)
        {
            int i, j, k, l;
            for (i = src_row_start; i < src_row_end; i += blocksize)
            {
                int i_end = i + blocksize;
                for (j = src_col_start; j < src_col_end; j += blocksize)
                {
                    int j_end = j + blocksize;
                    // transpose the block beginning at [i,j]
                    for (k = i; k < i_end; ++k)
                    {
                        T[] src_k = src[k];
                        for (l = j; l < j_end; ++l)
                        {
                            dest[l][k] = src_k[l];
                        }
                    }
                }
            }
        }

        internal static T[][] Submatrix<T>(this T[][] A, int firstRow, int firstCol, int rows, int cols)
        {
            if (A == null) throw new ArgumentNullException();

            T[][] sub = MatrixInternalExtensions.JMatrix<T>(rows, cols);
            int r, c;
            for (r = 0; r < rows; r++)
            {
                T[] src_row = A[firstRow + r], dest_row = sub[r];
                for (c = 0; c < cols; c++)
                {
                    dest_row[c] = src_row[firstCol + c];
                }
            }
            return sub;
        }
        internal static T[][] Submatrix<T>(this T[][] A, int firstRow, int firstCol)
        {
            if (A == null) throw new ArgumentNullException();
            int rows = A.Length - firstRow;
            int cols = A[0].Length - firstCol;
            return Submatrix(A, firstRow, firstCol, rows, cols);
        }
    }
}
