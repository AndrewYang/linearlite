﻿using LinearNet.Structs;
using System;
using System.Collections.Generic;

namespace LinearNet
{
    public static class MatrixConversionExtensions
    {
        public static DenseMatrix<T> Convert<F, T>(this DenseMatrix<F> matrix, Func<F, T> conversion) where T : new() where F : new()
        {
            return new DenseMatrix<T>(Convert(matrix.Values, conversion));
        }
        public static DenseVector<T> Convert<F, T>(this DenseVector<F> vector, Func<F, T> conversion) where T : new() where F : new()
        {
            return new DenseVector<T>(Convert(vector.Values, conversion));
        }
        public static T[,] Convert<F, T>(this F[,] matrix, Func<F, T> conversion)
        {
            if (matrix == null) return null;

            int rows = matrix.GetLength(0), cols = matrix.GetLength(1);
            T[,] dest = new T[rows, cols];
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < cols; j++)
                {
                    dest[i, j] = conversion(matrix[i, j]);
                }
            }
            return dest;
        }
        public static T[] Convert<F, T>(this F[] vector, Func<F, T> conversion)
        {
            if (vector == null) return null;

            T[] result = new T[vector.Length];
            for (int i = 0; i < vector.Length; ++i)
            {
                result[i] = conversion(vector[i]);
            }
            return result;
        }
        public static T[][] Convert<F, T>(this F[][] src, Func<F, T> conversion)
        {
            if (src == null) return null;

            int rows = src.Length;

            T[][] dest = new T[rows][];
            for (int i = 0; i < rows; i++)
            {
                F[] src_row = src[i];

                int cols = src_row.Length;
                T[] dest_row = new T[cols];
                for (int j = 0; j < cols; j++)
                {
                    dest_row[j] = conversion(src_row[j]);
                }
                dest[i] = dest_row;
            }
            return dest;
        }
        public static DOKSparseMatrix<T> Convert<F, T>(this DOKSparseMatrix<F> matrix, Func<F, T> conversion) where F : new() where T : new()
        {
            Dictionary<long, F> from = matrix.Values;
            Dictionary<long, T> to = new Dictionary<long, T>();
            foreach (KeyValuePair<long, F> pair in from)
            {
                to.Add(pair.Key, conversion(pair.Value));
            }
            return new DOKSparseMatrix<T>(matrix.Rows, matrix.Columns, to);
        }

        public static Complex[,] ToComplex(this int[,] matrix) => Convert(matrix, x => new Complex(x));
        public static Complex[,] ToComplex(this long[,] matrix) => Convert(matrix, x => new Complex(x));
        public static Complex[,] ToComplex(this float[,] matrix) => Convert(matrix, x => new Complex(x));
        public static Complex[,] ToComplex(this double[,] matrix) => Convert(matrix, x => new Complex(x));
        public static Complex[,] ToComplex(this decimal[,] matrix) => Convert(matrix, x => new Complex((double)x));
        public static double[,] ToDouble(this int[,] matrix) => Convert(matrix, x => (double)x);
        public static double[,] ToDouble(this long[,] matrix) => Convert(matrix, x => (double)x);
        public static double[,] ToDouble(this float[,] matrix) => Convert(matrix, x => (double)x);
        public static Rational[,] ToRational(this int[,] matrix) => Convert(matrix, x => new Rational(x));

        public static DenseMatrix<Complex> ToComplex(this DenseMatrix<int> matrix) => Convert(matrix, x => new Complex(x));
        public static DenseMatrix<Complex> ToComplex(this DenseMatrix<long> matrix) => Convert(matrix, x => new Complex(x));
        public static DenseMatrix<Complex> ToComplex(this DenseMatrix<float> matrix) => Convert(matrix, x => new Complex(x));
        public static DenseMatrix<Complex> ToComplex(this DenseMatrix<double> matrix) => Convert(matrix, x => new Complex(x));
        public static DenseMatrix<Complex> ToComplex(this DenseMatrix<decimal> matrix) => Convert(matrix, x => new Complex((double)x));
        public static DenseMatrix<double> ToDouble(this DenseMatrix<int> matrix) => Convert(matrix, x => (double)x);
        public static DenseMatrix<double> ToDouble(this DenseMatrix<long> matrix) => Convert(matrix, x => (double)x);
        public static DenseMatrix<double> ToDouble(this DenseMatrix<float> matrix) => Convert(matrix, x => (double)x);
        public static DenseMatrix<Rational> ToRational(this DenseMatrix<int> matrix) => Convert(matrix, x => new Rational(x));

        public static DenseVector<Complex> ToComplex(this DenseVector<int> vector) => Convert(vector, x => new Complex(x));
        public static DenseVector<Complex> ToComplex(this DenseVector<long> vector) => Convert(vector, x => new Complex(x));
        public static DenseVector<Complex> ToComplex(this DenseVector<float> vector) => Convert(vector, x => new Complex(x));
        public static DenseVector<Complex> ToComplex(this DenseVector<double> vector) => Convert(vector, x => new Complex(x));
        public static DenseVector<Complex> ToComplex(this DenseVector<decimal> vector) => Convert(vector, x => new Complex((double)x));
        public static DenseVector<double> ToDouble(this DenseVector<int> vector) => Convert(vector, x => (double)x);
        public static DenseVector<double> ToDouble(this DenseVector<long> vector) => Convert(vector, x => (double)x);
        public static DenseVector<double> ToDouble(this DenseVector<float> vector) => Convert(vector, x => (double)x);
        public static DenseVector<Rational> ToRational(this DenseVector<int> vector) => Convert(vector, x => new Rational(x));

        public static DOKSparseMatrix<Complex> ToComplex(this DOKSparseMatrix<int> matrix) => Convert(matrix, x => new Complex(x));
        public static DOKSparseMatrix<Complex> ToComplex(this DOKSparseMatrix<long> matrix) => Convert(matrix, x => new Complex(x));
        public static DOKSparseMatrix<Complex> ToComplex(this DOKSparseMatrix<float> matrix) => Convert(matrix, x => new Complex(x));
        public static DOKSparseMatrix<Complex> ToComplex(this DOKSparseMatrix<double> matrix) => Convert(matrix, x => new Complex(x));
        public static DOKSparseMatrix<Complex> ToComplex(this DOKSparseMatrix<decimal> matrix) => Convert(matrix, x => new Complex((double)x));
        public static DOKSparseMatrix<double> ToDouble(this DOKSparseMatrix<int> matrix) => Convert(matrix, x => (double)x);
        public static DOKSparseMatrix<double> ToDouble(this DOKSparseMatrix<long> matrix) => Convert(matrix, x => (double)x);
        public static DOKSparseMatrix<double> ToDouble(this DOKSparseMatrix<float> matrix) => Convert(matrix, x => (double)x);
        public static DOKSparseMatrix<Rational> ToRational(this DOKSparseMatrix<int> matrix) => Convert(matrix, x => new Rational(x));



        public static double[] ToDouble(this int[] vector)
        {
            return Convert(vector, new Func<int, double>(d => d));
        }
        public static Rational[] ToFraction(this int[] vector)
        {
            return Convert(vector, new Func<int, Rational>(i => new Rational(i)));
        }
    }
}
