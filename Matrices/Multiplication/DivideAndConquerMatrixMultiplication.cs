﻿using LinearNet.Matrices.Multiplication;
using LinearNet.Providers;
using LinearNet.Providers.LinearAlgebra;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Matrices
{
    /// <summary>
    /// <p>Implements a cache-oblivious <a href="https://en.wikipedia.org/wiki/Matrix_multiplication_algorithm#Divide_and_conquer_algorithm">divide-and-conquer</a> 
    /// block matrix multiplication algorithm.</p>
    /// <p>This algorithm calculates the matrix product $C = AB$ by breaking up each matrix into $2 \times 2$ submatrices:</p>
    /// 
    /// $$
    /// A = \begin{pmatrix} A_{11} & A_{12} \\ A_{21} & A_{22} \end{pmatrix}, 
    /// B = \begin{pmatrix} B_{11} & B_{12} \\ B_{21} & B_{22} \end{pmatrix},
    /// C = \begin{pmatrix} C_{11} & C_{12} \\ C_{21} & C_{22} \end{pmatrix}
    /// $$
    /// 
    /// and calculates the products:
    /// $$C_{11} := A_{11}B_{11} + A_{12}B_{21},$$
    /// $$C_{12} := A_{11}B_{12} + A_{12}B_{22},$$
    /// $$C_{21} := A_{21}B_{11} + A_{22}B_{21},$$
    /// $$C_{22} := A_{21}B_{12} + A_{22}B_{22}.$$
    /// 
    /// <p>
    /// This implementation calculates the 8 subproducts using Morton ordering (based on Hilbert's space-filling curve).
    /// </p>
    /// 
    /// <p>
    /// This process is recursively applied until submatrices' dimensions are smaller than <txt>threshold</txt>, below which naive
    /// matrix multiplication is used. For an optimised choice of <txt>threshold</txt>, the number of cache-misses involved with multiplying each submatrix
    /// block is minimised, as a larger porportion of each block fits inside a CPU cache. The resultant performance improvement over 'naive'
    /// matrix multiplication is hardware dependent but can be several times faster on modern CPUs.
    /// </p>
    /// 
    /// <p>This implementation uses block-multiplication and achieves $O(n^3 / \sqrt{Z})$ cache complexity.</p>
    /// 
    /// <p>
    /// The arithmetic complexity of this algorithm is $O(n^3)$. See <a href="IStrassenMMAlgorithm_T_.html">Strassen's algorithm</a>
    /// for a subcubic-complexity matrix multiplication algorithm.
    /// </p>
    /// 
    /// <p>
    /// <a href="https://www.sciencedirect.com/science/article/pii/S0024379506001595">Some research</a> suggests that the 
    /// $2 \times 2$ divide-and-conquer algorithm is not asymptotically optimal for simultaneously minimising spatial and 
    /// temporal locality of data access. See <txt><a href="PeanoCurveMMAlgorithm_T_.html">PeanoCurveMMAlgorithm</a></txt> 
    /// for an implementation of the cache-oblivious multiplication algorithm based on Peano space-filling curves.
    /// </p>
    /// </summary>
    /// <cat>linear-algebra</cat>
    public class DivideAndConquerMatrixMultiplication<T> : IMatrixMultiplication<T>
    {
        private readonly int _threshold;
        private readonly IMatrixMultiplication<T> _baseAlgorithm;
        private readonly IDenseBLAS1<T> _blas;
        private readonly IProvider<T> _provider;
        private T[][] _workspace;

        /// <summary>
        /// The mode of multiplication
        /// </summary>
        public MultiplicationMode Mode { get; private set; }

        /// <summary>
        /// Initialize a new cache-oblivious matrix multiplication algorithm.
        /// </summary>
        /// <param name="baseAlgorithm">
        /// The base matrix multiplication to use for submatrices smaller than <txt>thresholdSize</txt>.
        /// </param>
        /// <param name="provider">
        /// A BLAS-1 linear algebra provider for type <txt>T</txt>.
        /// </param>
        /// <param name="thresholdSize">
        /// The threshold size of the block-submatrix below which the base matrix multiplication will be used.
        /// <p>Specifically, for a <m, n, p> matrix multiplication problem, the algorithm will default to 
        /// naive multiplication if $\frac{m + n + p}{3} < n$ where $n$ is the threshold size.</p>
        /// </param>
        /// <param name="mode">
        /// The mode of multiplication - prioritize either memory efficiency or speed.
        /// </param>
        /// <param name="workspace">
        /// A workspace of at least the size of $B^T$ for a matrix multiplication $AB$. Used to improve 
        /// performance of the algorithm. Will be ignored if multiplication mode is not <txt>OPTIMIZE_SPEED</txt>.
        /// </param>
        public DivideAndConquerMatrixMultiplication(
            IMatrixMultiplication<T> baseAlgorithm, 
            IDenseBLAS1Provider<T> provider, 
            int thresholdSize, 
            MultiplicationMode mode = MultiplicationMode.OPTIMIZE_SPEED,
            T[][] workspace = null) 
            : this(baseAlgorithm, provider as IDenseBLAS1<T>, thresholdSize, mode) 
        {
            _workspace = workspace;
        }

        /// <summary>
        /// Initialize a divide-and-conquer matrix multiplication algorithm with naive-matrix multiplication as its 
        /// base algorithm.
        /// </summary>
        /// <param name="provider">
        /// A level-1 BLAS provider for type <txt>T</txt>.
        /// </param>
        /// <param name="thresholdSize">
        /// The threshold size of the block-submatrix below which naive matrix multiplication will be used.
        /// </param>
        /// <param name="mode">
        /// The mode of multiplication - prioritize either memory efficiency or speed.
        /// </param>
        public DivideAndConquerMatrixMultiplication(
            IDenseBLAS1Provider<T> provider, 
            int thresholdSize, 
            MultiplicationMode mode = MultiplicationMode.OPTIMIZE_SPEED)
            : this(new NaiveMatrixMultiplication<T>(provider), provider as IDenseBLAS1<T>, thresholdSize, mode) { }

        internal DivideAndConquerMatrixMultiplication(
            IMatrixMultiplication<T> baseAlgorithm, 
            IDenseBLAS1<T> provider, 
            int thresholdSize, 
            MultiplicationMode mode = MultiplicationMode.OPTIMIZE_SPEED)
        {
            _baseAlgorithm = baseAlgorithm ?? throw new ArgumentNullException(nameof(baseAlgorithm));
            _blas = provider ?? throw new ArgumentNullException(nameof(provider));

            // If threshold size is an invalid value, default to 32.
            if (thresholdSize <= 0)
            {
                thresholdSize = 32;
            }

            Mode = mode;
            _provider = provider.Provider;
            _threshold = 3 * thresholdSize;
        }


        public void Multiply(T[][] A, T[][] B, T[][] result, Size3 size, bool increment)
        {
            if (Mode == MultiplicationMode.OPTIMIZE_SPEED)
            {
                // Check if workspace requires expansion
                if (_workspace == null || _workspace.Length < size.P || _workspace[0].Length < size.N)
                {
                    _workspace = MatrixInternalExtensions.JMatrix<T>(size.P, size.N);
                }

                MultiplyWithWorkspace(A, B, _workspace, result, size, increment);
            }
            else
            {
                MultiplyWithoutWorkspace(A, B, result, size.M, size.N, size.P, increment);
            }
        }
        private void MultiplyWithoutWorkspace(T[][] A, T[][] B, T[][] result, int m, int n, int p, bool increment)
        {
            // TODO: implement more intelligent checking for highly-rectangular matrices.

            if (!increment)
            {
                Fill(result, _provider.Zero, 0, m, 0, p);
            }

            if (m + n + p < _threshold)
            {
                // SIZETODO
                _baseAlgorithm.Multiply(A, B, result, new Size3(m, n, p), true);
                return;
            }

            // Divide-and-conquer block multiplication method
            int a1 = m / 2, b1 = n / 2, c1 = p / 2,
                a2 = m - a1, b2 = n - b1, c2 = p - c1;

            // C11 += A11 * B11
            MultiplyWithoutWorkspace(A, B, result,                          a1, b1, c1, true);
            // C11 += A12 * B21
            MultiplyWithoutWorkspace(A, B, result, 0, b1, b1, 0, 0, 0,      a1, b2, c1, true);

            // C12 += A11 * B12
            MultiplyWithoutWorkspace(A, B, result, 0, 0, 0, c1, 0, c1,      a1, b1, c2, true);
            // C12 += A12 * B22
            MultiplyWithoutWorkspace(A, B, result, 0, b1, b1, c1, 0, c1,    a1, b2, c2, true);

            // C21 += A21 * B11
            MultiplyWithoutWorkspace(A, B, result, a1, 0, 0, 0, a1, 0,      a2, b1, c1, true);
            // C21 += A22 * B21
            MultiplyWithoutWorkspace(A, B, result, a1, b1, b1, 0, a1, 0,    a2, b2, c1, true);

            // C22 += A21 * B12
            MultiplyWithoutWorkspace(A, B, result, a1, 0, 0, c1, a1, c1,    a2, b1, c2, true);
            // C22 += A22 * B22
            MultiplyWithoutWorkspace(A, B, result, a1, b1, b1, c1, a1, c1,  a2, b2, c2, true);
        }
        private void MultiplyWithWorkspace(T[][] A, T[][] B, T[][] Bt_workspace, T[][] result, Size3 size, bool increment)
        {
            if (!increment)
            {
                Fill(result, _provider.Zero, 0, size.M, 0, size.P);
            }

            // Copy the transpose
            Bt_workspace.CopyTransposeFrom(B, 0, 0, size.N, size.P);

            MultiplyTransposed(A, Bt_workspace, result, size);
        }

        public void Multiply(T[][] A, T[][] B, T[][] result, int A_start_row, int A_start_col, int B_start_row, int B_start_col, int C_start_row, int C_start_col, Size3 size, bool increment)
        {
            if (Mode == MultiplicationMode.OPTIMIZE_SPEED)
            {
                // Check if workspace requires expansion
                if (_workspace == null || _workspace.Length < size.P || _workspace[0].Length < size.N)
                {
                    _workspace = MatrixInternalExtensions.JMatrix<T>(size.P, size.N);
                }

                MultiplyWithWorkspace(A, B, _workspace, result, A_start_row, A_start_col, B_start_row, B_start_col, C_start_row, C_start_col, size, increment);
            }
            else
            {
                MultiplyWithoutWorkspace(A, B, result, A_start_row, A_start_col, B_start_row, B_start_col, C_start_row, C_start_col, size.M, size.N, size.P, increment);
            }
        }
        private void MultiplyWithoutWorkspace(T[][] A, T[][] B, T[][] result, int A_start_row, int A_start_col, int B_start_row, int B_start_col, int C_start_row, int C_start_col, int m, int n, int p, bool increment)
        {
            if (!increment)
            {
                Fill(result, _provider.Zero, C_start_row, C_start_row + m, C_start_col, C_start_col + p);
            }

            if (m + n + p < _threshold)
            {
                // SIZETODO
                _baseAlgorithm.Multiply(A, B, result, A_start_row, A_start_col, B_start_row, B_start_col, C_start_row, C_start_col, new Size3(m, n, p), increment);
                return;
            }

            // The widths
            int a1 = m / 2, b1 = n / 2, c1 = p / 2,
                a2 = m - a1, b2 = n - b1, c2 = p - c1;

            // C11 += A11 * B11
            MultiplyWithoutWorkspace(A, B, result, A_start_row, A_start_col, B_start_row, B_start_col, C_start_row, C_start_col,                                a1, b1, c1, true);
            // C11 += A12 * B21
            MultiplyWithoutWorkspace(A, B, result, A_start_row, A_start_col + b1, B_start_row + b1, B_start_col, C_start_row, C_start_col,                      a1, b2, c1, true);

            // C12 += A11 * B12
            MultiplyWithoutWorkspace(A, B, result, A_start_row, A_start_col, B_start_row, B_start_col + c1, C_start_row, C_start_col + c1,                      a1, b1, c2, true);
            // C12 += A12 * B22
            MultiplyWithoutWorkspace(A, B, result, A_start_row, A_start_col + b1, B_start_row + b1, B_start_col + c1, C_start_row, C_start_col + c1,            a1, b2, c2, true);

            // C21 += A21 * B11
            MultiplyWithoutWorkspace(A, B, result, A_start_row + a1, A_start_col, B_start_row, B_start_col, C_start_row + a1, C_start_col,                      a2, b1, c1, true);
            // C21 += A22 * B21
            MultiplyWithoutWorkspace(A, B, result, A_start_row + a1, A_start_col + b1, B_start_row + b1, B_start_col, C_start_row + a1, C_start_col,            a2, b2, c1, true);

            // C22 += A21 * B12
            MultiplyWithoutWorkspace(A, B, result, A_start_row + a1, A_start_col, B_start_row, B_start_col + c1, C_start_row + a1, C_start_col + c1,            a2, b1, c2, true);
            // C22 += A22 * B22
            MultiplyWithoutWorkspace(A, B, result, A_start_row + a1, A_start_col + b1, B_start_row + b1, B_start_col + c1, C_start_row + a1, C_start_col + c1,  a2, b2, c2, true);
        }
        private void MultiplyWithWorkspace(T[][] A, T[][] B, T[][] Bt_workspace, T[][] result, int A_start_row, int A_start_col, int B_start_row, int B_start_col, int C_start_row, int C_start_col, Size3 size, bool increment)
        {
            if (!increment)
            {
                Fill(result, _provider.Zero, 0, size.M, 0, size.P);
            }

            // Copy the transpose
            Bt_workspace.CopyTransposeFrom(B, 0, 0, size.N, size.P);

            MultiplyTransposed(A, Bt_workspace, result, A_start_row, A_start_col, B_start_row, B_start_col, C_start_row, C_start_col, size.M, size.P, size.N);
        }

        /// <summary>
        /// Full [row_start : row_end, col_start : col_end] with the value
        /// </summary>
        private void Fill(T[][] A, T value, int row_start, int row_end, int col_start, int col_end)
        {
            int i, j;
            for (i = row_start; i < row_end; ++i)
            {
                T[] A_i = A[i];
                for (j = col_start; j < col_end; ++j)
                {
                    A_i[j] = value;
                }
            }
        }
        /// <summary>
        /// Calculates result += AB, given A and B^T.
        /// </summary>
        private void MultiplyTransposed(T[][] A, T[][] Bt, T[][] result, Size3 size)
        {
            if (size.DimSum < _threshold)
            {
                int rows = size.M, depth = size.N, columns = size.P, i, j; 
                for (i = 0; i < rows; ++i)
                {
                    T[] A_i = A[i], result_i = result[i];
                    for (j = 0; j < columns; ++j)
                    {
                        result_i[j] = _provider.Add(result_i[j], _blas.DOT(A_i, Bt[j], 0, depth));
                    }
                }
                return;
            }

            // Divide-and-conquer block multiplication method
            int a1 = size.HalfM, b1 = size.HalfN, c1 = size.HalfP,
                a2 = size.M - a1, b2 = size.N - b1, c2 = size.P - c1;

            // C11 += A11 * B11
            MultiplyTransposed(A, Bt, result, new Size3(a1, b1, c1));
            // C11 += A12 * B21
            MultiplyTransposed(A, Bt, result, 0, b1, b1, 0, 0, 0, a1, c1, b2);

            // C12 += A11 * B12
            MultiplyTransposed(A, Bt, result, 0, 0, 0, c1, 0, c1, a1, c2, b1);
            // C12 += A12 * B22
            MultiplyTransposed(A, Bt, result, 0, b1, b1, c1, 0, c1, a1, c2, b2);

            // C21 += A21 * B11
            MultiplyTransposed(A, Bt, result, a1, 0, 0, 0, a1, 0, a2, c1, b1);
            // C21 += A22 * B21
            MultiplyTransposed(A, Bt, result, a1, b1, b1, 0, a1, 0, a2, c1, b2);

            // C22 += A21 * B12
            MultiplyTransposed(A, Bt, result, a1, 0, 0, c1, a1, c1, a2, c2, b1);
            // C22 += A22 * B22
            MultiplyTransposed(A, Bt, result, a1, b1, b1, c1, a1, c1, a2, c2, b2);
        }
        private void MultiplyTransposed(T[][] A, T[][] Bt, T[][] result, int Ar, int Ac, int Br, int Bc, int Cr, int Cc, int rows, int cols, int N)
        {
            if (rows + cols + N < _threshold)
            {
                int CA_offset = Cr - Ar,
                    BA_offset = Br - Ac,
                    BC_offset = Bc - Cc,
                    A_end_row = Ar + rows,
                    A_end_col = Ac + N,
                    C_end_col = Cc + cols,
                    i, j;

                if (BA_offset == 0)
                {
                    if (BC_offset == 0)
                    {
                        if (CA_offset == 0)
                        {
                            // We're hoping that we spend most of our time inside this conditional...
                            // Saves O(n^2) additions from CA_offset = 0.
                            // Saves O(n^2) additions from BC_offset = 0.
                            // Saves O(n^3) additions from BA_offset = 0.
                            // Costs O(n^2 / Z^2) checks to implement.
                            for (i = Ar; i < A_end_row; ++i)
                            {
                                T[] A_i = A[i], result_i = result[i];
                                for (j = Cc; j < C_end_col; ++j)
                                {
                                    result_i[j] = _provider.Add(result_i[j], _blas.DOT(A_i, Bt[j], Ac, A_end_col));
                                }
                            }
                        }
                        else
                        {
                            for (i = Ar; i < A_end_row; ++i)
                            {
                                T[] A_i = A[i], result_i = result[i + CA_offset];
                                for (j = Cc; j < C_end_col; ++j)
                                {
                                    result_i[j] = _provider.Add(result_i[j], _blas.DOT(A_i, Bt[j], Ac, A_end_col));
                                }
                            }
                        }
                    }
                    else
                    {
                        for (i = Ar; i < A_end_row; ++i)
                        {
                            T[] A_i = A[i], result_i = result[i + CA_offset];
                            for (j = Cc; j < C_end_col; ++j)
                            {
                                result_i[j] = _provider.Add(result_i[j], _blas.DOT(A_i, Bt[j + BC_offset], Ac, A_end_col));
                            }
                        }
                    }
                }
                else
                {
                    for (i = Ar; i < A_end_row; ++i)
                    {
                        T[] A_i = A[i], result_i = result[i + CA_offset];
                        for (j = Cc; j < C_end_col; ++j)
                        {
                            result_i[j] = _provider.Add(result_i[j], _blas.DOT(A_i, Bt[j + BC_offset], Ac, A_end_col, BA_offset));
                        }
                    }
                }
                return;
            }

            // The widths
            int a1 = rows / 2, b1 = N / 2, c1 = cols / 2,
                a2 = rows - a1, b2 = N - b1, c2 = cols - c1,
                Ar1 = Ar + a1, Ac1 = Ac + b1,
                Br1 = Br + b1, Bc1 = Bc + c1,
                Cr1 = Cr + a1, Cc1 = Cc + c1;

            /*
            // C11 += A11 * B11  [0, 0, 0]
            MultiplyTransposed(A, Bt, result, Ar, Ac, Br, Bc, Cr, Cc,       a1, c1, b1);
            // C11 += A12 * B21  [1, 2, 0]
            MultiplyTransposed(A, Bt, result, Ar, Ac1, Br1, Bc, Cr, Cc,     a1, c1, b2);

            // C12 += A11 * B12  [0, 1, 1]
            MultiplyTransposed(A, Bt, result, Ar, Ac, Br, Bc1, Cr, Cc1,     a1, c2, b1);
            // C12 += A12 * B22  [1, 3, 1]
            MultiplyTransposed(A, Bt, result, Ar, Ac1, Br1, Bc1, Cr, Cc1,   a1, c2, b2);

            // C21 += A21 * B11  [2, 0, 2]
            MultiplyTransposed(A, Bt, result, Ar1, Ac, Br, Bc, Cr1, Cc,     a2, c1, b1);
            // C21 += A22 * B21  [3, 2, 2]
            MultiplyTransposed(A, Bt, result, Ar1, Ac1, Br1, Bc, Cr1, Cc,   a2, c1, b2);

            // C22 += A21 * B12  [2, 1, 3]
            MultiplyTransposed(A, Bt, result, Ar1, Ac, Br, Bc1, Cr1, Cc1,   a2, c2, b1);
            // C22 += A22 * B22  [3, 3, 3]
            MultiplyTransposed(A, Bt, result, Ar1, Ac1, Br1, Bc1, Cr1, Cc1, a2, c2, b2);
            */

            // Using Morton ordering (based on Hilbert's space filling curve 2 x 2)
            // Although it seems like there is no performance benefit to doing this...

            // C11 += A11 * B11  [0, 0, 0]
            MultiplyTransposed(A, Bt, result, Ar, Ac, Br, Bc, Cr, Cc,               a1, c1, b1);
            // C12 += A11 * B12  [0, 1, 1]
            MultiplyTransposed(A, Bt, result, Ar, Ac, Br, Bc1, Cr, Cc1,             a1, c2, b1);
            // C11 += A12 * B21  [1, 2, 0]
            MultiplyTransposed(A, Bt, result, Ar, Ac1, Br1, Bc, Cr, Cc,             a1, c1, b2);
            // C12 += A12 * B22  [1, 3, 1]
            MultiplyTransposed(A, Bt, result, Ar, Ac1, Br1, Bc1, Cr, Cc1,           a1, c2, b2);

            // C21 += A21 * B11  [2, 0, 2]
            MultiplyTransposed(A, Bt, result, Ar1, Ac, Br, Bc, Cr1, Cc,             a2, c1, b1);
            // C22 += A21 * B12  [2, 1, 3]
            MultiplyTransposed(A, Bt, result, Ar1, Ac, Br, Bc1, Cr1, Cc1,           a2, c2, b1);
            // C21 += A22 * B21  [3, 2, 2]
            MultiplyTransposed(A, Bt, result, Ar1, Ac1, Br1, Bc, Cr1, Cc,           a2, c1, b2);
            // C22 += A22 * B22  [3, 3, 3]
            MultiplyTransposed(A, Bt, result, Ar1, Ac1, Br1, Bc1, Cr1, Cc1,         a2, c2, b2);
        }
    }
}
