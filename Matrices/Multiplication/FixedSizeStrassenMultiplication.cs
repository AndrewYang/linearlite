﻿using LinearNet.Providers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Matrices
{
    /// <summary>
    /// Matrix multiplication implementation for Strassen and Strassen-like algorithms.
    /// Used to solve a <m, n, p> multiplication problem for which the size of the problem
    /// is fixed.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class FixedSizeStrassenMultiplication<T> : IMatrixMultiplication<T> where T : new()
    {
        private readonly int _m, _n, _p;
        private readonly bool _useWinogradVariant;
        private readonly int _baseAlgoThreshold;
        private readonly Dictionary<int, T[][]> _tempAs, _tempBs, _Ms, _tempCs;
        private readonly Dictionary<int, T[][]> _A_3s, _B_3s, _tempMs;

        private readonly IDenseBLAS1<T> _blas1;
        private readonly IDenseBLAS2<T> _blas2;
        private readonly IMatrixMultiplication<T> _baseMultiplication;

        /// <summary>
        /// Gets the set of initialized workspaces for holding the product matrix.
        /// </summary>
        internal List<int> RegisterSizes { get { return _tempCs.Keys.ToList(); } }
        internal IDenseBLAS1<T> BLAS1 { get { return _blas1; } }
        internal IDenseBLAS2<T> BLAS2 { get { return _blas2; } }

        /// <summary>
        /// Set up a <m, n, p> matrix multiplication problem.
        /// </summary>
        /// <param name="blas1">A level-1 BLAS provider for type <txt>T</txt>.</param>
        /// <param name="blas2">A level-2 BLAS provider for type <txt>T</txt>.</param>
        /// <param name="baseAlgo">The base-matrix mutliplication algorithm which will be used for submatrices under a certain threshold.</param>
        /// <param name="m">The max. number of rows of the 1st matrix to be multiplied.</param>
        /// <param name="n">The max number of columns of the 1st matrix to be multiplied.</param>
        /// <param name="p">The max number of columns of the 2nd matrix to be multiplied.</param>
        /// <param name="useWinogradVariant">If true, uses the Winograd variant of Strassen's algorithm (15 addition/subtractions instead of 18)</param>
        /// <param name="baseAlgorithmThreshold">The matrix dimensions under which we revert back to the base matrix multiplication algorithm.</param>
        /// <param name="BTransposed">If true, the matrix B^T will be provided in place of B (improves caching)</param>
        internal FixedSizeStrassenMultiplication(
            IDenseBLAS1<T> blas1,
            IDenseBLAS2<T> blas2,
            IMatrixMultiplication<T> baseAlgo,
            int m, int n, int p, 
            bool useWinogradVariant = false, 
            int baseAlgorithmThreshold = 32)
        {
            //_instruction = instructions;
            _blas1 = blas1;
            _blas2 = blas2;
            _baseMultiplication = baseAlgo;
            _m = m;
            _n = n;
            _p = p;
            _useWinogradVariant = useWinogradVariant;
            _baseAlgoThreshold = baseAlgorithmThreshold;

            // Pre-calculate the blocks
            _tempAs = new Dictionary<int, T[][]>();
            _tempBs = new Dictionary<int, T[][]>();
            _Ms = new Dictionary<int, T[][]>();
            _tempCs = new Dictionary<int, T[][]>();

            if (_useWinogradVariant)
            {
                _A_3s = new Dictionary<int, T[][]>();
                _B_3s = new Dictionary<int, T[][]>();
                _tempMs = new Dictionary<int, T[][]>();
            }

            while (!use_naive_algorithm(m, n, p))
            {
                _tempCs[m] = MatrixInternalExtensions.JMatrix<T>(m, p);

                m /= 2;
                n /= 2;
                p /= 2;

                _tempAs[m] = MatrixInternalExtensions.JMatrix<T>(m, n);
                _tempBs[n] = MatrixInternalExtensions.JMatrix<T>(n, p);
                _Ms[m] = MatrixInternalExtensions.JMatrix<T>(m, p);

                if (_useWinogradVariant)
                {
                    _A_3s[m] = MatrixInternalExtensions.JMatrix<T>(m, n);
                    _B_3s[n] = MatrixInternalExtensions.JMatrix<T>(n, p);
                    _tempMs[m] = MatrixInternalExtensions.JMatrix<T>(m, p);
                }
            }
        }

        private bool use_naive_algorithm(int m, int n, int p)
        {
            return m < _baseAlgoThreshold || n < _baseAlgoThreshold || p < _baseAlgoThreshold;
        }

        public void Multiply(T[][] A, T[][] B, T[][] result, Size3 size, bool increment)
        {
            // TODO range checks 
            if (_useWinogradVariant)
            {
                mult_winograd(A, B, result, size.M, size.N, size.P, increment);
            }
            else
            {
                mult_strassen(A, B, result, size.M, size.N, size.P, increment);
            }
        }
        public void Multiply(T[][] A, T[][] B, T[][] result, int A_row, int A_col, int B_row, int B_col, int C_row, int C_col, Size3 size, bool increment)
        {
            // TODO range checks
            if (_useWinogradVariant)
            {
                mult_winograd(A, B, result, A_row, A_col, B_row, B_col, C_row, C_col, size.M, size.N, size.P, increment);
            }
            else
            {
                mult_strassen(A, B, result, A_row, A_col, B_row, B_col, C_row, C_col, size.M, size.N, size.P, increment);
            }
        }

        private void mult_strassen(T[][] A, T[][] B, T[][] C, int m, int n, int p, bool increment)
        {
            int a = m / 2, b = n / 2, c = p / 2;

            // base case - use simple matrix multiplication
            if (use_naive_algorithm(m, n, p))
            {
                // SIZETODO
                _baseMultiplication.Multiply(A, B, C, new Size3(m, n, p), increment);
                return;
            }

            T[][] tempA = _tempAs[a];
            T[][] tempB = _tempBs[b];
            T[][] M = _Ms[a];
            T[][] tempResult = _tempCs[m];

            // M_1 = (A_11 + A_22)(B_11 + B_22)
            _blas2.Add(A, A, tempA, 0, 0, a, b, a, b);
            _blas2.Add(B, B, tempB, 0, 0, b, c, b, c);
            mult_strassen(tempA, tempB, M, a, b, c, false);
            _blas2.Copy(tempResult, M, a, c);               // C_11 := M_1
            _blas2.Copy(tempResult, M, a, c, 0, 0, a, c);   // C_22 := M_1

            // M_2 = (A_21 + A_22)B_11
            _blas2.Add(A, A, tempA, a, 0, a, b, a, b);
            _blas2.Copy(tempB, B, b, c);
            mult_strassen(tempA, tempB, M, a, b, c, false);
            _blas2.Copy(tempResult, M, a, 0, 0, 0, a, c);   // C_21 := M_2
            _blas2.Decrement(tempResult, M, a, c, a, c);    // C_22 -= M_2

            // M_3 = A_11(B_12 - B_22) 
            _blas2.Copy(tempA, A, a, b);
            _blas2.Subtract(B, B, tempB, 0, c, b, c, b, c);
            mult_strassen(tempA, tempB, M, a, b, c, false);
            _blas2.Copy(tempResult, M, 0, c, 0, 0, a, c);   // C_12 := M_3
            _blas2.Increment(tempResult, M, a, c, a, c);    // C_22 += M_3

            // M_4 = A_22(B_21 - B_11)
            _blas2.Copy(tempA, A, a, b, a, b);
            _blas2.Subtract(B, B, tempB, b, 0, 0, 0, b, c);
            mult_strassen(tempA, tempB, M, a, b, c, false);
            _blas2.Increment(tempResult, M, a, c);          // C_11 += M_4
            _blas2.Increment(tempResult, M, a, 0, a, c);    // C_21 += M_4

            // M_5 = (A_11 + A_12)B_22
            _blas2.Add(A, A, tempA, 0, 0, 0, b, a, b);
            _blas2.Copy(tempB, B, b, c, b, c);
            mult_strassen(tempA, tempB, M, a, b, c, false);
            _blas2.Decrement(tempResult, M, a, c);          // C_11 -= M_5
            _blas2.Increment(tempResult, M, 0, c, a, c);    // C_12 += M_5

            // M_6 = (A_21 - A_11)(B_11 + B_12)
            _blas2.Subtract(A, A, tempA, a, 0, 0, 0, a, b);
            _blas2.Add(B, B, tempB, 0, 0, 0, c, b, c);
            mult_strassen(tempA, tempB, M, a, b, c, false);
            _blas2.Increment(tempResult, M, a, c, a, c);    // C_22 += M_6

            // M_7 = (A_12 - A_22)(B_21 + B_22) 
            _blas2.Subtract(A, A, tempA, 0, b, a, b, a, b);
            _blas2.Add(B, B, tempB, b, 0, b, c, b, c);
            mult_strassen(tempA, tempB, M, a, b, c, false);
            _blas2.Increment(tempResult, M, a, c);          // C_11 += M_7

            // If m is odd, append the bottom row
            if (m % 2 == 1)
            {
                append_bottom_row(A, B, tempResult, m, n, p);
            }
            // If n is odd, append to the top-left submatrix
            if (n % 2 == 1)
            {
                append_top_left_submatrix(A, B, tempResult, m, n, p);
            }
            // If p is odd, append the rightmost column
            if (p % 2 == 1)
            {
                append_right_column(A, B, tempResult, m, n, p);
            }

            if (increment)
            {
                _blas2.Increment(C, tempResult, m, p);
            }
            else
            {
                _blas2.Copy(C, tempResult, m, p);
            }
        }
        private void mult_strassen(T[][] A, T[][] B, T[][] C, int Ar0, int Ac0, int Br0, int Bc0, int Cr0, int Cc0, int m, int n, int p, bool increment)
        {
            int a = m / 2, 
                b = n / 2,
                c = p / 2,
                Ar1 = Ar0 + a,
                Ac1 = Ac0 + b,
                Br1 = Br0 + b,
                Bc1 = Bc0 + c;

            bool is_simple = Ar0 == 0 && Ac0 == 0 && Br0 == 0 && Bc1 == 0 && Cr0 == 0 && Cc0 == 0;

            // base case - use simple matrix multiplication
            if (use_naive_algorithm(m, n, p))
            {
                // SIZETODO
                Size3 size = new Size3(m, n, p);
                if (is_simple)
                {
                    _baseMultiplication.Multiply(A, B, C, size, increment);
                }
                else
                {
                    _baseMultiplication.Multiply(A, B, C, Ar0, Ac0, Br0, Bc0, Cr0, Cc0, size, increment);
                }
                return;
            }

            T[][] tempA = _tempAs[a];
            T[][] tempB = _tempBs[b];
            T[][] M = _Ms[a];
            T[][] tempResult = _tempCs[m];

            // M_1 = (A_11 + A_22)(B_11 + B_22)
            _blas2.Add(A, A, tempA, Ar0, Ac0, Ar1, Ac1, a, b);
            _blas2.Add(B, B, tempB, Br0, Bc0, Br1, Bc1, b, c);
            mult_strassen(tempA, tempB, M, a, b, c, false);
            _blas2.Copy(tempResult, M, a, c);                       // C_11 := M_1
            _blas2.Copy(tempResult, M, a, c, 0, 0, a, c);           // C_22 := M_1

            // M_2 = (A_21 + A_22)B_11
            _blas2.Add(A, A, tempA, Ar1, Ac0, Ar1, Ac1, a, b);
            _blas2.Copy(tempB, B, Br0, Bc0, b, c);
            mult_strassen(tempA, tempB, M, a, b, c, false);
            _blas2.Copy(tempResult, M, a, 0, 0, 0, a, c);           // C_21 := M_2
            _blas2.Decrement(tempResult, M, a, c, a, c);

            // M_3 = A_11(B_12 - B_22) 
            _blas2.Copy(tempA, A, Ar0, Ac0, a, b);
            _blas2.Subtract(B, B, tempB, Br0, Bc1, Br1, Bc1, b, c);
            mult_strassen(tempA, tempB, M, a, b, c, false);
            _blas2.Copy(tempResult, M, 0, c, 0, 0, a, c);           // C_12 := M_3
            _blas2.Increment(tempResult, M, a, c, a, c);

            // M_4 = A_22(B_21 - B_11)
            _blas2.Copy(tempA, A, Ar1, Ac1, a, b);
            _blas2.Subtract(B, B, tempB, Br1, Bc0, Br0, Bc0, b, c);
            mult_strassen(tempA, tempB, M, a, b, c, false);
            _blas2.Increment(tempResult, M, a, c);
            _blas2.Increment(tempResult, M, a, 0, a, c);

            // M_5 = (A_11 + A_12)B_22
            _blas2.Add(A, A, tempA, Ar0, Ac0, Ar0, Ac1, a, b);
            _blas2.Copy(tempB, B, Br1, Bc1, b, c);
            mult_strassen(tempA, tempB, M, a, b, c, false);
            _blas2.Decrement(tempResult, M, a, c);
            _blas2.Increment(tempResult, M, 0, c, a, c);

            // M_6 = (A_21 - A_11)(B_11 + B_12)
            _blas2.Subtract(A, A, tempA, Ar1, Ac0, Ar0, Ac0, a, b);
            _blas2.Add(B, B, tempB, Br0, Bc0, Br0, Bc1, b, c);
            mult_strassen(tempA, tempB, M, a, b, c, false);
            _blas2.Increment(tempResult, M, a, c, a, c);

            // M_7 = (A_12 - A_22)(B_21 + B_22) 
            _blas2.Subtract(A, A, tempA, Ar0, Ac1, Ar1, Ac1, a, b);
            _blas2.Add(B, B, tempB, Br1, Bc0, Br1, Bc1, b, c);
            mult_strassen(tempA, tempB, M, a, b, c, false);
            _blas2.Increment(tempResult, M, a, c);

            // Take care of irregular tensor shapes...
            if (m % 2 == 1)
            {
                if (is_simple)
                {
                    append_bottom_row(A, B, tempResult, m, n, p);
                }
                else
                {
                    append_bottom_row(A, B, tempResult, Ar0, Ac0, Br0, Bc0, 0, 0, m, n, p);
                }
            }
            if (n % 2 == 1)
            {
                if (is_simple)
                {
                    append_top_left_submatrix(A, B, tempResult, m, n, p);
                }
                else
                {
                    append_top_left_submatrix(A, B, tempResult, Ar0, Ac0, Br0, Bc0, 0, 0, m, n, p);
                }
            }
            if (p % 2 == 1)
            {
                if (is_simple)
                {
                    append_right_column(A, B, tempResult, m, n, p);
                }
                else
                {
                    append_right_column(A, B, tempResult, Ar0, Ac0, Br0, Bc0, 0, 0, m, n, p);
                }
            }

            if (increment)
            {
                _blas2.Increment(C, tempResult, Cr0, Cc0, m, p);
            }
            else
            {
                _blas2.Copy(C, tempResult, Cr0, Cc0, 0, 0, m, p);
            }
        }

        /// <summary>
        /// Postmultiply A (a x b) by B (b x c), storing the result in 'result' (a x c).
        /// 
        /// This method uses the Winograd variant of Strassen's algorithm, which uses 15 additions
        /// instead of 18 (at the cost of higher memory consumption).
        /// 
        /// This implementation runs approximately 5% faster than the naive Strassen algorithm
        /// implementation, and consumes approximately twice the memory of the native 
        /// Strassen algorithm.
        /// 
        /// Algorithm: 
        /// With A_11, A_12, A_21, A_22, B_11, B_12, B_21, B_22 defined as the same submatrices as per
        /// Strassen, we define:
        /// 
        /// M_1 := A_11 * B_11
        /// M_2 := A_12 * B_21
        /// M_3 := (-A_11 + A_21 + A_22) * (B_11 - B_12 + B_22)
        /// M_4 := (A_11 - A_21) * (-B_12 + B_22)
        /// M_5 := (A_21 + A_22) * (-B_11 + B_12)
        /// M_6 := (A_11 + A_12 - A_21 - A_22) * B_22
        /// M_7 := A_22 * (-B_11 + B_12 + B_21 - B_22)
        /// 
        /// The result of the multiplication is calculated as follows:
        /// 
        /// result_11 := M_1 + M_2
        /// result_12 := M_1 + M_3 + M_5 + M_6
        /// result_21 := M_1 + M_3 + M_4 + M_7
        /// result_22 := M_1 + M_3 + M_4 + M_5
        /// 
        /// </summary>
        /// <param name="A"></param>
        /// <param name="B"></param>
        /// <param name="C"></param>
        /// <param name="m"></param>
        /// <param name="n"></param>
        /// <param name="p"></param>
        private void mult_winograd(T[][] A, T[][] B, T[][] C, int m, int n, int p, bool increment)
        {
            int a = m / 2, 
                b = n / 2, 
                c = p / 2;

            // base case - use simple matrix multiplication
            if (use_naive_algorithm(m, n, p))
            {
                // SIZETODO
                Size3 size = new Size3(m, n, p);
                //_baseMultiplication.Multiply(A, B, C, m, p, n, increment);
                _baseMultiplication.Multiply(A, B, C, size, increment);
                return;
            }

            T[][] tempA = _tempAs[a];
            T[][] tempB = _tempBs[b];
            T[][] M = _Ms[a];
            T[][] tempResult = _tempCs[m];

            //_instruction.clear(tempResult, a, c);

            T[][] A_3 = _A_3s[a];
            T[][] B_3 = _B_3s[b];

            // Stores M_1, M_1 + M_3, M_1 + M_3 + M_4
            T[][] M_temp = _tempMs[a];

            // M_1 := A_11 * B_11
            _blas2.Copy(tempA, A, a, b);
            _blas2.Copy(tempB, B, b, c);
            mult_winograd(tempA, tempB, M_temp, a, b, c, false);
            _blas2.Copy(tempResult, M_temp, a, c); // C_11 = M_1

            // M_2 := A_12 * B_21, C11 := M_1 + M_2
            _blas2.Copy(tempA, A, 0, b, a, b);
            _blas2.Copy(tempB, B, b, 0, b, c);
            mult_winograd(tempA, tempB, M, a, b, c, false);
            _blas2.Increment(tempResult, M, 0, 0, a, c); // C_11 = M_1 + M_2, complete

            // A_3 := A_21 + A_22
            _blas2.Add(A, A, A_3, a, 0, a, b, a, b);
            // B_3 := B_12 - B_11
            _blas2.Subtract(B, B, B_3, 0, c, 0, 0, b, c);

            // M_5 := A_3 * B_3
            mult_winograd(A_3, B_3, M, a, b, c, false);
            _blas2.Copy(tempResult, M, 0, c, 0, 0, a, c); // C_12 = M_5
            _blas2.Copy(tempResult, M, a, c, 0, 0, a, c); // C_22 = M_5

            // B_3 := B_22 - B_3 (B_3 := B_11 - B_12 + B_22)
            _blas2.Subtract(B, B_3, B_3, b, c, 0, 0, b, c);
            // A_3 -= A_11 (A_3 := -A_11 + A_21 + A_22)
            _blas2.Decrement(A_3, A, a, b);

            // M_3 := A_3 * B_3
            mult_winograd(A_3, B_3, M, a, b, c, false);
            _blas2.Increment(M_temp, M, a, c);
            _blas2.Increment(tempResult, M_temp, 0, c, a, c); // C_12 = M_5 + (M_1 + M_3)

            // A_3 := A_12 - A_3 (A_3 := A_11 + A_12 - A_21 - A_22)
            _blas2.Subtract(A, A_3, A_3, 0, b, 0, 0, a, b);
            // M_6 := A_3 * B_22
            _blas2.Copy(tempB, B, b, c, b, c);
            mult_winograd(A_3, tempB, M, a, b, c, false);
            _blas2.Increment(tempResult, M, 0, c, a, c); // C_12 = (M_1 + M_3) + M_5 + M_6, complete

            // B_3 := B_21 - B_3 (B_3 := -B_11 + B_12 + B_21 - B_22)
            _blas2.Subtract(B, B_3, B_3, b, 0, 0, 0, b, c);
            // M_7 := A_22 * B_3;
            _blas2.Copy(tempA, A, a, b, a, b);
            mult_winograd(tempA, B_3, M, a, b, c, false);
            _blas2.Copy(tempResult, M, a, 0, 0, 0, a, c);

            // A_3 := A_11 - A_21
            _blas2.Subtract(A, A, A_3, 0, 0, a, 0, a, b);
            // B_3 := B_22 - B_12
            _blas2.Subtract(B, B, B_3, b, c, 0, c, b, c);
            // M_4 := A_3 * B_3
            mult_winograd(A_3, B_3, M, a, b, c, false);
            _blas2.Increment(M_temp, M, a, c);
            _blas2.Increment(tempResult, M_temp, a, 0, a, c); // C_21 = M_7 + (M_1 + M_3 + M_4), complete
            _blas2.Increment(tempResult, M_temp, a, c, a, c); // C_22 = M_5 + (M_1 + M_3 + M_4), complete

            // If m is odd, append the bottom row
            if (m % 2 == 1)
            {
                append_bottom_row(A, B, tempResult, m, n, p);
            }
            // If n is odd, append to the top-left submatrix
            if (n % 2 == 1)
            {
                append_top_left_submatrix(A, B, tempResult, m, n, p);
            }
            // If p is odd, append the rightmost column
            if (p % 2 == 1)
            {
                append_right_column(A, B, tempResult, m, n, p);
            }

            if (increment)
            {
                _blas2.Increment(C, tempResult, m, p);
            }
            else
            {
                _blas2.Copy(C, tempResult, m, p);
            }
        }
        private void mult_winograd(T[][] A, T[][] B, T[][] C, int Ar0, int Ac0, int Br0, int Bc0, int Cr0, int Cc0, int m, int n, int p, bool increment)
        {
            int a = m / 2,
                b = n / 2,
                c = p / 2,
                Ar1 = Ar0 + a,
                Ac1 = Ac0 + b,
                Br1 = Br0 + b,
                Bc1 = Bc0 + c;

            bool is_simple = Ar0 == 0 && Ac0 == 0 && Br0 == 0 && Bc0 == 0 && Cr0 == 0 && Cc0 == 0;

            // base case - use simple matrix multiplication
            if (use_naive_algorithm(m, n, p))
            {
                // SIZETODO
                Size3 size = new Size3(m, n, p);
                if (is_simple)
                {
                    //_baseMultiplication.Multiply(A, B, C, m, p, n, increment);
                    _baseMultiplication.Multiply(A, B, C, size, increment);
                }
                else
                {
                    //_baseMultiplication.Multiply(A, B, C, Ar0, Ac0, Br0, Bc0, Cr0, Cc0, m, p, n, increment);
                    _baseMultiplication.Multiply(A, B, C, Ar0, Ac0, Br0, Bc0, Cr0, Cc0, size, increment);
                }
                return;
            }

            T[][] tempA = _tempAs[a];
            T[][] M = _Ms[a];
            T[][] tempResult = _tempCs[m];
            T[][] A_3 = _A_3s[a];
            T[][] B_3 = _tempBs[b];

            // Stores M_1, M_1 + M_3, M_1 + M_3 + M_4
            T[][] M_temp = _tempMs[a];

            // M_1 := A_11 * B_11
            mult_winograd(A, B, M_temp, Ar0, Ac0, Br0, Bc0, 0, 0, a, b, c, false);
            _blas2.Copy(tempResult, M_temp, a, c);                                  // C_11 = M_1

            // M_2 := A_12 * B_21, C11 := M_1 + M_2 
            mult_winograd(A, B, M, Ar0, Ac1, Br1, Bc0, 0, 0, a, b, c, false);
            _blas2.Increment(tempResult, M, a, c);                                  // C_11 = M_1 + M_2, complete

            // A_3 := A_21 + A_22
            _blas2.Add(A, A, A_3, Ar1, Ac0, Ar1, Ac1, a, b);
            // B_3 := B_12 - B_11
            _blas2.Subtract(B, B, B_3, Br0, Bc1, Br0, Bc0, b, c);

            // M_5 := A_3 * B_3
            mult_winograd(A_3, B_3, M, a, b, c, false);
            _blas2.Copy(tempResult, M, 0, c, 0, 0, a, c);                           // C_12 = M_5
            _blas2.Copy(tempResult, M, a, c, 0, 0, a, c);                           // C_22 = M_5

            // B_3 := B_22 - B_3 (B_3 := B_11 - B_12 + B_22)
            _blas2.Subtract(B, B_3, B_3, Br1, Bc1, 0, 0, b, c);
            // A_3 -= A_11 (A_3 := -A_11 + A_21 + A_22)
            _blas2.Copy(tempA, A, Ar0, Ac0, a, b);
            _blas2.Decrement(A_3, tempA, a, b);

            // M_3 := A_3 * B_3
            mult_winograd(A_3, B_3, M, a, b, c, false);
            _blas2.Increment(M_temp, M, a, c);
            _blas2.Increment(tempResult, M_temp, 0, c, a, c); // C_12 = M_5 + (M_1 + M_3)

            // A_3 := A_12 - A_3 (A_3 := A_11 + A_12 - A_21 - A_22)
            _blas2.Subtract(A, A_3, A_3, Ar0, Ac1, 0, 0, a, b);
            // M_6 := A_3 * B_22
            mult_winograd(A_3, B, M, 0, 0, Br1, Bc1, 0, 0, a, b, c, false);
            _blas2.Increment(tempResult, M, 0, c, a, c); // C_12 = (M_1 + M_3) + M_5 + M_6, complete

            // B_3 := B_21 - B_3 (B_3 := -B_11 + B_12 + B_21 - B_22)
            _blas2.Subtract(B, B_3, B_3, Br1, Bc0, 0, 0, b, c);
            // M_7 := A_22 * B_3;
            mult_winograd(A, B_3, M, Ar1, Ac1, 0, 0, 0, 0, a, b, c, false);
            _blas2.Copy(tempResult, M, a, 0, 0, 0, a, c);

            // A_3 := A_11 - A_21
            _blas2.Subtract(A, A, A_3, Ar0, Ac0, Ar1, Ac0, a, b);
            // B_3 := B_22 - B_12
            _blas2.Subtract(B, B, B_3, Br1, Bc1, Br0, Bc1, b, c);
            // M_4 := A_3 * B_3
            mult_winograd(A_3, B_3, M, a, b, c, false);
            _blas2.Increment(M_temp, M, a, c);
            _blas2.Increment(tempResult, M_temp, a, 0, a, c); // C_21 = M_7 + (M_1 + M_3 + M_4), complete
            _blas2.Increment(tempResult, M_temp, a, c, a, c); // C_22 = M_5 + (M_1 + M_3 + M_4), complete

            // If size is odd, append the bottom row
            if (m % 2 == 1)
            {
                if (is_simple)
                {
                    append_bottom_row(A, B, tempResult, m, n, p);
                }
                else
                {
                    append_bottom_row(A, B, tempResult, Ar0, Ac0, Br0, Bc0, 0, 0, m, n, p);
                }
            }
            if (n % 2 == 1)
            {
                if (is_simple)
                {
                    append_top_left_submatrix(A, B, tempResult, m, n, p);
                }
                else
                {
                    append_top_left_submatrix(A, B, tempResult, Ar0, Ac0, Br0, Bc0, 0, 0, m, n, p);
                }
            }
            if (p % 2 == 1)
            {
                if (is_simple)
                {
                    append_right_column(A, B, tempResult, m, n, p);
                }
                else
                {
                    append_right_column(A, B, tempResult, Ar0, Ac0, Br0, Bc0, 0, 0, m, n, p);
                }
            }

            if (increment)
            {
                _blas2.Increment(C, tempResult, Cr0, Cc0, m, p);
            }
            else
            {
                _blas2.Copy(C, tempResult, Cr0, Cc0, 0, 0, m, p);
            }
        }


        /*
        /// <summary>
        /// Second parallel implementation that parallelizes within each instruction
        /// </summary>
        /// <param name="A"></param>
        /// <param name="B"></param>
        /// <param name="result"></param>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="c"></param>
        private void mult_winograd_parallel(T[][] A, T[][] B, T[][] result, int a, int b, int c)
        {
            int half_a = a / 2, half_b = b / 2, half_c = c / 2;

            // base case - use simple matrix multiplication
            if (use_naive_algorithm(a, b, c))
            {
                _instruction.multiply_naive(A, B, result, a, c, b, false); // TODO changed
                return;
            }

            T[][] tempA = _tempAs[half_a];
            T[][] tempB = _tempBs[half_b];
            T[][] M = _Ms[half_a];
            T[][] tempResult = _tempCs[a];

            //_instruction.clear(tempResult, a, c);

            T[][] A_3 = _A_3s[half_a];
            T[][] B_3 = _B_3s[half_b];

            // Stores M_1, M_1 + M_3, M_1 + M_3 + M_4
            T[][] M_temp = _tempMs[half_a];

            // M_1 := A_11 * B_11
            copy_parallel(tempA, A, 0, 0, 0, 0, half_a, half_b);
            copy_parallel(tempB, B, 0, 0, 0, 0, half_b, half_c);
            mult_winograd_parallel(tempA, tempB, M_temp, half_a, half_b, half_c);
            copy_parallel(tempResult, M_temp, 0, 0, 0, 0, half_a, half_c); // C_11 = M_1

            // M_2 := A_12 * B_21, C11 := M_1 + M_2
            copy_parallel(tempA, A, 0, 0, 0, half_b, half_a, half_b);
            copy_parallel(tempB, B, 0, 0, half_b, 0, half_b, half_c);
            mult_winograd_parallel(tempA, tempB, M, half_a, half_b, half_c);
            _instruction.increment_parallel(tempResult, M, 0, 0, half_a, half_c); // C_11 = M_1 + M_2, complete

            // A_3 := A_21 + A_22
            _instruction.add_parallel(A, A, A_3, half_a, 0, half_a, half_b, half_a, half_b);
            // B_3 := B_12 - B_11
            _instruction.subtract_parallel(B, B, B_3, 0, half_c, 0, 0, half_a, half_b);

            // M_5 := A_3 * B_3
            mult_winograd_parallel(A_3, B_3, M, half_a, half_b, half_c);
            copy_parallel(tempResult, M, 0, half_b, 0, 0, half_a, half_b); // C_12 = M_5
            copy_parallel(tempResult, M, half_a, half_b, 0, 0, half_a, half_b); // C_22 = M_5

            // B_3 := B_22 - B_3 (B_3 := B_11 - B_12 + B_22)
            _instruction.subtract_parallel(B, B_3, B_3, half_b, half_c, 0, 0, half_b, half_c);
            // A_3 -= A_11 (A_3 := -A_11 + A_21 + A_22)
            _instruction.decrement_parallel(A_3, A, 0, 0, half_a, half_b);

            // M_3 := A_3 * B_3
            mult_winograd_parallel(A_3, B_3, M, half_a, half_b, half_c);
            _instruction.increment_parallel(M_temp, M, 0, 0, half_a, half_c);
            _instruction.increment_parallel(tempResult, M_temp, 0, half_c, half_a, half_c); // C_12 = M_5 + (M_1 + M_3)

            // A_3 := A_12 - A_3 (A_3 := A_11 + A_12 - A_21 - A_22)
            _instruction.subtract_parallel(A, A_3, A_3, 0, half_b, 0, 0, half_a, half_b);
            // M_6 := A_3 * B_22
            copy_parallel(tempB, B, 0, 0, half_b, half_c, half_b, half_c);
            mult_winograd_parallel(A_3, tempB, M, half_a, half_b, half_c);
            _instruction.increment_parallel(tempResult, M, 0, half_c, half_a, half_c); // C_12 = (M_1 + M_3) + M_5 + M_6, complete

            // B_3 := B_21 - B_3 (B_3 := -B_11 + B_12 + B_21 - B_22)
            _instruction.subtract_parallel(B, B_3, B_3, half_b, 0, 0, 0, half_b, half_c);
            // M_7 := A_22 * B_3;
            copy_parallel(tempA, A, 0, 0, half_a, half_b, half_a, half_b);
            mult_winograd_parallel(tempA, B_3, M, half_a, half_b, half_c);
            copy_parallel(tempResult, M, half_a, 0, 0, 0, half_a, half_c);

            // A_3 := A_11 - A_21
            _instruction.subtract_parallel(A, A, A_3, 0, 0, half_a, 0, half_a, half_b);
            // B_3 := B_22 - B_12
            _instruction.subtract_parallel(B, B, B_3, half_b, half_c, 0, half_c, half_b, half_c);
            // M_4 := A_3 * B_3
            mult_winograd_parallel(A_3, B_3, M, half_a, half_b, half_c);
            _instruction.increment_parallel(M_temp, M, 0, 0, half_a, half_c);
            _instruction.increment_parallel(tempResult, M_temp, half_a, 0, half_a, half_c); // C_21 = M_7 + (M_1 + M_3 + M_4), complete
            _instruction.increment_parallel(tempResult, M_temp, half_a, half_c, half_a, half_c); // C_22 = M_5 + (M_1 + M_3 + M_4), complete

            copy_parallel(result, tempResult, 0, 0, 0, 0, a, c);
        }

        /// <summary>
        /// The original parallel implementation of Strassen's.. not used for now because it does not match the 
        /// other implementation's performance
        /// </summary>
        private void mult_winograd_parallel_ss(T[][] A, T[][] B, T[][] result, int a, int b, int c)
        {
            int half_a = a / 2, half_b = b / 2, half_c = c / 2;

            // base case - use simple matrix multiplication
            if (use_naive_algorithm(a, b, c))
            {
                _instruction.multiply_naive(A, B, result, a, c, b, false); // TODO parallel
                return;
            }

            T[][] tempA = _tempAs[half_a];
            T[][] tempB = _tempBs[half_b];
            T[][] M = _Ms[half_a];
            T[][] tempResult = _tempCs[a];

            //_instruction.clear(tempResult, a, c);

            T[][] A_3 = _A_3s[half_a];
            T[][] B_3 = _B_3s[half_b];

            // Stores M_1, M_1 + M_3, M_1 + M_3 + M_4
            T[][] M_temp = _tempMs[half_a];

            // M_1 := A_11 * B_11
            Parallel.Invoke(
                () => copy(tempA, A, 0, 0, half_a, half_b),
                () => copy(tempB, B, 0, 0, half_b, half_c)
                );

            mult_winograd_parallel(tempA, tempB, M_temp, half_a, half_b, half_c);
            Parallel.Invoke(
                () => copy(tempResult, M_temp, 0, 0, half_a, half_c), // C_11 = M_1
                () => copy(tempA, A, 0, half_b, half_a, half_b),
                () => copy(tempB, B, half_b, 0, half_b, half_c)
                );

            // M_2 := A_12 * B_21, C11 := M_1 + M_2
            mult_winograd_parallel(tempA, tempB, M, half_a, half_b, half_c);
            Parallel.Invoke(
                () => _instruction.increment(tempResult, M, 0, 0, half_a, half_c), // C_11 = M_1 + M_2, complete
                () => _instruction.add(A, A, A_3, half_a, 0, half_a, half_b, half_a, half_b), // A_3 := A_21 + A_22
                () => _instruction.subtract(B, B, B_3, 0, half_c, 0, 0, half_a, half_b) // B_3 := B_12 - B_11
                );

            // M_5 := A_3 * B_3
            mult_winograd_parallel(A_3, B_3, M, half_a, half_b, half_c);
            Parallel.Invoke(
                () => copy(tempResult, M, 0, half_b, 0, 0, half_a, half_b), // C_12 = M_5
                () => copy(tempResult, M, half_a, half_b, 0, 0, half_a, half_b), // C_22 = M_5
                () => _instruction.subtract(B, B_3, B_3, half_b, half_c, 0, 0, half_b, half_c), // B_3 := B_22 - B_3 (B_3 := B_11 - B_12 + B_22)
                () => _instruction.decrement(A_3, A, 0, 0, half_a, half_b) // A_3 -= A_11 (A_3 := -A_11 + A_21 + A_22)
                );

            // M_3 := A_3 * B_3
            mult_winograd_parallel(A_3, B_3, M, half_a, half_b, half_c);
            Parallel.Invoke(
                () => _instruction.increment(M_temp, M, 0, 0, half_a, half_c),
                () => _instruction.increment(tempResult, M_temp, 0, half_c, half_a, half_c), // C_12 = M_5 + (M_1 + M_3)
                () => _instruction.subtract(A, A_3, A_3, 0, half_b, 0, 0, half_a, half_b), // A_3 := A_12 - A_3 (A_3 := A_11 + A_12 - A_21 - A_22)
                () => copy(tempB, B, half_b, half_c, half_b, half_c) // M_6 := A_3 * B_22
                );

            mult_winograd_parallel(A_3, tempB, M, half_a, half_b, half_c);
            Parallel.Invoke(
                () => _instruction.increment(tempResult, M, 0, half_c, half_a, half_c), // C_12 = (M_1 + M_3) + M_5 + M_6, complete
                () => _instruction.subtract(B, B_3, B_3, half_b, 0, 0, 0, half_b, half_c), // B_3 := B_21 - B_3 (B_3 := -B_11 + B_12 + B_21 - B_22)
                () => copy(tempA, A, half_a, half_b, half_a, half_b) // M_7 := A_22 * B_3;
                );

            mult_winograd_parallel(tempA, B_3, M, half_a, half_b, half_c);
            Parallel.Invoke(
                () => copy(tempResult, M, half_a, 0, 0, 0, half_a, half_c),
                () => _instruction.subtract(A, A, A_3, 0, 0, half_a, 0, half_a, half_b), // A_3 := A_11 - A_21
                () => _instruction.subtract(B, B, B_3, half_b, half_c, 0, half_c, half_b, half_c) // B_3 := B_22 - B_12
                );

            // M_4 := A_3 * B_3
            mult_winograd_parallel(A_3, B_3, M, half_a, half_b, half_c);
            Parallel.Invoke(
                () => _instruction.increment(M_temp, M, 0, 0, half_a, half_c),
                () => _instruction.increment(tempResult, M_temp, half_a, 0, half_a, half_c), // C_21 = M_7 + (M_1 + M_3 + M_4), complete
                () => _instruction.increment(tempResult, M_temp, half_a, half_c, half_a, half_c), // C_22 = M_5 + (M_1 + M_3 + M_4), complete
                () => copy(result, tempResult, 0, 0, a, c)
                );
        }*/

        /// <summary>
        /// Complete the bottom row of a <2k + 1, n, p> multiplication, where the (2k + 1)-th row
        /// is not complete because of Strassen. 
        /// Here, a = 2k + 1, b = n, c = p.
        /// </summary>
        private void append_bottom_row(T[][] A, T[][] B, T[][] result, int m, int n, int p)
        {
            int submatrix_rows = (m / 2) * 2,
                submatrix_cols = (p / 2) * 2;

            T[] A_last = A[submatrix_rows];
            T[] result_row = result[submatrix_rows];
            for (int j = 0; j < submatrix_cols; ++j)
            {
                result_row[j] = _blas1.XTA(A_last, B, j, 0, n);
            }
        }
        private void append_bottom_row(T[][] A, T[][] B, T[][] result, int A_row_start, int A_col_start, int B_row_start, int B_col_start, int result_row_start, int result_col_start, int m, int n, int p)
        {
            int submatrix_rows = (m / 2) * 2,
                A_col_end = A_col_start + n,
                B_col_end = B_col_start + (p / 2) * 2,
                B_A_offset = B_row_start - A_col_start,
                result_B_col_offset = result_col_start - B_col_start;

            int k, j;
            T[] A_last = A[A_row_start + submatrix_rows],
                result_row = result[result_row_start + submatrix_rows];

            var provider = _blas1.Provider;
            for (j = B_col_start; j < B_col_end; ++j)
            {
                T sum = provider.Zero;
                for (k = A_col_start; k < A_col_end; ++k)
                {
                    sum = provider.Add(sum, provider.Multiply(A_last[k], B[k + B_A_offset][j]));
                }
                result_row[j + result_B_col_offset] = sum;
            }
        }
        /// <summary>
        /// Complete the bottom row of a <a, b, c> multiplication, where both or either of a, b is 
        /// odd and incomplete due to Strassen's multiplication.
        /// </summary>
        private void append_top_left_submatrix(T[][] A, T[][] B, T[][] result, int m, int n, int p)
        {
            int submatrix_rows = (m / 2) * 2, submatrix_cols = (p / 2) * 2, submatrix_terms = n - 1;

            T[] B_last = B[submatrix_terms];
            for (int i = 0; i < submatrix_rows; ++i)
            {
                _blas1.AXPY(result[i], B_last, A[i][submatrix_terms], 0, submatrix_cols);
            }
        }
        private void append_top_left_submatrix(T[][] A, T[][] B, T[][] result, int Ar, int Ac, int Br, int Bc, int Cr, int Cc, int m, int n, int p)
        {
            int submatrix_rows = (m / 2) * 2,
                submatrix_cols = (p / 2) * 2,
                Cc_end = Cc + submatrix_cols,
                B_row = Br + n - 1,
                A_col = Ac + n - 1,
                BC_offset = Bc - Cc;

            T[] B_last = B[B_row];
            for (int i = 0; i < submatrix_rows; ++i)
            {
                _blas1.AXPY(result[i + Cr], B_last, A[i + Ar][A_col], Cc, Cc_end, BC_offset);
            }
        }

        private void append_right_column(T[][] A, T[][] B, T[][] result, int m, int n, int p)
        {
            int last_col = p - 1, i;
            for (i = 0; i < m; ++i)
            {
                result[i][last_col] = _blas1.XTA(A[i], B, last_col, 0, n);
            }
        }
        private void append_right_column(T[][] A, T[][] B, T[][] result, int A_row_start, int A_col_start, int B_row_start, int B_col_start, int result_row_start, int result_col_start, int m, int n, int p)
        {
            int B_col = B_col_start + p - 1,
                result_col = result_col_start + p - 1,
                A_col_end = A_col_start + n,
                B_offset = B_row_start - A_col_start;

            for (int i = 0; i < m; ++i)
            {
                result[i + result_row_start][result_col] = _blas1.XTA(A[i + A_row_start], B, B_col, A_col_start, A_col_end, B_offset);
            }
        }

    }
}
