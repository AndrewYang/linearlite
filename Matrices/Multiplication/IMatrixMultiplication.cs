﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Matrices
{
    /// <summary>
    /// <p>Interface for all matrix multiplication algorithms for dense matrices over type <txt>T</txt>.</p>
    /// <p>Implemented by the following classes.</p>
    /// <ul>
    /// <li><a href="DivideAndConquerMMAlgorithm_T_.html">DivideAndConquerMMAlgorithm&ltT&gt</a></li>
    /// <li><a href="PeanoCurveMMAlgorithm_T_.html">PeanoCurveMMAlgorithm&ltT&gt</a></li>
    /// <li><a href="SquareStrassenMMAlgorithm_T_.html">SquareStrassenMMAlgorithm&ltT&gt</a></li>
    /// </ul>
    /// 
    /// <br/>
    /// <h2>Usage example</h2>
    /// <p>
    /// Although the interface implementations can be used directly to multiply two matrices represented as 
    /// jagged arrays, it is cleaner to use the <txt>Multiply</txt> extension methods, example:
    /// </p>
    /// <pre><code class="cs">
    /// DenseMatrix&ltdouble&gt A = DenseMatrix.Random&ltdouble&gt(1000, 1000), B = DenseMatrix.Random&ltdouble&gt(1000, 1000);
    /// 
    /// // Example: multiply using a single threaded, pure-C# divide-and-conquer algorithm
    /// var algo = new DivideAndConquerMMAlgorithm(new DoubleLinearAlgebraProvider(), 32);
    /// DenseMatrix&ltdouble&gt AB = A.Multiply(B, algo);
    /// 
    /// // Example: multiply using Strassen's algorithm
    /// var strassen = new SquareStrassenMMAlgorithm&ltdouble&gt(1000);
    /// DenseMatrix&ltdouble&gt AB1 = A.Multiply(B, strassen);
    /// 
    /// // Under most circumstances we can improve performance by providing a workspace to store B^T. 
    /// DenseMatrix&ltdouble&gt workspace = new DenseMatrix&ltdouble&gt(1000, 1000);
    /// DenseMatrix&ltdouble&gt AB2 = A.Multiply(B, workspace, algo);
    /// 
    /// // If no algorithm is specified, the default is naive matrix multiplication.
    /// DenseMatrix&ltdouble&gt AB3 = A.Multiply(B);
    /// 
    /// </code></pre>
    /// 
    /// <br/>
    /// <h2>Performance tips</h2>
    /// <p>
    /// For performance-critical applications, there are several things we can do to increase computation speed (beyond
    /// the choice of algorithm).
    /// </p>
    /// <ol>
    /// <li>
    /// <h4>Use parallel implementations</h4> 
    /// Most matrix multiplication algorithms have their respective 
    /// parallel implementations. E.g. <txt>ParallelDivideAndConquerMMAlgorithm&ltT&gt</txt> vs <txt>DivideAndConquerMMAlgorithm&ltT&gt</txt>.
    /// </li>
    /// <li>
    /// <h4>Use a workspace</h4> By default all multiplication algorithms will run in 'memory-starved' mode, where 
    /// the minimum additional memory is consumed to store intermediary products of the multiplication. Most implementations
    /// will be more efficient if a pre-initialized workspace of the same size as $B^T$ is passed in as a parameter. 
    /// <br/>
    /// <b>Example:</b>
    /// <pre><code class="cs">
    /// // Under most circumstances we can improve performance by providing a workspace to store B^T. 
    /// DenseMatrix&ltdouble&gt workspace = new DenseMatrix&ltdouble&gt(1000, 1000);
    /// DenseMatrix&ltdouble&gt AB = A.Multiply(B, workspace, algo);
    /// 
    /// </code></pre>
    /// </li>
    /// <li>
    /// <h4>Use a different linear algebra provider</h4>
    /// The pure-C# linear-algebra library/implementation is chosen by default. Managed C# tends to be slower than unmanaged code.
    /// SIMD (AVX, AVX2), MKL, CUDA, OpenCL and distributed libraries are available.
    /// <br/>
    /// <b>Example:</b> using a SIMD linear algebra provider
    /// <pre><code class="cs">
    /// var algo = new DivideAndConquerMMAlgorithm(new DoubleSIMDLinearAlgebraProvider(), 32);
    /// DenseMatrix&ltdouble&gt AB = A.Multiply(B, algo);
    /// 
    /// </code></pre>
    /// </li>
    /// </ol>
    /// </summary>
    /// <cat>linear-algebra</cat>
    public interface IMatrixMultiplication<T>
    {
        /// <summary>
        /// <p>Solves a <txt>&ltrows, N, columns&gt</txt> multiplication problem.</p>
        /// 
        /// <p>
        /// Multiply the top-left rows $\times$ N submatrix of matrix <txt>A</txt>, with the top-left 
        /// N $\times$ columns submatrix of matrix <txt>B</txt>, storing the result in the top-left 
        /// <txt>rows</txt> $\times$ <txt>columns</txt> submatrix of <txt>result</txt>. The other entries 
        /// of <txt>result</txt>, as well as <txt>A</txt> and <txt>B</txt>, will remain unchanged.
        /// </p>
        /// 
        /// <p>
        /// If <txt>increment</txt> is <txt>true</txt>, then the result matrix will be incremented by
        /// <txt>AB</txt>, otherwise it will be set to the value of <txt>AB</txt>.
        /// </p>
        /// </summary>
        /// <param name="A">The left matrix to be multiplied.</param>
        /// <param name="B">The right matrix to be multiplied.</param>
        /// <param name="result">The matrix in which the multiplication result will be stored.</param>
        /// <param name="rows">The number of rows of <txt>A</txt> to include in the multiplication.</param>
        /// <param name="columns">The number of columns of <txt>B</txt> to include in the multiplication.</param>
        /// <param name="N">The number of columns of <txt>A</txt>, and the number of rows of <txt>B</txt>, to include in the multiplication.</param>
        /// <param name="increment">If <txt>true</txt>, the matrix <txt>result</txt> will be incremented 
        /// by product <txt>AB</txt>. Otherwise <txt>result</txt> will be overwritten by the product.</param>
        void Multiply(T[][] A, T[][] B, T[][] result, Size3 size, bool increment);

        /// <summary>
        /// <p>
        /// Solves a <txt>&ltrows, N, cols&gt</txt> block-matrix multiplication problem.
        /// </p>
        /// 
        /// <p>
        /// Multiply a continuous block from matrix $A$ with a continuous block from matrix $B$, storing the result in 
        /// a continuous block in <txt>result</txt>. 
        /// </p>
        /// </summary>
        /// <param name="A">The left matrix to be multiplied.</param>
        /// <param name="B">The right matrix to be multiplied.</param>
        /// <param name="result">The matrix in which the multiplication result will be stored.</param>
        /// <param name="A_start_row">The first row of $A$ to multiply.</param>
        /// <param name="A_start_col">The first column of $A$ to multiply.</param>
        /// <param name="B_start_row">The first row of $B$ to multiply.</param>
        /// <param name="B_start_col">The first column of $B$ to multiply.</param>
        /// <param name="C_start_row">The first row of the result matrix to store the results.</param>
        /// <param name="C_start_col">The first column of the result matrix to store the results.</param>
        /// <param name="rows">The number of rows in the resulting multiplied block.</param>
        /// <param name="cols">The number of columns in the resulting multiplied block.</param>
        /// <param name="N">The number of cols in $A$ or rows in $B$ to multiply together.</param>
        /// <param name="increment">If <txt>true</txt>, result will be incremented by $AB$. If <txt>false</txt>, then result will be set to $AB$.</param>
        void Multiply(T[][] A, T[][] B, T[][] result,
            int A_start_row, int A_start_col,
            int B_start_row, int B_start_col,
            int C_start_row, int C_start_col,
            Size3 size, bool increment);
    }
}
