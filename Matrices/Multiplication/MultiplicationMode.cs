﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LinearNet.Matrices.Multiplication
{
    /// <summary>
    /// The mode of multiplication - most matrix multiplication algorithms face a time-space 
    /// trade-off. This enumeration allows us to specify which of the two dimensions to 
    /// priotise. 
    /// </summary>
    public enum MultiplicationMode
    {
        /// <summary>
        /// Minimise the memory consumption of the matrix multiplication algorithm
        /// </summary>
        OPTIMIZE_MEMORY,

        /// <summary>
        /// Maximise the speed of the matrix multiplication algorithm
        /// </summary>
        OPTIMIZE_SPEED
    }
}
