﻿using LinearNet.Providers;
using LinearNet.Providers.LinearAlgebra;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Matrices.Multiplication
{
    /// <summary>
    /// Implementation of matrix multiplication via a 'naive' algorithm.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <cat>linear-algebra</cat>
    public sealed class NaiveMatrixMultiplication<T> : IMatrixMultiplication<T>
    {
        private readonly IDenseBLAS1<T> _provider;
        private T[][] _workspace;

        /// <summary>
        /// The multiplication mode, used to specify whether memory efficiency or speed should be prioritized.
        /// </summary>
        public MultiplicationMode Mode { get; private set; }

        /// <summary>
        /// Create a new naive matrix multiplication algorithm for type <txt>T</txt>.
        /// </summary>
        /// <param name="provider">A level-1 BLAS provider for type <txt>T</txt></param>
        /// <param name="mode">The multiplication mode.</param>
        /// <param name="workspace">A workspace of at least the same size as $B^T$ in the multiplication $AB$.</param>
        public NaiveMatrixMultiplication(
            IDenseBLAS1Provider<T> provider,
            MultiplicationMode mode = MultiplicationMode.OPTIMIZE_SPEED,
            T[][] workspace = null) 
            : this(provider as IDenseBLAS1<T>, mode, workspace) 
        { 
        }

        internal NaiveMatrixMultiplication(
            IDenseBLAS1<T> provider,
            MultiplicationMode mode = MultiplicationMode.OPTIMIZE_SPEED,
            T[][] workspace = null)
        {
            _provider = provider;
            Mode = mode;
            _workspace = workspace;
        }


        /// <summary>
        /// Copy B^T into Bt, where (B_start_row, B_start_col) maps to (0, 0);
        /// </summary>
        internal static void TransposeAndShift(T[][] B, T[][] Bt, int B_start_row, int B_start_col, int rows, int cols, bool parallel)
        {
            if (parallel)
            {
                int batchSize = 16;
                Parallel.ForEach(Partitioner.Create(0, cols, batchSize), range =>
                {
                    int min = range.Item1, max = range.Item2;
                    for (int i = min; i < max; ++i)
                    {
                        T[] Bt_i = Bt[i];
                        int _i = i + B_start_col,
                            _j = B_start_row;
                        for (int j = 0; j < rows; ++j, ++_j)
                        {
                            Bt_i[j] = B[_j][_i];
                        }
                    }
                });
            }
            else
            {
                for (int i = 0; i < cols; ++i)
                {
                    T[] Bt_i = Bt[i];
                    int _i = i + B_start_col,
                        _j = B_start_row;
                    for (int j = 0; j < rows; ++j, ++_j)
                    {
                        Bt_i[j] = B[_j][_i];
                    }
                }
            }
        }

        /// <summary>
        /// Copy the transpose of the top-left (rows x columns) submatrix of matrix B into Bt_workspace
        /// </summary>
        /// <param name="B"></param>
        /// <param name="Bt_workspace"></param>
        /// <param name="rows"></param>
        /// <param name="columns"></param>
        internal static void Transpose(T[][] B, T[][] Bt_workspace, int rows, int columns)
        {
            int i, j;
            for (i = 0; i < rows; ++i)
            {
                T[] row = B[i];
                for (j = 0; j < columns; ++j)
                {
                    Bt_workspace[j][i] = row[j];
                }
            }
        }

        public void Multiply(T[][] A, T[][] B, T[][] result, Size3 size, bool increment)
        {
            if (Mode == MultiplicationMode.OPTIMIZE_SPEED)
            {
                // Check if workspace requires expansion
                if (_workspace == null || _workspace.Length < size.P || _workspace[0].Length < size.N)
                {
                    _workspace = MatrixInternalExtensions.JMatrix<T>(size.P, size.N);
                }

                MultiplyWithWorkspace(A, B, _workspace, result, size.M, size.P, size.N, increment);
            }
            else
            {
                MultiplyWithoutWorkspace(A, B, result, size.M, size.P, size.N, increment);
            }
        }
        private void MultiplyWithoutWorkspace(T[][] A, T[][] B, T[][] result, int rows, int columns, int N, bool increment)
        {
            if (A == null) throw new ArgumentNullException(nameof(A));
            if (B == null) throw new ArgumentNullException(nameof(B));
            if (result == null) throw new ArgumentNullException(nameof(result));

            int i, j;
            if (increment)
            {
                IProvider<T> p = _provider.Provider;
                for (i = 0; i < rows; ++i)
                {
                    T[] A_i = A[i], result_i = result[i];
                    for (j = 0; j < columns; ++j)
                    {
                        result_i[j] = p.Add(result_i[j], _provider.XTA(A_i, B, j, 0, N));
                    }
                }
            }
            else
            {
                for (i = 0; i < rows; ++i)
                {
                    T[] A_i = A[i], result_i = result[i];
                    for (j = 0; j < columns; ++j)
                    {
                        result_i[j] = _provider.XTA(A_i, B, j, 0, N);
                    }
                }
            }
        }
        private void MultiplyWithWorkspace(T[][] A, T[][] B, T[][] Bt_workspace, T[][] result, int rows, int columns, int N, bool increment)
        {
            if (A == null) throw new ArgumentNullException(nameof(A));
            if (B == null) throw new ArgumentNullException(nameof(B));
            if (result == null) throw new ArgumentNullException(nameof(result));

            Transpose(B, Bt_workspace, N, columns);

            int i, j;
            if (increment)
            {
                IProvider<T> p = _provider.Provider;
                for (i = 0; i < rows; ++i)
                {
                    T[] A_i = A[i], result_i = result[i];
                    for (j = 0; j < columns; ++j)
                    {
                        result_i[j] = p.Add(result_i[j], _provider.DOT(A_i, Bt_workspace[j], 0, N));
                    }
                }
            }
            else
            {
                for (i = 0; i < rows; ++i)
                {
                    T[] A_i = A[i], result_i = result[i];
                    for (j = 0; j < columns; ++j)
                    {
                        result_i[j] = _provider.DOT(A_i, Bt_workspace[j], 0, N);
                    }
                }
            }
        }

        public void Multiply(T[][] A, T[][] B, T[][] result, int A_start_row, int A_start_col, int B_start_row, int B_start_col, int C_start_row, int C_start_col, Size3 size, bool increment)
        {
            if (Mode == MultiplicationMode.OPTIMIZE_SPEED)
            {
                // Check if workspace requires expansion
                if (_workspace == null || _workspace.Length < size.P || _workspace[0].Length < size.N)
                {
                    _workspace = MatrixInternalExtensions.JMatrix<T>(size.P, size.N);
                }

                MultiplyWithWorkspace(A, B, _workspace, result, A_start_row, A_start_col, B_start_row, B_start_col, C_start_row, C_start_col, size.M, size.P, size.N, increment);
            }
            else
            {
                MultiplyWithoutWorkspace(A, B, result, A_start_row, A_start_col, B_start_row, B_start_col, C_start_row, C_start_col, size.M, size.P, size.N, increment);
            }
        }
        private void MultiplyWithoutWorkspace(T[][] A, T[][] B, T[][] result, int A_start_row, int A_start_col, int B_start_row, int B_start_col, int C_start_row, int C_start_col, int rows, int cols, int N, bool increment)
        {
            if (A == null) throw new ArgumentNullException(nameof(A));
            if (B == null) throw new ArgumentNullException(nameof(B));
            if (result == null) throw new ArgumentNullException(nameof(result));

            int A_last_row = A_start_row + rows,
                A_last_col = A_start_col + N,
                B_last_col = B_start_col + cols,
                CA_offset = C_start_row - A_start_row,
                BA_offset = B_start_row - A_start_col,
                CB_offset = C_start_col - B_start_col;

            int i, j, k;

            IProvider<T> p = _provider.Provider;
            T zero = p.Zero;

            if (increment)
            {
                for (i = A_start_row; i < A_last_row; ++i)
                {
                    T[] A_i = A[i], result_i = result[i + CA_offset];
                    for (j = B_start_col; j < B_last_col; ++j)
                    {
                        T sum = zero;
                        for (k = A_start_col; k < A_last_col; ++k)
                        {
                            sum = p.Add(sum, p.Multiply(A_i[k], B[k + BA_offset][j]));
                        }
                        result_i[j + CB_offset] = p.Add(result_i[j + CB_offset], sum);
                    }
                }
            }
            else
            {
                for (i = A_start_row; i < A_last_row; ++i)
                {
                    T[] A_i = A[i], result_i = result[i + CA_offset];
                    for (j = B_start_col; j < B_last_col; ++j)
                    {
                        T sum = zero;
                        for (k = A_start_col; k < A_last_col; ++k)
                        {
                            sum = p.Add(sum, p.Multiply(A_i[k], B[k + BA_offset][j]));
                        }
                        result_i[j + CB_offset] = sum;
                    }
                }
            }
        }
        private void MultiplyWithWorkspace(T[][] A, T[][] B, T[][] Bt_workspace, T[][] result, int A_start_row, int A_start_col, int B_start_row, int B_start_col, int C_start_row, int C_start_col, int rows, int cols, int N, bool increment)
        {
            if (A == null) throw new ArgumentNullException(nameof(A));
            if (B == null) throw new ArgumentNullException(nameof(B));
            if (result == null) throw new ArgumentNullException(nameof(result));

            int A_last_row = A_start_row + rows,
                CA_offset = C_start_row - A_start_row, i, j;

            IProvider<T> p = _provider.Provider;
            TransposeAndShift(B, Bt_workspace, B_start_row, B_start_col, N, cols, false);

            if (increment)
            {
                for (i = A_start_row; i < A_last_row; ++i)
                {
                    T[] A_i = A[i], result_i = result[i + CA_offset];
                    for (j = 0; j < cols; ++j)
                    {
                        int k = j + C_start_col;
                        result_i[k] = p.Add(result_i[k], _provider.DOT(Bt_workspace[j], A_i, 0, N, A_start_col));
                    }
                }
            }
            else
            {
                for (i = A_start_row; i < A_last_row; ++i)
                {
                    T[] A_i = A[i], result_i = result[i + CA_offset];
                    for (j = 0; j < cols; ++j)
                    {
                        result_i[j + C_start_col] = _provider.DOT(Bt_workspace[j], A_i, 0, N, A_start_col);
                    }
                }
            }
        }

    }
}
