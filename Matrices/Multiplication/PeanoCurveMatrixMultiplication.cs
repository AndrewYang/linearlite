﻿using LinearNet.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Matrices.Multiplication
{
    public class PeanoCurveMatrixMultiplication<T> : IMatrixMultiplication<T>
    {
        private readonly IDenseBLAS1<T> _provider;
        private readonly int _threshold;

        public PeanoCurveMatrixMultiplication(IDenseBLAS1Provider<T> provider, int thresholdSize)
            : this(provider as IDenseBLAS1<T>, thresholdSize) { }

        internal PeanoCurveMatrixMultiplication(IDenseBLAS1<T> provider, int thresholdSize)
        {
            _provider = provider ?? throw new ArgumentNullException(nameof(provider));
            _threshold = 3 * thresholdSize;
        }

        public void Multiply(T[][] A, T[][] B, T[][] result, Size3 size, bool increment)
        {
            throw new NotImplementedException();
        }
        public void Multiply(T[][] A, T[][] B, T[][] result, int A_start_row, int A_start_col, int B_start_row, int B_start_col, int C_start_row, int C_start_col, Size3 size, bool increment)
        {
            throw new NotImplementedException();
        }

    }
}
