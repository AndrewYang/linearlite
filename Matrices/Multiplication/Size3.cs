﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LinearNet.Matrices
{
    public readonly struct Size3
    {
        public readonly int M, N, P;

        internal int DimSum { get { return M + N + P; } }
        internal int HalfM { get { return M / 2; } }
        internal int HalfN { get { return N / 2; } }
        internal int HalfP { get { return P / 2; } }

        public Size3(int m, int n, int p)
        {
            M = m;
            N = n;
            P = p;
        }
    }
}
