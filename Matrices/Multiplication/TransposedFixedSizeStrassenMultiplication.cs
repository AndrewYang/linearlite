﻿using LinearNet.Providers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace LinearNet.Matrices.Multiplication
{
    /// <summary>
    /// Special internal matrix multiplication that operates on transposes of the B matrix, for 
    /// additional efficiency.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    internal class TransposedFixedSizeStrassenMultiplication<T> : IMatrixMultiplication<T>
    {
        private readonly IDenseBLAS1<T> _blas1;
        private readonly IDenseBLAS2<T> _blas2;
        private readonly int _baseAlgoThreshold;
        private readonly Dictionary<int, T[][]> _tempAs, _tempBs, _Ms, _tempCs;
        private readonly Dictionary<int, T[][]> _A_3s, _B_3s, _tempMs;

        internal TransposedFixedSizeStrassenMultiplication(IDenseBLAS1<T> blas1, IDenseBLAS2<T> blas2, int m, int n, int p, int baseAlgoThreshold = 32)
        {
            _blas1 = blas1;
            _blas2 = blas2;
            _baseAlgoThreshold = baseAlgoThreshold;

            // Pre-calculate the blocks
            _tempAs = new Dictionary<int, T[][]>();
            _tempBs = new Dictionary<int, T[][]>();
            _Ms = new Dictionary<int, T[][]>();
            _tempCs = new Dictionary<int, T[][]>();
            _A_3s = new Dictionary<int, T[][]>();
            _B_3s = new Dictionary<int, T[][]>();
            _tempMs = new Dictionary<int, T[][]>();

            while (!use_naive_algorithm(m, n, p))
            {
                _tempCs[m] = MatrixInternalExtensions.JMatrix<T>(m, p);

                m /= 2;
                n /= 2;
                p /= 2;

                _tempAs[m] = MatrixInternalExtensions.JMatrix<T>(m, n);
                _tempBs[n] = MatrixInternalExtensions.JMatrix<T>(n, p);
                _Ms[m] = MatrixInternalExtensions.JMatrix<T>(m, p);
                _A_3s[m] = MatrixInternalExtensions.JMatrix<T>(m, n);
                _B_3s[n] = MatrixInternalExtensions.JMatrix<T>(n, p);
                _tempMs[m] = MatrixInternalExtensions.JMatrix<T>(m, p);
            }
        }

        void IMatrixMultiplication<T>.Multiply(T[][] A, T[][] Bt, T[][] C, Size3 size, bool increment)
        {
            mult_winograd_transposed(A, Bt, C, size.M, size.N, size.P, increment);
        }

        void IMatrixMultiplication<T>.Multiply(T[][] A, T[][] Bt, T[][] C, int Ar0, int Ac0, int Br0, int Bc0, int Cr0, int Cc0, Size3 size, bool increment)
        {
            mult_winograd_transposed(A, Bt, C, Ar0, Ac0, Br0, Bc0, Cr0, Cc0, size.M, size.N, size.P, increment);
        }

        private bool use_naive_algorithm(int m, int n, int p)
        {
            return m < _baseAlgoThreshold || n < _baseAlgoThreshold || p < _baseAlgoThreshold;
        }

        private void mult_naive_transposed(T[][] A, T[][] Bt, T[][] result, int m, int n, int p, bool increment)
        {
            int i, j;
            if (increment)
            {
                IProvider<T> prov = _blas1.Provider;
                for (i = 0; i < m; ++i)
                {
                    T[] A_i = A[i], result_i = result[i];
                    for (j = 0; j < p; ++j)
                    {
                        result_i[j] = prov.Add(result_i[j], _blas1.DOT(A_i, Bt[j], 0, n));
                    }
                }
            }
            else
            {
                for (i = 0; i < m; ++i)
                {
                    T[] A_i = A[i], result_i = result[i];
                    for (j = 0; j < p; ++j)
                    {
                        result_i[j] = _blas1.DOT(A_i, Bt[j], 0, n);
                    }
                }
            }
        }
        private void mult_naive_transposed(T[][] A, T[][] Bt, T[][] C, int Ar0, int Ac0, int Br0, int Bc0, int Cr0, int Cc0, int m, int n, int p, bool increment)
        {
            int Ar_end = Ar0 + m,
                Br_end = Br0 + n,
                AB_offset = Ac0 - Br0,
                CA_offset = Cr0 - Ar0, i, j;

            IProvider<T> prov = _blas1.Provider;
            if (increment)
            {
                for (i = Ar0; i < Ar_end; ++i)
                {
                    T[] A_i = A[i], result_i = C[i + CA_offset];
                    for (j = 0; j < p; ++j)
                    {
                        int k = j + Cc0;
                        result_i[k] = prov.Add(result_i[k], _blas1.DOT(Bt[Bc0 + j], A_i, Br0, Br_end, AB_offset));
                    }
                }
            }
            else
            {
                for (i = Ar0; i < Ar_end; ++i)
                {
                    T[] A_i = A[i], result_i = C[i + CA_offset];
                    for (j = 0; j < p; ++j)
                    {
                        result_i[Cc0 + j] = _blas1.DOT(Bt[Bc0 + j], A_i, Br0, Br_end, AB_offset);
                    }
                }
            }
        }

        private void mult_winograd_transposed(T[][] A, T[][] Bt, T[][] C, int m, int n, int p, bool increment)
        {
            int a = m / 2, b = n / 2, c = p / 2;

            if (use_naive_algorithm(m, n, p))
            {
                mult_naive_transposed(A, Bt, C, m, n, p, increment);
                return;
            }

            T[][] tempA = _tempAs[a];
            T[][] M = _Ms[a];
            T[][] tempResult = _tempCs[m];
            T[][] A_3 = _A_3s[a];
            T[][] B_3 = _tempBs[b];

            // Stores M_1, M_1 + M_3, M_1 + M_3 + M_4
            T[][] M_temp = _tempMs[a];

            // M_1 := A_11 * (B_11)^T = A_11 * (B^T)_11
            mult_winograd_transposed(A, Bt, M_temp, a, b, c, false);
            _blas2.Copy(tempResult, M_temp, a, c);                                              // C_11 = M_1

            // M_2 := A_12 * (B_21)^T = A_12 * (B^T)_12, C11 := M_1 + M_2
            mult_winograd_transposed(A, Bt, M, 0, b, b, 0, 0, 0, a, b, c, false);
            _blas2.Increment(tempResult, M, a, c);                                              // C_11 = M_1 + M_2, complete

            // A_3 := A_21 + A_22
            _blas2.Add(A, A, A_3, a, 0, a, b, a, b);
            // B_3 := (B_12 - B_11)^T = (B_12)^T - (B_11)^T = (B^T)_21 - (B^T)_11
            _blas2.Subtract(Bt, Bt, B_3, c, 0, 0, 0, b, c);

            // M_5 := A_3 * B_3
            mult_winograd_transposed(A_3, B_3, M, a, b, c, false);
            _blas2.Copy(tempResult, M, 0, c, 0, 0, a, c);                                       // C_12 = M_5
            _blas2.Copy(tempResult, M, a, c, 0, 0, a, c);                                       // C_22 = M_5

            // B_3 := (B_22)^T - B_3 = (B^T)_22 - B_3 (i.e. B_3 := (B^T)_11 - (B^T)_12 + (B^T)_22)
            _blas2.Subtract(Bt, B_3, B_3, c, b, 0, 0, b, c);
            // A_3 -= A_11 (A_3 := -A_11 + A_21 + A_22)
            _blas2.Copy(tempA, A, a, b);
            _blas2.Decrement(A_3, tempA, a, b);

            // M_3 := A_3 * B_3
            mult_winograd_transposed(A_3, B_3, M, a, b, c, false);
            _blas2.Increment(M_temp, M, a, c);
            _blas2.Increment(tempResult, M_temp, 0, c, a, c);                                   // C_12 = M_5 + (M_1 + M_3)

            // A_3 := A_12 - A_3 (A_3 := A_11 + A_12 - A_21 - A_22)
            _blas2.Subtract(A, A_3, A_3, 0, b, 0, 0, a, b);
            // M_6 := A_3 * (B_22)^T = A_3 * (B^T)_22
            mult_winograd_transposed(A_3, Bt, M, 0, 0, b, c, 0, 0, a, b, c, false);
            _blas2.Increment(tempResult, M, 0, c, a, c);                                        // C_12 = (M_1 + M_3) + M_5 + M_6, complete

            // B_3 := (B_21)^T - B_3 = (B^T)_12 - B_3 (i.e. B_3 := -B_11 + B_12 + B_21 - B_22)
            _blas2.Subtract(Bt, B_3, B_3, 0, b, 0, 0, b, c);
            // M_7 := A_22 * B_3;
            mult_winograd_transposed(A, B_3, M, a, b, 0, 0, 0, 0, a, b, c, false);
            _blas2.Copy(tempResult, M, a, 0, 0, 0, a, c);

            // A_3 := A_11 - A_21
            _blas2.Subtract(A, A, A_3, 0, 0, a, 0, a, b);
            // B_3 := (B_22 - B_12)^T = (B^T)_22 - (B^T)_21
            _blas2.Subtract(Bt, Bt, B_3, c, b, c, 0, b, c);
            // M_4 := A_3 * B_3
            mult_winograd_transposed(A_3, B_3, M, a, b, c, false);
            _blas2.Increment(M_temp, M, a, c);
            _blas2.Increment(tempResult, M_temp, a, 0, a, c);                                   // C_21 = M_7 + (M_1 + M_3 + M_4), complete
            _blas2.Increment(tempResult, M_temp, a, c, a, c);                                   // C_22 = M_5 + (M_1 + M_3 + M_4), complete

            if (increment)
            {
                _blas2.Increment(C, tempResult, m, p);
            }
            else
            {
                _blas2.Copy(C, tempResult, m, p);
            }
        }
        /// <summary>
        /// Perform the Winograd variant of Strassen's multiplication on A and B, where B is given in transposed format 
        /// for accelerating naive multiplication
        /// 
        /// Note that Br0 and Bc0 apply to B, not Bt. 
        /// </summary>
        private void mult_winograd_transposed(T[][] A, T[][] Bt, T[][] C, int Ar0, int Ac0, int Br0, int Bc0, int Cr0, int Cc0, int m, int n, int p, bool increment)
        {
            int a = m / 2,
                b = n / 2,
                c = p / 2,
                Ar1 = Ar0 + a,
                Ac1 = Ac0 + b,
                Br1 = Br0 + b,
                Bc1 = Bc0 + c;

            bool is_simple = Ar0 == 0 && Ac0 == 0 && Br0 == 0 && Bc0 == 0 && Cr0 == 0 && Cc0 == 0;

            // base case - use simple matrix multiplication
            if (use_naive_algorithm(m, n, p))
            {
                if (is_simple)
                {
                    mult_naive_transposed(A, Bt, C, m, n, p, increment);
                }
                else
                {
                    mult_naive_transposed(A, Bt, C, Ar0, Ac0, Br0, Bc0, Cr0, Cc0, m, n, p, increment);
                }
                return;
            }

            T[][] tempA = _tempAs[a];
            T[][] M = _Ms[a];
            T[][] tempResult = _tempCs[m];
            T[][] A_3 = _A_3s[a];
            T[][] B_3 = _tempBs[b];

            // Stores M_1, M_1 + M_3, M_1 + M_3 + M_4
            T[][] M_temp = _tempMs[a];

            // M_1 := A_11 * (B_11)^T = A_11 * (B^T)_11
            mult_winograd_transposed(A, Bt, M_temp, Ar0, Ac0, Br0, Bc0, 0, 0, a, b, c, false);
            _blas2.Copy(tempResult, M_temp, a, c);                                              // C_11 = M_1

            // M_2 := A_12 * (B_21)^T = A_12 * (B^T)_12, C11 := M_1 + M_2
            mult_winograd_transposed(A, Bt, M, Ar0, Ac1, Br1, Bc0, 0, 0, a, b, c, false);
            _blas2.Increment(tempResult, M, a, c);                                              // C_11 = M_1 + M_2, complete

            // A_3 := A_21 + A_22
            _blas2.Add(A, A, A_3, Ar1, Ac0, Ar1, Ac1, a, b);
            // B_3 := (B_12 - B_11)^T = (B_12)^T - (B_11)^T = (B^T)_21 - (B^T)_11
            _blas2.Subtract(Bt, Bt, B_3, Bc1, Br0, Bc0, Br0, b, c);

            // M_5 := A_3 * B_3
            mult_winograd_transposed(A_3, B_3, M, a, b, c, false);
            _blas2.Copy(tempResult, M, 0, c, 0, 0, a, c);                                       // C_12 = M_5
            _blas2.Copy(tempResult, M, a, c, 0, 0, a, c);                                       // C_22 = M_5

            // B_3 := (B_22)^T - B_3 = (B^T)_22 - B_3 (i.e. B_3 := (B^T)_11 - (B^T)_12 + (B^T)_22)
            _blas2.Subtract(Bt, B_3, B_3, Bc1, Br1, 0, 0, b, c);
            // A_3 -= A_11 (A_3 := -A_11 + A_21 + A_22)
            _blas2.Copy(tempA, A, Ar0, Ac0, a, b);
            _blas2.Decrement(A_3, tempA, a, b);

            // M_3 := A_3 * B_3
            mult_winograd_transposed(A_3, B_3, M, a, b, c, false);
            _blas2.Increment(M_temp, M, a, c);
            _blas2.Increment(tempResult, M_temp, 0, c, a, c);                                   // C_12 = M_5 + (M_1 + M_3)

            // A_3 := A_12 - A_3 (A_3 := A_11 + A_12 - A_21 - A_22)
            _blas2.Subtract(A, A_3, A_3, Ar0, Ac1, 0, 0, a, b);
            // M_6 := A_3 * (B_22)^T = A_3 * (B^T)_22
            mult_winograd_transposed(A_3, Bt, M, 0, 0, Br1, Bc1, 0, 0, a, b, c, false);
            _blas2.Increment(tempResult, M, 0, c, a, c);                                        // C_12 = (M_1 + M_3) + M_5 + M_6, complete

            // B_3 := (B_21)^T - B_3 = (B^T)_12 - B_3 (i.e. B_3 := -B_11 + B_12 + B_21 - B_22)
            _blas2.Subtract(Bt, B_3, B_3, Bc0, Br1, 0, 0, b, c);
            // M_7 := A_22 * B_3;
            mult_winograd_transposed(A, B_3, M, Ar1, Ac1, 0, 0, 0, 0, a, b, c, false);
            _blas2.Copy(tempResult, M, a, 0, 0, 0, a, c);

            // A_3 := A_11 - A_21
            _blas2.Subtract(A, A, A_3, Ar0, Ac0, Ar1, Ac0, a, b);
            // B_3 := (B_22 - B_12)^T = (B^T)_22 - (B^T)_21
            _blas2.Subtract(Bt, Bt, B_3, Bc1, Br1, Bc1, Br0, b, c);
            // M_4 := A_3 * B_3
            mult_winograd_transposed(A_3, B_3, M, a, b, c, false);
            _blas2.Increment(M_temp, M, a, c);
            _blas2.Increment(tempResult, M_temp, a, 0, a, c);                                   // C_21 = M_7 + (M_1 + M_3 + M_4), complete
            _blas2.Increment(tempResult, M_temp, a, c, a, c);                                   // C_22 = M_5 + (M_1 + M_3 + M_4), complete

            // If size is odd, append the bottom row
            if (m % 2 == 1)
            {
                throw new NotImplementedException();
                /*
                if (is_simple)
                {
                    _instruction.append_bottom_row(A, B, tempResult, size, size, size);
                    _instruction.append_top_left_submatrix(A, B, tempResult, size, size, size);
                    _instruction.append_right_column(A, B, tempResult, size, size, size);
                }
                else
                {
                    _instruction.append_bottom_row(A, B, tempResult,
                        A_row_start, A_col_start,
                        B_row_start, B_col_start,
                        0, 0,
                        size, size, size);

                    _instruction.append_top_left_submatrix(A, B, tempResult,
                        A_row_start, A_col_start,
                        B_row_start, B_col_start,
                        0, 0,
                        size, size, size);

                    _instruction.append_right_column(A, B, tempResult,
                        A_row_start, A_col_start,
                        B_row_start, B_col_start,
                        0, 0,
                        size, size, size);
                }*/
            }
            if (increment)
            {
                _blas2.Increment(C, tempResult, Cr0, Cc0, m, p);
            }
            else
            {
                _blas2.Copy(C, tempResult, Cr0, Cc0, 0, 0, m, p);
            }
        }

    }
}
