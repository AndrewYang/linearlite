﻿using LinearNet.Geometry.Tiling;
using LinearNet.Global;
using LinearNet.Providers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Matrices
{
    /// <summary>
    /// The naive (exact size) implementation of Strassen's algorithm for rectangular matrices suffers from 
    /// 'level skipping' when the sizes of the initialized problem do not exactly match those of the matrices to be multiplied. 
    /// This implementation circumvents that problem by using tile multiplication via smaller square Strassen's algorithm.
    /// </summary>
    public class VariableSizeStrassenMultiplication<T> : IMatrixMultiplication<T> where T : new()
    {
        private readonly int _baseAlgoThreshold;
        private readonly IMatrixMultiplication<T> _squareMultiplier;
        private readonly IMatrixMultiplication<T> _baseAlgo;
        private readonly IDenseBLAS2<T> _blas2;
        private readonly List<int> _registerSizesInc;
        private readonly ITilingStrategy _tilingStrategy;

        internal IDenseBLAS2<T> BLAS2 { get { return _blas2; } }

        /// <summary>
        /// Set up a Strassen's matrix multiplication problem.
        /// </summary>
        /// <param name="instructions">The instruction set to use - availble for most primitive types and IField<T></param>
        /// <param name="m">The max. number of rows of the 1st matrix to be multiplied.</param>
        /// <param name="n">The max number of columns of the 1st matrix to be multiplied.</param>
        /// <param name="p">The max number of columns of the 2nd matrix to be multiplied.</param>
        /// <param name="useWinogradVariant">If true, uses the Winograd variant of Strassen's algorithm (15 addition/subtractions instead of 18)</param>
        /// <param name="baseAlgoThreshold">The matrix dimensions under which we revert back to regular matrix multiplication.</param>
        internal VariableSizeStrassenMultiplication(
            IDenseBLAS1<T> blas1,
            IDenseBLAS2<T> blas2,
            IMatrixMultiplication<T> baseAlgo,
            int m, int n, int p, 
            bool useWinogradVariant = false, 
            int baseAlgoThreshold = 32)
        {
            // Round down to the nearest power of 2 (to minimise 'skips' efficiency)
            int power2 = 1 << (int)(Math.Log(Util.Min(m, n, p)) / Math.Log(2));
            var blockMultiplyAlgo = new FixedSizeStrassenMultiplication<T>(blas1, blas2, baseAlgo, power2, power2, power2, useWinogradVariant, baseAlgoThreshold);
            _squareMultiplier = blockMultiplyAlgo;
            _baseAlgo = baseAlgo;
            _blas2 = blas2;
            _baseAlgoThreshold = baseAlgoThreshold;
            _registerSizesInc = blockMultiplyAlgo.RegisterSizes.OrderBy(o => o).ToList();
            _tilingStrategy = new GreedyTilingStrategy(_registerSizesInc);
        }

        /// <summary>
        /// Internal use only - we generate powers of 2 register sizes
        /// </summary>
        internal VariableSizeStrassenMultiplication(IMatrixMultiplication<T> blockAlgo, IMatrixMultiplication<T> baseAlgo, int m, int n, int p, int baseAlgoThreshold = 32)
        {
            if (m <= 0) throw new ArgumentOutOfRangeException(nameof(m), ExceptionMessages.NonPositiveNotAllowed);
            if (n <= 0) throw new ArgumentOutOfRangeException(nameof(n), ExceptionMessages.NonPositiveNotAllowed);
            if (p <= 0) throw new ArgumentOutOfRangeException(nameof(p), ExceptionMessages.NonPositiveNotAllowed);

            _squareMultiplier = blockAlgo;
            _baseAlgo = baseAlgo;

            // Generate the register sizes
            int power2 = 1 << (int)(Math.Log(Util.Min(m, n, p)) / Math.Log(2));
            List<int> registerSizes = new List<int>();
            while (power2 >= baseAlgoThreshold)
            {
                registerSizes.Add(power2);
                power2 /= 2;
            }

            _baseAlgoThreshold = baseAlgoThreshold;
            _registerSizesInc = registerSizes.OrderBy(o => o).ToList();
            _tilingStrategy = new GreedyTilingStrategy(_registerSizesInc);
        }

        private bool use_naive_algorithm(int m, int n, int p)
        {
            return m < _baseAlgoThreshold || n < _baseAlgoThreshold || p < _baseAlgoThreshold;
        }

        public void Multiply(T[][] A, T[][] B, T[][] result, Size3 size, bool increment)
        {
            Multiply(A, B, result, 0, 0, 0, 0, 0, 0, size, increment);
        }
        public void Multiply(T[][] A, T[][] B, T[][] result, 
            int A_row_start, int A_col_start, 
            int B_row_start, int B_col_start,
            int C_row_start, int C_col_start,
            Size3 size, bool increment)
        {
            // SIZETODO
            if (use_naive_algorithm(size.M, size.N, size.P))
            {
                _baseAlgo.Multiply(
                    A, B, result,
                    A_row_start, A_col_start,
                    B_row_start, B_col_start,
                    C_row_start, C_col_start,
                    size, increment);
                return;
            }

            SpatialPartition3D tiling = _tilingStrategy.Partition(0, 0, 0, size.M, size.N, size.P);

            // Because we are not incrementing, need a once-off clear of the result space.
            if (!increment)
            {
                int C_row_end = C_row_start + size.M,
                    C_col_end = C_col_start + size.P;

                T zero = new T();
                for (int i = C_row_start; i < C_row_end; ++i)
                {
                    T[] result_i = result[i];
                    for (int j = C_col_start; j < C_col_end; ++j)
                    {
                        result_i[j] = zero;
                    }
                }
            }

            foreach (Tile3D t in tiling.Tiles)
            {
                Size3 t_size = new Size3(t.LengthX, t.LengthY, t.LengthZ);
                if (t.IsRemainder)
                {
                    _baseAlgo.Multiply(
                        A, B, result,
                        A_row_start + t.X, A_col_start + t.Y,
                        B_row_start + t.Y, B_col_start + t.Z,
                        C_row_start + t.X, C_col_start + t.Z,
                        t_size, true);
                }
                else
                {
                    _squareMultiplier.Multiply(
                        A, B, result,
                        A_row_start + t.X, A_col_start + t.Y,
                        B_row_start + t.Y, B_col_start + t.Z,
                        C_row_start + t.X, C_col_start + t.Z,
                        t_size, true);
                }
            }
        }
    }
}
