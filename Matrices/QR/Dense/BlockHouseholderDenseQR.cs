﻿using LinearNet.Global;
using LinearNet.Matrices.Multiplication;
using LinearNet.Providers;
using LinearNet.Structs;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace LinearNet.Matrices.QR
{
    internal class BlockHouseholderDenseQR<T> : IDenseExplicitQR<T> where T : new()
    {
        private readonly bool _useTranspose = true;
        private readonly IDenseExplicitQR<T> _baseAlgorithm;

        private readonly IDenseBLAS1<T> _blas1;
        private readonly IDenseBLAS2<T> _blas2;
        private readonly IDenseHouseholder<T> _house;

        private readonly int _p;
        private readonly int _naiveMultiplicationThreshold; // for Strassen's

        internal BlockHouseholderDenseQR(IDenseExplicitQR<T> baseAlgo, IDenseBLAS1<T> blas1, IDenseBLAS2<T> blas2, 
            IDenseHouseholder<T> house, int p, int naiveMultiplicationThreshold, bool positiveDiagonals = false)
        {
            if (baseAlgo is null)
            {
                throw new ArgumentNullException(nameof(baseAlgo));
            }
            if (blas1 is null)
            {
                throw new ArgumentNullException(nameof(blas1));
            }
            if (blas2 is null)
            {
                throw new ArgumentNullException(nameof(blas2));
            }
            if (house is null)
            {
                throw new ArgumentNullException(nameof(house));
            }
            if (p <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(p), ExceptionMessages.NonPositiveNotAllowed);
            }
            if (naiveMultiplicationThreshold < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(naiveMultiplicationThreshold), ExceptionMessages.NonPositiveNotAllowed);
            }
            _provider = blas1.Provider;
            _positiveDiagonals = positiveDiagonals;

            _baseAlgorithm = baseAlgo;
            _blas1 = blas1;
            _blas2 = blas2;
            _house = house;
            _p = p;
            _naiveMultiplicationThreshold = Math.Min(p, naiveMultiplicationThreshold);
        }
        internal BlockHouseholderDenseQR(int p = 256, int naiveMultiplicationThreshold = 32, bool positiveDiagonals = false)
        {
            if (!ProviderFactory.TryGetDefaultBLAS1Provider(out IDenseBLAS1<T> blas1) || 
                !ProviderFactory.TryGetDefaultBLAS2Provider(out IDenseBLAS2<T> blas2) ||
                !ProviderFactory.TryGetDefaultHouseholderProvider(out IDenseHouseholder<T> house))
            {
                throw new NotSupportedException(ExceptionMessages.TypeNotSupported);
            }
            if (p <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(p), ExceptionMessages.NonPositiveNotAllowed);
            }
            if (naiveMultiplicationThreshold < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(naiveMultiplicationThreshold), ExceptionMessages.NonPositiveNotAllowed);
            }

            _provider = blas1.Provider;
            _positiveDiagonals = positiveDiagonals;

            _baseAlgorithm = new HouseholderDenseQR<T>(blas1.Provider, house);
            _blas1 = blas1;
            _blas2 = blas2;
            _house = house;
            _p = p;
            _naiveMultiplicationThreshold = Math.Min(p, naiveMultiplicationThreshold);
        }

        internal override void DecomposeInner(DenseMatrix<T> _R, DenseMatrix<T> _Q, bool parallel)
        {
            if (parallel)
            {
                throw new NotImplementedException();
            }

            T[][] R = _R.Values, Q = _Q.Values;
            int m = R.Length, n = R[0].Length, p = _p;

            if (n < p)
            {
                _baseAlgorithm.DecomposeInner(R, Q, parallel);
                return;
            }

            if (_useTranspose)
            {
                QrDecomposeTranspose(Q, R, m, n, p);
            }
            else
            {
                QrDecomposeWithoutTranspose(Q, R, m, n, p);
            }
        }
        private void QrDecomposeWithoutTranspose(T[][] Q, T[][] R, int m, int n, int p)
        {
            // For now
            if (n % p != 0)
            {
                throw new Exception();
            }

            T[][] Y = MatrixInternalExtensions.JMatrix<T>(p, m);
            T[][] Wt = MatrixInternalExtensions.JMatrix<T>(p, m);
            T[][] temp = MatrixInternalExtensions.JMatrix<T>(p, n);
            T[][] Yt = MatrixInternalExtensions.JMatrix<T>(m, p);

            T[] w = new T[n];
            var baseAlgo = new NaiveMatrixMultiplication<T>(_blas1, MultiplicationMode.OPTIMIZE_SPEED);
            var strassen = new VariableSizeStrassenMultiplication<T>(_blas1, _blas2, baseAlgo, p, p, p, true, _naiveMultiplicationThreshold);

            IProvider<T> provider = _blas1.Provider;

            // iterate through the p's 
            int n0 = n / p, r, c;
            for (int k = 0; k < n0; ++k)
            {
                int s = k * p, end = s + p;
                for (c = 0; c < p; ++c)
                {
                    int _c = s + c;
                    T[] Y_c = Y[c];
                    _house.HouseholderTransform(R, Q, Y_c, w, _c, end, _c, m, true);

                    // Set the first to 0
                    for (r = s; r < _c; ++r)
                    {
                        Y_c[r] = provider.Zero;
                    }
                }

                if (n - end > 0)
                {
                    // initialise, and calculate Wt, Y using recursive formula
                    for (c = 0; c < p; ++c)
                    {
                        partial_product(Wt, Y, Y[c], Wt[c], _blas1, provider, s, m, c);
                    }

                    // <p, m - s, n - end> multiplication problem
                    Size3 size_W = new Size3(p, m - s, n - end);
                    strassen.Multiply(Wt, R, temp,
                        0, s,
                        s, end,
                        0, 0,
                        size_W, false);

                    copy_transpose(Y, Yt, s);

                    // <m - s, p, n - end> multiplication problem
                    Size3 size_Y = new Size3(m - s, p, n - end);
                    strassen.Multiply(
                        Yt, temp, R,
                        s, 0,
                        0, 0,
                        s, end,
                        size_Y, true);
                }
            }
        }
        private void QrDecomposeTranspose(T[][] Q, T[][] R, int m, int n, int p)
        {
            const int blockSize = 16;

            IProvider<T> provider = _blas1.Provider;

            T[][] Y = MatrixInternalExtensions.JMatrix<T>(p, m),
                Yt = MatrixInternalExtensions.JMatrix<T>(m, p),
                Wt = MatrixInternalExtensions.JMatrix<T>(p, m),

                // We require p x n for R updates, and p x m for Q updates. Since m >= n, the size is p x m
                temp = MatrixInternalExtensions.JMatrix<T>(p, m),
                tempT = MatrixInternalExtensions.JMatrix<T>(m, p),
                Rt = MatrixInternalExtensions.JMatrix<T>(n, m);

            T[] w = new T[n];

            var baseAlgo = new NaiveMatrixMultiplication<T>(_blas1, MultiplicationMode.OPTIMIZE_SPEED);
            var blockAlgo = new TransposedFixedSizeStrassenMultiplication<T>(_blas1, _blas2, p, p, p, _naiveMultiplicationThreshold);
            var strassen = new VariableSizeStrassenMultiplication<T>(blockAlgo, baseAlgo, p, p, p, _naiveMultiplicationThreshold);
            bool useCacheEfficientTransposition = p % blockSize == 0;

            // iterate through the p's, the number of iterations rounds up to the next integer
            int r, c;
            for (int s = 0; s < n; s += p)
            {
                int end = Math.Min(n, s + p), _p = Math.Min(p, n - s);
                for (c = 0; c < _p; ++c)
                {
                    int _c = s + c;
                    T[] Y_c = Y[c];

                    // Do not transform Q
                    _house.HouseholderTransform(R, Y_c, w, _c, end, _c, m, true);

                    // Set the first to 0
                    for (r = s; r < _c; ++r)
                    {
                        Y_c[r] = provider.Zero;
                    }
                }

                int width = n - end;
                if (width >= 0)
                {
                    // initialise, and calculate Wt, Y using recursive formula
                    for (c = 0; c < _p; ++c)
                    {
                        partial_product(Wt, Y, Y[c], Wt[c], _blas1, provider, s, m, c);
                    }
                    // These are 'thin' matrices so we dont bother with cache-efficient algorithms
                    Yt.CopyTransposeFrom(Y, 0, s, p, m);

                    if (width > 0)
                    {
                        // Since R^T = n x m, by far the largest transposition required in each iteration, 
                        // we try to exploit cache-efficient transposition if convenient
                        if (useCacheEfficientTransposition)
                        {
                            Rt.CopyTransposeFrom(R, s, end, m, n, blockSize);
                        }
                        else
                        {
                            Rt.CopyTransposeFrom(R, s, end, m, n);
                        }

                        // <p, m - s, n - end> multiplication problem
                        Size3 size_WR = new Size3(p, m - s, width);
                        strassen.Multiply(
                            Wt, // cache size: [p, m], data: [0: p, s: m], occupied size: [p, m - s]
                            Rt, // cache size: [n, m], data: [s: m, end: n]^T, occupied size: [m - s, n - end]^T
                            temp, // cache size: [p, n], data: [0: p, end: n], occupied size: [p, n - end]
                            0, s,
                            s, end,
                            0, 0,
                            size_WR, false);

                        // These are 'thin' matrices so we dont bother with cache-efficient algorithms
                        tempT.CopyTransposeFrom(temp, 0, 0, p, width);

                        // <m - s, p, n - end> multiplication problem
                        Size3 size_YT = new Size3(m - s, p, width);
                        strassen.Multiply(
                            Yt, // cache size: [m, p], data: [s: m, 0: p], occupied size: [m - s, p]
                            tempT, // cache size: [p, n], data: [0: p, end: n], occupied size: [p, n - end]
                            R, // cache size: [n, m], data: [m: s, end: n], occupied size: [m - s, n - end]
                            s, 0,
                            0, 0,
                            s, end,
                            size_YT, true);
                    }

                    // temp <- QW
                    // <m, m - s, p> multiplication problem
                    Size3 size_QW = new Size3(m, m - s, p);
                    strassen.Multiply(
                        Q,      // m x (m - s)
                        Wt,     // p x (m - s) = ((m - s) x p)^T
                        tempT,  // m x p
                        0, s,
                        s, 0,
                        0, 0,
                        size_QW, false);

                    // Q += temp * Y (Q += Q * Y * W^T)
                    // <m, p, m - s> problem
                    Size3 size_YQ = new Size3(m, p, m - s);
                    strassen.Multiply(
                        tempT,  // m x p
                        Yt,     // (m - s) x p = [p x (m - s)]^T
                        Q,      // m x (m - s)
                        0, 0,
                        0, s,
                        0, s,
                        size_YQ, true);

                }
            }
        }

        private static void partial_product(T[][] Wt, T[][] Y, T[] u, T[] product, IDenseBLAS1<T> blas, IProvider<T> provider, int s, int m, int c)
        {
            /*
            T negone = provider.Negate(provider.One);
            Array.Copy(u, s, product, s, m - s);
            blas.SCAL(product, negone, s, m);
            */

            // TODO !important There could be some major performance improvements 
            // if we could cancel out the double negation
            for (int i = s; i < m; ++i)
            {
                product[i] = provider.Negate(u[i]);
            }

            for (int k = 0; k < c; ++k)
            {
                T Yk_u = blas.DOT(Y[k], u, s, m);
                blas.AXPY(product, Wt[k], provider.Negate(Yk_u), s, m);
            }
        }
        private static void copy_transpose(T[][] src, T[][] dest, int src_col_start)
        {
            // We anticipate that cols >> rows for src, and cols << rows for dest

            int src_cols = src[0].Length, src_rows = src.Length;
            int i, j;
            for (j = src_col_start; j < src_cols; ++j)
            {
                T[] dest_j = dest[j];
                for (i = 0; i < src_rows; ++i)
                {
                    dest_j[i] = src[i][j];
                }
            }
        }
    }
}
