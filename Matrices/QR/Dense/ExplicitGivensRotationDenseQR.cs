﻿using LinearNet.Global;
using LinearNet.Providers;
using LinearNet.Structs;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace LinearNet.Matrices.QR
{
    public class ExplicitGivensRotationDenseQR<T> : IDenseExplicitQR<T> where T : new()
    {
        private readonly IDenseBLAS1<T> _blas1;

        public ExplicitGivensRotationDenseQR(IDenseBLAS1Provider<T> blas1, bool positiveDiagonals = false)
        {
            if (blas1 is null)
            {
                throw new ArgumentNullException(nameof(blas1));
            }
            if (!(blas1 is IDenseBLAS1<T> b))
            {
                throw new NotSupportedException(ExceptionMessages.TypeNotSupported);
            }
            _blas1 = b;
            _provider = b.Provider;
            _positiveDiagonals = positiveDiagonals;
        }
        public ExplicitGivensRotationDenseQR(bool positiveDiagonals = false) : this(ProviderFactory.GetDefaultBLAS1<T>(), positiveDiagonals)
        {

        }

        internal override void DecomposeInner(DenseMatrix<T> A, DenseMatrix<T> Q, bool parallel)
        {
            int rows = A.Rows, columns = A.Columns, i, j;

            if (parallel)
            {
                throw new NotImplementedException();
            }

            T[][] _R = A.Values, _Q = Q.Values;
            T[] cs = new T[2];
            for (j = 0; j < columns; ++j)
            {
                for (i = j + 1; i < rows; ++i)
                {
                    // Unable to place this on the outside loop because the values of 
                    // R and Q are editted within this inner loop.
                    _provider.Givens(_R[j][j], _R[i][j], cs);

                    _blas1.ROTR(_R, j, i, cs[0], cs[1], j, columns);
                    _blas1.ROTC(_Q, j, i, cs[0], cs[1], 0, rows);
                }
            }
        }
    }
}
