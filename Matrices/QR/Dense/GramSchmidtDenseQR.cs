﻿using LinearNet.Global;
using LinearNet.Providers;
using LinearNet.Structs;
using System;
using System.Collections.Generic;
using System.Text;

namespace LinearNet.Matrices.QR
{
    public class GramSchmidtDenseQR<T> : IDenseExplicitQR<T> where T : new()
    {
        private readonly IDenseBLAS1<T> _blas1;

        public GramSchmidtDenseQR(IDenseBLAS1Provider<T> blas, bool positiveDiagonals = false)
        {
            if (blas is null)
            {
                throw new ArgumentNullException(nameof(blas));
            }
            if (!(blas is IDenseBLAS1<T> b))
            {
                throw new NotSupportedException(ExceptionMessages.TypeNotSupported);
            }

            _positiveDiagonals = positiveDiagonals;
            _provider = b.Provider;
            _blas1 = b;
        }
        public GramSchmidtDenseQR(bool positiveDiagonals = false)
        {
            if (!ProviderFactory.TryGetDefaultBLAS1Provider(out IDenseBLAS1<T> blas))
            {
                throw new NotSupportedException(ExceptionMessages.TypeNotSupported);
            }

            _positiveDiagonals = positiveDiagonals;
            _provider = blas.Provider;
            _blas1 = blas;
        }

        internal override void DecomposeInner(DenseMatrix<T> A, DenseMatrix<T> Q, bool parallel)
        {
            if (parallel)
            {
                throw new NotImplementedException();
            }
            if (!A.IsSquare)
            {
                throw new NotImplementedException("The Gram-Schmidt orthogonalization algorithm is only implemented for square matrices.");
            }

            Decompose(A.Values, Q.Values, true);
        }
        private void Decompose(T[][] A, T[][] Q, bool upperMatrix)
        {
            int rows = A.Length, cols = A[0].Length, r, c;

            T zero = _provider.Zero;
            T[][] u = OrthogonalColumnMatrix(A, upperMatrix);

            // convert u's into basis vectors e's, which produces the orthogonal matrix Q via a transpose
            T[] column = new T[rows], u_c;
            for (c = 0; c < cols; ++c)
            {
                u_c = u[c];
                for (r = 0; r < rows; ++r)
                {
                    Q[r][c] = u_c[r];
                }

                A.Column(c, column);

                if (upperMatrix)
                {
                    for (r = 0; r <= c; ++r)
                    {
                        A[r][c] = _blas1.DOT(u[r], column, 0, rows);
                    }
                    for (r = c + 1; r < rows; ++r)
                    {
                        A[r][c] = zero;
                    }
                }
                else
                {
                    for (r = 0; r < c; ++r)
                    {
                        A[r][c] = zero;
                    }
                    for (r = c; r < rows; ++r)
                    {
                        A[r][c] = _blas1.DOT(u[r], column, 0, rows);
                    }
                }
            }
        }
        private T[][] OrthogonalColumnMatrix(T[][] A, bool leftToRight)
        {
            int rows = A.Length, cols = A[0].Length, c;

            T[][] u = new T[cols][];
            T[] column = new T[rows];

            T one = _provider.One;
            if (leftToRight)
            {
                for (c = 0; c < cols; ++c)
                {
                    A.Column(c, column);

                    T[] u_c = new T[rows];
                    Array.Copy(column, u_c, rows);

                    for (int j = 0; j < c; ++j)
                    {
                        T alpha = Alpha(column, u[j], rows);
                        _blas1.AXPY(u_c, u[j], _provider.Negate(alpha), 0, rows);
                    }

                    T dot = _provider.Sqrt(_blas1.DOT(u_c, u_c, 0, rows));
                    _blas1.SCAL(u_c, _provider.Divide(one, dot), 0, rows);
                    u[c] = u_c;
                }
            }
            else
            {
                for (c = cols - 1; c >= 0; --c)
                {
                    A.Column(c, column);

                    T[] u_c = new T[rows];
                    Array.Copy(column, u_c, rows);

                    for (int j = cols - 1; j > c; --j)
                    {
                        T alpha = Alpha(column, u[j], rows);
                        _blas1.AXPY(u_c, u[j], _provider.Negate(alpha), 0, rows);
                    }

                    T dot = _provider.Sqrt(_blas1.DOT(u_c, u_c, 0, rows));
                    _blas1.SCAL(u_c, _provider.Divide(one, dot), 0, rows);
                    u[c] = u_c;
                }
            }
            return u;
        }

        // Calculate the scalar a so that a * v is the projection of u onto v 
        private T Alpha(T[] u, T[] v, int dim)
        {
            return _provider.Divide(_blas1.DOT(u, v, 0, dim), _blas1.DOT(v, v, 0, dim));
        }
    }
}
