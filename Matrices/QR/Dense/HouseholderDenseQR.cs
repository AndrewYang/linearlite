﻿using LinearNet.Providers;
using LinearNet.Structs;
using System;
using System.Collections.Generic;
using System.Text;

namespace LinearNet.Matrices.QR
{
    public class HouseholderDenseQR<T> : IDenseExplicitQR<T> where T : new()
    {
        private readonly IDenseHouseholder<T> _house;

        internal HouseholderDenseQR(IProvider<T> provider, IDenseHouseholder<T> house, bool positiveDiagonals = false)
        {
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            if (house is null)
            {
                throw new ArgumentNullException(nameof(house));
            }

            _positiveDiagonals = positiveDiagonals;
            _provider = provider;
            _house = house;
        }
        public HouseholderDenseQR(bool positiveDiagonals = false) : 
            this(ProviderFactory.GetDefaultProvider<T>(), ProviderFactory.GetDefaultHouseholder<T>(), positiveDiagonals)
        {

        }

        internal override void DecomposeInner(DenseMatrix<T> A, DenseMatrix<T> Q, bool parallel)
        {
            if (parallel)
            {
                throw new NotImplementedException();
            }

            int m = A.Rows, n = A.Columns, c;

            // Initialize workspaces
            T[] v = new T[m], w = new T[n];

            // Get inner values
            T[][] _R = A.Values, _Q = Q.Values;

            // Apply Householder column transformations to reduce R
            for (c = 0; c < n; ++c)
            {
                _house.HouseholderTransform(_R, _Q, v, w, c, n, c, m, true);
            }
        }
    }
}
