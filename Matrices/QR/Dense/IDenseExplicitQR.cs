﻿using LinearNet.Providers;
using LinearNet.Structs;
using System;

namespace LinearNet.Matrices.QR
{
    public abstract class IDenseExplicitQR<T> : IDenseQRAlgorithm<T> where T : new()
    {
        protected bool _positiveDiagonals;
        protected IProvider<T> _provider;

        protected IDenseExplicitQR(IProvider<T> provider, bool positiveDiagonals)
        {
            _provider = provider;
            _positiveDiagonals = positiveDiagonals;
        }
        protected IDenseExplicitQR()
        {

        }

        public QR<T> Decompose(DenseMatrix<T> A, bool overwrite)
        {
            DenseMatrix<T> R = A;
            if (!overwrite)
            {
                R = A.Copy();
            }

            DenseMatrix<T> Q = DenseMatrix.Identity<T>(R.Rows);
            Decompose(R, Q, false, false);

            return new ExplicitQR<T>(Q, R);
        }

        /// <summary>
        /// Calculates the QR decomposition of A, overwriting A with its upper triangular factor R.
        /// </summary>
        /// <param name="A"></param>
        /// <param name="Q">The orthogonal matrix $Q$.</param>
        /// <param name="resetQ"></param>
        /// <param name="parallel"></param>
        internal void Decompose(DenseMatrix<T> A, DenseMatrix<T> Q, bool resetQ, bool parallel)
        {
            if (resetQ)
            {
                ResetQ(Q.Values, _provider);
            }

            DecomposeInner(A, Q, parallel);

            if (_positiveDiagonals)
            {
                SetPositiveDiagonalsQR(Q.Values, A.Values, _provider);
            }
        }
        internal abstract void DecomposeInner(DenseMatrix<T> A, DenseMatrix<T> Q, bool parallel);

        private void ResetQ(T[][] Q, IProvider<T> provider)
        {
            int rows = Q.Length, cols = Q[0].Length;
            T zero = provider.Zero, one = provider.One;
            if (provider.Equals(default, provider.Zero))
            {
                for (int r = 0; r < rows; ++r)
                {
                    Array.Clear(Q[r], 0, cols);
                    Q[r][r] = one;
                }
            }
            else
            {
                for (int r = 0; r < rows; ++r)
                {
                    T[] row = Q[r];
                    for (int c = 0; c < cols; ++c)
                    {
                        row[c] = zero;
                    }
                    row[r] = one;
                }
            }
        }
        private void SetPositiveDiagonalsQR(T[][] Q, T[][] R, IProvider<T> provider)
        {
            int m = Q.Length, n = R[0].Length, c, r;
            for (c = 0; c < n; ++c)
            {
                if (provider.IsRealAndNegative(R[c][c]))
                {
                    for (r = 0; r < m; ++r)
                    {
                        Q[r][c] = provider.Negate(Q[r][c]);
                    }
                    T[] R_c = R[c];
                    for (r = c; r < n; ++r)
                    {
                        R_c[r] = provider.Negate(R_c[r]);
                    }
                }
            }
        }

    }
}
