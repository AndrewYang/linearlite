﻿using LinearNet.Structs;
using System;
using System.Collections.Generic;
using System.Text;

namespace LinearNet.Matrices.QR
{
    public interface IDenseQRAlgorithm<T> where T : new()
    {
        QR<T> Decompose(DenseMatrix<T> matrix, bool overwrite);
    }
}
