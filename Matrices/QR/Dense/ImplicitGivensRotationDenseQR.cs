﻿using LinearNet.Global;
using LinearNet.Providers;
using LinearNet.Structs;
using System;
using System.Collections.Generic;
using System.Text;

namespace LinearNet.Matrices.QR.Dense
{
    /// <summary>
    /// Performs QR decomposition using Givens rotations, without explicitly computing Q and R matrices
    /// </summary>
    public class ImplicitGivensRotationDenseQR<T> : IDenseQRAlgorithm<T> where T : new()
    {
        private readonly IDenseBLAS1<T> _blas1;
        private readonly IProvider<T> _provider;

        public ImplicitGivensRotationDenseQR(IDenseBLAS1Provider<T> blas1)
        {
            if (blas1 is null)
            {
                throw new ArgumentNullException(nameof(blas1));
            }
            if (!(blas1 is IDenseBLAS1<T> b))
            {
                throw new NotSupportedException(ExceptionMessages.TypeNotSupported);
            }

            _blas1 = b;
            _provider = b.Provider;
        }

        public QR<T> Decompose(DenseMatrix<T> A, bool overwrite)
        {
            int rows = A.Rows, columns = A.Columns, i, j, k = 0;
            T[][] _R = A.Values;

            T[] tau = new T[rows * columns - (columns * (columns + 1) / 2)], cs = new T[2];
            for (j = 0; j < columns; ++j)
            {
                for (i = j + 1; i < rows; ++i)
                {
                    _provider.Givens(_R[j][j], _R[i][j], cs);
                    tau[k++] = cs[0];
                }
            }

            return new ImplicitQR<T>(A, tau);
        }
    }
}
