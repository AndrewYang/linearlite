﻿using LinearNet.Providers;
using LinearNet.Structs;
using System;
using System.Collections.Generic;
using System.Text;

namespace LinearNet.Matrices
{
    /// <summary>
    /// A QR factorization where the Q and R factors are explicitly stored
    /// </summary>
    public class ExplicitQR<T> : QR<T> where T : new()
    {
        private readonly Matrix<T> _Q, _R;
        private readonly IProvider<T> _provider;
        private readonly IDenseBLAS1<T> _blas1;

        public override Matrix<T> Q => _Q;
        public override Matrix<T> R => _R;

        internal ExplicitQR(Matrix<T> Q, Matrix<T> R, IDenseBLAS1<T> blas)
        {
            _Q = Q;
            _R = R;
            _provider = blas.Provider;
            _blas1 = blas;
        }
        internal ExplicitQR(Matrix<T> Q, Matrix<T> R) : this(Q, R, ProviderFactory.GetDefaultBLAS1<T>())
        {
            
        }

        public override T Determinant()
        {
            // Change to an IEnumerable to avoid memory wasteage
            return _provider.Product(_R.LeadingDiagonal());
        }

        public override T LogDeterminant()
        {
            return _provider.LogProduct(_R.LeadingDiagonal());
        }

        protected override void SolveInner(T[] b, T[] x)
        {
            // Since this method is used for both least squares and solve, 
            // need to ensure there is enough space to store Q' * b
            T[] workspace = x.Length >= _Q.Rows ? x : new T[_Q.Rows];

            // y <- Q' * b
            _Q.TransposeAndMultiply(b, workspace, _provider, false);

            // Solve R * y = x
            _R.SolveTriangular(workspace, x, _blas1, false, false);
        }
    }
}
