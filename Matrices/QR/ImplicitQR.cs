﻿using LinearNet.Structs;
using System;

namespace LinearNet.Matrices.QR
{
    public sealed class ImplicitQR<T> : QR<T> where T : new()
    {
        private readonly DenseMatrix<T> _A; 
        private readonly T[] _tau;

        public override Matrix<T> Q => throw new NotImplementedException();

        public override Matrix<T> R => throw new NotImplementedException();

        internal ImplicitQR(DenseMatrix<T> A, T[] tau)
        {
            _A = A;
            _tau = tau;
        }

        public override T Determinant()
        {
            throw new NotImplementedException();
        }

        public override T LogDeterminant()
        {
            throw new NotImplementedException();
        }

        protected override void SolveInner(T[] b, T[] x)
        {
            throw new NotImplementedException();
        }
    }
}
