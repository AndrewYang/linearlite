﻿using LinearNet.Structs;
using System;

namespace LinearNet.Matrices
{
    public abstract class QR<T> where T : new()
    {
        public abstract Matrix<T> Q { get; }
        public abstract Matrix<T> R { get; }

        /// <summary>
        /// Calculate the determinant of the original matrix $A$ using its QR decomposition.
        /// </summary>
        /// <returns>The matrix determinant.</returns>
        public abstract T Determinant();

        /// <summary>
        /// Returns the natural logarithm of the determinant of the original matrix $A$, using 
        /// its QR decomposition
        /// </summary>
        /// <returns></returns>
        public abstract T LogDeterminant();

        public DenseVector<T> Solve(DenseVector<T> b)
        {
            if (b is null)
            {
                throw new ArgumentNullException(nameof(b));
            }
            DenseVector<T> x = new DenseVector<T>(R.Columns);
            Solve(b.Values, x.Values);
            return x;
        }

        /// <summary>
        /// Solve the linear system $Ax = b$, or the optimization problem  $\operatorname{argmin} ||Ax - b||_2$ if the system
        /// is overdetermined.
        /// </summary>
        /// <param name="b"></param>
        /// <param name="x"></param>
        public void Solve(DenseVector<T> b, DenseVector<T> x)
        {
            if (b is null)
            {
                throw new ArgumentNullException(nameof(b));
            }
            if (x is null)
            {
                throw new ArgumentNullException(nameof(x));
            }
            Solve(b.Values, x.Values);
        }

        /// <summary>
        /// Solve the linear system $Ax = b$ where $A$ is the original matrix of this QR factorization,
        /// and $b$ is a dense vector. If the system is overdetermined (i.e. the number of rows of $A$
        /// is greater than the number of columns of $A$), this method finds the vector $x$
        /// that minimises the norm $||Ax - b||_2$. 
        /// </summary>
        /// <param name="b"></param>
        /// <param name="x"></param>
        public void Solve(T[] b, T[] x)
        {
            if (b is null)
            {
                throw new ArgumentNullException(nameof(b));
            }
            if (x is null)
            {
                throw new ArgumentNullException(nameof(x));
            }
            if (b.Length < R.Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(b), $"The dimension of '{nameof(b)}' is less than the number of rows of the matrix.");
            }
            if (x.Length < R.Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(x), $"The dimension of '{nameof(x)}' is less than the number of columns of the matrix.");
            }
            SolveInner(b, x);
        }

        /// <summary>
        /// Solve without any argument checks
        /// </summary>
        /// <param name="b"></param>
        /// <param name="x"></param>
        protected abstract void SolveInner(T[] b, T[] x);
    }
}
