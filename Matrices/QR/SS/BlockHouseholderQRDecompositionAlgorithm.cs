﻿using LinearNet.Providers;
using LinearNet.Decomposition;
using LinearNet.Matrices.Decompositions.QR;
using LinearNet.Structs;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LinearNet.Providers.LinearAlgebra;
using LinearNet.Matrices.Multiplication;

namespace LinearNet.Matrices.Decompositions
{
    internal class BlockHouseholderQRDecompositionAlgorithm : IDenseQRDecompositionAlgorithm
    {
        /// <summary>
        /// This algorithm defaults to the householder algorithm for small dimension values
        /// </summary>
        private HouseholderQRDecompositionAlgorithm _defaultAlgorithm;

        private readonly int _p;
        private readonly int _naiveMultiplicationThreshold; // for Strassen's

        internal BlockHouseholderQRDecompositionAlgorithm(int p = 256, int naiveMultiplicationThreshold = 32)
        {
            _p = p;
            _naiveMultiplicationThreshold = Math.Min(p, naiveMultiplicationThreshold);
            _defaultAlgorithm = new HouseholderQRDecompositionAlgorithm();
        }

        private static void partial_product(double[][] Wt, double[][] Y, double[] u, double[] product, IDenseBLAS1<double> blas, int s, int m, int c)
        {
            int i, k;

            for (i = s; i < m; ++i)
            {
                product[i] = -u[i];
            }

            for (k = 0; k < c; ++k)
            {
                double Yk_u = blas.DOT(Y[k], u, s, m);
                blas.AXPY(product, Wt[k], -Yk_u, s, m);
            }
        }
        private static void partial_product_parallel(double[][] Wt, double[][] Y, double[] u, double[] product, IDenseBLAS1<double> blas, int s, int m, int c)
        {
            if (c < 2)
            {
                partial_product(Wt, Y, u, product, blas, s, m, c);
                return;
            }

            for (int i = s; i < m; ++i)
            {
                product[i] = -u[i];
            }

            Parallel.ForEach(Partitioner.Create(0, c), range =>
            {
                int min = range.Item1, max = range.Item2, i, k;
                for (k = min; k < max; ++k)
                {
                    double Yk_u = blas.DOT(Y[k], u, s, m);
                    double[] Wt_k = Wt[k];
                    for (i = s; i < m; ++i)
                    {
                        product[i] -= Wt_k[i] * Yk_u;
                    }
                }
            });
        }
        private static void copy_transpose(double[][] src, double[][] dest, int src_col_start)
        {
            // We anticipate that cols >> rows for src, and cols << rows for dest

            int src_cols = src[0].Length, src_rows = src.Length;
            int i, j;
            for (j = src_col_start; j < src_cols; ++j)
            {
                double[] dest_j = dest[j];
                for (i = 0; i < src_rows; ++i)
                {
                    dest_j[i] = src[i][j];
                }
            }
        }
        private static void copy_transpose_parallel(double[][] src, double[][] dest, int src_col_start)
        {
            int src_cols = src[0].Length, src_rows = src.Length;
            Parallel.ForEach(Partitioner.Create(src_col_start, src_cols), range =>
            {
                int min = range.Item1, max = range.Item2, i, j;
                for (j = min; j < max; ++j)
                {
                    double[] dest_j = dest[j];
                    for (i = 0; i < src_rows; ++i)
                    {
                        dest_j[i] = src[i][j];
                    }
                }
            });
        }

        protected override void QR_decompose_inner(float[][] Q, float[][] R)
        {
            throw new NotImplementedException();
        }

        protected override void QR_decompose_inner(double[][] Q, double[][] R)
        {
            //QR_decompose_inner_no_transpose(Q, R);
            QR_decompose_inner_using_transpose(Q, R);
        }
        protected void QR_decompose_inner_no_transpose(double[][] Q, double[][] R)
        {
            int m = R.Length, n = R[0].Length, r, c, p = _p;

            if (n < p)
            {
                _defaultAlgorithm.QRDecompose(null, Q, R, false, false, false);
                return;
            }

            // For now
            if (n % p != 0)
            {
                throw new Exception();
            }

            double[][] Y = MatrixInternalExtensions.JMatrix<double>(p, m);
            double[][] Wt = MatrixInternalExtensions.JMatrix<double>(p, m);
            double[][] temp = MatrixInternalExtensions.JMatrix<double>(p, n);
            double[][] Yt = MatrixInternalExtensions.JMatrix<double>(m, p);

            double[] w = new double[n];
            var blas = new NativeDoubleProvider();
            var baseAlgo = new NaiveMatrixMultiplication<double>(blas, MultiplicationMode.OPTIMIZE_SPEED);
            var strassen = new VariableSizeStrassenMultiplication<double>(blas, blas, baseAlgo, p, p, p, true, _naiveMultiplicationThreshold);

            // iterate through the p's 
            int n0 = n / p;
            for (int k = 0; k < n0; ++k)
            {
                int s = k * p, end = s + p;
                for (c = 0; c < p; ++c)
                {
                    int _c = s + c;
                    double[] Y_c = Y[c];
                    Householder.Transform(R, Q, Y_c, w, _c, end, _c, m, true);

                    // Set the first to 0
                    for (r = s; r < _c; ++r)
                    {
                        Y_c[r] = 0.0;
                    }
                }

                if (n - end > 0)
                {
                    // initialise, and calculate Wt, Y using recursive formula
                    for (c = 0; c < p; ++c)
                    {
                        partial_product(Wt, Y, Y[c], Wt[c], blas, s, m, c);
                    }

                    // <p, m - s, n - end> multiplication problem
                    Size3 size_W = new Size3(p, m - s, n - end);
                    strassen.Multiply(Wt, R, temp,
                        0, s,
                        s, end,
                        0, 0,
                        size_W, false);

                    copy_transpose(Y, Yt, s);

                    // <m - s, p, n - end> multiplication problem
                    Size3 size_Y = new Size3(m - s, p, n - end);
                    strassen.Multiply(
                        Yt, temp, R,
                        s, 0, 
                        0, 0, 
                        s, end,
                        size_Y, true);
                }
            }
        }
        protected void QR_decompose_inner_using_transpose(double[][] Q, double[][] R)
        {
            int m = R.Length, n = R[0].Length, r, c, p = _p, blockSize = 16;

            if (n < p)
            {
                _defaultAlgorithm.QRDecompose(null, Q, R, false, false, false);
                return;
            }

            double[][] Y = MatrixInternalExtensions.JMatrix<double>(p, m),
                Yt = MatrixInternalExtensions.JMatrix<double>(m, p),
                Wt = MatrixInternalExtensions.JMatrix<double>(p, m),

                // We require p x n for R updates, and p x m for Q updates. Since m >= n, the size is p x m
                temp = MatrixInternalExtensions.JMatrix<double>(p, m), 
                tempT = MatrixInternalExtensions.JMatrix<double>(m, p),
                Rt = MatrixInternalExtensions.JMatrix<double>(n, m);

            double[] w = new double[n];

            var blas = new NativeDoubleProvider();
            var baseAlgo = new NaiveMatrixMultiplication<double>(blas, MultiplicationMode.OPTIMIZE_SPEED);
            var blockAlgo = new TransposedFixedSizeStrassenMultiplication<double>(blas, blas, p, p, p, _naiveMultiplicationThreshold);
            var strassen = new VariableSizeStrassenMultiplication<double>(blockAlgo, baseAlgo, p, p, p, _naiveMultiplicationThreshold);
            bool useCacheEfficientTransposition = p % blockSize == 0;

            // iterate through the p's, the number of iterations rounds up to the next integer
            for (int s = 0; s < n; s += p)
            {
                int end = Math.Min(n, s + p), _p = Math.Min(p, n - s);
                for (c = 0; c < _p; ++c)
                {
                    int _c = s + c;
                    double[] Y_c = Y[c];

                    // Do not transform Q
                    Householder.TransformColumn(R, Y_c, w, _c, end, _c, m);

                    // Set the first to 0
                    for (r = s; r < _c; ++r)
                    {
                        Y_c[r] = 0.0;
                    }
                }

                int width = n - end;
                if (width >= 0)
                {
                    // initialise, and calculate Wt, Y using recursive formula
                    for (c = 0; c < _p; ++c)
                    {
                        partial_product(Wt, Y, Y[c], Wt[c], blas, s, m, c);
                    }
                    // These are 'thin' matrices so we dont bother with cache-efficient algorithms
                    Yt.CopyTransposeFrom(Y, 0, s, p, m);

                    if (width > 0)
                    {
                        // Since R^T = n x m, by far the largest transposition required in each iteration, 
                        // we try to exploit cache-efficient transposition if convenient
                        if (useCacheEfficientTransposition)
                        {
                            Rt.CopyTransposeFrom(R, s, end, m, n, blockSize);
                        }
                        else
                        {
                            Rt.CopyTransposeFrom(R, s, end, m, n);
                        }

                        // <p, m - s, n - end> multiplication problem
                        Size3 size_WR = new Size3(p, m - s, width);
                        strassen.Multiply(
                            Wt, // cache size: [p, m], data: [0: p, s: m], occupied size: [p, m - s]
                            Rt, // cache size: [n, m], data: [s: m, end: n]^T, occupied size: [m - s, n - end]^T
                            temp, // cache size: [p, n], data: [0: p, end: n], occupied size: [p, n - end]
                            0, s,
                            s, end,
                            0, 0,
                            size_WR, false);

                        // These are 'thin' matrices so we dont bother with cache-efficient algorithms
                        tempT.CopyTransposeFrom(temp, 0, 0, p, width);

                        // <m - s, p, n - end> multiplication problem
                        Size3 size_YT = new Size3(m - s, p, width);
                        strassen.Multiply(
                            Yt, // cache size: [m, p], data: [s: m, 0: p], occupied size: [m - s, p]
                            tempT, // cache size: [p, n], data: [0: p, end: n], occupied size: [p, n - end]
                            R, // cache size: [n, m], data: [m: s, end: n], occupied size: [m - s, n - end]
                            s, 0,
                            0, 0,
                            s, end,
                            size_YT, true);
                    }

                    // temp <- QW
                    // <m, m - s, p> multiplication problem
                    Size3 size_QW = new Size3(m, m - s, p);
                    strassen.Multiply(
                        Q,      // m x (m - s)
                        Wt,     // p x (m - s) = ((m - s) x p)^T
                        tempT,  // m x p
                        0, s,
                        s, 0,
                        0, 0,
                        size_QW, false);

                    // Q += temp * Y (Q += Q * Y * W^T)
                    // <m, p, m - s> problem
                    Size3 size_YQ = new Size3(m, p, m - s);
                    strassen.Multiply(
                        tempT,  // m x p
                        Yt,     // (m - s) x p = [p x (m - s)]^T
                        Q,      // m x (m - s)
                        0, 0,
                        0, s,
                        0, s,
                        size_YQ, true);
                }
            }
        }

        protected override void QR_decompose_inner(decimal[][] Q, decimal[][] R)
        {
            throw new NotImplementedException();
        }
        protected override void QR_decompose_inner(Complex[][] Q, Complex[][] R)
        {
            throw new NotImplementedException();
        }

        protected override void QR_decompose_inner_parallel(float[][] Q, float[][] R)
        {
            throw new NotImplementedException();
        }
        protected void QR_decompose_inner_parallel1(double[][] Q, double[][] R)
        {
            int m = R.Length, n = R[0].Length, r, c, p = _p;

            // For now
            if (n % p != 0)
            {
                _defaultAlgorithm.QRDecompose(null, Q, R, false, false, true);
                return;
            }

            double[][] Y = MatrixInternalExtensions.JMatrix<double>(p, m),
                Yt = MatrixInternalExtensions.JMatrix<double>(m, p),
                Wt = MatrixInternalExtensions.JMatrix<double>(p, m),
                temp = MatrixInternalExtensions.JMatrix<double>(p, m),
                tempT = MatrixInternalExtensions.JMatrix<double>(m, p);

            double[] w = new double[n];
            NativeDoubleProvider blas = new NativeDoubleProvider();

            // iterate through the p's 
            int n0 = n / p;
            for (int k = 0; k < n0; ++k)
            {
                int s = k * p, end = s + p;
                for (c = 0; c < p; ++c)
                {
                    int _c = s + c;
                    double[] Y_c = Y[c];
                    Householder.TransformParallel(R, Q, Y_c, w, _c, end, _c, m, true);

                    // Set the first to 0
                    for (r = s; r < _c; ++r)
                    {
                        Y_c[r] = 0.0;
                    }
                }

                int width = n - end;
                if (width > 0)
                {
                    // initialise, and calculate Wt, Y using recursive formula
                    for (c = 0; c < _p; ++c)
                    {
                        partial_product_parallel(Wt, Y, Y[c], Wt[c], blas, s, m, c);
                    }

                    // <p, m - s, n - end> multiplication problem
                    Wt.multiply_parallel_unsafe(
                        R,
                        temp,
                        0, s,
                        s, end,
                        0, 0,
                        p, width, m - s, false);

                    copy_transpose_parallel(Y, Yt, s);
                    tempT.CopyTransposeFrom(temp, 0, 0, p, width);

                    // <m - s, p, n - end> multiplication problem
                    Yt.multiply_parallel_unsafe(
                        tempT, 
                        R,
                        s, 0,
                        0, 0,
                        s, end,
                        m - s, width, p, true);
                }
            }
        }
        protected override void QR_decompose_inner_parallel(double[][] Q, double[][] R)
        {
            int m = R.Length, n = R[0].Length, r, c, p = _p, blockSize = 16;

            if (n < p)
            {
                _defaultAlgorithm.QRDecompose(null, Q, R, false, false, false);
                return;
            }

            double[][] Y = MatrixInternalExtensions.JMatrix<double>(p, m),
                Yt = MatrixInternalExtensions.JMatrix<double>(m, p),
                W = MatrixInternalExtensions.JMatrix<double>(m, p),
                Wt = MatrixInternalExtensions.JMatrix<double>(p, m),

                // We require p x n for R updates, and p x m for Q updates. Since m >= n, the size is p x m
                temp = MatrixInternalExtensions.JMatrix<double>(p, m),
                tempT = MatrixInternalExtensions.JMatrix<double>(m, p),
                Rt = MatrixInternalExtensions.JMatrix<double>(n, m);

            double[] w = new double[n];
            NativeDoubleProvider blas = new NativeDoubleProvider();

            // iterate through the p's, the number of iterations rounds up to the next integer
            for (int s = 0; s < n; s += p)
            {
                int end = Math.Min(n, s + p), _p = Math.Min(p, n - s);
                for (c = 0; c < _p; ++c)
                {
                    int _c = s + c;
                    double[] Y_c = Y[c];

                    // Do not transform Q
                    Householder.TransformColumn(R, Y_c, w, _c, end, _c, m);

                    // Set the first to 0
                    for (r = s; r < _c; ++r)
                    {
                        Y_c[r] = 0.0;
                    }
                }

                int width = n - end;
                if (width >= 0)
                {
                    // initialise, and calculate Wt, Y using recursive formula
                    for (c = 0; c < _p; ++c)
                    {
                        partial_product(Wt, Y, Y[c], Wt[c], blas, s, m, c);
                    }
                    // These are 'thin' matrices so we dont bother with cache-efficient algorithms
                    Yt.CopyTransposeFrom(Y, 0, s, p, m);

                    if (width > 0)
                    {
                        // <p, m - s, n - end> multiplication problem
                        Wt.multiply_parallel_unsafe(
                            R, // cache size: [n, m], data: [s: m, end: n]^T, occupied size: [m - s, n - end]^T
                            Rt,
                            temp, // cache size: [p, n], data: [0: p, end: n], occupied size: [p, n - end]
                            0, s,
                            s, end,
                            0, 0,
                            p, width, m - s, false);

                        // These are 'thin' matrices so we dont bother with cache-efficient algorithms
                        //tempT.CopyTransposeFrom(temp, 0, 0, p, width);

                        // <m - s, p, n - end> multiplication problem
                        Yt.multiply_parallel_unsafe(// cache size: [m, p], data: [s: m, 0: p], occupied size: [m - s, p]
                            temp, // cache size: [p, n], data: [0: p, end: n], occupied size: [p, n - end]
                            tempT,
                            R, // cache size: [n, m], data: [m: s, end: n], occupied size: [m - s, n - end]
                            s, 0,
                            0, 0,
                            s, end,
                            m - s, width, p, true);
                    }

                    W.CopyTransposeFrom(Wt, 0, s, p, m);
                    // temp <- QW
                    // <m, m - s, p> multiplication problem
                    Q.multiply_parallel_unsafe( // m x m - s
                        W,  // p x m - s = (m - s x p)^T
                        Wt,
                        tempT, // m x p
                        0, s,
                        s, 0,
                        0, 0,
                        m, p, m - s, false);

                    // Q += temp * W^T (Q += Q * Y * W^T)
                    // <m, p, m - s> problem
                    tempT.multiply_parallel_unsafe( // m x p
                        Y, // (m - s) x p = [p x (m - s)]^T
                        Yt,
                        Q, // m x (m - s)
                        0, 0,
                        0, s,
                        0, s,
                        m, m - s, p, true);
                }
            }
        }
        protected override void QR_decompose_inner_parallel(decimal[][] Q, decimal[][] R)
        {
            throw new NotImplementedException();
        }
        protected override void QR_decompose_inner_parallel(Complex[][] Q, Complex[][] R)
        {
            throw new NotImplementedException();
        }

        protected override void QL_decompose_inner(float[][] Q, float[][] L)
        {
            throw new NotImplementedException();
        }
        protected override void QL_decompose_inner(double[][] Q, double[][] L)
        {
            throw new NotImplementedException();
        }
        protected override void QL_decompose_inner(decimal[][] Q, decimal[][] L)
        {
            throw new NotImplementedException();
        }
        protected override void QL_decompose_inner(Complex[][] Q, Complex[][] L)
        {
            throw new NotImplementedException();
        }

        protected override void QL_decompose_inner_parallel(float[][] Q, float[][] R)
        {
            throw new NotImplementedException();
        }
        protected override void QL_decompose_inner_parallel(double[][] Q, double[][] R)
        {
            throw new NotImplementedException();
        }
        protected override void QL_decompose_inner_parallel(decimal[][] Q, decimal[][] R)
        {
            throw new NotImplementedException();
        }
        protected override void QL_decompose_inner_parallel(Complex[][] Q, Complex[][] R)
        {
            throw new NotImplementedException();
        }

        protected override void QR_decompose_inner(float[][] R)
        {
            throw new NotImplementedException();
        }
        protected override void QR_decompose_inner(double[][] R)
        {
            throw new NotImplementedException();
        }
        protected override void QR_decompose_inner(decimal[][] R)
        {
            throw new NotImplementedException();
        }
        protected override void QR_decompose_inner(Complex[][] R)
        {
            throw new NotImplementedException();
        }

        protected override void QR_decompose_hessenberg_unsafe(float[][] Q, float[][] R)
        {
            throw new NotImplementedException();
        }
        protected override void QR_decompose_hessenberg_unsafe(double[][] Q, double[][] R)
        {
            throw new NotImplementedException();
        }
        protected override void QR_decompose_hessenberg_unsafe(decimal[][] Q, decimal[][] R)
        {
            throw new NotImplementedException();
        }
        protected override void QR_decompose_hessenberg_unsafe(Complex[][] Q, Complex[][] R)
        {
            throw new NotImplementedException();
        }

        protected override void QR_decompose_hessenberg_unsafe(float[][] R)
        {
            throw new NotImplementedException();
        }
        protected override void QR_decompose_hessenberg_unsafe(double[][] R)
        {
            throw new NotImplementedException();
        }
        protected override void QR_decompose_hessenberg_unsafe(decimal[][] R)
        {
            throw new NotImplementedException();
        }
        protected override void QR_decompose_hessenberg_unsafe(Complex[][] R)
        {
            throw new NotImplementedException();
        }
    }
}
