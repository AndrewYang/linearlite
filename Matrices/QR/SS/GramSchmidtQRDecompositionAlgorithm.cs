﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LinearNet.Structs;

namespace LinearNet.Matrices.Decompositions.QR
{
    internal class GramSchmidtQRDecompositionAlgorithm : IDenseQRDecompositionAlgorithm
    {
        protected override void QL_decompose_inner(float[][] Q, float[][] L)
        {
            throw new NotImplementedException();
        }
        protected override void QL_decompose_inner(double[][] Q, double[][] L)
        {
            throw new NotImplementedException();
        }
        protected override void QL_decompose_inner(decimal[][] Q, decimal[][] L)
        {
            throw new NotImplementedException();
        }
        protected override void QL_decompose_inner(Complex[][] Q, Complex[][] L)
        {
            throw new NotImplementedException();
        }

        protected override void QL_decompose_inner_parallel(float[][] Q, float[][] R)
        {
            throw new NotImplementedException();
        }
        protected override void QL_decompose_inner_parallel(double[][] Q, double[][] R)
        {
            throw new NotImplementedException();
        }
        protected override void QL_decompose_inner_parallel(decimal[][] Q, decimal[][] R)
        {
            throw new NotImplementedException();
        }
        protected override void QL_decompose_inner_parallel(Complex[][] Q, Complex[][] R)
        {
            throw new NotImplementedException();
        }

        protected override void QR_decompose_inner(float[][] Q, float[][] R)
        {
            throw new NotImplementedException();
        }
        protected override void QR_decompose_inner(double[][] Q, double[][] R)
        {
            throw new NotImplementedException();
        }
        protected override void QR_decompose_inner(decimal[][] Q, decimal[][] R)
        {
            throw new NotImplementedException();
        }
        protected override void QR_decompose_inner(Complex[][] Q, Complex[][] R)
        {
            throw new NotImplementedException();
        }

        protected override void QR_decompose_inner_parallel(float[][] Q, float[][] R)
        {
            throw new NotImplementedException();
        }
        protected override void QR_decompose_inner_parallel(double[][] Q, double[][] R)
        {
            throw new NotImplementedException();
        }
        protected override void QR_decompose_inner_parallel(decimal[][] Q, decimal[][] R)
        {
            throw new NotImplementedException();
        }
        protected override void QR_decompose_inner_parallel(Complex[][] Q, Complex[][] R)
        {
            throw new NotImplementedException();
        }

        protected override void QR_decompose_inner(float[][] R)
        {
            throw new NotImplementedException();
        }
        protected override void QR_decompose_inner(double[][] R)
        {
            throw new NotImplementedException();
        }
        protected override void QR_decompose_inner(decimal[][] R)
        {
            throw new NotImplementedException();
        }
        protected override void QR_decompose_inner(Complex[][] R)
        {
            throw new NotImplementedException();
        }

        protected override void QR_decompose_hessenberg_unsafe(float[][] Q, float[][] R)
        {
            throw new NotImplementedException();
        }
        protected override void QR_decompose_hessenberg_unsafe(double[][] Q, double[][] R)
        {
            throw new NotImplementedException();
        }
        protected override void QR_decompose_hessenberg_unsafe(decimal[][] Q, decimal[][] R)
        {
            throw new NotImplementedException();
        }
        protected override void QR_decompose_hessenberg_unsafe(Complex[][] Q, Complex[][] R)
        {
            throw new NotImplementedException();
        }

        protected override void QR_decompose_hessenberg_unsafe(float[][] R)
        {
            throw new NotImplementedException();
        }
        protected override void QR_decompose_hessenberg_unsafe(double[][] R)
        {
            throw new NotImplementedException();
        }
        protected override void QR_decompose_hessenberg_unsafe(decimal[][] R)
        {
            throw new NotImplementedException();
        }
        protected override void QR_decompose_hessenberg_unsafe(Complex[][] R)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="A"></param>
        /// <param name="upperMatrix">true for QR decomposition, false for QL decomposition</param>
        /// <param name="Q"></param>
        /// <param name="M"></param>
        private static void Decompose(double[,] A, bool upperMatrix, double[,] Q, double[,] M)
        {
            int rows = A.GetLength(0), cols = A.GetLength(1), r, c;

            double[][] u = OrthogonalColumnMatrix(A, upperMatrix);

            // convert u's into basis vectors e's, which produces the orthogonal matrix Q via a transpose
            double[] column = new double[rows], u_c;
            for (c = 0; c < cols; ++c)
            {
                u_c = u[c];
                for (r = 0; r < rows; ++r)
                {
                    Q[r, c] = u_c[r];
                }

                A.Column(c, column);

                if (upperMatrix)
                {
                    for (r = 0; r <= c; ++r)
                    {
                        M[r, c] = u[r].Dot(column);
                    }
                    for (r = c + 1; r < rows; ++r)
                    {
                        M[r, c] = 0;
                    }
                }
                else
                {
                    for (r = 0; r < c; ++r)
                    {
                        M[r, c] = 0;
                    }
                    for (r = c; r < rows; ++r)
                    {
                        M[r, c] = u[r].Dot(column);
                    }
                }
            }
        }
        private static double[][] OrthogonalColumnMatrix(double[,] A, bool leftToRight)
        {
            int rows = A.GetLength(0), cols = A.GetLength(1), c;

            double[][] u = new double[cols][];
            double[] column = new double[rows], u_c;

            if (leftToRight)
            {
                for (c = 0; c < cols; ++c)
                {
                    A.Column(c, column);

                    u_c = new double[rows];
                    Array.Copy(column, 0, u_c, 0, rows);

                    for (int j = 0; j < c; ++j)
                    {
                        double[] proj_u_a = column.ProjectOnto(u[j]);
                        u_c = u_c.Subtract(proj_u_a);
                    }

                    u_c = u_c.Multiply(1 / Math.Sqrt(u_c.Dot(u_c)));

                    u[c] = u_c;
                }
            }
            else
            {
                for (c = cols - 1; c >= 0; --c)
                {
                    A.Column(c, column);

                    u[c] = new double[rows];
                    Array.Copy(column, 0, u[c], 0, rows);

                    for (int j = cols - 1; j > c; --j)
                    {
                        double[] proj_u_a = column.ProjectOnto(u[j]);
                        u[c] = u[c].Subtract(proj_u_a);
                    }

                    u[c] = u[c].Multiply(1 / Math.Sqrt(u[c].Dot(u[c])));
                }
            }
            return u;
        }

        private static void Decompose(Complex[,] A, bool leftDecomp, bool upperMatrix, ref Complex[,] Q, ref Complex[,] M)
        {
            int rows = A.GetLength(0), cols = A.GetLength(1), c, r, k;

            Complex[][] e = new Complex[cols][];
            Complex[] u = new Complex[rows], a = new Complex[rows];

            if (upperMatrix)
            {
                for (c = 0; c < cols; ++c)
                {
                    A.Column(c, a);
                    A.Column(c, u);

                    for (k = 0; k < c; ++k)
                    {
                        Complex ae = a.Dot(e[k]);

                        // This is where we set M for [0, c)
                        M[k, c] = ae;

                        for (r = 0; r < rows; ++r)
                        {
                            u[r] -= ae * e[k][r];
                        }
                    }

                    double factor = 1 / u.Norm();
                    e[c] = new Complex[rows];
                    for (r = 0; r < rows; r++)
                    {
                        e[c][r] = u[r] * factor;

                        // Q matrix
                        Q[r, c] = e[c][r];
                    }

                    // set M for [c, c]
                    M[c, c] = a.Dot(e[c]);

                    // set M for [c + 1, cols)
                    for (k = c + 1; k < cols; k++)
                    {
                        M[k, c] = Complex.Zero;
                    }
                }
            }
            else
            {
                for (c = cols - 1; c >= 0; --c)
                {
                    A.Column(c, a);
                    A.Column(c, u);

                    for (k = c + 1; k < cols; ++k)
                    {
                        Complex ae = a.Dot(e[k]);

                        // This is where we set M for [c + 1, cols)
                        M[k, c] = ae;

                        for (r = 0; r < rows; ++r)
                        {
                            u[r] -= ae * e[k][r];
                        }
                    }

                    double factor = 1 / u.Norm();
                    e[c] = new Complex[rows];
                    for (r = 0; r < rows; r++)
                    {
                        e[c][r] = u[r] * factor;

                        // Q matrix
                        Q[r, c] = e[c][r];
                    }

                    // set M for [c, c]
                    M[c, c] = a.Dot(e[c]);

                    // set M for [c + 1, cols)
                    for (k = 0; k < c; k++)
                    {
                        M[k, c] = Complex.Zero;
                    }
                }
            }
        }
        private static Complex[][] OrthogonalColumnMatrix(Complex[,] A, bool leftToRight)
        {
            int rows = A.GetLength(0), cols = A.GetLength(1), c;

            Complex[][] u = new Complex[cols][];
            Complex[] column = new Complex[rows];

            if (leftToRight)
            {
                for (c = 0; c < cols; ++c)
                {
                    A.Column(c, column);

                    u[c] = new Complex[rows];
                    Array.Copy(column, 0, u[c], 0, rows);

                    for (int j = 0; j < c; ++j)
                    {
                        Complex[] proj_u_a = column.ProjectOnto(u[j]);
                        u[c] = u[c].Subtract(proj_u_a);
                    }

                    u[c] = u[c].Multiply(1 / u[c].Norm());
                }
            }
            else
            {
                for (c = cols - 1; c >= 0; --c)
                {
                    A.Column(c, column);

                    u[c] = new Complex[rows];
                    Array.Copy(column, 0, u[c], 0, rows);

                    for (int j = cols - 1; j > c; --j)
                    {
                        Complex[] proj_u_a = column.ProjectOnto(u[j]);
                        u[c] = u[c].Subtract(proj_u_a);
                    }

                    u[c] = u[c].Multiply(1 / u[c].Norm());
                }
            }
            return u;
        }
    }
}
