﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LinearNet.Providers;
using LinearNet.Global;
using LinearNet.Structs;

namespace LinearNet.Matrices.Decompositions.QR
{
    internal class HouseholderQRDecompositionAlgorithm : IDenseQRDecompositionAlgorithm
    {
        protected override void QL_decompose_inner(float[][] Q, float[][] L)
        {
            int m = L.Length, n = L[0].Length, r, c, i, j;
            float[] z = new float[m], w = new float[n], L_row, Q_row;

            float sqrt2 = Constants.Sqrt2_f, w_j, z_i, q_i;
            for (c = n - 1; c >= 0; --c)
            {
                int col_end = c + 1, row_end = m - n + col_end, row_last = row_end - 1;
                for (r = 0; r < row_end; ++r) z[r] = L[r][c];
                z[row_last] += Math.Sign(L[row_last][c]) * z.Norm(0, row_end);
                z.MultiplyOverwrite(sqrt2 / z.Norm(0, row_end), 0, row_end);

                // Update the R matrix using HH reflection on column c (using v)
                for (j = 0; j < col_end; ++j)
                {
                    w_j = 0.0f;
                    for (i = 0; i < row_end; ++i)
                    {
                        w_j += z[i] * L[i][j];
                    }
                    w[j] = w_j;
                }

                L[row_last][c] -= z[row_last] * w[c];
                for (i = 0; i < row_end; ++i)
                {
                    L_row = L[i];
                    z_i = z[i];
                    if (i < row_last)
                    {
                        L_row[c] = 0.0f;
                    }
                    for (j = 0; j < c; ++j)
                    {
                        L_row[j] -= z_i * w[j];
                    }
                }

                // Update the Q matrix using the same HH reflection- TODO
                for (i = 0; i < m; ++i)
                {
                    Q_row = Q[i];
                    q_i = 0.0f;
                    for (j = 0; j < row_end; ++j)
                    {
                        q_i += z[j] * Q_row[j];
                    }
                    for (j = 0; j < row_end; ++j)
                    {
                        Q_row[j] -= q_i * z[j];
                    }
                }
            }
        }
        protected override void QL_decompose_inner(double[][] Q, double[][] L)
        {
            int m = L.Length, n = L[0].Length, r, c, i, j;
            double[] z = new double[m], w = new double[n], L_row, Q_row;

            double sqrt2 = Math.Sqrt(2), w_j, z_i, q_i;
            for (c = n - 1; c >= 0; --c)
            {
                int col_end = c + 1, row_end = m - n + col_end, row_last = row_end - 1;
                for (r = 0; r < row_end; ++r) z[r] = L[r][c];
                z[row_last] += Math.Sign(L[row_last][c]) * z.Norm(0, row_end);
                z.MultiplyOverwrite(sqrt2 / z.Norm(0, row_end), 0, row_end);

                // Update the R matrix using HH reflection on column c (using v)
                for (j = 0; j < col_end; ++j)
                {
                    w_j = 0;
                    for (i = 0; i < row_end; ++i)
                    {
                        w_j += z[i] * L[i][j];
                    }
                    w[j] = w_j;
                }

                L[row_last][c] -= z[row_last] * w[c];
                for (i = 0; i < row_end; ++i)
                {
                    L_row = L[i];
                    z_i = z[i];
                    if (i < row_last)
                    {
                        L_row[c] = 0.0;
                    }
                    for (j = 0; j < c; ++j)
                    {
                        L_row[j] -= z_i * w[j];
                    }
                }

                // Update the Q matrix using the same HH reflection- TODO
                for (i = 0; i < m; ++i)
                {
                    Q_row = Q[i];
                    q_i = 0;
                    for (j = 0; j < row_end; ++j)
                    {
                        q_i += z[j] * Q_row[j];
                    }
                    for (j = 0; j < row_end; ++j)
                    {
                        Q_row[j] -= q_i * z[j];
                    }
                }
            }
        }
        protected override void QL_decompose_inner(decimal[][] Q, decimal[][] L)
        {
            int m = L.Length, n = L[0].Length, r, c, i, j;
            decimal[] z = new decimal[m], w = new decimal[n], L_row, Q_row;

            decimal sqrt2 = Constants.Sqrt2_m, w_j, z_i, q_i;
            for (c = n - 1; c >= 0; --c)
            {
                int col_end = c + 1, row_end = m - n + col_end, row_last = row_end - 1;
                for (r = 0; r < row_end; ++r) z[r] = L[r][c];
                z[row_last] += Math.Sign(L[row_last][c]) * z.Norm(0, row_end);
                z.MultiplyOverwrite(sqrt2 / z.Norm(0, row_end), 0, row_end);

                // Update the R matrix using HH reflection on column c (using v)
                for (j = 0; j < col_end; ++j)
                {
                    w_j = 0;
                    for (i = 0; i < row_end; ++i)
                    {
                        w_j += z[i] * L[i][j];
                    }
                    w[j] = w_j;
                }

                L[row_last][c] -= z[row_last] * w[c];
                for (i = 0; i < row_end; ++i)
                {
                    L_row = L[i];
                    z_i = z[i];
                    if (i < row_last)
                    {
                        L_row[c] = 0.0m;
                    }
                    for (j = 0; j < c; ++j)
                    {
                        L_row[j] -= z_i * w[j];
                    }
                }

                // Update the Q matrix using the same HH reflection- TODO
                for (i = 0; i < m; ++i)
                {
                    Q_row = Q[i];
                    q_i = 0;
                    for (j = 0; j < row_end; ++j)
                    {
                        q_i += z[j] * Q_row[j];
                    }
                    for (j = 0; j < row_end; ++j)
                    {
                        Q_row[j] -= q_i * z[j];
                    }
                }
            }
        }
        protected override void QL_decompose_inner(Complex[][] Q, Complex[][] L)
        {
            int m = L.Length, n = L[0].Length, r, c, i, j;
            Complex[] z = new Complex[m],
                z_conj = new Complex[m],
                w = new Complex[n],
                L_row, Q_row;

            double sqrt2 = Math.Sqrt(2);
            Complex _i = Complex.I, zero = Complex.Zero, w_j, z_i, q_i;
            for (c = n - 1; c >= 0; --c)
            {
                int col_end = c + 1, row_end = m - n + col_end, row_last = row_end - 1;
                for (r = 0; r < row_end; ++r) z[r] = L[r][c];

                Complex sign = Complex.Exp(_i.Multiply(L[row_last][c].Argument()));
                sign.MultiplyEquals(z.Norm(0, row_end));
                z[row_last].IncrementBy(sign);

                z.MultiplyOverwrite(sqrt2 / z.Norm(0, row_end), 0, row_end);

                // precalculate the conjugate of z
                for (r = 0; r < row_end; ++r) z_conj[r] = z[r].Conjugate();

                // Update the L matrix using HH reflection on column c (using v)
                for (j = 0; j < col_end; ++j)
                {
                    w_j = zero;
                    for (i = 0; i < row_end; ++i)
                    {
                        w_j.IncrementBy(z_conj[i].Multiply(L[i][j]));
                    }
                    w[j] = w_j;
                }

                L[row_last][c].DecrementBy(z[row_last].Multiply(w[c]));
                for (i = 0; i < row_end; ++i)
                {
                    L_row = L[i];
                    z_i = z[i];
                    if (i < row_last)
                    {
                        L_row[c] = zero;
                    }
                    for (j = 0; j < c; ++j)
                    {
                        L_row[j].DecrementBy(z_i.Multiply(w[j]));
                    }
                }

                // Update the Q matrix using the same HH reflection- TODO
                for (i = 0; i < m; ++i)
                {
                    Q_row = Q[i];
                    q_i = zero;
                    for (j = 0; j < row_end; ++j)
                    {
                        q_i.IncrementBy(z[j].Multiply(Q_row[j]));
                    }
                    for (j = 0; j < row_end; ++j)
                    {
                        Q_row[j].DecrementBy(q_i.Multiply(z_conj[j]));
                    }
                }
            }
        }

        protected override void QL_decompose_inner_parallel(float[][] Q, float[][] L)
        {
            throw new NotImplementedException();
        }
        protected override void QL_decompose_inner_parallel(double[][] Q, double[][] L)
        {
            throw new NotImplementedException();
        }
        protected override void QL_decompose_inner_parallel(decimal[][] Q, decimal[][] L)
        {
            throw new NotImplementedException();
        }
        protected override void QL_decompose_inner_parallel(Complex[][] Q, Complex[][] L)
        {
            throw new NotImplementedException();
        }

        protected override void QR_decompose_inner(float[][] Q, float[][] R) => qr_decompose_unsafe(Q, R, Householder.Transform);
        protected override void QR_decompose_inner(double[][] Q, double[][] R) => qr_decompose_unsafe(Q, R, Householder.Transform);
        protected override void QR_decompose_inner(decimal[][] Q, decimal[][] R) => qr_decompose_unsafe(Q, R, Householder.Transform);
        protected override void QR_decompose_inner(Complex[][] Q, Complex[][] R) => qr_decompose_unsafe(Q, R, Householder.Transform);

        protected override void QR_decompose_inner_parallel(float[][] Q, float[][] R) => qr_decompose_unsafe(Q, R, Householder.TransformParallel);
        protected override void QR_decompose_inner_parallel(double[][] Q, double[][] R) => qr_decompose_unsafe(Q, R, Householder.TransformParallel);
        protected override void QR_decompose_inner_parallel(decimal[][] Q, decimal[][] R) => qr_decompose_unsafe(Q, R, Householder.TransformParallel);
        protected override void QR_decompose_inner_parallel(Complex[][] Q, Complex[][] R) => qr_decompose_unsafe(Q, R, Householder.TransformParallel);

        protected override void QR_decompose_inner(float[][] R) => qr_decompose_unsafe(R, Householder.Transform);
        protected override void QR_decompose_inner(double[][] R) => qr_decompose_unsafe(R, Householder.Transform);
        protected override void QR_decompose_inner(decimal[][] R) => qr_decompose_unsafe(R, Householder.Transform);
        protected override void QR_decompose_inner(Complex[][] R) => qr_decompose_unsafe(R, Householder.Transform);

        protected override void QR_decompose_hessenberg_unsafe(float[][] Q, float[][] R) => qr_decompose_hessenberg_unsafe(Q, R, Householder.TransformHessenbergColumn);
        protected override void QR_decompose_hessenberg_unsafe(double[][] Q, double[][] R) => qr_decompose_hessenberg_unsafe(Q, R, Householder.TransformHessenbergColumn);
        protected override void QR_decompose_hessenberg_unsafe(decimal[][] Q, decimal[][] R) => qr_decompose_hessenberg_unsafe(Q, R, Householder.TransformHessenbergColumn);
        protected override void QR_decompose_hessenberg_unsafe(Complex[][] Q, Complex[][] R) => qr_decompose_hessenberg_unsafe(Q, R, Householder.TransformHessenbergColumn);

        protected override void QR_decompose_hessenberg_unsafe(float[][] R) => qr_decompose_hessenberg_unsafe(R, Householder.TransformHessenbergColumn);
        protected override void QR_decompose_hessenberg_unsafe(double[][] R) => qr_decompose_hessenberg_unsafe(R, Householder.TransformHessenbergColumn);
        protected override void QR_decompose_hessenberg_unsafe(decimal[][] R) => qr_decompose_hessenberg_unsafe(R, Householder.TransformHessenbergColumn);
        protected override void QR_decompose_hessenberg_unsafe(Complex[][] R) => qr_decompose_hessenberg_unsafe(R, Householder.TransformHessenbergColumn);

        private static void qr_decompose_unsafe<T>(T[][] Q, T[][] R, Action<T[][], T[][], T[], T[], int, int, int, int, bool> HouseholderTransformColumn)
        {
            int m = R.Length, n = R[0].Length, c;
            T[] v = new T[m], w = new T[n];

            // Apply Householder column transformations to reduce R
            for (c = 0; c < n; ++c)
            {
                HouseholderTransformColumn(R, Q, v, w, c, n, c, m, true);
            }
        }
        private static void qr_decompose_unsafe<T>(T[][] R, Action<T[][], T[], T[], int, int, int, int, bool> HouseholderTransform)
        {
            int m = R.Length, n = R[0].Length, c;
            T[] v = new T[m], w = new T[n];

            // Apply Householder transformations to reduce R
            for (c = 0; c < n; ++c)
            {
                HouseholderTransform(R, v, w, c, n, c, m, true);
            }
        }
        private static void qr_decompose_hessenberg_unsafe<T>(T[][] Q, T[][] R, Action<T[][], T[][], T[], int, int, int, int, bool> HouseholderTransformColumn)
        {
            int m = R.Length, n = R[0].Length, c, min = Math.Min(m, n);
            T[] v = new T[m];

            // Apply Householder transformations to reduce R
            for (c = 0; c < min; ++c)
            {
                HouseholderTransformColumn(R, Q, v, c, n, c, m, true);
            }
        }
        private static void qr_decompose_hessenberg_unsafe<T>(T[][] R, Action<T[][], T[], int, int, int, int, bool> HouseholderTransformColumn)
        {
            int m = R.Length, n = R[0].Length, c, min = Math.Min(m, n);
            T[] v = new T[m];

            // Apply Householder transformations to reduce R
            for (c = 0; c < min; ++c)
            {
                HouseholderTransformColumn(R, v, c, n, c, m, true);
            }
        }
    }
}
