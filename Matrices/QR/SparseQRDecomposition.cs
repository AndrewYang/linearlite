﻿using LinearNet.Providers;
using LinearNet.Structs;
using System.Collections.Generic;
using System.Linq;

namespace LinearNet.Matrices.Sparse.Decompositions
{
    public static class SparseQRDecomposition
    {
        private static void QRDecompose(this DOKSparseMatrix<double> A, DOKSparseMatrix<double> Q, DOKSparseMatrix<double> R, ISparseBLAS1<double> blas1)
        {
            Dictionary<long, double> values = A.Values;
            Dictionary<long, Dictionary<long, double>> row_then_columns = A.GroupByRows();

            HashSet<long> column_index_set = new HashSet<long>();
            foreach (Dictionary<long, double> row in row_then_columns.Values)
            {
                column_index_set.UnionWith(row.Keys);
            }

            List<long> column_ids = column_index_set.ToList();
            List<long> row_ids = row_then_columns.Keys.ToList();
            column_ids.Sort();
            row_ids.Sort();

            long columns = A.Columns;

            double[] cs = new double[2];
            foreach (long c in column_ids)
            {
                long diagonal_element_id = c * columns + c;

                Dictionary<long, double> pivot_row;
                double pivot;
                if (values.ContainsKey(diagonal_element_id))
                {
                    pivot_row = row_then_columns[c];
                    pivot = values[diagonal_element_id];
                }
                else
                {
                    pivot_row = new Dictionary<long, double>();
                    pivot = 0.0;
                }

                foreach (KeyValuePair<long, double> e in values)
                {
                    // If the column is c
                    if (e.Key % columns == c)
                    {
                        long r = e.Key / columns;
                        if (r > c)
                        {
                            Util.Givens(pivot, row_then_columns[r][c], cs);
                            blas1.ROTR(row_then_columns[c], row_then_columns[r], cs[0], cs[1], c, columns);
                        }
                    }
                }
            }
        }
    }
}
