﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet
{
    public static class RectangularMatrixCommonExtensions
    {
        /// <summary>
        /// Returns the leading (main) diagonal of a square or rectangular matrix, as an array.
        /// </summary>
        /// <param name="A">The matrix.</param>
        /// <returns>The main diagonal.</returns>
        /// <cat>la</cat>
        public static T[] LeadingDiagonal<T>(this T[,] A)
        {
            if (A is null)
            {
                throw new ArgumentNullException(nameof(A));
            }

            int n = Math.Min(A.GetLength(0), A.GetLength(1)), i;
            T[] diagonal = new T[n];
            for (i = 0; i < n; i++)
            {
                diagonal[i] = A[i, i];
            }
            return diagonal;
        }

        /// <summary>
        /// Calculates the matrix transpose for a real matrix, without changing the original matrix.
        /// </summary>
        /// <param name="A">The matrix.</param>
        /// <returns>The matrix transpose.</returns>
        /// <cat>la</cat>
        public static T[,] Transpose<T>(this T[,] A)
        {
            MatrixChecks.CheckNotNull(A);

            int rows = A.GetLength(0), cols = A.GetLength(1), r, c;
            T[,] transpose = new T[cols, rows];
            for (r = 0; r < rows; r++)
            {
                for (c = 0; c < cols; c++)
                {
                    transpose[c, r] = A[r, c];
                }
            }
            return transpose;
        }
        public static T[,] ParallelTranspose<T>(this T[,] A)
        {
            MatrixChecks.CheckNotNull(A);

            int rows = A.GetLength(0), cols = A.GetLength(1);
            T[,] transpose = new T[cols, rows];

            Parallel.ForEach(Partitioner.Create(0, rows), range =>
            {
                int r, c, min = range.Item1, max = range.Item2;
                for (r = min; r < max; r++)
                {
                    for (c = 0; c < cols; c++)
                    {
                        transpose[c, r] = A[r, c];
                    }
                }
            });
            return transpose;
        }


        /// <summary>
        /// Returns the column at index 'columnIndex' for a matrix A. Note that 'columnIndex' is 0-indexed.
        /// </summary>
        /// <param name="A">A non-degenerate matrix with at least ('columnIndex' + 1) columns.</param>
        /// <param name="columnIndex">A non-negative index of the column to get. 0-indexed.</param>
        /// <returns>The 'columnIndex'-th column ('columnIndex' is 0-indexed)</returns>
        /// <cat>la</cat>
        public static T[] Column<T>(this T[,] A, int columnIndex)
        {
            MatrixChecks.CheckNotNull(A);

            if (A.GetLength(1) <= columnIndex)
            {
                throw new ArgumentOutOfRangeException($"Column index out of bounds. Matrix width = {A.GetLength(1)}, columnIndex = {columnIndex}.");
            }

            int n = A.GetLength(0);

            T[] col = new T[n];
            for (int i = 0; i < n; i++)
            {
                col[i] = A[i, columnIndex];
            }
            return col;
        }

        /// <summary>
        /// Copy a column of a matrix into an array.
        /// </summary>
        /// <param name="A">The matrix.</param>
        /// <param name="columnIndex">The index of the column to copy.</param>
        /// <param name="column">An array that will store the values of that column.</param>
        /// <cat>la</cat>
        public static void Column<T>(this T[,] A, int columnIndex, T[] column)
        {
            MatrixChecks.CheckNotNull(A);

            if (A.GetLength(1) <= columnIndex)
            {
                throw new ArgumentOutOfRangeException($"Column index out of bounds. Matrix width = {A.GetLength(1)}, columnIndex = {columnIndex}.");
            }
            if (A.GetLength(0) != column.Length)
            {
                throw new ArgumentOutOfRangeException($"Matrix rows does not match column vector length. {A.GetLength(0)} vs {column.Length}");
            }

            int n = column.Length, i;
            for (i = 0; i < n; i++)
            {
                column[i] = A[i, columnIndex];
            }
        }

        /// <summary>
        /// Returns the row at index <txt>rowIndex</txt>.
        /// </summary>
        /// <param name="A">A matrix.</param>
        /// <param name="rowIndex">The index of the row to extract.</param>
        /// <returns>The row of the matrix.</returns>
        /// <cat>la</cat>
        public static T[] Row<T>(this T[,] A, int rowIndex)
        {
            MatrixChecks.CheckNotNull(A);

            T[] row = new T[A.GetLength(1)];
            Row(A, rowIndex, row);
            return row;
        }

        /// <summary>
        /// Copy a row of the matrix into an array.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="A">The matrix.</param>
        /// <param name="rowIndex">The index of the row to copy.</param>
        /// <param name="row">The array that will hold the values from the matrix row.</param>
        public static void Row<T>(this T[,] A, int rowIndex, T[] row)
        {
            MatrixChecks.CheckNotNull(A);
            if (row == null)
            {
                throw new ArgumentNullException();
            }
            if (rowIndex < 0 || rowIndex >= A.GetLength(0))
            {
                throw new ArgumentOutOfRangeException($"Row index out of bounds. Matrix height = {A.GetLength(0)}, rowIndex = {rowIndex}.");
            }
            if (A.GetLength(1) != row.Length)
            {
                throw new ArgumentOutOfRangeException();
            }

            int len = row.Length, i;
            for (i = 0; i < len; i++)
            {
                row[i] = A[rowIndex, i];
            }
        }

        /// <summary>
        /// Swap two rows of a matrix.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="A">The matrix.</param>
        /// <param name="row1">The first row index to swap.</param>
        /// <param name="row2">The second row index to swap.</param>
        /// <cat>la</cat>
        public static void SwapRows<T>(this T[,] A, int row1, int row2)
        {
            if (A is null)
            {
                throw new ArgumentNullException(nameof(A));
            }

            int m = A.GetLength(0), n = A.GetLength(1), i;
            if (row1 < 0 || row1 >= m)
            {
                throw new ArgumentOutOfRangeException(nameof(row1));
            }
            if (row2 < 0 || row2 >= m)
            {
                throw new ArgumentOutOfRangeException(nameof(row2));
            }

            for (i = 0; i < n; ++i)
            {
                T temp = A[row1, i];
                A[row1, i] = A[row2, i];
                A[row2, i] = temp;
            }
        }
        /// <summary>
        /// Swap two columns of a matrix.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="A">The matrix.</param>
        /// <param name="c1">The first column index to swap.</param>
        /// <param name="c2">The second column index to swap.</param>
        /// <cat>la</cat>
        public static void SwapColumns<T>(this T[,] A, int c1, int c2)
        {
            MatrixChecks.CheckNotNull(A);

            int m = A.GetLength(0), n = A.GetLength(1), i;
            if (c1 < 0 || c2 < 0 || c1 >= n || c2 >= n) throw new ArgumentOutOfRangeException();

            for (i = 0; i < m; ++i)
            {
                T temp = A[i, c1];
                A[i, c1] = A[i, c2];
                A[i, c2] = temp;
            }
        }

        /// <summary>
        /// <p>
        /// Returns a submatrix formed by taking from matrix A the rows [<txt>firstRow</txt>, <txt>firstRow + rows</txt>) and columns 
        /// [<txt>firstCol</txt>, <txt>firstCol + columns</txt>).
        /// </p>
        /// <p>
        /// Returned submatrix will be of dimension <txt>rows</txt> by <txt>columns</txt>.
        /// </p>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="A">The matrix to take the submatrix of.</param>
        /// <param name="firstRow">The index of the first row to include in the submatrix.</param>
        /// <param name="firstCol">The index of the first column to include in the submatrix.</param>
        /// <param name="rows">The number of rows of the submatrix.</param>
        /// <param name="columns">The number of columns of the submatrix.</param>
        /// <returns>The submatrix of size (<txt>rows</txt> by <txt>columns</txt>)</returns>
        /// <cat>la</cat>
        public static T[,] Submatrix<T>(this T[,] A, int firstRow, int firstCol, int rows, int columns)
        {
            if (A is null)
            {
                throw new ArgumentNullException(nameof(A));
            }
            if (A.GetLength(0) < firstRow + rows || A.GetLength(1) < firstCol + columns)
            {
                throw new ArgumentOutOfRangeException("Submatrix refers to a region outside bounds of the matrix.");
            }

            T[,] sub = new T[rows, columns];
            int r, c;
            for (r = 0; r < rows; r++)
            {
                for (c = 0; c < columns; c++)
                {
                    sub[r, c] = A[firstRow + r, firstCol + c];
                }
            }
            return sub;
        }


        /// <summary>
        /// Copy a (rows, cols) block from source to destination matrix, starting with (firstRow, firstCol) in the 
        /// upper left corner.
        /// </summary>
        /// <param name="dest"></param>
        /// <param name="src"></param>
        /// <param name="srcFirstRow"></param>
        /// <param name="srcFirstCol"></param>
        /// <param name="rows"></param>
        /// <param name="cols"></param>
        public static void CopyFrom<T>(this T[,] dest, T[,] src, int srcFirstRow, int srcFirstCol, int destFirstRow, int destFirstCol, int rows, int cols)
        {
            MatrixChecks.CheckNotNull(dest);
            MatrixChecks.CheckNotNull(src);

            if (srcFirstRow < 0 || srcFirstCol < 0 || rows < 0 || cols < 0 || destFirstRow < 0 || destFirstCol < 0)
            {
                throw new ArgumentOutOfRangeException("Index is less than 0.");
            }
            if (srcFirstRow + rows > src.GetLength(0) || srcFirstCol + cols > src.GetLength(1))
            {
                throw new ArgumentOutOfRangeException("Index refers to a location outside the source matrix.");
            }
            if (destFirstRow + rows > dest.GetLength(0) || destFirstCol + cols > dest.GetLength(1))
            {
                throw new ArgumentOutOfRangeException("Index refers to a location outside the destination matrix.");
            }

            int r, c;
            for (r = 0; r < rows; ++r)
            {
                int dest_row = destFirstRow + r, src_row = srcFirstRow + r;
                for (c = 0; c < cols; ++c)
                {
                    dest[dest_row, destFirstCol + c] = src[src_row, srcFirstCol + c];
                }
            }
        }
        public static void CopyFrom<T>(this T[,] dest, T[,] src, int srcFirstRow, int srcFirstCol, int destFirstRow, int destFirstCol)
        {
            CopyFrom(dest, src, srcFirstRow, srcFirstCol, destFirstRow, destFirstCol, src.GetLength(0), src.GetLength(1));
        }

        public static T[,] Copy<T>(this T[,] a)
        {
            return a.Clone() as T[,];
        }
    }
}
