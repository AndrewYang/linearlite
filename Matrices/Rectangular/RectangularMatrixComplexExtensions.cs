﻿using LinearNet.Structs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet
{
    public static class RectangularMatrixComplexExtensions
    {
        /// <summary>
        /// Calculates A^H, the conjugate transpose of A, where (A^H)(i, j) = conjugate(A(j, i))
        /// </summary>
        /// <param name="a"></param>
        /// <returns></returns>
        public static Complex[,] ConjugateTranspose(this Complex[,] a)
        {
            MatrixChecks.CheckNotNull(a);

            int rows = a.GetLength(0), cols = a.GetLength(1), r, c;
            Complex[,] transpose = new Complex[cols, rows];
            for (r = 0; r < rows; r++)
            {
                for (c = 0; c < cols; c++)
                {
                    transpose[c, r] = a[r, c].Conjugate();
                }
            }
            return transpose;
        }

    }
}
