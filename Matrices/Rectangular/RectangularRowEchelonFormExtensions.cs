﻿using LinearNet.Providers;
using LinearNet.Decomposition;
using LinearNet.Global;
using LinearNet.Matrices;
using LinearNet.Structs;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet
{
    public static class RectangularRowEchelonFormExtensions
    {
        /// <summary>
        /// Converts a matrix into its row echelon form, using Gaussian elimination. The original matrix is 
        /// unchanged.
        /// </summary>
        /// <param name="A">Any non-degenerate real matrix.</param>
        /// <returns>The matrix in row echelon form.</returns>
        public static double[,] ToRowEchelonForm(this double[,] A)
        {
            MatrixChecks.CheckNotNull(A);

            int rows = A.GetLength(0), cols = A.GetLength(1), r, c, i;

            double[,] re = A.Copy();

            int n = Math.Min(rows, cols);
            for (c = 0; c < n; ++c)
            {
                for (r = c + 1; r < n; ++r)
                {
                    double s = re[r, c] / re[c, c];
                    for (i = c; i < cols; i++)
                    {
                        re[r, i] -= s * re[c, i];
                    }
                }
            }

            // If there are any extra rows - remove them
            for (r = n; r < rows; ++r)
            {
                for (c = 0; c < cols; ++c)
                {
                    re[r, c] = 0;
                }
            }
            return re;
        }
        public static float[,] ToRowEchelonForm(this float[,] A)
        {
            MatrixChecks.CheckNotNull(A);

            int rows = A.GetLength(0), cols = A.GetLength(1), r, c, i;

            float[,] re = A.Copy();

            int n = Math.Min(rows, cols);
            for (c = 0; c < n; ++c)
            {
                for (r = c + 1; r < n; ++r)
                {
                    float s = re[r, c] / re[c, c];
                    for (i = c; i < cols; i++)
                    {
                        re[r, i] -= s * re[c, i];
                    }
                }
            }

            // If there are any extra rows - remove them
            for (r = n; r < rows; ++r)
            {
                for (c = 0; c < cols; ++c)
                {
                    re[r, c] = 0f;
                }
            }
            return re;
        }
        public static decimal[,] ToRowEchelonForm(this decimal[,] A)
        {
            MatrixChecks.CheckNotNull(A);

            int rows = A.GetLength(0), cols = A.GetLength(1), r, c, i;

            decimal[,] re = A.Copy();

            int n = Math.Min(rows, cols);
            for (c = 0; c < n; ++c)
            {
                for (r = c + 1; r < n; ++r)
                {
                    decimal s = re[r, c] / re[c, c];
                    for (i = c; i < cols; i++)
                    {
                        re[r, i] -= s * re[c, i];
                    }
                }
            }

            // If there are any extra rows - remove them
            for (r = n; r < rows; ++r)
            {
                for (c = 0; c < cols; ++c)
                {
                    re[r, c] = 0m;
                }
            }
            return re;
        }
        public static T[,] ToRowEchelonForm<T>(this T[,] a) where T : IField<T>, new()
        {
            MatrixChecks.CheckNotNull(a);

            int rows = a.GetLength(0), cols = a.GetLength(1), r, c, i;

            T[,] re = a.Copy();
            int n = Math.Min(rows, cols);
            for (c = 0; c < n; ++c)
            {
                for (r = c + 1; r < n; ++r)
                {
                    T s = re[r, c].Divide(re[c, c]);
                    for (i = c; i < cols; i++)
                    {
                        re[r, i] = re[r, i].Subtract(s.Multiply(re[c, i]));
                    }
                }
            }

            // If there are any extra rows - remove them
            T zero = a[0, 0].AdditiveIdentity;
            for (r = n; r < rows; ++r)
            {
                for (c = 0; c < cols; ++c)
                {
                    re[r, c] = zero;
                }
            }
            return re;
        }
    }
}
