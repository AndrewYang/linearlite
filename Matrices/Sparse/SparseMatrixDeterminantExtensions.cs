﻿using LinearNet.Providers;
using LinearNet.Structs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Matrices.Sparse
{
    public static class SparseMatrixDeterminantExtensions
    {
        /// <summary>
        /// This method is inferior to the one implemented for double types, since it does not use log determinants to combat numeric 
        /// overflow/underflow.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="matrix"></param>
        /// <param name="blas"></param>
        /// <param name="Norm"></param>
        /// <returns></returns>
        private static T det_with_pivot<T>(this DOKSparseMatrix<T> matrix, IProvider<T> blas, bool usePivot, Func<T, double> Norm) where T : new()
        {
            Dictionary<long, Dictionary<long, T>> values = matrix.GroupByRows();
            T zero = blas.Zero;
            long m = matrix.Rows, r, c;

            // Rank deficient matrices have 0 determinant 
            if (values.Count < m)
            {
                return zero;
            }

            bool negative = false;
            T determinant = blas.One;
            for (c = 0; c < m; ++c)
            {
                Dictionary<long, T> values_c = values[c];

                if (usePivot)
                {
                    double pivot = values_c.ContainsKey(c) ? Norm(values_c[c]) : 0.0;
                    long pivot_row = c;
                    for (r = c + 1; r < m; ++r)
                    {
                        Dictionary<long, T> values_r = values[r];
                        double pivot_candidate = values_r.ContainsKey(c) ? Norm(values_r[c]) : 0.0;
                        if (pivot_candidate > pivot)
                        {
                            pivot = pivot_candidate;
                            pivot_row = r;
                        }
                    }

                    // Pivot value is 0, matrix has a 0 determinant
                    if (pivot == 0.0)
                    {
                        return zero;
                    }

                    // Perform row swap
                    if (pivot_row != c)
                    {
                        negative = !negative;

                        values[c] = values[pivot_row];
                        values[pivot_row] = values_c;
                        values_c = values[c];
                    }
                }

                // Row echelon form reduction
                T values_cc = values_c.ContainsKey(c) ? values_c[c] : zero;
                for (r = c + 1; r < m; ++r)
                {
                    Dictionary<long, T> values_r = values[r];
                    T values_rc = values_r.ContainsKey(c) ? values_r[c] : zero;
                    T factor = blas.Divide(values_rc, values_cc);

                    // Perform sparse YSAX values_r -= alpha * values_c
                    foreach (KeyValuePair<long, T> pair in values_c)
                    {
                        long key = pair.Key;
                        if (values_r.ContainsKey(key))
                        {
                            values_r[key] = blas.Subtract(values_r[key], blas.Multiply(factor, pair.Value));
                        }
                        else
                        {
                            values_r[key] = blas.Negate(blas.Multiply(factor, pair.Value));
                        }
                    }

                    // (r, c) will be 0
                    values_r.Remove(c);
                }

                determinant = blas.Multiply(determinant, values_cc);
            }

            if (negative)
            {
                return blas.Negate(determinant);
            }
            return determinant;
        }

        public static float Determinant(this DOKSparseMatrix<float> matrix) => det_with_pivot(matrix, new FloatProvider(), true, x => Math.Abs(x));
        public static decimal Determinant(this DOKSparseMatrix<decimal> matrix) => det_with_pivot(matrix, new DecimalProvider(), true, x => (double)Math.Abs(x));
        public static Complex Determinant(this DOKSparseMatrix<Complex> matrix) => det_with_pivot(matrix, new ComplexProvider(), true, x => x.Modulus());
        public static T Determinant<T>(this DOKSparseMatrix<T> matrix) where T : IField<T>, new() => det_with_pivot(matrix, new FieldProvider<T>(), false, x => 0.0);
        public static double Determinant(this DOKSparseMatrix<double> matrix)
        {
            Dictionary<long, Dictionary<long, double>> values = matrix.GroupByRows();
            long m = matrix.Rows, r, c;

            // Rank deficient matrices have 0 determinant 
            if (values.Count < m)
            {
                return 0.0;
            }

            bool negative = false;
            double logDeterminant = 0.0;
            for (c = 0; c < m; ++c)
            {
                Dictionary<long, double> values_c = values[c];

                double pivot = values_c.ContainsKey(c) ? Math.Abs(values_c[c]) : 0.0;
                long pivot_row = c;
                for (r = c + 1; r < m; ++r)
                {
                    Dictionary<long, double> values_r = values[r];
                    double pivot_candidate = values_r.ContainsKey(c) ? Math.Abs(values_r[c]) : 0.0;
                    if (pivot_candidate > pivot)
                    {
                        pivot = pivot_candidate;
                        pivot_row = r;
                    }
                }

                // Pivot value is 0, matrix has a 0 determinant
                if (pivot == 0.0)
                {
                    return 0.0;
                }

                // Perform row swap
                if (pivot_row != c)
                {
                    negative = !negative;

                    values[c] = values[pivot_row];
                    values[pivot_row] = values_c;
                    values_c = values[c];
                }

                // Row echelon form reduction
                double values_cc = values_c.ContainsKey(c) ? values_c[c] : 0.0;
                for (r = c + 1; r < m; ++r)
                {
                    Dictionary<long, double> values_r = values[r];
                    double values_rc = values_r.ContainsKey(c) ? values_r[c] : 0.0;
                    double factor = values_rc / values_cc;

                    // Perform sparse YSAX values_r -= alpha * values_c
                    foreach (KeyValuePair<long, double> pair in values_c)
                    {
                        if (values_r.ContainsKey(pair.Key))
                        {
                            values_r[pair.Key] -= factor * pair.Value;
                        }
                        else
                        {
                            values_r[pair.Key] = -factor * pair.Value;
                        }
                    }

                    // (r, c) will be 0
                    values_r.Remove(c);
                }

                logDeterminant += Math.Log(Math.Abs(values_cc));
                if (values_cc < 0)
                {
                    negative = !negative;
                }
            }

            if (negative)
            {
                return -Math.Exp(logDeterminant);
            }
            return Math.Exp(logDeterminant);
        }
    }
}
