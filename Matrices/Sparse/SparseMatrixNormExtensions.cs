﻿using LinearNet.Providers;
using LinearNet.Helpers;
using LinearNet.Structs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet
{
    public static class SparseMatrixNormExtensions
    {
        public static double ElementwiseNorm(this DOKSparseMatrix<int> A, int p, int q) => ElementwiseNorm(A, p, q, new DoubleProvider(), x => Math.Abs(x), (a, b) => Math.Pow(a, b));
        public static double ElementwiseNorm(this DOKSparseMatrix<long> A, int p, int q) => ElementwiseNorm(A, p, q, new DoubleProvider(), x => Math.Abs(x), (a, b) => Math.Pow(a, b));
        public static float ElementwiseNorm(this DOKSparseMatrix<float> A, int p, int q) => ElementwiseNorm(A, p, q, new FloatProvider(), x => Math.Abs(x), (a, b) => (float)Math.Pow(a, b));
        public static double ElementwiseNorm(this DOKSparseMatrix<double> A, int p, int q) => ElementwiseNorm(A, p, q, new DoubleProvider(), x => Math.Abs(x), (a, b) => Math.Pow(a, b));
        public static decimal ElementwiseNorm(this DOKSparseMatrix<decimal> A, int p, int q) => ElementwiseNorm(A, p, q, new DecimalProvider(), x => Math.Abs(x), (a, b) => DecimalMath.Pow(a, (decimal)b));
        public static double ElementwiseNorm(this DOKSparseMatrix<Complex> A, int p, int q) => ElementwiseNorm(A, p, q, new DoubleProvider(), x => x.Modulus(), (a, b) => Math.Pow(a, b));
        private static F ElementwiseNorm<T, F>(this DOKSparseMatrix<T> A, int p, int q, IProvider<F> blas, Func<T, F> Norm, Func<F, double, F> Pow) where T : new()
        {
            MatrixChecks.CheckNotNull(A);
            if (p < 1) throw new ArgumentOutOfRangeException();
            if (q < 1) throw new ArgumentOutOfRangeException();

            int columns = A.Columns;
            F norm = blas.Zero;

            // L(1, 1) norm special case
            if (p == 1 && q == 1)
            {
                foreach (T value in A.Values.Values)
                {
                    norm = blas.Add(norm, Norm(value));
                }
                return norm;
            }

            double p_q = q / (double)p;

            Dictionary<long, F> columnSums = new Dictionary<long, F>();
            foreach (KeyValuePair<long, T> pair in A.Values)
            {
                long column = pair.Key % columns;
                if (columnSums.ContainsKey(column))
                {
                    columnSums[column] = blas.Add(columnSums[column], Pow(Norm(pair.Value), p));
                }
            }

            foreach (F value in columnSums.Values)
            {
                norm = blas.Add(norm, Pow(value, p_q));
            }
            return Pow(norm, 1.0 / q);
        }

        public static int InducedInfinityNorm(this DOKSparseMatrix<int> A) => InducedInfinityNorm(A, new Int32Provider(), x => Math.Abs(x));
        public static long InducedInfinityNorm(this DOKSparseMatrix<long> A) => InducedInfinityNorm(A, new Int64Provider(), x => Math.Abs(x));
        public static float InducedInfinityNorm(this DOKSparseMatrix<float> A) => InducedInfinityNorm(A, new FloatProvider(), x => Math.Abs(x));
        public static double InducedInfinityNorm(this DOKSparseMatrix<double> A) => InducedInfinityNorm(A, new DoubleProvider(), x => Math.Abs(x));
        public static decimal InducedInfinityNorm(this DOKSparseMatrix<decimal> A) => InducedInfinityNorm(A, new DecimalProvider(), x => Math.Abs(x));
        public static double InducedInfinityNorm(this DOKSparseMatrix<Complex> A) => InducedInfinityNorm(A, new DoubleProvider(), x => x.Modulus());
        private static F InducedInfinityNorm<T, F>(this DOKSparseMatrix<T> A, IProvider<F> blas, Func<T, F> Norm) where F : IComparable<F> where T : new()  
        {
            int n = A.Columns;
            F max = blas.Zero;

            // Group by row 
            Dictionary<long, F> rowSums = new Dictionary<long, F>();
            foreach (KeyValuePair<long, T> pair in A.Values)
            {
                long row = pair.Key / n;
                if (rowSums.ContainsKey(row))
                {
                    rowSums[row] = blas.Add(rowSums[row], Norm(pair.Value));
                }
                else
                {
                    rowSums[row] = Norm(pair.Value);
                }
            }

            foreach (F value in rowSums.Values)
            {
                if (value.CompareTo(max) > 0)
                {
                    max = value;
                }
            }
            return max;
        }
    }
}
