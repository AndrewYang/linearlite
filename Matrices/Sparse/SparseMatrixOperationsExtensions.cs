﻿using LinearNet.Providers;
using LinearNet.Helpers;
using LinearNet.Structs;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LinearNet
{
    public static class SparseMatrixOperationsExtensions
    {
        /// <summary>
        /// <p>Returns the sum of two sparse matrices $A$ and $B$ of the same size, without altering either matrix.</p>
        /// <p>Throws <txt>InvalidOperationException</txt> if matrix dimensions don't match.</p>
        /// <p>Implemented for types <txt>int, long, float, double, decimal</txt> and all custom types <txt>T</txt>
        /// that implement the interface <txt>IAdditiveGroup&ltT&gt</txt>.</p>
        /// </summary> 
        /// <param name="A">The first matrix to be added.</param>
        /// <param name="B">The second matrix to be added.</param>
        /// <returns>The matrix sum as a sparse matrix.</returns>
        /// <cat>la</cat>
        public static DOKSparseMatrix<int> Add(this DOKSparseMatrix<int> A, DOKSparseMatrix<int> B) => A.Add(B, DefaultProviders.Int32);
        public static DOKSparseMatrix<long> Add(this DOKSparseMatrix<long> A, DOKSparseMatrix<long> B) => A.Add(B, DefaultProviders.Int64);
        public static DOKSparseMatrix<float> Add(this DOKSparseMatrix<float> A, DOKSparseMatrix<float> B) => A.Add(B, DefaultProviders.Float);
        public static DOKSparseMatrix<double> Add(this DOKSparseMatrix<double> A, DOKSparseMatrix<double> B) => A.Add(B, DefaultProviders.Double);
        public static DOKSparseMatrix<decimal> Add(this DOKSparseMatrix<decimal> A, DOKSparseMatrix<decimal> B) => A.Add(B, DefaultProviders.Decimal);
        public static DOKSparseMatrix<T> Add<T>(this DOKSparseMatrix<T> A, DOKSparseMatrix<T> B) where T : IAdditiveGroup<T>, new() => A.Add(B, new AdditiveGroupProvider<T>());

        /// <summary>
        /// <p>Returns the difference of two sparse matrices $A$ and $B$ of the same size, without altering either matrix.</p>
        /// <p>Throws <txt>InvalidOperationException</txt> if the matrix dimensions don't match.</p>
        /// <p>Implemented for types <txt>int, long, float, double, decimal</txt> as well as all custom types <txt>T</txt>
        /// that implement the interface <txt>IAdditiveGroup&ltT&gt</txt>.</p>
        /// </summary>
        /// <param name="A">The first matrix.</param>
        /// <param name="B">The second matrix.</param>
        /// <returns>The first matrix subtract the second matrix, as a sparse matrix.</returns>
        /// <cat>la</cat>
        public static DOKSparseMatrix<int> Subtract(this DOKSparseMatrix<int> A, DOKSparseMatrix<int> B) => A.Subtract(B, new Int32Provider());
        public static DOKSparseMatrix<long> Subtract(this DOKSparseMatrix<long> A, DOKSparseMatrix<long> B) => A.Subtract(B, new Int64Provider());
        public static DOKSparseMatrix<float> Subtract(this DOKSparseMatrix<float> A, DOKSparseMatrix<float> B) => A.Subtract(B, new FloatProvider());
        public static DOKSparseMatrix<double> Subtract(this DOKSparseMatrix<double> A, DOKSparseMatrix<double> B) => A.Subtract(B, new DoubleProvider());
        public static DOKSparseMatrix<decimal> Subtract(this DOKSparseMatrix<decimal> A, DOKSparseMatrix<decimal> B) => A.Subtract(B, new DecimalProvider());
        public static DOKSparseMatrix<T> Subtract<T>(this DOKSparseMatrix<T> A, DOKSparseMatrix<T> B) where T : IAdditiveGroup<T>, new() => A.Subtract(B, new AdditiveGroupProvider<T>());

        /*
        /// <summary>
        /// <p>Returns the matrix product of two sparse matrices $A$ and $B$ of compatible dimensions, without altering either matrix.</p>
        /// <p>Implemented for types <txt>int, long, float, double, decimal</txt> as well as all types <txt>T</txt> that implement the 
        /// interface <txt>IRing&ltT&gt</txt>.</p>
        /// </summary>
        /// <param name="A">The left matrix to be multiplied.</param>
        /// <param name="B">The right matrix to be multiplied.</param>
        /// <returns>The matrix product $AB$.</returns>
        /// <cat>la</cat>
        public static DOKSparseMatrix<int> Multiply(this DOKSparseMatrix<int> A, DOKSparseMatrix<int> B) => A.Multiply(B, new NativeInt32Provider());
        public static DOKSparseMatrix<long> Multiply(this DOKSparseMatrix<long> A, DOKSparseMatrix<long> B) => A.Multiply(B, new NativeInt64Provider());
        public static DOKSparseMatrix<float> Multiply(this DOKSparseMatrix<float> A, DOKSparseMatrix<float> B) => A.Multiply(B, new NativeFloatProvider());
        public static DOKSparseMatrix<double> Multiply(this DOKSparseMatrix<double> A, DOKSparseMatrix<double> B) => A.Multiply(B, new NativeDoubleProvider());
        public static DOKSparseMatrix<decimal> Multiply(this DOKSparseMatrix<decimal> A, DOKSparseMatrix<decimal> B) => A.Multiply(B, new NativeDecimalProvider());
        public static DOKSparseMatrix<T> Multiply<T>(this DOKSparseMatrix<T> A, DOKSparseMatrix<T> B) where T : IRing<T>, new() => A.Multiply(B, new NativeRingProvider<T>());
        */

        /// <summary>
        /// <p>Calculates a sparse matrix $A$ multiplied by a sparse vector $x$, returning the product $Ax$ as a sparse vector.</p>
        /// <p>The origin matrix and vector are unchanged.</p>
        /// <p>Throws <txt>InvalidOperationException</txt> if the number of columns of $A$ does not equal the dimension of $x$.</p>
        /// <p>Supported for types <txt>int, long, float, double, decimal</txt> as well as all types <txt>T</txt> that implement the interface 
        /// <txt>IRing&ltT&gt</txt>.</p>
        /// </summary>
        /// <param name="A">The matrix to be multiplied.</param>
        /// <param name="x">The sparse vector to be multiplied.</param>
        /// <returns>The matrix-vector product as a sparse vector.</returns>
        /// <cat>la</cat>
        public static BigSparseVector<int> Multiply(this DOKSparseMatrix<int> A, BigSparseVector<int> x) => A.Multiply(x, new Int32Provider());
        public static BigSparseVector<long> Multiply(this DOKSparseMatrix<long> A, BigSparseVector<long> B) => A.Multiply(B, new Int64Provider());
        public static BigSparseVector<float> Multiply(this DOKSparseMatrix<float> A, BigSparseVector<float> B) => A.Multiply(B, new FloatProvider());
        public static BigSparseVector<double> Multiply(this DOKSparseMatrix<double> A, BigSparseVector<double> B) => A.Multiply(B, new DoubleProvider());
        public static BigSparseVector<decimal> Multiply(this DOKSparseMatrix<decimal> A, BigSparseVector<decimal> B) => A.Multiply(B, new DecimalProvider());
        public static BigSparseVector<T> Multiply<T>(this DOKSparseMatrix<T> A, BigSparseVector<T> B) where T : IRing<T>, new() => A.Multiply(B, new RingProvider<T>());

        /// <summary>
        /// Calculates a sparse matrix $A$ multiplied by a dense vector $x$ represented as an array.
        /// </summary>
        /// <param name="A">The sparse matrix to be multiplied.</param>
        /// <param name="x">The dense vector.</param>
        /// <returns>The matrix-vector product as a sparse vector.</returns>
        /// <cat>la</cat>
        public static BigSparseVector<int> Multiply(this DOKSparseMatrix<int> A, int[] x) => A.Multiply(x, new Int32Provider());
        public static BigSparseVector<long> Multiply(this DOKSparseMatrix<long> A, long[] x) => A.Multiply(x, new Int64Provider());
        public static BigSparseVector<float> Multiply(this DOKSparseMatrix<float> A, float[] x) => A.Multiply(x, new FloatProvider());
        public static BigSparseVector<double> Multiply(this DOKSparseMatrix<double> A, double[] x) => A.Multiply(x, new DoubleProvider());
        public static BigSparseVector<decimal> Multiply(this DOKSparseMatrix<decimal> A, decimal[] x) => A.Multiply(x, new DecimalProvider());
        public static BigSparseVector<T> Multiply<T>(this DOKSparseMatrix<T> A, T[] x) where T : IRing<T>, new() => A.Multiply(x, new RingProvider<T>());

        /// <summary>
        /// Multiplies a sparse matrix with a scalar.
        /// </summary>
        /// <param name="A">The sparse matrix to be multiplied.</param>
        /// <param name="s">A scalar</param>
        /// <returns>The matrix-scalar product as a sparse matrix.</returns>
        /// <cat>la</cat>
        public static DOKSparseMatrix<int> Multiply(this DOKSparseMatrix<int> A, int s) => A.Multiply(s, DefaultProviders.Int32);
        public static DOKSparseMatrix<long> Multiply(this DOKSparseMatrix<long> A, long s) => A.Multiply(s, DefaultProviders.Int64);
        public static DOKSparseMatrix<float> Multiply(this DOKSparseMatrix<float> A, float s) => A.Multiply(s, DefaultProviders.Float);
        public static DOKSparseMatrix<double> Multiply(this DOKSparseMatrix<double> A, double s) => A.Multiply(s, DefaultProviders.Double);
        public static DOKSparseMatrix<decimal> Multiply(this DOKSparseMatrix<decimal> A, decimal s) => A.Multiply(s, DefaultProviders.Decimal);
        public static DOKSparseMatrix<T> Multiply<T>(this DOKSparseMatrix<T> A, T s) where T : IRing<T>, new() => A.Multiply(s, new RingProvider<T>());

        private static DOKSparseMatrix<T> HadamardProduct<T>(this DOKSparseMatrix<T> A, DOKSparseMatrix<T> B, IProvider<T> blas) where T : new()
        {
            MatrixChecks.CheckNotNull(A, B);
            MatrixChecks.AssertMatrixDimensionsEqual(A, B);

            Dictionary<long, T> product = new Dictionary<long, T>(Math.Max(A.Values.Count, B.Values.Count));
            Dictionary<long, T> _B = B.Values;
            foreach (KeyValuePair<long, T> pair in A.Values)
            {
                if (_B.ContainsKey(pair.Key))
                {
                    product[pair.Key] = blas.Multiply(pair.Value, _B[pair.Key]);
                }
            }
            return new DOKSparseMatrix<T>(A.Rows, A.Columns, product);
        }
        /// <summary>
        /// Returns the Hadamard product between two sparse matrices $A$ and $B$, $A \circ B$, without altering either matrix.
        /// </summary>
        /// <param name="A">The first matrix to be multiplied.</param>
        /// <param name="B">The second matrix to be multiplied.</param>
        /// <returns>The Hadamard product of the two matrices as a sparse matrix.</returns>
        /// <cat>la</cat>
        public static DOKSparseMatrix<int> HadamardProduct(this DOKSparseMatrix<int> A, DOKSparseMatrix<int> B) => HadamardProduct(A, B, new Int32Provider());
        public static DOKSparseMatrix<long> HadamardProduct(this DOKSparseMatrix<long> A, DOKSparseMatrix<long> B) => HadamardProduct(A, B, new Int64Provider());
        public static DOKSparseMatrix<float> HadamardProduct(this DOKSparseMatrix<float> A, DOKSparseMatrix<float> B) => HadamardProduct(A, B, new FloatProvider());
        public static DOKSparseMatrix<double> HadamardProduct(this DOKSparseMatrix<double> A, DOKSparseMatrix<double> B) => HadamardProduct(A, B, new DoubleProvider());
        public static DOKSparseMatrix<decimal> HadamardProduct(this DOKSparseMatrix<decimal> A, DOKSparseMatrix<decimal> B) => HadamardProduct(A, B, new DecimalProvider());
        public static DOKSparseMatrix<T> HadamardProduct<T>(this DOKSparseMatrix<T> A, DOKSparseMatrix<T> B) where T : IRing<T>, new() => HadamardProduct(A, B, new RingProvider<T>());

        /// <summary>
        /// Returns the Hadamard power of a sparse matrix $A^{\circ n}$, without altering the original matrix.
        /// </summary>
        /// <param name="A">A sparse matrix.</param>
        /// <param name="power">The power to raise each element.</param>
        /// <returns>The Hadamard power of a sparse matrix.</returns>
        /// <cat>la</cat>
        public static DOKSparseMatrix<int> HadamardPower(this DOKSparseMatrix<int> A, int power) => A.Elementwise(x => IntegerMath.Pow(x, power));
        public static DOKSparseMatrix<long> HadamardPower(this DOKSparseMatrix<long> A, long power) => A.Elementwise(x => IntegerMath.Pow(x, power));
        public static DOKSparseMatrix<float> HadamardPower(this DOKSparseMatrix<float> A, float power) => A.Elementwise(x => (float)Math.Pow(x, power));
        public static DOKSparseMatrix<double> HadamardPower(this DOKSparseMatrix<double> A, double power) => A.Elementwise(x => Math.Pow(x, power));
        public static DOKSparseMatrix<decimal> HadamardPower(this DOKSparseMatrix<decimal> A, decimal power) => A.Elementwise(x => DecimalMath.Pow(x, power));

        /// <summary>
        /// <p>Returns the Kronecker product between two sparse matrices $A$ and $B$, without altering either matrix.</p>
        /// <p>Supported for sparse matrices over the types <txt>int, long, float, double, decimal</txt> and all types <txt>T</txt> that implement 
        /// the interface <txt>IRing&ltT&gt</txt>.</p>
        /// </summary>
        /// <param name="A">The first matrix.</param>
        /// <param name="B">The second matrix.</param>
        /// <returns>The Kronecker product between two sparse matrices.</returns>
        /// <cat>la</cat>
        public static DOKSparseMatrix<int> KroneckerProduct(this DOKSparseMatrix<int> A, DOKSparseMatrix<int> B) => A.KroneckerProduct(B, new Int32Provider());
        public static DOKSparseMatrix<long> KroneckerProduct(this DOKSparseMatrix<long> A, DOKSparseMatrix<long> B) => A.KroneckerProduct(B, new Int64Provider());
        public static DOKSparseMatrix<float> KroneckerProduct(this DOKSparseMatrix<float> A, DOKSparseMatrix<float> B) => A.KroneckerProduct(B, new FloatProvider());
        public static DOKSparseMatrix<double> KroneckerProduct(this DOKSparseMatrix<double> A, DOKSparseMatrix<double> B) => A.KroneckerProduct(B, new DoubleProvider());
        public static DOKSparseMatrix<decimal> KroneckerProduct(this DOKSparseMatrix<decimal> A, DOKSparseMatrix<decimal> B) => A.KroneckerProduct(B, new DecimalProvider());
        public static DOKSparseMatrix<T> KroneckerProduct<T>(this DOKSparseMatrix<T> A, DOKSparseMatrix<T> B) where T : IRing<T>, new() => A.KroneckerProduct(B, new RingProvider<T>());

        /// <summary>
        /// Returns the conjugate transpose of a sparse matrix with complex elements.
        /// </summary>
        /// <param name="A">A complex sparse matrix.</param>
        /// <returns>The conjugate transpose of a matrix, as a sparse matrix.</returns>
        /// <cat>la</cat>
        public static DOKSparseMatrix<Complex> ConjugateTranspose(this DOKSparseMatrix<Complex> A)
        {
            DOKSparseMatrix<Complex> transpose = A.Transpose();

            Dictionary<long, Complex> values = transpose.Values;
            List<long> keys = values.Keys.ToList();
            foreach (long key in keys)
            {
                // Convert into conjugate
                values[key] = values[key].Conjugate();
            }
            return transpose;
        }

        /// <summary>
        /// <p>Returns the trace of a sparse matrix.</p>
        /// <p>Supported for the types <txt>int, long, float, double, decimal</txt> and all custom types <txt>T</txt> that implement 
        /// the interface <txt>IAdditiveGroup&ltT&gt</txt>.</p>
        /// </summary>
        /// <param name="A">A sparse matrix.</param>
        /// <returns>The matrix trace.</returns>
        /// <cat>la</cat>
        public static int Trace(this DOKSparseMatrix<int> A) => A.Trace(new Int32Provider());
        public static long Trace(this DOKSparseMatrix<long> A) => A.Trace(new Int64Provider());
        public static float Trace(this DOKSparseMatrix<float> A) => A.Trace(new FloatProvider());
        public static double Trace(this DOKSparseMatrix<double> A) => A.Trace(new DoubleProvider());
        public static decimal Trace(this DOKSparseMatrix<decimal> A) => A.Trace(new DecimalProvider());
        public static T Trace<T>(this DOKSparseMatrix<T> A) where T : IAdditiveGroup<T>, new() => A.Trace(new AdditiveGroupProvider<T>());

    }
}
