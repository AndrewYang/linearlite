﻿using LinearNet.Graphs;
using LinearNet.Structs;

namespace LinearNet.Matrices.Sparse.Symbolic
{
    internal class CSCSymbolic<T> where T : new()
    {
        internal SymbolicCholesky AnalyseCholesky(CSCSparseMatrix<T> A)
        {
            // Calculate the climination tree
            EliminationTree etree = EliminationTree.Calculate(A);

            // The post-ordering of nodes
            int[] post = new int[A.Rows];
            etree.CalculatePostOrdering(post);

            // Calculate the column indices for the L factor
            int[] columnIndices = CalculateColumnIndices(A, etree, post);
            return new SymbolicCholesky(etree, columnIndices);
        }

        /// <summary>
        /// Row count algorithm determines the number of non-zeroes in each row of the Cholesky factor L, given a matrix A and its 
        /// elimination tree. Based on the algorithm presented in Davis, T. (2006).
        /// </summary>
        /// <param name="etree"></param>
        /// <param name="postOrdering"></param>
        /// <param name="level"></param>
        /// <param name="firstDescendant"></param>
        /// <returns></returns>
        internal static int[] CalculateRowIndices(CSCSparseMatrix<T> A, EliminationTree etree, int[] postOrdering, int[] level, int[] firstDescendant)
        {
            int[] columnIndices = A.ColumnIndices,
                rowIndices = A.RowIndices;

            int dim = columnIndices.Length - 1;

            // used to determine the skeleton matrix of A
            int[] maxFirstDescendant = RectangularVector.Repeat(-1, dim);

            // previousLeaf[i] keeps track of the previous leaf of subtree i
            int[] previousLeaf = RectangularVector.Repeat(-1, dim);

            // Stores the results - the number of non-zero elements in each row
            // Initialized to 1 for the non-zero diagonal term in each row
            int[] rowcounts = RectangularVector.Repeat(1, dim + 1);

            // ancestor[i] keeps track of the oldest known ancestor of node i,
            // initialized to itself.
            int[] ancestor = new int[dim];
            for (int i = 0; i < dim; ++i)
            {
                ancestor[i] = i;
            }

            for (int k = 0; k < dim; ++k)
            {
                // Node c is the k-th node under this postordering 
                int c = postOrdering[k],
                    firstDesc = firstDescendant[c];

                int col_end = columnIndices[c + 1];
                for (int i = columnIndices[c]; i < col_end; ++i)
                {
                    int r = rowIndices[i];

                    // Check whether node j is a leaf
                    if (r <= c || firstDesc <= maxFirstDescendant[r])
                    {
                        continue;
                    }

                    // first descendent exceeds all seen so far - update
                    maxFirstDescendant[r] = firstDesc;

                    // Record the previous leaf, and overwrite with this 
                    // node for the next iteration
                    int prev_r = previousLeaf[r];
                    previousLeaf[r] = c;

                    // c is the first leaf
                    if (prev_r < 0)
                    {
                        rowcounts[r] += level[c] - level[r];
                        continue;
                    }

                    // Iterate until q is equal to its own ancestor, i.e. 
                    // it is a root (that we're aware of so far). This will 
                    // be the 'target' to which each step's ancestor will be 
                    // set to in our ancestral path traversal.
                    int root = prev_r;
                    while (root != ancestor[root])
                    {
                        root = ancestor[root];
                    }

                    // Update all ancestors - this is purely for benefit of 
                    // the next loops
                    int pnext;
                    for (int p = prev_r; p != root; p = pnext)
                    {
                        // Skip the parents, go directly to ancestor of p
                        pnext = ancestor[p];

                        // Update the ancestor of this step to the target, root
                        ancestor[p] = root;
                    }

                    // Update the row counts
                    rowcounts[r] += level[c] - level[root];
                }

                if (etree.Parents[c] >= 0)
                {
                    ancestor[c] = etree.Parents[c];
                }
            }

            // Shift and accumulate
            for (int i = dim; i > 0; --i)
            {
                rowcounts[i] = rowcounts[i - 1];
            }
            rowcounts[0] = 0;
            for (int i = 0; i < dim; ++i)
            {
                rowcounts[i + 1] += rowcounts[i];
            }

            return rowcounts;
        }
        internal static int[] CalculateRowIndices(int[] columnIndices, int[] rowIndices, EliminationTree etree, int[] postOrdering, int[] level, int[] firstDescendant)
        {
            int dim = columnIndices.Length - 1;

            // used to determine the skeleton matrix of A
            int[] maxFirstDescendant = RectangularVector.Repeat(-1, dim);

            // previousLeaf[i] keeps track of the previous leaf of subtree i
            int[] previousLeaf = RectangularVector.Repeat(-1, dim);

            // Stores the results - the number of non-zero elements in each row
            // Initialized to 1 for the non-zero diagonal term in each row
            int[] rowcounts = RectangularVector.Repeat(1, dim + 1);

            // ancestor[i] keeps track of the oldest known ancestor of node i,
            // initialized to itself.
            int[] ancestor = new int[dim];
            for (int i = 0; i < dim; ++i)
            {
                ancestor[i] = i;
            }

            for (int k = 0; k < dim; ++k)
            {
                // Node c is the k-th node under this postordering 
                int c = postOrdering[k],
                    firstDesc = firstDescendant[c];

                int col_end = columnIndices[c + 1];
                for (int i = columnIndices[c]; i < col_end; ++i)
                {
                    int r = rowIndices[i];

                    // Check whether node j is a leaf
                    if (r <= c || firstDesc <= maxFirstDescendant[r])
                    {
                        continue;
                    }

                    // first descendent exceeds all seen so far - update
                    maxFirstDescendant[r] = firstDesc;

                    // Record the previous leaf, and overwrite with this 
                    // node for the next iteration
                    int prev_r = previousLeaf[r];
                    previousLeaf[r] = c;

                    // c is the first leaf
                    if (prev_r < 0)
                    {
                        rowcounts[r] += level[c] - level[r];
                        continue;
                    }

                    // Iterate until q is equal to its own ancestor, i.e. 
                    // it is a root (that we're aware of so far). This will 
                    // be the 'target' to which each step's ancestor will be 
                    // set to in our ancestral path traversal.
                    int root = prev_r;
                    while (root != ancestor[root])
                    {
                        root = ancestor[root];
                    }

                    // Update all ancestors - this is purely for benefit of 
                    // the next loops
                    int pnext;
                    for (int p = prev_r; p != root; p = pnext)
                    {
                        // Skip the parents, go directly to ancestor of p
                        pnext = ancestor[p];

                        // Update the ancestor of this step to the target, root
                        ancestor[p] = root;
                    }

                    // Update the row counts
                    rowcounts[r] += level[c] - level[root];
                }

                if (etree.Parents[c] >= 0)
                {
                    ancestor[c] = etree.Parents[c];
                }
            }

            // Shift and accumulate
            for (int i = dim; i > 0; --i)
            {
                rowcounts[i] = rowcounts[i - 1];
            }
            rowcounts[0] = 0;
            for (int i = 0; i < dim; ++i)
            {
                rowcounts[i + 1] += rowcounts[i];
            }

            return rowcounts;
        }

        /// <summary>
        /// Column count algorithm determines the number of symbolic non-zeroes in each column of the Cholesky
        /// factor of a matrix A. Based on the 'cs_counts' algorithm presented in Davis, T. (2006).
        /// The matrix A is defined implicitly using its column indexes and row indexes.
        /// </summary>
        /// <param name="A">The matrix to analyse</param>
        /// <param name="etree">The elimination tree of the matrix.</param>
        /// <param name="postOrdering">A postordering of the matrix.</param>
        /// <returns></returns>
        internal static int[] CalculateColumnIndices(CSCSparseMatrix<T> A, EliminationTree etree, int[] postOrdering)
        {
            int[] columnIndices = A.ColumnIndices, 
                rowIndices = A.RowIndices;

            int dim = columnIndices.Length - 1;

            // Initialize arrays to empty
            int[] ancestor = new int[dim],
                maxFirstDescendant = RectangularVector.Repeat(-1, dim),
                previousLeaf = RectangularVector.Repeat(-1, dim),
                firstDescendant = RectangularVector.Repeat(-1, dim);

            // Allocate the column count array, of size dim + 1
            // because it will be accumulated and returned as column 
            // index array
            int[] counts = new int[dim + 1];

            // Calculate the first descendants and column counts of nodes 
            // that are leaves. 
            for (int k = 0; k < dim; ++k)
            {
                // Get the k-th node unde the postordering (node j)
                int j = postOrdering[k];

                // Borrow the column counts array to store the delta values
                // for each node. Node j is a leaf if it has no first descendant
                // We do not need to calculate all first descendants in another 
                // loop, since we are traversing the elimination tree in postordered
                // order, so if we haven't set the first descendant of node j by the 
                // time we reach node j, it is a leaf. 
                // Since node j is a leaf, we set its delta value to 1.
                if (firstDescendant[j] < 0)
                {
                    counts[j] = 1;

                    // Traverse the tree starting at leaf j and set the first descendant 
                    // of all parents and ancestors of node j to k
                    for (int p = j; p >= 0 && firstDescendant[p] < 0; p = etree.Parents[p])
                    {
                        firstDescendant[p] = k;
                    }
                }
            }

            // Initialize the ancestor array by setting the ancestors of each 
            // node to the node itself (which will be changed as we traverse the tree)
            for (int k = 0; k < dim; ++k)
            {
                ancestor[k] = k;
            }

            for (int k = 0; k < dim; ++k)
            {
                // Get the k-th post-ordered node, c.
                int c = postOrdering[k];

                if (!etree.IsRoot(c))
                {
                    // counts is still holding the delta values, decrement
                    // if j is not a root
                    counts[etree.Parents[c]]--;
                }

                // Store the first descendant of node c
                int firstDesc = firstDescendant[c];

                // Iterate through all entries in the c-th column of A
                int col_start = columnIndices[c],
                    col_end = columnIndices[c + 1];
                for (int i = col_start; i < col_end; ++i)
                {
                    int r = rowIndices[i];

                    // Check whether node c is a leaf
                    if (r <= c || firstDesc <= maxFirstDescendant[r])
                    {
                        continue;
                    }

                    // first descendent exceeds all seen so far - update
                    maxFirstDescendant[r] = firstDesc;

                    // Record the previous leaf, and overwrite with this 
                    // node for the next iteration
                    int prev_r = previousLeaf[r];
                    previousLeaf[r] = c;

                    // c is the first leaf
                    if (prev_r < 0)
                    {
                        // Increment the delta of node c, since A[r, c] is in the 
                        // skeleton matrix of A
                        counts[c]++;
                        continue;
                    }

                    // Iterate until q is equal to its own ancestor, i.e. 
                    // it is a root (that we're aware of so far). This will 
                    // be the 'target' to which each step's ancestor will be 
                    // set to in our ancestral path traversal.
                    int root = prev_r;
                    while (root != ancestor[root])
                    {
                        root = ancestor[root];
                    }

                    // Update all ancestors - this is purely for benefit of 
                    // the next loops
                    int pnext;
                    for (int p = prev_r; p != root; p = pnext)
                    {
                        // Skip the parents, go directly to ancestor of p
                        pnext = ancestor[p];

                        // Update the ancestor of this step to the target, root
                        ancestor[p] = root;
                    }

                    // Subtract away the overlap for the root, since it is 
                    // a node with > 1 children and hence double counted
                    counts[root]--;
                    counts[c]++;
                }

                if (!etree.IsRoot(c))
                {
                    ancestor[c] = etree.Parents[c];
                }
            }

            // Update the column counts with the column counts of all children
            // of each node
            for (int i = 0; i < dim; ++i)
            {
                if (!etree.IsRoot(i))
                {
                    counts[etree.Parents[i]] += counts[i];
                }
            }

            // Shift the column counts array right by 1 
            for (int i = dim; i > 0; --i)
            {
                counts[i] = counts[i - 1];
            }
            counts[0] = 0;

            // Accumulate the column counts array to form the column indices
            for (int i = 0; i < dim; ++i)
            {
                counts[i + 1] += counts[i];
            }
            return counts;
        }

        internal SymbolicQR AnalyseQR(CSCSparseMatrix<T> A)
        {
            // Calculate the climination tree
            EliminationTree etree = EliminationTree.Calculate(A);

            // The post-ordering of nodes
            int[] post = new int[A.Rows];
            etree.CalculatePostOrdering(post);

            // Calculate the column indices for the L factor
            int[] counts = CalculateColumnIndicesATA(A.Transpose(), etree, post, false);
            return new SymbolicQR(etree, counts);
        }

        /// <summary>
        /// Analyze the matrix $A^TA$ given the (possibly rectangular) matrix $A^T$, without explicitly computing 
        /// $A^TA$, using its elimination tree and a suitable postordering
        /// </summary>
        /// <param name="AT"></param>
        private int[] CalculateColumnIndicesATA(CSCSparseMatrix<T> AT, EliminationTree etree, int[] post, bool accumulate)
        {
            int[] columnIndices = AT.ColumnIndices,
                rowIndices = AT.RowIndices;

            int columns = AT.Columns, rows = AT.Rows;

            // Initialize arrays to empty or -1
            int[] ancestor = new int[columns],
                maxFirstDescendant = RectangularVector.Repeat(-1, columns),
                previousLeaf = RectangularVector.Repeat(-1, columns),
                firstDescendant = RectangularVector.Repeat(-1, columns),
                head = RectangularVector.Repeat(-1, columns),
                next = RectangularVector.Repeat(-1, rows + 1); // initialization may be redundant

            // Invert the post ordring and store temporarily into 'ancestor' 
            // (which is not used yet)
            for (int c = 0; c < columns; ++c)
            {
                ancestor[post[c]] = c;
            }

            // Calculate head, next arrays
            for (int r = 0; r < rows; ++r)
            {
                // Set k to the minimum postordered node
                int k = columns, end = columnIndices[r + 1];
                for (int i = columnIndices[r]; i < end; ++i)
                {
                    int _r = ancestor[rowIndices[i]];
                    if (_r < k)
                    {
                        k = _r;
                    }
                }

                // Insert row r into the k-th linked list
                next[r] = head[k];
                head[k] = r;
            }

            // Allocate the column count array, of size dim + 1
            // because it will be accumulated and returned as column 
            // index array
            int[] counts = new int[accumulate ? columns + 1 : columns];

            // Calculate the first descendants and column counts of nodes 
            // that are leaves. 
            for (int k = 0; k < columns; ++k)
            {
                // Get the k-th node unde the postordering (node j)
                int j = post[k];

                // Borrow the column counts array to store the delta values
                // for each node. Node j is a leaf if it has no first descendant
                // We do not need to calculate all first descendants in another 
                // loop, since we are traversing the elimination tree in postordered
                // order, so if we haven't set the first descendant of node j by the 
                // time we reach node j, it is a leaf. 
                // Since node j is a leaf, we set its delta value to 1.
                if (firstDescendant[j] < 0)
                {
                    counts[j] = 1;

                    // Traverse the tree starting at leaf j and set the first descendant 
                    // of all parents and ancestors of node j to k
                    for (int p = j; p >= 0 && firstDescendant[p] < 0; p = etree.Parents[p])
                    {
                        firstDescendant[p] = k;
                    }
                }
            }

            // Initialize the ancestor array by setting the ancestors of each 
            // node to the node itself (which will be changed as we traverse the tree)
            for (int k = 0; k < columns; ++k)
            {
                ancestor[k] = k;
            }

            for (int k = 0; k < columns; ++k)
            {
                // Get the k-th post-ordered node, c.
                int j = post[k];

                if (!etree.IsRoot(j))
                {
                    // counts is still holding the delta values, decrement
                    // if j is not a root
                    counts[etree.Parents[j]]--;
                }

                for (int c = head[k]; c != -1; c = next[c])
                {
                    // Store the first descendant of node c
                    int firstDesc = firstDescendant[c];

                    // Iterate through all entries in the c-th column of A
                    int col_start = columnIndices[c],
                        col_end = columnIndices[c + 1];
                    for (int i = col_start; i < col_end; ++i)
                    {
                        int r = rowIndices[i];

                        // Check whether node c is a leaf
                        if (r <= c || firstDesc <= maxFirstDescendant[r])
                        {
                            continue;
                        }

                        // first descendent exceeds all seen so far - update
                        maxFirstDescendant[r] = firstDesc;

                        // Record the previous leaf, and overwrite with this 
                        // node for the next iteration
                        int prev_r = previousLeaf[r];
                        previousLeaf[r] = c;

                        // c is the first leaf
                        if (prev_r < 0)
                        {
                            // Increment the delta of node c, since A[r, c] is in the 
                            // skeleton matrix of A
                            counts[c]++;
                            continue;
                        }

                        // Iterate until q is equal to its own ancestor, i.e. 
                        // it is a root (that we're aware of so far). This will 
                        // be the 'target' to which each step's ancestor will be 
                        // set to in our ancestral path traversal.
                        int root = prev_r;
                        while (root != ancestor[root])
                        {
                            root = ancestor[root];
                        }

                        // Update all ancestors - this is purely for benefit of 
                        // the next loops
                        int pnext;
                        for (int p = prev_r; p != root; p = pnext)
                        {
                            // Skip the parents, go directly to ancestor of p
                            pnext = ancestor[p];

                            // Update the ancestor of this step to the target, root
                            ancestor[p] = root;
                        }

                        // Subtract away the overlap for the root, since it is 
                        // a node with > 1 children and hence double counted
                        counts[root]--;
                        counts[c]++;
                    }
                }

                if (!etree.IsRoot(j))
                {
                    ancestor[j] = etree.Parents[j];
                }
            }

            // Update the column counts with the column counts of all children
            // of each node
            for (int i = 0; i < columns; ++i)
            {
                if (!etree.IsRoot(i))
                {
                    counts[etree.Parents[i]] += counts[i];
                }
            }

            if (accumulate)
            {
                // Shift the column counts array right by 1 
                for (int i = columns; i > 0; --i)
                {
                    counts[i] = counts[i - 1];
                }
                counts[0] = 0;

                // Accumulate the column counts array to form the column indices
                for (int i = 0; i < columns; ++i)
                {
                    counts[i + 1] += counts[i];
                }
            }

            return counts;
        }
    }
}
