﻿using LinearNet.Graphs;
using System;
using System.Collections.Generic;
using System.Text;

namespace LinearNet.Matrices.Sparse
{
    public sealed class SymbolicCholesky
    {
        private readonly int[] _columnIndices;
        private readonly int[] _rowIndices;
        private readonly EliminationTree _etree;
        private readonly int _dimension;

        /// <summary>
        /// The number of rows and columns of the Cholesky factor
        /// of a matrix.
        /// </summary>
        public int Dimension { get { return _dimension; } }

        /// <summary>
        /// An array of size (columns + 1) which contain the start and 
        /// end indices of the non-zero elements for each column of 
        /// the lower-triangular Cholesky factor of a matrix.
        /// </summary>
        public int[] ColumnIndices { get { return _columnIndices; } }

        /// <summary>
        /// An array of size (rows + 1) containing the starting and ending
        /// indices of the non-zero entries in each row of the lower-triangular
        /// Cholesky factor of a matrix.
        /// </summary>
        public int[] RowIndices { get { return _rowIndices; } }

        /// <summary>
        /// The elimination tree for the non-zero pattern of the Cholesky 
        /// factor of a matrix.
        /// </summary>
        public EliminationTree ElimTree { get { return _etree; } }

        /// <summary>
        /// The number of non-zero symbolic elements in the Cholesky factor
        /// of a matrix.
        /// </summary>
        public int NumNonZeroElements 
        { 
            get
            {
                return _columnIndices[_dimension];
            } 
        }

        /// <summary>
        /// Returns the largest number of non-zeroes in any column of the Cholesky factor. 
        /// Useful for initializing workspaces.
        /// </summary>
        public int MaxColumnCount
        {
            get
            {
                if (_columnIndices is null)
                {
                    return 0;
                }

                int max = 0;
                for (int c = 0; c < _dimension; ++c)
                {
                    int count = _columnIndices[c + 1] - _columnIndices[c];
                    if (count > max)
                    {
                        max = count;
                    }
                }
                return max;
            }
        }

        internal SymbolicCholesky(EliminationTree etree, int[] indices, bool isColumn = true)
        {
            _etree = etree;
            _dimension = etree.NodeCount;

            if (isColumn)
            {
                _columnIndices = indices;
                _rowIndices = null;
            }
            else
            {
                _columnIndices = null;
                _rowIndices = indices;
            }
        }
    }
}
