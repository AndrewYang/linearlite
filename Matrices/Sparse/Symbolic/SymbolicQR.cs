﻿using LinearNet.Graphs;
using System;
using System.Collections.Generic;
using System.Text;

namespace LinearNet.Matrices.Sparse
{
    public sealed class SymbolicQR
    {
        public EliminationTree ElimTree { get; private set; }
        public int[] RowCounts { get; private set; }

        internal SymbolicQR(EliminationTree etree, int[] rowCounts)
        {
            ElimTree = etree;
            RowCounts = rowCounts;
        }
    }
}
