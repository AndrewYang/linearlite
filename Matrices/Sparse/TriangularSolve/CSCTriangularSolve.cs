﻿using LinearNet.Providers;
using LinearNet.Structs;
using System;
using System.Collections.Generic;

namespace LinearNet.Matrices.Sparse
{
    internal class CSCTriangularSolve<T> where T : new()
    {
        private readonly IProvider<T> _provider;

        private CSCSparseMatrix<T> _matrix;
        private SparseVector<T> _vector;
        private int _dim;

        internal CSCTriangularSolve(IProvider<T> provider)
        {
            _provider = provider;
        }

        /// <summary>
        /// Solve the system $Lx = b$ for lower triangular $L$ and sparse $b$. The solution $x$ is stored 
        /// implicitly in the workspace <txt>x</txt>, at the indices specified by the list <txt>xi</txt>.
        /// Note that <txt>xi</txt> is not necessarily sorted.
        /// 
        /// Only the top-left <txt>dim</txt> by <txt>dim</txt> submatrix of <txt>L</txt> is used, and only
        /// the first <txt>dim</txt> dimensions of the right-hand-size vector <txt>b</txt> is used.
        /// 
        /// This method is O(flops) and is based on the work of Davis, T. (2006).
        /// </summary>
        /// <param name="L"></param>
        /// <param name="x"></param>
        /// <param name="b"></param>
        /// <param name="dim"></param>
        internal void SolveLower(CSCSparseMatrix<T> L, T[] x, List<int> xi, SparseVector<T> b, int dim)
        {
            _matrix = L;
            _vector = b;
            _dim = dim;

            // Find all non-zero indices
            xi.Clear();
            Reach(xi);

            // Clear the values of x to be overwritten
            for (int i = xi.Count - 1; i >= 0; --i)
            {
                x[xi[i]] = _provider.Zero;
            }

            // Initialize x with b
            for (int i = 0; i < _vector.Values.Length; ++i)
            {
                int ind = _vector.Indices[i];
                if (ind < _dim)
                {
                    x[ind] = _vector.Values[i];
                }
            }

            // required to iterate backwards 
            for (int i = xi.Count - 1; i >= 0; --i)
            {
                // x[j] is non-zero
                int j = xi[i];

                // We assume that the matrix is well-formed, in particular that the 
                // first term of each column is a term along the leading diagonal
                T Ljj = _matrix.Values[_matrix.ColumnIndices[j]];

                // x[j] /= L[j, j]
                x[j] = _provider.Divide(x[j], Ljj);

                // Iterate through all non-zero elements of column j
                // Do not include the diagonal term
                int end = _matrix.ColumnIndices[j + 1];
                for (int k = _matrix.ColumnIndices[j] + 1; k < end; ++k)
                {
                    int r = _matrix.RowIndices[k];
                    if (r < _dim)
                    {
                        x[r] = _provider.Subtract(x[r], _provider.Multiply(_matrix.Values[k], x[j]));
                    }
                }
            }
        }

        /// <summary>
        /// Solves the system $Lx = b$ for lower triangular $L$ and sparse $b$ using the Reach(b) algorithm that 
        /// runs in $O(flops)$ time, as per Davis, T. (2006). 
        /// 
        /// If a non lower-triangular matrix is passed in, only the lower triangular portion of the matrix is used 
        /// (everything else is ignored). Note that the complexity upper bound is only guaranteed if the matrix is 
        /// lower triangular. 
        /// 
        /// An exception is thrown if the matrix contains a zero entry along its leading diagonal.
        /// 
        /// This method requires 2 workspaces 
        /// - xi, a vector of length n, which will hold indexes of the non-zero elements of the solution vector x, and 
        /// - pstack, a vector also of length n which will be used temporarily to store the stack in place of the 
        /// recursion loop
        /// </summary>
        /// <param name="L"></param>
        /// <param name="x"></param>
        /// <param name="xi"></param>
        /// <param name="pstack"></param>
        /// <param name="b"></param>
        /// <param name="dim"></param>
        /// <returns>The top of the stack of non-zero indices of x. i.e. the non-zero elements of x is in xi[0, top).</returns>
        internal int SolveLower(CSCSparseMatrix<T> L, T[] x, int[] xi, int[] pstack, SparseVector<T> b, int dim)
        {
            _matrix = L;
            _vector = b;
            _dim = dim;

            // Find all non-zero indices
            int top = Reach2(xi, pstack);

            // Clear the values of x to be overwritten
            for (int i = top; i < _dim; ++i)
            {
                x[xi[i]] = _provider.Zero;
            }

            // Initialize x with b
            for (int i = 0; i < _vector.Values.Length; ++i)
            {
                int ind = _vector.Indices[i];
                if (ind < _dim)
                {
                    x[ind] = _vector.Values[i];
                }
            }

            // required to iterate backwards 
            for (int i = top; i < _dim; ++i)
            {
                // x[j] is non-zero... we hereby deal with column j of the matrix
                int j = xi[i];

                int col_start = _matrix.ColumnIndices[j],
                    col_end = _matrix.ColumnIndices[j + 1];

                // Ensure that col_start is pointing to the element at L(j, j)
                if (_matrix.RowIndices[col_start] != j)
                {
                    while (col_start < col_end && _matrix.RowIndices[col_start] < j)
                    {
                        ++col_start;
                    }

                    if (col_start >= col_end || _matrix.RowIndices[col_start] != j)
                    {
                        throw new ArgumentOutOfRangeException($"Coefficient matrix '{nameof(L)}' does not contain the required non-zero leading diagonal entry at ({j}, {j}).");
                    }
                }

                T Ljj = _matrix.Values[col_start];

                // Ensure that L(j, j) is non-zero
                if (Ljj.Equals(_provider.Zero))
                {
                    throw new ArgumentOutOfRangeException($"Coefficient matrix '{nameof(L)}' contains a zero along its leading diagonal at ({j}, {j}).");
                }

                // x[j] /= L[j, j]
                T xj = _provider.Divide(x[j], Ljj);
                x[j] = xj;

                // Iterate through all non-zero elements of column j
                // Do not include the diagonal term
                for (int k = col_start + 1; k < col_end; ++k)
                {
                    int r = _matrix.RowIndices[k];
                    if (r < _dim)
                    {
                        x[r] = _provider.Subtract(x[r], _provider.Multiply(_matrix.Values[k], xj));
                    }
                }
            }

            return top;
        }

        /// <summary>
        /// Calculate the reach of the set of non-zero indices in b, storing them in xi.
        /// </summary>
        private void Reach(List<int> xi)
        {
            // For each non-zero index in b...
            foreach (int pi in _vector.Indices)
            {
                // Marked nodes have flipped signs
                if (pi < _dim && !Marked(pi))
                {
                    // Start a dfs at pi
                    Dfs(pi, xi);
                }
            }

            // Finish by undo-ing all markings
            int n = xi.Count, i;
            for (i = 0; i < n; ++i)
            {
                Unmark(xi[i]);
            }
        }
        private void Dfs(int j, List<int> xi)
        {
            // Find all non-zero L[j, i] in descending order.

            int row_end = GetUnmarkedValue(_matrix.ColumnIndices[j + 1]),
                row_start = GetUnmarkedValue(_matrix.ColumnIndices[j]);

            // Mark node j
            Mark(j);

            for (int k = row_end - 1; k >= row_start; --k)
            {
                // For all unmarked children - recursively mark using depth-first search
                int r = _matrix.RowIndices[k];
                if (r < _dim && !Marked(r))
                {
                    Dfs(r, xi);
                }
            }

            // Push node j into stack
            xi.Add(j);
        }

        /// <summary>
        /// Non-recursive implementation based on Davis, T. (2006)
        /// </summary>
        /// <param name="xi">An array of length n which holds the non-zero pattern of x in topological order.</param>
        /// <param name="pstack">A length-n workspace.</param>
        private int Reach2(int[] xi, int[] pstack)
        {
            // xi should be of length n
            int top = _dim;
            foreach (int i in _vector.Indices)
            {
                if (i < _dim && !Marked(i))
                {
                    top = Dfs2(i, top, xi, pstack);
                }
            }

            // Unmark nodes when finished
            for (int k = 0; k < top; ++k)
            {
                Unmark(xi[k]);
            }

            // Return the top of the marked nodes stack in array xi.
            return top;
        }
        private int Dfs2(int j, int top, int[] xi, int[] pstack)
        {
            // As in the implementation of Davis, T. (2006), xi serves as the workspace for 
            // 2 distinct stacks that never hold more than n objects in total. 
            // The first stack begins from the bottom (end) of xi, index n - 1, and holds the marked nodes 
            // in reverse order. 
            // The second stack begins from the top (start) of xi, index 0, and holds the nodes which we 
            // are current recursing through. This stack is always empty by the end of this 
            // method, since we are only visiting one node at a time every time Dfs2 is called.

            // Add current node j to the top (since stack2 is empty)
            int head = 0;
            xi[head] = j;

            while (head >= 0)
            {
                j = xi[head];

                // Mark node j as visited - before iterating over its neighbours
                if (!Marked(j))
                {
                    pstack[head] = _matrix.ColumnIndices[j];
                    Mark(j);
                }

                // Iterate through all neighbours of j:
                int p, p_end = GetUnmarkedValue(_matrix.ColumnIndices[j + 1]);
                for (p = pstack[head]; p < p_end; ++p)
                {
                    int r = _matrix.RowIndices[p];

                    // Skip if the node r is visited
                    if (Marked(r))
                    {
                        continue;
                    }

                    // Node r is unvisited - pause DFS of j, start DFS of r by placing it to the 
                    // end of stack2. 
                    pstack[head] = p;
                    ++head;
                    xi[head] = r;
                    break;
                }

                // Check if all neighbours are visited
                if (p == p_end)
                {
                    // Remove node j from the recursion stack (stack2) - note j is not actually removed, 
                    // the bottom of the stack is simply moved up
                    --head;

                    // Add j to the output stack (stack1), since it is complete
                    xi[--top] = j;
                }
            }
            return top;
        }

        /// <summary>
        /// Check if node i is marked in the array. Since indices is guaranteed to be non-negative, 
        /// any negative element is a marked node.
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        private bool Marked(int i) => _matrix.ColumnIndices[i] < 0;

        private void Mark(int i)
        {
            if (_matrix.ColumnIndices[i] >= 0)
            {
                _matrix.ColumnIndices[i] = -1 - _matrix.ColumnIndices[i];
            }
        }
        private void Unmark(int i)
        {
            if (_matrix.ColumnIndices[i] < 0)
            {
                _matrix.ColumnIndices[i] = -1 - _matrix.ColumnIndices[i];
            }
        }
        private int GetUnmarkedValue(int value)
        {
            if (value < 0)
            {
                return -1 - value;
            }
            return value;
        }
    }
}
