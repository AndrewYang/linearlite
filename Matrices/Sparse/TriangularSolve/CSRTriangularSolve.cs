﻿using LinearNet.Providers;
using LinearNet.Structs;
using System.Collections.Generic;

namespace LinearNet.Matrices.Sparse
{
    public class CSRTriangularSolve
    {
        /// <summary>
        /// Solve a lower triangular system L^T * x = b where L is square and lower triangular, x and b are sparse.
        /// Instead of the whole matrix L, we only take the upper-left hand submatrix of L, [0:dim) x [0:dim)
        /// Similarly we only take the [0:dim) coordinates of both x and b vectors.
        /// 
        /// The coefficient matrix is taken as L^T since CSR format is fast for triangular transpose solving, and CSC is 
        /// fast for triangular solving.
        /// 
        /// All credit goes to fantastic book 'Direct methods for sparse linear systems' by Davis, T. (2006).
        /// </summary>
        internal void SolveLower<T>(CSRSparseMatrix<T> L, SparseVector<T> b, T[] x, int dim, IProvider<T> provider) where T : new()
        {
            // Find all non-zero indices
            List<int> xi = new List<int>();
            Reach(L, b, xi);

            // Clear the values of x to be overwritten
            for (int i = xi.Count - 1; i >= 0; --i)
            {
                x[xi[i]] = provider.Zero;
            }

            // Initialize x with b
            for (int i = 0; i < b.Values.Length; ++i)
            {
                x[b.Indices[i]] = b.Values[i];
            }

            // required to iterate backwards 
            for (int i = xi.Count - 1; i >= 0; --i)
            {
                // x[j] is non-zero
                int j = xi[i]; 

                // We assume that the matrix is well-formed, and that the non-zero 
                // diagonal term L[j, j] is in fact the last term of row j.
                T Ljj = L.Values[L.RowIndices[j + 1] - 1];

                // x[j] /= L[j, j]
                x[j] = provider.Divide(x[j], Ljj);

                // Iterate through all non-zero elements of row j (i.e. column j of L^T) 
                // Do not include the diagonal term
                int end = L.RowIndices[j + 1] - 1;
                for (int k = L.RowIndices[j]; k < end; ++k)
                {
                    int c = L.ColumnIndices[k];
                    x[c] = provider.Subtract(x[c], provider.Multiply(L.Values[k], x[j]));
                }
            }
        }

        /// <summary>
        /// Calculate the reach of the set of non-zero indices in b, storing them in xi.
        /// </summary>
        private void Reach<T>(CSRSparseMatrix<T> L, SparseVector<T> b, List<int> xi) where T : new()
        {
            // For each non-zero index in b...
            foreach (int pi in b.Indices)
            {
                // Marked nodes have flipped signs
                if (!Marked(L.RowIndices, pi))
                {
                    // Start a dfs at pi
                    Dfs(pi, L, xi);
                }
            }

            // Finish by undo-ing all markings
            int n = xi.Count, i;
            for (i = 0; i < n; ++i)
            {
                ToggleMarkNode(L.RowIndices, xi[i]);
            }
        }
        private void Dfs<T>(int j, CSRSparseMatrix<T> L, List<int> xi) where T : new()
        {
            // Find all non-zero L[j, i] in descending order.

            // rowIndices[j + 1] is guaranteed to be unmarked otherwise dfs wont be called...
            int row_end = L.RowIndices[j + 1], row_start = Unmark(L.RowIndices[j]);

            // Mark node j
            ToggleMarkNode(L.RowIndices, j);

            for (int k = row_end; k >= row_start; --k)
            {
                // For all unmarked children - recursively mark using depth-first search
                if (!Marked(L.RowIndices, k))
                {
                    Dfs(k, L, xi);
                }
            }

            // Push node j into stack
            xi.Add(j);
        }

        /// <summary>
        /// Check if node i is marked in the array. Since indices is guaranteed to be non-negative, 
        /// any negative element is a marked node.
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        private bool Marked(int[] indices, int i) => indices[i] < 0;

        /// <summary>
        /// Mark or unmark node i in the indices array
        /// </summary>
        /// <param name="indices"></param>
        /// <param name="i"></param>
        private void ToggleMarkNode(int[] indices, int i) => indices[i] = 1 - indices[i];

        private int Unmark(int index) => index < 0 ? 1 - index : index;
    }
}
