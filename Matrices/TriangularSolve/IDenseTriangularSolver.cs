﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LinearNet.Matrices.TriangularSolve
{
    internal interface IDenseTriangularSolver<T>
    {
        /// <summary>
        /// Solve LX = B, given dense matrices L and B, overwriting B with the answer X.
        /// L and B are defined implicitly as submatrices of the matrix 'A' and 'B' respectively.
        /// L is taken to be the submatrix at [A_row_offset: A_row_offset + rows, A_col_offset: A_col_offset + rows]
        /// B is taken to be the submatrix at [B_row_offset: B_row_offset + rows, B_col_offset: B_col_offset + columns]
        /// </summary>
        /// <param name="A"></param>
        /// <param name="B"></param>
        /// <param name="A_row_offset"></param>
        /// <param name="A_col_offset"></param>
        /// <param name="B_row_offset"></param>
        /// <param name="B_col_offset"></param>
        /// <param name="rows"></param>
        /// <param name="columns"></param>
        /// <param name="lower">true if A is lower triangular</param>
        /// <param name="A_transposed">true if we should solve A'X = B instead of AX = B</param>
        /// <param name="B_column_major">true if B is arranged by column, i.e. we should be solving AX' = B'</param>
        /// <returns>Whether the solve was successful</returns>
        bool Solve(T[][] A, T[][] B, int A_row_offset, int A_col_offset, int B_row_offset, int B_col_offset, int rows, int columns, 
            bool lower, bool A_transposed, bool B_column_major);
    }
}
