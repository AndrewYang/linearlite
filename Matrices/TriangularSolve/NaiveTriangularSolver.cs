﻿using LinearNet.Providers;
using System;
using System.Collections.Generic;
using System.Text;

namespace LinearNet.Matrices.TriangularSolve
{
    internal class NaiveTriangularSolver<T> : IDenseTriangularSolver<T>
    {
        private readonly IDenseBLAS1<T> _blas;

        internal NaiveTriangularSolver(IDenseBLAS1<T> blas)
        {
            _blas = blas;
        }

        bool IDenseTriangularSolver<T>.Solve(T[][] L, T[][] B, 
            int A_row_offset, int A_col_offset, int B_row_offset, int B_col_offset, int rows, int columns, 
            bool lower, bool A_transposed, bool B_column_major)
        {
            IProvider<T> provider = _blas.Provider;
            T zero = provider.Zero;


            if (lower)
            {
                if (A_transposed)
                {
                    
                }
                else
                {
                    if (B_column_major)
                    {
                        int BA_offset = B_row_offset - A_col_offset;
                        for (int c = 0; c < columns; ++c)
                        {
                            T[] bCol = B[B_col_offset + c];

                            for (int r = 0, ai = A_row_offset, bix = B_row_offset; r < rows; ++r, ++ai, ++bix)
                            {
                                T dot = _blas.DOT(L[ai], bCol, A_col_offset, A_col_offset + r, BA_offset);

                                T L_cc = L[ai][ai];
                                if (provider.Equals(zero, L_cc))
                                {
                                    return false;
                                }
                                bCol[bix] = provider.Divide(provider.Subtract(bCol[bix], dot), L_cc);
                            }
                        }
                        return true;
                    }
                    else
                    {
                        int BA_offset = B_col_offset - A_col_offset;
                        for (int k = 0; k < rows; ++k)
                        {
                            T[] b = B[B_row_offset + k];

                            for (int c = 0, ai = A_row_offset, bix = B_col_offset; c < columns; ++c, ++ai, ++bix)
                            {
                                T dot = _blas.DOT(L[ai], b, A_col_offset, A_col_offset + c, BA_offset);

                                T L_cc = L[ai][ai];
                                if (provider.Equals(zero, L_cc))
                                {
                                    return false;
                                }
                                b[bix] = provider.Divide(provider.Subtract(b[bix], dot), L_cc);
                            }
                        }
                        return true;
                    }
                }
            }
            else
            {
                if (A_transposed)
                {

                }
            }

            throw new NotImplementedException();
        }
    }
}
