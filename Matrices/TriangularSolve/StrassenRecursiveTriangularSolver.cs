﻿using LinearNet.Global;
using LinearNet.Providers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace LinearNet.Matrices.TriangularSolve
{
    public class StrassenRecursiveTriangularSolver<T> : IDenseTriangularSolver<T> where T : new()
    {
        private readonly IDenseTriangularSolver<T> _baseAlgorithm;
        private readonly VariableSizeStrassenMultiplication<T> _strassen;
        private readonly IDenseBLAS1<T> _blas1;
        private readonly IDenseBLAS2<T> _blas2;
        private readonly int _thresholdSize;
        private int _workspaceSize;
        private T[][] _workspace, _workspace2;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="baseAlgorithm"></param>
        /// <param name="strassen"></param>
        /// <param name="blas1"></param>
        /// <param name="blas2"></param>
        /// <param name="thresholdSize">
        /// Below this size, a submatrix will default to the base algorithm triangular solve
        /// </param>
        /// <param name="blockSize">
        /// The size of the workspace
        /// </param>
        internal StrassenRecursiveTriangularSolver(
            IDenseTriangularSolver<T> baseAlgorithm,
            VariableSizeStrassenMultiplication<T> strassen,
            IDenseBLAS1<T> blas1,
            IDenseBLAS2<T> blas2,
            int thresholdSize,
            int blockSize)
        {
            _baseAlgorithm = baseAlgorithm;
            _blas1 = blas1;
            _blas2 = blas2;
            _strassen = strassen;
            _thresholdSize = thresholdSize;
            _workspaceSize = blockSize;
            _workspace = MatrixInternalExtensions.JMatrix<T>(blockSize, blockSize);
            _workspace2 = MatrixInternalExtensions.JMatrix<T>(blockSize, blockSize);
        }

        public StrassenRecursiveTriangularSolver(VariableSizeStrassenMultiplication<T> strassen, int thresholdSize, int blockHeight, int blockWidth)
        {
            if (strassen is null)
            {
                throw new ArgumentNullException(nameof(strassen));
            }
            if (thresholdSize < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(thresholdSize), ExceptionMessages.NonPositiveNotAllowed);
            }
            if (blockHeight < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(blockHeight), ExceptionMessages.NonPositiveNotAllowed);
            }
            if (blockWidth < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(blockWidth), ExceptionMessages.NonPositiveNotAllowed);
            }

            if (!ProviderFactory.TryGetDefaultBLAS1Provider(out IDenseBLAS1<T> b1) ||
                !ProviderFactory.TryGetDefaultBLAS2Provider(out IDenseBLAS2<T> b2))
            {
                throw new NotSupportedException(ExceptionMessages.TypeNotSupported);
            }

            _baseAlgorithm = new NaiveTriangularSolver<T>(b1);
            _blas1 = b1;
            _blas2 = b2;
            _strassen = strassen;
            _thresholdSize = thresholdSize;
            _workspaceSize = blockHeight;
            _workspace = MatrixInternalExtensions.JMatrix<T>(blockHeight, blockWidth);
            _workspace2 = MatrixInternalExtensions.JMatrix<T>(blockHeight, blockWidth);
        }

        public bool Solve(T[][] A, T[][] B, int A_row_offset, int A_col_offset, int B_row_offset, int B_col_offset, int rows, int columns, bool lower, bool A_transposed, bool B_column_major)
        {
            if (rows % 2 != 0)
            {
                throw new NotImplementedException(); // for now
            }

            IProvider<T> provider = _blas1.Provider;
            int half_row = rows / 2;

            // L_11 * X1 = B1
            if (half_row < _thresholdSize)
            {
                if (!_baseAlgorithm.Solve(A, B, 
                    A_row_offset, A_col_offset, 
                    B_row_offset, B_col_offset, 
                    half_row, columns, 
                    lower, A_transposed, B_column_major))
                {
                    return false;
                }
            }
            else
            {
                if (!Solve(A, B, 
                    A_row_offset, A_col_offset, 
                    B_row_offset, B_col_offset, 
                    half_row, columns, 
                    lower, A_transposed, B_column_major))
                {
                    return false;
                }
            }
            
            // With X1 computed, L_21 * X1 + L_22 * X2 = B2
            // Compute L_21 * X1 and store in workspace2
            if (lower && !A_transposed && B_column_major)
            {
                // Copy X1' into workspace
                for (int c = 0; c < columns; ++c)
                {
                    T[] column = B[c + B_col_offset];
                    for (int r = 0; r < half_row; ++r)
                    {
                        _workspace[r][c] = column[r + B_row_offset];
                    }
                }

                // workspace2 <- L_21 * X1
                _strassen.Multiply(A, _workspace, _workspace2,
                    A_row_offset + half_row, A_col_offset,
                    0, 0,
                    0, 0,
                    new Size3(half_row, half_row, columns), 
                    false);

                // Subtract workspace2 into B2, B2 <- B2 - L_21 * X1
                int B2_row_offset = B_row_offset + half_row;
                for (int c = 0; c < columns; ++c)
                {
                    T[] column = B[c + B_col_offset];
                    for (int r = 0; r < half_row; ++r)
                    {
                        int r1 = B2_row_offset + r;
                        column[r1] = provider.Subtract(column[r1], _workspace2[r][c]);
                    }
                }
            }
            else
            {
                throw new NotImplementedException();
            }

            // Recursively solve L_22 * X2 = B2 - L_21 * X1
            if (half_row < _thresholdSize)
            {
                if (!_baseAlgorithm.Solve(A, B, 
                    A_row_offset + half_row, A_col_offset + half_row, 
                    B_row_offset + half_row, B_col_offset, 
                    half_row, columns, 
                    lower, A_transposed, B_column_major))
                {
                    return false;
                }
            }
            else
            {
                if (!Solve(A, B,
                    A_row_offset + half_row, A_col_offset + half_row,
                    B_row_offset + half_row, B_col_offset,
                    half_row, columns,
                    lower, A_transposed, B_column_major))
                {
                    return false;
                }
            }

            return true;
        }
    }
}
