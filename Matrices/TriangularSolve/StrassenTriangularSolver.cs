﻿using LinearNet.Providers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.ExceptionServices;
using System.Runtime.InteropServices;
using System.Text;

namespace LinearNet.Matrices.TriangularSolve
{
    /// <summary>
    /// Algorithmic accelerators for dense (matrix) triangular solve using Strassen's multiplication
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class StrassenTriangularSolver<T> : IDenseTriangularSolver<T> where T : new()
    {
        private readonly IDenseTriangularSolver<T> _baseAlgorithm;
        private readonly VariableSizeStrassenMultiplication<T> _strassen;
        private readonly IDenseBLAS1<T> _blas1;
        private readonly IDenseBLAS2<T> _blas2;
        private readonly int _blockSize;
        private int _width;
        private T[][] _workspace, _workspace2;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="baseAlgorithm"></param>
        /// <param name="strassen"></param>
        /// <param name="blas1"></param>
        /// <param name="blas2"></param>
        /// <param name="blockSize"></param>
        /// <param name="initialWidth">
        /// The width of B and X matrices - does not have to be initialized correctly, 
        /// however the method will be more performant if this is set to the correct width, since it needs to 
        /// generate workspaces to store the transpose. 
        /// </param>
        internal StrassenTriangularSolver(
            IDenseTriangularSolver<T> baseAlgorithm, 
            VariableSizeStrassenMultiplication<T> strassen, 
            IDenseBLAS1<T> blas1, 
            IDenseBLAS2<T> blas2,
            int blockSize,
            int initialWidth)
        {
            _baseAlgorithm = baseAlgorithm;
            _blas1 = blas1;
            _blas2 = blas2;
            _strassen = strassen;
            _blockSize = blockSize;
            _width = initialWidth;
            _workspace = MatrixInternalExtensions.JMatrix<T>(_blockSize, _width);
            _workspace2 = MatrixInternalExtensions.JMatrix<T>(_blockSize, _width);
        }

        public bool Solve(T[][] A, T[][] B, 
            int A_row_offset, int A_col_offset, int B_row_offset, int B_col_offset, int rows, int columns, 
            bool lower, bool A_transposed, bool B_column_major)
        {

            IProvider<T> provider = _blas1.Provider;

            if (lower)
            {
                if (A_transposed)
                {

                }
                else
                {
                    if (B_column_major)
                    {
                        if (rows % _blockSize != 0)
                        {
                            throw new NotImplementedException(); // for now
                        }

                        // Check size of workspace, if not sufficient create a bigger one
                        if (columns > _width)
                        {
                            _width = columns;
                            _workspace = MatrixInternalExtensions.JMatrix<T>(_blockSize, _width);
                            _workspace2 = MatrixInternalExtensions.JMatrix<T>(_blockSize, _width);
                        }

                        int nBlocks = rows / _blockSize;
                        for (int i = 0; i < nBlocks; ++i)
                        {
                            // B_block(i, *) -= L_block(i, j) * X_block(i, *)
                            // to do this we need to copy the negation of the 
                            // transpose of X_block(i, *) into the workspace
                            int top = i * _blockSize,
                                X_row_start = top + B_row_offset,
                                X_row_end = X_row_start + _blockSize,
                                X_col_end = B_col_offset + columns;

                            if (i > 0)
                            {
                                for (int j = 0; j < i; ++j)
                                {
                                    int row_start = B_row_offset + j * _blockSize,
                                        row_end = row_start + _blockSize;
                                    for (int c = B_col_offset; c < X_col_end; ++c)
                                    {
                                        T[] col = B[c];
                                        int c1 = c - B_col_offset;
                                        for (int r = row_start; r < row_end; ++r)
                                        {
                                            _workspace[r - row_start][c1] = col[r];
                                        }
                                    }

                                    // workspace = L_block(i, j) * X_block(i, *), first block requires a clear
                                    _strassen.Multiply(A, _workspace, _workspace2, top, j * _blockSize, 0, 0, 0, 0, new Size3(_blockSize, _blockSize, columns), (j > 0));
                                }

                                // Subtract transpose of workspace from B_block(i, *)
                                for (int c = B_col_offset; c < X_col_end; ++c)
                                {
                                    T[] col = B[c];
                                    int c1 = c - B_col_offset;
                                    for (int r = X_row_start; r < X_row_end; ++r)
                                    {
                                        col[r] = provider.Subtract(col[r], _workspace2[r - X_row_start][c1]);
                                    }
                                }
                            }
                            
                            // Lower triangular solve of L_block(i, i) * X_block(i, *) = workspace2
                            if (!_baseAlgorithm.Solve(A, B, 
                                A_row_offset + top, A_col_offset + top, 
                                X_row_start, B_col_offset, 
                                _blockSize, columns, 
                                lower, A_transposed, B_column_major))
                            {
                                return false;
                            }
                        }
                        return true;
                    }
                    else
                    {

                    }
                }
            }
            else
            {

            }

            throw new NotImplementedException();
        }
    }
}
