﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LinearNet.Meta.Compilers.Vectors
{
    internal class DenseVectorMetaInfo : VectorMetaInfo
    {
        internal DenseVectorMetaInfo(string name) : base("DenseVector", name, true, true)
        {

        }

        internal override VectorIterationHeader GetIterationHeader(HashSet<string> existingVariables, int indent)
        {
            StringBuilder sb = new StringBuilder();
            string _indent_ = new string('\t', indent);
            string _len_ = GetVariableName("len", existingVariables);
            string _i_ = GetVariableName("i", existingVariables);

            sb.AppendLine(_indent_ + $"int {_len_} = {VariableName}.Dimension;");
            sb.AppendLine(_indent_ + $"for (int {_i_} = 0; {_i_} < {_len_}; ++{_i_})");
            sb.AppendLine(_indent_ + "{");

            return new VectorIterationHeader()
            {
                SourceCode = sb.ToString(),
                IndexVariableName = _i_
            };
        }
        internal override string GetValueFromIndex(string indexName)
        {
            if (!SupportsLocate)
            {
                throw new NotImplementedException();
            }
            return $"{VariableName}[{indexName}]";
        }

        internal override string GetIterationFooter(int indent)
        {
            return new string('\t', indent) + "}";
        }
    }
}
