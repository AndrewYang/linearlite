﻿using LinearNet.Meta.Compilers.Vectors;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace LinearNet.Meta
{
    /// <summary>
    /// Internal class used to produce and compile vector addition methods that support 
    /// addition between any combination of implementations of the Vector<T> abstract class.
    /// 
    /// To use this class, compile it, execute the main method to generate code (which will
    /// reside in this project), then re-compile with the generated code. 
    /// </summary>
    internal class VectorAdditionCompiler
    {
        internal static void Compile(string type = "double")
        {
            // Test on just two metas 
            VectorMetaInfo 
                v1 = new DenseVectorMetaInfo("v1"),
                v2 = new DenseVectorMetaInfo("v2"),
                vSum = new DenseVectorMetaInfo("sum");

            // Set the vectors variable names
            HashSet<string> names = new HashSet<string>() { "v1", "v2", "sum" };

            // Keep track of the indentation
            int indent = 1;

            StringBuilder sb = new StringBuilder(GetMethodSignature(v1, v2, vSum, type));

            // E.g. for (int i = 0; i < len; ++i) {
            VectorIterationHeader header = v1.GetIterationHeader(names, indent);
            sb.AppendLine(header.SourceCode);

            ++indent;

            // E.g. c[i] = a[i] + b[i]
            string access_v1 = v1.GetValueFromIndex(header.IndexVariableName);
            string access_v2 = v2.GetValueFromIndex(header.IndexVariableName);
            string access_sum = vSum.GetValueFromIndex(header.IndexVariableName);
            sb.AppendLine($"{new string('\t', indent)}{access_sum} = {access_v1} + {access_v2}");

            --indent;

            // E.g. }
            sb.AppendLine(v1.GetIterationFooter(indent));

            Debug.WriteLine(sb.ToString());
        }

        private static string GetMethodSignature(VectorMetaInfo v1, VectorMetaInfo v2, VectorMetaInfo sum, string _type_)
        {
            return 
                $"public static {sum.TypeName}<{_type_}> Add(this {v1.TypeName}<{_type_}> {v1.VariableName}, {v2.TypeName}<{_type_}> {v2.VariableName})" + Environment.NewLine + 
                "{" + Environment.NewLine;
        }
    }
}
