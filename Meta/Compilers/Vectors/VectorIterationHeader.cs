﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LinearNet.Meta.Compilers.Vectors
{
    /// <summary>
    /// A code component
    /// </summary>
    internal class VectorIterationHeader
    {
        internal string SourceCode { get; set; }
        internal string IndexVariableName { get; set; }
    }
}
