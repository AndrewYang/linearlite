﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LinearNet.Meta.Compilers.Vectors
{
    /// <summary>
    /// Contains information regarding the structure for a particular type of vector
    /// Also contains some methods to generate code snippets
    /// 
    /// Based on the paper 'Formal abstraction for Sparse Tensor Algebra Compilers' by 
    /// Chou, Kjolstad and Amarasinghe (2018)
    /// </summary>
    internal abstract class VectorMetaInfo
    {
        internal string TypeName { get; set; }
        internal string VariableName { get; set; }
        internal bool SupportsLocate { get; }
        internal bool Ordered { get; }

        internal VectorMetaInfo(string type, string variableName, bool supportsLocate, bool ordered)
        {
            TypeName = type;
            VariableName = variableName;
            SupportsLocate = supportsLocate;
            Ordered = ordered;
        }

        /// <summary>
        /// Given a variable stem, e.g. 'x', returns the first available variable
        /// name that is not already taken, e.g. x2 if x and x1 are both taken.
        /// </summary>
        /// <param name="stem">The stem (first part) of the variable.</param>
        /// <param name="existingVariables">The set of existing variables. Will be updated 
        /// with the newly created variable.</param>
        /// <returns></returns>
        protected string GetVariableName(string stem, HashSet<string> existingVariables)
        {
            if (string.IsNullOrEmpty(stem))
            {
                throw new ArgumentNullException(nameof(stem));
            }
            if (existingVariables is null)
            {
                throw new ArgumentNullException(nameof(existingVariables));
            }

            if (!existingVariables.Contains(stem))
            {
                existingVariables.Add(stem);
                return stem;
            }

            int i = 1;
            while (true)
            {
                string candidate = stem + i;
                if (!existingVariables.Contains(candidate))
                {
                    existingVariables.Add(candidate);
                    return candidate;
                }
                ++i;
            }
        }

        /// <summary>
        /// Get the iteration header for this vector (only applies to variables that support iteration)
        /// </summary>
        /// <param name="vectorName"></param>
        /// <param name="existingVariables"></param>
        /// <returns></returns>
        internal abstract VectorIterationHeader GetIterationHeader(HashSet<string> existingVariables, int indent);

        internal abstract string GetValueFromIndex(string indexName);

        internal abstract string GetIterationFooter(int indent);
    }

    
}
