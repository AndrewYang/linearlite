﻿using LinearNet.Functions;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Meta
{
    /// <summary>
    /// This class optimizers the boundaries of a linear intrapolation approximator 
    /// for the logistic function (for fast approximate logistic function evaluation), 
    /// as applied to floating point arithmetic.
    /// 
    /// The results of this optimization routine is used to construct the method 
    /// MathFunctions.FastLogistic(double x)
    /// </summary>
    internal class FastLogisticLerpOptimizer
    {
        private double[] _testInputs;

        internal FastLogisticLerpOptimizer(double[] testInputs)
        {
            _testInputs = testInputs;
        }

        internal void OptimiseBounds(int segments)
        {
            double learningRate = 1;
            int iterations = 30000;

            // initialize segments at equally spaced intervals in (testInputs.Min(), testInputs.Max())
            double[] bounds = new double[segments - 1];
            double min = _testInputs.Min(), max = _testInputs.Max(), range = max - min;
            for (int b = 1; b < segments; ++b)
            {
                bounds[b - 1] = (double)b / segments * range + min;
            }

            double[] gradients = new double[segments - 1];
            for (int i = 0; i < iterations; ++i)
            {
                if (i % 100 == 0)
                {
                    Debug.WriteLine(i + "\t" + CalculateError(bounds, _testInputs));
                }
                CalculateErrorPseudoGradient(bounds, _testInputs, gradients);

                // Take a gradient step
                for (int d = 0; d < bounds.Length; ++d)
                {
                    bounds[d] -= learningRate * gradients[d];
                }
            }

            Debug.WriteLine("bounds");
            foreach (double b in bounds)
            {
                Debug.WriteLine(b.ToString("n16"));
            }

            double[] fx = bounds.Select(b => MathFunctions.Logistic(b)).ToArray();
            Debug.WriteLine("function values");
            fx.Print();
        }
        private double CalculateApproximateLogistic(double[] bounds, double x)
        {
            if (x < bounds[0])
            {
                return 0.0;
            }
            if (x >= bounds[bounds.Length - 1])
            {
                return 1.0;
            }

            for (int i = 1; i < bounds.Length; ++i)
            {
                double lower = bounds[i - 1], upper = bounds[i];
                if (x >= lower && x < upper)
                {
                    return MathFunctions.Logistic(lower) + (x - lower) / (upper - lower) * (MathFunctions.Logistic(upper) - MathFunctions.Logistic(lower));
                }
            }
            throw new ArgumentException();
        } 
        private double CalculateError(double[] bounds, double[] inputs)
        {
            double sum = 0.0;
            foreach (double x in inputs)
            {
                sum += Math.Pow(CalculateApproximateLogistic(bounds, x) - MathFunctions.Logistic(x), 2.0);
            }
            return sum / inputs.Length;
        }

        /// <summary>
        /// Calculate the derivative of the error with respect to each of the bounds values
        /// </summary>
        private void CalculateErrorPseudoGradient(double[] bounds, double[] inputs, double[] gradients)
        {
            double eps = 1e-4;
            for (int i = 0; i < bounds.Length; ++i)
            {
                bounds[i] += eps;
                double fplus = CalculateError(bounds, inputs);
                bounds[i] -= 2 * eps;
                double fminus = CalculateError(bounds, inputs);
                bounds[i] += eps;
                gradients[i] = (fplus - fminus) / (2 * eps);
            }
        }

        internal static void Run()
        {
            // equally spaced in the logit space
            int n = 1000;
            double[] testCases = new double[n];
            for (int i = 0; i < n; ++i)
            {
                testCases[i] = MathFunctions.Logit((double)(i + 1) / (n + 1));
                Debug.WriteLine(testCases[i]);
            }

            FastLogisticLerpOptimizer optimizer = new FastLogisticLerpOptimizer(testCases);
            optimizer.OptimiseBounds(8);
        }
    }
}
