﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Optimisation
{
    public abstract class DifferentiableFunction<TInput, TOutput> : Function<TInput, TOutput>
    {
        public int Dimension { get; }

        public DifferentiableFunction(int inputDimension)
        {
            Dimension = inputDimension;
        }

        public abstract void EvaluateDerivative(TInput x, ref TInput gradient);
    }
}
