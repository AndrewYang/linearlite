﻿using LinearNet.Structs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Optimisation
{
    public abstract class Function<TInput, TOutput>
    {
        public abstract TOutput Evaluate(TInput point);

        public static implicit operator Function<TInput, TOutput>(Func<TInput, TOutput> func)
        {
            return new DefaultFunction<TInput, TOutput>(func);
        }
        public static implicit operator Func<TInput, TOutput>(Function<TInput, TOutput> function)
        {
            // Don't create unnecessary nesting
            if (function is DefaultFunction<TInput, TOutput>)
            {
                return ((DefaultFunction<TInput, TOutput>)function)._function;
            }
            return x => function.Evaluate(x);
        }
    }

    internal class DefaultFunction<TInput, TOutput> : Function<TInput, TOutput>
    {
        internal readonly Func<TInput, TOutput> _function;

        internal DefaultFunction(Func<TInput, TOutput> function)
        {
            _function = function;
        }

        public override TOutput Evaluate(TInput point)
        {
            return _function(point);
        }
    }
}
