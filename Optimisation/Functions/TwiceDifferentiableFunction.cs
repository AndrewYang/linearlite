﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Optimisation
{
    /// <summary>
    /// A class implementing a function that is twice-differentiable everywhere in its domain, i.e.
    /// the second order derivative is defined everywhere in its domain.
    /// 
    /// This type of function is useful for optimization methods that exploit curvature (e.g. 
    /// Newton based methods).
    /// </summary>
    public abstract class TwiceDifferentiableFunction<TInput, TOutput> : DifferentiableFunction<TInput, TOutput>
    { 
        public TwiceDifferentiableFunction(int inputDimension) : base(inputDimension)
        {

        }


    }
}
