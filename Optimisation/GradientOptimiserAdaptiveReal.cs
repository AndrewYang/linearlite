﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Optimisation
{
    public class GradientOptimiserAdaptiveReal : IOptimiser<DifferentiableFunction<double[], double>, double[], double>
    {
        public int _maxIterationsWithoutImprovementBeforeReset = 5;
        public double _stepSizeIncreaseFactor = 1.2;
        public double _stepSizeDecreaseFactor = 0.8;
        public int MaxIterations = 1000;

        /// <summary>
        /// If not NaN, then if the gradient's 2-norm goes below this value, the optimisation exits. 
        /// </summary>
        public double GradientTerminationThreshold = double.NaN;
        public double LearningRate = double.NaN;
        public bool Verbose = false;
        public OptimisationType Type = OptimisationType.Minimise;

        private double[] _point;
        private double[] _gradient;

        public void Reset()
        {
            _point = null;
            _gradient = null;
        }

        public double[] Optimise(DifferentiableFunction<double[], double> objFunc)
        {
            Reset();

            int dim = objFunc.Dimension;
            _point = new double[dim];

            double[] optimum = _point.Copy();
            double optimalValue = Type == OptimisationType.Minimise ? double.PositiveInfinity : double.NegativeInfinity;
            int iterationsSinceOptimalValue = 0;

            for (int i = 0; i < MaxIterations; i++)
            {
                objFunc.EvaluateDerivative(_point, ref _gradient);

                if (Type == OptimisationType.Minimise)
                {
                    for (int d = 0; d < dim; d++)
                    {
                        _point[d] -= LearningRate * _gradient[d];
                    }
                }
                else
                {
                    for (int d = 0; d < dim; d++)
                    {
                        _point[d] += LearningRate * _gradient[d];
                    }
                }

                double fn = objFunc.Evaluate(_point);
                if ((Type == OptimisationType.Minimise && fn < optimalValue) || (Type == OptimisationType.Maximise && fn > optimalValue))
                {
                    optimalValue = fn;
                    iterationsSinceOptimalValue = 0;
                    optimum = _point.Copy();
                    LearningRate *= _stepSizeIncreaseFactor;
                }
                else
                {
                    iterationsSinceOptimalValue++;
                    LearningRate *= _stepSizeDecreaseFactor;
                }

                if (iterationsSinceOptimalValue > _maxIterationsWithoutImprovementBeforeReset)
                {
                    _point = optimum.Copy();
                    iterationsSinceOptimalValue = 0;
                }

                if (!double.IsNaN(GradientTerminationThreshold))
                {
                    if (_gradient.Norm(2) < GradientTerminationThreshold) return optimum;
                }

                if (Verbose)
                {
                    Console.WriteLine($"Iteration{i}: f(x) = {objFunc.Evaluate(_point)}, Learning rate = {LearningRate}");
                }
            }
            return optimum;
        }
    }
}
