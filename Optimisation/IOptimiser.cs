﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Optimisation
{
    /// <summary>
    /// Optimiser interface for maximising/minimising a function F subject in the space S
    /// </summary>
    /// <typeparam name="F">The objective function, which is defined in space S</typeparam>
    /// <typeparam name="TInput">The function's space</typeparam>
    public interface IOptimiser<TFunc, TInput, TOutput> where TFunc : Function<TInput, TOutput>
    {
        void Reset();

        TInput Optimise(TFunc objectiveFunction);
    }
}
