﻿using LinearNet.Structs;
using System;

namespace LinearNet.Optimisation
{
    /// <summary>
    /// Newton optimisation class for objective functions f: C^n -> R
    /// </summary>
    public class NewtonOptimiserComplex : IOptimiser<TwiceDifferentiableFunction<Complex[], Complex>, Complex[], Complex>
    {
        private Complex[,] _hessian;
        private Complex[,] _gradient;
        private Complex[] _point;

        public void Reset()
        {
            throw new NotImplementedException();
        }
        public Complex[] Optimise(TwiceDifferentiableFunction<Complex[], Complex> objFunc)
        {
            throw new NotImplementedException();
        }

    }
}
