﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Optimisation
{
    public enum OptimisationType
    {
        Minimise, Maximise
    }
}
