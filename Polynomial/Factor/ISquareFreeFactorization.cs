﻿using LinearNet.Structs;
using System;
using System.Collections.Generic;
using System.Text;

namespace LinearNet.LinearAlgebra
{
    /// <summary>
    /// Interface for square-free factorization algorithms for univariate 
    /// monic polynomials over <txt>T</txt>
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <cat>linear-algebra</cat>
    public interface ISquareFreeFactorization<T> where T : new()
    {
        /// <summary>
        /// Performs a square-free factorization of a univariate polynomial
        /// </summary>
        /// <param name="polynomial">The polynomial to factor.</param>
        /// <returns>
        /// The square-free factorization as a collection of tuples of 
        /// monic square-free factors and their powers.
        /// </returns>
        SquareFreeFactorization<T> Factor(Polynomial<T> polynomial);
    }
}
