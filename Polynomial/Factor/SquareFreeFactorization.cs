﻿using LinearNet.Structs;
using System;
using System.Collections.Generic;
using System.Text;

namespace LinearNet.LinearAlgebra
{
    /// <summary>
    /// Represents a square-free factorization of a univariate polynomial over <txt>T</txt>
    /// </summary>
    /// <cat>linear-algebra</cat>
    public sealed class SquareFreeFactorization<T> where T : new()
    {
        public T ConstantFactor { get; private set; }
        public Dictionary<Polynomial<T>, long> Factors { get; private set; }

        internal SquareFreeFactorization(T constant, Dictionary<Polynomial<T>, long> factors)
        {
            ConstantFactor = constant;
            Factors = factors;
        }
        internal SquareFreeFactorization(T constant)
        {
            ConstantFactor = constant;
            Factors = new Dictionary<Polynomial<T>, long>();
        }
        internal void Add(Polynomial<T> term, long power)
        {
            if (Factors.ContainsKey(term))
            {
                Factors[term] += power;
            }
            else
            {
                Factors[term] = power;
            }
        }
    }
}
