﻿using LinearNet.Providers;
using LinearNet.Structs;
using System;
using System.Collections.Generic;
using System.Text;

namespace LinearNet.LinearAlgebra
{
    /// <summary>
    /// Implementation of Musser's variation of the squarefree polynomial factorization algorithm
    /// initially presented by Tobey and Torowitz.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class TobeyHorowitzFactorization<T> : ISquareFreeFactorization<T> where T : new()
    {
        public TobeyHorowitzFactorization()
        {
            // Only defined for fields with characteristic 0.
            if (typeof(IField<>).IsAssignableFrom(typeof(T)))
            {
                IField<T> f = new T() as IField<T>;
                if (!f.Characteristic.IsZero)
                {
                    throw new InvalidOperationException("Tobey-Horowitz factorization is only defined for fields with characteristic 0.");
                }
            }
        }

        public SquareFreeFactorization<T> Factor(Polynomial<T> f)
        {
            if (f is null)
            {
                throw new ArgumentNullException(nameof(f));
            }

            // Ensure that the polynomial is monic
            IProvider<T> provider = f.Provider;
            SquareFreeFactorization<T> solution;
            if (!f.IsMonic)
            {
                T lc = f.LeadingCoefficient;
                f = f.Divide(lc);
                solution = new SquareFreeFactorization<T>(lc);
            }
            else
            {
                solution = new SquareFreeFactorization<T>(provider.One);
            }

            Polynomial<T> g = Polynomial.GCD(f, f.Differentiate());
            Polynomial<T> h = f.Divide(g);

            long i = 1L;
            while (!h.IsConstant)
            {
                Polynomial<T> h1 = Structs.Polynomial.GCD(g, h);
                g = g.Divide(h1);

                Polynomial<T> m = h.Divide(h1);
                if (!m.IsConstant)
                {
                    solution.Add(m, i);
                }

                h = h1;
                ++i;
            }

            return solution;
        }
    }
}
