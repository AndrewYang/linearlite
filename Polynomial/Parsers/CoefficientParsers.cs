﻿using LinearNet.Structs.Fields;
using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;
using System.Text.RegularExpressions;

namespace LinearNet
{
    internal interface ICoefficientParser<T>
    {
        /// <summary>
        /// Parse and remove coefficient from string.
        /// </summary>
        /// <param name="s"></param>
        /// <param name="coefficient"></param>
        /// <returns></returns>
        internal bool TryParseCoefficient(ref string s, out T coefficient);
    }

    internal class BigRationalCoefficientParser : ICoefficientParser<BigRational>
    {
        bool ICoefficientParser<BigRational>.TryParseCoefficient(ref string s, out BigRational coefficient)
        {
            coefficient = default;

            // coefficient must appear at the start
            if (Regex.IsMatch(s, "^[\\-0-9]+\\/[0-9]+"))
            {
                // Parse coefficient as a rational p/q
                Match match = Regex.Match(s, "^[\\-0-9]+\\/[0-9]+");
                s = s.Substring(match.Value.Length);

                string[] parts = match.Value.Split('/');

                if (parts.Length != 2) return false;
                if (!BigInteger.TryParse(parts[0], out BigInteger num)) return false;
                if (!BigInteger.TryParse(parts[1], out BigInteger den)) return false;

                coefficient = new BigRational(num, den);
                return true;
            }

            // Special case: -x^2
            else if (Regex.IsMatch(s, "^\\-[a-zA-Z]+"))
            {
                coefficient = new BigRational(-1);
                s = s.Substring(1);
                return true;
            }

            else if (Regex.IsMatch(s, "^[\\-0-9]+"))
            {
                // Parse coefficient as integer
                Match match = Regex.Match(s, "^[\\-0-9]+");
                s = s.Substring(match.Value.Length);

                if (!BigInteger.TryParse(match.Value, out BigInteger num)) return false;

                coefficient = new BigRational(num);
                return true;
            }

            // Special case: 1
            else
            {
                coefficient = BigRational.One;
                return true;
            }
        }
    }
}
