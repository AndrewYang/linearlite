﻿using LinearNet.Structs;
using LinearNet.Structs.Fields;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Text.RegularExpressions;

namespace LinearNet
{
    internal abstract class PolynomialParser<T, TPolynomial>
    {
        internal abstract TPolynomial Zero { get; }
        internal abstract ICoefficientParser<T> CoeffParser { get; }

        public TPolynomial Parse(string s)
        {
            // Return the zero multivariate polynomial if string is empty
            if (string.IsNullOrEmpty(s))
            {
                return Zero;
            }

            // Multivariate polynomials over the rationals have limited expressivity:
            // The only allowed symbols are [0-9], [a-zA-Z], +, *, ^ and / (for fractions)
            if (!Regex.IsMatch(s, "[0-9a-zA-Z\\+\\-\\*\\^\\/\\s]*"))
            {
                throw new ArgumentOutOfRangeException("Invalid characters in string");
            }

            // Get all the indeterminates 
            string[] indeterminates = GetAllIndeterminates(s);

            // Split by addition or subtraction
            List<string> terms = SplitByAddSubtract(s);

            List<Tuple<int[], T>> coeffs = new List<Tuple<int[], T>>();
            List<string> list = indeterminates.ToList();
            foreach (string term in terms)
            {
                if (!TryParseTerm(term, list, out T coeff, out int[] monomial))
                {
                    throw new FormatException($"Unparsable term: '{term}'");
                }
                coeffs.Add(new Tuple<int[], T>(monomial, coeff));
            }

            return ToPolynomial(indeterminates, coeffs);
        }

        protected string[] GetAllIndeterminates(string s)
        {
            HashSet<string> variables = new HashSet<string>();

            MatchCollection matches = Regex.Matches(s, "[a-zA-Z]");
            foreach (Match match in matches)
            {
                if (!variables.Contains(match.Value))
                {
                    variables.Add(match.Value);
                }
            }

            string[] array = new string[variables.Count];
            int i = 0;
            foreach (string v in variables)
            {
                array[i++] = v;
            }
            return array;
        }

        /// <summary>
        /// Split by + or -
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        private List<string> SplitByAddSubtract(string s)
        {
            s = s.Trim();

            List<string> summands = new List<string>();

            StringBuilder termBuilder = new StringBuilder();
            foreach (char c in s)
            {
                if (c == '+' || c == '-')
                {
                    if (termBuilder.Length > 0)
                    {
                        string term = termBuilder.ToString();
                        if (!string.IsNullOrWhiteSpace(term))
                        {
                            summands.Add(term.Trim());
                        }
                        termBuilder.Clear();
                    }
                }

                if (c != '+' && c != ' ')
                {
                    termBuilder.Append(c);
                }
            }

            // Collect up anything at the end
            if (termBuilder.Length > 0)
            {
                string term = termBuilder.ToString();
                if (!string.IsNullOrWhiteSpace(term))
                {
                    summands.Add(term.Trim());
                }
            }

            return summands;
        }

        private bool TryParseTerm(string s, List<string> indeterminates, out T coefficient, out int[] monomial)
        {
            coefficient = default;
            monomial = new int[indeterminates.Count];

            if (string.IsNullOrWhiteSpace(s))
            {
                return false;
            }

            // Remove all spaces
            s = Regex.Replace(s, "\\s+", "");

            // coefficient must appear at the start
            if (!CoeffParser.TryParseCoefficient(ref s, out coefficient))
            {
                return false;
            }

            while (s.Length > 0)
            {
                Match match = Regex.Match(s, "^[a-zA-Z]\\^[0-9]+");
                if (match.Success)
                {
                    s = s.Substring(match.Value.Length);

                    string[] parts = match.Value.Split('^');
                    if (parts.Length != 2) return false;

                    string variable = parts[0];
                    int index = indeterminates.IndexOf(variable);
                    if (index < 0) return false;

                    if (!int.TryParse(parts[1], out int power)) return false;
                    monomial[index] = power;
                }

                // Otherwise - check for singular powers e.g. x = x^1
                else
                {
                    match = Regex.Match(s, "^[a-zA-Z]");
                    if (match.Success)
                    {
                        string variable = match.Value;
                        s = s.Substring(variable.Length);

                        int index = indeterminates.IndexOf(variable);
                        if (index < 0) return false;

                        monomial[index] = 1;
                    }
                    else
                    {
                        // Unparsable term
                        return false;
                    }
                }
            }

            return true;
        }

        protected abstract TPolynomial ToPolynomial(string[] indeterminates, List<Tuple<int[], T>> terms);
    }

    /// <summary>
    /// Parsing multivariate polynomials of the BigRational type
    /// </summary>
    internal class MultivariatePolynomialParser : PolynomialParser<BigRational, MultivariatePolynomial<BigRational>>
    {
        internal override MultivariatePolynomial<BigRational> Zero => new MultivariatePolynomial<BigRational>();
        internal override ICoefficientParser<BigRational> CoeffParser => new BigRationalCoefficientParser();

        protected override MultivariatePolynomial<BigRational> ToPolynomial(string[] indeterminates, List<Tuple<int[], BigRational>> terms)
        {
            return new MultivariatePolynomial<BigRational>(indeterminates, terms);
        }
    }

    internal class SparsePolynomialParser : PolynomialParser<BigRational, Polynomial<BigRational>>
    {
        internal override Polynomial<BigRational> Zero => new Polynomial<BigRational>();
        internal override ICoefficientParser<BigRational> CoeffParser => new BigRationalCoefficientParser();

        protected override Polynomial<BigRational> ToPolynomial(string[] indeterminates, List<Tuple<int[], BigRational>> terms)
        {
            if (indeterminates.Length > 1)
            {
                throw new FormatException("Polynomial has more than 1 indeterminate, please use another polynomial type.");
            }

            if (indeterminates.Length == 0)
            {
                if (terms.Count > 1)
                {
                    throw new FormatException("Polynomial contains more than one constant term.");
                }
                if (terms.Count == 1)
                {
                    return new Polynomial<BigRational>(terms[0].Item2);
                }
                if (terms.Count == 0)
                {
                    return new Polynomial<BigRational>();
                }
            }

            Dictionary<long, BigRational> coeffs = new Dictionary<long, BigRational>();
            foreach (Tuple<int[], BigRational> term in terms)
            {
                coeffs[term.Item1[0]] = term.Item2;
            }
            return new Polynomial<BigRational>(coeffs);
        }
    }

    internal class DensePolynomialParser : PolynomialParser<BigRational, DensePolynomial<BigRational>>
    {
        internal override DensePolynomial<BigRational> Zero => throw new NotImplementedException();
        internal override ICoefficientParser<BigRational> CoeffParser => new BigRationalCoefficientParser();

        protected override DensePolynomial<BigRational> ToPolynomial(string[] indeterminates, List<Tuple<int[], BigRational>> terms)
        {
            if (indeterminates.Length > 1)
            {
                throw new FormatException("Polynomial has more than 1 indeterminate, please use the multivariate polynomial class instead.");
            }

            if (terms.Count == 0)
            {
                return new DensePolynomial<BigRational>(); // zero polynomial
            }

            if (indeterminates.Length == 0)
            {
                if (terms.Count > 1)
                {
                    throw new FormatException("Polynomial contains more than one constant term.");
                }
                return new DensePolynomial<BigRational>(terms[0].Item2); // constant polynomial
            }

            int degree = terms.Max(t => t.Item1[0]);
            BigRational[] coefficients = new BigRational[degree + 1];
            foreach (Tuple<int[], BigRational> pair in terms)
            {
                coefficients[pair.Item1[0]] = pair.Item2;
            }
            return new DensePolynomial<BigRational>(coefficients);
        }
    }
}
