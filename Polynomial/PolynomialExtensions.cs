﻿using LinearNet.Solvers;
using LinearNet.Structs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.LinearAlgebra
{
    public static class PolynomialExtensions
    {
        public static DensePolynomial<T> Convert<F, T>(DensePolynomial<F> p, Func<F, T> Convert) where F : new() where T : new()
        {
            if (p == null)
            {
                throw new ArgumentNullException(nameof(p));
            }
            T[] coefficients = new T[p.Coefficients.Length];
            for (int i = 0; i < coefficients.Length; ++i)
            {
                coefficients[i] = Convert(p.Coefficients[i]);
            }
            return new DensePolynomial<T>(coefficients);
        }
        public static DensePolynomial<Complex> ToComplex(this DensePolynomial<int> p) => Convert(p, x => new Complex(x));
        public static DensePolynomial<Complex> ToComplex(this DensePolynomial<long> p) => Convert(p, x => new Complex(x));
        public static DensePolynomial<Complex> ToComplex(this DensePolynomial<float> p) => Convert(p, x => new Complex(x));
        public static DensePolynomial<Complex> ToComplex(this DensePolynomial<double> p) => Convert(p, x => new Complex(x));
        public static DensePolynomial<Complex> ToComplex(this DensePolynomial<decimal> p) => Convert(p, x => (Complex)x);

        public static DensePolynomial<double> ToDouble(this DensePolynomial<int> p) => Convert(p, x => (double)x);
        public static DensePolynomial<double> ToDouble(this DensePolynomial<long> p) => Convert(p, x => (double)x);
        public static DensePolynomial<double> ToDouble(this DensePolynomial<float> p) => Convert(p, x => (double)x);
        public static DensePolynomial<double> ToDouble(this DensePolynomial<decimal> p) => Convert(p, x => (double)x);

        public static DensePolynomial<float> ToFloat(this DensePolynomial<int> p) => Convert(p, x => (float)x);
        public static DensePolynomial<float> ToFloat(this DensePolynomial<long> p) => Convert(p, x => (float)x);
        public static DensePolynomial<float> ToFloat(this DensePolynomial<double> p) => Convert(p, x => (float)x);
        public static DensePolynomial<float> ToFloat(this DensePolynomial<decimal> p) => Convert(p, x => (float)x);

        public static DensePolynomial<long> ToLong(this DensePolynomial<int> p) => Convert(p, x => (long)x);

        public static Complex[] Roots(this DensePolynomial<int> p) => Roots(p.ToDouble());
        public static Complex[] Roots(this DensePolynomial<long> p) => Roots(p.ToDouble());
        public static Complex[] Roots(this DensePolynomial<float> p) => Roots(p.ToDouble());
        public static Complex[] Roots(this DensePolynomial<double> p)
        {
            IPolynomialRootFinder solver = new AberthMethodSolver();
            return solver.Solve(p);
        }
        public static Complex[] Roots(this DensePolynomial<decimal> p) => Roots(p.ToDouble());
        public static Complex[] Roots(this DensePolynomial<Complex> p)
        {
            IPolynomialRootFinder solver = new AberthMethodSolver();
            return solver.Solve(p);
        }
    }
}
