﻿using LinearNet.Benchmarks;
using LinearNet.Graphs.Tests;
using LinearNet.Meta;
using LinearNet.Tests;
using LinearNet.Tests.LinearAlgebra;
using LinearNet.Tests.LinearAlgebra.Tensor;
using LinearNet.Tests.LinearAlgebra.Vector;
using LinearNet.Tests.Symbolic;

namespace LinearNet
{
    class Program
    {
        static void Main(string[] args)
        {
            SparseVectorTests.RunAll(true);

            IMatrixTests.RunAll(true);
            RectangularMatrixTests.RunAll();
            DOKSparseMatrixTests.RunAll();
            DenseMatrixTests.RunAll(true);
            BandMatrixTests.RunAll(true);
            COOSparseMatrixTests.RunAll(true);
            CSRSparseMatrixTests.RunAll(true);
            CSCSparseMatrixTests.RunAll(true);
            SKYSparseMatrixTests.RunAll(true);
            MCSRSparseMatrixTests.RunAll(true);
            BlockDiagonalMatrixTests.RunAll(true);
            //BigSparseMatrixTests.RunAll(true);

            TensorTests.RunAll(false);
            DenseTensorTests.RunAll(true);
            COOSparseTensorTests.RunAll(true);
            CSFSparseTensorTests.RunAll(true);

            PermutationMatrixTests.PMatrixSwapRowsTest(false);

#if NETCOREAPP3_0
            SIMDTests.RunAll();
#endif

            SymbolicTests.RunAll(false);
            SymbolicTrigonometryTests.RunAll(false);
            //TemplateTests.RunAll(true);
            //PermutationIteratorTests.Test();
            //InstantiationTests.TestCreateDefaultProvider();

            //PolynomialTests.RunAll(false);

            //MinimualResidualSolverTests.RunAll(true);
            //SORSolverTests.RunAll(true);
            //JacobiMethodTests.RunAll(true);
            //CGSolverTest.RunAll(true);
            //SteepestDescentTests.RunAll();
            //LSQRSolverTest.RunAll(true);

            //MatrixBenchmarks.FixedSizeStrassenMultiplicationTest();
            //MatrixBenchmarks.MatrixAdditionTest();
            //MatrixBenchmarks.MatrixMultiplicationTest();
            //MatrixBenchmarks.JaggedMatrixBlockMultiplicationTest();
            //MatrixBenchmarks.DenseMatrixParallelMultiplicationTest();
            //MatrixBenchmarks.MatrixStrassenMultiplicationTest();
            //MatrixBenchmarks.DenseMatrixParallelStrassenMultiplicationTest();
            //MatrixBenchmarks.MatrixInversionTest();
            //MatrixBenchmarks.MatrixStrassenInversionTest();
            //MatrixBenchmarks.RealLinearSolverTest();
            //MatrixBenchmarks.QRDecompositionTest();
            //MatrixBenchmarks.OptimizeParameterBlockHouseholderTest();
            //MatrixBenchmarks.LUDecompositionTest();
            //MatrixBenchmarks.LUPDecompositionTest();
            //MatrixBenchmarks.MatrixTranspositionTest();
            //MatrixBenchmarks.InliningTest();
            //MatrixBenchmarks.SVDecompositionTest();
            //MatrixBenchmarks.CacheObliviousMultiplicationTest();
            //MatrixBenchmarks.QRDecomposition_Version2();
            //MatrixBenchmarks.BlockCholesky();

            //MatrixBenchmarks.ComplexLinearSolverTest();

            //MatrixBenchmarks.DenseMatrixParallelStrassenMultiplicationTest();
            //MatrixBenchmarks.BidiagonalFormTest();
            //MatrixBenchmarks.RealCharacteristicPolynomialTest();
            //MatrixBenchmarks.RealEigenvaluesTest();
            //MatrixBenchmarks.TridiagonalFormTest();
            //MatrixBenchmarks.HessenbergFormTest();
            //MatrixBenchmarks.SVDecompositionTest();
            //MatrixBenchmarks.TriangularSolveTest();

            //SparseMatrixBenchmarks.CSRAdd();
            //SparseMatrixBenchmarks.CSCMatrixMultiply();
            //SparseMatrixBenchmarks.MCSRvsCSRMatrixMultiply();
            //SparseMatrixBenchmarks.SKYSparseMatrixTriangularSolve();
            //SparseMatrixBenchmarks.SKYSparseMatrixCholesky();
            //SparseMatrixBenchmarks.LoadCSRSymmetric();
            //SparseMatrixBenchmarks.CSCCholesky();

            //BandMatrixBenchmarks.Multiplication();
            //BandMatrixBenchmarks.TriangularSolve();
            //BandMatrixBenchmarks.ParallelMultiplication();
            //BandMatrixBenchmarks.Cholesky();

            //TensorTests.TensorDimensionTest();
            //TensorTests.DenseTensorDirectSumTest();
            //TensorTests.DenseTensorProductTest();

            //TensorBenchmarks.AccessBenchmarks();
            //TensorBenchmarks.MatrixTensorBenchmarks();
            //TensorBenchmarks.DenseTensorContract();

            //RandomTests.TestDictionaryAccessPerformance();

            //MatrixAlgorithmSearcher3x3.TestLadermanAlgorithm();

            //FunctionsTests.RunAll();
            //ComplexTests.RunAll();

            //SIMDBenchmarks.AXPY();
            //SIMDBenchmarks.AXPY_Complex();
            //SIMDBenchmarks.DotProduct();
            //SIMDBenchmarks.DotProductComplex();
            //SIMDBenchmarks.MultiplicationNoTranspose();
            //SIMDBenchmarks.MultiplicationFloatMatrix4x4();

            //SSEBenchmarks.AXPY_double();
            //SSEBenchmarks.AXPY_float();
            //BLAS1Benchmarks.AXPY_Complex(new SSE2ComplexProvider());
            //SSEBenchmarks.DOT_double();
            //SSEBenchmarks.DOT_float();
            //BLAS1Benchmarks.DOT_Complex(new SSE2ComplexProvider());
            //BLAS1Benchmarks.GivensRotateRow(new AVX2DoubleProvider());
            //BLAS1Benchmarks.GivensRotateRow(new AVX2FloatProvider());
            //BLAS1Benchmarks.GivensRotateRow(new SSEFloatProvider(true));
            //BLAS1Benchmarks.GivensRotateRow(new SSE2DoubleProvider(true));
            //BLAS1Benchmarks.GivensRotateRow(new AVX2Provider<float>(new NativeFloatProvider()));
            //BLAS1Benchmarks.GivensRotateRow(new AVX2Int32Provider());

            //CUDABenchmarks.AXPY_double();
            //CUDABenchmarks.MMult();

            //PolynomialBenchmarks.UnivaratePowers();

            //VectorAdditionCompiler.Compile();

            //BinaryHeapTests.RunAll();

            TensorBenchmarks.Run();
        }
    }
}
