﻿#if NETCOREAPP3_0

using System;
using System.Numerics;
using Complex = LinearNet.Structs.Complex;

namespace LinearNet.Providers.AVX2
{
    /// <summary>
    /// Level-1 BLAS implementation for <txt>Complex</txt>, using AVX2 (advanced vector extensions) SIMD hardware acceleration. 
    /// </summary>
    /// <cat>linear-algebra</cat>
    public class AVX2ComplexProvider : IDenseBLAS1<Complex>
    {
        private readonly bool _useUnsafe;
        private readonly ComplexProvider _provider;

        public IProvider<Complex> Provider => _provider;

        /// <summary>
        /// Instantiate a new AVX2 level-1 BLAS implementation. 
        /// </summary>
        /// <param name="useUnsafe">
        /// If <txt>true</txt>, <txt>unsafe</txt> pointers will be used to improve performance.
        /// </param>
        public AVX2ComplexProvider(bool useUnsafe = false)
        {
            _useUnsafe = useUnsafe;
            _provider = new ComplexProvider();
        }

        #region BLAS 1

        void IDenseBLAS1<Complex>.ADD(Complex[] u, Complex[] v, Complex[] sum, int start, int end)
        {
            throw new NotImplementedException();
        }
        void IDenseBLAS1<Complex>.SUB(Complex[] u, Complex[] v, Complex[] diff, int start, int end)
        {
            throw new NotImplementedException();
        }

        // Unfortunately, this implementation does not outperform the naive implementation for Complex[20,000,000]
        void IDenseBLAS1<Complex>.AXPY(Complex[] y, Complex[] x, Complex alpha, int start, int end)
        {
            if (_useUnsafe)
            {
                AXPY_unsafe(y, x, alpha, start, end);
            }
            else
            {
                AXPY_safe(y, x, alpha, start, end);
            }
        }
        void IDenseBLAS1<Complex>.AXPY(Complex[] y, Complex[] x, Complex alpha, int y_start, int y_end, int xy_offset)
        {
            throw new NotImplementedException();
        }
        private static void AXPY_safe(Complex[] y, Complex[] x, Complex alpha, int start, int end)
        {
            int count = Vector<double>.Count,
                _end = (end - start) / count * count + start,
                i, j;

            Vector<double> alpha_re_vect = new Vector<double>(alpha.Real);
            Vector<double> alpha_im_vect = new Vector<double>(alpha.Imaginary);

            double[] x_re_buff = new double[count],
                x_im_buff = new double[count];

            for (i = start; i < _end;)
            {
                for (j = 0; j < count; ++j)
                {
                    Complex _x = x[i + j];
                    x_re_buff[j] = _x.Real;
                    x_im_buff[j] = _x.Imaginary;
                }

                Vector<double> x_re_vect = new Vector<double>(x_re_buff);
                Vector<double> x_im_vect = new Vector<double>(x_im_buff);
                Vector<double> re_buff =
                    Vector.Subtract(
                        Vector.Multiply(alpha_re_vect, x_re_vect),
                        Vector.Multiply(alpha_im_vect, x_im_vect));

                Vector<double> im_buff =
                    Vector.Add(
                        Vector.Multiply(alpha_re_vect, x_im_vect),
                        Vector.Multiply(alpha_im_vect, x_re_vect));

                for (j = 0; j < count; ++j, ++i)
                {
                    y[i].Real += re_buff[j];
                    y[i].Imaginary += im_buff[j];
                }
            }

            for (i = _end; i < end; ++i)
            {
                y[i] += alpha * x[i];
            }
        }
        private static unsafe void AXPY_unsafe(Complex[] y, Complex[] x, Complex alpha, int start, int end)
        {
            throw new NotImplementedException();
        }

        Complex IDenseBLAS1<Complex>.DOT(Complex[] u, Complex[] v, int start, int end)
        {
            int count = Vector<double>.Count,
                last = (end - start) / count * count + start,
                i, j;

            double[] u_re_buff = new double[count],
                u_im_buff = new double[count],
                v_re_buff = new double[count],
                v_im_buff = new double[count];

            double re = 0.0, im = 0.0;
            for (i = start; i < last; i += count)
            {
                // Load all into buffers
                for (j = 0; j < count; ++j)
                {
                    int index = i + j;
                    Complex _u = u[index], _v = v[index];
                    u_re_buff[j] = _u.Real;
                    u_im_buff[j] = _u.Imaginary;
                    v_re_buff[j] = _v.Real;
                    v_im_buff[j] = _v.Imaginary;
                }

                Vector<double> u_re = new Vector<double>(u_re_buff),
                    u_im = new Vector<double>(u_im_buff),
                    v_re = new Vector<double>(v_re_buff),
                    v_im = new Vector<double>(v_im_buff);

                re += Vector.Dot(u_re, v_re) - Vector.Dot(u_im, v_im);
                im += Vector.Dot(u_re, v_im) + Vector.Dot(u_im, v_re);
            }

            for (i = last; i < end; ++i)
            {
                Complex _u = u[i], _v = v[i];
                re += _u.Real * _v.Real - _u.Imaginary * _v.Imaginary;
                im += _u.Imaginary * _v.Real + _u.Real * _v.Imaginary;
            }

            return new Complex(re, im);
        }
        Complex IDenseBLAS1<Complex>.DOT(Complex[] u, Complex[] v, int u_start, int u_end, int v_u_offset)
        {
            throw new NotImplementedException();
        }
        void IDenseBLAS1<Complex>.SCAL(Complex[] x, Complex s, int start, int end)
        {
            throw new NotImplementedException();
        }
        Complex IDenseBLAS1<Complex>.XTA(Complex[] x, Complex[][] A, int column, int start, int end)
        {
            throw new NotImplementedException();
        }
        Complex IDenseBLAS1<Complex>.XTA(Complex[] x, Complex[][] A, int column, int x_start, int x_end, int Ax_offset)
        {
            throw new NotImplementedException();
        }
        void IDenseBLAS1<Complex>.ROTR(Complex[][] A, int i, int j, Complex c, Complex s, int colStart, int colEnd)
        {
            throw new NotImplementedException();
        }
        void IDenseBLAS1<Complex>.ROTC(Complex[][] A, int i, int j, Complex c, Complex s, int rowStart, int rowEnd)
        {
            throw new NotImplementedException();
        }

        void IDenseBLAS1<Complex>.PMULT(Complex[] u, Complex[] v, Complex[] uv, int u_start, int u_end, int v_u_offset, int uv_u_offset)
        {
            throw new NotImplementedException();
        }

        #endregion

    }
}
#endif