﻿#if NETCOREAPP3_0

using LinearNet.Providers.LinearAlgebra;

namespace LinearNet.Providers.AVX2
{
    /// <summary>
    /// Level-1 BLAS implementation for <txt>double</txt> using AVX2 (advanced vector extensions) SIMD hardware acceleration. 
    /// </summary>
    /// <cat>linear-algebra</cat>
    public class AVX2DoubleProvider : AVX2Provider<double>
    {
        public AVX2DoubleProvider() : base(new NativeDoubleProvider(), new NativeDoubleProvider()) { }
    }
}

#endif