﻿#if NETCOREAPP3_0

using LinearNet.Providers.LinearAlgebra;

namespace LinearNet.Providers.AVX2
{
    /// <summary>
    /// Level-1 BLAS implementation for <txt>float</txt>, using AVX2 SIMD hardware acceleration. 
    /// </summary>
    /// <cat>linear-algebra</cat>
    public class AVX2FloatProvider : AVX2Provider<float>
    {
        public AVX2FloatProvider() : base(new NativeFloatProvider(), new NativeFloatProvider()) { }
    }
}

#endif