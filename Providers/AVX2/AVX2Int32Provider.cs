﻿#if NETCOREAPP3_0

using LinearNet.Providers.LinearAlgebra;

namespace LinearNet.Providers.AVX2
{
    /// <summary>
    /// Implements level-1 BLAS methods for type <txt>int</txt> using AVX2 hardware acceleration. 
    /// </summary>
    /// <cat>linear-algebra</cat>
    public sealed class AVX2Int32Provider : AVX2Provider<int>
    {
        /// <summary>
        /// Create a new instance of this provider.
        /// </summary>
        public AVX2Int32Provider() : base(new NativeInt32Provider(), new NativeInt32Provider()) { }
    }
}

#endif