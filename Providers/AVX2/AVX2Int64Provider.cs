﻿#if NETCOREAPP3_0

using LinearNet.Providers.LinearAlgebra;

namespace LinearNet.Providers.AVX2
{
    /// <summary>
    /// Implements level-1 BLAS methods for type <txt>long</txt> (64-bit integers), using AVX2 hardware acceleration.
    /// </summary>
    /// <cat>linear-algebra</cat>
    public sealed class AVX2Int64Provider : AVX2Provider<long>
    {
        /// <summary>
        /// Create a new instance of this provider.
        /// </summary>
        public AVX2Int64Provider() : base(new NativeInt64Provider(), new NativeInt64Provider()) { }
    }
}

#endif