﻿#if NETCOREAPP3_0

using System;
using System.Numerics;

namespace LinearNet.Providers.AVX2
{
    public class AVX2Provider<T> : IDenseBLAS1<T>, IDenseBLAS2<T> where T : unmanaged
    {
        public IProvider<T> Provider => _provider;

        /// <summary>
        /// Returns whether AVX2 instructions are supported on the host CPU.
        /// </summary>
        public bool IsSupported { get { return AVX2Checks.IsSupported(); } }

        IProvider<T> IDenseBLAS2<T>.Provider => throw new NotImplementedException();

        private readonly IProvider<T> _provider;
        private readonly IDenseBLAS1<T> _baseBlas1;
        private readonly IDenseBLAS2<T> _baseBlas2;
        private readonly int _count;

        internal AVX2Provider(IDenseBLAS1<T> fallback, IDenseBLAS2<T> blas2Fallback)
        {
            _baseBlas1 = fallback;
            _baseBlas2 = blas2Fallback;
            _provider = fallback.Provider;
            _count = Vector<T>.Count;
        }

        #region BLAS 1
        void IDenseBLAS1<T>.ADD(T[] u, T[] v, T[] sum, int start, int end)
        {
            int count = Vector<T>.Count,
                _end = (end - start) / count * count + start,
                i;

            for (i = start; i < _end; i += count)
            {
                Vector.Add(new Vector<T>(u, i), new Vector<T>(v, i)).CopyTo(sum, i);
            }

            // Take care of the remainder
            _baseBlas1.ADD(u, v, sum, _end, end);
        }
        void IDenseBLAS1<T>.SUB(T[] u, T[] v, T[] diff, int start, int end)
        {
            int count = Vector<T>.Count,
                _end = (end - start) / count * count + start,
                i;

            for (i = start; i < _end; i += count)
            {
                Vector.Subtract(new Vector<T>(u, i), new Vector<T>(v, i)).CopyTo(diff, i);
            }

            _baseBlas1.SUB(u, v, diff, _end, end);
        }

        void IDenseBLAS1<T>.AXPY(T[] y, T[] x, T alpha, int start, int end)
        {
            int count = Vector<T>.Count,
                _end = (end - start) / count * count + start,
                i;

            Vector<T> alpha_vect = new Vector<T>(alpha);
            for (i = start; i < _end; i += count)
            {
                Vector<T> x_vect = new Vector<T>(x, i);
                Vector<T> y_vect = new Vector<T>(y, i);
                Vector.Add(y_vect, Vector.Multiply(alpha_vect, x_vect)).CopyTo(y, i);
            }

            // Take care of the remainder
            _baseBlas1.AXPY(y, x, alpha, _end, end);
        }

        void IDenseBLAS1<T>.AXPY(T[] y, T[] x, T alpha, int y_start, int y_end, int xy_offset)
        {
            int count = Vector<T>.Count,
                _end = (y_end - y_start) / count * count + y_start,
                i;

            Vector<T> alpha_vect = new Vector<T>(alpha);
            for (i = y_start; i < _end; i += count)
            {
                Vector<T> x_vect = new Vector<T>(x, i + xy_offset);
                Vector<T> y_vect = new Vector<T>(y, i);
                Vector.Add(y_vect, Vector.Multiply(alpha_vect, x_vect)).CopyTo(y, i);
            }

            // Take care of the remainder
            _baseBlas1.AXPY(y, x, alpha, _end, y_end, xy_offset);
        }

        T IDenseBLAS1<T>.DOT(T[] u, T[] v, int start, int end)
        {
            int count = Vector<T>.Count;

            int _end = (end - start) / count * count + start, i;
            Vector<T> sum = Vector<T>.Zero;
            for (i = start; i < _end; i += count)
            {
                sum += Vector.Multiply(new Vector<T>(u, i), new Vector<T>(v, i));
            }

            T dot = _provider.Zero;
            for (i = 0; i < count; ++i)
            {
                dot = _provider.Add(dot, sum[i]);
            }
            return _provider.Add(dot, _baseBlas1.DOT(u, v, _end, end));
        }

        T IDenseBLAS1<T>.DOT(T[] u, T[] v, int u_start, int u_end, int v_u_offset)
        {
            int count = Vector<T>.Count;

            int _end = (u_end - u_start) / count * count + u_start, i;
            Vector<T> sum = Vector<T>.Zero;
            for (i = u_start; i < _end; i += count)
            {
                sum += Vector.Multiply(new Vector<T>(u, i), new Vector<T>(v, i + v_u_offset));
            }

            T dot = _provider.Zero;
            for (i = 0; i < count; ++i)
            {
                dot = _provider.Add(dot, sum[i]);
            }
            return _provider.Add(dot, _baseBlas1.DOT(u, v, _end, u_end, v_u_offset));
        }

        void IDenseBLAS1<T>.SCAL(T[] x, T s, int start, int end)
        {
            int count = Vector<T>.Count,
                _end = (end - start) / count * count + start,
                i;

            Vector<T> alpha_vect = new Vector<T>(s);
            for (i = start; i < _end; i += count)
            {
                Vector.Multiply(alpha_vect, new Vector<T>(x, i)).CopyTo(x, i);
            }

            _baseBlas1.SCAL(x, s, _end, end);
        }

        T IDenseBLAS1<T>.XTA(T[] x, T[][] A, int column, int start, int end)
        {
            int count = Vector<T>.Count;

            T[] buffer = new T[count];
            int _end = (end - start) / count * count + start, i, j;

            T sum = _provider.Zero;
            for (i = start; i < _end; i += count)
            {
                for (j = 0; j < count; ++j)
                {
                    buffer[j] = A[i + j][column];
                }
                sum = _provider.Add(sum, Vector.Dot(new Vector<T>(x, i), new Vector<T>(buffer)));
            }

            for (i = _end; i < end; ++i)
            {
                sum = _provider.Add(sum, _provider.Multiply(x[i], A[i][column]));
            }
            return sum;
        }

        T IDenseBLAS1<T>.XTA(T[] x, T[][] A, int column, int x_start, int x_end, int Ax_offset)
        {
            int count = Vector<T>.Count;

            T[] buffer = new T[count];
            int _end = (x_end - x_start) / count * count + x_start, i, j;

            T sum = _provider.Zero;
            for (i = x_start; i < _end; i += count)
            {
                int _i = i + Ax_offset;
                for (j = 0; j < count; ++j)
                {
                    buffer[j] = A[_i + j][column];
                }
                sum = _provider.Add(sum, Vector.Dot(new Vector<T>(x, i), new Vector<T>(buffer)));
            }

            for (i = _end; i < x_end; ++i)
            {
                sum = _provider.Add(sum, _provider.Multiply(x[i], A[i + Ax_offset][column]));
            }
            return sum;
        }

        void IDenseBLAS1<T>.ROTR(T[][] A, int i, int j, T c, T s, int col_start_incl, int col_end_excl)
        {
            int count = Vector<T>.Count,
                _end = (col_end_excl - col_start_incl) / count * count + col_start_incl;

            Vector<T> vect_c = new Vector<T>(c);
            Vector<T> vect_s = new Vector<T>(s);

            // only alter the rows i and j of matrix A
            T[] A_i = A[i], A_j = A[j];
            for (int k = col_start_incl; k < _end; k += count)
            {
                Vector<T> r_i = new Vector<T>(A_i, k);
                Vector<T> r_j = new Vector<T>(A_j, k);

                Vector.Add(Vector.Multiply(vect_c, r_i), Vector.Multiply(vect_s, r_j)).CopyTo(A_i, k);
                Vector.Subtract(Vector.Multiply(vect_c, r_j), Vector.Multiply(vect_s, r_i)).CopyTo(A_j, k);
            }

            _baseBlas1.ROTR(A, i, j, c, s, _end, col_end_excl);
        }

        void IDenseBLAS1<T>.ROTC(T[][] A, int i, int j, T c, T s, int rowStart, int rowEnd)
        {
            throw new NotImplementedException();
        }

        void IDenseBLAS1<T>.PMULT(T[] u, T[] v, T[] uv, int u_start, int u_end, int v_u_offset, int uv_u_offset)
        {
            int count = Vector<T>.Count,
                _end = (u_end - u_start) / count * count + u_start;

            if (uv_u_offset == 0)
            {
                for (int k = u_start; k < _end; k += count)
                {
                    Vector<T> u_vect = new Vector<T>(u, k);
                    Vector<T> v_vect = new Vector<T>(v, k + v_u_offset);
                    Vector.Add(new Vector<T>(uv, k), Vector.Multiply(u_vect, v_vect)).CopyTo(uv, k);
                }
            }
            else
            {
                for (int k = u_start; k < _end; k += count)
                {
                    Vector<T> u_vect = new Vector<T>(u, k);
                    Vector<T> v_vect = new Vector<T>(v, k + v_u_offset);

                    int uv_index = k + uv_u_offset;
                    Vector.Add(new Vector<T>(uv, uv_index), Vector.Multiply(u_vect, v_vect)).CopyTo(uv, uv_index);
                }
            }

            _baseBlas1.PMULT(u, v, uv, _end, u_end, v_u_offset, uv_u_offset);
        }

        #endregion

        #region BLAS 2

        void IDenseBLAS2<T>.Add(T[][] A, T[][] B, T[][] C, int a_row_start, int a_col_start, int b_row_start, int b_col_start, int rows, int cols)
        {
            int count = Vector<T>.Count,
                col_end = cols / count * count;

            for (int r = 0; r < rows; ++r)
            {
                T[] A_row = A[r + a_row_start], B_row = B[r + b_row_start], C_row = C[r];
                for (int c = 0; c < col_end; c += count)
                {
                    Vector.Add(
                        new Vector<T>(A_row, c + a_col_start), 
                        new Vector<T>(B_row, c + b_col_start))
                        .CopyTo(C_row, c);
                }
            }

            _baseBlas2.Add(A, B, C, a_row_start, a_col_start + col_end, b_row_start, b_col_start + col_end, rows, cols - col_end);
        }

        void IDenseBLAS2<T>.Subtract(T[][] A, T[][] B, T[][] C, int a_row_start, int a_col_start, int b_row_start, int b_col_start, int rows, int cols)
        {
            int count = _count,
                col_end = cols / count * count;

            for (int r = 0; r < rows; ++r)
            {
                T[] A_row = A[r + a_row_start], B_row = B[r + b_row_start], C_row = C[r];
                for (int c = 0; c < col_end; c += count)
                {
                    Vector.Subtract(
                        new Vector<T>(A_row, c + a_col_start),
                        new Vector<T>(B_row, c + b_col_start))
                        .CopyTo(C_row, c);
                }
            }

            _baseBlas2.Subtract(A, B, C, a_row_start, a_col_start + col_end, b_row_start, b_col_start + col_end, rows, cols - col_end);
        }

        void IDenseBLAS2<T>.Increment(T[][] A, T[][] B, int rows, int cols)
        {
            int count = _count,
                col_end = cols / count * count;

            for (int r = 0; r < rows; ++r)
            {
                T[] Ar = A[r], Br = B[r];
                for (int c = 0; c < col_end; c += count)
                {
                    Vector.Add(new Vector<T>(Ar, c), new Vector<T>(Br, c)).CopyTo(Ar, c);
                }
            }

            if (col_end < cols)
            {
                for (int r = 0; r < rows; ++r)
                {
                    _baseBlas1.ADD(A[r], B[r], A[r], col_end, cols);
                }
            }
        }

        void IDenseBLAS2<T>.Increment(T[][] A, T[][] B, int a_row_start, int a_col_start, int rows, int cols)
        {
            int count = _count,
                   col_end = cols / count * count;

            for (int r = 0; r < rows; ++r)
            {
                T[] Ar = A[r + a_row_start], Br = B[r];
                for (int c = 0, ac = a_col_start; c < col_end; c += count, ac += count)
                {
                    Vector.Add(new Vector<T>(Ar, ac), new Vector<T>(Br, c)).CopyTo(Ar, ac);
                }
            }

            if (col_end < cols)
            {
                T one = _provider.One;
                int begin = a_col_start + col_end, end = a_col_start + cols;
                for (int r = 0; r < rows; ++r)
                {
                    _baseBlas1.AXPY(A[r + a_row_start], B[r], one, begin, end, -a_col_start);
                }
            }
        }

        void IDenseBLAS2<T>.Decrement(T[][] A, T[][] B, int rows, int cols)
        {
            int count = _count,
                   col_end = cols / count * count;

            for (int r = 0; r < rows; ++r)
            {
                T[] Ar = A[r], Br = B[r];
                for (int c = 0; c < col_end; c += count)
                {
                    Vector.Subtract(new Vector<T>(Ar, c), new Vector<T>(Br, c)).CopyTo(Ar, c);
                }
            }

            if (col_end < cols)
            {
                for (int r = 0; r < rows; ++r)
                {
                    _baseBlas1.SUB(A[r], B[r], A[r], col_end, cols);
                }
            }
        }

        void IDenseBLAS2<T>.Decrement(T[][] A, T[][] B, int a_row_start, int a_col_start, int rows, int cols)
        {
            int count = _count,
                   col_end = cols / count * count;

            for (int r = 0; r < rows; ++r)
            {
                T[] Ar = A[r + a_row_start], Br = B[r];
                for (int c = 0, ac = a_col_start; c < col_end; c += count, ac += count)
                {
                    Vector.Subtract(new Vector<T>(Ar, ac), new Vector<T>(Br, c)).CopyTo(Ar, ac);
                }
            }

            if (col_end < cols)
            {
                T negone = _provider.Negate(_provider.One);
                int begin = a_col_start + col_end, end = a_col_start + cols;
                for (int r = 0; r < rows; ++r)
                {
                    _baseBlas1.AXPY(A[r + a_row_start], B[r], negone, begin, end, -a_col_start);
                }
            }
        }

        void IDenseBLAS2<T>.Negate(T[][] A, T[][] result, int a_row_start, int a_col_start, int result_row_start, int result_col_start, int rows, int cols)
        {
            int count = _count,
                a_row_end = a_row_start + rows,
                a_col_end = a_col_start + cols / 4 * 4;

            for (int r = a_row_start, r1 = result_row_start; r < a_row_end; )
            {
                T[] Ar = A[r++], Cr = result[r1++]; 
                for (int c = a_col_start, rc = result_col_start; c < a_col_end; c += count, rc += count)
                {
                    Vector.Negate(new Vector<T>(Ar, c)).CopyTo(Cr, rc);
                }
            }

            int end = a_col_start + cols;
            if (a_col_end < end)
            {
                _baseBlas2.Negate(A, result, 
                    a_row_start, end, 
                    result_row_start, result_col_start + (a_col_end - a_col_start), 
                    rows, end - a_col_end);
            }
        }

        void IDenseBLAS2<T>.Copy(T[][] dest, T[][] src, int rows, int cols)
        {
            _baseBlas2.Copy(dest, src, rows, cols);
        }

        void IDenseBLAS2<T>.Copy(T[][] dest, T[][] src, int src_row_start, int src_col_start, int rows, int cols)
        {
            _baseBlas2.Copy(dest, src, src_row_start, src_col_start, rows, cols);
        }

        void IDenseBLAS2<T>.Copy(T[][] dest, T[][] src, int dest_row_start, int dest_col_start, int src_row_start, int src_col_start, int rows, int cols)
        {
            _baseBlas2.Copy(dest, src, dest_row_start, dest_col_start, src_row_start, src_col_start, rows, cols);
        }
        #endregion
    }

    internal class AVX2Checks
    {
        [System.Runtime.InteropServices.DllImport("kernel32.dll")]
        private static extern long GetEnabledXStateFeatures();

        internal static bool IsSupported()
        {
            return (GetEnabledXStateFeatures() & 4) != 0;
        }
    }
}
#endif