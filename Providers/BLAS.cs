﻿using LinearNet.Structs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Providers.LinearAlgebra
{
    internal static class BLAS
    {
        /*
        internal static readonly IDenseBLAS1<Complex> Complex = new ComplexLinearAlgebraProvider();
        internal static readonly IDenseBLAS1<decimal> Decimal = new DecimalLinearAlgebraProvider();
        internal static readonly IDenseBLAS1<double> Double = new DoubleLinearAlgebraProvider();
        internal static readonly IDenseBLAS1<float> Float = new FloatLinearAlgebraProvider();
        internal static readonly IDenseBLAS1<int> Int32 = new Int32LinearAlgebraProvider();
        internal static readonly IDenseBLAS1<long> Int64 = new Int64LinearAlgebraProvider();*/

        internal static readonly NativeComplexProvider Complex = new NativeComplexProvider();
        internal static readonly NativeDecimalProvider Decimal = new NativeDecimalProvider();
        internal static readonly NativeDoubleProvider Double = new NativeDoubleProvider();
        internal static readonly NativeFloatProvider Float = new NativeFloatProvider();
        internal static readonly NativeInt32Provider Int32 = new NativeInt32Provider();
        internal static readonly NativeInt64Provider Int64 = new NativeInt64Provider();
    }
    internal static class BLAS2
    {
        internal static readonly IDenseBLAS2<Complex> Complex = new NativeComplexProvider();
        internal static readonly IDenseBLAS2<decimal> Decimal = new NativeDecimalProvider();
        internal static readonly IDenseBLAS2<double> Double = new NativeDoubleProvider();
        internal static readonly IDenseBLAS2<float> Float = new NativeFloatProvider();
    }
}
