﻿using LinearNet.Structs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Providers
{
    /// <summary>
    /// Honestly, why does this class even exist
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class AdditiveGroupProvider<T> : IProvider<T> where T : IAdditiveGroup<T>, new()
    {
        public override T One => throw new NotImplementedException();
        public override T Zero => new T().AdditiveIdentity;

        public override T Add(T a, T b) => a.Add(b);
        public override T Divide(T a, T b) => throw new NotImplementedException();
        public override T Multiply(T a, T b) => throw new NotImplementedException();
        public override T MultiplyInteger(T a, int n) => throw new NotImplementedException();
        public override T Product(T[] x) => throw new NotImplementedException();
        public override T LogProduct(T[] x) => throw new NotImplementedException();
        public override T Subtract(T a, T b) => a.Subtract(b);
        public override T Sqrt(T a) => throw new NotImplementedException();
        public override T Pow(T a, int power) => throw new NotImplementedException();
        public override T Pow(T a, long power) => throw new NotImplementedException();
        public override T Negate(T a) => a.AdditiveInverse();
        public override T FromInt(int n) => throw new NotImplementedException();
        public override T FromLong(long n) => throw new NotImplementedException();
        public override T FromBigInt(BigInteger n) => throw new NotImplementedException();

        public override bool IsRealAndNegative(T a) => throw new NotImplementedException();
        public override bool InLeftHalfPlane(T a) => throw new NotImplementedException();
        public override bool Equals(T a, T b) => a.Equals(b);
        public override bool ApproximatelyEquals(T a, T b, double eps) => a.ApproximatelyEquals(b, eps);

        public override void Givens(T a, T b, T[] cs)
        {
            throw new NotImplementedException();
        }
    }
}
