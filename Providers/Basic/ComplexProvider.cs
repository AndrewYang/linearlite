﻿using LinearNet.Matrices;
using LinearNet.Providers.Basic.Interfaces;
using LinearNet.Structs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Providers
{
    public class ComplexProvider : IProvider<Complex>, IComplexProvider<double, Complex>
    {
        public static Action<Complex[], double, int, int> dSCAL = (x, s, start, end) =>
        {
            for (int i = start; i < end; ++i)
            {
                x[i].MultiplyEquals(s);
            }
        };

        public override Complex One => Complex.One;
        public override Complex Zero => Complex.Zero;

        public override Complex Add(Complex u, Complex v) => u.Add(v);
        public override Complex Divide(Complex u, Complex v) => u.Divide(v);
        public override Complex Multiply(Complex u, Complex v) => u.Multiply(v);
        public override Complex MultiplyInteger(Complex z, int n) => z.Multiply(n);
        public override Complex Product(Complex[] x)
        {
            double logMagnitude = 1;
            double argument = 0;

            foreach (Complex c in x)
            {
                double modulus = c.Modulus();
                if (modulus <= 0)
                {
                    return Complex.Zero;
                }
                logMagnitude += Math.Log(modulus);
                argument += c.Argument();
            }

            return Complex.FromPolar(Math.Exp(logMagnitude), argument % (2 * Math.PI));
        }
        public override Complex LogProduct(Complex[] x)
        {
            double logMagnitude = 1;
            double argument = 0;

            foreach (Complex c in x)
            {
                double modulus = c.Modulus();
                if (modulus <= 0)
                {
                    return Complex.Zero;
                }
                logMagnitude += Math.Log(modulus);
                argument += c.Argument();
            }

            return Complex.FromPolar(Math.Exp(logMagnitude), argument % (2 * Math.PI));
        }
        public override Complex Subtract(Complex u, Complex v) => u.Subtract(v);
        public override Complex Sqrt(Complex a) => Complex.Sqrt(a);
        public override Complex Pow(Complex a, int power) => Complex.Pow(a, power);
        public override Complex Pow(Complex a, long power) => Complex.Pow(a, power);
        public override Complex Negate(Complex a) => new Complex(-a.Real, -a.Imaginary);
        public override Complex FromInt(int n) => n;
        public override Complex FromLong(long n) => new Complex(n);
        public override Complex FromBigInt(System.Numerics.BigInteger n) => new Complex((double)n);

        public override bool IsRealAndNegative(Complex a) => false;
        public override bool InLeftHalfPlane(Complex a) => a.Real < 0;
        public override bool Equals(Complex a, Complex b) => a.Equals(b);
        public override bool ApproximatelyEquals(Complex a, Complex b, double eps) => a.ApproximatelyEquals(b, eps);

        public override void Givens(Complex a, Complex b, Complex[] cs)
        {
            double mod_a = a.Modulus(), mod_b = b.Modulus();
            if (mod_a > mod_b)
            {
                if (mod_a == 0)
                {
                    cs[0] = Complex.Zero;
                    cs[1] = Complex.Zero;
                }
                else
                {
                    double t = mod_b / mod_a, c = 1.0 / Math.Sqrt(1.0 + t * t);
                    cs[0] = new Complex(c, 0);
                    cs[1] = Complex.FromPolar(c * t, a.Argument() - b.Argument());
                }
            }
            else
            {
                if (mod_b == 0)
                {
                    cs[0] = Complex.Zero;
                    cs[1] = Complex.Zero;
                }
                else
                {
                    double t = mod_a / mod_b, s = 1.0 / Math.Sqrt(1.0 + t * t);
                    cs[0] = new Complex(s * t, 0); // c
                    cs[1] = Complex.FromPolar(s, a.Argument() - b.Argument());
                }
            }
        }

        public double Modulus(Complex z) => z.Modulus();
        public double Argument(Complex z) => z.Argument();
        public Complex Conjugate(Complex z) => z.Conjugate();
    }
}
