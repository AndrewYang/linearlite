﻿using LinearNet.Global;
using LinearNet.Helpers;
using LinearNet.Matrices;
using LinearNet.Providers.Basic.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Providers
{
    public class DecimalProvider : IProvider<decimal>, IRealProvider<decimal>
    {
        public override decimal One => 1.0M;
        public override decimal Zero => 0.0M;

        public override decimal Add(decimal a, decimal b) => a + b;
        public override decimal Divide(decimal a, decimal b) => a / b;
        public override decimal Multiply(decimal a, decimal b) => a * b;
        public override decimal MultiplyInteger(decimal a, int n) => a * n;
        public override decimal Product(decimal[] x)
        {
            int sign = 1;
            decimal logProduct = 0.0M;
            foreach (decimal d in x)
            {
                if (d == 0.0M)
                {
                    return 0.0M;
                }

                logProduct += DecimalMath.Log(Math.Abs(d));
                if (d < 0.0M)
                {
                    sign = -sign;
                }
            }
            return sign * DecimalMath.Exp(logProduct);
        }
        public override decimal LogProduct(decimal[] x)
        {
            decimal logProduct = 0.0M;
            foreach (decimal d in x)
            {
                if (d == 0.0M)
                {
                    return decimal.MinValue;
                }

                logProduct += DecimalMath.Log(Math.Abs(d));
            }
            return logProduct;
        }
        public override decimal Subtract(decimal a, decimal b) => a - b;
        public override decimal Sqrt(decimal a) => DecimalMath.Sqrt(a);
        public override decimal Pow(decimal a, int power) => DecimalMath.Pow(a, power);
        public override decimal Pow(decimal a, long power) => DecimalMath.Pow(a, power);
        public override decimal Negate(decimal a) => -a;
        public override decimal FromInt(int n) => n;
        public override decimal FromLong(long n) => n;
        public override decimal FromBigInt(BigInteger n) => (decimal)n;

        public override bool IsRealAndNegative(decimal a) => a < 0.0m;
        public override bool InLeftHalfPlane(decimal a) => a < 0.0m;
        public override bool Equals(decimal a, decimal b) => a == b;
        public override bool ApproximatelyEquals(decimal a, decimal b, double eps) => Util.ApproximatelyEquals(a, b, eps);

        public override void Givens(decimal a, decimal b, decimal[] cs)
        {
            if (Math.Abs(a) > Math.Abs(b))
            {
                if (a == 0)
                {
                    cs[0] = 0.0m;
                    cs[1] = 0.0m;
                }
                else
                {
                    decimal t = b / a;
                    decimal c = 1.0m / DecimalMath.Sqrt(1.0m + t * t);
                    cs[0] = c;
                    cs[1] = c * t; // s
                }
            }
            else
            {
                if (b == 0)
                {
                    cs[0] = 0.0m;
                    cs[1] = 0.0m;
                }
                else
                {
                    decimal t = a / b;
                    decimal s = 1.0m / DecimalMath.Sqrt(1.0m + t * t);
                    cs[0] = s * t; // c
                    cs[1] = s;
                }
            }
        }

        decimal IRealProvider<decimal>.Abs(decimal x) => Math.Abs(x);
        int IComparer<decimal>.Compare(decimal x, decimal y) => x.CompareTo(y);
    }
}
