﻿using LinearNet.Matrices;
using LinearNet.Providers.Basic.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Providers
{
    public class DoubleProvider : IProvider<double>, IRealProvider<double>
    {
        public override double One => 1.0;
        public override double Zero => 0.0;

        public override double Add(double a, double b) => a + b;
        public override double Divide(double a, double b) => a / b;
        public override double Multiply(double a, double b) => a * b;
        public override double MultiplyInteger(double a, int n) => a * n;
        public override double Product(double[] x)
        {
            int sign = 1;
            double logProduct = 0.0;
            foreach (double d in x)
            {
                if (d == 0.0)
                {
                    return 0.0;
                }

                logProduct += Math.Log(Math.Abs(d));
                if (d < 0.0)
                {
                    sign = -sign;
                }
            }
            return sign * Math.Exp(logProduct);
        }
        public override double LogProduct(double[] x)
        {
            double logProduct = 0.0;
            foreach (double d in x)
            {
                if (d == 0.0)
                {
                    return double.NegativeInfinity;
                }
                logProduct += Math.Log(Math.Abs(d));
            }
            return logProduct;
        }
        public override double Subtract(double a, double b) => a - b;
        public override double Sqrt(double a) => Math.Sqrt(a);
        public override double Pow(double a, int power) => Math.Pow(a, power);
        public override double Pow(double a, long power) => Math.Pow(a, power);
        public override double Negate(double a) => -a;
        public override double FromInt(int n) => n;
        public override double FromLong(long n) => n;
        public override double FromBigInt(BigInteger n) => (double)n;

        public override bool IsRealAndNegative(double a) => a < 0.0;
        public override bool InLeftHalfPlane(double a) => a < 0.0;
        public override bool Equals(double a, double b) => a == b;
        public override bool ApproximatelyEquals(double a, double b, double eps) => Util.ApproximatelyEquals(a, b, eps);

        public override void Givens(double a, double b, double[] cs)
        {
            if (Math.Abs(a) > Math.Abs(b))
            {
                if (a == 0)
                {
                    cs[0] = 0.0;
                    cs[1] = 0.0;
                }
                else
                {
                    double t = b / a;
                    double c = 1.0 / Math.Sqrt(1.0 + t * t);
                    cs[0] = c;
                    cs[1] = c * t; // s
                }
            }
            else
            {
                if (b == 0)
                {
                    cs[0] = 0.0;
                    cs[1] = 0.0;
                }
                else
                {
                    double t = a / b;
                    double s = 1.0 / Math.Sqrt(1.0 + t * t);
                    cs[0] = s * t; // c
                    cs[1] = s;
                }
            }
        }

        double IRealProvider<double>.Abs(double x) => Math.Abs(x);
        int IComparer<double>.Compare(double x, double y) => x.CompareTo(y);
    }
}
