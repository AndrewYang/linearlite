﻿using LinearNet.Matrices;
using LinearNet.Providers.Basic.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Providers
{
    /// <summary>
    /// This class is not a full implementation of BLAS. Only a few subroutines are 
    /// implemented and are designed to be used publicly for other methods. This library
    /// has full support of the BLAS collection of algorithms via the vector/matrix namespaces. 
    /// </summary>
    public class FloatProvider : IProvider<float>, IRealProvider<float>
    {
        public static Action<float[], double, int, int> dSCAL = (x, s, start, end) =>
        {
            float f = (float)s;
            for (int i = start; i < end; ++i)
            {
                x[i] *= f;
            }
        };
        
        public override float One => 1.0f;
        public override float Zero => 0.0f;

        public override float Add(float a, float b) => a + b;
        public override float Divide(float a, float b) => a / b;
        public override float Multiply(float a, float b) => a * b;
        public override float MultiplyInteger(float a, int n) => a * n;
        public override float Product(float[] x)
        {
            int sign = 1;
            double logProduct = 0.0;
            foreach (float d in x)
            {
                if (d == 0.0f)
                {
                    return 0.0f;
                }

                logProduct += Math.Log(Math.Abs(d));
                if (d < 0.0)
                {
                    sign = -sign;
                }
            }
            return sign * (float)Math.Exp(logProduct);
        }
        public override float LogProduct(float[] x)
        {
            float logProduct = 0.0f;
            foreach (float d in x)
            {
                if (d == 0.0f)
                {
                    return float.NegativeInfinity;
                }
                logProduct += (float)Math.Log(Math.Abs(d));
            }
            return logProduct;
        }
        public override float Subtract(float a, float b) => a - b;
        public override float Sqrt(float a) => (float)Math.Sqrt(a);
        public override float Pow(float a, int power) => (float)Math.Pow(a, power);
        public override float Pow(float a, long power) => (float)Math.Pow(a, power);
        public override float Negate(float a) => -a;
        public override float FromInt(int n) => n;
        public override float FromLong(long n) => n;
        public override float FromBigInt(BigInteger n) => (float)n;

        public override bool IsRealAndNegative(float a) => a < 0.0f;
        public override bool InLeftHalfPlane(float a) => a < 0.0f;
        public override bool Equals(float a, float b) => a == b;
        public override bool ApproximatelyEquals(float a, float b, double eps) => Util.ApproximatelyEquals(a, b, eps);

        public override void Givens(float a, float b, float[] cs)
        {
            if (Math.Abs(a) > Math.Abs(b))
            {
                if (a == 0)
                {
                    cs[0] = 0.0f;
                    cs[1] = 0.0f;
                }
                else
                {
                    float t = b / a;
                    float c = (float)(1.0 / Math.Sqrt(1.0 + t * t));
                    cs[0] = c;
                    cs[1] = c * t; // s
                }
            }
            else
            {
                if (b == 0)
                {
                    cs[0] = 0.0f;
                    cs[1] = 0.0f;
                }
                else
                {
                    float t = a / b;
                    float s = (float)(1.0 / Math.Sqrt(1.0 + t * t));
                    cs[0] = s * t; // c
                    cs[1] = s;
                }
            }
        }

        float IRealProvider<float>.Abs(float x) => Math.Abs(x);
        int IComparer<float>.Compare(float x, float y) => x.CompareTo(y);
    }
}
