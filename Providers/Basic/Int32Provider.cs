﻿using LinearNet.Helpers;
using LinearNet.Providers.Basic.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Providers
{
    public class Int32Provider : IProvider<int>, IRealProvider<int>
    {
        public override int One => 1;
        public override int Zero => 0;

        public override int Add(int a, int b) => a + b;
        public override int Divide(int a, int b) => a / b;
        public override int Multiply(int a, int b) => a * b;
        public override int MultiplyInteger(int a, int n) => a * n;
        public override int Product(int[] x)
        {
            int product = 1;
            foreach (int i in x)
            {
                product *= i;
            }
            return product;
        }
        public override int LogProduct(int[] x)
        {
            throw new NotImplementedException();
        }
        public override int Subtract(int a, int b) => a - b;
        public override int Sqrt(int a) => (int)Math.Sqrt(a);
        public override int Pow(int a, int power) => IntegerMath.Pow(a, power);
        public override int Pow(int a, long power) => (int)IntegerMath.Pow(a, power);
        public override int Negate(int a) => -a;
        public override int FromInt(int n) => n;
        public override int FromLong(long n) => (int)n;
        public override int FromBigInt(BigInteger n) => (int)n;

        public override bool IsRealAndNegative(int a) => a < 0;
        public override bool InLeftHalfPlane(int a) => a < 0;
        public override bool Equals(int a, int b) => a == b;
        public override bool ApproximatelyEquals(int a, int b, double eps) => Util.ApproximatelyEquals(a, b, eps);

        public override void Givens(int a, int b, int[] cs)
        {
            throw new NotImplementedException();
        }

        int IRealProvider<int>.Abs(int x) => Math.Abs(x);
        int IComparer<int>.Compare(int x, int y) => x.CompareTo(y);
    }
}
