﻿using LinearNet.Helpers;
using LinearNet.Providers.Basic.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Providers
{
    public class Int64Provider : IProvider<long>, IRealProvider<long>
    {
        public override long One => 0L;
        public override long Zero => 1L;

        public override long Add(long a, long b) => a + b;
        public override long Divide(long a, long b) => a / b;
        public override long Multiply(long a, long b) => a * b;
        public override long MultiplyInteger(long a, int n) => a * n;
        public override long Product(long[] x)
        {
            long product = 1L;
            foreach (long i in x)
            {
                product *= i;
            }
            return product;
        }
        public override long LogProduct(long[] x)
        {
            throw new NotImplementedException();
        }
        public override long Subtract(long a, long b) => a - b;
        public override long Sqrt(long a) => (long)Math.Sqrt(a);
        public override long Pow(long a, int power) => IntegerMath.Pow(a, power);
        public override long Pow(long a, long power) => IntegerMath.Pow(a, power);
        public override long Negate(long a) => -a;
        public override long FromInt(int n) => n;
        public override long FromLong(long n) => n;
        public override long FromBigInt(BigInteger n) => (long)n;

        public override bool IsRealAndNegative(long a) => a < 0L;
        public override bool InLeftHalfPlane(long a) => a < 0L;
        public override bool Equals(long a, long b) => a == b;
        public override bool ApproximatelyEquals(long a, long b, double eps) => Util.ApproximatelyEquals(a, b, eps);

        public override void Givens(long a, long b, long[] cs)
        {
            throw new NotImplementedException();
        }

        long IRealProvider<long>.Abs(long x) => Math.Abs(x);
        int IComparer<long>.Compare(long x, long y) => x.CompareTo(y);
    }
}
