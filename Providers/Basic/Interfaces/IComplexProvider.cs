﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LinearNet.Providers.Basic.Interfaces
{
    public interface IComplexProvider<TReal, TComplex> where TReal : IComparable<TReal>
    {
        /// <summary>
        /// Returns the modulus of a complex type
        /// </summary>
        /// <returns></returns>
        TReal Modulus(TComplex z);

        /// <summary>
        /// Returns the argument of a complex type
        /// </summary>
        /// <param name="z"></param>
        /// <returns></returns>
        TReal Argument(TComplex z);

        /// <summary>
        /// Returns the complex conjugate of a complex number
        /// </summary>
        /// <param name="z"></param>
        /// <returns></returns>
        TComplex Conjugate(TComplex z);

    }
}
