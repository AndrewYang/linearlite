﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace LinearNet.Providers
{
    public interface IIntegerProvider<T>
    {
        /// <summary>
        /// Returns a % b
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        T Mod(T a, T b);

        /// <summary>
        /// Returns the greatest common divisor (highest common factor) of a, b
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        T GCD(T a, T b);
    }
}
