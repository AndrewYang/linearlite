﻿using LinearNet.Structs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Providers
{
    /// <summary>
    /// Generic provider class for access to commonly used non-linear algebra elements 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class IProvider<T>
    {
        public abstract T One { get; }
        public abstract T Zero { get; }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public abstract T Add(T a, T b);
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public abstract T Subtract(T a, T b);
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public abstract T Multiply(T a, T b);
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public abstract T MultiplyInteger(T a, int n);
        /// <summary>
        /// Numerically stable multiplication of a large number of elements
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public abstract T Product(T[] x);
        public abstract T LogProduct(T[] x);
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public abstract T Divide(T a, T b);
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public abstract T Sqrt(T a);
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public abstract T Pow(T a, int power);
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public abstract T Pow(T a, long power);

        /// <summary>
        /// Get the equivalent of the n-th ordinal in this algebra
        /// </summary>
        /// <param name="n"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public abstract T FromInt(int n);
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public abstract T FromLong(long n);
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public abstract T FromBigInt(BigInteger n);

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public abstract T Negate(T a);
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public abstract bool IsRealAndNegative(T a); // i.e. real and negative
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public abstract bool InLeftHalfPlane(T a); // i.e. negative or real part negative
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public abstract bool Equals(T a, T b);
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public abstract bool ApproximatelyEquals(T a, T b, double eps);

        public abstract void Givens(T a, T b, T[] cs);
    }
}
