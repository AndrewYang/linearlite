﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LinearNet.Providers.Basic.Interfaces
{
    public interface IRealProvider<T> : IComparer<T>
    {
        T Zero { get; }

        T Add(T a, T b);
        T Divide(T a, T b);
        T Multiply(T a, T b);
        T Abs(T x);
        T Sqrt(T x);
        T Subtract(T a, T b);
    }
}
