﻿using LinearNet.Structs;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Providers
{
    public class RingProvider<T> : IProvider<T> where T : IRing<T>, new()
    {
        public override T One => new T().MultiplicativeIdentity;
        public override T Zero => new T().AdditiveIdentity;

        public override T Add(T a, T b) => a.Add(b);
        public override T Divide(T a, T b) => throw new NotImplementedException();
        public override T Multiply(T a, T b) => a.Multiply(b);
        public override T MultiplyInteger(T a, int n)
        {
            if (n < 0)
            {
                return Negate(MultiplyInteger(a, -n));
            }
            if (n == 0)
            {
                return One;
            }
            if (n == 1)
            {
                return a;
            }

            T half_an = MultiplyInteger(a, n / 2);
            if (n % 2 == 0)
            {
                return half_an.Add(half_an);
            }
            return half_an.Add(half_an).Add(a);
        }
        public override T Product(T[] x)
        {
            T product = One;
            foreach (T e in x)
            {
                product = product.Multiply(e);
            }
            return product;
        }
        public override T LogProduct(T[] x)
        {
            throw new NotImplementedException();
        }
        public override T Subtract(T a, T b) => a.Subtract(b);
        public override T Sqrt(T a) => throw new NotImplementedException();
        public override T Negate(T a) => a.AdditiveInverse();
        public override T Pow(T a, int power)
        {
            if (power < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(power));
            }

            if (power == 0)
            {
                return One;
            }

            if (power == 1)
            {
                return a;
            }

            T sqrt = Pow(a, power / 2);
            if (power % 2 == 0)
            {
                return sqrt.Multiply(sqrt);
            }
            return sqrt.Multiply(sqrt).Multiply(a);
        }
        public override T Pow(T a, long power)
        {
            if (int.MinValue <= power && power <= int.MaxValue)
            {
                return Pow(a, (int)power);
            }
            return PowInner(a, power);
        }

        private T PowInner(T a, long power)
        {
            if (power < 0L)
            {
                return Divide(One, PowInner(a, -power));
            }

            if (power == 0L)
            {
                return One;
            }

            if (power == 1L)
            {
                return a;
            }

            T sqrt = PowInner(a, power / 2L);
            if (power % 2L == 0L)
            {
                return sqrt.Multiply(sqrt);
            }
            return sqrt.Multiply(sqrt).Multiply(a);
        }

        public override T FromInt(int n)
        {
            if (n < 0)
            {
                return Negate(FromInt(-n));
            }
            if (n == 0)
            {
                return Zero;
            }
            if (n == 1)
            {
                return One;
            }

            T half = FromInt(n / 2);
            if (n % 2 == 0)
            {
                return half.Add(half);
            }
            return half.Add(half).Add(One);
        }
        public override T FromLong(long n)
        {
            if (int.MinValue <= n && n <= int.MaxValue)
            {
                return FromInt((int)n);
            }
            return FromLongInner(n);
        }
        public override T FromBigInt(BigInteger n)
        {
            // int checks are handled from within FromLong(.)
            if (long.MinValue <= n && n <= long.MaxValue)
            {
                return FromLong((long)n);
            }
            return FromBigIntInner(n);
        }

        private T FromLongInner(long n)
        {
            if (n < 0)
            {
                return Negate(FromLongInner(-n));
            }
            if (n == 0)
            {
                return Zero;
            }
            if (n == 1)
            {
                return One;
            }

            T half = FromLongInner(n / 2);
            if (n % 2 == 0L)
            {
                return half.Add(half);
            }
            return half.Add(half).Add(One);
        }
        private T FromBigIntInner(BigInteger n)
        {
            if (n < 0)
            {
                return Negate(FromBigIntInner(-n));
            }
            if (n == 0)
            {
                return Zero;
            }
            if (n == 1)
            {
                return One;
            }

            T half = FromBigIntInner(n / 2);
            if (n % 2L == 0)
            {
                return half.Add(half);
            }
            return half.Add(half).Add(One);
        }

        public override bool IsRealAndNegative(T a) => throw new NotImplementedException();
        public override bool InLeftHalfPlane(T a) => throw new NotImplementedException();
        public override bool Equals(T a, T b) => a.Equals(b);
        public override bool ApproximatelyEquals(T a, T b, double eps) => a.ApproximatelyEquals(b, eps);

        public override void Givens(T a, T b, T[] cs)
        {
            throw new NotImplementedException();
        }
    }
}
