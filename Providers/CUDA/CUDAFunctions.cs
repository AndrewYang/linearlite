﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LinearNet.Providers.CUDA
{
    internal static class CudaFunctions
    {
        /// <summary>
        /// Row-major flatten matrix -> vector
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="matrix"></param>
        /// <returns></returns>
        internal static T[] Flatten<T>(T[][] matrix)
        {
            int rows = matrix.Length, columns = matrix[0].Length;
            T[] flat = new T[rows * columns];

            int k = 0;
            for (int i = 0; i < rows; ++i)
            {
                T[] row = matrix[i];
                for (int j = 0; j < columns; ++j)
                {
                    flat[k++] = row[j];
                }
            }
            return flat;
        }
        internal static T[] Flatten<T>(T[][] matrix, int row_start, int col_start, int rows, int columns)
        {
            int row_end = row_start + rows;
            int col_end = col_start + columns;
            T[] flat = new T[rows * columns];

            int k = 0;
            for (int i = row_start; i < row_end; ++i)
            {
                T[] row = matrix[i];
                for (int j = col_start; j < col_end; ++j)
                {
                    flat[k++] = row[j];
                }
            }
            return flat;
        }
        internal static void FlattenSyscpy<T>(T[][] matrix, T[] flat)
        {
            // only works for value types
            int rows = matrix.Length,
                columns = matrix[0].Length,
                start = 0;

            for (int i = 0; i < rows; ++i)
            {
                Array.Copy(matrix[i], 0, flat, start, columns);
                start += columns;
            }
        }
        internal static void FlattenSyscpy<T>(T[][] matrix, T[] flat, int row_start, int col_start, int rows, int columns)
        {
            int row_end = row_start + rows;
            int col_end = col_start + columns;

            int i, j, k = 0;
            for (i = row_start; i < row_end; ++i)
            {
                T[] row = matrix[i];
                for (j = col_start; j < col_end; ++j)
                {
                    flat[k++] = row[j];
                }
            }
        }

        /// <summary>
        /// Row-major copy vector -> matrix
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="src"></param>
        /// <param name="dest"></param>
        internal static void CopyToMatrix<T>(T[] src, T[][] dest)
        {
            int rows = dest.Length, columns = dest[0].Length;
            int k = 0;
            for (int i = 0; i < rows; ++i)
            {
                T[] row = dest[i];
                for (int j = 0; j < columns; ++j)
                {
                    row[j] = src[k++];
                }
            }
        }
        internal static void CopyToMatrix<T>(T[] src, T[][] dest, int row_start, int col_start, int rows, int columns)
        {
            int row_end = row_start + rows,
                col_end = col_start + columns;

            int k = 0;
            for (int i = row_start; i < row_end; ++i)
            {
                T[] row = dest[i];
                for (int j = col_start; j < col_end; ++j)
                {
                    row[j] = src[k++];
                }
            }
        }

        /// <summary>
        /// </summary>
        /// <param name="exitcode"></param>
        /// <returns>Whether the call was successful</returns>
        internal static bool InterpretCudaExitCode(int exitcode)
        {
            if (exitcode == CudaExitCodes.SUCCESS) return true;

            if (exitcode == CudaExitCodes.ERR_SET_DEVICE)
            {
                throw new NotSupportedException("Unable to find and set CUDA-supported device.");
            }
            if (exitcode == CudaExitCodes.ERR_MALLOC)
            {
                throw new OutOfMemoryException("Unable to allocate memory on compute device.");
            }
            if (exitcode == CudaExitCodes.ERR_MEMCPY)
            {
                throw new InvalidOperationException("Unable to copy between host memory and device memory.");
            }
            if (exitcode == CudaExitCodes.ERR_KERNEL)
            {
                throw new InvalidOperationException("Error encountered while executing the device kernel.");
            }
            if (exitcode == CudaExitCodes.ERR_HANDLE_CREATE)
            {
                throw new InvalidOperationException("Unable to create handle.");
            }
            if (exitcode == CudaExitCodes.ERR_HANDLE_DESTROY)
            {
                throw new InvalidOperationException("Unable to destroy handle.");
            }
            if (exitcode == CudaExitCodes.ERR_SYNC)
            {
                throw new InvalidOperationException("Unable to synchronize device.");
            }

            return false;
        }
    }
}
