﻿using LinearNet.Matrices;
using LinearNet.Structs;
using System;
using System.Collections.Generic;
using System.Text;

namespace LinearNet.Providers.CUDA
{
    public class CudaComplexProvider : IDenseBLAS3<Complex>
    {
        public void Multiply(Complex[][] A, Complex[][] B, Complex[][] result, Size3 size, bool increment)
        {
            throw new NotImplementedException();
        }

        public void Multiply(Complex[][] A, Complex[][] B, Complex[][] result, int A_start_row, int A_start_col, int B_start_row, int B_start_col, int C_start_row, int C_start_col, Size3 size, bool increment)
        {
            throw new NotImplementedException();
        }
    }
}
