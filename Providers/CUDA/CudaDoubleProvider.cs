﻿using LinearNet.Providers.LinearAlgebra;
using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using LinearNet.Matrices;
using LinearNet.Global;

namespace LinearNet.Providers.CUDA
{
    /// <summary>
    /// GPU-accelerated level 1 and 2 dense BLAS implementation using CUDA, for type <txt>double</txt>.
    /// </summary>
    /// <cat>linear-algebra</cat>
    public sealed class CudaDoubleProvider : IDenseBLAS1<double>, IDenseBLAS3<double>
    {
        private readonly uint _blockSize;

        private DoubleProvider _provider;

        public IProvider<double> Provider => _provider;
        public uint BlockSize { get { return _blockSize; } }

        public CudaDoubleProvider(uint blockSize = 32)
        {
            if (blockSize <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(blockSize), ExceptionMessages.NonPositiveNotAllowed);
            }

            _provider = new DoubleProvider();
            _blockSize = blockSize;
        }

        [DllImport("matrixMulCUBLAS.dll")]
        private static extern int gemm_float64(int devID, double[] A, double[] B, uint m, uint n, uint p, double alpha, double beta, uint block_size, bool store_result_in_A);
        [DllImport("Linear.Net.CUDA.dll")]
        internal static extern int axpy_float64(double[] y, double[] x, double alpha, int start, int end);
        [DllImport("Linear.Net.CUDA.dll")]
        internal static extern int scal_float64(double[] x, double scalar, int start, int end);

        // TODO: move this to own multiplication class.
        #region BLAS 3

        void IMatrixMultiplication<double>.Multiply(double[][] A, double[][] B, double[][] result, Size3 size, bool increment)
        {
            int A_len = size.M * size.N,
                B_len = size.N * size.P,
                C_len = size.M * size.P;

            // Make sure that A or B is long enough to store C.
            bool store_result_in_A = A_len >= B_len;
            if (store_result_in_A)
            {
                A_len = Math.Max(A_len, C_len);
            }
            else
            {
                B_len = Math.Max(B_len, C_len);
            }

            double[] flat_A = new double[A_len], flat_B = new double[B_len];
            CudaFunctions.FlattenSyscpy(A, flat_A);
            CudaFunctions.FlattenSyscpy(B, flat_B);

            int exitcode;
            if (increment)
            {
                exitcode = gemm_float64(0, flat_A, flat_B, (uint)size.M, (uint)size.N, (uint)size.P, 1.0, 1.0, _blockSize, store_result_in_A);
            }
            else
            {
                exitcode = gemm_float64(0, flat_A, flat_B, (uint)size.M, (uint)size.N, (uint)size.P, 1.0, 0.0, _blockSize, store_result_in_A);
            }

            CudaFunctions.InterpretCudaExitCode(exitcode);

            // The C matrix will be either stored in A or B.
            if (store_result_in_A)
            {
                CudaFunctions.CopyToMatrix(flat_A, result);
            }
            else
            {
                CudaFunctions.CopyToMatrix(flat_B, result);
            }
        }
        void IMatrixMultiplication<double>.Multiply(double[][] A, double[][] B, double[][] result, int A_start_row, int A_start_col, int B_start_row, int B_start_col, int C_start_row, int C_start_col, Size3 size, bool increment)
        {
            int A_len = size.M * size.N,
                B_len = size.N * size.P,
                C_len = size.M * size.P;

            // Make sure that A or B is long enough to store C.
            bool store_result_in_A = A_len >= B_len;
            if (store_result_in_A)
            {
                A_len = Math.Max(A_len, C_len);
            }
            else
            {
                B_len = Math.Max(B_len, C_len);
            }

            double[] flat_A = new double[A_len], flat_B = new double[B_len];
            CudaFunctions.FlattenSyscpy(A, flat_A, A_start_row, A_start_col, size.M, size.N);
            CudaFunctions.FlattenSyscpy(B, flat_B, B_start_row, B_start_col, size.N, size.P);

            int exitcode;
            if (increment)
            {
                exitcode = gemm_float64(0, flat_A, flat_B, (uint)size.M, (uint)size.N, (uint)size.P, 1.0, 1.0, _blockSize, store_result_in_A);
            }
            else
            {
                exitcode = gemm_float64(0, flat_A, flat_B, (uint)size.M, (uint)size.N, (uint)size.P, 1.0, 0.0, _blockSize, store_result_in_A);
            }

            CudaFunctions.InterpretCudaExitCode(exitcode);

            // The C matrix will be either stored in A or B.
            if (store_result_in_A)
            {
                CudaFunctions.CopyToMatrix(flat_A, result, C_start_row, C_start_col, size.M, size.P);
            }
            else
            {
                CudaFunctions.CopyToMatrix(flat_B, result, C_start_row, C_start_col, size.M, size.P);
            }
        }

        #endregion

        #region BLAS 1

        void IDenseBLAS1<double>.ADD(double[] u, double[] v, double[] sum, int start, int end)
        {
            throw new NotImplementedException();
        }
        void IDenseBLAS1<double>.SUB(double[] u, double[] v, double[] diff, int start, int end)
        {
            throw new NotImplementedException();
        }
        void IDenseBLAS1<double>.AXPY(double[] y, double[] x, double alpha, int start, int end)
        {
            CudaFunctions.InterpretCudaExitCode(axpy_float64(y, x, alpha, start, end));
        }
        void IDenseBLAS1<double>.AXPY(double[] y, double[] x, double alpha, int y_start, int y_end, int xy_offset)
        {
            throw new NotImplementedException();
        }
        double IDenseBLAS1<double>.DOT(double[] u, double[] v, int start, int end)
        {
            throw new NotImplementedException();
        }
        double IDenseBLAS1<double>.DOT(double[] u, double[] v, int u_start, int u_end, int v_u_offset)
        {
            throw new NotImplementedException();
        }
        void IDenseBLAS1<double>.SCAL(double[] x, double s, int start, int end)
        {
            CudaFunctions.InterpretCudaExitCode(scal_float64(x, s, start, end));
        }
        double IDenseBLAS1<double>.XTA(double[] x, double[][] A, int column, int start, int end)
        {
            throw new NotImplementedException();
        }
        double IDenseBLAS1<double>.XTA(double[] x, double[][] A, int column, int x_start, int x_end, int Ax_offset)
        {
            throw new NotImplementedException();
        }

        void IDenseBLAS1<double>.ROTR(double[][] A, int i, int j, double c, double s, int colStart, int colEnd)
        {
            throw new NotImplementedException();
        }
        void IDenseBLAS1<double>.ROTC(double[][] A, int i, int j, double c, double s, int rowStart, int rowEnd)
        {
            throw new NotImplementedException();
        }

        void IDenseBLAS1<double>.PMULT(double[] u, double[] v, double[] uv, int u_start, int u_end, int v_u_offset, int uv_u_offset)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
