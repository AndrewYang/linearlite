﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LinearNet.Providers.CUDA
{
    internal static class CudaExitCodes
    {
        internal const int 
            SUCCESS             = 0,
            ERR_SET_DEVICE      = 1,
            ERR_MALLOC          = 2,
            ERR_MEMCPY          = 3,
            ERR_HANDLE_CREATE   = 4,
            ERR_KERNEL          = 5,
            ERR_SYNC            = 6,
            ERR_HANDLE_DESTROY  = 7;
    }
}
