﻿using LinearNet.Matrices;
using LinearNet.Providers;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Providers
{
    /// <summary>
    /// <p>Interface for all dense level-1 BLAS (Basic Linear Algebra Subroutine) implementations.</p>
    /// <p>
    /// This public interface holds no methods, it is inherited by IDenseBLAS1Internal which is 
    /// implemented by all actual implementations internally. 
    /// </p>
    /// <p>
    /// The purpose of this patten is to hide the low-level BLAS while still allowing the user to 
    /// publically reference IDenseBLAS1Provider&ltT&gt.
    /// </p>
    /// </summary>
    /// <cat>linear-algebra</cat>
    public interface IDenseBLAS1Provider<T>
    {

    }
    public interface IDenseBLAS1<T> : IDenseBLAS1Provider<T>
    {
        IProvider<T> Provider { get; }

        /// <summary>
        /// ADD: sum <- u + v, for dense vectors u, v
        /// Add the two vectors u and v, storing the result in sum
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        void ADD(T[] u, T[] v, T[] sum, int start, int end);

        /// <summary>
        /// SUB: diff <- u - v, for dense vectors u, v
        /// Subtract the 2nd vector from the 1st vector, storing the result in diff.
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        void SUB(T[] u, T[] v, T[] diff, int start, int end);

        /// <summary>
        /// AXPY: y <- y + alpha * x, for dense vectors x and y.
        /// Add scalar multiple of vector x to vector y, storing the result in y.
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        void AXPY(T[] y, T[] x, T alpha, int start, int end);

        /// <summary>
        /// y <- y + alpha * x, for y in [y_start, y_end) and x in [y_start + xy_offset, y_end + xy_offset)
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        void AXPY(T[] y, T[] x, T alpha, int y_start, int y_end, int xy_offset);

        /// <summary>
        /// Calculate and return the dot product of dense vectors u and v. The vectors themselves are unchanged.
        /// The dot product will only include summands from indices 'start' (inclusive) to 'end' (exclusive)
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        T DOT(T[] u, T[] v, int start, int end);

        /// <summary>
        /// v_u_offset = v (start) - u (start).
        /// </summary>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        T DOT(T[] u, T[] v, int u_start, int u_end, int v_u_offset);


        /// <summary>
        /// SCAL: x <- x * s for a dense vector x.
        /// Scale the vector x by a scalar s, rewriting x, in the interval [start, end). The rest 
        /// of the vector will remain unchanged.
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        void SCAL(T[] x, T s, int start, int end);

        /// <summary>
        /// XTA: amult <- x^T . A[column]
        /// Calculates a vector multiplied by a column of a matrix
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        T XTA(T[] x, T[][] A, int column, int start, int end);

        /// <summary>
        /// XTA: amult <- x^T . A[column], where x in [x_start, x_end) and 
        /// the column of A has row-indices in [x_start + Ax_offset, x_end + Ax_offset)
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        T XTA(T[] x, T[][] A, int column, int x_start, int x_end, int Ax_offset);

        /// <summary>
        /// Givens rotate row i and row j of matrix A, using the rotation matrix (c, s, -s, c), for all entries 
        /// whose column indices lie in the range [colStart, colEnd).
        /// </summary>
        /// <param name="A"></param>
        /// <param name="i"></param>
        /// <param name="j"></param>
        /// <param name="c"></param>
        /// <param name="s"></param>
        /// <param name="colStart"></param>
        /// <param name="colEnd"></param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        void ROTR(T[][] A, int i, int j, T c, T s, int colStart, int colEnd);

        /// <summary>
        /// Givens rotate column i and column j of matrix A, using the rotation matrix (c, s, -s, c), for all
        /// entries whose row indices lie in the range [rowStart, rowEnd).
        /// </summary>
        /// <param name="A">m x n matrix</param>
        /// <param name="i">The 1st column index to rotate, must be in [0, n)</param>
        /// <param name="j">The 2nd column index to rotate, must be in [0, n)</param>
        /// <param name="c">Entry 'c' of the rotation matrix</param>
        /// <param name="s">Entry 's' of the rotation matrix</param>
        /// <param name="rowStart">Rotations for each of the columns begin at this row index (inclusive)</param>
        /// <param name="rowEnd">Rotations for each of the columns end at this row index (exclusive)</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        void ROTC(T[][] A, int i, int j, T c, T s, int rowStart, int rowEnd);

        /// <summary>
        /// Pointwise multiply two vectors u and v, then increment 'uv' by the elementwise product.
        /// uv += u * v.
        /// </summary>
        /// <param name="u">The first vector to pointwise-multiply</param>
        /// <param name="v">The second vector to pointwise-multiply</param>
        /// <param name="uv">The vector storing the product.</param>
        /// <param name="u_start">The index of the first entry (inclusive) in 'u' to multiply.</param>
        /// <param name="u_end">The index of the last entry (exclusive) in 'u' to multiply.</param>
        /// <param name="v_u_offset">The first entry of v to multiply, subtract u_start.</param>
        /// <param name="uv_u_offset">The firts entry of uv to write to, subtract u_start.</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        void PMULT(T[] u, T[] v, T[] uv, int u_start, int u_end, int v_u_offset, int uv_u_offset);
    }
}
