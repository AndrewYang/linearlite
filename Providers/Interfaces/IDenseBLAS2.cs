﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace LinearNet.Providers
{
    public interface IDenseBLAS2Provider<T>
    {

    }
    internal interface IDenseBLAS2<T> : IDenseBLAS2Provider<T>
    {
        IProvider<T> Provider { get; }

        /// <summary>
        /// Sets C[0, 0] = A[a_row_start, a_col_start] + B[b_row_start, b_col_start] for a (rows x cols) submatrix
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        void Add(T[][] A, T[][] B, T[][] C, int a_row_start, int a_col_start, int b_row_start, int b_col_start, int rows, int cols);

        /// <summary>
        /// Sets C[0, 0] = A[a_row_start, a_col_start] - B[b_row_start, b_col_start] for a (rows x cols) submatrix
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        void Subtract(T[][] A, T[][] B, T[][] C, int a_row_start, int a_col_start, int b_row_start, int b_col_start, int rows, int cols);

        /// <summary>
        /// A[0 : rows, 0 : cols] += B[0 : rows, 0 : cols] for a (rows x cols) submatrix.
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        void Increment(T[][] A, T[][] B, int rows, int cols);
        /// <summary>
        /// Perform A[a_row_start, a_col_start] += B[0, 0] for a (rows x cols) submatrix.
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        void Increment(T[][] A, T[][] B, int a_row_start, int a_col_start, int rows, int cols);

        /// <summary>
        /// A[0 : rows, 0 : cols] -= B[0 : rows, 0 : cols] for a (rows x cols) submatrix.
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        void Decrement(T[][] A, T[][] B, int rows, int cols);
        /// <summary>
        /// Perform A[a_row_start, a_col_start] -= B[0, 0] for a (rows x cols) submatrix.
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        void Decrement(T[][] A, T[][] B, int a_row_start, int a_col_start, int rows, int cols);

        /// <summary>
        /// Sets the region in matrix 'result' bounded by the submatrix 
        /// [result_row_start, result_row_start + rows) x [result_col_start, result_col_start + cols)
        /// 
        /// with the negation of the region in matrix 'A', bounded by the submatrix
        /// [a_row_start, a_row_start + rows) x [a_col_start, a_col_start + cols)
        /// 
        /// </summary>
        /// <param name="A">The matrix to negate and copy from (src)</param>
        /// <param name="result">The matrix to negate and copy to (dest)</param>
        /// <param name="a_row_start">The top boundary of the submatrix of A</param>
        /// <param name="a_col_start">The left boundary of the submatrix of A</param>
        /// <param name="result_row_start">The top boundary of the submatrix of 'result'</param>
        /// <param name="result_col_start">The left boundary of the submatrix of 'result'</param>
        /// <param name="rows">The number of rows to negate + copy</param>
        /// <param name="cols">The number of cols to negate + copy</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        void Negate(T[][] A, T[][] result, int a_row_start, int a_col_start, int result_row_start, int result_col_start, int rows, int cols);

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        void Copy(T[][] dest, T[][] src, int rows, int cols);

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        void Copy(T[][] dest, T[][] src, int src_row_start, int src_col_start, int rows, int cols);

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        void Copy(T[][] dest, T[][] src, int dest_row_start, int dest_col_start, int src_row_start, int src_col_start, int rows, int cols);
    }
}
