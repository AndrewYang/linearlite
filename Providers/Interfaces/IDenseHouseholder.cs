﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace LinearNet.Providers
{
    /// <summary>
    /// Interface for Householder transforms
    /// </summary>
    /// <typeparam name="T"></typeparam>
    internal interface IDenseHouseholder<T>
    {
        IProvider<T> Provider { get; }

        /// <summary>
        /// Calculate the Householder reflection of a submatrix of the matrix A, premultiplying 
        /// the matrix H by the Householder reflection matrix used.
        /// 
        /// The submatrix taken from A is defined by [rowStart : rowEnd) x [colStart : colEnd)
        /// 
        /// The vector used for the reflection is defined as either a row or column vector from 
        /// matrix A, depending on whether 'isColumn' is set to true.
        /// 
        /// If a column vector is used, the reflection vector is a column vector from A in the 
        /// column 'colStart', in the row indices [rowStart, rowEnd).
        /// 
        /// If a row vector is used, the reflection vector is a row vector from A in the row 
        /// 'rowStart', in the column indices [colStart, colEnd).
        /// 
        /// The specified row/column will be zeroed in all indices except the first index, i.e. 
        /// (rowStart, colStart), in matrix A. 
        /// </summary>
        /// <param name="A"></param>
        /// <param name="H"></param>
        /// <param name="v"></param>
        /// <param name="w"></param>
        /// <param name="colStart"></param>
        /// <param name="colEnd"></param>
        /// <param name="rowStart"></param>
        /// <param name="rowEnd"></param>
        /// <param name="isColumn"></param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        void HouseholderTransform(T[][] A, T[][] H, T[] v, T[] w, int colStart, int colEnd, int rowStart, int rowEnd, bool isColumn);

        /// <summary>
        /// Calculate the Householder reflection of a submatrix of A, without calculating the 
        /// orthogonal matrix.
        /// </summary>
        /// <param name="A"></param>
        /// <param name="v"></param>
        /// <param name="w"></param>
        /// <param name="colStart"></param>
        /// <param name="colEnd"></param>
        /// <param name="rowStart"></param>
        /// <param name="rowEnd"></param>
        /// <param name="isColumn"></param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        void HouseholderTransform(T[][] A, T[] v, T[] w, int colStart, int colEnd, int rowStart, int rowEnd, bool isColumn);

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        void HouseholderTransformParallel(T[][] A, T[][] H, T[] v, T[] w, int colStart, int colEnd, int rowStart, int rowEnd, bool isColumn);

    }
}
