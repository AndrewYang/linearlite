﻿using LinearNet.Structs;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace LinearNet.Providers
{
    public interface ISparseBLAS1Provider<T>
    {

    }
    public interface ISparseBLAS1<T> : ISparseBLAS1Provider<T> where T : new()
    {
        IProvider<T> Provider { get; }

        /// <summary>
        /// z <- x + y
        /// </summary>
        /// <param name="xi">The non-zero indices of x</param>
        /// <param name="x">The non-zero values of x (same length as xi)</param>
        /// <param name="yi">The non-zero indices of y</param>
        /// <param name="y">The non-zero values of y (same length as yi)</param>
        /// <param name="zi">The non-zero indices of z (will be populated)</param>
        /// <param name="z">The non-zero values of z (will be populated)</param>
        /// <param name="x_start_index">The index at which to start adding x</param>
        /// <param name="y_start_index">The index at which to start adding y</param>
        /// <param name="z_start_index">The index at which to start populating z</param>
        /// <param name="x_end_index">The index at which to stop adding x</param>
        /// <param name="y_end_index">The index at which to stop adding y</param>
        /// <returns>The index at which we stopped adding z</returns>
        int ADD(int[] xi, T[] x, int[] yi, T[] y, int[] zi, T[] z, int x_start_index, int y_start_index, int z_start_index, int x_end_index, int y_end_index);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="yi">The nz indices in y, in a contingous block of length nnz (returned)</param>
        /// <param name="y">The y workspace, of size |y|</param>
        /// <param name="xi">The nz indices of x, in a contingous block</param>
        /// <param name="x">The nz values of x, in a contingous block</param>
        /// <param name="alpha">The scalar value to multiply by</param>
        /// <param name="xi_start">The index of the first index in xi to AXPY</param>
        /// <param name="xi_end"></param>
        /// <param name="nnz">The location to start inserting in yi</param>
        /// <param name="membership">Membership array for the set union</param>
        /// <param name="membership_threshold">Threshold for the set union</param>
        /// <returns></returns>
        int AXPY(int[] yi, T[] y, int[] xi, T[] x, T alpha, int xi_start, int xi_end, int nnz, int[] membership, int membership_threshold);

        /// <summary>
        /// AXPY: y <- y + alpha * x, for sparse vectors x and y.
        /// </summary>
        /// <param name="y">A sparse vector</param>
        /// <param name="x">A sparse vector</param>
        /// <param name="alpha">A scalar</param>
        /// <param name="start">The first index to begin adding</param>
        /// <param name="end">The second index to begin adding</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        void AXPY(Dictionary<long, T> y, Dictionary<long, T> x, T alpha, long start, long end);
        void AXPY(SparseVector<T> y, SparseVector<T> x, T alpha, int start, int end);

        /// <summary>
        /// Calculate and return the sum-product product of two sparse vectors u, v. The vectors themselves are unchanged.
        /// The sumproduct product will only include summands in the range [start, end)
        /// </summary>
        /// <param name="u">A sparse vector</param>
        /// <param name="v">A sparse vector</param>
        /// <param name="start">The first index to begin summing (inclusive).</param>
        /// <param name="end">The last index the sum (exclusive)</param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        T SUMPROD(Dictionary<long, T> u, Dictionary<long, T> v, long start, long end);

        /// <summary>
        /// Calculate and return the dot-product of two sparse vectors defined implicitly by the pairs 
        /// (xi, x) and (yi, y) respectively. 
        /// </summary>
        /// <param name="xi">The indexes of the non-zero entries in first vector.</param>
        /// <param name="x">The non-zero values of the first vector.</param>
        /// <param name="yi">The indexes of the non-zero entries in the second vector.</param>
        /// <param name="y">The non-zero values of the second vector.</param>
        /// <param name="x_start">The first index (inclusive) within xi to consider.</param>
        /// <param name="y_start">The first index (inclusive) within yi to consider.</param>
        /// <param name="x_end">The last index (exclusive) within xi to consider.</param>
        /// <param name="y_end">The last index (exclusive) within yi to consider.</param>
        /// <returns>The dot product x.y</returns>
        T DOT(int[] xi, T[] x, int[] yi, T[] y, int x_start, int y_start, int x_end, int y_end);

        /// <summary>
        /// SCAL: x <- x * s for a sparse vector x.
        /// Scale the vector x by a scalar x, rewriting x, in the interval [start, end).
        /// The rest of the vector will be unchanged.
        /// </summary>
        /// <param name="x">A sparse vector</param>
        /// <param name="s">A scalar</param>
        /// <param name="start">The first index to scale (inclusive)</param>
        /// <param name="end">The last index to scale (exclusive)</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        void SCAL(Dictionary<long, T> x, T s, long start, long end);

        /// <summary>
        /// See the equivalent documentation ADD(.)
        /// Computes z <- x - y
        /// </summary>
        /// <param name="xi"></param>
        /// <param name="x"></param>
        /// <param name="yi"></param>
        /// <param name="y"></param>
        /// <param name="zi"></param>
        /// <param name="z"></param>
        /// <param name="x_start_index"></param>
        /// <param name="y_start_index"></param>
        /// <param name="z_start_index"></param>
        /// <param name="x_end_index"></param>
        /// <param name="y_end_index"></param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        int SUB(int[] xi, T[] x, int[] yi, T[] y, int[] zi, T[] z, int x_start_index, int y_start_index, int z_start_index, int x_end_index, int y_end_index);

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        void ROTR(Dictionary<long, double> row1, Dictionary<long, double> row2, double c, double s, long col_start_incl, long col_end_excl);
    }
}
