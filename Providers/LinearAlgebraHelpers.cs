﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Providers
{
    internal static class LinearAlgebraHelpers
    {
        /// <summary>
        /// Copy B^T into Bt, where (B_start_row, B_start_col) maps to (0, 0);
        /// </summary>
        internal static void TransposeAndShift<T>(T[][] B, T[][] Bt, int B_start_row, int B_start_col, int rows, int cols, bool parallel)
        {
            if (parallel)
            {
                int batchSize = 16;
                Parallel.ForEach(Partitioner.Create(0, cols, batchSize), range =>
                {
                    int min = range.Item1, max = range.Item2;
                    for (int i = min; i < max; ++i)
                    {
                        T[] Bt_i = Bt[i];
                        int _i = i + B_start_col,
                            _j = B_start_row;
                        for (int j = 0; j < rows; ++j, ++_j)
                        {
                            Bt_i[j] = B[_j][_i];
                        }
                    }
                });
            }
            else
            {
                for (int i = 0; i < cols; ++i)
                {
                    T[] Bt_i = Bt[i];
                    int _i = i + B_start_col,
                        _j = B_start_row;
                    for (int j = 0; j < rows; ++j, ++_j)
                    {
                        Bt_i[j] = B[_j][_i];
                    }
                }
            }
        }
        /// <summary>
        /// Copy the transpose of the top-left (rows x columns) submatrix of matrix B into Bt_workspace
        /// </summary>
        /// <param name="B"></param>
        /// <param name="Bt_workspace"></param>
        /// <param name="rows"></param>
        /// <param name="columns"></param>
        internal static void Transpose<T>(T[][] B, T[][] Bt_workspace, int rows, int columns)
        {
            int i, j;
            for (i = 0; i < rows; ++i)
            {
                T[] row = B[i];
                for (j = 0; j < columns; ++j)
                {
                    Bt_workspace[j][i] = row[j];
                }
            }
        }
    }
}
