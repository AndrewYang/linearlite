﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LinearNet.Providers.Managed
{
    internal class DefaultNativeProvider<T> : IDenseBLAS1<T>
    {
        private readonly IProvider<T> _provider;

        public IProvider<T> Provider => _provider;

        IProvider<T> IDenseBLAS1<T>.Provider => throw new NotImplementedException();

        public DefaultNativeProvider(IProvider<T> provider)
        {
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }

            _provider = provider;
        }

        void IDenseBLAS1<T>.ADD(T[] u, T[] v, T[] sum, int start, int end)
        {
            for (int i = start; i < end; ++i)
            {
                sum[i] = _provider.Add(u[i], v[i]);
            }
        }

        void IDenseBLAS1<T>.AXPY(T[] y, T[] x, T alpha, int start, int end)
        {
            for (int i = start; i < end; ++i)
            {
                y[i] = _provider.Add(y[i], _provider.Multiply(alpha, x[i]));
            }
        }

        void IDenseBLAS1<T>.AXPY(T[] y, T[] x, T alpha, int y_start, int y_end, int xy_offset)
        {
            for (int i = y_start; i < y_end; ++i)
            {
                y[i] = _provider.Add(y[i], _provider.Multiply(alpha, x[i + xy_offset]));
            }
        }

        T IDenseBLAS1<T>.DOT(T[] u, T[] v, int start, int end)
        {
            T sum = _provider.Zero;
            for (int i = start; i < end; ++i)
            {
                sum = _provider.Add(sum, _provider.Multiply(u[i], v[i]));
            }
            return sum;
        }

        T IDenseBLAS1<T>.DOT(T[] u, T[] v, int u_start, int u_end, int v_u_offset)
        {
            T sum = _provider.Zero;
            for (int i = u_start; i < u_end; ++i)
            {
                sum = _provider.Add(sum, _provider.Multiply(u[i], v[i + v_u_offset]));
            }
            return sum;
        }

        void IDenseBLAS1<T>.ROTC(T[][] A, int i, int j, T c, T s, int rowStart, int rowEnd)
        {
            // only alter the columns i and j of matrix A
            T minusS = _provider.Negate(s);
            for (int k = rowStart; k < rowEnd; ++k)
            {
                T[] A_k = A[k];
                T A_ki = A_k[i], A_kj = A_k[j];
                A_k[j] = _provider.Add(_provider.Multiply(c, A_kj), _provider.Multiply(minusS, A_ki));
                A_k[i] = _provider.Add(_provider.Multiply(c, A_ki), _provider.Multiply(s, A_kj));
            }
        }

        void IDenseBLAS1<T>.ROTR(T[][] A, int i, int j, T c, T s, int colStart, int colEnd)
        {
            // only alter the rows i and j of matrix A
            T[] A_i = A[i], A_j = A[j];
            T minusS = _provider.Negate(s);
            for (int k = colStart; k < colEnd; ++k)
            {
                T A_ik = A_i[k], A_jk = A_j[k];
                A_i[k] = _provider.Add(_provider.Multiply(c, A_ik), _provider.Multiply(s, A_jk));
                A_j[k] = _provider.Add(_provider.Multiply(c, A_jk), _provider.Multiply(minusS, A_ik));
            }
        }

        void IDenseBLAS1<T>.SCAL(T[] x, T s, int start, int end)
        {
            for (int i = start; i < end; ++i)
            {
                x[i] = _provider.Multiply(x[i], s);
            }
        }

        void IDenseBLAS1<T>.SUB(T[] u, T[] v, T[] diff, int start, int end)
        {
            for (int i = start; i < end; ++i)
            {
                diff[i] = _provider.Subtract(u[i], v[i]);
            }
        }

        T IDenseBLAS1<T>.XTA(T[] x, T[][] A, int column, int start, int end)
        {
            T prod = _provider.Zero;
            for (int i = start; i < end; ++i)
            {
                prod = _provider.Add(prod, _provider.Multiply(x[i], A[i][column]));
            }
            return prod;
        }

        T IDenseBLAS1<T>.XTA(T[] x, T[][] A, int column, int x_start, int x_end, int Ax_offset)
        {
            T prod = _provider.Zero;
            for (int i = x_start; i < x_end; ++i)
            {
                prod = _provider.Add(prod, _provider.Multiply(x[i], A[i + Ax_offset][column]));
            }
            return prod;
        }

        void IDenseBLAS1<T>.PMULT(T[] u, T[] v, T[] uv, int u_start, int u_end, int v_u_offset, int uv_u_offset)
        {
            if (uv_u_offset == 0)
            {
                for (int k = u_start; k < u_end; ++k)
                {
                    uv[k] = _provider.Add(uv[k], _provider.Multiply(u[k], v[k + v_u_offset]));
                }
            }
            else
            {
                for (int k = u_start; k < u_end; ++k)
                {
                    int index = k + uv_u_offset;
                    uv[index] = _provider.Add(uv[index], _provider.Multiply(u[k], v[k + v_u_offset]));
                }
            }
        }
    }
}
