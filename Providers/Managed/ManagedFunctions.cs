﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Providers
{
    internal class ManagedFunctions<T>
    {
        internal void Copy(T[][] dest, T[][] src, int dest_first_row, int dest_first_col, int src_first_row, int src_first_col, int rows, int cols)
        {
            int r, c, k;
            if (src_first_row == 0 && src_first_col == 0)
            {
                for (r = 0; r < rows; ++r)
                {
                    T[] d = dest[dest_first_row + r], s = src[r];
                    for (c = 0, k = dest_first_col; c < cols; ++c, ++k)
                    {
                        d[k] = s[c];
                    }
                }
            }
            else
            {
                int end = cols + src_first_col;
                for (r = 0; r < rows; ++r)
                {
                    T[] d = dest[dest_first_row + r], s = src[src_first_row + r];
                    for (c = src_first_col, k = dest_first_col; c < end; ++c, ++k)
                    {
                        d[k] = s[c];
                    }
                }
            }
        }
        internal void Copy(T[][] dest, T[][] src, int src_first_row, int src_first_col, int rows, int cols)
        {
            int r, c;
            for (r = 0; r < rows; ++r)
            {
                T[] d = dest[r], s = src[src_first_row + r];
                for (c = 0; c < cols; ++c)
                {
                    d[c] = s[src_first_col + c];
                }
            }
        }
        internal void Copy(T[][] dest, T[][] src, int rows, int cols)
        {
            int r, c;
            for (r = 0; r < rows; ++r)
            {
                T[] d = dest[r], s = src[r];
                for (c = 0; c < cols; ++c)
                {
                    d[c] = s[c];
                }
            }
        }

        internal void CopyParallel(T[][] dest, T[][] src, int dest_first_row, int dest_first_col, int src_first_row, int src_first_col, int rows, int cols, int rangeSize)
        {
            if (src_first_row == 0 && src_first_col == 0)
            {
                Parallel.ForEach(Partitioner.Create(0, rows, rangeSize), range =>
                {
                    int min = range.Item1, max = range.Item2, r, c, k;
                    for (r = min; r < max; ++r)
                    {
                        T[] d = dest[dest_first_row + r], s = src[r];
                        for (c = 0, k = dest_first_col; c < cols; ++c, ++k)
                        {
                            d[k] = s[c];
                        }
                    }
                });
            }
            else
            {
                Parallel.ForEach(Partitioner.Create(0, rows, rangeSize), range =>
                {
                    int min = range.Item1, max = range.Item2, r, c, k;
                    int end = cols + src_first_col;
                    for (r = 0; r < rows; ++r)
                    {
                        T[] d = dest[dest_first_row + r], s = src[src_first_row + r];
                        for (c = src_first_col, k = dest_first_col; c < end; ++c, ++k)
                        {
                            d[k] = s[c];
                        }
                    }
                });
            }
        }
        internal void CopyParallel(T[][] dest, T[][] src, int src_first_row, int src_first_col, int rows, int cols, int rangeSize)
        {
            Parallel.ForEach(Partitioner.Create(0, rows, rangeSize), range =>
            {
                int min = range.Item1, max = range.Item2, r, c;
                for (r = 0; r < rows; ++r)
                {
                    T[] d = dest[r], s = src[src_first_row + r];
                    for (c = 0; c < cols; ++c)
                    {
                        d[c] = s[src_first_col + c];
                    }
                }
            });
        }
        internal void CopyParallel(T[][] dest, T[][] src, int rows, int cols, int rangeSize)
        {
            Parallel.ForEach(Partitioner.Create(0, rows, rangeSize), range =>
            {
                int min = range.Item1, max = range.Item2, r, c;
                for (r = 0; r < rows; ++r)
                {
                    T[] d = dest[r], s = src[r];
                    for (c = 0; c < cols; ++c)
                    {
                        d[c] = s[c];
                    }
                }
            });
        }
    }
}
