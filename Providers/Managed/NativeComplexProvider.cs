﻿using LinearNet.Providers;
using LinearNet.Matrices;
using LinearNet.Structs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.CompilerServices;
using LinearNet.Global;

namespace LinearNet.Providers.LinearAlgebra
{
    /// <summary>
    /// <p>Implements level-1 BLAS functions using a single-threaded, pure-C# managed code.</p>
    /// <p>
    /// Although managed-C# BLAS implementations like this one are selected by default,
    /// significant performance improvements can be obtained by swapping this out with hardware-accelerated 
    /// implementations like SIMD (SSE, AVX) or GPGPU (CUDA, OpenCL).
    /// </p>
    /// <p>
    /// Recommended only if the application requires a managed environment, and if parallel programming is 
    /// undesired.
    /// </p>
    /// </summary>
    /// <cat>linear-algebra</cat>
    public sealed class NativeComplexProvider : IDenseBLAS1<Complex>, IDenseBLAS2<Complex>, IDenseHouseholder<Complex>
    {
        private readonly ComplexProvider _provider;
        private readonly ManagedFunctions<Complex> _functions;
        public IProvider<Complex> Provider => _provider;

        public NativeComplexProvider()
        {
            _provider = new ComplexProvider();
            _functions = new ManagedFunctions<Complex>();
        }

        #region Internal 
        /// <summary>
        /// Please note that this is NOT the complex dot product. It is only so named because of 
        /// naming convention emplored by the other linear algebra providers
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        internal Complex DOT(Complex[] u, Complex[] v, int start, int end)
        {
            double re = 0.0, im = 0.0;
            for (int i = start; i < end; ++i)
            {
                Complex a = u[i], b = v[i];
                re += a.Real * b.Real - a.Imaginary * b.Imaginary;
                im += a.Imaginary * b.Real + a.Real * b.Imaginary;
            }
            return new Complex(re, im);
        }
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        internal void SCAL(Complex[] x, Complex s, int start, int end)
        {
            double re = s.Real, im = s.Imaginary;
            for (int i = start; i < end; ++i)
            {
                Complex c = x[i];
                double u_re = c.Real, u_im = c.Imaginary;
                x[i].Real = u_re * re - u_im * im;
                x[i].Imaginary = u_im * re + u_re * im;
            }
        }
        #endregion


        #region BLAS 1

        #region Dense
        void IDenseBLAS1<Complex>.ADD(Complex[] u, Complex[] v, Complex[] sum, int start, int end)
        {
            for (int i = start; i < end; ++i)
            {
                Complex a = u[i], b = v[i];
                sum[i] = new Complex(a.Real + b.Real, a.Imaginary + b.Imaginary);
            }
        }
        void IDenseBLAS1<Complex>.SUB(Complex[] u, Complex[] v, Complex[] diff, int start, int end)
        {
            for (int i = start; i < end; ++i)
            {
                Complex a = u[i], b = v[i];
                diff[i] = new Complex(a.Real - b.Real, a.Imaginary - b.Imaginary);
            }
        }

        void IDenseBLAS1<Complex>.AXPY(Complex[] y, Complex[] x, Complex alpha, int start, int end)
        {
            double re = alpha.Real, im = alpha.Imaginary;
            for (int i = start; i < end; ++i)
            {
                Complex c = x[i];
                y[i].Real += (c.Real * re - c.Imaginary * im);
                y[i].Imaginary += (c.Imaginary * re + c.Real * im);
            }
        }
        void IDenseBLAS1<Complex>.AXPY(Complex[] y, Complex[] x, Complex alpha, int y_start, int y_end, int xy_offset)
        {
            double re = alpha.Real, im = alpha.Imaginary;
            for (int i = y_start; i < y_end; ++i)
            {
                Complex c = x[i + xy_offset];
                y[i].Real += (c.Real * re - c.Imaginary * im);
                y[i].Imaginary += (c.Imaginary * re + c.Real * im);
            }
        }
        void IDenseBLAS1<Complex>.SCAL(Complex[] x, Complex s, int start, int end)
        {
            SCAL(x, s, start, end);
        }
        /// <summary>
        /// Please note that this is NOT the complex dot product. It is only so named because of 
        /// naming convention emplored by the other linear algebra providers
        /// </summary>
        /// <param name="u"></param>
        /// <param name="v"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        Complex IDenseBLAS1<Complex>.DOT(Complex[] u, Complex[] v, int start, int end) => DOT(u, v, start, end);
        Complex IDenseBLAS1<Complex>.DOT(Complex[] u, Complex[] v, int u_start, int u_end, int v_u_offset)
        {
            double re = 0.0, im = 0.0;
            for (int i = u_start; i < u_end; ++i)
            {
                Complex a = u[i], b = v[i + v_u_offset];
                re += a.Real * b.Real - a.Imaginary * b.Imaginary;
                im += a.Imaginary * b.Real + a.Real * b.Imaginary;
            }
            return new Complex(re, im);
        }
        Complex IDenseBLAS1<Complex>.XTA(Complex[] x, Complex[][] A, int column, int start, int end)
        {
            double re = 0.0, im = 0.0;
            for (int i = start; i < end; ++i)
            {
                Complex a = x[i], b = A[i][column];
                re += a.Real * b.Real - a.Imaginary * b.Imaginary;
                im += a.Imaginary * b.Real + a.Real * b.Imaginary;
            }
            return new Complex(re, im);
        }
        Complex IDenseBLAS1<Complex>.XTA(Complex[] x, Complex[][] A, int column, int x_start, int x_end, int Ax_offset)
        {
            double re = 0.0, im = 0.0;
            for (int i = x_start; i < x_end; ++i)
            {
                Complex a = x[i], b = A[i + Ax_offset][column];
                re += a.Real * b.Real - a.Imaginary * b.Imaginary;
                im += a.Imaginary * b.Real + a.Real * b.Imaginary;
            }
            return new Complex(re, im);
        }

        
        void IDenseBLAS1<Complex>.ROTC(Complex[][] A, int i, int j, Complex c, Complex s, int row_start_incl, int row_end_excl)
        {
            // only alter the rows i and j of matrix A
            double c_re = c.Real, c_im = c.Imaginary, s_re = s.Real, s_im = s.Imaginary;
            Complex[] A_k;
            for (int k = row_start_incl; k < row_end_excl; ++k)
            {
                A_k = A[k];
                Complex A_ki = A_k[i], A_kj = A_k[j];
                A[k][i].Set(
                    c_re * A_ki.Real - c_im * A_ki.Imaginary + s_re * A_kj.Real + s_im * A_kj.Imaginary,
                    c_im * A_ki.Real + c_re * A_ki.Imaginary - s_im * A_kj.Real + s_re * A_kj.Imaginary);
                A[k][j].Set(
                    c_re * A_kj.Real - c_im * A_kj.Imaginary - s_re * A_ki.Real + s_im * A_ki.Imaginary,
                    c_im * A_kj.Real + c_re * A_kj.Imaginary - s_im * A_ki.Real - s_re * A_ki.Imaginary);
            }
        }
        void IDenseBLAS1<Complex>.ROTR(Complex[][] A, int i, int j, Complex c, Complex s, int col_start_incl, int col_end_excl)
        {
            // only alter the rows i and j of matrix A
            Complex[] A_i = A[i], A_j = A[j];
            double c_re = c.Real, c_im = c.Imaginary, s_re = s.Real, s_im = s.Imaginary;
            for (int k = col_start_incl; k < col_end_excl; ++k)
            {
                Complex A_ik = A_i[k], A_jk = A_j[k];
                A_i[k].Set(
                    c_re * A_ik.Real - c_im * A_ik.Imaginary + s_re * A_jk.Real - s_im * A_jk.Imaginary,
                    c_im * A_ik.Real + c_re * A_ik.Imaginary + s_im * A_jk.Real + s_re * A_jk.Imaginary);
                A_j[k].Set(
                    c_re * A_jk.Real - c_im * A_jk.Imaginary - s_re * A_ik.Real - s_im * A_ik.Imaginary,
                    c_im * A_jk.Real + c_re * A_jk.Imaginary + s_im * A_ik.Real - s_re * A_ik.Imaginary);
            }
        }

        void IDenseBLAS1<Complex>.PMULT(Complex[] u, Complex[] v, Complex[] uv, int u_start, int u_end, int v_u_offset, int uv_u_offset)
        {
            for (int i = u_start; i < u_end; ++i)
            {
                Complex _u = u[i], _v = v[i + v_u_offset];

                int index = i + uv_u_offset;
                uv[index].Real      += _u.Real * _v.Real - _u.Imaginary * _v.Imaginary;
                uv[index].Imaginary += _u.Real * _v.Imaginary + _u.Imaginary * _v.Real;
            }
        }

        #endregion

        #region Sparse
        void AXPY(Dictionary<long, Complex> y, Dictionary<long, Complex> x, Complex alpha, long start, long end)
        {
            double re = alpha.Real, im = alpha.Imaginary;
            foreach (KeyValuePair<long, Complex> pair in x)
            {
                long index = pair.Key;
                if (index >= start && index < end)
                {
                    if (y.ContainsKey(index))
                    {
                        Complex c = pair.Value, _y = y[index];
                        y[index] = new Complex(
                            _y.Real + c.Real * re - c.Imaginary * im,
                            _y.Imaginary + c.Imaginary * re + c.Real * im
                        );
                    }
                    else
                    {
                        Complex c = pair.Value;
                        y[index] = new Complex(
                            c.Real * re - c.Imaginary * im,
                            c.Imaginary * re + c.Real * im
                        );
                    }
                }
            }
        }
        Complex SUMPROD(Dictionary<long, Complex> u, Dictionary<long, Complex> v, long start, long end)
        {
            double re = 0.0, im = 0.0;
            foreach (KeyValuePair<long, Complex> pair in u)
            {
                long index = pair.Key;
                if (index >= start && index < end && v.ContainsKey(index))
                {
                    Complex a = pair.Value, b = v[index];
                    re += a.Real * b.Real - a.Imaginary * b.Imaginary;
                    im += a.Imaginary * b.Real + a.Real * b.Imaginary;
                }
            }
            return new Complex(re, im);
        }
        void SCAL(Dictionary<long, Complex> x, Complex s, long start, long end)
        {
            double re = s.Real, im = s.Imaginary;
            foreach (KeyValuePair<long, Complex> pair in x)
            {
                double _re = pair.Value.Real, _im = pair.Value.Imaginary;
                x[pair.Key] = new Complex(_re * re - _im * im, _re * im + re * _im);
            }
        }
        void ROTR(Dictionary<long, double> row1, Dictionary<long, double> row2, double c, double s, long col_start_incl, long col_end_excl)
        {
            throw new NotImplementedException();
        }

        void AXPY(SparseVector<Complex> y, SparseVector<Complex> x, Complex alpha, int start, int end)
        {
            throw new NotImplementedException();
        }
        Complex DOT(SparseVector<Complex> u, SparseVector<Complex> v, int start, int end)
        {
            throw new NotImplementedException();
        }

        int ADD(int[] xi, Complex[] x, int[] yi, Complex[] y, int[] zi, Complex[] z, int x_start_index, int y_start_index, int z_start_index, int x_end_index, int y_end_index)
        {
            throw new NotImplementedException();
        }
        #endregion

        #endregion


        #region Householder 

        /// <summary>
        /// Calculate the householder vector v
        /// </summary>
        /// <param name="v"></param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void HouseholderVector(Complex[] v, int start, int end)
        {
            Complex sign = Complex.Exp(Complex.I.Multiply(v[start].Argument()));
            v[start].IncrementBy(sign.Multiply(v.Norm(start, end)));
            SCAL(v, Constants.Sqrt2 / v.Norm(start, end), start, end);
        }
        void IDenseHouseholder<Complex>.HouseholderTransform(Complex[][] A, Complex[] v, Complex[] w, int colStart, int colEnd, int rowStart, int rowEnd, bool isColumn)
        {
            if (isColumn)
            {
                Complex[] row;
                Complex zero = new Complex(0, 0), v_i, a, b;
                int r, i, j;

                // Calculate the vertical projection vector
                for (r = rowStart; r < rowEnd; ++r) v[r] = A[r][colStart];
                HouseholderVector(v, rowStart, rowEnd);

                // HH reflect on column c using vector v
                for (j = colStart; j < colEnd; ++j)
                {
                    double w_j_re = 0.0, w_j_im = 0.0;
                    for (i = rowStart; i < rowEnd; ++i)
                    {
                        a = v[i];
                        b = A[i][j];

                        // w_j += conj(a) * b
                        w_j_re += (a.Real * b.Real + a.Imaginary * b.Imaginary);
                        w_j_im += (-a.Imaginary * b.Real + a.Real * b.Imaginary);
                    }
                    w[j] = new Complex(w_j_re, w_j_im);
                }

                A[rowStart][colStart].DecrementBy(w[colStart].Multiply(v[rowStart]));
                for (i = rowStart + 1; i < rowEnd; ++i)
                {
                    A[i][colStart] = zero;
                }

                int col_first = colStart + 1;
                for (i = rowStart; i < rowEnd; ++i)
                {
                    row = A[i];
                    v_i = v[i];
                    for (j = col_first; j < colEnd; ++j)
                    {
                        b = w[j];
                        row[j].Real -= (v_i.Real * b.Real - v_i.Imaginary * b.Imaginary);
                        row[j].Imaginary -= (v_i.Imaginary * b.Real + v_i.Real * b.Imaginary);
                    }
                }
            }
            else
            {
                Complex[] row = A[colStart];
                Complex a, b;
                int i, j;

                // Calculate the horizontal projection vector
                for (j = rowStart; j < colEnd; ++j) v[j] = row[j];
                HouseholderVector(v, rowStart, colEnd);

                // HH reflect on row c using vector v
                for (i = colStart; i < rowEnd; ++i)
                {
                    row = A[i];
                    double w_i_re = 0.0, w_i_im = 0.0;
                    for (j = rowStart; j < colEnd; ++j)
                    {
                        a = v[j];
                        b = row[j];
                        w_i_re += (a.Real * b.Real + a.Imaginary * b.Imaginary);
                        w_i_im += (-a.Imaginary * b.Real + a.Real * b.Imaginary);
                    }
                    for (j = rowStart; j < colEnd; ++j)
                    {
                        a = v[j];
                        row[j].Real -= (a.Real * w_i_re - a.Imaginary * w_i_im);
                        row[j].Imaginary -= (a.Imaginary * w_i_re + a.Real * w_i_im);
                    }
                }
            }
        }
        void IDenseHouseholder<Complex>.HouseholderTransform(Complex[][] A, Complex[][] H, Complex[] v, Complex[] w, int colStart, int colEnd, int rowStart, int rowEnd, bool isColumn)
        {
            Householder.Transform(A, H, v, w, colStart, colEnd, rowStart, rowEnd, isColumn);
        }
        void IDenseHouseholder<Complex>.HouseholderTransformParallel(Complex[][] A, Complex[][] H, Complex[] v, Complex[] w, int colStart, int colEnd, int rowStart, int rowEnd, bool isColumn)
        {
            Householder.TransformParallel(A, H, v, w, colStart, colEnd, rowStart, rowEnd, isColumn);
        }

        #endregion


        #region BLAS 2

        void IDenseBLAS2<Complex>.Add(Complex[][] A, Complex[][] B, Complex[][] C, int a_row_start, int a_col_start, int b_row_start, int b_col_start, int rows, int cols)
        {
            int r, c;
            Complex[] result_r, A_r, B_r;

            if (a_row_start == 0 && a_col_start == 0)
            {
                for (r = 0; r < rows; ++r)
                {
                    result_r = C[r];
                    A_r = A[r];
                    B_r = B[b_row_start + r];
                    for (c = 0; c < cols; ++c)
                    {
                        Complex a = A_r[c], b = B_r[b_col_start + c];
                        result_r[c].Real = a.Real + b.Real;
                        result_r[c].Imaginary = a.Imaginary + b.Imaginary;
                    }
                }
            }
            else
            {
                for (r = 0; r < rows; ++r)
                {
                    result_r = C[r];
                    A_r = A[a_row_start + r];
                    B_r = B[b_row_start + r];
                    for (c = 0; c < cols; ++c)
                    {
                        Complex a = A_r[a_col_start + c], b = B_r[b_col_start + c];
                        result_r[c].Real = a.Real + b.Real;
                        result_r[c].Imaginary = a.Imaginary + b.Imaginary;
                    }
                }
            }
        }

        void IDenseBLAS2<Complex>.Copy(Complex[][] dest, Complex[][] src, int rows, int cols)
        {
            _functions.Copy(dest, src, rows, cols);
        }

        void IDenseBLAS2<Complex>.Copy(Complex[][] dest, Complex[][] src, int src_row_start, int src_col_start, int rows, int cols)
        {
            _functions.Copy(dest, src, src_row_start, src_col_start, rows, cols);
        }

        void IDenseBLAS2<Complex>.Copy(Complex[][] dest, Complex[][] src, int dest_row_start, int dest_col_start, int src_row_start, int src_col_start, int rows, int cols)
        {
            _functions.Copy(dest, src, dest_row_start, dest_col_start, src_row_start, src_col_start, rows, cols);
        }

        void IDenseBLAS2<Complex>.Subtract(Complex[][] A, Complex[][] B, Complex[][] C, int a_row_start, int a_col_start, int b_row_start, int b_col_start, int rows, int cols)
        {
            int r, c;
            Complex[] C_r, A_r, B_r;
            for (r = 0; r < rows; ++r)
            {
                C_r = C[r];
                A_r = A[a_row_start + r];
                B_r = B[b_row_start + r];
                for (c = 0; c < cols; ++c)
                {
                    Complex a = A_r[a_col_start + c], b = B_r[b_col_start + c];
                    C_r[c].Real = a.Real - b.Real;
                    C_r[c].Imaginary = a.Imaginary - b.Imaginary;
                }
            }
        }

        void IDenseBLAS2<Complex>.Increment(Complex[][] A, Complex[][] B, int rows, int cols)
        {
            int i, j;
            for (i = 0; i < rows; ++i)
            {
                Complex[] A_i = A[i], B_i = B[i];
                for (j = 0; j < cols; ++j)
                {
                    A_i[j].IncrementBy(B_i[j]);
                }
            }
        }

        void IDenseBLAS2<Complex>.Increment(Complex[][] A, Complex[][] B, int a_row_start, int a_col_start, int rows, int cols)
        {
            int i, j;
            for (i = 0; i < rows; ++i)
            {
                Complex[] A_i = A[a_row_start + i], B_i = B[i];
                for (j = 0; j < cols; ++j)
                {
                    A_i[a_col_start + j].IncrementBy(B_i[j]);
                }
            }
        }

        void IDenseBLAS2<Complex>.Decrement(Complex[][] A, Complex[][] B, int rows, int cols)
        {
            int i, j;
            for (i = 0; i < rows; ++i)
            {
                Complex[] A_i = A[i], B_i = B[i];
                for (j = 0; j < cols; ++j)
                {
                    A_i[j].DecrementBy(B_i[j]);
                }
            }
        }

        void IDenseBLAS2<Complex>.Decrement(Complex[][] A, Complex[][] B, int a_row_start, int a_col_start, int rows, int cols)
        {
            int i, j;
            for (i = 0; i < rows; ++i)
            {
                Complex[] A_i = A[a_row_start + i], B_i = B[i];
                for (j = 0; j < cols; ++j)
                {
                    int k = a_col_start + j;
                    A_i[k].DecrementBy(B_i[j]);
                }
            }
        }

        void IDenseBLAS2<Complex>.Negate(Complex[][] A, Complex[][] result, int a_row_start, int a_col_start, int result_row_start, int result_col_start, int rows, int cols)
        {
            int i, j;
            for (i = 0; i < rows; ++i)
            {
                Complex[] A_i = A[a_row_start + i], result_i = result[result_row_start + i];
                for (j = 0; j < cols; ++j)
                {
                    result_i[result_col_start + j] = -A_i[a_col_start + j];
                }
            }
        }


        #endregion
    }
}
