﻿using LinearNet.Matrices;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using LinearNet.Structs;
using LinearNet.Global;
using System.Diagnostics;

namespace LinearNet.Providers.LinearAlgebra
{
    /// <summary>
    /// Implements level-1 BLAS functions for <txt>double</txt> via a single-threaded, managed C# code.
    /// </summary>
    /// <cat>linear-algebra</cat>
    public sealed class NativeDoubleProvider : IDenseBLAS1<double>, IDenseBLAS2<double>, IDenseHouseholder<double>, ISparseBLAS1<double>
    {
        private readonly DoubleProvider _provider;
        private readonly ManagedFunctions<double> _functions;

        public IProvider<double> Provider => _provider;

        internal NativeDoubleProvider()
        {
            _provider = new DoubleProvider();
            _functions = new ManagedFunctions<double>();
        }

        #region Internal

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        internal void AXPY(double[] y, double[] x, double alpha, int start, int end)
        {
            for (int i = start; i < end; ++i)
            {
                y[i] += alpha * x[i];
            }
        }
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        internal double DOT(double[] u, double[] v, int start, int end)
        {
            double sum = 0.0;
            for (int i = start; i < end; ++i)
            {
                sum += u[i] * v[i];
            }
            return sum;
        }
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        internal double DOT(double[] u, double[] v, int u_start, int u_end, int v_u_offset)
        {
            double sum = 0.0;
            for (int i = u_start; i < u_end; ++i)
            {
                sum += u[i] * v[i + v_u_offset];
            }
            return sum;
        }
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        internal void SCAL(double[] x, double s, int start, int end)
        {
            for (int i = start; i < end; ++i)
            {
                x[i] *= s;
            }
        }
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        internal double XTA(double[] x, double[][] A, int column, int start, int end)
        {
            double prod = 0.0;
            for (int i = start; i < end; ++i)
            {
                prod += x[i] * A[i][column];
            }
            return prod;
        }
        #endregion


        #region BLAS 1

        #region Dense

        void IDenseBLAS1<double>.ADD(double[] u, double[] v, double[] sum, int start, int end)
        {
            for (int i = start; i < end; ++i)
            {
                sum[i] = u[i] + v[i];
            }
        }
        void IDenseBLAS1<double>.SUB(double[] u, double[] v, double[] diff, int start, int end)
        {
            for (int i = start; i < end; ++i)
            {
                diff[i] = u[i] - v[i];
            }
        }
        void IDenseBLAS1<double>.AXPY(double[] y, double[] x, double alpha, int start, int end) => AXPY(y, x, alpha, start, end);
        void IDenseBLAS1<double>.AXPY(double[] y, double[] x, double alpha, int y_start, int y_end, int xy_offset)
        {
            for (int i = y_start; i < y_end; ++i)
            {
                y[i] += alpha * x[i + xy_offset];
            }
        }
        double IDenseBLAS1<double>.DOT(double[] u, double[] v, int start, int end) => DOT(u, v, start, end);
        double IDenseBLAS1<double>.DOT(double[] u, double[] v, int u_start, int u_end, int v_u_offset) => DOT(u, v, u_start, u_end, v_u_offset);
        void IDenseBLAS1<double>.SCAL(double[] x, double s, int start, int end) => SCAL(x, s, start, end);
        double IDenseBLAS1<double>.XTA(double[] x, double[][] A, int column, int start, int end) => XTA(x, A, column, start, end);
        double IDenseBLAS1<double>.XTA(double[] x, double[][] A, int column, int x_start, int x_end, int Ax_offset)
        {
            double prod = 0.0;
            for (int i = x_start; i < x_end; ++i)
            {
                prod += x[i] * A[i + Ax_offset][column];
            }
            return prod;
        }

        void IDenseBLAS1<double>.ROTC(double[][] A, int i, int j, double c, double s, int row_start_incl, int row_end_excl)
        {
            // only alter column i and j of matrix A
            for (int k = row_start_incl; k < row_end_excl; ++k)
            {
                double[] A_k = A[k];
                double A_ki = A_k[i], A_kj = A_k[j];
                A_k[i] = c * A_ki + s * A_kj;
                A_k[j] = c * A_kj - s * A_ki;
            }
        }
        void IDenseBLAS1<double>.ROTR(double[][] A, int i, int j, double c, double s, int col_start_incl, int col_end_excl)
        {
            // only alter the rows i and j of matrix A
            double[] A_i = A[i], A_j = A[j];
            for (int k = col_start_incl; k < col_end_excl; ++k)
            {
                double A_ik = A_i[k], A_jk = A_j[k];
                A_i[k] = c * A_ik + s * A_jk;
                A_j[k] = c * A_jk - s * A_ik;
            }
        }
        void IDenseBLAS1<double>.PMULT(double[] u, double[] v, double[] uv, int u_start, int u_end, int v_u_offset, int uv_u_offset)
        {
            for (int i = u_start; i < u_end; ++i)
            {
                uv[i + uv_u_offset] += u[i] * v[i + v_u_offset];
            }
        }
        #endregion

        #region Sparse

        void ISparseBLAS1<double>.AXPY(Dictionary<long, double> y, Dictionary<long, double> x, double alpha, long start, long end)
        {
            foreach (KeyValuePair<long, double> pair in x)
            {
                long index = pair.Key;
                if (index >= start && index < end)
                {
                    if (y.ContainsKey(index))
                    {
                        y[index] += alpha * pair.Value;
                    }
                    else
                    {
                        y[index] = alpha * pair.Value;
                    }
                }
            }
        }
        double ISparseBLAS1<double>.SUMPROD(Dictionary<long, double> u, Dictionary<long, double> v, long start, long end)
        {
            double sum = 0.0;
            foreach (KeyValuePair<long, double> pair in u)
            {
                long index = pair.Key;
                if (index >= start && index < end && v.ContainsKey(index))
                {
                    sum += pair.Value * v[index];
                }
            }
            return sum;
        }
        void ISparseBLAS1<double>.SCAL(Dictionary<long, double> x, double s, long start, long end)
        {
            foreach (long key in x.Keys)
            {
                x[key] *= s;
            }
        }
        void ISparseBLAS1<double>.ROTR(Dictionary<long, double> row1, Dictionary<long, double> row2, double c, double s, long col_start_incl, long col_end_excl)
        {
            HashSet<long> union = new HashSet<long>(row1.Keys);
            union.UnionWith(row2.Keys);

            foreach (long k in union)
            {
                if (k >= col_start_incl && k < col_end_excl)
                {
                    double A_ik = row1.ContainsKey(k) ? row1[k] : 0.0;
                    double A_jk = row2.ContainsKey(k) ? row2[k] : 0.0;

                    double temp = c * A_ik + s * A_jk;
                    row2[k] = c * A_jk - s * A_ik;
                    row1[k] = temp;
                }
            }
        }

        void ISparseBLAS1<double>.AXPY(SparseVector<double> y, SparseVector<double> x, double alpha, int start, int end)
        {
            int[] yInd = y.Indices, xInd = x.Indices;
            double[] yVal = y.Values, xVal = x.Values;
            int ylen = yVal.Length,
                xlen = xVal.Length, 
                yfirst = 0,
                xfirst = 0,
                ylast = ylen - 1, 
                xlast = xlen - 1;

            // Calculate the index boundaries
            while (yfirst < ylen && yInd[yfirst] < start) ++yfirst;
            while (xfirst < xlen && xInd[xfirst] < start) ++xfirst;
            while (ylast >= 0 && yInd[ylast] >= end) --ylast;
            while (xlast >= 0 && xInd[xlast] >= end) --xlast;

            int xi = xfirst, yi = yfirst;

            // Calculate nnz
            int count = 0;
            while (yi < ylast || xi < xlast)
            {
                int a = yi < ylast ? yInd[yi] : int.MaxValue;
                int b = xi < xlast ? xInd[xi] : int.MaxValue;

                if (a > b)
                {
                    ++xi;
                }
                else if (a < b)
                {
                    ++yi;
                }
                else
                {
                    ++xi;
                    ++yi;
                }
                ++count;
            }

            // reset indices
            xi = xfirst; yi = yfirst;

            // re-use the y-arrays if x-indices is a subset of y-indices
            if (count == ylast)
            {
                while (yi < ylast || xi < xlast)
                {
                    int a = yi < ylast ? yInd[yi] : int.MaxValue;
                    int b = xi < xlast ? xInd[xi] : int.MaxValue;

                    // a will never > b
                    if (a < b)
                    {
                        ++yi;
                    }
                    // a == b
                    else
                    {
                        yVal[yi] += alpha * xVal[xi];
                    }
                }
            }

            // Requires re-set of arrays
            else
            {
                int[] indices = new int[count];
                double[] values = new double[count];

                int k = 0;
                while (yi < ylast || xi < xlast)
                {
                    int a = yi < ylast ? yInd[yi] : int.MaxValue;
                    int b = xi < xlast ? xInd[xi] : int.MaxValue;

                    if (a > b)
                    {
                        indices[k] = b;
                        values[k] = alpha * xVal[xi];
                        ++k;
                        ++xi;
                    }
                    else if (a < b)
                    {
                        indices[k] = a;
                        values[k] = yVal[yi];
                        ++k;
                        ++yi;
                    }
                    else
                    {
                        indices[k] = a;
                        values[k] = yVal[yi] + alpha * xVal[xi];
                        ++k;
                        ++yi;
                        ++xi;
                    }
                }
                y.Values = values;
                y.Indices = indices;
            }
        }
        double ISparseBLAS1<double>.DOT(int[] xi, double[] x, int[] yi, double[] y, int x_start, int y_start, int x_end, int y_end)
        {
            int i = x_start, j = y_start;
            double dot = 0;
            while (i < x_end || j < y_end)
            {
                int a = i < x_end ? xi[i] : int.MaxValue;
                int b = j < y_end ? yi[j] : int.MaxValue;
                if (a < b)
                {
                    ++i;
                }
                else if (a > b)
                {
                    ++j;
                }
                else
                {
                    dot += x[i] * y[j];
                    ++i; ++j;
                }
            }
            return dot;
        }

        int ISparseBLAS1<double>.AXPY(int[] yi, double[] y_workspace, int[] xi, double[] x, double alpha, int xi_start, int xi_end, int nnz, int[] membership, int membership_threshold)
        {
            for (int i = xi_start; i < xi_end; ++i)
            {
                int j = xi[i];
                if (membership[j] < membership_threshold)
                {
                    membership[j] = membership_threshold;
                    y_workspace[j] = x[i] * alpha;
                    yi[nnz++] = j;
                }
                else
                {
                    y_workspace[j] += x[i] * alpha;
                }
            }
            return nnz;
        }
        int ISparseBLAS1<double>.ADD(int[] xi, double[] x, int[] yi, double[] y, int[] zi, double[] z, int x_start_index, int y_start_index, int z_start_index, int x_end_index, int y_end_index)
        {
            int i = x_start_index, j = y_start_index, k = z_start_index;
            while (i < x_end_index || j < y_end_index)
            {
                int a = i < x_end_index ? xi[i] : int.MaxValue;
                int b = j < y_end_index ? yi[j] : int.MaxValue;

                if (a < b)
                {
                    zi[k] = a;
                    z[k] = x[i];
                    ++i;
                }
                else if (a > b)
                {
                    zi[k] = b;
                    z[k] = y[j];
                    ++j;
                }
                else
                {
                    zi[k] = a;
                    z[k] = x[i] + y[j];
                    ++i;
                    ++j;
                }
                ++k;
            }
            return k;
        }
        int ISparseBLAS1<double>.SUB(int[] xi, double[] x, int[] yi, double[] y, int[] zi, double[] z, int x_start_index, int y_start_index, int z_start_index, int x_end_index, int y_end_index)
        {
            int i = x_start_index, j = y_start_index, k = z_start_index;
            while (i < x_end_index || j < y_end_index)
            {
                int a = i < x_end_index ? xi[i] : int.MaxValue;
                int b = j < y_end_index ? yi[j] : int.MaxValue;

                if (a < b)
                {
                    zi[k] = a;
                    z[k] = x[i];
                    ++i;
                }
                else if (a > b)
                {
                    zi[k] = b;
                    z[k] = -y[j];
                    ++j;
                }
                else
                {
                    zi[k] = a;
                    z[k] = x[i] - y[j];
                    ++i;
                    ++j;
                }
                ++k;
            }
            return k;
        }
        #endregion

        #endregion


        #region Householder 
        private void HouseholderVector(double[] v, int start, int end)
        {
            int sign = v[start] >= 0 ? 1 : -1;
            v[start] += sign * v.Norm(start, end);
            SCAL(v, Constants.Sqrt2 / v.Norm(start, end), start, end);
        }
        void IDenseHouseholder<double>.HouseholderTransform(double[][] A, double[] v, double[] w, int colStart, int colEnd, int rowStart, int rowEnd, bool isColumn)
        {
            if (isColumn)
            {
                int i;

                // Calculate the vertical projection vector
                for (i = rowStart; i < rowEnd; ++i) v[i] = A[i][colStart];
                HouseholderVector(v, rowStart, rowEnd);

                // HH reflect on column c using vector v
                Array.Clear(w, colStart, colEnd - colStart);
                for (i = rowStart; i < rowEnd; ++i)
                {
                    AXPY(w, A[i], v[i], colStart, colEnd);
                }
                for (i = rowStart; i < rowEnd; ++i)
                {
                    AXPY(A[i], w, -v[i], colStart, colEnd);
                }
            }
            else
            {
                Array.Copy(A[colStart], rowStart, v, rowStart, colEnd - rowStart);
                HouseholderVector(v, rowStart, colEnd);

                // HH reflect on row c using vector v
                for (int i = colStart; i < rowEnd; ++i)
                {
                    double[] row = A[i];
                    double w_i = DOT(v, row, rowStart, colEnd);
                    AXPY(row, v, -w_i, rowStart, colEnd);
                }
            }
        }
        void IDenseHouseholder<double>.HouseholderTransform(double[][] A, double[][] H, double[] v, double[] w, int colStart, int colEnd, int rowStart, int rowEnd, bool isColumn)
        {
            Householder.Transform(A, H, v, w, colStart, colEnd, rowStart, rowEnd, isColumn);
        }
        void IDenseHouseholder<double>.HouseholderTransformParallel(double[][] A, double[][] H, double[] v, double[] w, int colStart, int colEnd, int rowStart, int rowEnd, bool isColumn)
        {
            Householder.TransformParallel(A, H, v, w, colStart, colEnd, rowStart, rowEnd, isColumn);
        }
        #endregion

        #region Internal 
        internal void Increment(double[][] A, double[][] B, int rows, int cols)
        {
            int i, j;
            for (i = 0; i < rows; ++i)
            {
                double[] A_i = A[i], B_i = B[i];
                for (j = 0; j < cols; ++j)
                {
                    A_i[j] += B_i[j];
                }
            }
        }
        internal void Decrement(double[][] A, double[][] B, int rows, int cols)
        {
            int i, j;
            for (i = 0; i < rows; ++i)
            {
                double[] A_i = A[i], B_i = B[i];
                for (j = 0; j < cols; ++j)
                {
                    A_i[j] -= B_i[j];
                }
            }
        }
        #endregion

        #region BLAS 2

        void IDenseBLAS2<double>.Add(double[][] A, double[][] B, double[][] C, int a_row_start, int a_col_start, int b_row_start, int b_col_start, int rows, int cols)
        {
            int i, j;

            if (a_row_start == 0 && a_col_start == 0)
            {
                int k;
                for (i = 0; i < rows; ++i)
                {
                    double[] A_i = A[i], B_i = B[b_row_start + i], result_i = C[i];
                    for (j = 0, k = b_col_start; j < cols; ++j, ++k)
                    {
                        result_i[j] = A_i[j] + B_i[k];
                    }
                }
            }
            else
            {
                int k, l;
                for (i = 0; i < rows; ++i)
                {
                    double[] A_i = A[a_row_start + i], B_i = B[b_row_start + i], result_i = C[i];
                    for (j = 0, k = a_col_start, l = b_col_start; j < cols; ++j, ++k, ++l)
                    {
                        result_i[j] = A_i[k] + B_i[l];
                    }
                }
            }
        }

        void IDenseBLAS2<double>.Copy(double[][] dest, double[][] src, int rows, int cols)
        {
            _functions.Copy(dest, src, rows, cols);
        }

        void IDenseBLAS2<double>.Copy(double[][] dest, double[][] src, int src_row_start, int src_col_start, int rows, int cols)
        {
            _functions.Copy(dest, src, src_row_start, src_col_start, rows, cols);
        }

        void IDenseBLAS2<double>.Copy(double[][] dest, double[][] src, int dest_row_start, int dest_col_start, int src_row_start, int src_col_start, int rows, int cols)
        {
            _functions.Copy(dest, src, dest_row_start, dest_col_start, src_row_start, src_col_start, rows, cols);
        }

        void IDenseBLAS2<double>.Subtract(double[][] A, double[][] B, double[][] C, int a_row_start, int a_col_start, int b_row_start, int b_col_start, int rows, int cols)
        {
            int r, c;

            double[] result_r, A_r, B_r;

            if (a_row_start == 0 && a_col_start == 0)
            {
                if (b_row_start == 0 && b_col_start == 0)
                {
                    for (r = 0; r < rows; ++r)
                    {
                        result_r = C[r];
                        A_r = A[r];
                        B_r = B[r];
                        for (c = 0; c < cols; ++c)
                        {
                            result_r[c] = A_r[c] - B_r[c];
                        }
                    }
                }
                else
                {
                    for (r = 0; r < rows; ++r)
                    {
                        result_r = C[r];
                        A_r = A[r];
                        B_r = B[b_row_start + r];
                        for (c = 0; c < cols; ++c)
                        {
                            result_r[c] = A_r[c] - B_r[b_col_start + c];
                        }
                    }
                }
            }
            else
            {
                for (r = 0; r < rows; ++r)
                {
                    result_r = C[r];
                    A_r = A[a_row_start + r];
                    B_r = B[b_row_start + r];
                    for (c = 0; c < cols; ++c)
                    {
                        result_r[c] = A_r[a_col_start + c] - B_r[b_col_start + c];
                    }
                }
            }
        }

        void IDenseBLAS2<double>.Increment(double[][] A, double[][] B, int rows, int cols) => Increment(A, B, rows, cols);

        void IDenseBLAS2<double>.Increment(double[][] A, double[][] B, int a_row_start, int a_col_start, int rows, int cols)
        {
            int i, j;

            if (a_row_start == 0 && a_col_start == 0)
            {
                Increment(A, B, rows, cols);
            }
            else if (a_col_start == 0)
            {
                for (i = 0; i < rows; ++i)
                {
                    double[] A_i = A[a_row_start + i], B_i = B[i];
                    for (j = 0; j < cols; ++j)
                    {
                        A_i[j] += B_i[j];
                    }
                }
            }
            else
            {
                int k;
                for (i = 0; i < rows; ++i)
                {
                    double[] A_i = A[a_row_start + i], B_i = B[i];
                    for (j = 0, k = a_col_start; j < cols; ++j, ++k)
                    {
                        A_i[k] += B_i[j];
                    }
                }
            }
        }

        void IDenseBLAS2<double>.Decrement(double[][] A, double[][] B, int rows, int cols) => Decrement(A, B, rows, cols);

        void IDenseBLAS2<double>.Decrement(double[][] A, double[][] B, int a_row_start, int a_col_start, int rows, int cols)
        {
            int i, j;
            if (a_row_start == 0 && a_col_start == 0)
            {
                Decrement(A, B, rows, cols);
            }
            else
            {
                for (i = 0; i < rows; ++i)
                {
                    double[] A_i = A[a_row_start + i], B_i = B[i];
                    for (j = 0; j < cols; ++j)
                    {
                        A_i[a_col_start + j] -= B_i[j];
                    }
                }
            }
        }

        void IDenseBLAS2<double>.Negate(double[][] A, double[][] result, int a_row_start, int a_col_start, int result_row_start, int result_col_start, int rows, int cols)
        {
            int i, j;
            for (i = 0; i < rows; ++i)
            {
                double[] A_i = A[a_row_start + i], result_i = result[result_row_start + i];
                for (j = 0; j < cols; ++j)
                {
                    result_i[result_col_start + j] = -A_i[a_col_start + j];
                }
            }
        }

        #endregion
    }
}
