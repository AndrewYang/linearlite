﻿using LinearNet.Matrices;
using LinearNet.Providers;
using LinearNet.Structs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Providers.LinearAlgebra
{
    /// <summary>
    /// </summary>
    /// Currently this class does not provide any additional functionality on top of RingLinearAlgebraProvider<T>
    /// We maintain this class in case of future use cases.
    /// <cat>linear-algebra</cat>
    public sealed class NativeFieldProvider<T> : NativeRingProvider<T> where T : IField<T>, new()
    {
        
    }
}
