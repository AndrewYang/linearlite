﻿using LinearNet.Matrices;
using System;
using System.Collections.Generic;
using LinearNet.Structs;
using LinearNet.Global;
using System.Runtime.CompilerServices;

namespace LinearNet.Providers.LinearAlgebra
{
    /// <summary>
    /// 
    /// </summary>
    /// <cat>linear-algebra</cat>
    public sealed class NativeFloatProvider : IDenseBLAS1<float>, IDenseBLAS2<float>, IDenseHouseholder<float>
    {
        private readonly FloatProvider _provider;
        private readonly ManagedFunctions<float> _functions;

        public IProvider<float> Provider => _provider;

        public NativeFloatProvider()
        {
            _provider = new FloatProvider();
            _functions = new ManagedFunctions<float>();
        }

        #region BLAS 1

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        internal void AXPY(float[] y, float[] x, float alpha, int start, int end)
        {
            for (int i = start; i < end; ++i)
            {
                y[i] += alpha * x[i];
            }
        }
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        internal float DOT(float[] u, float[] v, int start, int end)
        {
            float sum = 0.0f;
            for (int i = start; i < end; ++i)
            {
                sum += u[i] * v[i];
            }
            return sum;
        }
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        internal void SCAL(float[] x, float s, int start, int end)
        {
            for (int i = start; i < end; ++i)
            {
                x[i] *= s;
            }
        }

        #region Dense
        void IDenseBLAS1<float>.ADD(float[] u, float[] v, float[] sum, int start, int end)
        {
            for (int i = start; i < end; ++i)
            {
                sum[i] = u[i] + v[i];
            }
        }
        void IDenseBLAS1<float>.SUB(float[] u, float[] v, float[] diff, int start, int end)
        {
            for (int i = start; i < end; ++i)
            {
                diff[i] = u[i] - v[i];
            }
        }
        void IDenseBLAS1<float>.AXPY(float[] y, float[] x, float alpha, int start, int end) => AXPY(y, x, alpha, start, end);
        void IDenseBLAS1<float>.AXPY(float[] y, float[] x, float alpha, int y_start, int y_end, int xy_offset)
        {
            for (int i = y_start; i < y_end; ++i)
            {
                y[i] += alpha * x[i + xy_offset];
            }
        }
        float IDenseBLAS1<float>.DOT(float[] u, float[] v, int start, int end) => DOT(u, v, start, end);
        float IDenseBLAS1<float>.DOT(float[] u, float[] v, int u_start, int u_end, int v_u_offset)
        {
            float sum = 0.0f;
            for (int i = u_start; i < u_end; ++i)
            {
                sum += u[i] * v[i + v_u_offset];
            }
            return sum;
        }
        void IDenseBLAS1<float>.SCAL(float[] x, float s, int start, int end) => SCAL(x, s, start, end);
        float IDenseBLAS1<float>.XTA(float[] x, float[][] A, int column, int start, int end)
        {
            float prod = 0.0f;
            for (int i = start; i < end; ++i)
            {
                prod += x[i] * A[i][column];
            }
            return prod;
        }
        float IDenseBLAS1<float>.XTA(float[] x, float[][] A, int column, int x_start, int x_end, int Ax_offset)
        {
            float prod = 0.0f;
            for (int i = x_start; i < x_end; ++i)
            {
                prod += x[i] * A[i + Ax_offset][column];
            }
            return prod;
        }
        void IDenseBLAS1<float>.ROTC(float[][] A, int i, int j, float c, float s, int row_start_incl, int row_end_excl)
        {
            // only alter the rows i and j of matrix A
            for (int k = row_start_incl; k < row_end_excl; ++k)
            {
                float[] A_k = A[k];
                float A_ki = A_k[i], A_kj = A_k[j];
                A_k[j] = c * A_kj - s * A_ki;
                A_k[i] = c * A_ki + s * A_kj;
            }
        }
        void IDenseBLAS1<float>.ROTR(float[][] A, int i, int j, float c, float s, int col_start_incl, int col_end_excl)
        {
            // only alter the rows i and j of matrix A
            float[] A_i = A[i], A_j = A[j];
            for (int k = col_start_incl; k < col_end_excl; ++k)
            {
                float A_ik = A_i[k], A_jk = A_j[k];
                A_i[k] = c * A_ik + s * A_jk;
                A_j[k] = c * A_jk - s * A_ik;
            }
        }
        void IDenseBLAS1<float>.PMULT(float[] u, float[] v, float[] uv, int u_start, int u_end, int v_u_offset, int uv_u_offset)
        {
            for (int i = u_start; i < u_end; ++i)
            {
                uv[i + uv_u_offset] += u[i] * v[i + v_u_offset];
            }
        }
        #endregion


        #region Sparse
        void AXPY(Dictionary<long, float> y, Dictionary<long, float> x, float alpha, long start, long end)
        {
            foreach (KeyValuePair<long, float> pair in x)
            {
                long index = pair.Key;
                if (index >= start && index < end)
                {
                    if (y.ContainsKey(index))
                    {
                        y[index] += alpha * pair.Value;
                    }
                    else
                    {
                        y[index] = alpha * pair.Value;
                    }
                }
            }
        }
        float SUMPROD(Dictionary<long, float> u, Dictionary<long, float> v, long start, long end)
        {
            float sum = 0.0f;
            foreach (KeyValuePair<long, float> pair in u)
            {
                long index = pair.Key;
                if (index >= start && index < end && v.ContainsKey(index))
                {
                    sum += pair.Value * v[index];
                }
            }
            return sum;
        }
        void SCAL(Dictionary<long, float> x, float s, long start, long end)
        {
            foreach (long key in x.Keys)
            {
                x[key] *= s;
            }
        }
        void ROTR(Dictionary<long, double> row1, Dictionary<long, double> row2, double c, double s, long col_start_incl, long col_end_excl)
        {
            throw new NotImplementedException();
        }


        void AXPY(SparseVector<float> y, SparseVector<float> x, float alpha, int start, int end)
        {
            throw new NotImplementedException();
        }
        float DOT(SparseVector<float> u, SparseVector<float> v, int start, int end)
        {
            throw new NotImplementedException();
        }

        int ADD(int[] xi, float[] x, int[] yi, float[] y, int[] zi, float[] z, int x_start_index, int y_start_index, int z_start_index, int x_end_index, int y_end_index)
        {
            throw new NotImplementedException();
        }
        #endregion

        #endregion


        #region Householder 
        private void HouseholderVector(float[] v, int start, int end)
        {
            int sign = v[start] >= 0 ? 1 : -1;
            v[start] += sign * v.Norm(start, end);
            SCAL(v, Constants.Sqrt2_f / v.Norm(start, end), start, end);
        }
        void IDenseHouseholder<float>.HouseholderTransform(float[][] A, float[] v, float[] w, int colStart, int colEnd, int rowStart, int rowEnd, bool isColumn)
        {
            if (isColumn)
            {
                int i;

                // Calculate the vertical projection vector
                for (i = rowStart; i < rowEnd; ++i) v[i] = A[i][colStart];
                HouseholderVector(v, rowStart, rowEnd);

                // HH reflect on column c using vector v
                Array.Clear(w, colStart, colEnd - colStart);
                for (i = rowStart; i < rowEnd; ++i)
                {
                    AXPY(w, A[i], v[i], colStart, colEnd);
                }
                for (i = rowStart; i < rowEnd; ++i)
                {
                    AXPY(A[i], w, -v[i], colStart, colEnd);
                }
            }
            else
            {
                Array.Copy(A[colStart], rowStart, v, rowStart, colEnd - rowStart);
                HouseholderVector(v, rowStart, colEnd);

                // HH reflect on row c using vector v
                for (int i = colStart; i < rowEnd; ++i)
                {
                    float[] row = A[i];
                    float w_i = DOT(v, row, rowStart, colEnd);
                    AXPY(row, v, -w_i, rowStart, colEnd);
                }
            }
        }
        void IDenseHouseholder<float>.HouseholderTransform(float[][] A, float[][] H, float[] v, float[] w, int colStart, int colEnd, int rowStart, int rowEnd, bool isColumn)
        {
            Householder.Transform(A, H, v, w, colStart, colEnd, rowStart, rowEnd, isColumn);
        }
        void IDenseHouseholder<float>.HouseholderTransformParallel(float[][] A, float[][] H, float[] v, float[] w, int colStart, int colEnd, int rowStart, int rowEnd, bool isColumn)
        {
            Householder.TransformParallel(A, H, v, w, colStart, colEnd, rowStart, rowEnd, isColumn);
        }
        #endregion


        #region Internal 
        internal void Increment(float[][] A, float[][] B, int rows, int cols)
        {
            int i, j;
            for (i = 0; i < rows; ++i)
            {
                float[] A_i = A[i], B_i = B[i];
                for (j = 0; j < cols; ++j)
                {
                    A_i[j] += B_i[j];
                }
            }
        }
        internal void Decrement(float[][] A, float[][] B, int rows, int cols)
        {
            int i, j;
            for (i = 0; i < rows; ++i)
            {
                float[] A_i = A[i], B_i = B[i];
                for (j = 0; j < cols; ++j)
                {
                    A_i[j] -= B_i[j];
                }
            }
        }
        #endregion


        #region BLAS 2

        void IDenseBLAS2<float>.Add(float[][] A, float[][] B, float[][] C, int a_row_start, int a_col_start, int b_row_start, int b_col_start, int rows, int cols)
        {
            int i, j;
            if (a_row_start == 0 && a_col_start == 0)
            {
                int k;
                for (i = 0; i < rows; ++i)
                {
                    float[] A_i = A[i], B_i = B[b_row_start + i], C_i = C[i];
                    for (j = 0, k = b_col_start; j < cols; ++j, ++k)
                    {
                        C_i[j] = A_i[j] + B_i[k];
                    }
                }
            }
            else
            {
                int k, l;
                for (i = 0; i < rows; ++i)
                {
                    float[] A_i = A[a_row_start + i], B_i = B[b_row_start + i], C_i = C[i];
                    for (j = 0, k = a_col_start, l = b_col_start; j < cols; ++j, ++k, ++l)
                    {
                        C_i[j] = A_i[k] + B_i[l];
                    }
                }
            }
        }

        void IDenseBLAS2<float>.Copy(float[][] dest, float[][] src, int rows, int cols)
        {
            _functions.Copy(dest, src, rows, cols);
        }

        void IDenseBLAS2<float>.Copy(float[][] dest, float[][] src, int src_row_start, int src_col_start, int rows, int cols)
        {
            _functions.Copy(dest, src, src_row_start, src_col_start, rows, cols);
        }

        void IDenseBLAS2<float>.Copy(float[][] dest, float[][] src, int dest_row_start, int dest_col_start, int src_row_start, int src_col_start, int rows, int cols)
        {
            _functions.Copy(dest, src, dest_row_start, dest_col_start, src_row_start, src_col_start, rows, cols);
        }

        void IDenseBLAS2<float>.Subtract(float[][] A, float[][] B, float[][] C, int a_row_start, int a_col_start, int b_row_start, int b_col_start, int rows, int cols)
        {
            float[] C_r, A_r, B_r;

            int r, c;
            if (a_row_start == 0 && a_col_start == 0)
            {
                if (b_row_start == 0 && b_col_start == 0)
                {
                    for (r = 0; r < rows; ++r)
                    {
                        C_r = C[r];
                        A_r = A[r];
                        B_r = B[r];
                        for (c = 0; c < cols; ++c)
                        {
                            C_r[c] = A_r[c] - B_r[c];
                        }
                    }
                }
                else
                {
                    for (r = 0; r < rows; ++r)
                    {
                        C_r = C[r];
                        A_r = A[r];
                        B_r = B[b_row_start + r];
                        for (c = 0; c < cols; ++c)
                        {
                            C_r[c] = A_r[c] - B_r[b_col_start + c];
                        }
                    }
                }
            }
            else
            {
                for (r = 0; r < rows; ++r)
                {
                    C_r = C[r];
                    A_r = A[a_row_start + r];
                    B_r = B[b_row_start + r];
                    for (c = 0; c < cols; ++c)
                    {
                        C_r[c] = A_r[a_col_start + c] - B_r[b_col_start + c];
                    }
                }
            }
        }

        void IDenseBLAS2<float>.Increment(float[][] A, float[][] B, int rows, int cols) => Increment(A, B, rows, cols);

        void IDenseBLAS2<float>.Increment(float[][] A, float[][] B, int a_row_start, int a_col_start, int rows, int cols)
        {
            int i, j;
            if (a_row_start == 0 && a_col_start == 0)
            {
                Increment(A, B, rows, cols);
            }
            else if (a_col_start == 0)
            {
                for (i = 0; i < rows; ++i)
                {
                    float[] A_i = A[a_row_start + i], B_i = B[i];
                    for (j = 0; j < cols; ++j)
                    {
                        A_i[j] += B_i[j];
                    }
                }
            }
            else
            {
                int k;
                for (i = 0; i < rows; ++i)
                {
                    float[] A_i = A[a_row_start + i], B_i = B[i];
                    for (j = 0, k = a_col_start; j < cols; ++j, ++k)
                    {
                        A_i[k] += B_i[j];
                    }
                }
            }
        }

        void IDenseBLAS2<float>.Decrement(float[][] A, float[][] B, int rows, int cols) => Decrement(A, B, rows, cols);

        void IDenseBLAS2<float>.Decrement(float[][] A, float[][] B, int a_row_start, int a_col_start, int rows, int cols)
        {
            if (a_row_start == 0 && a_col_start == 0)
            {
                Decrement(A, B, rows, cols);
            }
            else
            {
                int i, j;
                for (i = 0; i < rows; ++i)
                {
                    float[] A_i = A[a_row_start + i], B_i = B[i];
                    for (j = 0; j < cols; ++j)
                    {
                        A_i[a_col_start + j] -= B_i[j];
                    }
                }
            }
        }

        void IDenseBLAS2<float>.Negate(float[][] A, float[][] result, int a_row_start, int a_col_start, int result_row_start, int result_col_start, int rows, int cols)
        {
            int i, j;
            for (i = 0; i < rows; ++i)
            {
                float[] A_i = A[a_row_start + i], result_i = result[result_row_start + i];
                for (j = 0; j < cols; ++j)
                {
                    result_i[result_col_start + j] = -A_i[a_col_start + j];
                }
            }
        }

        #endregion


    }
}
