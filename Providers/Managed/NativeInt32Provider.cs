﻿using LinearNet.Structs;
using System;
using System.Collections.Generic;

namespace LinearNet.Providers.LinearAlgebra
{
    /// <summary>
    /// 
    /// </summary>
    /// <cat>linear-algebra</cat>
    public sealed class NativeInt32Provider : IDenseBLAS1<int>, IDenseBLAS2<int>
    {
        private readonly Int32Provider _provider;
        private readonly ManagedFunctions<int> _functions;

        public IProvider<int> Provider => _provider;

        internal NativeInt32Provider()
        {
            _provider = new Int32Provider();
            _functions = new ManagedFunctions<int>();
        }

        #region BLAS 1

        #region Dense
        void IDenseBLAS1<int>.ADD(int[] u, int[] v, int[] sum, int start, int end)
        {
            for (int i = start; i < end; ++i)
            {
                sum[i] = u[i] + v[i];
            }
        }
        void IDenseBLAS1<int>.SUB(int[] u, int[] v, int[] diff, int start, int end)
        {
            for (int i = start; i < end; ++i)
            {
                diff[i] = u[i] - v[i];
            }
        }
        void IDenseBLAS1<int>.AXPY(int[] y, int[] x, int alpha, int start, int end)
        {
            for (int i = start; i < end; ++i)
            {
                y[i] += alpha * x[i];
            }
        }
        void IDenseBLAS1<int>.AXPY(int[] y, int[] x, int alpha, int y_start, int y_end, int xy_offset)
        {
            for (int i = y_start; i < y_end; ++i)
            {
                y[i] += alpha * x[i + xy_offset];
            }
        }
        int IDenseBLAS1<int>.DOT(int[] u, int[] v, int start, int end)
        {
            int sum = 0;
            for (int i = start; i < end; ++i)
            {
                sum += u[i] * v[i];
            }
            return sum;
        }
        int IDenseBLAS1<int>.DOT(int[] u, int[] v, int u_start, int u_end, int v_u_offset)
        {
            int sum = 0;
            for (int i = u_start; i < u_end; ++i)
            {
                sum += u[i] * v[i + v_u_offset];
            }
            return sum;
        }
        void IDenseBLAS1<int>.SCAL(int[] x, int s, int start, int end)
        {
            for (int i = start; i < end; ++i)
            {
                x[i] *= s;
            }
        }
        int IDenseBLAS1<int>.XTA(int[] x, int[][] A, int column, int start, int end)
        {
            int prod = 0;
            for (int i = start; i < end; ++i)
            {
                prod += x[i] * A[i][column];
            }
            return prod;
        }
        int IDenseBLAS1<int>.XTA(int[] x, int[][] A, int column, int x_start, int x_end, int Ax_offset)
        {
            int prod = 0;
            for (int i = x_start; i < x_end; ++i)
            {
                prod += x[i] * A[i + Ax_offset][column];
            }
            return prod;
        }
        void IDenseBLAS1<int>.ROTR(int[][] A, int i, int j, int c, int s, int col_start_incl, int col_end_excl)
        {
            // only alter the rows i and j of matrix A
            int[] A_i = A[i], A_j = A[j];
            for (int k = col_start_incl; k < col_end_excl; ++k)
            {
                int A_ik = A_i[k], A_jk = A_j[k];
                A_i[k] = c * A_ik + s * A_jk;
                A_j[k] = c * A_jk - s * A_ik;
            }
        }
        void IDenseBLAS1<int>.ROTC(int[][] A, int i, int j, int c, int s, int row_start_incl, int row_end_excl)
        {
            // only alter the rows i and j of matrix A
            for (int k = row_start_incl; k < row_end_excl; ++k)
            {
                int[] A_k = A[k];
                int A_ki = A_k[i], A_kj = A_k[j];
                A_k[j] = c * A_kj - s * A_ki;
                A_k[i] = c * A_ki + s * A_kj;
            }
        }
        void IDenseBLAS1<int>.PMULT(int[] u, int[] v, int[] uv, int u_start, int u_end, int v_u_offset, int uv_u_offset)
        {
            for (int i = u_start; i < u_end; ++i)
            {
                uv[i + uv_u_offset] += u[i] * v[i + v_u_offset];
            }
        }
        #endregion

        #region Sparse
        void AXPY(Dictionary<long, int> y, Dictionary<long, int> x, int alpha, long start, long end)
        {
            foreach (KeyValuePair<long, int> pair in x)
            {
                long index = pair.Key;
                if (index >= start && index < end)
                {
                    if (y.ContainsKey(index))
                    {
                        y[index] += alpha * pair.Value;
                    }
                    else
                    {
                        y[index] = alpha * pair.Value;
                    }
                }
            }
        }
        int SUMPROD(Dictionary<long, int> u, Dictionary<long, int> v, long start, long end)
        {
            int sum = 0;
            foreach (KeyValuePair<long, int> pair in u)
            {
                long index = pair.Key;
                if (index >= start && index < end && v.ContainsKey(index))
                {
                    sum += pair.Value * v[index];
                }
            }
            return sum;
        }
        void SCAL(Dictionary<long, int> x, int s, long start, long end)
        {
            foreach (long key in x.Keys)
            {
                x[key] *= s;
            }
        }
        void ROTR(Dictionary<long, double> row1, Dictionary<long, double> row2, double c, double s, long col_start_incl, long col_end_excl)
        {
            throw new NotImplementedException();
        }

        void AXPY(SparseVector<int> y, SparseVector<int> x, int alpha, int start, int end)
        {
            throw new NotImplementedException();
        }
        int DOT(SparseVector<int> u, SparseVector<int> v, int start, int end)
        {
            throw new NotImplementedException();
        }

        int ADD(int[] xi, int[] x, int[] yi, int[] y, int[] zi, int[] z, int x_start_index, int y_start_index, int z_start_index, int x_end_index, int y_end_index)
        {
            throw new NotImplementedException();
        }
        #endregion

        #endregion


        #region Internal 
        internal void Increment(int[][] A, int[][] B, int rows, int cols)
        {
            int i, j;
            for (i = 0; i < rows; ++i)
            {
                int[] A_i = A[i], B_i = B[i];
                for (j = 0; j < cols; ++j)
                {
                    A_i[j] += B_i[j];
                }
            }
        }
        internal void Decrement(int[][] A, int[][] B, int rows, int cols)
        {
            int i, j;
            for (i = 0; i < rows; ++i)
            {
                int[] A_i = A[i], B_i = B[i];
                for (j = 0; j < cols; ++j)
                {
                    A_i[j] -= B_i[j];
                }
            }
        }
        #endregion


        #region BLAS 2

        void IDenseBLAS2<int>.Add(int[][] A, int[][] B, int[][] C, int a_row_start, int a_col_start, int b_row_start, int b_col_start, int rows, int cols)
        {
            int i, j;
            if (a_row_start == 0 && a_col_start == 0)
            {
                int k;
                for (i = 0; i < rows; ++i)
                {
                    int[] A_i = A[i], B_i = B[b_row_start + i], C_i = C[i];
                    for (j = 0, k = b_col_start; j < cols; ++j, ++k)
                    {
                        C_i[j] = A_i[j] + B_i[k];
                    }
                }
            }
            else
            {
                int k, l;
                for (i = 0; i < rows; ++i)
                {
                    int[] A_i = A[a_row_start + i], B_i = B[b_row_start + i], C_i = C[i];
                    for (j = 0, k = a_col_start, l = b_col_start; j < cols; ++j, ++k, ++l)
                    {
                        C_i[j] = A_i[k] + B_i[l];
                    }
                }
            }
        }

        void IDenseBLAS2<int>.Copy(int[][] dest, int[][] src, int rows, int cols)
        {
            _functions.Copy(dest, src, rows, cols);
        }

        void IDenseBLAS2<int>.Copy(int[][] dest, int[][] src, int src_row_start, int src_col_start, int rows, int cols)
        {
            _functions.Copy(dest, src, src_row_start, src_col_start, rows, cols);
        }

        void IDenseBLAS2<int>.Copy(int[][] dest, int[][] src, int dest_row_start, int dest_col_start, int src_row_start, int src_col_start, int rows, int cols)
        {
            _functions.Copy(dest, src, dest_row_start, dest_col_start, src_row_start, src_col_start, rows, cols);
        }

        void IDenseBLAS2<int>.Subtract(int[][] A, int[][] B, int[][] C, int a_row_start, int a_col_start, int b_row_start, int b_col_start, int rows, int cols)
        {
            int r, c;

            int[] C_r, A_r, B_r;

            if (a_row_start == 0 && a_col_start == 0)
            {
                if (b_row_start == 0 && b_col_start == 0)
                {
                    for (r = 0; r < rows; ++r)
                    {
                        C_r = C[r];
                        A_r = A[r];
                        B_r = B[r];
                        for (c = 0; c < cols; ++c)
                        {
                            C_r[c] = A_r[c] - B_r[c];
                        }
                    }
                }
                else
                {
                    for (r = 0; r < rows; ++r)
                    {
                        C_r = C[r];
                        A_r = A[r];
                        B_r = B[b_row_start + r];
                        for (c = 0; c < cols; ++c)
                        {
                            C_r[c] = A_r[c] - B_r[b_col_start + c];
                        }
                    }
                }
            }
            else
            {
                for (r = 0; r < rows; ++r)
                {
                    C_r = C[r];
                    A_r = A[a_row_start + r];
                    B_r = B[b_row_start + r];
                    for (c = 0; c < cols; ++c)
                    {
                        C_r[c] = A_r[a_col_start + c] - B_r[b_col_start + c];
                    }
                }
            }
        }

        void IDenseBLAS2<int>.Increment(int[][] A, int[][] B, int rows, int cols) => Increment(A, B, rows, cols);

        void IDenseBLAS2<int>.Increment(int[][] A, int[][] B, int a_row_start, int a_col_start, int rows, int cols)
        {
            int i, j;
            if (a_row_start == 0 && a_col_start == 0)
            {
                Increment(A, B, a_row_start, a_col_start);
            }
            else if (a_col_start == 0)
            {
                for (i = 0; i < rows; ++i)
                {
                    int[] A_i = A[a_row_start + i], B_i = B[i];
                    for (j = 0; j < cols; ++j)
                    {
                        A_i[j] += B_i[j];
                    }
                }
            }
            else
            {
                int k;
                for (i = 0; i < rows; ++i)
                {
                    int[] A_i = A[a_row_start + i], B_i = B[i];
                    for (j = 0, k = a_col_start; j < cols; ++j, ++k)
                    {
                        A_i[k] += B_i[j];
                    }
                }
            }
        }

        void IDenseBLAS2<int>.Decrement(int[][] A, int[][] B, int rows, int cols) => Decrement(A, B, rows, cols);

        void IDenseBLAS2<int>.Decrement(int[][] A, int[][] B, int a_row_start, int a_col_start, int rows, int cols)
        {
            if (a_row_start == 0 && a_col_start == 0)
            {
                Decrement(A, B, a_row_start, a_col_start);
            }
            else
            {
                int i, j;
                for (i = 0; i < rows; ++i)
                {
                    int[] A_i = A[a_row_start + i], B_i = B[i];
                    for (j = 0; j < cols; ++j)
                    {
                        A_i[a_col_start + j] -= B_i[j];
                    }
                }
            }
        }

        void IDenseBLAS2<int>.Negate(int[][] A, int[][] result, int a_row_start, int a_col_start, int result_row_start, int result_col_start, int rows, int cols)
        {
            int i, j;
            for (i = 0; i < rows; ++i)
            {
                int[] A_i = A[a_row_start + i], result_i = result[result_row_start + i];
                for (j = 0; j < cols; ++j)
                {
                    result_i[result_col_start + j] = -A_i[a_col_start + j];
                }
            }
        }

        #endregion
    }
}
