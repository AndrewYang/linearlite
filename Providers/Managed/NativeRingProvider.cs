﻿using LinearNet.Matrices;
using LinearNet.Structs;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace LinearNet.Providers.LinearAlgebra
{
    /// <summary>
    /// 
    /// </summary>
    /// <cat>linear-algebra</cat>
    public class NativeRingProvider<T> : IDenseBLAS1<T>, IDenseBLAS2<T> where T : IRing<T>, new()
    {
        private readonly RingProvider<T> _provider;
        private readonly ManagedFunctions<T> _functions;

        public IProvider<T> Provider => _provider;

        public NativeRingProvider()
        {
            _provider = new RingProvider<T>();
            _functions = new ManagedFunctions<T>();
        }

        #region Internal methods
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        internal void AXPY(T[] y, T[] x, T alpha, int start, int end)
        {
            for (int i = start; i < end; ++i)
            {
                y[i] = y[i].Add(alpha.Multiply(x[i]));
            }
        }
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        internal void AXPY(T[] y, T[] x, T alpha, int y_start, int y_end, int xy_offset)
        {
            for (int i = y_start; i < y_end; ++i)
            {
                y[i] = y[i].Add(alpha.Multiply(x[i + xy_offset]));
            }
        }
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        internal T DOT(T[] u, T[] v, int start, int end)
        {
            T sum = _provider.Zero;
            for (int i = start; i < end; ++i)
            {
                sum = sum.Add(u[i].Multiply(v[i]));
            }
            return sum;
        }
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        internal T DOT(T[] u, T[] v, int u_start, int u_end, int v_u_offset)
        {
            T sum = _provider.Zero;
            for (int i = u_start; i < u_end; ++i)
            {
                sum = sum.Add(u[i].Multiply(v[i + v_u_offset]));
            }
            return sum;
        }
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        internal void SCAL(T[] x, T s, int start, int end)
        {
            for (int i = start; i < end; ++i)
            {
                x[i] = x[i].Multiply(s);
            }
        }
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        internal T XTA(T[] x, T[][] A, int column, int start, int end)
        {
            T prod = _provider.Zero;
            for (int i = start; i < end; ++i)
            {
                prod = prod.Add(x[i].Multiply(A[i][column]));
            }
            return prod;
        }
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        internal T XTA(T[] x, T[][] A, int column, int x_start, int x_end, int Ax_offset)
        {
            T prod = _provider.Zero;
            for (int i = x_start; i < x_end; ++i)
            {
                prod = prod.Add(x[i].Multiply(A[i + Ax_offset][column]));
            }
            return prod;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        internal void AXPY(Dictionary<long, T> y, Dictionary<long, T> x, T alpha, long start, long end)
        {
            foreach (KeyValuePair<long, T> pair in x)
            {
                long index = pair.Key;
                if (index >= start && index < end)
                {
                    if (y.ContainsKey(index))
                    {
                        y[index] = y[index].Add(alpha.Multiply(pair.Value));
                    }
                    else
                    {
                        y[index] = alpha.Multiply(pair.Value);
                    }
                }
            }
        }
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        internal T DOT(Dictionary<long, T> u, Dictionary<long, T> v, long start, long end)
        {
            T sum = _provider.Zero;
            foreach (KeyValuePair<long, T> pair in u)
            {
                long index = pair.Key;
                if (index >= start && index < end && v.ContainsKey(index))
                {
                    sum = sum.Add(pair.Value.Multiply(v[index]));
                }
            }
            return sum;
        }
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        internal void SCAL(Dictionary<long, T> x, T s, long start, long end)
        {
            foreach (long key in x.Keys)
            {
                if (key <= start && key < end)
                {
                    x[key] = x[key].Multiply(s);
                }
            }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        internal void GivensRotateRows(T[][] A, int i, int j, T c, T s, int col_start_incl, int col_end_excl)
        {
            // only alter the rows i and j of matrix A
            T[] A_i = A[i], A_j = A[j];
            T minusS = s.AdditiveInverse();
            for (int k = col_start_incl; k < col_end_excl; ++k)
            {
                T A_ik = A_i[k], A_jk = A_j[k];
                A_i[k] = c.Multiply(A_ik).Add(s.Multiply(A_jk));
                A_j[k] = c.Multiply(A_jk).Add(minusS.Multiply(A_ik));
            }
        }
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        internal void GivensRotateColumns(T[][] A, int i, int j, T c, T s, int row_start_incl, int row_end_excl)
        {
            // only alter the columns i and j of matrix A
            T minusS = s.AdditiveInverse();
            for (int k = row_start_incl; k < row_end_excl; ++k)
            {
                T[] A_k = A[k];
                T A_ki = A_k[i], A_kj = A_k[j];
                A_k[j] = c.Multiply(A_kj).Add(minusS.Multiply(A_ki));
                A_k[i] = c.Multiply(A_ki).Add(s.Multiply(A_kj));
            }
        }
        #endregion


        #region BLAS 1

        #region Dense
        void IDenseBLAS1<T>.ADD(T[] u, T[] v, T[] sum, int start, int end)
        {
            for (int i = start; i < end; ++i)
            {
                sum[i] = u[i].Add(v[i]);
            }
        }
        void IDenseBLAS1<T>.SUB(T[] u, T[] v, T[] diff, int start, int end)
        {
            for (int i = start; i < end; ++i)
            {
                diff[i] = u[i].Subtract(v[i]);
            }
        }
        void IDenseBLAS1<T>.AXPY(T[] y, T[] x, T alpha, int start, int end) => AXPY(y, x, alpha, start, end);
        void IDenseBLAS1<T>.AXPY(T[] y, T[] x, T alpha, int y_start, int y_end, int xy_offset) => AXPY(y, x, alpha, y_start, y_end);
        T IDenseBLAS1<T>.DOT(T[] u, T[] v, int start, int end) => DOT(u, v, start, end);
        T IDenseBLAS1<T>.DOT(T[] u, T[] v, int u_start, int u_end, int v_u_offset) => DOT(u, v, u_start, u_end, v_u_offset);
        T IDenseBLAS1<T>.XTA(T[] x, T[][] A, int column, int start, int end) => XTA(x, A, column, start, end);
        T IDenseBLAS1<T>.XTA(T[] x, T[][] A, int column, int x_start, int x_end, int Ax_offset) => XTA(x, A, column, x_start, x_end, Ax_offset);
        void IDenseBLAS1<T>.SCAL(T[] x, T s, int start, int end) => SCAL(x, s, start, end);
        void IDenseBLAS1<T>.ROTR(T[][] A, int i, int j, T c, T s, int col_start_incl, int col_end_excl) => GivensRotateRows(A, i, j, c, s, col_start_incl, col_end_excl);
        void IDenseBLAS1<T>.ROTC(T[][] A, int i, int j, T c, T s, int row_start_incl, int row_end_excl) => GivensRotateColumns(A, i, j, c, s, row_start_incl, row_end_excl);
        void IDenseBLAS1<T>.PMULT(T[] u, T[] v, T[] uv, int u_start, int u_end, int v_u_offset, int uv_u_offset)
        {
            for (int i = u_start; i < u_end; ++i)
            {
                uv[i + uv_u_offset] = uv[i].Add(u[i].Multiply(v[i + v_u_offset]));
            }
        }
        #endregion

        #region Sparse
        T SUMPROD(Dictionary<long, T> u, Dictionary<long, T> v, long start, long end) => DOT(u, v, start, end);
        void ROTR(Dictionary<long, double> row1, Dictionary<long, double> row2, double c, double s, long col_start_incl, long col_end_excl)
        {
            throw new NotImplementedException();
        }

        void AXPY(SparseVector<T> y, SparseVector<T> x, T alpha, int start, int end)
        {
            throw new NotImplementedException();
        }
        T DOT(SparseVector<T> u, SparseVector<T> v, int start, int end)
        {
            throw new NotImplementedException();
        }

        int ADD(int[] xi, T[] x, int[] yi, T[] y, int[] zi, T[] z, int x_start_index, int y_start_index, int z_start_index, int x_end_index, int y_end_index)
        {
            throw new NotImplementedException();
        }
        #endregion

        #endregion

        #region Internal 
        internal void Increment(T[][] A, T[][] B, int rows, int cols)
        {
            int i, j;
            for (i = 0; i < rows; ++i)
            {
                T[] A_i = A[i], B_i = B[i];
                for (j = 0; j < cols; ++j)
                {
                    A_i[j] = A_i[j].Add(B_i[j]);
                }
            }
        }
        internal void Decrement(T[][] A, T[][] B, int rows, int cols)
        {
            int i, j;
            for (i = 0; i < rows; ++i)
            {
                T[] A_i = A[i], B_i = B[i];
                for (j = 0; j < cols; ++j)
                {
                    A_i[j] = A_i[j].Subtract(B_i[j]);
                }
            }
        }
        #endregion

        #region BLAS 2

        void IDenseBLAS2<T>.Add(T[][] A, T[][] B, T[][] C, int a_row_start, int a_col_start, int b_row_start, int b_col_start, int rows, int cols)
        {
            int i, j;
            if (a_row_start == 0 && a_col_start == 0)
            {
                int k;
                for (i = 0; i < rows; ++i)
                {
                    T[] A_i = A[i], B_i = B[b_row_start + i], C_i = C[i];
                    for (j = 0, k = b_col_start; j < cols; ++j, ++k)
                    {
                        C_i[j] = A_i[j].Add(B_i[k]);
                    }
                }
            }
            else
            {
                int k, l;
                for (i = 0; i < rows; ++i)
                {
                    T[] A_i = A[a_row_start + i], B_i = B[b_row_start + i], C_i = C[i];
                    for (j = 0, k = a_col_start, l = b_col_start; j < cols; ++j, ++k, ++l)
                    {
                        C_i[j] = A_i[k].Add(B_i[l]);
                    }
                }
            }
        }

        void IDenseBLAS2<T>.Copy(T[][] dest, T[][] src, int rows, int cols)
        {
            _functions.Copy(dest, src, rows, cols);
        }

        void IDenseBLAS2<T>.Copy(T[][] dest, T[][] src, int src_row_start, int src_col_start, int rows, int cols)
        {
            _functions.Copy(dest, src, src_row_start, src_col_start, rows, cols);
        }

        void IDenseBLAS2<T>.Copy(T[][] dest, T[][] src, int dest_row_start, int dest_col_start, int src_row_start, int src_col_start, int rows, int cols)
        {
            _functions.Copy(dest, src, dest_row_start, dest_col_start, src_row_start, src_col_start, rows, cols);
        }

        void IDenseBLAS2<T>.Subtract(T[][] A, T[][] B, T[][] C, int a_row_start, int a_col_start, int b_row_start, int b_col_start, int rows, int cols)
        {
            T[] C_r, A_r, B_r;

            int r, c;
            if (a_row_start == 0 && a_col_start == 0)
            {
                if (b_row_start == 0 && b_col_start == 0)
                {
                    for (r = 0; r < rows; ++r)
                    {
                        C_r = C[r];
                        A_r = A[r];
                        B_r = B[r];
                        for (c = 0; c < cols; ++c)
                        {
                            C_r[c] = A_r[c].Subtract(B_r[c]);
                        }
                    }
                }
                else
                {
                    for (r = 0; r < rows; ++r)
                    {
                        C_r = C[r];
                        A_r = A[r];
                        B_r = B[b_row_start + r];
                        for (c = 0; c < cols; ++c)
                        {
                            C_r[c] = A_r[c].Subtract(B_r[b_col_start + c]);
                        }
                    }
                }
            }
            else
            {
                for (r = 0; r < rows; ++r)
                {
                    C_r = C[r];
                    A_r = A[a_row_start + r];
                    B_r = B[b_row_start + r];
                    for (c = 0; c < cols; ++c)
                    {
                        C_r[c] = A_r[a_col_start + c].Subtract(B_r[b_col_start + c]);
                    }
                }
            }
        }

        void IDenseBLAS2<T>.Increment(T[][] A, T[][] B, int rows, int cols) => Increment(A, B, rows, cols);

        void IDenseBLAS2<T>.Increment(T[][] A, T[][] B, int a_row_start, int a_col_start, int rows, int cols)
        {
            int i, j;

            if (a_row_start == 0 && a_col_start == 0)
            {
                Increment(A, B, rows, cols);
            }
            else if (a_col_start == 0)
            {
                for (i = 0; i < rows; ++i)
                {
                    T[] A_i = A[a_row_start + i], B_i = B[i];
                    for (j = 0; j < cols; ++j)
                    {
                        A_i[j] = A_i[j].Add(B_i[j]);
                    }
                }
            }
            else
            {
                int k;
                for (i = 0; i < rows; ++i)
                {
                    T[] A_i = A[a_row_start + i], B_i = B[i];
                    for (j = 0, k = a_col_start; j < cols; ++j, ++k)
                    {
                        A_i[k] = A_i[k].Add(B_i[j]);
                    }
                }
            }
        }

        void IDenseBLAS2<T>.Decrement(T[][] A, T[][] B, int rows, int cols) => Decrement(A, B, rows, cols);

        void IDenseBLAS2<T>.Decrement(T[][] A, T[][] B, int a_row_start, int a_col_start, int rows, int cols)
        {
            int i, j;
            if (a_row_start == 0 && a_col_start == 0)
            {
                Decrement(A, B, rows, cols);
            }
            else
            {
                int k;
                for (i = 0; i < rows; ++i)
                {
                    T[] A_i = A[a_row_start + i], B_i = B[i];
                    for (j = 0, k = a_col_start; j < cols; ++j, ++k)
                    {
                        A_i[k] = A_i[k].Subtract(B_i[j]);
                    }
                }
            }
        }

        void IDenseBLAS2<T>.Negate(T[][] A, T[][] result, int a_row_start, int a_col_start, int result_row_start, int result_col_start, int rows, int cols)
        {
            int i, j;
            for (i = 0; i < rows; ++i)
            {
                T[] A_i = A[a_row_start + i], result_i = result[result_row_start + i];
                for (j = 0; j < cols; ++j)
                {
                    result_i[result_col_start + j] = A_i[a_col_start + j].AdditiveInverse();
                }
            }
        }

        #endregion
    }
}
