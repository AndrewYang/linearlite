﻿using LinearNet.Structs;
using System;
using System.Collections.Generic;
using System.Text;

namespace LinearNet.Providers.Managed.Parallel
{
    /// <summary>
    /// Level 2 BLAS provider using parallel methods in managed environment.
    /// </summary>
    /// <cat>linear-algebra</cat>
    public class NativeParallelComplexProvider : IDenseBLAS2<Complex>
    {
        IProvider<Complex> IDenseBLAS2<Complex>.Provider => throw new NotImplementedException();

        /// <summary>
        /// Create a new instance of a <txt>Complex</txt> parallel provider.
        /// </summary>
        public NativeParallelComplexProvider()
        {

        }

        void IDenseBLAS2<Complex>.Add(Complex[][] A, Complex[][] B, Complex[][] C, int a_row_start, int a_col_start, int b_row_start, int b_col_start, int rows, int cols)
        {
            throw new NotImplementedException();
        }

        void IDenseBLAS2<Complex>.Copy(Complex[][] dest, Complex[][] src, int rows, int cols)
        {
            throw new NotImplementedException();
        }

        void IDenseBLAS2<Complex>.Copy(Complex[][] dest, Complex[][] src, int src_row_start, int src_col_start, int rows, int cols)
        {
            throw new NotImplementedException();
        }

        void IDenseBLAS2<Complex>.Copy(Complex[][] dest, Complex[][] src, int dest_row_start, int dest_col_start, int src_row_start, int src_col_start, int rows, int cols)
        {
            throw new NotImplementedException();
        }

        void IDenseBLAS2<Complex>.Decrement(Complex[][] A, Complex[][] B, int rows, int cols)
        {
            throw new NotImplementedException();
        }

        void IDenseBLAS2<Complex>.Decrement(Complex[][] A, Complex[][] B, int a_row_start, int a_col_start, int rows, int cols)
        {
            throw new NotImplementedException();
        }

        void IDenseBLAS2<Complex>.Increment(Complex[][] A, Complex[][] B, int rows, int cols)
        {
            throw new NotImplementedException();
        }

        void IDenseBLAS2<Complex>.Increment(Complex[][] A, Complex[][] B, int a_row_start, int a_col_start, int rows, int cols)
        {
            throw new NotImplementedException();
        }

        void IDenseBLAS2<Complex>.Negate(Complex[][] A, Complex[][] result, int a_row_start, int a_col_start, int result_row_start, int result_col_start, int rows, int cols)
        {
            throw new NotImplementedException();
        }

        void IDenseBLAS2<Complex>.Subtract(Complex[][] A, Complex[][] B, Complex[][] C, int a_row_start, int a_col_start, int b_row_start, int b_col_start, int rows, int cols)
        {
            throw new NotImplementedException();
        }
    }
}
