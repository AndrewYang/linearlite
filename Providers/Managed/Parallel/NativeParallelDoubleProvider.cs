﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Providers.Managed.Parallel
{
    /// <summary>
    /// <p>Level 1 and 2 BLAS provider using CPU parallelization in managed environment, for primitive type <txt>double</txt>.</p>
    /// <p>Although this class implements BLAS-1 methods, it is recommended to instead use higher-level BLAS methods whenever 
    /// possible due to parallelization overhead.</p>
    /// </summary>
    /// <cat>linear-algebra</cat>
    public class NativeParallelDoubleProvider : IDenseBLAS1<double>, IDenseBLAS2<double>
    {
        private readonly ParallelOptions _options;
        private readonly DoubleProvider _provider;
        private readonly ManagedFunctions<double> _functions;
        private readonly int _rangeSize;

        public IProvider<double> Provider => _provider;

        /// <summary>
        /// Create a new instance of the parallel provider, with the default parallelization options.
        /// </summary>
        public NativeParallelDoubleProvider() : this(new ParallelOptions()) { }

        /// <summary>
        /// Create a new instance of the provider given parallelization options and the range size.
        /// </summary>
        /// <param name="options">Parallelization options for this provider.</param>
        /// <param name="rangeSize">The atomic block size in which work units are to be divided.</param>
        public NativeParallelDoubleProvider(ParallelOptions options, int rangeSize = 1)
        {
            _options = options;
            _provider = new DoubleProvider();
            _functions = new ManagedFunctions<double>();
            _rangeSize = rangeSize;
        }

        #region BLAS 1

        void IDenseBLAS1<double>.ADD(double[] u, double[] v, double[] sum, int start, int end)
        {
            System.Threading.Tasks.Parallel.ForEach(Partitioner.Create(start, end, _rangeSize), _options, range =>
            {
                int min = range.Item1, max = range.Item2, i;
                for (i = min; i < max; ++i)
                {
                    sum[i] = u[i] + v[i];
                }
            });
        }
        void IDenseBLAS1<double>.SUB(double[] u, double[] v, double[] diff, int start, int end)
        {
            System.Threading.Tasks.Parallel.ForEach(Partitioner.Create(start, end, _rangeSize), _options, range =>
            {
                int min = range.Item1, max = range.Item2, i;
                for (i = min; i < max; ++i)
                {
                    diff[i] = u[i] - v[i];
                }
            });
        }
        void IDenseBLAS1<double>.AXPY(double[] y, double[] x, double alpha, int start, int end)
        {
            System.Threading.Tasks.Parallel.ForEach(Partitioner.Create(start, end, _rangeSize), _options, range =>
            {
                int min = range.Item1, max = range.Item2, i;
                for (i = min; i < max; ++i)
                {
                    y[i] += alpha * x[i];
                }
            });
        }
        void IDenseBLAS1<double>.AXPY(double[] y, double[] x, double alpha, int y_start, int y_end, int xy_offset)
        {
            System.Threading.Tasks.Parallel.ForEach(Partitioner.Create(y_start, y_end, _rangeSize), _options, range =>
            {
                int min = range.Item1, max = range.Item2, i;
                for (i = min; i < max; ++i)
                {
                    y[i] += alpha * x[i + xy_offset];
                }
            });
        }
        double IDenseBLAS1<double>.DOT(double[] u, double[] v, int start, int end)
        {
            throw new NotImplementedException();
        }
        double IDenseBLAS1<double>.DOT(double[] u, double[] v, int u_start, int u_end, int v_u_offset)
        {
            throw new NotImplementedException();
        }
        void IDenseBLAS1<double>.ROTC(double[][] A, int i, int j, double c, double s, int row_start_incl, int row_end_excl)
        {
            System.Threading.Tasks.Parallel.ForEach(Partitioner.Create(row_start_incl, row_end_excl, _rangeSize), _options, range =>
            {
                int min = range.Item1, max = range.Item2, k;
                for (k = min; k < max; ++k)
                {
                    double[] A_k = A[k];
                    double A_ki = A_k[i], A_kj = A_k[j];
                    A_k[i] = c * A_ki + s * A_kj;
                    A_k[j] = c * A_kj - s * A_ki;
                }
            });
        }
        void IDenseBLAS1<double>.ROTR(double[][] A, int i, int j, double c, double s, int col_start_incl, int col_end_excl)
        {
            double[] A_i = A[i], A_j = A[j];
            System.Threading.Tasks.Parallel.ForEach(Partitioner.Create(col_start_incl, col_end_excl, _rangeSize), _options, range =>
            {
                int min = range.Item1, max = range.Item2, k;
                for (k = min; k < max; ++k)
                {
                    double A_ik = A_i[k], A_jk = A_j[k];
                    A_i[k] = c * A_ik + s * A_jk;
                    A_j[k] = c * A_jk - s * A_ik;
                }
            });
        }
        void IDenseBLAS1<double>.SCAL(double[] x, double s, int start, int end)
        {
            System.Threading.Tasks.Parallel.ForEach(Partitioner.Create(start, end, _rangeSize), _options, range =>
            {
                int min = range.Item1, max = range.Item2, i;
                for (i = min; i < max; ++i)
                {
                    x[i] *= s;
                }
            });
        }
        double IDenseBLAS1<double>.XTA(double[] x, double[][] A, int column, int start, int end)
        {
            throw new NotImplementedException();
        }
        double IDenseBLAS1<double>.XTA(double[] x, double[][] A, int column, int x_start, int x_end, int Ax_offset)
        {
            throw new NotImplementedException();
        }
        void IDenseBLAS1<double>.PMULT(double[] u, double[] v, double[] uv, int u_start, int u_end, int v_u_offset, int uv_u_offset)
        {
            System.Threading.Tasks.Parallel.ForEach(Partitioner.Create(u_start, u_end, _rangeSize), _options, range =>
            {
                int min = range.Item1, max = range.Item2, i;
                for (i = min; i < max; ++i)
                {
                    uv[i + uv_u_offset] += u[i] * v[i + v_u_offset];
                }
            });
        }
        #endregion

        #region Internal 
        internal void Increment(double[][] A, double[][] B, int rows, int cols)
        {
            System.Threading.Tasks.Parallel.ForEach(Partitioner.Create(0, rows, _rangeSize), _options, range =>
            {
                int min = range.Item1, max = range.Item2, i, j;
                for (i = min; i < max; ++i)
                {
                    double[] A_i = A[i], B_i = B[i];
                    for (j = 0; j < cols; ++j)
                    {
                        A_i[j] += B_i[j];
                    }
                }
            });
        }
        internal void Decrement(double[][] A, double[][] B, int rows, int cols)
        {
            System.Threading.Tasks.Parallel.ForEach(Partitioner.Create(0, rows, _rangeSize), _options, range =>
            {
                int min = range.Item1, max = range.Item2, i, j;
                for (i = min; i < max; ++i)
                {
                    double[] A_i = A[i], B_i = B[i];
                    for (j = 0; j < cols; ++j)
                    {
                        A_i[j] -= B_i[j];
                    }
                }
            });
        }
        #endregion

        #region BLAS 2
        void IDenseBLAS2<double>.Add(double[][] A, double[][] B, double[][] C, int a_row_start, int a_col_start, int b_row_start, int b_col_start, int rows, int cols)
        {
            if (a_row_start == 0 && a_col_start == 0)
            {
                System.Threading.Tasks.Parallel.ForEach(Partitioner.Create(0, rows, _rangeSize), _options, range =>
                {
                    int min = range.Item1, max = range.Item2, i, j, k;
                    for (i = min; i < max; ++i)
                    {
                        double[] A_i = A[i], B_i = B[b_row_start + i], result_i = C[i];
                        for (j = 0, k = b_col_start; j < cols; ++j, ++k)
                        {
                            result_i[j] = A_i[j] + B_i[k];
                        }
                    }
                });
            }
            else
            {
                System.Threading.Tasks.Parallel.ForEach(Partitioner.Create(0, rows, _rangeSize), _options, range =>
                {
                    int min = range.Item1, max = range.Item2, i, j, k, l;
                    for (i = min; i < max; ++i)
                    {
                        double[] A_i = A[a_row_start + i], B_i = B[b_row_start + i], result_i = C[i];
                        for (j = 0, k = a_col_start, l = b_col_start; j < cols; ++j, ++k, ++l)
                        {
                            result_i[j] = A_i[k] + B_i[l];
                        }
                    }
                });
            }
        }

        void IDenseBLAS2<double>.Copy(double[][] dest, double[][] src, int rows, int cols)
        {
            _functions.CopyParallel(dest, src, rows, cols, _rangeSize);
        }

        void IDenseBLAS2<double>.Copy(double[][] dest, double[][] src, int src_row_start, int src_col_start, int rows, int cols)
        {
            _functions.CopyParallel(dest, src, src_row_start, src_col_start, rows, cols, _rangeSize);
        }

        void IDenseBLAS2<double>.Copy(double[][] dest, double[][] src, int dest_row_start, int dest_col_start, int src_row_start, int src_col_start, int rows, int cols)
        {
            _functions.CopyParallel(dest, src, dest_row_start, dest_col_start, src_row_start, src_col_start, rows, cols, _rangeSize);
        }

        void IDenseBLAS2<double>.Subtract(double[][] A, double[][] B, double[][] C, int a_row_start, int a_col_start, int b_row_start, int b_col_start, int rows, int cols)
        {
            if (a_row_start == 0 && a_col_start == 0)
            {
                if (b_row_start == 0 && b_col_start == 0)
                {
                    System.Threading.Tasks.Parallel.ForEach(Partitioner.Create(0, rows, _rangeSize), _options, range =>
                    {
                        double[] result_r, A_r, B_r;

                        int min = range.Item1, max = range.Item2, r, c;
                        for (r = min; r < max; ++r)
                        {
                            result_r = C[r];
                            A_r = A[r];
                            B_r = B[r];
                            for (c = 0; c < cols; ++c)
                            {
                                result_r[c] = A_r[c] - B_r[c];
                            }
                        }
                    });
                }
                else
                {
                    System.Threading.Tasks.Parallel.ForEach(Partitioner.Create(0, rows, _rangeSize), _options, range =>
                    {
                        double[] result_r, A_r, B_r;

                        int min = range.Item1, max = range.Item2, r, c;
                        for (r = min; r < max; ++r)
                        {
                            result_r = C[r];
                            A_r = A[r];
                            B_r = B[b_row_start + r];
                            for (c = 0; c < cols; ++c)
                            {
                                result_r[c] = A_r[c] - B_r[b_col_start + c];
                            }
                        }
                    });
                }
            }
            else
            {
                System.Threading.Tasks.Parallel.ForEach(Partitioner.Create(0, rows, _rangeSize), range =>
                {
                    double[] result_r, A_r, B_r;

                    int min = range.Item1, max = range.Item2, r, c;
                    for (r = min; r < max; ++r)
                    {
                        result_r = C[r];
                        A_r = A[a_row_start + r];
                        B_r = B[b_row_start + r];
                        for (c = 0; c < cols; ++c)
                        {
                            result_r[c] = A_r[a_col_start + c] - B_r[b_col_start + c];
                        }
                    }
                });
            }
        }

        void IDenseBLAS2<double>.Increment(double[][] A, double[][] B, int rows, int cols) => Increment(A, B, rows, cols);

        void IDenseBLAS2<double>.Increment(double[][] A, double[][] B, int a_row_start, int a_col_start, int rows, int cols)
        {
            if (a_row_start == 0 && a_col_start == 0)
            {
                Increment(A, B, rows, cols);
            }
            else if (a_col_start == 0)
            {
                System.Threading.Tasks.Parallel.ForEach(Partitioner.Create(0, rows, _rangeSize), range =>
                {
                    int min = range.Item1, max = range.Item2, i, j;
                    for (i = min; i < max; ++i)
                    {
                        double[] A_i = A[a_row_start + i], B_i = B[i];
                        for (j = 0; j < cols; ++j)
                        {
                            A_i[j] += B_i[j];
                        }
                    }
                });
            }
            else
            {
                System.Threading.Tasks.Parallel.ForEach(Partitioner.Create(0, rows, _rangeSize), range =>
                {
                    int min = range.Item1, max = range.Item2, i, j, k;
                    for (i = min; i < max; ++i)
                    {
                        double[] A_i = A[a_row_start + i], B_i = B[i];
                        for (j = 0, k = a_col_start; j < cols; ++j, ++k)
                        {
                            A_i[k] += B_i[j];
                        }
                    }
                });
            }
        }

        void IDenseBLAS2<double>.Decrement(double[][] A, double[][] B, int rows, int cols) => Decrement(A, B, rows, cols);

        void IDenseBLAS2<double>.Decrement(double[][] A, double[][] B, int a_row_start, int a_col_start, int rows, int cols)
        {
            if (a_row_start == 0 && a_col_start == 0)
            {
                Decrement(A, B, rows, cols);
            }
            else
            {
                System.Threading.Tasks.Parallel.ForEach(Partitioner.Create(0, rows, _rangeSize), range =>
                {
                    int min = range.Item1, max = range.Item2, i, j;
                    for (i = min; i < max; ++i)
                    {
                        double[] A_i = A[a_row_start + i], B_i = B[i];
                        for (j = 0; j < cols; ++j)
                        {
                            A_i[a_col_start + j] -= B_i[j];
                        }
                    }
                });
            }
        }

        void IDenseBLAS2<double>.Negate(double[][] A, double[][] result, int a_row_start, int a_col_start, int result_row_start, int result_col_start, int rows, int cols)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
