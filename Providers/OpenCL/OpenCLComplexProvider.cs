﻿using LinearNet.Matrices;
using LinearNet.Structs;
using System;
using System.Collections.Generic;
using System.Text;

namespace LinearNet.Providers.OpenCL
{
    public sealed class OpenCLComplexProvider : IDenseBLAS1<Complex>, IDenseBLAS3<Complex>
    {
        public IProvider<Complex> Provider => throw new NotImplementedException();

        IProvider<Complex> IDenseBLAS1<Complex>.Provider => throw new NotImplementedException();

        void IDenseBLAS1<Complex>.ADD(Complex[] u, Complex[] v, Complex[] sum, int start, int end)
        {
            throw new NotImplementedException();
        }

        void IDenseBLAS1<Complex>.SUB(Complex[] u, Complex[] v, Complex[] diff, int start, int end)
        {
            throw new NotImplementedException();
        }

        void IDenseBLAS1<Complex>.AXPY(Complex[] y, Complex[] x, Complex alpha, int start, int end)
        {
            throw new NotImplementedException();
        }

        void IDenseBLAS1<Complex>.AXPY(Complex[] y, Complex[] x, Complex alpha, int y_start, int y_end, int xy_offset)
        {
            throw new NotImplementedException();
        }

        Complex IDenseBLAS1<Complex>.DOT(Complex[] u, Complex[] v, int start, int end)
        {
            throw new NotImplementedException();
        }

        Complex IDenseBLAS1<Complex>.DOT(Complex[] u, Complex[] v, int u_start, int u_end, int v_u_offset)
        {
            throw new NotImplementedException();
        }

        void IDenseBLAS1<Complex>.SCAL(Complex[] x, Complex s, int start, int end)
        {
            throw new NotImplementedException();
        }

        Complex IDenseBLAS1<Complex>.XTA(Complex[] x, Complex[][] A, int column, int start, int end)
        {
            throw new NotImplementedException();
        }

        Complex IDenseBLAS1<Complex>.XTA(Complex[] x, Complex[][] A, int column, int x_start, int x_end, int Ax_offset)
        {
            throw new NotImplementedException();
        }

        void IDenseBLAS1<Complex>.ROTR(Complex[][] A, int i, int j, Complex c, Complex s, int colStart, int colEnd)
        {
            throw new NotImplementedException();
        }
        void IDenseBLAS1<Complex>.ROTC(Complex[][] A, int i, int j, Complex c, Complex s, int rowStart, int rowEnd)
        {
            throw new NotImplementedException();
        }

        void IDenseBLAS1<Complex>.PMULT(Complex[] u, Complex[] v, Complex[] uv, int u_start, int u_end, int v_u_offset, int uv_u_offset)
        {
            throw new NotImplementedException();
        }

        public void Multiply(Complex[][] A, Complex[][] B, Complex[][] result, Size3 size, bool increment)
        {
            throw new NotImplementedException();
        }

        public void Multiply(Complex[][] A, Complex[][] B, Complex[][] result, int A_start_row, int A_start_col, int B_start_row, int B_start_col, int C_start_row, int C_start_col, Size3 size, bool increment)
        {
            throw new NotImplementedException();
        }

    }
}
