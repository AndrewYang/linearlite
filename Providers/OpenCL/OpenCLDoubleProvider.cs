﻿using LinearNet.Matrices;
using System;
using System.Collections.Generic;
using System.Text;

namespace LinearNet.Providers.OpenCL
{
    public class OpenCLDoubleProvider : IDenseBLAS3<double>
    {
        public void Multiply(double[][] A, double[][] B, double[][] result, Size3 size, bool increment)
        {
            throw new NotImplementedException();
        }
        public void Multiply(double[][] A, double[][] B, double[][] result, int A_start_row, int A_start_col, int B_start_row, int B_start_col, int C_start_row, int C_start_col, Size3 size, bool increment)
        {
            throw new NotImplementedException();
        }
    }
}
