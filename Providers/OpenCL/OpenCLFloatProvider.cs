﻿using LinearNet.Matrices;
using System;
using System.Collections.Generic;
using System.Text;

namespace LinearNet.Providers.OpenCL
{
    public sealed class OpenCLFloatProvider : IDenseBLAS1<float>, IDenseBLAS3<float>
    {
        IProvider<float> IDenseBLAS1<float>.Provider => throw new NotImplementedException();

        void IDenseBLAS1<float>.ADD(float[] u, float[] v, float[] sum, int start, int end)
        {
            throw new NotImplementedException();
        }

        void IDenseBLAS1<float>.SUB(float[] u, float[] v, float[] diff, int start, int end)
        {
            throw new NotImplementedException();
        }

        void IDenseBLAS1<float>.AXPY(float[] y, float[] x, float alpha, int start, int end)
        {
            throw new NotImplementedException();
        }

        void IDenseBLAS1<float>.AXPY(float[] y, float[] x, float alpha, int y_start, int y_end, int xy_offset)
        {
            throw new NotImplementedException();
        }

        float IDenseBLAS1<float>.DOT(float[] u, float[] v, int start, int end)
        {
            throw new NotImplementedException();
        }

        float IDenseBLAS1<float>.DOT(float[] u, float[] v, int u_start, int u_end, int v_u_offset)
        {
            throw new NotImplementedException();
        }

        void IDenseBLAS1<float>.SCAL(float[] x, float s, int start, int end)
        {
            throw new NotImplementedException();
        }

        float IDenseBLAS1<float>.XTA(float[] x, float[][] A, int column, int start, int end)
        {
            throw new NotImplementedException();
        }

        float IDenseBLAS1<float>.XTA(float[] x, float[][] A, int column, int x_start, int x_end, int Ax_offset)
        {
            throw new NotImplementedException();
        }

        void IDenseBLAS1<float>.ROTR(float[][] A, int i, int j, float c, float s, int colStart, int colEnd)
        {
            throw new NotImplementedException();
        }
        void IDenseBLAS1<float>.ROTC(float[][] A, int i, int j, float c, float s, int rowStart, int rowEnd)
        {
            throw new NotImplementedException();
        }

        public void Multiply(float[][] A, float[][] B, float[][] result, Size3 size, bool increment)
        {
            throw new NotImplementedException();
        }
        public void Multiply(float[][] A, float[][] B, float[][] result, int A_start_row, int A_start_col, int B_start_row, int B_start_col, int C_start_row, int C_start_col, Size3 size, bool increment)
        {
            throw new NotImplementedException();
        }

        void IDenseBLAS1<float>.PMULT(float[] u, float[] v, float[] uv, int u_start, int u_end, int v_u_offset, int uv_u_offset)
        {
            throw new NotImplementedException();
        }
    }
}
