﻿using LinearNet.Matrices;
using LinearNet.Matrices.Inversion;
using LinearNet.Matrices.Multiplication;
using LinearNet.Providers;
using LinearNet.Providers.LinearAlgebra;
using LinearNet.Structs;
using System;
using System.Collections.Generic;
using System.Text;

namespace LinearNet
{
    public static class ProviderFactory
    {
        internal static IProvider<T> GetDefaultProvider<T>() where T : new()
        {
            if (!TryGetDefaultProvider(out IProvider<T> provider))
            {
                throw new NotSupportedException($"No default provider is defined for type '{typeof(T)}'. Please use the explicit constructor instead.");
            }

            return provider;
        }
        internal static IDenseBLAS1<T> GetDefaultBLAS1<T>() where T : new()
        {
            if (!TryGetDefaultBLAS1Provider<T>(out IDenseBLAS1<T> blas))
            {
                throw new NotSupportedException($"No default level-1 BLAS is defined for type '{typeof(T)}'. Please use the explicit constructor instead.");
            }
            return blas;
        }
        internal static IDenseBLAS2<T> GetDefaultBLAS2<T>() where T : new() 
        { 
            if (!TryGetDefaultBLAS2Provider(out IDenseBLAS2<T> blas))
            {
                throw new NotSupportedException($"No default level-2 BLAS is defined for type '{typeof(T)}'. Please use the explicit constructor instead.");
            }
            return blas;
        }
        internal static IDenseHouseholder<T> GetDefaultHouseholder<T>()
        {
            if (!TryGetDefaultHouseholderProvider(out IDenseHouseholder<T> provider))
            {
                throw new NotSupportedException($"No default Householder provider is defined for type '{typeof(T)}'. Please use the explicit constructor instead.");
            }

            return provider;
        }
        internal static ISparseBLAS1<T> GetDefaultSparseBLAS1<T>() where T : new()
        {
            if (!TryGetDefaultSparseBLAS1Provider<T>(out ISparseBLAS1<T> blas))
            {
                throw new NotSupportedException($"No default level-1 BLAS is defined for type '{typeof(T)}'. Please use the explicit constructor instead.");
            }
            return blas;
        }

        internal static bool TryGetDefaultProvider<T>(out IProvider<T> provider) where T : new()
        {
            Type type = typeof(T);
            if (type == typeof(int))
            {
                provider = new Int32Provider() as IProvider<T>;
                return true;
            }
            if (type == typeof(long))
            {
                provider = new Int64Provider() as IProvider<T>;
                return true;
            }
            if (type == typeof(float))
            {
                provider = new FloatProvider() as IProvider<T>;
                return true;
            }
            if (type == typeof(double))
            {
                provider = new DoubleProvider() as IProvider<T>;
                return true;
            }
            if (type == typeof(decimal))
            {
                provider = new DecimalProvider() as IProvider<T>;
                return true;
            }
            if (type == typeof(Complex))
            {
                provider = new ComplexProvider() as IProvider<T>;
                return true;
            }
            if (typeof(IField<T>).IsAssignableFrom(type))
            {
                Type d = typeof(FieldProvider<>);
                Type t = d.MakeGenericType(typeof(T));
                provider = Activator.CreateInstance(t) as IProvider<T>;
                return true;
            }
            if (typeof(IRing<T>).IsAssignableFrom(type))
            {
                Type d = typeof(RingProvider<>);
                Type t = d.MakeGenericType(typeof(T));
                provider = Activator.CreateInstance(t) as IProvider<T>;
                return true;
            }
            if (typeof(IAdditiveGroup<T>).IsAssignableFrom(type))
            {
                Type d = typeof(AdditiveGroupProvider<>);
                Type t = d.MakeGenericType(typeof(T));
                provider = Activator.CreateInstance(t) as IProvider<T>;
                return true;
            }

            provider = null;
            return false;
        }
        internal static bool TryGetDefaultBLAS1Provider<T>(out IDenseBLAS1<T> blas) where T : new()
        {
            Type type = typeof(T);
            if (type == typeof(int))
            {
                blas = new NativeInt32Provider() as IDenseBLAS1<T>;
                return true;
            }
            if (type == typeof(long))
            {
                blas = new NativeInt64Provider() as IDenseBLAS1<T>;
                return true;
            }
            if (type == typeof(float))
            {
                blas = new NativeFloatProvider() as IDenseBLAS1<T>;
                return true;
            }
            if (type == typeof(double))
            {
                blas = new NativeDoubleProvider() as IDenseBLAS1<T>;
                return true;
            }
            if (type == typeof(decimal))
            {
                blas = new NativeDecimalProvider() as IDenseBLAS1<T>;
                return true;
            }
            if (type == typeof(Complex))
            {
                blas = new NativeComplexProvider() as IDenseBLAS1<T>;
                return true;
            }
            if (typeof(IField<T>).IsAssignableFrom(type))
            {
                Type d = typeof(NativeFieldProvider<>);
                Type t = d.MakeGenericType(typeof(T));
                blas = Activator.CreateInstance(t) as IDenseBLAS1<T>;
                return true;
            }
            if (typeof(IRing<T>).IsAssignableFrom(type))
            {
                Type d = typeof(NativeRingProvider<>);
                Type t = d.MakeGenericType(typeof(T));
                blas = Activator.CreateInstance(t) as IDenseBLAS1<T>;
                return true;
            }

            blas = null;
            return false;
        }
        internal static bool TryGetDefaultBLAS2Provider<T>(out IDenseBLAS2<T> blas) where T : new()
        {
            Type type = typeof(T);
            if (type == typeof(int))
            {
                blas = new NativeInt32Provider() as IDenseBLAS2<T>;
                return true;
            }
            if (type == typeof(long))
            {
                blas = new NativeInt64Provider() as IDenseBLAS2<T>;
                return true;
            }
            if (type == typeof(float))
            {
                blas = new NativeFloatProvider() as IDenseBLAS2<T>;
                return true;
            }
            if (type == typeof(double))
            {
                blas = new NativeDoubleProvider() as IDenseBLAS2<T>;
                return true;
            }
            if (type == typeof(decimal))
            {
                blas = new NativeDecimalProvider() as IDenseBLAS2<T>;
                return true;
            }
            if (type == typeof(Complex))
            {
                blas = new NativeComplexProvider() as IDenseBLAS2<T>;
                return true;
            }
            if (typeof(IField<T>).IsAssignableFrom(type))
            {
                Type d = typeof(NativeFieldProvider<>);
                Type t = d.MakeGenericType(typeof(T));
                blas = Activator.CreateInstance(t) as IDenseBLAS2<T>;
                return true;
            }
            if (typeof(IRing<T>).IsAssignableFrom(type))
            {
                Type d = typeof(NativeRingProvider<>);
                Type t = d.MakeGenericType(typeof(T));
                blas = Activator.CreateInstance(t) as IDenseBLAS2<T>;
                return true;
            }

            blas = null;
            return false;
        }
        internal static bool TryGetDefaultMatrixInversion<T>(out IMatrixInversion<T> inversion)
        {
            Type type = typeof(T);
            if (type == typeof(float) ||
                type == typeof(double) ||
                type == typeof(decimal) ||
                type == typeof(Complex))
            {
                inversion = new PivotedGaussianEliminationMatrixInversion() as IMatrixInversion<T>;
                return true;
            }

            inversion = null;
            return false;
        }
        internal static bool TryGetDefaultHouseholderProvider<T>(out IDenseHouseholder<T> house)
        {
            Type type = typeof(T);
            if (type == typeof(float))
            {
                house = new NativeFloatProvider() as IDenseHouseholder<T>;
                return true;
            }
            if (type == typeof(double))
            {
                house = new NativeDoubleProvider() as IDenseHouseholder<T>;
                return true;
            }
            if (type == typeof(decimal))
            {
                house = new NativeDecimalProvider() as IDenseHouseholder<T>;
                return true;
            }
            if (type == typeof(Complex))
            {
                house = new NativeComplexProvider() as IDenseHouseholder<T>;
                return true;
            }

            house = null;
            return false;
        }
        internal static bool TryGetDefaultSparseBLAS1Provider<T>(out ISparseBLAS1<T> blas) where T : new()
        {
            Type type = typeof(T);
            if (type == typeof(int))
            {
                blas = new NativeInt32Provider() as ISparseBLAS1<T>;
                return true;
            }
            if (type == typeof(long))
            {
                blas = new NativeInt64Provider() as ISparseBLAS1<T>;
                return true;
            }
            if (type == typeof(float))
            {
                blas = new NativeFloatProvider() as ISparseBLAS1<T>;
                return true;
            }
            if (type == typeof(double))
            {
                blas = new NativeDoubleProvider() as ISparseBLAS1<T>;
                return true;
            }
            if (type == typeof(decimal))
            {
                blas = new NativeDecimalProvider() as ISparseBLAS1<T>;
                return true;
            }
            if (type == typeof(Complex))
            {
                blas = new NativeComplexProvider() as ISparseBLAS1<T>;
                return true;
            }
            if (typeof(IField<T>).IsAssignableFrom(type))
            {
                Type d = typeof(NativeFieldProvider<>);
                Type t = d.MakeGenericType(typeof(T));
                blas = Activator.CreateInstance(t) as ISparseBLAS1<T>;
                return true;
            }
            if (typeof(IRing<T>).IsAssignableFrom(type))
            {
                Type d = typeof(NativeRingProvider<>);
                Type t = d.MakeGenericType(typeof(T));
                blas = Activator.CreateInstance(t) as ISparseBLAS1<T>;
                return true;
            }

            blas = null;
            return false;
        }
    }
    internal static class DefaultProviders
    {
        internal static Int32Provider Int32 = new Int32Provider();
        internal static Int64Provider Int64 = new Int64Provider();
        internal static FloatProvider Float = new FloatProvider();
        internal static DoubleProvider Double = new DoubleProvider();
        internal static DecimalProvider Decimal = new DecimalProvider();
        internal static ComplexProvider Complex = new ComplexProvider();
    }
}
