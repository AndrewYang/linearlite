﻿using LinearNet.Providers.LinearAlgebra;
using LinearNet.Structs;
using System;
using System.Collections.Generic;

#if NETCOREAPP3_0
using System.Runtime.Intrinsics;
using System.Runtime.Intrinsics.X86;
#endif

using System.Text;

namespace LinearNet.Providers.SSE
{
#if NETCOREAPP3_0
    /// <summary>
    /// Level-1 BLAS provider for complex numbers using SSE2 hardware acceleration.
    /// </summary>
    /// <cat>linear-algebra</cat>
    public sealed class SSE2ComplexProvider : IDenseBLAS1<Complex>
    {
        private readonly ComplexProvider _provider;
        private readonly bool _useUnsafe;

        public IProvider<Complex> Provider => _provider;

        public SSE2ComplexProvider(bool useUnsafe = false)
        {
            _provider = new ComplexProvider();
            _useUnsafe = useUnsafe;
        }

#region BLAS 1

#region Dense

        void IDenseBLAS1<Complex>.ADD(Complex[] u, Complex[] v, Complex[] sum, int start, int end)
        {
            throw new NotImplementedException();
        }

        void IDenseBLAS1<Complex>.SUB(Complex[] u, Complex[] v, Complex[] diff, int start, int end)
        {
            int _end = start + (start - end) / 2 * 2, i;

            Complex u0, u1, v0, v1;
            for (i = start; i < _end; i += 2)
            {
                int i1 = i + 1;
                u0 = u[i]; u1 = u[i1]; v0 = v[i]; v1 = v[i1];

                Vector128<double> re_vect = Sse2.Subtract(Vector128.Create(u0.Real, u1.Real), Vector128.Create(v0.Real, v1.Real));
                Vector128<double> im_vect = Sse2.Subtract(Vector128.Create(u0.Imaginary, u1.Imaginary), Vector128.Create(v0.Imaginary, v1.Imaginary));

                diff[i] = new Complex(re_vect.GetElement(0), im_vect.GetElement(0));
                diff[i1] = new Complex(re_vect.GetElement(1), im_vect.GetElement(1));
            }

            for (i = _end; i < end; ++i)
            {
                diff[i] = u[i].Add(v[i]);
            }
        }

        // Has approximately the same performance as naive AXPY (but outperforms AVX2 by >30%)
        void IDenseBLAS1<Complex>.AXPY(Complex[] y, Complex[] x, Complex alpha, int start, int end)
        {
            if (_useUnsafe)
            {
                AXPY_safe(y, x, alpha, start, end);
            }
            else
            {
                AXPY_unsafe(y, x, alpha, start, end);
            }
        }
        void IDenseBLAS1<Complex>.AXPY(Complex[] y, Complex[] x, Complex alpha, int y_start, int y_end, int xy_offset)
        {
            throw new NotImplementedException();
        }
        private void AXPY_safe(Complex[] y, Complex[] x, Complex alpha, int start, int end)
        {
            int _end = (end - start) / 2 * 2 + start, i;

            Vector128<double> alpha_re_vect = Vector128.Create(alpha.Real, alpha.Real);
            Vector128<double> alpha_im_vect = Vector128.Create(alpha.Imaginary, alpha.Imaginary);

            Complex a, b;
            for (i = start; i < _end; i += 2)
            {
                int i1 = i + 1;
                a = x[i];
                b = x[i1];

                Vector128<double> x_re_vect = Vector128.Create(a.Real, b.Real);
                Vector128<double> x_im_vect = Vector128.Create(a.Imaginary, b.Imaginary);

                Vector128<double> re_buff =
                    Sse2.Subtract(
                        Sse2.Multiply(alpha_re_vect, x_re_vect),
                        Sse2.Multiply(alpha_im_vect, x_im_vect));

                Vector128<double> im_buff =
                    Sse2.Add(
                        Sse2.Multiply(alpha_re_vect, x_im_vect),
                        Sse2.Multiply(alpha_im_vect, x_re_vect));

                y[i].Real += re_buff.GetElement(0);
                y[i].Imaginary += im_buff.GetElement(0);
                y[i1].Real += re_buff.GetElement(1);
                y[i1].Imaginary += im_buff.GetElement(1);
            }

            for (i = _end; i < end; ++i)
            {
                y[i] += alpha * x[i];
            }
        }
        private unsafe void AXPY_unsafe(Complex[] y, Complex[] x, Complex alpha, int start, int end)
        {
            throw new NotImplementedException();
        }

        Complex IDenseBLAS1<Complex>.DOT(Complex[] u, Complex[] v, int start, int end)
        {
            int last = (end - start) / 2 * 2 + start, i;
            Vector128<double> sum_re = Vector128.Create(0.0, 0.0), sum_im = Vector128.Create(0.0, 0.0);

            for (i = start; i < last; i += 2)
            {
                int i1 = i + 1;

                Complex u1 = u[i], u2 = u[i1], v1 = v[i], v2 = v[i1];

                Vector128<double>
                    u_re = Vector128.Create(u1.Real, u2.Real),
                    u_im = Vector128.Create(u1.Imaginary, u2.Imaginary),
                    v_re = Vector128.Create(v1.Real, v2.Real),
                    v_im = Vector128.Create(v1.Imaginary, v2.Imaginary);

                sum_re = Sse2.Add(sum_re, Sse2.Subtract(Sse2.Multiply(u_re, v_re), Sse2.Multiply(u_im, v_im)));
                sum_im = Sse2.Add(sum_im, Sse2.Add(Sse2.Multiply(u_im, v_re), Sse2.Multiply(u_re, v_im)));
            }

            double re = sum_re.GetElement(0) + sum_re.GetElement(1);
            double im = sum_im.GetElement(0) + sum_im.GetElement(1);
            for (i = last; i < end; ++i)
            {
                Complex _u = u[i], _v = v[i];
                re += _u.Real * _v.Real - _u.Imaginary * _v.Imaginary;
                im += _u.Imaginary * _v.Real + _u.Real * _v.Imaginary;
            }

            return new Complex(re, im);
        }
        Complex IDenseBLAS1<Complex>.DOT(Complex[] u, Complex[] v, int u_start, int u_end, int v_u_offset)
        {
            throw new NotImplementedException();
        }
        void IDenseBLAS1<Complex>.SCAL(Complex[] x, Complex s, int start, int end)
        {
            throw new NotImplementedException();
        }
        Complex IDenseBLAS1<Complex>.XTA(Complex[] x, Complex[][] A, int column, int start, int end)
        {
            throw new NotImplementedException();
        }
        Complex IDenseBLAS1<Complex>.XTA(Complex[] x, Complex[][] A, int column, int x_start, int x_end, int Ax_offset)
        {
            throw new NotImplementedException();
        }

        void IDenseBLAS1<Complex>.ROTR(Complex[][] A, int i, int j, Complex c, Complex s, int colStart, int colEnd)
        {
            throw new NotImplementedException();
        }
        void IDenseBLAS1<Complex>.ROTC(Complex[][] A, int i, int j, Complex c, Complex s, int rowStart, int rowEnd)
        {
            throw new NotImplementedException();
        }

        void IDenseBLAS1<Complex>.PMULT(Complex[] u, Complex[] v, Complex[] uv, int u_start, int u_end, int v_u_offset, int uv_u_offset)
        {
            throw new NotImplementedException();
        }

#endregion

#endregion

    }
#endif

}
