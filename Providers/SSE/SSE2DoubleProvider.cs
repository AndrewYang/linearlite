﻿using LinearNet.Providers.LinearAlgebra;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#if NETCOREAPP3_0
using System.Runtime.Intrinsics;
using System.Runtime.Intrinsics.X86;
#endif

using System.Runtime.CompilerServices;

namespace LinearNet.Providers.SSE
{
#if NETCOREAPP3_0
    /// <summary>
    /// Level-1 BLAS provider for <txt>double</txt> using SSE2 hardware acceleration.
    /// </summary>
    /// <cat>linear-algebra</cat>
    public sealed class SSE2DoubleProvider : IDenseBLAS1<double>
    {
        public static bool IsSupported { get { return Sse2.IsSupported; } }
        private readonly bool _useUnsafe;

        private readonly DoubleProvider _provider;

        public IProvider<double> Provider => _provider;

        public SSE2DoubleProvider(bool useUnsafe = false)
        {
            _useUnsafe = useUnsafe;
            _provider = new DoubleProvider();
        }

        #region Private

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static void AXPY_safe(double[] y, double[] x, double alpha, int start, int end)
        {
            int count = Vector128<double>.Count,
                   _end = (end - start) / count * count + start,
                   i;

            Vector128<double> alpha_vect = Vector128.Create(alpha, alpha),
                x_vect, y_vect, axpy;
            for (i = start; i < _end; i += 2)
            {
                int next = i + 1;

                x_vect = Vector128.Create(x[i], x[next]);
                y_vect = Vector128.Create(y[i], y[next]);
                axpy = Sse2.Add(y_vect, Sse2.Multiply(alpha_vect, x_vect));

                y[i] = Vector128.GetElement(axpy, 0);
                y[next] = Vector128.GetElement(axpy, 1);
            }

            for (i = _end; i < end; ++i)
            {
                y[i] += alpha * x[i];
            }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static unsafe void AXPY_unsafe(double[] y, double[] x, double alpha, int start, int end)
        {
            int _end = (end - start) / 2 * 2 + start, i;

            Vector128<double> alpha_vect = Vector128.Create(alpha, alpha), x_vect, y_vect, axpy;

            fixed (double* x_start = &x[start])
            fixed (double* y_start = &y[start])
            fixed (double* x_end = &x[_end - 2])
            {
                double* _x = x_start, _y = y_start;
                while (_x <= x_end)
                {
                    x_vect = Sse2.LoadVector128(_x);
                    y_vect = Sse2.LoadVector128(_y);
                    axpy = Sse2.Add(y_vect, Sse2.Multiply(alpha_vect, x_vect));

                    *(_y++) = Vector128.GetElement(axpy, 0);
                    *(_y++) = Vector128.GetElement(axpy, 1);

                    _x += 2;
                }
            }

            for (i = _end; i < end; ++i)
            {
                y[i] += alpha * x[i];
            }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static double DOT_safe(double[] x, double[] y, int start, int end)
        {
            int _end = (end - start) / 2 * 2 + start, i;

            Vector128<double> x_vect, y_vect, sum = Vector128.Create(0.0, 0.0);

            for (i = start; i < _end; i += 2)
            {
                int next = i + 1;
                x_vect = Vector128.Create(x[i], x[next]);
                y_vect = Vector128.Create(y[i], y[next]);
                sum = Sse2.Add(sum, Sse2.Multiply(x_vect, y_vect));
            }

            double dot = sum.GetElement(0) + sum.GetElement(1);
            for (i = _end; i < end; ++i)
            {
                dot += x[i] * y[i];
            }
            return dot;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static unsafe double DOT_unsafe(double[] x, double[] y, int start, int end)
        {
            int _end = (end - start) / 2 * 2 + start, i;

            Vector128<double> x_vect, y_vect, sum = Vector128.Create(0.0, 0.0);

            fixed (double* x_start = &x[start])
            fixed (double* y_start = &y[start])
            fixed (double* x_end = &x[_end - 2])
            {
                double* _x = x_start, _y = y_start;
                while (_x <= x_end)
                {
                    x_vect = Sse2.LoadVector128(_x);
                    y_vect = Sse2.LoadVector128(_y);
                    sum = Sse2.Add(sum, Sse2.Multiply(x_vect, y_vect));

                    _y += 2;
                    _x += 2;
                }
            }

            double dot = sum.GetElement(0) + sum.GetElement(1);
            for (i = _end; i < end; ++i)
            {
                dot += x[i] * y[i];
            }
            return dot;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static double DOT_safe(double[] x, double[] y, int u_start, int u_end, int v_u_offset)
        {
            int _end = (u_end - u_start) / 2 * 2 + u_start, i, _i;

            Vector128<double> x_vect, y_vect, sum = Vector128.Create(0.0, 0.0);

            for (i = u_start, _i = u_start + v_u_offset; i < _end; i += 2, _i += 2)
            {
                int next = i + 1, _next = _i + 1;
                x_vect = Vector128.Create(x[i], x[next]);
                y_vect = Vector128.Create(y[_i], y[_next]);
                sum = Sse2.Add(sum, Sse2.Multiply(x_vect, y_vect));
            }

            double dot = sum.GetElement(0) + sum.GetElement(1);
            for (i = _end; i < u_end; ++i)
            {
                dot += x[i] * y[i];
            }
            return dot;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static unsafe double DOT_unsafe(double[] x, double[] y, int u_start, int u_end, int v_u_offset)
        {
            int _end = (u_end - u_start) / 2 * 2 + u_start, i;

            Vector128<double> x_vect, y_vect, sum = Vector128.Create(0.0, 0.0);

            fixed (double* x_start = &x[u_start])
            fixed (double* y_start = &y[u_start + v_u_offset])
            fixed (double* x_end = &x[_end - 2])
            {
                double* _x = x_start, _y = y_start;
                while (_x <= x_end)
                {
                    x_vect = Sse2.LoadVector128(_x);
                    y_vect = Sse2.LoadVector128(_y);
                    sum = Sse2.Add(sum, Sse2.Multiply(x_vect, y_vect));

                    _y += 2;
                    _x += 2;
                }
            }

            double dot = sum.GetElement(0) + sum.GetElement(1);
            for (i = _end; i < u_end; ++i)
            {
                dot += x[i] * y[i];
            }
            return dot;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static void SCAL_safe(double[] x, double s, int start, int end)
        {
            int _end = (end - start) / 2 * 2 + start, i;
            Vector128<double> x_vect, scalar = Vector128.Create(s, s);

            for (i = start; i < _end; i += 2)
            {
                int next = i + 1;
                x_vect = Vector128.Create(x[i], x[next]);
                Vector128<double> prod = Sse2.Multiply(x_vect, scalar);
                x[i] = prod.GetElement(0);
                x[next] = prod.GetElement(1);
            }

            for (i = _end; i < end; ++i)
            {
                x[i] *= s;
            }
        }
        #endregion


        #region BLAS 1

        #region Dense

        void IDenseBLAS1<double>.ADD(double[] u, double[] v, double[] sum, int start, int end)
        {
            int count = Vector128<double>.Count,
                      _end = (end - start) / count * count + start,
                      i;

            for (i = start; i < _end; i += 2)
            {
                int i1 = i + 1;
                Vector128<double> vect = Sse2.Add(Vector128.Create(u[i], u[i1]), Vector128.Create(v[i], v[i1]));
                sum[i] = Vector128.GetElement(vect, 0);
                sum[i1] = Vector128.GetElement(vect, 1);
            }

            // If there is a remainder
            if ((end - start) % 2 == 1)
            {
                i = end - 1;
                sum[i] = u[i] + v[i];
            }
        }
        void IDenseBLAS1<double>.SUB(double[] u, double[] v, double[] diff, int start, int end)
        {
            int count = Vector128<double>.Count,
                   _end = (end - start) / count * count + start,
                   i;

            for (i = start; i < _end; i += 2)
            {
                int i1 = i + 1;
                Vector128<double> vect = Sse2.Subtract(Vector128.Create(u[i], u[i1]), Vector128.Create(v[i], v[i1]));
                diff[i] = Vector128.GetElement(vect, 0);
                diff[i1] = Vector128.GetElement(vect, 1);
            }

            // If there is a remainder
            if ((end - start) % 2 == 1)
            {
                i = end - 1;
                diff[i] = u[i] - v[i];
            }
        }

        void IDenseBLAS1<double>.AXPY(double[] y, double[] x, double alpha, int start, int end)
        {
            if (_useUnsafe)
            {
                AXPY_unsafe(y, x, alpha, start, end);
            }
            else
            {
                AXPY_safe(y, x, alpha, start, end);
            }
        }
        void IDenseBLAS1<double>.AXPY(double[] y, double[] x, double alpha, int y_start, int y_end, int xy_offset)
        {
            throw new NotImplementedException();
        }

        double IDenseBLAS1<double>.DOT(double[] u, double[] v, int start, int end)
        {
            if (_useUnsafe)
            {
                return DOT_unsafe(u, v, start, end);
            }
            else
            {
                return DOT_safe(u, v, start, end);
            }
        }
        double IDenseBLAS1<double>.DOT(double[] u, double[] v, int u_start, int u_end, int v_u_offset)
        {
            if (_useUnsafe)
            {
                return DOT_unsafe(u, v, u_start, u_end, v_u_offset);
            }
            else
            {
                return DOT_safe(u, v, u_start, u_end, v_u_offset);
            }
        }
        void IDenseBLAS1<double>.SCAL(double[] x, double s, int start, int end)
        {
            if (_useUnsafe)
            {
                throw new NotImplementedException();
            }
            else
            {
                SCAL_safe(x, s, start, end);
            }
        }
        double IDenseBLAS1<double>.XTA(double[] x, double[][] A, int column, int start, int end)
        {
            throw new NotImplementedException();
        }

        double IDenseBLAS1<double>.XTA(double[] x, double[][] A, int column, int x_start, int x_end, int Ax_offset)
        {
            throw new NotImplementedException();
        }

        void IDenseBLAS1<double>.ROTR(double[][] A, int i, int j, double c, double s, int col_start_incl, int col_end_excl)
        {
            if (_useUnsafe)
            {
                GivensRotateRows_unsafe(A, i, j, c, s, col_start_incl, col_end_excl);
            }
            else
            {
                throw new NotImplementedException();
            }
        }
        private unsafe void GivensRotateRows_unsafe(double[][] A, int i, int j, double c, double s, int col_start_incl, int col_end_excl)
        {
            int end = (col_end_excl - col_start_incl) / 2 * 2 + col_start_incl;
            if (end >= col_end_excl)
            {
                end -= 4; // because we need to reference the end of the array

                // arrays of length < 4
                if (end < col_start_incl)
                {
                    end = col_start_incl;
                }
            }

            Vector128<double> vect_c = Vector128.Create(c, c);
            Vector128<double> vect_s = Vector128.Create(s, s);
            Vector128<double> vect_minus_s = Vector128.Create(-s, -s);

            // only alter the rows i and j of matrix A
            double[] A_i = A[i], A_j = A[j];

            fixed (double* A_i_start = &A_i[0],
                            A_j_start = &A_j[0],
                            A_i_end = &A_i[end])
            {
                double* a = A_i_start;
                double* b = A_j_start;

                for (; a < A_i_end; a += 2, b += 2)
                {
                    Vector128<double> r_i = Sse2.LoadVector128(a);
                    Vector128<double> r_j = Sse2.LoadVector128(b);

                    Sse2.Store(a, Sse2.Add(Sse2.Multiply(vect_c, r_i), Sse2.Multiply(vect_s, r_j)));
                    Sse2.Store(b, Sse2.Add(Sse2.Multiply(vect_c, r_j), Sse2.Multiply(vect_minus_s, r_i)));
                }
            }

            for (int k = end; k < col_end_excl; ++k)
            {
                double A_ik = A_i[k], A_jk = A_j[k];
                A_i[k] = c * A_ik + s * A_jk;
                A_j[k] = c * A_jk - s * A_ik;
            }
        }

        void IDenseBLAS1<double>.ROTC(double[][] A, int i, int j, double c, double s, int rowStart, int rowEnd)
        {
            throw new NotImplementedException();
        }

        void IDenseBLAS1<double>.PMULT(double[] u, double[] v, double[] uv, int u_start, int u_end, int v_u_offset, int uv_u_offset)
        {
            throw new NotImplementedException();
        }

        #endregion

        #endregion

    }
#endif
}
