﻿using LinearNet.Providers.LinearAlgebra;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Text;

#if NETCOREAPP3_0
using System.Runtime.Intrinsics;
using System.Runtime.Intrinsics.X86;
#endif

namespace LinearNet.Providers.SSE
{
#if NETCOREAPP3_0
    /// <summary>
    /// <p>Level-1 BLAS provider for <txt>float</txt> using SSE4.1 hardware acceleration.</p>
    /// <p>SSE4.1 introduces dot products, main purpose of this class is to take avantage of this instruction.</p>
    /// </summary>
    /// <cat>linear-algebra</cat>
    public class SSE4FloatProvider : SSEFloatProvider, IDenseBLAS1<float>
    {
        public static new bool IsSupported { get { return Sse41.IsSupported; } }

        public SSE4FloatProvider(bool useUnsafe = false) : base(useUnsafe)
        {

        }

        // method doesn't seem to work properly on some processors
        float IDenseBLAS1<float>.DOT(float[] u, float[] v, int start, int end)
        {
            if (_useUnsafe)
            {
                return DOT_unsafe(u, v, start, end);
            }
            else
            {
                return DOT_safe(u, v, start, end);
            }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static float DOT_safe(float[] x, float[] y, int start, int end)
        {
            int _end = (end - start) / 4 * 4 + start, i;

            Vector128<float> x_vect, y_vect;

            byte src = 0;
            float dot = 0.0f;
            for (i = start; i < _end; i += 4)
            {
                int i1 = i + 1,
                    i2 = i + 2,
                    i3 = i + 3;

                x_vect = Vector128.Create(x[i], x[i1], x[i2], x[i3]);
                y_vect = Vector128.Create(y[i], y[i1], y[i2], y[i3]);
                dot += Sse41.DotProduct(x_vect, y_vect, src).GetElement(0);
            }
            for (i = _end; i < end; ++i)
            {
                dot += x[i] * y[i];
            }
            return dot;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static unsafe float DOT_unsafe(float[] x, float[] y, int start, int end)
        {
            int _end = (end - start) / 4 * 4 + start, i;

            Vector128<float> x_vect, y_vect;
            float dot = 0.0f;
            byte position = 1;

            fixed (float* x_start = &x[start])
            fixed (float* y_start = &y[start])
            fixed (float* x_end = &x[_end - 4])
            {
                float* _x = x_start, _y = y_start;
                while (_x <= x_end)
                {
                    x_vect = Sse.LoadVector128(_x);
                    y_vect = Sse.LoadVector128(_y);
                    Debug.WriteLine("before:\t\t\t" + x_vect + "\t" + y_vect);
                    dot += Sse41.DotProduct(x_vect, y_vect, position).GetElement(0);
                    Debug.WriteLine(Sse41.DotProduct(x_vect, y_vect, position) + "\t" + x_vect + "\t" + y_vect);

                    _y += 4;
                    _x += 4;
                }
            }

            for (i = _end; i < end; ++i)
            {
                dot += x[i] * y[i];
            }
            return dot;
        }
    }

#endif
}
