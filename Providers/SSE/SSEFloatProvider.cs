﻿using LinearNet.Matrices;
using LinearNet.Providers.LinearAlgebra;
using System;
using System.Collections.Generic;
using System.Text;

#if NETCOREAPP3_0
using System.Runtime.Intrinsics;
using System.Runtime.Intrinsics.X86;
#endif

namespace LinearNet.Providers.SSE
{
#if NETCOREAPP3_0
    /// <summary>
    /// <p>Level-1 BLAS provider for <txt>float</txt> using SSE hardware acceleration.</p>
    /// <p>Also see: <a href="SSE4FloatProvider">SSE4.1 BLAS-1 provider</a>.</p>
    /// </summary>
    /// <cat>linear-algebra</cat>
    public class SSEFloatProvider : IDenseBLAS1<float>
    {
        public static bool IsSupported { get { return Sse.IsSupported; } }

        public IProvider<float> Provider => _provider;

        internal readonly FloatProvider _provider;
        protected readonly bool _useUnsafe;

        public SSEFloatProvider(bool useUnsafe = false)
        {
            _useUnsafe = useUnsafe;
            _provider = new FloatProvider();
        }

#region BLAS 1
        void IDenseBLAS1<float>.ADD(float[] u, float[] v, float[] sum, int start, int end)
        {
            throw new NotImplementedException();
        }
        void IDenseBLAS1<float>.SUB(float[] u, float[] v, float[] diff, int start, int end)
        {
            throw new NotImplementedException();
        }
        void IDenseBLAS1<float>.AXPY(float[] y, float[] x, float alpha, int start, int end)
        {
            if (_useUnsafe)
            {
                AXPY_unsafe(y, x, alpha, start, end);
            }
            else
            {
                AXPY_safe(y, x, alpha, start, end);
            }
        }
        void IDenseBLAS1<float>.AXPY(float[] y, float[] x, float alpha, int y_start, int y_end, int xy_offset)
        {
            throw new NotImplementedException();
        }

        private static void AXPY_safe(float[] y, float[] x, float alpha, int start, int end)
        {
            int _end = (end - start) / 4 * 4 + start, i;

            Vector128<float> alpha_vect = Vector128.Create(alpha, alpha, alpha, alpha), x_vect, y_vect, axpy;
            for (i = start; i < _end; i += 4)
            {
                int i1 = i + 1, i2 = i + 2, i3 = i + 3;

                x_vect = Vector128.Create(x[i], x[i1], x[i2], x[i3]);
                y_vect = Vector128.Create(y[i], y[i1], y[i2], y[i3]);
                axpy = Sse.Add(y_vect, Sse.Multiply(alpha_vect, x_vect));

                y[i] = Vector128.GetElement(axpy, 0);
                y[i1] = Vector128.GetElement(axpy, 1);
                y[i2] = Vector128.GetElement(axpy, 2);
                y[i3] = Vector128.GetElement(axpy, 3);
            }

            for (i = _end; i < end; ++i)
            {
                y[i] += alpha * x[i];
            }
        }

        float IDenseBLAS1<float>.DOT(float[] u, float[] v, int start, int end)
        {
            if (_useUnsafe)
            {
                return DOT_unsafe(u, v, start, end);
            }
            else
            {
                return DOT_safe(u, v, start, end);
            }
        }
        float IDenseBLAS1<float>.DOT(float[] u, float[] v, int u_start, int u_end, int v_u_offset)
        {
            if (_useUnsafe)
            {
                return DOT_unsafe(u, v, u_start, u_end, v_u_offset);
            }
            else
            {
                return DOT_safe(u, v, u_start, u_end, v_u_offset);
            }
        }
        private static float DOT_safe(float[] x, float[] y, int start, int end)
        {
            int _end = (end - start) / 4 * 4 + start, i;

            Vector128<float> x_vect, y_vect, sum = Vector128.Create(0.0f, 0.0f, 0.0f, 0.0f);

            for (i = start; i < _end; i += 4)
            {
                int i1 = i + 1,
                    i2 = i + 2,
                    i3 = i + 3;

                x_vect = Vector128.Create(x[i], x[i1], x[i2], x[i3]);
                y_vect = Vector128.Create(y[i], y[i1], y[i2], y[i3]);
                sum = Sse.Add(sum, Sse.Multiply(x_vect, y_vect));
            }

            float dot = sum.GetElement(0) + sum.GetElement(1) + sum.GetElement(2) + sum.GetElement(3);
            for (i = _end; i < end; ++i)
            {
                dot += x[i] * y[i];
            }
            return dot;
        }
        private static float DOT_safe(float[] x, float[] y, int u_start, int u_end, int v_u_offset)
        {
            int _end = (u_end - u_start) / 4 * 4 + u_start, i;

            Vector128<float> x_vect, y_vect, sum = Vector128.Create(0.0f, 0.0f, 0.0f, 0.0f);

            for (i = u_start; i < _end; i += 4)
            {
                int i1 = i + 1,
                    i2 = i + 2,
                    i3 = i + 3;

                x_vect = Vector128.Create(x[i], x[i1], x[i2], x[i3]);
                y_vect = Vector128.Create(y[i + v_u_offset], y[i1 + v_u_offset], y[i2 + v_u_offset], y[i3 + v_u_offset]);
                sum = Sse.Add(sum, Sse.Multiply(x_vect, y_vect));
            }

            float dot = sum.GetElement(0) + sum.GetElement(1) + sum.GetElement(2) + sum.GetElement(3);
            for (i = _end; i < u_end; ++i)
            {
                dot += x[i] * y[i];
            }
            return dot;
        }

        void IDenseBLAS1<float>.SCAL(float[] x, float s, int start, int end)
        {
            throw new NotImplementedException();
        }

        float IDenseBLAS1<float>.XTA(float[] x, float[][] A, int column, int start, int end)
        {
            throw new NotImplementedException();
        }

        float IDenseBLAS1<float>.XTA(float[] x, float[][] A, int column, int x_start, int x_end, int Ax_offset)
        {
            throw new NotImplementedException();
        }
        void IDenseBLAS1<float>.ROTR(float[][] A, int i, int j, float c, float s, int col_start_incl, int col_end_excl)
        {
            if (_useUnsafe)
            {
                GivensRotateRows_unsafe(A, i, j, c, s, col_start_incl, col_end_excl);
            }
            else
            {
                GivensRotateRows_safe(A, i, j, c, s, col_start_incl, col_end_excl);
            }
        }
        private void GivensRotateRows_safe(float[][] A, int i, int j, float c, float s, int col_start_incl, int col_end_excl)
        {
            int end = (col_end_excl - col_start_incl) / 4 * 4 + col_start_incl;

            Vector128<float> vect_c = Vector128.Create(c, c, c, c);
            Vector128<float> vect_s = Vector128.Create(s, s, s, s);
            Vector128<float> vect_minus_s = Vector128.Create(-s, -s, -s, -s);

            // only alter the rows i and j of matrix A
            float[] A_i = A[i], A_j = A[j];
            for (int k = col_start_incl; k < end; k += 4)
            {
                int k1 = k + 1, k2 = k + 2, k3 = k + 3;

                Vector128<float> r_i = Vector128.Create(A_i[k], A_i[k1], A_i[k2], A_i[k3]);
                Vector128<float> r_j = Vector128.Create(A_j[k], A_j[k1], A_j[k2], A_j[k3]);

                Vector128<float> new_r_i = Sse.Add(Sse.Multiply(vect_c, r_i), Sse.Multiply(vect_s, r_j));
                Vector128<float> new_r_j = Sse.Add(Sse.Multiply(vect_c, r_j), Sse.Multiply(vect_minus_s, r_i));

                A_i[k] = new_r_i.GetElement(0);
                A_i[k1] = new_r_i.GetElement(1);
                A_i[k2] = new_r_i.GetElement(2);
                A_i[k3] = new_r_i.GetElement(3);

                A_j[k] = new_r_j.GetElement(0);
                A_j[k1] = new_r_j.GetElement(1);
                A_j[k2] = new_r_j.GetElement(2);
                A_j[k3] = new_r_j.GetElement(3);
            }

            for (int k = end; k < col_end_excl; ++k)
            {
                float A_ik = A_i[k], A_jk = A_j[k];
                A_i[k] = c * A_ik + s * A_jk;
                A_j[k] = c * A_jk - s * A_ik;
            }
        }
        private unsafe void GivensRotateRows_unsafe(float[][] A, int i, int j, float c, float s, int col_start_incl, int col_end_excl)
        {
            int end = (col_end_excl - col_start_incl) / 4 * 4 + col_start_incl;
            if (end >= col_end_excl)
            {
                end -= 4; // because we need to reference the end of the array

                // arrays of length < 4
                if (end < col_start_incl)
                {
                    end = col_start_incl;
                }
            }

            Vector128<float> vect_c = Vector128.Create(c, c, c, c);
            Vector128<float> vect_s = Vector128.Create(s, s, s, s);
            Vector128<float> vect_minus_s = Vector128.Create(-s, -s, -s, -s);

            // only alter the rows i and j of matrix A
            float[] A_i = A[i], A_j = A[j];

            fixed (float* A_i_start = &A_i[0], 
                            A_j_start = &A_j[0], 
                            A_i_end = &A_i[end])
            {
                float* a = A_i_start;
                float* b = A_j_start;

                for (; a < A_i_end; a += 4, b += 4)
                {
                    Vector128<float> r_i = Sse.LoadVector128(a);
                    Vector128<float> r_j = Sse.LoadVector128(b);

                    Sse.Store(a, Sse.Add(Sse.Multiply(vect_c, r_i), Sse.Multiply(vect_s, r_j)));
                    Sse.Store(b, Sse.Add(Sse.Multiply(vect_c, r_j), Sse.Multiply(vect_minus_s, r_i)));
                }
            }
            
            for (int k = end; k < col_end_excl; ++k)
            {
                float A_ik = A_i[k], A_jk = A_j[k];
                A_i[k] = c * A_ik + s * A_jk;
                A_j[k] = c * A_jk - s * A_ik;
            }
        }

        void IDenseBLAS1<float>.ROTC(float[][] A, int i, int j, float c, float s, int row_start_incl, int row_end_excl)
        {
            // only alter the rows i and j of matrix A
            for (int k = row_start_incl; k < row_end_excl; ++k)
            {
                float[] A_k = A[k];
                float A_ki = A_k[i], A_kj = A_k[j];
                A_k[j] = c * A_kj - s * A_ki;
                A_k[i] = c * A_ki + s * A_kj;
            }
        }

#region unsafe
        private static unsafe void AXPY_unsafe(float[] y, float[] x, float alpha, int start, int end)
        {
            int _end = (end - start) / 4 * 4 + start, i;

            Vector128<float> alpha_vect = Vector128.Create(alpha, alpha, alpha, alpha), x_vect, y_vect, axpy;

            fixed (float* x_start = &x[start])
            fixed (float* y_start = &y[start])
            fixed (float* x_end = &x[_end - 4])
            {
                float* _x = x_start, _y = y_start;
                while (_x <= x_end)
                {
                    x_vect = Sse.LoadVector128(_x);
                    y_vect = Sse.LoadVector128(_y);
                    axpy = Sse.Add(y_vect, Sse.Multiply(alpha_vect, x_vect));

                    *(_y++) = Vector128.GetElement(axpy, 0);
                    *(_y++) = Vector128.GetElement(axpy, 1);
                    *(_y++) = Vector128.GetElement(axpy, 2);
                    *(_y++) = Vector128.GetElement(axpy, 3);

                    _x += 4;
                }
            }

            for (i = _end; i < end; ++i)
            {
                y[i] += alpha * x[i];
            }
        }
        private static unsafe float DOT_unsafe(float[] x, float[] y, int start, int end)
        {
            int _end = (end - start) / 4 * 4 + start, i;

            Vector128<float> x_vect, y_vect, sum = Vector128.Create(0.0f, 0.0f, 0.0f, 0.0f);

            fixed (float* x_start = &x[start])
            fixed (float* y_start = &y[start])
            fixed (float* x_end = &x[_end - 4])
            {
                float* _x = x_start, _y = y_start;
                while (_x <= x_end)
                {
                    x_vect = Sse.LoadVector128(_x);
                    y_vect = Sse.LoadVector128(_y);
                    sum = Sse.Add(sum, Sse.Multiply(x_vect, y_vect));

                    _y += 4;
                    _x += 4;
                }
            }

            float dot = sum.GetElement(0) + sum.GetElement(1) + sum.GetElement(2) + sum.GetElement(3);
            for (i = _end; i < end; ++i)
            {
                dot += x[i] * y[i];
            }
            return dot;
        }
        private static unsafe float DOT_unsafe(float[] u, float[] v, int u_start, int u_end, int v_u_offset)
        {
            int _end = (u_end - u_start) / 4 * 4 + u_start, i;

            Vector128<float> x_vect, y_vect, sum = Vector128.Create(0.0f, 0.0f, 0.0f, 0.0f);

            fixed (float* x_start = &u[u_start])
            fixed (float* y_start = &v[u_start + v_u_offset])
            fixed (float* x_end = &u[_end - 4])
            {
                float* _x = x_start, _y = y_start;
                while (_x <= x_end)
                {
                    x_vect = Sse.LoadVector128(_x);
                    y_vect = Sse.LoadVector128(_y);
                    sum = Sse.Add(sum, Sse.Multiply(x_vect, y_vect));

                    _y += 4;
                    _x += 4;
                }
            }

            float dot = sum.GetElement(0) + sum.GetElement(1) + sum.GetElement(2) + sum.GetElement(3);
            for (i = _end; i < u_end; ++i)
            {
                dot += u[i] * v[i];
            }
            return dot;
        }

        void IDenseBLAS1<float>.PMULT(float[] u, float[] v, float[] uv, int u_start, int u_end, int v_u_offset, int uv_u_offset)
        {
            throw new NotImplementedException();
        }

#endregion

#endregion

    }
#endif
}
