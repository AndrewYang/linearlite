﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LinearNet.Quadrature
{
    public class GeneralizedMidpointIntegration : INumericalIntegration
    {
        private readonly int _N;

        public GeneralizedMidpointIntegration(int N)
        {
            if (N < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(N));
            }

            _N = N;
        }

        public override double Integrate(Func<double, double> univariateFunction, double a, double b)
        {
            if (univariateFunction is null)
            {
                throw new ArgumentNullException(nameof(univariateFunction));
            }
            if (double.IsNaN(a))
            {
                return double.NaN;
            }
            if (double.IsNaN(b))
            {
                return double.NaN;
            }

            if (b < a)
            {
                return -Integrate(univariateFunction, b, a);
            }

            double interval = (b - a) / _N;

            throw new NotImplementedException();
        }
    }
}
