﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Quadrature
{
    /// <summary>
    /// <p>An abstract parent class for all numerical integration implementations.</p>
    /// <p>Classes that extend this abstract class:</p>
    /// <ul>
    /// <li><a href="docs/numerics/TrapezoidalIntegration.html">TrapezoidalIntegration</a></li>
    /// </ul>
    /// </summary>
    /// <cat>numerics</cat>
    public abstract class INumericalIntegration
    {
        /// <summary>
        /// Integrate a univariate function in the interval $[a, b]$
        /// </summary>
        /// <param name="univariateFunction">An Riemann-integrable function $f(x)$ defined over the interval $[a, b]$</param>
        /// <param name="a">The lower bound $a$.</param>
        /// <param name="b">The upper bound $b$.</param>
        /// <returns>A numerical approximation for the integral $\int_a^b f(x) dx$</returns>
        public abstract double Integrate(Func<double, double> univariateFunction, double a, double b);
    }
}
