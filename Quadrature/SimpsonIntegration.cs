﻿using LinearNet.Global;
using System;
using System.Collections.Generic;
using System.Text;

namespace LinearNet.Quadrature
{
    /// <summary>
    /// Implementation of Simpsons integration.
    /// </summary>
    /// <cat>linear-algebra</cat>
    public class SimpsonIntegration : INumericalIntegration
    {
        private readonly int _n;

        /// <summary>
        /// Create a new instance of Simpson's integration 
        /// </summary>
        /// <param name="N">The minimum number of subdivisions of the interval.</param>
        public SimpsonIntegration(int N)
        {
            if (N <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(N), ExceptionMessages.NonPositiveNotAllowed);
            }

            // Round up to nearest even number
            _n = (N + 1) / 2 * 2;
        }

        public override double Integrate(Func<double, double> univariateFunction, double a, double b)
        {
            if (univariateFunction is null)
            {
                throw new ArgumentNullException(nameof(univariateFunction));
            }

            if (double.IsNaN(a))
            {
                return double.NaN;
            }
            if (double.IsNaN(b))
            {
                return double.NaN;
            }

            // Ensure that a < b
            if (b < a)
            {
                return -Integrate(univariateFunction, b, a);
            }

            double sum = univariateFunction(a) + univariateFunction(b);
            double interval = (b - a) / _n;

            int end = _n - 1;
            for (int i = 1; i < end; ++i)
            {
                double x = a + i * interval;
                double term = (i % 2 == 1) ? 4.0 * univariateFunction(x) : 2.0 * univariateFunction(x);

                if (double.IsNaN(term))
                {
                    return double.NaN;
                }
                sum += term;
            }

            if (double.IsNaN(sum))
            {
                return double.NaN;
            }

            return sum * interval / 3;
        }
    }
}
