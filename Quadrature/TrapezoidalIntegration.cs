﻿using LinearNet.Global;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Quadrature
{
    /// <summary>
    /// Implements numerical integration using the trapezoidal rule.
    /// <p>
    /// Trapezoidal integration is a simple numerical integration technique that approximates $\int_a^b f(x) dx$ by splitting 
    /// the interval $[a, b]$ into $N$ regular intervals of size $\delta x = \frac{b-a}{N}$. 
    /// </p>
    /// <p>
    /// The integral is calculated as the sum of the areas of all trapezoids whose parallel sides are given by the vertical lines 
    /// $x = a + i\delta x$ and $x = a + (i + 1)\delta x$ for all $0\le{i}<{N}$.
    /// </p>
    /// </summary>
    /// <cat>numerics</cat>
    public class TrapezoidalIntegration : INumericalIntegration
    {
        private readonly int _N;

        /// <summary>
        /// Create a new trapezoidal numerical integration instance with the specified number of terms.
        /// </summary>
        /// <param name="N">The number of terms to include in the approximation.</param>
        public TrapezoidalIntegration(int N)
        {
            if (N <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(N), ExceptionMessages.NonPositiveNotAllowed);
            }

            _N = N;
        }

        public override double Integrate(Func<double, double> univariateFunction, double a, double b)
        {
            if (double.IsNaN(a) || double.IsNaN(b))
            {
                return double.NaN;
            }

            // Try to figure out what to do in this case
            if (double.IsInfinity(a) || double.IsInfinity(b))
            {
                return double.NaN;
            }

            if (a > b)
            {
                return -Integrate(univariateFunction, b, a);
            }

            if (a == b)
            {
                return 0.0;
            }

            double b_minus_a = b - a;
            double interval = b_minus_a / _N;
            double integral = 0.5 * (univariateFunction(a) + univariateFunction(b));
            for (int n = 1; n < _N; ++n)
            {
                double x = n * interval / _N * b_minus_a + a;
                integral += univariateFunction(x);
            }

            return integral * interval;
        }
    }
}
