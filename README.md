
# Linear.NET

A high performance linear algebra and scientific computing library, written in native C#. 

- Dense, sparse and structured matrix package, supporting 12 matrix storage formats (including CSC, CSR, MCSR, Skyline, Banded, Ellpack, Block-diagonal, COO and DOK)
- All vector, matrix and tensors can be constructed over most primitives as well as custom, user defined structs.
- Supports hardware acceleration via SIMD (SSE, AVX/AVX2) and GPU (CUDA, OpenCL).
- Includes scalable computing package optimized for working with matrices larger than RAM.
- Includes `BigDecimal`, `BigComplex` and `BigRational` implementations for high precision computing.
- Symbolic algebra package and exact equation solvers (experimental)
- Numerical ODE and PDE solvers (coming soon)
- Machine learning library (coming soon)

Full documentation and examples are available at: http://lineardotnet.com/  
The library is currently under development; a stable release will be available soon.


## High Performance
Linear.NET is one of the fastest linear algebra libraries for single threaded pure-C# environments. Please see [benchmarks](http://lineardotnet.com/docs/#Benchmarks) for performance comparisons with other common C# numerical libraries.

The library uses Strassen-like algorithms to accelerate almost all of its dense level-3 routines, such as matrix multiplication, matrix inversion, Cholesky factorization, QR factorization and LU factorization. The lower algorithmic complexity leads to noticeable performance improvements in the range of 2-10x for large matrices. 

For implementation details, please see our documentation at http://lineardotnet.com/docs/.

## Flexible Framework
The matrix library supports many different types of storage schemes designed to increase accessibility and maximize the ability to exploit problem structure. In addition to the 11 matrix types, the matrix library also supports operations over rectangular matrices `T[,]` native to C#.

Vectors, matrices and tensors can be constructed over the following primitives: `int`, `long`, `float`, `double` and `decimal`. They also support any custom type that implement one of the exposed interfaces `IAdditiveGroup<T>`, `IRing<T>` and `IField<T>`. The package comes with several useful implementations of custom structs, such as `Complex`, `Rational`, `Quaternion`, `Polynomial<T>`, `int128`, `CyclicGroup<T>`, `BigDecimal`, `BigComplex` and`BigRational`, all of which can be used as entries in a matrix. 

For more info, please visit the documentation at  http://lineardotnet.com/docs/.