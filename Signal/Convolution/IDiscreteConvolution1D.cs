﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LinearNet.Signal.Convolution
{
    public interface IDiscreteConvolution1D<T>
    {
        /// <summary>
        /// Calculate the convolution $f * g$ from two discrete series <txt>x, y</txt> representing the outputs of 
        /// two discrete functions $f$ and $g$ respectively. The outputs of $f * g$ is stored in the <txt>convolution</txt>
        /// array.
        /// </summary>
        /// <param name="x">The outputs of the discrete function $f$.</param>
        /// <param name="y">The outputs of the discrete function $g$.</param>
        /// <param name="convolution">The outputs of the discrete function $f * g$ (populated by the method)</param>
        /// <param name="xlen">The number of terms in array <txt>x</txt> to transform</param>
        /// <param name="ylen">The number of terms in array <txt>y</txt> to transform</param>
        void Transform(T[] x, T[] y, T[] convolution, int xlen, int ylen);
    }
}
