﻿using LinearNet.Providers;
using System;
using System.Collections.Generic;
using System.Text;

namespace LinearNet.Signal.Convolution
{
    public class NaiveDiscreteConvolution<T> : IDiscreteConvolution1D<T> where T : new()
    {
        private readonly IProvider<T> _provider;

        public NaiveDiscreteConvolution() : this(ProviderFactory.GetDefaultProvider<T>()) 
        { 

        }
        internal NaiveDiscreteConvolution(IProvider<T> provider)
        {
            _provider = provider;
        }

        public void Transform(T[] a, T[] b, T[] convolution, int alen, int blen)
        {
            if (a is null) throw new ArgumentNullException(nameof(a));
            if (b is null) throw new ArgumentNullException(nameof(b));
            if (convolution is null) throw new ArgumentNullException(nameof(convolution));
            if (alen < 0 || alen > a.Length) throw new ArgumentOutOfRangeException(nameof(alen));
            if (blen < 0 || blen > b.Length) throw new ArgumentOutOfRangeException(nameof(blen));

            int d = alen + blen - 1, i, j;
            if (convolution.Length < d)
            {
                throw new ArgumentOutOfRangeException(nameof(convolution));
            }

            // Initialize array with zeroes, because there is an initialization issue with using new() in rationals...
            // ends up with NaN's because of the division by zero.
            for (i = 0; i < d; ++i)
            {
                convolution[i] = _provider.Zero;
            }

            for (i = 0; i < alen; ++i)
            {
                for (j = 0; j < blen; ++j)
                {
                    int index = i + j;
                    convolution[index] = _provider.Add(convolution[index], _provider.Multiply(a[i], b[j]));
                }
            }
        }
    }
}
