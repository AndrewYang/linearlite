﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LinearNet.Global;
using LinearNet.Structs;

namespace LinearNet.Signal.Fourier
{
    public class CooleyTukeyAlgorithm : IDiscreteFourierTransform
    {
        // Assumes that all checks have already been performed
        private static void FFT(Complex[] x, Complex[] X, int start, int N, int step)
        {
            if (N == 1)
            {
                X[0] = x[0];
                return;
            }

            int half_N = N / 2,
                step2 = step * 2;

            FFT(x, X, start, half_N, step2);
            FFT(x, X, start + half_N, half_N, step2);

            double theta = Constants.TwoPi / N;
            for (int k = 0; k < half_N; ++k)
            {
                double angle = theta * k;
                Complex t = X[k], wp = new Complex(Math.Cos(angle), -Math.Sin(angle));
                X[k] = t + wp * X[k + half_N];
                X[k + half_N] = t - wp * X[k + half_N];
            }
        }

        public override void Transform(Complex[] x, Complex[] X, int N = -1, int step = 1)
        {
            if (x == null) throw new ArgumentNullException(nameof(x));
            if (X == null) throw new ArgumentNullException(nameof(X));
            if (step <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(step), ExceptionMessages.NonPositiveNotAllowed);
            }

            if (N < 0)
            {
                N = x.Length / step;
            }
            else
            {
                if ((N - 1) * step >= x.Length)
                {
                    throw new ArgumentOutOfRangeException(nameof(N));
                }
            }

            // For now
            if (!Util.IsPowerOf2(N))
            {
                throw new NotImplementedException();
            }

            FFT(x, X, 0, N, step);

            // Account for sorting?
        }
    }
}
