﻿using LinearNet.Structs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Signal.Fourier
{
    /// <summary>
    /// An abstract parent class for all implementations of Discrete Fourier Transform algorithms
    /// </summary>
    public abstract class IDiscreteFourierTransform
    {
        /// <summary>
        /// Calculates the discrete fourier transform of elements of array x. 
        /// </summary>
        /// <param name="x">The input values</param>
        /// <param name="X">The transformed values</param>
        /// <param name="N">
        /// The number of terms in the DFT (defaults to -1). If -1, then as many terms as possible will be used, 
        /// given the size of the array 'x'.
        /// </param>
        /// <param name="step">The step size (default = 1), DFT will be calculated on x[0], x[step], x[2 * step], ... x[(N - 1) * step]</param>
        public abstract void Transform(Complex[] x, Complex[] X, int N = -1, int step = 1);
    }
}
