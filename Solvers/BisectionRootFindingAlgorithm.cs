﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LinearNet.Optimisation;
using LinearNet.Structs;

namespace LinearNet.Solvers
{
    /// <summary>
    /// Implementation of the <a href="https://en.wikipedia.org/wiki/Bisection_method">bisection method</a> for 
    /// finding roots of real and complex continuous functions.
    /// </summary>
    /// <cat>solvers</cat>
    public class BisectionRootFindingAlgorithm : IRootFindingAlgorithm<double[], double>, IRootFindingAlgorithm<Complex[], Complex>
    {
        /// <summary>
        /// Solver will terminate if the solution is within '_eps' of the true solution in a L2 sense,
        /// i.e. |x - x*|_2 < _eps, where x is our estimate and x* is the true zero of the function.
        /// </summary>
        private readonly double _eps = 1e-60;
        private readonly int _maxitr = 1000;

        private readonly double[] _a, _b;

        public BisectionRootFindingAlgorithm(double[] a, double[] b)
        {
            _a = a ?? throw new ArgumentNullException(nameof(a));
            _b = b ?? throw new ArgumentNullException(nameof(b));
        }

        public Solution<double[]> Solve(Function<double[], double> function)
        {
            if (function == null)
            {
                throw new ArgumentNullException(nameof(function));
            }

            double fa = function.Evaluate(_a), fb = function.Evaluate(_b);
            int sign_fa = Math.Sign(fa), sign_fb = Math.Sign(fb);
            if (sign_fa == 0) return new Solution<double[]>(ExitCode.Success, _a.Copy());
            if (sign_fb == 0) return new Solution<double[]>(ExitCode.Success, _b.Copy());

            if (sign_fa == sign_fb)
            {
                throw new InvalidOperationException($"f(a) and f(b) must have opposite signs. Currently f(a) = { fa } and f(b) = { fb }");
            }

            int dim = _a.Length;
            double[] a = _a.Copy(), b = _b.Copy(), c = new double[dim];
            for (int i = 0; i < _maxitr; ++i)
            {
                CalculateMidpoint(a, b, c);

                double fc = function.Evaluate(c);
                if (fc == 0 || a.DistTo(b) / 2 < _eps)
                {
                    // within _eps of real solution
                    return new Solution<double[]>(ExitCode.Success, c);
                }

                // Try to use the cached sign(f(a)) 
                // instead of re-calculating f(a) 
                if (Math.Sign(fc) == sign_fa)
                {
                    Array.Copy(c, 0, a, 0, dim);
                    sign_fa = Math.Sign(fc);
                }
                else
                {
                    Array.Copy(c, 0, b, 0, dim);
                }
            }

            // Reached max iterations without converging
            return new Solution<double[]>(ExitCode.MayNotHaveConverged, c);
        }

        /// <summary>
        /// Calculate the midpoint of two vectors a, b and store it in 'midpoint'
        /// </summary>
        private static void CalculateMidpoint(double[] a, double[] b, double[] midpoint)
        {
            int len = a.Length, i;
            for (i = 0; i < len; ++i)
            {
                midpoint[i] = 0.5 * (a[i] + b[i]);
            }
        }

        public Solution<Complex[]> Solve(Function<Complex[], Complex> function)
        {
            throw new NotImplementedException();
        }
    }
}
