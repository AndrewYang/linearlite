﻿using LinearNet.Optimisation;
using LinearNet.Structs;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Solvers
{
    /// <summary>
    /// Implementation of <a href="https://en.wikipedia.org/wiki/Brent%27s_method">Brent's method</a> for finding roots of a single continuous function within an interval 
    /// without the use of gradient information. 
    /// 
    /// <para>
    /// Brent's method is more numerically stable than Dekker's method on which it is based. The algorithm
    /// uses the same fast-converging secant method as in Dekker, but falls back to the more stable bisection 
    /// method if needed.
    /// </para>
    /// 
    /// <para>
    /// Also see: ChandrupatlaRootFindingAlgorithm for an implementation of Chandrupatla's method, which converges faster 
    /// for roots with multiplicity > 1.
    /// </para>
    /// 
    /// <para>
    /// This implementation is for solving single function systems. For the multivariate version, please see
    /// Broyden's method
    /// </para>
    /// </summary>
    /// <cat>solvers</cat>
    public class BrentsRootFindingAlgorithm : IRootFindingAlgorithm<double[], double>, IRootFindingAlgorithm<Complex[], Complex>
    {
        private readonly int _maxIterations = 100;

        // Program terminates successfully if |x - x*| < eps where | | is the Euclidean norm, 
        // x* is the true solution to f(x) = 0, and x is our calculated estimate.
        private readonly double _eps = 1e-60;

        private readonly double[] _a, _b;

        public BrentsRootFindingAlgorithm(double[] a, double[] b)
        {
            if (a == null)
            {
                throw new ArgumentNullException(nameof(a));
            }
            if (b == null)
            {
                throw new ArgumentNullException(nameof(b));
            }
            if (a.Length != b.Length)
            {
                throw new InvalidOperationException("Dimensions of boundary vectors don't match.");
            }

            _a = a;
            _b = b;
        }

        public Solution<double[]> Solve(Function<double[], double> function)
        {
            if (function == null)
            {
                throw new ArgumentNullException(nameof(function));
            }

            double fa = function.Evaluate(_a);
            if (fa == 0.0)
            {
                return new Solution<double[]>(ExitCode.Success, _a.Copy());
            }

            double fb = function.Evaluate(_b);
            if (fb == 0.0)
            {
                return new Solution<double[]>(ExitCode.Success, _b.Copy());
            }

            // Check signs
            if (fa * fb > 0.0)
            {
                throw new InvalidOperationException("Function does not bracket a zero within the specified interval.");
            }

            int dim = _a.Length;

            double[] a, b, c, d = new double[dim], s = new double[dim];
            // Perform swap if |f(a)| < |f(b)|
            if (Math.Abs(fa) < Math.Abs(fb))
            {
                a = _b.Copy();
                b = _a.Copy();
            }
            else
            {
                a = _a.Copy();
                b = _b.Copy();
            }
            c = a.Copy();

            bool usingMidpoint = true;
            double epsSqrd = _eps * _eps;
            double fc = fa;
            for (int i = 0; i < _maxIterations; ++i)
            {
                // Use inverse quadratic interpolation
                if (fa != fc && fb != fc)
                {
                    CalculateUsingQuadraticInterpolation(a, b, c, fa, fb, fc, s);
                }
                else
                {
                    CalculateUsingSecantMethod(a, b, fa, fb, s);
                }

                // Check each of the 5 conditions for defaulting to midpoint
                double lambda = CalculateLambda(a, b, s);
                double s_b = s.SqrdDistTo(b), b_c = b.SqrdDistTo(c), c_d = c.SqrdDistTo(d);

                
                if ((lambda < 0.25 || lambda > 1.0) ||      // Check if x is on the correct side of (3a + b)/4
                    (usingMidpoint && s_b >= b_c / 4) ||
                    (!usingMidpoint && s_b >= c_d / 4) ||
                    (usingMidpoint && b_c < epsSqrd) ||
                    (!usingMidpoint && c_d < epsSqrd))
                {
                    CalculateUsingBisectionMethod(a, b, s);
                    usingMidpoint = true;
                }
                else
                {
                    usingMidpoint = false;
                }

                // f(s) requires tracking from this point onwards ------------------------
                double fs = function.Evaluate(s);

                // d <- c
                Array.Copy(c, 0, d, 0, dim);

                // c <- b
                Array.Copy(b, 0, c, 0, dim);

                // f(a) requires tracking from this point onwards ------------------------
                fa = function.Evaluate(a);
                if (fa * fs < 0)
                {
                    // b <- s
                    Array.Copy(s, 0, b, 0, dim);
                }
                else
                {
                    // a <- s
                    Array.Copy(s, 0, a, 0, dim);
                    fa = fs;
                }

                // f(b) requires tracking from this point onwards ------------------------
                fb = function.Evaluate(b);
                if (Math.Abs(fa) < Math.Abs(fb))
                {
                    Util.Swap(ref a, ref b);
                    Util.Swap(ref fa, ref fb);
                }

                fc = function.Evaluate(c);
                if (fc == 0.0)
                {
                    return new Solution<double[]>(ExitCode.Success, s);
                }
                if (a.SqrdDistTo(b) < epsSqrd)
                {
                    return new Solution<double[]>(ExitCode.Success, s);
                }
            }

            return new Solution<double[]>(ExitCode.MayNotHaveConverged, s);
        }
        private static void CalculateUsingQuadraticInterpolation(double[] a, double[] b, double[] c, double fa, double fb, double fc, double[] s) 
        {
            double a_factor = fb * fc / ((fa - fb) * (fa - fc));
            double b_factor = fa * fc / ((fb - fa) * (fb - fc));
            double c_factor = fa * fb / ((fc - fa) * (fc - fb));

            int len = a.Length, i;
            for (i = 0; i < len; ++i)
            {
                s[i] = a[i] * a_factor + b[i] * b_factor + c[i] * c_factor;
            }
        }
        private static void CalculateUsingSecantMethod(double[] a, double[] b, double fa, double fb, double[] s)
        {
            double factor = fb / (fb - fa);

            int len = a.Length, i;
            for (i = 0; i < len; ++i)
            {
                s[i] = b[i] - factor * (b[i] - a[i]);
            }
        }
        private static void CalculateUsingBisectionMethod(double[] a, double[] b, double[] s)
        {
            int len = a.Length, i;
            for (i = 0; i < len; ++i)
            {
                s[i] = 0.5 * (a[i] + b[i]);
            }
        }

        /// <summary>
        /// lambda = (s - a).(b - a) / |b - a|
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="s"></param>
        private static double CalculateLambda(double[] a, double[] b, double[] s)
        {
            int len = a.Length, i;
            double lambda = 0.0, den = 0.0;
            for (i = 0; i < len; ++i)
            {
                double ba = b[i] - a[i];
                lambda += (s[i] - a[i]) * ba;
                den += ba * ba;
            }

            if (den == 0.0)
            {
                return 0.0;
            }
            return lambda / den;
        }

        public Solution<Complex[]> Solve(Function<Complex[], Complex> function)
        {
            throw new NotImplementedException();
        }

    }
}
