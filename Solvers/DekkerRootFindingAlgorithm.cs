﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LinearNet.Global;
using LinearNet.Optimisation;

namespace LinearNet.Solvers
{
    /// <summary>
    /// <para>
    /// Implementation of Dekker's method for finding roots of a univariate or multivariate function without 
    /// relying on gradient information. 
    /// </para>
    /// <para>
    /// For more information, please see <a href="https://en.wikipedia.org/wiki/Brent%27s_method">the wikipedia entry on Brent's method.</a>
    /// </para>
    /// <para>
    /// Also see: <a href="">Brent's method</a>, which converges faster than Dekker's method under most circumstances.
    /// </para>
    /// </summary>
    /// <cat>solvers</cat>
    public class DekkerRootFindingAlgorithm : IRootFindingAlgorithm<double[], double>
    {
        /// <summary>
        /// Program will terminate when |f(b_k) - f_(b_{k-1})| < _deltaEpsilon
        /// </summary>
        private readonly double _deltaEpsilon = 0;
        /// <summary>
        /// Iteration will terminate when |f(b_k)| < _epsilon
        /// </summary>
        private readonly double _epsilon = 1e-60;
        /// <summary>
        /// Iteration will terminate when max iteratons is reached
        /// </summary>
        private readonly int _maxIterations = 1000;
        private readonly double[] _a, _b;

        /// <summary>
        /// <para>
        /// Dekker's root finding algorithm requires two points a, b such that f(a) and f(b) 
        /// have opposite signs (i.e. f(a) < 0 and f(b) > 0, or f(a) > 0 and f(b) < 0).
        /// </para>
        /// <para>
        /// The algorithm will attempt to find the solution x such that a < x < b.
        /// </para>
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        public DekkerRootFindingAlgorithm(double a, double b)
        {
            if (double.IsNaN(a))
            {
                throw new ArgumentOutOfRangeException(nameof(a), ExceptionMessages.NaNNotAllowed);
            }
            if (double.IsInfinity(a))
            {
                throw new ArgumentOutOfRangeException(nameof(a), ExceptionMessages.InfinityNotAllowed);
            }
            if (double.IsNaN(b))
            {
                throw new ArgumentOutOfRangeException(nameof(b), ExceptionMessages.NaNNotAllowed);
            }
            if (double.IsInfinity(b))
            {
                throw new ArgumentOutOfRangeException(nameof(b), ExceptionMessages.InfinityNotAllowed);
            }

            _a = new double[] { a };
            _b = new double[] { b };
        }

        public Solution<double[]> Solve(Function<double[], double> function)
        {
            if (function == null)
            {
                throw new ArgumentNullException(nameof(function));
            }

            double fa = function.Evaluate(_a);
            double fb = function.Evaluate(_b);
            if (double.IsNaN(fa))
            {
                throw new ArgumentOutOfRangeException("f(a) is NaN.");
            }
            if (double.IsNaN(fb))
            {
                throw new ArgumentOutOfRangeException("f(b) is NaN.");
            }

            int sign_a = Math.Sign(fa);
            int sign_b = Math.Sign(fb);
            if (sign_a == 0)
            {
                return new Solution<double[]>(ExitCode.Success, _a.Copy());
            }
            if (sign_b == 0)
            {
                return new Solution<double[]>(ExitCode.Success, _b.Copy());
            }

            if (sign_a == sign_b)
            {
                throw new InvalidOperationException($"f(a) and f(b) must have opposite signs. Currently f(a) = { fa } and f(b) = { fb }");
            }

            // We require that |f(a)| > |f(b)|
            double[] ak = _a.Copy(), bk = _b.Copy(), bprev = _b.Copy();

            double f_prevEstimate = double.PositiveInfinity;
            double f_estimate = fb;
            double eps = 1e-30;
            int itr = 0;

            while (ContinueIterating(f_prevEstimate, f_estimate, itr))
            {
                double[] bnew = CalculateNextEstimate(bprev, bk, ak, f_estimate, f_prevEstimate, eps);

                double f_b_new = function.Evaluate(bnew);

                // Choose the contrapoint - ensure that ak and bk have different signs
                if (Math.Sign(function.Evaluate(ak)) == Math.Sign(f_b_new))
                {
                    ak = bk;
                }

                // Perform switch if |f(ak)| < |f(bk)|, since it is likely to be a better estimate
                if (Math.Abs(function.Evaluate(ak)) < Math.Abs(f_b_new))
                {
                    double[] tmp = bnew;
                    bnew = ak;
                    ak = tmp;
                }

                // Update point estimates 
                bprev = bk;
                bk = bnew;

                // Update function values
                f_prevEstimate = f_estimate;
                f_estimate = function.Evaluate(bnew);

                ++itr;
            }

            ExitCode code = itr == _maxIterations ? ExitCode.MayNotHaveConverged : ExitCode.Success;
            return new Solution<double[]>(code, bk);
        }

        /// <summary>
        /// Checks whether the termination criteria have been met. Returns true if we should continue iterating.
        /// </summary>
        /// <param name="f_prevEstimate"></param>
        /// <param name="f_estimate"></param>
        /// <param name="iteration"></param>
        /// <returns></returns>
        private bool ContinueIterating(double f_prevEstimate, double f_estimate, int iteration)
        {
            // Terminate if maximum number of iterations is reached
            if (iteration >= _maxIterations)
            {
                return false;
            }

            // Terminate if |f(x)| < eps
            if (Math.Abs(f_estimate) < _epsilon)
            {
                return false;
            }

            // Terminate if |f(x) - f(y)| < eps where y is the previous estimate
            if (Math.Abs(f_estimate - f_prevEstimate) < _deltaEpsilon)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Calculate the next root estimate using the <a href="https://en.wikipedia.org/wiki/Brent%27s_method">Dekker's method</a>, 
        /// given the current root estimate and the previous root estimate.
        /// 
        /// The method calculates vectors s and m according to:
        /// s := (bk - bprev) / (f(bk) - f(bprev)) * f(bk) 
        /// m := (ak + bk) / 2
        /// if f(bk) - f(bprev) = 0, then s := m.
        /// 
        /// If s is closer to bk than a, then s is returned as the next estimate. Otherwise, m is returned.
        /// </summary>
        /// <param name="bprev">b_{k-1}</param>
        /// <param name="bk">b_k</param>
        /// <param name="ak">a_k</param>
        /// <param name="f_estimate">f(b_k)</param>
        /// <param name="f_prevEstimate">f(b_{k-1})</param>
        /// <param name="eps">The threshold under which a double is considered identically zero.</param>
        /// <returns></returns>
        private double[] CalculateNextEstimate(double[] bprev, double[] bk, double[] ak, double f_estimate, double f_prevEstimate, double eps = Precision.DOUBLE_PRECISION)
        {
            if (double.IsNaN(f_estimate) || double.IsNaN(f_prevEstimate))
            {
                return null;
            }


            // If f(b_{k-1}) = f(b_k) (within floating-point precision), return the midpoint
            if (Util.ApproximatelyEquals(f_prevEstimate, f_estimate, eps))
            {
                return LinearIntrapolate(ak, bk, 0.5);
            }

            // Otherwise, use the secant method
            double r = -f_estimate / (f_estimate - f_prevEstimate);
            int len = bprev.Length;
            double[] est = new double[len];
            for (int i = 0; i < len; ++i)
            {
                est[i] = bk[i] + r * (bk[i] - bprev[i]);
            }

            // If the result is closer to A_k than B_k, use the midpoint, m, instead
            if (SqrdDistance(bk, est) >= SqrdDistance(ak, est))
            {
                return LinearIntrapolate(ak, bk, 0.5);
            }
            return est;
        }

        /// <summary>
        /// Returns s * a + (1 - s) * b for vectors a, b and scalar s.
        /// </summary>
        private double[] LinearIntrapolate(double[] a, double[] b, double s)
        {
            double r = 1.0 - s;
            int len = a.Length, i;
            double[] result = new double[len];
            for (i = 0; i < len; ++i)
            {
                result[i] = s * a[i] + r * b[i];
            }
            return result;
        }

        /// <summary>
        /// Returns the squared distance L2 distance between two vectors a, b.
        /// </summary>
        private double SqrdDistance(double[] a, double[] b)
        {
            int len = a.Length, i;
            double ds = 0.0;
            for (i = 0; i < len; ++i)
            {
                double d = a[i] - b[i];
                ds += d * d;
            }
            return ds;
        }
    }
}
