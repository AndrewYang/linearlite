﻿using LinearNet.Optimisation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Solvers
{
    /// <summary>
    /// An interface for solving the equation $f(x) = 0$ where $f: $ <txt>TInput</txt> $\to$ <txt>TOutput</txt>.
    /// The requirements that $f(x)$ must satisfy depends on the implementation of the root finding algorithm. 
    /// For example, <txt>BisectionRootFindingAlgorithm</txt>, <txt>DekkerRootFindingAlgorithm</txt> and <txt>BrentsRootFindingAlgorithm</txt>
    /// all require the function to be continuous within some neighbourbood of the root to be found. Newton's method 
    /// additionally requires the existence of a derivative over that neighbourhood. 
    /// </summary>
    /// <typeparam name="TInput">The input type of the function to be solved.</typeparam>
    /// <typeparam name="TOutput">The output type of the function to be solved.</typeparam>
    /// <cat>solvers</cat>
    public interface IRootFindingAlgorithm<TInput, TOutput>
    {
        /// <summary>
        /// Solves the equation $f(x) = 0$ for some differentiable function $f:$ <txt>T</txt> $\to$ <txt>S</txt>
        /// </summary>
        /// <param name="function">A differentiable function</param>
        /// <returns></returns>
        Solution<TInput> Solve(Function<TInput, TOutput> function);
    }
}
