﻿using LinearNet.Providers;
using LinearNet.Providers.Basic.Interfaces;
using LinearNet.Structs;
using System;

namespace LinearNet.Solvers.Linear
{
    /// <summary>
    /// Class for solving banded systems
    /// </summary>
    public class BandSolver<T> : ILinearSolver<BandMatrix<T>, DenseVector<T>> where T : new()
    {
        private readonly T _threshold;
        private readonly bool _pivotRows;
        private readonly IRealProvider<T> _realProvider;
        private readonly IDenseBLAS1<T> _blas1;

        internal BandSolver(IRealProvider<T> provider, IDenseBLAS1<T> blas, T threshold, bool pivotRows = true)
        {
            _realProvider = provider;
            _threshold = threshold;
            _blas1 = blas;
            _pivotRows = pivotRows;
        }

        /// <summary>
        /// Converts the band matrix defined by the tuple (lower, diagonal, upper, rows, columns)
        /// into a sparse matrix stored by row, in the jagged array 'values'. Each row is stored
        /// as a contiguous block (with zeroes between two non-zero elements explicitly stored). 
        /// 'startingIndex' is populated with the column index of the first non-zero entry in each row.
        /// </summary>
        private static void ConvertToRowStorage(T[][] lower, T[] diagonal, T[][] upper, int rows, int columns, T[][] values, int[] startingIndex)
        {
            int lowerBandwidth = lower.Length, upperBandwidth = upper.Length;
            for (int r = 0; r < rows; ++r)
            {
                int col_start = Math.Max(0, r - lowerBandwidth),
                    col_end = Math.Min(columns, r + upperBandwidth + 1);

                startingIndex[r] = Math.Max(0, col_start);

                T[] row = new T[col_end - col_start];
                int k = 0;
                // lower values first 
                for (int b = lowerBandwidth - 1; b >= 0; --b)
                {
                    int index = r - b - 1;
                    if (index >= 0)
                    {
                        row[k++] = lower[b][index];
                    }
                }
                // diagonal terms next
                if (r < diagonal.Length)
                {
                    row[k++] = diagonal[r];
                }
                // upper last
                for (int b = 0; b < upperBandwidth; ++b)
                {
                    T[] band = upper[b];
                    if (r < band.Length)
                    {
                        row[k++] = band[r];
                    }
                }
                values[r] = row;
            }
        }
        private void Clear(T[] x, int start, int end)
        {
            IProvider<T> provider = _blas1.Provider;
            T zero = provider.Zero;

            if (provider.Equals(zero, default))
            {
                Array.Clear(x, start, end - start);
            }
            else
            {
                for (int i = start; i < end; ++i)
                {
                    x[i] = zero;
                }
            }
        }

        private static ExitCode UpperTriangularSolve(T[][] values, int[] startingIndex, T[] x, int columns, IProvider<T> provider)
        {
            // Backward solve the square system
            for (int r = columns - 1; r >= 0; --r)
            {
                T[] row = values[r];
                int start = startingIndex[r], end = start + row.Length;

                T br = x[r];
                for (int c = end - 1; c > r; --c)
                {
                    int index = c - start;
                    if (index >= 0)
                    {
                        br = provider.Subtract(br, provider.Multiply(row[index], x[c]));
                    }
                }

                T diag = row[r - start];
                if (provider.Equals(diag, provider.Zero))
                {
                    return ExitCode.InconsistentEquations;
                }
                x[r] = provider.Divide(br, diag);
            }
            return ExitCode.Success;
        }

        /// <summary>
        /// Solve the banded system Bx = b where B is a band matrix defined by the tuple 
        /// (lower, diagonal, upper, rows, columns) and x and b are dense vectors
        /// 
        /// This method uses pivoting and is suitable for non diagonally dominant systems
        /// </summary>
        internal ExitCode SolveWithPivoting(T[][] lower, T[] diagonal, T[][] upper, int rows, int columns, T[] rhsVector, T[] x)
        {
            IProvider<T> prov = _blas1.Provider;

            // Arrange into horizontal form for easier row swap
            T[][] values = new T[rows][];

            // Keey track of the starting index of each row
            int[] startingIndex = new int[rows];

            ConvertToRowStorage(lower, diagonal, upper, rows, columns, values, startingIndex);

            // Copy the RHS vector as it will be modified 
            Array.Copy(rhsVector, x, columns);

            // Gaussian elimination
            for (int c = 0; c < columns; ++c)
            {
                // Find the entry with the maximum norm in column c
                T maxNorm = startingIndex[c] <= c ? _realProvider.Abs(values[c][c - startingIndex[c]]) : prov.Zero;
                int pivotRowIndex = c;
                for (int r = c + 1; r < rows; ++r)
                {
                    int start = startingIndex[r], end = start + values[r].Length;
                    if (start <= c && c < end)
                    {
                        T norm = _realProvider.Abs(values[r][c - start]);
                        if (_realProvider.Compare(norm, maxNorm) > 0)
                        {
                            maxNorm = norm;
                            pivotRowIndex = r;
                        }
                    }
                }

                // Perform pivot
                if (pivotRowIndex != c)
                {
                    Util.Swap(ref values[pivotRowIndex], ref values[c]);
                    Util.Swap(ref startingIndex[pivotRowIndex], ref startingIndex[c]);
                    Util.Swap(ref x[pivotRowIndex], ref x[c]);
                }

                // Gaussian elimination in [r + 1...Rows)
                T[] pivotRow = values[c];
                int pivotRowStart = startingIndex[c], pivotRowEnd = pivotRowStart + pivotRow.Length;
                T pivot = pivotRow[c - pivotRowStart];
                T xc = x[c];
                for (int r = c + 1; r < rows; ++r)
                {
                    int start = startingIndex[r], end = start + values[r].Length;
                    if (start <= c)
                    {
                        T[] row = values[r];
                        T alpha = prov.Divide(row[c - start], pivot);

                        // Check if current row is sufficient as a workspace
                        if (end >= pivotRowEnd)
                        {
                            _blas1.AXPY(row, pivotRow, prov.Negate(alpha), c - start, pivotRowEnd - start, start - pivotRowStart);
                        }
                        else
                        {
                            // Need a bigger workspace 
                            T[] ws = RectangularVector.Zeroes<T>(pivotRowEnd - c);
                            Array.Copy(row, c - start, ws, 0, row.Length - c + start);

                            startingIndex[r] = c;
                            values[r] = ws;
                            _blas1.AXPY(ws, pivotRow, prov.Negate(alpha), 0, pivotRowEnd - c, c - pivotRowStart);
                        }
                        x[r] = prov.Subtract(x[r], prov.Multiply(xc, alpha));
                    }
                }
            }

            // Backward solve the square system
            return UpperTriangularSolve(values, startingIndex, x, columns, prov);
        }

        /// <summary>
        /// Solve the banded system Bx = b where B is a band matrix defined by the tuple
        /// (lower, diagonal, upper, rows, columns) and x and b are dense vectors
        /// 
        /// This method does not use pivoting and is only guaranteed to be stable for 
        /// diagonally dominant systems. Typically it is much faster than the method using 
        /// pivoting to ensure stability.
        internal ExitCode SolveWithoutPivoting(T[][] lower, T[] diagonal, T[][] upper, int rows, int columns, T[] rhsVector, T[] x)
        {
            IProvider<T> provider = _blas1.Provider;

            T[][] values = new T[rows][];
            int[] startingIndex = new int[rows];
            ConvertToRowStorage(lower, diagonal, upper, rows, columns, values, startingIndex);

            Array.Copy(rhsVector, x, columns);

            // There is no array resizing required, since all modified rows fit within the band structure
            int lowerBandwidth = lower.Length;
            for (int c = 0; c < columns; ++c)
            {
                T[] row_c = values[c];
                int start_c = startingIndex[c], end_c = start_c + row_c.Length;
                T pivot = row_c[c - start_c], 
                    xc = x[c];

                int last_row = Math.Min(rows, c + lowerBandwidth + 1);
                for (int r = c + 1; r < last_row; ++r)
                {
                    T[] row_r = values[r];
                    int start_r = startingIndex[r];

                    if (c >= start_r)
                    {
                        T alpha = provider.Divide(row_r[c - start_r], pivot);
                        _blas1.AXPY(row_r, row_c, provider.Negate(alpha), c - start_r, end_c - start_r, start_r - start_c);
                        x[r] = provider.Subtract(x[r], provider.Multiply(xc, alpha));
                    }
                }
            }

            return UpperTriangularSolve(values, startingIndex, x, columns, provider);
        }

        /// <summary>
        /// Solve the tridiagonal system Bx = b where B is tridiagonal (upper and lower bandwidths both 1) and 
        /// both x and b are dense vectors. No pivoting is used.
        /// </summary>
        internal ExitCode SolveTridiagonal(T[] lower, T[] _diagonal, T[] upper, int rows, int columns, T[] rhsVector, T[] x)
        {
            IProvider<T> provider = _blas1.Provider;
            T zero = provider.Zero;

            // Only the diagonal is modified
            T[] diagonal = _diagonal.Copy();

            // Copy x <- b
            Array.Copy(rhsVector, x, columns);

            int last_col = columns - 1;
            for (int c = 0; c < last_col; ++c)
            {
                T diag = diagonal[c];
                if (provider.Equals(diag, zero))
                {
                    return ExitCode.InconsistentEquations;
                }

                T alpha = provider.Divide(lower[c], diag);
                //lower[c] = zero;

                int c1 = c + 1;
                diagonal[c1] = provider.Subtract(diagonal[c1], provider.Multiply(alpha, upper[c]));
                x[c1] = provider.Subtract(x[c1], provider.Multiply(alpha, x[c]));
            }

            // backward solve
            x[last_col] = provider.Divide(x[last_col], diagonal[last_col]);
            for (int r = last_col - 1; r >= 0; --r)
            {
                x[r] = provider.Divide(provider.Subtract(x[r], provider.Multiply(x[r + 1], upper[r])), diagonal[r]);
            }
            return ExitCode.Success;
        }

        /// <summary>
        /// Solve the lower bidiagonal system Bx = b where B is lower diagonal (lower bandwidth of 1, upper bandwidth of 0)
        /// and x and b are dense vectors. No pivoting is used.
        /// </summary>
        internal ExitCode SolveLowerBidiagonal(T[] lower, T[] _diagonal, int rows, int columns, T[] rhsVector, T[] x, T threshold)
        {
            IProvider<T> provider = _blas1.Provider;
            int last_col = columns - 1, min = Math.Min(rows, columns);

            // Only the diagonal is modified
            T[] diagonal = _diagonal.Copy();

            // Copy x <- b
            Array.Copy(rhsVector, x, min);

            // Default exit code
            ExitCode code = ExitCode.Success;

            // Overdetermined system, check the last lower diagonal term
            // to see if its zero
            if (lower.Length >= columns)
            {
                T norm = _realProvider.Abs(lower[last_col]);
                if (_realProvider.Compare(norm, threshold) > 0)
                {
                    return ExitCode.InconsistentEquations;
                }
            }

            // Underdetermined system - pad with zeroes
            if (rows < columns)
            {
                Clear(x, rows, columns);
                code = ExitCode.InfiniteSolutions;
            }

            // Backward solve
            x[0] = provider.Divide(x[0], diagonal[0]);
            for (int c = 1; c < min; ++c)
            {
                int c1 = c - 1;
                x[c] = provider.Divide(provider.Subtract(x[c], provider.Multiply(x[c1], lower[c1])), diagonal[c]);
            }
            return code;
        }

        /// <summary>
        /// Solve the upper bidiagonal system Bx = b where B is upper diagonal
        /// </summary>
        internal ExitCode SolveUpperBidiagonal(T[] _diagonal, T[] upper, int rows, int columns, T[] rhsVector, T[] x)
        {
            IProvider<T> provider = _blas1.Provider;
            int last_col = columns - 1, min = Math.Min(rows, columns);

            // Only the diagonal is modified
            T[] diagonal = _diagonal.Copy();

            // Copy x <- b
            Array.Copy(rhsVector, x, min);

            // Default exit code
            ExitCode code = ExitCode.Success;

            // Underdetermined system - pad with zeroes
            if (rows < columns)
            {
                Clear(x, rows, columns);
                code = ExitCode.InfiniteSolutions;
            }

            // Backward solve
            x[last_col] = provider.Divide(x[last_col], diagonal[last_col]);
            for (int c = last_col - 1; c >= 0; --c)
            {
                x[c] = provider.Divide(provider.Subtract(x[c], provider.Multiply(x[c + 1], upper[c])), diagonal[c]);
            }
            return code;
        }

        /// <summary>
        /// Solve the diagonal system Dx = b where D is a dense diagonal matrix.
        /// </summary>
        internal ExitCode SolveDiagonal(T[] diagonal, int rows, int columns, T[] rhsVector, T[] x)
        {
            IProvider<T> provider = _blas1.Provider;
            ExitCode code = ExitCode.Success;

            // Underdetermined system
            if (rows < columns)
            {
                Clear(x, rows, columns);
                code = ExitCode.InfiniteSolutions;
            }

            int min = Math.Min(rows, columns);
            for (int i = 0; i < min; ++i)
            {
                if (provider.Equals(diagonal[i], provider.Zero))
                {
                    return ExitCode.InconsistentEquations; // no solution
                }
                x[i] = provider.Divide(rhsVector[i], diagonal[i]);
            }

            return code;
        }

        public Solution<DenseVector<T>> Solve(BandMatrix<T> matrix, DenseVector<T> vector)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            if (vector is null)
            {
                throw new ArgumentNullException(nameof(vector));
            }

            T[] x = new T[matrix.Columns];

            ExitCode code;
            if (matrix.IsDiagonal())
            {
                code = SolveDiagonal(matrix.Diagonal, matrix.Rows, matrix.Columns, vector.Values, x);
            }
            else if (matrix.IsLowerBidiagonal)
            {
                code = SolveLowerBidiagonal(matrix.LowerBands[0], matrix.Diagonal,
                    matrix.Rows, matrix.Columns, vector.Values, x, _threshold);
            }
            else if (matrix.IsUpperBidiagonal)
            {
                code = SolveUpperBidiagonal(matrix.Diagonal, matrix.UpperBands[0],
                    matrix.Rows, matrix.Columns, vector.Values, x);
            }
            else if (matrix.IsTridiagonal)
            {
                code = SolveTridiagonal(matrix.LowerBands[0], matrix.Diagonal, matrix.UpperBands[0],
                    matrix.Rows, matrix.Columns, vector.Values, x);
            }
            else
            {
                if (_pivotRows)
                {
                    code = SolveWithPivoting(matrix.LowerBands, matrix.Diagonal, matrix.UpperBands,
                        matrix.Rows, matrix.Columns, vector.Values, x);
                }
                else
                {
                    code = SolveWithoutPivoting(matrix.LowerBands, matrix.Diagonal, matrix.UpperBands,
                        matrix.Rows, matrix.Columns, vector.Values, x);
                }
            }

            return new Solution<DenseVector<T>>(code, new DenseVector<T>(x));
        }
    }
}
