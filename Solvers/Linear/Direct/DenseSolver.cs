﻿using LinearNet.Providers;
using LinearNet.Structs;
using System;
using System.Collections.Generic;

namespace LinearNet.Solvers.Linear
{
    /// <summary>
    /// <p>
    /// Implementation of a dense linear systems solver, for equations of the form 
    /// $$Ax = b$$
    /// where $A \in \mathbb{F}^{m \times n}, x \in \mathbb{F}^{n}$ and $b \in \mathbb{F}^{m}$.
    /// </p>
    /// <p>Both real and complex valued systems are supported.</p>
    /// </summary>
    /// <cat>solvers</cat>
    public class DenseSolver<T> : ILinearSolver<DenseMatrix<T>, DenseVector<T>> where T : new()
    {
        private readonly bool _preserveCoefficientMatrix;
        private readonly IDenseBLAS1<T> _blas;
        private readonly IProvider<T> _provider;
        private readonly Func<T, bool> _isZero;

        private readonly IComparer<T> _sizeComparer;
        private readonly bool _pivotRows;

        /// <summary>
        /// Create a new instance of a dense linear solver, using a level-1 BLAS provider.
        /// </summary>
        /// <param name="provider">A level-1 BLAS implementation.</param>
        /// <param name="preserveCoefficientMatrix">
        /// If <txt>true</txt>, the coefficient matrix $A$ will be preserved. The algorithm is slightly more performant and memory conservative if 
        /// set to <txt>false</txt>.
        /// </param>
        /// <param name="IsZero">
        /// A function taking on a single variable of type <txt>T</txt> which determines whether the element should be 
        /// treated as equal to zero.
        /// </param>
        /// <param name="sizeComparer">
        /// A size comparer that orders the sizes of element of <txt>T</txt>.
        /// </param>
        public DenseSolver(
            IDenseBLAS1Provider<T> provider, 
            bool preserveCoefficientMatrix = true, 
            Func<T, bool> IsZero = null, 
            IComparer<T> sizeComparer = null)
        {
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }

            if (!(provider is IDenseBLAS1<T> blas)) 
            {
                throw new NotSupportedException($"The type { typeof(T) } is not supported");
            }

            _blas = blas;
            _provider = blas.Provider;
            _preserveCoefficientMatrix = preserveCoefficientMatrix;

            // Use the default zero equality strategy
            if (IsZero is null)
            {
                _isZero = new Func<T, bool>(x => _provider.Equals(x, _provider.Zero));
            }
            else
            {
                _isZero = IsZero;
            }

            // Use pivoting if a size comparison is provided.
            if (!(sizeComparer is null))
            {
                _sizeComparer = sizeComparer;
                _pivotRows = true;
            }
            else
            {
                _pivotRows = false;
            }
        }

        /// <summary>
        /// Calculates the solution to the linear system given by a dense matrix $A$ and a dense vector $b$.
        /// </summary>
        /// <param name="matrix"></param>
        /// <param name="vector"></param>
        /// <returns></returns>
        public Solution<DenseVector<T>> Solve(DenseMatrix<T> matrix, DenseVector<T> vector)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            if (vector is null)
            {
                throw new ArgumentNullException(nameof(vector));
            }
            if (matrix.Rows != vector.Dimension)
            {
                throw new InvalidOperationException("The number of rows of the coefficient matrix does not match the vector dimension.");
            }
            if (matrix.Rows < matrix.Columns)
            {
                return new Solution<DenseVector<T>>(ExitCode.InfiniteSolutions, "Not enough equations to solve this system. Infinite solutions exist.");
            }

            T[][] A = _preserveCoefficientMatrix ? matrix.Copy().Values : matrix.Values;
            T[] b = vector.Values.Copy();

            int rows = matrix.Rows, cols = matrix.Columns, r, c;

            // Gaussian elimination
            for (c = 0; c < cols; ++c)
            {
                if (_pivotRows)
                {
                    // pivot on the row r with largest magnitude on element _A[r][c]
                    T max = A[c][c];
                    int swap = c;
                    for (r = c + 1; r < rows; ++r)
                    {
                        if (_sizeComparer.Compare(A[r][c], max) > 0)
                        {
                            max = A[r][c];
                            swap = r;
                        }
                    }

                    // swap the row
                    Util.Swap(ref A[swap], ref A[c]);

                    // swap the b
                    Util.Swap(ref b[swap], ref b[c]);
                }
                

                // perform elimination on row c
                T[] A_c = A[c];
                T b_c = b[c], A_cc = A_c[c];
                for (r = c + 1; r < rows; ++r)
                {
                    T[] A_r = A[r];
                    T s = _provider.Divide(A_r[c], A_cc);
                    _blas.AXPY(A_r, A_c, _provider.Negate(s), c, cols);
                    b[r] = _provider.Subtract(b[r], _provider.Multiply(s, b_c));
                }
            }

            if (rows > cols && !_isZero(A[rows - 1][cols - 1]))
            {
                return new Solution<DenseVector<T>>(ExitCode.InconsistentEquations, "Equations are inconsistent. No solution exists.");
            }
            if (rows == cols && _isZero(A[cols - 1][cols - 1]))
            {
                return new Solution<DenseVector<T>>(ExitCode.InfiniteSolutions, "Not enough equations to solve this system. Infinite solutions exist.");
            }

            // Solve using backsubstitution
            T[] x = new T[cols];
            for (int i = cols - 1; i >= 0; --i)
            {
                T[] A_i = A[i];
                T sum = _provider.Subtract(b[i], _blas.DOT(x, A_i, i + 1, cols));
                x[i] = _provider.Divide(sum, A_i[i]);
            }
            return new Solution<DenseVector<T>>(ExitCode.Success, new DenseVector<T>(x));
        }

        [Obsolete]
        public Solution<double[]> Solve(double[][] A, double[] b, bool preserveA = true)
        {
            MatrixChecks.CheckNotNull(A);
            MatrixChecks.CheckNotNull(b);

            int m = A.GetLength(0), n = A.GetLength(1);
            if (m != b.Length)
            {
                throw new InvalidOperationException("Dimensions don't match.");
            }
            if (m < n)
            {
                return new Solution<double[]>(ExitCode.InfiniteSolutions, "Not enough equations to solve this system. Infinite solutions exist.");
            }

            double[][] _A = preserveA ? A.Copy() : A;
            double[] _b = b.Copy();

            // Gaussian elimination
            for (int c = 0; c < n; ++c)
            {
                // pivot on the row r with largest magnitude on element _A[r][c]
                double max = _A[c][c];
                int swap = c;
                for (int r = c + 1; r < m; ++r)
                {
                    double abs = Math.Abs(_A[r][c]);
                    if (abs > max)
                    {
                        max = abs;
                        swap = r;
                    }
                }

                // swap the row
                double[] tempRow = _A[swap];
                _A[swap] = _A[c];
                _A[c] = tempRow;

                // swap the b
                double tempB = _b[swap];
                _b[swap] = _b[c];
                _b[c] = tempB;

                // perform elimination on row c
                double[] A_c = _A[c];
                double b_c = _b[c], A_cc = A_c[c];
                for (int r = c + 1; r < m; ++r)
                {
                    double[] A_r = _A[r];
                    double s = A_r[c] / A_cc;
                    for (int i = c; i < n; i++)
                    {
                        A_r[i] -= s * A_c[i];
                    }
                    _b[r] -= s * b_c;
                }
            }

            if (m > n && !Util.ApproximatelyEquals(_A[m - 1][n - 1], 0))
            {
                return new Solution<double[]>(ExitCode.InconsistentEquations, "Equations are inconsistent. No solution exists.");
            }
            if (m == n && Util.ApproximatelyEquals(_A[n - 1][n - 1], 0))
            {
                return new Solution<double[]>(ExitCode.InfiniteSolutions, "Not enough equations to solve this system. Infinite solutions exist.");
            }

            // Solve using backsubstitution
            double[] x = new double[n];
            for (int i = n - 1; i >= 0; --i)
            {
                double[] A_i = _A[i];
                double sum = _b[i];
                for (int j = n - 1; j > i; j--)
                {
                    sum -= x[j] * A_i[j];
                }
                x[i] = sum / A_i[i];
            }
            return new Solution<double[]>(ExitCode.Success, x);
        }


        /// <summary>
        /// Solves the integer linear equation system, returning solution in exact form as fractions, 
        /// </summary>
        /// <param name="A"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        [Obsolete]
        public Solution<Rational[]> SolveExact(int[,] A, int[] b)
        {
            MatrixChecks.CheckNotNull(A);
            MatrixChecks.CheckNotNull(b);

            int m = A.GetLength(0), n = A.GetLength(1);
            if (m != b.Length)
            {
                throw new InvalidOperationException("Dimensions don't match.");
            }
            if (m < n)
            {
                return new Solution<Rational[]>(ExitCode.InfiniteSolutions, "Not enough equations to solve this system. Infinite solutions exist.");
            }

            Rational[,] AFrac = A.ToRational();
            Rational[] bFrac = b.ToFraction();

            // Gaussian elimination
            int r, c, i, j;
            for (c = 0; c < n; ++c)
            {
                for (r = c + 1; r < m; ++r)
                {
                    Rational s = (AFrac[r, c] / AFrac[c, c]).Simplify();
                    for (i = c; i < n; i++)
                    {
                        AFrac[r, i] -= s * AFrac[c, i];
                    }
                    bFrac[r] -= s * bFrac[c];
                }
            }

            if (m > n && AFrac[m - 1, n - 1].Equals(Rational.Zero))
            {
                return new Solution<Rational[]>(ExitCode.InconsistentEquations, "Equations are inconsistent. No solution exists.");
            }
            if (m == n && AFrac[n - 1, n - 1].Equals(Rational.Zero))
            {
                return new Solution<Rational[]>(ExitCode.InfiniteSolutions, "Not enough equations to solve this system. Infinite solutions exist.");
            }

            // Solve using backsubstitution
            Rational[] x = new Rational[n];
            for (i = n - 1; i >= 0; --i)
            {
                Rational sum = bFrac[i];
                for (j = n - 1; j > i; j--)
                {
                    sum -= x[j] * AFrac[i, j];
                }
                x[i] = (sum / AFrac[i, i]).Simplify();
            }
            return new Solution<Rational[]>(ExitCode.Success, x);
        }


        /// <summary>
        /// The complex method differs from that of a general Field because C 
        /// is a normed vector space. 
        /// We use the norm to pivot to ensure greater numerical stability
        /// </summary>
        /// <param name="Amatrix"></param>
        /// <param name="bVect"></param>
        /// <param name="tolerance"></param>
        /// <returns></returns>
        [Obsolete]
        public static Solution<Complex[]> Solve(Complex[,] Amatrix, Complex[] bVect, double tolerance = Precision.DOUBLE_PRECISION)
        {
            MatrixChecks.CheckNotNull(Amatrix);
            MatrixChecks.CheckNotNull(bVect);

            int m = Amatrix.GetLength(0), n = Amatrix.GetLength(1);
            if (m != bVect.Length)
            {
                throw new InvalidOperationException("Dimensions don't match.");
            }
            if (m < n)
            {
                return new Solution<Complex[]>(ExitCode.InfiniteSolutions, "Not enough equations to solve this system. Infinite solutions exist.");
            }

            // Solving the linear system requires O(n^3) time, it's worthwhile to spend O(n^2) 
            // time converting T[,] into the more efficient T[][] matrix
            Complex[][] A = Amatrix.ToJagged();
            Complex[] b = bVect.Copy();

            // The method as it stands suffers from numerical instability
            // Gaussian elimination
            for (int c = 0; c < n; ++c)
            {
                // pivot on the row r with largest magnitude on element _A[r][c]
                double max = A[c][c].Modulus();
                int swap = c;
                for (int r = c + 1; r < m; ++r)
                {
                    double abs = A[r][c].Modulus();
                    if (abs > max)
                    {
                        max = abs;
                        swap = r;
                    }
                }

                // swap the row
                Complex[] tempRow = A[swap];
                A[swap] = A[c];
                A[c] = tempRow;

                // swap the b
                Complex tempB = b[swap];
                b[swap] = b[c];
                b[c] = tempB;

                // perform elimination on row c
                Complex[] A_c = A[c];
                Complex b_c = b[c], A_cc_inv = A_c[c].MultiplicativeInverse();
                for (int r = c + 1; r < m; ++r)
                {
                    Complex[] A_r = A[r];
                    Complex s = A_r[c].Multiply(A_cc_inv);
                    for (int i = c; i < n; i++)
                    {
                        A_r[i] = A_r[i].Subtract(s.Multiply(A_c[i]));
                    }
                    b[r] = b[r].Subtract(s.Multiply(b_c));
                }
            }

            if (m > n && !A[m - 1][n - 1].ApproximatelyEquals(Complex.Zero, tolerance))
            {
                return new Solution<Complex[]>(ExitCode.InconsistentEquations, "Equations are inconsistent. No solution exists.");
            }
            if (m == n && A[n - 1][n - 1].ApproximatelyEquals(Complex.Zero, tolerance))
            {
                return new Solution<Complex[]>(ExitCode.InfiniteSolutions, "Not enough equations to solve this system. Infinite solutions exist.");
            }

            // Solve using backsubstitution
            Complex[] x = new Complex[n];
            for (int i = n - 1; i >= 0; --i)
            {
                Complex[] A_i = A[i];
                Complex sum = b[i];
                for (int j = n - 1; j > i; j--)
                {
                    sum = sum.Subtract(x[j].Multiply(A_i[j]));
                }
                x[i] = sum.Divide(A_i[i]);
            }
            return new Solution<Complex[]>(ExitCode.Success, x);
        }

        [Obsolete]
        public static Solution<F[]> Solve<F>(F[,] Amatrix, F[] bVect, double tolerance = Precision.DOUBLE_PRECISION) where F : IField<F>, new()
        {
            MatrixChecks.CheckNotNull(Amatrix);
            MatrixChecks.CheckNotNull(bVect);

            int m = Amatrix.GetLength(0), n = Amatrix.GetLength(1);
            if (m != bVect.Length)
            {
                throw new InvalidOperationException("Dimensions don't match.");
            }
            if (m < n)
            {
                return new Solution<F[]>(ExitCode.InfiniteSolutions, "Not enough equations to solve this system. Infinite solutions exist.");
            }

            // Solving the linear system requires O(n^3) time, it's worthwhile to spend O(n^2) 
            // time converting T[,] into the more efficient T[][] matrix
            F[][] A = Amatrix.ToJagged();
            F[] b = bVect.Copy();
            F zero = A[0][0].AdditiveIdentity;

            // The method as it stands suffers from numerical instability
            // Gaussian elimination
            for (int c = 0; c < n; ++c)
            {
                F[] A_c = A[c];
                F b_c = b[c], A_cc_inv = A_c[c].MultiplicativeInverse();
                for (int r = c + 1; r < m; ++r)
                {
                    F[] A_r = A[r];
                    F s = A_r[c].Multiply(A_cc_inv);
                    for (int i = c; i < n; i++)
                    {
                        A_r[i] = A_r[i].Subtract(s.Multiply(A_c[i]));
                    }
                    b[r] = b[r].Subtract(s.Multiply(b_c));
                }
            }

            if (m > n && !A[m - 1][n - 1].ApproximatelyEquals(zero, tolerance))
            {
                return new Solution<F[]>(ExitCode.InconsistentEquations, "Equations are inconsistent. No solution exists.");
            }
            if (m == n && A[n - 1][n - 1].ApproximatelyEquals(zero, tolerance))
            {
                return new Solution<F[]>(ExitCode.InfiniteSolutions, "Not enough equations to solve this system. Infinite solutions exist.");
            }

            // Solve using backsubstitution
            F[] x = new F[n];
            for (int i = n - 1; i >= 0; --i)
            {
                F[] A_i = A[i];
                F sum = b[i];
                for (int j = n - 1; j > i; j--)
                {
                    sum = sum.Subtract(x[j].Multiply(A_i[j]));
                }
                x[i] = sum.Divide(A_i[i]);
            }
            return new Solution<F[]>(ExitCode.Success, x);
        }
    }
}
