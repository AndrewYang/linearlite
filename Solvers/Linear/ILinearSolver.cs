﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LinearNet.Solvers.Linear
{
    internal interface ILinearSolver<TMatrix, TVector>
    {
        /// <summary>
        /// Solves the linear system $Ax = b$
        /// </summary>
        /// <param name="matrix">The matrix $A$.</param>
        /// <param name="vector">The vector $b$.</param>
        /// <returns>The solution vector $b$.</returns>
        Solution<TVector> Solve(TMatrix matrix, TVector vector);
    }
}
