﻿using LinearNet.Structs.Fields;
using System;
using System.Collections.Generic;
using System.Text;

namespace LinearNet.Solvers.Linear
{
    /// <summary>
    /// Complex systems conjugate gradient linear solver.
    /// </summary>
    /// <typeparam name="C"></typeparam>
    /// <typeparam name="R"></typeparam>
    /// <cat>solvers</cat>
    public class ComplexConjugateGradientLinearSolver<C, R> where C : IComplex<R>
    {

    }
}
