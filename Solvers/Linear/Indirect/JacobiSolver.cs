﻿using LinearNet.Global;
using LinearNet.Providers;
using LinearNet.Structs;
using System;

namespace LinearNet.Solvers.Linear
{
    /// <summary>
    /// Implementation of Jacobi method for solving square linear systems. 
    /// <para>
    /// Jacobi's method is best suited for highly diagonally-dominant systems. It is guaranteed 
    /// to converge if the coefficient matrix is strictly or irreducibly diagonally dominant. For non-diagonally dominant
    /// systems, the solver may diverge.
    /// </para>
    /// </summary>
    /// <cat>solvers</cat>
    public class JacobiSolver
    {
        private readonly int _maxIterations;
        private double _residualSqrdNorm = 1e-20;

        public JacobiSolver(int maxIterations)
        {
            _maxIterations = maxIterations;
        }

        /// <summary>
        /// Solves the linear system $Ax = b$ given a starting point.
        /// </summary>
        /// <param name="A">The square coefficient matrix.</param>
        /// <param name="x">The solution vector (initialized to the desired starting point).</param>
        /// <param name="b">The right-hand-side vector.</param>
        /// <returns>The solution</returns>
        public Solution<DenseVector<double>> Solve(CSRSparseMatrix<double> A, DenseVector<double> x, DenseVector<double> b)
        {
            if (A is null)
            {
                throw new ArgumentNullException(nameof(A));
            }
            if (x is null)
            {
                throw new ArgumentNullException(nameof(x));
            }
            if (b is null)
            {
                throw new ArgumentNullException(nameof(b));
            }

            if (!A.IsSquare)
            {
                throw new ArgumentOutOfRangeException(nameof(A), ExceptionMessages.MatrixNotSquare);
            }
            if (A.Columns != x.Dimension)
            {
                throw new ArgumentOutOfRangeException($"The number of columns of {nameof(A)} does not match the dimension of {nameof(x)}");
            }
            if (A.Rows != b.Dimension)
            {
                throw new ArgumentOutOfRangeException($"The number of rows of {nameof(A)} does not match the dimension of {nameof(b)}");
            }

            IProvider<double> provider = new DoubleProvider();

            // Temporary vector storing the diagonal terms as a dense array
            int dim = A.Rows;
            double[] values = A.Values;
            int[] rowIndices = A.RowIndices, colIndices = A.ColumnIndices;

            double[] diag = new double[dim];
            for (int r = 0; r < dim; ++r)
            {
                int row_end = rowIndices[r + 1];
                for (int i = rowIndices[r]; i < row_end; ++i)
                {
                    if (colIndices[i] == r)
                    {
                        diag[r] = values[i];
                    }
                }
            }

            // Check to make sure there are no zeroes
            for (int r = 0; r < dim; ++r)
            {
                if (Util.ApproximatelyEquals(diag[r], 0, Precision.DOUBLE_PRECISION))
                {
                    return new Solution<DenseVector<double>>(ExitCode.NaN, "Unable to solve this system using Jacobi iteration. " +
                        "The coefficient matrix contains an element along its main diagonal that is too close to zero.");
                }
            }

            double[] _x = x.Values, _b = b.Values;
            double[] buff = new double[dim];

            for (int i = 0; i < _maxIterations; ++i)
            {
                // buff <- Ax
                A.Multiply(_x, buff, provider, false);

                // Check for convergence
                double sqrdErr = buff.SqrdDistTo(_b);
                if (sqrdErr < _residualSqrdNorm)
                {
                    return new Solution<DenseVector<double>>(ExitCode.Success, x);
                }
                else if (double.IsNaN(sqrdErr))
                {
                    return new Solution<DenseVector<double>>(ExitCode.NaN, x);
                }
                else if (double.IsInfinity(sqrdErr))
                {
                    return new Solution<DenseVector<double>>(ExitCode.MayNotHaveConverged, x);
                }

                // x = D^{-1} * (b - buff + Dx)
                for (int d = 0; d < dim; ++d)
                {
                    _x[d] = (_b[d] - buff[d] + diag[d] * _x[d]) / diag[d];
                }
            }

            // Re-check
            if (buff.SqrdDistTo(_b) < _residualSqrdNorm)
            {
                return new Solution<DenseVector<double>>(ExitCode.Success, x);
            }

            return new Solution<DenseVector<double>>(ExitCode.MayNotHaveConverged, x);
        }
    }
}
