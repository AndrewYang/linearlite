﻿using LinearNet.Global;
using LinearNet.Providers;
using LinearNet.Structs;
using LinearNet.Vectors;
using System;

namespace LinearNet.Solvers.Linear
{
    /// <summary>
    /// Implements a LSQR iterative linear systems solver.
    /// </summary>
    /// <cat>linear-algebra</cat>
    public class LSQRSolver
    {
        private readonly int _maxIterations;

        public LSQRSolver(int maxIterations)
        {
            _maxIterations = maxIterations;
        }

        public Solution<DenseVector<double>> Solve(CSRSparseMatrix<double> A, DenseVector<double> x, DenseVector<double> b, IDenseBLAS1Provider<double> blas1)
        {
            if (A is null)
            {
                throw new ArgumentNullException(nameof(A));
            }
            if (x is null)
            {
                throw new ArgumentNullException(nameof(x));
            }
            if (b is null)
            {
                throw new ArgumentNullException(nameof(b));
            }
            if (blas1 is null)
            {
                throw new ArgumentNullException(nameof(blas1));
            }
            if (!(blas1 is IDenseBLAS1<double> blas))
            {
                throw new NotSupportedException(ExceptionMessages.TypeNotSupported);
            }

            if (A.Rows != b.Dimension)
            {
                throw new ArgumentOutOfRangeException($"The number of rows of { nameof(A) } does not match the dimension of { nameof(b) }.");
            }
            if (A.Columns != x.Dimension)
            {
                throw new ArgumentOutOfRangeException($"The number of columns of { nameof(A) } does not match the dimension of { nameof(x) }");
            }

            IProvider<double> provider = new DoubleProvider();

            int m = A.Rows, n = A.Columns;

            // Initialise temporary buffers
            double beta = Math.Sqrt(blas.DOT(b.Values, b.Values, 0, m));
            double[] u = b.Values.Multiply(1.0 / beta);

            double[] v = new double[n];
            A.TransposeAndMultiply(u, v, provider, true); // increment is faster - no clearing

            double alpha = Math.Sqrt(blas.DOT(v, v, 0, n));
            blas.SCAL(v, 1.0 / alpha, 0, n);

            double[] w = v.Copy();

            double phi_hat = beta;
            double rho_hat = alpha;

            // buffer for matrix-vector products
            double[] buff = new double[Math.Max(m, n)];
            for (int i = 0; i < _maxIterations; ++i)
            {
                // Calculate Av, store in buffer
                A.Multiply(v, buff, provider, false);

                // Set buffer to Av - alpha * u
                blas.AXPY(buff, u, -alpha, 0, m);

                // Copy buff values to u
                Array.Copy(buff, 0, u, 0, m);

                // Calculate ||u||
                beta = Math.Sqrt(blas.DOT(u, u, 0, m));

                // Normalise u
                blas.SCAL(u, 1.0 / beta, 0, m);

                // Calculate A^Tu, store in buffer
                A.TransposeAndMultiply(u, buff, provider, false);

                // Set buffer to A^Tu - beta * v
                blas.AXPY(buff, v, -beta, 0, n);

                // Copy buffer values to v
                Array.Copy(buff, 0, v, 0, n);

                // Calculate ||v||
                alpha = Math.Sqrt(blas.DOT(v, v, 0, n));

                // Normalise v
                blas.SCAL(v, 1.0 / alpha, 0, n);

                // Compute rotation
                double rho = Util.Hypot(rho_hat, beta);
                double c = rho_hat / rho;
                double s = beta / rho;

                double theta = s * alpha;
                rho_hat = -c * alpha;

                double phi = c * phi_hat;
                phi_hat = s * phi_hat;

                // x <- x + (phi / rho) w
                blas.AXPY(x.Values, w, phi / rho, 0, n);

                // w <- v - (theta / rho) w
                blas.SCAL(w, theta / rho, 0, n);
                blas.SUB(v, w, w, 0, n);

                // Check for convergence 
                // DEBUG ONLY
                double norm = A.Multiply(x, provider).Subtract(b).Norm();
                //Debug.WriteLine(i + "\t" + norm);

                if (norm < 1e-10)
                {
                    return new Solution<DenseVector<double>>(ExitCode.Success, x);
                }
            }
            return new Solution<DenseVector<double>>(ExitCode.MayNotHaveConverged, x);
        }
    }
}
