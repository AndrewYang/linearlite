﻿using LinearNet.Global;
using LinearNet.Providers;
using LinearNet.Structs;
using System;

namespace LinearNet.Solvers.Linear
{
    /// <summary>
    /// Implements a Minimual Residual (MR) linear systems solver.
    /// <para>Solves the system $Ax = b$ for square and positive definite $A$.</para>
    /// </summary>
    /// <cat>solvers</cat>
    public class MinimalResidualSolver<T> : ILinearSolver<Matrix<T>, DenseVector<T>> where T : IComparable<T>, new()
    {
        private readonly IProvider<T> _provider;
        private readonly IDenseBLAS1<T> _blas;
        private readonly int _maxIterations;
        private readonly T _terminationEpsilon;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="blas"></param>
        /// <param name="maxIterations">
        /// The maximum number of iterations taken by this solver.
        /// </param>
        /// <param name="terminationThreshold">
        /// The iteration will stop when $||r||^2 / ||b||^2 < \epsilon$, where $r = b - Ax$ and $||.||$ represents the 
        /// Euclidean norm.
        /// </param>
        public MinimalResidualSolver(IDenseBLAS1Provider<T> blas, int maxIterations, T terminationThreshold)
        {
            if (blas is null)
            {
                throw new ArgumentNullException(nameof(blas));
            }
            if (!(blas is IDenseBLAS1<T> b))
            {
                throw new NotSupportedException(ExceptionMessages.TypeNotSupported);
            }
            if (maxIterations <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(maxIterations), ExceptionMessages.NonPositiveNotAllowed);
            }
            if (terminationThreshold is null)
            {
                throw new ArgumentNullException(nameof(terminationThreshold));
            }

            _blas = b;
            _provider = b.Provider;
            _maxIterations = maxIterations;
            _terminationEpsilon = terminationThreshold;

            if (_terminationEpsilon.CompareTo(_provider.Zero) <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(_terminationEpsilon), ExceptionMessages.NonPositiveNotAllowed);
            }
        }

        /// <summary>
        /// Solve the system $Ax = b$, using $x = 0$ as an initial estimate.
        /// </summary>
        /// <param name="A"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public Solution<DenseVector<T>> Solve(Matrix<T> A, DenseVector<T> b)
        {
            if (A is null)
            {
                throw new ArgumentNullException(nameof(A));
            }
            int dim = A.Columns;
            return Solve(A, DenseVector.Repeat(dim, _provider.Zero), b);
        }

        /// <summary>
        /// Solve using an initial estimate $x_0$.
        /// </summary>
        /// <param name="A"></param>
        /// <param name="x0"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public Solution<DenseVector<T>> Solve(Matrix<T> A, DenseVector<T> x0, DenseVector<T> b)
        {
            if (A is null)
            {
                throw new ArgumentNullException(nameof(A));
            }
            if (b is null)
            {
                throw new ArgumentNullException(nameof(b));
            }

            if (!A.IsSquare)
            {
                throw new ArgumentOutOfRangeException(nameof(A), ExceptionMessages.MatrixNotSquare);
            }
            if (A.Rows != b.Dimension)
            {
                throw new ArgumentOutOfRangeException($"The number of rows of '{nameof(A)}' does not match the dimension of '{nameof(b)}'");
            }

            int dim = A.Rows;
            T[] r = new T[dim], x = x0.Values, p = new T[dim];

            // Calculate r <- b - Ax
            A.Multiply(x, r, _provider, false);
            _blas.SUB(b.Values, r, r, 0, dim);

            // Calculate p <- Ar
            A.Multiply(r, p, _provider, false);

            // Calculate and store ||b||2, the Euclidean norm
            T bb = _blas.DOT(b.Values, b.Values, 0, dim);

            // Iterate until convergence
            for (int i = 0; i < _maxIterations; ++i)
            {
                // Calculate alpha <- (p . r) / (p . p)
                T pr = _blas.DOT(p, r, 0, dim);
                T pp = _blas.DOT(p, p, 0, dim);
                T alpha = _provider.Divide(pr, pp);

                // Update solution x <- x + alpha * r
                _blas.AXPY(x, r, alpha, 0, dim);

                // Update residual r
                if (i % 50 == 49)
                {
                    // Evaluate using b - Ax, which is more accurate 
                    // but more expensive - use every n-th iteration 
                    // to prevent accumulation of errors
                    A.Multiply(x, r, _provider, false);
                    _blas.SUB(b.Values, r, r, 0, dim);
                }
                else
                {
                    // Evaluate using r <- r - alpha * p, which is 
                    // fast but prone to accumulation of roundoff errors
                    _blas.AXPY(r, p, _provider.Negate(alpha), 0, dim);
                }

                // Convergence check
                T rr = _blas.DOT(r, r, 0, dim);
                //Debug.WriteLine(i + "\t" + rr);
                if (_provider.Divide(rr, bb).CompareTo(_terminationEpsilon) <= 0)
                {
                    return new Solution<DenseVector<T>>(ExitCode.Success, x0);
                }

                // Re-compute p <- Ar
                A.Multiply(r, p, _provider, false);
            }

            return new Solution<DenseVector<T>>(ExitCode.MayNotHaveConverged, x0);
        }
    }
}
