﻿using LinearNet.Global;
using LinearNet.Providers;
using LinearNet.Solvers.Preconditioners;
using LinearNet.Structs;
using System;

namespace LinearNet.Solvers.Linear
{
    /// <summary>
    /// Conjugate gradient linear solver for real numbers. 
    /// Solves the system $Ax = b$ where $A \in \mathbb{R}^{n \times n}$ is symmetric and positive semidefinite, and $x, b\in\mathbb{R}^n$.
    /// 
    /// <p>Although this class supports dense coefficient matrices, it is best suited for solving sparse systems.</p>
    /// 
    /// <p><txt>T</txt> can be any decimal type with a defined level-1 BLAS provider, including 
    /// <txt>float</txt>, <txt>double</txt>, <txt>decimal</txt>, <txt>Rational</txt>, <txt>BigDecimal</txt> and <txt>BigRational</txt>.</p>
    /// </summary>
    /// <cat>solvers</cat>
    public class RealConjugateGradientSolver<T> : ILinearSolver<CSRSparseMatrix<T>, DenseVector<T>> where T : IComparable<T>, new()
    {
        private readonly IProvider<T> _provider;
        private readonly IDenseBLAS1<T> _blas;
        private readonly T _terminationThreshold;
        private int _maxIterations;

        public RealConjugateGradientSolver(IDenseBLAS1Provider<T> blas, T terminationThreshold, int maxIterations)
        {
            if (blas is null)
            {
                throw new ArgumentNullException(nameof(blas));
            }
            if (terminationThreshold is null)
            {
                throw new ArgumentNullException(nameof(terminationThreshold));
            }
            if (!(blas is IDenseBLAS1<T> blas1))
            {
                throw new NotSupportedException(ExceptionMessages.TypeNotSupported);
            }

            _blas = blas1;
            _provider = blas1.Provider;
            _terminationThreshold = terminationThreshold;
            _maxIterations = maxIterations;
        }
        internal RealConjugateGradientSolver(IDenseBLAS1<T> blas, T terminationThreshold, int maxIterations)
        {
            if (blas is null)
            {
                throw new ArgumentNullException(nameof(blas));
            }
            if (terminationThreshold is null)
            {
                throw new ArgumentNullException(nameof(terminationThreshold));
            }

            _provider = blas.Provider;
            _blas = blas;
            _terminationThreshold = terminationThreshold;
            _maxIterations = maxIterations;
        }

        public Solution<DenseVector<T>> Solve(CSRSparseMatrix<T> A, DenseVector<T> b, DenseVector<T> x)
        {
            if (A is null)
            {
                throw new ArgumentNullException(nameof(A));
            }
            if (b is null)
            {
                throw new ArgumentNullException(nameof(b));
            }
            if (!A.IsSquare)
            {
                throw new InvalidOperationException("Coefficient matrix is not square.");
            }
            if (A.Rows != b.Dimension)
            {
                throw new InvalidOperationException("The number of rows of the coefficient matrix does not match the dimension of the vector.");
            }
            if (A.Rows != x.Dimension)
            {
                throw new InvalidOperationException("The number of rows of the coefficient matrix does not match the dimension of the solution vector.");
            }

            int dim = A.Columns;

            // If the max iteration is not set
            if (_maxIterations < 0)
            {
                _maxIterations = dim;
            }

            // The residual term, used to store Ax - b
            DenseVector<T> r = new DenseVector<T>(dim);
            A.Multiply(x, r, _provider);
            r = b.Subtract(r, _blas);
            
            // The Ap workspaces
            DenseVector<T> Ap = new DenseVector<T>(dim);

            // The p term
            DenseVector<T> p = r.Copy();

            for (int i = 0; i < _maxIterations; ++i)
            {
                T rr = _blas.DOT(r.Values, r.Values, 0, dim);

                // Ap <- A * p
                A.Multiply(p, Ap, _provider);

                // alpha <- (r^T * r) / (p^T * A * p)
                T alpha = _provider.Divide(rr, _blas.DOT(p.Values, Ap.Values, 0, dim));

                // x <- x + alpha * p
                _blas.AXPY(x.Values, p.Values, alpha, 0, dim);

                // rnext <- r - alpha * A * p - (setting r to rnext)
                _blas.AXPY(r.Values, Ap.Values, _provider.Negate(alpha), 0, dim);

                T rnextSqrd = _blas.DOT(r.Values, r.Values, 0, dim);
                if (rnextSqrd.CompareTo(_terminationThreshold) < 0)
                {
                    return new Solution<DenseVector<T>>(ExitCode.Success, x);
                }

                // beta <- (rnext^T * rnext) / (r * r)
                T beta = _provider.Divide(rnextSqrd, rr);

                // p <- beta * p
                _blas.SCAL(p.Values, beta, 0, dim);

                // p <- r + p (p = r + beta * p)
                _blas.ADD(r.Values, p.Values, p.Values, 0, dim);
            }

            return new Solution<DenseVector<T>>(ExitCode.MayNotHaveConverged, x);
        }

        public Solution<DenseVector<double>> SolveWithPreconditioner(CSRSparseMatrix<double> A, DenseVector<double> b, IDenseBLAS1Provider<double> blas1, IPreconditioner<double> preconditioner = null)
        {
            // By default, use Jacobi preconditioner M = diag(A)
            if (preconditioner is null)
            {
                preconditioner = new JacobiPreconditioner<double>(A);
            }

            if (!A.IsSquare)
            {
                throw new ArgumentOutOfRangeException(nameof(A), "Coefficient matrix is not square");
            }
            
            if (!A.IsSymmetric((a, b) => Util.ApproximatelyEquals(a, b, Precision.DOUBLE_PRECISION)))
            {
                throw new ArgumentOutOfRangeException(nameof(A), "Coefficient matrix is not symmetric");
            }

            if (A.Rows != b.Dimension)
            {
                throw new ArgumentOutOfRangeException(nameof(b), "b vector dimension does not match the number of rows in coefficient matrix.");
            }

            if (blas1 is null)
            {
                throw new ArgumentNullException(nameof(blas1));
            }
            if (!(blas1 is IDenseBLAS1<double> blas))
            {
                throw new NotSupportedException(ExceptionMessages.TypeNotSupported);
            }

            int dim = A.Rows;

            // Set initial guess to zero
            DenseVector<double> x = new DenseVector<double>(dim);

            // Set r <- b - Ax, since x = 0, we simply set r <- b.
            DenseVector<double> r = b.Copy();

            // Find z by solving Mz = r, where M is the preconditioner
            DenseVector<double> z = new DenseVector<double>(dim);
            preconditioner.PostMultiplyInverse(r, z);

            // Set p <- z
            DenseVector<double> p = z.Copy();

            // Construct workspace for A * p
            DenseVector<double> Ap = new DenseVector<double>(dim);

            for (int i = 1; i <= _maxIterations; ++i)
            {
                // Calculate and cache r.z
                double rz = blas.DOT(r.Values, z.Values, 0, dim);

                // Ap <- A * p
                A.Multiply(p, Ap, blas.Provider);

                // alpha = (r . z) / (p^T . Ap)
                double alpha = rz / blas.DOT(p.Values, Ap.Values, 0, dim);

                // Check for NaN
                if (double.IsNaN(alpha))
                {
                    return new Solution<DenseVector<double>>(ExitCode.NaN, "Solver failed because NaN value/s were encountered.");
                }

                // x <- x + alpha * p
                blas.AXPY(x.Values, p.Values, alpha, 0, dim);

                // Calculate r using one of two methods - AXPY updates are 
                // faster but accumulates errors, while explicit computation of b - Ax
                // is slower but more accurate. The compromise is to perform the 
                // r <- b - Ax computation every n-th iteration
                if (i % 50 == 0)
                {
                    // rnext <- b - Ax
                    A.Multiply(x, r, blas.Provider);
                    blas.SUB(b.Values, r.Values, r.Values, 0, dim);
                }
                else
                {
                    // rnext <- r - alpha * Ap
                    blas.AXPY(r.Values, Ap.Values, -alpha, 0, dim);
                }

                // Calculate |rnext|_2, check for convergence
                double rnextSqrd = blas.DOT(r.Values, r.Values, 0, dim);
                //Debug.WriteLine(i + "\t" + rnextSqrd);

                // Check for NaN
                if (double.IsNaN(rnextSqrd))
                {
                    return new Solution<DenseVector<double>>(ExitCode.NaN, "Solver failed because NaN value/s were encountered.");
                }

                if (rnextSqrd < 1e-20)
                {
                    return new Solution<DenseVector<double>>(ExitCode.Success, x);
                }

                // znext <- M^-1r
                preconditioner.PostMultiplyInverse(r, z);

                // beta <- (rnext . znext) / (r * z)
                double beta = blas.DOT(r.Values, z.Values, 0, dim) / rz;

                // Check for NaN
                if (double.IsNaN(beta))
                {
                    return new Solution<DenseVector<double>>(ExitCode.NaN, "Solver failed because NaN value/s were encountered.");
                }

                // p <- beta * p
                blas.SCAL(p.Values, beta, 0, dim);

                // p <- z + p (p = z + beta * p)
                blas.ADD(z.Values, p.Values, p.Values, 0, dim);
            }

            return new Solution<DenseVector<double>>(ExitCode.MayNotHaveConverged, x);
        }

        public Solution<DenseVector<T>> Solve(CSRSparseMatrix<T> A, DenseVector<T> b)
        {
            return Solve(A, b, new DenseVector<T>(A.Columns));
        }
    }
}
