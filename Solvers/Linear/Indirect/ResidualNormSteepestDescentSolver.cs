﻿using LinearNet.Global;
using LinearNet.Providers;
using LinearNet.Structs;
using System;

namespace LinearNet.Solvers.Linear
{
    /// <summary>
    /// Can be used to solve any square, invertible system.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ResidualNormSteepestDescentSolver<T> : ILinearSolver<CSRSparseMatrix<T>, DenseVector<T>> where T : IComparable<T>, new()
    {
        private readonly IProvider<T> _provider;
        private readonly IDenseBLAS1<T> _blas;
        private readonly int _maxIterations;
        private readonly T _terminationEpsilon;

        public ResidualNormSteepestDescentSolver(IDenseBLAS1Provider<T> blas, int maxIterations, T terminationEpsilon)
        {
            if (blas is null)
            {
                throw new ArgumentNullException(nameof(blas));
            }
            if (!(blas is IDenseBLAS1<T> b))
            {
                throw new NotSupportedException(ExceptionMessages.TypeNotSupported);
            }

            _blas = b;
            _provider = b.Provider;

            if (maxIterations <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(maxIterations), ExceptionMessages.NonPositiveNotAllowed);
            }
            _maxIterations = maxIterations;

            if (terminationEpsilon.CompareTo(_provider.Zero) <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(terminationEpsilon), ExceptionMessages.NonPositiveNotAllowed);
            }
            _terminationEpsilon = terminationEpsilon;
        }

        public Solution<DenseVector<T>> Solve(CSRSparseMatrix<T> A, DenseVector<T> b)
        {
            if (A is null)
            {
                throw new ArgumentNullException(nameof(A));
            }

            int dim = A.Columns;
            return Solve(A, DenseVector.Repeat(dim, _provider.Zero), b);
        }

        public Solution<DenseVector<T>> Solve(CSRSparseMatrix<T> A, DenseVector<T> x, DenseVector<T> b)
        {
            if (A is null)
            {
                throw new ArgumentNullException(nameof(A));
            }
            if (x is null)
            {
                throw new ArgumentNullException(nameof(x));
            }
            if (b is null)
            {
                throw new ArgumentNullException(nameof(b));
            }
            if (!A.IsSquare)
            {
                throw new ArgumentOutOfRangeException(nameof(A), ExceptionMessages.MatrixNotSquare);
            }
            if (A.Rows != b.Dimension)
            {
                throw new ArgumentOutOfRangeException($"The number of rows of '{nameof(A)}' does not match the dimension of '{nameof(b)}'");
            }
            if (A.Columns != x.Dimension)
            {
                throw new ArgumentOutOfRangeException($"The number of columns of '{nameof(A)}' does not match the dimension of '{nameof(x)}'");
            }

            int dim = A.Rows;

            // Calculate r <- b - Ax
            T[] r = A.Multiply(x.Values, _provider), v = new T[dim], Av = new T[dim];
            _blas.SUB(b.Values, r, r, 0, dim);

            // Calculate ||b||_2 = (b . b)
            T bb = _blas.DOT(b.Values, b.Values, 0, dim);

            for (int i = 0; i < _maxIterations; ++i)
            {
                // v <- A^T * r
                A.TransposeAndMultiply(r, v, _provider, false);

                // Av <- A * v
                A.Multiply(v, Av, _provider, false);

                // alpha <- (v . v) / (Av . Av)
                T alpha = _provider.Divide(_blas.DOT(v, v, 0, dim), _blas.DOT(Av, Av, 0, dim));

                // x <- x + alpha * v
                _blas.AXPY(x.Values, v, alpha, 0, dim);

                if (i % 50 == 49)
                {
                    // r <- b - Ax
                    A.Multiply(x.Values, r, _provider, false);
                    _blas.SUB(b.Values, r, r, 0, dim);
                }
                else
                {
                    // r <- r - alpha * Av
                    _blas.AXPY(r, Av, _provider.Negate(alpha), 0, dim);
                }

                // Check convergence
                T rr = _blas.DOT(r, r, 0, dim);
                if (_provider.Divide(rr, bb).CompareTo(_terminationEpsilon) <= 0)
                {
                    return new Solution<DenseVector<T>>(ExitCode.Success, x);
                }
            }

            return new Solution<DenseVector<T>>(ExitCode.MayNotHaveConverged, x);
        }
    }
}
