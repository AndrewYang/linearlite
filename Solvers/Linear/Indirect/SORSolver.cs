﻿using LinearNet.Global;
using LinearNet.Structs;
using LinearNet.Vectors;
using System;

namespace LinearNet.Solvers.Linear
{
    /// <summary>
    /// Implements the successive over-relaxation (SOR) solver.
    /// </summary>
    /// <cat>solvers</cat>
    public class SORSolver
    {
        private readonly double _w, _one_minus_w;
        private readonly int _maxIterations;

        public SORSolver(double w, int maxIterations)
        {
            if (double.IsNaN(w) || double.IsInfinity(w) || w <= 0.0 || w >= 2.0)
            {
                throw new ArgumentOutOfRangeException(nameof(w));
            }
            if (maxIterations <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(maxIterations), ExceptionMessages.NonPositiveNotAllowed);
            }

            _w = w;
            _one_minus_w = 1.0 - w;
            _maxIterations = maxIterations;
        }

        public Solution<DenseVector<double>> Solve(CSRSparseMatrix<double> A, DenseVector<double> x, DenseVector<double> b)
        {
            if (!A.IsSquare)
            {
                throw new ArgumentOutOfRangeException();
            }

            double[] values = A.Values,
                _x = x.Values,
                _b = b.Values;

            int[] rowIndices = A.RowIndices, 
                colIndices = A.ColumnIndices;

            double b_norm = b.Norm(2);

            int dim = A.Rows;
            for (int t = 0; t < _maxIterations; ++t)
            {
                // Update the estimate x
                for (int i = 0; i < dim; ++i)
                {
                    // Calculate (Ax)[i] and store the diagonal entry
                    double sum = 0.0, diagonal = 0.0;
                    int row_end = rowIndices[i + 1];
                    for (int j = rowIndices[i]; j < row_end; ++j)
                    {
                        int c = colIndices[j];
                        if (i == c)
                        {
                            diagonal = values[j];
                        }
                        else
                        {
                            sum += values[j] * _x[colIndices[j]];
                        }
                    }

                    if (diagonal == 0)
                    {
                        return new Solution<DenseVector<double>>(ExitCode.NaN, x);
                    }

                    _x[i] = _one_minus_w * _x[i] + _w / diagonal * (_b[i] - sum);
                }

                // Too expensive to check every iteration
                if (t % 50 == 0)
                {
                    // Calculate the square of 2-norm of (Ax - b), without explicitly storing the product Ax
                    double normSqrd = 0.0;
                    for (int r = 0; r < dim; ++r)
                    {
                        double Ax_r = 0.0;
                        int row_end = rowIndices[r + 1];
                        for (int i = rowIndices[r]; i < row_end; ++i)
                        {
                            Ax_r += _x[colIndices[i]] * values[i];
                        }
                        double d = Ax_r - _b[r];
                        normSqrd += d * d;
                    }

                    // Return if ||Ax - b|| / ||b|| < threshold
                    if (Math.Sqrt(normSqrd) < 1e-10 * b_norm)
                    {
                        return new Solution<DenseVector<double>>(ExitCode.Success, x);
                    }
                    //Debug.WriteLine(t + "\t" + A.Multiply(x, DefaultProviders.Double).Subtract(b).Norm());
                }
            }

            return new Solution<DenseVector<double>>(ExitCode.MayNotHaveConverged, x);
        }
    }
}
