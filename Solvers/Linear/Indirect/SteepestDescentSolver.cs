﻿using LinearNet.Providers;
using LinearNet.Providers.LinearAlgebra;
using LinearNet.Structs;
using System;

namespace LinearNet.Solvers.Linear
{
    /// <summary>
    /// Solver for a linear system $Ax = b$ using gradient descent with line search.
    /// </summary>
    /// <cat>solvers</cat>
    public class SteepestDescentSolver : ILinearSolver<CSRSparseMatrix<double>, DenseVector<double>>
    {
        private readonly int _maxIterations;

        public SteepestDescentSolver(int maxIterations)
        {
            _maxIterations = maxIterations;
        }

        public Solution<DenseVector<double>> Solve(CSRSparseMatrix<double> A, DenseVector<double> b)
        {
            if (A is null)
            {
                throw new ArgumentNullException(nameof(A));
            }

            if (b is null)
            {
                throw new ArgumentNullException(nameof(b));
            }

            if (!A.IsSquare)
            {
                throw new ArgumentOutOfRangeException(nameof(A), "Coefficient matrix is not square.");
            }

            if (!A.IsSymmetric((a, b) => Util.ApproximatelyEquals(a, b, Precision.DOUBLE_PRECISION)))
            {
                throw new ArgumentOutOfRangeException(nameof(A), "Coefficient matrix is not symmetric.");
            }

            if (A.Rows != b.Dimension)
            {
                throw new InvalidOperationException("The number of rows in the coefficient matrix does not match the result vector.");
            }

            int dim = A.Rows;
            IDenseBLAS1<double> blas = new NativeDoubleProvider();

            // Initialize the solution vector to 0
            DenseVector<double> x = new DenseVector<double>(dim);

            // Initialize the residual vectors r <- b - Ax = b
            DenseVector<double> r = b.Copy();

            // Workspace to store Ar 
            DenseVector<double> Ar = new DenseVector<double>(dim);

            for (int i = 1; i <= _maxIterations; ++i)
            {
                // Set Ar <- A * r
                A.Multiply(r, Ar, blas.Provider);

                double rr = blas.DOT(r.Values, r.Values, 0, dim);
                if (rr < 1e-10)
                {
                    return new Solution<DenseVector<double>>(ExitCode.Success, x);
                }

                // Set alpha <- (r . r) / (r . Ar)
                double alpha = rr / blas.DOT(r.Values, Ar.Values, 0, dim);

                // x <- x + alpha * r
                blas.AXPY(x.Values, r.Values, alpha, 0, dim);

                // Calculate rnext <- b - Ax
                if (i % 50 == 0)
                {
                    // Using the naive method which is accurate
                    // r <- Ax
                    A.Multiply(x, r, blas.Provider);

                    // r <- b - r (= b - Ax)
                    blas.SUB(b.Values, r.Values, r.Values, 0, dim);
                }
                else
                {
                    // Using the incremental method
                    // r <- r - alpha * Ar
                    blas.AXPY(r.Values, Ar.Values, -alpha, 0, dim);
                }
            }

            return new Solution<DenseVector<double>>(ExitCode.MayNotHaveConverged, x);
        }
    }
}
