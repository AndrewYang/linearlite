﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LinearNet.Global;
using LinearNet.Optimisation;

namespace LinearNet.Solvers
{
    /// <summary>
    /// Implementation of Newton's algorithm for finding the roots of $f(x)$, for a single function $f$ (univariate or multivariate).
    /// Compare this with <txt>NewtonSolver</txt> which uses Newton's algorithm for solving systems of 
    /// equations. 
    /// </summary>
    /// <cat>solvers</cat>
    public class NewtonRootFindingAlgorithm : IRootFindingAlgorithm<double, double>
    {
        /// <summary>
        /// The maximum number of iterative steps that can be taken
        /// </summary>
        private readonly int _maxIterations;

        /// <summary>
        /// Solver will terminate if |x(t) - x(t - 1)| < eps, where x(i) is the root estimate at the i-th iteration
        /// </summary>
        private readonly double _eps;

        /// <summary>
        /// The update function is x(t + 1) := x(t) - a * f(x(t)) / f'(x(t)), where a is the damping factor
        /// </summary>
        private readonly double _dampingFactor;

        /// <summary>
        /// The maximum step size per iteration, used to control
        /// </summary>
        private readonly double _maxStepSize;

        /// <summary>
        /// The initial guess of the root of f(x)
        /// </summary>
        private readonly double _initialEstimate;

        public NewtonRootFindingAlgorithm(double initialEstimate = 0.0, int maxIterations = 100, double epsilon = 1e-60, double dampingFactor = 1.0, double maxStepSize = double.MaxValue)
        {
            if (double.IsNaN(initialEstimate)) throw new ArgumentOutOfRangeException(nameof(initialEstimate), ExceptionMessages.NaNNotAllowed);
            if (double.IsNaN(epsilon)) throw new ArgumentOutOfRangeException(nameof(epsilon), ExceptionMessages.NaNNotAllowed);
            if (double.IsNaN(dampingFactor)) throw new ArgumentOutOfRangeException(nameof(dampingFactor), ExceptionMessages.NaNNotAllowed);
            if (double.IsNaN(maxStepSize)) throw new ArgumentOutOfRangeException(nameof(maxStepSize), ExceptionMessages.NaNNotAllowed);

            if (double.IsInfinity(initialEstimate)) throw new ArgumentOutOfRangeException(nameof(initialEstimate), ExceptionMessages.InfinityNotAllowed);
            if (double.IsInfinity(epsilon)) throw new ArgumentOutOfRangeException(nameof(epsilon), ExceptionMessages.InfinityNotAllowed);
            if (double.IsInfinity(dampingFactor)) throw new ArgumentOutOfRangeException(nameof(dampingFactor), ExceptionMessages.InfinityNotAllowed);
            if (double.IsInfinity(maxStepSize)) throw new ArgumentOutOfRangeException(nameof(maxStepSize), ExceptionMessages.InfinityNotAllowed);

            if (dampingFactor <= 0) throw new ArgumentOutOfRangeException(nameof(dampingFactor), ExceptionMessages.NonPositiveNotAllowed);
            if (maxStepSize <= 0) throw new ArgumentOutOfRangeException(nameof(maxStepSize), ExceptionMessages.NonPositiveNotAllowed);
            if (epsilon <= 0) throw new ArgumentOutOfRangeException(nameof(epsilon), ExceptionMessages.NonPositiveNotAllowed);
            if (maxIterations <= 0) throw new ArgumentOutOfRangeException(nameof(epsilon), ExceptionMessages.NonPositiveNotAllowed);

            _initialEstimate = initialEstimate;
            _maxIterations = maxIterations;
            _eps = epsilon;
            _dampingFactor = dampingFactor;
            _maxStepSize = maxStepSize;
        }

        public Solution<double> Solve(Function<double, double> function)
        {
            if (function == null)
            {
                throw new ArgumentNullException(nameof(function));
            }

            if (!(function is DifferentiableFunction<double, double>))
            {
                throw new InvalidOperationException("Newton's method can only be used for differentiable functions.");
            }

            DifferentiableFunction<double, double> f = (DifferentiableFunction<double, double>)function;

            double x = _initialEstimate, fx = f.Evaluate(x);
            for (int i = 0; i < _maxIterations; ++i)
            {
                double derivative = 0.0;
                f.EvaluateDerivative(x, ref derivative);

                if (derivative == 0.0)
                {
                    return new Solution<double>(ExitCode.InvalidDerivative, x);
                }

                // Try to use numerically stable division ... the cost of instability is divergence 
                // which easily happens with Newton's method even for well-behaved functions.
                double step = _dampingFactor * Math.Exp(Math.Sign(derivative) * Math.Sign(fx) * (Math.Log(Math.Abs(derivative)) - Math.Log(Math.Abs(fx))));

                // Clamp to the maximum step size - trying to control for divergence
                if (Math.Abs(step) > _maxStepSize)
                {
                    step = Math.Sign(step) * _maxStepSize;
                }

                // Unable to perform next step
                if (double.IsNaN(step))
                {
                    return new Solution<double>(ExitCode.MayNotHaveConverged, x);
                }

                x -= step;

                if (Math.Abs(step) < _eps)
                {
                    return new Solution<double>(ExitCode.Success, x);
                }

                fx = f.Evaluate(x);
            }
            return new Solution<double>(ExitCode.MayNotHaveConverged, x);
        }
    }
}
