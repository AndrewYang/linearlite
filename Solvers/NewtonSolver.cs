﻿using LinearNet.Optimisation;
using LinearNet.Providers.LinearAlgebra;
using LinearNet.Solvers.Linear;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Solvers
{
    /// <summary>
    /// Implementation of Newton's solver for non-linear systems of equations.
    /// <p>Newton's method can be used to solve systems of the form $f_i(x) = 0, 1 < {i} < n, x \in \mathbb{F}^n$</p>
    /// </summary>
    /// <cat>solvers</cat>
    public class NewtonSolver
    {
        #region Defaults 
        private int _maxIterations = 100;
        private double _changeEps = 1e-16;
        #endregion 

        public int MaxIterations { get { return _maxIterations; } set { _maxIterations = value; } }
        public double ChangeEpsilon { get { return _changeEps; } set { _changeEps = value; } }

        private readonly int _dimension;
        private readonly DifferentiableFunction<double[], double>[] _system;
        private double[][] _jacobian;
        private double[] _functionValues;
        private readonly DenseSolver<double> _linearSolver;

        public NewtonSolver(params DifferentiableFunction<double[], double>[] system)
        {
            if (system == null || system.Length == 0)
            {
                throw new ArgumentException("System contains no equations.");
            }

            _dimension = system[0].Dimension;
            foreach (DifferentiableFunction<double[], double> equation in system)
            {
                if (equation.Dimension != _dimension)
                {
                    throw new ArgumentException("Incompatible dimensionality - please ensure that each function in the system is of the same dimension.");
                }
            }

            if (system.Length != _dimension)
            {
                throw new ArgumentException("The number of equations do not match the number of variables.");
            }

            _system = system;
            _linearSolver = new DenseSolver<double>(BLAS.Double);
        }

        /// <summary>
        /// Using Newton's method to solve a series of equations 
        /// f1(x) = 0, f2(x) = 0, ... fn(x) = 0
        /// for a vector x of dimension n.
        /// </summary>
        /// <param name="seed">The starting point of the iterative Newton's algorithm.</param>
        /// <returns>The solution</returns>
        public Solution<double[]> Solve(double[] seed = null)
        {
            double[] x;
            if (seed == null)
            {
                x = RectangularVector.Random<double>(_dimension);
            }
            else
            {
                x = seed.Copy();
            }

            _jacobian = new double[_dimension][];
            _functionValues = new double[_dimension];

            for (int i = 0; i < _maxIterations; ++i)
            {
                CalculateJacobian(x);
                CalculateFunctionValues(x);

                _jacobian[i] = new double[_dimension];
                Solution<double[]> soln = _linearSolver.Solve(_jacobian, _functionValues);
                if (!soln.Success)
                {
                    // Early termination - may not have converged
                    return new Solution<double[]>(ExitCode.MayNotHaveConverged, x);
                }

                for (int j = 0; j < _dimension; ++j)
                {
                    x[j] -= soln.Result[j];
                }

                if (soln.Result.Norm(2) < _changeEps)
                {
                    return new Solution<double[]>(ExitCode.Success, x);
                }

                x.Print();
            }
            return new Solution<double[]>(ExitCode.Success, x);
        }

        /// <summary>
        /// Calculate the Jacobian matrix
        /// </summary>
        /// <param name="point"></param>
        private void CalculateJacobian(double[] point)
        {
            for (int i = 0; i < _dimension; ++i)
            {
                _system[i].EvaluateDerivative(point, ref _jacobian[i]);
            }
        }
        private void CalculateFunctionValues(double[] point)
        {
            for (int i = 0; i < _dimension; ++i)
            {
                _functionValues[i] = _system[i].Evaluate(point);
            }
        }
    }
}
