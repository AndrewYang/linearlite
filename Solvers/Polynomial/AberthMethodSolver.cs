﻿using LinearNet.LinearAlgebra;
using LinearNet.Structs;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace LinearNet.Solvers
{
    /// <summary>
    /// <p>Implements the <a href="https://en.wikipedia.org/wiki/Aberth_method">Aberth method</a> for simultaneously finding all roots of a univariate polynomial.</p>
    /// <p>This iterative algorithm converges cubically (except around roots with multiplicity greater than 1).</p>
    /// <p>Implementation supports polynomials with both real and complex coefficients.</p>
    /// </summary>
    /// <cat>solvers</cat>
    internal class AberthMethodSolver : IPolynomialRootFinder
    {
        /// <summary>
        /// The maximum number of iterations that the solver is allowed to take, before exiting with the best 
        /// solution so far.
        /// </summary>
        public int MaxIterations { get; set; }
        /// <summary>
        /// If not set to NaN, iterative step will terminate when the norm of |w(t + 1) - w(t)|2 < eps.
        /// </summary>
        public double ChangeEpsilon { get; set; }
        /// <summary>
        /// If not set to NaN, iterative step will terminate when the log of product of all polynomials evaluated at 
        /// the root estimates so far is less than eps is magnitude.
        /// </summary>
        public double LogResidualProductEpsilon { get; set; }

        /// <summary>
        /// Initialize a new Aberth solver by specifying the termination conditions.
        /// </summary>
        /// <param name="maxIterations">The maximum number of iterations that the solver can take.</param>
        /// <param name="changeEpsilon">
        /// If not set to <txt>NaN</txt>, the solver will terminate if the norm of the vector of root estimates changes 
        /// by less than <txt>changeEpsilon</txt> in one interation. 
        /// </param>
        /// <param name="logResidualEpsilon">
        /// If not set to <txt>NaN</txt>, the solver will terminate if the log-product of the polynomial evaluated at each 
        /// of its root estimates is less than <txt>logResidualEpsilon</txt>.
        /// </param>
        public AberthMethodSolver(int maxIterations = 1000, double changeEpsilon = Precision.DOUBLE_PRECISION, double logResidualEpsilon = double.NaN)
        {
            MaxIterations = maxIterations;
            ChangeEpsilon = changeEpsilon;
            LogResidualProductEpsilon = logResidualEpsilon;
        }

        /// <summary>
        /// Finds the roots of a polynomial with complex coefficients
        /// using the Aberth method and the Cauchy bound.
        /// </summary>
        /// <param name="polynomial">A degree $n$ polynomial with complex coefficients.</param>
        /// <returns>
        /// A length-$n$ array containing each of the $n$ roots of the polynomial. Roots with multiplicity greater than 1 will 
        /// appear multiple times in the array.
        /// </returns>
        public override Complex[] Solve(DensePolynomial<Complex> polynomial)
        {
            if (polynomial == null)
            {
                throw new ArgumentNullException(nameof(polynomial));
            }
            if (polynomial.Degree < 0)
            {
                throw new InvalidOperationException(nameof(polynomial));
            }

            // Calculate the bounds of roots, and randomly initialise within [-upperBound, upperBound]
            // we impose a cap on the upper bound of roots, since we might run into numerical problems 

            double upperBound = Math.Min(Math.Pow(1e9, 1.0 / polynomial.Degree), CalculateCauchyBound(polynomial.Coefficients));

            Complex[] roots = RandomlyInitialise(polynomial.Degree, upperBound);
            Complex[] last = roots.Copy();

            for (int i = 0; i < MaxIterations; ++i)
            {
                Iterate(polynomial, roots);

                if (!double.IsNaN(ChangeEpsilon))
                {
                    if (roots.Subtract(last).Norm() < ChangeEpsilon)
                    {
                        break;
                    }
                }
                if (!double.IsNaN(LogResidualProductEpsilon))
                {
                    if (CalculateLogResidualProduct(polynomial, roots) < LogResidualProductEpsilon)
                    {
                        break;
                    }
                }

                last = roots.Copy();
            }
            return roots;
        }
        /// <summary>
        /// Find the roots of a polynomial with real coefficients using the Aberth method and the Cauchy bound.
        /// </summary>
        /// <param name="polynomial">A degree $n$ polynomial with real coefficients.</param>
        /// <returns>
        /// A length-$n$ array containing each of the $n$ roots of the polynomial. A root with multiplicity greater than 1 will appear more 
        /// than once in the array.
        /// </returns>
        public override Complex[] Solve(DensePolynomial<double> polynomial)
        {
            DensePolynomial<Complex> p = polynomial.ToComplex();
            return Solve(p);
        }

        /// <summary>
        /// Returns the upper bounds for the roots of a polynomial with coefficients [a_n, a_(n-1), a_(n-2), ..., a_0]
        /// Calculated as upper = 1 + max(|a_(n-1)/a_n|, |a_(n-2)/a_(n-1)|, ... , |a_0/a_n|)
        /// Calculated as lower = -upper
        /// </summary>
        /// <param name="coefficients"></param>
        /// <param name="lowerBound"></param>
        /// <param name="upperBound"></param>
        private double CalculateCauchyBound(Complex[] coefficients)
        {
            double max = 0;
            for (int i = 1; i < coefficients.Length; ++i)
            {
                double ratio = (coefficients[i] / coefficients[0]).Modulus();
                if (max < ratio)
                {
                    max = ratio;
                }
            }
            return max;
        }

        /// <summary>
        /// Randomly initialise a n-dimensional array of roots which represent our initial guesses, contained 
        /// inside a complex disk of radius 'upperBound'
        /// </summary>
        /// <param name="upperBound">The radius of the disk in which a complex root is to be sampled.</param>
        /// <returns>n-dimensional array of initial approximations</returns>
        private Complex[] RandomlyInitialise(int n, double upperBound)
        {
            Complex[] roots = new Complex[n];
            Random r = new Random();
            for (int i = 0; i < n; ++i)
            {
                roots[i] = Complex.FromPolar(r.NextDouble() * upperBound, r.NextDouble() * Math.PI * 2);
            }
            return roots;
        }

        private void Iterate(DensePolynomial<Complex> p, Complex[] roots)
        {
            // Requires high precision
            double eps = 1e-30;

            // Calculate w(k)
            int deg = roots.Length, k, j;
            for (k = 0; k < deg; ++k)
            {
                Complex r = roots[k], pZ = p.Evaluate(r);

                // w(k) is undefined for pZ close to 0
                if (Util.approx_zero(pZ, eps)) continue;

                Complex z_sum = Complex.Zero;
                for (j = 0; j < deg; ++j)
                {
                    if (j != k)
                    {
                        Complex diff = r.Subtract(roots[j]);

                        // w(k) is undefined for root(k) - root(j) close to 0
                        if (Util.approx_zero(diff, eps))
                        {
                            z_sum = Complex.Zero;
                            break;
                        }
                        z_sum.IncrementBy(diff.MultiplicativeInverse());
                    }
                }

                //Complex den = p.EvaluateDerivative(roots[k]) / pZ - z_sum;
                Complex den = p.Derivative(r).Divide(pZ).Subtract(z_sum);

                // w(k) is undefined for denominator close to 0
                if (!Util.approx_zero(pZ, eps))
                {
                    roots[k].DecrementBy(den.MultiplicativeInverse());
                }
            }
        }

        /// <summary>
        /// Returns the log product of the magnitude of the polynomial evaluated at each of the roots.
        /// If this value is close to 0, then we are close to a good solution
        /// </summary>
        /// <param name="p"></param>
        /// <param name="roots"></param>
        private double CalculateLogResidualProduct(DensePolynomial<Complex> p, Complex[] roots)
        {
            double logProduct = 0;
            foreach (Complex r in roots)
            {
                double px = p.Evaluate(r).Modulus();
                if (px <= 0)
                {
                    return double.NegativeInfinity;
                }
                logProduct += Math.Log(px);
            }
            return logProduct;
        }
    }
    public enum PolynomialRootAlgorithm
    {
        ABERTH
    }
}
