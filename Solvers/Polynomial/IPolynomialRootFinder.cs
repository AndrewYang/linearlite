﻿using LinearNet.Optimisation;
using LinearNet.Structs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Solvers
{
    internal abstract class IPolynomialRootFinder : IRootFindingAlgorithm<Complex[], Complex>
    {
        /// <summary>
        /// Calculates all real or complex roots for a polynomial with real coefficients.
        /// </summary>
        /// <param name="polynomial"></param>
        /// <returns></returns>
        public abstract Complex[] Solve(DensePolynomial<double> polynomial);

        /// <summary>
        /// Calculates all real or complex roots for a polynomial with complex coefficients.
        /// </summary>
        /// <param name="polynomial"></param>
        /// <returns></returns>
        public abstract Complex[] Solve(DensePolynomial<Complex> polynomial);


        public Solution<Complex[]> Solve(Function<Complex[], Complex> function)
        {
            throw new NotImplementedException();
        }
    }
}
