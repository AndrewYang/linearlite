﻿using LinearNet.Structs;

namespace LinearNet.Solvers.Preconditioners
{
    public abstract class IPreconditioner<T> where T : new()
    {
        /// <summary>
        /// Postmultiply the inverse of this preconditioning matrix by a vector, storing the result in another vector.
        /// </summary>
        /// <param name="vector"></param>
        /// <param name="product"></param>
        internal abstract void PostMultiplyInverse(DenseVector<T> vector, DenseVector<T> product);
    }
}
