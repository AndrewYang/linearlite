﻿using LinearNet.Providers;
using LinearNet.Structs;
using System;

namespace LinearNet.Solvers.Preconditioners
{
    /// <summary>
    /// Implements a Jacobi matrix preconditioner for symmetric, positive definite matrices.
    /// </summary>
    /// <cat>solvers</cat>
    public sealed class JacobiPreconditioner<T> : IPreconditioner<T> where T : new()
    {
        private readonly IProvider<T> _provider;
        private readonly T[] _diagonalFactors;

        public JacobiPreconditioner(Matrix<T> matrix)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            if (!matrix.IsSquare)
            {
                throw new ArgumentException("Matrix is not square.");
            }

            _provider = ProviderFactory.GetDefaultProvider<T>();

            int dim = matrix.Rows;
            _diagonalFactors = new T[dim];

            T one = _provider.One;
            for (int i = 0; i < dim; ++i)
            {
                _diagonalFactors[i] = _provider.Divide(one, matrix[i, i]);
            }
        }

        internal override void PostMultiplyInverse(DenseVector<T> vector, DenseVector<T> product)
        {
            // Pointwise multiplication
            T[] vector_values = vector.Values;
            T[] product_values = product.Values;

            int dim = vector.Dimension;
            for (int d = 0; d < dim; ++d)
            {
                product_values[d] = _provider.Multiply(vector_values[d], _diagonalFactors[d]);
            }
        }
    }
}
