﻿using LinearNet.Providers;
using LinearNet.Providers.LinearAlgebra;
using LinearNet.Structs;
using System;

namespace LinearNet.Solvers.Preconditioners
{
    /// <summary>
    /// <p>
    /// Implements the symmetric successive over-relaxation preconditioner, 
    /// $$M(w) = \frac{w}{2 - w} \bigg(\frac{1}{w}D + L\bigg) D^{-1} \bigg(\frac{1}{w} D + L\bigg)^T$$
    /// for $0 < w < 2$
    /// 
    /// </p>
    /// <p>
    /// $D, L$ are defined by the splitting $A = L + D + L^T$, with $D$ diagonal and 
    /// $L$ (strictly) lower triangular.
    /// </p>
    /// 
    /// <p>
    /// This implementation computes the product $M(w)^{-1}x$ by solving the sequence of equations
    /// $$x = \bigg(\frac{1}{w}D + L\bigg)x',$$
    /// $$Dx' = \bigg(\frac{1}{w}D + L\bigg)^Tx''$$
    /// $$M(w)^{-1}x = \frac{2 - w}{w} x''$$
    /// </p>
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <cat>solvers</cat>
    public sealed class SSORPreconditioner : IPreconditioner<double>
    {
        private readonly IDenseBLAS1<double> _blas;
        private readonly double[] _diagonal;
        private readonly CSRSparseMatrix<double> _tempMatrix, _tempMatrixT;
        private readonly double[] _workspace;
        private readonly double _w;

        public SSORPreconditioner(CSRSparseMatrix<double> matrix, double w = 1.0) 
        { 
            if (w <= 0.0 || w >= 2.0)
            {
                throw new ArgumentOutOfRangeException(nameof(w), "w must be between 0 and 2");
            }

            _w = w;
            _blas = new NativeDoubleProvider();
            _diagonal = new double[matrix.Rows];
            _workspace = new double[matrix.Rows];


            // Storing the same matrix twice - making a trade for speed and sacrificing memory
            _tempMatrix = CalculateTempMatrix(matrix, w, _blas.Provider, _diagonal);
            _tempMatrixT = _tempMatrix.Transpose();
        }

        /// <summary>
        /// Calculates D / w + L
        /// </summary>
        /// <param name="A"></param>
        /// <returns></returns>
        private static CSRSparseMatrix<T> CalculateTempMatrix<T>(CSRSparseMatrix<T> A, T w, IProvider<T> provider, T[] diagonal) where T : new()
        {
            // Calculate D / w + L as a CSR sparse matrix

            T[] values = A.Values;
            int[] rowIndices = A.RowIndices, 
                columnIndices = A.ColumnIndices;

            // Count the number of terms in 
            int rows = A.Rows, count = 0;
            for (int r = 0; r < rows; ++r)
            {
                int row_end = rowIndices[r + 1];
                for (int i = rowIndices[r]; i < row_end; ++i)
                {
                    if (columnIndices[i] <= r)
                    {
                        count++;
                    }
                }
            }

            T[] temp_values = new T[count];
            int[] temp_colIndices = new int[count];
            int[] temp_rowIndices = new int[rows + 1];

            for (int r = 0, k = 0; r < rows; ++r)
            {
                int row_end = rowIndices[r + 1];
                for (int i = rowIndices[r]; i < row_end; ++i)
                {
                    int c = columnIndices[i];

                    // Lower diagonal terms get copied
                    if (c < r)
                    {
                        temp_values[k] = values[i];
                        temp_colIndices[k] = c;
                        ++k;
                    }

                    // Diagonal terms get divided by w.
                    else if (c == r)
                    {
                        diagonal[r] = values[i];
                        temp_values[k] = provider.Divide(values[i], w);
                        temp_colIndices[k] = c;
                        ++k;
                    }
                }
                temp_rowIndices[r + 1] = k;
            }

            return new CSRSparseMatrix<T>(rows, temp_rowIndices, temp_colIndices, temp_values);
        }


        internal override void PostMultiplyInverse(DenseVector<double> vector, DenseVector<double> product)
        {
            // Evaluate (D / w + L)^(-1)x by solving (D / w + L)x' = x
            Solve(_tempMatrix, _workspace, vector.Values);

            // Premultiply by D
            int dim = _workspace.Length;
            for (int d = 0; d < dim; ++d)
            {
                _workspace[d] *= _diagonal[d];
            }

            // Evaluate (D / w + L)^(-T) * x' by solving (D / w + L)^T * x'' = x'
            Solve(_tempMatrixT, product.Values, _workspace);

            // Finally - multiply by (2 - w) / w
            _blas.SCAL(product.Values, (2 - _w) / _w, 0, dim);
        }


        /// <summary>
        /// Solves Lx = b for lower-triangular L.
        /// </summary>
        /// <param name="L"></param>
        /// <param name="x"></param>
        /// <param name="b"></param>
        private static void Solve(CSRSparseMatrix<double> L, double[] x, double[] b)
        {
            double[] values = L.Values;
            int[] rowIndices = L.RowIndices;
            int[] colIndices = L.ColumnIndices;

            int rows = L.Rows;
            for (int r = 0; r < rows; ++r)
            {
                int row_end = rowIndices[r + 1];

                double sum = b[r];
                for (int i = rowIndices[r]; i < row_end; ++i)
                {
                    int c = colIndices[i];

                    if (c == r)
                    {
                        sum /= values[c];
                        break;
                    }
                    else
                    {
                        sum -= values[c] * x[c];
                    }
                }

                x[r] = sum;
            }
        }

        private static void SolveTranspose(CSRSparseMatrix<double> Lt, double[] x, double[] b)
        {
            double[] values = Lt.Values;
            int[] rowIndices = Lt.RowIndices, colIndices = Lt.ColumnIndices;

            int rows = Lt.Rows;

        }
    }
}
