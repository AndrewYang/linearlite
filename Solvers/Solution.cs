﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Solvers
{
    /// <summary>
    /// Represents a solution object that contains information about exit codes, errors (if the solver failed) and 
    /// the solution itself (if the solver succeeded).
    /// </summary>
    /// <cat>solvers</cat>
    public sealed class Solution<T>
    {
        /// <summary>
        /// Whether the solver was successful in finding a solution.
        /// </summary>
        public bool Success { get; private set; }
        /// <summary>
        /// Gets the exit code of the solution. 
        /// </summary>
        public ExitCode ExitCode { get; private set; }
        /// <summary>
        /// If the solver was unsuccessful, this property will contain a more detailed error message.
        /// </summary>
        public string Error { get; private set; }
        /// <summary>
        /// If the solver was successful, this property will contain the solution.
        /// </summary>
        public T Result { get; private set; }

        internal Solution(ExitCode exitCode, T result)
        {
            ExitCode = exitCode;
            Success = true;
            Error = null;
            Result = result;
        }
        internal Solution(ExitCode exitCode, string error)
        {
            ExitCode = exitCode;
            Success = false;
            Error = error;
            Result = default(T);
        }
    }
    public enum ExitCode
    {
        /// <summary>
        /// Returned if the solution was successfully found
        /// </summary>
        Success,

        /// <summary>
        /// Returned if no solution exists because the system of equations are inconsistent
        /// </summary>
        InconsistentEquations,

        /// <summary>
        /// Returned if a solution is found, but infinite solutions exist
        /// </summary>
        InfiniteSolutions,

        /// <summary>
        /// Returned if using an iterative solver that fails to converge to a solution within the maximum
        /// allowed number of iterations. The best estimate for the solution up to that point will be returned.
        /// </summary>
        MayNotHaveConverged,

        /// <summary>
        /// Returned if using a gradient-dependent solver that encounters either a non-differentiable point or 
        /// a point whose derivative cannot be used by the solver. Program will be terminated early and the 
        /// best estimate for the solution up to that point will be returned.
        /// </summary>
        InvalidDerivative,

        /// <summary>
        /// Returned if the solution is not a number.
        /// </summary>
        NaN
    }
}
