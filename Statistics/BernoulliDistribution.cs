﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LinearNet.Global;
using LinearNet.Structs;

namespace LinearNet.Statistics
{
    /// <summary>
    /// Implements a Bernoulli distribution $\operatorname{Bern}(p)$, with a probability mass function given by 
    /// $$P(X = x) = \begin{cases}
    /// p &\text{if } x = 1\\
    /// 1 - p &\text{if } x = 0
    /// \end{cases}$$ 
    /// </summary>
    /// <cat>numerics</cat>
    public class BernoulliDistribution : IDiscreteUnivariateDistribution
    {
        private readonly double _p, _q;

        public override double Mean => _p;
        public override double Median
        {
            get
            {
                if (_p < 0.5) return 0.0;
                return 1.0;
            }
        }
        public override double Mode
        {
            get
            {
                if (_p < 0.5) return 0.0;
                return 1.0;
            }
        }
        public override double Variance => _p * _q;
        public override double Skewness => (_q - _p) / Math.Sqrt(_p * _q);
        public override double Kurtosis => 1.0 / (_p * _q) - 3.0;
        public override double Entropy => _q * Math.Log(_q) - _p * Math.Log(_p);
        public override DenseMatrix<double> FisherInformationMatrix
        {
            get
            {
                return new double[,] { { 1.0 / (_p * _q) } };
            }
        }

        /// <summary>
        /// Initialize a new Bernoulli distribution with the $p$ parameter
        /// </summary>
        /// <param name="p">The likelihood of success. Must be between 0 and 1 (inclusive).</param>
        public BernoulliDistribution(double p)
        {
            if (double.IsNaN(p))
            {
                throw new ArgumentOutOfRangeException(nameof(p), ExceptionMessages.NaNNotAllowed);
            }
            if (p < 0 || p > 1.0)
            {
                throw new ArgumentOutOfRangeException(nameof(p), ExceptionMessages.NotBetween0And1);
            }

            _p = p;
            _q = 1.0 - p;
        }

        public override double CDF(int x)
        {
            if (double.IsNaN(x))
            {
                return double.NaN;
            }
            if (double.IsNegativeInfinity(x))
            {
                return 0.0;
            }
            if (double.IsPositiveInfinity(x))
            {
                return 1.0;
            }

            if (x < 0.0)
            {
                return 0.0;
            }
            if (x < 1.0)
            {
                return _q;
            }
            return 1.0;
        }

        public override int InverseCDF(double x)
        {
            throw new NotImplementedException();
        }

        public override double KullbackLeiblerDivergence(IUnivariateDistribution<int> distribution)
        {
            throw new NotImplementedException();
        }

        public override double LogCDF(int x)
        {
            double cdf = CDF(x);
            if (double.IsNaN(cdf))
            {
                return double.NaN;
            }

            if (cdf == 0.0)
            {
                return double.NegativeInfinity;
            }

            return Math.Log(CDF(x));
        }

        public override double LogMoment(int n)
        {
            throw new NotImplementedException();
        }

        public override double LogPDF(int x)
        {
            if (x == 0)
            {
                return Math.Log(_q);
            }
            if (x == 1)
            {
                return Math.Log(_p);
            }
            return 0.0;
        }

        public override double Moment(int n)
        {
            throw new NotImplementedException();
        }

        public override double P(int x)
        {
            return PDF(x);
        }

        public override double PDF(int x)
        {
            if (x == 0)
            {
                return _q;
            }
            if (x == 1)
            {
                return _p;
            }
            return 0.0;
        }

        public override int Sample(Random random)
        {
            if (random.NextDouble() < _p)
            {
                return 0;
            }
            return 1;
        }
    }
}
