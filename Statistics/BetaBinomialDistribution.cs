﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LinearNet.Functions;
using LinearNet.Global;
using LinearNet.Structs;

namespace LinearNet.Statistics
{
    /// <summary>
    /// Implements a beta-binomial distribution, whose probability mass function is given by 
    /// $$P(X = k) = \dbinom{n}{k}\frac{\Beta(k + \alpha, n - k - \beta)}{\Beta(\alpha, \beta)},\enspace k = 0, 1, ..., n$$
    /// </summary>
    /// <cat>numerics</cat>
    public class BetaBinomialDistribution : IDiscreteUnivariateDistribution
    {
        /// <summary>
        /// Calculate these intermediate variables at construction time
        /// </summary>
        private readonly double _a_plus_b;
        private readonly double _beta_a_b; // Beta(a + b)

        /// <summary>
        /// The number of trials, $n$.
        /// </summary>
        public int N { get; private set; }
        /// <summary>
        /// The $\alpha$ parameter of the distribution.
        /// </summary>
        public double Alpha { get; private set; }
        /// <summary>
        /// The $\beta$ parameter of the distribution.
        /// </summary>
        public double Beta { get; private set; }

        public override double Mean => N * Alpha / _a_plus_b;

        public override double Median => throw new NotImplementedException();

        public override double Mode => throw new NotImplementedException();

        public override double Variance => N * Alpha * Beta * (_a_plus_b + N) / (_a_plus_b * _a_plus_b * (_a_plus_b + 1));

        public override double Skewness =>
            (_a_plus_b + 2 * N) * (Beta - Alpha) / (_a_plus_b + 2.0) * 
            Math.Sqrt((1 + _a_plus_b) / (N * Alpha * Beta * (N + _a_plus_b)));

        public override double Kurtosis => CalculateKurtosis();

        public override double Entropy => throw new NotImplementedException();

        public override DenseMatrix<double> FisherInformationMatrix => throw new NotImplementedException();

        /// <summary>
        /// Initializes a beta-binomial distribution using the parameters $n, \alpha$ and $\beta$.
        /// </summary>
        /// <param name="n">The number of trials, $n$.</param>
        /// <param name="alpha">The $\alpha$ shape parameter of the beta function.</param>
        /// <param name="beta">The $\beta$ shape parameter of the beta function.</param>
        public BetaBinomialDistribution(int n, double alpha, double beta)
        {
            if (n < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(n), ExceptionMessages.NegativeNotAllowed);
            }
            if (!(alpha > 0))
            {
                throw new ArgumentOutOfRangeException(nameof(alpha), ExceptionMessages.NonPositiveNotAllowed);
            }
            if (!(beta > 0))
            {
                throw new ArgumentOutOfRangeException(nameof(beta), ExceptionMessages.NonPositiveNotAllowed);
            }

            N = n;
            Alpha = alpha;
            Beta = beta;

            _a_plus_b = Alpha + Beta;
            _beta_a_b = MathFunctions.Beta(Alpha, Beta);
        }


        public override double CDF(int x)
        {
            throw new NotImplementedException();
        }

        public override int InverseCDF(double x)
        {
            throw new NotImplementedException();
        }

        public override double KullbackLeiblerDivergence(IUnivariateDistribution<int> distribution)
        {
            throw new NotImplementedException();
        }

        public override double LogCDF(int x)
        {
            throw new NotImplementedException();
        }

        public override double LogMoment(int n)
        {
            throw new NotImplementedException();
        }

        public override double LogPDF(int x)
        {
            if (x < 0.0) return double.NegativeInfinity;
            if (x >= N) return 0.0;

            return MathFunctions.LogCombinations(N, x) + MathFunctions.LogBeta(x + Alpha, N - x + Beta) - _beta_a_b;
        }

        public override double Moment(int n)
        {
            throw new NotImplementedException();
        }

        public override double P(int x)
        {
            return PDF(x);
        }

        public override double PDF(int x)
        {
            if (x < 0) return 0.0;
            if (x >= N) return 1.0;

            return Math.Exp(LogPDF(x));
        }

        public override int Sample(Random random)
        {
            throw new NotImplementedException();
        }

        private double CalculateKurtosis()
        {
            double ab = Alpha * Beta, abn = ab * N;
            double t1 = _a_plus_b * (_a_plus_b + 6.0 * N - 1.0);
            double t2 = 3.0 * ab * (N - 2.0);
            double t3 = 6 * N * N;
            double t4 = 3.0 * abn * (6 - N) / _a_plus_b;
            double t5 = 18.0 * abn * N / (_a_plus_b * _a_plus_b);

            double kurt = 
                _a_plus_b * _a_plus_b * (1.0 + _a_plus_b) / 
                (N * Alpha * Beta * (_a_plus_b + 2.0) * (_a_plus_b + 3.0) * (_a_plus_b + N)) * 
                (t1 + t2 + t3 - t4 - t5);

            return kurt;
        }
    }
}
