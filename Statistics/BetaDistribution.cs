﻿using LinearNet.Functions;
using LinearNet.Global;
using LinearNet.Structs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Statistics
{
    /// <summary>
    /// Implements a beta distribution, where the probability density function is given by 
    /// $$P(x) = \frac{x^{\alpha - 1}(1 - x)^{\beta - 1}}{\Beta(\alpha, \beta)},$$
    /// where $\Beta(\alpha, \beta)$ is the <a href="/docs/numerics/MathFunctions.html#Beta_Static_Method">beta function</a>.
    /// </summary>
    /// <cat>numerics</cat>
    public class BetaDistribution : IContinuousUnivariateDistribution
    {
        /// <summary>
        /// The $\alpha$ parameter.
        /// </summary>
        public double Alpha { get; private set; }
        /// <summary>
        /// The $\beta$ parameter.
        /// </summary>
        public double Beta { get; private set; }

        public override double Mean => Alpha / (Alpha + Beta);
        public override double Median => throw new NotImplementedException();
        public override double Mode => (Alpha - 1.0) / (Alpha + Beta - 2.0);
        public override double Variance
        {
            get
            {
                double a_plus_b = Alpha + Beta;
                return Alpha * Beta / (a_plus_b * a_plus_b * (a_plus_b + 1.0));
            }
        }
        public override double Skewness
        {
            get
            {
                double a_b_1 = Alpha + Beta + 1.0;
                return 2.0 * (Beta - Alpha) * Math.Sqrt(a_b_1 / (Alpha * Beta)) / (a_b_1 + 1.0);
            }
        }
        public override double Kurtosis
        {
            get
            {
                double a_b_2 = Alpha + Beta + 2.0;
                double a_minus_b = Alpha - Beta;
                double ab = Alpha * Beta;
                return 6.0 * (a_minus_b * a_minus_b * (a_b_2 - 1.0) - ab * a_b_2) /
                    (ab * a_b_2 * (a_b_2 + 1.0)) + 3.0;
            }
        }
        public override double Entropy
        {
            get
            {
                return MathFunctions.LogBeta(Alpha, Beta)
                    - (Alpha - 1.0) * MathFunctions.Gamma(Alpha)
                    - (Beta - 1.0) * MathFunctions.Gamma(Beta)
                    + (Alpha + Beta - 2.0) * MathFunctions.Gamma(Alpha + Beta);
            }
        }
        public override DenseMatrix<double> FisherInformationMatrix => throw new NotImplementedException();

        /// <summary>
        /// Initialize a beta distribution with the $\alpha$ and $\beta$ parameters.
        /// </summary>
        /// <param name="alpha">The $\alpha$ parameter. Must be positive.</param>
        /// <param name="beta">The $\beta$ parameter. Must be positive.</param>
        public BetaDistribution(double alpha, double beta)
        {
            if (double.IsNaN(alpha))
            {
                throw new ArgumentException(nameof(alpha), ExceptionMessages.NaNNotAllowed);
            }
            if (double.IsNaN(beta))
            {
                throw new ArgumentException(nameof(beta), ExceptionMessages.NaNNotAllowed);
            }
            if (alpha <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(alpha), ExceptionMessages.NonPositiveNotAllowed);
            }
            if (beta <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(beta), ExceptionMessages.NonPositiveNotAllowed);
            }
            Alpha = alpha;
            Beta = beta;
        }

        public override double CDF(double x)
        {
            if (double.IsNaN(x))
            {
                return double.NaN;
            }
            if (x < 0.0)
            {
                return 0.0;
            }
            if (x > 1.0)
            {
                return 1.0;
            }

            return MathFunctions.IncompleteBeta(x, Alpha, Beta) / MathFunctions.Beta(Alpha, Beta);
        }
        public override double LogCDF(double x)
        {
            if (double.IsNaN(x))
            {
                return double.NaN;
            }
            if (x < 0.0)
            {
                return double.NegativeInfinity;
            }
            if (x > 1.0)
            {
                return 0.0;
            }

            return Math.Log(CDF(x));
        }
        public override double InverseCDF(double x)
        {
            throw new NotImplementedException();
        }

        public override double PDF(double x)
        {
            if (double.IsNaN(x))
            {
                return double.NaN;
            }
            if (0.0 <= x || 1.0 <= x)
            {
                return 0.0;
            }
            return Math.Exp(LogPDF(x));
        }
        public override double LogPDF(double x)
        {
            if (x <= 0.0 || 1.0 <= x)
            {
                return 0.0;
            }
            return (Alpha - 1) * Math.Log(x) + (Beta - 1.0) * Math.Log(1.0 - x) - MathFunctions.LogBeta(Alpha, Beta);
        }

        public override double P(double x)
        {
            return 0.0;
        }

        public override double KullbackLeiblerDivergence(IUnivariateDistribution<double> distribution)
        {
            if (distribution is BetaDistribution d)
            {
                return MathFunctions.LogBeta(d.Alpha, d.Beta)
                    - MathFunctions.LogBeta(Alpha, Beta)
                    + (Alpha - d.Alpha) * MathFunctions.Gamma(Alpha)
                    + (Beta - d.Beta) * MathFunctions.Gamma(Beta)
                    + (d.Alpha - Alpha + d.Beta - Beta) * MathFunctions.Gamma(Alpha + Beta);
            }
            throw new ArgumentException(nameof(distribution), ExceptionMessages.DistributionNotSupported);
        }

        // E[X^n] = product((a + r) / (a + b + r), r in [0, n - 1])
        public override double Moment(int n)
        {
            return Math.Exp(LogMoment(n));
        }

        public override double LogMoment(int n)
        {
            if (n <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(n), ExceptionMessages.NonPositiveNotAllowed);
            }

            double ln = 0.0;
            for (int r = 0; r < n; ++r)
            {
                ln += Math.Log(Alpha + r) - Math.Log(Alpha + Beta + r);
            }
            return ln;
        }
    }
}
