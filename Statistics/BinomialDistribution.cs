﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LinearNet.Functions;
using LinearNet.Global;
using LinearNet.Structs;

namespace LinearNet.Statistics
{
    /// <summary>
    /// Implements the discrete binomial distribution $\operatorname{Bin}(n, p)$, where 
    /// $$P(X = k) = \dbinom{n}{k}p^k(1 - p)^{n - k},\quad k = 0, 1, ..., n$$
    /// </summary>
    /// <cat>numerics</cat>
    public class BinomialDistribution : IUnivariateDistribution<int>
    {
        private readonly int _n;
        private readonly double _p, _q;

        public override double Mean => _n * _p;
        public override double Median => throw new NotImplementedException();
        public override double Mode => throw new NotImplementedException();
        public override double Variance => _n * _p * _q;
        public override double Skewness => (_q - _p) / Math.Sqrt(_n * _p * _q);
        public override double Kurtosis => 1.0 / Math.Sqrt(_n * _p * _q) - 3.0;
        public override double Entropy => throw new NotImplementedException();

        public override DenseMatrix<double> FisherInformationMatrix
        {
            get
            {
                return new double[,] { { _n / (_p * _q) } };
            }
        }

        /// <summary>
        /// Initialize a beta distribution using the parameters $n$ and $p$.
        /// </summary>
        /// <param name="n">The number of trials, $n$. Must be positive.</param>
        /// <param name="p">The probability of success of each trial, $p$. Must be between 0 and 1 (inclusive).</param>
        public BinomialDistribution(int n, double p)
        {
            if (n <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(n), ExceptionMessages.NonPositiveNotAllowed);
            }
            if (p < 0.0 || p > 1.0)
            {
                throw new ArgumentOutOfRangeException(nameof(p), ExceptionMessages.NotBetween0And1);
            }

            _n = n;
            _p = p;
            _q = 1.0 - p;
        }

        public override double CDF(int x)
        {
            if (x < 0)
            {
                return 0.0;
            }

            double sum = 0.0, ln_p = Math.Log(_p), ln_q = Math.Log(_q);

            int end = Math.Min(x, _n);
            for (int i = 0; i <= end; ++i)
            {
                sum += Math.Exp(MathFunctions.LogCombinations(x, i) + i * ln_p + (_n - i) * ln_q);
            }
            return sum;
        }

        public override int InverseCDF(double x)
        {
            throw new NotImplementedException();
        }

        public override double KullbackLeiblerDivergence(IUnivariateDistribution<int> distribution)
        {
            throw new NotImplementedException();
        }

        public override double LogCDF(int x)
        {
            if (x < 0)
            {
                return double.NegativeInfinity;
            }
            return Math.Log(CDF(x));
        }

        public override double LogMoment(int n)
        {
            throw new NotImplementedException();
        }

        public override double LogPDF(int x)
        {
            if (x < 0 || x > _n)
            {
                return double.NegativeInfinity;
            }
            return MathFunctions.LogCombinations(_n, x) + x * Math.Log(_p) + (_n - x) * Math.Log(_q);
        }

        public override double Moment(int n)
        {
            throw new NotImplementedException();
        }

        public override double P(int x)
        {
            return PDF(x);
        }

        public override double PDF(int x)
        {
            if (x < 0 || x > _n)
            {
                return 0.0;
            }
            return Math.Exp(LogPDF(x));
        }

        public override int Sample(Random random)
        {
            throw new NotImplementedException();
        }
    }
}
