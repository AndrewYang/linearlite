﻿using LinearNet.Global;
using LinearNet.Structs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Statistics
{
    /// <summary>
    /// Implements the Cauchy distribution, whose probability density function is given by 
    /// $$f(x) = \frac{1}{\pi \gamma \big(1 + (\frac{x - x_0}{\gamma})^2 \big)}, \quad -\infty < x < \infty.$$
    /// </summary>
    /// <cat>numerics</cat>
    public class CauchyDistribution : IContinuousUnivariateDistribution
    {
        /// <summary>
        /// The location parameter, $x_0$.
        /// </summary>
        public double Location { get; private set; }
        /// <summary>
        /// The scale parameter, $\gamma$.
        /// </summary>
        public double Scale { get; private set; }

        public override double Mean => double.NaN;
        public override double Median => Location;
        public override double Mode => Location;
        public override double Variance => double.NaN;
        public override double Skewness => double.NaN;
        public override double Kurtosis => double.NaN;
        public override double Entropy => Math.Log(4.0 * Math.PI * Scale);

        public override DenseMatrix<double> FisherInformationMatrix => throw new NotImplementedException();

        /// <summary>
        /// Initializes a Cauchy distribution with the specified location and scale parameters.
        /// </summary>
        /// <param name="location">The location parameter $x_0$.</param>
        /// <param name="scale">The scale parameter $\gamma$.</param>
        public CauchyDistribution(double location, double scale)
        {
            if (double.IsNaN(location))
            {
                throw new ArgumentOutOfRangeException(nameof(location), ExceptionMessages.NaNNotAllowed);
            }
            if (double.IsInfinity(location))
            {
                throw new ArgumentOutOfRangeException(nameof(location), ExceptionMessages.InfinityNotAllowed);
            }
            if (double.IsNaN(scale))
            {
                throw new ArgumentOutOfRangeException(nameof(scale), ExceptionMessages.NaNNotAllowed);
            }
            if (double.IsInfinity(scale))
            {
                throw new ArgumentOutOfRangeException(nameof(scale), ExceptionMessages.InfinityNotAllowed);
            }
            if (scale <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(scale), ExceptionMessages.NonPositiveNotAllowed);
            }

            Location = location;
            Scale = scale;
        }

        public override double CDF(double x)
        {
            if (double.IsNaN(x))
            {
                return double.NaN;
            }
            if (double.IsPositiveInfinity(x))
            {
                return 1.0;
            }
            if (double.IsNegativeInfinity(x))
            {
                return 0.0;
            }

            return Math.Atan2(x - Location, Scale) / Math.PI + 0.5;
        }

        public override double InverseCDF(double x)
        {
            return Location + Scale * Math.Tan(Math.PI * (x - 0.5));
        }

        public override double KullbackLeiblerDivergence(IUnivariateDistribution<double> distribution)
        {
            throw new NotImplementedException();
        }

        public override double LogCDF(double x)
        {
            if (double.IsNaN(x))
            {
                return double.NaN;
            }
            if (double.IsPositiveInfinity(x))
            {
                return 0.0;
            }
            if (double.IsNegativeInfinity(x))
            {
                return double.NegativeInfinity;
            }
            return Math.Log(CDF(x));
        }

        public override double LogMoment(int n)
        {
            return double.NaN;
        }

        public override double LogPDF(double x)
        {
            if (double.IsNaN(x))
            {
                return double.NaN;
            }
            if (double.IsInfinity(x))
            {
                return double.NegativeInfinity;
            }
            return Math.Log(PDF(x));
        }

        public override double Moment(int n)
        {
            return double.NaN;
        }

        public override double P(double x)
        {
            return 0.0;
        }

        public override double PDF(double x)
        {
            if (double.IsNaN(x))
            {
                return double.NaN;
            }
            if (double.IsInfinity(x))
            {
                return 0.0;
            }

            double s = (x - Location) / Scale;
            return 1.0 / (Math.PI * Scale * (1.0 + s * s));
        }
    }
}
