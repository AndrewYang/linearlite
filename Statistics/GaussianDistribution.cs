﻿using LinearNet.Functions;
using LinearNet.Global;
using LinearNet.Structs;
using System;

namespace LinearNet.Statistics
{
    /// <summary>
    /// Implements a Gaussian (Normal) distribution, $N(\mu, \sigma^2)$, with a probability mass function 
    /// $$f(x) = \frac{1}{\sigma\sqrt{2\pi}} \exp \Big(-\frac{(x-\mu)^2}{2\sigma^2}\Big), \quad -\infty < x < \infty$$
    /// </summary>
    /// <cat>numerics</cat>
    public class GaussianDistribution : IContinuousUnivariateDistribution
    {
        private double _mean, _standardDeviation;

        public override double Mean => _mean;
        public override double Median => _mean;
        public override double Mode => _mean;
        public override double Variance => _standardDeviation * _standardDeviation;
        public override double Skewness => 0.0;
        public override double Kurtosis => 3.0;
        public override double Entropy => 0.5 * Math.Log(2.0 * Math.PI * Math.E * Variance);
        public override DenseMatrix<double> FisherInformationMatrix
        {
            get
            {
                if (_standardDeviation == 0.0)
                {
                    return new double[,]
                    {
                        { double.NaN,   0.0 },
                        { 0.0,          double.NaN }
                    };
                }

                double one_over_sd = 1.0 / Variance;
                return new double[,]
                {
                    { one_over_sd,  0.0 },
                    { 0.0,          2.0 * one_over_sd }
                };
            }
        }

        /// <summary>
        /// Initializes a Gaussian distribution using the specified mean and standard deviation.
        /// </summary>
        /// <param name="mean">The mean of the distribution $\mu$.</param>
        /// <param name="standardDeviation">The standard deviation, $\sigma$. Must be non-negative.</param>
        public GaussianDistribution(double mean, double standardDeviation)
        {
            if (double.IsNaN(mean))
            {
                throw new ArgumentOutOfRangeException(nameof(mean));
            }
            if (double.IsNaN(standardDeviation))
            {
                throw new ArgumentException(nameof(standardDeviation));
            }
            if (standardDeviation < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(standardDeviation), ExceptionMessages.NegativeNotAllowed);
            }

            _mean = mean;
            _standardDeviation = standardDeviation;
        }

        public override double CDF(double x)
        {
            if (double.IsNaN(x))
            {
                return double.NaN;
            }
            if (double.IsPositiveInfinity(x))
            {
                return 1.0;
            }
            if (double.IsNegativeInfinity(x))
            {
                return 0.0;
            }
            if (_standardDeviation == 0.0)
            {
                return x < _mean ? 0.0 : 1.0;
            }

            return 0.5 * (1 + MathFunctions.Erf((x - _mean) / (Constants.Sqrt2 * _standardDeviation)));
        }
        public override double LogCDF(double x)
        {
            if (double.IsNaN(x))
            {
                return double.NaN;
            }
            if (double.IsNegativeInfinity(x))
            {
                return double.NegativeInfinity;
            }

            if (_standardDeviation == 0.0)
            {
                return x < _mean ? double.NegativeInfinity : 0.0;
            }
            return Math.Log(CDF(x));
        }
        public override double InverseCDF(double x)
        {
            throw new NotImplementedException();
        }

        public override double PDF(double x)
        {
            if (double.IsNaN(x)) return double.NaN;
            if (double.IsInfinity(x)) return 0.0;

            if (_standardDeviation == 0.0)
            {
                return x == _mean ? 1.0 : 0.0;
            }

            double _x = (x - _mean) / _standardDeviation;
            return Math.Exp(-0.5 * _x * _x) / (_standardDeviation * Constants.Sqrt2Pi);
        }
        public override double LogPDF(double x)
        {
            if (double.IsNaN(x)) return double.NaN;
            if (double.IsInfinity(x)) return double.NegativeInfinity;

            if (_standardDeviation == 0.0)
            {
                return x == _mean ? 0.0 : double.NegativeInfinity;
            }

            double _x = (x - _mean) / _standardDeviation;
            return -0.5 * _x * _x - Math.Log(_standardDeviation * Constants.Sqrt2Pi);
        }

        public override double P(double x)
        {
            if (_standardDeviation == 0.0 && x == _mean)
            {
                return 1.0;
            }

            return 0.0;
        }

        public override double KullbackLeiblerDivergence(IUnivariateDistribution<double> distribution)
        {
            if (distribution == null)
            {
                throw new ArgumentNullException(nameof(distribution));
            }

            if (distribution is GaussianDistribution)
            {
                GaussianDistribution gd = (GaussianDistribution)distribution;
                double sd = gd._standardDeviation, mu = gd._mean;
                return 0.5 * (
                    Math.Pow(_standardDeviation / sd, 2.0) +
                    Math.Pow((_mean - mu) / sd, 2.0) -
                    1.0 +
                    2 * Math.Log(sd / _standardDeviation));
            }
            throw new ArgumentException(nameof(distribution), ExceptionMessages.DistributionNotSupported);
        }

        public override double Moment(int n)
        {
            throw new NotImplementedException();
        }

        public override double LogMoment(int n)
        {
            throw new NotImplementedException();
        }
    }
}
