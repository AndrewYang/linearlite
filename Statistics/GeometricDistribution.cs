﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LinearNet.Global;
using LinearNet.Structs;

namespace LinearNet.Statistics
{
    /// <summary>
    /// Implements the discrete geometric distribution, whose probability mass function is given by either:
    /// $$P_1(X = k) = (1 - p)^{k - 1}p,\quad k = 1, 2, 3, ... $$
    /// $$P_2(X = k) = (1 - p)^kp, \quad k = 0, 1, 2, ... $$
    /// This implementation contains an addition parameter, <txt>type1</txt>, which is used to 
    /// specify which of the two variants to use.
    /// </summary>
    /// <cat>numerics</cat>
    public class GeometricDistribution : IUnivariateDistribution<int>
    {
        /// <summary>
        /// Type 1: the number of Bernoulli trials needed to get 1 success, for { 1, 2, 3, ... }
        /// Type 2: the number of failures before the first success for X in { 0, 1, 2, ... }
        /// </summary>
        private readonly bool _type1;
        private readonly double _p, _q;

        public override double Mean
        {
            get
            {
                if (_type1)
                {
                    return 1.0 / _p;
                }
                return 1.0 / _p - 1.0;
            }
        }
        public override double Median
        {
            get
            {
                double med = Math.Ceiling(-1.0 / Util.Log2(_q));
                if (_type1)
                {
                    return med;
                }
                return med - 1.0;
            }
        }
        public override double Mode => _type1 ? 1.0 : 0.0;
        public override double Variance => _q / (_p * _p);
        public override double Skewness => (2.0 - _p) / Math.Sqrt(_q);
        public override double Kurtosis => _p * _p / _q + 9.0;
        public override double Entropy => -(_q * Util.Log2(_q) + _p * Util.Log2(_p)) / _p;

        public override DenseMatrix<double> FisherInformationMatrix => throw new NotImplementedException();

        /// <summary>
        /// Create a (discrete) geometric distribution with probability $p$.
        /// </summary>
        /// <param name="p">The probability of success for each Bernoulli trial.</param>
        /// <param name="type1">
        /// <p>
        /// If true, the distribution will be of the first type, i.e. $X$ is the number of Bernoulli trials needed for 1 success, 
        /// over the support $\{ 1, 2, 3, ... \}$.
        /// </p>
        /// <p>
        /// Otherwise, the distribution will be of the second type, i.e. $X$ is the number of failed Bernoulli trials before 
        /// the first success, over the support $\{ 0, 1, 2, ... \}$.
        /// </p>
        /// </param>
        public GeometricDistribution(double p, bool type1 = true)
        {
            if (type1)
            {
                if (!(0 < p && p < 1))
                {
                    throw new ArgumentOutOfRangeException(nameof(p), ExceptionMessages.NotBetween0And1);
                }
            }
            else
            {
                if (!(0 < p && p <= 1))
                {
                    throw new ArgumentOutOfRangeException(nameof(p), ExceptionMessages.NotBetween0And1);
                }
            }

            _p = p;
            _q = 1.0 - p;
            _type1 = type1;
        }

        public override double CDF(int k)
        {
            if (_type1)
            {
                if (k < 1)
                {
                    return 0.0;
                }
                return 1.0 - Math.Pow(_q, k);
            }
            else
            {
                if (k < 0)
                {
                    return 0.0;
                }
                return 1.0 - Math.Pow(_q, k + 1);
            }
        }

        public override int InverseCDF(double x)
        {
            throw new NotImplementedException();
        }

        public override double KullbackLeiblerDivergence(IUnivariateDistribution<int> distribution)
        {
            throw new NotImplementedException();
        }

        public override double LogCDF(int x)
        {
            double cdf = CDF(x);
            if (cdf <= 0.0)
            {
                return double.NegativeInfinity;
            }
            return Math.Log(cdf);
        }

        public override double LogMoment(int n)
        {
            throw new NotImplementedException();
        }

        public override double LogPDF(int k)
        {
            if (_type1)
            {
                if (k < 1)
                {
                    return double.NegativeInfinity;
                }
                return Math.Log(_p) + (k - 1) * Math.Log(_q);
            }
            else
            {
                if (k < 0)
                {
                    return double.NegativeInfinity;
                }
                return Math.Log(_p) + k * Math.Log(_q);
            }
        }

        public override double Moment(int n)
        {
            throw new NotImplementedException();
        }

        public override double P(int x)
        {
            return PDF(x);
        }

        public override double PDF(int x)
        {
            double pdf = LogPDF(x);
            if (double.IsNegativeInfinity(pdf))
            {
                return 0.0;
            }
            return Math.Exp(pdf);
        }

        public override int Sample(Random random)
        {
            throw new NotImplementedException();
        }
    }
}
