﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LinearNet.Functions;
using LinearNet.Global;
using LinearNet.Structs;

namespace LinearNet.Statistics
{
    /// <summary>
    /// Implements a discrete hypergeometric distribution.
    /// </summary>
    /// <cat>numerics</cat>
    public class HypergeometricDistribution : IUnivariateDistribution<int>
    {
        public int N { get; private set; }
        public int K { get; private set; }
        public int n { get; private set; }

        public override double Mean => n * K / (double)N;
        public override double Median => throw new NotImplementedException();
        public override double Mode => Math.Floor((n + 1) * (K + 1) / (double)(N + 2));
        public override double Variance => n * K * (N - K) * (N - n) / (double)(N * N * (N - 1));
        public override double Skewness => (N - 2 * K) * Math.Sqrt(N - 1) * (N - 2 * n) / (Math.Sqrt(n * K * (N - K) * (N - n)) * (N - 2));
        public override double Kurtosis
        {
            get
            {
                double num =
                    (N - 1) * N * N * (N * (N + 1) - 6 * K * (N - K) - 6 * n * (N - n)) +
                    6 * n * K * (N - K) * (N - n) * (5 * N - 6);
                double den =
                    n * K * (N - K) * (N - n) * (N - 2) * (N - 3);
                return num / den;
            }
        }
        public override double Entropy => throw new NotImplementedException();
        public override DenseMatrix<double> FisherInformationMatrix => throw new NotImplementedException();

        public HypergeometricDistribution(int N, int K, int n)
        {
            if (N < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(N), ExceptionMessages.NegativeNotAllowed);
            }
            if (!(0 <= K && K <= N))
            {
                throw new ArgumentOutOfRangeException(nameof(K), "K is not between 0 and N");
            }
            if (!(0 <= n && n <= N))
            {
                throw new ArgumentOutOfRangeException(nameof(n), "n is not between 0 and N");
            }

            this.N = N;
            this.K = K;
            this.n = n;
        }
        public override double CDF(int x)
        {
            throw new NotImplementedException();
        }

        public override int InverseCDF(double x)
        {
            throw new NotImplementedException();
        }

        public override double KullbackLeiblerDivergence(IUnivariateDistribution<int> distribution)
        {
            throw new NotImplementedException();
        }

        public override double LogCDF(int x)
        {
            throw new NotImplementedException();
        }

        public override double LogMoment(int n)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The support is max(0, n + K - N), ... , min(n, K)
        /// </summary>
        /// <param name="k"></param>
        /// <returns></returns>
        public override double LogPDF(int k)
        {
            int min = Math.Max(0, n + K - N), max = Math.Min(n, K);
            if (k < min || k > max)
            {
                return double.NegativeInfinity;
            }
            return MathFunctions.LogCombinations(K, k) + MathFunctions.LogCombinations(N - K, n - k) - MathFunctions.LogCombinations(N, n);
        }

        public override double Moment(int n)
        {
            throw new NotImplementedException();
        }

        public override double P(int x)
        {
            return PDF(x);
        }

        public override double PDF(int x)
        {
            double logPDF = LogPDF(x);
            if (double.IsNegativeInfinity(logPDF))
            {
                return 0.0;
            }
            return Math.Exp(logPDF);
        }

        public override int Sample(Random random)
        {
            throw new NotImplementedException();
        }
    }
}
