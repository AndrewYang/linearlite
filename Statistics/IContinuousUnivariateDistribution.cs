﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Statistics
{
    /// <summary>
    /// An abstract class representing a continuous univariate distribution.
    /// </summary>
    /// <cat>numerics</cat>
    public abstract class IContinuousUnivariateDistribution : IUnivariateDistribution<double>
    {
        /// <summary>
        /// Sample a random element according to this distribution.
        /// </summary>
        /// <param name="random">A random object.</param>
        /// <returns>The sampled element.</returns>
        public override double Sample(Random random)
        {
            return InverseCDF(random.NextDouble());
        }
    }
}
