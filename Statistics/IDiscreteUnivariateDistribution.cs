﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Statistics
{
    /// <summary>
    /// An abstract class representing a discrete univariate distribution.
    /// </summary>
    /// <cat>numerics</cat>
    public abstract class IDiscreteUnivariateDistribution : IUnivariateDistribution<int>
    {

    }
}
