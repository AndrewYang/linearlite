﻿using LinearNet.Structs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Statistics
{
    /// <summary>
    /// <p>
    /// An abstract class representing an univariate distribution with support 
    /// over the type <txt>T</txt>.
    /// </p>
    /// <p>All implementations of a univariate distribution extend this class.</p>
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <cat>numerics</cat>
    public abstract class IUnivariateDistribution<T> where T : IComparable<T>
    {
        /// <summary>
        /// The mean of the distribution, $\operatorname{E}(X)$.
        /// </summary>
        public abstract double Mean { get; }
        /// <summary>
        /// The median of the distribution.
        /// </summary>
        public abstract double Median { get; }
        /// <summary>
        /// The mode of the distribution.
        /// </summary>
        public abstract double Mode { get; }
        /// <summary>
        /// The variance, or second standardized moment, of the distribution, $\operatorname{Var}(X)$
        /// </summary>
        public abstract double Variance { get; }
        /// <summary>
        /// The skewness, or third standardized moment, of the distribution.
        /// </summary>
        public abstract double Skewness { get; }
        /// <summary>
        /// The kurtosis, or fourth standarized moment, of the distribution.
        /// </summary>
        public abstract double Kurtosis { get; }
        /// <summary>
        /// The entropy of the distribution, $-\operatorname{E}(\log(f(x)))$ where $f(x)$ is the p.m.f 
        /// for discrete distributions, and p.d.f. for continuous distributions.
        /// </summary>
        public abstract double Entropy { get; }
        /// <summary>
        /// Returns the standard derivation of the distribution, $\sigma(x) = \sqrt{\operatorname{Var}(X)}$.
        /// </summary>
        public double StandardDeviation
        {
            get
            {
                double var = Variance;
                if (double.IsNaN(var) || var < 0)
                {
                    return double.NaN;
                }
                return Math.Sqrt(var);
            }
        }
        /// <summary>
        /// Returns the excess kurtosis, which is just the kurtosis substract 3.
        /// </summary>
        public double ExcessKurtosis 
        { 
            get
            {
                return Kurtosis - 3.0;
            } 
        }
        /// <summary>
        /// Returns the <a href="https://en.wikipedia.org/wiki/Fisher_information">Fisher information matrix</a> of this distribution, a square matrix with the 
        /// same dimensionality as the number of parameters of this distribution.
        /// </summary>
        public abstract DenseMatrix<double> FisherInformationMatrix { get; }

        /// <summary>
        /// For continuous distributions, returns the probability density function at a point $x$.
        /// <p>For discrete distributions, the probability mass function is evaluated.</p>
        /// </summary>
        /// <param name="x">The point at which to evaluate the p.d.f. or p.m.f.</param>
        /// <returns>The value of the p.d.f. or p.m.f.</returns>
        public abstract double PDF(T x);

        /// <summary>
        /// Returns the natural logarithm of the p.d.f. or p.m.f. at a point $x$.
        /// </summary>
        /// <param name="x">The point at which to evaluate the p.d.f. or p.m.f.</param>
        /// <returns>The natural logarithm of the p.d.f. or p.m.f.</returns>
        public abstract double LogPDF(T x);

        /// <summary>
        /// Returns the cumulative distribution function evaluated at a point $x$.
        /// </summary>
        /// <param name="x">The point to evaluate the CDF.</param>
        /// <returns>The CDF evaluated at point $x$.</returns>
        public abstract double CDF(T x);

        /// <summary>
        /// Returns the natural logarithm of the cumulative distribution function, evaluated at a point $x$.
        /// </summary>
        /// <param name="x">The point to evaluate the CDF.</param>
        /// <returns>The natural logarithm of the CDF at a point $x$.</returns>
        public abstract double LogCDF(T x);

        /// <summary>
        /// Returns the inverse cumulative distribution function, evaluated at a point $x \in [0, 1]$.
        /// </summary>
        /// <param name="x">The point at which to evaluate the inverse cumulative distribution function. Must lie in the range $[0, 1]$</param>
        /// <returns>The inverse CDF.</returns>
        public abstract T InverseCDF(double x);

        /// <summary>
        /// Returns $P(a \le X < b)$, the probability that the random variable $X$ from this distribution lies in the range $[a, b)$.
        /// <!--inputs-->
        /// </summary>
        /// <param name="a">The (inclusive) lower bound.</param>
        /// <param name="b">The (exclusive) upper bound.</param>
        /// <returns>The probability of a random variable falling within the range.</returns>
        public double P(T a, T b)
        {
            if (a.CompareTo(b) >= 0)
            {
                return 0.0;
            }

            if (a == null)
            {
                throw new ArgumentNullException(nameof(a));
            }
            if (b == null)
            {
                throw new ArgumentNullException(nameof(b));
            }

            return CDF(b) - CDF(a);
        }

        /// <summary>
        /// The probability of realizing $x$, $P(X = x)$.
        /// </summary>
        /// <param name="x">A point in the distribution's support.</param>
        /// <returns>The probability of observing the point $x$.</returns>
        public abstract double P(T x);

        /// <summary>
        /// Returns the Kullback-Leibler divergence (KL divergence) between this distribution with another. 
        /// </summary>
        /// <param name="distribution">A second distribution to calculate the KL divergence with.</param>
        /// <returns>The KL divergence between this distribution and another.</returns>
        public abstract double KullbackLeiblerDivergence(IUnivariateDistribution<T> distribution);

        /// <summary>
        /// Returns the cross entropy between this distribution and another. The cross entropy is given by the entropy 
        /// of this distribution plus the KL divergence between the two distributions.
        /// </summary>
        /// <param name="distribution">A second distribution to calculate the cross entropy with.</param>
        /// <returns>The cross entropy between the two distributions.</returns>
        public double CrossEntropy(IUnivariateDistribution<T> distribution)
        {
            return Entropy + KullbackLeiblerDivergence(distribution);
        }

        /// <summary>
        /// Calculates and returns the $n$-th moment of the distribution, $\operatorname{E}(X^n)$.
        /// </summary>
        /// <param name="n">The order of the moment.</param>
        /// <returns>The $n$-th moment of this distribution.</returns>
        public abstract double Moment(int n);
        /// <summary>
        /// Calculates and returns the natural log of the $n$-th moment of the distribution, $\ln(\operatorname{E}(X^n))$.
        /// </summary>
        /// <param name="n">The order of the moment.</param>
        /// <returns>The natural logarithm of the $n$-th moment of this distribution.</returns>
        public abstract double LogMoment(int n);

        /// <summary>
        /// Randomly sample an element from the support of this distribution.
        /// </summary>
        /// <param name="random">A random object.</param>
        /// <returns>A random element sampled from this distribution.</returns>
        public abstract T Sample(Random random);
    }
}
