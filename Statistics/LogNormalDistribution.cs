﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LinearNet.Functions;
using LinearNet.Global;
using LinearNet.Structs;

namespace LinearNet.Statistics
{
    /// <summary>
    /// Implements the log-normal distribution $\operatorname{Lognormal}(\mu, \sigma^2)$, whose density function is given by 
    /// $$f(x) = \frac{1}{x\sigma\sqrt{2\pi}} \exp \Big(-\frac{(\ln(x)-\mu)^2}{2\sigma^2}\Big), \quad -\infty < x < \infty$$
    /// </summary>
    /// <cat>numerics</cat>
    public class LogNormalDistribution : IContinuousUnivariateDistribution
    {
        private double _mu, _sigma, _sigmaSqrd;

        public override double Mean => Math.Exp(_mu + 0.5 * _sigmaSqrd);
        public override double Median => Math.Exp(_mu);
        public override double Mode => Math.Exp(_mu - _sigmaSqrd);
        public override double Variance
        {
            get
            {
                return (Math.Exp(_sigmaSqrd) - 1.0) * Math.Exp(2.0 * _mu + _sigmaSqrd);
            }
        }
        public override double Skewness
        {
            get
            {
                double exp_var = Math.Exp(_sigma * _sigma);
                return (exp_var + 2.0) * Math.Sqrt(exp_var - 1.0);
            }
        }
        public override double Kurtosis
        {
            get
            {
                return Math.Exp(4.0 * _sigmaSqrd)
                    + 2.0 * Math.Exp(3.0 * _sigmaSqrd)
                    + 3.0 * Math.Exp(2.0 * _sigmaSqrd)
                    - 3.0;
            }
        }
        public override double Entropy
        {
            get
            {
                return Util.Log2(_sigma * Math.Exp(_mu + 0.5) * Constants.Sqrt2Pi);
            }
        }

        public override DenseMatrix<double> FisherInformationMatrix
        {
            get
            {
                double one_over_sigma_sqrd = 1.0 / _sigmaSqrd;
                return new double[,]
                {
                    { one_over_sigma_sqrd,  0.0 },
                    { 0.0,                  0.5 * one_over_sigma_sqrd * one_over_sigma_sqrd }
                };
            }
        }

        /// <summary>
        /// Initializes a log-normal distribution with the parameters $\mu$ and $\sigma$.
        /// </summary>
        /// <param name="mu">The parameter $\mu$.</param>
        /// <param name="sigma">The parameter $\sigma$.</param>
        public LogNormalDistribution(double mu, double sigma)
        {
            if (double.IsNaN(mu))
            {
                throw new ArgumentException(nameof(mu), ExceptionMessages.NaNNotAllowed);
            }
            if (double.IsNaN(sigma))
            {
                throw new ArgumentException(nameof(mu), ExceptionMessages.NaNNotAllowed);
            }
            if (sigma < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(sigma), ExceptionMessages.NegativeNotAllowed);
            }

            _mu = mu;
            _sigmaSqrd = sigma * sigma;
            _sigma = sigma;
        }
        public override double CDF(double x)
        {
            if (double.IsNaN(x))
            {
                return double.NaN;
            }
            if (x <= 0)
            {
                return 0.0;
            }
            if (double.IsPositiveInfinity(x))
            {
                return 1.0;
            }

            return 0.5 + 0.5 * MathFunctions.Erf((Math.Log(x) - _mu) / (Constants.Sqrt2 * _sigma));
        }

        public override double InverseCDF(double x)
        {
            throw new NotImplementedException();
        }

        public override double LogCDF(double x)
        {
            double cdf = CDF(x);
            if (double.IsNaN(cdf))
            {
                return double.NaN;
            }
            if (cdf == 0.0)
            {
                return double.NegativeInfinity;
            }
            return Math.Log(CDF(x));
        }

        public override double LogPDF(double x)
        {
            double pdf = PDF(x);
            if (double.IsNaN(pdf))
            {
                return double.NaN;
            }
            if (pdf == 0.0)
            {
                return double.NegativeInfinity;
            }
            return Math.Log(PDF(x));
        }

        public override double P(double x)
        {
            return 0.0;
        }

        public override double PDF(double x)
        {
            if (x < 0.0)
            {
                return 0.0;
            }
            if (double.IsNaN(x))
            {
                return double.NaN;
            }

            double s = (Math.Log(x) - _mu) / _sigma;
            return Math.Exp(-0.5 * s * s) / (x * _sigma * Constants.Sqrt2Pi);
        }

        public override double KullbackLeiblerDivergence(IUnivariateDistribution<double> distribution)
        {
            throw new NotImplementedException();
        }

        public override double Moment(int n)
        {
            return Math.Exp(LogMoment(n));
        }

        // E[X^n] = exp(n * mu + n^2 sigma^2 / 2)
        public override double LogMoment(int n)
        {
            if (n <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(n), ExceptionMessages.NonPositiveNotAllowed);
            }

            return n * _mu + 0.5 * n * n * _sigmaSqrd;
        }
    }
}
