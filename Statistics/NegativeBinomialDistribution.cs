﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LinearNet.Functions;
using LinearNet.Global;
using LinearNet.Structs;

namespace LinearNet.Statistics
{
    /// <summary>
    /// Implements the negative binomial distribution $\operatorname{NB}(r, p)$, whose probability mass function is given by:
    /// $$\operatorname{P}(X = k) = \dbinom{k + r - 1}{k}(1 - p)^rp^k,\quad k = 0, 1, 2, ...$$
    /// </summary>
    /// <cat>numerics</cat>
    public class NegativeBinomialDistribution : IDiscreteUnivariateDistribution
    {
        private readonly int _r;
        private readonly double _p, _q;

        public override double Mean => _p * _r / _q;
        public override double Median => throw new NotImplementedException();
        public override double Mode
        {
            get
            {
                if (_r > 1)
                {
                    return Math.Floor(_p * (_r - 1.0) / _q);
                }
                return 0.0;
            }
        }
        public override double Variance => _p * _r / (_q * _q);
        public override double Skewness => (1.0 + _p) / Math.Sqrt(_p * _r);
        public override double Kurtosis => 6.0 / _r + _q * _q / (_p * _r) + 3.0;
        public override double Entropy => throw new NotImplementedException();
        public override DenseMatrix<double> FisherInformationMatrix => throw new NotImplementedException();

        /// <summary>
        /// Initializes a negative binomial distribution using the parameters $r$ and $p$.
        /// </summary>
        /// <param name="r">The number of failed trials, $r > 0$.</param>
        /// <param name="p">The probability of success of each trial, $p \in [0, 1]$.</param>
        public NegativeBinomialDistribution(int r, double p)
        {
            if (r <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(r), ExceptionMessages.NonPositiveNotAllowed);
            }
            if (!(0 < p && p < 1))
            {
                throw new ArgumentOutOfRangeException(nameof(p), ExceptionMessages.NotBetween0And1);
            }

            _r = r;
            _p = p;
            _q = 1.0 - p;
        }
        public override double CDF(int x)
        {
            throw new NotImplementedException();
        }

        public override int InverseCDF(double x)
        {
            throw new NotImplementedException();
        }

        public override double KullbackLeiblerDivergence(IUnivariateDistribution<int> distribution)
        {
            throw new NotImplementedException();
        }

        public override double LogCDF(int x)
        {
            throw new NotImplementedException();
        }

        public override double LogMoment(int n)
        {
            throw new NotImplementedException();
        }

        public override double LogPDF(int k)
        {
            if (k < 0)
            {
                return double.NegativeInfinity;
            }
            return MathFunctions.LogCombinations(k + _r - 1, k) + _r * Math.Log(_q) + k * Math.Log(_p);
        }

        public override double Moment(int n)
        {
            throw new NotImplementedException();
        }

        public override double P(int x)
        {
            return PDF(x);
        }

        public override double PDF(int x)
        {
            if (x < 0)
            {
                return 0.0;
            }
            return Math.Exp(LogPDF(x));
        }

        public override int Sample(Random random)
        {
            throw new NotImplementedException();
        }
    }
}
