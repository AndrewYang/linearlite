﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LinearNet.Global;
using LinearNet.Structs;

namespace LinearNet.Statistics
{
    /// <summary>
    /// Implements a continuous uniform distribution $\mathcal{U}[a, b]$.
    /// </summary>
    /// <cat>numerics</cat>
    public class UniformDistribution : IContinuousUnivariateDistribution
    {
        private double _a, _b, _b_minus_a;

        public override double Mean => 0.5 * (_a + _b);
        public override double Median => 0.5 * (_a + _b);

        // The mode is actually any value in [_a, _b]
        public override double Mode => 0.5 * (_a + _b);
        public override double Variance => 0.5 * _b_minus_a * _b_minus_a;
        public override double Skewness => 0.0;
        public override double Kurtosis => -1.2 + 3.0;
        public override double Entropy => Math.Log(_b_minus_a);

        public override DenseMatrix<double> FisherInformationMatrix => throw new NotImplementedException();

        /// <summary>
        /// Create the uniform distribution [a, b]
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        public UniformDistribution(double a, double b)
        {
            if (double.IsInfinity(a))
            {
                throw new ArgumentOutOfRangeException(nameof(a), ExceptionMessages.InfinityNotAllowed);
            }
            if (double.IsInfinity(b))
            {
                throw new ArgumentOutOfRangeException(nameof(b), ExceptionMessages.InfinityNotAllowed);
            }
            if (double.IsNaN(a))
            {
                throw new ArgumentOutOfRangeException(nameof(a), ExceptionMessages.NaNNotAllowed);
            }
            if (double.IsNaN(b))
            {
                throw new ArgumentOutOfRangeException(nameof(b), ExceptionMessages.NaNNotAllowed);
            }
            if (b <= a)
            {
                throw new ArgumentOutOfRangeException("b is not greater than a");
            }

            _a = a;
            _b = b;
            _b_minus_a = b - a;
        }
        public override double CDF(double x)
        {
            if (double.IsNaN(x))
            {
                return double.NaN;
            }

            // these 2 cases take care of infinities
            if (x < _a)
            {
                return 0.0;
            }
            if (x > _b)
            {
                return 1.0;
            }

            return (x - _a) / _b_minus_a;
        }

        public override double InverseCDF(double x)
        {
            throw new NotImplementedException();
        }

        public override double KullbackLeiblerDivergence(IUnivariateDistribution<double> distribution)
        {
            throw new NotImplementedException();
        }

        public override double LogCDF(double x)
        {
            if (double.IsNaN(x))
            {
                return double.NaN;
            }

            if (x <= _a)
            {
                return double.NegativeInfinity;
            }
            if (x > _b)
            {
                return 0.0;
            }

            return Math.Log(x - _a) - Math.Log(_b_minus_a);
        }

        public override double LogPDF(double x)
        {
            if (double.IsNaN(x))
            {
                return double.NaN;
            }
            if (x < _a || x > _b)
            {
                return double.NegativeInfinity;
            }
            return -Math.Log(_b_minus_a);
        }

        public override double P(double x)
        {
            return 0.0;
        }

        public override double PDF(double x)
        {
            if (double.IsNaN(x))
            {
                return double.NaN;
            }
            if (x < _a || x > _b)
            {
                return 0.0;
            }
            return 1.0 / _b_minus_a;
        }

        public override double Moment(int n)
        {
            return Math.Exp(LogMoment(n));
        }

        // sum(a^k * b^(n - k), k in [0, n]) / (n + 1)
        public override double LogMoment(int n)
        {
            if (n < 1)
            {
                throw new ArgumentOutOfRangeException(nameof(n), ExceptionMessages.NonPositiveNotAllowed);
            }

            double ln = 0.0, ln_a = Math.Log(_a), ln_b = Math.Log(_b);
            for (int k = 0; k < n; ++k)
            {
                ln += k * ln_a + (n - k) * ln_b;
            }
            return ln - Math.Log(n + 1);
        }
    }
}
