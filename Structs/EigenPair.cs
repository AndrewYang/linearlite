﻿using LinearNet.Structs;
using System;

namespace LinearNet
{
    public class EigenPair
    {
        public Complex EigenValue { get; set; }
        public Complex[] EigenVector { get; set; }

        public EigenPair(Complex eigenvalue, Complex[] eigenvector)
        {
            EigenValue = eigenvalue;
            EigenVector = eigenvector;
        }
    }
}
