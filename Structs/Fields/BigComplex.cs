﻿using LinearNet.Global;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Structs.Fields
{
    /// <summary>
    /// <p>Implements a arbitrary-precision complex number.</p>
    /// <p>The real and imaginary parts of a <txt>BigComplex</txt> are stored as <txt>BigDecimal</txt>s.</p>
    /// </summary>
    /// <cat>numerics</cat>
    public partial struct BigComplex : IField<BigComplex>, IComplex<BigDecimal>
    {
        /// <summary>
        /// The real part of the complex number, Re(z), represented as a BigDecimal.
        /// </summary>
        public BigDecimal Real { get; private set; }

        /// <summary>
        /// The imaginary part of the complex number, Im(z), represented as a BigDecimal.
        /// </summary>
        public BigDecimal Imaginary { get; private set; }

        /// <implements>Ring<T></implements>
        public BigComplex MultiplicativeIdentity => One;
        
        /// <implements>AdditiveGroup<T></implements>
        public BigComplex AdditiveIdentity => Zero;

        /// <summary>
        /// Returns whether the complex number is zero ($0 + 0i$).
        /// </summary>
        public bool IsZero { get { return Real.IsZero && Imaginary.IsZero; } }

        /// <summary>
        /// Returns whether this complex number is equal to one ($1 + 0i$).
        /// </summary>
        public bool IsOne { get { return Real.IsOne && Imaginary.IsZero; } }

        /// <summary>
        /// Returns whether this complex number is equal to the imaginary unit, $i$.
        /// </summary>
        public bool IsI { get { return Real.IsZero && Imaginary.IsOne; } }


        public BigInteger Characteristic => BigInteger.Zero;

        /// <summary>
        /// The BigComplex struct representing zero, ($0 + 0i$) or (0, 0)
        /// </summary>
        public static readonly BigComplex Zero = new BigComplex(BigDecimal.Zero, BigDecimal.Zero);
        /// <summary>
        /// The BigComplex struct representing one, ($1 + 0i$) or (1, 0)
        /// </summary>
        public static readonly BigComplex One = new BigComplex(BigDecimal.One, BigDecimal.Zero);
        /// <summary>
        /// The BigComplex struct representing the imaginary unit $i$, ($0 + i$) or (0, 1)
        /// </summary>
        public static readonly BigComplex I = new BigComplex(BigDecimal.Zero, BigDecimal.One);

        #region Constructors and Factories
        /// <summary>
        /// Initialize a <txt>BigComplex</txt> using its real and imaginary parts, represented as BigDecimals.
        /// </summary>
        /// <param name="real">The real part of the complex number.</param>
        /// <param name="imaginary">The imaginary part of the complex number.</param>
        public BigComplex(BigDecimal real, BigDecimal imaginary)
        {
            Real = real;
            Imaginary = imaginary;
        }
        /// <summary>
        /// Constructor for a real valued BigComplex struct given its real part, as a BigDecimal.
        /// </summary>
        /// <param name="real">The real part of the complex number.</param>
        public BigComplex(BigDecimal real)
        {
            // Initialize the number of significant figures of imaginary
            // parts to the same as the number of significant figures of 
            // real part. 

            Real = real;
            Imaginary = new BigDecimal(0, real.SignificantFigures);
        }
        /// <summary>
        /// BigComplex constructor from a Complex struct, accurate up to the specified number of 
        /// significant figures. 
        /// </summary>
        /// <param name="z">A Complex struct</param>
        /// <param name="significantFigures">The number of significant figures of the resultant BigComplex struct</param>
        public BigComplex(Complex z, int significantFigures)
        {
            if (significantFigures <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(significantFigures), ExceptionMessages.NonPositiveNotAllowed);
            }

            if (double.IsNaN(z.Real) || double.IsNaN(z.Imaginary))
            {
                throw new ArgumentNullException();
            }

            Real = new BigDecimal(z.Real, significantFigures);
            Imaginary = new BigDecimal(z.Imaginary, significantFigures);
        }
        #endregion

        /// <summary>
        /// Calculates the argument of the complex number expressed in polar form.
        /// </summary>
        /// <returns>A BigDecimal representing the argument of the complex number</returns>
        /// <name>Argument</name>
        public BigDecimal Argument()
        {
            int sigFigs = Math.Max(Real.SignificantFigures, Imaginary.SignificantFigures);
            if (Real == 0.0)
            {
                if (Imaginary == 0.0) return double.NaN;
                if (Imaginary > 0.0)
                {
                    return BigDecimal.Pi(sigFigs) / new BigDecimal(2, sigFigs);
                }
                return -BigDecimal.Pi(sigFigs) / new BigDecimal(2, sigFigs);
            }
            return BigDecimal.Atan2(Imaginary, Real);
        }

        /// <summary>
        /// Calculates the modulus (length) of the complex number
        /// </summary>
        /// <returns>The modulus (length) of the complex number, as a BigDecimal.</returns>
        /// <name>Modulus</name>
        public BigDecimal Modulus()
        {
            return Util.Hypot(Real, Imaginary);
        }

        #region Standard object overrides 
        /// <summary>
        /// Returns whether the complex number is equal to an object.
        /// </summary>
        /// <param name="obj">An object.</param>
        /// <returns>Whether the two objects are equal.</returns>
        /// <name>Equals_Obj</name>
        public override bool Equals(object obj)
        {
            if (obj == null || !(obj is BigComplex))
            {
                return false;
            }
            return Equals((BigComplex)obj);
        }
        /// <summary>
        /// Returns the hash code for this <txt>BigComplex</txt>.
        /// </summary>
        /// <implements>object</implements>
        /// <returns>The hash code.</returns>
        public override int GetHashCode()
        {
            return Real.GetHashCode() ^ Imaginary.GetHashCode();
        }
        /// <summary>
        /// Returns the string representation of this <txt>BigComplex</txt> struct.
        /// </summary>
        /// <implements>object</implements>
        /// <returns>String representation of the struct.</returns>
        public override string ToString()
        {
            return Real.ToString() + " + " + Imaginary.ToString() + " i";
        }
        #endregion

        #region Field methods 
        /// <summary>
        /// Returns the multiplicative inverse 1 / x of a BigComplex x.
        /// </summary>
        /// <implements>Field<T></implements>
        /// <returns>The multiplicative inverse of a BigComplex.</returns>
        public BigComplex MultiplicativeInverse()
        {
            BigDecimal divisor = Real * Real + Imaginary * Imaginary;
            if (divisor.IsZero)
            {
                return new BigComplex(BigDecimal.Zero, BigDecimal.Zero);
            }

            // Division is very expensive - save a step here
            divisor = BigDecimal.One / divisor;
            return new BigComplex(Real * divisor, Imaginary * divisor);
        }
        /// <summary>
        /// Returns the additive inverse, -x, such that x + (-x) = 0 
        /// where 0 is the identity element of the additive group. 
        /// </summary>
        /// <implements>AdditiveGroup<T></implements>
        /// <returns>The additive inverse of a BigComplex.</returns>
        public BigComplex AdditiveInverse()
        {
            return new BigComplex(-Real, -Imaginary);
        }
        /// <summary>
        /// Returns the product of this complex number with another. The number of significant 
        /// figures will be the lessor of the two multiplicands.
        /// </summary>
        /// <implements>Ring<T></implements>
        /// <param name="e">A BigComplex</param>
        /// <returns>The product of this BigComplex and another.</returns>
        public BigComplex Multiply(BigComplex e)
        {
            return new BigComplex(
                Real * e.Real - Imaginary * e.Imaginary,
                Real * e.Imaginary + Imaginary * e.Real);
        }
        /// <summary>
        /// Returns the sum of two complex numbers.
        /// </summary>
        /// <implements>AdditiveGroup<T></implements>
        /// <param name="g"></param>
        /// <returns>The sum</returns>
        public BigComplex Add(BigComplex g)
        {
            return new BigComplex(Real + g.Real, Imaginary + g.Imaginary);
        }
        /// <summary>
        /// Returns whether this BigComplex struct is equal to another.
        /// </summary>
        /// <param name="e">A <txt>BigComplex</txt> struct.</param>
        /// <returns>Returns whether the two BigComplex structs are the same.</returns>
        public bool Equals(BigComplex e)
        {
            return Real == e.Real && Imaginary == e.Imaginary;
        }

        /// <summary>
        /// Returns whether this BigComplex struct approximately equal to another, up to a tolerance 
        /// of <txt>eps</txt>.
        /// <para>
        /// This method returns <txt>true</txt> for two <txt>BigComplex</txt> structs $w, z$ if $|w/z-1| < \epsilon$.
        /// </para>
        /// </summary>
        /// <param name="e">A <txt>BigComplex</txt> struct.</param>
        /// <param name="eps">the degree of tolerance.</param>
        /// <returns>Whether the two complex numbers are equal within tolerance of <txt>eps</txt>.</returns>
        public bool ApproximatelyEquals(BigComplex e, double eps)
        {
            BigDecimal bigEps = new BigDecimal(eps);
            return Util.ApproximatelyEquals(Real, e.Real, bigEps) &&
                Util.ApproximatelyEquals(Imaginary, e.Imaginary, bigEps);
        }
        #endregion

        #region Operator overrides 

        /// <summary>
        /// Adds two complex numbers together.
        /// </summary>
        /// <param name="w">A complex number.</param>
        /// <param name="z">A complex number.</param>
        /// <returns>The sum of two <txt>BigComplex</txt> structs.</returns>
        public static BigComplex operator +(BigComplex w, BigComplex z) => w.Add(z);

        /// <summary>
        /// Subtracts the second complex number from the first.
        /// </summary>
        /// <param name="w">A complex number.</param>
        /// <param name="z">A complex number.</param>
        /// <returns>The difference between the two complex numbers.</returns>
        public static BigComplex operator -(BigComplex w, BigComplex z) => w.Subtract(z);

        /// <summary>
        /// Calculates the product of two complex numbers.
        /// </summary>
        /// <param name="w">A complex number.</param>
        /// <param name="z">A complex number.</param>
        /// <returns>The product of the two <txt>BigComplex</txt> numbers.</returns>
        public static BigComplex operator *(BigComplex w, BigComplex z) => w.Multiply(z);

        /// <summary>
        /// Divides the first complex number by the second complex number.
        /// </summary>
        /// <param name="w">The divisee</param>
        /// <param name="z">The divisor</param>
        /// <returns>The quotient of two complex numbers.</returns>
        public static BigComplex operator /(BigComplex w, BigComplex z) => w.Divide(z);

        /// <summary>
        /// Returns the opposite of a complex number.
        /// </summary>
        /// <param name="z">A complex number</param>
        /// <returns>The opposite of a complex number.</returns>
        public static BigComplex operator -(BigComplex z) => z.AdditiveInverse();

        /// <summary>
        /// Returns whether two complex numbers are (exactly) equal.
        /// </summary>
        /// <param name="w">A complex number.</param>
        /// <param name="z">A complex number.</param>
        /// <returns>Whether two complex numbers are equal.</returns>
        public static bool operator ==(BigComplex w, BigComplex z) => w.Equals(z);

        /// <summary>
        /// Returns whether the two complex numbers are not exactly equal.
        /// </summary>
        /// <param name="w">A complex number.</param>
        /// <param name="z">A complex number.</param>
        /// <returns>Whether two complex numbers are not exactly equal.</returns>
        public static bool operator !=(BigComplex w, BigComplex z) => !w.Equals(z);
        #endregion

        #region Cast overrides 
        /// <summary>
        /// Implicitly cast a <txt>BigDecimal</txt> representing a real number into a <txt>BigComplex</txt>
        /// </summary>
        /// <param name="x">A real number.</param>
        public static implicit operator BigComplex(BigDecimal x) => new BigComplex(x);
        #endregion
    }
}
