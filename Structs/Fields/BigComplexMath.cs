﻿using LinearNet.Global;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Structs.Fields
{
    /// <cat>numerics</cat>
    public partial struct BigComplex
    {
        /// <summary>
        /// <para>
        /// Convert a modulus and argument into a <txt>BigComplex</txt> the specified number 
        /// of significant figures.
        /// </para>
        /// <para>
        /// If the number of significant figures is negative, then the returned BigComplex 
        /// struct will have the same number of significant figures as the lesser of 
        /// (modulus, argument).
        /// </para>
        /// </summary>
        /// <param name="modulus">The modulus (length) of a complex number.</param>
        /// <param name="argument">The argument of a complex number.</param>
        /// <param name="sigFigs">The number of significant figures of the resulting complex number.</param>
        /// <returns>A complex number calculated from its polar form.</returns>
        public static BigComplex FromPolar(BigDecimal modulus, BigDecimal argument, int sigFigs = -1)
        {
            if (sigFigs < 0)
            {
                sigFigs = Math.Min(modulus.SignificantFigures, argument.SignificantFigures);
            }

            if (modulus.IsNegative)
            {
                throw new ArgumentOutOfRangeException(nameof(modulus), ExceptionMessages.NegativeNotAllowed);
            }

            if (modulus.IsZero)
            {
                BigDecimal zero = new BigDecimal(0, sigFigs);
                return new BigComplex(zero, zero);
            }

            BigDecimal re = modulus.Multiply(BigDecimal.Cos(argument, sigFigs), sigFigs);
            BigDecimal im = modulus.Multiply(BigDecimal.Sin(argument, sigFigs), sigFigs);
            return new BigComplex(re, im);
        }

        /// <summary>
        /// Calculate $\exp(z)$ for a complex number $z$.
        /// </summary>
        /// <param name="z">A complex number.</param>
        /// <param name="sigFigs">
        /// <b>Optional</b>, defaults to -1. <br/>
        /// The number of significant figures of the calculated exponential. If negative, it will default to the 
        /// same number of significant figures as <txt>z</txt>.
        /// </param>
        /// <returns>The exponential function evaluated at that complex number.</returns>
        public static BigComplex Exp(BigComplex z, int sigFigs = -1)
        {
            if (sigFigs < 0)
            {
                sigFigs = Math.Min(z.Real.SignificantFigures, z.Imaginary.SignificantFigures);
            }

            return FromPolar(BigDecimal.Exp(z.Real, sigFigs), z.Imaginary, sigFigs);
        }

        /// <summary>
        /// Calculate the square root of a complex number $z$.
        /// </summary>
        /// <param name="z">A complex number.</param>
        /// <param name="sigFigs">
        /// The number of significant figures of the result. If negative, it will default to the 
        /// same number of significant figures as <txt>z</txt>.
        /// </param>
        /// <returns>$\sqrt{z}$</returns>
        public static BigComplex Sqrt(BigComplex z, int sigFigs = -1)
        {
            if (sigFigs < 0)
            {
                sigFigs = Math.Min(z.Real.SignificantFigures, z.Imaginary.SignificantFigures);
            }

            // Use sqrt(z) instead of pow(0.5) because non-integer exponentiation is expensive
            // for BigDecimal
            return FromPolar(
                BigDecimal.Sqrt(z.Modulus(), sigFigs), 
                z.Argument().Multiply(2, sigFigs), 
                sigFigs);
        }

        /// <summary>
        /// Calculate a complex number raised to a real power, $z^w$, where $z$ is complex and $w$ is real.
        /// </summary>
        /// <param name="z">A complex number.</param>
        /// <param name="power">A real number.</param>
        /// <returns>$z^w$</returns>
        public static BigComplex Pow(BigComplex z, BigDecimal power, int sigFigs = -1)
        {
            if (sigFigs < 0)
            {
                sigFigs = Util.Min(z.Real.SignificantFigures, z.Imaginary.SignificantFigures, power.SignificantFigures);
            }

            if (power.IsZero)
            {
                return new BigDecimal(1, sigFigs);
            }

            if (z.IsZero)
            {
                return new BigDecimal(0, sigFigs);
            }

            return FromPolar(
                BigDecimal.Pow(z.Modulus(), power, sigFigs), 
                z.Argument().Multiply(power, sigFigs),
                sigFigs);
        }

        /// <summary>
        /// Calculate a complex number raised to the power of another complex number, $z^w$, where both
        /// z and w are complex.
        /// </summary>
        /// <param name="z">A complex number.</param>
        /// <param name="w">A complex number.</param>
        /// <param name="sigFigs">The number of significant figures to calculate $z^w$</param>
        /// <returns>$z^w$</returns>
        public static BigComplex Pow(BigComplex z, BigComplex w, int sigFigs = -1)
        {
            if (sigFigs < 0)
            {
                sigFigs = Util.Min(z.Real.SignificantFigures,
                    z.Imaginary.SignificantFigures,
                    w.Real.SignificantFigures,
                    w.Imaginary.SignificantFigures);
            }

            if (w.IsZero)
            {
                return new BigDecimal(1, sigFigs);
            }
            if (z.IsZero)
            {
                return new BigDecimal(0, sigFigs);
            }

            BigDecimal w_re = w.Real, w_im = w.Imaginary,
                r = z.Modulus(), theta = z.Argument();

            return FromPolar(
                BigDecimal.Pow(r, w_re, sigFigs).Multiply(BigDecimal.Exp(-w_im.Multiply(theta, sigFigs), sigFigs), sigFigs),
                w_re.Multiply(theta, sigFigs) + w_im.Multiply(BigDecimal.Log(r, sigFigs), sigFigs),
                sigFigs);
        }

        /// <summary>
        /// Calculate the natural logarithm of a complex number z.
        /// </summary>
        /// <param name="z">A complex number.</param>
        /// <param name="sigFigs">The number of significant figures of the result.</param>
        /// <returns>The natural logarithm, $\ln(z)$</returns>
        public static BigComplex Log(BigComplex z, int sigFigs = -1)
        {
            BigDecimal mod = z.Modulus();
            BigDecimal arg = z.Argument();

            if (mod.IsZero)
            {
                BigDecimal zero = new BigDecimal(0, sigFigs);
                return new BigComplex(zero, zero);
            }

            return new BigComplex(BigDecimal.Log(mod, sigFigs), arg);
        }

        /// <summary>
        /// Calculates the sine of a complex value z, correct to the specified number of significant figures.
        /// </summary>
        /// <param name="z">A complex number.</param>
        /// <param name="sigFigs">The number of significant figures.</param>
        /// <returns>The sine function evaluated at the complex number, $\sin(z)$.</returns>
        public static BigComplex Sin(BigComplex z, int sigFigs = -1)
        {
            if (sigFigs < 0)
            {
                sigFigs = Math.Min(z.Real.SignificantFigures, z.Imaginary.SignificantFigures);
            }

            BigDecimal re = BigDecimal.Sin(z.Real, sigFigs) * BigDecimal.Cosh(z.Imaginary, sigFigs);
            BigDecimal im = BigDecimal.Cos(z.Real, sigFigs) * BigDecimal.Sinh(z.Imaginary, sigFigs);
            return new BigComplex(re, im);
        }

        /// <summary>
        /// Calculates the cosine of a complex value $z$, correct to the specified number of significant figures.
        /// </summary>
        /// <param name="z">A complex number.</param>
        /// <param name="sigFigs">The number of significant figures.</param>
        /// <returns>The cosine function evaluated at the complex number, $\cos(z)$.</returns>
        public static BigComplex Cos(BigComplex z, int sigFigs = -1)
        {
            if (sigFigs < 0)
            {
                sigFigs = Math.Min(z.Real.SignificantFigures, z.Imaginary.SignificantFigures);
            }

            BigDecimal re = BigDecimal.Cos(z.Real, sigFigs) * BigDecimal.Cosh(z.Imaginary, sigFigs);
            BigDecimal im = -BigDecimal.Sin(z.Real, sigFigs) * BigDecimal.Sinh(z.Imaginary, sigFigs);
            return new BigComplex(re, im);
        }
        /// <summary>
        /// Calculates the tangent of a complex value z, correct to the specified number of significant figures.
        /// </summary>
        /// <param name="z">A complex number.</param>
        /// <param name="sigFigs">The number of significant figures.</param>
        /// <returns>The tangent function evaluated at the complex number, $\tan(z)$</returns>
        public static BigComplex Tan(BigComplex z, int sigFigs = -1)
        {
            if (sigFigs < 0)
            {
                sigFigs = Math.Min(z.Real.SignificantFigures, z.Imaginary.SignificantFigures);
            }

            BigDecimal 
                a2 = z.Real.Multiply(new BigDecimal(2, sigFigs)),
                b2 = z.Imaginary.Multiply(new BigDecimal(2, sigFigs)),
                den = BigDecimal.Cos(a2, sigFigs) + BigDecimal.Cosh(b2, sigFigs);

            return new BigComplex(
                BigDecimal.Sin(a2, sigFigs) / den,
                BigDecimal.Sinh(b2, sigFigs) / den);
        }

        /// <summary>
        /// Calculates the inverse sine function of a complex value z, correct to the specified number of significant figures.
        /// </summary>
        /// <param name="z">A complex number.</param>
        /// <param name="sigFigs">The number of significant figures</param>
        /// <returns>The inverse sine function evaluated at the complex number, $\sin^{-1}(z)$.</returns>
        public static BigComplex Asin(BigComplex z, int sigFigs = -1)
        {
            if (sigFigs < 0)
            {
                sigFigs = Math.Min(z.Real.SignificantFigures, z.Imaginary.SignificantFigures);
            }

            BigDecimal z_re = z.Real, z_im = z.Imaginary;
            z_re.SignificantFigures = sigFigs;
            z_im.SignificantFigures = sigFigs;

            BigDecimal one = new BigDecimal(1, sigFigs);
            BigDecimal minus_two = new BigDecimal(-2, sigFigs);

            BigComplex iz = new BigComplex(-z_im, z_re);
            BigComplex one_minus_z_sqrd = new BigComplex(
                one - z_re * z_re + z_im * z_im,
                minus_two * z_re * z_im);

            BigComplex iw = Log(iz + Sqrt(one_minus_z_sqrd, sigFigs), sigFigs);
            return new BigComplex(iw.Imaginary, -iw.Real);
        }
    }
}
