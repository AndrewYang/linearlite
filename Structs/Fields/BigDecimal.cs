﻿using LinearNet.Global;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Structs.Fields
{
    /// <summary>
    /// Implements an arbitrary-precision decimal object.
    /// The value of this struct is equal to Mantissa $\times$ 10<sup>Exponent</sup>.
    /// 
    /// <para>
    /// By default, division and multiplicative inversion will occur with:
    /// </para>
    /// <ul>
    /// <li>1,000 significant figures (s.f.) precision if neither divisor nor divisee have a precision greater than 1,000 s.f.</li>
    /// <li>the maximum s.f. precision of the divisor and divisee, if either have a precision greater than 1,000 s.f.</li>
    /// </ul>
    /// </summary>
    /// <cat>numerics</cat>
    public partial struct BigDecimal : IField<BigDecimal>, IComparable<BigDecimal>, IEquatable<BigDecimal>
    {
        /// <summary>
        /// The mantissa of the decimal object. The value of the <txt>BigDecimal</txt> is taken to equal Mantissa $\times$ 10<sup>Exponent</sup>
        /// </summary>
        public BigInteger Mantissa { get; private set; }
        /// <summary>
        /// The exponent of the decimal object. The value of the <txt>BigDecimal</txt> is taken to equal Mantissa $\times$ 10<sup>Exponent</sup>.
        /// </summary>
        public int Exponent { get; private set; }

        /// <summary>
        /// <para>
        /// The number of significant figures of this <txt>BigDecimal</txt> struct. Any extra significant figures in <txt>Mantissa</txt> are assumed
        /// to be inaccurate and hence ignored. 
        /// </para>
        /// <para>
        /// The number of significant figures are preserved through all arithmetic operations (e.g. addition, 
        /// multiplication, exponentiation), unless otherwise specified or if the operation involves multiple <txt>BigDecimal</txt> structs with 
        /// different significant figures. In such cases typically the lowest number of significant figures amongst the operands is used.
        /// </para>
        /// </summary>
        public int SignificantFigures { get; internal set; }

        /// <summary>
        /// Returns the sign of the decimal object (-1 for negative numbers, 0 for zero and 1 for positive numbers).
        /// </summary>
        public int Sign { get { return Mantissa.Sign; } }

        /// <summary>
        /// Returns whether this <txt>BigDecimal</txt> is equal to zero.
        /// </summary>
        public bool IsZero { get { return Mantissa.IsZero; } }

        /// <summary>
        /// Returns whether this <txt>BigDecimal</txt> is equal to one.
        /// </summary>
        public bool IsOne { get { return Equals(One); } }

        /// <summary>
        /// Returns whether this <txt>BigDecimal</txt> object is negative.
        /// </summary>
        public bool IsNegative { get { return Sign < 0; } }

        /// <summary>
        /// Returns whether this <txt>BigDecimal</txt> object is positive.
        /// </summary>
        public bool IsPositive { get { return Sign > 0; } }

        /// <summary>
        /// TODO: add edge test cases for this
        /// </summary>
        public bool IsInteger { get { return DigitCount(Mantissa) + Exponent >= SignificantFigures; } }

        private static readonly BigInteger _ten = new BigInteger(10);

        /// <summary>
        /// The <txt>BigDecimal</txt> representing zero.
        /// </summary>
        public static readonly BigDecimal Zero = new BigDecimal(0);

        /// <summary>
        /// The <txt>BigDecimal</txt> representing one.
        /// </summary>
        public static readonly BigDecimal One = new BigDecimal(1);

        /// <summary>
        /// $\pi$ correct to 1,000 decimal places.
        /// </summary>
        public static readonly BigDecimal PI = new BigDecimal(
            new BigInteger(new byte[]
            {
                5, 142, 202, 95, 15, 141, 176, 226, 93, 215, 174, 154, 196, 21, 168, 190, 83, 124, 14, 96,
                98, 154, 52, 125, 42, 231, 147, 248, 96, 159, 21, 31, 79, 137, 82, 75, 42, 49, 234, 195,
                214, 105, 169, 100, 249, 33, 222, 102, 126, 122, 200, 66, 40, 124, 180, 110, 230, 235, 74, 34,
                50, 4, 165, 99, 184, 133, 153, 181, 62, 217, 190, 255, 40, 14, 108, 139, 43, 61, 248, 248,
                139, 146, 214, 32, 4, 62, 33, 11, 203, 240, 116, 62, 88, 64, 197, 42, 24, 65, 125, 101,
                159, 31, 64, 25, 41, 241, 112, 217, 137, 38, 81, 186, 148, 138, 54, 227, 154, 249, 111, 144,
                56, 223, 20, 180, 198, 188, 141, 13, 41, 32, 53, 235, 205, 99, 177, 95, 244, 43, 70, 17,
                62, 92, 236, 218, 160, 138, 15, 66, 135, 255, 30, 160, 245, 100, 62, 162, 84, 39, 107, 21,
                127, 219, 197, 233, 250, 254, 112, 129, 36, 118, 98, 28, 118, 204, 97, 155, 65, 214, 17, 71,
                219, 124, 58, 192, 7, 167, 188, 195, 26, 244, 253, 125, 147, 0, 82, 34, 207, 140, 244, 14,
                1, 129, 132, 15, 171, 174, 116, 38, 249, 220, 185, 92, 197, 83, 170, 175, 84, 10, 34, 208,
                173, 108, 6, 188, 98, 138, 3, 130, 246, 10, 215, 202, 48, 206, 169, 155, 242, 241, 200, 168,
                248, 229, 243, 222, 57, 203, 220, 30, 30, 151, 30, 167, 1, 20, 95, 221, 118, 101, 34, 151,
                157, 107, 221, 52, 130, 121, 136, 220, 48, 100, 133, 250, 236, 236, 65, 104, 45, 38, 0, 157,
                233, 124, 98, 221, 149, 89, 235, 120, 248, 74, 68, 225, 245, 225, 163, 22, 247, 170, 199, 148,
                29, 132, 49, 166, 191, 89, 198, 71, 81, 111, 170, 83, 67, 202, 37, 95, 111, 50, 161, 150,
                186, 4, 83, 43, 117, 121, 61, 229, 176, 88, 35, 89, 191, 75, 36, 229, 175, 190, 201, 200,
                18, 192, 164, 40, 125, 226, 26, 238, 87, 210, 15, 91, 151, 194, 141, 63, 175, 116, 150, 17,
                10, 115, 22, 55, 49, 134, 1, 36, 247, 232, 76, 50, 139, 253, 94, 215, 124, 86, 189, 47,
                47, 37, 104, 65, 44, 174, 196, 72, 240, 145, 105, 135, 95, 140, 106, 105, 154, 58, 14, 100,
                220, 241, 88, 191, 242, 103, 217, 111, 119, 187, 85, 184, 96, 149, 244, 11
            }), -1000, 1001);

        /// <summary>
        /// Euler's constant $e$ correct to 1,000 decimal places.
        /// </summary>
        public static readonly BigDecimal E = new BigDecimal(
            new BigInteger(new byte[]
            {
                18,245,11,214,109,206,116,30,227,4,98,224,100,129,153,174,225,141,163,52,
                59,79,85,47,149,225,122,43,139,200,17,231,204,59,125,225,194,161,123,77,
                47,95,126,136,75,44,98,172,155,241,41,229,47,155,66,215,181,255,7,75,
                58,134,102,171,225,226,7,203,238,202,37,245,15,103,204,168,106,229,102,254,
                103,240,202,224,69,198,8,233,76,34,119,201,5,186,155,251,20,136,78,172,
                81,110,52,115,173,64,191,3,35,226,82,2,232,168,225,232,100,237,40,96,
                4,129,163,105,44,132,205,201,253,170,70,225,79,3,82,193,233,215,200,232,
                152,9,88,107,62,116,95,51,131,166,137,218,183,193,81,68,229,238,111,119,
                132,217,174,85,45,136,33,57,81,80,22,109,148,133,220,61,35,198,13,203,
                62,247,78,135,158,128,183,147,207,218,155,248,101,220,210,149,243,234,237,80,
                105,181,5,232,29,179,149,215,198,58,240,24,211,12,58,180,4,155,184,157,
                98,128,116,173,123,36,239,179,132,181,173,28,186,200,180,216,71,105,0,132,
                104,174,180,182,3,143,51,22,43,174,239,65,218,185,85,241,63,90,157,158,
                83,60,64,123,101,85,97,22,219,87,248,253,129,155,213,180,34,173,154,156,
                65,160,224,0,246,255,246,48,15,136,212,19,161,86,63,44,245,101,152,141,
                158,220,64,162,174,158,150,12,221,2,132,172,40,104,222,58,255,227,200,191,
                242,233,196,47,34,193,242,3,100,67,177,135,100,213,184,216,185,177,117,193,
                224,235,218,230,219,134,239,191,185,216,184,135,99,154,114,172,208,90,164,74,
                140,193,23,169,54,14,95,173,101,99,200,71,106,59,114,197,104,235,175,102,
                59,161,30,127,140,91,6,92,92,202,227,105,163,159,55,50,105,184,75,47,
                127,81,87,85,45,203,17,94,89,224,103,145,44,48,88,10
            }), -1000, 1001);

        /// <summary>
        /// The multiplicative identity of the ring of <txt>BigDecimal</txt> (under addition, multiplication) excluding zero.
        /// </summary>
        /// <implements>Ring<T></implements>
        public BigDecimal MultiplicativeIdentity => One;

        /// <summary>
        /// The additive identity of the group of <txt>BigDecimal</txt> (under addition).
        /// </summary>
        /// <implements>AdditiveGroup<T></implements>
        public BigDecimal AdditiveIdentity => Zero;

        public BigInteger Characteristic => BigInteger.Zero;

        /// <summary>
        /// Initialize a <txt>BigDecimal</txt> object given its mantissa, exponent and the number of significant figures that it
        /// is accurate to. 
        /// <para>
        /// The value of the <txt>BigDecimal</txt> is interpreted as Mantissa $\times$ 10<sup>Exponent</sup>.
        /// </para>
        /// </summary>
        /// <param name="mantissa">The mantissa of the decimal object.</param>
        /// <param name="exponent">The exponent of the decimal object.</param>
        /// <param name="sigFigs">The number of significant figures of the decimal object.</param>
        public BigDecimal(BigInteger mantissa, int exponent, int sigFigs)
        {
            Mantissa = mantissa;
            Exponent = exponent;
            SignificantFigures = sigFigs;

            Simplify();
        }

        /// <summary>
        /// Initialize a <txt>BigDecimal</txt> object with a <txt>BigInteger</txt> and the number of significant figures.
        /// </summary>
        /// <param name="value">The value of the decimal.</param>
        /// <param name="sigFigs">The number of significant figures.</param>
        public BigDecimal(BigInteger value, int sigFigs = 1000) : this(value, 0, sigFigs) { }

        /// <summary>
        /// Initialize a <txt>BigDecimal</txt> object with a 32-bit integer, and the number of significant figures.
        /// </summary>
        /// <param name="value">The value of the <txt>BigDecimal</txt>.</param>
        /// <param name="sigFigs">The number of significant figures of the <txt>BigDecimal</txt></param>
        public BigDecimal(int value, int sigFigs = 1000) : this(value, 0, sigFigs) { }
        /// <summary>
        /// Initialize a <txt>BigDecimal</txt> struct with a 64-bit integer and the number of significant figures.
        /// </summary>
        /// <param name="value">The value of the <txt>BigDecimal</txt>.</param>
        /// <param name="sigFigs">The number of significant figures of the <txt>BigDecimal</txt></param>
        public BigDecimal(long value, int sigFigs = 1000) : this(value, 0, sigFigs) { }
        /// <summary>
        /// Initialize a <txt>BigDecimal</txt> struct with a 32-bit float and the number of significant figures.
        /// </summary>
        /// <param name="value">The value of the <txt>BigDecimal</txt>.</param>
        /// <param name="sigFigs">
        /// The number of significant figures of the <txt>BigDecimal</txt>.</param>
        public BigDecimal(float value, int sigFigs = 1000) : this((decimal)value, sigFigs) { }
        /// <summary>
        /// Initialize a <txt>BigDecimal</txt> struct with a 64-bit floating point number and the number of significant figures.
        /// </summary>
        /// <param name="value">The value of the <txt>BigDecimal</txt>.</param>
        /// <param name="sigFigs">The number of significant figures of the <txt>BigDecimal</txt>.</param>
        public BigDecimal(double value, int sigFigs = 1000) : this((decimal)value, sigFigs) { }
        /// <summary>
        /// Initialize a <txt>BigDecimal</txt> struct with a decimal value and the number of significant figures.
        /// </summary>
        /// <param name="value">The value of the <txt>BigDecimal</txt>.</param>
        /// <param name="sigFigs">The number of significant figures of the <txt>BigDecimal</txt>.</param>
        public BigDecimal(decimal value, int sigFigs = 1000)
        {
            decimal scale = 1m;
            Mantissa = (BigInteger)(value * scale);
            Exponent = 0;
            SignificantFigures = sigFigs;

            while ((decimal)Mantissa != scale * value)
            {
                scale *= 10.0m;
                Mantissa = (BigInteger)(value * scale);
                Exponent--;
            }
        }

        /// <summary>
        /// Simplify the <txt>BigDecimal</txt> object by removing unnecessary trailing zeroes from the 
        /// Mantissa. Simplified <txt>BigDecimal</txt>s are more performant.
        /// </summary>
        public void Simplify()
        {
            if (Mantissa.IsZero)
            {
                Exponent = 0;
                return;
            }

            while (Mantissa % _ten == 0L)
            {
                Mantissa /= _ten;
                Exponent++;
            }
        }

        /// <summary>
        /// Remove the least significant figures by truncating.
        /// <para>
        /// Also see: <a href="#Round"><txt>Round(int significantFigures)</txt></a>.
        /// </para>
        /// </summary>
        /// <param name="significantFigures"></param>
        public void Truncate(int significantFigures = -1)
        {
            int curr_sf = DigitCount(Mantissa);
            if (curr_sf > significantFigures)
            {
                int shift = curr_sf - significantFigures;
                Mantissa /= BigInteger.Pow(10, shift);
                Exponent += shift;
            }
        }
        /// <summary>
        /// Round the value, leaving a specified number of significant figures
        /// <para>
        /// Also see: <a href="#Truncate"><txt>Truncate(int significantFigures)</txt></a> which is a faster but cruder method.
        /// </para>
        /// </summary>
        /// <param name="significantFigures"></param>
        public void Round(int significantFigures)
        {
            int curr_sf = DigitCount(Mantissa);

            if (curr_sf > significantFigures)
            {
                int shift = curr_sf - significantFigures;

                BigInteger ten = new BigInteger(10);

                // Divide the Mantissa down to the first truncated digit
                Mantissa /= BigInteger.Pow(ten, shift - 1);

                // Divide by another 10 and use the first truncated digit to decide whether to round up or down
                Mantissa = BigInteger.DivRem(Mantissa, ten, out BigInteger first_truncated_digit);
                if (first_truncated_digit >= 5)
                {
                    Mantissa += BigInteger.One;
                }
                Exponent += shift;
            }
            SignificantFigures = significantFigures;
        }
        
        /// <summary>
        /// Returns the number of digits of a BigInteger.
        /// </summary>
        /// <param name="integer"></param>
        /// <returns></returns>
        private static int DigitCount(BigInteger integer)
        {
            return (int)Math.Ceiling(BigInteger.Log10(BigInteger.Abs(integer)));
        }

        /// <summary>
        /// Returns the multiplicative inverse of the ring of <txt>BigDecimal</txt> over addition and multiplication,
        /// accurate to the specified number of significant figures.
        /// </summary>
        /// <implements>Ring<T></implements>
        /// <param name="sigFigs">The number of significant figures.</param>
        /// <returns>The multiplicative inverse of this <txt>BigDecimal</txt> struct.</returns>
        public BigDecimal MultiplicativeInverse(int sigFigs)
        {
            int digitCount = DigitCount(Mantissa);

            // -Exponent -> significantFigures
            BigInteger one = BigInteger.Pow(_ten, digitCount + sigFigs - 1);
            BigInteger inv = BigInteger.Divide(one, Mantissa);
            BigDecimal x = new BigDecimal(inv, 1 - Exponent - digitCount - sigFigs, sigFigs);

            return x;
        }

        /// <summary>
        /// Returns the additive inverse of this BigDecimal in the additive group of <txt>BigDecimal</txt> over addition, 
        /// accurate to the specified number of significant figures.
        /// </summary>
        /// <param name="sigFigs">The number of significant figures.</param>
        /// <returns>Returns the resulting additive inverse of the original BigDecimal</returns>
        public BigDecimal AdditiveInverse(int sigFigs) 
        {
            return new BigDecimal(-Mantissa, Exponent, sigFigs);
        }

        /// <summary>
        /// Adds another BigDecimal to this BigDecimal,
        /// accurate to the number of significant figures
        /// </summary>
        /// <param name="x"></param>
        /// <param name="sigFigs"></param>
        /// <returns>Returns the added result.</returns>
        public BigDecimal Add(BigDecimal x, int sigFigs)
        {
            BigDecimal result;
            if (Exponent > x.Exponent)
            {
                // shift = the difference in exponents (always > 0)
                int shift = Exponent - x.Exponent;

                // |this| > |g| so we multiply (this) by factor, and use g's Exponent
                result = new BigDecimal(BigInteger.Pow(_ten, shift) * Mantissa + x.Mantissa, x.Exponent, sigFigs);
            }
            else if (Exponent < x.Exponent)
            {
                int shift = x.Exponent - Exponent;
                result = new BigDecimal(Mantissa + BigInteger.Pow(_ten, shift) * x.Mantissa, Exponent, sigFigs);
            }
            else
            {
                // exponents are the same - simple addition
                result = new BigDecimal(Mantissa + x.Mantissa, Exponent, sigFigs);
            }

            // Addition can cause the appearance of 0's at the end (e.g. 1 + 9999) - simplify
            result.Simplify();

            return result;
        }

        /// <summary>
        /// Subtracts this BigDecimal by another BigDecimal, computationally cheaper as it saves you from 
        /// creating an intermediary BigDecimal,
        /// accurate to the number of specified significant figures.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="sigFigs"></param>
        /// <returns>Returns the subtracted result.</returns>
        public BigDecimal Subtract(BigDecimal x, int sigFigs)
        {
            // This method is implemented separately to Add + AdditiveInverse 
            // since it safes the construction of an intermediary BigDecimal which is 
            // expensive

            BigDecimal result;
            if (Exponent > x.Exponent)
            {
                // shift = the difference in exponents (always > 0)
                int shift = Exponent - x.Exponent;

                // |this| > |g| so we multiply (this) by factor, and use g's Exponent
                result = new BigDecimal(BigInteger.Pow(_ten, shift) * Mantissa - x.Mantissa, x.Exponent, sigFigs);
            }
            else if (Exponent < x.Exponent)
            {
                int shift = x.Exponent - Exponent;
                result = new BigDecimal(Mantissa - BigInteger.Pow(_ten, shift) * x.Mantissa, Exponent, sigFigs);
            }
            else
            {
                // exponents are the same - simple subtraction
                result = new BigDecimal(Mantissa - x.Mantissa, Exponent, sigFigs);
            }

            // Addition can cause the appearance of 0's at the end (e.g. 1 + 9999) - simplify
            result.Simplify();

            return result;
        }

        /// <summary>
        /// Multiply this BigDecimal by another BigDecimal,
        /// accurate to the specified number of significant figures.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="sigFigs"></param>
        /// <returns></returns>
        public BigDecimal Multiply(BigDecimal x, int sigFigs)
        {
            // Add exponents and multiply Mantissas'
            BigDecimal d = new BigDecimal(Mantissa * x.Mantissa, Exponent + x.Exponent, sigFigs);
            d.Round(sigFigs);
            return d;
        }

        /// <summary>
        /// Multiply this BigDecimal by a BigInteger,
        /// accurate to the specified number of significant figures.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="sigFigs"></param>
        /// <returns></returns>
        public BigDecimal Multiply(BigInteger x, int sigFigs)
        {
            BigDecimal d = new BigDecimal(Mantissa * x, Exponent, sigFigs);
            d.Round(sigFigs);
            return d;
        }

        /// <summary>
        /// Divides this BigDecimal by another BigDecimal, computationally cheaper than
        /// creating an intermediary BigDecimal,
        /// accurate to the specified number of significant figures
        /// </summary>
        /// <param name="x"></param>
        /// <param name="sigFigs"></param>
        /// <returns></returns>
        public BigDecimal Divide(BigDecimal x, int sigFigs)
        {
            // This method is implemented separately to Multiply + Multiplicative inverse 
            // since it saves the construction of an intermediary BigDecimal which is 
            // expensive. 

            // This is the extra significant figures we must use in the computation of 
            // (this.Mantissa / x.Mantissa) in order to guarantee at least sigFigs 
            // significant figures in the result.
            int s = sigFigs + DigitCount(x.Mantissa) - DigitCount(Mantissa);

            // if s < 0, we can sacrifice some of the significant figures present in 
            // this.Mantissa while still guaranteeing at least 'sigFigs' significant figures 
            // in the result. While this is tempting to do, for simplicity we are going to
            // only going to implement the case where s >= 0, i.e. more zeroes need to be 
            // added to (this.Mantissa) in order to achieve the required level of precision
            s = Math.Max(0, s);

            // Perform the BigInteger division (this.Mantissa / x.Mantissa) using 
            // (this.Mantissa * 10^s / x.Mantissa) * 10^{-s}
            BigInteger man = BigInteger.Divide(Mantissa * BigInteger.Pow(10, s), x.Mantissa);

            // The resultant exponent will be (-s) + (this.Exponent - x.Exponent)
            BigDecimal result = new BigDecimal(man, -s + Exponent - x.Exponent, sigFigs);

            // Due to the rough step where we took s := max(0, s), we need a rounding step 
            // in case the result contains too many significant figures 
            result.Round(sigFigs);

            return result;
        }

        /// <summary>
        /// Divides this BigDecimal by a BigInteger,
        /// accurate to the specified amount of significant figures.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="sigFigs"></param>
        /// <returns></returns>
        public BigDecimal Divide(BigInteger x, int sigFigs)
        {
            // This method is very similar in contruction to Divide(BigDecimal x, int sigFigs) 
            // , the BigInteger is assumed to take the form of a BigDecimal(x, 0)

            int s = Math.Max(0, sigFigs + DigitCount(x) - DigitCount(Mantissa));
            BigInteger man = BigInteger.Divide(Mantissa * BigInteger.Pow(10, s), x);
            BigDecimal result = new BigDecimal(man, -s + Exponent, sigFigs);
            result.Round(sigFigs);
            return result;
        }

        public BigDecimal MultiplicativeInverse()
        {
            return MultiplicativeInverse(SignificantFigures);
        }
        public BigDecimal AdditiveInverse()
        {
            return AdditiveInverse(SignificantFigures);
        }

        public BigDecimal Add(BigDecimal x)
        {
            return Add(x, Math.Min(SignificantFigures, x.SignificantFigures));
        }
        public BigDecimal Multiply(BigDecimal x)
        {
            return Multiply(x, Math.Min(SignificantFigures, x.SignificantFigures));
        }
        public BigDecimal Multiply(BigInteger x)
        {
            return Multiply(x, SignificantFigures);
        }
        public BigDecimal Divide(BigDecimal x)
        {
            return Divide(x, Math.Min(SignificantFigures, x.SignificantFigures));
        }
        public BigDecimal Divide(BigInteger x)
        {
            return Divide(x, SignificantFigures);
        }

        /// <summary>
        /// Checks for Type and Value equality between this BigDecimal and another object. 
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        public override bool Equals(object e)
        {
            if (e == null || !(e is BigDecimal))
            {
                return false;
            }

            return Equals((BigDecimal)e);
        }

        public override int GetHashCode()
        {
            return Mantissa.GetHashCode() ^ Exponent;
        }

        /// <summary>
        /// Checks for value equality between this BigDecimal and another BigDecimal
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        public bool Equals(BigDecimal e)
        {
            Simplify();
            e.Simplify();

            return Exponent == e.Exponent && Mantissa == e.Mantissa;
        }
        public int CompareTo(BigDecimal other)
        {
            if (Exponent > other.Exponent)
            {
                int shift = Exponent - other.Exponent;
                return (BigInteger.Pow(_ten, shift) * Mantissa).CompareTo(other.Mantissa);
            }
            else if (Exponent < other.Exponent)
            {
                int shift = other.Exponent - Exponent;
                return Mantissa.CompareTo(BigInteger.Pow(_ten, shift) * other.Mantissa);
            }

            // Same exponents - just compare the Mantissa
            else
            {
                return Mantissa.CompareTo(other.Mantissa);
            }
        }
        public bool ApproximatelyEquals(BigDecimal e, double eps)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Returns a string representation of the BigDecimal object.
        /// </summary>
        /// <returns>A string representation of the BigDecimal</returns>
        public override string ToString()
        {
            string s = Mantissa.ToString();

            if (Exponent == 0)
            {
                return s;
            }
            if (Exponent > 0)
            {
                // Pad with zeroes
                return s + new string('0', Exponent);
            }

            bool negative = false;
            if (s.StartsWith("-"))
            {
                negative = true;
                s = s.Substring(1);
            }

            // If exponent < 0
            int index = s.Length + Exponent;

            // If |this| < 1, require a 0.x
            if (index <= 0 || IsNegative && index <= 1)
            {
                return (negative ? "-0." : "0.") + new string('0', Math.Abs(index)) + s;
            }
            else
            {
                return (negative ? "-" : "") + s.Insert(index, ".");
            }
        }

        /// <summary>
        /// Parses a BigDecimal object from a string.
        /// </summary>
        /// <param name="s">The string representing the BigDecimal</param>
        /// <returns>Returns the BigDecimal object</returns>
        public static BigDecimal Parse(string s)
        {
            if (string.IsNullOrEmpty(s))
            {
                return Zero;
            }

            s = s.ToUpper().Trim();

            // Deal with scientific format first
            if (s.Contains("E"))
            {
                int index = s.IndexOf("E");

                // E cannot be at the end
                if (index == s.Length - 1) 
                {
                    throw new FormatException(nameof(s));
                }    
                // Exponent must be integer
                if (!int.TryParse(s.Substring(index + 1), out int exp))
                {
                    throw new FormatException(nameof(s));
                }

                BigDecimal num = Parse(s.Substring(0, index));
                num.Exponent += exp;
                return num;
            }

            // Free of scientific formats -----------------------------------

            // If there is a decimal point, record its location (in exponent shifts)
            // and remove it. All other error handling will rely on BigInteger's implementation.
            int decimalIndex = s.IndexOf('.');
            if (decimalIndex >= 0)
            {
                int exponentShift = s.Length - decimalIndex - 1;

                // a.b -> (a * 10^(exp) + b) E(-exp)
                BigInteger a = BigInteger.Parse(s.Substring(0, decimalIndex));
                BigInteger b = BigInteger.Parse(s.Substring(decimalIndex + 1));
                BigInteger mantissa = a * BigInteger.Pow(10, exponentShift) + b;
                BigDecimal d = new BigDecimal(mantissa, -exponentShift, DigitCount(mantissa));
                d.Simplify();
                return d;
            }
            else
            {
                BigInteger mantissa = BigInteger.Parse(s);
                BigDecimal d = new BigDecimal(mantissa, DigitCount(mantissa));
                d.Simplify();
                return d;
            }
        }

        #region Casts
        public static implicit operator BigDecimal(int x) => new BigDecimal(x);
        public static implicit operator BigDecimal(long x) => new BigDecimal(x);
        public static implicit operator BigDecimal(float x) => new BigDecimal(x);
        public static implicit operator BigDecimal(double x) => new BigDecimal(x);
        public static implicit operator BigDecimal(decimal x) => new BigDecimal(x);
        public static implicit operator BigDecimal(BigInteger x) => new BigDecimal(x);

        public static explicit operator int(BigDecimal x)
        {
            BigInteger man = (BigInteger)x;
            if (man < int.MinValue || man > int.MaxValue)
            {
                throw new OverflowException(nameof(x));
            }
            return (int)man;
        }
        public static explicit operator long(BigDecimal x)
        {
            BigInteger man = (BigInteger)x;
            if (man < long.MinValue || man > long.MaxValue)
            {
                throw new OverflowException(nameof(x));
            }
            return (long)man;
        }
        public static explicit operator float(BigDecimal x)
        {
            return (float)((double)x.Mantissa / Math.Pow(10, x.Exponent));
        }
        public static explicit operator double(BigDecimal x)
        {
            return (double)x.Mantissa / Math.Pow(10, x.Exponent);
        }
        public static explicit operator BigInteger(BigDecimal x)
        {
            if (x.IsZero)
            {
                return BigInteger.Zero;
            }

            int exp = x.Exponent;
            BigInteger man = x.Mantissa;
            if (exp < 0)
            {
                man /= BigInteger.Pow(10, -exp);
            }
            else if (exp > 0)
            {
                man *= BigInteger.Pow(10, exp);
            }
            return man;
        }
        #endregion

        #region Operator overrides 
        public static BigDecimal operator +(BigDecimal x, BigDecimal y) => x.Add(y);
        public static BigDecimal operator -(BigDecimal x, BigDecimal y) => x.Subtract(y);
        public static BigDecimal operator *(BigDecimal x, BigDecimal y) => x.Multiply(y);
        public static BigDecimal operator *(BigDecimal x, BigInteger y) => x.Multiply(y);
        public static BigDecimal operator /(BigDecimal x, BigDecimal y) => x.Divide(y);
        public static BigDecimal operator -(BigDecimal x) => x.AdditiveInverse();
        public static bool operator >(BigDecimal x, BigDecimal y) => x.CompareTo(y) > 0;
        public static bool operator <(BigDecimal x, BigDecimal y) => x.CompareTo(y) < 0;
        public static bool operator ==(BigDecimal x, BigDecimal y) => x.Equals(y);
        public static bool operator !=(BigDecimal x, BigDecimal y) => !x.Equals(y);
        #endregion 
    }
}
