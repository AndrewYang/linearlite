﻿using LinearNet.Functions;
using LinearNet.Global;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Structs.Fields
{
    /// <summary>
    /// 
    /// </summary>
    /// <cat>numerics</cat>
    public partial struct BigDecimal
    {
        internal const int DEFAULT_SIG_FIGS = 1000;

        /// <summary>
        /// Calculates the square root of a non-negative <txt>BigDecimal</txt> $x$, returning the answer as a 
        /// <txt>BigDecimal</txt> correct up to '<txt>sigFigs</txt>' significant figures.
        /// </summary>
        /// <name>Sqrt</name>
        /// <proto>BigDecimal Sqrt(BigDecimal x, int sigFigs)</proto>
        /// <cat>functions</cat>
        /// <param name="x"></param>
        /// <returns></returns>
        public static BigDecimal Sqrt(BigDecimal x, int sigFigs = -1)
        {
            /// Use the Babylonian strategy
            /// Strategy is to express N = a * (10^k)^2 where a < 100,
            /// Then Sqrt(N) ~ Math.Sqrt(a) * 10^k
            /// Sqrt(a) is evaluated using the Math library. 
            
            if (x.IsNegative)
            {
                throw new ArgumentOutOfRangeException(nameof(x), ExceptionMessages.NegativeNotAllowed);
            }

            if (sigFigs < 0)
            {
                sigFigs = x.SignificantFigures;
            }

            if (x.IsZero)
            {
                return Zero;
            }

            GetInitialSqrtEstimate(x, out double a, out int k);

            // If k is negative, BigInteger has trouble evaluating 10^k
            // perform a shift to make sure that the power can be evaluated
            int shift = 0;
            if (k < 0)
            {
                shift = k;
                x = new BigDecimal(x.Mantissa, x.Exponent + 2 * shift, x.SignificantFigures);
                k = 0;
            }

            // Increase the number of s.f. by 2 to account for rounding errors
            sigFigs += 2;

            // Set the number of significant figures of x to the desired amount
            x.SignificantFigures = sigFigs;

            BigDecimal sqrt = new BigDecimal(Math.Sqrt(a), sigFigs) * BigInteger.Pow(10, k);
            BigDecimal half = new BigDecimal(0.5m, sigFigs);

            int sqrt_sf = x.SignificantFigures;
            int maxitr = (int)Util.Log2(x.SignificantFigures) * 10, i;
            for (i = 0; i < maxitr; ++i)
            {
                BigDecimal next = (x / sqrt + sqrt) * half;
                next.Round(sqrt_sf);

                if (next == sqrt)
                {
                    //Debug.WriteLine("Calculated sqrt in " + i + " out of " + maxitr);
                    break;
                }
                sqrt = next;
            }

            // undo the shift 
            sqrt.Exponent -= shift;

            // Return to the desired number of significant figures
            sqrt.Round(sigFigs - 2);

            return sqrt;
        }

        public static BigDecimal NthRoot(BigDecimal x, int n, int sigFigs = -1)
        {
            if (n <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(x), ExceptionMessages.NonPositiveNotAllowed);
            }
            if (sigFigs < 0)
            {
                sigFigs = x.SignificantFigures;
            }
            if (n == 1)
            {
                return x;
            }
            if (n == 2)
            {
                return Sqrt(x);
            }
            if (x.IsZero)
            {
                return Zero;
            }

            GetInitialNthRootEstimate(x, n, out double a, out int k);

            // Increase the number of s.f. by 2 to account for rounding errors
            sigFigs += 2;

            // The return threshold is when the change in sum is lower than half a 
            // place value
            BigDecimal eps = new BigDecimal(5, -sigFigs, sigFigs);

            // Set the number of significant figures of x to the desired amount
            if (x.SignificantFigures < sigFigs)
            {
                x.SignificantFigures = sigFigs;
            }

            // The number of required iteration is O(log2(sf))
            int maxitr = (int)Util.Log2(x.SignificantFigures) * 10;

            BigDecimal _n = new BigDecimal(n, sigFigs);
            BigDecimal u = new BigDecimal(n - 1, sigFigs) / _n;
            BigDecimal v = x / _n;
            BigDecimal root = new BigDecimal(Math.Pow(a, 1.0 / n), sigFigs) * new BigDecimal(1, k, sigFigs);

            for (int i = 0; i < maxitr; ++i)
            {
                BigDecimal next = u * root + v / Pow(root, n - 1);

                if (Abs(next - root) < eps)
                {
                    //Debug.WriteLine("Calculated in " + i + " out of " + maxitr + " steps");
                    break;
                }
                root = next;
            }
            return root;
        }


        /// <summary>
        /// Get the initial square root estimate as 
        /// sqrt(N) ~ sqrt * 10^exponent
        /// </summary>
        /// <param name="x"></param>
        /// <param name="sqrt"></param>
        /// <param name="exponent"></param>
        private static void GetInitialSqrtEstimate(BigDecimal x, out double sqrt, out int exponent)
        {
            // The number of significant figures 
            string n = x.Mantissa.ToString();
            int digits = x.Exponent + n.Length;

            int k = digits / 2, a_len = digits - 2 * k;
            if (a_len == 0)
            {
                k--;
                sqrt = double.Parse((n + "0").Substring(0, 2));
            }
            else
            {
                sqrt = double.Parse(n.Substring(0, 1));
            }
            exponent = k;
        }
        private static void GetInitialNthRootEstimate(BigDecimal x, int n, out double nthRoot, out int exponent)
        {
            // The number of significant figures 
            string val = x.Mantissa.ToString();
            int digits = x.Exponent + val.Length;

            int k = digits / n, a_len = digits - n * k;
            if (a_len == 0)
            {
                k--;
                nthRoot = double.Parse((val + new string('0', n)).Substring(0, n));
            }
            else
            {
                if (val.Length >= a_len)
                {
                    nthRoot = double.Parse(val.Substring(0, a_len));
                }
                else
                {
                    nthRoot = double.Parse((val + new string('0', a_len)).Substring(0, a_len));
                }
            }
            exponent = k;
        }

        /// <summary>
        /// Calculates $\exp(x)$ for some <txt>BigDecimal</txt> $x$.
        /// <para>
        /// Returns result as a <txt>BigDecimal</txt> accurate up to the specified number of significant figures.
        /// </para>
        /// </summary>
        /// <name>Exp</name>
        /// <proto>BigDecimal Exp(BigDecimal x, int sigFigs)</proto>
        /// <cat>functions</cat>
        /// <param name="x"></param>
        /// <returns></returns>
        public static BigDecimal Exp(BigDecimal x, int sigFigs = -1)
        {
            if (sigFigs < 0)
            {
                sigFigs = x.SignificantFigures;
            }

            // All the shifting does not apply to 0
            if (x.IsZero)
            {
                return new BigDecimal(1, sigFigs);
            }

            // Set the number of significant figures of x to the desired amount
            if (x.SignificantFigures < sigFigs)
            {
                x.SignificantFigures = sigFigs;
            }

            // x -> 10^(-t) * x
            int magnitude = DigitCount(x.Mantissa) + x.Exponent;
            x.Exponent -= magnitude;

            BigInteger kFact = BigInteger.One;
            BigDecimal xk = new BigDecimal(1, sigFigs);
            BigDecimal exp = new BigDecimal(1, sigFigs);

            int N = x.SignificantFigures, k;
            for (k = 1; k < N; ++k)
            {
                xk *= x;
                kFact *= k;

                BigDecimal next = exp + xk / new BigDecimal(kFact, sigFigs);
                next.Round(sigFigs);

                if (next == exp)
                {
                    break;
                }
                exp = next;
            }

            // exp(10^(-t) * x) -> exp(10^(-t) * x)^(10^t) = exp(x)
            // Raise by 10^magnitude
            if (magnitude > 0)
            {
                // Raise by a power of 10: x <- x^10
                for (int i = 0; i < magnitude; ++i)
                {
                    BigDecimal
                        x2 = exp * exp,
                        x4 = x2 * x2,
                        x8 = x4 * x4;
                    exp = x8 * x2;
                }
            }
            else if (magnitude < 0)
            {
                // Divide by a power of 10: x <- x^(-10)
                for (int i = 0; i > magnitude; --i)
                {
                    BigDecimal
                        x2 = exp * exp,
                        x4 = x2 * x2,
                        x8 = x4 * x4;
                    exp /= (x8 * x2);
                }
            }

            return exp;
        }

        /// <summary>
        /// <para>
        /// Calculate the natural logarithm of a <txt>BigDecimal</txt>, up to the specified number of significant figures.
        /// </para>
        /// The following approximation is used:
        /// $$\log(z) \approx 2 \sum_{k = 0}^{N}\frac{1}{2k+1} \Big(\frac{z-1}{z+1}\Big)^{2k+1}$$
        /// </summary>
        /// <name>Log</name>
        /// <proto>BigDecimal Log(BigDecimal x, int sigFigs)</proto>
        /// <cat>functions</cat>
        /// <param name="x"></param>
        /// <param name="sigFigs"></param>
        /// <returns></returns>
        public static BigDecimal Log(BigDecimal x, int sigFigs = -1)
        {
            if (x.IsZero || x.IsNegative)
            {
                throw new ArgumentOutOfRangeException(nameof(x), ExceptionMessages.NonPositiveNotAllowed);
            }

            if (sigFigs < 0)
            {
                sigFigs = x.SignificantFigures;
            }

            // Increase the number of s.f. by 2 to account for rounding errors
            sigFigs += 2;

            // Set the number of significant figures of x to the desired amount
            x.SignificantFigures = sigFigs;

            int maxitr = sigFigs * 2;
            BigDecimal sum = new BigDecimal(0, sigFigs);
            BigDecimal one = new BigDecimal(1, sigFigs);
            BigDecimal frac = (x - one) / (x + one);
            BigDecimal fracSqrd = frac * frac;
            BigDecimal pow = frac;

            // The return threshold is when the change in sum is lower than half a 
            // place value
            BigDecimal eps = new BigDecimal(5, -sigFigs, sigFigs);
            for (int k = 0; k < maxitr; ++k)
            {
                BigDecimal next = sum + pow / new BigDecimal(2 * k + 1, sigFigs);
                next.Round(sigFigs);

                if (Abs(next - sum) < eps)
                {
                    //Debug.WriteLine("Calculated in " + k + " steps");
                    break;
                }
                sum = next;
                pow *= fracSqrd;
            }

            // Round off the errors 
            sum *= new BigDecimal(2, sigFigs);

            sum.Round(sigFigs - 2);
            return sum;
        }

        /// <summary>
        /// <para>
        /// Calculate the natural logarithm (base $e$) of a <txt>BigDecimal</txt>, up to the specified number of significant figures
        /// This method uses Newton's method, which exhibits quadratic convergence. 
        /// </para>
        /// <para>
        /// If $\{y_n\}$ is a sequence that converges to $\log(x)$, the update step is:
        /// $$y_{n+1} = y_{n} - 1 + \frac{x}{e^{y_n}}$$
        /// </para>
        /// <para>
        /// This method is used by default for significant figures greater than 10,000.
        /// </para>
        /// </summary>
        /// <name>LogNewton</name>
        /// <proto>BigDecimal LogNewton(BigDecimal x, int sigFigs)</proto>
        /// <cat>functions</cat>
        /// <param name="x"></param>
        /// <param name="sigFigs"></param>
        /// <returns></returns>
        public static BigDecimal LogNewton(BigDecimal x, int sigFigs = -1)
        {
            if (x.IsZero || x.IsNegative)
            {
                throw new ArgumentOutOfRangeException(nameof(x), ExceptionMessages.NonPositiveNotAllowed);
            }

            if (sigFigs < 0)
            {
                sigFigs = x.SignificantFigures;
            }

            // Increase the number of s.f. by 2 to account for rounding errors.
            // the last 2 significant figures will be removed before the result 
            // is returned. 
            sigFigs += 2;

            // Set the number of significant figures of x to the desired amount
            x.SignificantFigures = sigFigs;

            // Get initial estimate based on the number of significant figures 
            int digits = DigitCount(x.Mantissa) + x.Exponent;

            // get the initial estimate
            // digits = log_10(x), so ln(x) = log_10(x) * log_10(e) = digits * log_10(e)
            BigDecimal log = new BigDecimal(digits * Math.Log(10) - 1, sigFigs);
            BigDecimal one = new BigDecimal(1, sigFigs);

            // The return threshold is when the change in sum is lower than half a 
            // place value
            BigDecimal eps = new BigDecimal(5, -sigFigs, sigFigs);

            // Assume that we achieve quadratic convergence, so the number of steps 
            // required for convergence is O(sqrt(s.f.))
            int maxitr = (int)Math.Sqrt(sigFigs);
            for (int k = 0; k < maxitr; ++k)
            {
                BigDecimal next = log - one + Exp(-log, sigFigs) * x;
                if (Abs(next - log) < eps)
                {
                    //Debug.WriteLine("Calculated in " + k + " out of " + maxitr + " steps");
                    break;
                }
                log = next;
            }

            // Round off the errors
            log.Round(sigFigs - 2);
            return log;
        }

        /// <summary>
        /// Calculates the absolute value of a <txt>BigDecimal</txt> $x$.
        /// <para>
        /// The returned value will have the same number of significant figures as $x$.</para>
        /// </summary>
        /// <name>Abs</name>
        /// <proto>BigDecimal Abs(BigDecimal x)</proto>
        /// <cat>functions</cat>
        /// <param name="x"></param>
        /// <returns></returns>
        public static BigDecimal Abs(BigDecimal x)
        {
            if (x.IsNegative)
            {
                return x.AdditiveInverse();
            }
            return x;
        }

        public static BigDecimal Pow(BigDecimal x, BigDecimal y, int sigFigs = -1)
        {
            return Exp(y * Log(x, sigFigs), sigFigs);
        }

        public static BigDecimal Pow(BigDecimal x, int power)
        {
            if (power == 0) return new BigDecimal(1, x.SignificantFigures);
            if (power == 1) return x;

            if (power % 2 == 0)
            {
                BigDecimal sqrtx = Pow(x, power / 2);
                return sqrtx * sqrtx;
            }
            else
            {
                BigDecimal sqrtx = Pow(x, power / 2);
                return sqrtx * sqrtx * x;
            }
        }

        /// <summary>
        /// Calculate the sine of a <txt>BigDecimal</txt> $x$, in radians,
        /// $$\sin(x) \approx \sum_{n=0}^{N} \frac{(-1)^k x^{2k + 1}}{(2k + 1)!}$$
        /// </summary>
        /// <name>Sin</name>
        /// <proto>BigDecimal Sin(BigDecimal x, int sigFigs)</proto>
        /// <cat>functions</cat>
        /// <param name="x"></param>
        /// <returns></returns>
        public static BigDecimal Sin(BigDecimal x, int sigFigs = -1)
        {
            if (sigFigs < 0)
            {
                sigFigs = x.SignificantFigures;
            }

            if (x.IsZero)
            {
                return new BigDecimal(0, sigFigs);
            }

            // Set the number of significant figures of x to the desired amount
            x.SignificantFigures = sigFigs;

            // Shift to the correct multiple of 2 * pi - convergence is better for small x.
            x = Mod(x, new BigDecimal(2, sigFigs) * Pi(sigFigs));

            BigDecimal eps = new BigDecimal(5, -sigFigs, sigFigs);
            BigDecimal fact = new BigDecimal(1, sigFigs);
            BigDecimal x2 = x * x, xPow = x;

            BigDecimal sum = new BigDecimal(0, sigFigs);
            int N = sigFigs;
            for (int k = 1; k < N; ++k)
            {
                BigDecimal next;
                if (k % 2 == 0)
                {
                    next = sum - xPow / fact;
                }
                else
                {
                    next = sum + xPow / fact;
                }

                if (Abs(next - sum) < eps)
                {
                    //Debug.WriteLine("Completed in: " + k + " of " + N + " cycles");
                    break;
                }

                sum = next;

                xPow *= x2;
                fact *= new BigDecimal(2 * k * (2 * k + 1), sigFigs);
            }

            return sum;
        }

        /// <summary>
        /// Calculate the cosine of a <txt>BigDecimal</txt> $x$, in radians, using the Taylor series expansion
        /// accurate to <txt>sigFigs</txt> significant figures.
        /// $$\cos(x) \approx \sum_{n = 0}^{N} \frac{(-1)^n x^{2n}}{(2n)!}$$
        /// </summary>
        /// <name>Cos</name>
        /// <proto>BigDecimal Cos(BigDecimal x, int sigFigs)</proto>
        /// <cat>functions</cat>
        /// <param name="x"></param>
        /// <param name="sigFigs"></param>
        /// <returns></returns>
        public static BigDecimal Cos(BigDecimal x, int sigFigs = -1)
        {
            if (sigFigs < 0)
            {
                sigFigs = x.SignificantFigures;
            }

            if (x.IsZero)
            {
                return new BigDecimal(1, sigFigs);
            }

            // Set the number of significant figures of x to the desired amount
            x.SignificantFigures = sigFigs;

            // Shift to the correct multiple of 2 * pi - convergence is better for small x.
            x = Mod(x, new BigDecimal(2, sigFigs) * Pi(sigFigs));

            BigDecimal eps = new BigDecimal(5, -sigFigs, sigFigs);
            BigDecimal fact = new BigDecimal(1, sigFigs);
            BigDecimal x2 = x * x, xPow = 1;

            BigDecimal sum = new BigDecimal(0, sigFigs);
            int N = sigFigs;
            for (int k = 0; k < N; ++k)
            {
                BigDecimal next;
                if (k % 2 == 0)
                {
                    next = sum + xPow / fact;
                }
                else
                {
                    next = sum - xPow / fact;
                }

                if (Abs(next - sum) < eps)
                {
                    //Debug.WriteLine("Completed in: " + k + " of " + N + " cycles");
                    break;
                }

                sum = next;

                xPow *= x2;
                fact *= new BigDecimal((2 * k + 1) * (2 * k + 2), sigFigs);
            }

            return sum;
        }

        public static BigDecimal Tan(BigDecimal x, int sigFigs = -1)
        {
            return Sin(x, sigFigs) / Cos(x, sigFigs);
        }

        /// <summary>
        /// Returns the cotangent of a <txt>BigDecimal</txt> struct.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="sigFigs"></param>
        /// <returns></returns>
        public static BigDecimal Cot(BigDecimal x, int sigFigs = -1)
        {
            return Cos(x, sigFigs) / Sin(x, sigFigs);
        }

        /// <summary>
        /// Calculate $\pi$ accurate to <txt>sigFigs</txt> significant figures using the approximation
        /// $$\frac{1}{\pi} \approx 12 \sum_{k = 0}^{N} \frac{(-1)^k(6k!)(13591409 + 545140134k)}{(3k)!(k!)^3 640320^{3k+3/2}}$$
        /// </summary>
        /// <name>Pi</name>
        /// <proto>BigDecimal Pi(int sigFigs)</proto>
        /// <cat>functions</cat>
        /// <param name="sigFigs"></param>
        /// <returns></returns>
        public static BigDecimal Pi(int sigFigs)
        {
            if (sigFigs <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(sigFigs), ExceptionMessages.NonPositiveNotAllowed);
            }

            // Use the default
            if (sigFigs < DEFAULT_SIG_FIGS)
            {
                BigDecimal p = PI;
                p.Round(sigFigs);
                return p;
            }

            int k = 0;

            // Increase the number of s.f. by 2 to account for rounding errors
            sigFigs += 2;

            BigInteger six_k_fact = 1, three_k_fact = 1, k_fact = 1;
            BigInteger a = 13591409, a_delta = 545140134;
            BigDecimal r = new BigDecimal(640320, sigFigs);
            BigDecimal c = r * Sqrt(r, sigFigs);
            BigDecimal r3 = r * r * r;
            BigDecimal one_over_pi = new BigDecimal(0, sigFigs);

            // The return threshold is when the change in sum is lower than half a place value
            BigDecimal eps = new BigDecimal(5, -sigFigs, sigFigs);

            while (true)
            {
                BigDecimal term = new BigDecimal(six_k_fact, sigFigs) / new BigDecimal(three_k_fact * k_fact * k_fact * k_fact, sigFigs) * new BigDecimal(a, sigFigs) / c;

                BigDecimal next;
                if (k % 2 == 0)
                {
                    next = one_over_pi + term;
                }
                else
                {
                    next = one_over_pi - term;
                }
                next.Round(sigFigs);

                if (Abs(next - one_over_pi) < eps)
                {
                    break;
                }

                one_over_pi = next;

                ++k;

                // Calculate (6k)! = six_k_fact * (6(k - 1) + 1) * ... * (6k)
                for (int i = 6 * (k - 1) + 1; i <= 6 * k; ++i)
                {
                    six_k_fact *= new BigInteger(i);
                }

                // Calculate (3k)! = three_k_fact * (3(k - 1) + 1) * ... * (3k)
                for (int i = 3 * (k - 1) + 1; i <= 3 * k; ++i)
                {
                    three_k_fact *= new BigInteger(i);
                }

                // Calculate k! = k_fact * k
                k_fact *= new BigInteger(k);

                a += a_delta;
                c *= r3;
            }

            BigDecimal pi = new BigDecimal(1, sigFigs) / (new BigDecimal(12, sigFigs) * one_over_pi);
            pi.Round(sigFigs - 2);

            return pi;
        }

        /// <summary>
        /// Calculate $e$ accurate to <txt>sigFigs</txt> significant figures using the taylor series approximation
        /// $$e \approx \sum_{n = 0}^{N} \frac{1}{n!}$$
        /// </summary>
        /// <name>Euler</name>
        /// <proto>BigDecimal Euler(int sigFigs)</proto>
        /// <cat>functions</cat>
        /// <param name="sigFigs"></param>
        /// <returns></returns>
        public static BigDecimal Euler(int sigFigs)
        {
            if (sigFigs <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(sigFigs), ExceptionMessages.NonPositiveNotAllowed);
            }

            // Use the default
            if (sigFigs < DEFAULT_SIG_FIGS)
            {
                BigDecimal exp = E;
                exp.Round(sigFigs);
                return exp;
            }

            BigInteger n_fact = 1;
            int n = 0;

            // Increase the number of s.f. by 2 to account for rounding errors
            sigFigs += 2;

            // The return threshold is when the change in sum is lower than half a 
            // place value
            BigDecimal eps = new BigDecimal(5, -sigFigs, sigFigs);
            BigDecimal one = new BigDecimal(1, sigFigs);
            BigDecimal e = new BigDecimal(0, sigFigs);

            while (true)
            {
                BigDecimal next = e + one / new BigDecimal(n_fact, sigFigs);
                if (Abs(next - e) < eps)
                {
                    break;
                }
                e = next;

                n++;
                n_fact *= n;
            }

            e.Round(sigFigs - 2);
            return e;
        }

        public static BigDecimal EulerMascheroniConstant1(int sigFigs)
        {
            if (sigFigs <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(sigFigs), ExceptionMessages.NonPositiveNotAllowed);
            }
            return -MathFunctions.Digamma(new BigDecimal(1, sigFigs), sigFigs);
        }

        /// <summary>
        /// Implementation uses the digamma function evaluated at 1.0. This implementation has a limit in accuracy as does all Bernoulli-based 
        /// implementations of the digamma function.
        /// </summary>
        /// <param name="sigFigs"></param>
        /// <returns></returns>
        public static BigDecimal EulerMascheroniConstant(int sigFigs)
        {
            if (sigFigs < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(sigFigs), ExceptionMessages.NonPositiveNotAllowed);
            }

            BigDecimal one = new BigDecimal(1, sigFigs);
            BigDecimal x = one;

            // Shift positively
            BigDecimal sum = new BigDecimal(0, sigFigs);
            BigDecimal threshold = new BigDecimal(10, sigFigs);
            while (x < threshold)
            {
                sum -= one / x;
                x += one;
            }
            sum += (Log(x, sigFigs) - one / (x * 2));

            // Termination criterion
            BigDecimal eps = new BigDecimal(5, -sigFigs, sigFigs);

            BigDecimal x2 = x * x, xPow = x2;
            int maxitr = sigFigs;

            BigRational[] B_2n = new BigRational[maxitr];
            B_2n[0] = BigRational.One;

            BigDecimal lastDelta = One;
            for (int n = 1; n < maxitr; ++n)
            {
                MathFunctions.EvenBernoulli(B_2n, n, n + 1);
                int two_n = 2 * n;
                //BigDecimal delta = MathFunctions.Bernoulli(two_n).ToDecimal(sigFigs) / (xPow * two_n);
                BigDecimal delta = B_2n[n].ToDecimal(sigFigs) / (xPow * two_n);
                sum -= delta;
                sum.Round(sigFigs);

                BigDecimal abs = Abs(delta);
                if (abs < eps)
                {
                    break;
                }
                if (lastDelta < abs)
                {
                    break;
                }

                lastDelta = abs;
                xPow *= x2;
            }
            return -sum;
        }

        /// <summary>
        /// Calculating Euler Mascheroni constant using the "Bessel function method" as described 
        /// in the paper <a href="http://numbers.computation.free.fr/Constants/Gamma/gamma.pdf">link</a>
        /// </summary>
        /// <param name="sigFigs"></param>
        /// <returns></returns>
        public static BigDecimal EulerMascheroniConstant2(int sigFigs)
        {
            // constant = A(n) / B(n) - log(n)
            // A(n) = \sum_{k = 0}^{alpha * n} (n^k / k!)^2 H_k
            // B(n) = \sum_{k = 0}^{alpha * n} (n^k / k!)^2

            // with alpha the solution to x(log(x) - 1) = 1

            // The error term is O(exp(-4n)) = O(10^{-sigFIgs})
            //int n = (int)(sigFigs * Math.Log(10) / 4.0);
            int n = sigFigs;

            BigDecimal An = new BigDecimal(0, sigFigs);
            BigDecimal Bn = new BigDecimal(1, sigFigs);
            BigInteger n2k = 1;     // (n^k)^2
            BigInteger k_fact2 = 1; // (k!)^2
            BigDecimal Hk = new BigDecimal(0, sigFigs);
            BigDecimal one = new BigDecimal(1, sigFigs);
            int n2 = n * n;

            double alpha = 3.5911;
            int k = 1;
            while (true)
            {
                n2k *= n2;
                k_fact2 *= k * k;
                Hk += one / new BigDecimal(k, sigFigs);

                BigDecimal coeff = new BigDecimal(n2k, sigFigs) / new BigDecimal(k_fact2, sigFigs);
                An += coeff * Hk;
                Bn += coeff;

                //Debug.WriteLine("\t\tA(n)\t" + An);
                //Debug.WriteLine("\t\tB(n)\t" + Bn);
                //Debug.WriteLine("\t\tH(k)\t" + Hk);
                //Debug.WriteLine("\t\tlog(n)\t" + Log(n, sigFigs));
                //Debug.WriteLine(k + "\t" + (An / Bn - Log(new BigDecimal(n, sigFigs), sigFigs)));

                k++;
                if (alpha * n < k)
                {
                    break;
                }
            }

            return An / Bn - Log(new BigDecimal(n, sigFigs), sigFigs);
        }

        /// <summary>
        /// Calculates and returns the inverse sine function of a ratio $x$, where $-1\le{x}\le{1}$, correct to the specified number of significant figures.
        /// Returns an angle $\theta$ in the range $-\pi/2\le{\theta}\le{\pi/2}$.
        /// 
        /// <para>
        /// This implementation uses the series approximation 
        /// $$\arcsin(x) \approx \sum_{n=0}^{N} \frac{(2n)!}{(2^nn!)^2}\frac{x^{2n+1}}{2n+1}$$
        /// </para>
        /// <!--inputs-->
        /// </summary>
        /// <name>Asin</name>
        /// <proto>BigDecimal Asin(BigDecimal x, int sigFigs)</proto>
        /// <cat>functions</cat>
        /// <param name="x">The value of $x$ at which to evaluate the function.</param>
        /// <param name="sigFigs">The number of significant figures in the returned result.</param>
        /// <returns></returns>
        public static BigDecimal Asin(BigDecimal x, int sigFigs = -1)
        {
            if (x < -One || x > One)
            {
                throw new ArgumentOutOfRangeException(nameof(x), ExceptionMessages.NotBetweenPlusMinus1);
            }

            if (sigFigs < 0)
            {
                sigFigs = x.SignificantFigures;
            }
            if (x == One)
            {
                return Pi(sigFigs) / new BigDecimal(2, sigFigs);
            }
            if (x == -One)
            {
                return Pi(sigFigs) / -(new BigDecimal(2, sigFigs));
            }

            // Increase the number of s.f. by 2 to account for rounding errors
            sigFigs += 2;

            // Set the number of significant figures of x to the desired amount
            x.SignificantFigures = sigFigs;

            int maxitr = sigFigs;
            BigInteger
                two_n_choose_n = 1,
                four_power_n = 1;
            BigDecimal
                x_power = x,
                x2 = x * x,
                sum = new BigDecimal(0, sigFigs);

            // The return threshold is when the change in sum is lower than half a place value
            BigDecimal eps = new BigDecimal(5, -sigFigs, sigFigs);

            for (int n = 1; n < maxitr; ++n)
            {
                BigDecimal delta = new BigDecimal(two_n_choose_n, sigFigs) * x_power / (four_power_n * new BigDecimal(2 * n - 1, sigFigs));
                BigDecimal next = sum + delta;
                next.Round(sigFigs);

                if (Abs(delta) < eps)
                {
                    break;
                }
                sum = next;

                // C(2n, n) = C(2(n - 1), n - 1) * 2 * (2n - 1) / n
                two_n_choose_n *= (2 * (2 * n - 1));
                two_n_choose_n /= n;

                four_power_n *= 4;
                x_power *= x2;
            }

            sum.Round(sigFigs - 2);
            return sum;
        }

        /// <summary>
        /// Calculates and returns the inverse cosine function of a <txt>BigDecimal</txt> $x$, returning the result as a <txt>BigDecimal</txt>
        /// correct to the specified number of significant figures.
        /// </summary>
        /// <name>Acos</name>
        /// <proto>BigDecimal Acos(BigDecimal x, int sigFigs)</proto>
        /// <cat>functions</cat>
        /// <param name="x"></param>
        /// <param name="sigFigs"></param>
        /// <returns></returns>
        public static BigDecimal Acos(BigDecimal x, int sigFigs = -1)
        {
            if (sigFigs < 0)
            {
                sigFigs = x.SignificantFigures;
            }
            return Pi(sigFigs) / new BigDecimal(2, sigFigs) - Asin(x, sigFigs);
        }

        /// <summary>
        /// Returns the inverse tangent function of $x$, $\arctan(x)$. 
        /// <para>
        /// This implementation uses the Euler series:
        /// $$\arctan(x) \approx \sum_{n=0}^{N} \frac{2^{2n}(n!)^2}{(2n+1)!}\frac{x^{2n+1}}{(1+x^2)^{n+1}}$$
        /// </para>
        /// </summary>
        /// <name>Atan</name>
        /// <proto>BigDecimal Atan(BigDecimal x, int sigFigs)</proto>
        /// <cat>functions</cat>
        /// <param name="x"></param>
        /// <param name="sigFigs"></param>
        /// <returns></returns>
        public static BigDecimal Atan(BigDecimal x, int sigFigs = -1)
        {
            if (sigFigs <= 0)
            {
                sigFigs = x.SignificantFigures;
            }

            // Increase the number of s.f. by 2 to account for rounding errors
            sigFigs += 2;

            // Set the number of significant figures of x to the desired amount
            x.SignificantFigures = sigFigs;

            int maxitr = sigFigs * 2;

            // x^(2n + 1) / (1 + x^2)^(n + 1) = (x / (1 + x^2)) * (x^2 / (1 + x^2))^n
            BigDecimal
                one = new BigDecimal(1, sigFigs),
                B_n = one,              // B(n) = 2^(2n) (n!)^2 / (2n + 1)!
                x2 = x * x,
                xFact = x2 / (one + x2), // xFact = x^2 / (1 + x^2)
                xPow = x / (one + x2),
                sum = new BigDecimal(0, sigFigs);

            // The return threshold is when the change in sum is lower than half a place value
            BigDecimal eps = new BigDecimal(5, -sigFigs, sigFigs);
            for (int n = 1; n < maxitr; ++n)
            {
                BigDecimal delta = xPow * B_n;
                BigDecimal next = sum + delta;
                next.Round(sigFigs);

                //Debug.WriteLine(n + "\t" + next);

                if (Abs(delta) < eps)
                {
                    break;
                }
                sum = next;

                // B(n - 1) * 2n / (2n + 1) = B(n)
                B_n *= new BigDecimal(2 * n, sigFigs);
                B_n /= new BigDecimal(2 * n + 1, sigFigs);

                xPow *= xFact;
            }

            sum.Round(sigFigs - 2);
            return sum;
        }
        /// <summary>
        /// Returns the two-argument arctangent function $\operatorname{atan2}(y, x)$, accurate to the specified number of significant figures.
        /// </summary>
        /// <name>Atan2</name>
        /// <proto>BigDecimal Atan2(BigDecimal x, BigDecimal y, int sigFigs)</proto>
        /// <cat>functions</cat>
        /// <param name="y"></param>
        /// <param name="x"></param>
        /// <param name="sigFigs"></param>
        /// <returns></returns>
        public static BigDecimal Atan2(BigDecimal y, BigDecimal x, int sigFigs = -1)
        {
            if (sigFigs < 0)
            {
                sigFigs = Math.Min(y.SignificantFigures, x.SignificantFigures);
            }
            if (x.IsPositive)
            {
                y.SignificantFigures = sigFigs;
                x.SignificantFigures = sigFigs;
                return Atan(y / x);
            }
            if (x.IsNegative)
            {
                y.SignificantFigures = sigFigs;
                x.SignificantFigures = sigFigs;
                if (y.IsNegative)
                {
                    return Atan(y / x) + Pi(sigFigs);
                }
                else
                {
                    return Atan(y / x) - Pi(sigFigs);
                }
            }
            if (y.IsPositive)
            {
                return Pi(sigFigs) / new BigDecimal(2, sigFigs);
            }
            if (y.IsNegative)
            {
                return -Pi(sigFigs) / new BigDecimal(2, sigFigs);
            }

            // TODO: we ((REALLY)) need a NaN
            return new BigDecimal(0, sigFigs);
        }

        /// <summary>
        /// Returns the hyperbolic sine function, $\sinh(x)$, accurate to a specified number fo 
        /// significant figures.
        /// </summary>
        /// <name>Sinh</name>
        /// <proto>BigDecimal Sinh(BigDecimal x, int sigFigs)</proto>
        /// <cat>functions</cat>
        /// <param name="x"></param>
        /// <param name="sigFigs"></param>
        /// <returns></returns>
        public static BigDecimal Sinh(BigDecimal x, int sigFigs = -1)
        {
            if (sigFigs < 0)
            {
                sigFigs = x.SignificantFigures;
            }

            BigDecimal e = Exp(x, sigFigs);
            BigDecimal one = new BigDecimal(1, sigFigs), two = new BigDecimal(2, sigFigs);
            return (e * e - one) / (two * e);
        }
        public static BigDecimal Cosh(BigDecimal x, int sigFigs = -1)
        {
            if (sigFigs < 0)
            {
                sigFigs = x.SignificantFigures;
            }

            BigDecimal e = Exp(x, sigFigs);
            BigDecimal one = new BigDecimal(1, sigFigs), two = new BigDecimal(2, sigFigs);
            return (e * e + one) / (two * e);
        }
        public static BigDecimal Tanh(BigDecimal x, int sigFigs = -1)
        {
            if (sigFigs < 0)
            {
                sigFigs = x.SignificantFigures;
            }

            BigDecimal one = new BigDecimal(1, sigFigs), two = new BigDecimal(2, sigFigs);
            BigDecimal e2x = Exp(two * x, sigFigs);
            return (e2x - one) / (e2x + one);
        }
        /// <summary>
        /// Unlimited-precision implementation of the hyperbolic cosecant function, 
        /// $$\operatorname{csch}(x) = \frac{2}{e^x - e^{-x}},$$ 
        /// evaluated for a <txt>BigDecimal</txt> $x$, returning the result accurate
        /// up to a specified number of significant figures.
        /// </summary>
        /// <name>Csch</name>
        /// <proto>BigDecimal Csch(BigDecimal x, int sigFigs)</proto>
        /// <cat>functions</cat>
        /// <param name="x"></param>
        /// <param name="sigFigs"></param>
        /// <returns></returns>
        public static BigDecimal Csch(BigDecimal x, int sigFigs = -1)
        {
            if (sigFigs < 0)
            {
                sigFigs = x.SignificantFigures;
            }

            BigDecimal e = Exp(x, sigFigs);
            BigDecimal one = new BigDecimal(1, sigFigs), two = new BigDecimal(2, sigFigs);
            return (two * e) / (e * e - one);
        }
        /// <summary>
        /// Returns the hyperbolic secant function, $\operatorname{sech}(x)$ evaluated for a <txt>BigDecimal</txt> $x$
        /// </summary>
        /// <name>Sech</name>
        /// <proto>BigDecimal Sech(BigDecimal x, int sigFigs)</proto>
        /// <cat>functions</cat>
        /// <param name="x"></param>
        /// <param name="sigFigs"></param>
        /// <returns></returns>
        public static BigDecimal Sech(BigDecimal x, int sigFigs = -1)
        {
            if (sigFigs < 0)
            {
                sigFigs = x.SignificantFigures;
            }

            BigDecimal e = Exp(x, sigFigs);
            BigDecimal one = new BigDecimal(1, sigFigs), two = new BigDecimal(2, sigFigs);
            return (two * e) / (e * e + one);
        }
        /// <summary>
        /// Returns the hyperbolic cosecant, $\operatorname{coth}(x)$ evaluated for a <txt>BigDecimal</txt> $x$
        /// </summary>
        /// <name>Coth</name>
        /// <proto>BigDecimal Coth(BigDecimal x, int sigFigs)</proto>
        /// <cat>functions</cat>
        /// <param name="x"></param>
        /// <param name="sigFigs"></param>
        /// <returns></returns>
        public static BigDecimal Coth(BigDecimal x, int sigFigs = -1)
        {
            if (x.IsZero)
            {
                throw new ArgumentOutOfRangeException(nameof(x), ExceptionMessages.ZeroNotAllowed);
            }

            if (sigFigs < 0)
            {
                sigFigs = x.SignificantFigures;
            }

            BigDecimal one = new BigDecimal(1, sigFigs), two = new BigDecimal(2, sigFigs);
            BigDecimal e2x = Exp(two * x, sigFigs);
            return (e2x + one) / (e2x - one);
        }

        /// <summary>
        /// Returns a <txt>BigDecimal</txt> modulo another <txt>BigDecimal</txt>.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="mod"></param>
        /// <returns></returns>
        public static BigDecimal Mod(BigDecimal x, BigDecimal mod)
        {
            if (mod.Sign <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(mod), ExceptionMessages.NonPositiveNotAllowed);
            }

            BigDecimal result = x - mod * (BigInteger)(x / mod);
            if (result.Sign < 0)
            {
                return result + mod;
            }
            return result;
        }



        public static BigDecimal Sum(IEnumerable<BigDecimal> summands)
        {
            if (summands.Count() == 0)
            {
                return Zero;
            }

            BigDecimal sum = new BigDecimal(0, summands.First().SignificantFigures);
            foreach (BigDecimal d in summands)
            {
                sum += d;
            }
            return sum;
        }
        public static BigDecimal Product(IEnumerable<BigDecimal> factors)
        {
            if (factors.Count() == 0)
            {
                return One;
            }

            BigDecimal product = new BigDecimal(1, factors.First().SignificantFigures);
            foreach (BigDecimal factor in factors)
            {
                product *= factor;
            }
            return product;
        }
    }
}
