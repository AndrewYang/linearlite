﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;

namespace LinearNet.Structs.Fields
{
    public static class BigIntegerExtensions
    {
        internal static BigInteger Product(IEnumerable<BigInteger> factors)
        {
            BigInteger product = BigInteger.One;
            foreach (BigInteger factor in factors)
            {
                product *= factor;
            }
            return product;
        }
    }
}
