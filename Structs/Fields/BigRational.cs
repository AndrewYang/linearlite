﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Structs.Fields
{
    /// <summary>
    /// BigRational Description
    /// </summary>
    /// <remarks>
    /// Have debated the implementation details for a long time - here is a summary of my thoughts:
    /// - There is a need for GetHashCode() to be implemented non-trivally (e.g. not returning a constant
    /// for every BigRational struct), and consistently with Equals(object). 
    /// - If the struct can be guaranteed to always be in canonical form, then the Numerator/Denominator
    /// fields can be made readonly
    /// - Calculating GCD at construction time can be very expensive. 
    /// 
    /// In this implementation I have chosen to divide by GCD in the constructor to enable GetHashCode
    /// to be implemented properly, and be consistent with the Equals(object) method. This comes at some
    /// performance penalty.
    /// </remarks>
    /// <cat>numerics</cat>
    public partial struct BigRational : IField<BigRational>, IComparable<BigRational>, IEquatable<BigRational>
    {
        public readonly BigInteger Numerator, Denominator;

        public readonly static BigRational Zero = new BigRational(0, 1);
        public readonly static BigRational One = new BigRational(1, 1);
        public readonly static BigRational NaN = new BigRational(0, 0);

        public BigRational MultiplicativeIdentity => One;
        public BigRational AdditiveIdentity => Zero;
        public BigInteger Characteristic => BigInteger.Zero;
        public bool IsZero => Numerator.IsZero;
        public bool IsOne
        {
            get
            {
                if (Numerator.IsZero) return false; // 0 / 0 is not 1.
                return Numerator == Denominator;
            }
        }
        public bool IsNegative => Numerator > 0 && Denominator < 0 || Numerator < 0 && Denominator > 0;
        public bool IsPositive => Numerator > 0 && Denominator > 0 || Numerator < 0 && Denominator < 0;
        public bool IsInteger
        {
            get
            {
                if (Numerator.IsZero) return true; // denominator is allowed to be 0 in this case
                if (Denominator.IsZero) return false;
                return (Numerator % Denominator).IsZero;
            }
        }
        public bool IsNaN => Numerator.IsZero && Denominator.IsZero;

        public BigInteger IntegerValue
        {
            get
            {
                if (Denominator.IsOne)
                {
                    return Numerator;
                }
                return Numerator / Denominator;
            }
        }

        /// <summary>
        /// Initialises a BigRational object with a numerator and denominator
        /// </summary>
        /// <param name="numerator"></param>
        /// <param name="denominator"></param>
        public BigRational(BigInteger numerator, BigInteger denominator)
        {
            if (!denominator.IsZero)
            {
                BigInteger gcd = GreatestCommonDivisor(BigInteger.Abs(numerator), BigInteger.Abs(denominator));
                if (denominator.Sign < 0)
                {
                    gcd = -gcd;
                }
                Numerator = numerator / gcd;
                Denominator = denominator / gcd;
            }
            else
            {
                if (!numerator.IsZero)
                {
                    throw new DivideByZeroException();
                }
                // becomes NaN
                Numerator = numerator;
                Denominator = denominator;
            }
        }

        /// <summary>
        /// Internal constructor for creating BigRational objects that are not simplified.
        /// Only should be used if we are guaranteed that GCD(numerator, denominator) = 1. 
        /// </summary>
        internal BigRational(BigInteger numerator, BigInteger denominator, bool simplify)
        {
            if (simplify) throw new InvalidOperationException();
            Numerator = numerator;
            Denominator = denominator;
        }

        /// <summary>
        /// Create a new <txt>BigRational</txt> struct from an integer.
        /// </summary>
        /// <param name="i">A integer.</param>
        public BigRational(BigInteger i) : this(i, BigInteger.One, false) { }
        /// <summary>
        /// Create a new <txt>BigRational</txt> struct from a <txt>Rational</txt>
        /// struct, which uses 32-bit integers to represent its numerators and denominators.
        /// </summary>
        /// <param name="q">A 32-bit rational struct.</param>
        public BigRational(Rational q) : this(q.Numerator, q.Denominator) { }
        /// <summary>
        /// Create a new <txt>BigRational</txt> struct from its numerator and denominators.
        /// </summary>
        /// <param name="numerator">The numerator</param>
        /// <param name="denominator">The denominator</param>
        public BigRational(int numerator, int denominator) : this((BigInteger)numerator, (BigInteger)denominator) { }
        public BigRational(int i) : this(i, BigInteger.One, false) { }
        public BigRational(long numerator, long denominator) : this((BigInteger)numerator, (BigInteger)denominator) { }
        public BigRational(long i) : this(i, BigInteger.One, false) { }

        private static BigInteger GreatestCommonDivisor(BigInteger a, BigInteger b)
        {
            if (a == BigInteger.Zero)
            {
                return b;
            }
            if (b == BigInteger.Zero)
            {
                return a;
            }

            if (a > b)
            {
                return GreatestCommonDivisor(a % b, b);
            }
            return GreatestCommonDivisor(a, b % a);
        }

        /// <summary>
        /// Flips the numerator and denominator
        /// </summary>
        /// <returns></returns>
        public BigRational MultiplicativeInverse()
        {
            return new BigRational(Denominator, Numerator, false);
        }
        public BigRational AdditiveInverse()
        {
            return new BigRational(-Numerator, Denominator, false);
        }
        public BigRational Multiply(BigRational e)
        {
            return new BigRational(Numerator * e.Numerator, Denominator * e.Denominator);
        }
        public BigRational Add(BigRational e)
        {
            return new BigRational(Numerator * e.Denominator + e.Numerator * Denominator, Denominator * e.Denominator);
        }

        /// <summary>
        /// Returns whether this rational number is exactly equal to another, prior to 
        /// simplification. For example, this method will return false for 1/2 and 3/6. 
        /// <p>
        /// The BigRational struct uses aggressive simplification - i.e. the fraction 
        /// is simplified at construction time.
        /// </p>
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        public bool Equals(BigRational e)
        {
            // NaN get their own comparisons
            if (IsNaN || e.IsNaN)
            {
                return IsNaN == e.IsNaN;
            }

            // For non-NaN's
            return Numerator == e.Numerator && Denominator == e.Denominator;
        }
        public bool ApproximatelyEquals(BigRational e, double eps)
        {
            // Not correct
            return Equals(e);
        }
        public override string ToString()
        {
            if (IsNaN)
            {
                return "NaN";
            }
            if (IsInteger)
            {
                return (Numerator / Denominator).ToString();
            }
            return Numerator.ToString() + "/" + Denominator.ToString();
        }

        /// <summary>
        /// Not necessary, included for performance reasons
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        public BigRational Subtract(BigRational e)
        {
            return new BigRational(Numerator * e.Denominator - e.Numerator * Denominator, Denominator * e.Denominator);
        }
        /// <summary>
        /// Not necessary, included for performance reasons
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        public BigRational Divide(BigRational e)
        {
            return new BigRational(Numerator * e.Denominator, Denominator * e.Numerator);
        }

        public BigDecimal ToDecimal(int sigFigs)
        {
            if (IsNaN)
            {
                return new BigDecimal(double.NaN, sigFigs);
            }

            BigDecimal ratio = new BigDecimal(Numerator, sigFigs + 1) / new BigDecimal(Denominator, sigFigs + 1);
            ratio.Round(sigFigs);
            return ratio;
        }

        public int CompareTo(BigRational e)
        {
            // NaN's are the smallest 
            bool this_is_nan = IsNaN, e_is_nan = e.IsNaN;
            if (this_is_nan && e_is_nan)
            {
                return 0;
            }
            if (this_is_nan)
            {
                return -1;
            }
            if (e_is_nan)
            {
                return 1;
            }

            BigInteger a = Numerator * e.Denominator, b = e.Numerator * Denominator;
            if (a > b)
            {
                return 1;
            }
            if (a < b)
            {
                return -1;
            }
            return 0;
        }
        public BigDecimal ToBigDecimal(int sigFigs = BigDecimal.DEFAULT_SIG_FIGS)
        {
            return new BigDecimal(Numerator, sigFigs) / new BigDecimal(Denominator, sigFigs);
        }

        public override bool Equals(object obj)
        {
            if (obj is null)
            {
                return false;
            }
            if (!(obj is BigRational))
            {
                return false;
            }
            return Equals((BigRational) obj);
        }
        public override int GetHashCode()
        {
            return Numerator.GetHashCode() * 37 + Denominator.GetHashCode();
        }

        #region Implicit overrides 
        public static implicit operator BigRational(Rational q) => new BigRational(q); 
        public static implicit operator BigRational(BigInteger i) => new BigRational(i);
        public static implicit operator BigRational(int i) => new BigRational(i);
        public static implicit operator BigRational(long i) => new BigRational(i);
        public static explicit operator double(BigRational v)
        {
            // TODO: come up with a more numerically stable version 
            return (double)v.Numerator / (double)v.Denominator;
        }
        #endregion

        #region Operator overrides 
        public static BigRational operator +(BigRational f, BigRational g) => f.Add(g);
        public static BigRational operator -(BigRational f, BigRational g) => f.Subtract(g);
        public static BigRational operator *(BigRational f, BigRational g) => f.Multiply(g);
        public static BigRational operator /(BigRational f, BigRational g) => f.Divide(g);
        public static BigRational operator -(BigRational f) => f.AdditiveInverse();
        public static BigRational operator %(BigRational f, BigRational mod) => Mod(f, mod);

        public static bool operator ==(BigRational f, BigRational g) => f.Equals(g);
        public static bool operator !=(BigRational f, BigRational g) => !f.Equals(g);
        public static bool operator >(BigRational f, BigRational g) => f.CompareTo(g) > 0;
        public static bool operator <(BigRational f, BigRational g) => f.CompareTo(g) < 0;
        public static bool operator >=(BigRational f, BigRational g) => f.CompareTo(g) >= 0;
        public static bool operator <=(BigRational f, BigRational g) => f.CompareTo(g) <= 0;
        #endregion
    }
}
