﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Structs.Fields
{
    public partial struct BigRational
    {
        public static BigRational Pow(BigRational q, BigInteger pow)
        {
            if (int.MinValue <= pow && pow <= int.MaxValue)
            {
                return Pow(q, (int)pow);
            }

            if (pow.IsZero)
            {
                return One;
            }
            if (pow.IsOne)
            {
                return q;
            }
            if (pow < 0)
            {
                return PositivePow(q, -pow).MultiplicativeInverse();
            }
            return PositivePow(q, pow);
        }
        private static BigRational PositivePow(BigRational q, BigInteger pow)
        {
            if (pow.IsOne)
            {
                return q;
            }

            BigRational half = PositivePow(q, pow >> 1);
            if (pow % 2 == 1)
            {
                return new BigRational(half.Numerator * half.Numerator * q.Numerator, half.Denominator * half.Denominator * q.Denominator, false);
            }
            return new BigRational(half.Numerator * half.Numerator, half.Denominator * half.Denominator, false);
        }

        public static BigRational Pow(BigRational q, int power)
        {
            if (power == 0)
            {
                return One;
            }

            if (power < 0)
            {
                return Pow(q.MultiplicativeInverse(), -power);
            }

            return PositivePow(q, power);
        }

        private static BigRational PositivePow(BigRational q, int power)
        {
            if (power == 1)
            {
                return q;
            }

            BigRational half = PositivePow(q, power / 2);
            if (power % 2 == 1)
            {
                // Multiply without simplification (which is the default behaviour)
                return new BigRational(half.Numerator * half.Numerator * q.Numerator, half.Denominator * half.Denominator * q.Denominator, false);
            }

            // Multiply without simplification (which is the default behaviour)
            return new BigRational(half.Numerator * half.Numerator, half.Denominator * half.Denominator, false);
        }

        public static BigRational Abs(BigRational q)
        {
            if (q.IsNegative)
            {
                return q.AdditiveInverse();
            }
            return q;
        }

        public static BigRational Mod(BigRational q, BigRational mod)
        {
            q -= (q / mod).IntegerValue * mod;
            if (q < 0)
            {
                q += mod;
            }
            return q;
        }

        public static BigRational Sum(IEnumerable<BigRational> summands)
        {
            BigRational sum = new BigRational(0, 1, false);
            foreach (BigRational r in summands)
            {
                sum += r;
            }
            return sum;
        }
        public static BigRational Product(IEnumerable<BigRational> factors)
        {
            BigRational prod = new BigRational(1, 1, false);
            foreach (BigRational r in factors)
            {
                prod *= r;
            }
            return prod;
        }
    }
}
