﻿using LinearNet.Global;
using LinearNet.Structs.Fields;
using LinearNet.Structs.Spaces;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Structs
{
    /// <summary>
    /// A representation of a complex number, where both real and imaginary components are double types.
    /// Note: this class does not implement IEquatable<T> as computation in limited-precision arithmetic
    /// leads to roundoff errors. Use ApproximatelyEquals(double eps) instead.
    /// 
    /// This struct contains an implicit cast from types of System.Numerics.Complex.
    /// </summary>
    public struct Complex : IField<Complex>, IComplex<double>
    {
        public double Real, Imaginary;

        public static readonly Complex Zero = new Complex(0);
        public static readonly Complex One = new Complex(1);
        public static readonly Complex I = new Complex(0, 1);
        public static readonly Complex NaN = new Complex(double.NaN, double.NaN);

        public Complex AdditiveIdentity => Zero;
        public Complex MultiplicativeIdentity => One;
        public System.Numerics.BigInteger Characteristic => System.Numerics.BigInteger.Zero;


        /// <summary>
        /// Constructor for the complex number x + yi
        /// </summary>
        /// <param name="x">The real part of the complex number.</param>
        /// <param name="y">(Optional) The imaginary part of the complex number.</param>
        public Complex(double x, double y = 0)
        {
            Real = x;
            Imaginary = y;
        }

        /// <summary>
        /// Construct a complex number, z, using modulus |z| and argument arg(z) in radians
        /// </summary>
        /// <param name="modulus">The modulus of the complex number</param>
        /// <param name="argument">The argument of the complex number</param>
        /// <returns></returns>
        public static Complex FromPolar(double modulus, double argument)
        {
            if (modulus < 0)
            {
                throw new ArgumentOutOfRangeException("Modulus of a complex number must be positive");
            }
            return new Complex(modulus * Math.Cos(argument), modulus * Math.Sin(argument));
        }

        #region Public methods

        /// <summary>
        /// Returns the argument of the complex number, in radians, in the range [-pi/2, pi/2]
        /// </summary>
        public double Argument()
        {
            if (Real == 0.0)
            {
                if (Imaginary == 0.0) return double.NaN;
                if (Imaginary > 0.0) return Constants.PiOver2;
                return -Constants.PiOver2;
            }
            return Math.Atan2(Imaginary, Real);
        }

        /// <summary>
        /// Returns the complex conjugate of some complex number c.
        /// I.e. if c = a + bi, then a - bi is returned.
        /// </summary>
        /// <param name="c">A complex number</param>
        /// <returns>The complex conjugate of c</returns>
        public Complex Conjugate()
        {
            return new Complex(Real, -Imaginary);
        }

        /// <summary>
        /// Returns the modulus of the complex number, |z|
        /// </summary>
        public double Modulus()
        {
            return Util.Hypot(Real, Imaginary);
        }

        public override string ToString()
        {
            return ToString("n4");
        }
        public string ToString(string format)
        {
            if (Imaginary < 0)
            {
                return Real.ToString(format) + " - " + Math.Abs(Imaginary).ToString(format) + "i";
            }
            return Real.ToString(format) + " + " + Imaginary.ToString(format) + "i";
        }

        public Complex Add(Complex e)
        {
            return new Complex(Real + e.Real, Imaginary + e.Imaginary);
        }
        public Complex Multiply(Complex e)
        {
            return new Complex(
                Real * e.Real - Imaginary * e.Imaginary,
                Real * e.Imaginary + Imaginary * e.Real);
        }
        public Complex Multiply(double s)
        {
            // Method provided for performance reasons only
            return new Complex(Real * s, Imaginary * s);
        }
        public Complex Subtract(Complex e)
        {
            return new Complex(Real - e.Real, Imaginary - e.Imaginary);
        }
        public Complex AdditiveInverse()
        {
            return new Complex(-Real, -Imaginary);
        }
        public Complex MultiplicativeInverse()
        {
            double divisor = Real * Real + Imaginary * Imaginary;
            if (divisor == 0)
            {
                return new Complex(double.NaN, double.NaN);
            }
            return new Complex(Real / divisor, -Imaginary / divisor);
        }
        public bool Equals(Complex e)
        {
            return Real == e.Real && Imaginary == e.Imaginary;
        }
        public bool ApproximatelyEquals(Complex e, double eps = Precision.DOUBLE_PRECISION)
        {
            return Util.ApproximatelyEquals(Real, e.Real, eps) && Util.ApproximatelyEquals(Imaginary, e.Imaginary, eps);
        }

        #endregion

        /// <summary>
        /// Evaluate e^z where z is complex
        /// </summary>
        /// <param name="z"></param>
        /// <returns>e^z for complex z</returns>
        public static Complex Exp(Complex z)
        {
            return FromPolar(Math.Exp(z.Real), z.Imaginary);
        }

        public static Complex Sqrt(Complex c)
        {
            return Pow(c, 0.5);
        }

        /// <summary>
        /// Evaluate z^s for some real value s and complex z
        /// </summary>
        /// <param name="z">A complex number z</param>
        /// <param name="power">A scalar representing the power to raise z to.</param>
        /// <returns>Returns z^s</returns>
        public static Complex Pow(Complex z, double power)
        {
            if (power == 0)
            {
                return One;
            }
            if (z.Real == 0 && z.Imaginary == 0)
            {
                return Zero;
            }

            return FromPolar(Math.Pow(z.Modulus(), power), power * z.Argument());
        }

        public static Complex Pow(Complex z, Complex w)
        {
            // z^0 = 1 for all z
            if (w.Real == 0.0 && w.Imaginary == 0)
            {
                return One;
            }
            // 0^w = 0 for all w
            if (z.Real == 0.0 && z.Imaginary == 0)
            {
                return Zero;
            }

            double w_re = w.Real, w_im = w.Imaginary, r = z.Modulus(), theta = z.Argument();
            return FromPolar(
                Math.Pow(r, w_re) * Math.Exp(-w_im * theta),    // modulus of power
                w_re * theta + w_im * Math.Log(r));             // argument of power
        }

        /// <summary>
        /// Returns the natural logarithm of a complex number x, ln(x)
        /// </summary>
        /// <param name="z"></param>
        /// <returns></returns>
        public static Complex Log(Complex z)
        {
            double mod = z.Modulus();
            double arg = z.Argument();

            if (mod == 0)
            {
                return new Complex(double.NaN, double.NaN);
            }

            return new Complex(Math.Log(mod), arg);
        }

        /// <summary>
        /// Returns the sine of a complex number x
        /// </summary>
        /// <param name="z"></param>
        /// <returns></returns>
        public static Complex Sin(Complex z)
        {
            return new Complex(Math.Sin(z.Real) * Math.Cosh(z.Imaginary), Math.Cos(z.Real) * Math.Sinh(z.Imaginary));
        }
        public static Complex Cos(Complex z)
        {
            return new Complex(Math.Cos(z.Real) * Math.Cosh(z.Imaginary), -Math.Sin(z.Real) * Math.Sinh(z.Imaginary));
        }
        public static Complex Tan(Complex z)
        {
            double a2 = z.Real * 2.0, b2 = z.Imaginary * 2.0, den = Math.Cos(a2) + Math.Cosh(b2);
            return new Complex(Math.Sin(a2) / den, Math.Sinh(b2) / den);
        }
        public static Complex Asin(Complex z)
        {
            double re = z.Real, im = z.Imaginary;
            // i * z
            Complex iz = new Complex(-im, re);
            // 1 - z^2
            Complex one_minus_z_sqrd = new Complex(1.0 - re * re + im * im, -2 * re * im);
            // i * w := ln(iz + sqrt(1 - z^2))
            Complex iw = Log(iz.Add(Sqrt(one_minus_z_sqrd)));

            // result = -i * (iw) = (Im(iw), -Re(iw))
            return new Complex(iw.Imaginary, -iw.Real);
        }

        public static bool TryParse(string s, out Complex c)
        {
            if (string.IsNullOrEmpty(s))
            {
                c = Zero;
                return false;
            }
            string[] parts = s.Split('+');
            if (parts.Length != 2)
            {
                c = Zero;
                return false;
            }

            string im = parts[1];
            if (!im.EndsWith("i"))
            {
                c = Zero;
                return false;
            }
            im = im.Substring(0, im.Length - 1);

            double _re, _im;
            if (double.TryParse(parts[0].Trim(), out _re) && double.TryParse(im.Trim(), out _im))
            {
                c = new Complex(_re, _im);
                return true;
            }

            c = Zero;
            return false;
        }

        public static bool IsNaN(Complex c)
        {
            return double.IsNaN(c.Real) || double.IsNaN(c.Imaginary);
        }
        public static bool IsInfinity(Complex c)
        {
            return double.IsInfinity(c.Real) || double.IsInfinity(c.Imaginary);
        }
        public static bool IsNaNOrInfinity(Complex c)
        {
            return IsNaN(c) || IsInfinity(c);
        }

        #region Operator overrides 

        public static Complex operator +(Complex c1, Complex c2)
        {
            return c1.Add(c2);
        }
        public static Complex operator -(Complex c1, Complex c2)
        {
            return c1.Subtract(c2);
        }
        public static Complex operator *(Complex c1, Complex c2)
        {
            return c1.Multiply(c2);
        }
        public static Complex operator -(Complex c)
        {
            return c.AdditiveInverse();
        }
        public static Complex operator /(Complex c1, Complex c2)
        {
            return c1.Divide(c2);
        }

        public static implicit operator Complex(byte b) => new Complex(b);
        public static implicit operator Complex(short i) => new Complex(i);
        public static implicit operator Complex(ushort i) => new Complex(i);
        public static implicit operator Complex(int i) => new Complex(i);
        public static implicit operator Complex(uint i) => new Complex(i);
        public static implicit operator Complex(long l) => new Complex(l);
        public static implicit operator Complex(ulong l) => new Complex(l);
        public static implicit operator Complex(float f) => new Complex(f);
        public static implicit operator Complex(double d) => new Complex(d);
        public static implicit operator Complex(decimal d) => new Complex((double)d);
        public static implicit operator Complex(System.Numerics.Complex c) => new Complex(c.Real, c.Imaginary);
        public static implicit operator Complex(System.Numerics.BigInteger i) => new Complex((double)i);

        #endregion

        #region internal modifiers
        internal void IncrementBy(Complex e)
        {
            Real += e.Real;
            Imaginary += e.Imaginary;
        }
        internal void DecrementBy(Complex e)
        {
            Real -= e.Real;
            Imaginary -= e.Imaginary;
        }
        internal void MultiplyEquals(double s)
        {
            Real *= s;
            Imaginary *= s;
        }
        internal void MultiplyEquals(Complex e)
        {
            double re = Real;
            Real = re * e.Real - Imaginary * e.Imaginary;
            Imaginary = re * e.Imaginary + Imaginary * e.Real;
        }
        internal void Negate()
        {
            Real = -Real;
            Imaginary = -Imaginary;
        }
        internal void Set(double real, double imaginary)
        {
            Real = real;
            Imaginary = imaginary;
        }

        #endregion internal modifiers
    }
    public static class ComplexMethodsExtensions
    {
        /// <summary>
        /// Returns whether a complex number is the conjugate of another, up to 'tolerance'
        /// </summary>
        /// <param name="c"></param>
        /// <param name="other"></param>
        /// <param name="tolerance">Defaults to 1e-8</param>
        /// <returns>true if the two complex numbers are complex conjugates of each other.</returns>
        public static bool IsConjugateOf(this Complex c, Complex other, double tolerance = 1e-8)
        {
            return Util.ApproximatelyEquals(c.Real, other.Real, tolerance) && 
                Util.ApproximatelyEquals(c.Imaginary, -other.Imaginary, tolerance); 
        }

        /// <summary>
        /// Determines whether the complex number is real within 'tolerance', i.e. |Im(z)| < tolerance
        /// </summary>
        /// <param name="tolerance">The tolerance, defaults to 1e-8</param>
        /// <returns>true if the complex number is real</returns>
        public static bool IsReal(this Complex c, double tolerance = 1e-8)
        {
            return Util.ApproximatelyEquals(c.Imaginary, 0, tolerance);
        }

        /// <summary>
        /// Determines whether the complex number is purely imaginary within 'tolerance', i.e. |Re(z)| < tolerance
        /// </summary>
        /// <param name="tolerance">The tolerance, defaults to 1e-8</param>
        /// <returns>true if the complex number is purely imaginary</returns>
        public static bool IsImaginary(this Complex c, double tolerance = 1e-8)
        {
            return Util.ApproximatelyEquals(c.Real, 0, tolerance);
        }

        internal static Complex Random(Random _random, double maxModulus)
        {
            return Complex.FromPolar(_random.NextDouble() * maxModulus, _random.NextDouble() * Math.PI * 2);
        }
    }
}
