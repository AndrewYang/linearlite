﻿using LinearNet.Global;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Structs.Fields
{
    /// <summary>
    /// This struct represents a complex number where both real and imaginary components are float (single) types.
    /// See also: Complex
    /// </summary>
    public struct ComplexF : IComplex<float>, IField<ComplexF>
    {
        public float Real, Imaginary;

        public static readonly ComplexF One = new ComplexF(1.0f);
        public static readonly ComplexF Zero = new ComplexF(0.0f);
        public static readonly ComplexF I = new ComplexF(0.0f, 1.0f);

        public ComplexF MultiplicativeIdentity => One;
        public ComplexF AdditiveIdentity => Zero;
        public BigInteger Characteristic => BigInteger.Zero;

        public ComplexF(float real, float imaginary)
        {
            Real = real;
            Imaginary = imaginary;
        }
        public ComplexF(float real)
        {
            Real = real;
            Imaginary = 0.0f;
        }

        #region Public methods

        public float Argument()
        {
            if (Real == 0.0f)
            {
                if (Imaginary == 0.0f) return float.NaN;
                if (Imaginary > 0.0f) return Constants.PiOver2_f;
                return -Constants.PiOver2_f;
            }
            return (float)Math.Atan2(Imaginary, Real);
        }
        public float Modulus()
        {
            return Util.Hypot(Real, Imaginary);
        }

        public ComplexF MultiplicativeInverse()
        {
            float divisor = Real * Real + Imaginary * Imaginary;
            if (divisor == 0)
            {
                return new ComplexF(float.NaN, float.NaN);
            }
            return new ComplexF(Real / divisor, -Imaginary / divisor);
        }
        public ComplexF AdditiveInverse()
        {
            return new ComplexF(-Real, -Imaginary);
        }
        public ComplexF Multiply(ComplexF e)
        {
            return new ComplexF(Real * e.Real - Imaginary * e.Imaginary,
                Real * e.Imaginary + Imaginary * e.Real);
        }
        public ComplexF Add(ComplexF g)
        {
            return new ComplexF(Real + g.Real, Imaginary + g.Imaginary);
        }
        public ComplexF Subtract(ComplexF e)
        {
            return new ComplexF(Real - e.Real, Imaginary - e.Imaginary);
        }
        public bool Equals(ComplexF e)
        {
            return Real == e.Real && Imaginary == e.Imaginary;
        }
        public bool ApproximatelyEquals(ComplexF e, double eps)
        {
            return Util.ApproximatelyEquals(Real, e.Real, eps) &&
                Util.ApproximatelyEquals(Imaginary, e.Imaginary, eps);
        }

        #endregion

        #region Operator overrides

        public static ComplexF operator +(ComplexF w, ComplexF z) => w.Add(z);
        public static ComplexF operator -(ComplexF w, ComplexF z) => w.Subtract(z);
        public static ComplexF operator -(ComplexF w) => w.AdditiveInverse();
        public static ComplexF operator *(ComplexF w, ComplexF z) => w.Multiply(z);
        public static ComplexF operator /(ComplexF w, ComplexF z) => w.Divide(z);

        public static implicit operator ComplexF(byte b) => new ComplexF(b);
        public static implicit operator ComplexF(short b) => new ComplexF(b);
        public static implicit operator ComplexF(ushort b) => new ComplexF(b);
        public static implicit operator ComplexF(int b) => new ComplexF(b);
        public static implicit operator ComplexF(uint b) => new ComplexF(b);
        public static implicit operator ComplexF(long b) => new ComplexF(b);
        public static implicit operator ComplexF(ulong b) => new ComplexF(b);
        public static implicit operator ComplexF(double b) => new ComplexF((float)b);
        public static implicit operator ComplexF(decimal b) => new ComplexF((float)b);
        public static implicit operator ComplexF(System.Numerics.Complex b) => new ComplexF((float)b.Real, (float)b.Imaginary);
        public static implicit operator ComplexF(System.Numerics.BigInteger b) => new ComplexF((float)b);
        
        #endregion
    }
}
