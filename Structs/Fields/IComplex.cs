﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Structs.Fields
{
    /// <summary>
    /// Represents a complex object over type T
    /// E.g. T may be double, float, decimal etc.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IComplex<T>
    {
        T Modulus();
        T Argument();
    }
}
