﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Structs
{
    /// <summary>
    /// Implements a Field structure
    /// 
    /// The following methods are inherited from Group<T>:
    /// T AdditiveIdentity { get; }
    /// T Add(T e);
    /// T AdditiveInverse();
    /// 
    /// The following methods are inherited from IRing<T>:
    /// T MultiplicativeIdentity { get; }
    /// T Multiply(T e);
    /// 
    /// The following methods are inherited from DivisionRing<T>
    /// T MultiplicativeInverse();
    /// 
    /// As with DivisionRing<T>, IField<T> implements an 
    /// multiplicative inverse method (and subsequently a 
    /// 'division' method) which is not present in IRing<T>
    /// 
    /// Unlike DivisionRing<T>, IField<T> multiplication must be 
    /// commutative. This is never enforced in the interface 
    /// level, and we leave it up to the user to ensure that 
    /// their IField<T> multiplication implementations do in fact 
    /// commute. Failure to do so may result in unexpected results 
    /// when using IField<T> methods.
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <cat>linear-algebra</cat>
    public interface IField<T> : IDivisionRing<T>, ICommutativeRing<T> where T : new()
    {
        BigInteger Characteristic { get; }
    }

}
