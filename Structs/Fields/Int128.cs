﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;

namespace LinearNet.Structs.Fields
{
    public struct Int128 : IField<Int128>
    {
        private readonly long _high;
        private readonly ulong _low;

        public BigInteger Characteristic => throw new NotImplementedException();
        public Int128 MultiplicativeIdentity => throw new NotImplementedException();
        public Int128 AdditiveIdentity => throw new NotImplementedException();

        public Int128(long high, ulong low)
        {
            _high = high;
            _low = low;
        }

        public Int128 Add(Int128 g)
        {
            long h = _high + g._high;
            ulong l = _low + g._low;
            if (l < _low || l < g._low)
            {
                ++h;
            }
            return new Int128(h, l);
        }

        public Int128 AdditiveInverse()
        {
            return new Int128(-_high, _low);
        }

        public bool ApproximatelyEquals(Int128 e, double eps)
        {
            return Equals(e);
        }

        public bool Equals(Int128 e)
        {
            return _high == e._high && _low == e._low;
        }

        public Int128 MultiplicativeInverse()
        {
            throw new NotImplementedException();
        }

        public Int128 Multiply(Int128 b)
        {
            throw new NotImplementedException();
        }
    }
}
