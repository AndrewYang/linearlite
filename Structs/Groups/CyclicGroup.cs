﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Structs
{
    /// <summary>
    /// Implements (an element of) the cyclic group Z(n) for some integer n, consisting of the set of integers
    /// {0, 1, 2, ..., n - 1}. The group operator is addition.
    /// Cyclic groups or order n have the property that g^n = e for any g in G
    /// </summary>
    public class CyclicGroupElement : IGroup<CyclicGroupElement>
    {
        public CyclicGroupElement Identity => new CyclicGroupElement(0, Order);

        /// <summary>
        /// The element's value (possible values are in the set {1, 2, ..., order - 1}).
        /// </summary>
        public int Element { get; set; }

        /// <summary>
        /// The order of the group
        /// </summary>
        public int Order { get; set; }

        /// <summary>
        /// Constructor for a element of the cyclic group Z(order), having value e which 
        /// must be in the set {0, 1, ..., order - 1}.
        /// </summary>
        /// <param name="e"></param>
        /// <param name="order"></param>
        public CyclicGroupElement(int e, int order)
        {
            if (e >= order || e < 0)
            {
                throw new ArgumentOutOfRangeException();
            }
            if (order <= 0)
            {
                throw new ArgumentException();
            }

            Element = e;
            Order = order;
        }

        /// <summary>
        /// The default cyclic group is the degenerate cyclic group of order 1.
        /// </summary>
        public CyclicGroupElement()
        {
            Element = 0;
            Order = 1;
        }

        /// <summary>
        /// Returns whether (this) is approximately equal to the same group element
        /// as e, subject to some precision level eps.
        /// Note that as cyclic groups are exact, which method is equivalent to 
        /// Equals(e).
        /// </summary>
        /// <param name="e"></param>
        /// <param name="eps"></param>
        /// <returns></returns>
        public bool ApproximatelyEquals(CyclicGroupElement e, double eps)
        {
            return Element == e.Element;
        }

        /// <summary>
        /// Returns whether (this) is equal to the same group element as e
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        public bool Equals(CyclicGroupElement e)
        {
            return Element == e.Element;
        }

        /// <summary>
        /// Calculates and returns (this) + e, which is another element of the same cyclic group
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        public CyclicGroupElement Operate(CyclicGroupElement e)
        {
            if (Order != e.Order)
            {
                throw new InvalidOperationException();
            }
            return new CyclicGroupElement((Element + e.Element) % Order, Order);
        }

        /// <summary>
        /// Returns the element from the same group, e, such that (this) + e = identity
        /// </summary>
        /// <returns></returns>
        public CyclicGroupElement OperatorInverse()
        {
            return new CyclicGroupElement((-Element + Order) % Order, Order);
        }
    }
}
