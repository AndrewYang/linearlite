﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Structs
{
    /// <summary>
    /// <p>
    /// Interface for the algebraic structure of an abelian group, which is 
    /// a group with a commutative operation $*$, i.e. 
    /// $a * b = b * a$ for any $a, b \in T$.
    /// </p>
    /// 
    /// <p>
    /// We leave the enforcement of commutativity to the user implementation of 
    /// <txt>Operate(T e)</txt> method. For performance reasons no checks are performed 
    /// for commutativity during the execution of methods in this class.
    /// </p>
    /// 
    /// <p>
    /// Please note that if <txt>Operate(T e)</txt> is not 
    /// commutative, then unexpected/undefined results may be produced by the methods that rely 
    /// on this property.
    /// </p>
    /// </summary>
    /// <cat>linear-algebra</cat>
    public interface IAbelianGroup<T> : IGroup<T> where T : new()
    {

    }
}
