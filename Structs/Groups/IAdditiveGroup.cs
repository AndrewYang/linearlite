﻿namespace LinearNet.Structs
{
    /// <summary>
    /// <p>
    /// Interface for a group $G = (S, +)$ where $S$ is a set of elements of type $T$
    /// This group does not extend the <txt>Group<T></txt> interface since the method has 
    /// different names.
    /// </p>
    /// </summary>
    /// <cat>linear-algebra</cat>
    public interface IAdditiveGroup<T> : IAdditiveMonoid<T> where T : new()
    {
        /// <summary>
        /// <p>Returns the additive inverse of this element.</p>
        /// <p>Guaranteed to exist since every element of a group has an inverse.</p>
        /// </summary>
        /// <returns>The additive inverse of this element.</returns>
        T AdditiveInverse();
    }

    /// <cat>linear-algebra</cat>
    public static class AdditiveGroupExtensions
    {
        /// <summary>
        /// Subtracts the second element from the first element, where both elements belong to an additive group.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="e">The first element.</param>
        /// <param name="f">The second element.</param>
        /// <returns>The difference between the two elements.</returns>
        public static T Subtract<T>(this T e, T f) where T : IAdditiveGroup<T>, new()
        {
            return e.Add(f.AdditiveInverse());
        }
    }
}
