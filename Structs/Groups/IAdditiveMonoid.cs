﻿using LinearNet.Structs;

namespace LinearNet.Structs
{
    /// <summary>
    /// Interface for a monoid structure whose binary operation is addition.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <cat>linear-algebra</cat>
    public interface IAdditiveMonoid<T> : IAdditiveSemigroup<T> where T : new()
    {
        /// <summary>
        /// The identity element $e$, which satisfies $e + g = g + e = g$ for any element $g$ in set $T$.
        /// </summary>
        T AdditiveIdentity { get; }
    }
}
