﻿using System;

namespace LinearNet.Structs
{
    /// <summary>
    /// Implements an additive semigroup structure, which is a <a href="SemiGroup.html">semigroup</a>
    /// with addition as its binary operation. 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <cat>linear-algebra</cat>
    public interface IAdditiveSemigroup<T> : IAlgebra<T> where T : new()
    {
        /// <summary>
        /// <p>The binary operation $+: T \times T \to T$.</p>
        /// <p>Calculate and returns (this) $* g$ for some element $g$ in the set $T$.</p>
        /// <p>Note that as this is a semigroup, the binary operation must be associative.</p>
        /// </summary>
        /// <param name="g">Some element from the set T</param>
        /// <returns>The result of the addition</returns>
        T Add(T g);
    }
}
