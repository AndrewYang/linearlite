﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Structs
{
    /// <summary>
    /// Interface for the group $(T, *)$ over a set $T$ and a binary operation $* : T * T \to T$, 
    /// for which a inverse exists for every element in $T$, and an identity element exists. 
    /// 
    /// <p>A group is hence also:</p>
    /// <ul>
    /// <li>A monoid with an inverse.</li>
    /// <li>A semigroup with an identity element and an inverse.</li>
    /// </ul>
    /// 
    /// <p>
    /// This interface is inherited by all commonly used algebraic interfaces <txt>IRing<T></txt> (for rings) and 
    /// <txt>IField<T></txt> (for fields), but not <txt>IAdditiveGroup<T></txt> (for additive groups) due to method name clashes.
    /// </p>
    /// 
    /// </summary>
    /// <remarks>
    /// 
    /// The following methods are inherited from Algebra<T>:
    /// - Equals(T e);
    /// - ApproximatelyEquals(T e);
    /// 
    /// The following methods are inherited from Semigroup<T>
    /// - Operate(T e);
    /// 
    /// The following methods are inherited from Monoid<T>
    /// - T Identity { get; }
    /// 
    /// </remarks>
    /// <typeparam name="T"></typeparam>
    /// <cat>linear-algebra</cat>
    public interface IGroup<T> : IMonoid<T> where T : new()
    {
        /// <summary>
        /// Calculates the inversion operation of (this) 
        /// </summary>
        /// <returns></returns>
        T OperatorInverse();
    }
}
