﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Structs
{
    /// <summary>
    /// <p>
    /// Implements a monoid algebraic structure over the set $T$, and operation $* : T \times T \to T$
    /// ($T$ is closed under $*$), plus an identity element $e \in T$, such that $e * g = g * e = g$
    /// for every element $g \in T$.
    /// </p>
    /// <p>
    /// The operation is not necessarily commutative. A monoid is a semigroup with an identity element.
    /// </p>
    /// </summary>
    /// <remarks>
    /// The following methods are inherited from Semigroup:
    /// T Operate(T e);
    /// </remarks>
    /// <typeparam name="T"></typeparam>
    /// <cat>linear-algebra</cat>
    public interface IMonoid<T> : ISemigroup<T> where T : new()
    {
        /// <summary>
        /// The identity element $e$, for operation $*$, which satisfies 
        /// $e * g = g * e = g$ for any element $g$ in $T$.
        /// </summary>
        T Identity { get; }
    }
}
