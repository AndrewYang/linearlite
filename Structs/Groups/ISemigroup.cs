﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Structs
{
    /// <summary>
    /// Interface for an object representing the semigroup algebraic structure $(T, *)$ 
    /// where $T$ is a set and $*$ is the associative binary operation $*:T \times T \to T$.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <cat>linear-algebra</cat>
    public interface ISemigroup<T>  : IAlgebra<T> where T : new()
    {
        /// <summary>
        /// The binary operation * : T x T -> T
        /// Calculate and returns (this) * g for some element g in the set T.
        /// 
        /// Note that in semigroups, the binary operation is associative.
        /// </summary>
        /// <param name="g">Some element from the set T</param>
        /// <returns>The result of the binary operation</returns>
        T Operate(T g);
    }
}
