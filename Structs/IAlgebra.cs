﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Structs
{
    /// <summary>
    /// Interface for all algebraic structures.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <cat>linear-algebra</cat>
    public interface IAlgebra<T> where T : new()
    {
        /// <summary>
        /// Returns true if (this) is equal to e
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        bool Equals(T e);

        /// <summary>
        /// Returns true if (this) is deemed approximately equal to e, up to 
        /// the specified level machine precision, eps
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        bool ApproximatelyEquals(T e, double eps);
    }
}
