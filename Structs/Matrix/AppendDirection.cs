﻿
namespace LinearNet.Structs
{
    public enum AppendDirection
    {
        TOP, BOTTOM, LEFT, RIGHT,
        TOPLEFT, TOPRIGHT, BOTTOMLEFT, BOTTOMRIGHT
    }
}
