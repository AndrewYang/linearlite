﻿using LinearNet.Global;
using LinearNet.Matrices.InducedNorm;
using LinearNet.Providers;
using LinearNet.Providers.Managed;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Numerics;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Runtime.Versioning;
using System.Text;
using System.Threading;

namespace LinearNet.Structs.Matrix
{
    /// <summary>
    /// Matrix storage structure representing a block-sparse matrix. Suitable for representing sparse matrices 
    /// whose non-zero entries lie inside dense blocks of the same size. 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <cat>linear-algebra</cat>
    public sealed class BSRSparseMatrix<T> : Matrix<T> where T : new()
    {
        private int _blockHeight, _blockWidth;
        private int[] _rowIndices, _columnIndices;
        private T[] _values;
        private readonly T _zero;
        
        public override T[] this[int index] 
        {
            get
            {
                if (index < 0 || index >= Rows)
                {
                    throw new ArgumentOutOfRangeException(nameof(index));
                }

                T[] row = RectangularVector.Zeroes<T>(Columns);

                int blockRowIndex = index / _blockHeight,
                    end = _rowIndices[blockRowIndex + 1],
                    r = index % _blockHeight,
                    offset = blockRowIndex * _blockHeight * _blockWidth + r * _blockWidth;

                for (int k = _rowIndices[blockRowIndex]; k < end; ++k)
                {
                    int colOffset = _columnIndices[k] * _blockWidth;
                    for (int j = 0; j < _blockWidth; ++j)
                    {
                        row[colOffset + j] = _values[offset + j];
                    }
                }

                return row;
            }
            set => throw new NotImplementedException(); 
        }

        public override T this[int rowIndex, int columnIndex] 
        {
            get 
            { 
                if (rowIndex < 0 || rowIndex >= Rows)
                {
                    throw new ArgumentOutOfRangeException(nameof(rowIndex));
                }
                if (columnIndex < 0 || columnIndex >= Columns)
                {
                    throw new ArgumentOutOfRangeException(nameof(columnIndex));
                }

                int blockRowIndex = rowIndex / _blockHeight,
                    blockColumnIndex = columnIndex / _blockWidth;

                // Find the block
                int start = _rowIndices[blockRowIndex], end = _rowIndices[blockRowIndex + 1];
                int index = Array.BinarySearch(_columnIndices, start, end - start, blockColumnIndex);

                if (index < 0)
                {
                    return _zero;
                }

                int offset = index * _blockHeight * _blockWidth,
                    r = rowIndex - blockRowIndex * _blockHeight,
                    c = columnIndex - blockColumnIndex * _blockWidth;

                return _values[offset + r * _blockWidth + c];
            }
            set
            {
                if (rowIndex < 0 || rowIndex >= Rows)
                {
                    throw new ArgumentOutOfRangeException(nameof(rowIndex));
                }
                if (columnIndex < 0 || columnIndex >= Columns)
                {
                    throw new ArgumentOutOfRangeException(nameof(columnIndex));
                }

                int blockRowIndex = rowIndex / _blockHeight,
                    blockColumnIndex = columnIndex / _blockWidth;

                // Find the block
                int start = _rowIndices[blockRowIndex], end = _rowIndices[blockRowIndex + 1];
                int index = Array.BinarySearch(_columnIndices, start, end - start, blockColumnIndex);

                if (index < 0)
                {
                    // Insert new block
                    int cIndex = -1;
                    for (int i = start; i < end; ++i)
                    {
                        if (_columnIndices[i] > blockColumnIndex)
                        {
                            cIndex = i;
                            break;
                        }
                    }
                    if (cIndex < 0)
                    {
                        cIndex = end;
                    }

                    // Extend the _columnIndices array
                    int blocks = _rowIndices[Rows / _blockHeight];
                    int clen = blocks + 1;
                    if (_columnIndices.Length < clen)
                    {
                        Array.Resize(ref _columnIndices, clen);
                    }
                    for (int i = clen - 1; i > cIndex; --i)
                    {
                        _columnIndices[i] = _columnIndices[i - 1];
                    }
                    _columnIndices[cIndex] = blockColumnIndex;

                    // Extend the _values array
                    int bsize = _blockHeight * _blockWidth;
                    long vlen = clen * bsize;
                    if (vlen > int.MaxValue)
                    {
                        throw new OverflowException("The matrix is too large to be stored in block-Compressed-Sparse-Row format.");
                    }
                    if (_values.Length < vlen)
                    {
                        Array.Resize(ref _values, (int)vlen);
                    }
                    for (int i = clen - 1; i > cIndex; --i)
                    {
                        Array.Copy(_values, (i - 1) * bsize, _values, i * bsize, bsize);
                    }
                    // Clear the block
                    for (int b = 0; b < bsize; ++b)
                    {
                        _values[cIndex * bsize + b] = _zero;
                    }
                    // Add the new value
                    int offset = cIndex * _blockHeight * _blockWidth,
                        r = rowIndex - blockRowIndex * _blockHeight,
                        c = columnIndex - blockColumnIndex * _blockWidth;
                    _values[offset + r * _blockWidth + c] = value;

                    // Update the rowIndex array
                    for (int i = blockRowIndex + 1; i <= Rows; ++i)
                    {
                        _rowIndices[i]++;
                    }
                }
                else
                {
                    int offset = index * _blockHeight * _blockWidth,
                        r = rowIndex - blockRowIndex * _blockHeight,
                        c = columnIndex - blockColumnIndex * _blockWidth;

                    _values[offset + r * _blockWidth + c] = value;
                }
            }
        }

        public override IEnumerable<MatrixEntry<T>> NonZeroEntries
        {
            get
            {
                for (int r = 0, rlen = _rowIndices.Length - 1, k = 0; r < rlen; ++r)
                {
                    int offset_r = r * _blockHeight, 
                        end = _rowIndices[r + 1],
                        block_end_r = offset_r + _blockHeight;

                    for (int i = _rowIndices[r]; i < end; ++i)
                    {
                        int offset_c = _columnIndices[i] * _blockWidth,
                            block_end_c = offset_c + _blockWidth;

                        // Iterate within the block
                        for (int bi = offset_r; bi < block_end_r; ++bi)
                        {
                            for (int bj = offset_c; bj < block_end_c; ++bj)
                            {
                                yield return new MatrixEntry<T>(bi, bj, _values[k++]);
                            }
                        }
                    }
                }
            }
        }

        public override int SymbolicNonZeroCount =>
            // This is more accurate that just _values.Length
            _rowIndices[Rows / _blockHeight] * _blockHeight * _blockWidth; 

        #region Constructors

        public BSRSparseMatrix(int rows, int columns, int blockWidth, int blockHeight, int[] rowIndices, int[] columnIndices, T[] values)
            : base(MatrixStorageType.BCSR)
        {
            if (rows <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(rows), ExceptionMessages.NonPositiveNotAllowed);
            }
            if (columns <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(columns), ExceptionMessages.NonPositiveNotAllowed);
            }
            if (blockWidth <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(blockWidth), ExceptionMessages.NonPositiveNotAllowed);
            }
            if (blockHeight <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(blockHeight), ExceptionMessages.NonPositiveNotAllowed);
            }
            if (columns % blockWidth != 0)
            {
                throw new ArgumentOutOfRangeException(nameof(blockWidth), $"The number of columns is not a multiple of '{nameof(blockWidth)}'");
            }
            if (rows % blockHeight != 0)
            {
                throw new ArgumentOutOfRangeException(nameof(blockHeight), $"The number of rows is not a multiple of '{nameof(blockHeight)}'");
            }
            if (rowIndices is null)
            {
                throw new ArgumentNullException(nameof(rowIndices));
            }
            if (columnIndices is null)
            {
                throw new ArgumentNullException(nameof(columnIndices));
            }
            if ((rowIndices.Length - 1) * blockHeight != rows)
            {
                throw new ArgumentOutOfRangeException(nameof(rowIndices), $"Expected '{nameof(rowIndices)}' to be of length {rows / blockHeight + 1}, received {rowIndices.Length}.");
            }
            if (values is null)
            {
                throw new ArgumentNullException(nameof(values));
            }
            if (values.Length != columnIndices.Length * blockHeight * blockWidth)
            {
                throw new ArgumentOutOfRangeException(nameof(values), $"Incorrect length of '{nameof(values)}' array: expected {columnIndices.Length * blockHeight * blockWidth}.");
            }
            ThrowHelper.AssertCollectionInRange(rowIndices, 0, columnIndices.Length, nameof(rowIndices));
            ThrowHelper.AssertCollectionInRange(columnIndices, 0, columns / blockWidth, nameof(columnIndices));
            ThrowHelper.AssertMonotonicIncreasing(rowIndices, nameof(rowIndices));
            ThrowHelper.AssertStrictlyIncreasingWithinRange(columnIndices, rowIndices, nameof(columnIndices));


            Rows = rows;
            Columns = columns;
            _zero = new T();

            _blockHeight = blockHeight;
            _blockWidth = blockWidth;
            _rowIndices = rowIndices;
            _columnIndices = columnIndices;
            _values = values;
        }

        /// <summary>
        /// Converts a matrix into block-sparse format, with each block having dimensions <txt>blockWidth</txt> x <txt>blockHeight</txt>.
        /// </summary>
        /// <param name="matrix">The matrix to convert into block-sparse format.</param>
        /// <param name="blockWidth">The width of each non-zero block.</param>
        /// <param name="blockHeight">The height of each non-zero block.</param>
        public BSRSparseMatrix(Matrix<T> matrix, int blockWidth, int blockHeight) : base(MatrixStorageType.BCSR)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            if (blockWidth < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(blockWidth), ExceptionMessages.NonPositiveNotAllowed);
            }
            if (blockHeight < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(blockHeight), ExceptionMessages.NonPositiveNotAllowed);
            }
            if (matrix.Rows % blockHeight != 0)
            {
                throw new ArgumentOutOfRangeException(nameof(blockHeight), "The number of rows is not divisible by the block height.");
            }
            if (matrix.Columns % blockWidth != 0)
            {
                throw new ArgumentOutOfRangeException(nameof(blockWidth), "The number of columns is not divisible by the block width.");
            }

            Rows = matrix.Rows;
            Columns = matrix.Columns;
            _zero = new T();

            _blockWidth = blockWidth;
            _blockHeight = blockHeight;

            int blockRows = Rows / _blockHeight,
                blockColumns = Columns / _blockWidth;

            _rowIndices = new int[blockRows + 1];

            // use a bitarray to keep track of which blocks to include
            BitArray include = new BitArray(blockRows * blockColumns);

            foreach (MatrixEntry<T> e in matrix.NonZeroEntries)
            {
                int br = e.RowIndex / _blockHeight,
                    bc = e.ColumnIndex / _blockWidth;
                include[br * blockColumns + bc] = true;
            }

            for (int r = 0, k = 0; r < blockRows; ++r)
            {
                int count = 0;
                for (int c = 0; c < blockColumns; ++c)
                {
                    if (include[k++]) ++count;
                }
                _rowIndices[r + 1] = _rowIndices[r] + count;
            }

            int bsize = _blockWidth * _blockHeight;
            _columnIndices = new int[_rowIndices[blockRows]];
            _values = RectangularVector.Zeroes<T>(_columnIndices.Length * bsize);

            for (int r = 0, k = 0, ci = 0; r < blockRows; ++r)
            {
                for (int c = 0; c < blockColumns; ++c)
                {
                    if (include[k++])
                    {
                        _columnIndices[ci++] = c;
                    }
                }
            }

            // Entries are likely to be close together, i.e. in the same block 
            // as the last entry. So we cache the last block (and the associated 
            // index within _columnIndices) for reuse
            int last_br = -1, last_bc = -1, last_index = -1;
            foreach (MatrixEntry<T> e in matrix.NonZeroEntries)
            {
                int br = e.RowIndex / _blockHeight,
                    bc = e.ColumnIndex / _blockWidth,
                    r = e.RowIndex % _blockHeight,
                    c = e.ColumnIndex % _blockWidth;

                int index;
                if (br == last_br && bc == last_bc)
                {
                    index = last_index;
                }
                else
                {
                    int rstart = _rowIndices[br], rend = _rowIndices[br + 1];
                    index = Array.BinarySearch(_columnIndices, rstart, rend - rstart, bc);

                    // Update cache
                    last_bc = bc;
                    last_br = br;
                    last_index = index;
                }

                _values[index * bsize + r * _blockWidth + c] = e.Value;
            }
        }
       
        #endregion

        private int[] sym_union_row_index_matching_blocks(BSRSparseMatrix<T> matrix)
        {
            int blockRows = Rows / _blockHeight;
            int[] rowIndices = new int[blockRows + 1];
            for (int ri = 0; ri < blockRows; ++ri)
            {
                int end1 = _rowIndices[ri + 1],
                    end2 = matrix._rowIndices[ri + 1],
                    i1 = _rowIndices[ri],
                    i2 = matrix._rowIndices[ri],
                    intersect = 0;

                // If we're being smart about this, we just need to count the intersection
                // instead of the union - slightly faster
                while (i1 < end1 && i2 < end2)
                {
                    int c1 = _columnIndices[i1], c2 = matrix._columnIndices[i2];
                    if (c1 > c2)
                    {
                        ++i2;
                    }
                    else if (c1 < c2)
                    {
                        ++i1;
                    }
                    else
                    {
                        ++i1;
                        ++i2;
                    }
                    ++intersect;
                }

                int nnz = (end1 - _rowIndices[ri]) + (end2 - matrix._rowIndices[ri]) - intersect;
                rowIndices[ri + 1] = rowIndices[ri] + nnz;
            }
            return rowIndices;
        }

        private BSRSparseMatrix<T> add_unmatching_blocks(BSRSparseMatrix<T> matrix, IDenseBLAS1<T> b)
        {
            // Calculate the sum block height and width
            int bWidth = Util.GCD(_blockWidth, matrix._blockWidth),
                bHeight = Util.GCD(_blockHeight, matrix._blockHeight),
                blockRows = Rows / bHeight,
                bsize1 = _blockWidth * _blockHeight,
                bsize2 = matrix._blockWidth * matrix._blockHeight;

            int[] rowIndices = new int[blockRows + 1];

            List<int> columnIndices = new List<int>(
                Math.Max(
                    _rowIndices[Rows / _blockHeight], 
                    matrix._rowIndices[Rows / matrix._blockHeight]));

            List<T> values = new List<T>(
                Math.Max(
                    SymbolicNonZeroCount,
                    matrix.SymbolicNonZeroCount));

            T[] workspace1 = new T[bWidth], workspace2 = new T[bWidth];

            for (int ri = 0; ri < blockRows; ++ri)
            {
                // The actual row index that we are processing 
                int r = ri * bHeight;

                int r1 = r / _blockHeight, // The row index from matrix 1 that we are processing
                    top1 = r % _blockHeight, // The first row within a block from matrix 1 to process
                    r_end1 = _rowIndices[r1 + 1], // The index of the last column index from matrix 1
                    r_start1 = _rowIndices[r1], // The index of the first column index from matrix 1

                    r2 = r / matrix._blockHeight, // The row index from matrix 2 that we are processing
                    top2 = r % matrix._blockHeight, // The first row within a block from matrix 2 to process
                    r_end2 = matrix._rowIndices[r2 + 1],
                    r_start2 = matrix._rowIndices[r2],

                    i1 = r_start1, // iterators
                    i2 = r_start2,

                    left1 = 0, // offset marker for the current column within a block in matrix 1
                    left2 = 0; // offset market for the current column within a block in matrix 2

                while (i1 < r_end1 || i2 < r_end2)
                {
                    // The literal column indexes of the left-most entry in the next block for each matrix
                    int c1 = i1 < r_end1 ? _columnIndices[i1] * _blockWidth + left1 : int.MaxValue,
                        c2 = i2 < r_end2 ? matrix._columnIndices[i2] * matrix._blockWidth + left2 : int.MaxValue;

                    if (c1 < c2)
                    {
                        // Process block from matrix 1 only
                        int k1 = i1 * bsize1 + top1 * _blockWidth + left1;
                        for (int i = 0; i < bHeight; ++i)
                        {
                            for (int j = 0; j < bWidth; ++j)
                            {
                                values.Add(_values[k1 + j]);
                            }
                            k1 += _blockWidth;
                        }
                        columnIndices.Add(c1 / bWidth);

                        left1 += bWidth;
                        if (left1 == _blockWidth) // carry
                        {
                            ++i1;
                            left1 = 0;
                        }
                    }
                    else if (c1 > c2)
                    {
                        // Process block for matrix 2 only
                        int k2 = i2 * bsize2 + top2 * matrix._blockWidth + left2;
                        for (int i = 0; i < bHeight; ++i)
                        {
                            for (int j = 0; j < bWidth; ++j)
                            {
                                values.Add(matrix._values[k2 + j]);
                            }
                            k2 += matrix._blockWidth;
                        }
                        columnIndices.Add(c2 / bWidth);

                        left2 += bWidth;
                        if (left2 == matrix._blockWidth)
                        {
                            // Carry
                            left2 = 0;
                            ++i2;
                        }
                    }
                    else
                    {
                        // Calculate the sum of block from matrix 1 and 2
                        int k1 = i1 * bsize1 + top1 * _blockWidth + left1,
                            k2 = i2 * bsize2 + top2 * matrix._blockWidth + left2;
                        for (int i = 0; i < bHeight; ++i)
                        {
                            Array.Copy(_values, k1, workspace1, 0, bWidth);
                            Array.Copy(matrix._values, k2, workspace2, 0, bWidth);
                            b.ADD(workspace1, workspace2, workspace1, 0, bWidth);
                            values.AddRange(workspace1);
                            k1 += bWidth;
                            k2 += bWidth;
                        }

                        columnIndices.Add(c1 / bWidth);

                        left1 += bWidth;
                        if (left1 == _blockWidth)
                        {
                            left1 = 0;
                            ++i1;
                        }

                        left2 += bWidth;
                        if (left2 == matrix._blockWidth)
                        {
                            left2 = 0;
                            ++i2;
                        }
                    }
                }

                rowIndices[ri + 1] = columnIndices.Count;
            }

            return new BSRSparseMatrix<T>(Rows, Columns, bWidth, bHeight, rowIndices.ToArray(), columnIndices.ToArray(), values.ToArray());
        }
        private BSRSparseMatrix<T> add_matching_blocks(BSRSparseMatrix<T> matrix, IDenseBLAS1<T> b)
        {
            int blockRows = Rows / _blockHeight;
            int[] rowIndices = sym_union_row_index_matching_blocks(matrix);

            int nBlocks = rowIndices[blockRows],
                bsize = _blockHeight * _blockWidth;

            int[] columnIndices = new int[nBlocks];
            T[] values = new T[nBlocks * bsize];
            T[] buff1 = new T[bsize], buff2 = new T[bsize];

            int k = 0;
            for (int ri = 0; ri < blockRows; ++ri)
            {
                int end1 = _rowIndices[ri + 1],
                    end2 = matrix._rowIndices[ri + 1],
                    start1 = _rowIndices[ri],
                    start2 = matrix._rowIndices[ri],
                    i1 = start1,
                    i2 = start2;

                while (i1 < end1 || i2 < end2)
                {
                    int c1 = i1 < end1 ? _columnIndices[i1] : int.MaxValue,
                        c2 = i2 < end2 ? matrix._columnIndices[i2] : int.MaxValue;

                    if (c1 > c2)
                    {
                        columnIndices[k] = c2;
                        Array.Copy(matrix._values, i2 * bsize, values, k * bsize, bsize);
                        ++i2;
                    }
                    else if (c1 < c2)
                    {
                        columnIndices[k] = c1;
                        Array.Copy(_values, i1 * bsize, values, k * bsize, bsize);
                        ++i1;
                    }
                    else
                    {
                        columnIndices[k] = c1;
                        Array.Copy(matrix._values, i2 * bsize, buff1, 0, bsize);
                        Array.Copy(_values, i1 * bsize, buff2, 0, bsize);
                        b.ADD(buff1, buff2, buff1, 0, bsize);
                        Array.Copy(buff1, 0, values, k * bsize, bsize);
                        ++i1;
                        ++i2;
                    }
                    ++k;
                }
            }

            return new BSRSparseMatrix<T>(Rows, Columns, _blockWidth, _blockHeight, rowIndices, columnIndices, values);
        }

        public override Matrix<T> Add(Matrix<T> matrix, IProvider<T> provider)
        {
            throw new NotImplementedException();
        }

        public BSRSparseMatrix<T> Add(BSRSparseMatrix<T> matrix, IDenseBLAS1Provider<T> blas)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            if (blas is null)
            {
                throw new ArgumentNullException(nameof(blas));
            }
            if (!(blas is IDenseBLAS1<T> b))
            {
                throw new NotSupportedException(ExceptionMessages.TypeNotSupported);
            }
            MatrixChecks.AssertMatrixDimensionsEqual(this, matrix);

            if (_blockWidth != matrix._blockWidth || _blockHeight != matrix._blockHeight)
            {
                return add_unmatching_blocks(matrix, b);
            }
            else
            {
                return add_matching_blocks(matrix, b);
            }
        }

        public override void Clear()
        {
            Array.Clear(_rowIndices, 0, _rowIndices.Length);
            _columnIndices = new int[0];
            _values = new T[0];
        }

        public override void Clear(Predicate<T> select)
        {
            if (select is null)
            {
                throw new ArgumentNullException(nameof(select));
            }

            for (int k = 0, len = SymbolicNonZeroCount; k < len; ++k)
            {
                if (select(_values[k]))
                {
                    _values[k] = _zero;
                }
            }
        }

        public override void ClearColumn(int columnIndex)
        {
            if (columnIndex < 0 || columnIndex >= Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(columnIndex));
            }

            int ci = columnIndex / _blockWidth, 
                j = columnIndex % _blockWidth,
                bsize = _blockHeight * _blockWidth;

            for (int ri = 0, rlen = Rows / _blockHeight; ri < rlen; ++ri)
            {
                int end = _rowIndices[ri + 1];
                for (int k = _rowIndices[ri]; k < end; ++k)
                {
                    if (_columnIndices[k] == ci)
                    {
                        int offset = k * bsize;
                        for (int i = 0; i < _blockHeight; ++i)
                        {
                            _values[offset + i * _blockWidth + j] = _zero;
                        }
                    }
                }
            }
        }

        public override void ClearLower()
        {
            int bsize = _blockHeight * _blockWidth;
            for (int ri = 0, rlen = Rows / _blockHeight; ri < rlen; ++ri)
            {
                int end = _rowIndices[ri + 1],
                    r_offset = ri * _blockHeight,
                    corner_r = r_offset + _blockHeight - 1;

                for (int k = _rowIndices[ri]; k < end; ++k)
                {
                    int ci = _columnIndices[k],
                        corner_c = ci * _blockWidth;

                    // check if a block is in the lower half by testing its bottom-left corner
                    if (corner_r > corner_c)
                    {
                        int offset = k * bsize;

                        // The block is at least partly in the lower-half, so check every entry
                        for (int r = 0; r < _blockHeight; ++r)
                        {
                            int row = r + r_offset;
                            for (int c = 0; c < _blockWidth; ++c)
                            {
                                int col = c + corner_c;
                                if (row > col)
                                {
                                    // offset already contains the row coordinate
                                    _values[offset + c] = _zero;
                                }
                            }

                            // Update offset to incorporate new row
                            offset += _blockWidth;
                        }
                    }
                }
            }
        }

        public override void ClearRow(int rowIndex)
        {
            if (rowIndex < 0 || rowIndex >= Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(rowIndex));
            }

            int ri = rowIndex / _blockHeight,
                r = rowIndex % _blockHeight,
                end = _rowIndices[ri + 1],
                bsize = _blockHeight * _blockWidth;

            for (int ci = _rowIndices[ri]; ci < end; ++ci)
            {
                int offset = ci * bsize + r * _blockWidth;
                for (int i = 0; i < _blockWidth; ++i)
                {
                    _values[offset + i] = _zero;
                }
            }
        }

        public override void ClearSubmatrix(int rowIndex, int columnIndex, int rows, int columns)
        {
            if (rowIndex < 0 || rowIndex >= Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(rowIndex));
            }
            if (columnIndex < 0 || columnIndex >= Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(columnIndex));
            }

            int rowEnd = rowIndex + rows;
            if (rows < 0 || rowEnd >= Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(rows));
            }
            int colEnd = columnIndex + columns;
            if (columns < 0 || colEnd >= Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(columns));
            }

            int ri_start = rowIndex / _blockHeight,
                ri_end = rowEnd / _blockHeight,
                ci_start = columnIndex / _blockWidth,
                ci_end = colEnd / _blockWidth,
                bsize = _blockWidth * _blockHeight;

            for (int ri = ri_start; ri <= ri_end; ++ri)
            {
                int end = _rowIndices[ri + 1],
                    top = ri * _blockHeight;

                for (int k = _rowIndices[ri]; k < end; ++k)
                {
                    // Check if block is in range of columns
                    int ci = _columnIndices[k];
                    if (ci_start <= ci && ci <= ci_end)
                    {
                        int left = ci * _blockWidth;

                        // Iterate through the block 
                        for (int i = 0, z = k * bsize; i < _blockHeight; ++i)
                        {
                            int r = i + top;
                            if (rowIndex <= r && r < rowEnd)
                            {
                                for (int j = 0; j < _blockWidth; ++j)
                                {
                                    int c = left + j;
                                    if (columnIndex <= c && c < colEnd)
                                    {
                                        _values[z + j] = _zero;
                                    }
                                }
                            }
                            // Need to be careful here because of the r conditional
                            z += bsize;
                        }
                    }
                }
            }
        }

        public override void ClearUpper()
        {
            int bsize = _blockHeight * _blockWidth,
                blockRows = Rows / _blockHeight;

            for (int ri = 0; ri < blockRows; ++ri)
            {
                int end = _rowIndices[ri + 1],
                    start = _rowIndices[ri],
                    top = start * _blockHeight;
                for (int k = start; k < end; ++k)
                {
                    int ci = _columnIndices[k],
                        left = ci * _blockWidth,
                        right = left + _blockWidth - 1;

                    // Check of top-right corner is in upper half
                    if (right > top)
                    {
                        for (int i = 0, z = k * bsize; i < _blockHeight; ++i)
                        {
                            int r = top + i;
                            for (int j = 0; j < _blockWidth; ++j, ++z)
                            {
                                int c = left + j;
                                if (c > r)
                                {
                                    _values[z] = _zero;
                                }
                            }
                        }
                    }
                }
            }
        }

        public override object Clone()
        {
            return Copy();
        }

        public BSRSparseMatrix<T> Copy()
        {
            return new BSRSparseMatrix<T>(Rows, Columns, _blockWidth, _blockHeight, _rowIndices, _columnIndices, _values);
        }

        public override DenseVector<T> Column(int columnIndex)
        {
            if (columnIndex < 0 || columnIndex >= Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(columnIndex));
            }

            T[] column = RectangularVector.Zeroes<T>(Rows);
            int ci = columnIndex / _blockWidth,
                c = columnIndex % _blockWidth,
                bsize = _blockHeight * _blockWidth;

            int blockRows = Rows / _blockHeight;
            for (int ri = 0; ri < blockRows; ++ri)
            {
                int end = _rowIndices[ri + 1],
                    r_offset = ri * _blockHeight;
                for (int k = _rowIndices[ri]; k < end; ++k)
                {
                    if (_columnIndices[k] == ci)
                    {
                        for (int r = 0, z = k * bsize + c; r < _blockHeight; ++r)
                        {
                            column[r_offset + r] = _values[z];
                            z += _blockWidth;
                        }
                    }
                }
            }
            return new DenseVector<T>(column);
        }

        public override DenseVector<T> ColumnSums(IProvider<T> provider)
        {
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }

            T[] sums = RectangularVector.Zeroes<T>(Columns);

            int bsize = _blockWidth * _blockHeight;
            for (int ri = 0, rlen = Rows / _blockHeight; ri < rlen; ++ri)
            {
                int end = _rowIndices[ri + 1];
                for (int k = _rowIndices[ri]; k < end; ++k)
                {
                    int left = _columnIndices[k] * _blockWidth,
                        right = left + _blockWidth;
                    for (int i = 0, z = k * bsize; i < _blockHeight; ++i)
                    {
                        // BLAS would do wonders here...
                        for (int j = left; j < right; ++j, ++z)
                        {
                            sums[j] = provider.Add(sums[j], _values[z]);
                        }
                    }
                }
            }

            return new DenseVector<T>(sums);
        }

        public override int Count(Predicate<T> predicate)
        {
            if (predicate is null)
            {
                throw new ArgumentNullException(nameof(predicate));
            }

            int count = 0;
            for (int i = 0, len = SymbolicNonZeroCount; i < len; ++i)
            {
                if (predicate(_values[i])) ++count;
            }
            return count;
        }

        public override bool Equals(object obj)
        {
            if (obj is null)
            {
                return false;
            }

            if (obj is BSRSparseMatrix<T>)
            {
                return Equals(obj as BSRSparseMatrix<T>);
            }
            if (obj is Matrix<T>)
            {
                return Equals(obj as Matrix<T>);
            }
            return false;
        }
        public bool Equals(BSRSparseMatrix<T> matrix, Func<T, T, bool> equals)
        {
            if (matrix is null)
            {
                return false;
            }

            if (Rows != matrix.Rows || Columns != matrix.Columns)
            {
                return false;
            }

            if (equals is null)
            {
                throw new ArgumentNullException(nameof(equals));
            }

            int bHeight = Util.GCD(_blockHeight, matrix._blockHeight),
                bWidth = Util.GCD(_blockWidth, matrix._blockWidth),
                bsize1 = _blockHeight * _blockWidth,
                bsize2 = matrix._blockWidth * matrix._blockHeight,
                bRows = Rows / bHeight;

            for (int ri = 0; ri < bRows; ++ri)
            {
                int r = bHeight * ri,
                    r1 = r / _blockHeight,
                    top1 = r % _blockHeight,
                    end1 = _rowIndices[r1 + 1],
                    i1 = _rowIndices[r1],

                    r2 = r / matrix._blockHeight,
                    top2 = r % matrix._blockHeight,
                    end2 = matrix._rowIndices[r2 + 1],
                    i2 = matrix._rowIndices[r2],
                    
                    left1 = 0,
                    left2 = 0;

                while (i1 < end1 || i2 < end2)
                {
                    int c1 = i1 < end1 ? _columnIndices[i1] : int.MaxValue,
                        c2 = i2 < end2 ? matrix._columnIndices[i2] : int.MaxValue;

                    if (c1 < c2)
                    {
                        // check block [r1, c1] is zero
                        int k = i1 * bsize1 + top1 * _blockWidth + left1;
                        for (int i = 0; i < bHeight; ++i)
                        {
                            for (int j = 0; j < bWidth; ++j)
                            {
                                if (!equals(_values[k + j], _zero))
                                {
                                    return false;
                                }
                            }
                            k += _blockWidth;
                        }

                        left1 += bWidth;
                        if (left1 == _blockWidth)
                        {
                            left1 = 0;
                            ++i1;
                        }
                    }
                    else if (c1 > c2)
                    {
                        // Check block [r2, c2] is zero
                        int k = i2 * bsize2 + top2 * matrix._blockWidth + left2;
                        for (int i = 0; i < bHeight; ++i)
                        {
                            for (int j = 0; j < bWidth; ++j)
                            {
                                if (!equals(matrix._values[k + j], _zero))
                                {
                                    return false;
                                }
                            }
                            k += matrix._blockWidth;
                        }

                        left2 += bWidth;
                        if (left2 == matrix._blockWidth)
                        {
                            left2 = 0;
                            ++i2;
                        }
                    }
                    else
                    {
                        // Compare the two blocks
                        int k1 = i1 * bsize1 + top1 * _blockWidth + left1,
                            k2 = i2 * bsize2 + top2 * matrix._blockWidth + left2;

                        for (int i = 0; i < bHeight; ++i)
                        {
                            for (int j = 0; j < bWidth; ++j)
                            {
                                if (!equals(_values[k1 + j], matrix._values[k2 + j]))
                                {
                                    return false;
                                }
                            }
                            k1 += _blockWidth;
                            k2 += matrix._blockWidth;
                        }

                        left1 += bWidth;
                        if (left1 == _blockWidth)
                        {
                            left1 = 0;
                            ++i1;
                        }

                        left2 += bWidth;
                        if (left2 == matrix._blockWidth)
                        {
                            left2 = 0;
                            ++i2;
                        }
                    }
                }
            }
            return true;
        } 
        public bool Equals(BSRSparseMatrix<T> matrix, IEqualityComparer<T> comparer)
        {
            return Equals(matrix, comparer.Equals);
        }
        public bool Equals(BSRSparseMatrix<T> matrix)
        {
            return Equals(matrix, EqualityComparer<T>.Default);
        }

        public override bool IsBanded(int lowerBandwidth, int upperBandwidth, Predicate<T> isZero)
        {
            if (lowerBandwidth < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(lowerBandwidth), ExceptionMessages.NegativeNotAllowed);
            }
            if (upperBandwidth < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(upperBandwidth), ExceptionMessages.NegativeNotAllowed);
            }
            if (isZero is null)
            {
                throw new ArgumentNullException(nameof(isZero));
            }

            int bsize = _blockHeight * _blockWidth;
            for (int ri = 0, rlen = Rows / _blockHeight; ri < rlen; ++ri)
            {
                int end = _rowIndices[ri + 1],
                    top = ri * _blockHeight,
                    bottom = top + _blockHeight - 1;
                for (int k = _rowIndices[ri]; k < end; ++k)
                {
                    int ci = _columnIndices[k],
                        left = ci * _blockWidth,
                        right = left + _blockWidth - 1;

                    // Check top right corner for breach of upper bandwidth
                    if (right - top > upperBandwidth)
                    {
                        // Check entire block
                        int offset = k * bsize;
                        for (int i = 0; i < _blockHeight; ++i)
                        {
                            int r = top + i;
                            for (int j = 0; j < _blockWidth; ++j, ++offset)
                            {
                                int c = left + j;
                                if (c - r > upperBandwidth && !isZero(_values[offset]))
                                {
                                    return false;
                                }
                            }
                        }
                    }

                    // Check bottom left corner for breach of lower bandwidth
                    if (bottom - left > lowerBandwidth)
                    {
                        int offset = k * bsize;
                        for (int i = 0; i < _blockHeight; ++i)
                        {
                            int r = top + i;
                            for (int j = 0; j < _blockWidth; ++j, ++offset)
                            {
                                int c = left + j;
                                if (c - r > lowerBandwidth && !isZero(_values[offset]))
                                {
                                    return false;
                                }
                            }
                        }
                    }
                }
            }

            return true;
        }

        public override bool IsDiagonal(Predicate<T> isZero)
        {
            if (isZero is null)
            {
                throw new ArgumentNullException(nameof(isZero));
            }

            for (int ri = 0, rlen = Rows / _blockHeight, z = 0; ri < rlen; ++ri)
            {
                int end = _rowIndices[ri + 1],
                    top = ri * _blockHeight;

                for (int k = _rowIndices[ri]; k < end; ++k)
                {
                    int ci = _columnIndices[k],
                        left = ci * _blockWidth;

                    for (int i = 0; i < _blockHeight; ++i)
                    {
                        int r = top + i;
                        for (int j = 0; j < _blockWidth; ++j, ++z)
                        {
                            int c = left + j;
                            if (r != c && !isZero(_values[z]))
                            {
                                return false;
                            }
                        }
                    }
                }
            }
            return true;
        }

        public override bool IsIdentity(Predicate<T> isZero, Predicate<T> isOne)
        {
            if (isZero is null)
            {
                throw new ArgumentNullException(nameof(isZero));
            }
            if (isOne is null)
            {
                throw new ArgumentNullException(nameof(isOne));
            }

            for (int ri = 0, rlen = Rows / _blockHeight, z = 0; ri < rlen; ++ri)
            {
                int end = _rowIndices[ri + 1],
                    top = ri * _blockHeight,
                    bottom = top + _blockHeight;

                for (int k = _rowIndices[ri]; k < end; ++k)
                {
                    int ci = _columnIndices[k],
                        left = ci * _blockWidth,
                        right = left + _blockWidth;

                    for (int r = top; r < bottom; ++r)
                    {
                        for (int c = left; c < right; ++c, ++z)
                        {
                            if (r != c)
                            {
                                if (!isZero(_values[z])) return false;
                            }
                            else
                            {
                                if (!isOne(_values[z])) return false;
                            }
                        }
                    }
                }
            }
            return true;
        }

        public override bool IsLower(Predicate<T> isZero)
        {
            if (isZero is null)
            {
                throw new ArgumentNullException(nameof(isZero));
            }

            int bsize = _blockHeight * _blockWidth;
            for (int ri = 0, rlen = Rows / _blockHeight; ri < rlen; ++ri)
            {
                int end = _rowIndices[ri + 1],
                    top = ri * _blockHeight,
                    bottom = top + _blockHeight;

                for (int k = _rowIndices[ri]; k < end; ++k)
                {
                    int ci = _columnIndices[k],
                        left = ci * _blockWidth,
                        right = left + _blockWidth - 1;

                    // Check if top-right corner is in upper half
                    if (top < right)
                    {
                        // Check entire block
                        for (int r = top, z = k * bsize; r < bottom; ++r)
                        {
                            for (int c = left; c <= right; ++c, ++z)
                            {
                                if (c > r && !isZero(_values[z]))
                                {
                                    return false;
                                }
                            }
                        }
                    }
                }
            }

            return true;
        }

        public override bool IsUpper(Predicate<T> isZero)
        {
            if (isZero is null)
            {
                throw new ArgumentNullException(nameof(isZero));
            }

            int bsize = _blockHeight * _blockWidth;
            for (int ri = 0, rlen = Rows / _blockHeight; ri < rlen; ++ri)
            {
                int end = _rowIndices[ri + 1],
                    top = ri * _blockHeight,
                    bottom = top + _blockHeight - 1;

                for (int k = _rowIndices[ri]; k < end; ++k)
                {
                    int ci = _columnIndices[k],
                        left = ci * _blockWidth,
                        right = left + _blockWidth;

                    // Check the bottom-left corner 
                    if (bottom > left)
                    {
                        // Check the entire block
                        for (int r = top, z = k * bsize; r <= bottom; ++r)
                        {
                            for (int c = left; c < right; ++c, ++z)
                            {
                                if (r > c && !isZero(_values[z]))
                                {
                                    return false;
                                }
                            }
                        }
                    }
                }
            }
            return true;
        }

        public override bool IsZero(Predicate<T> isZero)
        {
            if (SymbolicNonZeroCount == 0)
            {
                return false;
            }

            if (isZero is null)
            {
                throw new ArgumentNullException(nameof(isZero));
            }

            for (int i = 0, len = SymbolicNonZeroCount; i < len; ++i)
            {
                if (!isZero(_values[i]))
                {
                    return false;
                }
            }
            return true;
        }

        public override T[] LeadingDiagonal()
        {
            int bsize = _blockHeight * _blockWidth;
            T[] diagonal = RectangularVector.Zeroes<T>(Math.Min(Rows, Columns));
            for (int ri = 0, rlen = Rows / _blockHeight; ri < rlen; ++ri)
            {
                int end = _rowIndices[ri + 1],
                    top = ri * _blockHeight,
                    bottom = top + _blockHeight - 1;

                for (int k = _rowIndices[ri]; k < end; ++k)
                {
                    int ci = _columnIndices[k],
                        left = ci * _blockWidth,
                        right = left + _blockWidth - 1;

                    // Check if block contains the diagonal term
                    if (right >= top && bottom >= left)
                    {
                        int z = k * bsize;
                        for (int r = top; r <= bottom; ++r)
                        {
                            int j = r - left;
                            if (0 <= j && j < _blockWidth)
                            {
                                diagonal[r] = _values[z + r * _blockWidth + j];
                            }
                        }
                    }
                }
            }
            return diagonal;
        }

        public override SparseVector<T> Multiply(SparseVector<T> vector, IProvider<T> provider)
        {
            if (vector is null)
            {
                throw new ArgumentNullException(nameof(vector));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }

            int[] vect_indices = vector.Indices;
            T[] vect_values = vector.Values;
            int vlen = vect_indices.Length;

            int nnz = 0;
            for (int ri = 0, rlen = Rows / _blockHeight; ri < rlen; ++ri)
            {
                int end = _rowIndices[ri + 1],
                    i1 = _rowIndices[ri],
                    i2 = 0;

                // Co-iterate to see if there is any intersection
                while (i1 < end && i2 < vlen)
                {
                    int c1 = i1 < end ? _columnIndices[i1] : int.MaxValue;
                    int c2 = i2 < vlen ? vect_indices[i2] / _blockWidth : int.MaxValue;

                    if (c1 < c2)
                    {
                        ++i1;
                    }
                    else if (c1 > c2)
                    {
                        ++i2;
                    }
                    else
                    {
                        nnz += _blockHeight;
                        break;
                    }
                }
            }

            int[] indices = new int[nnz];
            T[] values = new T[nnz];

            int z = 0,
                bsize = _blockHeight * _blockWidth;

            for (int ri = 0, rlen = Rows / _blockHeight; ri < rlen; ++ri)
            {
                int end = _rowIndices[ri + 1],
                    ki = _rowIndices[ri],
                    vi = 0,
                    top = ri * _blockHeight;

                bool foundFirst = false;
                while (ki < end || vi < vlen)
                {
                    int c1 = ki < end ? _columnIndices[ki] : int.MaxValue;
                    int c2 = vi < vlen ? vect_indices[vi] / _blockWidth : int.MaxValue;

                    if (c1 > c2)
                    {
                        ++vi;
                    }
                    else if (c1 < c2)
                    {
                        ++ki;
                    }
                    else
                    {
                        T v = vect_values[vi];
                        int k = ki * bsize + vect_indices[vi] % _blockWidth;

                        if (!foundFirst)
                        {
                            foundFirst = true;
                            for (int r = 0; r < _blockHeight; ++r)
                            {
                                int index = z + r;
                                values[index] = provider.Multiply(_values[k], v);
                                indices[index] = top + r;
                                k += _blockWidth;
                            }
                        }
                        else
                        {
                            for (int r = 0; r < _blockHeight; ++r)
                            {
                                int index = z + r;
                                values[index] = provider.Add(values[index], provider.Multiply(_values[k], v));
                                k += _blockWidth;
                            }
                        }

                        // Only increment vi as it is smaller in size and we might still be 
                        // in the same block
                        ++vi;
                    }
                }

                // new row
                if (foundFirst)
                {
                    z += _blockHeight;
                }
            }

            return new SparseVector<T>(Rows, indices, values, true);
        }

        internal override void Multiply(T[] vector, T[] product, IProvider<T> provider, bool increment)
        {
            if (ProviderFactory.TryGetDefaultBLAS1Provider(out IDenseBLAS1<T> blas))
            {
                Multiply(vector, product, blas, increment);
            }
            else
            {
                Multiply(vector, product, new DefaultNativeProvider<T>(provider), increment);
            }
        }

        internal void Multiply(T[] vector, T[] product, IDenseBLAS1<T> blas, bool increment)
        {
            IProvider<T> provider = blas.Provider;

            if (!increment)
            {
                if (provider.Equals(_zero, default))
                {
                    Array.Clear(product, 0, Rows);
                }
                else 
                {
                    for (int i = 0; i < Rows; ++i)
                    {
                        product[i] = _zero;
                    }
                }
            }

            // No checks?

            int bsize = _blockHeight * _blockWidth;
            for (int ri = 0, rlen = Rows / _blockHeight; ri < rlen; ++ri)
            {
                int end = _rowIndices[ri + 1],
                    top = ri * _blockHeight,
                    bottom = top + _blockHeight;

                for (int k = _rowIndices[ri]; k < end; ++k)
                {
                    int ci = _columnIndices[k],
                        left = ci * _blockWidth,
                        right = left + _blockWidth,
                        z = k * bsize;

                    for (int r = top; r < bottom; ++r)
                    {
                        product[r] = provider.Add(product[r], blas.DOT(vector, _values, left, right, z - left));
                        z += _blockWidth;
                    }
                }
            }
        }

        public BSRSparseMatrix<T> Multiply(T scalar, IProvider<T> provider)
        {
            BSRSparseMatrix<T> copy = Copy();
            copy.MultiplyInPlace(scalar, provider);
            return copy;
        }

        public override void MultiplyInPlace(T scalar, IProvider<T> provider)
        {
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            if (scalar is null)
            {
                throw new ArgumentNullException(nameof(scalar));
            }

            for (int i = 0, len = _values.Length; i < len; ++i)
            {
                _values[i] = provider.Multiply(_values[i], scalar);
            }
        }

        public BSRSparseMatrix<T> Multiply(T scalar, IDenseBLAS1Provider<T> blas1)
        {
            BSRSparseMatrix<T> copy = Copy();
            copy.MultiplyInPlace(scalar, blas1);
            return copy;
        }

        public void MultiplyInPlace(T scalar, IDenseBLAS1Provider<T> blas1)
        {
            if (scalar is null)
            {
                throw new ArgumentNullException(nameof(scalar));
            }
            if (blas1 is null)
            {
                throw new ArgumentNullException(nameof(blas1));
            }
            if (!(blas1 is IDenseBLAS1<T> b))
            {
                throw new NotSupportedException(ExceptionMessages.TypeNotSupported);
            }
            b.SCAL(_values, scalar, 0, _values.Length);
        }

        public override DenseVector<T> Row(int rowIndex)
        {
            if (rowIndex < 0 || rowIndex >= Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(rowIndex));
            }

            T[] row = RectangularVector.Zeroes<T>(Columns);
            int ri = rowIndex / _blockHeight,
                r = rowIndex % _blockHeight,
                end = _rowIndices[ri + 1],
                bsize = _blockHeight * _blockWidth;

            for (int k = _rowIndices[ri]; k < end; ++k)
            {
                int ci = _columnIndices[k],
                    left = ci * _blockWidth,
                    right = left + _blockWidth,
                    z = k * bsize + r * _blockWidth;

                for (int c = left; c < right; ++c)
                {
                    row[c] = _values[z++];
                }
            }
            return new DenseVector<T>(row);
        }

        public override DenseVector<T> RowSums(IProvider<T> provider)
        {
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }

            // No need to initialize
            T[] sums = RectangularVector.Zeroes<T>(Rows);
            int z = 0;
            for (int ri = 0, rlen = Rows / _blockHeight; ri < rlen; ++ri)
            {
                int end = _rowIndices[ri + 1],
                    top = ri * _blockHeight,
                    bottom = top + _blockHeight;

                for (int k = _rowIndices[ri]; k < end; ++k)
                {
                    // No need to fetch the column index
                    for (int i = top; i < bottom; ++i)
                    {
                        T sum = _zero;
                        for (int j = 0; j < _blockWidth; ++j)
                        {
                            sum = provider.Add(sum, _values[z++]);
                        }
                        sums[i] = provider.Add(sums[i], sum);
                    }
                }
            }
            return new DenseVector<T>(sums);
        }

        public override Matrix<T> Select(int[] rowIndices, int[] columnIndices)
        {
            if (rowIndices is null)
            {
                throw new ArgumentNullException(nameof(rowIndices));
            }
            if (columnIndices is null)
            {
                throw new ArgumentNullException(nameof(columnIndices));
            }
            ThrowHelper.AssertCollectionInRange(rowIndices, 0, Rows, nameof(rowIndices));
            ThrowHelper.AssertCollectionInRange(columnIndices, 0, Columns, nameof(columnIndices));

            int rlen = rowIndices.Length, 
                clen = columnIndices.Length,
                bsize = _blockHeight * _blockWidth;

            // We use another O(clen) memory to avoid having to alter the input parameters
            int[] sortedColumnIndices = new int[clen],
                cIndex_inverse = new int[clen];

            Array.Copy(columnIndices, sortedColumnIndices, clen);
            for (int c = 0; c < clen; ++c)
            {
                cIndex_inverse[c] = c;
            }
            Array.Sort(sortedColumnIndices, cIndex_inverse);

            // Using another O(rlen) memory to avoid having to alter the rowIndices param
            int[] sortedRowIndices = new int[rlen],
                rIndex_inverse = new int[rlen];

            Array.Copy(rowIndices, sortedRowIndices, rlen);
            for (int r = 0; r < rlen; ++r)
            {
                rIndex_inverse[r] = r;
            }
            Array.Sort(sortedRowIndices, rIndex_inverse);

            // Get the non-zero row counts 
            int[] sel_rowIndices = new int[rlen + 1];
            for (int r = 0; r < rlen; ++r)
            {
                int ri = sortedRowIndices[r] / _blockHeight,
                    end = _rowIndices[ri + 1],
                    i1 = _rowIndices[ri],
                    i2 = 0,
                    cnz = 0;

                while (i1 < end && i2 < clen)
                {
                    int c1 = _columnIndices[i1],
                        c2 = sortedColumnIndices[i2] / _blockWidth;

                    if (c1 < c2)
                    {
                        ++i1;
                    }
                    else if (c1 > c2)
                    {
                        ++i2;
                    }
                    else
                    {
                        ++cnz;
                        ++i2;
                    }
                }

                int row = rIndex_inverse[r];
                sel_rowIndices[row + 1] = cnz;
            }

            // Accumulate - this needs to be carried out separately
            // as we are filling in the row indices in sorted order
            for (int r = 0; r < rlen; ++r)
            {
                sel_rowIndices[r + 1] += sel_rowIndices[r];
            }

            int nnz = sel_rowIndices[rlen];
            int[] sel_colIndices = new int[nnz];
            T[] sel_values = new T[nnz];

            for (int r = 0; r < rlen; ++r)
            {
                int ri = sortedRowIndices[r] / _blockHeight,
                    offset_r = sortedRowIndices[r] % _blockHeight * _blockWidth,
                    end = _rowIndices[ri + 1],
                    i1 = _rowIndices[ri],
                    i2 = 0,
                    k = sel_rowIndices[rIndex_inverse[r]];

                while (i1 < end && i2 < clen)
                {
                    int c1 = _columnIndices[i1],
                        c2 = sortedColumnIndices[i2] / _blockWidth,
                        offset_c = sortedColumnIndices[i2] % _blockWidth;

                    if (c1 < c2)
                    {
                        ++i1;
                    }
                    else if (c1 > c2)
                    {
                        ++i2;
                    }
                    else
                    {
                        sel_colIndices[k] = cIndex_inverse[i2];
                        sel_values[k++] = _values[i1 * bsize + offset_r + offset_c];
                        ++i2;
                    }
                }
            }

            return new CSRSparseMatrix<T>(clen, sel_rowIndices, sel_colIndices, sel_values);
        }

        public override Vector<T> SelectAsVector(int rowIndex, int[] columnIndices)
        {
            if (rowIndex < 0 || rowIndex >= Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(rowIndex));
            }
            if (columnIndices is null)
            {
                throw new ArgumentNullException(nameof(columnIndices));
            }
            ThrowHelper.AssertCollectionInRange(columnIndices, 0, Columns, nameof(columnIndices));

            int clen = columnIndices.Length;
            T[] vector = RectangularVector.Zeroes<T>(clen);

            int r = rowIndex / _blockHeight,
                offset_r = rowIndex % _blockHeight * _blockWidth,
                bsize = _blockHeight * _blockWidth;

            int[] sortedColumnIndices = new int[clen],
                cIndex_inverse = new int[clen];
            Array.Copy(columnIndices, sortedColumnIndices, clen);
            for (int c = 0; c < clen; ++c)
            {
                cIndex_inverse[c] = c;
            }
            Array.Sort(sortedColumnIndices, cIndex_inverse);

            int i1 = _rowIndices[r],
                end = _rowIndices[r + 1],
                i2 = 0;

            while (i1 < end && i2 < clen)
            {
                int c1 = _columnIndices[i1],
                    c2 = sortedColumnIndices[i2] / _blockWidth;

                if (c1 < c2)
                {
                    ++i1;
                }
                else if (c1 > c2)
                {
                    ++i2;
                }
                else
                {
                    int _c = sortedColumnIndices[i2] % _blockWidth;
                    vector[cIndex_inverse[i2]] = _values[i1 * bsize + offset_r + _c];
                    ++i2;
                }
            }

            return new DenseVector<T>(vector);
        }

        public override Vector<T> SelectAsVector(int[] rowIndices, int columnIndex)
        {
            if (rowIndices is null)
            {
                throw new ArgumentNullException(nameof(rowIndices));
            }
            if (columnIndex < 0 || columnIndex >= Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(columnIndex));
            }
            ThrowHelper.AssertCollectionInRange(rowIndices, 0, Rows, nameof(rowIndices));

            int ci = columnIndex / _blockWidth, 
                c_offset = columnIndex % _blockWidth,
                rlen = rowIndices.Length,
                bsize = _blockHeight * _blockWidth;

            int[] sortedRowIndices = new int[rlen],
                rIndex_inverse = new int[rlen];

            for (int r = 0; r < rlen; ++r)
            {
                sortedRowIndices[r] = rowIndices[r];
                rIndex_inverse[r] = r;
            }

            Array.Sort(sortedRowIndices, rIndex_inverse);

            T[] sel = RectangularVector.Zeroes<T>(rlen);
            for (int i = 0; i < rlen; ++i)
            {
                int sr = sortedRowIndices[i],
                    ri = sr / _blockHeight,
                    r = sr % _blockHeight,
                    start = _rowIndices[ri],
                    end = _rowIndices[ri + 1], 
                    index = Array.BinarySearch(_columnIndices, start, end - start, ci);
                
                if (index >= 0)
                {
                    sel[rIndex_inverse[i]] = _values[index * bsize + r * _blockWidth + c_offset];
                }
            }
            return new DenseVector<T>(sel);
        }

        public override SparseVector<T> SparseColumn(int columnIndex)
        {
            if (columnIndex < 0 || columnIndex >= Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(columnIndex));
            }

            int ci = columnIndex / _blockWidth, 
                nnz = 0,
                c = columnIndex % _blockWidth,
                bsize = _blockHeight * _blockWidth;

            for (int ri = 0, rlen = Rows / _blockHeight; ri < rlen; ++ri)
            {
                int start = _rowIndices[ri],
                    end = _rowIndices[ri + 1];

                if (Array.BinarySearch(_columnIndices, start, end - start, ci) >= 0)
                {
                    nnz += _blockHeight;
                }
            }

            int[] indices = new int[nnz];
            T[] values = new T[nnz];

            for (int ri = 0, rlen = Rows / _blockHeight, k = 0; ri < rlen; ++ri)
            {
                int start = _rowIndices[ri],
                    end = _rowIndices[ri + 1],
                    index = Array.BinarySearch(_columnIndices, start, end - start, ci);

                if (index >= 0)
                {
                    int top = ri * _blockHeight,
                        offset = index * bsize;

                    for (int r = 0; r < _blockHeight; ++r)
                    {
                        indices[k] = top + r;
                        values[k++] = _values[offset + r * _blockWidth + c];
                    }
                }
            }

            return new SparseVector<T>(Rows, indices, values, true);
        }

        public override SparseVector<T> SparseRow(int rowIndex)
        {
            if (rowIndex < 0 || rowIndex >= Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(rowIndex));
            }

            int ri = rowIndex / _blockHeight,
                r = rowIndex % _blockHeight,
                end = _rowIndices[ri + 1],
                start = _rowIndices[ri],
                nnz = (end - start) * _blockWidth,
                bsize = _blockHeight * _blockWidth;

            int[] indices = new int[nnz];
            T[] values = new T[nnz];

            for (int i = start, k = 0; i < end; ++i)
            {
                int left = _columnIndices[i] * _blockWidth,
                    offset = i * bsize + r * _blockWidth;

                for (int c = 0; c < _blockWidth; ++c)
                {
                    indices[k] = left + c;
                    values[k++] = _values[offset + c];
                }
            }

            return new SparseVector<T>(Columns, indices, values, true);
        }

        public override void SwapColumns(int columnIndex1, int columnIndex2)
        {
            throw new NotImplementedException();
        }

        public override void SwapRows(int rowIndex1, int rowIndex2)
        {
            throw new NotImplementedException();
        }

        public override BandMatrix<T> ToBandMatrix()
        {
            throw new NotImplementedException();
        }

        public override BlockDiagonalMatrix<T> ToBlockDiagonalMatrix()
        {
            throw new NotImplementedException();
        }

        public override COOSparseMatrix<T> ToCOOSparseMatrix()
        {
            throw new NotImplementedException();
        }

        public override CSCSparseMatrix<T> ToCSCSparseMatrix()
        {
            throw new NotImplementedException();
        }

        public override CSRSparseMatrix<T> ToCSRSparseMatrix()
        {
            throw new NotImplementedException();
        }

        public override DenseMatrix<T> ToDenseMatrix()
        {
            throw new NotImplementedException();
        }

        public override DiagonalMatrix<T> ToDiagonalMatrix()
        {
            throw new NotImplementedException();
        }

        public override DOKSparseMatrix<T> ToDOKSparseMatrix()
        {
            throw new NotImplementedException();
        }

        public override MCSRSparseMatrix<T> ToMCSRSparseMatrix()
        {
            throw new NotImplementedException();
        }

        public override SKYSparseMatrix<T> ToSKYSparseMatrix()
        {
            throw new NotImplementedException();
        }

        public override T Trace(IProvider<T> provider)
        {
            throw new NotImplementedException();
        }

        public override SparseVector<T> TransposeAndMultiply(SparseVector<T> vector, IProvider<T> provider)
        {
            throw new NotImplementedException();
        }

        internal override bool SolveTriangular(T[] b, T[] x, IDenseBLAS1<T> blas, bool lower, bool transpose)
        {
            throw new NotImplementedException();
        }

        internal override void TransposeAndMultiply(T[] vector, T[] product, IProvider<T> provider, bool increment)
        {
            throw new NotImplementedException();
        }
    }
}
