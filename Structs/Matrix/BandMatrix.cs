﻿using LinearNet.Providers;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading.Tasks;
using LinearNet.Global;
using LinearNet.Providers.Managed;
using LinearNet.Matrices.Sparse.Decompositions;
using LinearNet.Matrices;
using System.Collections;
using LinearNet.Providers.Basic.Interfaces;
using LinearNet.Solvers.Linear;
using LinearNet.Matrices.LU;
using LinearNet.IO;
using System.IO;

namespace LinearNet.Structs
{
    /// <summary>
    /// <p>
    /// Implements a band-matrix where the only non-zero elements are along a diagonal band containing the leading
    /// diagonal, whose width is small compared to the dimensionality of the matrix itself.
    /// </p>
    /// <p>
    /// Common functions such as arithmetic operations and matrix decompositions can be orders of magnitude
    /// faster for matrices that allow effective exploitation of its 'thin' structure. It will also usually outperform
    /// equivalent methods designed for general unstructured sparse matrices.
    /// </p>
    /// <p>
    /// Band matrices can also be more memory-efficient than dense or general sparse matrices.
    /// </p>
    /// <p>
    /// For matrices with a large bandwidth (compared to the dimensionality of the matrix), computation tends to be 
    /// slower than those equivalent methods designed for dense matrices, due to the additional overhead incurred from
    /// storing the matrix elements in diagonal bands rather than rows.
    /// </p>
    /// </summary>
    /// <typeparam name="T">The type of each element of the matrix.</typeparam>
    /// <cat>linear-algebra</cat>
    public partial class BandMatrix<T> : Matrix<T> where T : new()
    {
        private readonly T _zero;

        /// <summary>
        /// _upperBandwidth refers to the number of non-zero diagonals above the leading diagonal,
        /// _lowerBandwidth refers to the number of non-zero diagonals below the leading diagonal.
        /// </summary>
        private int _upperBandwidth, _lowerBandwidth;

        /// <summary>
        /// Bands are arranged diagonally,
        /// With the 0-index closest to the main diagonal
        /// </summary>
        private T[][] _upperBands, _lowerBands;
        private readonly T[] _diagonal;

        /// <summary>
        /// Gets or sets a row of this band matrix.
        /// <para>
        /// If setting to a value, all elements of the array that lie outside of the matrix's bandwidth 
        /// will be ignored.
        /// </para>
        /// </summary>
        /// <param name="index">The row index</param>
        /// <returns>An array containing a row of this band matrix.</returns>
        public override T[] this[int index]
        {
            get
            {
                if (index < 0 || index >= Rows) throw new ArgumentOutOfRangeException();

                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Gets or sets an element of this band matrix.
        /// <para>
        /// <txt>ArgumentOutOfRangeException</txt> will be thrown if trying to set to a location lying 
        /// outside the bandwidth of this matrix.
        /// </para>
        /// </summary>
        /// <param name="rowIndex">The row index</param>
        /// <param name="columnIndex">The column index</param>
        /// <returns>The value at (<txt>rowIndex</txt>, <txt>columnIndex</txt>).</returns>
        public override T this[int rowIndex, int columnIndex]
        {
            get
            {
                if (rowIndex < 0 || rowIndex >= Rows)
                {
                    throw new ArgumentOutOfRangeException(nameof(rowIndex));
                }
                if (columnIndex < 0 || columnIndex >= Columns)
                {
                    throw new ArgumentOutOfRangeException(nameof(columnIndex));
                }

                int diff = rowIndex - columnIndex;
                if (diff < -_upperBandwidth || diff > _lowerBandwidth)
                {
                    return _zero;
                }

                if (diff > 0)
                {
                    return _lowerBands[diff - 1][columnIndex];
                }
                if (diff < 0)
                {
                    return _upperBands[-1 - diff][rowIndex];
                }

                return _diagonal[rowIndex];
            }
            set
            {
                if (rowIndex < 0 || rowIndex >= Rows || columnIndex < 0 || columnIndex >= Columns)
                {
                    throw new ArgumentOutOfRangeException();
                }

                int diff = rowIndex - columnIndex;
                if (diff < -_upperBandwidth || diff > _lowerBandwidth)
                {
                    return;
                }

                if (diff > 0)
                {
                    _lowerBands[diff - 1][columnIndex] = value;
                }
                else if (diff < 0)
                {
                    _upperBands[diff - 1][rowIndex] = value;
                }
                else
                {
                    _diagonal[rowIndex] = value;
                }
            }
        }
        internal T[] Diagonal { get { return _diagonal; } }
        internal T[][] LowerBands { get { return _lowerBands; } }
        internal T[][] UpperBands { get { return _upperBands; } }

        /// <summary>
        /// Returns the upper bandwidth of this matrix.
        /// </summary>
        public int UpperBandwidth { get { return _upperBandwidth; } }
        /// <summary>
        /// Returns the lower bandwidth of this matrix.
        /// </summary>
        public int LowerBandwidth { get { return _lowerBandwidth; } }
        /// <summary>
        /// Returns the total bandwidth of this matrix.
        /// </summary>
        public int Bandwidth { get { return _upperBandwidth + _lowerBandwidth + 1; } }
        /// <summary>
        /// Returns whether this matrix is upper bidiagonal (i.e. upper bandwidth is 1 and lower bandwidth is 0).
        /// </summary>
        public bool IsUpperBidiagonal { get { return _lowerBandwidth == 0 && _upperBandwidth == 1; } }
        /// <summary>
        /// Returns whether this matrix is lower bidiagonal (i.e. upper bandwidth is 0 and lower bandwidth is 1).
        /// </summary>
        public bool IsLowerBidiagonal { get { return _lowerBandwidth == 1 && _upperBandwidth == 0; } }
        /// <summary>
        /// Returns whether this matrix is bidiagonal (i.e. either upper bidiagonal or lower bidiagonal).
        /// </summary>
        public bool IsBidiagonal { get { return IsUpperBidiagonal || IsLowerBidiagonal; } }
        /// <summary>
        /// Returns whether this matrix is tridiagonal (i.e. both upper and lower bandwidths are 1).
        /// </summary>
        public bool IsTridiagonal { get { return _lowerBandwidth == 1 && _upperBandwidth == 1; } }
        /// <summary>
        /// Returns whether this matrix is pentadiagonal (i.e. both upper and lower bandwidths are 2).
        /// </summary>
        public bool IsPentadiagonal { get { return _lowerBandwidth == 2 && _upperBandwidth == 2; } }

        public override int SymbolicNonZeroCount
        {
            get
            {
                int count = _diagonal.Length;
                for (int b = 0; b < _upperBandwidth; ++b) count += _upperBands[b].Length;
                for (int b = 0; b < _lowerBandwidth; ++b) count += _lowerBands[b].Length;
                return count;
            }
        }

        public override IEnumerable<MatrixEntry<T>> NonZeroEntries
        {
            get
            {
                for (int b = 0; b < _upperBandwidth; ++b)
                {
                    T[] band = _upperBands[b];
                    int len = band.Length, i, b1 = b + 1;
                    for (i = 0; i < len; ++i)
                    {
                        yield return new MatrixEntry<T>(i, i + b1, band[i]);
                    }
                }
                for (int b = 0; b < _lowerBandwidth; ++b)
                {
                    T[] band = _lowerBands[b];
                    int len = band.Length, i, b1 = b + 1;
                    for (i = 0; i < len; ++i)
                    {
                        yield return new MatrixEntry<T>(i + b1, i, band[i]);
                    }
                }
                for (int i = 0; i < _diagonal.Length; ++i)
                {
                    yield return new MatrixEntry<T>(i, i, _diagonal[i]);
                }
            }
        }

        #region Constructors 

        /// <summary>
        /// Initialize a band matrix with a specified number of bands above and below the leading diagonal.
        /// </summary>
        /// <param name="rows">The number of rows of the matrix.</param>
        /// <param name="columns">The number of columns of the matrix.</param>
        /// <param name="upperBandwidth">
        /// The upper bandwidth, i.e. the number of non-zero diagonals above the leading diagonal. Must be at least 0 and strictly
        /// less than the smaller dimension of the matrix.
        /// </param>
        /// <param name="lowerBandwidth">
        /// The lower bandwidth, i.e. the number of non-zero diagonals below the leading diagonal. Must be at least 0 and strictly 
        /// less than the smaller dimension of the matrix.
        /// </param>
        public BandMatrix(int rows, int columns, int upperBandwidth, int lowerBandwidth) : base(MatrixStorageType.BAND, rows, columns)
        {
            if (rows <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(rows), ExceptionMessages.NonPositiveNotAllowed);
            }
            if (columns <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(columns), ExceptionMessages.NonPositiveNotAllowed);
            }
            if (upperBandwidth < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(upperBandwidth), ExceptionMessages.NegativeNotAllowed);
            }
            if (lowerBandwidth < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(lowerBandwidth), ExceptionMessages.NegativeNotAllowed);
            }

            int leadingDiagonalDim = Math.Min(rows, columns);
            if (upperBandwidth >= leadingDiagonalDim)
            {
                throw new ArgumentOutOfRangeException(nameof(upperBandwidth), "The upper bandwidth cannot be greater than or equal to the dimensionality of the matrix.");
            }
            if (lowerBandwidth >= leadingDiagonalDim)
            {
                throw new ArgumentOutOfRangeException(nameof(lowerBandwidth), "The lower bandwidth cannot be greater than or equal to the dimensionality of the matrix.");
            }

            // Initialize to zero
            _zero = new T();

            _upperBandwidth = upperBandwidth;
            _lowerBandwidth = lowerBandwidth;

            _upperBands = new T[_upperBandwidth][];
            for (int b = 0; b < _upperBandwidth; ++b)
            {
                _upperBands[b] = RectangularVector.Zeroes<T>(Math.Min(leadingDiagonalDim, columns - b - 1));
            }
            _diagonal = RectangularVector.Zeroes<T>(leadingDiagonalDim);

            _lowerBands = new T[_lowerBandwidth][];
            for (int b = 0; b < _lowerBandwidth; ++b)
            {
                _lowerBands[b] = RectangularVector.Zeroes<T>(Math.Min(leadingDiagonalDim, rows - b - 1));
            }
        }

        /// <summary>
        /// Copy constructor - create a band matrix with the same values as another band matrix.
        /// </summary>
        /// <param name="matrix">Another band matrix.</param>
        public BandMatrix(BandMatrix<T> matrix) : base(MatrixStorageType.BAND)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            _zero = new T();

            Rows = matrix.Rows;
            Columns = matrix.Columns;
            _upperBandwidth = matrix.UpperBandwidth;
            _lowerBandwidth = matrix.LowerBandwidth;

            _upperBands = Copy(matrix._upperBands);
            _diagonal = matrix._diagonal.Copy();
            _lowerBands = Copy(matrix._lowerBands);
        }

        /// <summary>
        /// Create a band matrix from a sparse Dictionary-of-Keys (DOK) matrix.
        /// </summary>
        /// <param name="matrix">The DOK sparse matrix.</param>
        public BandMatrix(DOKSparseMatrix<T> matrix) : base(MatrixStorageType.BAND)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            Rows = matrix.Rows;
            Columns = matrix.Columns;

            _zero = new T();

            // Determine the lower and upper bandwidths

            _upperBandwidth = 0;
            _lowerBandwidth = 0;
            foreach (KeyValuePair<long, T> e in matrix.Values)
            {
                long row = e.Key / Columns, col = e.Key % Columns;

                int diff = (int)(col - row);
                if (diff > 0)
                {
                    if (_upperBandwidth < diff)
                    {
                        _upperBandwidth = diff;
                    }
                }
                else
                {
                    diff = -diff;
                    if (_lowerBandwidth < diff)
                    {
                        _lowerBandwidth = diff;
                    }
                }
            }

            _diagonal = RectangularVector.Zeroes<T>(Math.Min(Rows, Columns));
            _upperBands = new T[_upperBandwidth][];
            _lowerBands = new T[_lowerBandwidth][];

            for (int b = 0; b < _upperBandwidth; ++b)
            {
                _upperBands[b] = RectangularVector.Zeroes<T>(Math.Min(Columns - b - 1, Rows));
            }
            for (int b = 0; b < _lowerBandwidth; ++b)
            {
                _lowerBands[b] = RectangularVector.Zeroes<T>(Math.Min(Rows - b - 1, Columns));
            }

            foreach (KeyValuePair<long, T> e in matrix.Values)
            {
                int rowIndex = (int)(e.Key / Columns), colIndex = (int)(e.Key % Columns);
                if (rowIndex > colIndex)
                {
                    _lowerBands[rowIndex - colIndex - 1][colIndex] = e.Value;
                }
                else if (rowIndex == colIndex)
                {
                    _diagonal[rowIndex] = e.Value;
                }
                else
                {
                    _upperBands[colIndex - rowIndex - 1][rowIndex] = e.Value;
                }
            }
        }

        /// <summary>
        /// Create a band matrix from a sparse Coordinate List (COO) matrix.
        /// </summary>
        /// <param name="matrix">The COO sparse matrix.</param>
        public BandMatrix(COOSparseMatrix<T> matrix) : base(MatrixStorageType.BAND)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            Rows = matrix.Rows;
            Columns = matrix.Columns;
            _zero = new T();

            // Determine the upper and lower bandwidths
            _upperBandwidth = 0;
            _lowerBandwidth = 0;
            foreach (MatrixEntry<T> e in matrix.Values)
            {
                int diff = e.ColumnIndex - e.RowIndex;
                if (diff > 0)
                {
                    if (diff > _upperBandwidth)
                    {
                        _upperBandwidth = diff;
                    }
                }
                else if (diff < 0)
                {
                    diff = -diff;
                    if (diff > _lowerBandwidth)
                    {
                        _lowerBandwidth = diff;
                    }
                }
            }

            // initialize the bands
            _upperBands = new T[_upperBandwidth][];
            _lowerBands = new T[_lowerBandwidth][];
            _diagonal = RectangularVector.Zeroes<T>(Math.Min(Rows, Columns));

            for (int b = 0; b < _upperBandwidth; ++b)
            {
                _upperBands[b] = RectangularVector.Zeroes<T>(Math.Min(Columns - b - 1, Rows));
            }
            for (int b = 0; b < _lowerBandwidth; ++b)
            {
                _lowerBands[b] = RectangularVector.Zeroes<T>(Math.Min(Rows - b - 1, Columns));
            }

            foreach (MatrixEntry<T> e in matrix.Values)
            {
                if (e.RowIndex > e.ColumnIndex)
                {
                    _lowerBands[e.RowIndex - e.ColumnIndex - 1][e.ColumnIndex] = e.Value;
                }
                else if (e.RowIndex == e.ColumnIndex)
                {
                    _diagonal[e.RowIndex] = e.Value;
                }
                else
                {
                    _upperBands[e.ColumnIndex - e.RowIndex - 1][e.RowIndex] = e.Value;
                }
            }
        }

        /// <summary>
        /// Create a band matrix from a sparse Compressed Sparse Row (CSR) matrix.
        /// </summary>
        /// <param name="matrix">The CSR matrix.</param>
        public BandMatrix(CSRSparseMatrix<T> matrix) : base(MatrixStorageType.BAND)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            Rows = matrix.Rows;
            Columns = matrix.Columns;

            _zero = new T();

            _lowerBandwidth = 0;
            _upperBandwidth = 0;

            T[] values = matrix.Values;
            int[] rowIndices = matrix.RowIndices;
            int[] columnIndices = matrix.ColumnIndices;

            for (int r = 0; r < Rows; ++r)
            {
                int row_start = rowIndices[r], row_end = rowIndices[r + 1];

                // Check if there are any elements in this row
                if (row_end > row_start)
                {
                    int col_start = columnIndices[row_start], col_end = columnIndices[row_end - 1];
                    if (col_start < r)
                    {
                        int width = r - col_start;
                        if (width > _lowerBandwidth)
                        {
                            _lowerBandwidth = width;
                        }
                    }
                    if (col_end > r)
                    {
                        int width = col_end - r;
                        if (width > _upperBandwidth)
                        {
                            _upperBandwidth = width;
                        }
                    }
                }
            }

            // Initialize the arrays 
            _upperBands = new T[_upperBandwidth][];
            _lowerBands = new T[_lowerBandwidth][];
            _diagonal = new T[Math.Min(Rows, Columns)];

            for (int b = 0; b < _upperBandwidth; ++b)
            {
                _upperBands[b] = RectangularVector.Zeroes<T>(Math.Min(Columns - b - 1, Rows));
            }
            for (int b = 0; b < _lowerBandwidth; ++b)
            {
                _lowerBands[b] = RectangularVector.Zeroes<T>(Math.Min(Rows - b - 1, Columns));
            }

            // Fill in the values
            for (int r = 0; r < Rows; ++r)
            {
                int row_start = rowIndices[r], row_end = rowIndices[r + 1];
                for (int i = row_start; i < row_end; ++i)
                {
                    int c = columnIndices[i];
                    if (c > r)
                    {
                        _upperBands[c - r - 1][r] = values[i];
                    }
                    else if (c == r)
                    {
                        _diagonal[r] = values[i];
                    }
                    else
                    {
                        _lowerBands[r - c - 1][c] = values[i];
                    }
                }
            }
        }

        /// <summary>
        /// Create a band matrix from a sparse Compressed Sparse Column (CSC) matrix.
        /// </summary>
        /// <param name="matrix">The CSC matrix.</param>
        public BandMatrix(CSCSparseMatrix<T> matrix) : base(MatrixStorageType.BAND)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            Rows = matrix.Rows;
            Columns = matrix.Columns;

            int[] columnIndices = matrix.ColumnIndices,
                rowIndices = matrix.RowIndices;
            T[] values = matrix.Values;

            // Determine the upper and lower bandwidth
            _upperBandwidth = 0;
            _lowerBandwidth = 0;
            for (int c = 0; c < Columns; ++c)
            {
                int col_end = columnIndices[c + 1];
                for (int i = columnIndices[c]; i < col_end; ++i)
                {
                    int r = rowIndices[i], offset = c - r;
                    if (offset > _upperBandwidth)
                    {
                        _upperBandwidth = offset;
                    }
                    else if (-offset > _lowerBandwidth)
                    {
                        _lowerBandwidth = -offset;
                    }
                }
            }

            _upperBands = CreateBands(Columns, Rows, _upperBandwidth, true);
            _diagonal = Zeroes(Math.Min(Columns, Rows));
            _lowerBands = CreateBands(Columns, Rows, _lowerBandwidth, false);

            // Populate
            for (int c = 0; c < Columns; ++c)
            {
                int col_end = columnIndices[c + 1];
                for (int i = columnIndices[c]; i < col_end; ++i)
                {
                    int r = rowIndices[i];
                    if (r > c)
                    {
                        _lowerBands[r - c - 1][c] = values[i];
                    }
                    else if (r == c)
                    {
                        _diagonal[r] = values[i];
                    }
                    else
                    {
                        _upperBands[c - r - 1][r] = values[i];
                    }
                }
            }
        }

        /// <summary>
        /// Create a band matrix from a sparse Modified Compressed Sparse Row (MCSR) matrix.
        /// </summary>
        /// <param name="matrix">The modified CSR matrix.</param>
        public BandMatrix(MCSRSparseMatrix<T> matrix) : base(MatrixStorageType.BAND)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            Rows = matrix.Rows;
            Columns = matrix.Columns;
            _zero = new T();

            // Determine the lower and upper bandwidths
            _lowerBandwidth = 0;
            _upperBandwidth = 0;

            int[] indices = matrix.Indices;
            T[] values = matrix.Values;
            for (int r = 0; r < Rows; ++r)
            {
                int end = indices[r + 1];
                for (int i = indices[r]; i < end; ++i)
                {
                    int c = indices[i];
                    if (c > r)
                    {
                        if (c - r > _upperBandwidth && !values[i].Equals(_zero))
                        {
                            _upperBandwidth = c - r;
                        }
                    }
                    else
                    {
                        if (r - c > _lowerBandwidth && !values[i].Equals(_zero))
                        {
                            _lowerBandwidth = r - c;
                        }
                    }
                }
            }

            _upperBands = CreateBands(Columns, Rows, _upperBandwidth, true);
            _diagonal = new T[Math.Min(Columns, Rows)];
            _lowerBands = CreateBands(Columns, Rows, _lowerBandwidth, false);

            // Copy the diagonal entries first
            Array.Copy(values, _diagonal, _diagonal.Length);

            // Off-diagonal entries
            for (int r = 0; r < Rows; ++r)
            {
                int end = indices[r + 1];
                for (int i = indices[r]; i < end; ++i)
                {
                    if (!values[i].Equals(_zero))
                    {
                        int c = indices[i];
                        if (c > r)
                        {
                            _upperBands[c - r - 1][r] = values[i];
                        }
                        else
                        {
                            _lowerBands[r - c - 1][c] = values[i];
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Create a band matrix from a diagonal matrix
        /// </summary>
        /// <param name="matrix">The diagonal matrix.</param>
        public BandMatrix(DiagonalMatrix<T> matrix) : base(MatrixStorageType.BAND)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            Rows = matrix.Rows;
            Columns = matrix.Columns;
            _zero = new T();

            _lowerBandwidth = 0;
            _upperBandwidth = 0;

            _upperBands = new T[0][];
            _lowerBands = new T[0][];

            // This relies on the correct length of matrix diagonals ... should be fine
            _diagonal = matrix.DiagonalTerms.Copy();
        }

        /// <summary>
        /// Create a band matrix from a dense matrix. Elements that are exactly zero according to the 
        /// <txt>Equals</txt> method of type <txt>T</txt> itself.
        /// </summary>
        /// <param name="matrix">The dense matrix.</param>
        public BandMatrix(DenseMatrix<T> matrix) : base(MatrixStorageType.BAND)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            Rows = matrix.Rows;
            Columns = matrix.Columns;

            _zero = new T();

            _lowerBandwidth = 0;
            _upperBandwidth = 0;
            for (int r = 0; r < Rows; ++r)
            {
                T[] row = matrix[r];
                for (int c = 0; c < Columns; ++c)
                {
                    // There is a better way to check for bandwidth,
                    // however this is the 'safe' method for now, with 
                    // less need to consider edge cases 

                    if (r > c)
                    {
                        int width = r - c;
                        if (width > _lowerBandwidth && !row[c].Equals(_zero))
                        {
                            _lowerBandwidth = width;
                        }
                    }
                    else if (r < c)
                    {
                        int width = c - r;
                        if (width > _upperBandwidth && !row[c].Equals(_zero))
                        {
                            _upperBandwidth = width;
                        }
                    }
                }
            }

            // Initialize the bands
            _upperBands = new T[_upperBandwidth][];
            _lowerBands = new T[_lowerBandwidth][];
            _diagonal = RectangularVector.Zeroes<T>(Math.Min(Columns, Rows));

            for (int b = 0; b < _upperBandwidth; ++b)
            {
                _upperBands[b] = RectangularVector.Zeroes<T>(Math.Min(Columns - b - 1, Rows));
            }
            for (int b = 0; b < _lowerBandwidth; ++b)
            {
                _lowerBands[b] = RectangularVector.Zeroes<T>(Math.Min(Rows - b - 1, Columns));
            }

            // Fill in the values 
            for (int r = 0; r < Rows; ++r)
            {
                T[] row = matrix[r];
                for (int c = 0; c < Columns; ++c)
                {
                    T e = row[c];
                    if (e.Equals(_zero))
                    {
                        continue;
                    }

                    if (r > c)
                    {
                        _lowerBands[r - c - 1][c] = e;
                    }
                    else if (r == c)
                    {
                        _diagonal[r] = e;
                    }
                    else
                    {
                        _upperBands[c - r - 1][r] = e;
                    }
                }
            }
        }

        /// <summary>
        /// Create a band matrix from a Skyline sparse matrix. 
        /// </summary>
        /// <param name="matrix">The sparse matrix in Skyline storage.</param>
        public BandMatrix(SKYSparseMatrix<T> matrix) : base(MatrixStorageType.BAND)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            Rows = matrix.Rows;
            Columns = matrix.Columns;
            _zero = new T();

            int[] indices = matrix.Indices;
            T[] values = matrix.Values;

            // Calculate bandwidths
            int maxWidth = 0;
            for (int r = 0; r < Rows; ++r)
            {
                int width = indices[r + 1] - indices[r];
                if (width > maxWidth)
                {
                    maxWidth = width;
                }
            }

            // Create storage
            _diagonal = new T[Rows];
            if (matrix.Type == SKYType.LOWER)
            {
                _upperBandwidth = 0;
                _lowerBandwidth = maxWidth;
                _upperBands = new T[0][];
                _lowerBands = CreateBands(Columns, Rows, _lowerBandwidth, false);
            }
            else if (matrix.Type == SKYType.UPPER)
            {
                _upperBandwidth = maxWidth;
                _lowerBandwidth = 0;
                _upperBands = CreateBands(Columns, Rows, _upperBandwidth, true);
                _lowerBands = new T[0][];
            }
            else if (matrix.Type == SKYType.SYMMETRIC)
            {
                _lowerBandwidth = maxWidth;
                _upperBandwidth = maxWidth;
                _lowerBands = CreateBands(Columns, Rows, _lowerBandwidth, false);
                _upperBands = CreateBands(Columns, Rows, _upperBandwidth, true);
            }
            else
            {
                throw new NotSupportedException(nameof(matrix.Type));
            }

            // Fill values
            if (matrix.Type == SKYType.LOWER || matrix.Type == SKYType.SYMMETRIC)
            {
                for (int r = 0; r < Rows; ++r)
                {
                    int start = indices[r], end = indices[r + 1], col_start = r - (end - start) + 1;

                    // Set lower
                    int r1 = r - 1;
                    for (int c = col_start, i = start; c < r; ++c, ++i)
                    {
                        // r1 - c = r - c - 1
                        _lowerBands[r1 - c][c] = values[i];
                    }
                    // Set diagonal
                    _diagonal[r] = values[end - 1];
                }
            }
            if (matrix.Type == SKYType.UPPER || matrix.Type == SKYType.SYMMETRIC)
            {
                for (int c = 0; c < Columns; ++c)
                {
                    int start = indices[c], end = indices[c + 1], row_start = c - (end - start) + 1;

                    // Set upper
                    for (int r = row_start, i = start; r < c; ++r, ++i)
                    {
                        _upperBands[c - r - 1][r] = values[i];
                    }

                    // Set diagonal (symmetric case this would already be set)
                    _diagonal[c] = values[end - 1];
                }
            }
        }

        /// <summary>
        /// Create a band matrix from a block-diagonal matrix.
        /// </summary>
        /// <param name="matrix">The block-diagonal matrix.</param>
        public BandMatrix(BlockDiagonalMatrix<T> matrix) : base(MatrixStorageType.BAND)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            Rows = matrix.Rows;
            Columns = matrix.Columns;
            _zero = new T();

            int[] blockIndices = matrix.BlockIndices;
            DenseMatrix<T>[] blocks = matrix.Blocks;

            // Determine the bandwidth
            int maxSize = 0;
            for (int i = 0; i < blocks.Length; ++i)
            {
                if (blocks[i].Rows  > maxSize)
                {
                    maxSize = blocks[i].Rows;
                }
            }
            _upperBandwidth = maxSize - 1;
            _lowerBandwidth = maxSize - 1;

            _upperBands = CreateBands(Columns, Rows, _upperBandwidth, true);
            _diagonal = Zeroes(Math.Min(Rows, Columns));
            _lowerBands = CreateBands(Columns, Rows, _lowerBandwidth, false);

            for (int b = 0; b < blocks.Length; ++b)
            {
                int offset = blockIndices[b];
                DenseMatrix<T> block = blocks[b];
                T[][] values = block.Values;
                int size = block.Rows, r, c;
                for (r = 0; r < size; ++r)
                {
                    int _r = offset + r;
                    T[] row = values[r];
                    for (c = 0; c < size; ++c)
                    {
                        int _c = offset + c;
                        if (_r > _c)
                        {
                            _lowerBands[_r - _c - 1][_c] = row[c];
                        }
                        else if (_r == _c)
                        {
                            _diagonal[_r] = row[c];
                        }
                        else
                        {
                            _upperBands[_c - _r - 1][_r] = row[c];
                        }
                    }
                }
            }
        }

        public BandMatrix(Matrix<T> matrix) : base(MatrixStorageType.BAND)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            Rows = matrix.Rows;
            Columns = matrix.Columns;
            _zero = new T();

            // Determine the upper and lower bandwidths
            _upperBandwidth = 0;
            _lowerBandwidth = 0;
            foreach (MatrixEntry<T> e in matrix.NonZeroEntries)
            {
                int diff = e.ColumnIndex - e.RowIndex;
                if (diff > 0)
                {
                    if (diff > _upperBandwidth)
                    {
                        _upperBandwidth = diff;
                    }
                }
                else if (diff < 0)
                {
                    diff = -diff;
                    if (diff > _lowerBandwidth)
                    {
                        _lowerBandwidth = diff;
                    }
                }
            }

            // initialize the bands
            _upperBands = new T[_upperBandwidth][];
            _lowerBands = new T[_lowerBandwidth][];
            _diagonal = RectangularVector.Zeroes<T>(Math.Min(Rows, Columns));

            for (int b = 0; b < _upperBandwidth; ++b)
            {
                _upperBands[b] = RectangularVector.Zeroes<T>(Math.Min(Columns - b - 1, Rows));
            }
            for (int b = 0; b < _lowerBandwidth; ++b)
            {
                _lowerBands[b] = RectangularVector.Zeroes<T>(Math.Min(Rows - b - 1, Columns));
            }

            foreach (MatrixEntry<T> e in matrix.NonZeroEntries)
            {
                if (e.RowIndex > e.ColumnIndex)
                {
                    _lowerBands[e.RowIndex - e.ColumnIndex - 1][e.ColumnIndex] = e.Value;
                }
                else if (e.RowIndex == e.ColumnIndex)
                {
                    _diagonal[e.RowIndex] = e.Value;
                }
                else
                {
                    _upperBands[e.ColumnIndex - e.RowIndex - 1][e.RowIndex] = e.Value;
                }
            }
        }

        internal BandMatrix(int rows, int columns, T[][] upperBands, T[] diagonal, T[][] lowerBands) : base(MatrixStorageType.BAND, rows, columns)
        {
            // Initialize to zero
            _zero = new T();

            _upperBands = upperBands;
            _diagonal = diagonal;
            _lowerBands = lowerBands;
            _upperBandwidth = upperBands.Length;
            _lowerBandwidth = lowerBands.Length;
        }

        #endregion


        private static T[][] CreateBands(int width, int height, int bandwidth, bool upper)
        {
            T[][] bands = new T[bandwidth][];
            for (int b = 0; b < bandwidth; ++b)
            {
                if (upper)
                {
                    bands[b] = Zeroes(Math.Min(width - b - 1, height));
                }
                else
                {
                    bands[b] = Zeroes(Math.Min(height - b - 1, width));
                }
            }
            return bands;
        }
        private static T[] Zeroes(int dim)
        {
            T zero = new T();
            if (zero.Equals(default))
            {
                return new T[dim];
            }
            else
            {
                T[] array = new T[dim];
                for (int i = 0; i < dim; ++i)
                {
                    array[i] = zero;
                }
                return array;
            }
        }

        private T[][] AddBands(T[][] A, T[][] B, Func<T[], T[], T[]> Add)
        {
            int min = Math.Min(A.Length, B.Length), max = Math.Max(A.Length, B.Length);

            T[][] sum = new T[max][];
            for (int i = 0; i < min; ++i)
            {
                sum[i] = Add(A[i], B[i]);
            }

            if (A.Length > min)
            {
                for (int i = min; i < max; ++i)
                {
                    sum[i] = A[i].Copy();
                }
            }

            else if (B.Length > min)
            {
                for (int i = min; i < max; ++i)
                {
                    sum[i] = B[i].Copy();
                }
            }
            return sum;
        }
        private T[][] AddBands(T[][] A, T[][] B, IDenseBLAS1<T> blas)
        {
            int min = Math.Min(A.Length, B.Length), max = Math.Max(A.Length, B.Length);

            T[][] sum = new T[max][];
            for (int i = 0; i < min; ++i)
            {
                int len = A[i].Length;
                T[] buff = new T[len];
                blas.ADD(A[i], B[i], buff, 0, len);
                sum[i] = buff;
            }

            if (A.Length > min)
            {
                for (int i = min; i < max; ++i)
                {
                    sum[i] = A[i].Copy();
                }
            }

            else if (B.Length > min)
            {
                for (int i = min; i < max; ++i)
                {
                    sum[i] = B[i].Copy();
                }
            }
            return sum;
        }
        private T[][] AddBandsInPlace(T[][] A, T[][] B, IDenseBLAS1<T> blas)
        {
            // Caching this length is actually important
            int alen = A.Length,
                blen = B.Length;

            // Requires re-structing of bands
            if (blen > alen)
            {
                T[][] sum = new T[blen][];

                // only shallow copying is needed, and everything is 0-indexed
                for (int i = 0; i < alen; ++i)
                {
                    sum[i] = A[i]; 
                }

                // These will not be re-visited by the adder so direct copying is ok
                for (int i = alen; i < blen; ++i)
                {
                    sum[i] = B[i].Copy();
                }
                A = sum;
            }

            int min = Math.Min(alen, blen);
            for (int i = 0; i < min; ++i)
            {
                blas.ADD(A[i], B[i], A[i], 0, A[i].Length);
            }
            return A;
        }
        private T[][] SubtractBands(T[][] A, T[][] B, Func<T[], T[], T[]> Subtract, Func<T[], T[]> Negate)
        {
            int min = Math.Min(A.Length, B.Length), max = Math.Max(A.Length, B.Length);

            T[][] diff = new T[max][];
            for (int i = 0; i < min; ++i)
            {
                diff[i] = Subtract(A[i], B[i]);
            }

            if (A.Length > min)
            {
                for (int i = min; i < max; ++i)
                {
                    diff[i] = A[i].Copy();
                }
            }
            else if (B.Length > min)
            {
                for (int i = min; i < max; ++i)
                {
                    diff[i] = Negate(B[i]);
                }
            }
            return diff;
        }
        private T[][] SubtractBands(T[][] A, T[][] B, IDenseBLAS1<T> blas)
        {
            int min = Math.Min(A.Length, B.Length), max = Math.Max(A.Length, B.Length);

            T[][] diff = new T[max][];
            for (int i = 0; i < min; ++i)
            {
                int len = A[i].Length;
                T[] buff = new T[len];
                blas.SUB(A[i], B[i], buff, 0, len);
                diff[i] = buff;
            }

            if (A.Length > min)
            {
                for (int i = min; i < max; ++i)
                {
                    diff[i] = A[i].Copy();
                }
            }
            else if (B.Length > min)
            {
                T negativeone = blas.Provider.Negate(blas.Provider.One);
                for (int i = min; i < max; ++i)
                {
                    // Assuming that multiplication takes the same time as negation (?) - this is just as good
                    int len = B[i].Length;
                    diff[i] = B[i].Copy();
                    blas.SCAL(diff[i], negativeone, 0, len);
                }
            }
            return diff;
        }
        private T[][] SubtractBandsInPlace(T[][] A, T[][] B, IDenseBLAS1<T> blas)
        {
            int alen = A.Length,
                blen = B.Length;

            T negone = blas.Provider.Negate(blas.Provider.One);

            // Requires re-structuring of bands
            if (blen > alen)
            {
                T[][] sum = new T[blen][];

                // only shallow copying is needed, and everything is 0-indexed
                for (int i = 0; i < alen; ++i)
                {
                    sum[i] = A[i];
                }

                // These will not be re-visited by the subtract so direct copying is ok
                for (int i = alen; i < blen; ++i)
                {
                    T[] diag = B[i].Copy();
                    blas.SCAL(diag, negone, 0, diag.Length);
                    sum[i] = diag;
                }
                A = sum;
            }


            int min = Math.Min(alen, blen);
            for (int i = 0; i < min; ++i)
            {
                blas.SUB(A[i], B[i], A[i], 0, A[i].Length);
            }

            return A;
        }

        internal BandMatrix<T> Add(BandMatrix<T> matrix, Func<T[], T[], T[]> Add)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            MatrixChecks.AssertMatrixDimensionsEqual(this, matrix);

            T[][] upperBandSum = AddBands(_upperBands, matrix._upperBands, Add);
            T[] diagonal = Add(_diagonal, matrix._diagonal);
            T[][] lowerBandSum = AddBands(_lowerBands, matrix._lowerBands, Add);
            return new BandMatrix<T>(Rows, Columns, upperBandSum, diagonal, lowerBandSum);
        }
        internal BandMatrix<T> Subtract(BandMatrix<T> matrix, Func<T[], T[], T[]> Subtract, Func<T[], T[]> Negate)
        {
            MatrixChecks.CheckNotNull(matrix);
            MatrixChecks.AssertMatrixDimensionsEqual(this, matrix);

            T[][] upperBandSum = SubtractBands(_upperBands, matrix._upperBands, Subtract, Negate);
            T[] diagonal = Subtract(_diagonal, matrix._diagonal);
            T[][] lowerBandSum = SubtractBands(_lowerBands, matrix._lowerBands, Subtract, Negate);
            return new BandMatrix<T>(Rows, Columns, upperBandSum, diagonal, lowerBandSum);
        }
        internal BandMatrix<T> MultiplyParallel2(BandMatrix<T> matrix, IDenseBLAS1<T> blas)
        {
            MatrixChecks.CheckNotNull(matrix);
            MatrixChecks.AssertMatrixDimensionsForMultiplication(this, matrix);

            int upper = _upperBandwidth + matrix._upperBandwidth;
            int lower = _lowerBandwidth + matrix._lowerBandwidth;

            BandMatrix<T> product = new BandMatrix<T>(Rows, matrix.Columns, upper, lower);

            T[][] prod_upper = product._upperBands,
                prod_lower = product._lowerBands,
                A_upper = _upperBands,
                A_lower = _lowerBands,
                B_upper = matrix._upperBands,
                B_lower = matrix._lowerBands;

            T[] prod_diag = product._diagonal,
                A_diag = _diagonal,
                B_diag = matrix._diagonal,
                A_row = new T[Columns],
                B_col = new T[Columns];

            int row_end = Math.Min(Rows, Math.Min(product.Rows, product.Columns) + lower);
            Parallel.ForEach(Partitioner.Create(0, row_end), range =>
            {
                int min = range.Item1, max = range.Item2, i, j, k;
                for (i = min; i < max; ++i)
                {
                    int i_ = i + 1,
                        _i = i - 1,
                        col_start = Math.Max(0, i - lower),
                        col_end = Math.Min(matrix.Columns, i_ + upper),
                        start_lower_bound = Math.Max(0, i - _lowerBandwidth),
                        end_upper_bound = Math.Min(Rows, i_ + _upperBandwidth);

                    for (j = col_start; j < col_end; ++j)
                    {
                        int j_ = j + 1,
                            _j = j - 1,
                            k_start = Math.Max(start_lower_bound, j - matrix._upperBandwidth),
                            k_end = Math.Min(end_upper_bound, j_ + matrix._lowerBandwidth);

                        for (k = k_start; k < k_end; ++k)
                        {
                            if (k > i)
                            {
                                A_row[k] = A_upper[k - i_][i];
                            }
                            else if (k < i)
                            {
                                A_row[k] = A_lower[_i - k][k];
                            }
                            else
                            {
                                A_row[k] = A_diag[i];
                            }

                            if (k > j)
                            {
                                B_col[k] = B_lower[k - j_][j];
                            }
                            else if (k < j)
                            {
                                B_col[k] = B_upper[_j - k][k];
                            }
                            else
                            {
                                B_col[k] = B_diag[j];
                            }
                        }

                        T sum = blas.DOT(A_row, B_col, k_start, k_end);
                        if (j > i)
                        {
                            prod_upper[j - i_][i] = sum;
                        }
                        else if (j < i)
                        {
                            prod_lower[_i - j][j] = sum;
                        }
                        else
                        {
                            prod_diag[i] = sum;
                        }
                    }
                }
            });
            
            return product;
        }
        
        internal bool ApproximatelyEquals(BandMatrix<T> matrix, Func<T, T, bool> Equals)
        {
            if (Rows != matrix.Rows || Columns != matrix.Columns)
            {
                return false;
            }

            if (!ApproximatelyEquals(_upperBands, matrix._upperBands, Equals))
            {
                return false;
            }
            if (!ApproximatelyEquals(_lowerBands, matrix._lowerBands, Equals))
            {
                return false;
            }
            if (!ApproximatelyEquals(_diagonal, matrix._diagonal, Equals))
            {
                return false;
            }
            return true;
        }
        private bool ApproximatelyEquals(T[][] u, T[][] v, Func<T, T, bool> Equals)
        {
            int min = Math.Min(u.Length, v.Length), i;
            for (i = 0; i < min; ++i) 
            {
                if (!ApproximatelyEquals(u[i], v[i], Equals))
                {
                    return false;
                }
            }

            if (u.Length > min)
            {
                for (i = min; i < u.Length; ++i)
                {
                    foreach (T e in u[i])
                    {
                        if (!Equals(e, _zero))
                        {
                            return false;
                        }
                    }
                }
            }

            else if (v.Length > min)
            {
                for (i = min; i < v.Length; ++i)
                {
                    foreach (T e in v[i])
                    {
                        if (!Equals(e, _zero))
                        {
                            return false;
                        }
                    }
                }
            }
            return true;
        }
        private bool ApproximatelyEquals(T[] u, T[] v, Func<T, T, bool> Equals)
        {
            int len = u.Length;
            if (v.Length != len)
            {
                return false;
            }

            for (int i = 0; i < len; ++i)
            {
                if (!Equals(u[i], v[i]))
                {
                    return false;
                }
            }
            return true;
        }


        #region Public methods

        public override T Trace(IProvider<T> provider)
        {
            int len = _diagonal.Length, i;
            T sum = provider.Zero;
            for (i = 0; i < len; ++i)
            {
                sum = provider.Add(sum, _diagonal[i]);
            }
            return sum;
        }

        public override int Count(Predicate<T> predicate)
        {
            int b, i, len, count = 0;
            for (b = 0; b < _upperBandwidth; ++b)
            {
                T[] band = _upperBands[b];
                len = band.Length;
                for (i = 0; i < len; ++i)
                {
                    if (predicate(band[i]))
                    {
                        ++count;
                    }
                }
            }

            len = _diagonal.Length;
            for (i = 0; i < len; ++i)
            {
                if (predicate(_diagonal[i]))
                {
                    ++count;
                }
            }

            for (b = 0; b < _lowerBandwidth; ++b)
            {
                T[] band = _lowerBands[b];
                len = band.Length;
                for (i = 0; i < len; ++i)
                {
                    if (predicate(band[i]))
                    {
                        ++count;
                    }
                }
            }

            // Check zero 
            if (predicate(_zero))
            {
                count += Rows * Columns;
                for (b = 0; b < _upperBandwidth; ++b) count -= _upperBands[b].Length;
                count -= _diagonal.Length;
                for (b = 0; b < _lowerBandwidth; ++b) count -= _lowerBands[b].Length;
            }
            return count;
        }

        /// <summary>
        /// Returns the <txt>rowIndex</txt>-th row of the matrix as an array.
        /// </summary>
        /// <param name="rowIndex">The index of the row to return.</param>
        /// <returns>The <txt>rowIndex</txt>-th row of this band matrix.</returns>
        public T[] DenseRow(int rowIndex)
        {
            T[] row = new T[Columns];
            DenseRow(rowIndex, row, true);
            return row;
        }

        /// <summary>
        /// Copies the <txt>rowIndex</txt>-th row of the matrix into the array <txt>row</txt>.
        /// </summary>
        /// <param name="rowIndex">The index of the row to copy.</param>
        /// <param name="row">The array containing the copied row elements of the matrix.</param>
        /// <param name="clear">
        /// If <txt>true</txt>, the elements of the array <txt>row</txt> corresponding 
        /// to zeroes of the band matrix will be set to 0, otherwise they will be left unchanged.
        /// </param>
        public void DenseRow(int rowIndex, T[] row, bool clear = false)
        {
            if (Columns != row.Length)
            {
                throw new InvalidOperationException();
            }
            if (rowIndex < 0 || rowIndex >= Rows)
            {
                throw new ArgumentOutOfRangeException();
            }

            row[rowIndex] = _diagonal[rowIndex];

            int i, j;
            for (i = 0, j = rowIndex - 1; i < _lowerBandwidth; ++i, --j)
            {
                if (j < 0)
                {
                    break;
                }
                row[j] = _lowerBands[i][j];
            }
            if (clear)
            {
                for (; j >= 0; --j)
                {
                    row[j] = _zero;
                }
            }

            for (i = 0, j = rowIndex + 1; i < _upperBandwidth; ++i, ++j)
            {
                if (j >= Columns)
                {
                    break;
                }
                row[j] = _upperBands[i][rowIndex];
            }
            if (clear)
            {
                for (; j < Columns; ++j)
                {
                    row[j] = _zero;
                }
            }
        }

        public override DenseVector<T> Row(int rowIndex)
        {
            if (rowIndex < 0 || rowIndex >= Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(rowIndex));
            }

            T[] row = RectangularVector.Zeroes<T>(Columns);
            for (int b = 0; b < _upperBandwidth; ++b)
            {
                int col = rowIndex + b + 1;
                if (col < Columns)
                {
                    row[col] = _upperBands[b][rowIndex];
                }
            }
            if (rowIndex < _diagonal.Length)
            {
                row[rowIndex] = _diagonal[rowIndex];
            }
            for (int b = 0; b < _lowerBandwidth; ++b)
            {
                int col = rowIndex - b - 1;
                if (0 <= col && col < Columns)
                {
                    row[col] = _lowerBands[b][col];
                }
            }

            return new DenseVector<T>(row);
        }

        public override DenseVector<T> Column(int columnIndex)
        {
            if (columnIndex < 0 || columnIndex >= Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(columnIndex));
            }

            T[] column = RectangularVector.Zeroes<T>(Rows);

            for (int b = 0; b < _upperBandwidth; ++b)
            {
                int r = columnIndex - b - 1;
                if (0 <= r && r < Rows)
                {
                    column[r] = _upperBands[b][r];
                }
            }
            if (columnIndex < _diagonal.Length)
            {
                column[columnIndex] = _diagonal[columnIndex];
            }
            for (int b = 0; b < _lowerBandwidth; ++b)
            {
                int r = columnIndex + b + 1;
                if (r < Rows)
                {
                    column[r] = _lowerBands[b][columnIndex];
                }
            }
            return new DenseVector<T>(column);
        }

        public override SparseVector<T> SparseRow(int rowIndex)
        {
            if (rowIndex < 0 || rowIndex >= Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(rowIndex));
            }

            int colStart = Math.Max(0, rowIndex - _lowerBandwidth),
                colEnd = Math.Min(Columns, rowIndex + _upperBandwidth + 1),
                nz = colEnd - colStart;

            int[] indices = new int[nz];
            T[] values = new T[nz];

            int k = 0;
            for (int b = _lowerBandwidth - 1; b >= 0; --b)
            {
                int c = rowIndex - b - 1;
                if (c >= 0)
                {
                    indices[k] = c;
                    values[k++] = _lowerBands[b][c];
                }
            }

            if (rowIndex < _diagonal.Length)
            {
                indices[k] = rowIndex;
                values[k++] = _diagonal[rowIndex];
            }

            for (int b = 0; b < _upperBandwidth; ++b)
            {
                int c = rowIndex + b + 1;
                if (c < Columns)
                {
                    indices[k] = c;
                    values[k++] = _upperBands[b][rowIndex];
                }
            }

            return new SparseVector<T>(Columns, indices, values, true);
        }

        public override SparseVector<T> SparseColumn(int columnIndex)
        {
            if (columnIndex < 0 || columnIndex >= Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(columnIndex));
            }

            int rowStart = Math.Max(0, columnIndex - _upperBandwidth),
                rowEnd = Math.Min(Rows, columnIndex + _lowerBandwidth + 1),
                nz = rowEnd - rowStart;

            int[] indices = new int[nz];
            T[] values = new T[nz];

            int k = 0;
            for (int b = _upperBandwidth - 1; b >= 0; --b)
            {
                int r = columnIndex - b - 1;
                if (r >= 0)
                {
                    indices[k] = r;
                    values[k++] = _upperBands[b][r];
                }
            }

            if (columnIndex < _diagonal.Length)
            {
                indices[k] = columnIndex;
                values[k++] = _diagonal[columnIndex];
            }

            for (int b = 0; b < _lowerBandwidth; ++b)
            {
                int r = columnIndex + b + 1;
                if (r < Rows)
                {
                    indices[k] = r;
                    values[k++] = _lowerBands[b][columnIndex];
                }
            }

            return new SparseVector<T>(Rows, indices, values, true);
        }

        /// <summary>
        /// Returns the <txt>rowIndex</txt>-th row of the matrix as a sparse vector.
        /// </summary>
        /// <param name="rowIndex">The index of the row of the matrix to extract.</param>
        /// <returns>A row of this matrix as a sparse vector.</returns>
        public BigSparseVector<T> BigSparseRow(int rowIndex)
        {
            if (rowIndex < 0 || rowIndex >= Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(rowIndex));
            }

            Dictionary<long, T> elements = new Dictionary<long, T>();

            // Add all upper bands 
            for (int b = 0; b < _upperBandwidth; ++b)
            {
                int column = rowIndex + b + 1;
                if (column < Columns)
                {
                    elements[column] = _upperBands[b][rowIndex];
                }
            }

            // Add the diagonal 
            elements[rowIndex] = _diagonal[rowIndex];

            // Add all lower bands
            for (int b = 0; b < _lowerBandwidth; ++b)
            {
                int column = rowIndex - b - 1;
                T[] band = _lowerBands[b];
                if (0 <= column && column < band.Length)
                {
                    elements[column] = band[column];
                }
            }

            return new BigSparseVector<T>(Columns, elements);
        }

        /// <summary>
        /// Returns the <txt>colIndex</txt>-th row of the matrix as a sparse vector.
        /// </summary>
        /// <param name="colIndex">The index of the column to extract.</param>
        /// <returns>The column of the matrix.</returns>
        public BigSparseVector<T> BigSparseColumn(int colIndex)
        {
            if (colIndex < 0 || colIndex >= Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(colIndex));
            }

            int maxElements = Math.Min(Rows, Bandwidth);
            Dictionary<long, T> elements = new Dictionary<long, T>(maxElements);

            // Add all elements in the upper bands
            for (int b = 0; b < _upperBandwidth; ++b)
            {
                int rowIndex = colIndex - b - 1;
                T[] band = _upperBands[b];
                if (0 <= rowIndex && rowIndex < band.Length)
                {
                    elements[rowIndex] = band[rowIndex];
                }
            }

            // Add all elements from the diagonal
            if (colIndex < _diagonal.Length)
            {
                elements[colIndex] = _diagonal[colIndex];
            }

            // Add all elements in the lower bands
            for (int b = 0; b < _lowerBandwidth; ++b)
            {
                int rowIndex = colIndex + b + 1;
                if (rowIndex < Rows)
                {
                    elements[rowIndex] = _lowerBands[b][colIndex];
                }
            }

            return new BigSparseVector<T>(Rows, elements);
        }

        public override Matrix<T> Select(int[] rowIndices, int[] columnIndices)
        {
            if (rowIndices is null)
            {
                throw new ArgumentNullException(nameof(rowIndices));
            }
            if (columnIndices is null)
            {
                throw new ArgumentNullException(nameof(columnIndices));
            }

            ThrowHelper.AssertCollectionInRange(rowIndices, 0, Rows, nameof(rowIndices));
            ThrowHelper.AssertCollectionInRange(columnIndices, 0, Columns, nameof(columnIndices));

            // Iterate once to get the number of non-zeroes in selection
            int rlen = rowIndices.Length, 
                clen = columnIndices.Length, 
                upperBandwidth = 0, 
                lowerBandwidth = 0;

            for (int ri = 0; ri < rlen; ++ri)
            {
                int r = rowIndices[ri];
                for (int ci = 0; ci < clen; ++ci)
                {
                    int c = columnIndices[ci];

                    // Check if non-zero
                    if (r - c <= _lowerBandwidth && c - r <= _upperBandwidth)
                    {
                        // Calculate the new bandwidth
                        if (ri > ci)
                        {
                            if (ri - ci > lowerBandwidth)
                            {
                                lowerBandwidth = ri - ci;
                            }
                        }
                        else
                        {
                            if (ci - ri > upperBandwidth)
                            {
                                upperBandwidth = ci - ri;
                            }
                        }
                    }
                }
            }

            T[][] lower = CreateBands(clen, rlen, lowerBandwidth, false),
                upper = CreateBands(clen, rlen, upperBandwidth, true);
            T[] diag = RectangularVector.Zeroes<T>(Math.Min(rlen, clen));

            for (int ri = 0; ri < rlen; ++ri)
            {
                int r = rowIndices[ri];
                for (int ci = 0; ci < clen; ++ci)
                {
                    int c = columnIndices[ci];
                    if (r - c <= _lowerBandwidth && c - r <= _upperBandwidth)
                    {
                        T val;
                        if (r > c)
                        {
                            val = _lowerBands[r - c - 1][c];
                        }
                        else if (r < c)
                        {
                            val = _upperBands[c - r - 1][r];
                        }
                        else
                        {
                            val = _diagonal[r];
                        }

                        if (ri > ci)
                        {
                            lower[ri - ci - 1][ci] = val;
                        }
                        else if (ri < ci)
                        {
                            upper[ci - ri - 1][ri] = val;
                        }
                        else
                        {
                            diag[ri] = val;
                        }
                    }
                }
            }

            return new BandMatrix<T>(rlen, clen, upper, diag, lower);
        }

        public override Vector<T> SelectAsVector(int rowIndex, int[] columnIndices)
        {
            if (rowIndex < 0 || rowIndex >= Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(rowIndex));
            }
            if (columnIndices is null)
            {
                throw new ArgumentNullException(nameof(columnIndices));
            }
            ThrowHelper.AssertCollectionInRange(columnIndices, 0, Columns, nameof(columnIndices));

            // Count non-zeroes 
            int clen = columnIndices.Length, nz = 0;
            for (int ci = 0; ci < clen; ++ci)
            {
                int c = columnIndices[ci];
                if (rowIndex > c)
                {
                    if (rowIndex - c <= _lowerBandwidth)
                    {
                        ++nz;
                    }
                }
                else if (rowIndex < c)
                {
                    if (c - rowIndex <= _upperBandwidth)
                    {
                        ++nz;
                    }
                }
                else
                {
                    ++nz;
                }
            }

            // Construct the sparse vector
            int[] indices = new int[nz];
            T[] values = new T[nz];

            int k = 0;
            for (int ci = 0; ci < clen; ++ci)
            {
                int c = columnIndices[ci];
                if (rowIndex > c)
                {
                    if (rowIndex - c <= _lowerBandwidth)
                    {
                        indices[k] = ci;
                        values[k++] = _lowerBands[rowIndex - c - 1][c];
                    }
                }
                else if (rowIndex < c)
                {
                    if (c - rowIndex <= _upperBandwidth)
                    {
                        indices[k] = ci;
                        values[k++] = _upperBands[c - rowIndex - 1][rowIndex];
                    }
                }
                else
                {
                    indices[k] = ci;
                    values[k++] = _diagonal[c];
                }
            }

            return new SparseVector<T>(clen, indices, values, true);
        }

        public override Vector<T> SelectAsVector(int[] rowIndices, int columnIndex)
        {
            if (rowIndices is null)
            {
                throw new ArgumentNullException(nameof(rowIndices));
            }
            ThrowHelper.AssertCollectionInRange(rowIndices, 0, Rows, nameof(rowIndices));

            if (columnIndex < 0 || columnIndex >= Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(columnIndex));
            }

            int rlen = rowIndices.Length, nz = 0;
            for (int ri = 0; ri < rlen; ++ri)
            {
                int r = rowIndices[ri];
                if (r > columnIndex)
                {
                    if (r - columnIndex <= _lowerBandwidth)
                    {
                        ++nz;
                    }
                }
                else if (r < columnIndex)
                {
                    if (columnIndex - r <= _upperBandwidth)
                    {
                        ++nz;
                    }
                }
                else
                {
                    ++nz;
                }
            }

            int[] indices = new int[nz];
            T[] values = new T[nz];

            int k = 0;
            for (int ri = 0; ri < rlen; ++ri)
            {
                int r = rowIndices[ri];
                if (r > columnIndex)
                {
                    if (r - columnIndex <= _lowerBandwidth)
                    {
                        indices[k] = ri;
                        values[k++] = _lowerBands[r - columnIndex - 1][columnIndex];
                    }
                }
                else if (r < columnIndex)
                {
                    if (columnIndex - r <= _upperBandwidth)
                    {
                        indices[k] = ri;
                        values[k++] = _upperBands[columnIndex - r - 1][r];
                    }
                }
                else
                {
                    indices[k] = ri;
                    values[k++] = _diagonal[r];
                }
            }

            return new SparseVector<T>(rlen, indices, values, true);
        }

        public override DenseVector<T> RowSums(IProvider<T> provider)
        {
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }

            T[] sums = RectangularVector.Repeat(provider.Zero, Rows);

            for (int b = 0; b < _lowerBandwidth; ++b)
            {
                T[] band = _lowerBands[b];
                for (int i = 0; i < band.Length; ++i)
                {
                    int row = b + i + 1;
                    sums[row] = provider.Add(sums[row], band[i]);
                }
            }

            for (int i = 0; i < _diagonal.Length; ++i)
            {
                sums[i] = provider.Add(sums[i], _diagonal[i]);
            }

            for (int b = 0; b < _upperBandwidth; ++b)
            {
                T[] band = _upperBands[b];
                for (int i = 0; i < band.Length; ++i)
                {
                    sums[i] = provider.Add(sums[i], band[i]); // can be accelerated using level-1 BLAS
                }
            }

            return new DenseVector<T>(sums);
        }

        public override DenseVector<T> ColumnSums(IProvider<T> provider)
        {
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }

            T[] sums = RectangularVector.Repeat(provider.Zero, Columns);

            for (int b = 0; b < _lowerBandwidth; ++b)
            {
                T[] band = _lowerBands[b];
                for (int i = 0; i < band.Length; ++i)
                {
                    sums[i] = provider.Add(sums[i], band[i]);
                }
            }

            for (int i = 0; i < _diagonal.Length; ++i)
            {
                sums[i] = provider.Add(sums[i], _diagonal[i]);
            }

            for (int b = 0; b < _upperBandwidth; ++b)
            {
                T[] band = _upperBands[b];
                for (int i = 0; i < band.Length; ++i)
                {
                    int index = b + i + 1;
                    sums[index] = provider.Add(sums[index], band[i]);
                }
            }

            return new DenseVector<T>(sums);
        }

        public override void SwapRows(int rowIndex1, int rowIndex2)
        {
            if (rowIndex1 < 0 || rowIndex1 >= Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(rowIndex1));
            }
            if (rowIndex2 < 0 || rowIndex2 >= Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(rowIndex2));
            }

            if (rowIndex1 == rowIndex2) return;

            throw new NotImplementedException(); // TODO : implement
        }

        public override void SwapColumns(int columnIndex1, int columnIndex2)
        {
            throw new NotImplementedException(); // TODO : implement
        }

        /// <summary>
        /// Clears the entire matrix (sets all values to zero).
        /// </summary>
        public override void Clear()
        {
            _upperBands = new T[0][];
            _lowerBands = new T[0][];

            _upperBandwidth = 0;
            _lowerBandwidth = 0;

            if (typeof(T).IsValueType)
            {
                Array.Clear(_diagonal, 0, _diagonal.Length);
            }
            else
            {
                for (int i = 0; i < _diagonal.Length; ++i)
                {
                    _diagonal[i] = _zero;
                }
            }
        }

        public override void Clear(Predicate<T> select)
        {
            for (int b = 0; b < _upperBandwidth; ++b)
            {
                T[] band = _upperBands[b];
                int len = band.Length, i;
                for (i = 0; i < len; ++i)
                {
                    if (select(band[i]))
                    {
                        band[i] = _zero;
                    }
                }
            }

            for (int b = 0; b < _lowerBandwidth; ++b)
            {
                T[] band = _lowerBands[b];
                int len = band.Length, i;
                for (i = 0; i < len; ++i)
                {
                    if (select(band[i]))
                    {
                        band[i] = _zero;
                    }
                }
            }

            for (int i = 0; i < _diagonal.Length; ++i)
            {
                if (select(_diagonal[i]))
                {
                    _diagonal[i] = _zero;
                }
            }
        }

        /// <summary>
        /// Clears a row at the specified index from this array.
        /// </summary>
        /// <param name="rowIndex"></param>
        public override void ClearRow(int rowIndex)
        {
            if (rowIndex < 0 || rowIndex >= Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(rowIndex));
            }

            // No rangechecks here since _upperBands must be full
            for (int b = 0; b < _upperBandwidth; ++b)
            {
                T[] diag = _upperBands[b];
                if (rowIndex < diag.Length)
                {
                    diag[rowIndex] = _zero;
                }
            }

            if (rowIndex < _diagonal.Length)
            {
                _diagonal[rowIndex] = _zero;
            }

            for (int b = 0; b < _lowerBandwidth; ++b)
            {
                int index = rowIndex - b - 1;
                T[] band = _lowerBands[b];
                if (0 <= index && index < band.Length)
                {
                    band[index] = _zero;
                }
            }
        }

        public override void ClearColumn(int columnIndex)
        {
            if (columnIndex < 0 || columnIndex >= Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(columnIndex));
            }

            for (int b = 0; b < _lowerBandwidth; ++b)
            {
                T[] diag = _lowerBands[b];
                if (columnIndex < diag.Length)
                {
                    diag[columnIndex] = _zero;
                }
            }

            if (columnIndex < _diagonal.Length)
            {
                _diagonal[columnIndex] = _zero;
            }

            for (int b = 0; b < _upperBandwidth; ++b)
            {
                int index = columnIndex - b - 1;
                T[] band = _upperBands[b];
                if (0 <= index && index < band.Length)
                {
                    band[index] = _zero;
                }
            }
        }

        public override void ClearSubmatrix(int rowIndex, int columnIndex, int rows, int columns)
        {
            if (rows < 0 || rows >= Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(rows));
            }
            if (columns < 0 || columns >= Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(columns));
            }

            int rowEnd = rowIndex + rows;
            if (rowIndex < 0 || rowEnd >= Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(rowIndex));
            }

            int columnEnd = columnIndex + columns;
            if (columnIndex < 0 || columnEnd >= Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(columnIndex));
            }

            for (int r = rowIndex; r < rowEnd; ++r)
            {
                int end = Math.Min(r, columnIndex + columns);
                for (int c = Math.Max(columnIndex, r - _lowerBandwidth); c < end; ++c)
                {
                    _lowerBands[r - c - 1][c] = _zero;
                }

                if (columnIndex <= r && r < columnEnd)
                {
                    _diagonal[r] = _zero;
                }

                end = Math.Min(columnEnd - 1, r + _upperBandwidth);
                for (int c = Math.Max(columnIndex, r + 1); c <= end; ++c)
                {
                    _upperBands[c - r - 1][r] = _zero;
                }
            }
        }

        public override void ClearLower()
        {
            _lowerBandwidth = 0;
            _lowerBands = new T[0][];
        }

        public override void ClearUpper()
        {
            _upperBandwidth = 0;
            _upperBands = new T[0][];
        }

        public F FrobeniusNorm<F>(IProvider<F> provider, Func<T, F> norm)
        {
            F sum = provider.Zero;
            for (int b = 0; b < _upperBandwidth; ++b)
            {
                T[] band = _upperBands[b];
                for (int i = 0; i < band.Length; ++i)
                {
                    F n = norm(band[i]);
                    sum = provider.Add(sum, provider.Multiply(n, n));
                }
            }

            for (int i = 0; i < _diagonal.Length; ++i)
            {
                F n = norm(_diagonal[i]);
                sum = provider.Add(sum, provider.Multiply(n, n));
            }

            for (int b = 0; b < _lowerBandwidth; ++b)
            {
                T[] band = _lowerBands[b];
                for (int i = 0; i < band.Length; ++i)
                {
                    F n = norm(band[i]);
                    sum = provider.Add(sum, provider.Multiply(n, n));
                }
            }
            return provider.Sqrt(sum);
        }

        /// <summary>
        /// Returns the Cholesky decomposition of this matrix, if it exists.
        /// </summary>
        /// <param name="blas1">A dense level-1 BLAS implementation.</param>
        /// <returns>The Cholesky decomposition of this matrix.</returns>
        public Cholesky<T> Cholesky(IDenseBLAS1Provider<T> blas1)
        {
            if (!(blas1 is IDenseBLAS1<T> b))
            {
                throw new NotSupportedException(ExceptionMessages.TypeNotSupported);
            }
            return new Cholesky<T>(CholeskyFactor(b.Provider), b);
        }

        /// <summary>
        /// Calculate the lower triangular Cholesky factor of this matrix, if it exists. 
        /// </summary>
        /// <param name="provider">The provider used for elementwise operations.</param>
        /// <returns>The Cholesky factor, $L$.</returns>
        public BandMatrix<T> CholeskyFactor(IProvider<T> provider)
        {
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            BandCholesky<T> chol = new BandCholesky<T>();
            return chol.CholUp(this, provider);
        }
        public BandMatrix<T> CholeskyFactor(IDenseBLAS1Provider<T> blas1)
        {
            if (blas1 is null)
            {
                throw new ArgumentNullException(nameof(blas1));
            }
            if (!(blas1 is IDenseBLAS1<T> b))
            {
                throw new NotSupportedException(nameof(blas1));
            }
            BandCholesky<T> chol = new BandCholesky<T>();
            return chol.CholUp(this, b);
        }

        /// <summary>
        /// Returns the unpivoted LU decomposition of this matrix. This method is 
        /// only defined for square banded matrices with a non-zero diagonal. Numerical 
        /// stability is guaranteed if the matrix is diagonally-dominant.
        /// </summary>
        /// <param name="blas1">The level-1 BLAS implementation used to compute the LU 
        /// factorization.</param>
        /// <param name="overwrite">If <txt>true</txt>, this matrix will be overwritten
        /// with the lower and upper triangular factors. Otherwise, this matrix will be 
        /// unchanged and a new matrix is created to store the triangular factors. 
        /// The method is more performant and memory efficient if set to <txt>true</txt>.
        /// </param>
        /// <returns></returns>
        public LU<T> LU(IDenseBLAS1Provider<T> blas1, bool overwrite = false)
        {
            if (blas1 is null)
            {
                throw new ArgumentNullException(nameof(blas1));
            }
            if (!(blas1 is IDenseBLAS1<T> b))
            {
                throw new NotSupportedException(nameof(b));
            }

            BandLUAlgorithm<T> algo = new BandLUAlgorithm<T>(b);
            return algo.Decompose(overwrite ? this : Copy());
        }

        internal override bool SolveTriangular(T[] b, T[] x, IDenseBLAS1<T> blas, bool lower, bool transpose)
        {
            // Assumes that the matrix is square 
            if (!IsSquare)
            {
                throw new NotImplementedException(ExceptionMessages.MatrixNotSquare);
            }

            int dim = Rows;
            IProvider<T> provider = blas.Provider;

            // If lower is true, then all upper bands will be ignored, and vice versa
            if (lower && !transpose || !lower && transpose)
            {
                // Sparse matrix technique: gather all entries from the linked list and 
                // store them in buff, then use dense BLAS to operate, then put the values back 
                // into linked list

                // Even with the additional overhead of having to create an array and populating it 
                // within each inner loop, this method outperforms the equivalent one using IProvider
                // for bandwidths under ~ 1000
                T[] row_buff = new T[dim];

                for (int r = 0; r < dim; ++r)
                {
                    // Store [r - bandwidth, r) into the row buffer

                    // The first column
                    int c_start = Math.Max(0, r - _lowerBandwidth);

                    // The first band index
                    int i = r - c_start - 1;

                    for (int c = c_start; c < r; ++c, --i)
                    {
                        row_buff[c] = _lowerBands[i][c];
                    }

                    T Lx = blas.DOT(row_buff, x, c_start, r);
                    T diag = _diagonal[r];
                    if (diag.Equals(_zero))
                    {
                        return false;
                    }
                    x[r] = provider.Divide(provider.Subtract(b[r], Lx), diag);
                }
                return true;
            }
            else
            {
                T[] row_buff = new T[dim];
                for (int r = dim - 1; r >= 0; --r)
                {
                    // the last column
                    int c_end = Math.Min(r + _upperBandwidth + 1, dim);
                    int c_start = r + 1;
                    for (int c = c_start; c < c_end; ++c)
                    {
                        row_buff[c] = _upperBands[c - c_start][r];
                    }

                    T Lx = blas.DOT(row_buff, x, c_start, c_end);
                    T diag = _diagonal[r];
                    if (diag.Equals(_zero))
                    {
                        return false;
                    }
                    x[r] = provider.Divide(provider.Subtract(b[r], Lx), diag);
                }
                return true;
            }
        }
        internal bool SolveTriangular(T[] b, T[] x, IProvider<T> provider, bool lower, bool transpose)
        {
            // Assumes that the matrix is square 
            if (!IsSquare)
            {
                throw new NotImplementedException(ExceptionMessages.MatrixNotSquare);
            }

            int dim = Rows;

            // If lower is true, then all upper bands will be ignored, and vice versa
            if (lower && !transpose || !lower && transpose)
            {
                for (int r = 0; r < dim; ++r)
                {
                    // Store [r - bandwidth, r) into the row buffer

                    // The first column
                    int c_start = Math.Max(0, r - _lowerBandwidth);

                    // The first band index
                    int i = r - c_start - 1;

                    T Lx = provider.Zero;
                    for (int c = c_start; c < r; ++c, --i)
                    {
                        Lx = provider.Add(Lx, provider.Multiply(x[c], _lowerBands[i][c]));
                    }

                    T diag = _diagonal[r];
                    if (diag.Equals(_zero))
                    {
                        return false;
                    }
                    x[r] = provider.Divide(provider.Subtract(b[r], Lx), diag);
                }
                return true;
            }
            else
            {
                for (int r = dim - 1; r >= 0; --r)
                {
                    // the last column
                    int c_end = Math.Min(r + _upperBandwidth + 1, dim);
                    int c_start = r + 1;

                    T Lx = provider.Zero;
                    for (int c = c_start; c < c_end; ++c)
                    {
                        Lx = provider.Add(Lx, provider.Multiply(x[c], _upperBands[c - c_start][r]));
                    }

                    T diag = _diagonal[r];
                    if (diag.Equals(_zero))
                    {
                        return false;
                    }
                    x[r] = provider.Divide(provider.Subtract(b[r], Lx), diag);
                }
                return true;
            }
        }

        internal void Solve(T[] b, T[] x, IDenseBLAS1<T> blas, IRealProvider<T> provider, T threshold)
        {
            BandSolver<T> solver = new BandSolver<T>(provider, blas, threshold);
            solver.SolveWithPivoting(_lowerBands, _diagonal, _upperBands, Rows, Columns, b, x);
        }


        #region Operations

        public override Matrix<T> Add(Matrix<T> matrix, IProvider<T> provider)
        {
            if (CompareRepresentationPriority(this, matrix) < 0)
            {
                return matrix.Add(this, provider);
            }
            return Add(matrix.ToBandMatrix(), provider);
        }

        public BandMatrix<T> Add(BandMatrix<T> matrix, IProvider<T> provider)
        {
            return Add(matrix, new DefaultNativeProvider<T>(provider));
        }

        /// <summary>
        /// Adds another band matrix to this band matrix, using the specified level-1 BLAS provider.
        /// </summary>
        /// <param name="matrix">The matrix to add.</param>
        /// <param name="provider">The level-1 BLAS provider.</param>
        /// <returns>The matrix sum.</returns>
        public BandMatrix<T> Add(BandMatrix<T> matrix, IDenseBLAS1Provider<T> provider)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            if (Rows != matrix.Rows || Columns != matrix.Columns)
            {
                throw new InvalidOperationException("Sizes of matrices don't match.");
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            if (!(provider is IDenseBLAS1<T> blas1))
            {
                throw new NotSupportedException($"The type { typeof(T).ToString() } is not supported.");
            }

            T[][] upperBandSum = AddBands(_upperBands, matrix._upperBands, blas1);

            T[] diagonal = new T[_diagonal.Length];
            blas1.ADD(_diagonal, matrix._diagonal, diagonal, 0, _diagonal.Length);

            T[][] lowerBandSum = AddBands(_lowerBands, matrix._lowerBands, blas1);

            return new BandMatrix<T>(Rows, Columns, upperBandSum, diagonal, lowerBandSum);
        }

        public void AddInPlace(BandMatrix<T> matrix, IProvider<T> provider)
        {
            AddInPlace(matrix, new DefaultNativeProvider<T>(provider));
        }

        /// <summary>
        /// Add another matrix to this matrix, and overwrite this matrix with the sum. Equivalent to 
        /// incrementing this matrix with another matrix.
        /// </summary>
        /// <param name="matrix">A band matrix.</param>
        /// <param name="provider">A level-1 BLAS provider.</param>
        public void AddInPlace(BandMatrix<T> matrix, IDenseBLAS1Provider<T> provider)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            MatrixChecks.AssertMatrixDimensionsEqual(this, matrix);

            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            if (!(provider is IDenseBLAS1<T> blas1))
            {
                throw new NotSupportedException($"The type { typeof(T).ToString() } is not supported.");
            }

            _upperBands = AddBandsInPlace(_upperBands, matrix._upperBands, blas1);
            blas1.ADD(_diagonal, matrix._diagonal, _diagonal, 0, _diagonal.Length);
            _lowerBands = AddBandsInPlace(_lowerBands, matrix._lowerBands, blas1);

            _upperBandwidth = Math.Max(_upperBandwidth, matrix._upperBandwidth);
            _lowerBandwidth = Math.Max(_lowerBandwidth, matrix._lowerBandwidth);
        }

        public BandMatrix<T> Subtract(BandMatrix<T> matrix, IProvider<T> provider)
        {
            return Subtract(matrix, new DefaultNativeProvider<T>(provider));
        }

        /// <summary>
        /// Subtract another matrix from this matrix, using the specified level-1 BLAS provider.
        /// </summary>
        /// <param name="matrix">The matrix to subtract.</param>
        /// <param name="provider">A level-1 BLAS provider.</param>
        /// <returns>The matrix difference.</returns>
        public BandMatrix<T> Subtract(BandMatrix<T> matrix, IDenseBLAS1Provider<T> provider)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            if (Rows != matrix.Rows || Columns != matrix.Rows)
            {
                throw new InvalidOperationException("Sizes of matrices don't match.");
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            if (!(provider is IDenseBLAS1<T> blas1))
            {
                throw new NotSupportedException($"The type { typeof(T).ToString() } is not supported.");
            }
            MatrixChecks.CheckNotNull(matrix);
            MatrixChecks.AssertMatrixDimensionsEqual(this, matrix);

            T[][] upperBandSum = SubtractBands(_upperBands, matrix._upperBands, blas1);

            T[] diagonal = new T[_diagonal.Length];
            blas1.SUB(_diagonal, matrix._diagonal, diagonal, 0, _diagonal.Length);

            T[][] lowerBandSum = SubtractBands(_lowerBands, matrix._lowerBands, blas1);

            return new BandMatrix<T>(Rows, Columns, upperBandSum, diagonal, lowerBandSum);
        }

        public void SubtractInPlace(BandMatrix<T> matrix, IProvider<T> provider)
        {
            SubtractInPlace(matrix, new DefaultNativeProvider<T>(provider));
        }

        /// <summary>
        /// Subtract a band matrix from this matrix, and overwrite this matrix with the difference. 
        /// Equivalent to decrementing this matrix with another matrix.
        /// </summary>
        /// <param name="matrix">The matrix to subtract.</param>
        /// <param name="provider">A level-1 BLAS provider.</param>
        public void SubtractInPlace(BandMatrix<T> matrix, IDenseBLAS1Provider<T> provider)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            MatrixChecks.AssertMatrixDimensionsEqual(this, matrix);

            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            if (!(provider is IDenseBLAS1<T> blas1))
            {
                throw new NotSupportedException($"The type { typeof(T).ToString() } is not supported.");
            }

            _upperBands = SubtractBandsInPlace(_upperBands, matrix._upperBands, blas1);
            blas1.SUB(_diagonal, matrix._diagonal, _diagonal, 0, _diagonal.Length);
            _lowerBands = SubtractBandsInPlace(_lowerBands, matrix._lowerBands, blas1);

            _upperBandwidth = Math.Max(_upperBandwidth, matrix._upperBandwidth);
            _lowerBandwidth = Math.Max(_lowerBandwidth, matrix._lowerBandwidth);
        }

        public BandMatrix<T> Divide(T scalar, IProvider<T> provider)
        {
            return Divide(scalar, new DefaultNativeProvider<T>(provider));
        }

        /// <summary>
        /// Divide this matrix by a scalar, without changing this matrix, using the specified level-1 BLAS provider.
        /// </summary>
        /// <param name="scalar">The scalar to divide by.</param>
        /// <param name="provider">A level-1 BLAS provider.</param>
        public BandMatrix<T> Divide(T scalar, IDenseBLAS1Provider<T> provider)
        {
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            if (!(provider is IDenseBLAS1<T> blas1))
            {
                throw new NotSupportedException($"The type { typeof(T).ToString() } is not supported.");
            }

            // A / s = A * (1 / s)
            IProvider<T> p = blas1.Provider;
            return Multiply(p.Divide(p.One, scalar), provider);
        }

        /// <summary>
        /// Divide this band matrix by a scalar, overwriting this matrix, and using the specified level-1 BLAS provider.
        /// </summary>
        /// <param name="scalar">The scalar to divide by.</param>
        /// <param name="provider">A level-1 BLAS provider.</param>
        public void DivideInPlace(T scalar, IDenseBLAS1Provider<T> provider)
        {
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            if (!(provider is IDenseBLAS1<T> blas1))
            {
                throw new NotSupportedException($"The type { typeof(T).ToString() } is not supported.");
            }

            // A / s = A * (1 / s)
            IProvider<T> p = blas1.Provider;
            MultiplyInPlace(p.Divide(p.One, scalar), provider);
        }

        /// <summary>
        /// Multiply this band matrix by a vector represented as an array, using the specified level-1 BLAS provider.
        /// The result is stored in the 'product' array.
        /// </summary>
        /// <param name="vector">A vector of the same dimensions as the number of columns of this matrix.</param>
        /// <param name="product">The vector which will hold the result of the matrix-vector product.</param>
        /// <param name="provider">A level-1 BLAS provider.</param>
        /// <param name="increment">If <txt>true</txt>, the product array will be increment by the matrix-vector 
        /// product. Otherwise, it will be set to the value of the matrix-vector product.</param>
        public void Multiply(T[] vector, T[] product, IDenseBLAS1Provider<T> provider, bool increment)
        {
            if (vector is null)
            {
                throw new ArgumentNullException(nameof(vector));
            }
            if (product is null)
            {
                throw new ArgumentNullException(nameof(product));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            if (Columns != vector.Length)
            {
                throw new InvalidOperationException("The number of columns of this matrix does not match the dimension of this vector.");
            }
            if (Rows > product.Length)
            {
                throw new ArgumentOutOfRangeException($"'{nameof(product)}' is not large enough to store the matrix-vector product.");
            }
            if (!(provider is IDenseBLAS1<T> blas))
            {
                throw new NotSupportedException($"The type { typeof(T).ToString() } is not supported.");
            }

            Multiply(vector, product, blas, Rows, Columns, _upperBands, _diagonal, _lowerBands, increment);
        }

        private static void Multiply(T[] vector, T[] product, IDenseBLAS1<T> blas, int rows, int cols, T[][] upper, T[] diag, T[][] lower, bool increment)
        {
            if (vector is null)
            {
                throw new ArgumentNullException(nameof(vector));
            }
            if (product is null)
            {
                throw new ArgumentNullException(nameof(product));
            }
            if (blas is null)
            {
                throw new ArgumentNullException(nameof(blas));
            }
            if (cols != vector.Length)
            {
                throw new InvalidOperationException("The number of columns of this matrix does not match the dimension of this vector.");
            }
            if (rows > product.Length)
            {
                throw new ArgumentOutOfRangeException($"'{nameof(product)}' is not large enough to store the matrix-vector product.");
            }

            int lower_bw = lower.Length, 
                upper_bw = upper.Length, 
                i, j;

            T[] workspace = new T[cols];

            for (i = 0; i < rows; ++i)
            {
                int i_ = i + 1,
                    _i = i - 1,
                    start = Math.Max(0, i - lower_bw),
                    end = Math.Min(cols, i_ + upper_bw);
                for (j = start; j < end; ++j)
                {
                    if (j > i)
                    {
                        workspace[j] = upper[j - i_][i];
                    }
                    else if (j < i)
                    {
                        workspace[j] = lower[_i - j][j];
                    }
                    else
                    {
                        workspace[i] = diag[i];
                    }
                }

                if (increment)
                {
                    product[i] = blas.Provider.Add(product[i], blas.DOT(workspace, vector, start, end));
                }
                else
                {
                    product[i] = blas.DOT(workspace, vector, start, end);
                }
            }
        }
        internal override void Multiply(T[] vector, T[] product, IProvider<T> provider, bool increment)
        {
            Multiply(vector, product, new DefaultNativeProvider<T>(provider), increment);
        }

        internal override void TransposeAndMultiply(T[] vector, T[] product, IProvider<T> provider, bool increment)
        {
            Multiply(vector, product, new DefaultNativeProvider<T>(provider), Columns, Rows, _lowerBands, _diagonal, _upperBands, increment);
        }

        private static SparseVector<T> Multiply(T[][] lowerBands, T[] diagonal, T[][] upperBands, int lowerBandwidth, int upperBandwidth, 
            int Rows, SparseVector<T> vector, IProvider<T> provider)
        {

            // initialize workspaces of maximum dimension
            int[] indices = new int[Rows];
            T[] values = new T[Rows];

            // Set union - use this instead of having to keep track of zeroes
            BitArray in_set = new BitArray(Rows);

            int[] vect_indices = vector.Indices;
            T[] vect_values = vector.Values;

            int nz = 0;
            for (int k = 0; k < vect_indices.Length; ++k)
            {
                int c = vect_indices[k];
                T v = vect_values[k];

                // Upper bands
                for (int b = 0; b < upperBandwidth; ++b)
                {
                    int r = c - b - 1;
                    if (r >= 0)
                    {
                        T prod = provider.Multiply(upperBands[b][r], v);
                        if (!in_set[r])
                        {
                            values[r] = prod;
                            in_set[r] = true;
                            indices[nz++] = r;
                        }
                        else
                        {
                            values[r] = provider.Add(values[r], prod);
                        }
                    }
                }

                // diagonal
                if (c < Rows)
                {
                    T prod = provider.Multiply(diagonal[c], v);
                    if (!in_set[c])
                    {
                        in_set[c] = true;
                        indices[nz++] = c;
                        values[c] = prod;
                    }
                    else
                    {
                        values[c] = provider.Add(values[c], prod);
                    }
                }

                // Lower bands
                for (int b = 0; b < lowerBandwidth; ++b)
                {
                    int r = c + b + 1;
                    if (r < Rows)
                    {
                        T prod = provider.Multiply(lowerBands[b][c], v);
                        if (!in_set[r])
                        {
                            values[r] = prod;
                            in_set[r] = true;
                            indices[nz++] = r;
                        }
                        else
                        {
                            values[r] = provider.Add(values[r], prod);
                        }
                    }
                }
            }

            // Gather operation
            Array.Sort(indices, 0, nz);
            T[] prod_values = new T[nz];
            for (int i = 0; i < nz; ++i)
            {
                prod_values[i] = values[indices[i]];
            }

            return new SparseVector<T>(Rows, indices, prod_values, true);
        }
        public override SparseVector<T> Multiply(SparseVector<T> vector, IProvider<T> provider)
        {
            if (vector is null)
            {
                throw new ArgumentNullException(nameof(vector));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            if (vector.Dimension < Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(vector), "The vector dimension is less than the number of columns of the matrix.");
            }
            return Multiply(_lowerBands, _diagonal, _upperBands, _lowerBandwidth, _upperBandwidth, Rows, vector, provider);
        }
        public override SparseVector<T> TransposeAndMultiply(SparseVector<T> vector, IProvider<T> provider)
        {
            if (vector is null)
            {
                throw new ArgumentNullException(nameof(vector));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            if (vector.Dimension < Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(vector), "The vector dimension is less than the number of rows of the matrix.");
            }
            return Multiply(_upperBands, _diagonal, _lowerBands, _upperBandwidth, _lowerBandwidth, Columns, vector, provider);
        }

        /// <summary>
        /// Multiply this band matrix by a dense vector, using the specified level-1 BLAS provider. Neither the 
        /// matrix nor the vector are changed by this method.
        /// </summary>
        /// <param name="vector">A vector of the same dimensions as the number of columns of this matrix.</param>
        /// <param name="provider">A level-1 BLAS provider.</param>
        /// <returns>A dense vector representing the result of the matrix-vector multiplication.</returns>
        public DenseVector<T> Multiply(DenseVector<T> vector, IDenseBLAS1Provider<T> provider)
        {
            T[] product = new T[Rows];
            Multiply(vector.Values, product, provider, false);
            return new DenseVector<T>(product);
        }

        public T[] Multiply(T[] vector, IDenseBLAS1Provider<T> provider)
        {
            T[] product = new T[Rows];
            Multiply(vector, product, provider, false);
            return product;
        }

        /// <summary>
        /// Postmultiply this matrix by a sparse vector, returning the result as a dense vector 
        /// without altering the original multiplicands.
        /// </summary>
        /// <param name="vector">The sparse vector to postmultiply by.</param>
        /// <param name="provider">The provider used to multiply matrix elements together.</param>
        /// <returns>The matrix-vector product.</returns>
        public DenseVector<T> Multiply(BigSparseVector<T> vector, IProvider<T> provider)
        {
            if (vector is null)
            {
                throw new ArgumentNullException(nameof(vector));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }

            if (vector.Dimension != Columns)
            {
                throw new InvalidOperationException("The number of columns of this matrix does not match the dimension of the vector.");
            }

            T[] product = RectangularVector.Zeroes<T>(Rows);
            Dictionary<long, T> values = vector.Values;

            // Iterate through the elements of the sparse vector
            foreach (KeyValuePair<long, T> e in values)
            {
                // The column id in this matrix that correspond to this component 
                int c = (int)e.Key;

                // upper band terms
                for (int b = 0; b < _upperBandwidth; ++b)
                {
                    int r = c - b - 1;
                    if (r >= 0)
                    {
                        product[r] = provider.Add(product[r], provider.Multiply(_upperBands[b][r], e.Value));
                    }
                }

                // diagona lterms
                product[c] = provider.Add(product[c], provider.Multiply(_diagonal[c], e.Value));

                // lower band terms
                for (int b = 0; b < _lowerBandwidth; ++b)
                {
                    int r = c + b + 1;
                    if (r < Rows)
                    {
                        product[r] = provider.Add(product[r], provider.Multiply(_lowerBands[b][c], e.Value));
                    }
                }
            }

            return new DenseVector<T>(product);
        }

        public BandMatrix<T> Multiply2(BandMatrix<T> matrix, IProvider<T> provider)
        {
            return Multiply2(matrix, new DefaultNativeProvider<T>(provider));
        }

        /// <summary>
        /// Postmultiplies this band matrix by another band matrix, using the specified level-1 BLAS provider.
        /// This method creates a new matrix and does not change either of the original matrices.
        /// </summary>
        /// <param name="matrix">The second matrix to postmultiply by.</param>
        /// <param name="provider">The level-1 BLAS provider.</param>
        /// <returns>The matrix-matrix multiplication result, as a band matrix.</returns>
        public BandMatrix<T> Multiply2(BandMatrix<T> matrix, IDenseBLAS1Provider<T> provider)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            MatrixChecks.AssertMatrixDimensionsForMultiplication(this, matrix);

            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            if (!(provider is IDenseBLAS1<T> blas))
            {
                throw new NotSupportedException($"The type { typeof(T).ToString() } is not supported.");
            }

            int upper = _upperBandwidth + matrix._upperBandwidth;
            int lower = _lowerBandwidth + matrix._lowerBandwidth;

            BandMatrix<T> product = new BandMatrix<T>(Rows, matrix.Columns, upper, lower);

            T[][] prod_upper = product._upperBands,
                prod_lower = product._lowerBands,
                A_upper = _upperBands,
                A_lower = _lowerBands,
                B_upper = matrix._upperBands,
                B_lower = matrix._lowerBands;

            T[] prod_diag = product._diagonal,
                A_diag = _diagonal,
                B_diag = matrix._diagonal,

                // workspaces of O(n)
                A_row = new T[Columns],
                B_col = new T[Columns];

            int row_end = Math.Min(Rows, Math.Min(product.Rows, product.Columns) + lower), i, j, k;
            for (i = 0; i < row_end; ++i)
            {
                int i_ = i + 1,
                    _i = i - 1,
                    col_start = Math.Max(0, i - lower),
                    col_end = Math.Min(matrix.Columns, i_ + upper),
                    start_lower_bound = Math.Max(0, i - _lowerBandwidth),
                    end_upper_bound = Math.Min(Columns, i_ + _upperBandwidth);

                for (j = col_start; j < col_end; ++j)
                {
                    int j_ = j + 1,
                        _j = j - 1,
                        k_start = Math.Max(start_lower_bound, j - matrix._upperBandwidth),
                        k_end = Math.Min(end_upper_bound, j_ + matrix._lowerBandwidth);

                    for (k = k_start; k < k_end; ++k)
                    {
                        if (k > i)
                        {
                            A_row[k] = A_upper[k - i_][i];
                        }
                        else if (k < i)
                        {
                            A_row[k] = A_lower[_i - k][k];
                        }
                        else
                        {
                            A_row[k] = A_diag[i];
                        }

                        if (k > j)
                        {
                            B_col[k] = B_lower[k - j_][j];
                        }
                        else if (k < j)
                        {
                            B_col[k] = B_upper[_j - k][k];
                        }
                        else
                        {
                            B_col[k] = B_diag[j];
                        }
                    }

                    T sum = blas.DOT(A_row, B_col, k_start, k_end);
                    if (j > i)
                    {
                        prod_upper[j - i_][i] = sum;
                    }
                    else if (j < i)
                    {
                        prod_lower[_i - j][j] = sum;
                    }
                    else
                    {
                        prod_diag[i] = sum;
                    }
                }
            }
            return product;
        }

        public BandMatrix<T> Multiply(BandMatrix<T> matrix, IProvider<T> provider)
        {
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            return Multiply(matrix, new DefaultNativeProvider<T>(provider));
        }

        /// <summary>
        /// Postmultiplies this band matrix by another band matrix, using the specified level-1 BLAS provider.
        /// This method creates a new matrix and does not change either of the original matrices.
        /// 
        /// This multiplication method that is more cache efficient the Multiply2 because it accesses the matrix 
        /// entries diagonally, i.e. in their natural order for the storage method. This method can be over 20 times 
        /// faster than <txt>Multiply2</txt> for matrices of size $O(10^4)$.
        /// </summary>
        /// <param name="matrix">The second matrix to postmultiply by.</param>
        /// <param name="provider">The level-1 BLAS provider.</param>
        /// <returns>The matrix-matrix multiplication result, as a band matrix.</returns>
        public BandMatrix<T> Multiply(BandMatrix<T> matrix, IDenseBLAS1Provider<T> blas)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            MatrixChecks.AssertMatrixDimensionsForMultiplication(this, matrix);

            if (blas is null)
            {
                throw new ArgumentNullException(nameof(blas));
            }
            if (!(blas is IDenseBLAS1<T> b))
            {
                throw new NotSupportedException($"The type { typeof(T).ToString() } is not supported.");
            }


            int upper = _upperBandwidth + matrix._upperBandwidth;
            int lower = _lowerBandwidth + matrix._lowerBandwidth;

            BandMatrix<T> product = new BandMatrix<T>(Rows, matrix.Columns, upper, lower);

            // Split A = (L1 + D1 + U1) and B = (L2 + D2 + U2)

            // L1 * L2
            MultiplyLowerLower(_lowerBands, matrix._lowerBands, product._lowerBands, b, false);

            // U1 * U2 using (B' * A')' = AB
            MultiplyLowerLower(matrix._upperBands, _upperBands, product._upperBands, b, false);

            // L1 * U2
            MultiplyLowerUpper(_lowerBands, matrix._upperBands, product._lowerBands, product._diagonal, product._upperBands, b, false);

            // U1 * L2
            MultiplyUpperLower(_upperBands, matrix._lowerBands, product._lowerBands, product._diagonal, product._upperBands, b, false);
            
            // L1 * D2
            MultiplyLowerDiagonal(_lowerBands, matrix._diagonal, product._lowerBands, b, false);

            // U1 * D2
            MultiplyUpperDiagonal(_upperBands, matrix._diagonal, product._upperBands, b, false);

            // D1 * D2
            b.PMULT(_diagonal, matrix._diagonal, product._diagonal, 0, Util.Min(_diagonal.Length, matrix._diagonal.Length, product._diagonal.Length), 0, 0);

            // D1 * U2
            MultiplyLowerDiagonal(matrix._upperBands, _diagonal, product._upperBands, b, false);

            // D1 * L2
            MultiplyUpperDiagonal(matrix._lowerBands, _diagonal, product._lowerBands, b, false);

            return product;
        }
        public BandMatrix<T> MultiplyParallel(BandMatrix<T> matrix, IDenseBLAS1Provider<T> blas)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            MatrixChecks.AssertMatrixDimensionsForMultiplication(this, matrix);

            if (blas is null)
            {
                throw new ArgumentNullException(nameof(blas));
            }
            if (!(blas is IDenseBLAS1<T> b))
            {
                throw new NotSupportedException($"The type { typeof(T).ToString() } is not supported.");
            }


            int upper = _upperBandwidth + matrix._upperBandwidth;
            int lower = _lowerBandwidth + matrix._lowerBandwidth;

            BandMatrix<T> product = new BandMatrix<T>(Rows, matrix.Columns, upper, lower);

            // Split A = (L1 + D1 + U1) and B = (L2 + D2 + U2)

            // L1 * L2
            MultiplyLowerLower(_lowerBands, matrix._lowerBands, product._lowerBands, b, true);

            // U1 * U2 using (B' * A')' = AB
            MultiplyLowerLower(matrix._upperBands, _upperBands, product._upperBands, b, true);

            // L1 * U2
            MultiplyLowerUpper(_lowerBands, matrix._upperBands, product._lowerBands, product._diagonal, product._upperBands, b, true);

            // U1 * L2
            MultiplyUpperLower(_upperBands, matrix._lowerBands, product._lowerBands, product._diagonal, product._upperBands, b, true);

            // L1 * D2
            MultiplyLowerDiagonal(_lowerBands, matrix._diagonal, product._lowerBands, b, true);

            // U1 * D2
            MultiplyUpperDiagonal(_upperBands, matrix._diagonal, product._upperBands, b, true);

            // D1 * D2
            b.PMULT(_diagonal, matrix._diagonal, product._diagonal, 0, Util.Min(_diagonal.Length, matrix._diagonal.Length, product._diagonal.Length), 0, 0);

            // D1 * U2
            MultiplyLowerDiagonal(matrix._upperBands, _diagonal, product._upperBands, b, true);

            // D1 * L2
            MultiplyUpperDiagonal(matrix._lowerBands, _diagonal, product._lowerBands, b, true);

            return product;
        }

        /// <summary>
        /// Multiply the lower bands of two matrices A, B, and store the 
        /// results in C (assume that C is correctly sized etc).
        /// </summary>
        private void MultiplyLowerLower(T[][] A, T[][] B, T[][] C, IDenseBLAS1<T> blas1, bool parallel)
        {
            if (!parallel)
            {
                // Skip the first lower diagonal of the product bands, because it is not impacted by the lower-lower product
                for (int b = 1; b < C.Length; ++b)
                {
                    // Diagonal multiplication, if we let M[i, k] represent the 
                    // i-th band (starting with 0 at the band closest to the main diagonal), 
                    // and starting at index k in that band (i.e. starting at column k of 
                    // the matrix), then the following holds:
                    // C[b, 0] = sum( A[b - k, k] . B[k - 1, 0], k in [1, b] )
                    // so long as the A[b - k, *] and B[k - 1, *] bands exist.

                    T[] C_band = C[b];
                    for (int k = 1; k <= b; ++k)
                    {
                        int Ai = b - k, Bi = k - 1;
                        // Range check
                        if (0 <= Ai && Ai < A.Length && 0 <= Bi && Bi < B.Length)
                        {
                            //Debug.WriteLine($"A({Ai}, {k}) . B({Bi}, {0})");
                            T[] A_band = A[Ai], B_band = B[Bi];

                            int len = Math.Min(B_band.Length, A_band.Length - k);
                            blas1.PMULT(B_band, A_band, C_band, 0, len, k, 0);
                        }
                    }
                }
            }
            else
            {
                Parallel.ForEach(Partitioner.Create(1, C.Length), range =>
                {
                    for (int b = range.Item1; b < range.Item2; ++b)
                    {
                        T[] C_band = C[b];
                        for (int k = 1; k <= b; ++k)
                        {
                            int Ai = b - k, Bi = k - 1;
                            if (0 <= Ai && Ai < A.Length && 0 <= Bi && Bi < B.Length)
                            {
                                T[] A_band = A[Ai], B_band = B[Bi];
                                int len = Math.Min(B_band.Length, A_band.Length - k);
                                blas1.PMULT(B_band, A_band, C_band, 0, len, k, 0);
                            }
                        }
                    }
                });
            }
        }
        private void MultiplyLowerUpper(T[][] A_lower, T[][] B_upper, T[][] C_lower, T[] C_diag, T[][] C_upper, IDenseBLAS1<T> blas1, bool parallel)
        {
            if (!parallel)
            {
                // C_upper[b, *] = sum( A_lower[k, 0] . B_upper[b + k + 1, 0], for k in [0, ..))
                for (int b = 0; b < C_upper.Length; ++b)
                {
                    T[] C_band = C_upper[b];
                    for (int k = 0; ; ++k)
                    {
                        int b_index = b + k + 1;
                        if (k >= A_lower.Length || b_index >= B_upper.Length)
                        {
                            break;
                        }

                        T[] A_band = A_lower[k], B_band = B_upper[b_index];
                        int len = Math.Min(A_band.Length, B_band.Length);
                        blas1.PMULT(A_band, B_band, C_band, 0, len, 0, k + 1);
                    }
                }

                // C_diag[0, *] = sum( A_lower[k, 0] . B_upper[k, 0], for k in [0, ..))
                int k_end = Math.Min(A_lower.Length, B_upper.Length);
                for (int k = 0; k < k_end; ++k)
                {
                    int len = Math.Min(A_lower[k].Length, B_upper[k].Length);
                    blas1.PMULT(A_lower[k], B_upper[k], C_diag, 0, len, 0, k + 1);
                }

                // C_lower[b, *] = sum( A_lower[b + k + 1, 0] . B_upper[k, 0], for k in [0, ..))
                for (int b = 0; b < C_lower.Length; ++b)
                {
                    T[] C_band = C_lower[b];
                    for (int k = 0; ; ++k)
                    {
                        int A_index = b + k + 1;
                        if (A_index >= A_lower.Length || k >= B_upper.Length)
                        {
                            break;
                        }

                        T[] A_band = A_lower[A_index], B_band = B_upper[k];
                        int len = Math.Min(A_band.Length, B_band.Length);
                        blas1.PMULT(A_band, B_band, C_band, 0, len, 0, k + 1);
                    }
                }
            }
            else
            {
                Parallel.ForEach(Partitioner.Create(0, C_lower.Length + 1 + C_upper.Length, 1), range =>
                {
                    // Upper fill in [0, C_upper.Length)
                    if (range.Item1 < C_upper.Length)
                    {
                        // C_upper[b, *] = sum( A_lower[k, 0] . B_upper[b + k + 1, 0], for k in [0, ..))
                        int b = range.Item1;
                        T[] C_band = C_upper[b];
                        for (int k = 0; ; ++k)
                        {
                            int b_index = b + k + 1;
                            if (k >= A_lower.Length || b_index >= B_upper.Length)
                            {
                                break;
                            }

                            T[] A_band = A_lower[k], B_band = B_upper[b_index];
                            int len = Math.Min(A_band.Length, B_band.Length);
                            blas1.PMULT(A_band, B_band, C_band, 0, len, 0, k + 1);
                        }
                    }

                    // Diagonal fill for [C_upper.Length, C_upper.Length + 1)
                    else if (range.Item1 == C_upper.Length)
                    {
                        // C_diag[0, *] = sum( A_lower[k, 0] . B_upper[k, 0], for k in [0, ..))
                        int k_end = Math.Min(A_lower.Length, B_upper.Length);
                        for (int k = 0; k < k_end; ++k)
                        {
                            int len = Math.Min(A_lower[k].Length, B_upper[k].Length);
                            blas1.PMULT(A_lower[k], B_upper[k], C_diag, 0, len, 0, k + 1);
                        }
                    }

                    // Lower fill for [C_upper.Length + 1, ..)
                    else
                    {
                        // C_lower[b, *] = sum( A_lower[b + k + 1, 0] . B_upper[k, 0], for k in [0, ..))
                        int b = range.Item1 - C_upper.Length - 1;
                        T[] C_band = C_lower[b];
                        for (int k = 0; ; ++k)
                        {
                            int A_index = b + k + 1;
                            if (A_index >= A_lower.Length || k >= B_upper.Length)
                            {
                                break;
                            }

                            T[] A_band = A_lower[A_index], B_band = B_upper[k];
                            int len = Math.Min(A_band.Length, B_band.Length);
                            blas1.PMULT(A_band, B_band, C_band, 0, len, 0, k + 1);
                        }
                    }
                });
            }
        }
        internal void MultiplyUpperLower(T[][] A_upper, T[][] B_lower, T[][] C_lower, T[] C_diag, T[][] C_upper, IDenseBLAS1<T> blas1, bool parallel)
        {
            if (!parallel)
            {
                // C_lower[b, 0] = sum( A_upper[k, b + 1] . B[b + k + 1, 0] for k in [0, ..))
                for (int b = 0; b < C_lower.Length; ++b)
                {
                    T[] C_band = C_lower[b];
                    for (int k = 0; ; ++k)
                    {
                        int b_index = b + k + 1;
                        if (k >= A_upper.Length || b_index >= B_lower.Length)
                        {
                            break;
                        }

                        T[] A_band = A_upper[k], B_band = B_lower[b_index];
                        int len = Util.Min(A_band.Length, B_band.Length, C_band.Length);
                        blas1.PMULT(B_band, A_band, C_band, 0, len, b + 1, 0);
                    }
                }

                // C_diag[0, 0] = sum( A_upper[k, 0] . B_lower[k, 0] for k in [0, ..))
                int k_end = Math.Min(A_upper.Length, B_lower.Length);
                for (int k = 0; k < k_end; ++k)
                {
                    T[] A_band = A_upper[k], B_band = B_lower[k];
                    int len = Util.Min(A_band.Length, B_band.Length, C_diag.Length);
                    blas1.PMULT(A_band, B_band, C_diag, 0, len, 0, 0);
                }

                // C_upper[b, 0] = sum( A[b + k + 1, 0] . B[k, b + 1] for k in [0, ..))
                for (int b = 0; b < C_upper.Length; ++b)
                {
                    T[] C_band = C_upper[b];
                    for (int k = 0; ; ++k)
                    {
                        int A_index = b + k + 1;
                        if (A_index >= A_upper.Length || k >= B_lower.Length)
                        {
                            break;
                        }

                        T[] A_band = A_upper[A_index], B_band = B_lower[k];
                        int len = Util.Min(A_band.Length, B_band.Length, C_band.Length);
                        blas1.PMULT(A_band, B_band, C_band, 0, len, b + 1, 0);
                    }
                }
            }
            else
            {
                Parallel.ForEach(Partitioner.Create(0, C_lower.Length + 1 + C_upper.Length, 1), range =>
                {
                    if (range.Item1 < C_lower.Length)
                    {
                        // C_lower[b, 0] = sum( A_upper[k, b + 1] . B[b + k + 1, 0] for k in [0, ..))
                        int b = range.Item1;
                        T[] C_band = C_lower[b];
                        for (int k = 0; ; ++k)
                        {
                            int b_index = b + k + 1;
                            if (k >= A_upper.Length || b_index >= B_lower.Length)
                            {
                                break;
                            }

                            T[] A_band = A_upper[k], B_band = B_lower[b_index];
                            int len = Util.Min(A_band.Length, B_band.Length, C_band.Length);
                            blas1.PMULT(B_band, A_band, C_band, 0, len, b + 1, 0);
                        }
                    }
                    else if (range.Item1 == C_lower.Length)
                    {
                        // C_diag[0, 0] = sum( A_upper[k, 0] . B_lower[k, 0] for k in [0, ..))
                        int k_end = Math.Min(A_upper.Length, B_lower.Length);
                        for (int k = 0; k < k_end; ++k)
                        {
                            T[] A_band = A_upper[k], B_band = B_lower[k];
                            int len = Util.Min(A_band.Length, B_band.Length, C_diag.Length);
                            blas1.PMULT(A_band, B_band, C_diag, 0, len, 0, 0);
                        }
                    }
                    else
                    {
                        // C_upper[b, 0] = sum( A[b + k + 1, 0] . B[k, b + 1] for k in [0, ..))
                        int b = range.Item1 - C_lower.Length - 1;
                        T[] C_band = C_upper[b];
                        for (int k = 0; ; ++k)
                        {
                            int A_index = b + k + 1;
                            if (A_index >= A_upper.Length || k >= B_lower.Length)
                            {
                                break;
                            }

                            T[] A_band = A_upper[A_index], B_band = B_lower[k];
                            int len = Util.Min(A_band.Length, B_band.Length, C_band.Length);
                            blas1.PMULT(A_band, B_band, C_band, 0, len, b + 1, 0);
                        }
                    }
                });
            }
            
        }
        internal void MultiplyUpperDiagonal(T[][] A_upper, T[] B_diag, T[][] C_upper, IDenseBLAS1<T> blas1, bool parallel)
        {
            if (!parallel)
            {
                for (int b = 0; b < A_upper.Length; ++b)
                {
                    T[] A_band = A_upper[b], C_band = C_upper[b];
                    int len = Util.Min(A_band.Length, B_diag.Length - b - 1, C_band.Length);
                    blas1.PMULT(A_band, B_diag, C_band, 0, len, b + 1, 0);
                }
            }
            else
            {
                Parallel.ForEach(Partitioner.Create(0, A_upper.Length, 1), range =>
                {
                    int b = range.Item1;
                    T[] A_band = A_upper[b], C_band = C_upper[b];
                    int len = Util.Min(A_band.Length, B_diag.Length - b - 1, C_band.Length);
                    blas1.PMULT(A_band, B_diag, C_band, 0, len, b + 1, 0);
                });
            }
        }
        internal void MultiplyLowerDiagonal(T[][] A_lower, T[] B_diag, T[][] C_lower, IDenseBLAS1<T> blas1, bool parallel)
        {
            if (!parallel)
            {
                for (int b = 0; b < A_lower.Length; ++b)
                {
                    T[] A_band = A_lower[b], C_band = C_lower[b];
                    int len = Util.Min(A_band.Length, B_diag.Length, C_band.Length);
                    blas1.PMULT(A_band, B_diag, C_band, 0, len, 0, 0);
                }
            }
            else
            {
                Parallel.ForEach(Partitioner.Create(0, A_lower.Length, 1), range =>
                {
                    int b = range.Item1;
                    T[] A_band = A_lower[b], C_band = C_lower[b];
                    int len = Util.Min(A_band.Length, B_diag.Length, C_band.Length);
                    blas1.PMULT(A_band, B_diag, C_band, 0, len, 0, 0);
                });
            }
        }

        public BandMatrix<T> Multiply(T scalar, IProvider<T> provider)
        {
            return Multiply(scalar, new DefaultNativeProvider<T>(provider));
        }

        /// <summary>
        /// Multiplies this matrix by a scalar value, using the specified level-1 BLAS provider.
        /// This matrix creates a new band matrix, and keeps the original matrix unchanged.
        /// </summary>
        /// <param name="scalar">The scalar to multiply by.</param>
        /// <param name="provider">The level-1 BLAS provider.</param>
        /// <returns>The matrix-vector multiplication result.</returns>
        public BandMatrix<T> Multiply(T scalar, IDenseBLAS1Provider<T> provider)
        {
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            if (!(provider is IDenseBLAS1<T> blas))
            {
                throw new NotSupportedException($"The type { typeof(T).ToString() } is not supported.");
            }

            T[][] upper = new T[_upperBandwidth][];
            for (int i = 0; i < _upperBandwidth; ++i)
            {
                T[] x = _upperBands[i].Copy();
                blas.SCAL(x, scalar, 0, x.Length);
                upper[i] = x;
            }

            T[][] lower = new T[_lowerBandwidth][];
            for (int i = 0; i < _lowerBandwidth; ++i)
            {
                T[] x = _lowerBands[i].Copy();
                blas.SCAL(x, scalar, 0, x.Length);
                lower[i] = x;
            }

            T[] diag = _diagonal.Copy();
            blas.SCAL(diag, scalar, 0, diag.Length);

            return new BandMatrix<T>(Rows, Columns, upper, diag, lower);
        }

        /// <summary>
        /// Multiply this band matrix by a scalar using the specified provider, and overwrite this matrix with the product.
        /// </summary>
        /// <param name="scalar">The scalar value to multiply by.</param>
        /// <param name="provider">The provider type </param>
        public override void MultiplyInPlace(T scalar, IProvider<T> provider)
        {
            MultiplyInPlace(scalar, new DefaultNativeProvider<T>(provider));
        }

        /// <summary>
        /// Multiplies this band matrix by a scalar, overwriting this matrix, and using the specified level-1 BLAS provider.
        /// </summary>
        /// <param name="scalar">A scalar value.</param>
        /// <param name="provider">A level-1 BLAS provider.</param>
        public void MultiplyInPlace(T scalar, IDenseBLAS1Provider<T> provider)
        {
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            if (!(provider is IDenseBLAS1<T> blas1))
            {
                throw new NotSupportedException($"The type { typeof(T).ToString() } is not supported.");
            }

            int i;
            for (i = 0; i < _upperBandwidth; ++i)
            {
                T[] diag = _upperBands[i];
                blas1.SCAL(diag, scalar, 0, diag.Length);
            }

            blas1.SCAL(_diagonal, scalar, 0, _diagonal.Length);

            for (i = 0; i < _lowerBandwidth; ++i)
            {
                T[] diag = _lowerBands[i];
                blas1.SCAL(diag, scalar, 0, diag.Length);
            }
        }

        public BandMatrix<T> AdditiveInverse(IProvider<T> provider)
        {
            return AdditiveInverse(new DefaultNativeProvider<T>(provider));
        }

        /// <summary>
        /// Return the additive inverse (negation) of this band matrix, as another band matrix, using the 
        /// specified level-1 BLAS provider.
        /// </summary>
        /// <param name="provider">The level-1 BLAS provider.</param>
        /// <returns>The additive inverse of this band matrix.</returns>
        public BandMatrix<T> AdditiveInverse(IDenseBLAS1Provider<T> provider)
        {
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            if (!(provider is IDenseBLAS1<T> blas1))
            {
                throw new NotSupportedException($"The type { typeof(T).ToString() } is not supported.");
            }

            // TODO: check the performance of multiplying by -1 and simply changing the sign...
            // A proper implementation of Big-x structs should keep these roughly in line, but 
            // good to check regardless.

            return Multiply(blas1.Provider.Negate(blas1.Provider.One), blas1);
        }

        #endregion


        /// <summary>
        /// Returns the transpose of this band matrix.
        /// </summary>
        /// <returns>The matrix transpose.</returns>
        public BandMatrix<T> Transpose()
        {
            
            T[][] upperBands = new T[_lowerBandwidth][];
            for (int i = 0; i < _lowerBandwidth; ++i)
            {
                upperBands[i] = _lowerBands[i].Copy();
            }

            T[] diagonal = _diagonal.Copy();

            T[][] lowerBands = new T[_upperBandwidth][];
            for (int i = 0; i < _upperBandwidth; ++i)
            {
                lowerBands[i] = _upperBands[i].Copy();
            }
            return new BandMatrix<T>(Columns, Rows, upperBands, diagonal, lowerBands);
        }
        public void TransposeInPlace()
        {
            Util.Swap(ref _lowerBands, ref _upperBands);

            int tmp = Columns;
            Columns = Rows;
            Rows = tmp;
        }

        /// <summary>
        /// Returns the direct sum of this band matrix with another, $A \otimes B$, as a band matrix.
        /// </summary>
        /// <param name="matrix">The second band matrix.</param>
        /// <returns>The direct sum.</returns>
        public BandMatrix<T> DirectSum(BandMatrix<T> matrix)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            int cr_offset = Columns - Rows;
            int rows = Rows + matrix.Rows, cols = Columns + matrix.Columns;
            int upperBandwidth = Math.Max(_upperBandwidth, matrix._upperBandwidth + cr_offset);
            int lowerBandwidth = Math.Max(_lowerBandwidth, matrix._lowerBandwidth - cr_offset);

            BandMatrix<T> sum = new BandMatrix<T>(rows, cols, upperBandwidth, lowerBandwidth);

            // Copy this matrix into sum
            for (int b = 0; b < _upperBandwidth; ++b)
            {
                Array.Copy(_upperBands[b], 0, sum._upperBands[b], 0, _upperBands[b].Length);
            }
            Array.Copy(_diagonal, 0, sum._diagonal, 0, _diagonal.Length);
            for (int b = 0; b < _lowerBandwidth; ++b)
            {
                Array.Copy(_lowerBands[b], 0, sum._lowerBands[b], 0, _lowerBands[b].Length);
            }

            // Copy the second matrix into sum
            for (int b = 0; b < matrix._upperBandwidth; ++b)
            {
                T[] src = matrix._upperBands[b];
                T[] dst;

                int index = b + cr_offset + 1;
                if (index > 0)
                {
                    dst = sum._upperBands[index - 1];
                }
                else if (index == 0)
                {
                    dst = sum._diagonal;
                }
                else
                {
                    dst = sum._lowerBands[-1 - index];
                }
                Array.Copy(src, 0, dst, dst.Length - src.Length, src.Length);
            }

            // Copy the main diagonal from the second matrix -> sum
            T[] d = matrix._diagonal, diagdst;
            if (cr_offset > 0)
            {
                diagdst = sum._upperBands[cr_offset - 1];
            }
            else if (cr_offset == 0)
            {
                diagdst = sum._diagonal;
            }
            else
            {
                diagdst = sum._lowerBands[-1 - cr_offset];
            }
            Array.Copy(d, 0, diagdst, diagdst.Length - d.Length, d.Length);

            // Copy the lower diagonal terms from second matrix -> sum
            for (int b = 0; b < matrix._lowerBandwidth; ++b)
            {
                T[] src = matrix._lowerBands[b];
                T[] dst;

                int index = b - cr_offset + 1;
                if (index > 0)
                {
                    dst = sum._lowerBands[index - 1];
                }
                else if (index == 0)
                {
                    dst = sum._diagonal;
                }
                else
                {
                    dst = sum._upperBands[-1 - index];
                }
                Array.Copy(src, 0, dst, dst.Length - src.Length, src.Length);
            }

            return sum;
        }

        /// <summary>
        /// Returns the leading diagonal of this band matrix, as an array.
        /// </summary>
        /// <returns>The leading diagonal of this matrix.</returns>
        public override T[] LeadingDiagonal()
        {
            return _diagonal.Copy();
        }


        #region Conversion methods

        public override BandMatrix<T> ToBandMatrix()
        {
            return this;
        }

        public override BlockDiagonalMatrix<T> ToBlockDiagonalMatrix()
        {
            return new BlockDiagonalMatrix<T>(this);
        }

        /// <summary>
        /// Convert this band matrix into a sparse matrix in Coordinate List (COO) form.
        /// </summary>
        /// <returns>A COO sparse matrix.</returns>
        public override COOSparseMatrix<T> ToCOOSparseMatrix()
        {
            return new COOSparseMatrix<T>(this);
        }

        /// <summary>
        /// Convert this band matrix into a sparse matrix in Compressed Sparse Column (CSC) format.
        /// </summary>
        /// <returns>This matrix in CSC format.</returns>
        public override CSCSparseMatrix<T> ToCSCSparseMatrix()
        {
            return new CSCSparseMatrix<T>(this);
        }

        /// <summary>
        /// Convert this band matrix into a sparse matrix in Compressed Sparse Row (CSR) form.
        /// </summary>
        /// <returns>A CSR sparse matrix.</returns>
        public override CSRSparseMatrix<T> ToCSRSparseMatrix()
        {
            return new CSRSparseMatrix<T>(this);
        }

        /// <summary>
        /// Converts this band matrix into a dense matrix.
        /// </summary>
        /// <returns>A <txt>DenseMatrix</txt> that is identically equal to this BandMatrix.</returns>
        public override DenseMatrix<T> ToDenseMatrix()
        {
            return new DenseMatrix<T>(this);
        }

        /// <summary>
        /// Converts this band matrix into a diagonal matrix. All off-diagonal terms will be ignored and 
        /// assumed to be zero.
        /// </summary>
        /// <returns>The diagonal matrix.</returns>
        public override DiagonalMatrix<T> ToDiagonalMatrix()
        {
            return new DiagonalMatrix<T>(_diagonal.Copy());
        }

        /// <summary>
        /// Converts this band matrix into a sparse matrix.
        /// </summary>
        /// <returns>A <txt>SparseMatrix</txt> equivalent of this band matrix.</returns>
        public override DOKSparseMatrix<T> ToDOKSparseMatrix()
        { 
            return new DOKSparseMatrix<T>(this);
        }

        /// <summary>
        /// Convert this band matrix into a sparse matrix in Modified Compressed Sparse Row format.
        /// </summary>
        /// <returns>This matrix in MCSR format.</returns>
        public override MCSRSparseMatrix<T> ToMCSRSparseMatrix()
        {
            return new MCSRSparseMatrix<T>(this);
        }

        public override SKYSparseMatrix<T> ToSKYSparseMatrix()
        {
            if (_upperBandwidth == 0)
            {
                return new SKYSparseMatrix<T>(this, SKYType.LOWER);
            }
            else if (_lowerBandwidth == 0)
            {
                return new SKYSparseMatrix<T>(this, SKYType.UPPER);
            }
            else
            {
                // Assuming symmetry
                if (!IsSymmetric(EqualityComparer<T>.Default))
                {
                    throw new InvalidOperationException("Matrix cannot be stored in Skyline format, since it is not lower triangular, upper triangular " +
                        "or symmetric.");
                }
                return new SKYSparseMatrix<T>(this, SKYType.SYMMETRIC);
            }
        }

        #endregion


        /// <summary>
        /// Returns a copy of this band matrix as an <txt>object</txt>.
        /// </summary>
        /// <returns>A copy of this matrix.</returns>
        public override object Clone()
        {
            return Copy();
        }
        /// <summary>
        /// Returns a copy of this band matrix, as a <txt>BandMatrix</txt> object.
        /// </summary>
        /// <returns>A copy of this matrix.</returns>
        public BandMatrix<T> Copy()
        {
            return new BandMatrix<T>(Rows, Columns, Copy(_upperBands), _diagonal.Copy(), Copy(_lowerBands));
        }
        private T[][] Copy(T[][] bands)
        {
            T[][] copy = new T[bands.Length][];
            for (int i = 0; i < bands.Length; ++i)
            {
                copy[i] = bands[i].Copy();
            }
            return copy;
        }

        /// <summary>
        /// Returns a vector containing all the elements of this matrix arranged in either row-major
        /// of column-major order.
        /// </summary>
        /// <param name="rowMajor">If <txt>true</txt>, elements will be arranged in row-major order. Otherwise
        /// they will be arranged in column-major order.</param>
        /// <returns>The vectorized form of this matrix.</returns>
        public BigSparseVector<T> Vectorize(bool rowMajor = true)
        {
            // approximate count that is >= the actual count
            int count = _diagonal.Length * (_lowerBandwidth + _upperBandwidth + 1);
            Dictionary<long, T> vector = new Dictionary<long, T>(count);

            long lr = Rows, lc = Columns;
            for (int b = 0; b < _lowerBandwidth; ++b)
            {
                T[] band = _lowerBands[b];
                int len = band.Length;
                for (int i = 0; i < len; ++i)
                {
                    int row = b + i + 1, col = i;
                    long index = rowMajor ? row * lc + col : row + col * lr;
                    vector[index] = band[i];
                }
            }

            for (int i = 0; i < _diagonal.Length; ++i)
            {
                long index = rowMajor ? i * lc + i : i + i * lr;
                vector[index] = _diagonal[i];
            }

            for (int b = 0; b < _upperBandwidth; ++b)
            {
                T[] band = _upperBands[b];
                int len = band.Length;
                for (int i = 0; i < len; ++i)
                {
                    int row = i, col = b + i + 1;
                    long index = rowMajor ? row * lc + col : row + col * lr;
                    vector[index] = band[i];
                }
            }

            return new BigSparseVector<T>(lr * lc, vector);
        }



        #region Boolean methods

        /// <summary>
        /// See <txt>Equals(BandMatrix<T>)</txt>.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            if (obj is null)
            {
                return false;
            }

            // Comparisons directly with BandMatrix<T> is faster
            if (obj is BandMatrix<T>)
            {
                return Equals(obj as BandMatrix<T>);
            }

            // If not a band matrix - use generic matrix comparisons
            return Equals(obj as Matrix<T>);
        }

        /// <summary>
        /// Returns the hash code of this matrix.
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            // Taking the hash code of each element is far too time consuming.
            // Instead, we take a sampling of (up to) 9 elements close to the
            // main diagonal (since non-zero elements of band matrices typically
            // concentrate around the main diagonal).

            int size_hash = 7 + Rows * 11 + Columns * 101 + UpperBandwidth * 103 + LowerBandwidth * 107;
            int el_hash = 5;

            // Hash along the main diagonal
            if (Rows > 0 && Columns > 0)
            {
                el_hash += _diagonal[0].GetHashCode() ^ 13;
                el_hash += _diagonal[_diagonal.Length - 1].GetHashCode() ^ 47;
            }
            if (Rows > 1 && Columns > 1)
            {
                el_hash += _diagonal[1].GetHashCode() ^ 17;
                el_hash += _diagonal[_diagonal.Length - 2].GetHashCode() ^ 43;
            }
            if (Rows > 2 && Columns > 2)
            {
                el_hash += _diagonal[2].GetHashCode() ^ 19;
                el_hash += _diagonal[_diagonal.Length - 3].GetHashCode() ^ 41;
            }

            // Add hash in upper and lower bands
            if (_upperBandwidth > 0 && _upperBands[0].Length > 0)
            {
                T[] diag = _upperBands[0];
                el_hash += diag[diag.Length / 2].GetHashCode() ^ 23;

                if (_upperBandwidth > 1 && _upperBands[1].Length > 0)
                {
                    diag = _upperBands[1];
                    el_hash += diag[diag.Length / 3].GetHashCode() ^ 29;
                }
            }
            if (_lowerBandwidth > 0 && _lowerBands[0].Length > 0)
            {
                T[] diag = _lowerBands[0];
                el_hash += diag[diag.Length / 2].GetHashCode() ^ 37;

                if (_lowerBandwidth > 1 && _lowerBands[1].Length > 0)
                {
                    diag = _lowerBands[1];
                    el_hash += diag[2 * diag.Length / 3].GetHashCode() ^ 29;
                }
            }

            return size_hash ^ el_hash;
        }

        /// <summary>
        /// Returns whether this band matrix is exactly equal to another band matrix.
        /// Two band matrices are deemed equal if they have the same dimension 
        /// and if every corresponding pair of elements are equal according to the <txt>Equals(object)</txt> method
        /// of the elements themselves.
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public bool Equals(BandMatrix<T> other)
        {
            if (Rows != other.Rows ||
                Columns != other.Columns ||
                _upperBandwidth != other._upperBandwidth ||
                _lowerBandwidth != other._lowerBandwidth)
            {
                return false;
            }

            if (!ApproximatelyEquals(_upperBands, other._upperBands, (a, b) => a.Equals(b)))
            {
                return false;
            }
            if (!ApproximatelyEquals(_lowerBands, other._lowerBands, (a, b) => a.Equals(b)))
            {
                return false;
            }
            if (!ApproximatelyEquals(_diagonal, other._diagonal, (a, b) => a.Equals(b)))
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Returns whether this band matrix is equal to another band matrix in value, using the specified 
        /// predicate that returns true if two matrix elements should be considered equal.
        /// <para>
        /// If the two matrices being compared do not have the same bandwidth, this method will compare 
        /// elements from any missing bands as though they are equal to <txt>new T()</txt>, which is assumed to be 
        /// identically zero.
        /// </para>
        /// </summary>
        /// <param name="other">The matrix to compare to.</param>
        /// <param name="equals">A predicate that takes two arguments (the two matrix elements to compare) 
        /// and returns whether they should be considered as being equal.</param>
        /// <returns>Whether the two matrices are equal in value.</returns>
        public bool Equals(BandMatrix<T> other, Func<T, T, bool> equals)
        {
            return ApproximatelyEquals(other, equals);
        }

        /// <summary>
        /// Returns whether this band matrix is equal in value to another band matrix, using an <txt>IEqualityComparer</txt>
        /// to decide whether two matrix elements should be considered equal.
        /// </summary>
        /// <param name="other">The band matrix to compare to.</param>
        /// <param name="comparer">The equality comparer object.</param>
        /// <returns>Whether the two matrices are equal in value.</returns>
        public bool Equals(BandMatrix<T> other, IEqualityComparer<T> comparer)
        {
            return ApproximatelyEquals(other, comparer.Equals);
        }

        public override bool IsLower(Predicate<T> isZero)
        {
            if (_upperBandwidth > 0)
            {
                int b, i, len;
                for (b = 0; b < _upperBandwidth; ++b)
                {
                    T[] band = _upperBands[b];
                    len = band.Length;
                    for (i = 0; i < len; ++i)
                    {
                        if (!isZero(band[i]))
                        {
                            return false;
                        }
                    }
                }
            }
            return true;
        }

        public override bool IsUpper(Predicate<T> isZero)
        {
            if (_lowerBandwidth > 0)
            {
                int b, i, len;
                for (b = 0; b < _lowerBandwidth; ++b)
                {
                    T[] band = _lowerBands[b];
                    len = band.Length;
                    for (i = 0; i < len; ++i)
                    {
                        if (!isZero(band[i]))
                        {
                            return false;
                        }
                    }
                }
            }
            return true;
        }

        public override bool IsDiagonal(Predicate<T> isZero)
        {
            if (_upperBandwidth == 0 && _lowerBandwidth == 0) return true;

            if (isZero is null)
            {
                throw new ArgumentNullException(nameof(isZero));
            }

            for (int b = 0; b < _upperBandwidth; ++b)
            {
                T[] band = _upperBands[b];
                int len = band.Length, i;
                for (i = 0; i < len; ++i)
                {
                    if (!isZero(band[i]))
                    {
                        return false;
                    }
                }
            }
            for (int b = 0; b < _lowerBandwidth; ++b)
            {
                T[] band = _lowerBands[b];
                int len = band.Length, i;
                for (i = 0; i < len; ++i)
                {
                    if (!isZero(band[i]))
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        public override bool IsBanded(int lowerBandwidth, int upperBandwidth, Predicate<T> isZero)
        {
            if (_lowerBandwidth <= lowerBandwidth && _upperBandwidth <= upperBandwidth)
            {
                return true;
            }

            if (isZero is null)
            {
                throw new ArgumentNullException(nameof(isZero));
            }

            for (int b = lowerBandwidth; b < _lowerBandwidth; ++b)
            {
                T[] band = _lowerBands[b];
                int len = band.Length, i;
                for (i = 0; i < len; ++i)
                {
                    if (!isZero(band[i]))
                    {
                        return false;
                    }
                }
            }
            for (int b = upperBandwidth; b < _upperBandwidth; ++b)
            {
                T[] band = _upperBands[b];
                int len = band.Length, i;
                for (i = 0; i < len; ++i)
                {
                    if (!isZero(band[i]))
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        public override bool IsZero(Predicate<T> isZero)
        {
            if (isZero is null)
            {
                throw new ArgumentNullException(nameof(isZero));
            }

            int len = _diagonal.Length, i;
            for (i = 0; i < len; ++i)
            {
                if (!isZero(_diagonal[i]))
                {
                    return false;
                }
            }

            for (int b = 0; b < _upperBandwidth; ++b)
            {
                T[] band = _upperBands[b];
                len = band.Length;
                for (i = 0; i < len; ++i)
                {
                    if (!isZero(band[i]))
                    {
                        return false;
                    }
                }
            }

            for (int b = 0; b < _lowerBandwidth; ++b)
            {
                T[] band = _lowerBands[b];
                len = band.Length;
                for (i = 0; i < len; ++i)
                {
                    if (!isZero(band[i]))
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        public override bool IsIdentity(Predicate<T> isZero, Predicate<T> isOne)
        {
            if (!IsSquare)
            {
                return false;
            }

            if (!IsDiagonal(isZero))
            {
                return false;
            }

            if (isOne is null)
            {
                throw new ArgumentNullException(nameof(isOne));
            }

            int len = Rows, i;
            for (i = 0; i < len; ++i) 
            { 
                if (!isOne(_diagonal[i]))
                {
                    return false;
                }
            }
            return true;
        }

        #endregion

        #endregion

        #region Operators (uses default BLAS1 providers)

        /// <summary>
        /// Returns the sum of two matrices.
        /// </summary>
        /// <param name="A">The left band matrix.</param>
        /// <param name="B">The right band matrix.</param>
        /// <returns>The matrix sum.</returns>
        public static BandMatrix<T> operator +(BandMatrix<T> A, BandMatrix<T> B)
        {
            if (A is null) throw new ArgumentNullException(nameof(A));
            return A.Add(B, ProviderFactory.GetDefaultBLAS1<T>());
        }

        /// <summary>
        /// Returns the difference between two band matrices.
        /// </summary>
        /// <param name="A">The left band matrix.</param>
        /// <param name="B">The right band matrix.</param>
        /// <returns>The matrix difference.</returns>
        public static BandMatrix<T> operator -(BandMatrix<T> A, BandMatrix<T> B)
        {
            if (A is null) throw new ArgumentNullException(nameof(A));
            return A.Subtract(B, ProviderFactory.GetDefaultBLAS1<T>());
        }

        /// <summary>
        /// Returns the negation (opposite; additive inverse) of a band matrix.
        /// </summary>
        /// <param name="A">A band matrix.</param>
        /// <returns>The additive inverse of the band matrix.</returns>
        public static BandMatrix<T> operator -(BandMatrix<T> A)
        {
            if (A is null) throw new ArgumentNullException(nameof(A));
            return A.AdditiveInverse(ProviderFactory.GetDefaultBLAS1<T>());
        }

        /// <summary>
        /// Returns the product of two band matrices.
        /// </summary>
        /// <param name="A">The left band matrix.</param>
        /// <param name="B">The right band matrix.</param>
        /// <returns>The matrix product.</returns>
        public static BandMatrix<T> operator *(BandMatrix<T> A, BandMatrix<T> B)
        {
            if (A is null) throw new ArgumentNullException(nameof(A));
            return A.Multiply(B, ProviderFactory.GetDefaultBLAS1<T>());
        }

        /// <summary>
        /// Returns the product of a band matrix and a dense vector.
        /// </summary>
        /// <param name="A">A band matrix.</param>
        /// <param name="x">A dense vector.</param>
        /// <returns>The matrix-vector product.</returns>
        public static DenseVector<T> operator *(BandMatrix<T> A, DenseVector<T> x)
        {
            if (A is null) throw new ArgumentNullException(nameof(A));
            return A.Multiply(x, ProviderFactory.GetDefaultBLAS1<T>());
        }

        /// <summary>
        /// Returns the product between a band matrix and a dense vector (represented by an array).
        /// </summary>
        /// <param name="A">A band matrix.</param>
        /// <param name="x">A dense vector.</param>
        /// <returns>The matrix-vector product.</returns>
        public static T[] operator *(BandMatrix<T> A, T[] x)
        {
            if (A is null) throw new ArgumentNullException(nameof(A));

            return A.Multiply(x, ProviderFactory.GetDefaultBLAS1<T>());
        }

        /// <summary>
        /// Returns the product of a band matrix and a scalar.
        /// </summary>
        /// <param name="A">The band matrix.</param>
        /// <param name="scalar">The scalar.</param>
        /// <returns>The matrix-scalar product.</returns>
        public static BandMatrix<T> operator *(BandMatrix<T> A, T scalar)
        {
            if (A is null) throw new ArgumentNullException(nameof(A));
            return A.Multiply(scalar, ProviderFactory.GetDefaultBLAS1<T>());
        }

        /// <summary>
        /// Returns the product of a band matrix and a scalar.
        /// </summary>
        /// <param name="scalar">The scalar.</param>
        /// <param name="A">The band matrix.</param>
        /// <returns>The matrix-scalar product.</returns>
        public static BandMatrix<T> operator *(T scalar, BandMatrix<T> A)
        {
            if (A is null) throw new ArgumentNullException(nameof(A));
            return A.Multiply(scalar, ProviderFactory.GetDefaultBLAS1<T>());
        }

        /// <summary>
        /// Returns a band matrix divided by a scalar.
        /// </summary>
        /// <param name="A">A band matrix.</param>
        /// <param name="scalar">The scalar to divide by.</param>
        /// <returns>The matrix division result.</returns>
        public static BandMatrix<T> operator /(BandMatrix<T> A, T scalar)
        {
            if (A is null) throw new ArgumentNullException(nameof(A));
            return A.Divide(scalar, ProviderFactory.GetDefaultBLAS1<T>());
        }

        /// <summary>
        /// Returns whether two band matrices are exactly equal. The definition of equality 
        /// is the same as that of the <txt>Equals(object)</txt> method.
        /// </summary>
        /// <param name="A">The left band matrix.</param>
        /// <param name="B">The right band matrix.</param>
        /// <returns>Whether two band matrices are equal.</returns>
        public static bool operator ==(BandMatrix<T> A, BandMatrix<T> B)
        {
            if (A is null || B is null)
            {
                return (A is null) == (B is null);
            }
            return A.Equals(B);
        }

        /// <summary>
        /// Returns whether two band matrices are not exactly equal, as per the definition of equality 
        /// given by the method <txt>Equals(object)</txt>.
        /// </summary>
        /// <param name="A">The left band matrix.</param>
        /// <param name="B">The right band matrix.</param>
        /// <returns>Whether the two band matrices are not exactly equal.</returns>
        public static bool operator !=(BandMatrix<T> A, BandMatrix<T> B) => !(A == B);

        #endregion
    }
}
