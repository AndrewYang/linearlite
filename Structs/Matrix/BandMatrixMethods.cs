﻿using LinearNet.Providers;
using System;

namespace LinearNet.Structs
{
    /// <summary>
    /// 
    /// </summary>
    public partial class BandMatrix<T> where T : new()
    {
        internal void BandReduce(int lowerBandwidth, int upperBandwidth, IDenseBLAS1<T> blas)
        {
            if (lowerBandwidth < 0 || upperBandwidth < 0 || lowerBandwidth + upperBandwidth < 1)
            {
                throw new ArgumentOutOfRangeException();
            }
            if (blas is null)
            {
                throw new ArgumentNullException(nameof(blas));
            }

        }
    }
}
