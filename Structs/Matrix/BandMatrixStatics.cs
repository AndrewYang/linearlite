﻿using LinearNet.Global;
using LinearNet.Helpers;
using System;

namespace LinearNet.Structs
{
    /// <cat>linear-algebra</cat>
    public static class BandMatrix
    {
        /// <summary>
        /// Creates an identity matrix of size <txt>dim</txt>.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dim">The row and column dimensionality of the identity matrix.</param>
        /// <returns>An identity matrix.</returns>
        public static BandMatrix<T> Identity<T>(int dim) where T : new()
        {
            if (dim <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(dim), ExceptionMessages.NonPositiveNotAllowed);
            }

            T one = DefaultGenerators.GetOne<T>();

            BandMatrix<T> matrix = new BandMatrix<T>(dim, dim, 0, 0);
            T[] diagonal = matrix.Diagonal;
            for (int i = 0; i < dim; ++i)
            {
                diagonal[i] = one;
            }
            return matrix;
        }
        /// <summary>
        /// Creates a square diagonal matrix of size <txt>dim</txt>, using the specified diagonal elements.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="diagonal">The diagonal terms.</param>
        /// <returns>A square diagonal matrix.</returns>
        public static BandMatrix<T> Diag<T>(T[] diagonal) where T : new()
        {
            int dim = diagonal.Length;

            BandMatrix<T> matrix = new BandMatrix<T>(dim, dim, 0, 0);
            T[] diag = matrix.Diagonal;
            for (int i = 0; i < dim; ++i)
            {
                diag[i] = diagonal[i];
            }
            return matrix;
        }

        /// <summary>
        /// <para>
        /// Create <txt>rows</txt> $\times$ <txt>columns</txt> band matrix of type <txt>T</txt>, with <txt>upperBandwidth</txt> upper bands
        /// and <txt>lowerBandwidth</txt> lower bands, whose entries are random values.
        /// </para>
        /// <!--inputs-->
        /// </summary>
        /// <name>Random_Band</name>
        /// <proto>BandMatrix<T> BandMatrix<T>.Random(int rows, int columns, int upperBandwidth, int lowerBandwidth, Func<T> random)</proto>
        /// <cat>la</cat>
        /// <typeparam name="T"></typeparam>
        /// <param name="rows">The number of rows in the created matrix.</param>
        /// <param name="columns">The number of columns in the created matrix.</param>
        /// <param name="upperBandwidth">The upper-bandwidth, i.e. the number of diagonals above the main diagonal.</param>
        /// <param name="lowerBandwidth">The lower-bandwidth, i.e. the number of diagonals below the main diagonal.</param>
        /// <param name="random">
        /// <b>Optional</b>, the random <txt>T</txt> generator.<br/>
        /// If unspecified, a default generator will be used. See <a href="#Random"><txt><b>Random</b></txt></a> for details on default random generators.
        /// </param>
        /// <returns></returns>
        public static BandMatrix<T> Random<T>(int rows, int columns, int upperBandwidth, int lowerBandwidth, Func<T> random = null) where T : new()
        {
            if (rows < 0 || columns < 0 || upperBandwidth < 0 || lowerBandwidth < 0)
            {
                throw new ArgumentOutOfRangeException();
            }

            if (upperBandwidth >= columns - 1)
            {
                throw new ArgumentOutOfRangeException();
            }

            if (lowerBandwidth >= rows - 1)
            {
                throw new ArgumentOutOfRangeException();
            }

            if (random == null)
            {
                random = DefaultGenerators.GetRandomGenerator<T>();
            }

            //Console.WriteLine("Dude what is going on");
            BandMatrix<T> matrix = new BandMatrix<T>(rows, columns, upperBandwidth, lowerBandwidth);
            for (int i = 0; i < upperBandwidth; ++i)
            {
                FillRandom(matrix.UpperBands[i], random);
            }
            FillRandom(matrix.Diagonal, random);
            for (int i = 0; i < lowerBandwidth; ++i)
            {
                FillRandom(matrix.LowerBands[i], random);
            }

            return matrix;
        }
        private static void FillRandom<T>(T[] band, Func<T> Random)
        {
            for (int i = 0; i < band.Length; ++i)
            {
                band[i] = Random();
            }
        }

        /// <summary>
        /// Creates a random diagonal matrix of the specified dimensions (i.e. both lower and upper bandwidths are 0).
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="rows">The number of rows of the matrix.</param>
        /// <param name="columns">The number of columns of the matrix.</param>
        /// <param name="Random">A random element generator</param>
        /// <returns>A band matrix of bandwidth 1.</returns>
        public static BandMatrix<T> RandomDiagonal<T>(int rows, int columns, Func<T> Random = null) where T : new()
        {
            if (rows < 0 || columns < 0)
            {
                throw new ArgumentOutOfRangeException();
            }

            BandMatrix<T> matrix = new BandMatrix<T>(rows, columns, 0, 0);
            FillRandom(matrix.Diagonal, Random);
            return matrix;
        }
        /// <summary>
        /// Creates a random bidiagonal matrix of the specified dimensions. 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="rows">The number of rows of the matrix.</param>
        /// <param name="columns">The number of columns of the matrix.</param>
        /// <param name="upper">If <txt>true</txt>, an upper bidiagonal matrix is returned. Otherwise a lower bidiagonal matrix is returned.</param>
        /// <param name="Random"></param>
        /// <returns>A band matrix of bandwidth 2.</returns>
        public static BandMatrix<T> RandomBidiagonal<T>(int rows, int columns, bool upper = true, Func<T> Random = null) where T : new()
        {
            if (rows < 0 || columns < 0)
            {
                throw new ArgumentOutOfRangeException();
            }

            if (upper)
            {
                BandMatrix<T> matrix = new BandMatrix<T>(rows, columns, 1, 0);
                FillRandom(matrix.Diagonal, Random);
                FillRandom(matrix.UpperBands[0], Random);
                return matrix;
            }
            else
            {
                BandMatrix<T> matrix = new BandMatrix<T>(rows, columns, 0, 1);
                FillRandom(matrix.Diagonal, Random);
                FillRandom(matrix.LowerBands[0], Random);
                return matrix;
            }
        }
        /// <summary>
        /// Creates a random tridiagonal matrix of the specified dimensions.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="rows">The number of rows of the matrix.</param>
        /// <param name="columns">The number of columns of the matrix.</param>
        /// <param name="Random">A random element generator.</param>
        /// <returns>A band matrix of bandwidth 3.</returns>
        public static BandMatrix<T> RandomTridiagonal<T>(int rows, int columns, Func<T> Random = null) where T : new()
        {
            if (rows < 0 || columns < 0)
            {
                throw new ArgumentOutOfRangeException();
            }

            BandMatrix<T> matrix = new BandMatrix<T>(rows, columns, 1, 1);
            FillRandom(matrix.UpperBands[0], Random);
            FillRandom(matrix.Diagonal, Random);
            FillRandom(matrix.LowerBands[0], Random);

            return matrix;
        }
    }
}
