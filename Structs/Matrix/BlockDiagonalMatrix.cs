﻿using LinearNet.Global;
using LinearNet.Matrices;
using LinearNet.Matrices.Cholesky;
using LinearNet.Providers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LinearNet.Structs
{
    /// <summary>
    /// A sparse matrix structure where the only non-zeroes reside in square submatrix blocks along its 
    /// main diagonal.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class BlockDiagonalMatrix<T> : Matrix<T> where T : new()
    {
        private readonly T _zero;

        /// <summary>
        /// Block i starts at (_blockIndex[i], _blockIndex[i])
        /// </summary>
        private int[] _blockIndices;
        private DenseMatrix<T>[] _blocks;

        internal int[] BlockIndices { get { return _blockIndices; } }
        internal DenseMatrix<T>[] Blocks { get { return _blocks; } }

        public override T[] this[int index] 
        { 
            get 
            {
                throw new NotImplementedException();
            } 
            set => throw new NotImplementedException(); 
        }
        public override T this[int rowIndex, int columnIndex] 
        {
            get 
            {
                int blockIdx = FindBlock(rowIndex, columnIndex);
                if (blockIdx >= 0)
                {
                    int start = _blockIndices[blockIdx];
                    return _blocks[blockIdx][rowIndex - start, columnIndex - start];
                }
                return _zero;
            }
            set 
            {
                int blockIdx = FindBlock(rowIndex, columnIndex);
                if (blockIdx >= 0)
                {
                    int start = _blockIndices[blockIdx];
                    _blocks[blockIdx][rowIndex - start, columnIndex - start] = value;
                }

                throw new ArgumentOutOfRangeException($"Cannot set to location ({rowIndex}, {columnIndex}) since it is not within a block.");
            }
        }

        /// <summary>
        /// Returns the bandwidth of this matrix
        /// </summary>
        public int Bandwidth
        {
            get
            {
                int maxSize = 0, count = _blocks.Length, i;
                for (i = 0; i < count; ++i)
                {
                    int size = _blocks[i].Rows;
                    if (maxSize < size)
                    {
                        maxSize = size;
                    }
                }
                return maxSize * 2 - 1;
            }
        }

        public override int SymbolicNonZeroCount
        {
            get
            {
                int count = 0, blocks = _blocks.Length, i;
                for (i = 0; i < blocks; ++i)
                {
                    DenseMatrix<T> block = _blocks[i];
                    count += block.Rows * block.Columns;
                }
                return count;
            }
        }

        public override IEnumerable<MatrixEntry<T>> NonZeroEntries
        {
            get
            {
                int blocks = _blocks.Length, i, r, c;
                for (i = 0; i < blocks; ++i)
                {
                    DenseMatrix<T> block = _blocks[i];
                    int rows = block.Rows, cols = block.Columns, offset = _blockIndices[i];
                    T[][] values = block.Values;
                    for (r = 0; r < rows; ++r)
                    {
                        T[] row = values[r];
                        int rowIndex = offset + r;
                        for (c = 0; c < cols; ++c)
                        {
                            yield return new MatrixEntry<T>(rowIndex, offset + c, row[c]);
                        }
                    }
                }
            }
        }


        #region Constructors 
        public BlockDiagonalMatrix(BandMatrix<T> matrix) : base(MatrixStorageType.BLOCK_DIAGONAL)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            Rows = matrix.Rows;
            Columns = matrix.Columns;
            _zero = new T();

            T[][] lowerBands = matrix.LowerBands,
                upperBands = matrix.UpperBands;
            T[] diagonal = matrix.Diagonal;
            int lowerBandwidth = matrix.LowerBandwidth,
                upperBandwidth = matrix.UpperBandwidth;

            if (Rows > Columns)
            {
                // check if matrix can be represented as a block-diagonal matrix
                int colStart = Math.Max(0, Columns - lowerBandwidth);
                for (int b = 0; b < lowerBandwidth; ++b)
                {
                    T[] band = lowerBands[b];
                    for (int c = colStart; c < Columns; ++c)
                    {
                        // non-zero element below square
                        if (c < band.Length && c + b + 1 >= Columns && !band[c].Equals(_zero)) 
                        {
                            throw new InvalidOperationException("Matrix cannot be represented in block diagonal form.");
                        }
                    }
                }
            }
            else if (Rows < Columns)
            {
                int rowStart = Math.Max(0, Rows - upperBandwidth);
                for (int b = 0; b < upperBandwidth; ++b)
                {
                    T[] band = upperBands[b];
                    for (int r = rowStart; r < Rows; ++r)
                    {
                        // non-zero element to right of square
                        if (r < band.Length && r + b + 1 >= Rows && !band[r].Equals(_zero))
                        {
                            throw new InvalidOperationException("Matrix cannot be represented in block diagonal form.");
                        }
                    }
                }
            }

            // Find the most conservative block diagonal representation based 
            // on the non zero pattern of the matrix, in a greedy fashion starting
            // from the top-left corner

            List<DenseMatrix<T>> blocks = new List<DenseMatrix<T>>();
            List<int> blockIndices = new List<int>();

            int dim = Math.Min(Rows, Columns);
            for (int i = 0; i < dim; )
            {
                int size = 1, maxSize = dim - i;
                for (; size < maxSize; ++size)
                {
                    // Check if size viable
                    if (IsValidBlock(matrix, i, i + size, dim))
                    {
                        break;
                    }
                }

                // Check for an empty block
                if (size == 1 && diagonal[i].Equals(_zero))
                {
                    ++i;
                    continue;
                }

                // Extract block of size 'size'
                DenseMatrix<T> block = new DenseMatrix<T>(size, size);
                T[][] values = block.Values;
                for (int r = 0; r < size; ++r)
                {
                    int r1 = r + i;
                    T[] row = values[r];
                    for (int c = 0; c < size; ++c)
                    {
                        row[c] = matrix[r1, c + i];
                    }
                }

                blockIndices.Add(i);
                blocks.Add(block);

                i += size;
            }
        }
        public BlockDiagonalMatrix(BlockDiagonalMatrix<T> matrix) : base(MatrixStorageType.BLOCK_DIAGONAL)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            Rows = matrix.Rows;
            Columns = matrix.Columns;
            _zero = matrix._zero;

            _blockIndices = matrix._blockIndices.Copy();
            _blocks = new DenseMatrix<T>[_blockIndices.Length];
            for (int i = 0; i < _blocks.Length; ++i)
            {
                _blocks[i] = matrix._blocks[i].Copy();
            }
        }
        public BlockDiagonalMatrix(COOSparseMatrix<T> matrix) : base(MatrixStorageType.BLOCK_DIAGONAL)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            Rows = matrix.Rows;
            Columns = matrix.Columns;
            _zero = new T();

            CreateFromValues(matrix.Values, Rows, Columns);
        }
        public BlockDiagonalMatrix(Matrix<T> matrix) : base(MatrixStorageType.BLOCK_DIAGONAL)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            Rows = matrix.Rows;
            Columns = matrix.Columns;
            _zero = new T();

            CreateFromValues(matrix.NonZeroEntries, Rows, Columns);
        }
        public BlockDiagonalMatrix(IEnumerable<DenseMatrix<T>> blocks) : base(MatrixStorageType.BLOCK_DIAGONAL)
        {
            if (blocks is null)
            {
                throw new ArgumentNullException(nameof(blocks));
            }

            _blocks = new DenseMatrix<T>[blocks.Count()];
            _blockIndices = new int[_blocks.Length];

            int k = 0, size = 0;
            foreach (DenseMatrix<T> block in blocks)
            {
                if (block is null)
                {
                    throw new ArgumentNullException(nameof(blocks));
                }
                if (!block.IsSquare)
                {
                    throw new ArgumentOutOfRangeException(nameof(blocks), ExceptionMessages.MatrixNotSquare);
                }
                _blocks[k] = block;
                _blockIndices[k] = size;
                size += block.Rows;
                ++k;
            }

            Rows = size;
            Columns = size;
            _zero = new T();
        }
        public BlockDiagonalMatrix(params DenseMatrix<T>[] blocks) : base(MatrixStorageType.BLOCK_DIAGONAL)
        {
            if (blocks is null)
            {
                throw new ArgumentNullException(nameof(blocks));
            }

            _blockIndices = new int[blocks.Length];
            int k = 0, size = 0;
            foreach (DenseMatrix<T> block in blocks)
            {
                if (block is null)
                {
                    throw new ArgumentNullException(nameof(blocks));
                }
                if (!block.IsSquare)
                {
                    throw new ArgumentOutOfRangeException(nameof(blocks), ExceptionMessages.MatrixNotSquare);
                }
                _blockIndices[k++] = size;
                size += block.Rows;
            }
            _blocks = blocks;

            Rows = size;
            Columns = size;
            _zero = new T();
        }

        /// <summary>
        /// Create a new block-diagonal matrix given the matrix dimensions, and two arrays of length
        /// equal to the number of blocks in the matrix.
        /// <para>
        /// The first array <txt>blockIndices</txt> holds the row and column index of the top-leftmost 
        /// entry of each block. 
        /// </para>
        /// <para>
        /// The second array <txt>blocks</txt> holds the blocks themselves, represented as dense matrices.
        /// </para>
        /// </summary>
        /// <param name="rows">The number of rows of this matrix.</param>
        /// <param name="columns">The number of columns of this matrix.</param>
        /// <param name="blockIndices">The row and column index of the top-left entry of each block.</param>
        /// <param name="blocks">The blocks.</param>
        public BlockDiagonalMatrix(int rows, int columns, int[] blockIndices, DenseMatrix<T>[] blocks) : base(MatrixStorageType.BLOCK_DIAGONAL)
        {
            if (blockIndices is null)
            {
                throw new ArgumentNullException(nameof(blockIndices));
            }
            if (blocks is null)
            {
                throw new ArgumentNullException(nameof(blocks));
            }
            if (rows <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(rows), ExceptionMessages.NonPositiveNotAllowed);
            }
            if (columns <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(columns), ExceptionMessages.NonPositiveNotAllowed);
            }

            if (blocks.Length != blockIndices.Length)
            {
                throw new ArgumentOutOfRangeException(
                    $"The length of '{nameof(blockIndices)}' array does not match the size of '{nameof(blocks)}' array.");
            }

            int dim = Math.Min(Rows, Columns);
            for (int b = 0; b < blockIndices.Length; ++b) 
            {
                if (!blocks[b].IsSquare)
                {
                    throw new ArgumentOutOfRangeException(nameof(blocks), "Blocks must be square.");
                }

                int i = blockIndices[b];
                if (i < 0 || i + blocks[b].Rows >= dim)
                {
                    throw new ArgumentOutOfRangeException(nameof(blockIndices) + 
                        " contains an index that lies outside the bounds of the matrix.");
                }
            }
            for (int b = 1; b < blockIndices.Length; ++b)
            {
                if (blocks[b - 1].Rows > blockIndices[b] - blockIndices[b - 1])
                {
                    throw new ArgumentOutOfRangeException("Overlapping blocks are not allowed.");
                }
            }

            Rows = rows;
            Columns = columns;
            _zero = new T();
            _blockIndices = blockIndices;
            _blocks = blocks;
        }

        /// <summary>
        /// Returns whether [i, j) is a valid block
        /// </summary>
        private bool IsValidBlock(BandMatrix<T> matrix, int i, int j, int dim)
        {
            int lowerBandwidth = matrix.LowerBandwidth,
                upperBandwidth = matrix.UpperBandwidth;

            // Check to the right of the block
            for (int r = i; r < j; ++r)
            {
                int col_end = Math.Min(dim, r + upperBandwidth);
                for (int c = j; c < col_end; ++c)
                {
                    if (!matrix[r, c].Equals(_zero))
                    {
                        return false;
                    }
                }
            }

            // Check under the block
            for (int c = i; c < j; ++c)
            {
                int row_end = Math.Min(dim, c + lowerBandwidth);
                for (int r = j; r < row_end; ++r)
                {
                    if (!matrix[r, c].Equals(_zero))
                    {
                        return false;
                    }
                }
            }
            return true;
        }
        private void CreateFromValues(IEnumerable<MatrixEntry<T>> values, int rows, int columns)
        {
            int dim = Math.Min(rows, columns);

            // Initialize each diagonal term to their own group,
            // characterised as the lowest index within that group
            int[] first = new int[dim];
            for (int i = 0; i < dim; ++i)
            {
                first[i] = i;
            }
            
            foreach (MatrixEntry<T> e in values)
            {
                if (!e.Value.Equals(_zero))
                {
                    int min, max;
                    if (e.RowIndex < e.ColumnIndex)
                    {
                        min = e.RowIndex;
                        max = e.ColumnIndex;
                    }
                    else
                    {
                        min = e.ColumnIndex;
                        max = e.RowIndex;
                    }

                    // A non-zero entry at (min, max) or (max, min) will mean that 
                    // all entries in [min, ..., max] will now belong
                    // to the same group (same firsts). 
                    // first is monotically increasing (its a loop invariant)
                    int group = first[min];
                    for (int i = max; i >= min; --i)
                    {
                        if (first[i] > group)
                        {
                            first[i] = group;
                        }
                        else
                        {
                            // since first is monotonically increasing, all remaining 
                            // terms will have first[i] <= group
                            break;
                        }
                    }
                }
            }

            int nBlocks = 0,
                prevFirst = first[0];
            for (int i = 0; i < dim; ++i)
            {
                if (first[i] != prevFirst)
                {
                    prevFirst = first[i];
                    ++nBlocks;
                }
            }
            ++nBlocks; // last block

            DenseMatrix<T>[] blocks = new DenseMatrix<T>[nBlocks];
            int[] blockIndices = new int[nBlocks];
            prevFirst = first[0];
            int b = 0;
            for (int i = 0; i < dim; ++i)
            {
                if (first[i] != prevFirst)
                {
                    int size = i - prevFirst;
                    blocks[b] = new DenseMatrix<T>(size, size);
                    blockIndices[b] = prevFirst;
                    prevFirst = first[i];
                    ++b;
                }

                // Update the first index to point to the block index
                first[i] = b;
            }
            // Add the last block
            blocks[b] = new DenseMatrix<T>(dim - prevFirst, dim - prevFirst);
            blockIndices[b] = prevFirst;

            // Fill in numeric values
            foreach (MatrixEntry<T> e in values)
            {
                int min = Math.Min(e.RowIndex, e.ColumnIndex);
                int blockIndex = first[min], 
                    offset = blockIndices[blockIndex];
                blocks[blockIndex][e.RowIndex - offset][e.ColumnIndex - offset] = e.Value;
            }

            // Remove zeroes 
            int k = 0;
            for (int i = 0; i < nBlocks; ++i)
            {
                if (blocks[i].Rows == 1 && blocks[i][0][0].Equals(_zero))
                {
                    // Empty
                    continue;
                }

                if (k != i)
                {
                    blocks[k] = blocks[i];
                    blockIndices[k] = blockIndices[i];
                }
                ++k;
            }

            // Resize arrays
            if (k < nBlocks)
            {
                Array.Resize(ref blocks, k);
                Array.Resize(ref blockIndices, k);
            }

            _blocks = blocks;
            _blockIndices = blockIndices;
        }

        #endregion

        /// <summary>
        /// Finds the index of the block in which the point (rowIndex, columnIndex) resides in.
        /// </summary>
        private int FindBlock(int rowIndex, int columnIndex)
        {
            if (rowIndex < 0 || rowIndex >= Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(rowIndex));
            }
            if (columnIndex < 0 || columnIndex >= Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(columnIndex));
            }

            // Find the row
            for (int i = 1; i < _blockIndices.Length; ++i)
            {
                if (_blockIndices[i] > rowIndex)
                {
                    // Check if contained in block i - 1
                    if (_blockIndices[i] > columnIndex && columnIndex >= _blockIndices[i - 1])
                    {
                        return i - 1;
                    }

                    // Not contained in block i - 1 (so its not contained in any block)
                    return -1;
                }
            }

            // Check the last block
            if (columnIndex >= _blockIndices[_blockIndices.Length - 1])
            {
                return _blockIndices.Length - 1;
            }

            // Not contained in any block
            return -1;
        }

        /// <summary>
        /// Find the block containing a diagonal term [index, index]
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        private int FindBlock(int index)
        {
            if (index < 0 || index >= Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(index));
            }

            int count = _blockIndices.Length, i;
            for (i = 0; i < count; ++i)
            {
                if (_blockIndices[i] <= index && index < _blockIndices[i] + _blocks[i].Rows)
                {
                    return i;
                }
            }

            return -1;
        }

        private void Clear(T[] array, int start, int end)
        {
            if (_zero.Equals(default))
            {
                Array.Clear(array, start, end - start);
            }
            else
            {
                for (int i = start; i < end; ++i)
                {
                    array[i] = _zero;
                }
            }
        }

        public override object Clone()
        {
            return Copy();
        }
        public BlockDiagonalMatrix<T> Copy()
        {
            return new BlockDiagonalMatrix<T>(this);
        }


        #region Boolean methods

        public override bool Equals(object obj)
        {
            throw new NotImplementedException();
        }

        public bool Equals(BlockDiagonalMatrix<T> matrix, Func<T, T, bool> equals)
        {
            throw new NotImplementedException();
            if (matrix is null)
            {
                return false;
            }
            if (equals is null)
            {
                throw new ArgumentNullException(nameof(equals));
            }

            if (Rows != matrix.Rows || Columns != matrix.Columns)
            {
                return false;
            }

            if (_blockIndices.Length == 0)
            {
                if (matrix._blockIndices.Length > 0)
                {
                    return matrix.IsZero(x => equals(x, _zero));
                }
                return true;
            }
            else if (matrix._blockIndices.Length == 0)
            {
                return IsZero(x => equals(x, _zero));
            }

            int alen = _blockIndices.Length, 
                blen = matrix._blockIndices.Length, 
                a = 0, 
                b = 0;

            while (a < alen || b < blen)
            {
                int aIndex = a < alen ? _blockIndices[a] : int.MaxValue;
                int bIndex = b < blen ? matrix._blockIndices[b] : int.MaxValue;

                if (aIndex < bIndex)
                {
                    int aEnd = Math.Min(bIndex, aIndex + _blocks[a].Rows);

                }
            }

        }

        public override bool IsLower(Predicate<T> isZero)
        {
            foreach (DenseMatrix<T> matrix in _blocks)
            {
                if (!matrix.IsLower(isZero))
                {
                    return false;
                }
            }
            return true;
        }

        public override bool IsUpper(Predicate<T> isZero)
        {
            foreach (DenseMatrix<T> matrix in _blocks)
            {
                if (!matrix.IsUpper(isZero))
                {
                    return false;
                }
            }
            return true;
        }

        public override bool IsDiagonal(Predicate<T> isZero)
        {
            if (isZero is null)
            {
                throw new ArgumentNullException(nameof(isZero));
            }

            foreach (DenseMatrix<T> block in _blocks)
            {
                if (!block.IsDiagonal(isZero))
                {
                    return false;
                }
            }
            return true;
        }

        public override bool IsBanded(int lowerBandwidth, int upperBandwidth, Predicate<T> isZero)
        {
            if (isZero is null)
            {
                throw new ArgumentNullException(nameof(isZero));
            }

            foreach (DenseMatrix<T> block in _blocks)
            {
                if (!block.IsBanded(lowerBandwidth, upperBandwidth, isZero))
                {
                    return false;
                }
            }
            return true;
        }

        public override bool IsZero(Predicate<T> isZero)
        {
            if (isZero is null)
            {
                throw new ArgumentNullException(nameof(isZero));
            }

            foreach (DenseMatrix<T> block in _blocks)
            {
                if (!block.IsZero(isZero))
                {
                    return false;
                }
            }
            return true;
        }

        public override bool IsIdentity(Predicate<T> isZero, Predicate<T> isOne)
        {
            if (isZero is null)
            {
                throw new ArgumentNullException(nameof(isZero));
            }
            if (isOne is null)
            {
                throw new ArgumentNullException(nameof(isOne));
            }

            foreach (DenseMatrix<T> block in _blocks)
            {
                if (!block.IsIdentity(isZero, isOne))
                {
                    return false;
                }
            }

            // Check the gap between the first block and the start of the matrix
            if (_blockIndices[0] != 0)
            {
                return false;
            }
            // Check for gaps in between blocks
            int len = _blocks.Length;
            for (int i = 1; i < len; ++i)
            {
                if (_blocks[i - 1].Rows != _blockIndices[i] - _blockIndices[i - 1])
                {
                    return false;
                }
            }
            // Check the gap between the last block and the end of this matrix
            if (_blockIndices[len - 1] + _blocks[len - 1].Rows != Rows)
            {
                return false;
            }
            return true;
        }

        #endregion


        /// <summary>
        /// Extend this matrix by appending a dense matrix to its lower-right corner.
        /// </summary>
        /// <param name="matrix">The dense matrix to append.</param>
        public void Append(DenseMatrix<T> matrix)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            if (!matrix.IsSquare)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            int len = _blockIndices.Length, last = len - 1;
            _blockIndices = _blockIndices.InsertAt(len, _blockIndices[last] + _blocks[last].Rows);
            _blocks.InsertAt(len, matrix);

            Rows += matrix.Rows;
            Columns += matrix.Columns;
        }
        
        /// <summary>
        /// Extend this matrix by appending a block-diagonal matrix to its lower-right corner.
        /// </summary>
        /// <param name="matrix">The block-diagonal matrix to append.</param>
        public void Append(BlockDiagonalMatrix<T> matrix)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            int len1 = _blockIndices.Length, 
                len2 = matrix._blockIndices.Length,
                len = len1 + len2;

            int[] blockIndices = new int[len];
            DenseMatrix<T>[] blocks = new DenseMatrix<T>[len];

            Array.Copy(_blockIndices, blockIndices, len1);
            Array.Copy(_blocks, blocks, len1);
            Array.Copy(matrix._blocks, 0, blocks, len1, len2);

            for (int i = 0; i < len2; ++i)
            {
                blockIndices[len1 + i] = matrix._blockIndices[i] + Rows;
            }

            _blockIndices = blockIndices;
            _blocks = blocks;

            Rows += matrix.Rows;
            Columns += matrix.Columns;
        }

        /// <summary>
        /// Returns an array of the block sizes in order.
        /// </summary>
        /// <returns>The diagonal block sizes.</returns>
        public int[] BlockSizes()
        {
            int[] sizes = new int[_blockIndices.Length];
            for (int b = 0; b < _blockIndices.Length; ++b)
            {
                sizes[b] = _blocks[b].Rows;
            }
            return sizes;
        }

        public override DenseVector<T> RowSums(IProvider<T> provider)
        {
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }

            T[] sums = RectangularVector.Zeroes<T>(Rows);
            for (int b = 0; b < _blocks.Length; ++b)
            {
                T[][] block = _blocks[b].Values;
                int offset = _blockIndices[b],
                    rows = block.Length, 
                    cols = block[0].Length;
                for (int r = 0; r < rows; ++r)
                {
                    T[] row = block[r];
                    T sum = provider.Zero;
                    for (int c = 0; c < cols; ++c)
                    {
                        sum = provider.Add(sum, row[c]);
                    }
                    sums[r + offset] = sum;
                }
            }
            return new DenseVector<T>(sums);
        }

        public override DenseVector<T> ColumnSums(IProvider<T> provider)
        {
            throw new NotImplementedException();
        }

        public override void SwapRows(int rowIndex1, int rowIndex2)
        {
            throw new NotImplementedException();
        }

        public override void SwapColumns(int columnIndex1, int columnIndex2)
        {
            throw new NotImplementedException();
        }

        public override DenseVector<T> Row(int rowIndex)
        {
            if (rowIndex < 0 || rowIndex >= Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(rowIndex));
            }

            // Find the block index
            int b = 0;
            while (b < _blocks.Length && _blockIndices[b] < rowIndex) ++b;

            int start = _blockIndices[b],
                offset = rowIndex - start;
            DenseMatrix<T> block = _blocks[b - 1];

            T[] row = RectangularVector.Zeroes<T>(Columns);
            Array.Copy(block[offset], 0, row, start, block.Columns);

            return new DenseVector<T>(row);
        }

        public override DenseVector<T> Column(int columnIndex)
        {
            if (columnIndex < 0 || columnIndex > Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(columnIndex));
            }

            // Find the block index
            int b = 0;
            while (b < _blocks.Length && _blockIndices[b] < columnIndex) ++b;

            int start = _blockIndices[b],
                offset = columnIndex - start;
            DenseMatrix<T> block = _blocks[b - 1];

            T[] column = RectangularVector.Zeroes<T>(Rows);
            for (int r = 0; r < block.Rows; ++r)
            {
                column[r + start] = block.Values[offset][r];
            }

            return new DenseVector<T>(column);
        }

        public override SparseVector<T> SparseRow(int rowIndex)
        {
            if (rowIndex < 0 || rowIndex > Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(rowIndex));
            }

            int index = FindBlock(rowIndex);
            if (index < 0)
            {
                return new SparseVector<T>(Columns);
            }

            DenseMatrix<T> block = _blocks[index];
            int offset = _blockIndices[index], cols = block.Columns;

            int[] indices = new int[cols];
            T[] values = new T[cols], row = block[rowIndex - offset];
            for (int c = 0; c < cols; ++c)
            {
                indices[c] = offset + c;
                values[c] = row[c];
            }

            return new SparseVector<T>(Columns, indices, values, true);
        }

        public override SparseVector<T> SparseColumn(int columnIndex)
        {
            if (columnIndex < 0 || columnIndex >= Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(columnIndex));
            }

            int index = FindBlock(columnIndex);
            if (index < 0)
            {
                return new SparseVector<T>(Rows);
            }

            DenseMatrix<T> block = _blocks[index];
            int offset = _blockIndices[index], rows = block.Rows;

            int[] indices = new int[rows];
            T[] values = new T[rows];

            for (int r = 0, c = columnIndex - offset; r < rows; ++r)
            {
                indices[r] = offset + r;
                values[r] = block.Values[r][c];
            }
            return new SparseVector<T>(Rows, indices, values, true);
        }

        public override Matrix<T> Select(int[] rowIndices, int[] columnIndices)
        {
            if (rowIndices is null)
            {
                throw new ArgumentNullException(nameof(rowIndices));
            }
            if (columnIndices is null)
            {
                throw new ArgumentNullException(nameof(columnIndices));
            }

            ThrowHelper.AssertCollectionInRange(rowIndices, 0, Rows, nameof(rowIndices));
            ThrowHelper.AssertCollectionInRange(columnIndices, 0, Columns, nameof(columnIndices));

            int rlen = rowIndices.Length, clen = columnIndices.Length;

            // Returns a COOSparseMatrix in sort order
            List<MatrixEntry<T>> values = new List<MatrixEntry<T>>();
            for (int ri = 0; ri < rlen; ++ri)
            {
                int r = rowIndices[ri];
                int index = FindBlock(r);
                if (index < 0) continue;
                DenseMatrix<T> block = _blocks[index];
                int start = _blockIndices[index], 
                    end = start + block.Rows;
                T[] row = block.Values[r - start];

                for (int ci = 0; ci < clen; ++ci)
                {
                    int c = columnIndices[ci];
                    if (start <= c && c < end)
                    {
                        values.Add(new MatrixEntry<T>(ri, ci, row[c - start]));
                    }
                }
            }
            return new COOSparseMatrix<T>(rlen, clen, values, true);
        }

        public override Vector<T> SelectAsVector(int rowIndex, int[] columnIndices)
        {
            if (rowIndex < 0 || rowIndex >= Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(rowIndex));
            }
            if (columnIndices is null)
            {
                throw new ArgumentNullException(nameof(columnIndices));
            }
            ThrowHelper.AssertCollectionInRange(columnIndices, 0, Columns, nameof(columnIndices));

            int index = FindBlock(rowIndex);
            if (index < 0)
            {
                return new SparseVector<T>(columnIndices.Length);
            }

            DenseMatrix<T> block = _blocks[index];
            int start = _blockIndices[index], 
                end = start + block.Rows, 
                clen = columnIndices.Length;

            int nz = 0;
            for (int ci = 0; ci < clen; ++ci)
            {
                int c = columnIndices[ci];
                if (start <= c && c < end)
                {
                    ++nz;
                }
            }

            int[] indices = new int[nz];
            T[] values = new T[nz];

            T[] row = block.Values[rowIndex - start];
            int k = 0;
            for (int ci = 0; ci < clen; ++ci)
            {
                int c = columnIndices[ci];
                if (start <= c && c < end)
                {
                    indices[k] = ci;
                    values[k++] = row[c - start];
                }
            }

            return new SparseVector<T>(clen, indices, values, true);
        }

        public override Vector<T> SelectAsVector(int[] rowIndices, int columnIndex)
        {
            if (rowIndices is null)
            {
                throw new ArgumentNullException(nameof(rowIndices));
            }
            ThrowHelper.AssertCollectionInRange(rowIndices, 0, Rows, nameof(rowIndices));

            if (columnIndex < 0 || columnIndex >= Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(columnIndex));
            }

            int rlen = rowIndices.Length,
                index = FindBlock(columnIndex);

            if (index < 0)
            {
                return new SparseVector<T>(rlen);
            }

            DenseMatrix<T> block = _blocks[index];
            int start = _blockIndices[index],
                end = start + block.Rows, 
                nz = 0;

            for (int ri = 0; ri < rlen; ++ri)
            {
                int r = rowIndices[ri];
                if (start <= r && r < end)
                {
                    ++nz;
                }
            }

            int[] indices = new int[nz];
            T[] values = new T[nz];

            int k = 0, cIndex = columnIndex - start;
            for (int ri = 0; ri < rlen; ++ri)
            {
                int r = rowIndices[ri];
                if (start <= r && r < end)
                {
                    indices[k] = ri;
                    values[k++] = block[r - start][cIndex];
                }
            }

            return new SparseVector<T>(rlen, indices, values, true);
        }

        public override void Clear()
        {
            // We could just remove the blocks, however we will maintain the workspace
            foreach (DenseMatrix<T> block in _blocks)
            {
                block.Clear();
            }
        }

        public override void Clear(Predicate<T> select)
        {
            if (select is null)
            {
                throw new ArgumentNullException(nameof(select));
            }

            foreach (DenseMatrix<T> block in _blocks)
            {
                block.Clear(select);
            }
        }

        public override void ClearRow(int rowIndex)
        {
            // Find block
            int index = FindBlock(rowIndex);
            if (index < 0) return;

            _blocks[index].ClearRow(rowIndex - _blockIndices[index]);
        }

        public override void ClearColumn(int columnIndex)
        {
            int index = FindBlock(columnIndex);
            if (index < 0) return;

            _blocks[index].ClearColumn(columnIndex - _blockIndices[index]);
        }

        public override void ClearSubmatrix(int rowIndex, int columnIndex, int rows, int columns)
        {
            int count = _blocks.Length, i;
            for (i = 0; i < count; ++i)
            {
                DenseMatrix<T> block = _blocks[i];
                int start = _blockIndices[i], end = (i < count - 1) ? _blockIndices[i + 1] : Columns,
                    bottom = rowIndex + rows, right = columnIndex + columns;

                if (rowIndex < end && columnIndex < end)
                {
                    int offset_r = rowIndex - start,
                        offset_c = columnIndex - start;
                    block.ClearSubmatrix(offset_r, offset_c, Math.Min(rows, block.Rows - offset_r), Math.Min(columns, block.Columns - offset_c));
                }

                if (bottom > start && right > start)
                {
                    block.ClearSubmatrix(0, 0, Math.Min(rows - (bottom - start), block.Rows), Math.Min(columns - (right - start), block.Columns));
                }
            }
        }

        public override void ClearLower()
        {
            int count = _blocks.Length, i;
            for (i = 0; i < count; ++i)
            {
                _blocks[i].ClearLower();
            }
        }

        public override void ClearUpper()
        {
            int count = _blocks.Length, i;
            for (i = 0; i < count; ++i)
            {
                _blocks[i].ClearUpper();
            }
        }

        public override int Count(Predicate<T> predicate)
        {
            if (predicate is null) throw new ArgumentNullException(nameof(predicate));

            int count;
            if (predicate(_zero))
            {
                count = Rows * Columns;
                foreach (DenseMatrix<T> block in _blocks)
                {
                    count -= (block.Rows * block.Columns);
                }
            }
            else
            {
                count = 0;
            }

            foreach (DenseMatrix<T> block in _blocks)
            {
                count += block.Count(predicate);
            }
            return count;
        }

        public override T Trace(IProvider<T> provider)
        {
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }

            T tr = provider.Zero;
            for (int b = 0; b < _blocks.Length; ++b)
            {
                tr = provider.Add(tr, _blocks[b].Trace(provider));
            }
            return tr;
        }

        public override T[] LeadingDiagonal()
        {
            T[] diag = RectangularVector.Zeroes<T>(Math.Min(Rows, Columns));
            for (int b = 0; b < _blockIndices.Length; ++b)
            {
                int offset = _blockIndices[b];
                DenseMatrix<T> block = _blocks[b];
                T[][] values = block.Values;
                int rows = block.Rows, r;
                for (r = 0; r < rows; ++r)
                {
                    diag[r + offset] = values[r][r];
                }
            }
            return diag;
        }

        internal override bool SolveTriangular(T[] b, T[] x, IDenseBLAS1<T> blas, bool lower, bool transpose)
        {
            // ensure first that all x in between blocks are set to zero
            if (_blocks.Length > 0)
            {
                if (_blockIndices[0] > 0)
                {
                    Clear(x, 0, _blockIndices[0]);
                }
                int last = _blocks.Length;
                for (int i = 1; i <= last; ++i)
                {
                    int start = _blockIndices[i - 1] + _blocks[i - 1].Rows, end = _blockIndices[i];
                    if (end > start)
                    {
                        Clear(x, start, end);
                    }
                }
                if (_blockIndices[last] + _blocks[last].Rows < Rows)
                {
                    Clear(x, _blockIndices[last] + _blocks[last].Rows, Rows);
                }
            }
            else
            {
                // If no blocks, then return zero
                Clear(x, 0, Rows);
                return true;
            }

            for (int i = 0; i < _blocks.Length; ++i)
            {
                int offset = _blockIndices[i];
                DenseMatrix<T> block = _blocks[i];
                
                if (!block.SolveTriangular(b, x, blas, lower, transpose, offset, offset, block.Rows))
                {
                    return false;
                }
            }
            return true;
        }

        public Cholesky<T> Cholesky(IDenseCholeskyAlgorithm<T> algorithm, bool overwrite = false)
        {
            if (algorithm is null)
            {
                throw new ArgumentNullException(nameof(algorithm));
            }
            if (!IsSquare)
            {
                throw new InvalidOperationException(ExceptionMessages.MatrixNotSquare);
            }

            // Cholesky requires strictly positive diagonal - no gaps
            if (_blockIndices[0] > 0)
            {
                throw new InvalidOperationException("Matrix is not positive definite.");
            }

            int last = _blocks.Length - 1;
            for (int b = 0; b <= last; ++b)
            {
                if (_blockIndices[b + 1] - _blockIndices[b] > _blocks[b - 1].Rows)
                {
                    throw new InvalidOperationException("Matrix is not positive definite.");
                }
            }
            if (_blockIndices[last] + _blocks[last].Rows < Rows)
            {
                throw new InvalidOperationException("Matrix is not positive definite.");
            }

            BlockDiagonalMatrix<T> A = overwrite ? this : Copy();
            IDenseBLAS1<T> blas1 = null;

            for (int b = 0; b < A._blocks.Length; ++b)
            {
                DenseMatrix<T> block = A._blocks[b];
                Cholesky<T> chol = block.Cholesky(algorithm, true);
                if (blas1 is null)
                {
                    blas1 = chol.BLAS1;
                }
            }
            return new Cholesky<T>(A, blas1);
        }


        #region Operations

        private static void AssertBlockIndicesMatch(int[] blockIndices1, int[] blockIndices2)
        {
            // Check block dimensions
            if (blockIndices1.Length != blockIndices2.Length)
            {
                throw new InvalidOperationException("Unequal number of blocks in the two matrices.");
            }

            for (int i = 0; i < blockIndices1.Length; ++i)
            {
                if (blockIndices1[i] != blockIndices2[i])
                {
                    throw new InvalidOperationException("Block indices do not match.");
                }
            }
        }
        private static void AssertBlockSizesMatch(DenseMatrix<T>[] blocks1, DenseMatrix<T>[] blocks2)
        {
            if (blocks1.Length != blocks2.Length)
            {
                throw new InvalidOperationException("Unequal number of blocks.");
            }

            for (int i = 0; i < blocks1.Length; ++i)
            {
                if (blocks1[i].Rows != blocks2[i].Rows)
                {
                    throw new InvalidOperationException("Block sizes do not match.");
                }
            }
        }

        public override Matrix<T> Add(Matrix<T> matrix, IProvider<T> provider)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            if (CompareRepresentationPriority(this, matrix) < 0)
            {
                return matrix.Add(this, provider);
            }

            // Special case for block diagonal matrix but not of the same block size?

            return Add(matrix.ToBlockDiagonalMatrix(), provider);
        }

        /// <summary>
        /// Calculates the sum of this matrix with another matrix, returning the sum as a <txt>BlockDiagonalMatrix&lt;T&gt;</txt>.
        /// <para>
        /// This method only supports adding pairs of matrices with the identical corresponding block sizes, 
        /// as otherwise the resulting sum will not be a block diagonal matrix.
        /// </para>
        /// </summary>
        /// <param name="matrix">The matrix to add.</param>
        /// <param name="blas1">The level-1 BLAS provider.</param>
        /// <returns>The matrix sum.</returns>
        public BlockDiagonalMatrix<T> Add(BlockDiagonalMatrix<T> matrix, IDenseBLAS1Provider<T> blas1)
        {
            BlockDiagonalMatrix<T> copy = Copy();
            copy.AddInPlace(matrix, blas1);
            return copy;
        }

        /// <summary>
        /// Add another matrix to this matrix, and overwrite this matrix with the sum, using the specified 
        /// level-1 BLAS implementation.
        /// </summary>
        /// <param name="matrix">The matrix to add.</param>
        /// <param name="blas1">The level-1 BLAS implementation to use.</param>
        public void AddInPlace(BlockDiagonalMatrix<T> matrix, IDenseBLAS1Provider<T> blas1)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            if (blas1 is null)
            {
                throw new ArgumentNullException(nameof(blas1));
            }
            MatrixChecks.AssertMatrixDimensionsEqual(this, matrix);

            AssertBlockIndicesMatch(_blockIndices, matrix._blockIndices);
            AssertBlockSizesMatch(_blocks, matrix._blocks);

            int len = _blockIndices.Length, i;
            for (i = 0; i < len; ++i)
            {
                _blocks[i].AddInPlace(matrix._blocks[i], blas1);
            }
        }

        /// <summary>
        /// Add another matrix to this matrix, and overwrite this matrix with their sum.
        /// </summary>
        /// <param name="matrix">The matrix to add.</param>
        /// <param name="blas2">The level-2 BLAS implementation to use.</param>
        public void AddInPlace(BlockDiagonalMatrix<T> matrix, IDenseBLAS2Provider<T> blas2)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            if (blas2 is null)
            {
                throw new ArgumentNullException(nameof(blas2));
            }

            MatrixChecks.AssertMatrixDimensionsEqual(this, matrix);

            AssertBlockIndicesMatch(_blockIndices, matrix._blockIndices);
            AssertBlockSizesMatch(_blocks, matrix._blocks);

            int len = _blockIndices.Length, i;
            for (i = 0; i < len; ++i)
            {
                _blocks[i].AddInPlace(matrix._blocks[i], blas2);
            }
        }

        /// <summary>
        /// Calculates this matrix subtract another matrix, returning the difference as a <txt>BlockDiagonalMatrix&lt;T&gt;</txt>.
        /// </summary>
        /// <param name="matrix">The matrix to subtract.</param>
        /// <param name="blas1">The level-1 BLAS provider to use.</param>
        /// <returns>The matrix difference.</returns>
        public BlockDiagonalMatrix<T> Subtract(BlockDiagonalMatrix<T> matrix, IDenseBLAS1Provider<T> blas1)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            if (blas1 is null)
            {
                throw new ArgumentNullException(nameof(blas1));
            }
            AssertBlockIndicesMatch(_blockIndices, matrix._blockIndices);
            AssertBlockSizesMatch(_blocks, matrix._blocks);

            int len = _blockIndices.Length, i;
            DenseMatrix<T>[] blocks = new DenseMatrix<T>[len];
            for (i = 0; i < len; ++i)
            {
                blocks[i] = _blocks[i].Subtract(matrix._blocks[i], blas1);
            }
            return new BlockDiagonalMatrix<T>(Rows, Columns, _blockIndices.Copy(), blocks);
        }

        /// <summary>
        /// Subtract another matrix from this matrix, and overwrite this matrix with the result, using the specified 
        /// level-1 BLAS implementation.
        /// </summary>
        /// <param name="matrix">The matrix to subtract.</param>
        /// <param name="blas1">The level-1 BLAS implementation to use.</param>
        public void SubtractInPlace(BlockDiagonalMatrix<T> matrix, IDenseBLAS1Provider<T> blas1)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            if (blas1 is null)
            {
                throw new ArgumentNullException(nameof(blas1));
            }
            MatrixChecks.AssertMatrixDimensionsEqual(this, matrix);

            AssertBlockIndicesMatch(_blockIndices, matrix._blockIndices);
            AssertBlockSizesMatch(_blocks, matrix._blocks);

            int len = _blockIndices.Length, i;
            for (i = 0; i < len; ++i)
            {
                _blocks[i].SubtractInPlace(matrix._blocks[i], blas1);
            }
        }

        /// <summary>
        /// Subtract another matrix to this matrix, and overwrite this matrix with their sum.
        /// </summary>
        /// <param name="matrix">The matrix to subtract.</param>
        /// <param name="blas2">The level-2 BLAS implementation to use.</param>
        public void SubtractInPlace(BlockDiagonalMatrix<T> matrix, IDenseBLAS2Provider<T> blas2)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            if (blas2 is null)
            {
                throw new ArgumentNullException(nameof(blas2));
            }

            MatrixChecks.AssertMatrixDimensionsEqual(this, matrix);

            AssertBlockIndicesMatch(_blockIndices, matrix._blockIndices);
            AssertBlockSizesMatch(_blocks, matrix._blocks);

            int len = _blockIndices.Length, i;
            for (i = 0; i < len; ++i)
            {
                _blocks[i].SubtractInPlace(matrix._blocks[i], blas2);
            }
        }

        /// <summary>
        /// Returns the negation of this matrix $-A$ (i.e. its additive inverse).
        /// </summary>
        /// <param name="blas2">The level-2 BLAS implementation to use.</param>
        /// <returns>The matrix negation.</returns>
        public BlockDiagonalMatrix<T> Negate(IDenseBLAS2Provider<T> blas2)
        {
            BlockDiagonalMatrix<T> copy = Copy();
            copy.NegateInPlace(blas2);
            return copy;
        }

        /// <summary>
        /// Replace this matrix $A$ with its negation, $-A$.
        /// </summary>
        /// <param name="blas2">The level-2 BLAS implementation to use.</param>
        public void NegateInPlace(IDenseBLAS2Provider<T> blas2)
        {
            if (blas2 is null)
            {
                throw new ArgumentNullException(nameof(blas2));
            }

            int len = _blocks.Length, i;
            for (i = 0; i < len; ++i)
            {
                _blocks[i].NegateInPlace(blas2);
            }
        }

        /// <summary>
        /// Multiply two block-diagonal matrices together, returning a block-diagonal matrix.
        /// This operation is only valid if the corresponding block sizes of the two matrices are the same (otherwise
        /// the matrix product will not be a block-diagonal matrix).
        /// </summary>
        /// <param name="matrix"></param>
        /// <param name="algo"></param>
        /// <returns></returns>
        public BlockDiagonalMatrix<T> Multiply(BlockDiagonalMatrix<T> matrix, IMatrixMultiplication<T> algo)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            if (algo is null)
            {
                throw new ArgumentNullException(nameof(algo));
            }
            MatrixChecks.AssertMatrixDimensionsForMultiplication(this, matrix);

            AssertBlockIndicesMatch(_blockIndices, matrix._blockIndices);
            AssertBlockSizesMatch(_blocks, matrix._blocks);

            // Multiply blocks
            DenseMatrix<T>[] prod_blocks = new DenseMatrix<T>[_blockIndices.Length];
            for (int i = 0; i < _blockIndices.Length; ++i)
            {
                prod_blocks[i] = _blocks[i].Multiply(matrix._blocks[i], algo);
            }
            return new BlockDiagonalMatrix<T>(Rows, Columns, _blockIndices.Copy(), prod_blocks);
        }

        public override void MultiplyInPlace(T scalar, IProvider<T> provider)
        {
            if (scalar is null)
            {
                throw new ArgumentNullException(nameof(scalar));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            for (int i = 0; i < _blocks.Length; ++i)
            {
                _blocks[i].MultiplyInPlace(scalar, provider);
            }
        }
        public void MultiplyInPlace(T scalar, IDenseBLAS1Provider<T> blas1)
        {
            if (scalar is null)
            {
                throw new ArgumentNullException(nameof(scalar));
            }
            if (blas1 is null)
            {
                throw new ArgumentNullException(nameof(blas1));
            }
            for (int i = 0; i < _blocks.Length; ++i)
            {
                _blocks[i].MultiplyInPlace(scalar, blas1);
            }
        }
        public BlockDiagonalMatrix<T> Multiply(T scalar, IProvider<T> provider)
        {
            BlockDiagonalMatrix<T> copy = Copy();
            copy.MultiplyInPlace(scalar, provider);
            return copy;
        }
        public BlockDiagonalMatrix<T> Multiply(T scalar, IDenseBLAS1Provider<T> blas1)
        {
            BlockDiagonalMatrix<T> copy = Copy();
            copy.MultiplyInPlace(scalar, blas1);
            return copy;
        }

        public BlockDiagonalMatrix<T> Divide(T scalar, IProvider<T> provider)
        {
            BlockDiagonalMatrix<T> copy = Copy();
            copy.DivideInPlace(scalar, provider);
            return copy;
        }

        internal override void Multiply(T[] vector, T[] product, IProvider<T> provider, bool increment)
        {
            if (vector is null) throw new ArgumentNullException(nameof(vector));
            if (product is null) throw new ArgumentNullException(nameof(product));
            if (vector.Length < Columns) throw new ArgumentOutOfRangeException(nameof(vector));
            if (product.Length < Rows) throw new ArgumentOutOfRangeException(nameof(product));
            if (provider is null) throw new ArgumentNullException(nameof(provider));

            // Calculate size of required workspace
            int size = 0, nBlocks = _blocks.Length, b;
            for (b = 0; b < nBlocks; ++b)
            {
                DenseMatrix<T> block = _blocks[b];
                if (block.Rows > size)
                {
                    size = block.Rows;
                }
            }

            T[] vect_workspace = new T[size];
            T[] prod_workspace = new T[size];
            for (b = 0; b < nBlocks; ++b)
            {
                DenseMatrix<T> block = _blocks[b];
                int offset = _blockIndices[b];
                int dim = block.Rows;
                Array.Copy(vector, offset, vect_workspace, 0, dim);

                if (increment)
                {
                    Array.Copy(product, offset, prod_workspace, 0, dim);
                }
                block.Multiply(vect_workspace, prod_workspace, provider, increment);

                // Copy result back into product vector
                Array.Copy(prod_workspace, 0, product, offset, dim);
            }
        }

        internal override void TransposeAndMultiply(T[] vector, T[] product, IProvider<T> provider, bool increment)
        {
            if (vector is null) throw new ArgumentNullException(nameof(vector));
            if (product is null) throw new ArgumentNullException(nameof(product));
            if (vector.Length < Rows) throw new ArgumentOutOfRangeException(nameof(vector));
            if (product.Length < Columns) throw new ArgumentOutOfRangeException(nameof(product));
            if (provider is null) throw new ArgumentNullException(nameof(provider));

            int size = 0, nBlocks = _blocks.Length, b;
            for (b = 0; b < nBlocks; ++b)
            {
                int dim = _blocks[b].Rows;
                if (size < dim)
                {
                    size = dim;
                }
            }

            T[] vect_workspace = new T[size], prod_workspace = new T[size];
            for (b = 0; b < nBlocks; ++b)
            {
                DenseMatrix<T> block = _blocks[b];
                int offset = _blockIndices[b], dim = block.Rows;

                Array.Copy(vector, offset, vect_workspace, 0, dim);

                if (increment)
                {
                    Array.Copy(product, offset, prod_workspace, 0, dim);
                }
                block.TransposeAndMultiply(vect_workspace, prod_workspace, provider, increment);

                // Copy result back into product vector
                Array.Copy(prod_workspace, 0, product, offset, dim);
            }
        }

        /// <summary>
        /// Count the number of non-zeroes in the product between this matrix and a sparse vector.
        /// </summary>
        private int CountNNZ(SparseVector<T> vector)
        {
            int[] indices = vector.Indices;

            // co-iterate to find the nnz in the product vector
            int bi = 0,
                blen = _blocks.Length,
                vi = 0,
                vlen = indices.Length,
                nnz = 0;

            while (bi < blen || vi < vlen)
            {
                int blockStart = bi < blen ? _blockIndices[bi] : int.MaxValue;
                int blockEnd = bi < blen ? blockStart + _blocks[bi].Rows : int.MaxValue;
                int vectIndex = vi < vlen ? indices[vi] : int.MaxValue;

                if (blockStart <= vectIndex)
                {
                    // Block is hit - add size of block to nnz
                    if (vectIndex < blockEnd)
                    {
                        nnz += (blockEnd - blockStart);
                        // Prevent counting this block again by incrementing bi
                        ++bi;
                    }
                    else
                    {
                        // vectIndex lies beyond the range of block - 
                        // increment block
                        ++bi;
                    }
                }
                else
                {
                    ++vi;
                }
            }
            return nnz;
        }

        public override SparseVector<T> Multiply(SparseVector<T> vector, IProvider<T> provider)
        {
            if (vector is null)
            {
                throw new ArgumentNullException(nameof(vector));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }

            if (vector.Dimension < Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(vector));
            }

            int[] indices = vector.Indices;
            T[] values = vector.Values;

            // co-iterate to find the nnz in the product vector
            int blen = _blocks.Length, 
                vlen = indices.Length,
                nnz = CountNNZ(vector);

            int[] prod_indices = new int[nnz];
            T[] prod_values = new T[nnz];
            int k = 0;

            for (int bi = 0, vi = 0; bi < blen; ++bi)
            {
                DenseMatrix<T> block = _blocks[bi];
                int blockStart = _blockIndices[bi],
                    blockEnd = blockStart + block.Rows;

                T[][] block_values = block.Values;

                // Skip over 'dead' indices
                while (vi < vlen && indices[vi] < blockStart) ++vi;

                bool any = false;
                while (vi < vlen && indices[vi] < blockEnd)
                {
                    int column = indices[vi] - blockStart;
                    T v = values[vi];
                    for (int r = 0; r < block.Rows; ++r)
                    {
                        int index = k + r;
                        prod_values[index] = provider.Add(prod_values[index], provider.Multiply(v, block_values[r][column]));
                    }
                    ++vi;
                    any = true;
                }

                // insert the indicies
                if (any)
                {
                    for (int r = 0; r < block.Rows; ++r)
                    {
                        prod_indices[k++] = blockStart + r;
                    }
                }
            }

            return new SparseVector<T>(Rows, prod_indices, prod_values, true);
        }

        public override SparseVector<T> TransposeAndMultiply(SparseVector<T> vector, IProvider<T> provider)
        {
            if (vector is null)
            {
                throw new ArgumentNullException(nameof(vector));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }

            if (vector.Dimension < Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(vector));
            }

            int[] indices = vector.Indices;
            T[] values = vector.Values;

            // co-iterate to find the nnz in the product vector
            int blen = _blocks.Length,
                vlen = indices.Length,
                nnz = CountNNZ(vector);

            int[] prod_indices = new int[nnz];
            T[] prod_values = new T[nnz];
            int k = 0;

            for (int bi = 0, vi = 0; bi < blen; ++bi)
            {
                DenseMatrix<T> block = _blocks[bi];
                int blockStart = _blockIndices[bi],
                    blockEnd = blockStart + block.Rows;

                T[][] block_values = block.Values;

                // Skip over 'dead' indices
                while (vi < vlen && indices[vi] < blockStart) ++vi;

                bool any = false;
                while (vi < vlen && indices[vi] < blockEnd)
                {
                    T[] column = block_values[indices[vi] - blockStart];
                    T v = values[vi];
                    for (int r = 0; r < block.Rows; ++r)
                    {
                        int index = k + r;
                        prod_values[index] = provider.Add(prod_values[index], provider.Multiply(v, column[r]));
                    }
                    ++vi;
                    any = true;
                }

                // insert the indicies
                if (any)
                {
                    for (int r = 0; r < block.Rows; ++r)
                    {
                        prod_indices[k++] = blockStart + r;
                    }
                }
            }

            return new SparseVector<T>(Rows, prod_indices, prod_values, true);
        }

        public SparseVector<T> Multiply(SparseVector<T> vector, IDenseBLAS1Provider<T> blas1)
        {
            if (vector is null) throw new ArgumentNullException(nameof(vector));
            if (blas1 is null) throw new ArgumentNullException(nameof(blas1));
            if (!(blas1 is IDenseBLAS1<T> b)) throw new NotSupportedException(ExceptionMessages.TypeNotSupported);
            if (vector.Dimension < Columns) throw new ArgumentOutOfRangeException(nameof(vector));

            int[] indices = vector.Indices;
            T[] values = vector.Values;

            // co-iterate to find the nnz in the product vector
            int blen = _blocks.Length,
                vlen = indices.Length,
                nnz = CountNNZ(vector);

            // Create workspace
            int size = 0;
            for (int bi = 0; bi < blen; ++bi)
            {
                int dim = _blocks[bi].Rows;
                if (size < dim)
                {
                    size = dim;
                }
            }
            T[] workspace = new T[size];

            int[] prod_indices = new int[nnz];
            T[] prod_values = new T[nnz];
            int k = 0;

            for (int bi = 0, vi = 0; bi < blen; ++bi)
            {
                DenseMatrix<T> block = _blocks[bi];
                int blockStart = _blockIndices[bi],
                    blockEnd = blockStart + block.Rows;

                T[][] block_values = block.Values;

                // Skip over 'dead' indices
                while (vi < vlen && indices[vi] < blockStart) ++vi;

                bool any = false;
                while (vi < vlen && indices[vi] < blockEnd)
                {
                    int column = indices[vi] - blockStart;
                    int dim = blockEnd - blockStart;
                    for (int r = 0; r < dim; ++r)
                    {
                        workspace[r] = block_values[r][column];
                    }

                    b.AXPY(prod_values, workspace, values[vi], k, k + dim, -k);

                    ++vi;
                    any = true;
                }

                // insert the indicies
                if (any)
                {
                    for (int r = 0; r < block.Rows; ++r)
                    {
                        prod_indices[k++] = blockStart + r;
                    }
                }
            }

            return new SparseVector<T>(Rows, prod_indices, prod_values, true);


        }

        public SparseVector<T> TransposeAndMultiply(SparseVector<T> vector, IDenseBLAS1Provider<T> blas1)
        {
            if (vector is null) throw new ArgumentNullException(nameof(vector));
            if (blas1 is null) throw new ArgumentNullException(nameof(blas1));
            if (!(blas1 is IDenseBLAS1<T> b)) throw new NotSupportedException(ExceptionMessages.TypeNotSupported);
            if (vector.Dimension < Rows) throw new ArgumentOutOfRangeException(nameof(vector));

            int[] indices = vector.Indices;
            T[] values = vector.Values;

            // co-iterate to find the nnz in the product vector
            int blen = _blocks.Length,
                vlen = indices.Length,
                nnz = CountNNZ(vector);

            int[] prod_indices = new int[nnz];
            T[] prod_values = new T[nnz];
            int k = 0;

            for (int bi = 0, vi = 0; bi < blen; ++bi)
            {
                DenseMatrix<T> block = _blocks[bi];
                int blockStart = _blockIndices[bi],
                    blockEnd = blockStart + block.Rows;

                T[][] block_values = block.Values;

                // Skip over 'dead' indices
                while (vi < vlen && indices[vi] < blockStart) ++vi;

                bool any = false;
                while (vi < vlen && indices[vi] < blockEnd)
                {
                    b.AXPY(prod_values, block_values[indices[vi] - blockStart], values[vi], k, k + block.Rows, -k);
                    ++vi;
                    any = true;
                }

                // insert the indicies
                if (any)
                {
                    for (int r = 0; r < block.Rows; ++r)
                    {
                        prod_indices[k++] = blockStart + r;
                    }
                }
            }

            return new SparseVector<T>(Rows, prod_indices, prod_values, true);
        }

        #endregion


        #region Convertors 

        public override BandMatrix<T> ToBandMatrix()
        {
            return new BandMatrix<T>(this);
        }

        public override BlockDiagonalMatrix<T> ToBlockDiagonalMatrix()
        {
            return this;
        }

        public override COOSparseMatrix<T> ToCOOSparseMatrix()
        {
            return new COOSparseMatrix<T>(this);
        }

        public override CSCSparseMatrix<T> ToCSCSparseMatrix()
        {
            return new CSCSparseMatrix<T>(this);
        }

        public override CSRSparseMatrix<T> ToCSRSparseMatrix()
        {
            return new CSRSparseMatrix<T>(this);
        }

        public override DenseMatrix<T> ToDenseMatrix()
        {
            return new DenseMatrix<T>(this);
        }

        public override DiagonalMatrix<T> ToDiagonalMatrix()
        {
            return new DiagonalMatrix<T>(this);
        }

        public override DOKSparseMatrix<T> ToDOKSparseMatrix()
        {
            return new DOKSparseMatrix<T>(this);
        }

        public override MCSRSparseMatrix<T> ToMCSRSparseMatrix()
        {
            return new MCSRSparseMatrix<T>(this);
        }

        public override SKYSparseMatrix<T> ToSKYSparseMatrix()
        {
            return new SKYSparseMatrix<T>(this);
        }

        #endregion


        #region Operators

        public static BlockDiagonalMatrix<T> operator +(BlockDiagonalMatrix<T> A, BlockDiagonalMatrix<T> B)
        {
            if (A is null) throw new ArgumentNullException(nameof(A));
            return A.Add(B, ProviderFactory.GetDefaultBLAS1<T>());
        }
        public static BlockDiagonalMatrix<T> operator -(BlockDiagonalMatrix<T> A, BlockDiagonalMatrix<T> B)
        {
            if (A is null) throw new ArgumentNullException(nameof(A));
            return A.Subtract(B, ProviderFactory.GetDefaultBLAS1<T>());
        }
        public static BlockDiagonalMatrix<T> operator -(BlockDiagonalMatrix<T> A)
        {
            if (A is null)
            {
                throw new ArgumentNullException(nameof(A));
            }
            return A.Negate(ProviderFactory.GetDefaultBLAS2<T>());
        }
        public static BlockDiagonalMatrix<T> operator *(BlockDiagonalMatrix<T> A, T scalar)
        {
            if (A is null)
            {
                throw new ArgumentNullException(nameof(A));
            }
            return A.Multiply(scalar, ProviderFactory.GetDefaultBLAS1<T>());
        }
        public static BlockDiagonalMatrix<T> operator *(T scalar, BlockDiagonalMatrix<T> A) => A * scalar;
        public static DenseVector<T> operator *(BlockDiagonalMatrix<T> A, DenseVector<T> v)
        {
            if (A is null)
            {
                throw new ArgumentNullException(nameof(A));
            }
            return A.Multiply(v, ProviderFactory.GetDefaultProvider<T>());
        }
        public static SparseVector<T> operator *(BlockDiagonalMatrix<T> A, SparseVector<T> v)
        {
            if (A is null)
            {
                throw new ArgumentNullException(nameof(A));
            }
            return A.Multiply(v, ProviderFactory.GetDefaultProvider<T>());
        }
        public static BlockDiagonalMatrix<T> operator /(BlockDiagonalMatrix<T> A, T scalar)
        {
            if (A is null)
            {
                throw new ArgumentNullException(nameof(A));
            }
            return A.Divide(scalar, ProviderFactory.GetDefaultProvider<T>());
        }

        #endregion
    }
}
