﻿using LinearNet.Global;
using System;

namespace LinearNet.Structs
{
    public static class BlockDiagonalMatrix
    {
        public static BlockDiagonalMatrix<T> Random<T>(int blocks, int minBlockSize, int maxBlockSize, Func<T> random = null) where T : new()
        {
            if (blocks <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(blocks), ExceptionMessages.NonPositiveNotAllowed);
            }
            if (minBlockSize <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(minBlockSize), ExceptionMessages.NonPositiveNotAllowed);
            }
            if (maxBlockSize <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(maxBlockSize), ExceptionMessages.NonPositiveNotAllowed);
            }
            if (minBlockSize > maxBlockSize)
            {
                throw new ArgumentOutOfRangeException(nameof(minBlockSize), "The minimum block size is larger than the maximum block size.");
            }

            Random rand = new Random();
            DenseMatrix<T>[] bl = new DenseMatrix<T>[blocks];
            for (int b = 0; b < blocks; ++b)
            {
                int size = rand.Next(maxBlockSize - minBlockSize + 1) + minBlockSize;
                bl[b] = DenseMatrix.Random(size, size, random);
            }
            return new BlockDiagonalMatrix<T>(bl);
        }
    }
}
