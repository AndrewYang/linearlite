﻿using LinearNet.Global;
using LinearNet.Providers;
using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;

namespace LinearNet.Structs
{
    /// <summary>
    /// <p>Implementation of a sparse matrix in Coordinate List form (COO).</p>
    /// <p>Matrix elements are stored in (row index, column index, value) triplets. </p>
    /// <p>
    /// As with the DOK (dictionary of keys) sparse matrix, this matrix type is fast 
    /// for incremental construction, but suboptimal for matrix operations such as multiplication
    /// and decompositions. For best results, create a matrix in COO form, then convert it 
    /// into a different format (such as CSR) for computation. 
    /// </p>
    /// <p>
    /// The maximum size of this matrix is <txt>int.MaxValue</txt> by <txt>int.MaxValue</txt> ($2^{31} - 1$ by $2^{31} - 1$).
    /// </p>
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <cat>linear-algebra</cat>
    public class COOSparseMatrix<T> : Matrix<T> where T : new()
    {
        /// <summary>
        /// Only true if the matrix is guaranteed to be sorted. It is possible for the 
        /// matrix to be sorted and for this field to be false.
        /// </summary>
        private bool _isSorted;
        private readonly T _zero;
        private List<MatrixEntry<T>> _values;

        /// <summary>
        /// This is just lazy - should keep values private
        /// </summary>
        internal List<MatrixEntry<T>> Values { get { return _values; } }
        internal bool IsSorted { get { return _isSorted; } }

        #region Properties

        public override T this[int rowIndex, int columnIndex] 
        {
            get
            {
                if (rowIndex < 0 || rowIndex >= Rows)
                {
                    throw new ArgumentOutOfRangeException(nameof(rowIndex));
                }

                if (columnIndex < 0 || columnIndex >= Columns)
                {
                    throw new ArgumentOutOfRangeException(nameof(columnIndex));
                }

                if (_isSorted)
                {
                    foreach (MatrixEntry<T> element in _values)
                    {
                        if (element.RowIndex < rowIndex)
                        {
                            continue;
                        }
                        if (element.RowIndex == rowIndex && element.ColumnIndex == columnIndex)
                        {
                            return element.Value;
                        }
                        if (element.RowIndex > rowIndex)
                        {
                            return _zero;
                        }
                    }
                    return _zero;
                }

                // If not sorted, need to loop through the entire list
                foreach (MatrixEntry<T> element in _values)
                {
                    if (element.RowIndex == rowIndex && element.ColumnIndex == columnIndex)
                    {
                        return element.Value;
                    }
                }
                return _zero;
            }
            set
            {
                MatrixEntry<T> insert = new MatrixEntry<T>(rowIndex, columnIndex, value);
                if (_isSorted)
                {
                    for (int i = 0; i < _values.Count; ++i)
                    {
                        MatrixEntry<T> element = _values[i];

                        int comp = element.CompareTo(insert);
                        if (comp < 0)
                        {
                            continue;
                        }
                        else if (comp == 0)
                        {
                            Debug.WriteLine("comp = 0 with: " + insert + ", " + _values[i]);
                            _values[i] = insert;
                            return;
                        }
                        else
                        {
                            // Insert prior to the first element that > the inserted element.
                            _values.Insert(i, insert);
                            Debug.WriteLine("insert with: " + insert + ", " + _values[i]);
                            return;
                        }
                    }

                    // Insert at the end of the list
                    _values.Add(insert);
                    return;
                }

                // If not sorted... need to loop through the entire list 
                for (int i = 0; i < _values.Count; ++i)
                {
                    MatrixEntry<T> element = _values[i];
                    if (element.CompareTo(insert) == 0)
                    {
                        _values[i] = insert;
                        return;
                    }
                }

                // Not already contained - add to end of the list
                _values.Add(insert);
                return;
            }
        }
        public override T[] this[int index] 
        { 
            get 
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Returns the number of non-zero elements (nnz).
        /// </summary>
        public override int SymbolicNonZeroCount { get { return _values.Count; } }

        public override IEnumerable<MatrixEntry<T>> NonZeroEntries => _values;

        /// <summary>
        /// Returns the density of this matrix. A density of 1 means all entries are non-zero, while a 
        /// density of 0 means all entries are zero (i.e. the matrix is a zero matrix).
        /// </summary>
        public double Density { get { return _values.Count / (double)(Rows * Columns); } }

        #endregion


        #region Constructor

        /// <summary>
        /// Create an empty sparse matrix (all elements assumed to be zero).
        /// </summary>
        /// <param name="rows">The number of rows of the matrix.</param>
        /// <param name="columns">The number of columns of the matrix.</param>
        public COOSparseMatrix(int rows, int columns) : base(MatrixStorageType.COO, rows, columns)
        {
            _isSorted = true;
            _zero = new T();
            _values = new List<MatrixEntry<T>>();
        }

        /// <summary>
        /// Create a COO sparse matrix from a band matrix.
        /// </summary>
        /// <param name="matrix">The band matrix.</param>
        public COOSparseMatrix(BandMatrix<T> matrix) : base(MatrixStorageType.COO)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            Rows = matrix.Rows;
            Columns = matrix.Columns;
            _values = new List<MatrixEntry<T>>();
            _zero = new T();
            _isSorted = false;

            T[][] upperBands = matrix.UpperBands;
            for (int b = 0; b < matrix.UpperBandwidth; ++b)
            {
                int offset = b + 1;
                T[] band = upperBands[b];
                for (int i = 0; i < band.Length; ++i)
                {
                    if (!band[i].Equals(_zero))
                    {
                        _values.Add(new MatrixEntry<T>(i, i + offset, band[i]));
                    }
                }
            }

            T[][] lowerBands = matrix.LowerBands;
            for (int b = 0; b < matrix.LowerBandwidth; ++b)
            {
                int offset = b + 1;
                T[] band = lowerBands[b];
                for (int i = 0; i < band.Length; ++i)
                {
                    if (!band[i].Equals(_zero))
                    {
                        _values.Add(new MatrixEntry<T>(i + offset, i, band[i]));
                    }
                }
            }

            T[] diagonal = matrix.Diagonal;
            for (int i = 0; i < diagonal.Length; ++i)
            {
                if (!diagonal[i].Equals(_zero))
                {
                    _values.Add(new MatrixEntry<T>(i, i, diagonal[i]));
                }
            }
        }

        public COOSparseMatrix(BlockDiagonalMatrix<T> matrix) : base(MatrixStorageType.COO)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            Rows = matrix.Rows;
            Columns = matrix.Columns;
            _zero = new T();

            int count = 0;
            foreach (DenseMatrix<T> block in matrix.Blocks)
            {
                count += block.Rows * block.Columns;
            }
            _values = new List<MatrixEntry<T>>(count);

            int nBlocks = matrix.Blocks.Length;
            for (int i = 0; i < nBlocks; ++i)
            {
                DenseMatrix<T> block = matrix.Blocks[i];
                int offset = matrix.BlockIndices[i], dim = block.Rows;
                T[][] values = block.Values;
                for (int r = 0; r < dim; ++r)
                {
                    T[] row = values[r];
                    int rowIndex = r + offset;
                    for (int c = 0; c < dim; ++c)
                    {
                        _values.Add(new MatrixEntry<T>(rowIndex, c + offset, row[c]));
                    }
                }
            }
            _isSorted = true;
        }

        /// <summary>
        /// Copy constructor - creates a deep copy of another COO sparse matrix.
        /// </summary>
        /// <param name="matrix">A COO sparse matrix.</param>
        public COOSparseMatrix(COOSparseMatrix<T> matrix) : base(MatrixStorageType.COO)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            _isSorted = matrix._isSorted;
            Rows = matrix.Rows;
            Columns = matrix.Columns;

            _zero = matrix._zero;
            _values = new List<MatrixEntry<T>>(matrix._values);
        }

        public COOSparseMatrix(CSCSparseMatrix<T> matrix) : base(MatrixStorageType.COO)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            Rows = matrix.Rows;
            Columns = matrix.Columns;
            _zero = new T();

            int[] columnIndices = matrix.ColumnIndices, rowIndices = matrix.RowIndices;
            T[] values = matrix.Values;

            _values = new List<MatrixEntry<T>>(values.Length);
            _isSorted = false;

            for (int c = 0; c < Columns; ++c)
            {
                int end = columnIndices[c + 1];
                for (int i = columnIndices[c]; i < end; ++i)
                {
                    _values.Add(new MatrixEntry<T>(rowIndices[i], c, values[i]));
                }
            }
        }

        /// <summary>
        /// Create a COO sparse matrix from a CSR sparse matrix.
        /// </summary>
        /// <param name="matrix">A sparse matrix in CSR format.</param>
        public COOSparseMatrix(CSRSparseMatrix<T> matrix) : base(MatrixStorageType.COO)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            _isSorted = true;
            _zero = new T();
            _values = new List<MatrixEntry<T>>(matrix.Values.Length);
            Rows = matrix.Rows;
            Columns = matrix.Columns;

            T[] values = matrix.Values;
            int[] rowIndices = matrix.RowIndices,
                columnIndices = matrix.ColumnIndices;

            for (int r = 0; r < Rows; ++r)
            {
                int row_end = rowIndices[r + 1];
                for (int i = rowIndices[r]; i < row_end; ++i)
                {
                    _values.Add(new MatrixEntry<T>(r, columnIndices[i], values[i]));
                }
            }
        }

        /// <summary>
        /// Create a COO sparse matrix from a diagonal matrix.
        /// </summary>
        /// <param name="matrix">The diagonal matrix.</param>
        public COOSparseMatrix(DiagonalMatrix<T> matrix) : base(MatrixStorageType.COO)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            Rows = matrix.Rows;
            Columns = matrix.Columns;
            _values = new List<MatrixEntry<T>>();
            _zero = new T();
            _isSorted = true;

            T[] diagonal = matrix.DiagonalTerms;

            for (int i = 0; i < diagonal.Length; ++i)
            {
                if (!diagonal[i].Equals(_zero))
                {
                    _values.Add(new MatrixEntry<T>(i, i, diagonal[i]));
                }
            }
        }

        /// <summary>
        /// Create a COO sparse matrix from a dense matrix.
        /// </summary>
        /// <param name="matrix">The dense matrix.</param>
        public COOSparseMatrix(DenseMatrix<T> matrix) : base(MatrixStorageType.COO)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            Rows = matrix.Rows;
            Columns = matrix.Columns;
            _values = new List<MatrixEntry<T>>();
            _zero = new T();
            _isSorted = true;

            for (int r = 0; r < Rows; ++r)
            {
                T[] row = matrix.Values[r];
                for (int c = 0; c < Columns; ++c)
                {
                    if (!row[c].Equals(_zero))
                    {
                        _values.Add(new MatrixEntry<T>(r, c, row[c]));
                    }
                }
            }
        }

        /// <summary>
        /// Create a COO sparse matrix using a DOK (dictionary of keys) sparse matrix.
        /// </summary>
        /// <param name="matrix">The DOK sparse matrix.</param>
        public COOSparseMatrix(DOKSparseMatrix<T> matrix) : base(MatrixStorageType.COO)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            _isSorted = false;
            Rows = matrix.Rows;
            Columns = matrix.Columns;

            _zero = new T();
            _values = new List<MatrixEntry<T>>();

            foreach (KeyValuePair<long, T> entry in matrix.Values)
            {
                long row = entry.Key / Columns, col = entry.Key % Columns;
                _values.Add(new MatrixEntry<T>((int)row, (int)col, entry.Value));
            }
        }

        public COOSparseMatrix(Matrix<T> matrix) : base(MatrixStorageType.COO)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            Rows = matrix.Rows;
            Columns = matrix.Columns;
            _zero = new T();
            _values = new List<MatrixEntry<T>>(matrix.NonZeroEntries);
            _isSorted = false;
        }

        /// <summary>
        /// Create a COO sparse matrix from a dense 2-D array. Elements equal in value to 
        /// <txt>new T()</txt> will be treated as equal to zero.
        /// </summary>
        /// <param name="matrix">The dense 2-D array.</param>
        public COOSparseMatrix(T[,] matrix) : base(MatrixStorageType.COO)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            Rows = matrix.GetLength(0);
            Columns = matrix.GetLength(1);

            _values = new List<MatrixEntry<T>>();
            _isSorted = true;

            for (int i = 0; i < Rows; ++i)
            {
                for (int j = 0; j < Columns; ++j)
                {
                    T e = matrix[i, j];
                    if (!e.Equals(_zero))
                    {
                        _values.Add(new MatrixEntry<T>(i, j, e));
                    }
                }
            }
        }

        /// <summary>
        /// Create a COO sparse matrix from a jagged array of arrays. The number of rows of this 
        /// matrix will be set to the number of 1D arrays in the jagged array. The number of columns 
        /// will be set to the maximum length of the 1D arrays contained in the jagged array. Any 
        /// element equal to <txt>new T()</txt> will be treated as zero and ignored.
        /// </summary>
        /// <param name="matrix">The jagged array of arrays.</param>
        public COOSparseMatrix(T[][] matrix) : base(MatrixStorageType.COO)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            Rows = matrix.Length;
            _values = new List<MatrixEntry<T>>();
            _isSorted = true;

            int column = -1;
            for (int i = 0; i < Rows; ++i)
            {
                T[] row = matrix[i];
                if (row is null)
                {
                    throw new ArgumentNullException(nameof(matrix), "Matrix contains a pointer to a null array.");
                }

                if (row.Length > column)
                {
                    column = row.Length;
                }

                for (int j = 0; j < row.Length; ++j)
                {
                    T e = row[j];
                    if (!e.Equals(_zero))
                    {
                        _values.Add(new MatrixEntry<T>(i, j, e));
                    }
                }
            }
            Columns = column;
        }

        /// <summary>
        /// Create a COO sparse matrix using the values of a vector. If <txt>rowMajor</txt> is <txt>true</txt>,
        /// the values of this matrix will be filled one row at a time, left to right. Otherwise, this matrix
        /// will be filled one column at a time, form top to bottom within each column.
        /// </summary>
        /// <param name="vector">The vector containing the elements of this matrix.</param>
        /// <param name="columns">The number of columns in this matrix.</param>
        /// <param name="rowMajor">Specifies whether this matrix should be filled in row-major fashion.</param>
        public COOSparseMatrix(BigSparseVector<T> vector, int columns, bool rowMajor = true) : base(MatrixStorageType.COO)
        {
            if (vector is null)
            {
                throw new ArgumentNullException(nameof(vector));
            }
            if (columns < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(columns), ExceptionMessages.NonPositiveNotAllowed);
            }
            if (vector.Dimension % columns != 0)
            {
                throw new ArgumentOutOfRangeException("The dimension of the vector is not divisible by the number of columns of this matrix.");
            }

            _zero = new T();
            _values = new List<MatrixEntry<T>>(vector.Values.Count);
            Rows = vector.Dimension / columns;
            Columns = columns;

            if (rowMajor)
            {
                foreach (KeyValuePair<long, T> pair in vector.Values)
                {
                    _values.Add(new MatrixEntry<T>((int)(pair.Key / columns), (int)(pair.Key % columns), pair.Value));
                }
            }
            else
            {
                foreach (KeyValuePair<long, T> pair in vector.Values)
                {
                    _values.Add(new MatrixEntry<T>((int)(pair.Key % Rows), (int)(pair.Key / Rows), pair.Value));
                }
            }
        }

        internal COOSparseMatrix(int rows, int columns, List<MatrixEntry<T>> values, bool sorted) : base(MatrixStorageType.COO, rows, columns)
        {
            _isSorted = sorted;
            _zero = new T();
            _values = values;
        }

        #endregion

        #region Private methods
        /// <summary>
        /// Find the index of element e within list in O(log n) time where n is size of the list.
        /// </summary>
        private int FindWithinOrderedList(MatrixEntry<T> e, List<MatrixEntry<T>> list, int startIndex, int endIndex)
        {
            if (startIndex == endIndex)
            {
                if (list.Count > startIndex && e.CompareTo(list[startIndex]) == 0)
                {
                    return startIndex;
                }

                // list does not contain element.
                return -1;
            }

            int mid = (startIndex + endIndex) / 2;

            // Look in [startIndex, mid)
            if (e.CompareTo(list[mid]) < 0)
            {
                return FindWithinOrderedList(e, list, startIndex, mid);
            }

            // Look in [mid, endIndex)
            return FindWithinOrderedList(e, list, mid, endIndex);
        }

        /// <summary>
        /// If the element already exists, replace it, otherwise insert at the appropriate location.
        /// Returns the index in which it was either inserted or replaced at.
        /// </summary>
        private int UpsertIntoOrderedList(MatrixEntry<T> e, List<MatrixEntry<T>> list, int startIndex, int endIndex)
        {
            if (startIndex == endIndex)
            {
                // List might have size 0 - just add to end of list
                if (list.Count <= startIndex)
                {
                    list.Add(e);
                    return list.Count - 1;
                }

                int cmp = e.CompareTo(list[startIndex]);
                if (cmp == 0)
                {
                    list[startIndex] = e;
                    return startIndex;
                }

                // Insert prior
                else if (cmp < 0)
                {
                    list.Insert(startIndex, e);
                    return startIndex;
                }

                // Insert after
                else
                {
                    list.Insert(startIndex + 1, e);
                    return startIndex + 1;
                }
            }

            int mid = (startIndex + endIndex) / 2;

            // Look in [startIndex, mid)
            if (e.CompareTo(list[mid]) < 0)
            {
                return UpsertIntoOrderedList(e, list, startIndex, mid);
            }

            // Look in [mid, endIndex)
            return UpsertIntoOrderedList(e, list, mid, endIndex);

        }

        private int FindStartIndexOfRow(int rowIndex, List<MatrixEntry<T>> list, int startIndex, int endIndex)
        {
            // Base case
            if (endIndex - startIndex < 5)
            {
                for (int i = startIndex; i < endIndex; ++i)
                {
                    if (list[i].RowIndex == rowIndex)
                    {
                        return i;
                    }
                }
                return -1;
            }

            int mid = (startIndex + endIndex) / 2, midRowIndex = list[mid].RowIndex;
            if (midRowIndex > rowIndex)
            {
                return FindStartIndexOfRow(rowIndex, list, startIndex, mid);
            }
            if (midRowIndex < rowIndex)
            {
                return FindStartIndexOfRow(rowIndex, list, mid, endIndex);
            }

            int k = mid;
            while (k >= startIndex && list[k].RowIndex == rowIndex)
            {
                --k;
            }
            return k + 1;
        }

        #endregion

        public override int Count(Predicate<T> predicate)
        {
            if (predicate is null) throw new ArgumentNullException(nameof(predicate));

            int count;
            if (predicate(_zero))
            {
                count = Rows * Columns - _values.Count;
            }
            else
            {
                count = 0;
            }

            int len = _values.Count, i;
            for (i = 0; i < len; ++i)
            {
                if (predicate(_values[i].Value))
                {
                    ++count;
                }
            }
            return count;
        }

        /// <summary>
        /// Add a non-zero element at the specified row and column index.
        /// </summary>
        /// <param name="rowIndex">The row index of the element.</param>
        /// <param name="columnIndex">The column index of the element.</param>
        /// <param name="value">The value of the element.</param>
        public void Add(int rowIndex, int columnIndex, T value)
        {
            if (!value.Equals(_zero))
            {
                // If values are already sorted, spend O(log(n)) time to preserve order.
                if (_isSorted)
                {
                    UpsertIntoOrderedList(new MatrixEntry<T>(rowIndex, columnIndex, value), _values, 0, _values.Count);
                }

                // Otherwise, just add in any order - values is not sorted anyway
                else
                {
                    _values.Add(new MatrixEntry<T>(rowIndex, columnIndex, value));
                }
            }
        }

        /// <summary>
        /// Returns the matrix trace given a provider.
        /// </summary>
        /// <param name="provider">A provider for type <txt>T</txt>.</param>
        /// <returns>The matrix trace.</returns>
        public override T Trace(IProvider<T> provider)
        {
            T sum = provider.Zero;
            foreach (MatrixEntry<T> e in _values)
            {
                if (e.RowIndex == e.ColumnIndex)
                {
                    sum = provider.Add(sum, e.Value);
                }
            }
            return sum;
        }

        /// <summary>
        /// Returns the matrix transpose of this matrix, as a COO sparse matrix.
        /// </summary>
        /// <returns>The transpose of this matrix.</returns>
        public COOSparseMatrix<T> Transpose()
        {
            List<MatrixEntry<T>> values = new List<MatrixEntry<T>>(_values.Count);
            foreach (MatrixEntry<T> element in _values)
            {
                values.Add(new MatrixEntry<T>(element.ColumnIndex, element.RowIndex, element.Value));
            }
            return new COOSparseMatrix<T>(Columns, Rows, values, false);
        }

        /// <summary>
        /// Returns a vector of the row sums (i.e. the sum of all elements of each row).
        /// </summary>
        /// <param name="provider">The provider to use for the summation.</param>
        /// <returns>The row sums of this matrix, as a <txt>SparseVector</txt>.</returns>
        public BigSparseVector<T> SparseRowSums(IProvider<T> provider)
        {
            Dictionary<long, T> rowsums = new Dictionary<long, T>();

            if (_values.Count == 0)
            {
                return new BigSparseVector<T>(Rows, rowsums);
            }

            // Faster if already pre-sorted
            if (_isSorted)
            {
                int rowIndex = _values[0].RowIndex;

                T sum = provider.Zero;
                foreach (MatrixEntry<T> element in _values)
                {
                    // New row
                    if (element.RowIndex != rowIndex)
                    {
                        rowsums[rowIndex] = sum;

                        rowIndex = element.RowIndex;
                        sum = provider.Zero;
                    }

                    sum = provider.Add(sum, element.Value);
                }

                // Add the last row if sum != 0
                if (!provider.Equals(sum, _zero))
                {
                    rowsums[rowIndex] = sum;
                }

                return new BigSparseVector<T>(Rows, rowsums);
            }

            // If the values are not sorted, simply loop through once. 
            // It is too expensive to sort, since a single loop is O(n), while a 
            // full sort of the values alone is O(n log n)

            else
            {
                foreach (MatrixEntry<T> element in _values)
                {
                    long r = element.RowIndex;
                    if (rowsums.ContainsKey(r))
                    {
                        rowsums[r] = provider.Add(rowsums[r], element.Value);
                    }
                    else
                    {
                        rowsums[r] = element.Value;
                    }
                }

                return new BigSparseVector<T>(Rows, rowsums);
            }
        }

        /// <summary>
        /// Returns a vector of the column sums (i.e. the sum of all elements of each column).
        /// </summary>
        /// <param name="provider">The provider to use for the summation.</param>
        /// <returns>The column sums of this matrix, as a <txt>SparseVector</txt></returns>
        public BigSparseVector<T> SparseColumnSums(IProvider<T> provider)
        {
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }

            // Unlike row sums, a row-major sorted order does nothing 
            // to speed up this operation - so we default to the slow 
            // O(n) method.

            Dictionary<long, T> sums = new Dictionary<long, T>();
            foreach (MatrixEntry<T> e in _values)
            {
                long c = e.ColumnIndex;
                if (sums.ContainsKey(c))
                {
                    sums[c] = provider.Add(sums[c], e.Value);
                }
                else
                {
                    sums[c] = e.Value;
                }
            }

            return new BigSparseVector<T>(Columns, sums);
        }

        public override DenseVector<T> RowSums(IProvider<T> provider)
        {
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }

            T[] sums = RectangularVector.Zeroes<T>(Rows);

            int len = _values.Count, i;
            for (i = 0; i < len; ++i)
            {
                MatrixEntry<T> e = _values[i];
                sums[e.RowIndex] = provider.Add(sums[e.RowIndex], e.Value);
            }
            return new DenseVector<T>(sums);
        }

        public override DenseVector<T> ColumnSums(IProvider<T> provider)
        {
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }

            T[] sums = RectangularVector.Zeroes<T>(Columns);

            int len = _values.Count, i;
            for (i = 0; i < len; ++i)
            {
                MatrixEntry<T> e = _values[i];
                sums[e.ColumnIndex] = provider.Add(sums[e.ColumnIndex], e.Value);
            }
            return new DenseVector<T>(sums);
        }

        /// <summary>
        /// Returns the direct sum between this matrix $A$ and another matrix $B$, i.e. $A \oplus B$.
        /// </summary>
        /// <param name="matrix">The other sparse matrix.</param>
        /// <returns>The direct sum.</returns>
        public COOSparseMatrix<T> DirectSum(COOSparseMatrix<T> matrix)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            int r = Rows, c = Columns;

            // All elements of the first matrix does not change indexes - add directly
            List<MatrixEntry<T>> elements = new List<MatrixEntry<T>>(_values);
            foreach (MatrixEntry<T> e in matrix._values)
            {
                elements.Add(new MatrixEntry<T>(r + e.RowIndex, c + e.ColumnIndex, e.Value));
            }

            // Only sorted if both matrices are sorted.
            return new COOSparseMatrix<T>(Rows + matrix.Rows, Columns + matrix.Columns, elements, _isSorted && matrix._isSorted);
        }

        /// <summary>
        /// Clears all elements from this matrix (matrix will be interpreted as a zero matrix after this).
        /// </summary>
        public override void Clear()
        {
            _values.Clear();
            _isSorted = true;
        }

        public void Clear(Predicate<MatrixEntry<T>> select)
        {
            if (select is null)
            {
                throw new ArgumentOutOfRangeException(nameof(select));
            }

            // Random removal is expensive for lists (up to O(n)), so 
            // we create a new list instead 

            int count = _values.Count, i;
            List<MatrixEntry<T>> newValues = new List<MatrixEntry<T>>(count);

            for (i = 0; i < count; ++i)
            {
                if (!select(_values[i]))
                {
                    newValues.Add(_values[i]);
                }
            }

            _values = newValues;
        }

        public override void Clear(Predicate<T> select)
        {
            if (select is null)
            {
                throw new ArgumentNullException(nameof(select));
            }

            Clear(e => select(e.Value));
        }

        /// <summary>
        /// Clears the specified row from this matrix.
        /// </summary>
        /// <param name="rowIndex">The index of the row to clear.</param>
        public override void ClearRow(int rowIndex)
        {
            if (rowIndex < 0 || rowIndex >= Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(rowIndex));
            }

            if (_isSorted)
            {
                int row_start_index = GetStartOfRow(_values, rowIndex);
                if (row_start_index < 0)
                {
                    return;
                }

                while (row_start_index < _values.Count && _values[row_start_index].RowIndex == rowIndex)
                {
                    _values.RemoveAt(row_start_index);
                }
            }
            else
            {
                for (int i = _values.Count - 1; i >= 0; --i)
                {
                    if (_values[i].RowIndex == rowIndex)
                    {
                        _values.RemoveAt(i);
                    }
                }
            }
        }

        /// <summary>
        /// Clears the specified column of this matrix.
        /// </summary>
        /// <param name="columnIndex">The index of the column to clear.</param>
        public override void ClearColumn(int columnIndex)
        {
            for (int i = _values.Count - 1; i >= 0; --i)
            {
                if (_values[i].ColumnIndex == columnIndex)
                {
                    _values.RemoveAt(i);
                }
            }
        }

        /// <summary>
        /// Clears the specified submatrix from this matrix
        /// </summary>
        /// <param name="firstRow">The first row of this matrix to clear.</param>
        /// <param name="firstColumn">The first column of this matrix to clear.</param>
        /// <param name="rows">The number of rows to clear.</param>
        /// <param name="columns">The number of columns to clear.</param>
        public override void ClearSubmatrix(int firstRow, int firstColumn, int rows, int columns)
        {
            if (rows < 0 || rows >= Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(rows));
            }
            if (columns < 0 || columns >= Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(columns));
            }

            if (firstRow < 0 || firstRow + rows > Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(firstRow));
            }
            if (firstColumn < 0 || firstColumn + columns > Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(firstColumn));
            }

            // A better strategy is to use binary search to find the top-left and bottom-right corner,
            // then use dense iteration in between...
            // But we'll leave this for future implementations lol

            int lastRow = firstRow + rows, lastColumn = firstColumn + columns;
            for (int i = _values.Count - 1; i >= 0; --i)
            {
                MatrixEntry<T> e = _values[i];
                if (firstRow <= e.RowIndex && e.RowIndex < lastRow &&
                    firstColumn <= e.ColumnIndex && e.ColumnIndex < lastColumn)
                {
                    _values.RemoveAt(i);
                }
            }

        }

        public override void ClearLower()
        {
            Clear(e => e.RowIndex > e.ColumnIndex);
        }

        public override void ClearUpper()
        {
            Clear(e => e.RowIndex < e.ColumnIndex);
        }

        /// <summary>
        /// Extracts a row at the specified index, returning a sparse vector. The matrix is 
        /// unchanged.
        /// </summary>
        /// <param name="rowIndex">The index of the row to extract.</param>
        /// <returns>The extracted row of this matrix.</returns>
        public BigSparseVector<T> BigSparseRow(int rowIndex)
        {
            if (rowIndex < 0 || rowIndex >= Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(rowIndex));
            }

            Dictionary<long, T> row = new Dictionary<long, T>();
            if (_isSorted)
            {
                for (int i = 0; i < _values.Count; ++i)
                {
                    MatrixEntry<T> e = _values[i];
                    if (e.RowIndex < rowIndex) continue;

                    if (e.RowIndex == rowIndex)
                    {
                        row[e.ColumnIndex] = e.Value;
                    }

                    if (e.RowIndex > rowIndex)
                    {
                        break;
                    }
                }
                return new BigSparseVector<T>(Columns, row);
            }

            else
            {
                foreach (MatrixEntry<T> e in _values)
                {
                    if (e.RowIndex == rowIndex)
                    {
                        row[e.ColumnIndex] = e.Value;
                    }
                }
                return new BigSparseVector<T>(Columns, row);
            }
        }

        /// <summary>
        /// Extracts a column of this matrix at the specified column index, returning a sparse vector.
        /// The matrix is unchanged.
        /// </summary>
        /// <param name="columnIndex">The index of the column to extract.</param>
        /// <returns>The extracted column of this matrix.</returns>
        public BigSparseVector<T> BigSparseColumn(int columnIndex)
        {
            if (columnIndex < 0 || columnIndex >= Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(columnIndex));
            }

            Dictionary<long, T> vect = new Dictionary<long, T>();
            foreach (MatrixEntry<T> e in _values)
            {
                if (e.ColumnIndex == columnIndex)
                {
                    vect[e.RowIndex] = e.Value;
                }
            }
            return new BigSparseVector<T>(Rows, vect);
        }

        public override DenseVector<T> Row(int rowIndex)
        {
            if (rowIndex < 0 || rowIndex >= Rows)
            {
                throw new ArgumentNullException(nameof(rowIndex));
            }

            T[] row = RectangularVector.Zeroes<T>(Columns);

            int len = _values.Count, i;
            for (i = 0; i < len; ++i)
            {
                MatrixEntry<T> e = _values[i];
                if (e.RowIndex == rowIndex)
                {
                    row[e.ColumnIndex] = e.Value;
                }
            }

            return new DenseVector<T>(row);
        }

        public override DenseVector<T> Column(int columnIndex)
        {
            if (columnIndex < 0 || columnIndex >= Columns)
            {
                throw new ArgumentNullException(nameof(columnIndex));
            }

            T[] column = RectangularVector.Zeroes<T>(Rows);

            int len = _values.Count, i;
            for (i = 0; i < len; ++i)
            {
                MatrixEntry<T> e = _values[i];
                if (e.ColumnIndex == columnIndex)
                {
                    column[e.RowIndex] = e.Value;
                }
            }

            return new DenseVector<T>(column);
        }

        public override SparseVector<T> SparseRow(int rowIndex)
        {
            if (rowIndex < 0 || rowIndex >= Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(rowIndex));
            }

            if (_isSorted)
            {
                int count = _values.Count,
                    index = FindStartIndexOfRow(rowIndex, _values, 0, count);

                if (index < 0)
                {
                    return new SparseVector<T>(Columns);
                }

                int nz = 0;
                for (int i = index; i < count; ++i)
                {
                    if (_values[i].RowIndex == rowIndex)
                    {
                        ++nz;
                    }
                    else
                    {
                        break;
                    }
                }

                int[] indices = new int[nz];
                T[] values = new T[nz];

                for (int i = 0; i < nz; ++i)
                {
                    MatrixEntry<T> e = _values[i + index];
                    indices[i] = e.ColumnIndex;
                    values[i] = e.Value;
                }

                return new SparseVector<T>(Columns, indices, values, true);
            }
            else
            {
                int count = _values.Count;
                List<int> indices = new List<int>();
                List<T> values = new List<T>();

                for (int i = 0; i < count; ++i)
                {
                    MatrixEntry<T> e = _values[i];
                    if (e.RowIndex == rowIndex)
                    {
                        indices.Add(e.ColumnIndex);
                        values.Add(e.Value);
                    }
                }

                int[] indices_arr = indices.ToArray();
                T[] values_arr = values.ToArray();

                Array.Sort(indices_arr, values_arr);
                return new SparseVector<T>(Columns, indices_arr, values_arr, true);
            }
        }

        public override SparseVector<T> SparseColumn(int columnIndex)
        {
            if (columnIndex < 0 || columnIndex >= Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(columnIndex));
            }

            List<int> indices = new List<int>();
            List<T> values = new List<T>();
            int count = _values.Count;
            for (int i = 0; i < count; ++i)
            {
                MatrixEntry<T> e = _values[i];
                if (e.ColumnIndex == columnIndex)
                {
                    indices.Add(e.RowIndex);
                    values.Add(e.Value);
                }
            }

            int[] indices_arr = indices.ToArray();
            T[] values_arr = values.ToArray();

            Array.Sort(indices_arr, values_arr);

            return new SparseVector<T>(Rows, indices_arr, values_arr, true);
        }

        /// <summary>
        /// Returns a submatrix from this matrix.
        /// </summary>
        /// <param name="firstRow">The first row of the matrix to include in the submatrix.</param>
        /// <param name="firstColumn">The first column of the matrix to include in the submatrix.</param>
        /// <param name="rows">The number of rows to include in the submatrix.</param>
        /// <param name="columns">The number of columns to include in the submatrix.</param>
        /// <returns>The submatrix.</returns>
        public COOSparseMatrix<T> Submatrix(int firstRow, int firstColumn, int rows, int columns)
        {
            if (firstRow < 0 || firstRow + rows > Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(firstRow));
            }
            if (firstColumn < 0 || firstColumn + columns > Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(firstColumn));
            }

            if (rows < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(rows));
            }
            if (columns < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(columns));
            }

            List<MatrixEntry<T>> submatrix = new List<MatrixEntry<T>>(_values.Count);
            foreach (MatrixEntry<T> e in _values)
            {
                if (firstRow <= e.RowIndex && 
                    firstColumn <= e.ColumnIndex &&
                    e.RowIndex < firstRow + rows && 
                    e.ColumnIndex < firstColumn + columns)
                {
                    submatrix.Add(new MatrixEntry<T>(e.RowIndex - firstRow, e.ColumnIndex - firstColumn, e.Value));
                }
            }
            return new COOSparseMatrix<T>(rows, columns, submatrix, _isSorted);
        }

        public override Matrix<T> Select(int[] rowIndices, int[] columnIndices)
        {
            if (rowIndices is null) throw new ArgumentNullException(nameof(rowIndices));
            if (columnIndices is null) throw new ArgumentNullException(nameof(columnIndices));

            ThrowHelper.AssertCollectionInRange(rowIndices, 0, Rows, nameof(rowIndices));
            ThrowHelper.AssertCollectionInRange(columnIndices, 0, Columns, nameof(columnIndices));

            int rlen = rowIndices.Length, clen = columnIndices.Length;

            // Initialize inverse arrays to keep track of the original positions
            // of each row and column index selection
            int[] 
                rIndices_inverse = new int[rlen],
                cIndices_inverse = new int[clen];
            for (int i = 0; i < rlen; ++i)
            {
                rIndices_inverse[i] = i;
            }
            for (int i = 0; i < clen; ++i)
            {
                cIndices_inverse[i] = i;
            }
            Array.Sort(rowIndices, rIndices_inverse);
            Array.Sort(columnIndices, cIndices_inverse);

            // Generate dictionarys that point to the first instance of each index
            Dictionary<int, int> rowLookup = new Dictionary<int, int>(rlen),
                columnLookup = new Dictionary<int, int>(clen);
            for (int i = 0; i < rlen; ++i)
            {
                int r = rowIndices[i];
                if (!rowLookup.ContainsKey(r))
                {
                    rowLookup.Add(r, i);
                }
            }
            for (int i = 0; i < clen; ++i)
            {
                int c = columnIndices[i];
                if (!columnLookup.ContainsKey(c))
                {
                    columnLookup.Add(c, i);
                }
            }

            List<MatrixEntry<T>> selection = new List<MatrixEntry<T>>();
            int count = _values.Count;
            for (int i = 0; i < count; ++i)
            {
                MatrixEntry<T> e = _values[i];
                if (rowLookup.ContainsKey(e.RowIndex) && columnLookup.ContainsKey(e.ColumnIndex))
                {
                    int ri = rowLookup[e.RowIndex], ci = columnLookup[e.ColumnIndex];

                    // Extend the range to include duplicates
                    int rEnd = ri, cEnd = ci;
                    while (rEnd < rlen && rowIndices[rEnd] == e.RowIndex) ++rEnd;
                    while (cEnd < clen && columnIndices[cEnd] == e.ColumnIndex) ++cEnd;

                    // Insert all values of the clique
                    for (int r = ri; r < rEnd; ++r)
                    {
                        int rowIndex = rIndices_inverse[r];
                        for (int c = ci; c < cEnd; ++c)
                        {
                            selection.Add(new MatrixEntry<T>(rowIndex, cIndices_inverse[c], e.Value));
                        }
                    }
                }
            }

            // Restore the original row, column indices 
            Array.Sort(rIndices_inverse, rowIndices);
            Array.Sort(cIndices_inverse, columnIndices);

            return new COOSparseMatrix<T>(rlen, clen, selection, false);
        }

        public override Vector<T> SelectAsVector(int rowIndex, int[] columnIndices)
        {
            if (rowIndex < 0 || rowIndex >= Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(rowIndex));
            }
            if (columnIndices is null)
            {
                throw new ArgumentNullException(nameof(columnIndices));
            }
            ThrowHelper.AssertCollectionInRange(columnIndices, 0, Columns, nameof(columnIndices));

            BitArray isnz = new BitArray(Columns);
            int[] cIndex_lookup = new int[Columns];
            int clen = columnIndices.Length;

            // Calculate the non-zero pattern of row
            if (_isSorted)
            {
                int count = _values.Count,
                    start = FindStartIndexOfRow(rowIndex, _values, 0, count);
                if (start < 0)
                {
                    return new SparseVector<T>(columnIndices.Length);
                }

                for (int i = start; i < count; ++i)
                {
                    if (_values[i].RowIndex != rowIndex) break;
                    int c = _values[i].ColumnIndex;
                    isnz[c] = true;
                    cIndex_lookup[c] = i;
                }
            }
            else
            {
                int count = _values.Count;
                for (int i = 0; i < count; ++i)
                {
                    if (_values[i].RowIndex == rowIndex)
                    {
                        int c = _values[i].ColumnIndex;
                        isnz[c] = true;
                        cIndex_lookup[c] = i;
                    }
                }
            }

            int nz = 0;
            for (int ci = 0; ci < clen; ++ci)
            {
                if (isnz[columnIndices[ci]]) ++nz;
            }

            int[] indices = new int[nz];
            T[] values = new T[nz];

            int k = 0;
            for (int ci = 0; ci < clen; ++ci)
            {
                int c = columnIndices[ci];
                if (isnz[c])
                {
                    indices[k] = ci;
                    values[k++] = _values[cIndex_lookup[c]].Value;
                }
            }

            return new SparseVector<T>(clen, indices, values, true);
        }

        public override Vector<T> SelectAsVector(int[] rowIndices, int columnIndex)
        {
            if (rowIndices is null)
            {
                throw new ArgumentNullException(nameof(rowIndices));
            }
            ThrowHelper.AssertCollectionInRange(rowIndices, 0, Rows, nameof(rowIndices));
            if (columnIndex < 0 || columnIndex >= Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(columnIndex));
            }

            BitArray isnz = new BitArray(Rows);
            int[] rIndex_lookup = new int[Rows];
            int rlen = rowIndices.Length, count = _values.Count;

            for (int i = 0; i < count; ++i)
            {
                MatrixEntry<T> e = _values[i];
                if (e.ColumnIndex == columnIndex)
                {
                    isnz[e.RowIndex] = true;
                    rIndex_lookup[e.RowIndex] = i;
                }
            }

            int nz = 0;
            for (int ri = 0; ri < rlen; ++ri)
            {
                if (isnz[rowIndices[ri]])
                {
                    ++nz;
                }
            }

            int[] indices = new int[nz];
            T[] values = new T[nz];

            int k = 0;
            for (int ri = 0; ri < rlen; ++ri)
            {
                int r = rowIndices[ri];
                if (isnz[r])
                {
                    indices[k] = ri;
                    values[k++] = _values[rIndex_lookup[r]].Value;
                }
            }

            return new SparseVector<T>(rlen, indices, values, true);
        }

        /// <summary>
        /// Sets the specified row of this matrix to be equal to a sparse vector of values
        /// </summary>
        /// <param name="rowIndex">The index of the row to set.</param>
        /// <param name="rowValues">The vector of values to set the row to.</param>
        public void SetRow(int rowIndex, BigSparseVector<T> rowValues)
        {
            if (rowIndex < 0 || rowIndex >= Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(rowIndex));
            }
            if (rowValues is null)
            {
                throw new ArgumentNullException(nameof(rowValues));
            }
            if (rowValues.LongDimension != Columns)
            {
                throw new InvalidOperationException("The dimension of the row vector does not match the number of columns of this matrix.");
            }

            if (_isSorted)
            {
                // Find start of row
                int row_start_index = GetStartOfRow(_values, rowIndex);

                // If there are elements, remove them
                if (row_start_index >= 0)
                {
                    while (_values.Count > row_start_index && _values[row_start_index].RowIndex == rowIndex)
                    {
                        _values.RemoveAt(row_start_index);
                    }
                }

                // Add the row replacement values
                List<MatrixEntry<T>> row = new List<MatrixEntry<T>>(rowValues.Values.Count);
                foreach (KeyValuePair<long, T> v in rowValues.Values)
                {
                    if (v.Key > int.MaxValue)
                    {
                        throw new ArgumentOutOfRangeException("The size of the vector is too large. Maximum supported size is int.MaxValue.");
                    }
                    row.Add(new MatrixEntry<T>(rowIndex, (int)v.Key, v.Value));
                }
                row.Sort();
                _values.InsertRange(row_start_index, row);
            }
            else
            {
                // Requires looping through once to remove all rowIndex elements
                for (int i = _values.Count - 1; i >= 0; --i)
                {
                    if (_values[i].RowIndex == rowIndex)
                    {
                        _values.RemoveAt(i);
                    }
                }

                foreach (KeyValuePair<long, T> pair in rowValues.Values)
                {
                    if (pair.Key > int.MaxValue)
                    {
                        throw new ArgumentOutOfRangeException("The size of the row vector is too large. Maximum supported size is int.MaxValue.");
                    }

                    _values.Add(new MatrixEntry<T>(rowIndex, (int)pair.Key, pair.Value));
                }
            }
        }

        /// <summary>
        /// Sets the specified column of this matrix to be equal to a sparse vector if values.
        /// </summary>
        /// <param name="columnIndex">The index of the column to set.</param>
        /// <param name="columnValues"></param>
        public void SetColumn(int columnIndex, BigSparseVector<T> columnValues)
        {
            if (columnIndex < 0 || columnIndex >= Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(columnIndex));
            }

            if (columnValues is null)
            {
                throw new ArgumentNullException(nameof(columnValues));
            }

            if (columnValues.LongDimension != Rows)
            {
                throw new InvalidOperationException("The dimension of the column vector does not match the number of rows of this matrix.");
            }

            for (int i = _values.Count - 1; i >= 0; --i)
            {
                if (_values[i].ColumnIndex == columnIndex)
                {
                    _values.RemoveAt(i);
                }
            }

            foreach (KeyValuePair<long, T> pair in columnValues.Values)
            {
                if (pair.Key > int.MaxValue)
                {
                    throw new ArgumentOutOfRangeException("The size of the column vector is too large. Maximum supported size is int.MaxValue.");
                }
                _values.Add(new MatrixEntry<T>((int)pair.Key, columnIndex, pair.Value));
            }

            _isSorted = false;
        }

        /// <summary>
        /// Copy a block of values of a submatrix into this matrix.
        /// </summary>
        /// <param name="rowIndex">The first row of this matrix to copy into.</param>
        /// <param name="columnIndex">The first column of this matrix to copy into.</param>
        /// <param name="rows">The number of rows to copy.</param>
        /// <param name="columns">The number of columns to copy.</param>
        /// <param name="submatrix">The submatrix containing the values to copy.</param>
        public void SetSubmatrix(int rowIndex, int columnIndex, int rows, int columns, COOSparseMatrix<T> submatrix)
        {
            if (rows < 0 || rows >= submatrix.Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(rows));
            }
            if (columns < 0 || columns >= submatrix.Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(columns));
            }

            int lastRow = rowIndex + rows;
            int lastColumn = columnIndex + columns;
            if (rowIndex < 0 || lastRow >= Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(rowIndex));
            }
            if (columnIndex < 0 || lastColumn >= Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(columnIndex));
            }

            // Remove existing elements 
            for (int i = _values.Count - 1; i >= 0; --i)
            {
                MatrixEntry<T> e = _values[i];
                if (rowIndex <= e.RowIndex && e.RowIndex < lastRow && 
                    columnIndex <= e.ColumnIndex && e.ColumnIndex < lastColumn)
                {
                    _values.RemoveAt(i);
                }
            }

            // Add the new elements 
            foreach (MatrixEntry<T> e in submatrix.Values)
            {
                if (e.RowIndex < rows && e.ColumnIndex < columns)
                {
                    _values.Add(new MatrixEntry<T>(rowIndex + e.RowIndex, columnIndex + e.ColumnIndex, e.Value));
                }
            }

            if (_isSorted)
            {
                _values.Sort();
            }
        }

        /// <summary>
        /// O(n) method for getting start of row
        /// </summary>
        /// <param name="sortedList"></param>
        /// <param name="rowIndex"></param>
        /// <returns></returns>
        private static int GetStartOfRow(List<MatrixEntry<T>> sortedList, int rowIndex)
        {
            for (int i = 0; i < sortedList.Count; ++i)
            {
                int r = sortedList[i].RowIndex;
                if (r < rowIndex)
                {
                    continue;
                }
                if (r == rowIndex)
                {
                    return i;
                }
                if (r > rowIndex)
                {
                    return -1;
                }
            }

            return -1;
        }

        /// <summary>
        /// O(log(n)) method for getting start of row 
        /// Search for start of row in [start, end), returning the index if there is an element frm 
        /// </summary>
        private static int GetStartOfRow(List<MatrixEntry<T>> sortedList, int rowIndex, int start, int end)
        {
            if (end - start < 2)
            {
                int i;
                if (sortedList[start].RowIndex == rowIndex)
                {
                    i = start;
                }
                else if (end > start + 1 && sortedList[start + 1].RowIndex == rowIndex)
                {
                    i = start + 1;
                }
                else
                {
                    // Does not exist!
                    return -1;
                }

                // Traverse to the start of row
                while (i >= 0 && sortedList[i].RowIndex == rowIndex)
                {
                    i--;
                }

                return i + 1;
            }

            int mid = (start + end) / 2;
            if (sortedList[mid].RowIndex < rowIndex)
            {
                // Search in [start, mid)
                return GetStartOfRow(sortedList, rowIndex, start, mid);
            }

            // Search in [mid, end)
            return GetStartOfRow(sortedList, rowIndex, mid, end);
        }

        /// <summary>
        /// Swaps two rows in this matrix.
        /// </summary>
        /// <param name="row1">The index of the first row to swap.</param>
        /// <param name="row2">The index of the second row to swap.</param>
        public override void SwapRows(int row1, int row2)
        {
            if (row1 < 0 || row1 >= Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(row1));
            }
            if (row2 < 0 || row2 >= Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(row2));
            }

            if (row1 == row2)
            {
                return;
            }

            if (_isSorted)
            {
                int row1_start = GetStartOfRow(_values, row1),
                    row2_start = GetStartOfRow(_values, row2);

                List<MatrixEntry<T>> row1Values = new List<MatrixEntry<T>>(),
                    row2Values = new List<MatrixEntry<T>>();

                while (row1_start < _values.Count && _values[row1_start].RowIndex == row1)
                {
                    MatrixEntry<T> e = _values[row1_start];
                    row1Values.Add(new MatrixEntry<T>(row2, e.ColumnIndex, e.Value));
                    _values.RemoveAt(row1_start);
                }

                while (row2_start < _values.Count && _values[row2_start].RowIndex == row2)
                {
                    MatrixEntry<T> e = _values[row2_start];
                    row2Values.Add(new MatrixEntry<T>(row1, e.ColumnIndex, e.Value));
                    _values.RemoveAt(row2_start);
                }

                if (row2_start > row1_start)
                {
                    _values.InsertRange(row2_start, row1Values);
                    _values.InsertRange(row1_start, row2Values);
                }
                else
                {
                    _values.InsertRange(row1_start, row2Values);
                    _values.InsertRange(row2_start, row1Values);
                }
            }
            else
            {
                for (int i = _values.Count - 1; i >= 0; --i)
                {
                    MatrixEntry<T> e = _values[i];
                    if (e.RowIndex == row1)
                    {
                        _values[i] = new MatrixEntry<T>(row2, e.ColumnIndex, e.Value);
                    }
                    else if (e.RowIndex == row2)
                    {
                        _values[i] = new MatrixEntry<T>(row1, e.ColumnIndex, e.Value);
                    }
                }
            }
        }

        /// <summary>
        /// Swaps two columns in this matrix.
        /// </summary>
        /// <param name="column1">The index of the first column to swap.</param>
        /// <param name="column2">The index of the second column to swap.</param>
        public override void SwapColumns(int column1, int column2)
        {
            if (column1 < 0 || column1 >= Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(column1));
            }
            if (column2 < 0 || column2 >= Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(column2));
            }

            for (int i = _values.Count - 1; i >= 0; --i)
            {
                MatrixEntry<T> e = _values[i];
                if (e.ColumnIndex == column1)
                {
                    _values[i] = new MatrixEntry<T>(e.RowIndex, column2, e.Value);
                }
                else if (e.ColumnIndex == column2)
                {
                    _values[i] = new MatrixEntry<T>(e.RowIndex, column1, e.Value);
                }
            }
            _isSorted = false;
        }

        /// <summary>
        /// Append another matrix to this matrix, in the specified direction, without changing any of 
        /// the original matrices.
        /// </summary>
        /// <param name="matrix">The matrix to append.</param>
        /// <param name="direction">The direction in which to append the second matrix.</param>
        /// <returns>The appended matrix.</returns>
        public COOSparseMatrix<T> Append(COOSparseMatrix<T> matrix, AppendDirection direction)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            if (direction == AppendDirection.TOP)
            {
                return matrix.Append(this, AppendDirection.BOTTOM);
            }
            if (direction == AppendDirection.LEFT)
            {
                return matrix.Append(this, AppendDirection.RIGHT);
            }
            if (direction == AppendDirection.TOPLEFT)
            {
                return matrix.Append(this, AppendDirection.BOTTOMRIGHT);
            }
            if (direction == AppendDirection.BOTTOMLEFT)
            {
                return matrix.Append(this, AppendDirection.TOPRIGHT);
            }

            if (direction == AppendDirection.BOTTOM)
            {
                if (Columns != matrix.Columns)
                {
                    throw new InvalidOperationException(ExceptionMessages.MatrixColumnSize);
                }

                List<MatrixEntry<T>> values = new List<MatrixEntry<T>>(_values.Count + matrix._values.Count);
                values.AddRange(_values);
                foreach (MatrixEntry<T> element in matrix._values)
                {
                    values.Add(new MatrixEntry<T>(Rows + element.RowIndex, element.ColumnIndex, element.Value));
                }
                return new COOSparseMatrix<T>(Rows + matrix.Rows, Columns, values, _isSorted && matrix._isSorted);
            }

            if (direction == AppendDirection.RIGHT)
            {
                if (Rows != matrix.Rows)
                {
                    throw new InvalidOperationException(ExceptionMessages.MatrixRowSize);
                }

                List<MatrixEntry<T>> values = new List<MatrixEntry<T>>(_values.Count + matrix._values.Count);
                values.AddRange(_values);
                foreach (MatrixEntry<T> element in matrix._values)
                {
                    values.Add(new MatrixEntry<T>(element.RowIndex, Columns + element.ColumnIndex, element.Value));
                }
                return new COOSparseMatrix<T>(Rows, Columns + matrix.Columns, values, false);
            }

            if (direction == AppendDirection.BOTTOMRIGHT)
            {
                List<MatrixEntry<T>> values = new List<MatrixEntry<T>>(_values.Count + matrix._values.Count);
                values.AddRange(_values);
                foreach (MatrixEntry<T> e in matrix._values)
                {
                    values.Add(new MatrixEntry<T>(Rows + e.RowIndex, Columns + e.ColumnIndex, e.Value));
                }
                return new COOSparseMatrix<T>(Rows + matrix.Rows, Columns + matrix.Columns, values, _isSorted && matrix._isSorted);
            }

            if (direction == AppendDirection.TOPRIGHT)
            {
                List<MatrixEntry<T>> values = new List<MatrixEntry<T>>(_values.Count + matrix._values.Count);
                foreach (MatrixEntry<T> e in matrix._values)
                {
                    values.Add(new MatrixEntry<T>(e.RowIndex, Columns + e.ColumnIndex, e.Value));
                }
                foreach (MatrixEntry<T> e in _values)
                {
                    values.Add(new MatrixEntry<T>(Rows + e.RowIndex, e.ColumnIndex, e.Value));
                }
                return new COOSparseMatrix<T>(Rows + matrix.Rows, Columns + matrix.Columns, values, _isSorted && matrix._isSorted);
            }

            throw new NotImplementedException();
        }

        /// <summary>
        /// Returns the leading (main) diagonal of this matrix as a sparse vector.
        /// </summary>
        /// <returns>The main diagonal of this matrix.</returns>
        public BigSparseVector<T> BigSparseLeadingDiagonal()
        {
            Dictionary<long, T> diag = new Dictionary<long, T>();
            foreach (MatrixEntry<T> e in _values)
            {
                if (e.RowIndex == e.ColumnIndex)
                {
                    diag[e.RowIndex] = e.Value;
                }
            }
            return new BigSparseVector<T>(Math.Min(Rows, Columns), diag);
        }
        public override T[] LeadingDiagonal()
        {
            T[] diag = RectangularVector.Zeroes<T>(Math.Min(Rows, Columns));
            foreach (MatrixEntry<T> e in _values)
            {
                if (e.RowIndex == e.ColumnIndex)
                {
                    diag[e.RowIndex] = e.Value;
                }
            }
            return diag;
        }

        /// <summary>
        /// Permute the columns of this $m \times n$ matrix $A$ by a $n \times n$ permutation matrix $P$, 
        /// i.e. setting this matrix to $AP$
        /// </summary>
        /// <param name="matrix">The permutation matrix $P$</param>
        public void PermuteColumns(PermutationMatrix matrix)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            if (Columns != matrix.Dimension)
            {
                throw new InvalidOperationException("The number of columns of this matrix does not match the dimension of the permutation matrix.");
            }

            int[] permutationVector = matrix.PermutationVector;
            for (int i = _values.Count - 1; i >= 0; --i)
            {
                MatrixEntry<T> e = _values[i];
                _values[i] = new MatrixEntry<T>(e.RowIndex, permutationVector[e.ColumnIndex], e.Value);
            }
            _isSorted = false;
        }

        /// <summary>
        /// Permute the rows of this $m \times n$ matrix $A$ by a $m \times m$ permutation matrix $P$, 
        /// i.e. setting this matrix to $PA$.
        /// </summary>
        /// <param name="matrix">The permutation matrix $P$</param>
        public void PermuteRows(PermutationMatrix matrix)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            if (Rows != matrix.Dimension)
            {
                throw new InvalidOperationException("The number of rows of this matrix does not match the dimension of the permutation matrix.");
            }

            int[] permutationVector = matrix.PermutationVector;
            for (int i = _values.Count - 1; i >= 0; --i)
            {
                MatrixEntry<T> e = _values[i];
                _values[i] = new MatrixEntry<T>(permutationVector[e.RowIndex], e.RowIndex, e.Value);
            }
            _isSorted = false;
        }

        /// <summary>
        /// Permute the columns of this matrix is parallel, using the specified parallelization options if available.
        /// </summary>
        /// <param name="matrix">The permutation matrix.</param>
        /// <param name="options">The parallelization options.</param>
        public void PermuteColumnsParallel(PermutationMatrix matrix, ParallelOptions options = null)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            if (Columns != matrix.Dimension)
            {
                throw new InvalidOperationException("The number of columns of this matrix does not match the dimension of the permutation matrix.");
            }

            if (options is null)
            {
                options = new ParallelOptions();
            }

            int[] permutationVector = matrix.PermutationVector;
            Parallel.ForEach(Partitioner.Create(0, _values.Count), options, range =>
            {
                int min = range.Item1, max = range.Item2;
                for (int i = min; i < max; ++i)
                {
                    MatrixEntry<T> e = _values[i];
                    _values[i] = new MatrixEntry<T>(e.RowIndex, permutationVector[e.ColumnIndex], e.Value);
                }
            });
            _isSorted = false;
        }

        /// <summary>
        /// Permute the rows of this matrix in parallel, using the specified parallelization options if 
        /// available.
        /// </summary>
        /// <param name="matrix">The permutation matrix.</param>
        /// <param name="options">The parallelization options.</param>
        public void PermuteRowsParallel(PermutationMatrix matrix, ParallelOptions options = null)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            if (matrix.Dimension != Rows)
            {
                throw new InvalidOperationException("The dimension of the permutation matrix does not match the row dimension of this matrix.");
            }

            if (options is null)
            {
                options = new ParallelOptions();
            }

            // No thread safety since we are read-only for this vector.
            int[] permutationVector = matrix.PermutationVector;

            Parallel.ForEach(Partitioner.Create(0, _values.Count, 16), options, range =>
            {
                int min = range.Item1, max = range.Item2;
                for (int i = min; i < max; ++i)
                {
                    MatrixEntry<T> e = _values[i];
                    _values[i] = new MatrixEntry<T>(permutationVector[e.RowIndex], e.RowIndex, e.Value);
                }
            });

            _isSorted = false;
        }

        /// <summary>
        /// Returns the vectorized version of this matrix, where all elements are stored in a single vector.
        /// </summary>
        /// <param name="rowMajor">
        /// Indicates whether the vectorization should proceed in row-major or 
        /// column-major order.
        /// </param>
        /// <returns>The vectorized matrix, as a <txt>SparseVector</txt>.</returns>
        public BigSparseVector<T> Vectorize(bool rowMajor = true)
        {
            Dictionary<long, T> values = new Dictionary<long, T>();

            long lr = Rows, lc = Columns;
            if (rowMajor)
            {
                foreach (MatrixEntry<T> e in _values)
                {
                    long key = e.RowIndex * lc + e.ColumnIndex;
                    values.Add(key, e.Value);
                }
            }
            else
            {
                foreach (MatrixEntry<T> e in _values)
                {
                    long key = e.RowIndex + e.ColumnIndex * lr;
                    values.Add(key, e.Value);
                }
            }

            return new BigSparseVector<T>(lr * lc, values);
        }

        /// <summary>
        /// For a $m \times n$ matrix, and a row count $r > 0$, returns a matrix of size 
        /// $r \times \frac{mn}{r}$.
        /// </summary>
        /// <param name="rows">The number of rows of the reshaped matrix, $r$.</param>
        /// <param name="rowMajor">Specifies whether the reshaped matrix should be filled 
        /// in row major order.</param>
        /// <returns>The reshaped matrix.</returns>
        public COOSparseMatrix<T> Reshape(int rows, bool rowMajor = true)
        {
            if (rows <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(rows));
            }

            if ((Rows * Columns) % rows != 0)
            {
                throw new InvalidOperationException("The number of elements in this matrix is not divisible by the number of rows");
            }

            int columns = (Rows * Columns) / rows;
            if (rowMajor)
            {
                List<MatrixEntry<T>> reshaped = new List<MatrixEntry<T>>(_values.Count);
                foreach (MatrixEntry<T> e in _values)
                {
                    long index = e.RowIndex * Columns + e.ColumnIndex;
                    reshaped.Add(new MatrixEntry<T>((int)(index / columns), (int)(index % columns), e.Value));
                }

                // reshaped list should be sorted as long as the original list is also sorted.
                return new COOSparseMatrix<T>(rows, columns, reshaped, _isSorted);
            }
            else
            {
                List<MatrixEntry<T>> reshaped = new List<MatrixEntry<T>>(_values.Count);
                foreach (MatrixEntry<T> e in _values)
                {
                    long index = e.RowIndex + e.ColumnIndex * Rows;
                    reshaped.Add(new MatrixEntry<T>((int)(index / Rows), (int)(index % Rows), e.Value));
                }

                return new COOSparseMatrix<T>(rows, columns, reshaped, false);
            }
        }

        internal override bool SolveTriangular(T[] b, T[] x, IDenseBLAS1<T> blas, bool lower, bool transpose)
        {
            throw new NotImplementedException();
        }

        #region Boolean methods

        public override bool IsDiagonal(Predicate<T> isZero)
        {
            if (isZero is null)
            {
                throw new ArgumentNullException(nameof(isZero));
            }

            int len = _values.Count, i;
            for (i = 0; i < len; ++i)
            {
                MatrixEntry<T> e = _values[i];
                if (e.RowIndex != e.ColumnIndex && !isZero(e.Value))
                {
                    return false;
                }
            }
            return true;
        }

        public override bool IsBanded(int lowerBandwidth, int upperBandwidth, Predicate<T> isZero)
        {
            if (isZero is null)
            {
                throw new ArgumentNullException(nameof(isZero));
            }

            int len = _values.Count, i;
            for (i = 0; i < len; ++i)
            {
                MatrixEntry<T> e = _values[i];
                if (e.RowIndex < e.ColumnIndex)
                {
                    if (e.ColumnIndex - e.RowIndex > upperBandwidth && !isZero(e.Value))
                    {
                        return false;
                    }
                }
                else
                {
                    if (e.RowIndex - e.ColumnIndex > lowerBandwidth && !isZero(e.Value))
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        public override bool IsZero(Predicate<T> isZero)
        {
            if (isZero is null)
            {
                throw new ArgumentNullException(nameof(isZero));
            }

            int count = _values.Count, i;
            for (i = 0; i < count; ++i)
            {
                MatrixEntry<T> e = _values[i];
                if (!isZero(e.Value))
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Returns whether this matrix is symmetric, using the specified equalityComparer.
        /// </summary>
        /// <param name="equalityComparer">
        /// The equality comparer used to determine whether two elements of this 
        /// matrix should be treated as being equal. If <txt>null</txt> the default equality comparer 
        /// for type <txt>T</txt> will be used to establish equality.
        /// </param>
        /// <returns>Whether this matrix is symmetric</returns>
        public override bool IsSymmetric(IEqualityComparer<T> equalityComparer = null)
        {
            if (!IsSquare)
            {
                return false;
            }

            if (equalityComparer is null)
            {
                equalityComparer = EqualityComparer<T>.Default;
            }

            Dictionary<long, T> missingKeys = new Dictionary<long, T>(_values.Count / 2);
            long cols = Columns;
            foreach (MatrixEntry<T> e in _values)
            {
                // Diagonal terms are not paired
                if (e.RowIndex == e.ColumnIndex)
                {
                    continue;
                }

                long key = e.RowIndex * cols + e.ColumnIndex;

                if (missingKeys.ContainsKey(key))
                {
                    // Check values are equal
                    if (!equalityComparer.Equals(missingKeys[key], e.Value))
                    {
                        return false;
                    }

                    // Remove from dictionary
                    missingKeys.Remove(key);
                }
                else
                {
                    // Ignore if zero
                    if (equalityComparer.Equals(e.Value, _zero))
                    {
                        continue;
                    }

                    // Add the complement key for comparison later
                    missingKeys[e.ColumnIndex * cols + e.RowIndex] = e.Value;
                }
            }

            // All keys are matched
            return missingKeys.Count == 0;
        }

        /// <summary>
        /// Returns whether this matrix is an identity matrix, using two predicates that return true if 
        /// an matrix element should be treated as being equal to one and zero respectively.
        /// </summary>
        /// <param name="isOne">A predicate that returns true if the argument should be treated as being equal to one.</param>
        /// <param name="isZero">A predicate that returns true if the argument should be treated as being equal to zero.</param>
        /// <returns>Whether this matrix is an identity matrix.</returns>
        public override bool IsIdentity(Predicate<T> isOne, Predicate<T> isZero)
        {
            if (!IsSquare)
            {
                return false;
            }

            BitArray diagonalFound = new BitArray(Rows);
            foreach (MatrixEntry<T> e in _values)
            {
                if (e.RowIndex == e.ColumnIndex)
                {
                    if (!isOne(e.Value))
                    {
                        return false;
                    }
                    diagonalFound[e.RowIndex] = true;
                }
                else
                {
                    if (!isZero(e.Value))
                    {
                        return false;
                    }
                }
            }

            // Check all diagonal terms present
            for (int i = 0; i < Rows; ++i)
            {
                if (!diagonalFound[i])
                {
                    return false;
                }
            }

            return true;
        }

        public override bool IsLower(Predicate<T> isZero)
        {
            int count = _values.Count, i;
            for (i = 0; i < count; ++i)
            {
                MatrixEntry<T> e = _values[i];
                if (e.ColumnIndex > e.RowIndex && !isZero(e.Value))
                {
                    return false;
                }
            }
            return true;
        }

        public override bool IsUpper(Predicate<T> isZero)
        {
            int count = _values.Count, i;
            for (i = 0; i < count; ++i)
            {
                MatrixEntry<T> e = _values[i];
                if (e.RowIndex < e.ColumnIndex && !isZero(e.Value))
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Returns <txt>true</txt> if <txt>obj</txt> is a <txt>COOSparseMatrix&lt;T&gt;</txt> and equal 
        /// to this matrix according to the <txt>Equals(COOSparseMatrix&lt;T&gt;)</txt> method, and false otherwise.
        /// </summary>
        /// <param name="obj">The object to compare to.</param>
        /// <returns>Whether this matrix is exactly equal to another object.</returns>
        public override bool Equals(object obj)
        {
            if (obj is null)
            {
                return false;
            }
            if (obj is COOSparseMatrix<T>)
            {
                return Equals(obj as COOSparseMatrix<T>);
            }
            return Equals(obj as Matrix<T>);
        }

        /// <summary>
        /// Returns whether this matrix is exactly equal to another.
        /// <p>Two matrices are assumed to be exactly equal if they have the same dimensions, 
        /// and all elements are equal to the corresponding element in the other matrix according 
        /// to the <txt>Equals</txt> method of the type <txt>T</txt> itself.</p>
        /// </summary>
        /// <param name="matrix">The other matrix.</param>
        /// <returns>Whether this matrix is exactly equal to another matrix.</returns>
        public bool Equals(COOSparseMatrix<T> matrix)
        {
            if (matrix is null)
            {
                return false;
            }

            if (Rows != matrix.Rows || Columns != matrix.Columns)
            {
                return false;
            }

            // Remove all zeroes - this is O(k) so is not asymptotically faster than the actual check itself
            Clean();
            matrix.Clean();

            // This should be sufficient to remove a lot of unequal matrices.
            if (_values.Count != matrix._values.Count)
            {
                return false;
            }

            // Elementwise comparisons requires the correct sort order.
            if (!_isSorted)
            {
                Sort();
            }
            if (!matrix._isSorted)
            {
                matrix.Sort();
            }

            for (int i = 0; i < _values.Count; ++i)
            {
                MatrixEntry<T> e1 = _values[i], e2 = matrix._values[i];
                if (e1.RowIndex != e2.RowIndex || e1.ColumnIndex != e2.ColumnIndex || !e1.Value.Equals(e2.Value))
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Returns the hash code of this matrix.
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            // Should we compare elements? - might be too expensive to do a sort

            int hashcode = 13;
            hashcode += 7 * Rows;
            hashcode += 11 * Columns;

            // O(k)
            foreach (MatrixEntry<T> e in _values)
            {
                if (!e.Equals(_zero))
                {
                    hashcode += (17 * e.RowIndex + 101 * e.ColumnIndex) + e.Value.GetHashCode();
                }
            }

            return hashcode;
        }

        #endregion


        #region Operations 

        public override Matrix<T> Add(Matrix<T> matrix, IProvider<T> provider)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            if (CompareRepresentationPriority(this, matrix) < 0)
            {
                return matrix.Add(this, provider); // return the sum as type(matrix)
            }
            return Add(matrix.ToCOOSparseMatrix(), provider);
        }

        /// <summary>
        /// Returns the matrix sum of this matrix and another COO sparse matrix, given a provider for type <txt>T</txt>.
        /// </summary>
        /// <param name="matrix">The other matrix.</param>
        /// <param name="provider">A provider for type <txt>T</txt></param>
        /// <returns>The matrix sum.</returns>
        public COOSparseMatrix<T> Add(COOSparseMatrix<T> matrix, IProvider<T> provider)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            MatrixChecks.AssertMatrixDimensionsEqual(this, matrix);

            List<MatrixEntry<T>> sum = new List<MatrixEntry<T>>(_values);
            sum.AddRange(matrix._values);
            sum.Sort();

            AddAndCondense(sum, provider);

            return new COOSparseMatrix<T>(Rows, Columns, sum, true);
        }

        /// <summary>
        /// Adds this matrix to another, then overwrite this matrix with the sum.
        /// </summary>
        /// <param name="matrix">The other matrix to add.</param>
        /// <param name="provider">A provider for type <txt>T</txt></param>
        public void AddInPlace(COOSparseMatrix<T> matrix, IProvider<T> provider)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            MatrixChecks.AssertMatrixDimensionsEqual(this, matrix);

            _values.AddRange(matrix._values);
            _values.Sort();

            AddAndCondense(_values, provider);

            _isSorted = true;
        }
        private static void AddAndCondense(List<MatrixEntry<T>> sortedList, IProvider<T> provider)
        {
            // Loop through in reverse order 
            for (int i = sortedList.Count - 1; i >= 1; --i)
            {
                // Check to see if adjacent elements are the same, if so add them
                if (sortedList[i].CompareTo(sortedList[i - 1]) == 0)
                {
                    MatrixEntry<T> a = sortedList[i], b = sortedList[i - 1];
                    sortedList[i - 1] = new MatrixEntry<T>(a.RowIndex, a.ColumnIndex, provider.Add(a.Value, b.Value));
                    sortedList.RemoveAt(i);
                }
            }
        }

        /// <summary>
        /// Returns this matrix divided by a scalar, using the specified provider. A new matrix is 
        /// created and the original matrix is unchanged. 
        /// matrix is unchanged.
        /// </summary>
        /// <param name="scalar">The scalar value to divide by.</param>
        /// <param name="provider">The provider to use.</param>
        /// <returns>The result of the matrix-scalar division.</returns>
        public COOSparseMatrix<T> Divide(T scalar, IProvider<T> provider)
        {
            if (scalar is null)
            {
                throw new ArgumentNullException(nameof(scalar));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }

            return Multiply(provider.Divide(provider.One, scalar), provider);
        }

        /// <summary>
        /// Returns this matrix multiplied by a scalar, using the specified provider. A new matrix 
        /// is created, and this matrix is unchanged.
        /// </summary>
        /// <param name="scalar">The scalar to multiply by.</param>
        /// <param name="provider">The provider.</param>
        /// <returns>The matrix-scalar multiplication result.</returns>
        public COOSparseMatrix<T> Multiply(T scalar, IProvider<T> provider)
        {
            if (scalar is null)
            {
                throw new ArgumentNullException(nameof(scalar));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }

            if (scalar.Equals(_zero))
            {
                return new COOSparseMatrix<T>(Rows, Columns);
            }

            List<MatrixEntry<T>> prod = new List<MatrixEntry<T>>(_values.Count);
            foreach (MatrixEntry<T> e in _values)
            {
                prod.Add(new MatrixEntry<T>(e.RowIndex, e.ColumnIndex, provider.Multiply(e.Value, scalar)));
            }
            return new COOSparseMatrix<T>(Rows, Columns, prod, _isSorted);
        }
        /// <summary>
        /// Multiply this matrix by a scalar, and overwrite this matrix with the product.
        /// </summary>
        /// <param name="scalar">The scalar to multiply by.</param>
        /// <param name="provider">The provider to use for the multiplication.</param>
        public override void MultiplyInPlace(T scalar, IProvider<T> provider)
        {
            if (scalar is null)
            {
                throw new ArgumentNullException(nameof(scalar));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }

            if (scalar.Equals(_zero))
            {
                _values.Clear();
                _isSorted = true;
                return;
            }

            for (int i = 0; i < _values.Count; ++i)
            {
                MatrixEntry<T> e = _values[i];
                _values[i] = new MatrixEntry<T>(e.RowIndex, e.ColumnIndex, provider.Multiply(e.Value, scalar));
            }
        }

        /// <summary>
        /// Postmultiply this matrix by a vector, using the specified provider. A sparse vector 
        /// is returned, and neither of the original multiplicands are changed.
        /// </summary>
        /// <param name="vector">The vector to multiplied by.</param>
        /// <param name="provider">The provider.</param>
        /// <returns>The matrix-vector multiplication result.</returns>
        public BigSparseVector<T> Multiply(BigSparseVector<T> vector, IProvider<T> provider)
        {
            if (vector is null)
            {
                throw new ArgumentNullException(nameof(vector));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            if (vector.Dimension != Columns)
            {
                throw new InvalidOperationException("The number of columns of this matrix does not match the dimension of the vector.");
            }

            // In row major sorted order - only need to loop once
            Dictionary<long, T> vect = vector.Values;
            Dictionary<long, T> prod = new Dictionary<long, T>();
            foreach (MatrixEntry<T> element in _values)
            {
                if (vect.ContainsKey(element.ColumnIndex))
                {
                    T product = provider.Multiply(element.Value, vect[element.ColumnIndex]);
                    if (prod.ContainsKey(element.RowIndex))
                    {
                        prod[element.RowIndex] = provider.Add(prod[element.RowIndex], product);
                    }
                    else
                    {
                        prod[element.RowIndex] = product;
                    }
                }
            }

            return new BigSparseVector<T>(Rows, prod);
        }
        
        public override SparseVector<T> Multiply(SparseVector<T> vector, IProvider<T> provider)
        {
            if (vector is null)
            {
                throw new ArgumentNullException(nameof(vector));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            if (vector.Dimension < Columns)
            {
                throw new InvalidOperationException("The vector's dimension is less than the number of columns of this matrix.");
            }

            // Index the vector
            int[] vect_indices = vector.Indices;
            T[] vect_values = vector.Values;
            Dictionary<int, T> dict = new Dictionary<int, T>(vect_indices.Length);
            for (int i = 0; i < vect_indices.Length; ++i)
            {
                dict[vect_indices[i]] = vect_values[i];
            }

            Dictionary<int, T> prod = new Dictionary<int, T>();
            // Iterate once
            for (int i = 0; i < _values.Count; ++i)
            {
                MatrixEntry<T> e = _values[i];
                int r = e.RowIndex, c = e.ColumnIndex;
                if (dict.ContainsKey(c))
                {
                    T v = provider.Multiply(dict[c], e.Value);
                    if (prod.ContainsKey(r))
                    {
                        prod[r] = provider.Add(prod[r], v);
                    }
                    else
                    {
                        prod[r] = v;
                    }
                }
            }

            // Convert into sparse format
            int[] prod_indices = new int[prod.Count];
            int k = 0;
            foreach (int key in prod.Keys)
            {
                prod_indices[k++] = key;
            }
            Array.Sort(prod_indices);

            T[] prod_values = new T[prod_indices.Length];
            for (int i = 0; i < prod_indices.Length; ++i)
            {
                prod_values[i] = prod[prod_indices[i]];
            }

            return new SparseVector<T>(Rows, prod_indices, prod_values, true);
        }

        public override SparseVector<T> TransposeAndMultiply(SparseVector<T> vector, IProvider<T> provider)
        {
            if (vector is null)
            {
                throw new ArgumentNullException(nameof(vector));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            if (vector.Dimension < Rows)
            {
                throw new InvalidOperationException("The vector's dimension is less than the number of rows of this matrix.");
            }

            // Index the vector
            int[] vect_indices = vector.Indices;
            T[] vect_values = vector.Values;
            Dictionary<int, T> dict = new Dictionary<int, T>(vect_indices.Length);
            for (int i = 0; i < vect_indices.Length; ++i)
            {
                dict[vect_indices[i]] = vect_values[i];
            }

            Dictionary<int, T> prod = new Dictionary<int, T>();
            // Iterate once
            for (int i = 0; i < _values.Count; ++i)
            {
                MatrixEntry<T> e = _values[i];
                int r = e.RowIndex, c = e.ColumnIndex;
                if (dict.ContainsKey(r))
                {
                    T v = provider.Multiply(dict[r], e.Value);
                    if (prod.ContainsKey(c))
                    {
                        prod[c] = provider.Add(prod[c], v);
                    }
                    else
                    {
                        prod[c] = v;
                    }
                }
            }

            // Convert into sparse format
            int[] prod_indices = new int[prod.Count];
            int k = 0;
            foreach (int key in prod.Keys)
            {
                prod_indices[k++] = key;
            }
            Array.Sort(prod_indices);

            T[] prod_values = new T[prod_indices.Length];
            for (int i = 0; i < prod_indices.Length; ++i)
            {
                prod_values[i] = prod[prod_indices[i]];
            }

            return new SparseVector<T>(Rows, prod_indices, prod_values, true);
        }

        internal override void Multiply(T[] vector, T[] result, IProvider<T> provider, bool increment)
        {
            if (vector is null)
            {
                throw new ArgumentNullException(nameof(vector));
            }
            if (result is null)
            {
                throw new ArgumentNullException(nameof(result));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }

            if (Columns != vector.Length)
            {
                throw new InvalidOperationException("The number of columns of this matrix does not match the vector dimension.");
            }
            if (result.Length < Rows)
            {
                throw new ArgumentOutOfRangeException("The result vector is not large enough to store the matrix-vector product.");
            }

            // Clear existing values if not increment
            if (!increment)
            {
                if (_zero.Equals(default))
                {
                    Array.Clear(result, 0, Rows);
                }
                else
                {
                    for (int i = 0; i < Rows; ++i)
                    {
                        result[i] = _zero;
                    }
                }
            }

            int len = _values.Count;
            for (int i = 0; i < len; ++i)
            {
                MatrixEntry<T> e = _values[i];
                result[e.RowIndex] = provider.Add(result[e.RowIndex], provider.Multiply(e.Value, vector[e.ColumnIndex]));
            }
        }

        internal override void TransposeAndMultiply(T[] vector, T[] product, IProvider<T> provider, bool increment)
        {
            if (vector is null)
            {
                throw new ArgumentNullException(nameof(vector));
            }
            if (product is null)
            {
                throw new ArgumentNullException(nameof(product));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            if (Rows != vector.Length)
            {
                throw new InvalidOperationException("The number of rows of this matrix does not match the vector dimension.");
            }
            if (product.Length < Columns)
            {
                throw new ArgumentOutOfRangeException("The result vector is not large enough to store the matrix-vector product.");
            }

            if (!increment)
            {
                if (_zero.Equals(default))
                {
                    Array.Clear(product, 0, Columns);
                }
                else
                {
                    for (int i = 0; i < Columns; ++i)
                    {
                        product[i] = _zero;
                    }
                }
            }

            for (int i = _values.Count - 1; i >= 0; --i)
            {
                MatrixEntry<T> e = _values[i];
                product[e.ColumnIndex] = provider.Add(product[e.ColumnIndex], provider.Multiply(e.Value, vector[e.RowIndex]));
            }
        }

        /// <summary>
        /// Postmultiply this matrix by another matrix, using the specified provider. A new matrix 
        /// is created, and neither of the original matrices are changed.
        /// </summary>
        /// <param name="matrix">The matrix to postmultiply by.</param>
        /// <param name="provider">The provider to use.</param>
        /// <returns>The matrix-matrix product.</returns>
        public COOSparseMatrix<T> Multiply(COOSparseMatrix<T> matrix, IProvider<T> provider)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            MatrixChecks.AssertMatrixDimensionsForMultiplication(this, matrix);

            if (!_isSorted)
            {
                Sort();
            }
            if (!matrix._isSorted)
            {
                matrix.Sort();
            }

            Dictionary<int, int> B_start_indexes = matrix.CalculateRowStartIndices();

            List<MatrixEntry<T>> product = new List<MatrixEntry<T>>();
            Dictionary<int, T> rowProducts = new Dictionary<int, T>();

            int rowIndex;

            // Calculate each row of the product in turn
            // Since _values is in row-sorted order, the outer loop is linear.
            for (int i = 0; i < _values.Count; ++i)
            {
                MatrixEntry<T> e = _values[i];
                rowIndex = e.RowIndex;

                // Loop through the row from matrix B
                if (B_start_indexes.ContainsKey(e.ColumnIndex))
                {
                    T A_ij = e.Value;

                    int j = B_start_indexes[e.ColumnIndex];
                    while (j < matrix._values.Count)
                    {
                        MatrixEntry<T> f = matrix._values[j];
                        if (f.RowIndex > e.ColumnIndex)
                        {
                            break;
                        }

                        T prod = provider.Multiply(A_ij, f.Value);
                        if (rowProducts.ContainsKey(f.ColumnIndex))
                        {
                            rowProducts[f.ColumnIndex] = provider.Add(rowProducts[f.ColumnIndex], prod);
                        }
                        else
                        {
                            rowProducts[f.ColumnIndex] = prod;
                        }

                        j++;
                    }
                }

                // If the row is about to change or if its the last index
                if (i == _values.Count - 1 || _values[i + 1].RowIndex != rowIndex)
                {
                    // new row
                    List<MatrixEntry<T>> row = new List<MatrixEntry<T>>();
                    foreach (KeyValuePair<int, T> entry in rowProducts)
                    {
                        if (!provider.Equals(entry.Value, _zero))
                        {
                            row.Add(new MatrixEntry<T>(rowIndex, entry.Key, entry.Value));
                        }
                    }
                    row.Sort();
                    product.AddRange(row);
                    rowProducts.Clear();
                }
            }

            // Since we traversed in row-major order and each row is sorted before insertion, 
            // the 'product' list is actually in correct sorted order.
            return new COOSparseMatrix<T>(Rows, matrix.Columns, product, true);
        }
        /// <summary>
        /// For sorted matrices only - returns the index of the first element for each row index.
        /// The returned dictionary has structure: [row-index, index of first element of that row]
        /// </summary>
        /// <returns></returns>
        private Dictionary<int, int> CalculateRowStartIndices()
        {
            if (!_isSorted)
            {
                throw new NotImplementedException();
            }

            Dictionary<int, int> indexes = new Dictionary<int, int>();

            int currentRowIndex = -1;
            for (int i = 0; i < _values.Count; ++i)
            {
                MatrixEntry<T> element = _values[i];
                if (element.RowIndex != currentRowIndex)
                {
                    indexes[element.RowIndex] = i;
                    currentRowIndex = element.RowIndex;
                }
            }

            return indexes;
        }

        /// <summary>
        /// Postmultiply this matrix by a permutation matrix.
        /// </summary>
        /// <param name="matrix">The permutation matrix to postmultiply by.</param>
        public void MultiplyInPlace(PermutationMatrix matrix)
        {
            PermuteColumns(matrix);
        }

        /// <summary>
        /// Returns the negation of this matrix (i.e. its additive identity, $-A$), using a provider. 
        /// A separate matrix is created, and this matrix is unchanged.
        /// </summary>
        /// <param name="provider">A provider for type <txt>T</txt>.</param>
        /// <returns>The negation of this matrix.</returns>
        public COOSparseMatrix<T> Negate(IProvider<T> provider)
        {
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }

            COOSparseMatrix<T> copy = Copy();
            copy.NegateInPlace(provider);
            return copy;
        }
        /// <summary>
        /// Set this matrix to its additive inverse, $-A$.
        /// </summary>
        /// <param name="provider">A provider for type <txt>T</txt></param>
        public void NegateInPlace(IProvider<T> provider)
        {
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }

            for (int i = 0; i < _values.Count; ++i)
            {
                MatrixEntry<T> e = _values[i];
                _values[i] = new MatrixEntry<T>(e.RowIndex, e.ColumnIndex, provider.Negate(e.Value));
            }
        }

        /// <summary>
        /// Returns this matrix subtract another matrix, using a provider. A new matrix is created,
        /// and neither of the original matrices are changed.
        /// </summary>
        /// <param name="matrix">The matrix to subtract.</param>
        /// <param name="provider">A provider.</param>
        /// <returns>The matrix difference.</returns>
        public COOSparseMatrix<T> Subtract(COOSparseMatrix<T> matrix, IProvider<T> provider)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            MatrixChecks.AssertMatrixDimensionsEqual(this, matrix);

            return Add(matrix.Negate(provider), provider);
        }
        /// <summary>
        /// Subtract another matrix from this matrix, and overwrite this matrix with the result.
        /// </summary>
        /// <param name="matrix">The matrix to subtract.</param>
        /// <param name="provider">A provider.</param>
        public void SubtractInPlace(COOSparseMatrix<T> matrix, IProvider<T> provider)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            MatrixChecks.AssertMatrixDimensionsEqual(this, matrix);

            AddInPlace(matrix.Negate(provider), provider);
        }

        /// <summary>
        /// Return a new matrix where each element is equal to a function applied to the 
        /// corresponding element in this matrix. Elements in this matrix equal to zero will be ignored.
        /// </summary>
        /// <typeparam name="U"></typeparam>
        /// <param name="predicate">The function to apply, $f$ : <txt>T</txt> $\to$ <txt>U</txt>.</param>
        public COOSparseMatrix<U> Elementwise<U>(Func<T, U> predicate) where U : new()
        {
            if (predicate is null)
            {
                throw new ArgumentNullException(nameof(predicate));
            }

            List<MatrixEntry<U>> values = new List<MatrixEntry<U>>(_values.Count);
            foreach (MatrixEntry<T> e in _values)
            {
                if (!e.Equals(_zero))
                {
                    values.Add(new MatrixEntry<U>(e.RowIndex, e.ColumnIndex, predicate(e.Value)));
                }
            }
            return new COOSparseMatrix<U>(Rows, Columns, values, _isSorted);
        }

        /// <summary>
        /// Overwrite each element of this matrix to the output of a function
        /// </summary>
        /// <param name="predicate">A function $f:$ <txt>T</txt> $\to$ <txt>T</txt></param>
        public void ElementwiseInPlace(Func<T, T> predicate)
        {
            if (predicate is null)
            {
                throw new ArgumentNullException(nameof(predicate));
            }

            for (int i = 0; i < _values.Count; ++i)
            {
                MatrixEntry<T> e = _values[i];
                _values[i] = new MatrixEntry<T>(e.RowIndex, e.ColumnIndex, predicate(e.Value));
            }
        }

        #endregion


        /// <summary>
        /// Sorts the elements of this matrix in row-major order.
        /// A sorted matrix is more performant.
        /// </summary>
        public void Sort()
        {
            if (_isSorted)
            {
                return;
            }

            _values.Sort();
            _isSorted = true;
        }

        /// <summary>
        /// Removes all entries that are numerically zero from the coordinate list.
        /// </summary>
        public void Clean()
        {
            for (int i = _values.Count - 1; i >= 0; --i)
            {
                if (_values[i].Equals(_zero))
                {
                    _values.RemoveAt(i);
                }
            }
        }

        public override object Clone()
        {
            return Copy();
        }

        /// <summary>
        /// Create a deep clone of this matrix.
        /// </summary>
        /// <returns>A copy of this matrix.</returns>
        public COOSparseMatrix<T> Copy()
        {
            return new COOSparseMatrix<T>(this);
        }

        /// <summary>
        /// Copy a submatrix of size <txt>rows</txt> $\times$ <txt>columns</txt> from <txt>src</txt> to this matrix.
        /// </summary>
        /// <param name="src">The source matrix containing the values to be copied.</param>
        /// <param name="srcFirstRow">The index of the first row from the source matrix to copy.</param>
        /// <param name="srcFirstCol">The index of the first column from the source matrix to copy.</param>
        /// <param name="destFirstRow">The index of the first row in this matrix to which the values will be copied.</param>
        /// <param name="destFirstCol">The index of the first column in this matrix to which the values will be copied.</param>
        /// <param name="rows">The number of rows to copy.</param>
        /// <param name="columns">The number of columns to copy.</param>
        public void CopyFrom(COOSparseMatrix<T> src, int srcFirstRow, int srcFirstCol, int destFirstRow, int destFirstCol, int rows, int columns)
        {
            if (src is null)
            {
                throw new ArgumentNullException(nameof(src));
            }

            if (rows < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(rows));
            }
            if (columns < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(columns));
            }

            int srcLastRow = srcFirstRow + rows,
                srcLastCol = srcFirstCol + columns,
                destLastRow = destFirstRow + rows,
                destLastCol = destFirstCol + columns;

            if (srcFirstRow < 0 || srcLastRow > src.Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(srcFirstRow));
            }
            if (srcFirstCol < 0 || srcLastCol > src.Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(srcFirstCol));
            }
            if (destFirstRow < 0 || destLastRow > Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(destFirstRow));
            }
            if (destFirstCol < 0 || destLastCol > Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(destFirstCol));
            }

            // delete from this matrix 
            for (int i = _values.Count - 1; i >= 0; --i)
            {
                MatrixEntry<T> e = _values[i];
                if (destFirstRow <= e.RowIndex && e.RowIndex < destLastRow &&
                    destFirstCol <= e.ColumnIndex && e.ColumnIndex < destLastCol)
                {
                    _values.RemoveAt(i);
                }
            }

            // Add to this matrix
            int row_offset = destFirstRow - srcFirstRow;
            int col_offset = destFirstCol - srcFirstCol;
            foreach (MatrixEntry<T> e in src._values)
            {
                if (srcFirstRow <= e.RowIndex && e.RowIndex < srcLastRow &&
                    srcFirstCol <= e.ColumnIndex && e.ColumnIndex < srcLastCol)
                {
                    _values.Add(new MatrixEntry<T>(e.RowIndex + row_offset, e.ColumnIndex + col_offset, e.Value));
                }
            }

            _isSorted = false;
        }


        #region Convertors

        public override BandMatrix<T> ToBandMatrix()
        {
            return new BandMatrix<T>(this);
        }

        public override BlockDiagonalMatrix<T> ToBlockDiagonalMatrix()
        {
            return new BlockDiagonalMatrix<T>(this);
        }

        public override COOSparseMatrix<T> ToCOOSparseMatrix()
        {
            return this;
        }

        public override CSCSparseMatrix<T> ToCSCSparseMatrix()
        {
            return new CSCSparseMatrix<T>(this);
        }

        public override CSRSparseMatrix<T> ToCSRSparseMatrix()
        {
            return new CSRSparseMatrix<T>(this);
        }

        public override DenseMatrix<T> ToDenseMatrix()
        {
            return new DenseMatrix<T>(this);
        }

        public override DiagonalMatrix<T> ToDiagonalMatrix()
        {
            return new DiagonalMatrix<T>(this);
        }

        public override DOKSparseMatrix<T> ToDOKSparseMatrix()
        {
            return new DOKSparseMatrix<T>(this);
        }

        public override MCSRSparseMatrix<T> ToMCSRSparseMatrix()
        {
            return new MCSRSparseMatrix<T>(this);
        }

        public override SKYSparseMatrix<T> ToSKYSparseMatrix()
        {
            return new SKYSparseMatrix<T>(this);
        }

        #endregion


        #region Operators 

        /// <summary>
        /// Returns the product of two COO sparse matrices.
        /// </summary>
        /// <param name="A">The left matrix.</param>
        /// <param name="B">The right matrix.</param>
        /// <returns>The sparse matrix product.</returns>
        public static COOSparseMatrix<T> operator *(COOSparseMatrix<T> A, COOSparseMatrix<T> B)
        {
            if (A is null)
            {
                throw new ArgumentNullException(nameof(A));
            }
            return A.Multiply(B, ProviderFactory.GetDefaultProvider<T>());
        }

        /// <summary>
        /// Returns the product between a COO sparse matrix and a sparse vector.
        /// </summary>
        /// <param name="A">The left matrix.</param>
        /// <param name="x">The right sparse vector.</param>
        /// <returns>The matrix-vector product.</returns>
        public static BigSparseVector<T> operator *(COOSparseMatrix<T> A, BigSparseVector<T> x)
        {
            if (A is null)
            {
                throw new ArgumentNullException(nameof(A));
            }
            return A.Multiply(x, ProviderFactory.GetDefaultProvider<T>());
        }

        /// <summary>
        /// Returns the product between a COO sparse matrix and a dense vector, returning the product as a dense 
        /// vector.
        /// </summary>
        /// <param name="A">The left matrix.</param>
        /// <param name="x">The right dense vector.</param>
        /// <returns>The matrix-vector product.</returns>
        public static DenseVector<T> operator *(COOSparseMatrix<T> A, DenseVector<T> x)
        {
            if (A is null)
            {
                throw new ArgumentNullException(nameof(A));
            }
            return A.Multiply(x, ProviderFactory.GetDefaultProvider<T>());
        }

        /// <summary>
        /// Returns the product between a COO sparse matrix and a scalar.
        /// </summary>
        /// <param name="A">The sparse matrix.</param>
        /// <param name="scalar">The scalar.</param>
        /// <returns>The matrix-scalar product.</returns>
        public static COOSparseMatrix<T> operator *(COOSparseMatrix<T> A, T scalar)
        {
            if (A is null)
            {
                throw new ArgumentNullException(nameof(A));
            }
            return A.Multiply(scalar, ProviderFactory.GetDefaultProvider<T>());
        }

        /// <summary>
        /// Returns the product between a COO sparse matrix and a scalar.
        /// </summary>
        /// <param name="scalar">The scalar.</param>
        /// <param name="A">The sparse matrix.</param>
        /// <returns>The matrix-scalar product.</returns>
        public static COOSparseMatrix<T> operator *(T scalar, COOSparseMatrix<T> A) => A * scalar;

        /// <summary>
        /// Returns a sparse matrix divided by a scalar.
        /// </summary>
        /// <param name="A">The sparse matrix.</param>
        /// <param name="scalar">The scalar.</param>
        /// <returns>The matrix divided by a scalar.</returns>
        public static COOSparseMatrix<T> operator /(COOSparseMatrix<T> A, T scalar)
        {
            if (A is null)
            {
                throw new ArgumentNullException(nameof(A));
            }
            return A.Divide(scalar, ProviderFactory.GetDefaultProvider<T>());
        }

        /// <summary>
        /// Returns the sum of two COO sparse matrices.
        /// </summary>
        /// <param name="A">The left matrix.</param>
        /// <param name="B">The right matrix.</param>
        /// <returns>The sparse matrix sum.</returns>
        public static COOSparseMatrix<T> operator +(COOSparseMatrix<T> A, COOSparseMatrix<T> B)
        {
            if (A is null)
            {
                throw new ArgumentNullException(nameof(A));
            }
            return A.Add(B, ProviderFactory.GetDefaultProvider<T>());
        }

        /// <summary>
        /// Returns the left matrix subtract the right matrix.
        /// </summary>
        /// <param name="A">The left matrix.</param>
        /// <param name="B">The right matrix.</param>
        /// <returns>The sparse matrix difference.</returns>
        public static COOSparseMatrix<T> operator -(COOSparseMatrix<T> A, COOSparseMatrix<T> B)
        {
            if (A is null)
            {
                throw new ArgumentNullException(nameof(A));
            }
            return A.Subtract(B, ProviderFactory.GetDefaultProvider<T>());
        }

        /// <summary>
        /// Returns the negation of this matrix.
        /// </summary>
        /// <param name="A">The matrix to negate.</param>
        /// <returns>The matrix negation.</returns>
        public static COOSparseMatrix<T> operator -(COOSparseMatrix<T> A)
        {
            if (A is null)
            {
                throw new ArgumentNullException(nameof(A));
            }
            return A.Negate(ProviderFactory.GetDefaultProvider<T>());
        }

        /// <summary>
        /// Returns whether two COO sparse matrices are exactly equal to each other, according to 
        /// the <txt>COOSparseMatrix&lt;T&gt;.Equals(COOSparseMatrix&lt;T&gt; matrix)</txt> equality definition.
        /// </summary>
        /// <param name="A">The left matrix.</param>
        /// <param name="B">The right matrix.</param>
        /// <returns>Whether the two matrices are exactly equal.</returns>
        public static bool operator ==(COOSparseMatrix<T> A, COOSparseMatrix<T> B)
        {
            if (A is null)
            {
                return false;
            }
            return A.Equals(B);
        }

        /// <summary>
        /// Returns whether two COO sparse matrices are not exactly equal to each other, according 
        /// to the <txt>COOSparseMatrix&lt;T&gt;.Equals(COOSparseMatrix&lt;T&gt; matrix)</txt> equality definition.
        /// </summary>
        /// <param name="A">The left matrix.</param>
        /// <param name="B">The right matrix.</param>
        /// <returns>Whether the two matrices are not exactly equal.</returns>
        public static bool operator !=(COOSparseMatrix<T> A, COOSparseMatrix<T> B) => !(A == B);

        #endregion


        #region Casts

        /// <summary>
        /// Cast a CSR sparse matrix into a COO sparse matrix.
        /// </summary>
        /// <param name="matrix">A CSR sparse matrix.</param>
        public static explicit operator COOSparseMatrix<T>(CSRSparseMatrix<T> matrix) => new COOSparseMatrix<T>(matrix);

        /// <summary>
        /// Cast a DOK sparse matrix into a COO sparse matrix.
        /// </summary>
        /// <param name="matrix">A DOK sparse matrix.</param>
        public static explicit operator COOSparseMatrix<T>(DOKSparseMatrix<T> matrix) => new COOSparseMatrix<T>(matrix);

        /// <summary>
        /// Cast a dense matrix into a COO sparse matrix.
        /// </summary>
        /// <param name="matrix">A dense matrix.</param>
        public static explicit operator COOSparseMatrix<T>(DenseMatrix<T> matrix) => new COOSparseMatrix<T>(matrix);

        /// <summary>
        /// Cast a band matrix into a COO sparse matrix.
        /// </summary>
        /// <param name="matrix">A band matrix.</param>
        public static explicit operator COOSparseMatrix<T>(BandMatrix<T> matrix) => new COOSparseMatrix<T>(matrix);

        /// <summary>
        /// Cast a diagonal matrix into a COO sparse matrix.
        /// </summary>
        /// <param name="matrix">The diagonal matrix.</param>
        public static explicit operator COOSparseMatrix<T>(DiagonalMatrix<T> matrix) => new COOSparseMatrix<T>(matrix);

        #endregion
    }
    public readonly struct MatrixEntry<T> : IComparable<MatrixEntry<T>>
    {
        internal readonly int RowIndex;
        internal readonly int ColumnIndex;
        internal readonly T Value;

        internal MatrixEntry(int rowIndex, int columnIndex, T value)
        {
            RowIndex = rowIndex;
            ColumnIndex = columnIndex;
            Value = value;
        }

        public int CompareTo(MatrixEntry<T> other)
        {
            if (RowIndex < other.RowIndex)
            {
                return -1;
            }
            else if (RowIndex > other.RowIndex)
            {
                return 1;
            }

            // Same row - compare by column
            return ColumnIndex.CompareTo(other.ColumnIndex);
        }

        public override string ToString()
        {
            return $"({RowIndex},{ColumnIndex},{Value})";
        }
    }
    public readonly struct BigMatrixEntry<T>
    {
        public readonly long RowIndex, ColumnIndex;
        public readonly T Value;

        internal BigMatrixEntry(long rowIndex, long columnIndex, T value)
        {
            RowIndex = rowIndex;
            ColumnIndex = columnIndex;
            Value = value;
        }
    }
}
