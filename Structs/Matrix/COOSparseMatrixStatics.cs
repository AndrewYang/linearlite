﻿using LinearNet.Global;
using LinearNet.Helpers;
using LinearNet.Providers;
using LinearNet.Statistics;
using System;
using System.Collections.Generic;
using System.IO;

namespace LinearNet.Structs
{
    /// <summary>
    /// Static class containing useful methods for creating Coordinate List (COO) sparse matrices.
    /// </summary>
    /// <cat>linear-algebra</cat>
    public static class COOSparseMatrix
    {
        /// <summary>
        /// Returns a new sparse matrix with the specified dimensions, whose entries are all zero.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="rows">The number of rows of the matrix.</param>
        /// <param name="columns">The number of columns of the matrix.</param>
        /// <returns>The zero matrix.</returns>
        public static COOSparseMatrix<T> Zeroes<T>(int rows, int columns) where T : new()
        {
            return new COOSparseMatrix<T>(rows, columns);
        }

        /// <summary>
        /// <p>
        /// Create a random COO sparse matrix, with roughly the specified density. 
        /// </p>
        /// 
        /// <p>
        /// This method is designed for
        /// efficiently creating matrices that approximately fit a certain sparsity criteria. 
        /// </p>
        /// 
        /// <p>
        /// The complexity is $O(k)$ where $k$ is the number of non-zero elements of the matrix 
        /// (compared to $O(\log k)$ for the method the generates an exact number of non-zero elements).
        /// </p>
        /// </summary>
        /// <param name="rows">The number of rows in the created matrix.</param>
        /// <param name="columns">The number of columns in the created matrix.</param>
        /// <param name="approximateDensity">The approximately density (between 0 and 1) of the matrix.</param>
        /// <param name="randomPredicate">A random element generator.</param>
        /// <returns>The random sparse matrix with a certain approximate density, and whose non-zero elements follow a certain independent distribution.</returns>
        public static COOSparseMatrix<T> Random<T>(int rows, int columns, double approximateDensity, Func<T> randomPredicate = null) where T : new()
        {
            Random rand = new Random();
            if (randomPredicate is null)
            {
                randomPredicate = DefaultGenerators.GetRandomGenerator<T>();
            }

            List<MatrixEntry<T>> values = new List<MatrixEntry<T>>();
            for (int i = 0; i < rows; ++i)
            {
                for (int j = 0; j < columns; ++j)
                {
                    if (rand.NextDouble() <= approximateDensity)
                    {
                        values.Add(new MatrixEntry<T>(i, j, randomPredicate()));
                    }
                }
            }
            return new COOSparseMatrix<T>(rows, columns, values, true);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="rows"></param>
        /// <param name="columns"></param>
        /// <param name="approximateDensity"></param>
        /// <param name="distribution"></param>
        /// <param name="seed"></param>
        /// <returns></returns>
        public static COOSparseMatrix<T> Random<T>(int rows, int columns, double approximateDensity, IUnivariateDistribution<T> distribution, int seed = 0) where T : IComparable<T>, new()
        {
            if (distribution is null)
            {
                throw new ArgumentNullException(nameof(distribution));
            }

            Random random = new Random(seed);
            return Random(rows, columns, approximateDensity, () => distribution.Sample(random));
        }

        /// <summary>
        /// Create a square identity matrix of the specified type and dimension.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dimension">The row or column dimension of the matrix.</param>
        /// <returns>The identity matrix over type <txt>T</txt>.</returns>
        public static COOSparseMatrix<T> Identity<T>(int dimension) where T : new()
        {
            if (dimension <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(dimension), ExceptionMessages.NonPositiveNotAllowed);
            }

            IProvider<T> provider = ProviderFactory.GetDefaultProvider<T>();

            List<MatrixEntry<T>> values = new List<MatrixEntry<T>>(dimension);
            for (int d = 0; d < dimension; ++d)
            {
                values.Add(new MatrixEntry<T>(d, d, provider.One));
            }
            return new COOSparseMatrix<T>(dimension, dimension, values, true);
        }

        /// <summary>
        /// Create a square diagonal matrix given the leading diagonal entries of the matrix.
        /// </summary>
        /// <param name="diagonalElements">The diagonal terms of the matrix.</param>
        /// <returns>The diagonal matrix.</returns>
        public static COOSparseMatrix<T> Diag<T>(T[] diagonalElements) where T : new()
        {
            if (diagonalElements is null)
            {
                throw new ArgumentNullException(nameof(diagonalElements));
            }

            int dim = diagonalElements.Length;
            List<MatrixEntry<T>> values = new List<MatrixEntry<T>>(dim);
            for (int d = 0; d < dim; ++d)
            {
                values.Add(new MatrixEntry<T>(d, d, diagonalElements[d]));
            }
            return new COOSparseMatrix<T>(dim, dim, values, true);
        }

        /// <summary>
        /// Create a square diagonal matrix, given the diagonal entries of the matrix as a sparse vector.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="diagonal">The diagonal terms of the matrix.</param>
        /// <returns>The diagonal matrix.</returns>
        public static COOSparseMatrix<T> Diag<T>(BigSparseVector<T> diagonal) where T : new()
        {
            if (diagonal is null)
            {
                throw new ArgumentNullException(nameof(diagonal));
            }

            List<MatrixEntry<T>> elements = new List<MatrixEntry<T>>(diagonal.Values.Count);
            foreach (KeyValuePair<long, T> e in diagonal.Values)
            {
                if (e.Key > int.MaxValue)
                {
                    throw new ArgumentOutOfRangeException(nameof(diagonal), "The dimension of the vector is too large. The maximum supported dimension for this method is int.MaxValue");
                }

                int index = (int)e.Key;
                elements.Add(new MatrixEntry<T>(index, index, e.Value));
            }
            return new COOSparseMatrix<T>(diagonal.Dimension, diagonal.Dimension, elements, true);
        }

        /// <summary>
        /// Structure of file is row[delim]column[delim]value, one per row
        /// Lines starting with '%' or '//' or '#' are ignored
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="path"></param>
        /// <param name="indexing">Specifies where the indexing starts - e.g. 0-indexed means indices start at 0 (most programming 
        /// languages), and 1-indexed means indices start counting at 1 (e.g. Matlab).</param>
        /// <returns></returns>
        public static COOSparseMatrix<T> Load<T>(string path, Func<string, T> parser, char delim = ',', bool raiseNonCriticalExceptions = true, int indexing = 0) where T : new()
        {
            if (string.IsNullOrEmpty(path))
            {
                throw new ArgumentNullException(nameof(path));
            }
            if (parser is null)
            {
                throw new ArgumentNullException(nameof(parser));
            }
            if (!System.IO.File.Exists(path))
            {
                throw new ArgumentException(nameof(path), "File does not exist.");
            }

            // First row is [row] [column] [nnz] (the meta info)
            int rows = -1, columns = -1;
            List<MatrixEntry<T>> entries = null;
            bool foundMetaInfo = false, sorted = true;

            using var fs = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
            using var sr = new StreamReader(fs);

            while (sr.Peek() > -1)
            {
                string line = sr.ReadLine();
                if (string.IsNullOrEmpty(line)) 
                {
                    if (raiseNonCriticalExceptions)
                    {
                        throw new FormatException($"Null or empty line.");
                    }
                    continue;
                }

                // Ignore comments
                char first = line[0];
                if (first == '%' || first == '#' || line.StartsWith("//")) continue;

                string[] split = line.Split(delim);
                if (split.Length < 3)
                {
                    if (raiseNonCriticalExceptions)
                    {
                        throw new FormatException($"Invalid number of entries in the line: {line}.");
                    }
                    continue; // fail silently
                }

                // Parse meta info
                if (!foundMetaInfo)
                {
                    if (!int.TryParse(split[0], out rows) || !int.TryParse(split[1], out columns) || !int.TryParse(split[2], out int nnz))
                    {
                        throw new FormatException("Invalid meta info: expected 3 integers.");
                    }
                    entries = new List<MatrixEntry<T>>(nnz);
                    foundMetaInfo = true;
                }
                else
                {
                    int r, c = 0;
                    if (!int.TryParse(split[0], out r) || !int.TryParse(split[1], out c))
                    {
                        if (raiseNonCriticalExceptions)
                        {
                            throw new FormatException($"Row or column index is not an integer in the line '{line}'.");
                        }
                    }

                    r -= indexing;
                    c -= indexing;

                    if (r < 0 || r >= rows)
                    {
                        throw new ArgumentOutOfRangeException("Row index out of range.");
                    }
                    if (c < 0 || c >= columns)
                    {
                        throw new ArgumentOutOfRangeException("Column index out of range.");
                    }

                    MatrixEntry<T> e = new MatrixEntry<T>(r, c, parser(split[2]));

                    // Check sort
                    if (sorted && entries.Count > 0 && e.CompareTo(entries[entries.Count - 1]) < 0)
                    {
                        sorted = false;
                    }

                    entries.Add(e);
                }
            }
            return new COOSparseMatrix<T>(rows, columns, entries, sorted);
        }
        public static COOSparseMatrix<double> Load(string path, char delim = ',', bool raiseNonCriticalExceptions = true, int indexing = 0)
        {
            return Load(path, s => double.Parse(s), delim, raiseNonCriticalExceptions, indexing);
        }
    }
}
