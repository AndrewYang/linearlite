﻿using LinearNet.Global;
using LinearNet.Matrices;
using LinearNet.Matrices.Cholesky.Kernels;
using LinearNet.Matrices.Multiplication;
using LinearNet.Matrices.Sparse;
using LinearNet.Matrices.Sparse.Decompositions;
using LinearNet.Matrices.Sparse.Symbolic;
using LinearNet.Providers;
using LinearNet.Providers.Managed;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace LinearNet.Structs
{
    /// <summary>
    /// Sparse matrix implementation using compressed-sparse column storage format.  
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <cat>linear-algebra</cat>
    public class CSCSparseMatrix<T> : Matrix<T> where T : new()
    {
        private int[] _columnIndices, _rowIndices;
        private T[] _values;
        private readonly T _zero;

        internal T[] Values { get { return _values; } }
        internal int[] ColumnIndices { get { return _columnIndices; } }
        internal int[] RowIndices { get { return _rowIndices; } }

        public override T[] this[int index] { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public override T this[int rowIndex, int columnIndex] 
        {
            get
            {
                if (rowIndex < 0 || rowIndex >= Rows)
                {
                    throw new ArgumentOutOfRangeException(nameof(rowIndex));
                }
                if (columnIndex < 0 || columnIndex >= Columns)
                {
                    throw new ArgumentOutOfRangeException(nameof(columnIndex));
                }

                return CSRHelper<T>.Get(columnIndex, rowIndex, _columnIndices, _rowIndices, _values, _zero);
            }
            set
            {
                if (rowIndex < 0 || rowIndex >= Rows)
                {
                    throw new ArgumentOutOfRangeException(nameof(rowIndex));
                }
                if (columnIndex < 0 || columnIndex >= Columns)
                {
                    throw new ArgumentOutOfRangeException(nameof(columnIndex));
                }

                CSRHelper<T>.Set(columnIndex, rowIndex, _columnIndices, ref _rowIndices, ref _values, value);
            }
        }

        public override int SymbolicNonZeroCount => _values.Length;
        public override IEnumerable<MatrixEntry<T>> NonZeroEntries
        {
            get
            {
                for (int c = 0; c < Columns; ++c)
                {
                    int end = _columnIndices[c + 1];
                    for (int i = _columnIndices[c]; i < end; ++i)
                    {
                        yield return new MatrixEntry<T>(_rowIndices[i], c, _values[i]);
                    }
                }
            }
        }

        public double Density { get { return _values.Length / ((double)Rows * Columns); } }


        #region Constructors

        public CSCSparseMatrix(int rows, int columns) : base(MatrixStorageType.CSC, rows, columns)
        {
            _columnIndices = RectangularVector.Repeat(0, columns + 1);
            _values = new T[0];
            _rowIndices = new int[0];
            _zero = new T();
        }
        public CSCSparseMatrix(BandMatrix<T> matrix) : base(MatrixStorageType.CSC)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            Rows = matrix.Rows;
            Columns = matrix.Columns;
            _zero = new T();

            int upperWidth = matrix.UpperBandwidth,
                lowerWidth = matrix.LowerBandwidth;
            T[][] upper = matrix.UpperBands,
                lower = matrix.LowerBands;
            T[] diagonal = matrix.Diagonal;

            // Assume the band matrix is well-formed to calculate length
            int nnz = diagonal.Length;
            for (int b = 0; b < upperWidth; ++b)
            {
                nnz += upper[b].Length;
            }
            for (int b = 0; b < lowerWidth; ++b)
            {
                nnz += lower[b].Length;
            }

            _values = new T[nnz];
            _rowIndices = new int[nnz];
            _columnIndices = new int[Columns + 1];

            int k = 0;
            for (int c = 0; c < Columns; ++c)
            {
                // Upper bands first 
                for (int b = upperWidth - 1; b >= 0; --b)
                {
                    int index = c - b - 1;
                    if (0 <= index && index < upper[b].Length)
                    {
                        _values[k] = upper[b][index];
                        _rowIndices[k] = index;
                        ++k;
                    }
                }

                // diagonal band
                if (c < diagonal.Length)
                {
                    _values[k] = diagonal[c];
                    _rowIndices[k] = c;
                    ++k;
                }

                // Lower bands last
                for (int b = 0; b < lowerWidth; ++b)
                {
                    if (c < lower[b].Length)
                    {
                        _values[k] = lower[b][c];
                        _rowIndices[k] = c + b + 1;
                        ++k;
                    }
                }
                _columnIndices[c + 1] = k;
            }
        }
        public CSCSparseMatrix(BlockDiagonalMatrix<T> matrix) : base(MatrixStorageType.CSC)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            Rows = matrix.Rows;
            Columns = matrix.Columns;
            _zero = new T();

            int[] blockIndices = matrix.BlockIndices;
            DenseMatrix<T>[] blocks = matrix.Blocks;

            int nnz = matrix.SymbolicNonZeroCount;
            _columnIndices = new int[Columns + 1];
            _rowIndices = new int[nnz];
            _values = new T[nnz];

            int k = 0;
            for (int b = 0; b < blocks.Length; ++b)
            {
                DenseMatrix<T> block = blocks[b];
                int offset = blockIndices[b], rows = block.Rows, cols = block.Columns;
                for (int c = 0; c < cols; ++c)
                {
                    int colIndex = c + offset;
                    for (int r = 0; r < rows; ++r)
                    {
                        _rowIndices[k] = r + offset;
                        _values[k] = block.Values[r][c];
                        ++k;
                    }
                    _columnIndices[colIndex + 1] = k;
                }
            }
        }
        public CSCSparseMatrix(CSRSparseMatrix<T> matrix) : base(MatrixStorageType.CSC)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            Rows = matrix.Rows;
            Columns = matrix.Columns;
            _zero = new T();

            int[] rowIndices = matrix.RowIndices, colIndices = matrix.ColumnIndices;
            T[] values = matrix.Values;

            int len = values.Length;
            _columnIndices = new int[Columns + 1];
            _values = new T[len];
            _rowIndices = new int[len];
            _zero = new T();

            // Count the number of elements in each column, store in the respective cell in _columnIndices
            for (int r = 0; r < Rows; ++r)
            {
                int row_end = rowIndices[r + 1];
                for (int i = rowIndices[r]; i < row_end; ++i)
                {
                    _columnIndices[colIndices[i] + 1]++;
                }
            }

            // Accumulate the column indices
            for (int c = 0; c < Columns; ++c)
            {
                _columnIndices[c + 1] += _columnIndices[c];
            }

            // Set up temporary buffer to store the number of elements entered in each column so far
            int[] colCounts = new int[Columns];

            for (int r = 0; r < Rows; ++r)
            {
                int row_end = rowIndices[r + 1];
                for (int i = rowIndices[r]; i < row_end; ++i)
                {
                    int c = colIndices[i],
                        k = colCounts[c] + _columnIndices[c];

                    _rowIndices[k] = r;
                    _values[k] = values[i];
                    colCounts[c]++;
                }
            }
        }
        public CSCSparseMatrix(DOKSparseMatrix<T> matrix) : base(MatrixStorageType.CSC)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            Rows = matrix.Rows;
            Columns = matrix.Columns;
            _zero = new T();

            Dictionary<long, T> values = matrix.Values;
            long lc = Columns;

            _columnIndices = new int[Columns + 1];
            _rowIndices = new int[values.Count];
            _values = new T[values.Count];

            // Calculate column counts
            foreach (KeyValuePair<long, T> e in values)
            {
                _columnIndices[(int)(e.Key % lc) + 1]++;
            }

            // Accumulate column counts
            for (int c = 0; c < Columns; ++c)
            {
                _columnIndices[c + 1] += _columnIndices[c];
            }

            // Create temp buffer to hold the column counts so far
            int[] colCounts = new int[Columns];

            // Iterate a 2nd time, to insert values/rowIndices
            foreach (KeyValuePair<long, T> e in values.OrderBy(p => p.Key))
            {
                int r = (int)(e.Key / lc), 
                    c = (int)(e.Key % lc), 
                    i = colCounts[c] + _columnIndices[c];

                _values[i] = e.Value;
                _rowIndices[i] = r;
                colCounts[c]++;
            }
        }
        public CSCSparseMatrix(COOSparseMatrix<T> matrix) : base(MatrixStorageType.CSC)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            Rows = matrix.Rows;
            Columns = matrix.Columns;
            _zero = new T();

            List<MatrixEntry<T>> values = matrix.Values;
            _columnIndices = new int[Columns + 1];
            _rowIndices = new int[values.Count];
            _values = new T[values.Count];

            // Keeping the rowIndices in ascending order within each column
            if (!matrix.IsSorted)
            {
                values = new List<MatrixEntry<T>>(matrix.Values);
                values.Sort();
            }

            // Unfortunately, even if values is sorted, it will be sorted by row 
            // then by column, rendering the sort order useless. 
            // We must proceed as per the other constructors by looping through the 
            // entire non-zero list twice, first time collecting the column counts
            // and second time to insert the values. 
            // The complexity is still O(nnz)
            for (int i = values.Count - 1; i >= 0; --i)
            {
                _columnIndices[values[i].ColumnIndex + 1]++;
            }

            // Accumulate column counts
            for (int i = 0; i < Columns; ++i)
            {
                _columnIndices[i + 1] += _columnIndices[i];
            }

            // Temp buffer to store the number of elements per column already inserted
            int[] colCounts = new int[Columns];

            // 2nd loop - insert the values
            for (int i = 0; i < values.Count; ++i)
            {
                MatrixEntry<T> e = values[i];
                int c = e.ColumnIndex, index = _columnIndices[c] + colCounts[c];
                _values[index] = e.Value;
                _rowIndices[index] = e.RowIndex;
                colCounts[c]++;
            }
        }
        public CSCSparseMatrix(CSCSparseMatrix<T> matrix) : base(MatrixStorageType.CSC)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            Rows = matrix.Rows;
            Columns = matrix.Columns;
            _zero = matrix._zero;

            _rowIndices = matrix._rowIndices.Copy();
            _columnIndices = matrix._columnIndices.Copy();
            _values = matrix._values.Copy();
        }
        public CSCSparseMatrix(DenseMatrix<T> matrix) : base(MatrixStorageType.CSC)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            Rows = matrix.Rows;
            Columns = matrix.Columns;
            _zero = new T();

            _columnIndices = new int[Columns + 1];

            // Although it is possible to avoid working with lists and its associated 
            // memory overhead if we loop through twice as per the other constructors,
            // since we are dealing with a dense matrix to begin with it is assumed that
            // memory is not a limiting factor. Checking if an element is zero could 
            // potentially be expensive for some types, so we stick to a single loop.

            List<T> values = new List<T>();
            List<int> rowIndices = new List<int>();

            T[][] dvalues = matrix.Values;
            for (int c = 0; c < Columns; ++c)
            {
                for (int r = 0; r < Rows; ++r)
                {
                    if (!dvalues[r][c].Equals(_zero))
                    {
                        values.Add(dvalues[r][c]);
                        rowIndices.Add(r);
                    }
                }
                _columnIndices[c + 1] = values.Count;
            }

            _rowIndices = rowIndices.ToArray();
            _values = values.ToArray();
        }
        public CSCSparseMatrix(DiagonalMatrix<T> matrix) : base(MatrixStorageType.CSC)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            Rows = matrix.Rows;
            Columns = matrix.Columns;
            _zero = new T();

            T[] diagonal = matrix.DiagonalTerms;
            int len = diagonal.Length;

            _columnIndices = new int[Columns + 1];
            _rowIndices = new int[len];
            _values = new T[len];

            for (int i = 0; i < len; )
            {
                _rowIndices[i] = i;
                _values[i] = diagonal[i];
                ++i;
                _columnIndices[i] = i;
            }

            for (int i = len + 1; i < Columns; ++i)
            {
                _columnIndices[i] = len;
            }
        }
        public CSCSparseMatrix(MCSRSparseMatrix<T> matrix) : base(MatrixStorageType.CSC)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            Rows = matrix.Rows;
            Columns = matrix.Columns;
            _zero = new T();

            int[] indices = matrix.Indices;
            T[] values = matrix.Values;

            int nz = values.Length - 1;
            _columnIndices = new int[Columns + 1];
            _rowIndices = new int[nz];
            _values = new T[nz];

            // Calculate the column counts
            for (int r = 0; r < Rows; ++r)
            {
                int end = indices[r + 1];
                for (int i = indices[r]; i < end; ++i)
                {
                    _columnIndices[indices[i]]++;
                }
            }
            // Add the diagonal terms
            for (int c = 0; c < Columns; ++c)
            {
                _columnIndices[c]++;
            }
            // Shift
            for (int c = Columns - 1; c >= 0; --c)
            {
                _columnIndices[c + 1] = _columnIndices[c];
            }
            // Accumulate
            for (int c = 0; c < Columns; ++c)
            {
                _columnIndices[c + 1] += _columnIndices[c];
            }

            // Initialize next array
            int[] next = new int[Columns];
            Array.Copy(_columnIndices, next, Columns);

            // Populate all arrays
            for (int r = 0; r < Rows; ++r)
            {
                int end = indices[r + 1],
                    i = indices[r];

                for (; i < end; ++i)
                {
                    int c = indices[i];
                    if (c > r) // to the right of the main diagonal
                    {
                        break;
                    }
                    int index = next[c]++;
                    _rowIndices[index] = r;
                    _values[index] = values[i];
                }

                // Add the diagonal term
                int diagIndex = next[r]++;
                _rowIndices[diagIndex] = r;
                _values[diagIndex] = values[r];

                // Values to the right of the main diagonal
                for (; i < end; ++i)
                {
                    int index = next[indices[i]]++;
                    _rowIndices[index] = r;
                    _values[index] = values[i];
                }
            }
        }
        public CSCSparseMatrix(Matrix<T> matrix) : base(MatrixStorageType.CSC)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            Rows = matrix.Rows;
            Columns = matrix.Columns;
            _zero = new T();

            int nz = matrix.SymbolicNonZeroCount;
            _columnIndices = new int[Columns + 1];
            _rowIndices = new int[nz];
            _values = new T[nz];

            IEnumerable<MatrixEntry<T>> values = matrix.NonZeroEntries;

            // First iteration is to calculate the column counts
            foreach (MatrixEntry<T> e in values)
            {
                _columnIndices[e.ColumnIndex]++;
            }

            // Shift and aggregate column counts
            for (int c = Columns; c >= 1; --c)
            {
                _columnIndices[c] = _columnIndices[c - 1];
            }
            _columnIndices[0] = 0;
            for (int c = 0; c < Columns; ++c)
            {
                _columnIndices[c + 1] += _columnIndices[c];
            }

            int[] next = new int[Columns];
            Array.Copy(_columnIndices, next, Columns);

            // Second pass is to insert numerical values
            foreach (MatrixEntry<T> e in values)
            {
                int index = next[e.ColumnIndex]++;
                _rowIndices[index] = e.RowIndex;
                _values[index] = e.Value;
            }

            // CSC requires sorted columns. Check whether already pre-sorted
            // in O(r) time or spend O(r log(r)) time sorting
            for (int c = 0; c < Columns; ++c)
            {
                int start = _columnIndices[c], end = _columnIndices[c + 1];
                bool isSorted = true;
                for (int i = start + 1; i < end; ++i)
                {
                    if (_rowIndices[i] < _rowIndices[i - 1])
                    {
                        isSorted = false;
                        break;
                    }
                }

                if (!isSorted)
                {
                    Array.Sort(_columnIndices, start, end - start);
                }
            }
        }

        internal CSCSparseMatrix(int rows, int[] columnIndices, int[] rowIndices, T[] values) : base(MatrixStorageType.CSC)
        {
            Rows = rows;
            Columns = columnIndices.Length - 1;

            _columnIndices = columnIndices;
            _rowIndices = rowIndices;
            _values = values;
        }

        #endregion


        #region Boolean methods

        public override bool IsDiagonal(Predicate<T> isZero)
        {
            return CSRBooleanHelper<T>.IsDiagonal(_columnIndices, _rowIndices, _values, Columns, isZero);
        }

        public override bool IsBanded(int lowerBandwidth, int upperBandwidth, Predicate<T> isZero)
        {
            return CSRBooleanHelper<T>.IsBanded(_columnIndices, _rowIndices, _values, Columns, upperBandwidth, lowerBandwidth, isZero);
        }

        public override bool IsZero(Predicate<T> isZero)
        {
            return CSRBooleanHelper<T>.IsZero(_values, isZero);
        }

        public override bool IsIdentity(Predicate<T> isZero, Predicate<T> isOne)
        {
            return CSRBooleanHelper<T>.IsIdentity(_columnIndices, _rowIndices, _values, Columns, Rows, isZero, isOne);
        }

        public override bool IsLower(Predicate<T> isZero)
        {
            return CSRHelper<T>.IsUpper(_columnIndices, _rowIndices, _values, Columns, isZero);
        }
        public bool IsLowerSymbolic()
        {
            return CSRHelper<T>.IsUpperSymbolic(_columnIndices, _rowIndices, Rows);
        }
        public override bool IsUpper(Predicate<T> isZero)
        {
            return CSRHelper<T>.IsLower(_columnIndices, _rowIndices, _values, Columns, isZero);
        }
        public bool IsUpperSymbolic()
        {
            return CSRHelper<T>.IsLowerSymbolic(_columnIndices, _rowIndices, Columns);
        }

        public bool Equals(CSCSparseMatrix<T> matrix, Func<T, T, bool> equals)
        {
            if (matrix is null)
            {
                return false;
            }
            if (Rows != matrix.Rows || Columns != matrix.Columns)
            {
                return false;
            }

            for (int c = 0; c < Columns; ++c)
            {
                int c1 = c + 1;
                if (!CSRHelper<T>.RowEquals(
                    _rowIndices,
                    _values,
                    _columnIndices[c],
                    _columnIndices[c1],

                    matrix._rowIndices,
                    matrix._values,
                    matrix._columnIndices[c],
                    matrix._columnIndices[c1],

                    _zero,
                    equals))
                {
                    return false;
                }
            }
            return true;
        }
        public bool Equals(CSCSparseMatrix<T> matrix, IEqualityComparer<T> comparer)
        {
            if (comparer is null)
            {
                throw new ArgumentNullException(nameof(comparer));
            }
            return Equals(matrix, comparer.Equals);
        }
        public bool Equals(CSCSparseMatrix<T> matrix)
        {
            return Equals(matrix, EqualityComparer<T>.Default);
        }
        public override bool Equals(object obj)
        {
            if (obj is null)
            {
                return false;
            }
            if (obj is CSCSparseMatrix<T>)
            {
                return Equals(obj as CSCSparseMatrix<T>);
            }
            return Equals(obj as Matrix<T>);
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        #endregion


        internal void PrintStructure()
        {
            Debug.WriteLine("CSC matrix(" + Rows + " x " + Columns + ")\t" + new string('=', 100));
            _columnIndices.Print();
            _rowIndices.Print();
            _values.Print();
        }


        public override int Count(Predicate<T> predicate)
        {
            if (predicate is null) throw new ArgumentNullException(nameof(predicate));

            int count, i, len = _values.Length;
            if (predicate(_zero))
            {
                count = Rows * Columns - _values.Length;
            }
            else
            {
                count = 0;
            }

            for (i = 0; i < len; ++i)
            {
                if (predicate(_values[i]))
                {
                    ++count;
                }
            }
            return count;
        }

        public override T[] LeadingDiagonal()
        {
            return CSRHelper<T>.LeadingDiagonal(_columnIndices, _rowIndices, _values, Math.Min(Rows, Columns));
        }

        public override T Trace(IProvider<T> provider)
        {
            return CSRHelper<T>.Trace(_columnIndices, _rowIndices, _values, Columns, provider);
        }

        public CSCSparseMatrix<T> Transpose()
        {
            CSRHelper<T>.Transpose(_columnIndices, _rowIndices, _values, out int[] rowIndices, out int[] colIndices, out T[] values, Columns, Rows);
            return new CSCSparseMatrix<T>(Columns, colIndices, rowIndices, values);
        }

        // TODO: complete this method
        public CSCSparseMatrix<T> Reshape(int rows)
        {
            if (rows <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(rows), ExceptionMessages.NonPositiveNotAllowed);
            }
            if ((long)Rows * Columns % rows != 0L)
            {
                throw new ArgumentOutOfRangeException(nameof(rows), "Capacity of this matrix is not divisible by the number of rows of the reshaped matrix.");
            }

            int cols = (int)((long)Rows * Columns / rows);
            int[] colIndices = new int[cols + 1];

            throw new NotImplementedException();
        }

        public SparseVector<T> Vectorize(bool rowMajor = true)
        {
            int len = _values.Length;
            int[] indices = new int[len];
            
            if (rowMajor)
            {
                int k = 0;
                // Requires a sort
                for (int c = 0; c < Columns; ++c)
                {
                    int col_end = _columnIndices[c + 1];
                    for (int i = _columnIndices[c]; i < col_end; ++i)
                    {
                        indices[k++] = _rowIndices[i] * Columns + c;
                    }
                }

                T[] values = _values.Copy();
                Array.Sort(indices, values);

                return new SparseVector<T>(Rows * Columns, indices, values, true);
            }
            else
            {
                int k = 0;
                for (int c = 0; c < Columns; ++c)
                {
                    int col_end = _columnIndices[c + 1];
                    for (int i = _columnIndices[c]; i < col_end; ++i)
                    {
                        indices[k++] = _rowIndices[i] + c * Rows;
                    }
                }

                return new SparseVector<T>(Rows * Columns, indices, _values.Copy(), true);
            }
        }

        public T FrobeniusNorm(IProvider<T> provider)
        {
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            return FrobeniusNorm(x => x, provider);
        }
        public F FrobeniusNorm<F>(Func<T, F> norm, IProvider<F> provider)
        {
            if (norm is null)
            {
                throw new ArgumentNullException(nameof(norm));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            return CSRHelper<T>.FrobeniusNorm(_values, norm, provider);
        }
        public T FrobeniusDistance(CSCSparseMatrix<T> matrix, IProvider<T> provider)
        {
            return FrobeniusDistance(matrix, x => x, provider, provider);
        }
        public F FrobeniusDistance<F>(CSCSparseMatrix<T> matrix, Func<T, F> norm, IProvider<T> providerT, IProvider<F> providerF)
        {
            if (norm is null)
            {
                throw new ArgumentNullException(nameof(norm));
            }
            if (providerT is null)
            {
                throw new ArgumentNullException(nameof(providerT));
            }
            if (providerF is null)
            {
                throw new ArgumentNullException(nameof(providerF));
            }
            return CSRHelper<T>.FrobeniusDistance<F>(_columnIndices, _rowIndices, _values, matrix._columnIndices, matrix._rowIndices, matrix._values, norm, providerT, providerF);
        }

        public override object Clone()
        {
            return Copy();
        }
        public CSCSparseMatrix<T> Copy()
        {
            return new CSCSparseMatrix<T>(this);
        }

        /// <summary>
        /// Returns the number of structural (symbolic) non-zero entries stored in each row.
        /// </summary>
        /// <returns>The number of structural non-zeroes in each row.</returns>
        public DenseVector<int> RowCountsSymbolic()
        {
            return CSRHelper<T>.ColumnCountSymbolic(_rowIndices, Rows);
        }

        /// <summary>
        /// Returns the number of structural (symbolic) non-zero entries stored in each column.
        /// </summary>
        /// <returns>The number of structural non-zeroes in each column.</returns>
        public DenseVector<int> ColumnCountsSymbolc()
        {
            return CSRHelper<T>.RowCountSymbolic(_columnIndices, Columns);
        }

        public BigSparseVector<T> BigColumn(int columnIndex)
        {
            return CSRHelper<T>.BigSparseRow(_columnIndices, _rowIndices, _values, Columns, Rows, columnIndex);
        }
        public override DenseVector<T> Column(int columnIndex)
        {
            return CSRHelper<T>.DenseRow(_columnIndices, _rowIndices, _values, _zero, Columns, Rows, columnIndex);
        }
        public override SparseVector<T> SparseColumn(int columnIndex)
        {
            return CSRHelper<T>.SparseRow(_columnIndices, _rowIndices, _values, Columns, Rows, columnIndex);
        }

        public BigSparseVector<T> BigRow(int rowIndex)
        {
            return CSRHelper<T>.BigSparseColumn(_columnIndices, _rowIndices, _values, Columns, Rows, rowIndex);
        }
        public override DenseVector<T> Row(int rowIndex)
        {
            return CSRHelper<T>.DenseColumn(_columnIndices, _rowIndices, _values, _zero, Columns, Rows, rowIndex);
        }
        public override SparseVector<T> SparseRow(int rowIndex)
        {
            return CSRHelper<T>.SparseColumn(_columnIndices, _rowIndices, _values, Columns, Rows, rowIndex);
        }

        /// <summary>
        /// Returns a contigous block submatrix of size <txt>rows</txt> x <txt>columns</txt>, starting at (<txt>firstRow</txt>, <txt>firstColumn</txt>).
        /// </summary>
        /// <param name="firstRow"></param>
        /// <param name="firstColumn"></param>
        /// <param name="rows"></param>
        /// <param name="columns"></param>
        /// <returns></returns>
        public CSCSparseMatrix<T> Submatrix(int firstRow, int firstColumn, int rows, int columns)
        {
            CSRHelper<T>.Submatrix(_columnIndices, _rowIndices, _values, Columns, Rows, firstColumn, firstRow, columns, rows,
                out int[] columnIndices, out int[] rowIndices, out T[] values);

            return new CSCSparseMatrix<T>(rows, columnIndices, rowIndices, values);
        }

        /// <summary>
        /// Returns the lower triangular half of this matrix in compressed-sparse-column storage format.
        /// </summary>
        /// <returns>The lower triangular matrix.</returns>
        public CSCSparseMatrix<T> Lower()
        {
            CSRHelper<T>.Upper(_columnIndices, _rowIndices, _values, Columns, out int[] columnIndices, out int[] rowIndices, out T[] values);
            return new CSCSparseMatrix<T>(Rows, columnIndices, rowIndices, values);
        }

        /// <summary>
        /// Returns the upper triangular half of this matrix in compressed-sparse-column storage format.
        /// </summary>
        /// <returns>The upper triangular matrix.</returns>
        public CSCSparseMatrix<T> Upper()
        {
            CSRHelper<T>.Lower(_columnIndices, _rowIndices, _values, Columns, out int[] columnIndices, out int[] rowIndices, out T[] values);
            return new CSCSparseMatrix<T>(Rows, columnIndices, rowIndices, values);
        }

        /// <summary>
        /// If the matrix is square, returns a new symmetric matrix where the upper half is overwritten 
        /// by its lower triangular half.
        /// </summary>
        /// <returns></returns>
        public CSCSparseMatrix<T> ReflectLower()
        {
            if (!IsSquare)
            {
                throw new InvalidOperationException();
            }

            // Calculate column counts
            int[] columnIndices = new int[Columns + 1];
            for (int c = 0; c < Columns; ++c)
            {
                int col_end = _columnIndices[c + 1];
                for (int i = _columnIndices[c]; i < col_end; ++i)
                {
                    int r = _rowIndices[i];
                    if (r < c) continue;

                    if (r == c)
                    {
                        columnIndices[c]++;
                    }
                    else
                    {
                        columnIndices[c]++; // the original
                        columnIndices[r]++; // the reflection
                    }
                }
            }

            // Shift and accumulate column counts 
            for (int c = Columns; c > 0; --c)
            {
                columnIndices[c] = columnIndices[c - 1];
            }
            columnIndices[0] = 0;
            for (int c = 0; c < Columns; ++c)
            {
                columnIndices[c + 1] += columnIndices[c];
            }

            // Allocate storage
            int[] rowIndices = new int[columnIndices[Columns]];
            T[] values = new T[columnIndices[Columns]];

            // Keep track of the index to insert next for each column
            int[] next = new int[Columns];
            for (int c = 0; c < Columns; ++c)
            {
                next[c] = columnIndices[c];
            }

            // Insert all upper elements first
            for (int c = 0; c < Columns; ++c)
            {
                int col_end = _columnIndices[c + 1];
                for (int i = _columnIndices[c]; i < col_end; ++i)
                {
                    int r = _rowIndices[i];
                    if (r < c) continue;

                    // Insert into transposed half
                    int index = next[r]++;
                    rowIndices[index] = c;
                    values[index] = _values[i];
                }
            }

            // Insert all lower triangular elements next, as a block
            for (int c = 0; c < Columns; ++c)
            {
                int col_end = _columnIndices[c + 1];

                // Find first term in lower triangular
                int i = _columnIndices[c];
                while (i < col_end && _rowIndices[i] <= c) ++i;

                int index = next[c], len = col_end - i;
                Array.Copy(_rowIndices, i, rowIndices, index, len);
                Array.Copy(_values, i, values, index, len);
            }

            return new CSCSparseMatrix<T>(Rows, columnIndices, rowIndices, values);
        }

        public override Matrix<T> Select(int[] rowIndices, int[] columnIndices)
        {
            if (rowIndices is null)
            {
                throw new ArgumentNullException(nameof(rowIndices));
            }
            if (columnIndices is null)
            {
                throw new ArgumentNullException(nameof(columnIndices));
            }

            ThrowHelper.AssertCollectionInRange(rowIndices, 0, Rows, nameof(rowIndices));
            ThrowHelper.AssertCollectionInRange(columnIndices, 0, Columns, nameof(columnIndices));

            CSRHelper<T>.Select(columnIndices, rowIndices, _columnIndices, _rowIndices, _values, Columns, Rows,
                out int[] rIndices, out int[] cIndices, out T[] values);

            return new CSCSparseMatrix<T>(rowIndices.Length, rIndices, cIndices, values);
        }

        public override Vector<T> SelectAsVector(int rowIndex, int[] columnIndices)
        {
            if (rowIndex < 0 || rowIndex >= Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(rowIndex));
            }
            if (columnIndices is null)
            {
                throw new ArgumentNullException(nameof(columnIndices));
            }
            ThrowHelper.AssertCollectionInRange(columnIndices, 0, Columns, nameof(columnIndices));

            return CSRHelper<T>.Select(columnIndices, rowIndex, _columnIndices, _rowIndices, _values);
        }

        public override Vector<T> SelectAsVector(int[] rowIndices, int columnIndex)
        {
            if (rowIndices is null)
            {
                throw new ArgumentNullException(nameof(rowIndices));
            }
            ThrowHelper.AssertCollectionInRange(rowIndices, 0, Rows, nameof(rowIndices));

            if (columnIndex < 0 || columnIndex >= Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(columnIndex));
            }

            return CSRHelper<T>.Select(columnIndex, rowIndices, _columnIndices, _rowIndices, _values, Rows);
        }

        public override DenseVector<T> RowSums(IProvider<T> provider)
        {
            return CSRHelper<T>.ColumnSums(_rowIndices, _values, Rows, provider);
        }
        public override DenseVector<T> ColumnSums(IProvider<T> provider)
        {
            return CSRHelper<T>.RowSums(_columnIndices, _values, Columns, provider);
        }

        public override void Clear()
        {
            Array.Clear(_columnIndices, 0, Columns + 1);
            _rowIndices = new int[0];
            _values = new T[0];
        }
        public override void Clear(Predicate<T> select)
        {
            if (select is null)
            {
                throw new ArgumentNullException(nameof(select));
            }
            Clear(e => select(e.Value), true);
        }
        public override void ClearRow(int rowIndex)
        {
            CSRHelper<T>.ClearColumn(_columnIndices, _rowIndices, _values, Columns, Rows, rowIndex);
        }
        public override void ClearColumn(int columnIndex)
        {
            CSRHelper<T>.ClearRow(_columnIndices, ref _rowIndices, ref _values, Columns, columnIndex);
        }
        public override void ClearSubmatrix(int rowIndex, int columnIndex, int rows, int columns)
        {
            if (rowIndex < 0 || rowIndex >= Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(rowIndex));
            }
            if (columnIndex < 0 || columnIndex >= Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(columnIndex));
            }

            int bottom = rowIndex + rows, right = columnIndex + columns;
            if (rows < 0 || bottom >= Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(rows));
            }
            if (columns < 0 || right >= Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(columns));
            }

            Clear(e => rowIndex <= e.RowIndex && e.RowIndex < bottom && columnIndex <= e.ColumnIndex && e.ColumnIndex < right, true);
        }
        public override void ClearLower()
        {
            Clear(e => e.RowIndex > e.ColumnIndex, true);
        }
        public override void ClearUpper()
        {
            Clear(e => e.RowIndex < e.ColumnIndex, true);
        }

        /// <summary>
        /// Modifies this matrix by removing all elements that match a predicate
        /// </summary>
        /// <param name="select">
        /// A predicate with 3 arguments - (1) the row index, (2) the column index,
        /// and the value of the element itself.
        /// </param>
        /// <param name="defrag">If <txt>true</txt>, the internal matrix structures will 
        /// be resized. Recommended if a large number of elements are being removed from 
        /// the matrix.</param>
        public void Clear(Predicate<MatrixEntry<T>> select, bool defrag = true)
        {
            if (select is null)
            {
                throw new ArgumentNullException(nameof(select));
            }

            int p = 0;
            for (int c = 0; c < Columns; ++c)
            {
                int col_end = _columnIndices[c + 1];
                for (int i = _columnIndices[c]; i < col_end; ++i)
                {
                    if (!select(new MatrixEntry<T>(_rowIndices[i], c, _values[i])))
                    {
                        // Guaranteed that p <= i, so this subroutine will not 
                        // overwrite values
                        _rowIndices[p] = _rowIndices[i];
                        _values[p] = _values[i];
                        ++p;
                    }
                }

                // We cannot overwrite _columnIndices[c + 1] since 
                // we need the old value in the next iteration of the loop,
                // so we store it in _columnIndices[c] (which is no longer needed)
                // and spend O(n) time shifting it at the end
                _columnIndices[c] = p;
            }

            // Shift the column indices
            for (int c = Columns; c > 0; --c)
            {
                _columnIndices[c] = _columnIndices[c - 1];
            }
            _columnIndices[0] = 0;

            // Resize arrays
            if (defrag)
            {
                Array.Resize(ref _rowIndices, p);
                Array.Resize(ref _values, p);
            }
        }

        public void SetColumn(int columnIndex, BigSparseVector<T> column)
        {
            if (column is null)
            {
                throw new ArgumentNullException(nameof(column));
            }
            if (columnIndex < 0 || columnIndex >= Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(columnIndex));
            }
            if (column.Dimension != Rows)
            {
                throw new ArgumentOutOfRangeException("The vector's dimension does not match the number of rows of this matrix.");
            }
            CSRHelper<T>.SetRow(_columnIndices, ref _rowIndices, ref _values, Columns, columnIndex, column);
        }
        public void SetColumn(int columnIndex, SparseVector<T> column)
        {
            if (column is null)
            {
                throw new ArgumentNullException(nameof(column));
            }
            if (columnIndex < 0 || columnIndex >= Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(columnIndex));
            }
            if (column.Dimension != Rows)
            {
                throw new ArgumentOutOfRangeException("The vector's dimension does not match the number of rows of this matrix.");
            }
            CSRHelper<T>.SetRow(_columnIndices, ref _rowIndices, ref _values, Columns, columnIndex, column);
        }
        /// <summary>
        /// Set a column of this matrix using the values of a implicitly defined sparse array, consisting of the indexes of 
        /// non-zero values (<txt>indices</txt>) and a workspace containing the values themselves.
        /// </summary>
        /// <param name="columnIndex"></param>
        /// <param name="workspace"></param>
        /// <param name="indices"></param>
        public void SetColumn(int columnIndex, T[] workspace, IEnumerable<int> indices)
        {
            if (workspace is null)
            {
                throw new ArgumentNullException(nameof(workspace));
            }
            if (indices is null)
            {
                throw new ArgumentNullException(nameof(indices));
            }
            if (columnIndex < 0 || columnIndex >= Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(columnIndex));
            }

            int count = indices.Count();
            int col_start = _columnIndices[columnIndex], 
                col_end = _columnIndices[columnIndex + 1],
                col_count = col_end - col_start;
            int len = _values.Length;

            T[] values = new T[col_count - count + len];
            int[] rowIndices = new int[col_count - count + len];

            Array.Copy(_values,     0,          values,     0,                  col_start);
            Array.Copy(_rowIndices, 0,          rowIndices, 0,                  col_start);
            Array.Copy(_values,     col_end,    values,     col_start + count,  len - col_end);
            Array.Copy(_rowIndices, col_end,    rowIndices, col_start + count,  len - col_end);

            int k = col_start;
            foreach (int i in indices)
            {
                _values[k] = workspace[i];
                _rowIndices[k] = i;
                ++k;
            }

            int offset = count - col_count;
            for (int c = columnIndex + 1; c <= Columns; ++c)
            {
                _columnIndices[c] -= offset;
            }
        }
        public void SetColumn(int columnIndex, DenseVector<T> column)
        {
            if (column is null)
            {
                throw new ArgumentNullException(nameof(column));
            }
            if (columnIndex < 0 || columnIndex >= Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(columnIndex));
            }
            if (column.Dimension != Rows)
            {
                throw new ArgumentOutOfRangeException("The vector's dimension does not match the number of rows of this matrix.");
            }
            CSRHelper<T>.SetRow(_columnIndices, ref _rowIndices, ref _values, Columns, columnIndex, column, _zero);
        }

        public void SetRow(int rowIndex, BigSparseVector<T> row)
        {
            if (row is null)
            {
                throw new ArgumentNullException(nameof(row));
            }
            if (rowIndex < 0 || rowIndex >= Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(rowIndex));
            }
            if (row.Dimension != Columns)
            {
                throw new ArgumentOutOfRangeException("The vector's dimension does not match the number of columns of this matrix.");
            }
            CSRHelper<T>.SetColumn(_columnIndices, ref _rowIndices, ref _values, Columns, rowIndex, _zero, row);
        }
        public void SetRow(int rowIndex, SparseVector<T> row)
        {
            if (row is null)
            {
                throw new ArgumentNullException(nameof(row));
            }
            if (rowIndex < 0 || rowIndex >= Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(rowIndex));
            }
            if (row.Dimension != Columns)
            {
                throw new ArgumentOutOfRangeException("The vector's dimension does not match the number of columns of this matrix.");
            }
            CSRHelper<T>.SetColumn(_columnIndices, ref _rowIndices, ref _values, Columns, rowIndex, _zero, row);
        }

        public override void SwapRows(int rowIndex1, int rowIndex2)
        {
            CSRHelper<T>.SwapColumns(_columnIndices, _rowIndices, _values, rowIndex1, rowIndex2, Columns, Rows);
        }
        public override void SwapColumns(int columnIndex1, int columnIndex2)
        {
            CSRHelper<T>.SwapRows(_columnIndices, _rowIndices, _values, columnIndex1, columnIndex2, Columns);
        }

        /// <summary>
        /// Permute the rows of this matrix using the specified permutation vector.
        /// </summary>
        /// <param name="rowPermutations">The permutation vector.</param>
        public void PermuteRows(int[] rowPermutations)
        {
            if (rowPermutations is null)
            {
                throw new ArgumentNullException(nameof(rowPermutations));
            }
            if (rowPermutations.Length != Rows)
            {
                throw new InvalidOperationException("The length of rowPermutations does not match the number of rows of this matrix.");
            }
            if (!rowPermutations.IsPermutation())
            {
                throw new ArgumentException("rowPermutations is not a permutation vector.");
            }

            static int compFirst(Tuple<int, T> a, Tuple<int, T> b) => a.Item1.CompareTo(b.Item1);

            int[] invPermutations = new int[Rows];
            for (int r = 0; r < Rows; ++r)
            {
                invPermutations[rowPermutations[r]] = r;
            }

            List<Tuple<int, T>> rowIndices = new List<Tuple<int, T>>();
            for (int c = 0; c < Columns; ++c)
            {
                int col_end = _columnIndices[c + 1],
                    col_start = _columnIndices[c];

                rowIndices.Clear();
                for (int i = col_start; i < col_end; ++i)
                {
                    rowIndices.Add(new Tuple<int, T>(invPermutations[_rowIndices[i]], _values[i]));
                }
                rowIndices.Sort(compFirst);

                for (int i = col_start, k = 0; i < col_end; ++i, ++k)
                {
                    Tuple<int, T> entry = rowIndices[k];
                    _rowIndices[i] = entry.Item1;
                    _values[i] = entry.Item2;
                }
            }
        }
        /// <summary>
        /// Permute the rows of this matrix using the specified permutation matrix. This is equivalent to 
        /// premultiplying this matrix by the permutation matrix $P$, $PA$.
        /// </summary>
        /// <param name="permutation">The permutation matrix.</param>
        public void PermuteRows(PermutationMatrix permutation)
        {
            if (permutation is null)
            {
                throw new ArgumentNullException(nameof(permutation));
            }
            if (permutation.Dimension != Rows)
            {
                throw new InvalidOperationException("The number of rows of this matrix does not match the dimension of the permutation matrix.");
            }
            PermuteRows(permutation.PermutationVector);
        }

        /// <summary>
        /// Permute the columns of this matrix using the specified permutation vector.
        /// </summary>
        /// <param name="permutations">The column permutation vector.</param>
        public void PermuteColumns(int[] permutations)
        {
            if (permutations is null)
            {
                throw new ArgumentNullException(nameof(permutations));
            }
            if (permutations.Length != Columns)
            {
                throw new InvalidOperationException("The number of columns of this matrix does not match the dimension of the column permutation vector.");
            }
            if (!permutations.IsPermutation())
            {
                throw new ArgumentException("The vector is not a permutation vector.");
            }

            int count = _rowIndices.Length;

            // Temp buffers
            int[] columnIndices = new int[Columns + 1];
            T[] values = new T[count];
            int[] rowIndices = new int[count];

            // store the column counts inside colIndices
            for (int c = 0; c < Columns; ++c)
            {
                int pc = permutations[c];
                columnIndices[pc + 1] = (_columnIndices[c + 1] - _columnIndices[c]);
            }
            // Accumulate
            for (int c = 0; c < Columns; ++c)
            {
                columnIndices[c + 1] += columnIndices[c];
            }

            for (int c = 0; c < Columns; ++c)
            {
                int pc = permutations[c];
                int len = _columnIndices[c + 1] - _columnIndices[c];

                // Copy a block of row indexes
                Array.Copy(_rowIndices, _columnIndices[c], rowIndices, columnIndices[pc], len);
                Array.Copy(_values, _columnIndices[c], values, columnIndices[pc], len);
            }

            _rowIndices = rowIndices;
            _columnIndices = columnIndices;
            _values = values;
        }
        /// <summary>
        /// Permute the columns of this matrix using the specified permutation matrix.
        /// </summary>
        /// <param name="permutation">The permutation matrix.</param>
        public void PermuteColumns(PermutationMatrix permutation)
        {
            if (permutation is null)
            {
                throw new ArgumentNullException(nameof(permutation));
            }
            if (permutation.Dimension != Columns)
            {
                throw new InvalidOperationException("The number of columns of this matrix does not match the dimension of the permutation matrix.");
            }
            PermuteColumns(permutation.PermutationVector);
        }

        /// <summary>
        /// Solve the lower triangular system $Lx = b$ using this matrix as the lower-triangular coefficient 
        /// matrix. $b$ is sparse and $x$ is dense.
        /// </summary>
        /// <param name="b">The right-hand-side vector.</param>
        /// <param name="x">The solution vector.</param>
        /// <param name="provider">The provider used for elementwise operations.</param>
        public void SolveTriangular(SparseVector<T> b, DenseVector<T> x, IProvider<T> provider)
        {
            CSCTriangularSolve<T> solver = new CSCTriangularSolve<T>(provider);
            solver.SolveLower(this, x.Values, new List<int>(), b, Rows);
        }

        /// <summary>
        /// Solve the square lower (upper) triangular system $Lx = b$ ($Ux = b$) with $b, x$ dense.
        /// If this matrix is not lower-triangular, all entries strictly above the main diagonal will be treated 
        /// as being equal to zero.
        /// </summary>
        /// <param name="b">The dense right-hand-side vector $b$.</param>
        /// <param name="x">The dense solution vector $x$.</param>
        /// <param name="provider"></param>
        /// <param name="lower">
        /// If true, the coefficient matrix is assumed to be lower triangular (all elements above the main diagonal will 
        /// be ignored). Otherwise, the coefficient matrix is assumed to be upper triangular (all elements below the main
        /// diagonal will be ignored).
        /// </param>
        /// <param name="transpose">
        /// If true, the coefficient matrix will be transposed, i.e. the system $L^Tx = b$ ($U^Tx = b$) will be solved instead.
        /// </param>
        internal override bool SolveTriangular(T[] b, T[] x, IDenseBLAS1<T> blas, bool lower, bool transpose)
        {
            int dim = Rows;
            Array.Copy(b, 0, x, 0, dim);

            // TODO: make use of the dense BLAS itself, instead of relying on its provider
            IProvider<T> provider = blas.Provider;

            if (lower)
            {
                if (transpose)
                {
                    for (int r = dim - 1; r >= 0; --r)
                    {
                        int row_start = _columnIndices[r],
                            row_end = _columnIndices[r + 1];

                        if (_rowIndices[row_start] != r)
                        {
                            // Get to the correct index
                            while (row_start < row_end && _rowIndices[row_start] < r)
                            {
                                ++row_start;
                            }

                            // Check again
                            if (row_start >= row_end || _rowIndices[row_start] != r)
                            {
                                //throw new ArgumentOutOfRangeException($"Coefficient matrix is missing the diagonal entry ({r}, {r})");
                                return false;
                            }
                        }

                        T xr = x[r];
                        for (int i = row_start + 1; i < row_end; ++i)
                        {
                            xr = provider.Subtract(xr, provider.Multiply(_values[i], x[_rowIndices[i]]));
                        }
                        x[r] = provider.Divide(xr, _values[row_start]);
                    }
                }
                else
                {
                    for (int c = 0; c < dim; ++c)
                    {
                        int col_start = _columnIndices[c],
                            col_end = _columnIndices[c + 1];

                        // Confirm diagonal term is present
                        if (_rowIndices[col_start] != c)
                        {
                            // Iterate until we reach the diagonal
                            while (col_start < col_end && _rowIndices[col_start] < c)
                            {
                                ++col_start;
                            }

                            // Check again - if not the diagonal, then the diagonal is missing
                            if (col_start >= col_end || _rowIndices[col_start] != c)
                            {
                                //throw new ArgumentOutOfRangeException($"Coefficient matrix is missing the diagonal entry ({c}, {c}).");
                                return false;
                            }
                        }

                        T xc = provider.Divide(x[c], _values[col_start]);
                        for (int k = col_start + 1; k < col_end; ++k)
                        {
                            int r = _rowIndices[k];
                            if (r > c)
                            {
                                x[r] = provider.Subtract(x[r], provider.Multiply(_values[k], xc));
                            }
                        }
                        x[c] = xc;
                    }
                }
            }
            else
            {
                if (transpose)
                {
                    for (int r = 0; r < dim; ++r)
                    {
                        int col_start = _columnIndices[r],
                            col_end_inc = _columnIndices[r + 1] - 1;

                        if (_rowIndices[col_end_inc] != r)
                        {
                            while (col_end_inc >= col_start && _rowIndices[col_end_inc] > r)
                            {
                                --col_end_inc;
                            }

                            if (col_end_inc < col_start || _rowIndices[col_end_inc] != r)
                            {
                                //throw new ArgumentOutOfRangeException($"Coefficient matrix is missing the diagonal entry ({r}, {r}).");
                                return false;
                            }
                        }

                        T xr = x[r];
                        for (int i = col_start; i < col_end_inc; ++i)
                        {
                            xr = provider.Subtract(xr, provider.Multiply(_values[i], x[_rowIndices[i]]));
                        }
                        x[r] = provider.Divide(xr, _values[col_end_inc]);
                    }
                }
                else
                {
                    for (int c = dim - 1; c >= 0; --c)
                    {
                        int col_start = _columnIndices[c],
                            col_end_inc = _columnIndices[c + 1] - 1;

                        if (_rowIndices[col_end_inc] != c)
                        {
                            while (col_end_inc >= col_start && _rowIndices[col_end_inc] > c)
                            {
                                --col_end_inc;
                            }

                            if (col_end_inc < col_start || _rowIndices[col_end_inc] != c)
                            {
                                //throw new ArgumentOutOfRangeException($"Coefficient matrix is missing the diagonal entry ({c}, {c}).");
                                return false;
                            }
                        }

                        T xc = provider.Divide(x[c], _values[col_end_inc]);
                        for (int i = col_start; i < col_end_inc; ++i)
                        {
                            int r = _rowIndices[i];
                            x[r] = provider.Subtract(x[r], provider.Multiply(_values[i], xc));
                        }
                        x[c] = xc;
                    }
                }
            }
            return true;
        }


        #region Operations

        public override Matrix<T> Add(Matrix<T> matrix, IProvider<T> provider)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            if (CompareRepresentationPriority(this, matrix) < 0)
            {
                return matrix.Add(this, provider); // return the sum as type(matrix)
            }
            return Add(matrix.ToCSCSparseMatrix(), provider);
        }
        public CSCSparseMatrix<T> Add(CSCSparseMatrix<T> matrix, IProvider<T> provider)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            MatrixChecks.AssertMatrixDimensionsEqual(this, matrix);

            int[] columnIndices = new int[Columns + 1];

            CSRHelper<T>.Add(_columnIndices, _rowIndices, _values,
                matrix._columnIndices, matrix._rowIndices, matrix._values,
                columnIndices, out int[] rowIndices, out T[] values,
                Columns, provider);

            return new CSCSparseMatrix<T>(Rows, columnIndices, rowIndices, values);
        }
        public CSCSparseMatrix<T> Subtract(CSCSparseMatrix<T> matrix, IProvider<T> provider)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            MatrixChecks.AssertMatrixDimensionsEqual(this, matrix);

            int[] columnIndices = new int[Columns + 1];

            CSRHelper<T>.Subtract(_columnIndices, _rowIndices, _values,
                matrix._columnIndices, matrix._rowIndices, matrix._values,
                columnIndices, out int[] rowIndices, out T[] values,
                Columns, provider);

            return new CSCSparseMatrix<T>(Rows, columnIndices, rowIndices, values);
        }
        public void NegateInPlace(IDenseBLAS1Provider<T> blas)
        {
            if (!(blas is IDenseBLAS1<T> b))
            {
                throw new NotSupportedException(ExceptionMessages.TypeNotSupported);
            }
            b.SCAL(_values, b.Provider.Negate(b.Provider.One), 0, _values.Length);
        }
        public void NegateInPlace(IProvider<T> provider)
        {
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            int dim = _values.Length, i;
            for (i = 0; i < dim; ++i)
            {
                _values[i] = provider.Negate(_values[i]);
            }
        }
        public CSCSparseMatrix<T> Negate(IDenseBLAS1Provider<T> blas)
        {
            CSCSparseMatrix<T> copy = Copy();
            copy.NegateInPlace(blas);
            return copy;
        }
        public CSCSparseMatrix<T> Negate(IProvider<T> provider)
        {
            CSCSparseMatrix<T> copy = Copy();
            copy.NegateInPlace(provider);
            return copy;
        }

        public CSCSparseMatrix<T> Multiply(T scalar, IProvider<T> provider)
        {
            CSCSparseMatrix<T> copy = Copy();
            copy.MultiplyInPlace(scalar, provider);
            return copy;
        }
        public CSCSparseMatrix<T> Multiply(T scalar, IDenseBLAS1Provider<T> blas)
        {
            CSCSparseMatrix<T> copy = Copy();
            copy.MultiplyInPlace(scalar, blas);
            return copy;
        }
        public override void MultiplyInPlace(T scalar, IProvider<T> provider)
        {
            MultiplyInPlace(scalar, new DefaultNativeProvider<T>(provider));
        }
        public void MultiplyInPlace(T scalar, IDenseBLAS1Provider<T> blas)
        {
            if (scalar is null)
            {
                throw new ArgumentNullException(nameof(scalar));
            }
            if (!(blas is IDenseBLAS1<T> b))
            {
                throw new NotSupportedException(ExceptionMessages.TypeNotSupported);
            }
            b.SCAL(_values, scalar, 0, _values.Length);
        }

        internal override void Multiply(T[] vector, T[] product, IProvider<T> provider, bool increment)
        {
            if (vector is null)
            {
                throw new ArgumentNullException(nameof(vector));
            }
            if (product is null)
            {
                throw new ArgumentNullException(nameof(product));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            if (vector.Length != Columns)
            {
                throw new InvalidOperationException("The number of columns of this matrix does not match the dimension of the vector.");
            }
            if (product.Length < Rows)
            {
                throw new ArgumentOutOfRangeException("The result vector is not large enough to store the matrix-vector product.");
            }

            CSRHelper<T>.TransposeAndMultiply(vector, product, provider, _columnIndices, _rowIndices, _values, Columns, Rows, increment);
        }
        internal override void TransposeAndMultiply(T[] vector, T[] product, IProvider<T> provider, bool increment)
        {
            if (vector is null)
            {
                throw new ArgumentNullException(nameof(vector));
            }
            if (product is null)
            {
                throw new ArgumentNullException(nameof(product));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            if (vector.Length != Rows)
            {
                throw new InvalidOperationException("The number of rows of this matrix does not match the dimension of the vector.");
            }
            if (product.Length < Columns)
            {
                throw new ArgumentOutOfRangeException("The result vector is not large enough to store the matrix-vector product.");
            }

            CSRHelper<T>.Multiply(vector, product, provider, _columnIndices, _rowIndices, _values, Columns, increment);
        }

        public override SparseVector<T> Multiply(SparseVector<T> vector, IProvider<T> provider)
        {
            if (vector is null)
            {
                throw new ArgumentNullException(nameof(vector));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            if (vector.Dimension != Columns)
            {
                throw new InvalidOperationException("The number of columns of this matrix does not match the dimension of the vector.");
            }
            return CSRHelper<T>.TransposeAndMultiply(vector, provider, _columnIndices, _rowIndices, _values, Rows, Columns);
        }
        /// <summary>
        /// Returns $A^Tv$ for sparse $v$.
        /// </summary>
        /// <param name="vector"></param>
        /// <param name="provider"></param>
        /// <returns></returns>
        public override SparseVector<T> TransposeAndMultiply(SparseVector<T> vector, IProvider<T> provider)
        {
            if (vector is null)
            {
                throw new ArgumentNullException(nameof(vector));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            if (vector.Dimension != Rows)
            {
                throw new InvalidOperationException("The number of rows of the matrix does not match the size of the vector.");
            }

            return CSRHelper<T>.Multiply(vector, provider, _columnIndices, _rowIndices, _values, Columns);
        }

        public CSCSparseMatrix<T> Multiply(CSCSparseMatrix<T> matrix, ISparseBLAS1Provider<T> blas, MultiplicationMode mode = MultiplicationMode.OPTIMIZE_SPEED)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            if (blas is null)
            {
                throw new ArgumentNullException(nameof(blas));
            }
            if (!(blas is ISparseBLAS1<T> b))
            {
                throw new NotSupportedException(ExceptionMessages.TypeNotSupported);
            }

            MatrixChecks.AssertMatrixDimensionsForMultiplication(this, matrix);

            if (mode == MultiplicationMode.OPTIMIZE_SPEED)
            {
                //return Multiply_OptimizeSpeed(matrix, provider);
                //return Multiply_OptimizeSpeed1(matrix, b);
                //return Multiply_OptimizeSpeed2(matrix, b);
                return Multiply_OptimizeSpeed3(matrix, b);
            }
            else
            {
                return Multiply_OptimizeMemory(matrix, b.Provider);
            }
        }

        /// <summary>
        /// Multiply two sparse matrices, optimizing for memory usage
        /// There are two ways in which memory is conserved:
        /// - precounting the nnz in the product
        /// - storing the temporary vector as a dictionary, which is O(column nnz) as opposed to O(m)
        /// </summary>
        private CSCSparseMatrix<T> Multiply_OptimizeMemory(CSCSparseMatrix<T> matrix, IProvider<T> provider)
        {
            int bcol = matrix.Columns;

            int[] prod_colIndices = new int[bcol + 1];

            // Count the nnz in the product matrix - this is a difficult routine to keep efficient, however 
            // since we are optimizing for memory, we perform the count to avoid having to convert a list into 
            // an array at the end 
            int count = 0;
            HashSet<int> column_rowIndices = new HashSet<int>();
            for (int c = 0; c < bcol; ++c)
            {
                int b_col_end = matrix._columnIndices[c + 1];
                column_rowIndices.Clear();

                for (int i = matrix._columnIndices[c]; i < b_col_end; ++i)
                {
                    int k = matrix._rowIndices[i],
                        a_col_end = _columnIndices[k + 1];

                    for (int j = _columnIndices[k]; j < a_col_end; ++j)
                    {
                        int r = _rowIndices[j];
                        if (!column_rowIndices.Contains(r))
                        {
                            column_rowIndices.Add(r);
                        }
                    }
                }
                count += column_rowIndices.Count;
            }

            // Storage for the product matrix
            int[] prod_rowIndices = new int[count];
            T[] prod_values = new T[count];
            int t = 0;

            // Used to temporarily hold a column of the computed product 
            Dictionary<int, T> column = new Dictionary<int, T>();
            for (int c = 0; c < bcol; ++c)
            {
                int b_col_start = matrix._columnIndices[c],
                    b_col_end = matrix._columnIndices[c + 1];

                column.Clear();

                // For each element in the non-zero pattern of the c-th column of B...
                for (int i = b_col_start; i < b_col_end; ++i)
                {
                    int k = matrix._rowIndices[i];

                    // Loop through column k of matrix A
                    int a_col_start = _columnIndices[k],
                        a_col_end = _columnIndices[k + 1];

                    T value = matrix._values[i];
                    for (int j = a_col_start; j < a_col_end; ++j)
                    {
                        int r = _rowIndices[j];
                        if (column.ContainsKey(r))
                        {
                            column[r] = provider.Add(column[r], provider.Multiply(_values[j], value));
                        }
                        else
                        {
                            column[r] = provider.Multiply(_values[j], value);
                        }
                    }
                }

                int start = t;
                foreach (KeyValuePair<int, T> entry in column)
                {
                    prod_rowIndices[t] = entry.Key;
                    prod_values[t] = entry.Value;
                    ++t;
                }

                // Sort the row indices in order within this column
                Array.Sort(prod_rowIndices, prod_values, start, t - start);
                prod_colIndices[c + 1] = t;
            }
            return new CSCSparseMatrix<T>(Rows, prod_colIndices, prod_rowIndices, prod_values);
        }
        /// <summary>
        /// Slightly faster but more memory hungry - see the optimize memory method for details
        /// </summary>
        /// <param name="matrix"></param>
        /// <param name="provider"></param>
        /// <returns></returns>
        private CSCSparseMatrix<T> Multiply_OptimizeSpeed(CSCSparseMatrix<T> matrix, IProvider<T> provider)
        {
            int bcol = matrix.Columns;

            int[] prod_colIndices = new int[bcol + 1];
            List<int> prod_rowIndices = new List<int>();
            List<T> prod_values = new List<T>();

            // Used to temporarily hold a column of the computed product 
            T[] column = new T[Rows];
            if (!_zero.Equals(default))
            {
                for (int i = 0; i < Rows; ++i)
                {
                    column[i] = _zero;
                }
            }
            // Stores the index of the last column containing an nz element in this row
            int[] row_present = new int[Rows];

            // Stores the non-zero indices so we don't have to loop through row_present
            int[] nz_rowIndices = new int[Rows];
            // The number of non-zero in the temporary column
            int row_nnz;

            for (int c = 0; c < bcol; ++c)
            {
                int b_col_start = matrix._columnIndices[c],
                    b_col_end = matrix._columnIndices[c + 1];

                row_nnz = 0;

                // For each element in the non-zero pattern of the c-th column of B...
                for (int i = b_col_start; i < b_col_end; ++i)
                {
                    int k = matrix._rowIndices[i];

                    // Loop through column k of matrix A
                    int a_col_start = _columnIndices[k],
                        a_col_end = _columnIndices[k + 1];

                    T value = matrix._values[i];
                    for (int j = a_col_start; j < a_col_end; ++j)
                    {
                        int r = _rowIndices[j];
                        if (row_present[r] <= c)
                        {
                            row_present[r] = c + 1;
                            column[r] = provider.Multiply(_values[j], value);
                            nz_rowIndices[row_nnz++] = r;
                        }
                        else
                        {
                            column[r] = provider.Add(column[r], provider.Multiply(_values[j], value));
                        }
                    }
                }

                Array.Sort(nz_rowIndices, 0, row_nnz);

                for (int i = 0; i < row_nnz; ++i)
                {
                    int r = nz_rowIndices[i];
                    if (!provider.Equals(column[r], _zero))
                    {
                        prod_rowIndices.Add(r);
                        prod_values.Add(column[r]);
                    }

                    // Clear the column for the next iteration of the loop
                    column[r] = _zero;
                }

                prod_colIndices[c + 1] = prod_values.Count;
            }

            return new CSCSparseMatrix<T>(Rows, prod_colIndices, prod_rowIndices.ToArray(), prod_values.ToArray());
        }
        /// <summary>
        /// This method is similar to Multiply_OptimizeSpeed except it precomputes the number of non-zeroes 
        /// and only initializes the storage once (instead of using lists)
        /// </summary>
        /// <param name="matrix"></param>
        /// <param name="blas"></param>
        /// <returns></returns>
        private CSCSparseMatrix<T> Multiply_OptimizeSpeed1(CSCSparseMatrix<T> matrix, ISparseBLAS1<T> blas)
        {
            int m = Rows, p = matrix.Columns;

            // Used to temporarily hold a column of the computed product 
            T[] column = new T[m];

            // Stores the index of the last column containing an nz element in this row
            int[] in_column = new int[m];
            int membership_threshold = 1;

            // Stores the non-zero indices so we don't have to loop through row_present
            int[] nz_rowIndices = new int[m];

            // Count the number of symbolically nz entries
            int nnz = 0;
            for (int c = 0; c < p; ++c)
            {
                // Get the nonzero pattern of column c of B
                int end = matrix._columnIndices[c + 1];
                for (int j = matrix._columnIndices[c]; j < end; ++j)
                {
                    int col = matrix._rowIndices[j], 
                        A_col_end = _columnIndices[col + 1];
                    for (int i = _columnIndices[col]; i < A_col_end; ++i)
                    {
                        int row = _rowIndices[i];
                        if (in_column[row] < membership_threshold)
                        {
                            in_column[row] = membership_threshold;
                            ++nnz;
                        }
                    }
                }

                ++membership_threshold;

                // Check overflow
                if (membership_threshold == int.MaxValue)
                {
                    Array.Clear(in_column, 0, m);
                    membership_threshold = 1;
                }
            }

            // Storage for the product matrix
            int[] prod_colIndices = new int[p + 1];
            int[] prod_rowIndices = new int[nnz];
            T[] prod_values = new T[nnz];
            int z = 0;

            // The number of non-zero in the temporary column
            int row_nnz;

            for (int c = 0; c < p; ++c)
            {
                int b_col_start = matrix._columnIndices[c],
                    b_col_end = matrix._columnIndices[c + 1];

                row_nnz = 0;

                // For each element in the non-zero pattern of the c-th column of B...
                for (int i = b_col_start; i < b_col_end; ++i)
                {
                    int k = matrix._rowIndices[i];

                    // Loop through column k of matrix A
                    row_nnz = blas.AXPY(nz_rowIndices, column, _rowIndices, _values, matrix._values[i],
                        _columnIndices[k], _columnIndices[k + 1], row_nnz, in_column, membership_threshold);
                }

                Array.Sort(nz_rowIndices, 0, row_nnz);

                for (int i = 0; i < row_nnz; ++i)
                {
                    int r = nz_rowIndices[i];
                    prod_rowIndices[z] = r;
                    prod_values[z] = column[r];
                    ++z;
                }

                // Reset membership and check overflow
                ++membership_threshold;
                if (membership_threshold == int.MaxValue)
                {
                    Array.Clear(in_column, 0, m);
                    membership_threshold = 1;
                }

                prod_colIndices[c + 1] = z;
            }

            return new CSCSparseMatrix<T>(Rows, prod_colIndices, prod_rowIndices, prod_values);
        }
        /// <summary>
        /// This implementation avoids the log(k) complexity factor by inserting the computed product entries
        /// by row, then performing a transposition. Otherwise method is similar to Multiply_OptimizeSpeed1
        /// </summary>
        /// <param name="matrix"></param>
        /// <param name="blas"></param>
        /// <returns></returns>
        private CSCSparseMatrix<T> Multiply_OptimizeSpeed2(CSCSparseMatrix<T> matrix, ISparseBLAS1<T> blas)
        {
            int m = Rows, p = matrix.Columns;

            // Used to temporarily hold a column of the computed product 
            T[] column = new T[m];

            // Stores the index of the last column containing an nz element in this row
            int[] in_column = new int[m];
            int membership_threshold = 1;

            // Stores the non-zero indices so we don't have to loop through row_present
            int[] nz_rowIndices = new int[m];

            // The number of non-zero in the temporary column
            int row_nnz;

            // Get the symbolic row count of the product
            int[] rowIndices = new int[m + 1];
            for (int c = 0; c < p; ++c)
            {
                // Get the nonzero pattern of column c of B
                int end = matrix._columnIndices[c + 1];
                for (int j = matrix._columnIndices[c]; j < end; ++j)
                {
                    int col = matrix._rowIndices[j];
                    int A_col_end = _columnIndices[col + 1];
                    for (int i = _columnIndices[col]; i < A_col_end; ++i)
                    {
                        int row = _rowIndices[i];
                        if (in_column[row] < membership_threshold)
                        {
                            in_column[row] = membership_threshold;
                            rowIndices[row]++;
                        }
                    }
                }
                ++membership_threshold;

                // Check overflow
                if (membership_threshold == int.MaxValue)
                {
                    Array.Clear(in_column, 0, m);
                    membership_threshold = 1;
                }
            }

            // Shift row indices
            for (int r = m - 1; r >= 0; --r)
            {
                rowIndices[r + 1] = rowIndices[r];
            }
            rowIndices[0] = 0;
            // Accumulate row indices
            for (int r = 0; r < m; ++r)
            {
                rowIndices[r + 1] += rowIndices[r];
            }

            // Next array for populating the product transpose
            int[] next = new int[m];
            Array.Copy(rowIndices, next, m);

            // Prod will be stored by row
            int[] prod_t_colIndices = new int[rowIndices[m]];
            T[] prod_t_values = new T[rowIndices[m]];

            for (int c = 0; c < p; ++c)
            {
                int b_col_start = matrix._columnIndices[c],
                    b_col_end = matrix._columnIndices[c + 1];

                row_nnz = 0;

                // For each element in the non-zero pattern of the c-th column of B...
                for (int i = b_col_start; i < b_col_end; ++i)
                {
                    int k = matrix._rowIndices[i];

                    // Loop through column k of matrix A
                    row_nnz = blas.AXPY(nz_rowIndices, column, _rowIndices, _values, matrix._values[i],
                        _columnIndices[k], _columnIndices[k + 1], row_nnz, in_column, membership_threshold);
                }

                for (int i = 0; i < row_nnz; ++i)
                {
                    int r = nz_rowIndices[i];
                    int index = next[r]++;
                    prod_t_colIndices[index] = c;
                    prod_t_values[index] = column[r];
                }

                // Reset membership
                ++membership_threshold;
                if (membership_threshold == int.MaxValue)
                {
                    Array.Clear(in_column, 0, m);
                    membership_threshold = 1;
                }
            }

            // Perform the transpose
            CSRHelper<T>.Transpose(rowIndices, prod_t_colIndices, prod_t_values, 
                out int[] prod_rowIndices, out int[] prod_colIndices, out T[] prod_values, m, p);

            return new CSCSparseMatrix<T>(m, prod_colIndices, prod_rowIndices, prod_values);
        }
        /// <summary>
        /// Using custom sort function with complexity O(n log(p)) where p is the number of partitions,
        /// as opposed to O(n log(n))
        /// </summary>
        /// <param name="matrix"></param>
        /// <param name="blas"></param>
        /// <returns></returns>
        private CSCSparseMatrix<T> Multiply_OptimizeSpeed3(CSCSparseMatrix<T> matrix, ISparseBLAS1<T> blas)
        {
            int m = Rows, p = matrix.Columns;

            // Used to temporarily hold a column of the computed product 
            T[] column = new T[m];

            // Stores the index of the last column containing an nz element in this row
            int[] in_column = new int[m];
            int membership_threshold = 1;

            int nnz = 0,                // Count the number of symbolically nz entries
                col_capacity = 0,       // Count the max number of nz entries per column
                partition_capacity = 0; // Count the max number of partitions per column

            for (int c = 0; c < p; ++c)
            {
                // Get the nonzero pattern of column c of B
                int start = matrix._columnIndices[c],
                    end = matrix._columnIndices[c + 1],
                    len = end - start,
                    // The number of non-zeroes in this column
                    col_nnz = 0;

                // update the required partition size
                if (partition_capacity < len)
                {
                    partition_capacity = len;
                }

                for (int j = start; j < end; ++j)
                {
                    int col = matrix._rowIndices[j],
                        A_col_end = _columnIndices[col + 1];
                    for (int i = _columnIndices[col]; i < A_col_end; ++i)
                    {
                        int row = _rowIndices[i];
                        if (in_column[row] < membership_threshold)
                        {
                            in_column[row] = membership_threshold;
                            ++col_nnz;
                        }
                    }
                }

                nnz += col_nnz;
                if (col_nnz > col_capacity)
                {
                    col_capacity = col_nnz;
                }

                ++membership_threshold;

                // Check overflow
                if (membership_threshold == int.MaxValue)
                {
                    Array.Clear(in_column, 0, m);
                    membership_threshold = 1;
                }
            }

            // Stores the indices of the non-zero entries in the temp column
            int[] nz_rowIndices = new int[col_capacity],
                // workspace required for sorting the nz indices
                nz_rowIndices_ws = new int[col_capacity];

            // Array to store the starting index of each partition within the 
            // set union of a column
            int[] partition_index = new int[partition_capacity + 1];

            // Storage for the product matrix
            int[] prod_colIndices = new int[p + 1];
            int[] prod_rowIndices = new int[nnz];
            T[] prod_values = new T[nnz];

            int z = 0,
                row_nnz,    // The number of non-zero in the temporary column
                partitions; // The number of partitions in the temporary column

            for (int c = 0; c < p; ++c)
            {
                int b_col_start = matrix._columnIndices[c],
                    b_col_end = matrix._columnIndices[c + 1];

                row_nnz = 0;
                partitions = b_col_end - b_col_start;

                // For each element in the non-zero pattern of the c-th column of B...
                for (int i = b_col_start; i < b_col_end; ++i)
                {
                    // Record the partition index 
                    partition_index[i - b_col_start] = row_nnz;

                    int k = matrix._rowIndices[i];

                    // Loop through column k of matrix A
                    row_nnz = blas.AXPY(nz_rowIndices, column, _rowIndices, _values, matrix._values[i],
                        _columnIndices[k], _columnIndices[k + 1], row_nnz, in_column, membership_threshold);
                }
                partition_index[partitions] = row_nnz;

                // Sort the nz_rowIndices array
                // Each block in the range [partition_index[i], partition_index[i + 1]]
                // is sorted, so we do a merge sort from the last partition (since the 
                // partitions are likely to get smaller in size in real matrices).
                // This sorting method is O(n log(p)) versus O(n log n) using a traditional
                // sorting algorithn, where p is the number of partitions. Since p <= n, 
                // this method is at least as fast (asymptotically) as a traditional sorting
                // algorithm, however for most sparse (in particular real-world) matrices, 
                // p << n. 
                PartitionSort(nz_rowIndices, nz_rowIndices_ws, partition_index, partitions);

                for (int i = 0; i < row_nnz; ++i)
                {
                    int r = nz_rowIndices[i];
                    prod_rowIndices[z] = r;
                    prod_values[z] = column[r];
                    ++z;
                }

                // Reset membership and check overflow
                ++membership_threshold;
                if (membership_threshold == int.MaxValue)
                {
                    Array.Clear(in_column, 0, m);
                    membership_threshold = 1;
                }

                prod_colIndices[c + 1] = z;
            }

            return new CSCSparseMatrix<T>(Rows, prod_colIndices, prod_rowIndices, prod_values);
        }

        /// <summary>
        /// Perform a sort of the indices array, given that each partitioned block is sorted
        /// </summary>
        /// <param name="indices"></param>
        /// <param name="partitions"></param>
        /// <param name="n"></param>
        internal static void PartitionSort(int[] indices, int[] workspace, int[] partitions, int n)
        {
            const int MAX = int.MaxValue;

            // Incrementally reduce the number of partitions
            // until there is only one left
            while (n > 1)
            {
                int j = 1, i = 2;
                for (; i <= n; i += 2)
                {
                    // Sort the p-th partition and the q-th partition
                    int p_start = partitions[i - 2],
                        p_end = partitions[i - 1],
                        q_start = p_end,
                        q_end = partitions[i];

                    int k = 0;
                    int p = p_start, q = q_start;
                    while (p < p_end || q < q_end)
                    {
                        int pv = p < p_end ? indices[p] : MAX,
                            qv = q < q_end ? indices[q] : MAX;

                        if (pv > qv)
                        {
                            workspace[k] = qv;
                            ++q;
                        }
                        else
                        {
                            workspace[k] = pv;
                            ++p;
                        }
                        ++k;
                    }

                    Array.Copy(workspace, 0, indices, p_start, k);

                    // Update the partition array (p and q are merged)
                    partitions[j++] = q_end;
                }

                // Shift the odd index
                if ((i - n) % 2 == 1)
                {
                    partitions[j] = partitions[n];
                }
                n -= (j - 1);
            }
        }
        /// <summary>
        /// An alternative implementation of partition sort that uses 3-compare instead of 2-compare
        /// hoping for better cache efficiency
        /// </summary>
        internal static void PartitionSort2(int[] indices, int[] workspace, int[] partitions, int n)
        {
            // Incrementally reduce the number of partitions
            // until there is only one left
            while (n > 2)
            {
                int j = 1, i = 3;
                for (; i <= n; i += 3)
                {
                    // Sort the p-th partition and the q-th partition
                    int p_start = partitions[i - 3],
                        p_end = partitions[i - 2],
                        q_start = p_end,
                        q_end = partitions[i - 1],
                        r_start = q_end,
                        r_end = partitions[i];

                    int k = 0;
                    int p = p_start, q = q_start, r = r_start;
                    while (p < p_end || q < q_end || r < r_end)
                    {
                        int pv = p < p_end ? indices[p] : int.MaxValue;
                        int qv = q < q_end ? indices[q] : int.MaxValue;
                        int rv = r < r_end ? indices[r] : int.MaxValue;

                        if (pv > qv)
                        {
                            if (qv > rv)
                            {
                                workspace[k] = rv;
                                ++r;
                            }
                            else
                            {
                                workspace[k] = qv;
                                ++q;
                            }
                        }
                        else
                        {
                            if (pv > rv)
                            {
                                workspace[k] = rv;
                                ++r;
                            }
                            else
                            {
                                workspace[k] = pv;
                                ++p;
                            }
                        }
                        ++k;
                    }

                    Array.Copy(workspace, 0, indices, p_start, k);

                    // Update the partition array (p and q are merged)
                    partitions[j++] = r_end;
                }

                // Shift the odd index
                if ((i - n) % 3 != 0)
                {
                    partitions[j] = partitions[n];
                }
                n -= 2 * (j - 1);
            }

            // Requires a 2-sort
            if (n > 1)
            {
                int j = 1, i = 2;
                for (; i <= n; i += 2)
                {
                    // Sort the p-th partition and the q-th partition
                    int p_start = partitions[i - 2],
                        p_end = partitions[i - 1],
                        q_start = p_end,
                        q_end = partitions[i];

                    int k = 0;
                    int p = p_start, q = q_start;
                    while (p < p_end || q < q_end)
                    {
                        int pv = p < p_end ? indices[p] : int.MaxValue;
                        int qv = q < q_end ? indices[q] : int.MaxValue;

                        if (pv > qv)
                        {
                            workspace[k] = qv;
                            ++q;
                        }
                        else
                        {
                            workspace[k] = pv;
                            ++p;
                        }
                        ++k;
                    }

                    Array.Copy(workspace, 0, indices, p_start, k);
                }
            }
        }

        /// <summary>
        /// Use the dot product method to calculate A' * B. For a <m, n, p> problem, this method is fast (and memory-conservative) for m, p << n
        /// </summary>
        private CSCSparseMatrix<T> TransposeAndMultiply_DotProductMethod(CSCSparseMatrix<T> matrix, IProvider<T> provider)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            if (Rows != matrix.Rows)
            {
                throw new ArgumentOutOfRangeException("The number of rows of this matrix does not match the number of rows of the second matrix.");
            }

            int[] prod_colIndices = new int[matrix.Columns + 1];
            List<int> prod_rowIndices = new List<int>();
            List<T> prod_values = new List<T>();

            for (int c = 0; c < matrix.Columns; ++c)
            {
                int bi = matrix._columnIndices[c],
                    B_end = matrix._columnIndices[c + 1];

                // Empty row
                if (bi == B_end)
                {
                    continue;
                }

                for (int r = 0; r < Columns; ++r)
                {
                    int ai = _columnIndices[r],
                        A_end = _columnIndices[r + 1];

                    // Empty row
                    if (ai == A_end)
                    {
                        continue;
                    }

                    T dot = provider.Zero;
                    bool any = false;

                    while (ai < A_end || bi < B_end)
                    {
                        int aIndex = ai < A_end ? _rowIndices[ai] : int.MaxValue;
                        int bIndex = bi < B_end ? matrix._rowIndices[bi] : int.MaxValue;

                        if (aIndex > bIndex)
                        {
                            ++bi;
                        }
                        else if (aIndex < bIndex)
                        {
                            ++ai;
                        }
                        else
                        {
                            dot = provider.Add(dot, provider.Multiply(_values[ai], matrix._values[bi]));
                            any = true;
                            ++bi;
                            ++ai;
                        }
                    }

                    if (any)
                    {
                        prod_values.Add(dot);
                        prod_rowIndices.Add(r);
                    }
                }

                prod_colIndices[c + 1] = prod_values.Count;
            }

            return new CSCSparseMatrix<T>(Columns, prod_colIndices, prod_rowIndices.ToArray(), prod_values.ToArray());
        }

        public CSCSparseMatrix<T> Multiply(CSCSparseMatrix<T> matrix, IProvider<T> provider, int maxDegreesOfParallelism)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            MatrixChecks.AssertMatrixDimensionsForMultiplication(this, matrix);

            int bcol = matrix.Columns;

            int[] prod_colIndices = new int[bcol + 1];
            List<int> prod_rowIndices = new List<int>();
            List<T> prod_values = new List<T>();

            // Used to temporarily hold a column of the computed product 
            Dictionary<int, T>[] column_buffs = new Dictionary<int, T>[maxDegreesOfParallelism];
            for (int i = 0; i < maxDegreesOfParallelism; ++i)
            {
                column_buffs[i] = new Dictionary<int, T>();
            }

            TaskFactory<List<int>> factory = Task<List<int>>.Factory;

            for (int j = 0; j < bcol; j += maxDegreesOfParallelism)
            {
                int j_end = Math.Min(bcol, j + maxDegreesOfParallelism);

                List<Task<List<int>>> tasks = new List<Task<List<int>>>();
                for (int _c = j; _c < j_end; ++_c)
                {
                    // Save c <- _c
                    int c = _c;

                    tasks.Add(factory.StartNew(() =>
                    {
                        int b_col_start = matrix._columnIndices[c],
                        b_col_end = matrix._columnIndices[c + 1];

                        Dictionary<int, T> column = column_buffs[c - j];
                        column.Clear();

                        // For each element in the non-zero pattern of the c-th column of B...
                        for (int i = b_col_start; i < b_col_end; ++i)
                        {
                            int k = matrix._rowIndices[i];

                            // Loop through column k of matrix A
                            int a_col_start = _columnIndices[k],
                                a_col_end = _columnIndices[k + 1];

                            T value = matrix._values[i];
                            for (int j = a_col_start; j < a_col_end; ++j)
                            {
                                int r = _rowIndices[j];
                                if (column.ContainsKey(r))
                                {
                                    column[r] = provider.Add(column[r], provider.Multiply(_values[j], value));
                                }
                                else
                                {
                                    column[r] = provider.Multiply(_values[j], value);
                                }
                            }
                        }

                        List<int> row_indices = new List<int>(column.Keys);
                        row_indices.Sort();

                        return row_indices;
                    }));
                }

                Task.WaitAll(tasks.ToArray());

                for (int k = j; k < j_end; ++k)
                {
                    List<int> row_indices = tasks[k - j].Result;
                    Dictionary<int, T> column = column_buffs[k - j];

                    for (int i = 0; i < row_indices.Count; ++i)
                    {
                        int r = row_indices[i];
                        if (!provider.Equals(column[r], _zero))
                        {
                            prod_rowIndices.Add(r);
                            prod_values.Add(column[r]);
                        }
                    }
                    prod_colIndices[k + 1] = prod_values.Count;
                }
            }

            return new CSCSparseMatrix<T>(Rows, prod_colIndices, prod_rowIndices.ToArray(), prod_values.ToArray());
        }

        public CSCSparseMatrix<T> Divide(T scalar, IProvider<T> provider)
        {
            CSCSparseMatrix<T> copy = Copy();
            copy.DivideInPlace(scalar, provider);
            return copy;
        }
        public void DivideInPlace(T scalar, IDenseBLAS1Provider<T> blas)
        {
            if (!(blas is IDenseBLAS1<T> b))
            {
                throw new NotSupportedException(ExceptionMessages.TypeNotSupported);
            }
            MultiplyInPlace(b.Provider.Divide(b.Provider.One, scalar), blas);
        }
        public CSCSparseMatrix<T> Divide(T scalar, IDenseBLAS1Provider<T> blas)
        {
            CSCSparseMatrix<T> copy = Copy();
            copy.DivideInPlace(scalar, blas);
            return copy;
        }

        #endregion


        #region Convertors 

        public override BandMatrix<T> ToBandMatrix()
        {
            return new BandMatrix<T>(this);
        }

        public override BlockDiagonalMatrix<T> ToBlockDiagonalMatrix()
        {
            return new BlockDiagonalMatrix<T>(this);
        }

        public override COOSparseMatrix<T> ToCOOSparseMatrix()
        {
            return new COOSparseMatrix<T>(this);
        }

        public override CSCSparseMatrix<T> ToCSCSparseMatrix()
        {
            return this;
        }

        public override CSRSparseMatrix<T> ToCSRSparseMatrix()
        {
            return new CSRSparseMatrix<T>(this);
        }

        public override DenseMatrix<T> ToDenseMatrix()
        {
            return new DenseMatrix<T>(this);
        }

        public override DiagonalMatrix<T> ToDiagonalMatrix()
        {
            return new DiagonalMatrix<T>(this);
        }

        public override DOKSparseMatrix<T> ToDOKSparseMatrix()
        {
            return new DOKSparseMatrix<T>(this);
        }

        public override MCSRSparseMatrix<T> ToMCSRSparseMatrix()
        {
            return new MCSRSparseMatrix<T>(this);
        }

        public override SKYSparseMatrix<T> ToSKYSparseMatrix()
        {
            return new SKYSparseMatrix<T>(this);
        }
        #endregion


        #region Decompositions

        /// <summary>
        /// Analyze this matrix for a Cholesky decomposition.
        /// </summary>
        /// <returns>The symbolic analysis.</returns>
        public SymbolicCholesky CholeskyAnalysis()
        {
            CSCSymbolic<T> cholesky = new CSCSymbolic<T>();
            return cholesky.AnalyseCholesky(this);
        }

        public CSCSparseMatrix<T> CholeskyFactor(SymbolicCholesky analysis, IDenseBLAS1Provider<T> blas1)
        {
            if (analysis is null)
            {
                throw new ArgumentNullException(nameof(analysis));
            }
            if (blas1 is null)
            {
                throw new ArgumentNullException(nameof(blas1));
            }
            if (!(blas1 is IDenseBLAS1<T> b))
            {
                throw new NotSupportedException(ExceptionMessages.TypeNotSupported);
            }

            // There is an option to use AVX2 but unfortunately is not supported yet for .NET Framework 4.5
            // This kernel has a ~30% performance penalty (even when using AVX2 as its base kernel)
            ICSCCholeskyTriangularSolveKernel<T> kernel = new Blas1CSCCholeskyTriangularSolveKernel2<T>(analysis, b);
            CSCCholesky<T> cholesky = new CSCCholesky<T>();
            return cholesky.CholUp2(this, analysis, b.Provider, kernel);
        }

        public CSCSparseMatrix<T> CholeskyFactor(IDenseBLAS1Provider<T> blas1)
        {
            return CholeskyFactor(CholeskyAnalysis(), blas1);
        }

        public Cholesky<T> Cholesky(SymbolicCholesky analysis, IDenseBLAS1Provider<T> blas1)
        {
            if (blas1 is null)
            {
                throw new ArgumentNullException(nameof(blas1));
            }
            if (!(blas1 is IDenseBLAS1<T> b))
            {
                throw new NotSupportedException(ExceptionMessages.TypeNotSupported);
            }
            return new Cholesky<T>(CholeskyFactor(analysis, b), b);
        }

        public Cholesky<T> Cholesky(IDenseBLAS1Provider<T> blas1)
        {
            if (blas1 is null)
            {
                throw new ArgumentNullException(nameof(blas1));
            }
            if (!(blas1 is IDenseBLAS1<T> b))
            {
                throw new NotSupportedException(ExceptionMessages.TypeNotSupported);
            }
            return new Cholesky<T>(CholeskyFactor(CholeskyAnalysis(), b), b);
        }

        #endregion


        #region Operators 

        public static CSCSparseMatrix<T> operator +(CSCSparseMatrix<T> A, CSCSparseMatrix<T> B)
        {
            if (A is null)
            {
                throw new ArgumentNullException(nameof(A));
            }
            return A.Add(B, ProviderFactory.GetDefaultProvider<T>());
        }
        public static CSCSparseMatrix<T> operator *(CSCSparseMatrix<T> A, CSCSparseMatrix<T> B)
        {
            if (A is null)
            {
                throw new ArgumentNullException(nameof(A));
            }
            return A.Multiply(B, ProviderFactory.GetDefaultSparseBLAS1<T>());
        }
        public static DenseVector<T> operator *(CSCSparseMatrix<T> A, DenseVector<T> v)
        {
             if (A is null)
            {
                throw new ArgumentNullException(nameof(A));
            }
            return A.Multiply(v, ProviderFactory.GetDefaultProvider<T>());
        }
        public static SparseVector<T> operator *(CSCSparseMatrix<T> A, SparseVector<T> v)
        {
            if (A is null)
            {
                throw new ArgumentNullException(nameof(A));
            }
            return A.Multiply(v, ProviderFactory.GetDefaultProvider<T>());
        }
        public static CSCSparseMatrix<T> operator *(CSCSparseMatrix<T> A, T scalar)
        {
            if (A is null)
            {
                throw new ArgumentNullException(nameof(A));
            }
            return A.Multiply(scalar, ProviderFactory.GetDefaultBLAS1<T>());
        }
        public static CSCSparseMatrix<T> operator *(T scalar, CSCSparseMatrix<T> A) => A * scalar;
        public static CSCSparseMatrix<T> operator -(CSCSparseMatrix<T> A, CSCSparseMatrix<T> B)
        {
            if (A is null)
            {
                throw new ArgumentNullException(nameof(A));
            }
            return A.Subtract(B, ProviderFactory.GetDefaultProvider<T>());
        }
        public static CSCSparseMatrix<T> operator -(CSCSparseMatrix<T> A)
        {
            if (A is null)
            {
                throw new ArgumentNullException(nameof(A));
            }
            return A.Negate(ProviderFactory.GetDefaultBLAS1<T>());
        }
        public static CSCSparseMatrix<T> operator /(CSCSparseMatrix<T> A, T scalar)
        {
            if (A is null)
            {
                throw new ArgumentNullException(nameof(A));
            }
            return A.Divide(scalar, ProviderFactory.GetDefaultBLAS1<T>());
        }

        #endregion


        #region Casts

        public static explicit operator CSCSparseMatrix<T>(CSRSparseMatrix<T> matrix) => new CSCSparseMatrix<T>(matrix);
        public static explicit operator CSCSparseMatrix<T>(DOKSparseMatrix<T> matrix) => new CSCSparseMatrix<T>(matrix);
        public static explicit operator CSCSparseMatrix<T>(COOSparseMatrix<T> matrix) => new CSCSparseMatrix<T>(matrix);
        public static explicit operator CSCSparseMatrix<T>(DenseMatrix<T> matrix) => new CSCSparseMatrix<T>(matrix);
        public static explicit operator CSCSparseMatrix<T>(BandMatrix<T> matrix) => new CSCSparseMatrix<T>(matrix);
        public static explicit operator CSCSparseMatrix<T>(DiagonalMatrix<T> matrix) => new CSCSparseMatrix<T>(matrix);

        #endregion
    }
}
