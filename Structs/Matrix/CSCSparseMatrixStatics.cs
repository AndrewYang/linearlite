﻿using LinearNet.Global;
using LinearNet.Helpers;
using LinearNet.IO;
using System;
using System.Collections.Generic;

namespace LinearNet.Structs
{
    public static class CSCSparseMatrix
    {
        public static CSCSparseMatrix<T> Random<T>(int rows, int columns, double approxDensity, Func<T> random = null, int seed = -1) where T : new()
        {
            if (rows <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(rows), ExceptionMessages.NonPositiveNotAllowed);
            }
            if (columns <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(columns), ExceptionMessages.NonPositiveNotAllowed);
            }
            if (approxDensity < 0.0 || approxDensity > 1.0)
            {
                throw new ArgumentOutOfRangeException(nameof(approxDensity), ExceptionMessages.NotBetween0And1);
            }

            if (random is null)
            {
                random = DefaultGenerators.GetRandomGenerator<T>();
            }

            int[] columnIndices = new int[columns + 1];
            List<int> rowIndices = new List<int>();
            List<T> values = new List<T>();

            // This is a O(mn) implementation, which is terrible when nnz << mn (which is almost always the case)
            // Change this later to be faster for very sparse matrices.

            Random rand = seed >= 0 ? new Random(seed) : new Random();
            for (int c = 0; c < columns; ++c)
            {
                for (int r = 0; r < rows; ++r)
                {
                    if (rand.NextDouble() < approxDensity)
                    {
                        values.Add(random());
                        rowIndices.Add(r);
                    }
                }
                columnIndices[c + 1] = values.Count;
            }

            return new CSCSparseMatrix<T>(rows, columnIndices, rowIndices.ToArray(), values.ToArray());
        }

        public static CSCSparseMatrix<double> LoadTriplet(string path, char delim = ',', bool raiseNonCriticalExceptions = true, int indexing = 0)
        {
            return LoadTriplet(path, x => double.Parse(x), delim, raiseNonCriticalExceptions, indexing);
        }

        public static CSCSparseMatrix<T> LoadTriplet<T>(string path, Func<string, T> parse, 
            char delim = ',', bool raiseNonCriticalExceptions = true, int indexing = 0) where T : new()
        {
            MatrixReader<T> reader = new MatrixReader<T>();

            int[] columnCounts = null,
                columnIndices = null,
                rowIndices = null;

            T[] values = null;

            int rows = -1, 
                columns = -1;

            int k = 0;
            foreach (IMatrixData data in reader.LoadTriplet(path, parse, delim, raiseNonCriticalExceptions, indexing))
            {
                if (data is MatrixHeader)
                {
                    MatrixHeader header = data as MatrixHeader;
                    rows = header.Rows;
                    columns = header.Columns;
                    columnCounts = new int[columns + 1];
                    columnIndices = new int[header.NNZ];
                    rowIndices = new int[header.NNZ];
                    values = new T[header.NNZ];
                }

                else
                {
                    MatrixElement<T> e = data as MatrixElement<T>;
                    columnCounts[e.Column]++;
                    rowIndices[k] = e.Row;
                    columnIndices[k] = e.Column;
                    values[k] = e.Value;
                    ++k;
                }
            }

            // Shift and accumulate column counts
            for (int c = columns - 1; c >= 0; --c)
            {
                columnCounts[c + 1] = columnCounts[c];
            }
            columnCounts[0] = 0;
            for (int c = 0; c < columns; ++c)
            {
                columnCounts[c + 1] += columnCounts[c];
            }

            // Check if sort order is valid
            if (IsSorted(rowIndices, columnIndices))
            {
                return new CSCSparseMatrix<T>(rows, columnCounts, rowIndices, values);
            }
            else
            {
                // Requires sort - too complicated to do this in-place, create new workspaces 
                int[] sortedRowIndices = new int[rowIndices.Length];
                T[] sortedValues = new T[values.Length];

                // Keep track of the next index for each column
                int[] next = new int[columns];
                Array.Copy(columnCounts, 0, next, 0, columns);

                // Fill the sorted arrays
                for (int i = 0; i < values.Length; ++i)
                {
                    int c = columnIndices[i];
                    int index = next[c];
                    sortedRowIndices[index] = rowIndices[i];
                    sortedValues[index] = values[i];
                    next[c]++;
                }

                // Sort within each column
                for (int c = 0; c < columns; ++c)
                {
                    Array.Sort(sortedRowIndices, sortedValues, columnCounts[c], columnCounts[c + 1]);
                }

                return new CSCSparseMatrix<T>(rows, columnCounts, sortedRowIndices, sortedValues);
            }
        }
        private static bool IsSorted(int[] rowIndices, int[] columnIndices)
        {
            int len = rowIndices.Length;
            for (int i = 1; i < len; ++i)
            {
                if (columnIndices[i - 1] > columnIndices[i])
                {
                    return false;
                }
                if (columnIndices[i - 1] == columnIndices[i] && rowIndices[i - 1] > rowIndices[i])
                {
                    return false;
                }
            }
            return true;
        }
    }
}
