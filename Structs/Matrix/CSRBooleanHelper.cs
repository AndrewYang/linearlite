﻿using System;

namespace LinearNet.Structs
{
    /// <summary>
    /// Static methods that implement useful boolean functions for CSR matrix storage
    /// </summary>
    internal static class CSRBooleanHelper<T> where T : new()
    {
        internal static bool IsBanded(int[] _rowIndices, int[] _columnIndices, T[] _values, int Rows, int lowerBandwidth, int upperBandwidth, Predicate<T> isZero)
        {
            if (isZero is null)
            {
                throw new ArgumentNullException(nameof(isZero));
            }

            for (int r = 0; r < Rows; ++r)
            {
                int end = _rowIndices[r + 1];
                for (int i = _rowIndices[r]; i < end; ++i)
                {
                    int c = _columnIndices[i];
                    if (c > r)
                    {
                        if (c - r > upperBandwidth && !isZero(_values[i]))
                        {
                            return false;
                        }
                    }
                    else
                    {
                        if (r - c > lowerBandwidth && !isZero(_values[i]))
                        {
                            return false;
                        }
                    }
                }
            }

            return true;
        }
    
        internal static bool IsIdentity(int[] _rowIndices, int[] _columnIndices, T[] _values, int Rows, int Columns, Predicate<T> isZero, Predicate<T> isOne)
        {
            if (Rows != Columns)
            {
                return false;
            }

            if (isZero is null)
            {
                throw new ArgumentNullException(nameof(isZero));
            }
            if (isOne is null)
            {
                throw new ArgumentNullException(nameof(isOne));
            }

            for (int r = 0; r < Rows; ++r)
            {
                int row_end = _rowIndices[r + 1];

                bool foundDiagonalTerm = false;
                for (int i = _rowIndices[r]; i < row_end; ++i)
                {
                    // Is diagonal term
                    if (_columnIndices[i] == r)
                    {
                        if (!isOne(_values[i]))
                        {
                            return false;
                        }
                        foundDiagonalTerm = true;
                    }

                    // off-diagonal term
                    else
                    {
                        if (!isZero(_values[i]))
                        {
                            return false;
                        }
                    }
                }

                // No 1 found on this row.
                if (!foundDiagonalTerm)
                {
                    return false;
                }
            }

            return true;
        }

        internal static bool IsDiagonal(int[] _rowIndices, int[] _columnIndices, T[] _values, int Rows, Predicate<T> isZero)
        {
            if (isZero is null)
            {
                throw new ArgumentNullException(nameof(isZero));
            }

            for (int r = 0; r < Rows; ++r)
            {
                int row_end = _rowIndices[r + 1];
                for (int i = _rowIndices[r]; i < row_end; ++i)
                {
                    if (_columnIndices[i] != r && !isZero(_values[i]))
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        internal static bool IsZero(T[] _values, Predicate<T> isZero)
        {
            if (isZero is null)
            {
                throw new ArgumentNullException(nameof(isZero));
            }

            for (int i = 0; i < _values.Length; ++i)
            {
                if (!isZero(_values[i]))
                {
                    return false;
                }
            }
            return true;
        }
    }
}
