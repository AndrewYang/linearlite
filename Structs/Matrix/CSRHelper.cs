﻿using LinearNet.Global;
using LinearNet.Providers;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace LinearNet.Structs
{
    /// <summary>
    /// A collection of methods for CSR and CSC matrix implementations
    /// </summary>
    internal static class CSRHelper<T> where T : new()
    {
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        internal static T Get(int rowIndex, int columnIndex, int[] rowIndices, int[] columnIndices, T[] values, T zero)
        {
            int rowStart = rowIndices[rowIndex], rowEnd = rowIndices[rowIndex + 1];
            int i = BinarySearch(columnIndices, columnIndex, rowStart, rowEnd);
            if (i < 0)
            {
                return zero;
            }
            return values[i];
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        internal static void Set(int rowIndex, int columnIndex, int[] rowIndices, ref int[] columnIndices, ref T[] values, T value)
        {
            int rowStart = rowIndices[rowIndex], rowEnd = rowIndices[rowIndex + 1];
            int index = BinarySearch(columnIndices, columnIndex, rowStart, rowEnd);

            // Fast case - O(log(n)) if the index already exists
            if (index >= 0)
            {
                values[index] = value;
                return;
            }

            // Slow case - O(n) if we need to re-index
            int insertion_index = -1;
            for (int i = rowStart; i < rowEnd; ++i)
            {
                if (columnIndices[i] > columnIndex)
                {
                    insertion_index = i;
                    break;
                }
            }

            // not found - columnIndex > all existing column indices
            if (insertion_index < 0)
            {
                insertion_index = rowEnd;
            }

            // Insert the (columnIndex, value) tuple at 'insertion_index'
            values = values.InsertAt(insertion_index, value);
            columnIndices = columnIndices.InsertAt(insertion_index, columnIndex);

            // All row indices must be shifted
            int len = rowIndices.Length;
            for (int i = rowIndex + 1; i < len; ++i)
            {
                rowIndices[i]++;
            }
        }

        /// <summary>
        /// Find 'value' in values (sorted) using binary search
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static int BinarySearch(int[] values, int value, int start, int end)
        {
            // Try to reduce strain on stack by a couple of levels
            if (end - start < 4)
            {
                for (int i = start; i < end; ++i)
                {
                    if (values[i] == value)
                    {
                        return i;
                    }
                }
                return -1;
            }

            int mid = (start + end) / 2;
            if (value < values[mid])
            {
                return BinarySearch(values, value, start, mid);
            }
            return BinarySearch(values, value, mid, end);
        }

        /// <summary>
        /// Returns whether two rows from two different matrices are equal
        /// </summary>
        internal static bool RowEquals(
            int[] colIndices1, T[] values1, int rowStart1, int rowEnd1,
            int[] colIndices2, T[] values2, int rowStart2, int rowEnd2, T zero, Func<T, T, bool> equals)
        {
            int r1 = rowStart1, r2 = rowStart2;

            while (r1 < rowEnd1 || r2 < rowEnd2)
            {
                int c1 = r1 < rowEnd1 ? colIndices1[r1] : int.MaxValue;
                int c2 = r2 < rowEnd2 ? colIndices2[r2] : int.MaxValue;

                if (c1 > c2)
                {
                    if (!equals(values2[r2], zero))
                    {
                        return false;
                    }
                    ++r2;
                }
                else if (c1 < c2)
                {
                    if (!equals(values1[r1], zero))
                    {
                        return false;
                    }
                    ++r1;
                }

                // c1 = c2, requires comparisons
                else
                {
                    if (!equals(values1[r1], values2[r2]))
                    {
                        return false;
                    }
                    ++r1;
                    ++r2;
                }
            }

            return true;
        }

        /// <summary>
        /// Returns a row of this matrix as a sparse vector.
        /// </summary>
        /// <param name="rowIndex">The index of the row to extract.</param>
        /// <returns>A row of this matrix.</returns>
        internal static BigSparseVector<T> BigSparseRow(int[] rowIndices, int[] columnIndices, T[] values, int rows, int columns, int rowIndex)
        {
            if (rowIndex < 0 || rowIndex >= rows)
            {
                throw new ArgumentOutOfRangeException(nameof(rowIndex));
            }

            // Algorithm is O(n / k) on average...
            Dictionary<long, T> row = new Dictionary<long, T>();
            int row_end = rowIndices[rowIndex + 1];
            for (int r = rowIndices[rowIndex]; r < row_end; ++r)
            {
                row[columnIndices[r]] = values[r];
            }
            return new BigSparseVector<T>(columns, row);
        }
        /// <summary>
        /// Returns a row of the matrix as a sparse vector.
        /// </summary>
        internal static SparseVector<T> SparseRow(int[] rowIndices, int[] columnIndices, T[] values, int rows, int columns, int rowIndex)
        {
            if (rowIndex < 0 || rowIndex >= rows)
            {
                throw new ArgumentOutOfRangeException(nameof(rowIndex));
            }

            int start = rowIndices[rowIndex],
                end = rowIndices[rowIndex + 1],
                len = end - start;

            int[] indices = new int[len];
            Array.Copy(columnIndices, start, indices, 0, len);

            T[] vect = new T[len];
            Array.Copy(values, start, vect, 0, len);

            return new SparseVector<T>(columns, indices, vect, true);
        }
        /// <summary>
        /// Returns a row of the matrix as a dense vector.
        /// </summary>
        internal static DenseVector<T> DenseRow(int[] rowIndices, int[] columnIndices, T[] values, T zero, int rows, int columns, int rowIndex)
        {
            if (rowIndex < 0 || rowIndex >= rows)
            {
                throw new ArgumentOutOfRangeException(nameof(rowIndex));
            }

            int start = rowIndices[rowIndex],
                end = rowIndices[rowIndex + 1];

            T[] vector = RectangularVector.Repeat(zero, columns);
            for (int i = start; i < end; ++i)
            {
                vector[columnIndices[i]] = values[i];
            }
            return new DenseVector<T>(vector);
        }

        /// <summary>
        /// Returns a column of this matrix as a big sparse vector.
        /// </summary>
        internal static BigSparseVector<T> BigSparseColumn(int[] rowIndices, int[] columnIndices, T[] values, int rows, int columns, int columnIndex)
        {
            if (columnIndex < 0 || columnIndex >= columns)
            {
                throw new ArgumentOutOfRangeException(nameof(columnIndex));
            }

            // This ends up being O(max(n, k)) ?
            Dictionary<long, T> column = new Dictionary<long, T>();
            for (int r = 0; r < rows; ++r)
            {
                int index = BinarySearch(columnIndices, columnIndex, rowIndices[r], rowIndices[r + 1]);
                if (index >= 0)
                {
                    column[r] = values[index];
                }
            }
            return new BigSparseVector<T>(rows, column);
        }
        /// <summary>
        /// Returns a column of this matrix as a sparse vector.
        /// </summary>
        internal static SparseVector<T> SparseColumn(int[] rowIndices, int[] columnIndices, T[] values, int rows, int columns, int columnIndex)
        {
            if (columnIndex < 0 || columnIndex >= columns)
            {
                throw new ArgumentOutOfRangeException(nameof(columnIndex));
            }

            int count = 0;
            for (int r = 0; r < rows; ++r)
            {
                int index = BinarySearch(columnIndices, columnIndex, rowIndices[r], rowIndices[r + 1]);
                if (index >= 0)
                {
                    ++count;
                }
            }

            int[] indices = new int[count];
            T[] vect = new T[count];
            for (int r = 0, k = 0; r < rows; ++r)
            {
                int index = BinarySearch(columnIndices, columnIndex, rowIndices[r], rowIndices[r + 1]);
                if (index >= 0)
                {
                    indices[k] = r;
                    vect[k] = values[index];
                    ++k;
                }
            }

            return new SparseVector<T>(rows, indices, vect, true);
        }
        /// <summary>
        /// Returns a column of this matrix as a dense vector.
        /// </summary>
        internal static DenseVector<T> DenseColumn(int[] rowIndices, int[] columnIndices, T[] values, T zero, int rows, int columns, int columnIndex)
        {
            if (columnIndex < 0 || columnIndex >= columns)
            {
                throw new ArgumentOutOfRangeException(nameof(columnIndex));
            }

            T[] column = RectangularVector.Repeat(zero, rows);
            for (int r = 0; r < rows; ++r)
            {
                int index = BinarySearch(columnIndices, columnIndex, rowIndices[r], rowIndices[r + 1]);
                if (index >= 0)
                {
                    column[r] = values[index];
                }
            }
            return new DenseVector<T>(column);
        }

        /// <summary>
        /// Matrix defined by (_rowIndices, _columnIndices, _values, matrix_rows, matrix_columns) 
        /// Extract the submatrix at (firstRow, firstColumn) of dimension (rows, columns), storing the result in (sub_rowIndex, sub_columnIndex, sub_values)
        /// </summary>
        internal static void Submatrix(int[] _rowIndices, int[] _columnIndices, T[] _values, int matrix_rows, int matrix_columns, 
            int firstRow, int firstColumn, int rows, int columns, out int[] sub_rowIndices, out int[] sub_columnIndices, out T[] sub_values)
        {
            if (rows < 0 || rows >= matrix_rows)
            {
                throw new ArgumentOutOfRangeException(nameof(rows));
            }
            if (columns < 0 || columns >= matrix_columns)
            {
                throw new ArgumentOutOfRangeException(nameof(columns));
            }

            int lastRow = firstRow + rows, lastColumn = firstColumn + columns;
            if (firstRow < 0 || lastRow >= matrix_rows)
            {
                throw new ArgumentOutOfRangeException(nameof(firstRow));
            }
            if (firstColumn < 0 || lastColumn >= matrix_columns)
            {
                throw new ArgumentOutOfRangeException(nameof(firstColumn));
            }

            // First count the number of elements in this submatrix, 
            // - this is used (instead of using lists) to conserve memory
            int count = 0;
            for (int r = firstRow; r < lastRow; ++r)
            {
                int start = _rowIndices[r], end = _rowIndices[r + 1];
                for (int i = start; i < end; ++i)
                {
                    int c = _columnIndices[i];
                    if (firstColumn <= c && c < lastColumn)
                    {
                        ++count;
                    }
                }
            }

            T[] values = new T[count];
            int[] columnIndices = new int[count];
            int[] rowIndices = new int[rows + 1];

            int offset = firstRow - 1, k = 0;
            for (int r = firstRow; r < lastRow; ++r)
            {
                int start = _rowIndices[r], end = _rowIndices[r + 1];
                for (int i = start; i < end; ++i)
                {
                    int c = _columnIndices[i];
                    if (firstColumn <= c && c < lastColumn)
                    {
                        values[k] = _values[i];
                        columnIndices[k] = _columnIndices[i] - firstColumn;
                        ++k;
                    }
                }
                rowIndices[r - offset] = k;
            }

            // Set the outputs
            sub_values = values;
            sub_columnIndices = columnIndices;
            sub_rowIndices = rowIndices;
        }

        /// <summary>
        /// Set a row from the matrix (rowIndices, columnIndices, values). The vectors are overwritten
        /// </summary>
        internal static void SetRow(int[] _rowIndices, ref int[] colIndices, ref T[] values, int rows, int rowIndex, BigSparseVector<T> row)
        {
            List<long> keys = new List<long>(row.Values.Keys);
            keys.Sort();

            // The number of existing elements in this row
            int row_start = _rowIndices[rowIndex], 
                row_end = _rowIndices[rowIndex + 1],
                row_count = row_end - row_start;

            // Requires a vector resize
            int offset = keys.Count - row_count;
            T[] _values = new T[values.Length + offset];
            int[] _columnIndices = new int[colIndices.Length + offset];

            // Copy other rows 
            Array.Copy(values,         0,        _values,        0,                  row_start);
            Array.Copy(colIndices,  0,        _columnIndices, 0,                  row_start);
            Array.Copy(values,         row_end,  _values,        row_end + offset,   values.Length - row_end);
            Array.Copy(colIndices,  row_end,  _columnIndices, row_end + offset,   values.Length - row_end);

            // Copy over the new values of this row.
            for (int i = 0; i < keys.Count; ++i)
            {
                int index = i + row_start;
                _columnIndices[index] = (int)keys[i];
                _values[index] = row.Values[keys[i]];
            }

            // Set to arrays
            colIndices = _columnIndices;
            values = _values;

            // Update the row start/end
            for (int i = rowIndex + 1; i <= rows; ++i)
            {
                _rowIndices[i] += offset;
            }
        }
        internal static void SetRow(int[] _rowIndices, ref int[] colIndices, ref T[] values, int rows, int rowIndex, SparseVector<T> row)
        {
            // The number of existing elements in this row
            int row_start = _rowIndices[rowIndex],
                row_end = _rowIndices[rowIndex + 1],
                row_count = row_end - row_start;

            // Requires a vector resize
            int[] vect_indices = row.Indices;
            T[] vect_values = row.Values;
            int vect_count = vect_indices.Length, offset = vect_count - row_count;

            T[] _values = new T[values.Length + offset];
            int[] _columnIndices = new int[colIndices.Length + offset];

            // Copy other rows 
            Array.Copy(values, 0, _values, 0, row_start);
            Array.Copy(colIndices, 0, _columnIndices, 0, row_start);
            Array.Copy(values, row_end, _values, row_end + offset, values.Length - row_end);
            Array.Copy(colIndices, row_end, _columnIndices, row_end + offset, values.Length - row_end);

            // Copy over the new values of this row.
            for (int i = 0; i < vect_count; ++i)
            {
                int index = i + row_start;
                _columnIndices[index] = vect_indices[i];
                _values[index] = vect_values[i];
            }

            // Set to arrays
            colIndices = _columnIndices;
            values = _values;

            // Update the row start/end
            for (int i = rowIndex + 1; i <= rows; ++i)
            {
                _rowIndices[i] += offset;
            }
        }
        internal static void SetRow(int[] _rowIndices, ref int[] _columnIndices, ref T[] _values, int Rows, int rowIndex, DenseVector<T> row, T _zero)
        {
            // Count the number of non-zeroes in the new row
            int new_nnz = 0, dim = row.Dimension;
            T[] vect_values = row.Values;
            for (int i = 0; i < dim; ++i)
            {
                if (!vect_values[i].Equals(_zero)) new_nnz++;
            }

            int row_start = _rowIndices[rowIndex],
                row_end = _rowIndices[rowIndex + 1],
                old_nnz = row_end - row_start,
                offset = new_nnz - old_nnz;

            // Requires a vector resize
            T[] values = new T[_values.Length + offset];
            int[] columnIndices = new int[_columnIndices.Length + offset];

            // Copy other rows 
            Array.Copy(_values, 0, values, 0, row_start);
            Array.Copy(_columnIndices, 0, columnIndices, 0, row_start);
            Array.Copy(_values, row_end, values, row_end + offset, _values.Length - row_end);
            Array.Copy(_columnIndices, row_end, columnIndices, row_end + offset, _columnIndices.Length - row_end);

            // Copy over the new values of this row.
            for (int i = 0, k = row_start; i < dim; ++i)
            {
                if (!vect_values[i].Equals(_zero))
                {
                    values[k] = vect_values[i];
                    columnIndices[k] = i;
                    ++k;
                }
            }

            // Set to arrays
            _columnIndices = columnIndices;
            _values = values;

            // Update the row start/end
            for (int i = rowIndex + 1; i <= Rows; ++i)
            {
                _rowIndices[i] += offset;
            }
        }
        internal static void SetColumn(int[] rowIndices, ref int[] colIndices, ref T[] values, int rows, int colIndex, T zero, BigSparseVector<T> column)
        {
            // Count the number of required insertions
            int count = values.Length;
            foreach (KeyValuePair<long, T> entry in column.Values)
            {
                int r = (int)entry.Key;
                if (BinarySearch(colIndices, colIndex, rowIndices[r], rowIndices[r + 1]) < 0)
                {
                    ++count;
                }
            }

            // Initialize the new arrays
            int[] _columnIndices = new int[count];
            T[] _values = new T[count];

            int offset = 0;
            for (int r = 0; r < rows; ++r)
            {
                // Undo the effects of offset from row_start, from the previous iteration
                int row_start = rowIndices[r] - offset, row_end = rowIndices[r + 1];

                if (column.Values.ContainsKey(r))
                {
                    int index = BinarySearch(colIndices, colIndex, row_start, row_end);
                    if (index >= 0)
                    {
                        // Overwrite a single value
                        values[index] = column.Values[r];

                        // Copy
                        int len = row_end - row_start;
                        if (len > 0)
                        {
                            Array.Copy(values,       row_start, _values,         row_start + offset, len);
                            Array.Copy(colIndices,   row_start, _columnIndices,  row_start + offset, len);
                        }
                    }
                    else
                    {
                        // Find the index in O(n)
                        index = -1;
                        for (int i = row_start; i < row_end; ++i)
                        {
                            if (colIndices[i] > colIndex)
                            {
                                index = i;
                                break;
                            }
                        }
                        if (index < 0)
                        {
                            index = row_end;
                        }

                        // [start, index) -> [start + offset, index + offset)
                        int len = index - row_start;
                        if (len > 0)
                        {
                            Array.Copy(values,       row_start, _values,         row_start + offset, len);
                            Array.Copy(colIndices,   row_start, _columnIndices,  row_start + offset, len);
                        }

                        // (new) -> [index + offset]
                        _values[index + offset] = column.Values[r];
                        _columnIndices[index + offset] = colIndex;

                        // [index, end) -> [index + offset + 1, end + offset + 1)
                        len = row_end - index;
                        if (len > 0)
                        {
                            Array.Copy(values,       index, _values,         index + offset + 1, len);
                            Array.Copy(colIndices,   index, _columnIndices,  index + offset + 1, len);
                        }

                        // Inserting term
                        ++offset;
                    }
                }
                else
                {
                    int index = BinarySearch(colIndices, colIndex, row_start, row_end);
                    if (index >= 0)
                    {
                        values[index] = zero;
                    }

                    // Straight copy
                    int len = row_end - row_start;
                    if (len > 0)
                    {
                        Array.Copy(values, row_start, _values, row_start + offset, len);
                        Array.Copy(colIndices, row_start, _columnIndices, row_start + offset, len);
                    }
                }

                // Update row boundaries
                rowIndices[r + 1] += offset;
            }

            colIndices = _columnIndices;
            values = _values;
        }
        internal static void SetColumn(int[] rowIndices, ref int[] colIndices, ref T[] values, int rows, int colIndex, T zero, SparseVector<T> column)
        {
            T[] vect_values = column.Values;
            int[] vect_indices = column.Indices;
            int vect_count = vect_values.Length;

            // Count the number of required insertions
            int count = values.Length;
            for (int i = 0; i < vect_count; ++i)
            {
                int r = vect_indices[i];
                if (BinarySearch(colIndices, colIndex, rowIndices[r], rowIndices[r + 1]) < 0)
                {
                    ++count;
                }
            }

            // Initialize the new arrays
            int[] _columnIndices = new int[count];
            T[] _values = new T[count];

            int offset = 0, vect_index = 0;
            for (int r = 0; r < rows; ++r)
            {
                // Undo the effects of offset from row_start, from the previous iteration
                int row_start = rowIndices[r] - offset, row_end = rowIndices[r + 1];

                if (vect_index < vect_count && vect_indices[vect_index] == r)
                {
                    int index = BinarySearch(colIndices, colIndex, row_start, row_end);
                    if (index >= 0)
                    {
                        // Overwrite a single value
                        values[index] = vect_values[vect_index];

                        // Copy
                        int len = row_end - row_start;
                        if (len > 0)
                        {
                            Array.Copy(values, row_start, _values, row_start + offset, len);
                            Array.Copy(colIndices, row_start, _columnIndices, row_start + offset, len);
                        }
                    }
                    else
                    {
                        // Find the index in O(n)
                        index = -1;
                        for (int i = row_start; i < row_end; ++i)
                        {
                            if (colIndices[i] > colIndex)
                            {
                                index = i;
                                break;
                            }
                        }
                        if (index < 0)
                        {
                            index = row_end;
                        }

                        // [start, index) -> [start + offset, index + offset)
                        int len = index - row_start;
                        if (len > 0)
                        {
                            Array.Copy(values, row_start, _values, row_start + offset, len);
                            Array.Copy(colIndices, row_start, _columnIndices, row_start + offset, len);
                        }

                        // (new) -> [index + offset]
                        _values[index + offset] = vect_values[vect_index];
                        _columnIndices[index + offset] = colIndex;

                        // [index, end) -> [index + offset + 1, end + offset + 1)
                        len = row_end - index;
                        if (len > 0)
                        {
                            Array.Copy(values, index, _values, index + offset + 1, len);
                            Array.Copy(colIndices, index, _columnIndices, index + offset + 1, len);
                        }

                        // Inserting term
                        ++offset;
                    }

                    // Increment vector index
                    vect_index++;
                }
                else
                {
                    int index = BinarySearch(colIndices, colIndex, row_start, row_end);
                    if (index >= 0)
                    {
                        values[index] = zero;
                    }

                    // Straight copy
                    int len = row_end - row_start;
                    if (len > 0)
                    {
                        Array.Copy(values, row_start, _values, row_start + offset, len);
                        Array.Copy(colIndices, row_start, _columnIndices, row_start + offset, len);
                    }
                }

                // Update row boundaries
                rowIndices[r + 1] += offset;
            }

            colIndices = _columnIndices;
            values = _values;
        }
        internal static void SetColumn(int[] rowIndices, ref int[] colIndices, ref T[] values, int rows, int colIndex, T zero, DenseVector<T> column)
        {
            // First count the number of additions
            int n = colIndices.Length, len = n + rows;
            for (int i = 0; i < n; ++i)
            {
                if (colIndices[i] == colIndex) --len;
            }

            int[] new_colIndices = new int[len];
            T[] new_values = new T[len],
                column_values = column.Values;

            int k = 0;
            for (int r = 0; r < rows; ++r)
            {
                int row_start = rowIndices[r], row_end = rowIndices[r + 1];
                for (int ci = row_start; ci < row_end; ++ci)
                {
                    int c = colIndices[ci];
                    if (c < colIndex)
                    {
                        new_colIndices[k] = c;
                        new_values[k] = values[ci];
                        ++k;
                    }
                    else if (c == colIndex)
                    {
                        new_colIndices[k] = c;
                        new_values[k] = column_values[r];
                        ++k;
                    }

                    // This doesn't work yet
                    else
                    {
                        new_colIndices[k] = c;
                        new_values[k] = column_values[r];
                    }
                }
            }

            throw new NotImplementedException();
        }

        internal static void ClearRow(int[] _rowIndices, ref int[] _columnIndices, ref T[] _values, int Rows, int rowIndex)
        {
            if (rowIndex < 0 || rowIndex >= Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(rowIndex));
            }

            int row_start = _rowIndices[rowIndex],
                row_end = _rowIndices[rowIndex + 1];

            // An empty row happens regularly emough to warrant this check
            if (row_end == row_start)
            {
                return;
            }

            int offset = row_end - row_start;

            // shift the row indices
            for (int r = rowIndex + 1; r <= Rows; ++r)
            {
                _rowIndices[r] -= offset;
            }

            int new_len = _values.Length - offset;
            T[] values = new T[new_len];
            int[] columnIndices = new int[new_len];

            Array.Copy(_values, 0, values, 0, row_start);
            Array.Copy(_columnIndices, 0, columnIndices, 0, row_start);

            int len = _values.Length - row_end;
            Array.Copy(_values, row_end, values, row_start, len);
            Array.Copy(_columnIndices, row_end, columnIndices, row_start, len);

            _values = values;
            _columnIndices = columnIndices;
        }


        /// <summary>
        /// TODO: there is a rare bug here
        /// </summary>
        /// <param name="_rowIndices"></param>
        /// <param name="_columnIndices"></param>
        /// <param name="_values"></param>
        /// <param name="rows"></param>
        /// <param name="columns"></param>
        /// <param name="columnIndex"></param>
        internal static void ClearColumn(int[] _rowIndices, ref int[] _columnIndices, ref T[] _values, int rows, int columns, int columnIndex)
        {
            if (columnIndex < 0 || columnIndex >= columns)
            {
                throw new ArgumentOutOfRangeException(nameof(columnIndex));
            }

            // Count the number of elements to remove
            int len = _rowIndices[rows], removeCount = 0;
            for (int c = 0; c < len; ++c)
            {
                if (_columnIndices[c] == columnIndex)
                {
                    ++removeCount;
                }
            }

            // Degenerate case - nothing to remove
            if (removeCount == 0)
            {
                return;
            }

            // Create the new storage arrays
            T[] values = new T[len - removeCount];
            int[] columnIndices = new int[len - removeCount];

            int rowIndex = 0, i, k;
            for (i = 0, k = 0; i < len; ++i)
            {
                if (_rowIndices[rowIndex] == i)
                {
                    _rowIndices[rowIndex] = k;
                    ++rowIndex;
                }

                if (_columnIndices[i] != columnIndex)
                {
                    values[k] = _values[i];
                    columnIndices[k] = _columnIndices[i];
                    ++k;
                }
            }

            _rowIndices[rows] = k;
            _values = values;
            _columnIndices = columnIndices;
        }
        internal static void ClearColumn(int[] _rowIndices, int[] _columnIndices, T[] _values, int rows, int columns, int columnIndex)
        {
            if (columnIndex < 0 || columnIndex >= columns)
            {
                throw new ArgumentOutOfRangeException(nameof(columnIndex));
            }

            // In-place column clearing (dangerous!)
            // k is the insertion index
            int next_row_index = 0;
            for (int r = 0, k = 0; r < rows; ++r)
            {
                for (int i = _rowIndices[r], rend = _rowIndices[r + 1]; i < rend; ++i)
                {
                    int c = _columnIndices[i];
                    if (c != columnIndex)
                    {
                        _columnIndices[k] = c;
                        _values[k++] = _values[i];
                    }
                }

                // Delay the setting of row indexes by 1, so as to not conflict with 
                // the boundaries of the _columnIndex, _values inner loop
                _rowIndices[r] = next_row_index;
                next_row_index = k;
            }
            // Set the last row index
            _rowIndices[rows] = next_row_index;
        }

        internal static DenseVector<int> RowCountSymbolic(int[] _rowIndices, int Rows)
        {
            int[] counts = new int[Rows];

            int start, end = 0;
            for (int r = 0; r < Rows; ++r)
            {
                start = end;
                end = _rowIndices[r + 1];
                counts[r] = end - start;
            }
            return new DenseVector<int>(counts);
        }
        internal static DenseVector<int> ColumnCountSymbolic(int[] _columnIndices, int Columns)
        {
            int[] counts = new int[Columns];
            int len = _columnIndices.Length, i;
            for (i = 0; i < len; ++i)
            {
                counts[_columnIndices[i]]++;
            }
            return new DenseVector<int>(counts);
        }

        internal static DenseVector<T> RowSums(int[] _rowIndices, T[] _values, int Rows, IProvider<T> provider)
        {
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }

            T[] sums = RectangularVector.Zeroes<T>(Rows);
            for (int r = 0; r < Rows; ++r)
            {
                T sum = provider.Zero;
                int end = _rowIndices[r + 1];
                for (int i = _rowIndices[r]; i < end; ++i)
                {
                    sum = provider.Add(sum, _values[i]);
                }
                sums[r] = sum;
            }
            return new DenseVector<T>(sums);
        }
        internal static DenseVector<T> ColumnSums(int[] _columnIndices, T[] _values, int Columns, IProvider<T> provider)
        {
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }

            T[] colSums = RectangularVector.Repeat(provider.Zero, Columns);

            int len = _values.Length, i;
            for (i = 0; i < len; ++i)
            {
                int c = _columnIndices[i];
                colSums[c] = provider.Add(colSums[c], _values[i]);
            }
            return new DenseVector<T>(colSums);
        }

        internal static void SwapRows(int[] _rowIndices, int[] _columnIndices, T[] _values, int rowIndex1, int rowIndex2, int Rows)
        {
            // Enforce rowIndex1 <= rowIndex2
            if (rowIndex1 > rowIndex2)
            {
                SwapRows(_rowIndices, _columnIndices, _values, rowIndex2, rowIndex1, Rows);
                return;
            }

            if (rowIndex1 == rowIndex2)
            {
                return;
            }

            if (rowIndex1 < 0 || rowIndex1 >= Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(rowIndex1));
            }
            if (rowIndex2 < 0 || rowIndex2 >= Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(rowIndex2));
            }

            int row1_start = _rowIndices[rowIndex1], row1_end = _rowIndices[rowIndex1 + 1],
                row2_start = _rowIndices[rowIndex2], row2_end = _rowIndices[rowIndex2 + 1],
                row1_len = row1_end - row1_start,
                row2_len = row2_end - row2_start;

            T[] row1_buff = new T[row1_len], row2_buff = new T[row2_len];
            int[] _row1_buff = new int[row1_len], _row2_buff = new int[row2_len];

            Array.Copy(_values, row1_start, row1_buff, 0, row1_len);
            Array.Copy(_values, row2_start, row2_buff, 0, row2_len);

            Array.Copy(_columnIndices, row1_start, _row1_buff, 0, row1_len);
            Array.Copy(_columnIndices, row2_start, _row2_buff, 0, row2_len);

            // Shift the middle section in between row1 and row2
            // row1_end -> row1_start + (row2_end - row2_start)
            _values.Shift(row1_end, row2_start - row1_end, row2_len - row1_len);
            _columnIndices.Shift(row1_end, row2_start - row1_end, row2_len - row1_len);

            Array.Copy(row1_buff, 0, _values, row2_end - row1_len, row1_len);
            Array.Copy(row2_buff, 0, _values, row1_start, row2_len);

            Array.Copy(_row1_buff, 0, _columnIndices, row2_end - row1_len, row1_len);
            Array.Copy(_row2_buff, 0, _columnIndices, row1_start, row2_len);
        }
        internal static void SwapColumns(int[] _rowIndices, int[] _columnIndices, T[] _values, int columnIndex1, int columnIndex2, int Rows, int Columns)
        {
            if (columnIndex1 > columnIndex2)
            {
                SwapColumns(_rowIndices, _columnIndices, _values, columnIndex2, columnIndex1, Rows, Columns);
                return;
            }

            if (columnIndex1 < 0 || columnIndex1 >= Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(columnIndex1));
            }
            if (columnIndex2 < 0 || columnIndex2 >= Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(columnIndex2));
            }

            // The number of non-zero elements within each row is invariant
            // so we don't need to adjust row indices at all.

            for (int r = 0; r < Rows; ++r)
            {
                int row_start = _rowIndices[r],
                    row_end = _rowIndices[r + 1];

                int col_index1 = -1, col_index2 = -1;
                for (int i = row_start; i < row_end; ++i)
                {
                    if (_columnIndices[i] == columnIndex1)
                    {
                        col_index1 = i;
                    }
                    if (_columnIndices[i] == columnIndex2)
                    {
                        col_index2 = i;
                    }
                }

                // If both columns are non-zero, simply swap the values and move on
                if (col_index1 >= 0 && col_index2 >= 0)
                {
                    T swap = _values[col_index2];
                    _values[col_index2] = _values[col_index1];
                    _values[col_index1] = swap;

                    continue;
                }

                // Only the first index is non-zero - need to shift all indices in [columnIndex1, columnIndex2) down by 1.
                else if (col_index1 >= 0)
                {
                    T value = _values[col_index1];
                    int i = col_index1 + 1;
                    while (i < col_index2)
                    {
                        // Shift down
                        _values[i - 1] = _values[i];
                        _columnIndices[i - 1] = _columnIndices[i];
                        ++i;
                    }
                    _values[i] = value;
                    _columnIndices[i] = columnIndex2;
                }

                // Only the second index is non-zero - need to shift all indices in [columnIndex1, columnIndex2] up by 1.
                else if (col_index2 >= 0)
                {
                    T value = _values[col_index2];
                    int i = col_index2 - 1;
                    while (i > col_index1)
                    {
                        // Shift up
                        _values[i + 1] = _values[i];
                        _columnIndices[i + 1] = _columnIndices[i];
                        --i;
                    }
                    _values[i] = value;
                    _columnIndices[i] = columnIndex1;
                }
            }
        }

        /// <summary>
        /// Computes A^Tx for A defined by the triplet (rowIndices, columnIndices, values) in CSR format. x = 'vector', product is stored in 'result'.
        /// </summary>
        internal static void TransposeAndMultiply(T[] vector, T[] result, IProvider<T> provider, int[] rowIndices, int[] columnIndices, T[] values, int rows, int columns, bool increment)
        {
            if (vector is null)
            {
                throw new ArgumentNullException(nameof(vector));
            }
            if (result is null)
            {
                throw new ArgumentNullException(nameof(result));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            if (vector.Length != rows)
            {
                throw new InvalidOperationException("The vector dimension does not match the row dimension of this matrix.");
            }
            if (result.Length < columns)
            {
                throw new ArgumentOutOfRangeException(nameof(result), "The result vector is not large enough to store the matrix-vector product.");
            }

            if (increment)
            {
                Array.Clear(result, 0, result.Length);
            }

            int row_start, row_end = rowIndices[0];
            for (int r = 0; r < rows; ++r)
            {
                row_start = row_end;
                row_end = rowIndices[r + 1];

                for (int i = row_start; i < row_end; ++i)
                {
                    int c = columnIndices[i];
                    result[c] = provider.Add(result[c], provider.Multiply(values[i], vector[r]));
                }
            }
        }

        /// <summary>
        /// Computes Ax for A defined by the triplet (rowIndices, columnIndices, values) in CSR format. x = 'vector', product is stored in 'product'
        /// </summary>
        internal static void Multiply(T[] vector, T[] product, IProvider<T> provider, int[] rowIndices, int[] columnIndices, T[] values, int rows, bool increment)
        {
            int row_start, row_end = 0;

            if (increment)
            {
                for (int r = 0; r < rows; ++r)
                {
                    row_start = row_end;
                    row_end = rowIndices[r + 1];

                    T sum = provider.Zero;
                    for (int i = row_start; i < row_end; ++i)
                    {
                        sum = provider.Add(sum, provider.Multiply(values[i], vector[columnIndices[i]]));
                    }
                    product[r] = provider.Add(product[r], sum);
                }
            }
            else
            {
                for (int r = 0; r < rows; ++r)
                {
                    row_start = row_end;
                    row_end = rowIndices[r + 1];

                    T sum = provider.Zero;
                    for (int i = row_start; i < row_end; ++i)
                    {
                        sum = provider.Add(sum, provider.Multiply(values[i], vector[columnIndices[i]]));
                    }
                    product[r] = sum;
                }
            }
            
        }
        
        internal static SparseVector<T> Multiply(SparseVector<T> vector, IProvider<T> provider, int[] rowIndices, int[] columnIndices, T[] values, int rows)
        {
            T[] vect_values = vector.Values;
            int[] vect_indices = vector.Indices;
            int vlen = vect_indices.Length;

            // Loop through and count the nnz of the resulting product vector
            int nnz = 0;
            for (int r = 0; r < rows; ++r)
            {
                int row_end = rowIndices[r + 1];
                int v = 0;
                for (int i = rowIndices[r]; i < row_end; ++i)
                {
                    int c = columnIndices[i];
                    while (c > vect_indices[v])
                    {
                        ++v;
                        if (v >= vlen)
                        {
                            goto cend;
                        }
                    }

                    if (v < vlen && c == vect_indices[v])
                    {
                        ++nnz;
                        break;
                    }
                }

            cend:
                continue;
            }

            T[] prod_values = new T[nnz];
            int[] prod_indices = new int[nnz];

            int k = 0;
            for (int r = 0; r < rows; ++r)
            {
                int row_end = rowIndices[r + 1];
                int v = 0;

                T sum = provider.Zero;
                bool any = false;

                for (int i = rowIndices[r]; i < row_end; ++i)
                {
                    int c = columnIndices[i];
                    while (c > vect_indices[v])
                    {
                        ++v;
                        if (v >= vlen)
                        {
                            goto end;
                        }
                    }

                    if (c == vect_indices[v])
                    {
                        sum = provider.Add(sum, provider.Multiply(values[i], vect_values[v]));
                        any = true;
                    }
                }

            end:
                if (any)
                {
                    prod_indices[k] = r;
                    prod_values[k] = sum;
                    ++k;
                }
            }

            return new SparseVector<T>(rows, prod_indices, prod_values, true);
        }

        internal static SparseVector<T> TransposeAndMultiply(SparseVector<T> vector, IProvider<T> provider, int[] rowIndices, int[] columnIndices, T[] values, int rows, int columns)
        {
            int[] vect_indices = vector.Indices;
            T[] vect_values = vector.Values;

            Dictionary<int, T> product = new Dictionary<int, T>();

            int len = vect_indices.Length;
            for (int v = 0; v < len; ++v)
            {
                int r = vect_indices[v];
                T vect_val = vect_values[v];
                int row_end = rowIndices[r + 1];

                for (int j = rowIndices[r]; j < row_end; ++j)
                {
                    int c = columnIndices[j];
                    if (product.ContainsKey(c))
                    {
                        product[c] = provider.Add(product[c], provider.Multiply(values[j], vect_val));
                    }
                    else
                    {
                        product[c] = provider.Multiply(values[j], vect_val);
                    }
                }
            }

            int prod_len = product.Count, k = 0;
            int[] indices = new int[prod_len];
            foreach (int key in product.Keys)
            {
                indices[k++] = key;
            }
            Array.Sort(indices);

            T[] prod_values = new T[prod_len];
            for (k = 0; k < prod_len; ++k)
            {
                prod_values[k] = product[indices[k]];
            }

            return new SparseVector<T>(columns, indices, prod_values, true);
        }

        internal static void Add(
            int[] rowIndices1, int[] colIndices1, T[] values1, // The first matrix triplet
            int[] rowIndices2, int[] colIndices2, T[] values2, // The second matrix triplet
            int[] rowIndices, out int[] colIndices, out T[] values, // the matrix sum triplet
            int rows, IProvider<T> provider)
        {
            // Iterate through once to find the number of non-zero elements
            int nnz = CountNnzInUnion(rowIndices1, colIndices1, rowIndices2, colIndices2, rows);

            colIndices = new int[nnz];
            values = new T[nnz];

            // Second iteration is for the actual addition step
            for (int r = 0, k = 0; r < rows; ++r)
            {
                int r_ = r + 1;
                int r1 = rowIndices1[r], row_end_1 = rowIndices1[r_];
                int r2 = rowIndices2[r], row_end_2 = rowIndices2[r_];

                while (r1 < row_end_1 || r2 < row_end_2)
                {
                    int c1 = r1 < row_end_1 ? colIndices1[r1] : int.MaxValue;
                    int c2 = r2 < row_end_2 ? colIndices2[r2] : int.MaxValue;

                    if (c1 > c2)
                    {
                        colIndices[k] = c2;
                        values[k] = values2[r2];
                        ++k;
                        ++r2;
                    }
                    else if (c1 < c2)
                    {
                        colIndices[k] = c1;
                        values[k] = values1[r1];
                        ++k;
                        ++r1;
                    }

                    else
                    {
                        // c1 = c2, requires addition
                        // Previously this block of code checked for exact numeric cancellation
                        // to preserve memory in the resulting matrix sum. This was removed because
                        // exact cancellation is rare in practice.
                        // To add this check back in, we need to also alter the nnz counter subroutine
                        // at the top of this method

                        colIndices[k] = c1;
                        values[k] = provider.Add(values1[r1], values2[r2]);
                        ++k;
                        ++r1;
                        ++r2;
                    }
                }
                rowIndices[r_] = k;
            }
        }

        internal static void Add(
            int[] rowIndices1, int[] colIndices1, T[] values1, // The first matrix triplet
            int[] rowIndices2, int[] colIndices2, T[] values2, // The second matrix triplet
            int[] rowIndices, out int[] colIndices, out T[] values, // the matrix sum triplet
            int rows, ISparseBLAS1<T> blas)
        {
            // Iterate through once to find the number of non-zero elements
            int nnz = CountNnzInUnion(rowIndices1, colIndices1, rowIndices2, colIndices2, rows);

            colIndices = new int[nnz];
            values = new T[nnz];

            // Second iteration is for the actual addition step
            for (int r = 0, k = 0; r < rows; ++r)
            {
                int r_ = r + 1;
                k = blas.ADD(colIndices1, values1, colIndices2, values2, colIndices, values, rowIndices1[r], rowIndices2[r], k, rowIndices1[r_], rowIndices2[r_]);
                rowIndices[r_] = k;
            }
        }
        internal static void Subtract(
            int[] rowIndices1, int[] colIndices1, T[] values1, // The first matrix triplet
            int[] rowIndices2, int[] colIndices2, T[] values2, // The second matrix triplet
            int[] rowIndices, out int[] colIndices, out T[] values, // the matrix sum triplet
            int rows, IProvider<T> provider)
        {
            // Iterate through once to find the number of non-zero elements
            int nnz = CountNnzInUnion(rowIndices1, colIndices1, rowIndices2, colIndices2, rows);

            colIndices = new int[nnz];
            values = new T[nnz];

            // Second iteration is for the actual addition step
            for (int r = 0, k = 0; r < rows; ++r)
            {
                int r_ = r + 1,
                    r1 = rowIndices1[r], row_end_1 = rowIndices1[r_],
                    r2 = rowIndices2[r], row_end_2 = rowIndices2[r_];

                while (r1 < row_end_1 || r2 < row_end_2)
                {
                    int c1 = r1 < row_end_1 ? colIndices1[r1] : int.MaxValue;
                    int c2 = r2 < row_end_2 ? colIndices2[r2] : int.MaxValue;

                    if (c1 > c2)
                    {
                        colIndices[k] = c2;
                        values[k] = provider.Negate(values2[r2]);
                        ++r2;
                    }
                    else if (c1 < c2)
                    {
                        colIndices[k] = c1;
                        values[k] = values1[r1];
                        ++r1;
                    }

                    else
                    {
                        // c1 = c2, requires addition
                        // Previously this block of code checked for exact numeric cancellation
                        // to preserve memory in the resulting matrix sum. This was removed because
                        // exact cancellation is rare in practice.
                        // To add this check back in, we need to also alter the nnz counter subroutine
                        // at the top of this method

                        colIndices[k] = c1;
                        values[k] = provider.Subtract(values1[r1], values2[r2]);
                        ++r1;
                        ++r2;
                    }
                    ++k;
                }
                rowIndices[r_] = k;
            }
        }

        private static int CountNnzInUnion(int[] rowIndices1, int[] colIndices1, int[] rowIndices2, int[] colIndices2, int rows)
        {
            int nnz = 0;
            for (int r = 0; r < rows; ++r)
            {
                int r_ = r + 1,
                    r1 = rowIndices1[r], row_end_1 = rowIndices1[r_],
                    r2 = rowIndices2[r], row_end_2 = rowIndices2[r_];

                while (r1 < row_end_1 || r2 < row_end_2)
                {
                    int c1 = r1 < row_end_1 ? colIndices1[r1] : int.MaxValue;
                    int c2 = r2 < row_end_2 ? colIndices2[r2] : int.MaxValue;

                    if (c1 > c2)
                    {
                        ++r2;
                    }
                    else if (c1 < c2)
                    {
                        ++r1;
                    }
                    else
                    {
                        ++r1;
                        ++r2;
                    }
                    ++nnz;
                }
            }
            return nnz;
        }

        /// <summary>
        /// (_rowIndices, _columnIndices, _values)^T -> (rowIndices_t, colIndices_t, values_t)
        /// </summary>
        internal static void Transpose(int[] _rowIndices, int[] _colIndices, T[] _values, out int[] rowIndices_t, out int[] colIndices_t, out T[] values_t, int rows, int cols)
        {
            int count = _colIndices.Length;
            int[] rowIndices = new int[count];
            T[] values = new T[count];

            // This temporarily stores the number of elements of each column
            int[] colIndices = new int[cols + 1];
            for (int i = 0; i < count; ++i)
            {
                colIndices[_colIndices[i] + 1]++;
            }

            // calculate cumulative column indices
            for (int i = 1; i <= cols; ++i)
            {
                colIndices[i] += colIndices[i - 1];
            }

            int[] colCounts = new int[cols];
            // loop through the elements once, so this is actually O(n)
            for (int r = 0; r < rows; ++r)
            {
                int row_start = _rowIndices[r];
                int row_end = _rowIndices[r + 1];

                for (int i = row_start; i < row_end; ++i)
                {
                    int c = _colIndices[i];
                    int index = colIndices[c] + colCounts[c];
                    values[index] = _values[i];
                    rowIndices[index] = r;
                    colCounts[c]++;
                }
            }

            rowIndices_t = rowIndices;
            colIndices_t = colIndices;
            values_t = values;
        }

        /// <summary>
        /// O(m log(k)) for m x n CSR matrix with k nnz.
        /// </summary>
        internal static T Trace(int[] _rowIndices, int[] _columnIndices, T[] _values, int Rows, IProvider<T> provider)
        {
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }

            T sum = provider.Zero;
            for (int r = 0; r < Rows; ++r)
            {
                int i = BinarySearch(_columnIndices, r, _rowIndices[r], _rowIndices[r + 1]);
                if (i >= 0)
                {
                    sum = provider.Add(sum, _values[i]);
                }
            }
            return sum;
        }

        internal static T[] LeadingDiagonal(int[] _rowIndices, int[] _columnIndices, T[] _values, int min)
        {
            T[] diag = RectangularVector.Zeroes<T>(min);
            for (int r = 0; r < min; ++r)
            {
                int index = BinarySearch(_columnIndices, r, _rowIndices[r], _rowIndices[r + 1]);
                if (index >= 0)
                {
                    diag[r] = _values[index];
                }
            }
            return diag;
        }

        internal static bool IsLower(int[] _rowIndices, int[] _columnIndices, T[] _values, int Rows, Predicate<T> isZero)
        {
            if (isZero is null)
            {
                throw new ArgumentNullException(nameof(isZero));
            }

            for (int r = 0; r < Rows; ++r)
            {
                int row_start = _rowIndices[r], row_end = _rowIndices[r + 1];
                if (row_end > row_start && _columnIndices[row_end - 1] > r)
                {
                    // Iterate the row from right to left, checking for non-zeroes
                    for (int i = row_end - 1; i >= row_start; --i)
                    {
                        if (_columnIndices[i] <= r)
                        {
                            break; // break when we enter the lower-triangular half
                        }
                        if (!isZero(_values[i]))
                        {
                            return false;
                        }
                    }
                }
            }
            return true;
        }
        internal static bool IsLowerSymbolic(int[] _rowIndices, int[] _columnIndices, int Rows)
        {
            for (int r = 0; r < Rows; ++r)
            {
                int end = _rowIndices[r + 1];
                if (end > _rowIndices[r] && _columnIndices[end - 1] > r)
                {
                    return false;
                }
            }
            return true;
        }
        internal static bool IsUpper(int[] _rowIndices, int[] _columnIndices, T[] _values, int Rows, Predicate<T> isZero)
        {
            if (isZero is null)
            {
                throw new ArgumentNullException(nameof(isZero));
            }

            for (int r = 0; r < Rows; ++r)
            {
                int row_start = _rowIndices[r], row_end = _rowIndices[r + 1];
                if (row_end > row_start && _columnIndices[row_start] < r)
                {
                    // Iterate from left to right, until we enter the upper half
                    for (int i = row_start; i < row_end; ++i)
                    {
                        if (_columnIndices[i] >= r)
                        {
                            break;
                        }
                        if (!isZero(_values[i]))
                        {
                            return false;
                        }
                    }
                }
            }
            return true;
        }
        internal static bool IsUpperSymbolic(int[] _rowIndices, int[] _columnIndices, int Rows)
        {
            for (int r = 0; r < Rows; ++r)
            {
                int start = _rowIndices[r];
                if (start < _rowIndices[r + 1] && _columnIndices[start] < r)
                {
                    return false;
                }
            }
            return true;
        }

        internal static F FrobeniusNorm<F>(T[] _values, Func<T, F> ElementNorm, IProvider<F> provider)
        {
            F zero = provider.Zero, sum = zero;
            for (int i = 0; i < _values.Length; ++i)
            {
                F norm = ElementNorm(_values[i]);
                if (!provider.Equals(norm, zero))
                {
                    sum = provider.Add(sum, provider.Multiply(norm, norm));
                }
            }
            return provider.Sqrt(sum);
        }
        /// <summary>
        /// Returns the Frobenius norm of the matrix difference of two matrices.
        /// </summary>
        internal static F FrobeniusDistance<F>(int[] rowIndices1, int[] columnIndices1, T[] values1, int[] rowIndices2, int[] columnIndices2, T[] values2, 
            Func<T, F> elementNorm, IProvider<T> providerT, IProvider<F> providerF)
        {
            if (rowIndices1.Length != rowIndices2.Length)
            {
                throw new InvalidOperationException(ExceptionMessages.MatrixSize);
            }

            F sum = providerF.Zero;
            int rows = rowIndices1.Length - 1;
            for (int r = 0; r < rows; ++r)
            {
                int i1 = rowIndices1[r], row_end1 = rowIndices1[r + 1],
                    i2 = rowIndices2[r], row_end2 = rowIndices2[r + 1];

                while (i1 < row_end1 || i2 < row_end2)
                {
                    int c1 = i1 < row_end1 ? columnIndices1[i1] : int.MaxValue;
                    int c2 = i2 < row_end2 ? columnIndices2[i2] : int.MaxValue;

                    F norm;
                    if (c1 < c2)
                    {
                        norm = elementNorm(values1[i1]);
                        ++i1;
                    }
                    else if (c1 > c2)
                    {
                        norm = elementNorm(providerT.Negate(values2[i2]));
                        ++i2;
                    }
                    else
                    {
                        norm = elementNorm(providerT.Subtract(values1[i1], values2[i2]));
                        ++i1;
                        ++i2;
                    }
                    sum = providerF.Add(sum, providerF.Multiply(norm, norm));
                }
            }
            return providerF.Sqrt(sum);
        }

        internal static void Clear(int[] _rowIndices, ref int[] _columnIndices, ref T[] _values, int Rows, Predicate<MatrixEntry<T>> select, bool defrag)
        {
            if (select is null)
            {
                throw new ArgumentNullException(nameof(select));
            }

            int p = 0;
            for (int r = 0; r < Rows; ++r)
            {
                int col_end = _rowIndices[r + 1];
                for (int i = _rowIndices[r]; i < col_end; ++i)
                {
                    if (!select(new MatrixEntry<T>(r, _columnIndices[i], _values[i])))
                    {
                        // Guaranteed that p <= i, so this subroutine will not 
                        // overwrite values
                        _columnIndices[p] = _columnIndices[i];
                        _values[p] = _values[i];
                        ++p;
                    }
                }

                // We cannot overwrite _rowIndices[c + 1] since 
                // we need the old value in the next iteration of the loop,
                // so we store it in _rowIndices[c] (which is no longer needed)
                // and spend O(n) time shifting it at the end
                _rowIndices[r] = p;
            }

            // Shift the row indices
            for (int c = Rows; c > 0; --c)
            {
                _rowIndices[c] = _rowIndices[c - 1];
            }
            _rowIndices[0] = 0;

            // Resize arrays
            if (defrag)
            {
                Array.Resize(ref _columnIndices, p);
                Array.Resize(ref _values, p);
            }
        }

        internal static void Lower(int[] _rowIndices, int[] _columnIndices, T[] _values, int Rows, out int[] rowIndices, out int[] columnIndices, out T[] values)
        {
            // Count the number of nz on or below the main diagonal
            rowIndices = new int[Rows + 1];
            for (int r = 0; r < Rows; ++r)
            {
                int row_start = _rowIndices[r], row_end = _rowIndices[r + 1], i;
                for (i = row_start; i < row_end; ++i)
                {
                    int c = _columnIndices[i];
                    if (c > r)
                    {
                        break;
                    }
                }
                rowIndices[r + 1] = rowIndices[r] + (i - row_start);
            }

            int[] next = new int[Rows];
            Array.Copy(rowIndices, 0, next, 0, Rows);

            columnIndices = new int[rowIndices[Rows]];
            values = new T[rowIndices[Rows]];
            for (int r = 0; r < Rows; ++r)
            {
                int start = rowIndices[r],
                    end = rowIndices[r + 1];

                Array.Copy(_columnIndices, _rowIndices[r], columnIndices, start, end - start);
                Array.Copy(_values, _rowIndices[r], values, start, end - start);
            }
        }
        internal static void Upper(int[] _rowIndices, int[] _columnIndices, T[] _values, int Rows, out int[] rowIndices, out int[] columnIndices, out T[] values)
        {
            // Record the start of each row for the upper matrix
            int[] starts = new int[Rows];
            for (int r = 0; r < Rows; ++r)
            {
                int row_end = _rowIndices[r + 1], i;
                for (i = _rowIndices[r]; i < row_end; ++i)
                {
                    if (_columnIndices[i] >= r)
                    {
                        break;
                    }
                }
                starts[r] = i;
            }

            rowIndices = new int[Rows + 1];
            for (int r = 0; r < Rows; ++r)
            {
                rowIndices[r + 1] = rowIndices[r] + (_rowIndices[r + 1] - starts[r]);
            }

            columnIndices = new int[rowIndices[Rows]];
            values = new T[rowIndices[Rows]];

            for (int r = 0; r < Rows; ++r)
            {
                int start = rowIndices[r], len = rowIndices[r + 1] - start;
                Array.Copy(_columnIndices, starts[r], columnIndices, start, len);
                Array.Copy(_values, starts[r], values, start, len);
            }
        }
    
        internal static void Select(int[] rowSelection, int[] columnSelection, int[] _rowIndices, int[] _columnIndices, T[] _values, int Rows, int Columns,
            out int[] rowIndices, out int[] columnIndices, out T[] values)
        {
            int rlen = rowSelection.Length, 
                clen = columnSelection.Length;

            int[] cIndex_lookup = new int[Columns];
            BitArray in_row = new BitArray(Columns);

            int nz = 0;
            for (int ri = 0; ri < rlen; ++ri)
            {
                int r = rowSelection[ri], end = _rowIndices[r + 1];

                // Copy non-zero pattern of row into in_row bitarray
                for (int i = _rowIndices[r]; i < end; ++i)
                {
                    in_row[_columnIndices[i]] = true;
                }

                for (int ci = 0; ci < clen; ++ci)
                {
                    if (in_row[columnSelection[ci]]) ++nz;
                }

                // Unmark the non-zero pattern of this row
                for (int i = _rowIndices[r]; i < end; ++i)
                {
                    in_row[_columnIndices[i]] = false;
                }
            }

            rowIndices = new int[rlen + 1];
            columnIndices = new int[nz];
            values = new T[nz];

            int k = 0;
            for (int ri = 0; ri < rlen; ++ri)
            {
                int r = rowSelection[ri], end = _rowIndices[r + 1];

                // Copy non-zero pattern of row into in_row bitarray
                for (int i = _rowIndices[r]; i < end; ++i)
                {
                    int c = _columnIndices[i];
                    in_row[c] = true;
                    cIndex_lookup[c] = i;
                }

                for (int ci = 0; ci < clen; ++ci)
                {
                    int c = columnSelection[ci];
                    if (in_row[c])
                    {
                        columnIndices[k] = ci;
                        values[k++] = _values[cIndex_lookup[c]];
                    }
                }

                // Unmark the non-zero pattern of this row
                for (int i = _rowIndices[r]; i < end; ++i)
                {
                    in_row[_columnIndices[i]] = false;
                }
                rowIndices[ri + 1] = k;
            }
        }

        internal static SparseVector<T> Select(int rowSelection, int[] columnSelections, int[] _rowIndices, int[] _columnIndices, T[] _values, int Columns)
        {
            int[] cIndex_lookup = new int[Columns];
            BitArray isnz = new BitArray(Columns);

            int end = _rowIndices[rowSelection + 1];
            for (int i = _rowIndices[rowSelection]; i < end; ++i)
            {
                int c = _columnIndices[i];
                cIndex_lookup[c] = i;
                isnz[c] = true;
            }

            int clen = columnSelections.Length, nz = 0;
            for (int ci = 0; ci < clen; ++ci)
            {
                if (isnz[columnSelections[ci]])
                {
                    ++nz;
                }
            }

            int[] indices = new int[nz];
            T[] values = new T[nz];

            int k = 0;
            for (int ci = 0; ci < clen; ++ci)
            {
                int c = columnSelections[ci];
                if (isnz[c])
                {
                    indices[k] = ci;
                    values[k++] = _values[cIndex_lookup[c]];
                }
            }

            return new SparseVector<T>(clen, indices, values, true);
        }

        internal static SparseVector<T> Select(int[] rowSelections, int columnSelection, int[] _rowIndices, int[] _columnIndices, T[] _values)
        {
            int rlen = rowSelections.Length;
            int[] indices = new int[rlen];
            T[] values = new T[rlen];

            int k = 0;
            for (int ri = 0; ri < rlen; ++ri)
            {
                int r = rowSelections[ri],
                    start = _rowIndices[r],
                    end = _rowIndices[r + 1];
                int index = Array.BinarySearch(_columnIndices, start, end - start, columnSelection);
                if (index >= 0)
                {
                    indices[k] = ri;
                    values[k++] = _values[index];
                }
            }

            if (k < rlen)
            {
                Array.Resize(ref indices, k);
                Array.Resize(ref values, k);
            }
            return new SparseVector<T>(rlen, indices, values, true);
        }
    }
}
