﻿using LinearNet.Global;
using LinearNet.Matrices.Sparse;
using LinearNet.Matrices.Sparse.Decompositions;
using LinearNet.Providers;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LinearNet.Structs
{
    /// <summary>
    /// Implements a sparse matrix in Compressed Sparse Row (CSR) form. This storage method allows for faster operations but 
    /// is slow for incremental matrix creation. For best result, create a matrix using a Dictionary-Of-Keys (DOK) matrix or 
    /// Coordinate-List (COO) form.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <cat>linear-algebra</cat>
    public class CSRSparseMatrix<T> : Matrix<T> where T : new()
    {
        private readonly T _zero;
        private int[] _rowIndices, _columnIndices;
        private T[] _values;

        internal int[] RowIndices { get { return _rowIndices; } }
        internal int[] ColumnIndices { get { return _columnIndices; } }
        internal T[] Values { get { return _values; } }

        public override T[] this[int index] { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public override T this[int rowIndex, int columnIndex]
        {
            get
            {
                if (rowIndex < 0 || rowIndex >= Rows)
                {
                    throw new ArgumentOutOfRangeException(nameof(rowIndex));
                }
                if (columnIndex < 0 || columnIndex >= Columns)
                {
                    throw new ArgumentOutOfRangeException(nameof(columnIndex));
                }

                return CSRHelper<T>.Get(rowIndex, columnIndex, _rowIndices, _columnIndices, _values, _zero);
            }
            set
            {
                if (rowIndex < 0 || rowIndex >= Rows)
                {
                    throw new ArgumentOutOfRangeException(nameof(rowIndex));
                }
                if (columnIndex < 0 || columnIndex >= Columns)
                {
                    throw new ArgumentOutOfRangeException(nameof(columnIndex));
                }

                CSRHelper<T>.Set(rowIndex, columnIndex, _rowIndices, ref _columnIndices, ref _values, value);
            }
        }

        /// <summary>
        /// Returns the number of non-zero entries in this matrix.
        /// </summary>
        public override int SymbolicNonZeroCount { get { return _columnIndices.Length; } }

        public override IEnumerable<MatrixEntry<T>> NonZeroEntries
        {
            get
            {
                for (int r = 0; r < Rows; ++r)
                {
                    int end = _rowIndices[r + 1];
                    for (int i = _rowIndices[r]; i < end; ++i)
                    {
                        yield return new MatrixEntry<T>(r, _columnIndices[i], _values[i]);
                    }
                }
            }
        }

        /// <summary>
        /// Returns the density of this matrix, defined as the number in of non-zero elements divided by the 
        /// capacity of this matrix. 
        /// </summary>
        public double Density { get { return _columnIndices.Length / (double)(Rows * Columns); } }


        #region Constructors

        /// <summary>
        /// Create a zero matrix of the specified dimensions.
        /// </summary>
        /// <param name="rows">The numebr of rows of the matrix.</param>
        /// <param name="columns">The number of columns of the matrix.</param>
        public CSRSparseMatrix(int rows, int columns) : base(MatrixStorageType.CSR, rows, columns)
        {
            if (rows <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(rows), ExceptionMessages.NonPositiveNotAllowed);
            }
            if (columns <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(columns), ExceptionMessages.NonPositiveNotAllowed);
            }

            _rowIndices = new int[rows + 1];
            _columnIndices = new int[0];
            _values = new T[0];
            _zero = new T();
        }

        /// <summary>
        /// Create a CSR sparse matrix using the number of columns, and the three arrays. 
        /// <p>There are some structural requirements of these arrays:</p>
        /// <ul>
        /// <li>The first element of <txt>rowIndices</txt> must be zero.</li>
        /// <li>The length of <txt>columnIndices</txt> and <txt>values</txt> must be equal.</li>
        /// <li>The last element of <txt>rowIndices</txt> must be equal to the length of <txt>columnIndices</txt> (and <txt>values</txt>).</li>
        /// <li><txt>columns</txt> must be greater than the largest element of <txt>columnIndices</txt>.</li>
        /// <li>The elements of <txt>rowIndices</txt> must be monotonically increasing.</li>
        /// </ul>
        /// </summary>
        /// <param name="columns">The number of columns of this matrix.</param>
        /// <param name="rowIndices">The row indices, which contain the start and end indexes of each row.</param>
        /// <param name="columnIndices">The column indices, which contain the column index for each non-zero element, in order.</param>
        /// <param name="values">The value of each non-zero element, in order.</param>
        public CSRSparseMatrix(int columns, int[] rowIndices, int[] columnIndices, T[] values) : base(MatrixStorageType.CSR)
        {
            if (rowIndices is null)
            {
                throw new ArgumentNullException(nameof(rowIndices));
            }
            if (columnIndices is null)
            {
                throw new ArgumentNullException(nameof(columnIndices));
            }
            if (values is null)
            {
                throw new ArgumentNullException(nameof(values));
            }

            if (columnIndices.Length != values.Length)
            {
                throw new ArgumentOutOfRangeException(nameof(values), "The length of the column index array does not match the number of values.");
            }
            if (rowIndices[0] != 0)
            {
                throw new ArgumentOutOfRangeException(nameof(rowIndices), "The first entry of rowIndices must be zero");
            }
            if (rowIndices[rowIndices.Length - 1] != columnIndices.Length)
            {
                throw new ArgumentOutOfRangeException(nameof(rowIndices), "The last entry of rowIndices must be equal to the length of columnIndices array.");
            }

            // these checks are O(n) and O(k) respectively - should avoid using if we are already sure of compliance
            for (int i = 1; i < rowIndices.Length; ++i)
            {
                if (rowIndices[i - 1] > rowIndices[i])
                {
                    throw new ArgumentOutOfRangeException(nameof(rowIndices), "rowIndices array is not monotically increasing.");
                }
            }
            for (int i = 0; i < columnIndices.Length; ++i)
            {
                if (columnIndices[i] >= columns)
                {
                    throw new ArgumentOutOfRangeException(nameof(columnIndices), $"columnIndices array contains an element that is outside the bounds of the matrix ({columnIndices[i]} > {columns}).");
                }
            }

            Rows = rowIndices.Length - 1;
            Columns = columns;

            _rowIndices = rowIndices;
            _columnIndices = columnIndices;
            _values = values;
            _zero = new T();
        }

        /// <summary>
        /// Create a CSR sparse matrix using a band matrix.
        /// </summary>
        /// <param name="matrix">The band matrix.</param>
        public CSRSparseMatrix(BandMatrix<T> matrix) : base(MatrixStorageType.CSR)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            Rows = matrix.Rows;
            Columns = matrix.Columns;
            _zero = new T();

            _rowIndices = new int[Rows + 1];

            List<int> columnIndices = new List<int>();
            List<T> values = new List<T>();

            for (int r = 0; r < Rows; ++r)
            {
                int colStart = Math.Max(0, r - matrix.LowerBandwidth),
                    colEnd = Math.Min(Rows - 1, r + matrix.UpperBandwidth);

                for (int c = colStart; c <= colEnd; ++c)
                {
                    T e = matrix[r, c];
                    if (!e.Equals(_zero))
                    {
                        columnIndices.Add(c);
                        values.Add(e);
                    }
                }

                _rowIndices[r + 1] = values.Count;
            }

            _columnIndices = columnIndices.ToArray();
            _values = values.ToArray();
        }

        /// <summary>
        /// Create a CSR sparse matrix using a block-diagonal matrix.
        /// </summary>
        /// <param name="matrix">The block-diagonal matrix.</param>
        public CSRSparseMatrix(BlockDiagonalMatrix<T> matrix, bool defrag = false) : base(MatrixStorageType.CSR)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            Rows = matrix.Rows;
            Columns = matrix.Columns;
            _zero = new T();

            int[] blockIndices = matrix.BlockIndices;
            DenseMatrix<T>[] blocks = matrix.Blocks;

            _rowIndices = new int[Rows + 1];

            int nz = 0;

            if (defrag)
            {
                for (int b = 0; b < blockIndices.Length; ++b)
                {
                    int block_offset = blockIndices[b] + 1;

                    DenseMatrix<T> block = blocks[b];
                    T[][] blockValues = block.Values;
                    int rows = block.Rows, cols = block.Columns, r, c;
                    for (r = 0; r < rows; ++r)
                    {
                        T[] row = blockValues[r];

                        // The number of non-zeroes in this row
                        int row_nz = 0;
                        for (c = 0; c < cols; ++c)
                        {
                            if (!row[c].Equals(_zero))
                            {
                                ++row_nz;
                            }
                        }

                        // Rows are unique - store the number of nz or row r in index [r + 1]
                        _rowIndices[r + block_offset] = row_nz;
                        nz += row_nz;
                    }
                }
            }
            else
            {
                for (int b = 0; b < blockIndices.Length; ++b)
                {
                    int dim = blocks[b].Rows, block_offset = blockIndices[b] + 1;
                    for (int r = 0; r < dim; ++r)
                    {
                        _rowIndices[block_offset + r] = dim;
                    }
                    nz += dim * dim;
                }
            }

            // Accumulate row indices
            for (int r = 0; r < Rows; ++r)
            {
                _rowIndices[r + 1] += _rowIndices[r];
            }

            // Initialze arrays
            _columnIndices = new int[nz];
            _values = new T[nz];

            int k = 0;
            for (int b = 0; b < blockIndices.Length; ++b)
            {
                int offset = blockIndices[b];
                DenseMatrix<T> block = blocks[b];

                T[][] blockValues = block.Values;
                int rows = block.Rows, cols = block.Columns, r, c;
                for (r = 0; r < rows; ++r)
                {
                    T[] row = blockValues[r];
                    for (c = 0; c < cols; ++c)
                    {
                        if (defrag && row[c].Equals(_zero))
                        {
                            continue;
                        }
                        _columnIndices[k] = offset + c;
                        _values[k] = row[c];
                        ++k;
                    }
                }
            }

        }
        
        /// <summary>
        /// Create a CSR sparse matrix from a CSC sparse matrix.
        /// </summary>
        /// <param name="matrix">A matrix in CSC storage format.</param>
        public CSRSparseMatrix(CSCSparseMatrix<T> matrix) : base(MatrixStorageType.CSR)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            Rows = matrix.Rows;
            Columns = matrix.Columns;
            _zero = new T();

            CSRHelper<T>.Transpose(matrix.ColumnIndices, matrix.RowIndices, matrix.Values, out _columnIndices, out _rowIndices, out _values, Columns, Rows);
        }

        /// <summary>
        /// Copy constructor
        /// </summary>
        /// <param name="matrix">Another CSR sparse matrix.</param>
        public CSRSparseMatrix(CSRSparseMatrix<T> matrix) : base(MatrixStorageType.CSR)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            Rows = matrix.Rows;
            Columns = matrix.Columns;
            _zero = new T();
            _rowIndices = matrix._rowIndices.Copy();
            _columnIndices = matrix._columnIndices.Copy();
            _values = matrix._values.Copy();
        }

        /// <summary>
        /// Create a CSR sparse matrix from a COO sparse matrix.
        /// </summary>
        /// <param name="matrix">A COO sparse matrix.</param>
        public CSRSparseMatrix(COOSparseMatrix<T> matrix) : base(MatrixStorageType.CSR)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            Rows = matrix.Rows;
            Columns = matrix.Columns;
            _zero = new T();

            int nnz = matrix.Values.Count;
            _values = new T[nnz];
            _columnIndices = new int[nnz];
            _rowIndices = new int[matrix.Rows + 1];

            bool sorted = true;
            MatrixEntry<T> prev = default;
            for (int i = 0; i < nnz; ++i)
            {
                MatrixEntry<T> e = matrix.Values[i];

                _rowIndices[e.RowIndex]++;
                _columnIndices[i] = e.ColumnIndex;
                _values[i] = e.Value;

                // Check sort
                if (sorted && i > 0 && prev.CompareTo(e) > 1)
                {
                    sorted = false;
                }
                prev = e;
            }

            // Shift
            for (int r = Rows; r > 0; --r)
            {
                _rowIndices[r] = _rowIndices[r - 1];
            }
            _rowIndices[0] = 0;

            // Accumulate row indices
            for (int r = 0; r < Rows; ++r)
            {
                _rowIndices[r + 1] += _rowIndices[r];
            }

            // Sort if not sorted
            if (!sorted)
            {
                for (int r = 0; r < Rows; ++r)
                {
                    int start = _rowIndices[r], end = _rowIndices[r + 1];
                    Array.Sort(_columnIndices, _values, start, end - start);
                }
            }
        }

        /// <summary>
        /// Create a CSR sparse matrix from a dense matrix.
        /// </summary>
        /// <param name="matrix">The dense matrix.</param>
        public CSRSparseMatrix(DenseMatrix<T> matrix) : base(MatrixStorageType.CSR)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            _zero = new T();
            Rows = matrix.Rows;
            Columns = matrix.Columns;


            List<T> values = new List<T>();
            List<int> columnIndices = new List<int>();

            _rowIndices = new int[matrix.Rows + 1];

            int rows = matrix.Rows, cols = matrix.Columns, i, j;
            int k = 1;

            for (i = 0; i < rows; ++i)
            {
                T[] row = matrix[i];
                for (j = 0; j < cols; ++j)
                {
                    if (!_zero.Equals(row[j]))
                    {
                        values.Add(row[j]);
                        columnIndices.Add(j);
                    }
                }
                _rowIndices[k++] = values.Count;
            }
            _columnIndices = columnIndices.ToArray();
            _values = values.ToArray();
        }

        /// <summary>
        /// Create a CSR sparse matrix from a diagonal matrix.
        /// </summary>
        /// <param name="matrix">The diagonal matrix</param>
        public CSRSparseMatrix(DiagonalMatrix<T> matrix) : base(MatrixStorageType.CSR)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            Rows = matrix.Rows;
            Columns = matrix.Columns;
            _zero = new T();

            _rowIndices = new int[Rows + 1];

            List<int> columnIndices = new List<int>();
            List<T> values = new List<T>();

            int min = Math.Min(Rows, Columns);
            for (int i = 0; i < min; ++i)
            {
                T e = matrix.DiagonalTerms[i];
                if (!e.Equals(_zero))
                {
                    columnIndices.Add(i);
                    values.Add(e);
                }
                _rowIndices[i + 1] = values.Count;
            }

            // Complete the rest of the row indexes
            for (int i = min; i < Rows;)
            {
                _rowIndices[++i] = values.Count;
            }

            _columnIndices = columnIndices.ToArray();
            _values = values.ToArray();
        }

        /// <summary>
        /// Create a CSR sparse matrix from a DOK sparse matrix.
        /// </summary>
        /// <param name="matrix">The DOK sparse matrix.</param>
        public CSRSparseMatrix(DOKSparseMatrix<T> matrix) : base(MatrixStorageType.CSR)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            Rows = matrix.Rows;
            Columns = matrix.Columns;

            int nnz = matrix.Values.Count;
            _rowIndices = new int[Rows + 1];
            _columnIndices = new int[nnz];
            _values = new T[nnz];
            _zero = new T();

            Dictionary<long, Dictionary<long, T>> byRow = matrix.GroupByRows();

            int row_index = 0, col_index = 0;
            for (int r = 0; r < Rows; ++r)
            {
                if (byRow.ContainsKey(r))
                {
                    Dictionary<long, T> row = byRow[r];

                    // Sorting is not technically required, however improves access performance 
                    // if all matrices are in sorted (canonical) form.
                    List<long> keys = new List<long>(row.Keys);
                    keys.Sort();

                    foreach (long key in keys)
                    {
                        _columnIndices[col_index] = (int)key;
                        _values[col_index] = row[key];
                        col_index++;
                    }
                    row_index += row.Count;
                }
                _rowIndices[r + 1] = row_index;
            }
        }

        /// <summary>
        /// Create a CSR sparse matrix using a Modified Compressed Sparse Row (MCSR) matrix.
        /// </summary>
        /// <param name="matrix">A matrix stored in MCSR format.</param>
        public CSRSparseMatrix(MCSRSparseMatrix<T> matrix) : base(MatrixStorageType.CSR)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            Rows = matrix.Rows;
            Columns = matrix.Columns;

            T[] values = matrix.Values;
            int[] indices = matrix.Indices;

            _rowIndices = new int[Rows + 1];
            _columnIndices = new int[values.Length - 1];
            _values = new T[values.Length - 1];

            int k = 0;
            for (int r = 0; r < Rows; ++r)
            {
                int r1 = r + 1;
                int row_start = indices[r], row_end = indices[r1];

                // nz values under the main diagonal
                int i = row_start;
                for (; i < row_end; ++i)
                {
                    int c = indices[i];
                    if (c > r)
                    {
                        break;
                    }
                    _columnIndices[k] = c;
                    _values[k] = values[i];
                    ++k;
                }

                // main diagonal term
                _columnIndices[k] = r;
                _values[k] = values[r];
                ++k;

                // nz values above the main diagonal
                for (; i < row_end; ++i)
                {
                    _columnIndices[k] = indices[i];
                    _values[k] = values[i];
                    ++k;
                }

                _rowIndices[r1] = k;
            }
        }

        /// <summary>
        /// Create a CSR sparse matrix using a Skyline matrix.
        /// </summary>
        /// <param name="matrix">The Skyline matrix</param>
        public CSRSparseMatrix(SKYSparseMatrix<T> matrix) : base(MatrixStorageType.CSR)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            Rows = matrix.Rows;
            Columns = matrix.Columns;
            _zero = new T();

            int[] indices = matrix.Indices;
            T[] values = matrix.Values;

            if (matrix.Type == SKYType.LOWER)
            {
                // Matrix already arranged by row - can just re-use row index and values index
                _rowIndices = indices.Copy();
                _values = values.Copy();

                _columnIndices = new int[_values.Length];
                for (int r = 0; r < Rows; ++r)
                {
                    int start = indices[r], end = indices[r + 1], c = r - (end - start) + 1;
                    for (int i = start; c <= r; ++c, ++i)
                    {
                        _columnIndices[i] = c;
                    }
                }
            }
            else if (matrix.Type == SKYType.UPPER)
            {
                // As the values are grouped by column, this require a row-count first
                // store rowIndices[r + 1] = # nz in row r
                _rowIndices = new int[Rows + 1];
                for (int c = 0; c < Columns; ++c)
                {
                    int start = indices[c], end = indices[c + 1];
                    for (int r = c - (end - start) + 1; r <= c; )
                    {
                        ++r;
                        _rowIndices[r]++;
                    }
                }

                // Accumulate the row indices
                for (int r = 0; r < Rows; ++r)
                {
                    _rowIndices[r + 1] += _rowIndices[r];
                }

                // Temporary storage for the number of nz elements added so far in each row
                int[] counts = new int[Rows];

                int len = values.Length;
                _values = new T[len];
                _columnIndices = new int[len];
                for (int c = 0; c < Columns; ++c)
                {
                    int start = indices[c], end = indices[c + 1], r = c - (end - start) + 1;
                    for (int i = start; r <= c; ++r, ++i)
                    {
                        int index = _rowIndices[r] + counts[r];
                        _values[index] = values[i];
                        _columnIndices[index] = c;
                        counts[r]++;
                    }
                }
            }
            else if (matrix.Type == SKYType.SYMMETRIC)
            {
                _rowIndices = new int[Rows + 1];

                // Set _rowIndices[r + 1] = # nz in row r
                for (int r = 0; r < Rows; ++r)
                {
                    int start = indices[r], end = indices[r + 1], rowCount = end - start;
                    
                    // the lower-diagonal and diagonal count
                    _rowIndices[r + 1] += rowCount; 

                    // The upper count
                    for (int c = r - rowCount + 1; c < r; ++c)
                    {
                        _rowIndices[c + 1]++;
                    }
                }

                // Accumulate row indices
                for (int r = 0; r < Rows; ++r)
                {
                    _rowIndices[r + 1] += _rowIndices[r];
                }

                // Initialize storage
                _values = new T[_rowIndices[Rows]];
                _columnIndices = new int[_rowIndices[Rows]];

                // The number of nz encountered in each row, so far
                int[] counts = new int[Rows];

                // Fill in all lower-diagonal and diagonal terms first
                for (int r = 0; r < Rows; ++r)
                {
                    int start = indices[r], end = indices[r + 1], nz = end - start;

                    Array.Copy(values, start, _values, _rowIndices[r], nz);
                    for (int c = r - nz + 1, i = _rowIndices[r]; c <= r; ++c, ++i)
                    {
                        _columnIndices[i] = c;
                    }
                    counts[r] = nz;
                }

                // Fill in all strictly upper terms
                for (int c = 0; c < Columns; ++c)
                {
                    int start = indices[c], end = indices[c + 1], r = c - (end - start) + 1;
                    for (int i = start; r < c; ++r, ++i)
                    {
                        int index = _rowIndices[r] + counts[r];
                        _values[index] = values[i];
                        _columnIndices[index] = c;
                        counts[r]++;
                    }
                }
            }
            else
            {
                throw new NotSupportedException(nameof(matrix.Type));
            }
        }

        public CSRSparseMatrix(Matrix<T> matrix) : base(MatrixStorageType.CSR)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            Rows = matrix.Rows;
            Columns = matrix.Columns;
            _zero = new T();

            int nz = matrix.SymbolicNonZeroCount;
            _rowIndices = new int[Rows + 1];
            _columnIndices = new int[nz];
            _values = new T[nz];

            // 1st pass is to count the number of non-zero entries in each row
            IEnumerable<MatrixEntry<T>> values = matrix.NonZeroEntries;
            foreach (MatrixEntry<T> e in values)
            {
                _rowIndices[e.RowIndex]++;
            }

            // Shift and accumulate
            for (int r = Rows - 1; r >= 0; --r)
            {
                _rowIndices[r + 1] = _rowIndices[r];
            }
            _rowIndices[0] = 0;
            for (int r = 0; r < Rows; ++r)
            {
                _rowIndices[r + 1] += _rowIndices[r];
            }

            int[] next = new int[Rows];
            Array.Copy(_rowIndices, next, Rows);

            foreach (MatrixEntry<T> e in values)
            {
                int index = next[e.RowIndex]++;
                _columnIndices[index] = e.ColumnIndex;
                _values[index] = e.Value;
            }

            // Check sort order in O(k) time, and if unsorted, spend 
            // O(k log(k)) time sorting the row
            for (int r = 0; r < Rows; ++r)
            {
                int start = _rowIndices[r], end = _rowIndices[r + 1];
                bool sorted = true;
                for (int i = start + 1; i < end; ++i)
                {
                    if (_columnIndices[i] < _columnIndices[i - 1])
                    {
                        sorted = false;
                        break;
                    }
                }
                if (!sorted)
                {
                    Array.Sort(_columnIndices, _values, start, end - start);
                }
            }
        }

        /// <summary>
        /// Create a sparse matrix with the specified dimensions, and using the values provided by 
        /// the sparse vector.
        /// The values will be inserted into the matrix in either row-major or column major order.
        /// The length of the vector must be at least equal to rows * columns. If rows * columns is 
        /// less than the length of the vector, only the first (rows * columns) entries will be used.
        /// </summary>
        /// <param name="rows"></param>
        /// <param name="columns"></param>
        /// <param name="values"></param>
        /// <param name="rowMajorOrder"></param>
        public CSRSparseMatrix(int rows, int columns, SparseVector<T> values, bool rowMajorOrder = true) : base(MatrixStorageType.CSR)
        {
            if (rows <= 0) throw new ArgumentOutOfRangeException(nameof(rows), ExceptionMessages.NonPositiveNotAllowed);
            if (columns <= 0) throw new ArgumentOutOfRangeException(nameof(columns), ExceptionMessages.NonPositiveNotAllowed);
            if (values is null) throw new ArgumentNullException(nameof(values));
            if (rows * columns > values.Dimension) throw new ArgumentOutOfRangeException(nameof(values), $"The length of the vector exceeds {nameof(rows)} * {nameof(columns)}");

            int[] vect_indices = values.Indices;
            T[] vect_values = values.Values;

            int size = rows * columns,
                vnnz = vect_values.Length;

            Rows = rows;
            Columns = columns;
            _rowIndices = new int[rows + 1];

            if (rowMajorOrder)
            {

                for (int i = 0; i < vnnz; ++i)
                {
                    int vi = vect_indices[i];
                    if (vi >= size) break;

                    int r = vi / columns;
                    _rowIndices[r]++;
                }

                // Shift and accumulate
                for (int r = rows; r > 0; --r)
                {
                    _rowIndices[r] = _rowIndices[r - 1];
                }
                _rowIndices[0] = 0;
                for (int r = 0; r < rows; ++r)
                {
                    _rowIndices[r + 1] += _rowIndices[r];
                }

                // Insert the values
                int nnz = _rowIndices[rows];
                _columnIndices = new int[nnz];
                _values = new T[nnz];

                for (int i = 0; i < nnz; ++i)
                {
                    _columnIndices[i] = vect_indices[i] % columns;
                    _values[i] = vect_values[i]; // straight copy
                }
            }
            else
            {
                // Column major order insertion
                for (int i = 0; i < vnnz; ++i)
                {
                    int vi = vect_indices[i];
                    if (vi >= size) break;
                    _rowIndices[vi % rows]++;
                }

                // Shift and accumulate
                for (int r = rows; r > 0; --r)
                {
                    _rowIndices[r] = _rowIndices[r - 1];
                }
                _rowIndices[0] = 0;
                for (int r = 0; r < rows; ++r)
                {
                    _rowIndices[r + 1] += _rowIndices[r];
                }

                // Copy rowIndices to form next array
                int[] next = _rowIndices.Copy();

                // Insert values according to next array
                int nnz = _rowIndices[rows];
                _columnIndices = new int[nnz];
                _values = new T[nnz];

                for (int i = 0; i < nnz; ++i)
                {
                    int vi = vect_indices[i],
                        r = vi % rows,
                        c = vi / rows,
                        index = next[r]++;
                    _columnIndices[index] = c;
                    _values[index] = vect_values[i];
                }
            }
        }

        #endregion

        /// <summary>
        /// Returns the leading (main) diagonal of this matrix.
        /// </summary>
        /// <returns>The leading diagonal.</returns>
        public BigSparseVector<T> BigLeadingDiagonal()
        {
            // This ends up being O(max(n, k))
            Dictionary<long, T> diag = new Dictionary<long, T>();
            for (int r = 0; r < Rows; ++r)
            {
                int row_end = _rowIndices[r + 1];
                for (int i = _rowIndices[r]; i < row_end; ++i)
                {
                    if (_columnIndices[i] == r)
                    {
                        diag[r] = _values[i];
                    }
                }
            }
            return new BigSparseVector<T>(Math.Min(Rows, Columns), diag);
        }
        public override T[] LeadingDiagonal()
        {
            return CSRHelper<T>.LeadingDiagonal(_rowIndices, _columnIndices, _values, Math.Min(Rows, Columns));
        }

        /// <summary>
        /// Returns a diagonal from this matrix. 
        /// <p>If <txt>position</txt> is 0, the leading (main) diagonal is returned.</p>
        /// <p>If <txt>position</txt> is positive, the <txt>position</txt>-th upper diagonal is returned.</p>
        /// <p>If <txt>position</txt> is negative, the |<txt>position</txt>|-th lower diagonal is returned.</p>
        /// </summary>
        /// <param name="position">The position of the diagonal to return.</param>
        /// <returns>The diagonal of this matrix.</returns>
        public BigSparseVector<T> Diagonal(int position)
        {
            if (position >= 0)
            {
                Dictionary<long, T> diagonal = new Dictionary<long, T>();
                for (int r = 0; r < Rows; ++r)
                {
                    int row_end = _rowIndices[r + 1];
                    for (int i = _rowIndices[r]; i < row_end; ++i)
                    {
                        if (_columnIndices[i] - r == position)
                        {
                            // r <= c
                            diagonal[r] = _values[i];
                        }
                    }
                }
                return new BigSparseVector<T>(Math.Min(Columns - position, Rows), diagonal);
            }
            else
            {
                Dictionary<long, T> diagonal = new Dictionary<long, T>();
                for (int r = 0; r < Rows; ++r)
                {
                    int row_end = _rowIndices[r + 1];
                    for (int i = _rowIndices[r]; i < row_end; ++i)
                    {
                        if (_columnIndices[i] - r == position)
                        {
                            // r > c
                            diagonal[_columnIndices[i]] = _values[i];
                        }
                    }
                }
                return new BigSparseVector<T>(Math.Min(Columns, Rows + position), diagonal);
            }
        }

        /// <summary>
        /// Returns the transpose of this matrix, without changing this matrix.
        /// </summary>
        public CSRSparseMatrix<T> Transpose()
        {
            CSRHelper<T>.Transpose(_rowIndices, _columnIndices, _values, out int[] rowIndices, out int[] colIndices, out T[] values, Rows, Columns);
            return new CSRSparseMatrix<T>(Rows, colIndices, rowIndices, values);
        }

        public override int Count(Predicate<T> predicate)
        {
            if (predicate is null) throw new ArgumentNullException(nameof(predicate));

            int len = _values.Length, i, count = 0;
            for (i = 0; i < len; ++i)
            {
                if (predicate(_values[i]))
                {
                    ++count;
                }
            }
            return count;
        }

        /// <summary>
        /// Returns the number of (symbolic) non-zero entries in each row. This will treat entries 
        /// as non-zero as long as it is stored in the matrix, even if the entry is numerically zero.
        /// </summary>
        /// <returns>The symbolic row count of this matrix.</returns>
        public DenseVector<int> RowCountsSymbolic()
        {
            return CSRHelper<T>.RowCountSymbolic(_rowIndices, Rows);
        }

        /// <summary>
        /// Returns the number of (symbolic) non-zero entries in each column. This will treat entries 
        /// as non-zero as long as it is stored in the matrix, even if the entry is numerically zero.
        /// </summary>
        /// <returns></returns>
        public DenseVector<int> ColumnCountsSymbolic()
        {
            return CSRHelper<T>.ColumnCountSymbolic(_columnIndices, Columns);
        }

        /// <summary>
        /// Returns a row of this matrix as a big sparse vector.
        /// </summary>
        /// <param name="rowIndex">The index of the row to extract.</param>
        /// <returns>A row of this matrix.</returns>
        public BigSparseVector<T> BigRow(int rowIndex)
        {
            return CSRHelper<T>.BigSparseRow(_rowIndices, _columnIndices, _values, Rows, Columns, rowIndex);
        }

        /// <summary>
        /// Returns a row of this matrix as a sparse vector.
        /// </summary>
        /// <param name="rowIndex">The index of the row to extract.</param>
        /// <returns>A row of this matrix, as a sparse vector.</returns>
        public override SparseVector<T> SparseRow(int rowIndex)
        {
            return CSRHelper<T>.SparseRow(_rowIndices, _columnIndices, _values, Rows, Columns, rowIndex);
        }

        /// <summary>
        /// Returns a row of this matrix as a dense vector.
        /// </summary>
        /// <param name="rowIndex">The index of the row to return.</param>
        /// <returns>A row of this matrix.</returns>
        public override DenseVector<T> Row(int rowIndex)
        {
            return CSRHelper<T>.DenseRow(_rowIndices, _columnIndices, _values, _zero, Rows, Columns, rowIndex);
        }

        /// <summary>
        /// Returns a column of this matrix as a big sparse vector.
        /// </summary>
        /// <param name="columnIndex">The index of the column to extract.</param>
        /// <returns>A column of this matrix.</returns>
        public BigSparseVector<T> BigColumn(int columnIndex)
        {
            return CSRHelper<T>.BigSparseColumn(_rowIndices, _columnIndices, _values, Rows, Columns, columnIndex);
        }

        /// <summary>
        /// Returns a column of this matrix as a sparse vector.
        /// </summary>
        /// <param name="columnIndex">The index of the column to extract.</param>
        /// <returns>A column of this matrix.</returns>
        public override SparseVector<T> SparseColumn(int columnIndex)
        {
            return CSRHelper<T>.SparseColumn(_rowIndices, _columnIndices, _values, Rows, Columns, columnIndex);
        }

        /// <summary>
        /// Returns a column of this matrix as a dense vector.
        /// </summary>
        /// <param name="columnIndex">The index of the column to return.</param>
        /// <returns>A column of this matrix.</returns>
        public override DenseVector<T> Column(int columnIndex)
        {
            return CSRHelper<T>.DenseColumn(_rowIndices, _columnIndices, _values, _zero, Rows, Columns, columnIndex);
        }

        /// <summary>
        /// Returns a submatrix of this matrix at the specified coordinates
        /// </summary>
        /// <param name="firstRow">The index of the first row to include in the submatrix.</param>
        /// <param name="firstColumn">The index of the first column to include in the submatrix.</param>
        /// <param name="rows">The number of rows to include in the submatrix.</param>
        /// <param name="columns">The number of columns to include in the submatrix.</param>
        /// <returns>The submatrix.</returns>
        public CSRSparseMatrix<T> Submatrix(int firstRow, int firstColumn, int rows, int columns)
        {
            CSRHelper<T>.Submatrix(_rowIndices, _columnIndices, _values, Rows, Columns, firstRow, firstColumn, rows, columns,
                out int[] rowIndices, out int[] columnIndices, out T[] values);

            return new CSRSparseMatrix<T>(columns, rowIndices, columnIndices, values);
        }

        public override Matrix<T> Select(int[] rowIndices, int[] columnIndices)
        {
            if (rowIndices is null)
            {
                throw new ArgumentNullException(nameof(rowIndices));
            }
            if (columnIndices is null)
            {
                throw new ArgumentNullException(nameof(columnIndices));
            }

            ThrowHelper.AssertCollectionInRange(rowIndices, 0, Rows, nameof(rowIndices));
            ThrowHelper.AssertCollectionInRange(columnIndices, 0, Columns, nameof(columnIndices));

            CSRHelper<T>.Select(rowIndices, columnIndices, _rowIndices, _columnIndices, _values, Rows, Columns,
                out int[] rIndices, out int[] cIndices, out T[] values);

            return new CSRSparseMatrix<T>(columnIndices.Length, rIndices, cIndices, values);
        }

        public override Vector<T> SelectAsVector(int rowIndex, int[] columnIndices)
        {
            if (rowIndex < 0 || rowIndex >= Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(rowIndex));
            }
            if (columnIndices is null)
            {
                throw new ArgumentNullException(nameof(columnIndices));
            }
            ThrowHelper.AssertCollectionInRange(columnIndices, 0, Columns, nameof(columnIndices));

            return CSRHelper<T>.Select(rowIndex, columnIndices, _rowIndices, _columnIndices, _values, Columns);
        }

        public override Vector<T> SelectAsVector(int[] rowIndices, int columnIndex)
        {
            if (rowIndices is null)
            {
                throw new ArgumentNullException(nameof(rowIndices));
            }
            ThrowHelper.AssertCollectionInRange(rowIndices, 0, Rows, nameof(rowIndices));
            if (columnIndex < 0 || columnIndex >= Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(columnIndex));
            }

            return CSRHelper<T>.Select(rowIndices, columnIndex, _rowIndices, _columnIndices, _values);
        }

        /// <summary>
        /// Overwrites a row of this matrix with the values of a sparse vector.
        /// </summary>
        /// <param name="rowIndex">The index of the row to be overwritten.</param>
        /// <param name="row">The values of the new row, stored in a big sparse vector.</param>
        public void SetRow(int rowIndex, BigSparseVector<T> row)
        {
            if (row is null)
            {
                throw new ArgumentNullException(nameof(row));
            }
            if (rowIndex < 0 || rowIndex >= Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(rowIndex));
            }
            if (row.Dimension != Columns)
            {
                throw new ArgumentOutOfRangeException("The number of columns of this matrix does not match the dimension of the vector.");
            }
            CSRHelper<T>.SetRow(_rowIndices, ref _columnIndices, ref _values, Rows, rowIndex, row);
        }

        /// <summary>
        /// Overwrites a row of this matrix with the values of a sparse vector.
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <param name="row"></param>
        public void SetRow(int rowIndex, SparseVector<T> row)
        {
            if (row is null)
            {
                throw new ArgumentNullException(nameof(row));
            }
            if (rowIndex < 0 || rowIndex >= Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(rowIndex));
            }
            if (row.Dimension != Columns)
            {
                throw new ArgumentOutOfRangeException("The number of columns of this matrix does not match the dimension of the vector.");
            }
            CSRHelper<T>.SetRow(_rowIndices, ref _columnIndices, ref _values, Rows, rowIndex, row);
        }

        /// <summary>
        /// Overwrites a row of thie matrix with the values of a dense vector.
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <param name="row"></param>
        public void SetRow(int rowIndex, DenseVector<T> row)
        {
            if (row is null)
            {
                throw new ArgumentNullException(nameof(row));
            }
            if (rowIndex < 0 || rowIndex >= Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(rowIndex));
            }
            if (row.Dimension != Columns)
            {
                throw new ArgumentOutOfRangeException("The number of columns of this matrix does not match the dimension of the vector.");
            }

            CSRHelper<T>.SetRow(_rowIndices, ref _columnIndices, ref _values, Rows, rowIndex, row, _zero);
        }

        /// <summary>
        /// Fill a row with a value. This will change all entries in this row, including the 
        /// symbolically zero entries.
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <param name="value"></param>
        public void SetRow(int rowIndex, T value)
        {
            if (rowIndex < 0 || rowIndex >= Rows)
            {
                throw new ArgumentNullException(nameof(rowIndex));
            }
            if (value is null)
            {
                throw new ArgumentNullException(nameof(value));
            }

            if (value.Equals(_zero))
            {
                ClearRow(rowIndex);
                return;
            }

            int row_start = _rowIndices[rowIndex], row_end = _rowIndices[rowIndex + 1];

            // The length of the new array
            int len = _values.Length + (row_end - row_start) + Columns;
            int[] columnIndices = new int[len];
            T[] values = new T[len];

            // Copy before
            Array.Copy(_columnIndices, columnIndices, row_start);
            Array.Copy(_values, values, row_start);

            // Insert values
            for (int c = 0; c < Columns; ++c)
            {
                int index = row_start + c;
                _columnIndices[index] = c;
                _values[index] = value;
            }

            // Copy after 
            Array.Copy(_columnIndices, row_end, columnIndices, row_start + Columns, _values.Length - row_end);
            Array.Copy(_values, row_end, values, row_start + Columns, _values.Length - row_end);

            // Update the row indices
            int offset = Columns - (row_end - row_start);
            for (int r = rowIndex + 1; r <= Rows; ++r)
            {
                _rowIndices[r] += offset;
            }

            _columnIndices = columnIndices;
            _values = values;
        }

        /// <summary>
        /// Overwrites a column of this matrix with the values of a vector.
        /// </summary>
        /// <param name="columnIndex">The index of the column to overwrite.</param>
        /// <param name="column">The values of the new column, stored in a big sparse vector.</param>
        public void SetColumn(int columnIndex, BigSparseVector<T> column)
        {
            if (column is null)
            {
                throw new ArgumentNullException(nameof(column));
            }
            if (columnIndex < 0 || columnIndex >= Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(columnIndex));
            }
            if (Rows != column.Dimension)
            {
                throw new InvalidOperationException("The number of rows of this matrix does not match the dimension of the column vector.");
            }

            CSRHelper<T>.SetColumn(_rowIndices, ref _columnIndices, ref _values, Rows, columnIndex, _zero, column);
        }

        /// <summary>
        /// Overwrites a column of this matrix with the values of a sparse vector.
        /// If the vector's dimension exceeds the number of rows of this matrix, only the first
        /// entries will be copied from the vector.
        /// If the vector's dimension is less than the number of rows of this matrix, then only
        /// the top part of the matrix's column will be modified.
        /// </summary>
        /// <param name="columnIndex">The index of the column to overwrite.</param>
        /// <param name="column">The sparse column vector of values to overwrite with.</param>
        public void SetColumn(int columnIndex, SparseVector<T> column)
        {
            if (column is null)
            {
                throw new ArgumentNullException(nameof(column));
            }
            if (columnIndex < 0 || columnIndex >= Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(columnIndex));
            }
            if (Rows != column.Dimension)
            {
                throw new InvalidOperationException("The number of rows of this matrix does not match the dimension of the column vector.");
            }

            CSRHelper<T>.SetColumn(_rowIndices, ref _columnIndices, ref _values, Rows, columnIndex, _zero, column);
        }

        /// <summary>
        /// Overwrites a column of this matrix with the values of a dense vector.
        /// </summary>
        /// <param name="columnIndex">The index of the column to overwrite.</param>
        /// <param name="column">The column vector to overwrite with.</param>
        public void SetColumn(int columnIndex, DenseVector<T> column)
        {
            if (columnIndex < 0 || columnIndex >= Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(columnIndex));
            }

            if (column is null)
            {
                throw new ArgumentNullException(nameof(column));
            }

            int dim = Math.Min(Rows, column.Dimension);

            // Count the number of additional entries required
            int nnz = _values.Length;
            for (int r = 0; r < dim; ++r)
            {
                int begin = _rowIndices[r], finish = _rowIndices[r + 1];
                if (Array.BinarySearch(_columnIndices, begin, finish - begin, columnIndex) < 0)
                {
                    ++nnz; // requires insertion
                }
            }

            // New arrays
            int[] columnIndices = new int[nnz];
            T[] values = new T[nnz];
            T[] vect_values = column.Values;

            int start, end = 0, k = 0;
            for (int r = 0; r < dim; ++r)
            {
                start = end;
                end = _rowIndices[r + 1];

                bool found = false;
                for (int i = start; i < end; ++i)
                {
                    int c = _columnIndices[i];

                    // insert prior to next
                    if (!found && c > columnIndex)
                    {
                        values[k] = vect_values[r];
                        columnIndices[k] = columnIndex;
                        ++k;
                        found = true;
                    }

                    if (c == columnIndex)
                    {
                        values[k] = vect_values[r];
                        found = true;
                    }
                    else
                    {
                        values[k] = _values[i];
                    }
                    columnIndices[k] = c;
                    ++k;
                }

                // Append at end
                if (!found)
                {
                    values[k] = vect_values[r];
                    columnIndices[k] = columnIndex;
                    ++k;
                }

                _rowIndices[r + 1] = k;
            }

            // copy the rest of the rows over
            if (dim < Rows)
            {
                Array.Copy(_values, end, values, k, _values.Length - end);
                Array.Copy(_columnIndices, end, columnIndices, k, _columnIndices.Length - end);

                int offset = k - end;
                for (int r = dim + 1; r < Rows; ++r)
                {
                    _rowIndices[r] += offset;
                }
            }

            _columnIndices = columnIndices;
            _values = values;
        }

        /// <summary>
        /// Fill a column with the same value. This will change all entries in the column, including 
        /// entries that are currently zero.
        /// </summary>
        /// <param name="columnIndex">The index of the column to modify.</param>
        /// <param name="value">The value to set all column entries to.</param>
        public void SetColumn(int columnIndex, T value)
        {
            if (columnIndex < 0 || columnIndex >= Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(columnIndex));
            }
            if (value is null)
            {
                throw new ArgumentNullException(nameof(value));
            }

            if (value.Equals(_zero))
            {
                ClearColumn(columnIndex);
                return;
            }

            // Count the number of additional entries required
            int nnz = _values.Length;
            for (int r = 0; r < Rows; ++r)
            {
                int begin = _rowIndices[r], finish = _rowIndices[r + 1];
                if (Array.BinarySearch(_columnIndices, begin, finish - begin, columnIndex) < 0)
                {
                    ++nnz; // requires insertion
                }
            }

            // New arrays
            int[] columnIndices = new int[nnz];
            T[] values = new T[nnz];

            int start, end = 0, k = 0;
            for (int r = 0; r < Rows; ++r)
            {
                start = end;
                end = _rowIndices[r + 1];

                bool found = false;
                for (int i = start; i < end; ++i)
                {
                    int c = _columnIndices[i];

                    // insert prior to next
                    if (!found && c > columnIndex)
                    {
                        values[k] = value;
                        columnIndices[k] = columnIndex;
                        ++k;
                        found = true;
                    }

                    if (c == columnIndex)
                    {
                        values[k] = value;
                        found = true;
                    }
                    else
                    {
                        values[k] = _values[i];
                    }
                    columnIndices[k] = c;
                    ++k;
                }

                if (!found)
                {
                    values[k] = value;
                    columnIndices[k] = columnIndex;
                    ++k;
                }

                _rowIndices[r + 1] = k;
            }

            _columnIndices = columnIndices;
            _values = values;
        }

        /// <summary>
        /// Sets all entries that satisfy a predicate to a new value. 
        /// </summary>
        /// <param name="predicate">A predicate returning true for values to be modified.</param>
        /// <param name="value">The value to set to.</param>
        public void Set(Predicate<T> predicate, T value, bool defrag = false)
        {
            if (predicate is null)
            {
                throw new ArgumentNullException(nameof(predicate));
            }
            if (value is null)
            {
                throw new ArgumentNullException(nameof(value));
            }

            if (value.Equals(_zero))
            {
                Clear(e => predicate(e.Value), defrag);
                return;
            }

            // Simple case - zeroes are not altered
            if (!predicate(_zero))
            {
                for (int i = 0; i < _values.Length; ++i)
                {
                    if (predicate(_values[i])) _values[i] = value;
                }
            }

            else
            {
                throw new NotSupportedException("Resulting matrix is not sparse.");
            }
        }

        public override void Clear()
        {
            Clear(true);
        }

        /// <summary>
        /// Clears all entries from this matrix.
        /// </summary>
        public void Clear(bool defrag)
        {
            for (int r = 0; r < Rows; ++r)
            {
                _rowIndices[r] = 0;
            }

            if (defrag)
            {
                _values = new T[0];
                _columnIndices = new int[0];
            }
        }

        public override void Clear(Predicate<T> select)
        {
            if (select is null)
            {
                throw new ArgumentNullException(nameof(select));
            }
            Clear(e => select(e.Value), true);
        }

        /// <summary>
        /// Clears a row of this matrix, at the specified row index.
        /// </summary>
        /// <param name="rowIndex">The index of the row to clear.</param>
        public override void ClearRow(int rowIndex)
        {
            CSRHelper<T>.ClearRow(_rowIndices, ref _columnIndices, ref _values, Rows, rowIndex);
        }

        /// <summary>
        /// Clears a column of this matrix, at the specified column index.
        /// </summary>
        /// <param name="columnIndex">The index of the column to remove from this matrix.</param>
        public override void ClearColumn(int columnIndex)
        {
            CSRHelper<T>.ClearColumn(_rowIndices, _columnIndices, _values, Rows, Columns, columnIndex);
        }

        public override void ClearSubmatrix(int rowIndex, int columnIndex, int rows, int columns)
        {
            ClearSubmatrix(rowIndex, columnIndex, rows, columns, true);
        }

        /// <summary>
        /// Clears a submatrix block from this matrix.
        /// </summary>
        /// <param name="rowIndex">The index of the first row to clear.</param>
        /// <param name="columnIndex">The index of the first column to clear.</param>
        /// <param name="rows">The number of rows to clear.</param>
        /// <param name="columns">The number of columns to clear.</param>
        /// <param name="defrag">
        /// If <txt>true</txt>, the matrix storage will be shrunk after the values are cleared.
        /// </param>
        public void ClearSubmatrix(int rowIndex, int columnIndex, int rows, int columns, bool defrag)
        {
            if (rowIndex < 0 || rowIndex >= Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(rowIndex));
            }
            if (columnIndex < 0 || columnIndex >= Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(columnIndex));
            }
            if (rows < 0 || rowIndex + rows >= Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(rows));
            }
            if (columns < 0 || columnIndex + columns >= Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(columns));
            }

            // Count the number of elements to remove for defrag
            int rowEnd = rowIndex + rows, colEnd = columnIndex + columns, remove = 0;
            for (int r = rowIndex; r < rowEnd; ++r)
            {
                int row_end = _rowIndices[r + 1];
                for (int i = _rowIndices[r]; i < row_end; ++i)
                {
                    int c = _columnIndices[i];
                    if (c < columnIndex) continue;
                    if (c >= colEnd) break;

                    if (defrag)
                    {
                        remove++;
                    }
                    else
                    {
                        _values[i] = _zero;
                    }
                }
            }

            if (defrag)
            {
                int len = _columnIndices.Length - remove, k = _rowIndices[rowIndex];
                int[] columnIndices = new int[len];
                T[] values = new T[len];

                // Copy the first columns
                Array.Copy(_columnIndices, 0, columnIndices, 0, k);
                Array.Copy(_values, 0, values, 0, k);

                int row_begin, row_finish = _rowIndices[rowIndex];
                for (int r = rowIndex; r < rowEnd; ++r)
                {
                    row_begin = row_finish;
                    row_finish = _rowIndices[r + 1];

                    int i = row_begin;

                    // Find the first column index
                    while (i < row_finish && _columnIndices[i] < columnIndex) ++i;

                    int n = i - row_begin;
                    Array.Copy(_columnIndices, row_begin, columnIndices, k, n);
                    Array.Copy(_values, row_begin, values, k, n);
                    k += n;

                    // Find the last column index
                    while (i < row_finish && _columnIndices[i] < colEnd) ++i;

                    n = row_finish - i;
                    Array.Copy(_columnIndices, i, columnIndices, k, n);
                    Array.Copy(_values, i, values, k, n);
                    k += n;

                    _rowIndices[r + 1] = k;
                }

                // Copy the last columns
                Array.Copy(_columnIndices, row_finish, columnIndices, k, _columnIndices.Length - row_finish);
                Array.Copy(_values, row_finish, values, k, _values.Length - row_finish);

                // Update the row indices
                for (int r = rowEnd; r < Rows; ++r)
                {
                    row_begin = row_finish;
                    row_finish = _rowIndices[r + 1];

                    _rowIndices[r + 1] = (row_finish - row_begin) + _rowIndices[r];
                }

                _columnIndices = columnIndices;
                _values = values;
            }
        }

        /// <summary>
        /// Clears a diagonal from this matrix. If the diagonal index $n$ is positive, then the $n$-th 
        /// upper diagonal will be cleared. If negative, then the $|n|$-th lower diagonal will be cleared. 
        /// If zero, then the main diagonal will be cleared.
        /// </summary>
        /// <param name="diagonalIndex">The index of the diagonal to clear.</param>
        /// <param name="defrag">Specified whether the resize the matrix storage for lower memory consumption.</param>s
        public void ClearDiagonal(int diagonalIndex = 0, bool defrag = false)
        {
            if (diagonalIndex > 0 && diagonalIndex >= Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(diagonalIndex));
            }
            if (diagonalIndex < 0 && Math.Abs(diagonalIndex) >= Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(diagonalIndex));
            }

            int remove = 0;
            for (int r = 0; r < Rows; ++r)
            {
                int c = r + diagonalIndex;
                if (c < 0) continue;
                if (c >= Columns) break;

                int index = Array.BinarySearch(_columnIndices, _rowIndices[r], _rowIndices[r + 1] - _rowIndices[r], c);
                if (index >= 0)
                {
                    if (defrag) remove++;
                    else
                    {
                        _values[index] = _zero;
                    }
                }
            }

            if (defrag)
            {
                int len = _columnIndices.Length - remove;
                int[] columnIndices = new int[len];
                T[] values = new T[len];

                int row_start, row_end = 0, k = 0;
                for (int r = 0; r < Rows; ++r)
                {
                    row_start = row_end;
                    row_end = _rowIndices[r + 1];

                    int c = r + diagonalIndex;
                    int index = -1;
                    if (0 <= c && c < Columns)
                    {
                        index = Array.BinarySearch(_columnIndices, row_start, row_end - row_start, c);
                    }

                    if (index >= 0)
                    {
                        int len1 = index - row_start, len2 = row_end - index - 1;
                        Array.Copy(_columnIndices, row_start, columnIndices, k, len1);
                        Array.Copy(_values, row_start, values, k, len1);
                        k += len1;

                        Array.Copy(_columnIndices, index + 1, columnIndices, k, len2);
                        Array.Copy(_values, index + 1, values, k, len2);
                        k += len2;
                    }
                    else
                    {
                        int l = row_end - row_start;
                        Array.Copy(_columnIndices, row_start, columnIndices, k, l);
                        Array.Copy(_values, row_start, values, k, l);
                        k += l;
                    }

                    _rowIndices[r + 1] = k;
                }

                _columnIndices = columnIndices;
                _values = values;
            }
        }

        public override void ClearLower()
        {
            Clear(e => e.RowIndex > e.ColumnIndex);
        }

        public override void ClearUpper()
        {
            Clear(e => e.RowIndex < e.ColumnIndex);
        }

        /// <summary>
        /// Clears all entries that satisfy a predicate from this matrix. Only existing non-zero values 
        /// will be modified.
        /// </summary>
        /// <param name="predicate">The predicate to decide whether an entry should be removed.</param>
        /// <param name="defrag">If true, the matrix storage will be resized after removing the entries.</param>
        public void Clear(Predicate<MatrixEntry<T>> predicate, bool defrag = false)
        {
            CSRHelper<T>.Clear(_rowIndices, ref _columnIndices, ref _values, Rows, predicate, defrag);
        }

        /// <summary>
        /// Permute the rows of this matrix using the specified permutation vector.
        /// </summary>
        /// <param name="permutations">The permutation vector.</param>
        public void PermuteRows(int[] permutations)
        {
            if (permutations is null)
            {
                throw new ArgumentNullException(nameof(permutations));
            }
            if (permutations.Length != Rows)
            {
                throw new InvalidOperationException("The length of rowPermutations does not match the number of rows of this matrix.");
            }
            if (!permutations.IsPermutation())
            {
                throw new ArgumentException("rowPermutations is not a permutation vector.");
            }

            int count = _columnIndices.Length;

            // Temp buffers
            int[] columnIndices = new int[count];
            T[] values = new T[count];
            int[] rowIndices = new int[Rows + 1];

            // store the row counts inside rowIndices
            for (int r = 0; r < Rows; ++r)
            {
                int pr = permutations[r];
                rowIndices[r + 1] = (_rowIndices[pr + 1] - _rowIndices[pr]);
            }
            for (int r = 0; r < Rows; ++r)
            {
                rowIndices[r + 1] += rowIndices[r];
            }

            for (int r = 0; r < Rows; ++r)
            {
                int pr = permutations[r];
                int len = _rowIndices[pr + 1] - _rowIndices[pr];

                Array.Copy(_columnIndices, _rowIndices[pr], columnIndices, rowIndices[r], len);
                Array.Copy(_values, _rowIndices[pr], values, rowIndices[r], len);
            }

            _rowIndices = rowIndices;
            _columnIndices = columnIndices;
            _values = values;
        }

        /// <summary>
        /// Permute the rows of this matrix using the specified permutation matrix. This is equivalent to 
        /// premultiplying this matrix by the permutation matrix $P$, $PA$.
        /// </summary>
        /// <param name="permutation">The permutation matrix.</param>
        public void PermuteRows(PermutationMatrix permutation)
        {
            if (permutation is null)
            {
                throw new ArgumentNullException(nameof(permutation));
            }
            if (permutation.Dimension != Rows)
            {
                throw new InvalidOperationException("The number of rows of this matrix does not match the dimension of the permutation matrix.");
            }
            PermuteRows(permutation.PermutationVector);
        }

        /// <summary>
        /// Permute the columns of this matrix using the specified permutation vector.
        /// </summary>
        /// <param name="columnPermutations">The column permutation vector.</param>
        public void PermuteColumns(int[] columnPermutations)
        {
            if (columnPermutations is null)
            {
                throw new ArgumentNullException(nameof(columnPermutations));
            }
            if (columnPermutations.Length != Columns)
            {
                throw new InvalidOperationException("The number of columns of this matrix does not match the dimension of the column permutation vector.");
            }
            if (!columnPermutations.IsPermutation())
            {
                throw new ArgumentException("The vector is not a permutation vector.");
            }

            static int compFirst(Tuple<int, T> a, Tuple<int, T> b) => a.Item1.CompareTo(b.Item1);

            List<Tuple<int, T>> columnIndices = new List<Tuple<int, T>>();
            for (int r = 0; r < Rows; ++r)
            {
                int row_end = _rowIndices[r + 1],
                    row_start = _rowIndices[r];

                columnIndices.Clear();
                for (int i = row_start; i < row_end; ++i)
                {
                    columnIndices.Add(new Tuple<int, T>(columnPermutations[_columnIndices[i]], _values[i]));
                }
                columnIndices.Sort(compFirst);

                for (int i = row_start, k = 0; i < row_end; ++i, ++k)
                {
                    Tuple<int, T> entry = columnIndices[k];
                    _columnIndices[i] = entry.Item1;
                    _values[i] = entry.Item2;
                }
            }
        }

        /// <summary>
        /// Permute the columns of this matrix using the specified permutation matrix.
        /// </summary>
        /// <param name="permutation">The permutation matrix.</param>
        public void PermuteColumns(PermutationMatrix permutation)
        {
            if (permutation is null)
            {
                throw new ArgumentNullException(nameof(permutation));
            }
            if (permutation.Dimension != Columns)
            {
                throw new InvalidOperationException("The number of columns of this matrix does not match the dimension of the permutation matrix.");
            }
            PermuteColumns(permutation.PermutationVector);
        }

        /// <summary>
        /// Insert a row into this matrix at the specified index.
        /// </summary>
        /// <param name="rowIndex">The index of the row to insert at.</param>
        /// <param name="row">The row to insert.</param>
        public void InsertRow(int rowIndex, SparseVector<T> row)
        {
            if (rowIndex < 0 || rowIndex > Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(rowIndex));
            }
            if (row is null)
            {
                throw new ArgumentNullException(nameof(row));
            }

            int row_len = row.Values.Length, 
                len = _values.Length + row_len;

            int[] rowIndices = new int[Rows + 2];
            Array.Copy(_rowIndices, rowIndices, rowIndex + 1);
            for (int r = rowIndex; r <= Rows; ++r)
            {
                rowIndices[r + 1] = _rowIndices[r] + row_len;
            }

            int[] columnIndices = new int[len];
            T[] values = new T[len];

            int a = _rowIndices[rowIndex];
            Array.Copy(_columnIndices, columnIndices, a);
            Array.Copy(_values, values, a);

            Array.Copy(row.Values, 0, values, a, row_len);
            for (int i = 0; i < row_len; ++i)
            {
                columnIndices[a + i] = row.Indices[i];
            }

            Array.Copy(_columnIndices, a, columnIndices, a + row_len, _columnIndices.Length - a);
            Array.Copy(_values, a, values, a + row_len, _values.Length - a);

            _rowIndices = rowIndices;
            _columnIndices = columnIndices;
            _values = values;

            ++Rows;
        }

        /// <summary>
        /// Insert a column into this matrix at the specified index.
        /// </summary>
        /// <param name="columnIndex">The column index to insert at.</param>
        /// <param name="column">The column to insert.</param>
        public void InsertColumn(int columnIndex, SparseVector<T> column)
        {
            if (columnIndex < 0 || columnIndex > Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(columnIndex));
            }
            if (column is null)
            {
                throw new ArgumentNullException(nameof(column));
            }

            int nnz = column.Dimension + _values.Length;
            int[] columnIndices = new int[nnz];
            T[] values = new T[nnz];

            int k = 0, vi = 0, vlen = column.Indices.Length;

            int row_start, row_end = 0;
            for (int r = 0; r < Rows; ++r)
            {
                row_start = row_end;
                row_end = _rowIndices[r + 1];

                if (vi < vlen && column.Indices[vi] == r)
                {
                    bool found = false;
                    for (int i = row_start; i < row_end; ++i)
                    {
                        int c = _columnIndices[i];
                        if (!found && c >= columnIndex)
                        {
                            columnIndices[k] = columnIndex;
                            values[k] = column.Values[vi];
                            ++k;
                            found = true;
                        }

                        columnIndices[k] = found ? (c + 1) : c;
                        values[k] = _values[i];
                        ++k;
                    }

                    if (!found)
                    {
                        columnIndices[k] = columnIndex;
                        values[k] = column.Values[vi];
                        ++k;
                    }

                    ++vi;
                }
                else
                {
                    // column indices need to be shifted
                    for (int i = row_start; i < row_end; ++i)
                    {
                        int c = _columnIndices[i];
                        values[k] = _values[i];
                        columnIndices[k] = c >= columnIndex ? (c + 1) : c;
                        ++k;
                    }
                }
                _rowIndices[r + 1] = k;
            }

            _columnIndices = columnIndices;
            _values = values;

            ++Columns;
        }

        /// <summary>
        /// Returns the trace of this matrix.
        /// </summary>
        /// <param name="provider">The provider used for addition of matrix element.</param>
        /// <returns>The matrix trace.</returns>
        public override T Trace(IProvider<T> provider)
        {
            return CSRHelper<T>.Trace(_rowIndices, _columnIndices, _values, Rows, provider);
        }

        /// <summary>
        /// Returns the lower-triangular part of the matrix as a new sparse matrix.
        /// </summary>
        /// <returns>The lower-triangular half of the matrix.</returns>
        public CSRSparseMatrix<T> Lower()
        {
            CSRHelper<T>.Lower(_rowIndices, _columnIndices, _values, Rows, out int[] rowIndices, out int[] columnIndices, out T[] values);
            return new CSRSparseMatrix<T>(Rows, rowIndices, columnIndices, values);
        }

        /// <summary>
        /// Returns the upper triangular half of this matrix as a new matrix.
        /// If this matrix is not square, then all elements on or above the main diagonal will be returned.
        /// </summary>
        /// <returns></returns>
        public CSRSparseMatrix<T> Upper()
        {
            CSRHelper<T>.Upper(_rowIndices, _columnIndices, _values, Rows, out int[] rowIndices, out int[] columnIndices, out T[] values);
            return new CSRSparseMatrix<T>(Columns, rowIndices, columnIndices, values);
        }

        /// <summary>
        /// Returns a symmetric matrix created by overwriting the upper triangular portion of 
        /// this matrix with its lower-triangular half.
        /// </summary>
        /// <returns>The reflected matrix.</returns>
        public CSRSparseMatrix<T> ReflectLower()
        {
            if (!IsSquare)
            {
                throw new InvalidOperationException(ExceptionMessages.MatrixNotSquare);
            }

            int[] rowIndices = new int[Rows + 1];

            // Count the number of non-zeroes in each row of the reflected matrix
            for (int r = 0; r < Rows; ++r)
            {
                int row_end = _rowIndices[r + 1], i;
                for (i = _rowIndices[r]; i < row_end; ++i)
                {
                    int c = _columnIndices[i];
                    if (c >= r) break; // count the diagonal only once, so include =

                    rowIndices[c]++;
                }
                rowIndices[r] += (row_end - i);
            }

            // Accumulate the row indices
            for (int r = 0; r < Rows; ++r)
            {
                rowIndices[r + 1] += rowIndices[r];
            }

            int[] columnIndices = new int[rowIndices[Rows]];
            T[] values = new T[rowIndices[Rows]];

            int[] next = new int[Rows];
            Array.Copy(rowIndices, 0, next, 0, Rows);

            // Add the lower entries first to each row
            for (int r = 0; r < Rows; ++r)
            {
                int row_end = _rowIndices[r + 1], k = next[r];
                for (int i = _rowIndices[r]; i < row_end; ++i)
                {
                    int c = _columnIndices[i];
                    if (c > r)
                    {
                        break;
                    }
                    columnIndices[k] = _columnIndices[i];
                    values[k] = _values[i];
                    ++k;
                }
                next[r] = k;
            }

            // Add the upper entries (the reflected entries) in a separate loop
            for (int r = 0; r < Rows; ++r)
            {
                int row_end = _rowIndices[r + 1];
                for (int i = _rowIndices[r]; i < row_end; ++i)
                {
                    int c = _columnIndices[i];
                    if (c < r)
                    {
                        int index = next[c]++;
                        columnIndices[index] = r;
                        values[index] = _values[i];
                    }
                }
            }

            return new CSRSparseMatrix<T>(Columns, rowIndices, columnIndices, values);
        }

        /// <summary>
        /// Returns the sum of all entries that satisfy a predicate.
        /// </summary>
        /// <param name="predicate">The predicate used to select matrix elements.</param>
        /// <param name="provider">The provider used for element operations.</param>
        /// <returns></returns>
        public T Sum(Predicate<MatrixEntry<T>> predicate, IProvider<T> provider)
        {
            if (predicate is null)
            {
                throw new ArgumentNullException(nameof(predicate));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }

            T sum = provider.Zero;
            for (int r = 0; r < Rows; ++r)
            {
                int row_end = _rowIndices[r + 1];
                for (int i = _rowIndices[r]; i < row_end; ++i)
                {
                    if (predicate(new MatrixEntry<T>(r, _columnIndices[i], _values[i])))
                    {
                        provider.Add(sum, _values[i]);
                    }
                }
            }
            return sum;
        }

        /// <summary>
        /// Returns the sum of a function applied to all entries that satisfy a predicate. $\sum_{p(A_{ij}} f(A_ij)$
        /// </summary>
        /// <typeparam name="F"></typeparam>
        /// <param name="predicate">The predicate used to select matrix entries.</param>
        /// <param name="function">The function to apply to each selected entry.</param>
        /// <param name="provider">The provider used for elementwise operations.</param>
        /// <returns>The sum</returns>
        public F Sum<F>(Predicate<MatrixEntry<T>> predicate, Func<T, F> function, IProvider<F> provider)
        {
            if (predicate is null)
            {
                throw new ArgumentNullException(nameof(predicate));
            }
            if (function is null)
            {
                throw new ArgumentNullException(nameof(function));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }

            F sum = provider.Zero;
            F fz = function(_zero);
            if (!provider.Equals(fz, provider.Zero))
            {
                checked
                {
                    sum = provider.MultiplyInteger(fz, Rows * Columns - _values.Length);
                }
            }

            for (int r = 0; r < Rows; ++r)
            {
                int row_end = _rowIndices[r + 1];
                for (int i = _rowIndices[r]; i < row_end; ++i)
                {
                    if (predicate(new MatrixEntry<T>(r, _columnIndices[i], _values[i])))
                    {
                        sum = provider.Add(sum, function(_values[i]));
                    }
                }
            }

            return sum;
        }

        /// <summary>
        /// Returns the row sums of this matrix, as a sparse vector. The $i$-th element of the returned 
        /// vector is equal to the sum of all elements in the $i$-th row of this matrix.
        /// </summary>
        /// <param name="provider">The provider used to sum elements of the matrix.</param>
        /// <returns>The row sums of this matrix.</returns>
        public BigSparseVector<T> SparseRowSums(IProvider<T> provider)
        {
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }

            Dictionary<long, T> rowsums = new Dictionary<long, T>();
            for (int r = 0; r < Rows; ++r)
            {
                int row_start = _rowIndices[r], 
                    row_end = _rowIndices[r + 1];

                if (row_start < row_end)
                {
                    T sum = provider.Zero;
                    for (int i = row_start; i < row_end; ++i)
                    {
                        sum = provider.Add(sum, _values[i]);
                    }

                    if (!provider.Equals(sum, provider.Zero))
                    {
                        rowsums[r] = sum;
                    }
                }
            }
            return new BigSparseVector<T>(Rows, rowsums);
        }

        /// <summary>
        /// Returns the column sums of this matrix, as a sparse vector. The $i$-th element of the 
        /// returned vector is equal to the sum of all elements in the $i$-th column of this matrix. 
        /// </summary>
        /// <param name="provider">The provider used to sum elements of the matrix.</param>
        /// <returns>The column sums of this matrix.</returns>
        public BigSparseVector<T> SparseColumnSums(IProvider<T> provider)
        {
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }

            Dictionary<long, T> colsums = new Dictionary<long, T>();
            for (int r = 0; r < Rows; ++r)
            {
                int row_end = _rowIndices[r + 1];
                for (int i = _rowIndices[r]; i < row_end; ++i)
                {
                    long c = _columnIndices[i];
                    if (colsums.ContainsKey(c))
                    {
                        colsums[c] = provider.Add(colsums[c], _values[i]);
                    }
                    else
                    {
                        colsums[c] = _values[i];
                    }
                }
            }
            return new BigSparseVector<T>(Columns, colsums);
        }

        /// <summary>
        /// Returns the row sums of this matrix as a dense vector.
        /// </summary>
        /// <param name="provider">The provider used for elementwise operations.</param>
        /// <returns>The row sums of this matrix as a dense vector.</returns>
        public override DenseVector<T> RowSums(IProvider<T> provider)
        {
            return CSRHelper<T>.RowSums(_rowIndices, _values, Rows, provider);
        }

        /// <summary>
        /// Returns the column sums of this matrix, as a dense vector.
        /// </summary>
        /// <param name="provider">The provider to use for summing matrix elements.</param>
        /// <returns>The column sums of this matrix.</returns>
        public override DenseVector<T> ColumnSums(IProvider<T> provider)
        {
            return CSRHelper<T>.ColumnSums(_columnIndices, _values, Columns, provider);
        }

        /// <summary>
        /// Swaps two rows of this matrix.
        /// </summary>
        /// <param name="rowIndex1">The index of the first row to swap.</param>
        /// <param name="rowIndex2">The index of the second row to swap.</param>
        public override void SwapRows(int rowIndex1, int rowIndex2)
        {
            CSRHelper<T>.SwapRows(_rowIndices, _columnIndices, _values, rowIndex1, rowIndex2, Rows);
        }

        /// <summary>
        /// Swaps two columns of this matrix.
        /// </summary>
        /// <param name="columnIndex1">The index of the first column to swap.</param>
        /// <param name="columnIndex2">The index of the second column to swap.</param>
        public override void SwapColumns(int columnIndex1, int columnIndex2)
        {
            CSRHelper<T>.SwapColumns(_rowIndices, _columnIndices, _values, columnIndex1, columnIndex2, Rows, Columns);
        }

        /// <summary>
        /// Returns the 1-norm of this matrix, $||A||_1$
        /// </summary>
        /// <param name="provider"></param>
        /// <returns></returns>
        public T OneNorm(IProvider<T> provider, IComparer<T> comparer)
        {
            // Calculate the 1-norm as the maximum absolute column sum

            // Test the density of this matrix - if nnz > columns, use a dense vector
            // to collect the column sums. Otherwise, use a sparse vector
            if (_values.Length - 1 > Columns)
            {
                T[] absColumnSums = RectangularVector.Repeat(provider.Zero, Columns);
                for (int r = 0; r < Rows; ++r)
                {
                    int row_end = _rowIndices[r + 1];
                    for (int i = _rowIndices[r]; i < row_end; ++i)
                    {
                        int c = _columnIndices[i];

                        T value = _values[i];
                        if (provider.IsRealAndNegative(value))
                        {
                            value = provider.Negate(value);
                        }

                        absColumnSums[c] = provider.Add(absColumnSums[c], value);
                    }
                }

                T norm = absColumnSums[0];
                for (int c = 1; c < Columns; ++c)
                {
                    if (comparer.Compare(norm, absColumnSums[c]) < 0)
                    {
                        norm = absColumnSums[c];
                    }
                }
                return norm;
            }

            // Use sparse representation
            else
            {
                Dictionary<int, T> absColumnSums = new Dictionary<int, T>();
                for (int r = 0; r < Rows; ++r)
                {
                    int row_end = _rowIndices[r + 1];
                    for (int i = _rowIndices[r]; i < row_end; ++i)
                    {
                        int c = _columnIndices[i];

                        T value = _values[i];
                        if (provider.IsRealAndNegative(value))
                        {
                            value = provider.Negate(value);
                        }

                        if (absColumnSums.ContainsKey(c))
                        {
                            absColumnSums[c] = provider.Add(absColumnSums[c], value);
                        }
                        else
                        {
                            absColumnSums[c] = value;
                        }
                    }
                }

                T norm = provider.Zero;
                foreach (T value in absColumnSums.Values)
                {
                    if (comparer.Compare(value, norm) > 0)
                    {
                        norm = value;
                    }
                }
                return norm;
            }
        }

        /// <summary>
        /// Returns the $\infty$-norm of this matrix, $||A||_{\infty}$.
        /// </summary>
        /// <param name="provider">The provider used for summing elements of this matrix.</param>
        /// <param name="comparer">The comparer used to compare sizes of elements of this matrix.</param>
        /// <returns>The $\infty$-norm of this matrix.</returns>
        public T InfinityNorm(IProvider<T> provider, IComparer<T> comparer)
        {
            // inf-norm is the max abs row sum of the matrix
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            if (comparer is null)
            {
                throw new ArgumentNullException(nameof(comparer));
            }

            T norm = provider.Zero;
            for (int r = 0; r < Rows; ++r)
            {
                int row_end = _rowIndices[r + 1];
                T sumabs = provider.Zero;
                for (int i = _rowIndices[r]; i < row_end; ++i)
                {
                    T value = _values[i];
                    if (provider.IsRealAndNegative(value))
                    {
                        value = provider.Negate(value);
                    }
                    sumabs = provider.Add(sumabs, value);
                }

                if (comparer.Compare(sumabs, norm) > 0)
                {
                    norm = sumabs;
                }
            }
            return norm;
        }

        /// <summary>
        /// Returns the Frobenius norm of this matrix, $||A||_F$.
        /// </summary>
        /// <typeparam name="F"></typeparam>
        /// <param name="elementNorm"></param>
        /// <param name="provider"></param>
        /// <returns></returns>
        public F FrobeniusNorm<F>(Func<T, F> elementNorm, IProvider<F> provider)
        {
            if (elementNorm is null)
            {
                throw new ArgumentNullException(nameof(elementNorm));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }

            return CSRHelper<T>.FrobeniusNorm(_values, elementNorm, provider);
        }

        /// <summary>
        /// Returns the Frobenius norm of this matrix, for matrices over real data types. 
        /// </summary>
        /// <param name="provider"></param>
        /// <returns></returns>
        public T FrobeniusNorm(IProvider<T> provider)
        {
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            return CSRHelper<T>.FrobeniusNorm(_values, x => x, provider);
        }

        /// <summary>
        /// Returns $||A - B||_F$ where $A$ is this matrix, $B$ is another matrix, and $|| ||_F$ is 
        /// the Frobenius norm. This method is designed for matrices whose entries belong to some space $F$, 
        /// whose norm is not in $F$ (e.g. complex types).
        /// </summary>
        /// <typeparam name="F"></typeparam>
        /// <param name="matrix"></param>
        /// <param name="elementNorm"></param>
        /// <param name="providerT"></param>
        /// <param name="providerF"></param>
        /// <returns></returns>
        public F FrobeniusDistance<F>(CSRSparseMatrix<T> matrix, Func<T, F> elementNorm, IProvider<T> providerT, IProvider<F> providerF)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            if (elementNorm is null)
            {
                throw new ArgumentNullException(nameof(elementNorm));
            }
            if (providerT is null)
            {
                throw new ArgumentNullException(nameof(providerT));
            }
            if (providerF is null)
            {
                throw new ArgumentNullException(nameof(providerF));
            }
            return CSRHelper<T>.FrobeniusDistance(_rowIndices, _columnIndices, _values, matrix._rowIndices, matrix._columnIndices, matrix._values, 
                elementNorm, providerT, providerF);
        }

        /// <summary>
        /// Returns $||A - B||_F$ where $A$ is this matrix, $B$ is another matrix, and $|| ||_F$ is 
        /// the Frobenius norm. This method is designed for matrices whose entries belong to some space $F$, 
        /// and entries have a norm in the same space $F$ (e.g. real types).
        /// </summary>
        /// <param name="matrix"></param>
        /// <param name="provider"></param>
        /// <returns></returns>
        public T FrobeniusDistance(CSRSparseMatrix<T> matrix, IProvider<T> provider)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            MatrixChecks.AssertMatrixDimensionsEqual(this, matrix);

            return CSRHelper<T>.FrobeniusDistance(_rowIndices, _columnIndices, _values, matrix._rowIndices, matrix._columnIndices, matrix._values,
                x => x, provider, provider);
        }

        /// <summary>
        /// Performs a rank-1 update of this matrix, $A + \beta uv^T$.
        /// </summary>
        /// <param name="beta">The beta coefficient, $\beta$.</param>
        /// <param name="u">The first sparse vector $u$.</param>
        /// <param name="v">The second sparse vector $v$.</param>
        public void Rank1Update(T beta, SparseVector<T> u, SparseVector<T> v, IProvider<T> provider)
        {
            if (beta is null)
            {
                throw new ArgumentNullException(nameof(beta));
            }
            if (u is null)
            {
                throw new ArgumentNullException(nameof(u));
            }
            if (v is null)
            {
                throw new ArgumentNullException(nameof(v));
            }

            int[] u_indices = u.Indices, v_indices = v.Indices;
            T[] u_values = u.Values, v_values = v.Values;

            // Only [0, rows) rows are modified
            // First count the nnz in the resulting matrix, and check if reallocating 
            // memory is required
            int nnz = _values.Length;
            bool requiresAllocation = false;

            for (int ui = 0; ui < u_indices.Length; ++ui)
            {
                int r = u_indices[ui];

                int i1 = _rowIndices[r], row_end = _rowIndices[r + 1];
                int i2 = 0, v_end = v_indices.Length;

                while (i1 < row_end || i2 < v_end)
                {
                    int c1 = i1 < row_end ? _columnIndices[i1] : int.MaxValue;
                    int c2 = i2 < v_end ? v_indices[i2] : int.MaxValue;

                    if (c1 > c2)
                    {
                        ++i2;
                        ++nnz;
                        requiresAllocation = true;
                    }
                    else if (c1 < c2)
                    {
                        ++i1;
                    }
                    else
                    {
                        ++i1;
                        ++i2;
                    }
                }
            }

            // Requires allocation case
            if (requiresAllocation)
            {
                int[] columnIndices = new int[nnz];
                T[] values = new T[nnz];

                int ui = 0, k = 0;

                // Requires iteration over all rows to ensure all values are copied
                int row_start, row_end = 0;
                for (int r = 0; r < Rows; ++r)
                {
                    row_start = row_end;
                    row_end = _rowIndices[r + 1];
                    // Overwrite row
                    if (ui < u_indices.Length && r == u_indices[ui])
                    {
                        int i1 = row_start,
                            i2 = 0, v_end = v_indices.Length;

                        T factor = provider.Multiply(u_values[ui], beta);
                        while (i1 < row_end || i2 < v_end)
                        {
                            int c1 = i1 < row_end ? _columnIndices[i1] : int.MaxValue;
                            int c2 = i2 < v_end ? v_indices[i2] : int.MaxValue;

                            if (c1 < c2)
                            {
                                values[k] = _values[i1];
                                columnIndices[k] = c1;
                                ++i1;
                            }
                            else if (c1 > c2)
                            {
                                values[k] = provider.Multiply(factor, v_values[i2]);
                                columnIndices[k] = c2;
                                ++i2;
                            }
                            else
                            {
                                values[k] = provider.Add(_values[i1], provider.Multiply(factor, v_values[i2]));
                                columnIndices[k] = c1;
                                ++i1;
                                ++i2;
                            }
                            ++k;
                        }
                        ++ui;
                    }
                    else
                    {
                        int count = row_end - row_start;
                        Array.Copy(_values, row_start, values, k, count);
                        Array.Copy(_columnIndices, row_start, columnIndices, k, count);
                        k += count;
                    }
                    _rowIndices[r + 1] = k;
                }

                _columnIndices = columnIndices;
                _values = values;
            }

            // No reallocation needed - just overwrite existing arrays
            else
            {
                for (int ui = 0; ui < u_indices.Length; ++ui)
                {
                    int r = u_indices[ui],
                        row_end = _rowIndices[r + 1],
                        vi = 0;

                    T factor = provider.Multiply(beta, u_values[ui]);
                    for (int i = _rowIndices[r]; i < row_end; ++i)
                    {
                        if (vi >= v_indices.Length) break;
                        if (_columnIndices[i] == v_indices[vi])
                        {
                            _values[i] = provider.Add(_values[i], provider.Multiply(factor, v_values[vi]));
                            ++vi;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Solves the triangular system $Ax = b$ using this matrix as $A$.
        /// There are no checks in place to ensure that this matrix is in fact triangular.
        /// Only supported for square systems.
        /// 
        /// <para>This implementation will "give up" easily, returning <txt>false</txt> if it does 
        /// and the <txt>solution</txt> vector will be undefined.</para>
        /// </summary>
        /// <param name="rhsVector">b</param>
        /// <param name="solution">x</param>
        /// <param name="provider"></param>
        /// <param name="lower">Specifies whether this matrix is upper or lower triangular</param>
        internal override bool SolveTriangular(T[] rhsVector, T[] solution, IDenseBLAS1<T> blas, bool lower, bool transpose)
        {
            if (!IsSquare)
            {
                throw new NotSupportedException();
            }

            IProvider<T> provider = blas.Provider;
            int dim = Rows;
            if (lower)
            {
                if (transpose)
                {
                    throw new NotImplementedException();
                }
                else
                {
                    for (int r = 0; r < dim; ++r)
                    {
                        int row_end = _rowIndices[r + 1];

                        // Check if the diagonal term is zero
                        if (row_end == 0)
                        {
                            return false;
                        }

                        int diag_index = row_end - 1, c = _columnIndices[diag_index];
                        T d = _values[diag_index];
                        if (c != r || d.Equals(_zero))
                        {
                            return false;
                        }

                        T sumProduct = rhsVector[c];
                        for (int i = _rowIndices[r]; i < diag_index; ++i)
                        {
                            // assume the column index is <= rowIndex, and that it is in ascending order
                            sumProduct = provider.Subtract(sumProduct, provider.Multiply(solution[_columnIndices[i]], _values[i]));
                        }

                        solution[r] = provider.Divide(sumProduct, d);
                    }
                    return true;
                }
            }
            else
            {
                throw new NotSupportedException();
            }
        }
        
        /// <summary>
        /// Solve a sparse triangular system $Ax = b$ where $A$ is a <txt>dim</txt>-by-<txt>dim</txt> submatrix of this matrix, 
        /// starting at (<txt>rowIndex</txt>, <txt>columnIndex</txt>), and $b$ is a subvector of the specified RHS-vector
        /// of length <txt>dim</txt>, starting at index <txt>vectorIndex</txt>.
        /// </summary>
        /// <param name="rhsVector"></param>
        /// <param name="solution"></param>
        /// <param name="rowIndex"></param>
        /// <param name="columnIndex"></param>
        /// <param name="vectorIndex"></param>
        /// <param name="dim"></param>
        internal void SolveTriangular(SparseVector<T> rhsVector, SparseVector<T> solution, int rowIndex, int columnIndex, int vectorIndex, int dim)
        {
            throw new NotImplementedException();
        }

        public SymbolicCholesky CholeskyAnalysis()
        {
            CSRCholesky<T> chol = new CSRCholesky<T>();
            return chol.Analyse(this);
        }

        public CSRSparseMatrix<T> CholeskyFactor(IProvider<T> provider)
        {
            CSRCholesky<T> chol = new CSRCholesky<T>();
            return chol.CholLeft(this, chol.Analyse(this), provider);
        }

        #region Boolean methods

        public override bool IsBanded(int lowerBandwidth, int upperBandwidth, Predicate<T> isZero)
        {
            return CSRBooleanHelper<T>.IsBanded(_rowIndices, _columnIndices, _values, Rows, lowerBandwidth, upperBandwidth, isZero);
        }

        /// <summary>
        /// Returns whether this matrix is symmetric, using the default equality comparer to 
        /// test for equality of elements of this matrix.
        /// </summary>
        /// <returns>Whether this matrix is symmetric.</returns>
        public bool IsSymmetric()
        {
            return IsSymmetric(EqualityComparer<T>.Default);
        }
        
        public new bool IsSymmetric(Func<T, T, bool> isEqual)
        {
            if (!IsSquare)
            {
                return false;
            }

            // Its debatable whether this method is really the best. 
            // Even though it is asymptotically efficient O(max(n, k)) which 
            // is the theoretical optimal for k > n in the worse case, it does not get a chance to check 
            // for equality until the size of the dictionary is almost O(k)...
            // since it is iterating in row major order. Should run some experiments
            // to confirm

            Dictionary<long, T> values = new Dictionary<long, T>(_values.Length / 2);

            long lc = Columns;
            int dim = Rows;
            for (int r = 0; r < dim; ++r)
            {
                int row_end = _rowIndices[r + 1];
                long offset = r * lc;
                for (int i = _rowIndices[r]; i < row_end; ++i)
                {
                    int c = _columnIndices[i];

                    // Diagonal elements do not have a pair
                    if (r == c)
                    {
                        continue;
                    }

                    long key = c + offset;
                    if (values.ContainsKey(key))
                    {
                        if (!isEqual(values[key], _values[i]))
                        {
                            return false;
                        }
                        values.Remove(key);
                    }
                    else
                    {
                        // forgive if value is zero
                        if (isEqual(_values[i], _zero))
                        {
                            continue;
                        }

                        // Cache for comparison later
                        values[c * dim + r] = _values[i];
                    }
                }
            }
            return values.Count == 0;
        }

        /// <summary>
        /// Returns whether this matrix is symmetric
        /// </summary>
        /// <param name="equalityComparer">
        /// The equality comparer used to determine whether two matrix elements should 
        /// be considered equal.
        /// </param>
        /// <returns>Whether this matrix is symmetric.</returns>
        public new bool IsSymmetric(IEqualityComparer<T> equalityComparer)
        {
            if (!IsSquare)
            {
                return false;
            }

            if (equalityComparer is null)
            {
                throw new ArgumentNullException(nameof(equalityComparer));
            }

            return IsSymmetric((a, b) => equalityComparer.Equals(a, b));
        }

        /// <summary>
        /// Returns whether this matrix is structurally symmetric, i.e. the non-zero pattern of this 
        /// matrix is symmetric.
        /// </summary>
        /// <returns>Whether this matrix is structurally symmetric.</returns>
        public bool IsSymmetricSymbolic()
        {
            if (!IsSquare)
            {
                return false;
            }

            // Used to temporarily store the transpose of both rowIndices and columnIndices
            int[] t_row_counts = new int[Rows + 1];
            int[] t_column_indices = new int[_columnIndices.Length];

            // Populate indices with row counts of transpose
            for (int r = 0; r < Rows; ++r)
            {
                int row_end = _rowIndices[r + 1];
                for (int i = _rowIndices[r]; i < row_end; ++i)
                {
                    t_row_counts[_columnIndices[i]]++;
                }
            }

            // Check row counts equal
            for (int r = 0; r < Rows; ++r)
            {
                if (t_row_counts[r] != (_rowIndices[r + 1] - _rowIndices[r]))
                {
                    return false;
                }
            }

            // Shift and accumulate
            for (int r = Rows; r > 0; --r)
            {
                t_row_counts[r] = t_row_counts[r - 1];
            }
            t_row_counts[0] = 0;
            for (int r = 0; r < Rows; ++r)
            {
                t_row_counts[r + 1] += t_row_counts[r];
            }

            // Since row counts are equal, we just check the column counts in their order
            // borrow tranpose_row_counts to store the next index to insert at
            for (int r = 0; r < Rows; ++r)
            {
                int row_end = _rowIndices[r + 1];
                for (int i = _rowIndices[r]; i < row_end; ++i)
                {
                    t_column_indices[t_row_counts[_columnIndices[i]]++] = r;
                }
            }

            // Single pass is sufficient since row buckets are equal
            for (int i = 0; i < t_column_indices.Length; ++i)
            {
                if (t_column_indices[i] != _columnIndices[i])
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Returns whether this matrix is numerically lower triangular.
        /// </summary>
        /// <returns></returns>
        public override bool IsLower(Predicate<T> isZero)
        {
            return CSRHelper<T>.IsLower(_rowIndices, _columnIndices, _values, Rows, isZero);
        }

        /// <summary>
        /// Returns whether this matrix is structurally (symbolically) lower triangular.
        /// A matrix is structurally lower triangular if all recorded values are in the lower 
        /// half, irrespective of whether the values themselves are numerically zero.
        /// </summary>
        /// <returns></returns>
        public bool IsLowerSymbolic()
        {
            return CSRHelper<T>.IsLowerSymbolic(_rowIndices, _columnIndices, Rows);
        }

        /// <summary>
        /// Returns whether this matrix is upper triangular.
        /// </summary>
        /// <param name="isZero"></param>
        /// <returns></returns>
        public override bool IsUpper(Predicate<T> isZero)
        {
            return CSRHelper<T>.IsUpper(_rowIndices, _columnIndices, _values, Rows, isZero);
        }

        public bool IsUpperSymbolic()
        {
            return CSRHelper<T>.IsUpperSymbolic(_rowIndices, _columnIndices, Rows);
        }

        /// <summary>
        /// Returns whether this matrix is an identity matrix, using two predicates that 
        /// indicate whether an element should be regarded as zero and one respectively.
        /// </summary>
        /// <param name="isZero">
        /// A predicate that returns true if its argument should be regarded 
        /// as zero.</param>
        /// <param name="isOne">
        /// A predicate that returns true if its argument should be regarded 
        /// as one.
        /// </param>
        /// <returns>Whether this matrix is an identity matrix.</returns>
        public override bool IsIdentity(Predicate<T> isZero, Predicate<T> isOne)
        {
            return CSRBooleanHelper<T>.IsIdentity(_rowIndices, _columnIndices, _values, Rows, Columns, isZero, isOne);
        }

        /// <summary>
        /// Returns whether this matrix is a diagonal matrix, i.e. all non-zero elements are along the main diagonal.
        /// </summary>
        /// <param name="isZero">A predicate that returns true if an element is zero.</param>
        /// <returns>Whether this matrix is a diagonal matrix.</returns>
        public override bool IsDiagonal(Predicate<T> isZero)
        {
            return CSRBooleanHelper<T>.IsDiagonal(_rowIndices, _columnIndices, _values, Rows, isZero);
        }
        
        /// <summary>
        /// Returns whether this matrix is a diagonal matrix, i.e. all non-zero elements are along the main diagonal.
        /// </summary>
        /// <param name="equalityComparer">The equality comparer used to determine whether two matrix elements should 
        /// be considered equal.</param>
        /// <returns>Whether this matrix is a diagonal matrix.</returns>
        public bool IsDiagonal(IEqualityComparer<T> equalityComparer = null)
        {
            if (equalityComparer is null)
            {
                equalityComparer = EqualityComparer<T>.Default;
            }

            for (int r = 0; r < Rows; ++r)
            {
                int row_end = _rowIndices[r + 1];
                for (int i = _rowIndices[r]; i < row_end; ++i)
                {
                    if (_columnIndices[i] != r && !equalityComparer.Equals(_values[i], _zero))
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// Returns whether this matrix is a zero matrix, using a custom function to determine
        /// whether each element of the matrix should be considered equal to zero.
        /// </summary>
        /// <param name="isZero">A predicate that returns true if a matrix element should be 
        /// considered as being equal to zero.</param>
        /// <returns>Whether this matrix is a zero matrix.</returns>
        public override bool IsZero(Predicate<T> isZero)
        {
            return CSRBooleanHelper<T>.IsZero(_values, isZero);
        }

        /// <summary>
        /// Returns whether this matrix is a zero matrix, using a custom equality comparer. This 
        /// method makes the assumption that the value of <txt>new T()</txt> is zero, and equality 
        /// to zero is established by comparing each element of this matrix to the value of <txt>new T()</txt>.
        /// </summary>
        /// <param name="equalityComparer">The custom equality comparer to use for the matrix elements.</param>
        /// <returns>Whether this matrix is a zero matrix.</returns>
        public bool IsZero(IEqualityComparer<T> equalityComparer)
        {
            if (equalityComparer is null)
            {
                throw new ArgumentNullException(nameof(equalityComparer));
            }

            return IsZero(x => equalityComparer.Equals(x, _zero));
        }

        /// <summary>
        /// Returns whether this matrix is equal to another matrix, using a predicate to determine 
        /// whether two matrix elements should be treated as being equal.
        /// </summary>
        /// <param name="matrix"></param>
        /// <param name="isEqual"></param>
        /// <returns></returns>
        public bool Equals(CSRSparseMatrix<T> matrix, Func<T, T, bool> isEqual)
        {
            if (matrix is null)
            {
                return false;
            }
            if (Rows != matrix.Rows || Columns != matrix.Columns)
            {
                return false;
            }

            for (int r = 0; r < Rows; ++r)
            {
                int r1 = r + 1;
                if (!CSRHelper<T>.RowEquals(
                        _columnIndices, 
                        _values, 
                        _rowIndices[r], 
                        _rowIndices[r1],

                        matrix._columnIndices, 
                        matrix._values, 
                        matrix._rowIndices[r], 
                        matrix._rowIndices[r1],

                        _zero, isEqual))
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Returns whether this matrix is equal in value to another matrix, using an equality 
        /// comparer to determine whether two matrix elements should be considered equal.
        /// </summary>
        /// <param name="matrix">The other matrix to compare to.</param>
        /// <param name="comparer">The equality comparer to determine whether two matrix elements 
        /// should be considered equal.</param>
        /// <returns>Whether the two matrices are equal in value.</returns>
        public bool Equals(CSRSparseMatrix<T> matrix, IEqualityComparer<T> comparer)
        {
            return Equals(matrix, comparer.Equals);
        }

        /// <summary>
        /// Returns whether this matrix is equal in value to another matrix, using the default
        /// equality comparer for type <txt>T</txt>.
        /// </summary>
        /// <param name="matrix">The matrix to compare.</param>
        /// <returns>Whether the two matrices are equal in value.</returns>
        public bool Equals(CSRSparseMatrix<T> matrix)
        {
            return Equals(matrix, EqualityComparer<T>.Default);
        }

        public bool Equals(MCSRSparseMatrix<T> matrix, Func<T, T, bool> equals)
        {
            if (matrix is null)
            {
                return false;
            }
            return matrix.Equals(this, equals);
        }
        public bool Equals(MCSRSparseMatrix<T> matrix, IEqualityComparer<T> comparer)
        {
            if (matrix is null)
            {
                return false;
            }
            return matrix.Equals(this, comparer);
        }
        public bool Equals(MCSRSparseMatrix<T> matrix)
        {
            if (matrix is null)
            {
                return false;
            }
            return matrix.Equals(this);
        }

        public override bool Equals(object obj)
        {
            if (obj is null)
            {
                return false;
            }
            if (obj is CSRSparseMatrix<T>)
            {
                return Equals(obj as CSRSparseMatrix<T>);
            }
            if (obj is MCSRSparseMatrix<T>)
            {
                return Equals(obj as MCSRSparseMatrix<T>);
            }
            return Equals(obj as Matrix<T>);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        #endregion


        #region Tensor methods

        /// <summary>
        /// Returns the direct (tensorial) sum of this matrix with one or more other matrices.
        /// </summary>
        /// <param name="matrix">The matrices to sum with this matrix.</param>
        /// <returns>The direct sum.</returns>
        public CSRSparseMatrix<T> DirectSum(params CSRSparseMatrix<T>[] matrix)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            int rows = Rows, cols = Columns, nnz = _values.Length;
            for (int i = 0; i < matrix.Length; ++i)
            {
                CSRSparseMatrix<T> m = matrix[i];
                rows += m.Rows;
                cols += m.Columns;
                nnz += m.Values.Length;
            }

            int[] rowIndices = new int[rows + 1];
            int[] columnIndices = new int[nnz];
            T[] values = new T[nnz];
            Array.Copy(_rowIndices, rowIndices, Rows + 1);
            Array.Copy(_columnIndices, columnIndices, _values.Length);
            Array.Copy(_values, values, _values.Length);

            int row_offset = Rows, col_offset = Columns, k = _values.Length;
            for (int i = 0; i < matrix.Length; ++i)
            {
                CSRSparseMatrix<T> m = matrix[i];

                // k is the nnz entered so far
                for (int r = 1; r <= m.Rows; ++r)
                {
                    rowIndices[++row_offset] = k + m._rowIndices[r];
                }

                int len = m.Values.Length;
                for (int j = 0; j < len; ++j)
                {
                    columnIndices[k] = m._columnIndices[j] + col_offset;
                    values[k] = m._values[j];
                    ++k;
                }
            }

            return new CSRSparseMatrix<T>(cols, rowIndices, columnIndices, values);
        }

        public SparseTensor<T> TensorProduct(SparseVector<T> vector, IProvider<T> provider)
        {
            if (vector is null)
            {
                throw new ArgumentNullException(nameof(vector));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }

            SparseTensor<T> tensor = new SparseTensor<T>(Rows, Columns, vector.Dimension);

            int vect_len = vector.Indices.Length;
            int[] vect_indices = vector.Indices;
            T[] vect_values = vector.Values;

            int[] index = new int[3];
            for (int r = 0; r < Rows; ++r)
            {
                index[0] = r;
                int row_end = _rowIndices[r + 1];
                for (int i = _rowIndices[r]; i < row_end; ++i)
                {
                    index[1] = _columnIndices[i];
                    T e = _values[i];

                    for (int j = 0; j < vect_len; ++j)
                    {
                        index[2] = vect_indices[j];
                        tensor.Set(provider.Multiply(e, vect_values[j]), index);
                    }
                }
            }
            return tensor;
        }

        public SparseTensor<T> TensorProduct(CSRSparseMatrix<T> matrix, IProvider<T> provider)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }

            SparseTensor<T> tensor = new SparseTensor<T>(Rows, Columns, matrix.Rows, matrix.Columns);

            int[] index = new int[4];
            for (int r = 0; r < Rows; ++r)
            {
                index[0] = r;

                int row_end = _rowIndices[r + 1];
                for (int i = _rowIndices[r]; i < row_end; ++i)
                {
                    index[1] = _columnIndices[i];
                    T e = _values[i];

                    for (int s = 0; s < matrix.Rows; ++s)
                    {
                        index[2] = s;

                        int row_end_s = matrix._rowIndices[s + 1];
                        for (int j = matrix._rowIndices[s]; j < row_end_s; ++j)
                        {
                            index[3] = matrix._columnIndices[j];

                            tensor.Set(provider.Multiply(e, matrix._values[j]), index);
                        }
                    }
                }
            }
            return tensor;
        }

        public SparseTensor<T> TensorProduct(SparseTensor<T> tensor, IProvider<T> provider)
        {
            if (tensor is null)
            {
                throw new ArgumentNullException(nameof(tensor));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }

            int torder = tensor.Order, prodOrder = torder + 2;
            int[] index = new int[prodOrder];

            // Calculate the product tensor dimensions
            int[] tdims = tensor.GetDimensions();
            int[] dims = new int[prodOrder];
            dims[0] = Rows;
            dims[1] = Columns;
            for (int i = 0; i < torder; ++i)
            {
                dims[i + 2] = tdims[i];
            }

            SparseTensor<T> prod = new SparseTensor<T>(dims);
            foreach (TensorEntry<T> entry in tensor.NonZeroEntries)
            {
                Array.Copy(entry.Index, 0, index, 2, torder);
                T e = entry.Value;

                for (int r = 0; r < Rows; ++r)
                {
                    index[0] = r;
                    int row_end = _rowIndices[r + 1];
                    for (int i = _rowIndices[r]; i < row_end; ++i)
                    {
                        index[1] = _columnIndices[i];
                        prod.Set(provider.Multiply(e, _values[i]), index);
                    }
                }
            }
            return prod;
        }

        #endregion


        #region Operators

        public override Matrix<T> Add(Matrix<T> matrix, IProvider<T> provider)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            if (CompareRepresentationPriority(this, matrix) < 0)
            {
                return matrix.Add(this, provider); // return the sum as type(matrix)
            }
            return Add(matrix.ToCSRSparseMatrix(), provider);
        }

        /// <summary>
        /// Add this matrix to another, returning the result as a new CSR sparse matrix without changing 
        /// the original matrices.
        /// </summary>
        /// <param name="matrix">The matrix to add to this matrix.</param>
        /// <param name="provider">The provider to use for addition.</param>
        /// <returns>The matrix sum.</returns>
        public CSRSparseMatrix<T> Add(CSRSparseMatrix<T> matrix, IProvider<T> provider)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            MatrixChecks.AssertMatrixDimensionsEqual(this, matrix);

            int[] rowIndices = new int[Rows + 1];

            CSRHelper<T>.Add(
                _rowIndices, _columnIndices, _values,                       // matrix 1
                matrix._rowIndices, matrix._columnIndices, matrix._values,  // matrix 2
                rowIndices, out int[] colIndices, out T[] values,           // matrix sum
                Rows, provider);

            return new CSRSparseMatrix<T>(Columns, rowIndices, colIndices, values);
        }

        public CSRSparseMatrix<T> Add(CSRSparseMatrix<T> matrix, ISparseBLAS1Provider<T> blas1)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            if (blas1 is null)
            {
                throw new ArgumentNullException(nameof(blas1));
            }
            if (!(blas1 is ISparseBLAS1<T> b))
            {
                throw new NotSupportedException(ExceptionMessages.TypeNotSupported);
            }
            MatrixChecks.AssertMatrixDimensionsEqual(this, matrix);

            int[] rowIndices = new int[Rows + 1];

            CSRHelper<T>.Add(
                _rowIndices, _columnIndices, _values,                       // matrix 1
                matrix._rowIndices, matrix._columnIndices, matrix._values,  // matrix 2
                rowIndices, out int[] colIndices, out T[] values,           // matrix sum
                Rows, b);

            return new CSRSparseMatrix<T>(Columns, rowIndices, colIndices, values);
        }

        /// <summary>
        /// Returns the additive inverse of this matrix. A new matrix will be created, this matrix is unchanged.
        /// <p>This method is identical to <txt>Negate</txt>, it is re-implemented here for consistency of naming.</p>
        /// </summary>
        /// <param name="provder">The provider to use to negate elements of this matrix.</param>
        /// <returns></returns>
        public CSRSparseMatrix<T> AdditiveInverse(IProvider<T> provider)
        {
            return Negate(provider);
        }

        /// <summary>
        /// Subtract a matrix from this matrix, returning the result as a new CSR sparse matrix without changing 
        /// either of the original matrices.
        /// </summary>
        /// <param name="matrix">The matrix to subtract from this matrix.</param>
        /// <param name="provider">The provider for type <txt>T</txt>.</param>
        /// <returns>The matrix difference.</returns>
        public CSRSparseMatrix<T> Subtract(CSRSparseMatrix<T> matrix, IProvider<T> provider)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            MatrixChecks.AssertMatrixDimensionsEqual(this, matrix);

            int[] rowIndices = new int[Rows + 1];

            CSRHelper<T>.Subtract(
                _rowIndices, _columnIndices, _values,                       // matrix 1
                matrix._rowIndices, matrix._columnIndices, matrix._values,  // matrix 2
                rowIndices, out int[] colIndices, out T[] values,           // matrix diff
                Rows, provider);

            return new CSRSparseMatrix<T>(Columns, rowIndices, colIndices, values);
        }

        /// <summary>
        /// Returns the negation of this matrix (i.e. its additive identity $-A$), using the specified provider.
        /// A new matrix is created, and this matrix is unchanged.
        /// </summary>
        /// <param name="provider">The provider used to negate the elements of this matrix.</param>
        /// <returns>The matrix negation.</returns>
        public CSRSparseMatrix<T> Negate(IProvider<T> provider)
        {
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }

            CSRSparseMatrix<T> copy = Copy();
            copy.NegateInPlace(provider);
            return copy;
        }

        /// <summary>
        /// Overwrites this matrix with its negation ($-A$), using the specified provider.
        /// </summary>
        /// <param name="provider">The provider used to negate the elements of this matrix.</param>
        public void NegateInPlace(IProvider<T> provider)
        {
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }

            int len = _values.Length, i;
            for (i = 0; i < len; ++i)
            {
                _values[i] = provider.Negate(_values[i]);
            }
        }

        /// <summary>
        /// Postmultiply this matrix by another matrix of compatible dimension, and using the specified provider.
        /// A new matrix is created, and neither of the original matrices are modified.
        /// </summary>
        /// <param name="matrix">The matrix to postmultiply by.</param>
        /// <param name="provider">The provider to use to multiply elements of the matrices.</param>
        /// <returns>The matrix product, as a CSR sparse matrix.</returns>
        public CSRSparseMatrix<T> Multiply(CSRSparseMatrix<T> matrix, IProvider<T> provider)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            MatrixChecks.AssertMatrixDimensionsForMultiplication(this, matrix);

            // Holds the values of each row [result-column-index, value]
            Dictionary<int, T> temp_row_buffer = new Dictionary<int, T>();

            List<T> product_values = new List<T>();
            List<int> product_columnIndex = new List<int>();
            int[] product_rowIndex = new int[Rows + 1];

            for (int r = 0; r < Rows; ++r) 
            {
                int row_start = _rowIndices[r], row_end = _rowIndices[r + 1];

                // Clear the buffer which will hold the matrix product for this row.
                temp_row_buffer.Clear();

                for (int i = row_start; i < row_end; ++i)
                {
                    int colIndex = _columnIndices[i];
                    int B_row_start = matrix._rowIndices[colIndex],
                        B_row_end = matrix._rowIndices[colIndex + 1];

                    T a = _values[i];

                    for (int j = B_row_start; j < B_row_end; ++j)
                    {
                        T prod = provider.Multiply(a, matrix._values[j]);

                        int B_colIndex = matrix._columnIndices[j];
                        if (temp_row_buffer.ContainsKey(B_colIndex))
                        {
                            temp_row_buffer[B_colIndex] = provider.Add(temp_row_buffer[B_colIndex], prod);
                        }
                        else
                        {
                            temp_row_buffer[B_colIndex] = prod;
                        }
                    }
                }

                // Store the row buffer values into the main buffer
                int[] columnIndex = new int[temp_row_buffer.Keys.Count];

                // Collect all non-zero indexes into columnIndex, then sort 
                int k = 0;
                foreach (KeyValuePair<int, T> entry in temp_row_buffer)
                {
                    if (!provider.Equals(entry.Value, _zero))
                    {
                        columnIndex[k++] = entry.Key;
                    }
                }
                Array.Sort(columnIndex, 0, k);

                for (int i = 0; i < k; ++i)
                {
                    int index = columnIndex[i];
                    product_columnIndex.Add(index);
                    product_values.Add(temp_row_buffer[index]);
                }
                product_rowIndex[r + 1] = product_values.Count;
            }

            return new CSRSparseMatrix<T>(matrix.Columns, product_rowIndex, product_columnIndex.ToArray(), product_values.ToArray());
        }

        /// <summary>
        /// Postmultiply this matrix by a sparse vector, using the specified provider. Neither this matrix nor 
        /// the vector is changed.
        /// </summary>
        /// <param name="vector">The sparse vector to postmultiply by.</param>
        /// <param name="provider">The provider to use for elementwise multiplication and addition.</param>
        /// <returns>The matrix-vector product, as a sparse vector.</returns>
        public BigSparseVector<T> Multiply(BigSparseVector<T> vector, IProvider<T> provider)
        {
            if (vector is null)
            {
                throw new ArgumentNullException(nameof(vector));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            if (vector.Dimension != Columns)
            {
                throw new InvalidOperationException("The dimension of the vector does not match the number of columns of this matrix.");
            }

            Dictionary<long, T> x = vector.Values;
            Dictionary<long, T> prod = new Dictionary<long, T>(Rows);
            for (int r = 0; r < Rows; ++r)
            {
                int row_start = _rowIndices[r], row_end = _rowIndices[r + 1];

                T sum = provider.Zero;
                for (int i = row_start; i < row_end; ++i)
                {
                    int c = _columnIndices[i];
                    if (x.ContainsKey(c))
                    {
                        sum = provider.Add(sum, provider.Multiply(_values[i], x[c]));
                    }
                }

                // Shouldn't we be using the .Equals method?
                if (!provider.Equals(sum, _zero))
                {
                    prod[r] = sum;
                }
            }

            return new BigSparseVector<T>(Rows, prod);
        }

        /// <summary>
        /// Postmultiply this matrix by a sparse vector, using the specified provider.
        /// </summary>
        /// <param name="vector">The sparse vector to postmultiply by.</param>
        /// <param name="provider">The provider used for elementwise multiplication and addition.</param>
        /// <returns>The matrix-vector product, as a sparse vector.</returns>
        public override SparseVector<T> Multiply(SparseVector<T> vector, IProvider<T> provider)
        {
            if (vector is null)
            {
                throw new ArgumentNullException(nameof(vector));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            if (vector.Dimension != Columns)
            {
                throw new InvalidOperationException("The dimension of the vector does not match the number of columns of this matrix.");
            }

            return CSRHelper<T>.Multiply(vector, provider, _rowIndices, _columnIndices, _values, Rows);

            /*
            T[] vect_values = vector.Values;
            int[] vect_indices = vector.Indices;
            int vlen = vect_indices.Length;

            List<T> prod_values = new List<T>();
            List<int> prod_indices = new List<int>();

            for (int r = 0; r < Rows; ++r)
            {
                int row_end = _rowIndices[r + 1];
                int v = 0;

                T sum = provider.Zero;
                bool any = false;

                for (int i = _rowIndices[r]; i < row_end; ++i)
                {
                    int c = _columnIndices[i];
                    while (c > vect_indices[v])
                    {
                        ++v;
                        if (v >= vlen)
                        {
                            goto end;
                        }
                    }

                    if (c == vect_indices[v])
                    {
                        sum = provider.Add(sum, provider.Multiply(_values[i], vect_values[v]));
                        any = true;
                    }
                }

                end:
                if (any)
                {
                    prod_indices.Add(r);
                    prod_values.Add(sum);
                }
            }

            return new SparseVector<T>(Rows, prod_indices.ToArray(), prod_values.ToArray(), true);*/
        }

        /// <summary>
        /// Postmultiply the transpose of this matrix by a sparse vector, using the specified provider.
        /// The result is returned as a new sparse vector, and the original multiplicands are unchanged.
        /// </summary>
        /// <param name="vector">The vector to multiply by.</param>
        /// <param name="provider">The provider used for elementwise multiplication and addition.</param>
        /// <returns>The product of a matrix transpose and a vector.</returns>
        public override SparseVector<T> TransposeAndMultiply(SparseVector<T> vector, IProvider<T> provider)
        {
            if (vector is null)
            {
                throw new ArgumentNullException(nameof(vector));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            if (Rows != vector.Dimension)
            {
                throw new InvalidOperationException("The number of rows of the matrix does not match the dimension of the vector.");
            }

            return CSRHelper<T>.TransposeAndMultiply(vector, provider, _rowIndices, _columnIndices, _values, Rows, Columns);
        }

        internal override void Multiply(T[] vector, T[] product, IProvider<T> provider, bool increment)
        {
            if (vector is null)
            {
                throw new ArgumentNullException(nameof(vector));
            }
            if (product is null)
            {
                throw new ArgumentNullException(nameof(product));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            if (vector.Length != Columns)
            {
                throw new InvalidOperationException("The number of columns of this matrix does not match the dimension of the vector.");
            }
            if (product.Length < Rows)
            {
                throw new ArgumentOutOfRangeException("The result vector is not large enough to store the matrix-vector product.");
            }

            CSRHelper<T>.Multiply(vector, product, provider, _rowIndices, _columnIndices, _values, Rows, increment);
        }

        internal override void TransposeAndMultiply(T[] vector, T[] result, IProvider<T> provider, bool increment = false)
        {
            CSRHelper<T>.TransposeAndMultiply(vector, result, provider, _rowIndices, _columnIndices, _values, Rows, Columns, increment);
        }

        /// <summary>
        /// Postmultiply this matrix by a dense vector in parallel, and store the product in <txt>result</txt>.
        /// </summary>
        /// <param name="vector">The dense vector to postmultiply by.</param>
        /// <param name="result">The vector to store the product in.</param>
        /// <param name="provider">The provider used for multiplying matrix elements.</param>
        public void MultiplyParallel(DenseVector<T> vector, DenseVector<T> result, IProvider<T> provider, ParallelOptions options = null)
        {
            if (vector is null)
            {
                throw new ArgumentNullException(nameof(vector));
            }
            if (result is null)
            {
                throw new ArgumentNullException(nameof(result));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            if (vector.Dimension != Columns)
            {
                throw new InvalidOperationException("The number of columns of this matrix does not match the dimension of the vector.");
            }
            if (result.Dimension < Rows)
            {
                throw new ArgumentOutOfRangeException("The result vector is not large enough to store the matrix-vector product.");
            }

            if (options is null)
            {
                options = new ParallelOptions();
            }

            T[] values = vector.Values, result_values = result.Values;
            
            Parallel.ForEach(Partitioner.Create(0, Rows, 16), options, range =>
            {
                int min = range.Item1, max = range.Item2; 
                int row_start, row_end = _rowIndices[min];
                for (int r = min; r < max; ++r)
                {
                    row_start = row_end;
                    row_end = _rowIndices[r + 1];

                    T sum = provider.Zero;
                    for (int i = row_start; i < row_end; ++i)
                    {
                        sum = provider.Add(sum, provider.Multiply(_values[i], values[_columnIndices[i]]));
                    }
                    result_values[r] = sum;
                }
            });
        }

        /// <summary>
        /// Multiply this matrix by a scalar, using the specified provider. A new matrix is created, and this matrix
        /// is not modified.
        /// </summary>
        /// <param name="scalar">The scalar value to multiply by.</param>
        /// <param name="provider">The provider to use for elementwise multiplication.</param>
        /// <returns>The matrix-scalar product, as a CSR sparse matrix.</returns>
        public CSRSparseMatrix<T> Multiply(T scalar, IProvider<T> provider)
        {
            if (scalar is null)
            {
                throw new ArgumentNullException(nameof(scalar));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }

            CSRSparseMatrix<T> copy = new CSRSparseMatrix<T>(this);
            copy.MultiplyInPlace(scalar, provider);
            return copy;
        }

        /// <summary>
        /// Multiply this matrix by a scalar and overwrite this matrix with the product.
        /// </summary>
        /// <param name="scalar">The scalar value to multiply by.</param>
        /// <param name="provider">The provider to use to multiply the matrix elements.</param>
        public override void MultiplyInPlace(T scalar, IProvider<T> provider)
        {
            if (scalar is null)
            {
                throw new ArgumentNullException(nameof(scalar));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }

            int len = _values.Length, i;
            for (i = 0; i < len; ++i)
            {
                _values[i] = provider.Multiply(_values[i], scalar);
            }
        }

        /// <summary>
        /// Multiply this matrix by a scalar, using the specified level-1 BLAS provider, and returning a new matrix.
        /// This matrix is unchanged.
        /// </summary>
        /// <param name="scalar">The scalar value to multiply by.</param>
        /// <param name="blas1">The level-1 BLAS implementation</param>
        /// <returns>The matrix-scalar product as a CSR sparse matrix.</returns>
        public CSRSparseMatrix<T> Multiply(T scalar, IDenseBLAS1Provider<T> blas1)
        {
            CSRSparseMatrix<T> copy = Copy();
            copy.MultiplyInPlace(scalar, blas1);
            return copy;
        }

        /// <summary>
        /// Multiply this matrix by a scalar using the specified level-1 BLAS implementation, then overwrite 
        /// this matrix with the result. 
        /// </summary>
        /// <param name="scalar">The scalar value to multiply by.</param>
        /// <param name="blas1">The level-1 BLAS implementation.</param>
        public void MultiplyInPlace(T scalar, IDenseBLAS1Provider<T> blas1)
        {
            if (scalar is null)
            {
                throw new ArgumentNullException(nameof(scalar));
            }
            if (blas1 is null)
            {
                throw new ArgumentNullException(nameof(blas1));
            }
            if (!(blas1 is IDenseBLAS1<T> blas))
            {
                throw new NotSupportedException(ExceptionMessages.TypeNotSupported);
            }

            blas.SCAL(_values, scalar, 0, _values.Length);
        }

        /// <summary>
        /// Postmultiply this matrix by a permutation matrix, then overwrite this matrix with the result. 
        /// This is equivalent to permuting the columns of this matrix using the permutation represented 
        /// by this matrix.
        /// </summary>
        /// <param name="permutationMatrix">The permutation matrix to postmultiply by.</param>
        public void MultiplyInPlace(PermutationMatrix permutationMatrix)
        {
            if (permutationMatrix is null)
            {
                throw new ArgumentNullException(nameof(permutationMatrix));
            }
            if (Columns != permutationMatrix.Dimension)
            {
                throw new InvalidOperationException("The number of columns of this matrix does not match the number of rows of the permutation matrix.");
            }

            PermuteColumns(permutationMatrix.PermutationVector);
        }

        /// <summary>
        /// Divide this matrix by a scalar using the specified provider, returning a new matrix and leaving this 
        /// matrix unchanged.
        /// </summary>
        /// <param name="scalar">The scalar value to multiply by.</param>
        /// <param name="provider">The provider to use for dividing matrix elements.</param>
        /// <returns>The matrix divided by a scalar.</returns>
        public CSRSparseMatrix<T> Divide(T scalar, IProvider<T> provider)
        {
            CSRSparseMatrix<T> copy = Copy();
            DivideInPlace(scalar, provider);
            return copy;
        }

        /// <summary>
        /// Divide this matrix by a scalar using the specified level-1 BLAS implementation. A new matrix is 
        /// returned and this matrix is unchanged.
        /// </summary>
        /// <param name="scalar">The scalar value to divide by.</param>
        /// <param name="blas1">The level-1 BLAS implementation.</param>
        /// <returns>The matrix-scalar division result.</returns>
        public CSRSparseMatrix<T> Divide(T scalar, IDenseBLAS1Provider<T> blas1)
        {
            CSRSparseMatrix<T> copy = Copy();
            copy.DivideInPlace(scalar, blas1);
            return copy;
        }

        /// <summary>
        /// Divide this matrix by a scalar using the specified level-1 BLAS implementation, then overwrite this 
        /// matrix by the result.
        /// </summary>
        /// <param name="scalar">The scalar value to divide by.</param>
        /// <param name="blas1">The level-1 BLAS implementation.</param>
        public void DivideInPlace(T scalar, IDenseBLAS1Provider<T> blas1)
        {
            if (scalar is null)
            {
                throw new ArgumentNullException(nameof(scalar));
            }
            if (blas1 is null)
            {
                throw new ArgumentNullException(nameof(blas1));
            }
            if (!(blas1 is IDenseBLAS1<T> blas)) 
            {
                throw new NotSupportedException(ExceptionMessages.TypeNotSupported);
            }

            blas.SCAL(_values, blas.Provider.Divide(blas.Provider.One, scalar), 0, _values.Length);
        }

        #endregion


        public override object Clone()
        {
            return Copy();
        }

        /// <summary>
        /// Create a deep copy of this matrix.
        /// </summary>
        /// <returns>A deep copy of this matrix.</returns>
        public CSRSparseMatrix<T> Copy()
        {
            return new CSRSparseMatrix<T>(Columns, _rowIndices.Copy(), _columnIndices.Copy(), _values.Copy());
        }

        public override BandMatrix<T> ToBandMatrix()
        {
            return new BandMatrix<T>(this);
        }

        public override BlockDiagonalMatrix<T> ToBlockDiagonalMatrix()
        {
            return new BlockDiagonalMatrix<T>(this);
        }

        /// <summary>
        /// Return this matrix as a sparse matrix in Coordinate List (COO) format.
        /// </summary>
        /// <returns>This matrix as COO sparse matrix form.</returns>
        public override COOSparseMatrix<T> ToCOOSparseMatrix()
        {
            return new COOSparseMatrix<T>(this);
        }

        /// <summary>
        /// Return this matrix as a sparse matrix in Compressed Sparse Column (CSC) form.
        /// </summary>
        /// <returns></returns>
        public override CSCSparseMatrix<T> ToCSCSparseMatrix()
        {
            return new CSCSparseMatrix<T>(this);
        }

        public override CSRSparseMatrix<T> ToCSRSparseMatrix()
        {
            return this;
        }

        /// <summary>
        /// Return this matrix as a DOK sparse matrix.
        /// </summary>
        /// <returns>This matrix as a sparse DOK matrix.</returns>
        public override DOKSparseMatrix<T> ToDOKSparseMatrix()
        {
            return new DOKSparseMatrix<T>(this);
        }

        public override DenseMatrix<T> ToDenseMatrix()
        {
            return new DenseMatrix<T>(this);
        }

        public override DiagonalMatrix<T> ToDiagonalMatrix()
        {
            return new DiagonalMatrix<T>(this);
        }

        /// <summary>
        /// Return this matrix as a sparse matrix in Modified Compressed Sparse Row (MCSR) format.
        /// </summary>
        /// <returns>The MCSR sparse matrix.</returns>
        public override MCSRSparseMatrix<T> ToMCSRSparseMatrix()
        {
            return new MCSRSparseMatrix<T>(this);
        }

        public override SKYSparseMatrix<T> ToSKYSparseMatrix()
        {
            return new SKYSparseMatrix<T>(this);
        }


        #region Operators

        /// <summary>
        /// Returns the matrix sum between two CSR sparse matrices.
        /// </summary>
        /// <param name="A">The left matrix.</param>
        /// <param name="B">The right matrix.</param>
        /// <returns>The matrix sum.</returns>
        public static CSRSparseMatrix<T> operator +(CSRSparseMatrix<T> A, CSRSparseMatrix<T> B)
        {
            if (A is null)
            {
                throw new ArgumentNullException(nameof(A));
            }
            return A.Add(B, ProviderFactory.GetDefaultProvider<T>());
        }

        /// <summary>
        /// Returns the left matrix subtract the right matrix.
        /// </summary>
        /// <param name="A">The left matrix.</param>
        /// <param name="B">The right matrix.</param>
        /// <returns>The matrix difference.</returns>
        public static CSRSparseMatrix<T> operator -(CSRSparseMatrix<T> A, CSRSparseMatrix<T> B)
        {
            if (A is null)
            {
                throw new ArgumentNullException(nameof(A));
            }
            return A.Subtract(B, ProviderFactory.GetDefaultProvider<T>());
        }

        /// <summary>
        /// Return the negation (additive inverse) of a matrix.
        /// </summary>
        /// <param name="A">The matrix.</param>
        /// <returns>The matrix negation.</returns>
        public static CSRSparseMatrix<T> operator -(CSRSparseMatrix<T> A)
        {
            if (A is null)
            {
                throw new ArgumentNullException(nameof(A));
            }
            return A.Negate(ProviderFactory.GetDefaultProvider<T>());
        }

        /// <summary>
        /// Returns the product of two matrices.
        /// </summary>
        /// <param name="A">The left matrix.</param>
        /// <param name="B">The right matrix.</param>
        /// <returns>The matrix product.</returns>
        public static CSRSparseMatrix<T> operator *(CSRSparseMatrix<T> A, CSRSparseMatrix<T> B)
        {
            if (A is null)
            {
                throw new ArgumentNullException(nameof(A));
            }
            return A.Multiply(B, ProviderFactory.GetDefaultProvider<T>());
        }

        /// <summary>
        /// Returns the sparse matrix-vector product as a sparse vector.
        /// </summary>
        /// <param name="A">The sparse matrix.</param>
        /// <param name="x">The sparse vector.</param>
        /// <returns>The matrix-vector product.</returns>
        public static BigSparseVector<T> operator *(CSRSparseMatrix<T> A, BigSparseVector<T> x)
        {
            if (A is null)
            {
                throw new ArgumentNullException(nameof(A));
            }
            return A.Multiply(x, ProviderFactory.GetDefaultProvider<T>());
        }

        /// <summary>
        /// Returns the product between a sparse matrix and a sparse vector, as another sparse vector.
        /// </summary>
        /// <param name="A">The sparse matrix.</param>
        /// <param name="x">The sparse vector.</param>
        /// <returns>The sparse matrix-vector product.</returns>
        public static SparseVector<T> operator *(CSRSparseMatrix<T> A, SparseVector<T> x)
        {
            if (A is null)
            {
                throw new ArgumentNullException(nameof(A));
            }
            return A.Multiply(x, ProviderFactory.GetDefaultProvider<T>());
        }

        /// <summary>
        /// Returns the product between a sparse matrix and a scalar.
        /// </summary>
        /// <param name="A">The sparse matrix.</param>
        /// <param name="scalar">The scalar.</param>
        /// <returns>The matrix-scalar product.</returns>
        public static CSRSparseMatrix<T> operator *(CSRSparseMatrix<T> A, T scalar)
        {
            if (A is null)
            {
                throw new ArgumentNullException(nameof(A));
            }
            return A.Multiply(scalar, ProviderFactory.GetDefaultBLAS1<T>());
        }

        /// <summary>
        /// Returns the product between a sparse matrix and a scalar.
        /// </summary>
        /// <param name="scalar">The scalar.</param>
        /// <param name="A">The sparse matrix.</param>
        /// <returns>The matrix-scalar product.</returns>
        public static CSRSparseMatrix<T> operator *(T scalar, CSRSparseMatrix<T> A) => A * scalar;

        /// <summary>
        /// Returns a sparse matrix divided by a scalar.
        /// </summary>
        /// <param name="A">The sparse matrix.</param>
        /// <param name="scalar">The scalar to divide by.</param>
        /// <returns>The matrix divided by the scalar.</returns>
        public static CSRSparseMatrix<T> operator /(CSRSparseMatrix<T> A, T scalar)
        {
            if (A is null)
            {
                throw new ArgumentNullException(nameof(A));
            }
            return A.Divide(scalar, ProviderFactory.GetDefaultBLAS1<T>());
        }

        /// <summary>
        /// Returns whether two sparse matrices are equal in value to each other, using the default comparer
        /// for type <txt>T</txt> to perform elementwise comparisons. Note that two null matrices are 
        /// considered unequal.
        /// </summary>
        /// <param name="A">The first matrix to compare.</param>
        /// <param name="B">The second matrix to compare.</param>
        /// <returns>Whether the two matrices are equal in value.</returns>
        public static bool operator ==(CSRSparseMatrix<T> A, CSRSparseMatrix<T> B)
        {
            if (A is null || B is null)
            {
                return false;
            }
            return A.Equals(B);
        }
        /// <summary>
        /// Returns whether two sparse matrices are not equal to each other in value, using the default comparer
        /// for type <txt>T</txt> to perform elementwise comparisons. Note that this operation will return true 
        /// if either of the matrices are null.
        /// </summary>
        /// <param name="A">The first matrix to compare.</param>
        /// <param name="B">The second matrix to compare.</param>
        /// <returns>Whether the two matrices are not equal in value.</returns>
        public static bool operator !=(CSRSparseMatrix<T> A, CSRSparseMatrix<T> B) => !(A == B);

        #endregion


        #region Casts

        public static explicit operator CSRSparseMatrix<T>(BandMatrix<T> matrix) => new CSRSparseMatrix<T>(matrix);
        public static explicit operator CSRSparseMatrix<T>(BlockDiagonalMatrix<T> matrix) => new CSRSparseMatrix<T>(matrix);
        public static explicit operator CSRSparseMatrix<T>(COOSparseMatrix<T> matrix) => new CSRSparseMatrix<T>(matrix);
        public static explicit operator CSRSparseMatrix<T>(CSCSparseMatrix<T> matrix) => new CSRSparseMatrix<T>(matrix);
        public static explicit operator CSRSparseMatrix<T>(DenseMatrix<T> matrix) => new CSRSparseMatrix<T>(matrix);
        public static explicit operator CSRSparseMatrix<T>(DiagonalMatrix<T> matrix) => new CSRSparseMatrix<T>(matrix);
        public static explicit operator CSRSparseMatrix<T>(DOKSparseMatrix<T> matrix) => new CSRSparseMatrix<T>(matrix);
        public static explicit operator CSRSparseMatrix<T>(MCSRSparseMatrix<T> matrix) => new CSRSparseMatrix<T>(matrix);
        public static explicit operator CSRSparseMatrix<T>(SKYSparseMatrix<T> matrix) => new CSRSparseMatrix<T>(matrix);

        #endregion
    }
}
