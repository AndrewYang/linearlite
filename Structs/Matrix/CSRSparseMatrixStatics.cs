﻿using LinearNet.Global;
using LinearNet.Helpers;
using LinearNet.Providers;
using System;
using System.Collections.Generic;
using System.IO;

namespace LinearNet.Structs
{
    /// <summary>
    /// Static class containing method for creating CSR sparse matrices.
    /// </summary>
    /// <cat>linear-algebra</cat>
    public static class CSRSparseMatrix
    {
        public static CSRSparseMatrix<T> Zero<T>(int rows, int columns) where T : new()
        {
            return new CSRSparseMatrix<T>(rows, columns);   // checks will occur within the constructor
        }
        public static CSRSparseMatrix<T> Random<T>(int rows, int columns, double approxDensity, Func<T> random = null, int seed = int.MinValue) where T : new()
        {
            if (rows <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(rows), ExceptionMessages.NonPositiveNotAllowed);
            }
            if (columns <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(columns), ExceptionMessages.NonPositiveNotAllowed);
            }
            if (approxDensity < 0.0 || approxDensity > 1.0)
            {
                throw new ArgumentOutOfRangeException(nameof(approxDensity), ExceptionMessages.NotBetween0And1);
            }
            if (random is null)
            {
                random = DefaultGenerators.GetRandomGenerator<T>();
            }

            int[] rowIndices = new int[rows + 1];

            int estCount = Math.Max(1, (int)((rows * columns) * approxDensity));
            List<int> columnIndices = new List<int>(estCount);
            List<T> values = new List<T>(estCount);

            Random rand = seed == int.MinValue ? new Random() : new Random(seed);
            for (int r = 1; r <= rows; ++r)
            {
                for (int c = 0; c < columns; ++c)
                {
                    if (rand.NextDouble() < approxDensity)
                    {
                        columnIndices.Add(c);
                        values.Add(random());
                    }
                }
                rowIndices[r] = columnIndices.Count;
            }
            return new CSRSparseMatrix<T>(columns, rowIndices, columnIndices.ToArray(), values.ToArray());
        }
        public static CSRSparseMatrix<T> Diag<T>(T[] diagonal) where T : new()
        {
            int dim = diagonal.Length;

            int[] rowIndices = new int[dim + 1];
            for (int r = 1; r <= dim; ++r)
            {
                rowIndices[r] = r;
            }

            int[] columnIndices = new int[dim];
            T[] elements = diagonal.Copy();
            for (int i = 0; i < dim; ++i)
            {
                columnIndices[i] = i;
            }

            return new CSRSparseMatrix<T>(dim, rowIndices, columnIndices, elements);
        }
        public static CSRSparseMatrix<T> Identity<T>(int dim) where T : new()
        {
            if (dim <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(dim), ExceptionMessages.NonPositiveNotAllowed);
            }

            IProvider<T> provider = ProviderFactory.GetDefaultProvider<T>();
            return Diag(RectangularVector.Repeat(provider.One, dim));
        }

        /// <summary>
        /// Load a matrix stored in a file in triplet form (see COOSparseMatrix)
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="path"></param>
        /// <param name=""></param>
        /// <returns></returns>
        public static CSRSparseMatrix<T> LoadTriplet<T>(string path, Func<string, T> parse, char delim = ',', 
            bool raiseNonCriticalExceptions = true, int indexing = 0) where T : new()
        {
            if (string.IsNullOrEmpty(path))
            {
                throw new ArgumentNullException(nameof(path));
            }
            if (parse is null)
            {
                throw new ArgumentNullException(nameof(parse));
            }
            if (!File.Exists(path))
            {
                throw new ArgumentException(nameof(path), "File does not exist.");
            }

            // First row is [row] [column] [nnz] (the meta info)
            int rows = -1, columns = -1, k = 0;
            int[] row_counts = null, 
                row_indices = null,
                column_indices = null;
            T[] values = null;

            bool foundMetaInfo = false, 
                sorted = true;

            using var fs = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
            using var sr = new StreamReader(fs);

            while (sr.Peek() > -1)
            {
                string line = sr.ReadLine();
                if (string.IsNullOrEmpty(line))
                {
                    if (raiseNonCriticalExceptions)
                    {
                        throw new FormatException($"Null or empty line.");
                    }
                    continue;
                }

                // Ignore comments
                char first = line[0];
                if (first == '%' || first == '#' || line.StartsWith("//")) continue;

                string[] split = line.Split(delim);
                if (split.Length < 3)
                {
                    if (raiseNonCriticalExceptions)
                    {
                        throw new FormatException($"Invalid number of entries in the line: {line}.");
                    }
                    continue; // fail silently
                }

                // Parse meta info
                if (!foundMetaInfo)
                {
                    if (!int.TryParse(split[0], out rows) || !int.TryParse(split[1], out columns) || !int.TryParse(split[2], out int nnz))
                    {
                        throw new FormatException("Invalid meta info: expected 3 integers.");
                    }
                    row_counts = new int[rows];

                    // Triplet storage
                    row_indices = new int[nnz];
                    column_indices = new int[nnz];
                    values = new T[nnz];
                    foundMetaInfo = true;
                }
                else
                {
                    int r, c = 0;
                    if (!int.TryParse(split[0], out r) || !int.TryParse(split[1], out c))
                    {
                        if (raiseNonCriticalExceptions)
                        {
                            throw new FormatException($"Row or column index is not an integer in the line '{line}'.");
                        }
                    }

                    r -= indexing;
                    c -= indexing;

                    if (r < 0 || r >= rows)
                    {
                        throw new ArgumentOutOfRangeException("Row index out of range.");
                    }
                    if (c < 0 || c >= columns)
                    {
                        throw new ArgumentOutOfRangeException("Column index out of range.");
                    }

                    row_counts[r]++;
                    row_indices[k] = r;
                    column_indices[k] = c;
                    values[k] = parse(split[2]);

                    // Check sort
                    if (sorted && k > 0 && Before(r, c, row_indices[k - 1], column_indices[k - 1]))
                    {
                        sorted = false;
                    }
                    ++k;
                }
            }

            // Accumulate the row indices
            int[] pointers = new int[rows + 1];
            for (int r = 0; r < rows; ++r)
            {
                pointers[r + 1] = pointers[r] + row_counts[r];
            }

            if (sorted)
            {
                // If already in sorted order, then we are done
                return new CSRSparseMatrix<T>(columns, pointers, column_indices, values);
            }
            else
            {
                // If not in sorted order, then we need to sort
                // Keep track of the number of elements visited so far in each row
                int[] counts = new int[rows];

                // Sorted arrays (would be too expensive to do a single sort in place, although
                // that would make the code considerably simpler...)
                int nnz = column_indices.Length;
                int[] sorted_column_indices = new int[nnz];
                T[] sorted_values = new T[nnz];

                // Group into correct row buckets
                for (int i = 0; i < nnz; ++i)
                {
                    int r = row_indices[i];
                    int index = pointers[r] + counts[r];
                    sorted_column_indices[index] = column_indices[i];
                    sorted_values[index] = values[i];
                    counts[r]++;
                }

                // Sort within each row
                for (int r = 0; r < rows; ++r)
                {
                    int start = pointers[r], end = pointers[r + 1];
                    if (end > start)
                    {
                        Array.Sort(sorted_column_indices, sorted_values, start, end - start);
                    }
                }

                // Thank laawd for the garbage collector...

                return new CSRSparseMatrix<T>(columns, pointers, sorted_column_indices, sorted_values);
            }
        }
        private static bool Before(int r1, int c1, int r2, int c2)
        {
            // Either in a previous row or in the same row and a previous column
            return r1 < r2 || (r1 == r2 && c1 < c2);
        }
    }
}
