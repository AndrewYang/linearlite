﻿using LinearNet.Providers;
using LinearNet.Vectors;
using System;
using System.Collections.Generic;
using LinearNet.Global;
using System.IO;

namespace LinearNet.Structs
{
    /// <summary>
    /// <p>
    /// Implementation of a DOK (dictionary of keys) sparse matrix. This class is well-suited for incrementally constructing 
    /// sparse matrices, but slow for matrix operations. For best results, construct sparse matrices in this format then 
    /// convert into CSR form.
    /// </p>
    /// 
    /// <p>
    /// This matrix can be constructed over any non-abstract type <txt>T</txt>, however certain arithmetic operations, such 
    /// as matrix multiplication, can only be applied to sparse matrices over primitive types and custom types that implement 
    /// the <txt>IRing&ltT&gt</txt> interface. Such functionality are provided through extension methods.
    /// </p>
    /// 
    /// <p>
    /// The value of the default constructor (<txt>new T()</txt>) is assumed to be identically zero. This is important since 
    /// all elements equal to zero are not stored by the matrix. Here, equality of matrix elements is established 
    /// using the <txt>Equals(object)</txt> method of the type <txt>T</txt> itself. 
    /// </p>
    /// </summary>
    /// <cat>linear-algebra</cat>
    public class DOKSparseMatrix<T> : Matrix<T> where T : new()
    {
        private readonly T _zero;
        private readonly Dictionary<long, T> _values;
        internal Dictionary<long, T> Values { get { return _values; } }

        /// <summary>
        /// Gets or sets the <txt>index</txt>-th row of the matrix.
        /// </summary>
        /// <param name="index">The index of the row to extract.</param>
        /// <returns>The <txt>index</txt>-th row of the matrix.</returns>
        public override T[] this[int index]
        {
            get
            {
                if (index < 0 || index >= Rows)
                {
                    throw new ArgumentOutOfRangeException();
                }

                int cols = Columns;
                T[] row = new T[cols];
                foreach (long key in _values.Keys)
                {
                    int rowIndex = (int)(key / cols);
                    if (rowIndex == index)
                    {
                        int colIndex = (int)(key % cols);
                        row[colIndex] = _values[key];
                    }
                }
                return row;
            }
            set 
            {
                if (index < 0 || index >= Rows)
                {
                    throw new ArgumentOutOfRangeException();
                }

                // Only add values that are not = new()
                T def = new T();
                T[] row = value;

                int cols = Columns, i;
                if (row.Length != cols)
                {
                    throw new ArgumentOutOfRangeException();
                }

                long keybase = (long)index * cols;
                for (i = 0; i < cols; ++i)
                {
                    if (!row[i].Equals(def))
                    {
                        long key = keybase + i;
                        _values[key] = row[i];
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the (<txt>row</txt>, <txt>col</txt>)-th element of the matrix.
        /// </summary>
        /// <param name="row">The row index of the element.</param>
        /// <param name="col">The column index of the element.</param>
        /// <returns>The element of the matrix at row <txt>row</txt> and column <txt>col</txt>.</returns>
        public override T this[int row, int col]
        {
            get
            {
                if (row < 0 || col < 0 || row >= Rows || col >= Columns)
                {
                    throw new ArgumentOutOfRangeException();
                }

                long key = get_key(row, col);
                return _values.ContainsKey(key) ? _values[key] : default;
            }
            set
            {
                if (row < 0 || col < 0 || row >= Rows || col >= Columns)
                {
                    throw new ArgumentOutOfRangeException();
                }

                _values[get_key(row, col)] = value;
            }
        }

        public override int SymbolicNonZeroCount => _values.Count;

        public override IEnumerable<MatrixEntry<T>> NonZeroEntries
        {
            get
            {
                long lc = Columns;
                foreach (KeyValuePair<long, T> e in _values)
                {
                    yield return new MatrixEntry<T>((int)(e.Key / lc), (int)(e.Key % lc), e.Value);
                }
            }
        }


        #region Constructors 

        /// <summary>
        /// Initializes a empty sparse matrix of the specified dimension. All elements are assumed to be zero.
        /// </summary>
        /// <param name="rows">The number of rows of the matrix.</param>
        /// <param name="columns">The number of columns of the matrix.</param>
        public DOKSparseMatrix(int rows, int columns) : base(MatrixStorageType.DOK, rows, columns)
        {
            _zero = new T();
            _values = new Dictionary<long, T>();
        }

        /// <summary>
        /// Initializes a sparse matrix using a band matrix.
        /// </summary>
        /// <param name="matrix">A band matrix.</param>
        public DOKSparseMatrix(BandMatrix<T> matrix) : base(MatrixStorageType.DOK)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            Rows = matrix.Rows;
            Columns = matrix.Columns;
            _zero = new T();
            _values = new Dictionary<long, T>(matrix.SymbolicNonZeroCount);

            int upperBandwidth = matrix.UpperBandwidth,
                lowerBandwidth = matrix.LowerBandwidth,
                len, b, i;

            T[][] upperBands = matrix.UpperBands;
            for (b = 0; b < upperBandwidth; ++b)
            {
                T[] band = upperBands[b];
                len = band.Length;
                for (i = 0; i < len; ++i)
                {
                    if (!band[i].Equals(_zero))
                    {
                        int row = i, col = i + b + 1;
                        _values[row * Columns + col] = band[i];
                    }
                }
            }

            T[] diag = matrix.Diagonal;
            len = diag.Length;
            for (i = 0; i < len; ++i)
            {
                if (!diag[i].Equals(_zero))
                {
                    _values[i * Columns + i] = diag[i];
                }
            }

            T[][] lowerBands = matrix.LowerBands;
            for (b = 0; b < lowerBandwidth; ++b)
            {
                T[] band = lowerBands[b];
                len = band.Length;
                for (i = 0; i < len; ++i)
                {
                    if (!band[i].Equals(_zero))
                    {
                        int row = i + b + 1, col = i;
                        _values[row * Columns + col] = band[i];
                    }
                }
            }
        }

        public DOKSparseMatrix(BlockDiagonalMatrix<T> matrix) : base(MatrixStorageType.DOK)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            Rows = matrix.Rows;
            Columns = matrix.Columns;
            _zero = new T();

            int count = 0,
                nBlocks = matrix.Blocks.Length;

            for (int b = 0; b < nBlocks; ++b)
            {
                int dim = matrix.Blocks[b].Rows;
                count += dim * dim;
            }

            _values = new Dictionary<long, T>(count);

            long lc = Columns;
            for (int b = 0; b < nBlocks; ++b)
            {
                DenseMatrix<T> block = matrix.Blocks[b];
                int offset = matrix.BlockIndices[b], dim = block.Rows;
                for (int r = 0; r < dim; ++r)
                {
                    T[] row = block.Values[r];
                    long rowIndex = lc * (r + offset) + offset;
                    for (int c = 0; c < dim; ++c)
                    {
                        _values.Add(rowIndex + c, row[c]);
                    }
                }
            }
        }

        /// <summary>
        /// Create a DOK sparse matrix from a COO sparse matrix.
        /// </summary>
        /// <param name="matrix">A sparse matrix in coordinate list form.</param>
        public DOKSparseMatrix(COOSparseMatrix<T> matrix) : base(MatrixStorageType.DOK)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            Rows = matrix.Rows;
            Columns = matrix.Columns;
            _zero = new T();
            _values = new Dictionary<long, T>(matrix.Values.Count);

            foreach (MatrixEntry<T> e in matrix.Values)
            {
                if (!e.Value.Equals(_zero))
                {
                    _values[get_key(e.RowIndex, e.ColumnIndex)] = e.Value;
                }
            }
        }

        public DOKSparseMatrix(CSCSparseMatrix<T> matrix) : base(MatrixStorageType.DOK)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            Rows = matrix.Rows;
            Columns = matrix.Columns;
            _zero = new T();

            int[] columnIndices = matrix.ColumnIndices,
                rowIndices = matrix.RowIndices;
            T[] values = matrix.Values;

            _values = new Dictionary<long, T>(values.Length);
            long lc = Columns;
            for (int c = 0; c < Columns; ++c)
            {
                int end = columnIndices[c + 1];
                for (int i = columnIndices[c]; i < end; ++i)
                {
                    _values.Add(rowIndices[i] * lc + c, values[i]);
                }
            }
        }

        /// <summary>
        /// Create a DOK sparse matrix from a CSR sparse matrix.
        /// </summary>
        /// <param name="matrix">A sparse matrix in CSR format.</param>
        public DOKSparseMatrix(CSRSparseMatrix<T> matrix) : base(MatrixStorageType.DOK)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            Rows = matrix.Rows;
            Columns = matrix.Columns;
            _zero = new T();
            _values = new Dictionary<long, T>(matrix.Values.Length);

            int[] rowIndices = matrix.RowIndices, columnIndices = matrix.ColumnIndices;
            T[] values = matrix.Values;

            int rows = Rows;
            long lc = Columns;
            for (int r = 0; r < rows; ++r)
            {
                int row_end = rowIndices[r + 1];
                long row_offset = r * lc;
                for (int i = rowIndices[r]; i < row_end; ++i)
                {
                    _values[row_offset + columnIndices[i]] = values[i];
                }
            }
        }

        /// <summary>
        /// Initializes a sparse matrix given a dense matrix.
        /// </summary>
        /// <param name="matrix">A dense matrix.</param>
        public DOKSparseMatrix(DenseMatrix<T> matrix) : base(MatrixStorageType.DOK)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            Rows = matrix.Rows;
            Columns = matrix.Columns;

            _values = new Dictionary<long, T>(Rows * Columns);
            _zero = new T();

            long key = 0;
            int i, j;
            for (i = 0; i < matrix.Rows; ++i)
            {
                T[] row = matrix[i];
                for (j = 0; j < matrix.Columns; ++j)
                {
                    if (!row[j].Equals(_zero))
                    {
                        _values[key] = row[j];
                    }
                    key++;
                }
            }
        }

        /// <summary>
        /// Initializes a sparse matrix using a diagonal matrix.
        /// </summary>
        /// <param name="matrix">A diagonal matrix.</param>
        public DOKSparseMatrix(DiagonalMatrix<T> matrix) : base(MatrixStorageType.DOK)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            Rows = matrix.Rows;
            Columns = matrix.Columns;

            T[] diagonal = matrix.DiagonalTerms;
            long lc = Columns;
            for (int i = 0; i < diagonal.Length; ++i)
            {
                if (!diagonal[i].Equals(_zero))
                {
                    long key = i * lc + i;
                    _values[key] = diagonal[i];
                }
            }
        }

        public DOKSparseMatrix(Matrix<T> matrix) : base(MatrixStorageType.DOK)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            
            Rows = matrix.Rows;
            Columns = matrix.Columns;
            _zero = new T();

            _values = new Dictionary<long, T>(matrix.SymbolicNonZeroCount);
            long lc = Columns;
            foreach (MatrixEntry<T> e in matrix.NonZeroEntries)
            {
                _values.Add(e.RowIndex * lc + e.ColumnIndex, e.Value);
            }
        }

        /// <summary>
        /// Create a DOK sparse matrix from the values in a sparse vector. The 
        /// number of rows of the matrix will be equal to the dimension of the vector
        /// divided by <txt>columns</txt>.
        /// </summary>
        /// <param name="vector">The sparse vector to convert into a sparse matrix.</param>
        /// <param name="columns">The number of columns in this matrix.</param>
        /// <param name="rowMajor">
        /// Specifies whether the matrix values should be traversed in row-major order, i.e. the matrix 
        /// will be created by filling each row, one at a time.
        /// </param>
        public DOKSparseMatrix(BigSparseVector<T> vector, int columns, bool rowMajor = true) : base(MatrixStorageType.DOK)
        {
            if (vector is null)
            {
                throw new ArgumentNullException(nameof(vector));
            }
            if (columns <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(columns));
            }
            if (vector.Dimension % columns != 0)
            {
                throw new InvalidOperationException("The dimension of the vector is not divisible by the number of columns.");
            }

            Rows = vector.Dimension / columns;
            Columns = columns;
            _zero = new T();

            // Iterate in row-major order
            if (rowMajor)
            {
                _values = new Dictionary<long, T>(vector.Values);
            }

            // Iterate in column-major order
            else
            {
                _values = new Dictionary<long, T>(vector.Values.Count);
                foreach (KeyValuePair<long, T> pair in vector.Values)
                {
                    long row = pair.Key % Rows, column = pair.Key / Rows;
                    _values[row * columns + column] = pair.Value;
                }
            }
        }

        internal DOKSparseMatrix(int rows, int columns, Dictionary<long, T> values) : base(MatrixStorageType.DOK, rows, columns)
        {
            _values = values;
        }

        #endregion


        private long get_key(int rows, int columns) => (long)rows * Columns + columns;

        internal Dictionary<long, Dictionary<long, T>> GroupByRows()
        {
            long m = Columns;
            int row_dict_capacity = Math.Min(_values.Count, (int)m);

            // To row dictionary form
            var values = new Dictionary<long, Dictionary<long, T>>(row_dict_capacity);
            foreach (KeyValuePair<long, T> pair in _values)
            {
                long row = pair.Key / m, column = pair.Key % m;
                if (values.ContainsKey(row))
                {
                    values[row][column] = pair.Value;
                }
                else
                {
                    var rowDict = new Dictionary<long, T>(row_dict_capacity)
                    {
                        [column] = pair.Value
                    };
                    values[row] = rowDict;
                }
            }
            return values;
        }
        internal Dictionary<long, Dictionary<long, T>> GroupByColumns()
        {
            // To row dictionary form
            var values = new Dictionary<long, Dictionary<long, T>>();
            long m = Columns;

            foreach (KeyValuePair<long, T> pair in _values)
            {
                long row = pair.Key / m, column = pair.Key % m;
                if (values.ContainsKey(column))
                {
                    values[column][row] = pair.Value;
                }
                else
                {
                    var columnDict = new Dictionary<long, T>
                    {
                        [row] = pair.Value
                    };
                    values[column] = columnDict;
                }
            }
            return values;
        }

        internal DOKSparseMatrix<T> Multiply(DOKSparseMatrix<T> matrix, ISparseBLAS1<T> blas)
        {
            MatrixChecks.CheckNotNull(matrix);
            MatrixChecks.AssertMatrixDimensionsForMultiplication(this, matrix);

            IProvider<T> provider = blas.Provider;

            Dictionary<long, T> B_values = matrix._values;
            int A_columns = Columns, B_columns = matrix.Columns, ops = _values.Count * B_values.Count;

            Dictionary<long, T> product = new Dictionary<long, T>();
            if (ops < Rows * Columns * matrix.Columns)
            {
                foreach (KeyValuePair<long, T> a in _values)
                {
                    int row1 = (int)(a.Key / A_columns);
                    int column1 = (int)(a.Key % A_columns);

                    foreach (KeyValuePair<long, T> b in B_values)
                    {
                        int row2 = (int)(b.Key / B_columns);
                        if (column1 == row2)
                        {
                            int column2 = (int)(b.Key % B_columns);

                            long key = (long)row1 * B_columns + column2;
                            T prod = provider.Multiply(a.Value, b.Value);
                            if (product.ContainsKey(key))
                            {
                                product[key] = provider.Add(product[key], prod);
                            }
                            else
                            {
                                product[key] = prod;
                            }
                        }
                    }
                }
            }
            else
            {
                Dictionary<long, Dictionary<long, T>> A_values_by_row = GroupByRows(), B_values_by_column = matrix.GroupByColumns();
                foreach (KeyValuePair<long, Dictionary<long, T>> A_row in A_values_by_row)
                {
                    long index = A_row.Key * B_columns;
                    Dictionary<long, T> row = A_row.Value;
                    foreach (KeyValuePair<long, Dictionary<long, T>> B_col in B_values_by_column)
                    {
                        product[index + B_col.Key] = blas.SUMPROD(row, B_col.Value, 0, A_columns);
                    }
                }
            }

            return new DOKSparseMatrix<T>(Rows, B_columns, product);
        }
        internal new BigSparseVector<T> Multiply(T[] vector, IProvider<T> blas)
        {
            VectorChecks.CheckNotNull(vector);

            Dictionary<long, T> result = new Dictionary<long, T>();
            foreach (KeyValuePair<long, T> pair in _values)
            {
                long key = pair.Key, row = key / Columns, col = key % Columns;
                if (result.ContainsKey(row))
                {
                    result[row] = blas.Add(result[row], blas.Multiply(pair.Value, vector[col]));
                }
                else
                {
                    result[row] = blas.Multiply(pair.Value, vector[col]);
                }
            }
            return new BigSparseVector<T>(Rows, result);
        }

        public override int Count(Predicate<T> predicate)
        {
            if (predicate is null)
            {
                throw new ArgumentNullException(nameof(predicate));
            }

            int count;
            if (predicate(_zero))
            {
                count = Rows * Columns - _values.Count;
            }
            else
            {
                count = 0;
            }

            foreach (T e in _values.Values)
            {
                if (predicate(e))
                {
                    ++count;
                }
            }
            return count;
        }

        public override T[] LeadingDiagonal()
        {
            T[] diag = RectangularVector.Zeroes<T>(Math.Min(Rows, Columns));
            foreach (KeyValuePair<long, T> entry in _values)
            {
                long row = entry.Key / Columns, col = entry.Key % Columns;
                if (row == col)
                {
                    diag[(int)row] = entry.Value;
                }
            }
            return diag;
        }

        public override DenseVector<T> Row(int rowIndex)
        {
            if (rowIndex < 0 || rowIndex >= Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(rowIndex));
            }

            T[] row = RectangularVector.Zeroes<T>(Columns);
            long lc = Columns, lr = rowIndex;
            foreach (KeyValuePair<long, T> e in _values)
            {
                if (e.Key / lc == lr)
                {
                    row[(int)(e.Key % lc)] = e.Value;
                }
            }

            return new DenseVector<T>(row);
        }

        public override SparseVector<T> SparseRow(int rowIndex)
        {
            if (rowIndex < 0 || rowIndex >= Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(rowIndex));
            }

            List<int> indices = new List<int>();
            List<T> values = new List<T>();

            long lc = Columns, lr = rowIndex;
            foreach (KeyValuePair<long, T> e in _values)
            {
                if (e.Key / lc == lr)
                {
                    indices.Add((int)(e.Key % lc));
                    values.Add(e.Value);
                }
            }

            int nz = indices.Count;
            int[] indices_arr = new int[nz];
            T[] values_arr = new T[nz];

            for (int i = 0; i < nz; ++i)
            {
                indices_arr[i] = indices[i];
                values_arr[i] = values[i];
            }

            Array.Sort(indices_arr, values_arr);

            return new SparseVector<T>(Columns, indices_arr, values_arr, true);
        }

        public override DenseVector<T> Column(int columnIndex)
        {
            if (columnIndex < 0 || columnIndex >= Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(columnIndex));
            }

            T[] col = RectangularVector.Zeroes<T>(Rows);
            long lc = Columns;
            foreach (KeyValuePair<long, T> e in _values)
            {
                if (e.Key % lc == columnIndex)
                {
                    col[(int)(e.Key / lc)] = e.Value;
                }
            }

            return new DenseVector<T>(col);
        }

        public override SparseVector<T> SparseColumn(int columnIndex)
        {
            if (columnIndex < 0 || columnIndex >= Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(columnIndex));
            }

            List<int> indices = new List<int>();
            List<T> values = new List<T>();

            long lc = Columns, col = columnIndex;
            foreach (KeyValuePair<long, T> e in _values)
            {
                if (e.Key % lc == col)
                {
                    indices.Add((int)(e.Key / lc));
                    values.Add(e.Value);
                }
            }

            int nz = values.Count;
            int[] indices_arr = new int[nz];
            T[] values_arr = new T[nz];

            for (int i = 0; i < nz; ++i)
            {
                indices_arr[i] = indices[i];
                values_arr[i] = values[i];
            }

            Array.Sort(indices_arr, values_arr);

            return new SparseVector<T>(Rows, indices_arr, values_arr, true);
        }

        public override Matrix<T> Select(int[] rowIndices, int[] columnIndices)
        {
            if (rowIndices is null)
            {
                throw new ArgumentNullException(nameof(rowIndices));
            }
            if (columnIndices is null)
            {
                throw new ArgumentNullException(nameof(columnIndices));
            }
            
            ThrowHelper.AssertCollectionInRange(rowIndices, 0, Rows, nameof(rowIndices));
            ThrowHelper.AssertCollectionInRange(columnIndices, 0, Columns, nameof(columnIndices));

            int rlen = rowIndices.Length, clen = columnIndices.Length;
            Dictionary<long, T> values = new Dictionary<long, T>();

            long lc = Columns, new_lc = clen;
            for (int ri = 0; ri < rlen; ++ri)
            {
                int r = rowIndices[ri];
                long offset = r * lc, new_offset = ri * new_lc;

                for (int ci = 0; ci < clen; ++ci)
                {
                    int c = columnIndices[ci];
                    long key = offset + c;
                    if (_values.ContainsKey(key))
                    {
                        values[new_offset + ci] = _values[key];
                    }
                }
            }

            return new DOKSparseMatrix<T>(rlen, clen, values);
        }

        public override Vector<T> SelectAsVector(int rowIndex, int[] columnIndices)
        {
            if (rowIndex < 0 || rowIndex >= Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(rowIndex));
            }
            if (columnIndices is null)
            {
                throw new ArgumentNullException(nameof(columnIndices));
            }
            ThrowHelper.AssertCollectionInRange(columnIndices, 0, Columns, nameof(columnIndices));

            int clen = columnIndices.Length;
            T[] values = RectangularVector.Zeroes<T>(clen);

            long offset = (long)rowIndex * Columns;
            for (int ci = 0; ci < clen; ++ci)
            {
                long key = columnIndices[ci] + offset;
                if (_values.ContainsKey(key))
                {
                    values[ci] = _values[key];
                }
            }

            return new DenseVector<T>(values);
        }

        public override Vector<T> SelectAsVector(int[] rowIndices, int columnIndex)
        {
            if (rowIndices is null)
            {
                throw new ArgumentNullException(nameof(rowIndices));
            }
            ThrowHelper.AssertCollectionInRange(rowIndices, 0, Rows, nameof(rowIndices));

            if (columnIndex < 0 || columnIndex >= Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(columnIndex));
            }

            int rlen = rowIndices.Length;
            T[] values = RectangularVector.Zeroes<T>(rlen);
            long lc = Columns;
            for (int ri = 0; ri < rlen; ++ri)
            {
                long key = rowIndices[ri] * lc + columnIndex;
                if (_values.ContainsKey(key))
                {
                    values[ri] = _values[key];
                }
            }
            return new DenseVector<T>(values);
        }

        public override void Clear()
        {
            _values.Clear();
        }

        public override void Clear(Predicate<T> select)
        {
            if (select is null)
            {
                throw new ArgumentNullException(nameof(select));
            }

            Clear((row, col, value) => select(value));
        }

        public override void ClearColumn(int columnIndex)
        {
            if (columnIndex < 0 || columnIndex >= Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(columnIndex));
            }

            Clear((r, c, v) => c == columnIndex);
        }

        public override void ClearRow(int rowIndex)
        {
            if (rowIndex < 0 || rowIndex >= Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(rowIndex));
            }

            Clear((r, c, v) => r == rowIndex);
        }

        public override void ClearSubmatrix(int rowIndex, int columnIndex, int rows, int columns)
        {
            if (rowIndex < 0 || rowIndex >= Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(rowIndex));
            }
            if (columnIndex < 0 || columnIndex >= Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(columnIndex));
            }

            int bottom = rowIndex + rows, right = columnIndex + columns;
            if (rows < 0 || bottom >= Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(rows));
            }
            if (columns < 0 || right >= Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(columns));
            }

            Clear((r, c, v) => rowIndex <= r && r < bottom && columnIndex <= c && c < right);
        }

        public override void ClearLower()
        {
            Clear((r, c, v) => r > c);
        }

        public override void ClearUpper()
        {
            Clear((r, c, v) => r < c);
        }

        internal void Clear(Func<int, int, T, bool> remove)
        {
            if (remove is null)
            {
                throw new ArgumentNullException(nameof(remove));
            }

            // Create temporary list to hold keys, allows us to 
            // modify dictionary as it is traversed
            List<long> keys = new List<long>(_values.Keys);
            long lc = Columns;
            foreach (long key in keys)
            {
                int r = (int)(key / lc), c = (int)(key % lc);
                if (remove(r, c, _values[key]))
                {
                    _values.Remove(key);
                }
            }
        }
        public void Clear(Predicate<MatrixEntry<T>> remove)
        {
            if (remove is null)
            {
                throw new ArgumentNullException(nameof(remove));
            }
            Clear((int row, int col, T value) => remove(new MatrixEntry<T>(row, col, value)));
        }


        internal override bool SolveTriangular(T[] b, T[] x, IDenseBLAS1<T> blas, bool lower, bool transpose)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Add this matrix to another matrix, ans overwrite this matrix with the sum. This 
        /// method is identical to <txt>AddInPlace</txt>, and is re-implemented here for completeness.
        /// </summary>
        /// <param name="matrix">The matrix to increment to this matrix.</param>
        /// <param name="provider">The provider used to add matrix elements.</param>
        public void Increment(DOKSparseMatrix<T> matrix, IProvider<T> provider)
        {
            AddInPlace(matrix, provider);
        }

        /// <summary>
        /// Subtract a matrix away from this matrix, then overwrite this matrix with the difference.
        /// </summary>
        /// <param name="matrix">The matrix to decrement from this matrix.</param>
        /// <param name="provider">The provider used to subtract matrix elements.</param>
        public void Decrement(DOKSparseMatrix<T> matrix, IProvider<T> provider)
        {
            SubtractInPlace(matrix, provider);
        }

        public override Matrix<T> Add(Matrix<T> matrix, IProvider<T> provider)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            if (CompareRepresentationPriority(this, matrix) < 0)
            {
                return matrix.Add(this, provider); // return the sum as type(matrix)
            }
            return Add(matrix.ToDOKSparseMatrix(), provider);
        }

        /// <summary>
        /// Return the sum of this matrix and another matrix, as another DOK sparse matrix. 
        /// Neither of the original matrices are changed.
        /// </summary>
        /// <param name="matrix">The matrix to add to this matrix.</param>
        /// <param name="provider">The provider used to add matrix elements.</param>
        /// <returns>The matrix sum.</returns>
        public DOKSparseMatrix<T> Add(DOKSparseMatrix<T> matrix, IProvider<T> provider)
        {
            DOKSparseMatrix<T> result = Copy();
            result.Increment(matrix, provider);
            return result;
        }

        /// <summary>
        /// Add this matrix to another matrix, and overwrite this matrix with the sum. This 
        /// method is identical to <txt>Increment</txt>, and is re-implemented here for completeness.
        /// </summary>
        /// <param name="matrix">The matrix to add to this matrix.</param>
        /// <param name="provider">The provider used to add matrix elements.</param>
        public void AddInPlace(DOKSparseMatrix<T> matrix, IProvider<T> provider)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            MatrixChecks.AssertMatrixDimensionsEqual(this, matrix);

            Dictionary<long, T> _vs = matrix._values;
            foreach (KeyValuePair<long, T> pair in _vs)
            {
                long key = pair.Key;
                if (_values.ContainsKey(key))
                {
                    _values[key] = provider.Add(_values[key], pair.Value);
                }
                else
                {
                    _values[key] = pair.Value;
                }
            }
        }

        /// <summary>
        /// Subtract a sparse matrix from this matrix, using the specified provider.
        /// </summary>
        /// <param name="matrix">The matrix to subtract away from this matrix.</param>
        /// <param name="provider">The provider used to subtract matrix elements.</param>
        /// <returns>The matrix difference.</returns>
        public DOKSparseMatrix<T> Subtract(DOKSparseMatrix<T> matrix, IProvider<T> provider)
        {
            DOKSparseMatrix<T> result = Clone() as DOKSparseMatrix<T>;
            result.Decrement(matrix, provider);
            return result;
        }

        /// <summary>
        /// Subtract a matrix away from this matrix, and overwrite this matrix with the difference.
        /// </summary>
        /// <param name="matrix">The matrix to subtract away from this matrix.</param>
        /// <param name="provider">The provider used to subtract matrix elements.</param>
        public void SubtractInPlace(DOKSparseMatrix<T> matrix, IProvider<T> provider)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            MatrixChecks.AssertMatrixDimensionsEqual(this, matrix);

            Dictionary<long, T> _vs = matrix._values;
            foreach (KeyValuePair<long, T> pair in _vs)
            {
                long key = pair.Key;
                if (_values.ContainsKey(key))
                {
                    _values[key] = provider.Subtract(_values[key], pair.Value);
                }
                else
                {
                    _values[key] = provider.Negate(pair.Value);
                }
            }
        }

        /// <summary>
        /// Postmultiply this matrix by a sparse vector using the specified provider, returning the 
        /// result as a new sparse vector without changing the original matrix or vector.
        /// </summary>
        /// <param name="vector">The sparse vector to multiply by.</param>
        /// <param name="provider">The provider used to multiply elements together.</param>
        /// <returns>The matrix-vector product.</returns>
        public BigSparseVector<T> Multiply(BigSparseVector<T> vector, IProvider<T> provider)
        {
            VectorChecks.CheckNotNull(vector);

            Dictionary<long, T> vect_values = vector.Values;
            Dictionary<long, T> result = new Dictionary<long, T>();
            foreach (KeyValuePair<long, T> pair in _values)
            {
                long key = pair.Key, row = key / Columns, col = key % Columns;
                if (vect_values.ContainsKey(col))
                {
                    if (result.ContainsKey(row))
                    {
                        result[row] = provider.Add(result[row], provider.Multiply(pair.Value, vect_values[col]));
                    }
                    else
                    {
                        result[row] = provider.Multiply(pair.Value, vect_values[col]);
                    }
                }
            }
            return new BigSparseVector<T>(Rows, result);
        }

        public override SparseVector<T> Multiply(SparseVector<T> vector, IProvider<T> provider)
        {
            if (vector is null)
            {
                throw new ArgumentNullException(nameof(vector));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            if (vector.Dimension < Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(vector), "The dimension of the vector is less than the number of columns of the matrix.");
            }

            int[] vect_indices = vector.Indices;
            T[] vect_values = vector.Values;
            int len = vect_values.Length;

            Dictionary<int, T> vect_dict = new Dictionary<int, T>(len);
            for (int i = 0; i < len; ++i)
            {
                vect_dict[vect_indices[i]] = vect_values[i];
            }

            Dictionary<int, T> product = new Dictionary<int, T>();
            long lc = Columns;
            foreach (KeyValuePair<long, T> e in _values)
            {
                int col = (int)(e.Key % lc);
                if (vect_dict.ContainsKey(col))
                {
                    T v = provider.Multiply(e.Value, vect_dict[col]);
                    int row = (int)(e.Key / lc);
                    if (product.ContainsKey(row))
                    {
                        product[row] = provider.Add(product[row], v);
                    }
                    else
                    {
                        product[row] = v;
                    }
                }
            }

            int[] prod_indices = new int[product.Count];
            int k = 0;
            foreach (int key in product.Keys)
            {
                prod_indices[k++] = key;
            }

            Array.Sort(prod_indices);

            T[] values = new T[k];
            for (int i = 0; i < k; ++i)
            {
                values[i] = product[prod_indices[k]];
            }

            return new SparseVector<T>(Rows, prod_indices, values, true);
        }

        public override SparseVector<T> TransposeAndMultiply(SparseVector<T> vector, IProvider<T> provider)
        {
            if (vector is null)
            {
                throw new ArgumentNullException(nameof(vector));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            if (vector.Dimension < Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(vector), "The dimension of the vector is less than the number of rows of the matrix.");
            }

            int[] vect_indices = vector.Indices;
            T[] vect_values = vector.Values;
            int len = vect_values.Length;

            Dictionary<int, T> vect_dict = new Dictionary<int, T>(len);
            for (int i = 0; i < len; ++i)
            {
                vect_dict[vect_indices[i]] = vect_values[i];
            }

            Dictionary<int, T> product = new Dictionary<int, T>();
            long lc = Columns;
            foreach (KeyValuePair<long, T> e in _values)
            {
                int row = (int)(e.Key / lc);
                if (vect_dict.ContainsKey(row))
                {
                    T v = provider.Multiply(e.Value, vect_dict[row]);
                    int col = (int)(e.Key % lc);
                    if (product.ContainsKey(col))
                    {
                        product[col] = provider.Add(product[col], v);
                    }
                    else
                    {
                        product[col] = v;
                    }
                }
            }

            int[] prod_indices = new int[product.Count];
            int k = 0;
            foreach (int key in product.Keys)
            {
                prod_indices[k++] = key;
            }

            Array.Sort(prod_indices);

            T[] values = new T[k];
            for (int i = 0; i < k; ++i)
            {
                values[i] = product[prod_indices[k]];
            }

            return new SparseVector<T>(Columns, prod_indices, values, true);
        }

        internal override void Multiply(T[] vector, T[] result, IProvider<T> provider, bool increment)
        {
            if (vector is null)
            {
                throw new ArgumentNullException(nameof(vector));
            }
            if (result is null)
            {
                throw new ArgumentNullException(nameof(result));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }

            if (vector.Length != Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(vector), $"The number of columns of this matrix does not match the dimension of '{nameof(vector)}'.");
            }
            if (result.Length < Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(result), $"'{nameof(result)}' is not large enough to store the matrix-vector product.");
            }

            // Clear existing values if not increment
            if (!increment)
            {
                if (_zero.Equals(default))
                {
                    Array.Clear(result, 0, Rows);
                }
                else
                {
                    for (int i = 0; i < Rows; ++i)
                    {
                        result[i] = _zero;
                    }
                }
            }

            long lc = Columns;
            foreach (KeyValuePair<long, T> entry in _values)
            {
                int row = (int)(entry.Key / lc), col = (int)(entry.Key % lc);
                result[row] = provider.Add(result[row], provider.Multiply(entry.Value, vector[col]));
            }
        }

        internal override void TransposeAndMultiply(T[] vector, T[] product, IProvider<T> provider, bool increment)
        {
            if (vector is null)
            {
                throw new ArgumentNullException(nameof(vector));
            }
            if (product is null)
            {
                throw new ArgumentNullException(nameof(product));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            if (vector.Length != Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(vector), $"The number of rows of this matrix does not match the dimension of '{nameof(vector)}'.");
            }
            if (product.Length < Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(product), $"'{nameof(product)}' is not large enough to store the matrix-vector product.");
            }

            // Clear existing values if not increment
            if (!increment)
            {
                if (_zero.Equals(default))
                {
                    Array.Clear(product, 0, Columns);
                }
                else
                {
                    for (int i = 0; i < Columns; ++i)
                    {
                        product[i] = _zero;
                    }
                }
            }

            long lc = Columns;
            foreach (KeyValuePair<long, T> entry in _values)
            {
                int row = (int)(entry.Key / lc), col = (int)(entry.Key % lc);
                product[col] = provider.Add(product[col], provider.Multiply(entry.Value, vector[row]));
            }
        }

        /// <summary>
        /// Multiply this matrix by a scalar using the specified provider, returning the product as a new matrix 
        /// and leading this matrix unchanged.
        /// </summary>
        /// <param name="scalar">The scalar value to multiply by.</param>
        /// <param name="provider">The provider used to multiply matrix elements.</param>
        /// <returns>The matrix-scalar product.</returns>
        public DOKSparseMatrix<T> Multiply(T scalar, IProvider<T> provider)
        {
            Dictionary<long, T> result = new Dictionary<long, T>(_values.Count);
            foreach (KeyValuePair<long, T> pair in _values)
            {
                result[pair.Key] = provider.Multiply(scalar, pair.Value);
            }
            return new DOKSparseMatrix<T>(Rows, Columns, result);
        }

        /// <summary>
        /// Multiply this matrix by a scalar using the specified provider, then overwrite this matrix with the result.
        /// </summary>
        /// <param name="scalar">The scalar value to multiply by.</param>
        /// <param name="provider">The provider used to multiply matrix elements.</param>
        public override void MultiplyInPlace(T scalar, IProvider<T> provider)
        {
            if (provider is null) throw new ArgumentNullException(nameof(provider));
            foreach (long key in _values.Keys)
            {
                _values[key] = provider.Multiply(_values[key], scalar);
            }
        }

        /// <summary>
        /// Divide this matrix by a scalar, using the specified provider for elementwise division.
        /// A new matrix is returned without modifying this matrix.
        /// </summary>
        /// <param name="scalar">The scalar to divide by.</param>
        /// <param name="provider">The provider used for elementwise division.</param>
        /// <returns>The matrix-scalar division result.</returns>
        public DOKSparseMatrix<T> Divide(T scalar, IProvider<T> provider)
        {
            DOKSparseMatrix<T> copy = Copy();
            copy.DivideInPlace(scalar, provider);
            return copy;
        }

        /// <summary>
        /// Returns the negation (additive inverse) of this matrix. A new matrix is created, 
        /// and this matrix remains unchanged.
        /// </summary>
        /// <param name="provider">The provider used to negate matrix elements.</param>
        /// <returns>The matrix negation.</returns>
        public DOKSparseMatrix<T> Negate(IProvider<T> provider)
        {
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }

            Dictionary<long, T> negate = new Dictionary<long, T>(_values.Count);
            foreach (KeyValuePair<long, T> pair in _values) 
            {
                negate[pair.Key] = provider.Negate(pair.Value);
            }
            return new DOKSparseMatrix<T>(Rows, Columns, negate);
        }

        /// <summary>
        /// Overwrites this matrix with its negation (additive inverse). 
        /// </summary>
        /// <param name="provider">The provider used to negate the matrix elements.</param>
        public void NegateInPlace(IProvider<T> provider)
        {
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            foreach (long key in _values.Keys)
            {
                _values[key] = provider.Negate(_values[key]);
            }
        }

        /// <summary>
        /// Returns the Kronecker product between this matrix and another, using a provider.
        /// </summary>
        /// <param name="matrix">The other matrix to multiply with.</param>
        /// <param name="provider">The provider to use for multiplying elements of the matrices together.</param>
        /// <returns>The Kronecker product.</returns>
        public DOKSparseMatrix<T> KroneckerProduct(DOKSparseMatrix<T> matrix, IProvider<T> provider)
        {
            MatrixChecks.CheckNotNull(matrix);

            int A_rows = Rows, A_cols = Columns,
                B_rows = matrix.Rows, B_cols = matrix.Columns,
                rows = A_rows * B_rows, cols = A_cols * B_cols;

            Dictionary<long, T> result = new Dictionary<long, T>(), B_values = matrix.Values;
            foreach (KeyValuePair<long, T> p1 in _values)
            {
                long A_row = p1.Key / A_cols, A_col = p1.Key % A_cols;
                T A_ij = p1.Value;
                foreach (KeyValuePair<long, T> p2 in B_values)
                {
                    long B_row = p2.Key / B_cols, B_col = p2.Key % B_cols;
                    long row = A_row * B_rows + B_row, col = A_col * B_cols + B_col;
                    result[row * cols + col] = provider.Multiply(A_ij, p2.Value);
                }
            }
            return new DOKSparseMatrix<T>(rows, cols, result);
        }

        /// <summary>
        /// Returns the trace of this matrix.
        /// </summary>
        /// <param name="provider">The provider for adding elements from this matrix.</param>
        /// <returns>The matrix trace.</returns>
        public override T Trace(IProvider<T> provider)
        {
            T trace = provider.Zero;

            int minorDimension = Math.Min(Rows, Columns);
            if (_values.Count > minorDimension)
            {
                for (int i = 0; i < minorDimension; ++i)
                {
                    long key = get_key(i, i);
                    if (_values.ContainsKey(key))
                    {
                        trace = provider.Add(trace, _values[key]);
                    }
                }
            }
            else
            {
                foreach (KeyValuePair<long, T> pair in _values)
                {
                    long rows = pair.Key / Columns, cols = pair.Key % Columns;
                    if (rows == cols)
                    {
                        trace = provider.Add(trace, pair.Value);
                    }
                }
            }
            return trace;
        }

        /// <summary>
        /// Returns the row sums of this matrix as a sparse vector. The $i$-th element of the returned 
        /// vector equals the sum of all matrix elements in row $i$.
        /// </summary>
        /// <param name="provider">The provider to use to add matrix elements.</param>
        /// <returns>The row sums of this matrix.</returns>
        public BigSparseVector<T> SparseRowSums(IProvider<T> provider)
        {
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }

            Dictionary<long, T> sums = new Dictionary<long, T>();
            foreach (KeyValuePair<long, T> entry in _values)
            {
                long row = entry.Key / Columns;
                if (sums.ContainsKey(row))
                {
                    sums[row] = provider.Add(sums[row], entry.Value);
                }
                else
                {
                    sums[row] = entry.Value;
                }
            }
            return new BigSparseVector<T>(Rows, sums);
        }

        /// <summary>
        /// Returns the column sums of this matrix as a sparse matrix. The $i$-th element of the returned 
        /// vector equals the sum of all matrix elements in column $i$.
        /// </summary>
        /// <param name="provider">The provider to use to add matrix elements.</param>
        /// <returns>The column sums of this matrix.</returns>
        public BigSparseVector<T> SparseColumnSums(IProvider<T> provider)
        {
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }

            Dictionary<long, T> sums = new Dictionary<long, T>();
            foreach (KeyValuePair<long, T> entry in _values)
            {
                long col = entry.Key % Columns;
                if (sums.ContainsKey(col))
                {
                    sums[col] = provider.Add(sums[col], entry.Value);
                }
                else
                {
                    sums[col] = entry.Value;
                }
            }
            return new BigSparseVector<T>(Columns, sums);
        }

        public override DenseVector<T> RowSums(IProvider<T> provider)
        {
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }

            T[] sums = RectangularVector.Zeroes<T>(Rows);

            long lc = Columns;
            foreach (KeyValuePair<long, T> e in _values)
            {
                int row = (int)(e.Key / lc);
                sums[row] = provider.Add(sums[row], e.Value);
            }

            return new DenseVector<T>(sums);
        }

        public override DenseVector<T> ColumnSums(IProvider<T> provider)
        {
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }

            T[] sums = RectangularVector.Zeroes<T>(Columns);

            long lc = Columns;
            foreach (KeyValuePair<long, T> e in _values)
            {
                int col = (int)(e.Key % lc);
                sums[col] = provider.Add(sums[col], e.Value);
            }

            return new DenseVector<T>(sums);
        }

        public override void SwapRows(int rowIndex1, int rowIndex2)
        {
            if (rowIndex1 < 0 || rowIndex1 >= Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(rowIndex1));
            }

            if (rowIndex2 < 0 || rowIndex2 >= Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(rowIndex2));
            }

            Dictionary<long, T> affectedIndices = new Dictionary<long, T>();
            long lc = Columns;
            foreach (KeyValuePair<long, T> e in _values)
            {
                int row = (int)(e.Key / lc);
                if (row == rowIndex1 || row == rowIndex2)
                {
                    affectedIndices.Add(e.Key, e.Value);
                }
            }

            // Remove from _values
            foreach (long index in affectedIndices.Keys)
            {
                _values.Remove(index);
            }

            // Insert the new indices
            foreach (long index in affectedIndices.Keys)
            {
                int row = (int)(index / lc), col = (int)(index % lc);
                if (row == rowIndex1)
                {
                    _values[rowIndex2 * lc + col] = affectedIndices[index];
                }
                else
                {
                    _values[rowIndex1 * lc + col] = affectedIndices[index];
                }
            }
        }

        public override void SwapColumns(int columnIndex1, int columnIndex2)
        {
            if (columnIndex1 < 0 || columnIndex1 >= Columns)
            {
                throw new ArgumentNullException(nameof(columnIndex1));
            }

            if (columnIndex2 < 0 || columnIndex2 >= Columns)
            {
                throw new ArgumentNullException(nameof(columnIndex2));
            }

            Dictionary<long, T> changes = new Dictionary<long, T>();
            long lc = Columns;
            foreach (KeyValuePair<long, T> e in _values)
            {
                int col = (int)(e.Key % lc);
                if (col == columnIndex1 || col == columnIndex2)
                {
                    changes[e.Key] = e.Value;
                }
            }

            foreach (long key in changes.Keys)
            {
                _values.Remove(key);
            }

            foreach (long key in changes.Keys)
            {
                int row = (int)(key / lc), col = (int)(key % lc);
                if (col == columnIndex1)
                {
                    _values[row * lc + columnIndex2] = changes[key];
                }
                else
                {
                    _values[row * lc + columnIndex1] = changes[key];
                }
            }
        }

        /// <summary>
        /// Returns a deep copy of this sparse matrix, as an object.
        /// </summary>
        /// <returns>A copy of this matrix.</returns>
        public override object Clone()
        {
            return Copy();
        }

        /// <summary>
        /// Returns a deep copy of this sparse matrix, as another <txt>SparseMatrix<T></txt>
        /// </summary>
        /// <returns>A copy of this matrix.</returns>
        public DOKSparseMatrix<T> Copy()
        {
            Dictionary<long, T> values = new Dictionary<long, T>();
            foreach (var kvp in _values)
            {
                values[kvp.Key] = kvp.Value;
            }
            return new DOKSparseMatrix<T>(Rows, Columns, values);
        }

        /// <summary>
        /// Returns the transpose of this sparse matrix, as another sparse matrix.
        /// </summary>
        /// <returns>The transpose of this matrix.</returns>
        public DOKSparseMatrix<T> Transpose()
        {
            DOKSparseMatrix<T> transpose = new DOKSparseMatrix<T>(Columns, Rows);
            foreach (KeyValuePair<long, T> pair in _values)
            {
                int row = (int)(pair.Key / Columns), col = (int)(pair.Key % Columns);
                transpose[col, row] = pair.Value;
            }
            return transpose;
        }

        /// <summary>
        /// Returns the direct sum of this sparse matrix with another, $A \otimes B$.
        /// </summary>
        /// <param name="matrix">A sparse matrix.</param>
        /// <returns>The direct sum of the two matrices.</returns>
        public DOKSparseMatrix<T> DirectSum(DOKSparseMatrix<T> matrix) 
        {
            long columns = Columns + matrix.Columns;
            Dictionary<long, T> result = new Dictionary<long, T>(_values.Count + matrix.Values.Count);

            foreach (KeyValuePair<long, T> pair in _values)
            {
                long row = pair.Key / Columns, col = pair.Key % Columns, key = row * columns + col;
                result[key] = pair.Value;
            }
            foreach (KeyValuePair<long, T> pair in matrix.Values)
            {
                long row = Rows + pair.Key / Columns, col = Columns + pair.Key % Columns, key = row * columns + col;
                result[key] = pair.Value;
            }
            return new DOKSparseMatrix<T>(Rows + matrix.Rows, Columns + matrix.Columns, result);
        }

        /// <summary>
        /// Returns a new sparse matrix where a function has been applied elementwise to all elements of a sparse matrix.
        /// </summary>
        /// <param name="predicate">A <txt>Func</txt> representing the elementwise function.</param>
        /// <returns>A sparse matrix where the function has been applied elementwise.</returns>
        public DOKSparseMatrix<F> Elementwise<F>(Func<T, F> predicate) where F : new()
        {
            Dictionary<long, F> result = new Dictionary<long, F>(_values.Count);
            foreach (KeyValuePair<long, T> pair in _values)
            {
                result[pair.Key] = predicate(pair.Value);
            }
            return new DOKSparseMatrix<F>(Rows, Columns, result);
        }

        /// <summary>
        /// For a $m \times n$ matrix, and a row count $r > 0$, returns a matrix of size 
        /// $r \times \frac{mn}{r}$.
        /// </summary>
        /// <param name="rows">The number of rows of the reshaped matrix, $r$.</param>
        /// <param name="rowMajor">Specifies whether the reshaped matrix should be filled 
        /// in row major order.</param>
        /// <returns>The reshaped matrix.</returns>
        public DOKSparseMatrix<T> Reshape(int rows, bool rowMajor = true)
        {
            if (rows <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(rows));
            }

            if ((Rows * Columns) % rows != 0)
            {
                throw new InvalidOperationException("The number of elements in this matrix is not divisible by the number of rows");
            }

            int columns = (Rows * Columns) / rows;
            if (rowMajor)
            {
                // No need for any re-indexing
                Dictionary<long, T> reshaped = new Dictionary<long, T>(_values);
                return new DOKSparseMatrix<T>(rows, columns, reshaped);
            }
            else
            {
                Dictionary<long, T> reshaped = new Dictionary<long, T>(_values.Count);
                foreach (KeyValuePair<long, T> e in _values)
                {
                    long row = e.Key / Columns, column = e.Key % Columns;
                    reshaped[row + column * rows] = e.Value;
                }
                return new DOKSparseMatrix<T>(rows, columns, reshaped);
            }
        }


        #region Boolean methods

        public bool Equals(DOKSparseMatrix<T> matrix, Func<T, T, bool> equals)
        {
            if (matrix is null)
            {
                return false;
            }
            if (equals is null)
            {
                throw new ArgumentNullException(nameof(equals));
            }
            if (Rows != matrix.Rows || Columns != matrix.Columns)
            {
                return false;
            }
            
            // a double-loop...
            foreach (KeyValuePair<long, T> entry in _values)
            {
                if (equals(entry.Value, _zero))
                {
                    if (matrix._values.ContainsKey(entry.Key) && !equals(matrix._values[entry.Key], _zero))
                    {
                        return false;
                    }
                }
                else
                {
                    if (!matrix._values.ContainsKey(entry.Key) || !equals(matrix._values[entry.Key], entry.Value))
                    {
                        return false;
                    }
                }
            }

            foreach (KeyValuePair<long, T> entry in matrix._values)
            {
                if (equals(entry.Value, _zero))
                {
                    if (_values.ContainsKey(entry.Key) && !equals(_values[entry.Key], _zero))
                    {
                        return false;
                    }
                }
                else
                {
                    if (!_values.ContainsKey(entry.Key) || !equals(_values[entry.Key], entry.Value))
                    {
                        return false;
                    }
                }
            }
            return true;
        }
        public bool Equals(DOKSparseMatrix<T> matrix, IEqualityComparer<T> comparer)
        {
            if (comparer is null)
            {
                throw new ArgumentNullException(nameof(comparer));
            }
            return Equals(matrix, comparer.Equals);
        }
        public bool Equals(DOKSparseMatrix<T> matrix)
        {
            return Equals(matrix, EqualityComparer<T>.Default);
        }
        public override bool Equals(object obj)
        {
            if (obj is null)
            {
                return false;
            }
            if (obj is DOKSparseMatrix<T>)
            {
                return Equals(obj as DOKSparseMatrix<T>);
            }
            return Equals(obj as Matrix<T>);
        }

        public override bool IsLower(Predicate<T> isZero)
        {
            if (isZero is null)
            {
                throw new ArgumentNullException(nameof(isZero));
            }

            long lc = Columns;
            foreach (KeyValuePair<long, T> e in _values)
            {
                long row = e.Key / lc, col = e.Key % lc;
                if (row < col && !isZero(e.Value))
                {
                    return false;
                }
            }
            return true;
        }

        public override bool IsUpper(Predicate<T> isZero)
        {
            if (isZero is null)
            {
                throw new ArgumentNullException(nameof(isZero));
            }

            long lc = Columns;
            foreach (KeyValuePair<long, T> e in _values)
            {
                long row = e.Key / lc, col = e.Key % lc;
                if (row > col && !isZero(e.Value))
                {
                    return false;
                }
            }
            return true;
        }

        public override bool IsSymmetric(Func<T, T, bool> equals)
        {
            if (!IsSquare) return false;
            if (equals is null)
            {
                throw new ArgumentNullException(nameof(equals));
            }

            long lc = Columns;
            foreach (KeyValuePair<long, T> e in _values)
            {
                long row = e.Key / lc, col = e.Key % lc;
                if (!equals(e.Value, _zero))
                {
                    long transpose = col * lc + row;
                    if (!_values.ContainsKey(transpose) || !equals(_values[transpose], e.Value))
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        public override bool IsSymmetric(IEqualityComparer<T> comparer)
        {
            return IsSymmetric(comparer.Equals);
        }

        public override bool IsDiagonal(Predicate<T> isZero)
        {
            if (isZero is null)
            {
                throw new ArgumentNullException(nameof(isZero));
            }

            long lc = Columns;
            foreach (KeyValuePair<long, T> entry in _values)
            {
                int row = (int)(entry.Key / lc), col = (int)(entry.Key % lc);
                if (row != col && !isZero(entry.Value))
                {
                    return false;
                }
            }
            return true;
        }

        public override bool IsBanded(int lowerBandwidth, int upperBandwidth, Predicate<T> isZero)
        {
            if (isZero is null)
            {
                throw new ArgumentNullException(nameof(isZero));
            }

            long lc = Columns;
            foreach (KeyValuePair<long, T> entry in _values)
            {
                int row = (int)(entry.Key / lc), col = (int)(entry.Key % lc);
                if (row > col)
                {
                    if (row - col > lowerBandwidth && !isZero(entry.Value))
                    {
                        return false;
                    }
                }
                else
                {
                    if (col - row > upperBandwidth && !isZero(entry.Value))
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        public override bool IsZero(Predicate<T> isZero)
        {
            if (isZero is null)
            {
                throw new ArgumentNullException(nameof(isZero));
            }

            foreach (T value in _values.Values)
            {
                if (!isZero(value))
                {
                    return false;
                }
            }
            return true;
        }

        public override bool IsIdentity(Predicate<T> isZero, Predicate<T> isOne)
        {
            if (Rows != Columns)
            {
                return false;
            }
            if (isZero is null)
            {
                throw new ArgumentNullException(nameof(isZero));
            }
            if (isOne is null)
            {
                throw new ArgumentNullException(nameof(isOne));
            }

            long lc = Columns;
            foreach (KeyValuePair<long, T> entry in _values)
            {
                int row = (int)(entry.Key / lc), col = (int)(entry.Key % lc);
                if (row != col)
                {
                    if (!isZero(entry.Value))
                    {
                        return false;
                    }
                }
                else
                {
                    if (!isOne(entry.Value))
                    {
                        return false;
                    }
                }
            }

            // Check that all diagonal terms are present
            for (int i = 0; i < Rows; ++i)
            {
                if (!_values.ContainsKey(i * lc + i))
                {
                    return false;
                }
            }

            return true;
        }

        #endregion


        #region Convertors

        public override BandMatrix<T> ToBandMatrix()
        {
            return new BandMatrix<T>(this);
        }

        public override BlockDiagonalMatrix<T> ToBlockDiagonalMatrix()
        {
            return new BlockDiagonalMatrix<T>(this);
        }

        public override COOSparseMatrix<T> ToCOOSparseMatrix()
        {
            return new COOSparseMatrix<T>(this);
        }

        public override CSCSparseMatrix<T> ToCSCSparseMatrix()
        {
            return new CSCSparseMatrix<T>(this);
        }

        public override CSRSparseMatrix<T> ToCSRSparseMatrix()
        {
            return new CSRSparseMatrix<T>(this);
        }

        public override DenseMatrix<T> ToDenseMatrix()
        {
            return new DenseMatrix<T>(this);
        }

        public override DiagonalMatrix<T> ToDiagonalMatrix()
        {
            return new DiagonalMatrix<T>(this);
        }

        public override DOKSparseMatrix<T> ToDOKSparseMatrix()
        {
            return this;
        }

        public override MCSRSparseMatrix<T> ToMCSRSparseMatrix()
        {
            return new MCSRSparseMatrix<T>(this);
        }

        public override SKYSparseMatrix<T> ToSKYSparseMatrix()
        {
            return new SKYSparseMatrix<T>(this);
        }

        #endregion


        #region Operations

        /// <summary>
        /// Returns the sum of two DOK sparse matrices.
        /// </summary>
        /// <param name="A">The left sparse matrix.</param>
        /// <param name="B">The right sparse matrix.</param>
        /// <returns>The matrix sum as a DOK sparse matrix.</returns>
        public static DOKSparseMatrix<T> operator +(DOKSparseMatrix<T> A, DOKSparseMatrix<T> B)
        {
            if (A is null)
            {
                throw new ArgumentNullException(nameof(A));
            }
            return A.Add(B, ProviderFactory.GetDefaultProvider<T>());
        }

        /// <summary>
        /// Returns the left matrix subtract the right matrix.
        /// </summary>
        /// <param name="A">The left matrix.</param>
        /// <param name="B">The right matrix.</param>
        /// <returns>The matrix difference.</returns>
        public static DOKSparseMatrix<T> operator -(DOKSparseMatrix<T> A, DOKSparseMatrix<T> B)
        {
            if (A is null)
            {
                throw new ArgumentNullException(nameof(A));
            }
            return A.Add(B, ProviderFactory.GetDefaultProvider<T>());
        }

        /// <summary>
        /// Returns the negation of a sparse matrix.
        /// </summary>
        /// <param name="A">The sparse matrix.</param>
        /// <returns>The matrix negation.</returns>
        public static DOKSparseMatrix<T> operator -(DOKSparseMatrix<T> A)
        {
            if (A is null)
            {
                throw new ArgumentNullException(nameof(A));
            }
            return A.Negate(ProviderFactory.GetDefaultProvider<T>());
        }

        /// <summary>
        /// Returns the product between a sparse matrix and a sparse vector.
        /// </summary>
        /// <param name="A">The sparse matrix.</param>
        /// <param name="x">The sparse vector.</param>
        /// <returns>The sparse matrix-vector product.</returns>
        public static BigSparseVector<T> operator *(DOKSparseMatrix<T> A, BigSparseVector<T> x)
        {
            if (A is null)
            {
                throw new ArgumentNullException(nameof(A));
            }
            return A.Multiply(x, ProviderFactory.GetDefaultProvider<T>());
        }

        /// <summary>
        /// Returns the product between a sparse matrix and a scalar.
        /// </summary>
        /// <param name="A">The sparse matrix.</param>
        /// <param name="scalar">The scalar value.</param>
        /// <returns>The sparse matrix-scalar product.</returns>
        public static DOKSparseMatrix<T> operator *(DOKSparseMatrix<T> A, T scalar)
        {
            if (A is null)
            {
                throw new ArgumentNullException(nameof(A));
            }
            return A.Multiply(scalar, ProviderFactory.GetDefaultProvider<T>());
        }

        public static DOKSparseMatrix<T> operator *(DOKSparseMatrix<T> A, DOKSparseMatrix<T> B)
        {
            if (A is null)
            {
                throw new ArgumentNullException(nameof(A));
            }
            return A.Multiply(B, ProviderFactory.GetDefaultSparseBLAS1<T>());
        }

        /// <summary>
        /// Returns the product between a scalar and a sparse matrix.
        /// </summary>
        /// <param name="scalar">The scalar value.</param>
        /// <param name="A">The sparse matrix.</param>
        /// <returns>The sparse matrix-scalar product.</returns>
        public static DOKSparseMatrix<T> operator *(T scalar, DOKSparseMatrix<T> A) => A * scalar;

        #endregion


        #region Casts
        /// <summary>
        /// Casts a band matrix into the equivalent sparse matrix.
        /// </summary>
        /// <param name="matrix">A band matrix.</param>
        public static explicit operator DOKSparseMatrix<T>(BandMatrix<T> matrix) => new DOKSparseMatrix<T>(matrix);
        public static explicit operator DOKSparseMatrix<T>(BlockDiagonalMatrix<T> matrix) => new DOKSparseMatrix<T>(matrix);
        public static explicit operator DOKSparseMatrix<T>(COOSparseMatrix<T> matrix) => new DOKSparseMatrix<T>(matrix);
        public static explicit operator DOKSparseMatrix<T>(CSCSparseMatrix<T> matrix) => new DOKSparseMatrix<T>(matrix);
        public static explicit operator DOKSparseMatrix<T>(CSRSparseMatrix<T> matrix) => new DOKSparseMatrix<T>(matrix);
        /// <summary>
        /// Casts a dense matrix into the equivalent sparse matrix.
        /// </summary>
        /// <param name="matrix">A dense matrix.</param>
        public static explicit operator DOKSparseMatrix<T>(DenseMatrix<T> matrix) => new DOKSparseMatrix<T>(matrix);
        /// <summary>
        /// Casts a diagonal matrix into the equivalent sparse matrix.
        /// </summary>
        /// <param name="matrix">A diagonal matrix.</param>
        public static explicit operator DOKSparseMatrix<T>(DiagonalMatrix<T> matrix) => new DOKSparseMatrix<T>(matrix);
        public static explicit operator DOKSparseMatrix<T>(MCSRSparseMatrix<T> matrix) => new DOKSparseMatrix<T>(matrix);
        public static explicit operator DOKSparseMatrix<T>(SKYSparseMatrix<T> matrix) => new DOKSparseMatrix<T>(matrix);
        #endregion
    }

}
