﻿using LinearNet.Helpers;
using System;
using System.Collections.Generic;

namespace LinearNet.Structs
{
    /// <summary>
    /// A static class containing useful functions for creating special types of sparse matrices.
    /// </summary>
    /// <cat>linear-algebra</cat>
    public static class DOKSparseMatrix
    {
        private static readonly Random _random = new Random();

        /// <summary>
        /// <para>Create a random sparse matrix of dimensions <txt>rows</txt> $\times$ <txt>columns</txt> with up to <txt>maxNonZeroEntries</txt> non-zero entries.</para>
        /// <!--inputs-->
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="rows">The number of rows in the created matrix.</param>
        /// <param name="columns">The number of columns in the created matrix.</param>
        /// <param name="maxNonZeroEntries">The maximum number of non-zero entries in the matrix.</param>
        /// <param name="random">
        /// <b>Optional</b>, the random <txt>T</txt> generator.<br/>
        /// If unspecified, a default generator will be used. See <a href="#Random"><txt><b>random</b></txt></a> for details on default random generators.
        /// </param>
        /// <returns>A random sparse matrix.</returns>
        public static DOKSparseMatrix<T> Random<T>(int rows, int columns, int maxNonZeroEntries, Func<T> random = null) where T : new()
        {
            if (random == null)
            {
                random = DefaultGenerators.GetRandomGenerator<T>();
            }

            Dictionary<long, T> values = new Dictionary<long, T>();
            for (int i = 0; i < maxNonZeroEntries; ++i)
            {
                values[GetKey(columns, _random.Next(rows), _random.Next(columns))] = random();
            }
            return new DOKSparseMatrix<T>(rows, columns, values);
        }

        public static DOKSparseMatrix<T> Random<T>(int rows, int columns, double density, Func<T> random = null) where T : new()
        {
            if (random is null)
            {
                random = DefaultGenerators.GetRandomGenerator<T>();
            }

            Dictionary<long, T> values = new Dictionary<long, T>();
            Random rand = new Random();
            for (int r = 0; r < rows; ++r)
            {
                for (int c = 0; c < columns; ++c)
                {
                    if (rand.NextDouble() < density)
                    {
                        values[GetKey(columns, r, c)] = random();
                    }
                }
            }
            return new DOKSparseMatrix<T>(rows, columns, values);
        }

        /// <summary>
        /// Create a random sparse triangular matrix of the specified dimensions.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="rows"></param>
        /// <param name="columns"></param>
        /// <param name="maxNonZeroEntries"></param>
        /// <param name="upper"></param>
        /// <param name="Random"></param>
        /// <returns></returns>
        public static DOKSparseMatrix<T> RandomTriangular<T>(int rows, int columns, int maxNonZeroEntries, bool upper = true, Func<T> Random = null) where T : new()
        {
            if (Random == null)
            {
                Random = DefaultGenerators.GetRandomGenerator<T>();
            }

            Dictionary<long, T> values = new Dictionary<long, T>();
            for (int i = 0; i < maxNonZeroEntries; ++i)
            {
                int row = _random.Next(rows), column;
                if (upper)
                {
                    if (row >= columns) continue;
                    column = RandomInt(row, columns);
                }
                else
                {
                    column = _random.Next(Math.Min(rows, row + 1));
                }
                values[GetKey(columns, row, column)] = Random();
            }
            return new DOKSparseMatrix<T>(rows, columns, values);
        }
        /// <summary>
        /// Create a random sparse diagonal matrix of the specified dimensions.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="rows"></param>
        /// <param name="columns"></param>
        /// <param name="maxNonZeroEntries"></param>
        /// <param name="Random"></param>
        /// <returns></returns>
        public static DOKSparseMatrix<T> RandomDiagonal<T>(int rows, int columns, int maxNonZeroEntries, Func<T> Random = null) where T : new()
        {
            if (Random == null)
            {
                Random = DefaultGenerators.GetRandomGenerator<T>();
            }

            Dictionary<long, T> values = new Dictionary<long, T>();
            int end = Math.Min(rows, columns);
            for (int i = 0; i < maxNonZeroEntries; ++i)
            {
                int row = _random.Next(end);
                values[GetKey(columns, row, row)] = Random();
            }
            return new DOKSparseMatrix<T>(rows, columns, values);
        }
        /// <summary>
        /// Create a random sparse bidiagonal matrix of the specified dimensions.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="rows"></param>
        /// <param name="columns"></param>
        /// <param name="maxNonZeroEntries"></param>
        /// <param name="upper"></param>
        /// <param name="Random"></param>
        /// <returns></returns>
        public static DOKSparseMatrix<T> RandomBidiagonal<T>(int rows, int columns, int maxNonZeroEntries, bool upper = true, Func<T> Random = null) where T : new()
        {
            if (Random == null)
            {
                Random = DefaultGenerators.GetRandomGenerator<T>();
            }

            int len = Math.Min(rows, columns);

            // Calculate the number of unique locations in which to sample
            int locations;
            if (rows < columns && !upper) locations = 2 * len - 1;
            else if (rows > columns && upper) locations = 2 * len - 1;
            else locations = 2 * len;

            Dictionary<long, T> values = new Dictionary<long, T>();
            for (int i = 0; i < maxNonZeroEntries; ++i)
            {
                int location = _random.Next(locations);
                if (upper)
                {
                    int row = location / 2;
                    int col = row + location % 2;
                    values[GetKey(columns, row, col)] = Random();
                }
                else
                {
                    int col = location / 2;
                    int row = col + location % 2;
                    values[GetKey(columns, row, col)] = Random();
                }
            }
            return new DOKSparseMatrix<T>(rows, columns, values);
        }
        /// <summary>
        /// Create a random sparse tridiagonal matrix of the specified dimensions. 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="rows"></param>
        /// <param name="columns"></param>
        /// <param name="maxNonZeroEntries"></param>
        /// <param name="Random"></param>
        /// <returns></returns>
        public static DOKSparseMatrix<T> RandomTridiagonal<T>(int rows, int columns, int maxNonZeroEntries, Func<T> Random = null) where T : new()
        {
            if (Random == null)
            {
                Random = DefaultGenerators.GetRandomGenerator<T>();
            }

            int len = Math.Min(rows, columns);
            int locations;
            if (rows != columns)
            {
                locations = 3 * len - 1;
            }
            else
            {
                locations = 3 * len - 2;
            }

            Dictionary<long, T> values = new Dictionary<long, T>();
            for (int i = 0; i < maxNonZeroEntries; ++i)
            {
                int location = _random.Next(locations) + 1;

                int col = location / 3;
                int row = col - 1 + location % 3;

                // The final location might be pivoted
                if (location == locations && rows < columns)
                {
                    row--;
                    col++;
                }

                values[GetKey(columns, row, col)] = Random();
            }

            return new DOKSparseMatrix<T>(rows, columns, values);
        }
        /// <summary>
        /// Create a random Hessenberg matrix of the specified dimensions.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dimension"></param>
        /// <param name="maxNonZeroEntries"></param>
        /// <param name="upper"></param>
        /// <param name="Random"></param>
        /// <returns></returns>
        public static DOKSparseMatrix<T> RandomHessenberg<T>(int dimension, int maxNonZeroEntries, bool upper = true, Func<T> Random = null) where T : new()
        {
            if (Random == null)
            {
                Random = DefaultGenerators.GetRandomGenerator<T>();
            }

            Dictionary<long, T> values = new Dictionary<long, T>();
            for (int i = 0; i < maxNonZeroEntries; ++i)
            {
                int row = _random.Next(dimension), col = _random.Next(dimension);
                if (upper && row <= col + 1)
                {
                    values[GetKey(dimension, row, col)] = Random();
                }
                else if (!upper && col <= row + 1)
                {
                    values[GetKey(dimension, row, col)] = Random();
                }
            }
            return new DOKSparseMatrix<T>(dimension, dimension, values);
        }
        /// <summary>
        /// Create a square diagonal matrix given a vector of its diagonal elements.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="diagonal"></param>
        /// <returns></returns>
        public static DOKSparseMatrix<T> Diag<T>(T[] diagonal) where T : new()
        {
            int dim = diagonal.Length, i;
            DOKSparseMatrix<T> matrix = new DOKSparseMatrix<T>(dim, dim);
            for (i = 0; i < dim; ++i)
            {
                matrix[i, i] = diagonal[i];
            }
            return matrix;
        }
        /// <summary>
        /// Create an identity matrix given its dimensions.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dimension"></param>
        /// <returns></returns>
        public static DOKSparseMatrix<T> Identity<T>(int dimension) where T : new()
        {
            T one = DefaultGenerators.GetOne<T>();

            DOKSparseMatrix<T> matrix = new DOKSparseMatrix<T>(dimension, dimension);
            for (int i = 0; i < dimension; ++i)
            {
                matrix[i, i] = one;
            }
            return matrix;
        }

        private static long GetKey(int columns, int i, int j)
        {
            return (long)columns * i + j;
        }
        private static int RandomInt(int min, int max)
        {
            return _random.Next(max - min) + min;
        }
    }
}
