﻿using LinearNet.Decomposition;
using LinearNet.Global;
using LinearNet.Matrices;
using LinearNet.Matrices.Cholesky;
using LinearNet.Matrices.Inversion;
using LinearNet.Matrices.LU;
using LinearNet.Matrices.Multiplication;
using LinearNet.Matrices.QR;
using LinearNet.Providers;
using LinearNet.Providers.Managed;
using LinearNet.Structs;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LinearNet.Structs
{
    /// <summary>
    /// <p>
    /// Implements a dense matrix object where each element is of type <txt>T</txt>.
    /// </p>
    /// 
    /// <p>
    /// The linear algebra package contain optimised methods for <txt>DenseMatrix</txt> over the 
    /// primitive types <txt>int, long, float, double</txt> and <txt>decimal</txt>, as well as custom
    /// types that implement an algebraic interface (such as <txt>IRing&ltT&gt</txt> or <txt>IField&ltT&gt</txt>). 
    /// </p>
    /// 
    /// <p>
    /// This class is well suited for small to moderately-sized matrices where the number of non-zero elements is large compared to the number of 
    /// zeroes.
    /// </p>
    /// 
    /// <p>
    /// Only a small portion of dense matrix functionality is implementated as instance methods in this 
    /// class. The rest are implementated as <a href="extensions.html">extension methods</a> supporting dense matrices of specific types 
    /// (e.g. <txt>DenseMatrix&ltdouble&gt</txt>).
    /// </p>
    /// 
    /// <p>
    /// This class is for the matrix object itself. There is another static class with the same name <a href="DenseMatrix.html"><txt>DenseMatrix</txt></a>,
    /// which contain some convenience methods for quick initialization of such matrices.
    /// </p>
    /// </summary>
    /// <cat>linear-algebra</cat>
    public class DenseMatrix<T> : Matrix<T> where T : new()
    {
        private T[][] _values;

        /// <summary>
        /// The elements of this dense matrix arranged in row-major order (the $i$-th array represents the $i$-th row of this matrix).
        /// </summary>
        public T[][] Values { get { return _values; } }

        /// <summary>
        /// Gets or sets the $(i, j)$-th element of this matrix.
        /// </summary>
        /// <param name="row">The row index, $i$.</param>
        /// <param name="col">The column index, $j$.</param>
        /// <returns>The $(i, j)$-th element of this matrix.</returns>
        public override T this[int row, int col] { get => _values[row][col]; set => _values[row][col] = value; }
        public override T[] this[int index] { get => _values[index]; set => _values[index] = value; }

        public override int SymbolicNonZeroCount => Rows * Columns;

        public override IEnumerable<MatrixEntry<T>> NonZeroEntries
        {
            get
            {
                for (int r = 0; r < Rows; ++r)
                {
                    T[] row = _values[r];
                    for (int c = 0; c < Columns; ++c)
                    {
                        yield return new MatrixEntry<T>(r, c, row[c]);
                    }
                }
            }
        }

        #region Constructors

        /// <summary>
        /// Initialize a dense matrix with the specified number of rows and columns.
        /// </summary>
        /// <param name="rows">The number of rows of the matrix.</param>
        /// <param name="columns">The number of columns of the matrix.</param>
        public DenseMatrix(int rows, int columns) : base(MatrixStorageType.DENSE, rows, columns)
        {
            _values = MatrixInternalExtensions.JMatrix<T>(rows, columns);
        }

        /// <summary>
        /// Create a matrix given its values as a contingous array, and the number of rows.
        /// </summary>
        /// <param name="rows">The number of rows of the matrix.</param>
        /// <param name="values">The values of the matrix, as a single array, arranged in row-major order.</param>
        public DenseMatrix(int rows, T[] values) : base(MatrixStorageType.DENSE)
        {
            if (values is null)
            {
                throw new ArgumentNullException(nameof(values));
            }
            if (rows < 0 || rows > values.Length)
            {
                throw new ArgumentOutOfRangeException(nameof(rows));
            }

            Rows = rows;
            Columns = values.Length / rows;

            _values = new T[rows][];
            int i, j, k = 0;
            for (i = 0; i < rows; ++i)
            {
                T[] row = new T[Columns];
                for (j = 0; j < Columns; ++j)
                {
                    row[j] = values[k++];
                }
                _values[i] = row;
            }
        }

        /// <summary>
        /// Initialise a matrix with its values in a jagged array. 
        /// </summary>
        /// <param name="values">The values of the matrix.</param>
        public DenseMatrix(T[][] values) : base(MatrixStorageType.DENSE)
        {
            // Check all rows are of the same length
            if (values is null)
            {
                throw new ArgumentNullException(nameof(values));
            }
            if (values.Length == 0 || values[0].Length == 0)
            {
                throw new ArgumentOutOfRangeException("Cannot create an empty matrix");
            }

            Rows = values.Length;
            Columns = values[0].Length;

            int cols = values[0].Length, rows = values.Length, i;
            for (i = 1; i < rows; ++i)
            {
                if (values[i] is null)
                {
                    throw new ArgumentNullException(nameof(values));
                }
                if (values[i].Length != cols)
                {
                    throw new ArgumentOutOfRangeException("Inconsistent column dimensions.");
                }
            }

            _values = values;
        }

        /// <summary>
        /// Initialize a matrix with values of a 2-dimensional rectangular array.
        /// </summary>
        /// <param name="values">The values of the matrix.</param>
        public DenseMatrix(T[,] values) : base(MatrixStorageType.DENSE)
        {
            if (values is null)
            {
                throw new ArgumentNullException(nameof(values));
            }
            if (values.Length == 0)
            {
                throw new ArgumentOutOfRangeException("Cannot create an empty matrix.");
            }

            Rows = values.GetLength(0);
            Columns = values.GetLength(1);
            _values = MatrixInternalExtensions.ToJagged(values);
        }

        /// <summary>
        /// Initialize a dense matrix using the values of a band matrix of the same dimensions.
        /// </summary>
        /// <param name="matrix">A band matrix.</param>
        public DenseMatrix(BandMatrix<T> matrix) : base(MatrixStorageType.DENSE)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            Rows = matrix.Rows;
            Columns = matrix.Columns;
            _values = MatrixInternalExtensions.JMatrix<T>(matrix.Rows, matrix.Columns);

            int len, i, j;
            for (i = 0; i < matrix.UpperBandwidth; ++i)
            {
                T[] band = matrix.UpperBands[i];
                len = band.Length;
                int offset = i + 1;
                for (j = 0; j < len; ++j)
                {
                    _values[j][j + offset] = band[j];
                }
            }

            len = matrix.Diagonal.Length;
            for (i = 0; i < len; ++i)
            {
                _values[i][i] = matrix.Diagonal[i];
            }

            for (i = 0; i < matrix.LowerBandwidth; ++i)
            {
                T[] band = matrix.LowerBands[i];
                len = band.Length;
                int offset = i + 1;
                for (j = 0; j < len; ++j)
                {
                    _values[j + offset][j] = band[j];
                }
            }
        }

        public DenseMatrix(BlockDiagonalMatrix<T> matrix) : base(MatrixStorageType.DENSE)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            Rows = matrix.Rows;
            Columns = matrix.Columns;

            T zero = new T();
            if (zero.Equals(default))
            {
                _values = MatrixInternalExtensions.JMatrix<T>(Rows, Columns);
            }
            else
            {
                _values = MatrixInternalExtensions.JRepeat(zero, Rows, Columns);
            }

            int[] indices = matrix.BlockIndices;
            DenseMatrix<T>[] blocks = matrix.Blocks;

            for (int b = 0; b < indices.Length; ++b)
            {
                int offset = indices[b];
                _values.CopyFrom(blocks[b].Values, 0, 0, offset, offset);
            }
        }

        /// <summary>
        /// Initialize a dense matrix using a sparse matrix stored in Coordinate List (COO) form.
        /// </summary>
        /// <param name="matrix">The COO sparse matrix.</param>
        public DenseMatrix(COOSparseMatrix<T> matrix) : base(MatrixStorageType.DENSE)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            Rows = matrix.Rows;
            Columns = matrix.Columns;

            _values = new T[Rows][];
            for (int r = 0; r < Rows; ++r)
            {
                _values[r] = RectangularVector.Zeroes<T>(Columns);
            }

            List<MatrixEntry<T>> elements = matrix.Values;
            foreach (MatrixEntry<T> e in elements)
            {
                _values[e.RowIndex][e.ColumnIndex] = e.Value;
            }
        }

        /// <summary>
        /// Initialize a dense matrix from a sparse matrix stored in CSR (condensed sparse row) format. 
        /// </summary>
        /// <param name="matrix">The CSR sparse matrix.</param>
        public DenseMatrix(CSRSparseMatrix<T> matrix) : base(MatrixStorageType.DENSE)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            Rows = matrix.Rows;
            Columns = matrix.Columns;

            T[] values = matrix.Values;
            int[] columnIndices = matrix.ColumnIndices,
                rowIndices = matrix.RowIndices;

            _values = new T[Rows][];
            for (int r = 0; r < Rows; ++r)
            {
                int row_start = rowIndices[r],
                    row_end = rowIndices[r + 1];

                T[] row = RectangularVector.Zeroes<T>(Columns);
                for (int i = row_start; i < row_end; ++i)
                {
                    row[columnIndices[i]] = values[i];
                }
                _values[r] = row;
            }
        }

        /// <summary>
        /// Create a dense matrix from a sparse matrix stored in CSC (Compressed Sparse Column) format.
        /// </summary>
        /// <param name="matrix">The sparse matrix.</param>
        public DenseMatrix(CSCSparseMatrix<T> matrix) : base(MatrixStorageType.DENSE)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            Rows = matrix.Rows;
            Columns = matrix.Columns;

            _values = MatrixInternalExtensions.JZero<T>(Rows, Columns);

            int[] columnIndices = matrix.ColumnIndices, rowIndices = matrix.RowIndices;
            T[] values = matrix.Values;

            for (int c = 0; c < Columns; ++c)
            {
                int col_end = columnIndices[c + 1];
                for (int i = columnIndices[c]; i < col_end; ++i)
                {
                    _values[rowIndices[i]][c] = values[i];
                }
            }
        }

        /// <summary>
        /// Copy constructor - initialize a matrix given the values of another matrix. 
        /// </summary>
        /// <param name="matrix">A dense matrix.</param>
        public DenseMatrix(DenseMatrix<T> matrix) : base(MatrixStorageType.DENSE)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            Rows = matrix.Rows;
            Columns = matrix.Columns;

            int m = matrix.Rows, i;
            _values = new T[m][];
            for (i = 0; i < m; ++i)
            {
                _values[i] = matrix[i].Copy();
            }
        }

        /// <summary>
        /// Initialize a dense matrix using the values of a diagonal matrix of the same dimensions.
        /// </summary>
        /// <param name="matrix">A diagonal matrix.</param>
        public DenseMatrix(DiagonalMatrix<T> matrix) : base(MatrixStorageType.DENSE)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            Rows = matrix.Rows;
            Columns = matrix.Columns;

            _values = MatrixInternalExtensions.JMatrix<T>(Rows, Columns);

            T[] diag = matrix.DiagonalTerms;
            int min = Util.Min(diag.Length, Rows, Columns), i;
            for (i = 0; i < min; ++i)
            {
                _values[i][i] = diag[i];
            }
        }

        /// <summary>
        /// Initialize a dense matrix using the values of a sparse matrix of the same dimensions.
        /// </summary>
        /// <param name="matrix">A sparse matrix.</param>
        public DenseMatrix(DOKSparseMatrix<T> matrix) : base(MatrixStorageType.DENSE)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            Rows = matrix.Rows;
            Columns = matrix.Columns;

            _values = MatrixInternalExtensions.JZero<T>(Rows, Columns);

            Dictionary<long, T> values = matrix.Values;
            long lc = Columns;
            foreach (KeyValuePair<long, T> e in values)
            {
                int row = (int)(e.Key / lc), col = (int)(e.Key % lc);
                _values[row][col] = e.Value;
            }
        }

        /// <summary>
        /// Create a dense matrix from a sparse matrix stored in MCSR (Modified compressed sparse row) format.
        /// </summary>
        /// <param name="matrix">The sparse matrix.</param>
        public DenseMatrix(MCSRSparseMatrix<T> matrix) : base(MatrixStorageType.DENSE)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            Rows = matrix.Rows;
            Columns = matrix.Columns;

            if (new T().Equals(default))
            {
                _values = MatrixInternalExtensions.JMatrix<T>(Rows, Columns);
            }
            else
            {
                _values = MatrixInternalExtensions.JRepeat(new T(), Rows, Columns);
            }

            int[] indices = matrix.Indices;
            T[] values = matrix.Values;

            for (int r = 0; r < Rows; ++r)
            {
                T[] row = _values[r];

                // Copy the diagonal elements
                row[r] = values[r];

                // Copy the off-diagonal elements in row r
                int col_end = indices[r + 1];
                for (int i = indices[r]; i < col_end; ++i)
                {
                    int c = indices[i];
                    row[c] = values[i];
                }
            }
        }

        public DenseMatrix(SKYSparseMatrix<T> matrix) : base(MatrixStorageType.DENSE)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            Rows = matrix.Rows;
            Columns = matrix.Columns;

            T zero = new T();
            if (zero.Equals(default))
            {
                _values = MatrixInternalExtensions.JMatrix<T>(Rows, Columns);
            }
            else
            {
                _values = MatrixInternalExtensions.JRepeat(zero, Rows, Columns);
            }

            int[] indices = matrix.Indices;
            T[] values = matrix.Values;

            if (matrix.Type == SKYType.LOWER || matrix.Type == SKYType.SYMMETRIC)
            {
                for (int r = 0; r < Rows; ++r)
                {
                    T[] row = _values[r];

                    int start = indices[r], end = indices[r + 1],
                        col_start = r - (end - start) + 1;

                    for (int c = col_start, i = start; c <= r; ++c, ++i)
                    {
                        row[c] = values[i];
                    }
                }

                if (matrix.Type == SKYType.SYMMETRIC)
                {
                    // Also include the upper triangular part
                    for (int c = 0; c < Columns; ++c)
                    {
                        int start = indices[c], end = indices[c + 1],
                            row_start = c - (end - start) + 1;

                        for (int r = row_start, i = start; r < c; ++r, ++i)
                        {
                            _values[r][c] = values[i];
                        }
                    }
                }
            }
            else if (matrix.Type == SKYType.UPPER)
            {
                for (int c = 0; c < Columns; ++c)
                {
                    int start = indices[c], end = indices[c + 1],
                        row_start = c - (end - start) + 1;

                    for (int r = row_start, i = start; r <= c; ++r, ++i)
                    {
                        _values[r][c] = values[i];
                    }
                }
            }
        }

        public DenseMatrix(Matrix<T> matrix) : base(MatrixStorageType.DENSE)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            if (matrix.Rows < 0 || matrix.Columns < 0)
            {
                throw new OverflowException("Matrix cannot be represented as a dense matrix.");
            }

            Rows = matrix.Rows;
            Columns = matrix.Columns;

            try
            {
                _values = MatrixInternalExtensions.JZero<T>(Rows, Columns);
            }
            catch (OutOfMemoryException)
            {
                throw new OverflowException("Matrix is too large to be stored as a dense matrix.");
            }

            IEnumerable<MatrixEntry<T>> values = matrix.NonZeroEntries;
            foreach (MatrixEntry<T> e in values)
            {
                _values[e.RowIndex][e.ColumnIndex] = e.Value;
            }
        }

        internal DenseMatrix(int rows, int columns, T[][] values) : base(MatrixStorageType.DENSE)
        {
            Rows = rows;
            Columns = columns;
            _values = values;
        }

        #endregion

        #region Public methods 

        /// <summary>
        /// Returns the matrix inverse of this matrix. Throws <txt>InvalidOperationException</txt> if 
        /// the matrix is not square.
        /// </summary>
        /// <param name="algorithm">The matrix inversion algorithm to use.</param>
        /// <returns>The inverse of this matrix.</returns>
        public DenseMatrix<T> Invert(IMatrixInversion<T> algorithm)
        {
            if (!IsSquare) throw new InvalidOperationException("Matrix is not square.");

            DenseMatrix<T> inverse = new DenseMatrix<T>(Rows, Columns);
            algorithm.Invert(Values, inverse.Values, Rows);
            return inverse;
        }

        /// <summary>
        /// Return the leading diagonal (main diagonal) of matrix as an array.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns>An array containing the values of the leading diagonal.</returns>
        public override T[] LeadingDiagonal()
        {
            int n = Math.Min(Rows, Columns), i;
            T[] diagonal = new T[n];
            for (i = 0; i < n; i++)
            {
                diagonal[i] = _values[i][i];
            }
            return diagonal;
        }

        /// <summary>
        /// <para>Copy the row of index <txt>rowIndex</txt> from matrix $A$ into the array <txt>row</txt>.</para>
        /// <para>Also see: <a href="#Row"><txt>IMatrix&lt;T&gt;.Row(int rowIndex)</txt></a></para>
        /// <h4>Example</h4>
        /// <pre><code class="cs">
        /// DenseMatrix&lt;int&gt; matrix = new int[,] { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } };
        /// int[] row = new int[3];
        /// // Copy the 3rd row of 'matrix' into 'row'
        /// matrix.Row(2, row);
        /// row.Print();  // [7, 8, 9]
        /// 
        /// </code></pre>
        /// </summary>
        /// <param name="rowIndex">
        /// The index of the row to copy. <br/>
        /// Throws <txt>ArgumentOutOfRangeException</txt> if <txt>rowIndex</txt> is not a valid row index.
        /// </param>
        /// <param name="row">
        /// The array to copy onto. <br/>
        /// Throws <txt>ArgumentOutOfRangeException</txt> if its length does not equal the number of columns of the matrix.
        /// </param>
        public void Row(int rowIndex, T[] row)
        {
            if (rowIndex < 0 || rowIndex >= Rows || row.Length != Columns) throw new ArgumentOutOfRangeException();

            int len = row.Length, i;
            T[] A_row = _values[rowIndex];
            for (i = 0; i < len; ++i)
            {
                row[i] = A_row[i];
            }
        }

        /// <summary>
        /// <para>Returns the row of index <txt>rowIndex</txt> from a matrix.</para>
        /// <para>Also see: <a href="#RowSet"><txt>IMatrix&lt;T&gt;.Row(int rowIndex, T[] row)</txt></a></para>
        /// </summary>
        /// <param name="rowIndex">
        /// The index of the row to return.<br/>
        /// Throws <txt>ArgumentOutOfRangeException</txt> if <txt>rowIndex</txt> is not a valid row index.
        /// </param>
        /// <returns>The <txt>rowIndex</txt>-th row of the matrix.</returns>
        public override DenseVector<T> Row(int rowIndex)
        {
            if (rowIndex < 0 || rowIndex >= Rows) throw new ArgumentOutOfRangeException();
            return new DenseVector<T>(_values[rowIndex].Copy());
        }

        public override SparseVector<T> SparseRow(int rowIndex)
        {
            if (rowIndex < 0 || rowIndex >= Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(rowIndex));
            }

            int[] indices = new int[Columns];
            for (int i = 0; i < Columns; ++i)
            {
                indices[i] = i;
            }

            T[] values = new T[Columns];
            Array.Copy(_values[rowIndex], values, Columns);

            return new SparseVector<T>(Columns, indices, values, true);
        }

        /// <summary>
        /// Extract column from a dense matrix, storing it in the array 'column'.
        /// </summary>
        /// <param name="columnIndex">The index of the column of the matrix to copy.</param>
        /// <param name="column">An array that will contain the values of the matrix column.</param>
        public void Column(int columnIndex, T[] column)
        {
            _values.Column(columnIndex, column);
        }

        public override DenseVector<T> Column(int columnIndex)
        {
            T[] column = new T[Rows];
            Column(columnIndex, column);
            return new DenseVector<T>(column);
        }

        public T InnerProduct(DenseMatrix<T> matrix, IDenseBLAS1<T> blas1)
        {
            if (matrix is null) throw new ArgumentNullException(nameof(matrix));
            if (blas1 is null) throw new ArgumentNullException(nameof(blas1));

            MatrixChecks.AssertMatrixDimensionsEqual(this, matrix);

            IProvider<T> provider = blas1.Provider;
            T prod = provider.Zero;
            for (int i = 0; i < Rows; ++i)
            {
                prod = provider.Add(prod, blas1.DOT(_values[i], matrix._values[i], 0, Columns));
            }
            return prod;
        }

        public DenseTensor<T> TensorProduct(DenseVector<T> vector)
        {
            if (!ProviderFactory.TryGetDefaultBLAS1Provider(out IDenseBLAS1<T> blas))
            {
                throw new NotSupportedException(ExceptionMessages.TypeNotSupported);
            }
            return TensorProduct(vector, blas);
        }

        /// <summary>
        /// <para>Given a matrix $A\in\mathbb{F}^{m \times n}$ and a vector $x\in\mathbb{F}^p$, calculates and returns the order-3 outer product 
        /// $(A \otimes x)\in\mathbb{F}^{m \times n \times p}$, defined elementwise as
        /// $$(A \otimes x)_{ijk} := A_{ij}x_{k},$$
        /// for $1\le i\le{m}, 1\le j\le{n}, 1\le k\le{p}$.
        /// </para>
        /// <para>Implemented for matrices and vectors over <txt>int</txt>, <txt>long</txt>, <txt>float</txt>, <txt>double</txt>, <txt>decimal</txt> and all types <txt>T</txt> implementing <txt>Ring&lt;T&gt;</txt>.</para>
        /// <para>Also see: <a href="#OuterProductMatrixMatrix""><b><txt>ITensor&lt;T&gt; OuterProduct(this IMatrix&lt;T&gt; A, IMatrix&lt;T&gt; B)</txt></b></a></para>
        /// <!--inputs-->
        /// <!--returns-->
        /// </summary>
        /// <param name="v">A $p$-dimensional vector.</param>
        /// <returns>A order-3 tensor of dimension $m \times n \times p$.</returns>
        public DenseTensor<T> TensorProduct(DenseVector<T> vector, IDenseBLAS1Provider<T> blas1)
        {
            if (vector is null)
            {
                throw new ArgumentNullException(nameof(vector));
            }
            if (blas1 is null)
            {
                throw new ArgumentNullException(nameof(blas1));
            }
            if (!(blas1 is IDenseBLAS1<T> b))
            {
                throw new ArgumentNullException(ExceptionMessages.TypeNotSupported);
            }

            int dim = vector.Dimension;
            long len = (long)Rows * Columns * dim;
            T[] values = new T[len];

            if (values.LongLength > int.MaxValue)
            {
                IProvider<T> provider = b.Provider;
                long k = 0;
                for (int r = 0; r < Rows; ++r)
                {
                    T[] row = _values[r];
                    for (int c = 0; c < Columns; ++c)
                    {
                        T v = row[c];
                        for (int d = 0; d < dim; ++d)
                        {
                            values[k++] = provider.Multiply(v, vector.Values[d]);
                        }
                    }
                }
            }
            else
            {
                for (int r = 0, k = 0; r < Rows; ++r)
                {
                    T[] row = _values[r];
                    for (int c = 0; c < Columns; ++c)
                    {
                        Array.Copy(vector.Values, 0, values, k, dim);
                        b.SCAL(values, row[c], k, k + dim);
                        k += dim;
                    }
                }
            }
            return new DenseTensor<T>(new int[] { Rows, Columns, dim }, values);
        }

        public DenseTensor<T> TensorProduct(DenseMatrix<T> matrix)
        {
            return TensorProduct(matrix, ProviderFactory.GetDefaultBLAS1<T>());
        }

        /// <summary>
        /// <para>Given two matrices $A\in\mathbb{F}^{m \times n}$ and $B\in\mathbb{F}^{p \times q}$, calculates and returns the order-4 outer product 
        /// $(A \otimes B)\in\mathbb{F}^{m \times n \times p \times q}$, defined elementwise as
        /// $$(A \otimes B)_{ijkl} := A_{ij}B_{kl},$$
        /// for $1\le i\le{m}, 1\le j\le{n}, 1\le k\le{p}, 1\le l\le{q}$.
        /// </para>
        /// <para>Implemented for matrices over <txt>int</txt>, <txt>long</txt>, <txt>float</txt>, <txt>double</txt>, <txt>decimal</txt> and all types <txt>T</txt> implementing <txt>Ring&lt;T&gt;</txt>.</para>
        /// <para>
        /// Also see: <a href="#OuterProductMatrixVector""><b><txt>ITensor&lt;T&gt; OuterProduct(this IMatrix&lt;T&gt; A, IVector&lt;T&gt; v)</txt></b></a>
        /// </para>
        /// <!--inputs-->
        /// <!--returns-->
        /// </summary>
        /// <param name="matrix">A dense matrix.</param>
        /// <returns>A order-4 tensor of dimension $m \times n \times p \times q$.</returns>
        public DenseTensor<T> TensorProduct(DenseMatrix<T> matrix, IDenseBLAS1Provider<T> blas1)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            if (blas1 is null)
            {
                throw new ArgumentNullException(nameof(blas1));
            }
            if (!(blas1 is IDenseBLAS1<T> b))
            {
                throw new NotSupportedException(ExceptionMessages.TypeNotSupported);
            }

            int r1 = matrix.Rows, c1 = matrix.Columns;
            long len = (long)Rows * Columns * r1 * c1;
            T[] values = new T[len];

            if (len > int.MaxValue)
            {
                IProvider<T> provider = b.Provider;
                long k = 0;
                for (int r = 0; r < Rows; ++r)
                {
                    T[] row = _values[r];
                    for (int c = 0; c < Columns; ++c)
                    {
                        T v = row[c];

                        for (int p = 0; p < r1; ++p)
                        {
                            T[] row1 = matrix._values[p];
                            for (int q = 0; q < c1; ++q)
                            {
                                values[k++] = provider.Multiply(row1[q], v);
                            }
                        }
                    }
                }
            }
            else
            {
                for (int r = 0, k = 0; r < Rows; ++r)
                {
                    T[] row = _values[r];
                    for (int c = 0; c < Columns; ++c)
                    {
                        T v = row[c];
                        for (int p = 0; p < r1; ++p)
                        {
                            Array.Copy(matrix._values[p], 0, values, k, c1);
                            b.SCAL(values, v, k, k + c1);
                            k += c1;
                        }
                    }
                }
            }

            return new DenseTensor<T>(new int[] { Rows, Columns, r1, c1 }, values);
        }

        public override SparseVector<T> SparseColumn(int columnIndex)
        {
            if (columnIndex < 0 || columnIndex >= Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(columnIndex));
            }

            int[] indices = new int[Rows];
            T[] values = new T[Rows];

            for (int r = 0; r < Rows; ++r)
            {
                indices[r] = r;
                values[r] = _values[r][columnIndex];
            }

            return new SparseVector<T>(Rows, indices, values, true);
        }

        /// <summary>
        /// Extract the submatrix of dimension <txt>rows</txt> $\times$ <txt>columns</txt>, starting from 
        /// the coordinates (<txt>firstRow</txt>, <txt>firstCol</txt>), returning a new matrix.
        /// 
        /// <!--inputs-->
        /// 
        /// <para><b>Example</b></para>
        /// <pre><code class="cs">
        /// // 1 2 3
        /// // 4 5 6
        /// // 7 8 9 
        /// DenseMatrix&lt;double&gt; matrix = new double[,] { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } };
        /// DenseMatrix&lt;double&gt; submatrix = matrix.Submatrix(1, 1, 2, 2);
        /// 
        /// // 5 6
        /// // 8 9
        /// submatrix.Print();
        /// 
        /// </code></pre>
        /// </summary>
        /// <param name="rowStart">The first row of the matrix to extract to the submatrix.</param>
        /// <param name="columnStart">The first column of the matrix to extract to the submatrix.</param>
        /// <param name="rows">The number of rows of the submatrix.</param>
        /// <param name="columns">The number of columns of the submatrix.</param>
        /// <returns>The dense submatrix of this matrix.</returns>
        public DenseMatrix<T> Submatrix(int rowStart, int columnStart, int rows, int columns)
        {
            if (rowStart < 0 || columnStart < 0) throw new ArgumentOutOfRangeException();
            if (rowStart + rows > Rows || columnStart + columns > Columns) throw new ArgumentOutOfRangeException();

            return new DenseMatrix<T>(_values.Submatrix(rowStart, columnStart, rows, columns));
        }

        public override Matrix<T> Select(int[] rowIndices, int[] columnIndices)
        {
            if (rowIndices is null)
            {
                throw new ArgumentNullException(nameof(rowIndices));
            }
            if (columnIndices is null)
            {
                throw new ArgumentNullException(nameof(columnIndices));
            }

            ThrowHelper.AssertCollectionInRange(rowIndices, 0, Rows, nameof(rowIndices));
            ThrowHelper.AssertCollectionInRange(columnIndices, 0, Columns, nameof(columnIndices));

            int rlen = rowIndices.Length, clen = columnIndices.Length;
            T[][] values = MatrixInternalExtensions.JMatrix<T>(rlen, clen);

            for (int ri = 0; ri < rlen; ++ri)
            {
                T[] row = _values[rowIndices[ri]];
                T[] new_row = values[ri];
                for (int ci = 0; ci < clen; ++ci)
                {
                    new_row[ci] = row[columnIndices[ci]];
                }
            }
            return new DenseMatrix<T>(rlen, clen, values);
        }

        public override Vector<T> SelectAsVector(int rowIndex, int[] columnIndices)
        {
            if (rowIndex < 0 || rowIndex >= Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(rowIndex));
            }
            if (columnIndices is null)
            {
                throw new ArgumentNullException(nameof(columnIndices));
            }
            ThrowHelper.AssertCollectionInRange(columnIndices, 0, Columns, nameof(columnIndices));

            int clen = columnIndices.Length;
            T[] vector = new T[clen], row = _values[rowIndex];
            for (int ci = 0; ci < clen; ++ci)
            {
                vector[ci] = row[columnIndices[ci]];
            }
            return new DenseVector<T>(vector);
        }

        public override Vector<T> SelectAsVector(int[] rowIndices, int columnIndex)
        {
            if (rowIndices is null)
            {
                throw new ArgumentNullException(nameof(rowIndices));
            }
            ThrowHelper.AssertCollectionInRange(rowIndices, 0, Rows, nameof(rowIndices));
            if (columnIndex < 0 || columnIndex >= Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(columnIndex));
            }

            int rlen = rowIndices.Length;
            T[] vector = new T[rlen];
            for (int ri = 0; ri < rlen; ++ri)
            {
                vector[ri] = _values[rowIndices[ri]][columnIndex];
            }
            return new DenseVector<T>(vector);
        }

        /// <summary>
        /// Returns a new matrix equal to the lower-triangular matrix of this matrix. If this 
        /// matrix is not square, then all entries on or below the main diagonal are included.
        /// </summary>
        /// <returns>A lower-triangular matrix.</returns>
        public DenseMatrix<T> Lower()
        {
            T[][] lower = MatrixInternalExtensions.JZero<T>(Rows, Columns);
            for (int r = 0; r < Rows; ++r)
            {
                Array.Copy(_values[r], 0, lower[r], 0, Math.Min(r + 1, Columns));
            }
            return new DenseMatrix<T>(Rows, Columns, lower);
        }
        public DenseMatrix<T> Upper()
        {
            T[][] upper = MatrixInternalExtensions.JZero<T>(Rows, Columns);
            for (int r = 0; r < Rows; ++r)
            {
                if (r < Columns)
                {
                    Array.Copy(_values[r], r, upper[r], r, Columns - r);
                }
                else
                {
                    break;
                }
            }
            return new DenseMatrix<T>(Rows, Columns, upper);
        }

        /// <summary>
        /// Returns a new dense matrix of the same dimensions, where only entries that meet
        /// a condition are copied.
        /// </summary>
        /// <param name="predicate">The condition to meet.</param>
        /// <returns>The new dense matrix with the selected entries.</returns>
        public DenseMatrix<T> Select(Predicate<MatrixEntry<T>> predicate)
        {
            T[][] select = MatrixInternalExtensions.JZero<T>(Rows, Columns);
            for (int r = 0; r < Rows; ++r)
            {
                T[] row = _values[r], srow = select[r];
                for (int c = 0; c < Columns; ++c)
                {
                    if (predicate(new MatrixEntry<T>(r, c, row[c])))
                    {
                        srow[c] = row[c];
                    }
                }
            }
            return new DenseMatrix<T>(Rows, Columns, select);
        }

        public override T Trace(IProvider<T> provider)
        {
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }

            T trace = provider.Zero;
            for (int r = 0; r < Rows; ++r)
            {
                trace = provider.Add(trace, _values[r][r]);
            }
            return trace;
        }

        public override DenseVector<T> RowSums(IProvider<T> provider)
        {
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }

            T[] sums = RectangularVector.Repeat(provider.Zero, Rows);
            for (int r = 0; r < Rows; ++r)
            {
                T[] row = _values[r];
                T sum = row[0];
                for (int c = 1; c < Columns; ++c)
                {
                    sum = provider.Add(sum, row[c]);
                }
                sums[r] = sum;
            }
            return new DenseVector<T>(sums);
        }
        private DenseVector<T> ColumnSums(IDenseBLAS1<T> blas)
        {
            if (blas is null)
            {
                throw new ArgumentNullException(nameof(blas));
            }

            T[] sums = RectangularVector.Repeat(blas.Provider.Zero, Columns);
            for (int r = 0; r < Rows; ++r)
            {
                blas.ADD(sums, _values[r], sums, 0, Columns);
            }
            return new DenseVector<T>(sums);
        }
        public override DenseVector<T> ColumnSums(IProvider<T> provider)
        {
            if (ProviderFactory.TryGetDefaultBLAS1Provider(out IDenseBLAS1<T> blas))
            {
                return ColumnSums(blas);
            }

            T[] sums = RectangularVector.Repeat(provider.Zero, Columns);
            for (int r = 0; r < Rows; ++r)
            {
                T[] row = _values[r];
                for (int c = 0; c < Columns; ++c)
                {
                    sums[c] = provider.Add(sums[c], row[c]);
                }
            }
            return new DenseVector<T>(sums);
        }

        public override int Count(Predicate<T> predicate)
        {
            if (predicate is null) throw new ArgumentNullException(nameof(predicate));

            int r, c, count = 0;
            for (r = 0; r < Rows; ++r)
            {
                T[] row = _values[r];
                for (c = 0; c < Columns; ++c)
                {
                    if (predicate(row[c]))
                    {
                        ++count;
                    }
                }
            }
            return count;
        }

        #region Boolean methods 

        /// <summary>
        /// Returns whether this matrix is symmetric.
        /// </summary>
        /// <param name="equalityComparer"></param>
        /// <returns></returns>
        public new bool IsSymmetric(IEqualityComparer<T> equalityComparer)
        {
            if (!IsSquare)
            {
                return false;
            }

            if (equalityComparer is null)
            {
                throw new ArgumentNullException(nameof(equalityComparer));
            }

            return IsSymmetric((a, b) => equalityComparer.Equals(a, b));
        }

        /// <summary>
        /// Returns whether this matrix is symmetric, using the specified equality provider 
        /// to evaluate whether two matrix elements should be considered equal.
        /// </summary>
        /// <param name="equalityPredicate">
        /// A predicate that compares two elements of type <txt>T</txt>
        /// and returns true if they should be considered equal, false otherwise.
        /// </param>
        /// <returns>Whether this matrix is symmetric.</returns>
        public new bool IsSymmetric(Func<T, T, bool> equalityPredicate)
        {
            if (!IsSquare)
            {
                return false;
            }

            int dim = Rows, i, j;
            for (i = 0; i < dim; ++i)
            {
                T[] A_i = _values[i];
                for (j = 0; j < i; ++j)
                {
                    if (!equalityPredicate(A_i[j], _values[j][i])) return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Returns whether this matrix is a zero matrix (i.e. all elements equal to zero), using 
        /// a predicate that returns <txt>true</txt> if an element of type <txt>T</txt> should be 
        /// considered as equal to zero.
        /// </summary>
        /// <param name="isZero">A predicate that returns true if its argument should be considered 
        /// as being equal to zero.</param>
        /// <returns>Whether this matrix is a zero matrix.</returns>
        public bool IsZero(Func<T, bool> isZero)
        {
            if (isZero is null)
            {
                throw new ArgumentNullException(nameof(isZero));
            }

            int r = Rows, c = Columns, i, j;

            for (i = 0; i < r; ++i)
            {
                T[] row = _values[i];
                for (j = 0; j < c; ++j)
                {
                    if (!isZero(row[j]))
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// Returns whether this matrix is a diagonal matrix (i.e. all off-diagonal terms are zero),
        /// using a predicate that returns <txt>true</txt> if an element of type <txt>T</txt> should 
        /// be considered as being equal to zero.
        /// </summary>
        /// <param name="isZero">A predicate that returns <txt>true</txt> if its argument should be 
        /// considered as being equal to zero.
        /// </param>
        /// <returns>Whether this matrix is a diagonal matrix.</returns>
        public bool IsDiagonal(Func<T, bool> isZero)
        {
            if (isZero is null)
            {
                throw new ArgumentNullException(nameof(isZero));
            }

            int r = Rows, c = Columns, i, j;
            for (i = 0; i < r; ++i)
            {
                T[] row = _values[i];
                for (j = 0; j < c; ++j)
                {
                    if (i != j)
                    {
                        if (!isZero(row[j]))
                        {
                            return false;
                        }
                    }
                }
            }
            return true;
        }

        /// <summary>
        /// Returns whether this matrix equals in value to another matrix, using a predicate to 
        /// determine whether two matrix elements should be considered equal.
        /// </summary>
        /// <param name="matrix">The matrix to compare to.</param>
        /// <param name="equals">A predicate that takes two arguments and returns whether they
        /// should be considered equal.</param>
        /// <returns>Whether the two matrices are equal in value.</returns>
        public bool Equals(DenseMatrix<T> matrix, Func<T, T, bool> equals)
        {
            if (matrix is null)
            {
                return false;
            }

            if (Rows != matrix.Rows || Columns != matrix.Columns)
            {
                return false;
            }

            for (int r = 0; r < Rows; ++r)
            {
                T[] row = _values[r], row2 = matrix._values[r];
                for (int c = 0; c < Columns; ++c)
                {
                    if (!equals(row[c], row2[c]))
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        /// <summary>
        /// Returns whether this matrix equals in value to another matrix, using an equality comparer
        /// to determine whether two matrix elements should be considered equal.
        /// </summary>
        /// <param name="matrix">The matrix to compare to.</param>
        /// <param name="comparer">The equality comparer used to determine whether two matrix elements 
        /// should be considered equal.</param>
        /// <returns>Whether the two matrices are equal in value.</returns>
        public bool Equals(DenseMatrix<T> matrix, IEqualityComparer<T> comparer)
        {
            return Equals(matrix, comparer.Equals);
        }

        /// <summary>
        /// Returns whether this matrix equals in value to another matrix, using the default equality 
        /// comparer for type <txt>T</txt>.
        /// </summary>
        /// <param name="matrix">The matrix to compare to.</param>
        /// <returns>Whether the two matrices are equal in value.</returns>
        public bool Equals(DenseMatrix<T> matrix)
        {
            return Equals(matrix, EqualityComparer<T>.Default);
        }

        public override bool Equals(object obj)
        {
            if (obj is null)
            {
                return false;
            }
            if (obj is DenseMatrix<T>)
            {
                return Equals(obj as DenseMatrix<T>);
            }
            return Equals(obj as Matrix<T>);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override bool IsLower(Predicate<T> isZero)
        {
            int r, c;
            for (r = 0; r < Rows; ++r)
            {
                T[] row = _values[r];
                for (c = r + 1; c < Columns; ++c)
                {
                    if (!isZero(row[c]))
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        public override bool IsUpper(Predicate<T> isZero)
        {
            int r, c, end;
            for (r = 0; r < Rows; ++r)
            {
                T[] row = _values[r];
                end = Math.Min(r, Columns);
                for (c = 0; c < end; ++c)
                {
                    if (!isZero(row[c]))
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        public override bool IsDiagonal(Predicate<T> isZero)
        {
            if (isZero is null)
            {
                throw new ArgumentNullException(nameof(isZero));
            }

            for (int r = 0; r < Rows; ++r)
            {
                T[] row = _values[r];
                for (int c = 0; c < Columns; ++c)
                {
                    if (r != c && !isZero(row[c]))
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        public override bool IsBanded(int lowerBandwidth, int upperBandwidth, Predicate<T> isZero)
        {
            if (isZero is null)
            {
                throw new ArgumentNullException(nameof(isZero));
            }

            for (int r = 0; r < Rows; ++r)
            {
                T[] row = _values[r];
                for (int c = 0; c < Columns; ++c)
                {
                    if (c > r)
                    {
                        if (c - r > upperBandwidth && !isZero(row[c]))
                        {
                            return false;
                        }
                    }
                    else
                    {
                        if (r - c > lowerBandwidth && !isZero(row[c]))
                        {
                            return false;
                        }
                    }
                }
            }
            return true;
        }

        public override bool IsZero(Predicate<T> isZero)
        {
            if (isZero is null)
            {
                throw new ArgumentNullException(nameof(isZero));
            }

            for (int r = 0; r < Rows; ++r)
            {
                T[] row = _values[r];
                for (int c = 0; c < Columns; ++c)
                {
                    if (!isZero(row[c]))
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        public override bool IsIdentity(Predicate<T> isZero, Predicate<T> isOne)
        {
            if (!IsSquare)
            {
                return false;
            }
            if (isZero is null)
            {
                throw new ArgumentNullException(nameof(isZero));
            }
            if (isOne is null)
            {
                throw new ArgumentNullException(nameof(isOne));
            }
            for (int r = 0; r < Rows; ++r)
            {
                T[] row = _values[r];
                for (int c = 0; c < Columns; ++c)
                {
                    if (r == c)
                    {
                        if (!isOne(row[c]))
                        {
                            return false;
                        }
                    }
                    else
                    {
                        if (!isZero(row[c]))
                        {
                            return false;
                        }
                    }
                }
            }
            return true;
        }

        #endregion


        #region Value modifiers 

        /// <summary>
        /// <p>Sets each value of this matrix equal to the value of <txt>new T()</txt>.</p>
        /// <p>For primitive types, this will be equivalent to setting each element to zero.</p>
        /// </summary>
        public override void Clear()
        {
            int i, j;
            for (i = 0; i < Rows; ++i)
            {
                T[] row = _values[i];
                for (j = 0; j < Columns; ++j)
                {
                    row[j] = new T();
                }
            }
        }

        /// <summary>
        /// Clears all entries that satisfy a predicate.
        /// </summary>
        /// <param name="select">The predicate used to select entries to clear.</param>
        public void Clear(Predicate<MatrixEntry<T>> select)
        {
            Set(select, new T());
        }

        /// <summary>
        /// Sets all entries that match a predicate to zero.
        /// </summary>
        /// <param name="select">The predicate to match</param>
        public override void Clear(Predicate<T> select)
        {
            if (select is null)
            {
                throw new ArgumentNullException(nameof(select));
            }

            T zero = new T();
            for (int r = 0; r < Rows; ++r)
            {
                T[] row = _values[r];
                for (int c = 0; c < Columns; ++c)
                {
                    if (select(row[c]))
                    {
                        row[c] = zero;
                    }
                }
            }
        }

        /// <summary>
        /// <p>Sets all elements of a row to the value of <txt>new T()</txt>.</p>
        /// <p>For primitive types, this is equivalent to setting those elements to zero.</p>
        /// </summary>
        /// <param name="rowIndex">The index of the row to clear.</param>
        public override void ClearRow(int rowIndex)
        {
            if (rowIndex < 0 || rowIndex >= Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(rowIndex));
            }

            if (typeof(T).IsValueType)
            {
                Array.Clear(_values[rowIndex], 0, Columns);
            }
            else
            {
                T[] row = _values[rowIndex];
                for (int i = 0; i < Columns; ++i)
                {
                    row[i] = new T();
                }
            }
        }

        /// <summary>
        /// <p>Sets all elements of a column to the value of <txt>new T()</txt>.</p>
        /// <p>If <txt>T</txt> is primitive, this is equivalent to setting those elements to zero.</p>
        /// </summary>
        /// <param name="columnIndex">The index of the column to clear.</param>
        public override void ClearColumn(int columnIndex)
        {
            if (columnIndex < 0 || columnIndex >= Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(columnIndex));
            }

            for (int i = 0; i < Rows; ++i)
            {
                _values[i][columnIndex] = new T();
            }
        }

        /// <summary>
        /// Sets all elements in a submatrix to zero.
        /// </summary>
        /// <param name="rowIndex">The index of the first row to clear.</param>
        /// <param name="columnIndex">The index of the first column to clear.</param>
        /// <param name="rows">The number of rows in the submatrix block.</param>
        /// <param name="columns">The number of columns in the submatrix block.</param>
        public override void ClearSubmatrix(int rowIndex, int columnIndex, int rows, int columns)
        {
            if (rows < 0 || rows >= Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(rows));
            }
            if (columns < 0 || columns >= Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(columns));
            }

            int rowEnd = rowIndex + rows, columnEnd = columnIndex + columns;
            if (rowIndex < 0 || rowEnd >= Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(rowIndex));
            }
            if (columnIndex < 0 || columnEnd >= Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(columnIndex));
            }

            if (typeof(T).IsValueType)
            {
                for (int r = rowIndex; r < rowEnd; ++r)
                {
                    Array.Clear(_values[r], columnIndex, columns);
                }
            }
            else
            {
                for (int r = rowIndex; r < rowEnd; ++r)
                {
                    T[] row = _values[r];
                    for (int c = columnIndex; c < columnEnd; ++c)
                    {
                        row[c] = new T();
                    }
                }
            }
        }

        /// <summary>
        /// Sets all entries under the main diagonal to zero.
        /// </summary>
        public override void ClearLower()
        {
            T zero = new T();
            if (zero.Equals(default))
            {
                for (int r = 1; r < Rows; ++r)
                {
                    Array.Clear(_values[r], 0, r);
                }
            }
            else
            {
                SetLower(zero);
            }
        }

        /// <summary>
        /// Sets all entries above the main diagonal to zero.
        /// </summary>
        public override void ClearUpper()
        {
            T zero = new T();
            if (zero.Equals(default))
            {
                for (int r = 0; r < Rows; ++r)
                {
                    Array.Clear(_values[r], r + 1, Columns - r - 1);
                }
            }
            else
            {
                SetUpper(zero);
            }
        }

        /// <summary>
        /// Set all matrix entries that satisfy a predicate to a value.
        /// </summary>
        /// <param name="select">The predicate.</param>
        /// <param name="value">The value to set to.</param>
        public void Set(Predicate<MatrixEntry<T>> select, T value)
        {
            if (select is null)
            {
                throw new ArgumentNullException(nameof(select));
            }
            if (value is null)
            {
                throw new ArgumentNullException(nameof(value));
            }

            for (int r = 0; r < Rows; ++r)
            {
                T[] row = _values[r];
                for (int c = 0; c < Columns; ++c)
                {
                    if (select(new MatrixEntry<T>(r, c, row[c])))
                    {
                        row[c] = value;
                    }
                }
            }
        }

        /// <summary>
        /// <p>
        /// Copy the values of a vector into a row of this matrix.
        /// </p>
        /// <p>
        /// If the vector dimension is greater than the number of columns of this matrix, 
        /// only the first part of the vector that fit into a row is copied. 
        /// </p>
        /// <p>
        /// If the vector dimension is smaller than the number of columns, only the first
        /// part of the matrix row will be changed. 
        /// </p>
        /// </summary>
        /// <param name="rowIndex">The index of the row to copy to.</param>
        /// <param name="rowValues">A vector of values to copy.</param>
        public void SetRow(int rowIndex, DenseVector<T> rowValues)
        {
            if (rowIndex < 0 || rowIndex >= Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(rowIndex));
            }
            if (rowValues is null)
            {
                throw new ArgumentNullException(nameof(rowValues));
            }

            int min = Math.Min(Columns, rowValues.Dimension);
            T[] row = _values[rowIndex];

            for (int i = 0; i < min; ++i)
            {
                row[i] = rowValues[i];
            }
        }

        /// <summary>
        /// Set all entries in a row of this matrix to the same value.
        /// </summary>
        /// <param name="rowIndex">The index of the row to modify.</param>
        /// <param name="value">The value to set row entries to.</param>
        public void SetRow(int rowIndex, T value)
        {
            if (rowIndex < 0 || rowIndex >= Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(rowIndex));
            }
            if (value is null)
            {
                throw new ArgumentNullException(nameof(value));
            }

            T[] row = _values[rowIndex];
            for (int i = 0; i < Columns; ++i)
            {
                row[i] = value;
            }
        }

        /// <summary>
        /// <p>Copy the values of a vector into a column of this matrix.</p>
        /// <p>
        /// If the vector dimension is greater than the number of rows of this matrix,
        /// only the firts part of the vector will be copied.
        /// </p>
        /// <p>
        /// If the vector dimension is smaller than the number of rows of this matrix,
        /// only the first part of the matrix's column will be changed.
        /// </p>
        /// </summary>
        /// <param name="columnIndex"></param>
        /// <param name="columnValues"></param>
        public void SetColumn(int columnIndex, DenseVector<T> columnValues)
        {
            if (columnIndex < 0 || columnIndex >= Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(columnIndex));
            }
            if (columnValues is null)
            {
                throw new ArgumentNullException(nameof(columnValues));
            }

            int min = Math.Min(Rows, columnValues.Dimension);
            for (int i = 0; i < min; ++i)
            {
                _values[i][columnIndex] = columnValues[i];
            }
        }

        /// <summary>
        /// Set all entries in a column of this matrix to the same value.
        /// </summary>
        /// <param name="columnIndex">The index of the column to modify.</param>
        /// <param name="value">The value to set the entries to.</param>
        public void SetColumn(int columnIndex, T value)
        {
            if (columnIndex < 0 || columnIndex >= Columns)
            {
                throw new ArgumentNullException(nameof(columnIndex));
            }
            if (value is null)
            {
                throw new ArgumentNullException(nameof(value));
            }

            for (int r = 0; r < Rows; ++r)
            {
                _values[r][columnIndex] = value;
            }
        }

        /// <summary>
        /// Copy a vector's values into a diagonal of this matrix. 
        /// <p>
        /// If <txt>diagonalIndex</txt> $=0$, the values will be copied into the main (leading) diagonal of this matrix.
        /// </p> 
        /// <p>
        /// If <txt>diagonalIndex</txt> $>0$, then the values will be copied into the <txt>diagonalIndex</txt>-th upper diagonal. 
        /// </p>
        /// <p>
        /// If <txt>diagonalIndex</txt> $<0$, then the values will be copied into the corresponding lower diagonal.
        /// </p>
        /// </summary>
        /// <param name="diagonalValues">The vector of values to copy into this matrix.</param>
        /// <param name="diagonalIndex">The index of the diagonal.</param>
        public void SetDiagonal(DenseVector<T> diagonalValues, int diagonalIndex = 0)
        {
            if (diagonalValues is null)
            {
                throw new ArgumentNullException(nameof(diagonalValues));
            }

            if (diagonalIndex == 0)
            {
                int min = Util.Min(diagonalValues.Dimension, Rows, Columns);
                for (int i = 0; i < min; ++i)
                {
                    _values[i][i] = diagonalValues[i];
                }
            }
            else if (diagonalIndex > 0)
            {
                int min = Util.Min(diagonalValues.Dimension, Rows, Columns - diagonalIndex);
                for (int i = 0; i < min; ++i)
                {
                    _values[i][i + diagonalIndex] = diagonalValues[i];
                }
            }
            else
            {
                int min = Util.Min(diagonalValues.Dimension, Rows + diagonalIndex, Columns);
                for (int i = 0; i < min; ++i)
                {
                    _values[i - diagonalIndex][i] = diagonalValues[i];
                }
            }
        }

        /// <summary>
        /// Set all entries of a diagonal of this matrix to the same value. 
        /// </summary>
        /// <param name="value">The value to set elements to.</param>
        /// <param name="diagonalIndex">The index of the diagonal to set values to.</param>
        public void SetDiagonal(T value, int diagonalIndex = 0)
        {
            if (value is null)
            {
                throw new ArgumentNullException(nameof(value));
            }
            if (diagonalIndex > 0 && diagonalIndex >= Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(diagonalIndex));
            }
            if (diagonalIndex < 0 && Math.Abs(diagonalIndex) >= Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(diagonalIndex));
            }
            if (diagonalIndex == 0)
            {
                int min = Math.Min(Rows, Columns);
                for (int i = 0; i < min; ++i)
                {
                    _values[i][i] = value;
                }
            }
            else if (diagonalIndex > 0)
            {
                int min = Math.Min(Rows, Columns - diagonalIndex);
                for (int i = 0; i < min; ++i)
                {
                    _values[i][i + diagonalIndex] = value;
                }
            }
            else
            {
                int min = Math.Min(Rows + diagonalIndex, Columns);
                for (int i = 0; i < min; ++i)
                {
                    _values[i - diagonalIndex][i] = value;
                }
            }
        }

        /// <summary>
        /// Set a submatrix block of values in this matrix to the values of another matrix.
        /// </summary>
        /// <param name="submatrix">The matrix of values to copy over to this matrix.</param>
        /// <param name="rowStart">The first row of this matrix to change.</param>
        /// <param name="columnStart">The first column of this matrix to change.</param>
        /// <param name="rows">The number of rows to copy. If negative, will default to the number of rows in <txt>submatrix</txt>.</param>
        /// <param name="columns">The number of columns to copy. If negative, will default to the number of columns in <txt>submatrix</txt>.</param>
        public void SetSubmatrix(DenseMatrix<T> submatrix, int rowStart = 0, int columnStart = 0, int rows = -1, int columns = -1)
        {
            if (submatrix is null)
            {
                throw new ArgumentNullException(nameof(submatrix));
            }
            if (rows > submatrix.Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(rows));
            }
            if (columns > submatrix.Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(columns));
            }
            if (rows < 0)
            {
                rows = submatrix.Rows;
            }
            if (columns < 0)
            {
                columns = submatrix.Columns;
            }
            if (rowStart < 0 || rowStart + rows > Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(rowStart));
            }
            if (columnStart < 0 || columnStart + columns >= Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(columnStart));
            }

            _values.CopyFrom(submatrix._values, 0, 0, rowStart, columnStart, rows, columns);
        }
        
        /// <summary>
        /// Set a submatrix block of values in this matrix to the same value.
        /// </summary>
        /// <param name="value">The value to set the submatrix entries to.</param>
        /// <param name="rowStart">The first row of this matrix to change.</param>
        /// <param name="columnStart">The first column of this matrix to change.</param>
        /// <param name="rows">The number of rows to copy.</param>
        /// <param name="columns">The number of columns to copy.</param>
        public void SetSubmatrix(T value, int rowStart, int columnStart, int rows, int columns)
        {
            if (rowStart < 0 || rowStart >= Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(rowStart));
            }
            if (columnStart < 0 || columnStart >= Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(columnStart));
            }
            if (rowStart + rows >= Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(rows));
            }
            if (columnStart + columns >= Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(columns));
            }
            if (value is null)
            {
                throw new ArgumentNullException(nameof(value));
            }

            int rowEnd = rowStart + rows, colEnd = columnStart + columns;
            for (int r = rowStart; r < rowEnd; ++r)
            {
                T[] row = _values[r];
                for (int c = columnStart; c < colEnd; ++c)
                {
                    row[c] = value;
                }
            }
        }

        /// <summary>
        /// Sets all entries under the main diagonal to a value.
        /// </summary>
        /// <param name="value">The value to set to.</param>
        public void SetLower(T value)
        {
            if (value is null)
            {
                throw new ArgumentNullException(nameof(value));
            }

            for (int r = 1; r < Rows; ++r)
            {
                T[] row = _values[r];
                for (int c = 0; c < r; ++c)
                {
                    row[c] = value;
                }
            }
        }

        /// <summary>
        /// Sets all entries above the main diagonal to a value.
        /// </summary>
        /// <param name="value">The value to set to.</param>
        public void SetUpper(T value)
        {
            if (value is null)
            {
                throw new ArgumentNullException(nameof(value));
            }

            for (int r = 0; r < Rows; ++r)
            {
                T[] row = _values[r];
                for (int c = r + 1; c < Columns; ++c)
                {
                    row[c] = value;
                }
            }
        }

        public void ReflectLowerInPlace()
        {
            if (!IsSquare)
            {
                throw new InvalidOperationException(ExceptionMessages.MatrixNotSquare);
            }

            for (int r = 0; r < Rows; ++r)
            {
                T[] row = _values[r];
                for (int c = r + 1; c < Columns; ++c)
                {
                    row[c] = _values[c][r];
                }
            }
        }
        /// <summary>
        /// Returns a new matrix created by overwriting the upper triangular half with the 
        /// transpose of the lower-triangular half. Only defined for square matrices.
        /// </summary>
        /// <returns></returns>
        public DenseMatrix<T> ReflectLower()
        {
            DenseMatrix<T> copy = Copy();
            copy.ReflectLowerInPlace();
            return copy;
        }

        public void ReflectUpperInPlace()
        {
            if (!IsSquare)
            {
                throw new InvalidOperationException(ExceptionMessages.MatrixNotSquare);
            }

            for (int r = 1; r < Rows; ++r)
            {
                T[] row = _values[r];
                for (int c = 0; c < r; ++c)
                {
                    row[c] = _values[c][r];
                }
            }
        }
        public DenseMatrix<T> ReflectUpper()
        {
            DenseMatrix<T> copy = Copy();
            copy.ReflectUpperInPlace();
            return copy;
        }

        /// <summary>
        /// Perform a rank-1 update of this matrix, i.e. setting this matrix to $A + \beta uv^T$.
        /// </summary>
        /// <param name="u"></param>
        /// <param name="v"></param>
        public void Rank1Update(T beta, DenseVector<T> u, DenseVector<T> v, IDenseBLAS1Provider<T> blas1)
        {
            if (beta is null)
            {
                throw new ArgumentNullException(nameof(beta));
            }
            if (u is null)
            {
                throw new ArgumentNullException(nameof(u));
            }
            if (v is null)
            {
                throw new ArgumentNullException(nameof(v));
            }
            if (blas1 is null)
            {
                throw new ArgumentNullException(nameof(blas1));
            }
            if (u.Dimension != Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(u), $"Dimension of vector '{nameof(u)}' does not match the number of rows.");
            }
            if (v.Dimension != Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(v), $"Dimension of vector '{nameof(v)}' does not match the number of columns.");
            }
            if (!(blas1 is IDenseBLAS1<T> b))
            {
                throw new NotSupportedException(ExceptionMessages.TypeNotSupported);
            }

            T[] _u = u.Values, _v = v.Values;
            IProvider<T> provider = b.Provider;
            for (int r = 0; r < Rows; ++r)
            {
                b.AXPY(_values[r], _v, provider.Multiply(beta, _u[r]), 0, Columns);
            }
        }

        #endregion


        #region Reshaping, reordering operations

        /// <summary>
        /// Returns the matrix created by extending this matrix with another matrix in the specified direction.
        /// </summary>
        /// <param name="matrix">The matrix used to extend this matrix.</param>
        /// <param name="direction">The append direction.</param>
        /// <returns>The extended matrix.</returns>
        public DenseMatrix<T> Append(DenseMatrix<T> matrix, AppendDirection direction)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            if (direction == AppendDirection.TOP) return matrix.Append(this, AppendDirection.BOTTOM);
            if (direction == AppendDirection.LEFT) return matrix.Append(this, AppendDirection.RIGHT);
            if (direction == AppendDirection.TOPLEFT) return matrix.Append(this, AppendDirection.BOTTOMRIGHT);
            if (direction == AppendDirection.BOTTOMLEFT) return matrix.Append(this, AppendDirection.TOPRIGHT);

            if (direction == AppendDirection.BOTTOM)
            {
                if (Columns != matrix.Columns)
                {
                    throw new InvalidOperationException(ExceptionMessages.MatrixColumnSize);
                }

                T[][] result = MatrixInternalExtensions.JMatrix<T>(Rows + matrix.Rows, Columns);
                result.CopyFrom(_values, 0, 0, 0, 0, Rows, Columns);
                result.CopyFrom(matrix._values, 0, 0, Rows, 0, matrix.Rows, matrix.Columns);
                return new DenseMatrix<T>(result.Length, Columns, result);
            }

            if (direction == AppendDirection.RIGHT)
            {
                if (Rows != matrix.Rows)
                {
                    throw new InvalidOperationException(ExceptionMessages.MatrixRowSize);
                }

                T[][] result = MatrixInternalExtensions.JMatrix<T>(Rows, Columns + matrix.Columns);
                result.CopyFrom(_values, 0, 0, 0, 0, Rows, Columns);
                result.CopyFrom(matrix._values, 0, 0, 0, Columns, matrix.Rows, matrix.Columns);
                return new DenseMatrix<T>(Rows, Columns + matrix.Columns, result);
            }

            if (direction == AppendDirection.BOTTOMRIGHT)
            {
                // A 0 
                // 0 B
                T[][] result = MatrixInternalExtensions.JMatrix<T>(Rows + matrix.Rows, Columns + matrix.Columns);
                result.CopyFrom(_values, 0, 0, 0, 0, Rows, Columns);
                result.CopyFrom(matrix._values, 0, 0, Rows, Columns, matrix.Rows, matrix.Columns);
                return new DenseMatrix<T>(Rows + matrix.Rows, Columns + matrix.Columns, result);
            }

            if (direction == AppendDirection.TOPRIGHT)
            {
                // 0 B
                // A 0
                T[][] result = MatrixInternalExtensions.JMatrix<T>(Rows + matrix.Rows, Columns + matrix.Columns);
                result.CopyFrom(matrix._values, 0, 0, 0, Columns, matrix.Rows, matrix.Columns);
                result.CopyFrom(_values, 0, 0, matrix.Columns, 0, Rows, Columns);
                return new DenseMatrix<T>(Rows + matrix.Rows, Columns + matrix.Columns, result);
            }
            throw new NotImplementedException();
        }

        /// <summary>
        /// Calculates and returns the transpose of this matrix. The original matrix is unchanged.
        /// <!--returns-->
        /// </summary>
        /// <returns>The transpose of this matrix.</returns>
        public DenseMatrix<T> Transpose()
        {
            return new DenseMatrix<T>(_values.Transpose());
        }
        /// <summary>
        /// Calculates and returns the transpose of this matrix calculated in parallel. The original matrix is unchanged.
        /// </summary>
        /// <returns>The transpose of this matrix.</returns>
        public DenseMatrix<T> TransposeParallel()
        {
            return new DenseMatrix<T>(_values.TransposeParallel());
        }

        /// <summary>
        /// Swap the rows <txt>row1</txt> and <txt>row2</txt> of a matrix. Row indices start at 0.
        /// <h4>Example</h4>
        /// <pre><code class="cs">
        /// DenseMatrix&lt;int&gt; permutation = DenseMatrix&lt;int&gt;.Identity(3).SwapRows(1, 2);
        /// permutation.Print();
        /// // 1 0 0
        /// // 0 0 1
        /// // 0 1 0
        /// 
        /// </code></pre>
        /// </summary>
        /// <param name="row1">The index of the first row to swap.</param>
        /// <param name="row2">The index of the second row to swap.</param>
        public override void SwapRows(int row1, int row2)
        {
            if (row1 < 0 || row1 >= Rows) throw new ArgumentOutOfRangeException();
            if (row2 < 0 || row2 >= Rows) throw new ArgumentOutOfRangeException();

            T[] temp = _values[row1];
            _values[row1] = _values[row2];
            _values[row2] = temp;
        }

        /// <summary>
        /// Swap the columns <txt>column1</txt> and <txt>column2</txt> of a matrix. Column indices start at 0.
        /// </summary>
        /// <param name="column1">The index of the first column to swap.</param>
        /// <param name="column2">The index of the second column to swap.</param>
        public override void SwapColumns(int column1, int column2)
        {
            if (column1 < 0 || column1 >= Columns) throw new ArgumentOutOfRangeException();
            if (column2 < 0 || column2 >= Columns) throw new ArgumentOutOfRangeException();

            int rows = Rows, i;
            for (i = 0; i < rows; ++i)
            {
                T[] row = _values[i];
                T temp = row[column1];
                row[column1] = row[column2];
                row[column2] = temp;
            }
        }

        /// <summary>
        /// Permute the rows of a matrix $A$ from a permutation vector $v$, such that 
        /// $(PA)_i = A_{v_i}$ for $1\le{i}\le{n}$ where $P$ represents the row permutation matrix,
        /// and $A_j$ represents the $j$-th row of matrix $A$.
        /// 
        /// <!--inputs-->
        /// </summary>
        /// <param name="permutation">
        /// An <txt>int32</txt> array representing the permutation vector $v$, of dimension at least the number of rows of the matrix.<br/>
        /// The array must contain each of the integers $0, 1, ..., n - 1$ in its first $n$ entries, where $n$ is the number of rows of the matrix.
        /// </param>
        public void PermuteRows(int[] permutation)
        {
            if (permutation == null) throw new ArgumentNullException(nameof(permutation));
            if (!permutation.IsPermutation()) throw new InvalidOperationException(nameof(permutation));

            T[][] P = new T[Rows][];
            for (int i = 0; i < Rows; ++i)
            {
                P[i] = _values[permutation[i]];
            }
            _values = P;
        }
        /// <summary>
        /// Permute the rows of this matrix using a dense vector representing a permutation vector.
        /// </summary>
        /// <param name="permutation">The permutation vector.</param>
        public void PermuteRows(DenseVector<int> permutation) => PermuteRows(permutation.Values);
        /// <summary>
        /// Permute the rows of a $m\times n$ matrix $A$ using a $m\times m$ permutation matrix $P$, i.e. setting $A$ to $PA$.
        /// </summary>
        /// <param name="permutation">The permutation matrix $P$.</param>
        public void PermuteRows(PermutationMatrix permutation)
        {
            PermuteRows(permutation.PermutationVector);
        }

        /// <summary>
        /// Permute the columns of a matrix $A$ from a permutation vector $v$, such that 
        /// $(AP)_{:i} = A_{:v_i}$ for $1\le{i}\le{n}$ where $P$ represents the column permutation matrix,
        /// and $A_{:j}$ represents the $j$-th column of matrix $A$.
        /// 
        /// <!--inputs-->
        /// </summary>
        /// <param name="permutation">
        /// An <txt>int32</txt> array representing the permutation vector $v$, of dimension at least the number of columns of the matrix.<br/>
        /// The array must contain each of the integers $0, 1, ..., n - 1$ in its first $n$ entries, where $n$ is the number of columns of the matrix.
        /// </param>
        public void PermuteColumns(int[] permutation)
        {
            if (permutation == null) throw new ArgumentNullException(nameof(permutation));
            if (!permutation.IsPermutation()) throw new InvalidOperationException(nameof(permutation));

            int rows = Rows, cols = Columns, i, j;
            for (i = 0; i < rows; ++i)
            {
                T[] P_i = new T[Columns], values_i = _values[i];
                for (j = 0; j < cols; ++j) 
                {
                    P_i[permutation[j]] = values_i[j];
                }
                _values[i] = P_i;
            }
        }
        /// <summary>
        /// Permute the columns of this matrix using a dense vector representing a permutation vector.
        /// </summary>
        /// <param name="permutation">The dense vector representing a permutation.</param>
        public void PermuteColumns(DenseVector<int> permutation) => PermuteColumns(permutation.Values);
        /// <summary>
        /// Permute the columns of a $m\times n$ matrix $A$ using a $n\times n$ permutation matrix $P$, i.e. setting $A$ to $AP$.
        /// </summary>
        /// <param name="permutation">The permutation matrix $P$.</param>
        public void PermuteColumns(PermutationMatrix permutation)
        {
            PermuteColumns(permutation.PermutationVector);
        }

        /// <summary>
        /// Returns a new dense matrix with a specified row deleted. The original matrix is unchanged.
        /// <para>Also see: <a href="#RemoveRows"><txt>RemoveRows(IEnumerable&lt;int&gt; rows)</txt></a></para>
        /// <!--inputs-->
        /// </summary>
        /// <param name="row">The index of the row to remove.</param>
        /// <returns>A new matrix with the <txt>row</txt>-th row removed.</returns>
        public DenseMatrix<T> RemoveRow(int row)
        {
            if (row < 0 || row >= Rows)
            {
                throw new ArgumentOutOfRangeException();
            }

            T[][] newValues = new T[Rows - 1][];
            for (int i = 0, j = 0; i < Rows; ++i)
            {
                if (i != row) 
                {
                    newValues[j++] = _values[i].Copy();
                }
            }
            return new DenseMatrix<T>(newValues);
        }
        /// <summary>
        /// Returns a new dense matrix with every row in 'rows' deleted.
        /// </summary>
        /// <param name="rows">A collection of the row indices to be removed.</param>
        /// <returns>A new dense matrix with all rows in <txt>rows</txt> removed.</returns>
        public DenseMatrix<T> RemoveRows(IEnumerable<int> rows)
        {
            HashSet<int> uniqueRows = new HashSet<int>();
            foreach (int row in rows)
            {
                if (row < 0 || row > Rows)
                {
                    throw new ArgumentOutOfRangeException();
                }
                uniqueRows.Add(row);
            }

            int n = uniqueRows.Count;
            T[][] newValues = new T[Rows - n][];
            for (int i = 0, j = 0; i < Rows; ++i)
            {
                if (!uniqueRows.Contains(n))
                {
                    newValues[j++] = _values[i].Copy();
                }
            }
            return new DenseMatrix<T>(newValues);
        }

        /// <summary>
        /// Returns a new dense matrix with the column 'column' deleted.
        /// </summary>
        /// <param name="column">The index of the column to be removed.</param>
        /// <returns>A new matrix with <txt>column</txt>-th column removed.</returns>
        public DenseMatrix<T> RemoveColumn(int column)
        {
            if (column < 0 || column >= Columns)
            {
                throw new ArgumentOutOfRangeException();
            }

            int len = Columns - 1, i, j, k;
            T[][] newValues = new T[Rows][];
            for (i = 0; i < Rows; ++i)
            {
                T[] row = new T[len], values_i = _values[i];
                for (j = 0, k = 0; j < Columns; ++j)
                {
                    if (j != column)
                    {
                        row[k++] = values_i[j];
                    }
                }
                newValues[i] = row;
            }
            return new DenseMatrix<T>(newValues);
        }
        /// <summary>
        /// Returns a new dense matrix with all columns in 'columns' deleted.
        /// </summary>
        /// <param name="columns">The collection of the column indices to be removed.</param>
        /// <returns>A new dense matrix with all columns in <txt>columns</txt> removed.</returns>
        public DenseMatrix<T> RemoveColumns(IEnumerable<int> columns)
        {
            HashSet<int> uniqueColumns = new HashSet<int>();
            foreach (int column in columns)
            {
                if (column < 0 || column >= Columns)
                {
                    throw new ArgumentOutOfRangeException();
                }
                uniqueColumns.Add(column);
            }

            int len = Columns - 1, i, j, k;
            T[][] newValues = new T[Rows][];
            for (i = 0; i < Rows; ++i)
            {
                T[] row = new T[len], values_i = _values[i];
                for (j = 0, k = 0; j < Columns; ++j)
                {
                    if (!uniqueColumns.Contains(j))
                    {
                        row[k++] = values_i[j];
                    }
                }
                newValues[i] = row;
            }
            return new DenseMatrix<T>(newValues);
        }

        /// <summary>
        /// Insert a row to into matrix at <txt>index</txt>. A new matrix is created; the original matrix is unaltered.
        /// </summary>
        /// <param name="index">The row index at which to insert the new row.</param>
        /// <param name="row">The new row to be added.</param>
        public DenseMatrix<T> InsertRow(int index, T[] row)
        {
            if (index < 0 || index > Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(index));
            }

            if (row == null)
            {
                throw new ArgumentNullException(nameof(row));
            }
            if (row.Length != Columns)
            {
                throw new InvalidOperationException(nameof(row));
            }

            T[][] newValues = new T[Rows + 1][];
            for (int i = 0, j = 0; i < Rows; ++i)
            {
                if (i == index)
                {
                    newValues[j++] = row.Copy();
                }
                newValues[j++] = _values[i].Copy();
            }
            if (index == Rows)
            {
                newValues[index] = row.Copy();
            }

            return new DenseMatrix<T>(newValues);
        }
        public DenseMatrix<T> InsertRow(int index, DenseVector<T> row) => InsertRow(index, row.Values);

        /// <summary>
        /// Insert a collection of rows at <txt>index</txt>. A new matrix is created; the original matrix is unaltered.
        /// </summary>
        /// <param name="index">The row index at which to insert the new rows.</param>
        /// <param name="rows">The new rows to insert.</param>
        public DenseMatrix<T> InsertRows(int index, T[][] rows)
        {
            if (index < 0 || index > Rows) throw new ArgumentOutOfRangeException(nameof(index));
            if (rows == null) throw new ArgumentNullException(nameof(rows));

            int count = rows.Length, i, j, k;
            T[][] newValues = new T[Rows + count][];
            for (i = 0, j = 0; i < Rows; ++i)
            {
                if (i == index)
                {
                    for (k = 0; k < count; ++k)
                    {
                        newValues[j++] = rows[k].Copy();
                    }
                }
                newValues[j++] = _values[i].Copy();
            }
            if (index == Rows)
            {
                for (k = 0; k < count; ++k)
                {
                    newValues[j++] = rows[k].Copy();
                }
            }
            return new DenseMatrix<T>(newValues);
        }

        /// <summary>
        /// Insert a column into a matrix at <txt>index</txt>. A new matrix is created; the original matrix is unaltered.
        /// </summary>
        /// <param name="index"></param>
        /// <param column="row"></param>
        public DenseMatrix<T> InsertColumn(int index, T[] column)
        {
            if (index < 0 || index > Columns)
            {
                throw new ArgumentOutOfRangeException();
            }

            T[][] newValues = new T[Rows][];
            int cols = Columns + 1;
            for (int i = 0; i < Rows; ++i)
            {
                T[] row = new T[cols], values_i = _values[i];
                T column_i = column[i];

                for (int j = 0, k = 0; j < Columns; ++j)
                {
                    if (j == index)
                    {
                        row[k++] = column_i;
                    }
                    row[k++] = values_i[j];
                }
                if (index == Columns)
                {
                    row[index] = column_i;
                }

                newValues[i] = row;
            }
            return new DenseMatrix<T>(newValues);
        }
        public DenseMatrix<T> InsertColumn(int index, DenseVector<T> column) => InsertColumn(index, column.Values);

        /// <summary>
        /// Insert a collection of columns at <txt>index</txt>. A new matrix is created; the original matrix is unaltered.
        /// </summary>
        /// <param name="index"></param>
        /// <param name="columns"></param>
        public DenseMatrix<T> InsertColumns(int index, T[][] columns)
        {
            if (index < 0 || index > Columns)
            {
                throw new ArgumentOutOfRangeException();
            }

            T[][] newValues = new T[Rows][];
            int count = columns.Length, cols = Columns + count, i, j, k;
            for (i = 0; i < Rows; ++i)
            {
                T[] row = new T[cols], values_i = _values[i];
                for (j = 0, k = 0; j < Columns; ++j)
                {
                    if (j == index)
                    {
                        for (int c = 0; c < count; ++c)
                        {
                            row[k++] = columns[c][i];
                        }
                    }
                    row[k++] = values_i[j];
                }
                if (index == Columns)
                {
                    for (int c = 0; c < Columns; ++c)
                    {
                        row[k++] = columns[c][i];
                    }
                }

                newValues[i] = row;
            }
            return new DenseMatrix<T>(newValues);
        }

        /// <summary>
        /// Calculates and returns the transpose of a matrix over type <txt>T</txt>. The original matrix is unchanged.
        /// </summary>
        /// <param name="rowMajor"><b>Optional, </b> defaults to <txt>false</txt>.<br/> 
        /// Specifies whether the vectorization should traverse in a row-major or column-major manner.</param>
        /// <returns>
        /// The vectorized form of the matrix, as a <txt>DenseVector&lt;T&gt;</txt>.
        /// </returns>
        public DenseVector<T> Vectorize(bool rowMajor = true)
        {
            int rows = Rows, cols = Columns, len = rows * cols, i, j, k;
            T[] vect = new T[len];

            if (rowMajor)
            {
                k = 0;
                for (i = 0; i < rows; ++i)
                {
                    T[] row = _values[i];
                    for (j = 0; j < cols; ++j, ++k)
                    {
                        vect[k] = row[j];
                    }
                }
            }
            else
            {
                for (i = 0; i < rows; ++i)
                {
                    T[] row = _values[i];
                    for (j = 0, k = i; j < cols; ++j, k += rows)
                    {
                        vect[k] = row[j];
                    }
                }
            }

            return new DenseVector<T>(vect);
        }

        /// <summary>
        /// For a $m \times n$ matrix, and a row count $r > 0$, returns a matrix of size 
        /// $r \times \frac{mn}{r}$.
        /// </summary>
        /// <param name="rows">The number of rows of the reshaped matrix, $r$.</param>
        /// <param name="rowMajor">Specifies whether the reshaped matrix should be filled 
        /// in row major order.</param>
        /// <returns>The reshaped matrix.</returns>
        public DenseMatrix<T> Reshape(int rows, bool rowMajor = true)
        {
            if (rows <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(rows));
            }

            if ((Rows * Columns) % rows != 0)
            {
                throw new InvalidOperationException("The number of elements in this matrix is not divisible by the number of rows");
            }

            int columns = (Rows * Columns) / rows;
            DenseMatrix<T> matrix = new DenseMatrix<T>(rows, columns);
            T[][] values = matrix.Values;

            if (rowMajor)
            {
                int _c = 0, _r = 0;
                for (int r = 0; r < Rows; ++r)
                {
                    T[] row = _values[r];
                    for (int c = 0; c < Columns; ++c)
                    {
                        values[_r][_c] = row[c];

                        _r++;
                        if (_r % rows == 0)
                        {
                            _c++;
                            _r = 0;
                        }
                    }
                }
            }
            else
            {
                int _r = 0, _c = 0;
                for (int c = 0; c < Columns; ++c)
                {
                    for (int r = 0; r < Rows; ++r)
                    {
                        values[_r][_c] = _values[r][c];

                        _r++;
                        if (_r % rows == 0)
                        {
                            _c++;
                            _r = 0;
                        }
                    }
                }
            }
            return new DenseMatrix<T>(rows, columns, values);
        }

        #endregion


        #region Solvers and decompositions

        internal override bool SolveTriangular(T[] b, T[] x, IDenseBLAS1<T> blas, bool lower, bool transpose)
        {
            int dim = Math.Min(Rows, Columns);
            IProvider<T> provider = blas.Provider;
            T zero = new T();

            if (lower)
            {
                if (transpose)
                {
                    // Copy
                    Array.Copy(b, 0, x, 0, dim);

                    for (int c = dim - 1; c >= 0; --c)
                    {
                        T[] column = _values[c];
                        T diag = column[c];
                        if (provider.Equals(diag, zero))
                        {
                            return false;
                        }

                        T xc = provider.Divide(x[c], diag);
                        blas.AXPY(x, column, provider.Negate(xc), 0, c);
                        x[c] = xc;
                    }
                    return true;
                }
                else
                {
                    for (int r = 0; r < dim; ++r)
                    {
                        T[] row = _values[r];
                        T diag = row[r];
                        if (provider.Equals(diag, zero))
                        {
                            return false;
                        }
                        x[r] = provider.Divide(provider.Subtract(b[r], blas.DOT(x, row, 0, r)), diag);
                    }
                    return true;
                }
            }
            else
            {
                if (transpose)
                {
                    Array.Copy(b, 0, x, 0, dim);
                    for (int c = 0; c < dim; ++c)
                    {
                        T[] column = _values[c];
                        T diag = column[c];
                        if (provider.Equals(diag, zero))
                        {
                            return false;
                        }

                        T xc = provider.Divide(x[c], diag);
                        blas.AXPY(x, column, provider.Negate(xc), c + 1, dim);
                        x[c] = xc;
                    }
                    return true;
                }
                else
                {
                    for (int r = dim - 1; r >= 0; --r)
                    {
                        T[] row = _values[r];
                        T diag = row[r];
                        if (provider.Equals(diag, zero))
                        {
                            return false;
                        }
                        x[r] = provider.Divide(provider.Subtract(b[r], blas.DOT(x, row, r + 1, dim)), diag);
                    }
                    return true;
                }
            }
        }

        /// <summary>
        /// Solve Ax = b where x is defined as a subarray starting at index x_start, and b is defined as a subarray starting at b_start
        /// </summary>
        internal bool SolveTriangular(T[] b, T[] x, IDenseBLAS1<T> blas, bool lower, bool transpose, int b_start, int x_start, int dim)
        {
            IProvider<T> provider = blas.Provider;
            T zero = new T();

            if (lower)
            {
                if (transpose)
                {
                    // Copy
                    Array.Copy(b, b_start, x, x_start, dim);

                    for (int c = dim - 1; c >= 0; --c)
                    {
                        T[] column = _values[c];
                        T diag = column[c];
                        if (provider.Equals(diag, zero))
                        {
                            return false;
                        }

                        int index = c + x_start;
                        T xc = provider.Divide(x[index], diag);
                        blas.AXPY(x, column, provider.Negate(xc), 0, c);
                        x[index] = xc;
                    }
                    return true;
                }
                else
                {
                    for (int r = 0; r < dim; ++r)
                    {
                        T[] row = _values[r];
                        T diag = row[r];
                        if (provider.Equals(diag, zero))
                        {
                            return false;
                        }
                        x[r + x_start] = provider.Divide(provider.Subtract(b[r + b_start], blas.DOT(row, x, 0, r, x_start)), diag);
                    }
                    return true;
                }
            }
            else
            {
                if (transpose)
                {
                    Array.Copy(b, b_start, x, x_start, dim);
                    for (int c = 0; c < dim; ++c)
                    {
                        T[] column = _values[c];
                        T diag = column[c];
                        if (provider.Equals(diag, zero))
                        {
                            return false;
                        }

                        int index = c + x_start;
                        T xc = provider.Divide(x[index], diag);
                        blas.AXPY(x, column, provider.Negate(xc), index + 1, x_start + dim, -x_start);
                        x[index] = xc;
                    }
                    return true;
                }
                else
                {
                    for (int r = dim - 1; r >= 0; --r)
                    {
                        T[] row = _values[r];
                        T diag = row[r];
                        if (provider.Equals(diag, zero))
                        {
                            return false;
                        }
                        x[r + x_start] = provider.Divide(provider.Subtract(b[r + b_start], blas.DOT(row, x, r + 1, dim, x_start)), diag);
                    }
                    return true;
                }
            }
        }

        /// <summary>
        /// Solve LX = B, L'X = B, UX = B or U'X = B for matrices X, B in row major order.
        /// </summary>
        private bool SolveTriangular(T[][] B, T[][] X, IDenseBLAS1<T> blas, bool lower, bool transpose)
        {
            // TODO: change implementation to iterate in m-order if m < n

            int dim = Math.Min(Rows, Columns), m = Math.Min(X[0].Length, B[0].Length);
            IProvider<T> provider = blas.Provider;
            T zero = provider.Zero, one = provider.One;

            if (lower)
            {
                if (transpose)
                {
                    // Copy
                    X.CopyFrom(B, 0, 0, 0, 0, dim, m);

                    for (int c = dim - 1; c >= 0; --c)
                    {
                        T[] column = _values[c], Xc = X[c];
                        T diag = column[c];
                        if (provider.Equals(diag, zero))
                        {
                            return false;
                        }

                        blas.SCAL(Xc, provider.Divide(one, diag), 0, m);
                        for (int r = 0; r < c; ++r)
                        {
                            blas.AXPY(X[r], Xc, provider.Negate(column[r]), 0, m);
                        }
                    }
                    return true;
                }
                else
                {
                    X.CopyFrom(B, 0, 0, 0, 0, dim, m);

                    for (int r = 0; r < dim; ++r)
                    {
                        T[] row = _values[r], Xr = X[r];
                        for (int c = 0; c < r; ++c)
                        {
                            blas.AXPY(Xr, X[c], provider.Negate(row[c]), 0, m);
                        }

                        T diag = row[r];
                        if (provider.Equals(diag, zero))
                        {
                            return false;
                        }
                        blas.SCAL(Xr, provider.Divide(one, diag), 0, m);
                    }
                    return true;
                }
            }
            else
            {
                if (transpose)
                {
                    X.CopyFrom(B, 0, 0, 0, 0, dim, m);
                    for (int c = 0; c < dim; ++c)
                    {
                        T[] column = _values[c], Xc = X[c];

                        T diag = column[c];
                        if (provider.Equals(diag, zero))
                        {
                            return false;
                        }

                        blas.SCAL(Xc, provider.Divide(one, diag), 0, m);

                        for (int r = c + 1; r < dim; ++r)
                        {
                            blas.AXPY(X[r], Xc, provider.Negate(column[r]), 0, m);
                        }
                    }
                    return true;
                }
                else
                {
                    X.CopyFrom(B, 0, 0, 0, 0, dim, m);
                    for (int r = dim - 1; r >= 0; --r)
                    {
                        T[] row = _values[r], Xr = X[r];

                        for (int c = r + 1; c < dim; ++c)
                        {
                            blas.AXPY(Xr, X[c], provider.Negate(row[c]), 0, m);
                        }

                        T diag = row[r];
                        if (provider.Equals(diag, zero))
                        {
                            return false;
                        }
                        blas.SCAL(Xr, provider.Divide(one, diag), 0, m);
                    }
                    return true;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <remarks>
        /// If the dimensions of X and B do not match, only a submatrix equal to the smaller of each of the two dimensions 
        /// will be used.
        /// </remarks>
        /// <param name="B"></param>
        /// <param name="X"></param>
        /// <param name="blas"></param>
        /// <param name="lower"></param>
        /// <param name="transpose"></param>
        /// <returns></returns>
        public bool SolveTriangular(DenseMatrix<T> B, DenseMatrix<T> X, IDenseBLAS1Provider<T> blas, bool lower, bool transpose)
        {
            if (B is null)
            {
                throw new ArgumentNullException(nameof(B));
            }
            if (X is null)
            {
                throw new ArgumentNullException(nameof(X));
            }
            if (blas is null)
            {
                throw new ArgumentNullException(nameof(blas));
            }
            if (!(blas is IDenseBLAS1<T> b))
            {
                throw new NotSupportedException(ExceptionMessages.TypeNotSupported);
            }
            if (X.Rows < Math.Min(Rows, Columns))
            {
                throw new ArgumentOutOfRangeException(nameof(X), $"The number of rows of {nameof(X)} is not large enough.");
            }
            if (B.Rows < Math.Min(Rows, Columns))
            {
                throw new ArgumentOutOfRangeException(nameof(B), $"The number of rows of {nameof(B)} is not large enough.");
            }

            return SolveTriangular(B.Values, X.Values, b, lower, transpose);
        }

        /// <summary>
        /// Returns the lower-triangular Cholesky factor of this matrix, $L$, where $LL^* = A$. Only defined for 
        /// symmetric, positive-definite matrices. 
        /// </summary>
        /// <param name="blas">The level-1 BLAS implementation used for the Cholesky factorization.</param>
        /// <returns>The lower-triangular Cholesky factor.</returns>
        public DenseMatrix<T> CholeskyFactor(IDenseBLAS1Provider<T> blas)
        {
            if (blas is null)
            {
                throw new ArgumentNullException(nameof(blas));
            }
            if (!(blas is IDenseBLAS1<T> b))
            {
                throw new NotSupportedException(ExceptionMessages.TypeNotSupported);
            }
            if (Rows != Columns)
            {
                throw new InvalidOperationException(ExceptionMessages.MatrixNotSquare);
            }

            DenseMatrix<T> L = new DenseMatrix<T>(Rows, Rows);

            if (!CholeskyDecomposition.CholeskyDecompose(_values, L.Values, b, b.Provider.Sqrt))
            {
                throw new InvalidOperationException("Matrix is not positive definite.");
            }

            return L;
        }

        /// <summary>
        /// Calculates the Cholesky decomposition of this matrix using the specified dense Cholesky algorithm.
        /// </summary>
        /// <param name="algorithm">The Cholesky decomposition algorithm to use.</param>
        /// <param name="overwrite">Specifies whether this matrix should be overwritten with the 
        /// lower-triangular Cholesky factor $L$. Only the lower-triangular half will be modified.
        /// This method is more performant if set to <txt>true</txt>.
        /// </param>
        /// <returns></returns>
        public Cholesky<T> Cholesky(IDenseCholeskyAlgorithm<T> algorithm, bool overwrite = false)
        {
            if (algorithm is null) throw new ArgumentNullException(nameof(algorithm));
            return algorithm.Decompose(overwrite ? this : Copy());
        }

        /// <summary>
        /// Returns the Cholesky decomposition of this matrix, using the default Cholesky decomposition
        /// algorithm (up-looking Cholesky). 
        /// The returned <txt>Cholesky</txt> object contains the Cholesky factor $L$ and can be used for
        /// solving linear systems that have this matrix as its coefficient matrix. Only defined for 
        /// symmetric, positive definite matrices.
        /// </summary>
        /// <param name="blas">The level-1 BLAS implementation used for the Cholesky factorization.</param>
        /// <returns>The Cholesky factorization of this matrix.</returns>
        public Cholesky<T> Cholesky(IDenseBLAS1Provider<T> blas)
        {
            return new Cholesky<T>(CholeskyFactor(blas), blas as IDenseBLAS1<T>);
        }

        /// <summary>
        /// Returns the QR decomposition of this matrix.
        /// </summary>
        /// <param name="blas"></param>
        /// <returns></returns>
        public QR<T> QR(IDenseQRAlgorithm<T> algorithm, bool overwrite = false)
        {
            if (algorithm is null)
            {
                throw new ArgumentNullException(nameof(algorithm));
            }
            return algorithm.Decompose(this, overwrite);
        }

        /// <summary>
        /// Returns the QR decomposition of this matrix, using the default QR factorization 
        /// algorithm (Householder reflections). 
        /// </summary>
        /// <param name="overwrite">Specifies whether this matrix should be overwritten with 
        /// the upper-triangular factor $R$. This method is more performant if set to <txt>true</txt>.
        /// </param>
        /// <returns>The QR factorization of this matrix.</returns>
        public QR<T> QR(bool overwrite = false)
        {
            HouseholderDenseQR<T> algo = new HouseholderDenseQR<T>(false);
            return algo.Decompose(this, overwrite);
        }

        /// <summary>
        /// Returns the LU decomposition of this matrix, using a specified algorithm.
        /// </summary>
        /// <param name="algorithm">The LU decomposition algorithm to use.</param>
        /// <param name="overwrite">Whether to overwrite this matrix with the lower 
        /// and upper triangular factors. This method is more performant if set to 
        /// <txt>true</txt>.</param>
        /// <returns>The LU decomposition of this matrix.</returns>
        public LU<T> LU(IDenseLUAlgorithm<T> algorithm, bool overwrite = false)
        {
            return algorithm.Decompose(overwrite ? this : Copy());
        }

        /// <summary>
        /// Returns the LU decomposition of this matrix, using the default dense LU factorization
        /// algorithm (the Doolittle algorithm).
        /// </summary>
        /// <param name="blas1">The level-1 BLAS implementation to use.</param>
        /// <param name="overwrite">Whether to overwrite this matrix. Method is more 
        /// performant and memory conservative if set to <txt>true</txt>.</param>
        /// <returns></returns>
        public LU<T> LU(IDenseBLAS1Provider<T> blas1, bool overwrite = false)
        {
            if (blas1 is null)
            {
                throw new ArgumentNullException(nameof(blas1));
            }
            if (!(blas1 is IDenseBLAS1<T> b))
            {
                throw new NotSupportedException(nameof(b));
            }
            return LU(new DenseLUAlgorithm<T>(b), overwrite);
        }

        /// <summary>
        /// Returns the LU decomposition of this matrix, using the default BLAS implementation
        /// and the default LU decomposition algorithm (the Doolittle algorithm).
        /// </summary>
        /// <param name="overwrite">Whether to overwrite this matrix with the L and U factors. 
        /// This method is more performant if set to <txt>true</txt>.</param>
        /// <returns></returns>
        public LU<T> LU(bool overwrite = false)
        {
            return LU(new DenseLUAlgorithm<T>(ProviderFactory.GetDefaultBLAS1<T>()), overwrite);
        }

        #endregion


        #region Copy methods

        /// <summary>
        /// Copy the entries from a dense matrix src, onto this matrix, starting at position (srcFirstRow, srcFirstCol)
        /// and extending for (rows, cols). The entries will be copied onto location starting with (destFirstRow, destFirstCol).
        /// </summary>
        /// <param name="src"></param>
        /// <param name="srcFirstRow"></param>
        /// <param name="srcFirstCol"></param>
        /// <param name="destFirstRow"></param>
        /// <param name="destFirstCol"></param>
        /// <param name="rows"></param>
        /// <param name="cols"></param>
        public void CopyFrom(DenseMatrix<T> src, int srcFirstRow, int srcFirstCol, int destFirstRow, int destFirstCol, int rows, int cols)
        {
            _values.CopyFrom(src._values, srcFirstRow, srcFirstCol, destFirstRow, destFirstCol, rows, cols);
        }
        public void CopyFrom(DenseMatrix<T> src, int srcFirstRow, int srcFirstCol, int destFirstRow, int destFirstCol)
        {
            _values.CopyFrom(src._values, srcFirstRow, srcFirstCol, destFirstRow, destFirstCol);
        }
        public void CopyFrom(DenseMatrix<T> src)
        {
            _values.CopyFrom(src._values);
        }

        /// <summary>
        /// Deep clone this dense matrix, returning it as an object.
        /// </summary>
        /// <returns>A copy of this dense matrix.</returns>
        public override object Clone()
        {
            return Copy();
        }

        /// <summary>
        /// Returns a deep copy of a matrix.
        /// </summary>
        /// <returns>A copy of this dense matrix.</returns>
        public DenseMatrix<T> Copy()
        {
            int rows = Rows, cols = Columns, i, j;
            T[][] copy = new T[rows][];
            for (i = 0; i < rows; ++i)
            {
                T[] row = new T[cols], value_i = _values[i];
                for (j = 0; j < cols; ++j)
                {
                    row[j] = value_i[j];
                }
                copy[i] = row;
            }
            return new DenseMatrix<T>(copy);
        }
        public DenseMatrix<T> CopyParallel()
        {
            int rows = Rows, cols = Columns;
            T[][] copy = new T[rows][];

            Parallel.ForEach(Partitioner.Create(0, rows), range =>
            {
                int min = range.Item1, max = range.Item2, i, j;
                for (i = min; i < max; ++i)
                {
                    T[] row = new T[cols], value_i = _values[i];
                    for (j = 0; j < cols; ++j)
                    {
                        row[j] = value_i[j];
                    }
                    copy[i] = row;
                }
            });
            return new DenseMatrix<T>(copy);
        }

        #endregion


        /// <summary>
        /// Extract the middle bands of this matrix and return as a band matrix. The returned band matrix will always 
        /// contain the leading diagonal; however the number of upper and lower bandwidth need to be specified.
        /// </summary>
        /// <param name="lowerBandwidth">The number of upper diagonals to include.</param>
        /// <param name="upperBandwidth">The number of lower diagonals to include.</param>
        /// <returns>The band matrix.</returns>
        public BandMatrix<T> ToBandMatrix(int lowerBandwidth, int upperBandwidth)
        {
            if (lowerBandwidth < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(lowerBandwidth), ExceptionMessages.NegativeNotAllowed);
            }
            if (upperBandwidth < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(upperBandwidth), ExceptionMessages.NegativeNotAllowed);
            }

            if (lowerBandwidth > Rows - 1)
            {
                lowerBandwidth = Rows - 1;
            }
            if (upperBandwidth > Columns - 1)
            {
                upperBandwidth = Columns - 1;
            }

            // Construct the upper bands 
            T[][] upperBands = new T[upperBandwidth][];
            for (int b = 0; b < upperBandwidth; ++b)
            {
                int offset = b + 1;
                int len = Math.Min(Columns - offset, Rows);
                T[] diag = new T[len];
                for (int i = 0; i < len; ++i)
                {
                    diag[i] = _values[i][i + offset];
                }
                upperBands[b] = diag;
            }

            // Construct the lower bands
            T[][] lowerBands = new T[lowerBandwidth][];
            for (int b = 0; b < lowerBandwidth; ++b)
            {
                int offset = b + 1;
                int len = Math.Min(Columns, Rows - offset);
                T[] diag = new T[len];
                for (int i = 0; i < len; ++i)
                {
                    diag[i] = _values[i + offset][i];
                }
                lowerBands[b] = diag;
            }

            T[] diagonal = LeadingDiagonal();
            return new BandMatrix<T>(Rows, Columns, upperBands, diagonal, lowerBands);
        }

        public override BandMatrix<T> ToBandMatrix()
        {
            // Calculate the bandwidth
            int upperBandwidth = Columns - 1;
            int lowerBandwidth = Rows - 1;

            T zero = new T();
            for ( ; upperBandwidth >= 1; --upperBandwidth)
            {
                // Check to see if band is all zero
                bool isZero = true;
                for (int r = 0; r < Rows; ++r)
                {
                    int c = r + upperBandwidth;
                    if (c >= Columns)
                    {
                        break;
                    }
                    if (!_values[r][c].Equals(zero))
                    {
                        isZero = false;
                        break;
                    }
                }
                if (!isZero)
                {
                    break; // bandwidth found
                }
            }
            for (; lowerBandwidth >= 1; --lowerBandwidth)
            {
                bool isZero = true;
                for (int c = 0; c < Columns; ++c)
                {
                    int r = c + lowerBandwidth;
                    if (r >= Rows)
                    {
                        break;
                    }
                    if (!_values[r][c].Equals(zero))
                    {
                        isZero = false;
                        break;
                    }
                }
                if (!isZero)
                {
                    break;
                }
            }

            return ToBandMatrix(lowerBandwidth, upperBandwidth);
        }

        public override BlockDiagonalMatrix<T> ToBlockDiagonalMatrix()
        {
            return new BlockDiagonalMatrix<T>(this);
        }

        /// <summary>
        /// Retuns this matrix as a sparse matrix in Coordinate List (COO) form.
        /// </summary>
        /// <returns>The COO sparse matrix.</returns>
        public override COOSparseMatrix<T> ToCOOSparseMatrix()
        {
            return new COOSparseMatrix<T>(this);
        }

        /// <summary>
        /// Returns this matrix as a sparse matrix in Compressed Sparse Row (CSR) form.
        /// </summary>
        /// <returns>The CSR sparse matrix.</returns>
        public override CSRSparseMatrix<T> ToCSRSparseMatrix()
        {
            return new CSRSparseMatrix<T>(this);
        }

        /// <summary>
        /// Returns this matrix as a sparse matrix in Compressed Sparse Column (CSC) format.
        /// </summary>
        /// <returns>A CSC sparse matrix.</returns>
        public override CSCSparseMatrix<T> ToCSCSparseMatrix()
        {
            return new CSCSparseMatrix<T>(this);
        }

        public override DenseMatrix<T> ToDenseMatrix()
        {
            return this;
        }

        /// <summary>
        /// Extract the leading diagonal of this matrix and return it as a diagonal matrix.
        /// </summary>
        /// <returns>The diagonal matrix whose diagonal elements are the same as this matrix.</returns>
        public override DiagonalMatrix<T> ToDiagonalMatrix()
        {
            return new DiagonalMatrix<T>(LeadingDiagonal());
        }

        /// <summary>
        /// Return this matrix as a Dictionary of Keys (DOK) sparse matrix.
        /// </summary>
        /// <returns>The DOK sparse matrix representation of this matrix.</returns>
        public override DOKSparseMatrix<T> ToDOKSparseMatrix()
        {
            return new DOKSparseMatrix<T>(this);
        }

        public override MCSRSparseMatrix<T> ToMCSRSparseMatrix()
        {
            return new MCSRSparseMatrix<T>(this);
        }

        /// <summary>
        /// Returns this matrix as a symmetric sparse matrix stored in Skyline form.
        /// </summary>
        /// <returns>The SKY sparse matrix.</returns>
        public override SKYSparseMatrix<T> ToSKYSparseMatrix()
        {
            return new SKYSparseMatrix<T>(this, SKYType.SYMMETRIC);
        }

        #endregion


        #region Arithmetic operations 

        public override Matrix<T> Add(Matrix<T> matrix, IProvider<T> provider)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            if (CompareRepresentationPriority(this, matrix) < 0)
            {
                return matrix.Add(this, provider); // return the sum as type(matrix)
            }

            if (ProviderFactory.TryGetDefaultBLAS1Provider(out IDenseBLAS1<T> blas))
            {
                return Add(matrix.ToDenseMatrix(), blas);
            }
            else
            {
                return Add(matrix.ToDenseMatrix(), new DefaultNativeProvider<T>(provider));
            }
        }

        /// <summary>
        /// Add this matrix to another matrix using a level-1 BLAS implementation, returning the result 
        /// as another matrix without changing either matrix.
        /// </summary>
        /// <param name="B">A matrix with the same dimension as this matrix.</param>
        /// <param name="blas">A level-1 BLAS implementation.</param>
        /// <returns></returns>
        public DenseMatrix<T> Add(DenseMatrix<T> B, IDenseBLAS1Provider<T> blas)
        {
            DenseMatrix<T> sum = Copy();
            sum.AddInPlace(B, blas);
            return sum;
        }

        /// <summary>
        /// Returns the sum of this matrix with another dense matrix of the same size, given a level-2 dense BLAS provider. Neither matrix is changed by this method.
        /// </summary>
        /// <param name="B">The second matrix to be added.</param>
        /// <param name="provider">The level-2 BLAS implementation.</param>
        /// <returns>The matrix sum.</returns>
        public DenseMatrix<T> Add(DenseMatrix<T> B, IDenseBLAS2Provider<T> provider)
        {
            if (B == null) throw new ArgumentNullException(nameof(B));
            if (Rows != B.Rows || Columns != B.Columns) throw new InvalidOperationException("Matrices are not the same size");

            if (provider == null) throw new ArgumentNullException(nameof(provider));
            if (!(provider is IDenseBLAS2<T> p)) throw new NotSupportedException("Provider is not supported");

            DenseMatrix<T> C = new DenseMatrix<T>(Rows, Columns);
            p.Add(Values, B.Values, C.Values, 0, 0, 0, 0, Rows, Columns);
            return C;
        }

        /// <summary>
        /// Add this matrix to another matrix using a level-1 BLAS implementation, and overwrite this matrix with the sum.
        /// </summary>
        /// <param name="B">A matrix of the same dimensions as this matrix.</param>
        /// <param name="blas1">A level-1 BLAS provider.</param>
        /// <returns></returns>
        public void AddInPlace(DenseMatrix<T> B, IDenseBLAS1Provider<T> blas1)
        {
            if (B is null)
            {
                throw new ArgumentNullException(nameof(B));
            }
            if (Rows != B.Rows || Columns != B.Columns)
            {
                throw new InvalidOperationException(ExceptionMessages.MatrixSize);
            }
            if (blas1 is null)
            {
                throw new ArgumentNullException(nameof(blas1));
            }
            if (!(blas1 is IDenseBLAS1<T> p))
            {
                throw new NotSupportedException(ExceptionMessages.TypeNotSupported);
            }

            int r = Rows, c = Columns, i;
            for (i = 0; i < r; ++i)
            {
                p.ADD(_values[i], B._values[i], _values[i], 0, c);
            }
        }

        /// <summary>
        /// Add this matrix to another matrix using a level-2 BLAS implementation, overwriting this 
        /// matrix with the sum. This method is equivalent to <txt>Increment</txt>, it is duplicated
        /// here for consistency.
        /// </summary>
        /// <param name="B">A dense matrix of the same dimension as this matrix.</param>
        /// <param name="blas2">A level-2 BLAS implementation.</param>
        public void AddInPlace(DenseMatrix<T> B, IDenseBLAS2Provider<T> blas2)
        {
            Increment(B, blas2);
        }

        /// <summary>
        /// Returns the additive inverse of this matrix, $-A$, given a level-2 BLAS implementation. 
        /// This method is identical to the <txt>Negate</txt> method, and is re-implemented here 
        /// for consistency with the naming convention of other rings.
        /// </summary>
        /// <param name="provider">A level-2 BLAS provider.</param>
        /// <returns></returns>
        public DenseMatrix<T> AdditiveInverse(IDenseBLAS2Provider<T> provider)
        {
            return Negate(provider);
        }

        /// <summary>
        /// Decrement this matrix by another dense matrix of the same size, given a level-2 dense BLAS provider.
        /// </summary>
        /// <param name="B">A dense matrix.</param>
        /// <param name="provider">A level-2 BLAS implementation.</param>
        public void Decrement(DenseMatrix<T> B, IDenseBLAS2Provider<T> provider)
        {
            if (B is null) throw new ArgumentNullException(nameof(B));
            if (Rows != B.Rows || Columns != B.Columns) throw new InvalidOperationException("Matrices are not the same size.");

            if (provider is null) throw new ArgumentNullException(nameof(provider));
            if (!(provider is IDenseBLAS2<T> p)) throw new NotSupportedException("Provider is not supported");

            p.Decrement(_values, B.Values, Rows, Columns);
        }

        /// <summary>
        /// Increment this matrix by another dense matrix of the same size, given a level-2 dense BLAS provider.
        /// </summary>
        /// <param name="B">A dense matrix.</param>
        /// <param name="provider">A level-2 BLAS implementation.</param>
        public void Increment(DenseMatrix<T> B, IDenseBLAS2Provider<T> provider)
        {
            if (B == null) throw new ArgumentNullException(nameof(B));
            if (Rows != B.Rows || Columns != B.Columns) throw new InvalidOperationException("Matrices are not the same size.");

            if (provider == null) throw new ArgumentNullException(nameof(provider));
            if (!(provider is IDenseBLAS2<T> p)) throw new NotSupportedException("Provider is not supported");

            p.Increment(_values, B.Values, Rows, Columns);
        }

        /// <summary>
        /// Divide this matrix by a scalar, using the specified level-1 BLAS implementation. This 
        /// method creates a new matrix, and this matrix will not be modified.
        /// </summary>
        /// <param name="scalar">The scalar to divide by.</param>
        /// <param name="blas1">The level-1 BLAS implementation.</param>
        /// <returns>A matrix-scalar division result.</returns>
        public DenseMatrix<T> Divide(T scalar, IDenseBLAS1Provider<T> blas1)
        {
            DenseMatrix<T> copy = Copy();
            copy.DivideInPlace(scalar, blas1);
            return copy;
        }

        /// <summary>
        /// Divide this matrix by a scalar, then overwrite this matrix with the result.s
        /// </summary>
        /// <param name="scalar">The scalar to divide by.</param>
        /// <param name="blas1">A level-1 BLAS implementation.</param>
        public void DivideInPlace(T scalar, IDenseBLAS1Provider<T> blas1)
        {
            if (blas1 is null)
            {
                throw new ArgumentNullException(nameof(blas1));
            }
            if (!(blas1 is IDenseBLAS1<T> p))
            {
                throw new NotSupportedException(ExceptionMessages.TypeNotSupported);
            }
            if (scalar is null)
            {
                throw new ArgumentNullException(nameof(scalar));
            }
            MultiplyInPlace(p.Provider.Divide(p.Provider.One, scalar), blas1);
        }

        /// <summary>
        /// Divide this matrix by a scalar, using a provider for elementwise division.
        /// </summary>
        /// <param name="scalar">The scalar to divide by.</param>
        /// <param name="provider">The provider used for elementwise division.</param>
        /// <returns></returns>
        public DenseMatrix<T> Divide(T scalar, IProvider<T> provider)
        {
            DenseMatrix<T> copy = Copy();
            copy.DivideInPlace(scalar, provider);
            return copy;
        }

        /// <summary>
        /// Postmultiply this matrix by another dense matrix of compatible dimension, given a matrix multiplication algorithm.
        /// Neither matrix will be altered by this method.
        /// </summary>
        /// <param name="B">A dense matrix to be postmultiplied by.</param>
        /// <param name="algo">A matrix multiplication algorithm.</param>
        /// <returns>The matrix product calculated using the multiplication algorithm.</returns>
        public DenseMatrix<T> Multiply(DenseMatrix<T> B, IMatrixMultiplication<T> algo)
        {
            if (B == null) throw new ArgumentNullException(nameof(B));
            if (Columns != B.Rows) throw new InvalidOperationException("The number of columns of the first matrix does not match the number of columns of the second matrix.");

            DenseMatrix<T> C = new DenseMatrix<T>(Rows, B.Columns);
            algo.Multiply(_values, B._values, C._values, new Size3(Rows, B.Rows, B.Columns), false);
            return C;
        }

        /// <summary>
        /// Postmultiply this matrix by another dense matrix, storing the result in <txt>result</txt>.
        /// </summary>
        /// <param name="B">The matrix to postmultiply by.</param>
        /// <param name="result">The matrix that will contain the matrix product.</param>
        /// <param name="algo">The matrix multiplication algorithm to use.</param>
        public void Multiply(DenseMatrix<T> B, DenseMatrix<T> result, IMatrixMultiplication<T> algo)
        {
            if (B is null)
            {
                throw new ArgumentNullException(nameof(B));
            }
            if (result is null)
            {
                throw new ArgumentNullException(nameof(result));
            }
            if (Columns != B.Rows)
            {
                throw new InvalidOperationException("The number of columns of the first matrix does not match the number of columns of the second matrix.");
            }
            if (Rows > result.Rows || B.Columns > result.Columns)
            {
                throw new ArgumentOutOfRangeException("The result matrix is not large enough to store the matrix product.");
            }

            algo.Multiply(_values, B.Values, result.Values, new Size3(Rows, B.Rows, B.Columns), false);
        }

        internal override void Multiply(T[] vector, T[] product, IProvider<T> provider, bool increment)
        {
            if (vector is null)
            {
                throw new ArgumentNullException(nameof(vector));
            }
            if (product is null)
            {
                throw new ArgumentNullException(nameof(product));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }

            if (vector.Length < Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(vector), $"The number of columns of this matrix is greater than the dimension of '{nameof(vector)}'.");
            }
            if (product.Length < Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(product), $"'{nameof(product)}' is not large enough to store the matrix-vector product.");
            }

            T zero = provider.Zero;
            int i, j;

            if (increment)
            {
                for (i = 0; i < Rows; ++i)
                {
                    T[] row = _values[i];
                    T sum = zero;
                    for (j = 0; j < Columns; ++j)
                    {
                        sum = provider.Add(sum, provider.Multiply(row[j], vector[j]));
                    }
                    product[i] = provider.Add(product[i], sum);
                }
            }
            else
            {
                for (i = 0; i < Rows; ++i)
                {
                    T[] row = _values[i];
                    T sum = zero;
                    for (j = 0; j < Columns; ++j)
                    {
                        sum = provider.Add(sum, provider.Multiply(row[j], vector[j]));
                    }
                    product[i] = sum;
                }
            }
        }

        internal override void TransposeAndMultiply(T[] vector, T[] product, IProvider<T> provider, bool increment)
        {
            if (vector is null)
            {
                throw new ArgumentNullException(nameof(vector));
            }
            if (product is null)
            {
                throw new ArgumentNullException(nameof(product));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            if (vector.Length < Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(vector), $"The number of rows of this matrix is greater than the dimension of '{nameof(vector)}'.");
            }
            if (product.Length < Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(product), $"'{nameof(product)}' is not large enough to store the matrix-vector product.");
            }

            T zero = provider.Zero;

            if (!increment)
            {
                if (zero.Equals(default))
                {
                    Array.Clear(product, 0, Columns);
                }
                else
                {
                    for (int c = 0; c < Columns; ++c)
                    {
                        product[c] = zero;
                    }
                }
            }

            int i, j;
            for (i = 0; i < Rows; ++i)
            {
                T[] row = _values[i];
                T vect = vector[i];

                // This is actually just a AXPY
                for (j = 0; j < Columns; ++j)
                {
                    product[j] = provider.Add(product[j], provider.Multiply(row[j], vect));
                }
            }
        }

        /// <summary>
        /// Postmultiply this matrix by a dense vector of compatible dimension, given a level-1 BLAS implementation. Neither this matrix nor 
        /// the vector will be changed by this method.
        /// </summary>
        /// <param name="x">A dense vector of the same dimension as the number of columns of this matrix.</param>
        /// <param name="provider">A level-1 BLAS implementation.</param>
        /// <returns>The matrix-vector product as a dense vector.</returns>
        public DenseVector<T> Multiply(DenseVector<T> x, IDenseBLAS1Provider<T> provider)
        {
            if (x == null) throw new ArgumentNullException(nameof(x));
            if (Columns != x.Dimension) throw new InvalidOperationException("The number of columns of the matrix does not match the dimensionality of the vector.");

            if (provider == null) throw new ArgumentNullException(nameof(provider));
            if (!(provider is IDenseBLAS1<T> p)) throw new NotSupportedException("Provider is not supported");

            DenseVector<T> Ax = new DenseVector<T>(Rows);
            T[] vect_values = x.Values;
            for (int i = 0; i < Rows; ++i)
            {
                Ax[i] = p.DOT(_values[i], vect_values, 0, Columns);
            }
            return Ax;
        }

        public override SparseVector<T> Multiply(SparseVector<T> vector, IProvider<T> provider)
        {
            // Fake sparse vector, since a dense-sparse matrix-vector multiply is dense
            return new SparseVector<T>(MultiplyAsDense(vector, provider));
        }

        public override SparseVector<T> TransposeAndMultiply(SparseVector<T> vector, IProvider<T> provider)
        {
            if (vector is null)
            {
                throw new ArgumentNullException(nameof(vector));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            if (vector.Dimension < Rows)
            {
                throw new InvalidOperationException("The vector's dimension is less than the number of rows of the matrix.");
            }

            int[] vect_indices = vector.Indices;
            T[] vect_values = vector.Values;

            // Trivial case first - dense BLAS is inefficient for this case so we 
            // need to check first
            if (vect_values.Length == 0)
            {
                return new SparseVector<T>(Columns);
            }

            // Check if we can exploit dense BLAS
            if (ProviderFactory.TryGetDefaultBLAS1Provider(out IDenseBLAS1<T> blas))
            {
                return new SparseVector<T>(TransposeAndMultiplyAsDense(vector, blas));
            }

            // Otherwise - stick with provider
            T[] prod = new T[Columns];
            T[] col = _values[vect_indices[0]];
            T v = vect_values[0];
            for (int j = 0; j < Columns; ++j)
            {
                prod[j] = provider.Multiply(col[j], v);
            }

            for (int i = 1; i < vect_indices.Length; ++i)
            {
                T[] column = _values[vect_indices[i]];
                v = vect_values[i];
                for (int j = 0; j < Columns; ++j)
                {
                    prod[j] = provider.Add(prod[j], provider.Multiply(column[j], v));
                }
            }
            return new SparseVector<T>(prod);
        }

        /// <summary>
        /// Postmultiply this matrix by a sparse vector, returning the product as a dense vector.
        /// </summary>
        /// <param name="x">The sparse vector to postmultiply by.</param>
        /// <param name="provider">The provider used for elementwise arithmetic.</param>
        /// <returns>The matrix-vector product.</returns>
        public DenseVector<T> MultiplyAsDense(SparseVector<T> x, IProvider<T> provider)
        {
            if (x is null)
            {
                throw new ArgumentNullException(nameof(x));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            if (x.Dimension != Columns)
            {
                throw new InvalidOperationException("The number of columns of the matrix does not match the vector's dimension.");
            }

            int[] vect_indices = x.Indices;
            T[] vect_values = x.Values;
            T[] product = new T[Rows];

            int len = vect_indices.Length, r, i;
            for (r = 0; r < Rows; ++r)
            {
                T[] row = _values[r];
                T sum = provider.Zero;
                for (i = 0; i < len; ++i)
                {
                    sum = provider.Add(sum, provider.Multiply(row[vect_indices[i]], vect_values[i]));
                }
                product[r] = sum;
            }
            return new DenseVector<T>(product);
        }

        public DenseVector<T> TransposeAndMultiplyAsDense(SparseVector<T> vector, IDenseBLAS1Provider<T> provider)
        {
            if (vector is null)
            {
                throw new ArgumentNullException(nameof(vector));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            if (vector.Dimension < Rows)
            {
                throw new InvalidOperationException("The vector's dimension is less than the number of rows of the matrix.");
            }
            if (!(provider is IDenseBLAS1<T> b))
            {
                throw new NotSupportedException(ExceptionMessages.TypeNotSupported);
            }

            int[] vect_indices = vector.Indices;
            T[] vect_values = vector.Values;

            if (vect_values.Length == 0)
            {
                return new DenseVector<T>(Columns);
            }

            T[] prod = _values[vect_indices[0]].Copy();
            b.SCAL(prod, vect_values[0], 0, Columns);

            for (int i = 1; i < vect_indices.Length; ++i)
            {
                b.AXPY(prod, _values[vect_indices[i]], vect_values[i], 0, Columns);
            }
            return new DenseVector<T>(prod);
        }

        /// <summary>
        /// Returns the product between this matrix and a scalar, given a provider for type T.
        /// </summary>
        /// <param name="x">The scalar value.</param>
        /// <param name="provider">A provider for type T.</param>
        /// <returns>The matrix-scalar product.</returns>
        public DenseMatrix<T> Multiply(T x, IProvider<T> provider)
        {
            DenseMatrix<T> copy = Copy();
            copy.MultiplyInPlace(x, provider);
            return copy;
        }

        /// <summary>
        /// Returns the product between this matrix and a scalar, given a level-1 BLAS implementation. This matrix will be unchanged. 
        /// </summary>
        /// <param name="x">A scalar value.</param>
        /// <param name="provider">A level-1 BLAS implementation.</param>
        /// <returns>The matrix-scalar product, as a new dense matrix.</returns>
        public DenseMatrix<T> Multiply(T x, IDenseBLAS1Provider<T> provider)
        {
            if (provider == null) throw new ArgumentNullException(nameof(provider));
            if (!(provider is IDenseBLAS1<T> p)) throw new NotSupportedException("Provider is not supported");

            DenseMatrix<T> copy = Copy();
            for (int i = 0; i < Rows; ++i)
            {
                p.SCAL(copy._values[i], x, 0, Columns);
            }
            return copy;
        }

        /// <summary>
        /// Multiplies this matrix by a scalar and overwrite this matrix with the result.
        /// </summary>
        /// <param name="x">A scalar value.</param>
        /// <param name="provider">A provider for type <txt>T</txt>.</param>
        public override void MultiplyInPlace(T x, IProvider<T> provider)
        {
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }

            int rows = Rows, cols = Columns, i, j;
            for (i = 0; i < rows; ++i)
            {
                T[] row = _values[i];
                for (j = 0; j < cols; ++j)
                {
                    row[j] = provider.Multiply(row[j], x);
                }
            }
        }

        /// <summary>
        /// Multiply this matrix by a scalar (using the specified level-1 BLAS provider) and overwrite this matrix with the result.
        /// </summary>
        /// <param name="x">The scalar value to multiply by.</param>
        /// <param name="provider">A level-1 BLAS provider.</param>
        public void MultiplyInPlace(T x, IDenseBLAS1Provider<T> provider)
        {
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            if (!(provider is IDenseBLAS1<T> p))
            {
                throw new NotSupportedException(ExceptionMessages.TypeNotSupported);
            }
            for (int i = 0; i < Rows; ++i)
            {
                p.SCAL(_values[i], x, 0, Columns);
            }
        }

        /// <summary>
        /// Postmultiply a $m \times n$ matrix $A$ by a $n \times n$ column permutation matrix $P$, returning the matrix product $AP$. The matrix $A$ is unchanged.
        /// </summary>
        /// <param name="permutation">The permutation matrix $P$.</param>
        public DenseMatrix<T> Multiply(PermutationMatrix permutation)
        {
            DenseMatrix<T> copy = Copy();
            copy.PermuteColumns(permutation);
            return copy;
        }

        /// <summary>
        /// Postmultiply this matrix by a permutation matrix of compatible dimension, then overwrite this 
        /// matrix with the product.
        /// </summary>
        /// <param name="permutation">The permutation matrix.</param>
        public void MultiplyInPlace(PermutationMatrix permutation)
        {
            PermuteColumns(permutation);
        }

        /// <summary>
        /// Returns the negation (additive inverse) of this matrix, $-A$. This method is 
        /// identical to the <txt>AdditiveInverse</txt> method
        /// </summary>
        /// <param name="provider">A level-2 BLAS implementation.</param>
        /// <returns>The negation of this matrix.</returns>
        public DenseMatrix<T> Negate(IDenseBLAS2Provider<T> provider)
        {
            DenseMatrix<T> copy = Copy();
            copy.NegateInPlace(provider);
            return copy;
        }

        /// <summary>
        /// Overwrites this matrix with its negation, $-A$, given a level-2 BLAS implementation.
        /// </summary>
        /// <param name="provider">A level-2 BLAS implementation.</param>
        public void NegateInPlace(IDenseBLAS2Provider<T> provider)
        {
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }

            if (!(provider is IDenseBLAS2<T> blas))
            {
                throw new NotSupportedException(ExceptionMessages.TypeNotSupported);
            }

            blas.Negate(_values, _values, 0, 0, 0, 0, Rows, Columns);
        }

        /// <summary>
        /// Returns the matrix difference between this matrix and anotherm matrix of the same dimensions, 
        /// using a level-1 BLAS implementation. 
        /// </summary>
        /// <param name="B">The dense matrix to subtract.</param>
        /// <param name="blas1">A level-1 BLAS implementation.</param>
        /// <returns>This matrix subtract another matrix.</returns>
        public DenseMatrix<T> Subtract(DenseMatrix<T> B, IDenseBLAS1Provider<T> blas1)
        {
            if (B is null) throw new ArgumentNullException(nameof(B));
            if (B.Rows != Rows || B.Columns != Columns) throw new InvalidOperationException(ExceptionMessages.MatrixSize);
            if (blas1 is null) throw new ArgumentNullException(nameof(blas1));
            if (!(blas1 is IDenseBLAS1<T> blas)) throw new NotSupportedException(ExceptionMessages.TypeNotSupported);

            int r = Rows, c = Columns, i;
            T[][] C = MatrixInternalExtensions.JMatrix<T>(Rows, Columns);
            for (i = 0; i < r; ++i)
            {
                blas.SUB(_values[i], B._values[i], C[i], 0, c);
            }
            return new DenseMatrix<T>(r, c, C);
        }

        /// <summary>
        /// Returns this matrix subtract another dense matrix of the same size, given a level-2 BLAS provider. Neither this matrix 
        /// or the second matrix will be changed by this method.
        /// </summary>
        /// <param name="B">A dense matrix.</param>
        /// <param name="provider">A level-2 BLAS implementation.</param>
        /// <returns>This matrix subtract another matrix.</returns>
        public DenseMatrix<T> Subtract(DenseMatrix<T> B, IDenseBLAS2Provider<T> provider)
        {
            if (B == null) throw new ArgumentNullException(nameof(B));
            if (Rows != B.Rows || Columns != B.Columns) throw new InvalidOperationException("Matrices are not the same size");

            if (provider == null) throw new ArgumentNullException(nameof(provider));
            if (!(provider is IDenseBLAS2<T> p)) throw new NotSupportedException("Provider is not supported");

            DenseMatrix<T> C = new DenseMatrix<T>(Rows, Columns);
            p.Subtract(Values, B.Values, C.Values, 0, 0, 0, 0, Rows, Columns);
            return C;
        }

        /// <summary>
        /// Subtract another matrix from this matrix using a level-1 BLAS implementation, and overwrite this matrix with the result.
        /// </summary>
        /// <param name="B">The matrix to subtract.</param>
        /// <param name="blas1">A level-1 BLAS implementation.</param>
        public void SubtractInPlace(DenseMatrix<T> B, IDenseBLAS1Provider<T> blas1)
        {
            if (B is null) throw new ArgumentNullException(nameof(B));
            MatrixChecks.AssertMatrixDimensionsEqual(this, B);
            if (blas1 is null) throw new ArgumentNullException(nameof(blas1));
            if (!(blas1 is IDenseBLAS1<T> blas)) throw new NotSupportedException(ExceptionMessages.TypeNotSupported);

            int r = Rows, c = Columns, i;
            for (i = 0; i < r; ++i)
            {
                blas.SUB(_values[i], B._values[i], _values[i], 0, c);
            }
        }

        /// <summary>
        /// Subtract a matrix from this matrix, using a level-2 BLAS implementation, and overwrite this 
        /// matrix with the result. This method is equivalent to <txt>Decrement</txt>, and is only 
        /// included for consistency.
        /// </summary>
        /// <param name="B">The dense matrix to subtract.</param>
        /// <param name="blas2">A level-2 BLAS implementation.</param>
        public void SubtractInPlace(DenseMatrix<T> B, IDenseBLAS2Provider<T> blas2)
        {
            Decrement(B, blas2);
        }

        #endregion


        #region Operators 

        /// <summary>
        /// Returns the matrix product of two dense matrices, using the default level-3 BLAS provider.
        /// </summary>
        /// <param name="A">The left matrix.</param>
        /// <param name="B">The right matrix.</param>
        /// <returns>The matrix product.</returns>
        public static DenseMatrix<T> operator *(DenseMatrix<T> A, DenseMatrix<T> B)
        {
            if (A is null) throw new ArgumentNullException(nameof(A));
            return A.Multiply(B, new NaiveMatrixMultiplication<T>(ProviderFactory.GetDefaultBLAS1<T>()));
        }

        /// <summary>
        /// Returns the matrix vector product between a dense matrix and a dense vector, using the default level-1 BLAS provider.
        /// </summary>
        /// <param name="A">The dense matrix.</param>
        /// <param name="x">The dense vector.</param>
        /// <returns>The matrix-vector product as a <txt>DenseVector</txt>.</returns>
        public static DenseVector<T> operator *(DenseMatrix<T> A, DenseVector<T> x)
        {
            if (A is null) throw new ArgumentNullException(nameof(A));
            return A.Multiply(x, ProviderFactory.GetDefaultBLAS1<T>());
        }

        /// <summary>
        /// Returns a dense matrix postmultiplied by a sparse vector.
        /// </summary>
        /// <param name="A">The dense matrix.</param>
        /// <param name="x">The sparse vector.</param>
        /// <returns>The matrix-vector product, as a dense vector.</returns>
        public static DenseVector<T> operator *(DenseMatrix<T> A, SparseVector<T> x)
        {
            if (A is null)
            {
                throw new ArgumentNullException(nameof(A));
            }
            return A.MultiplyAsDense(x, ProviderFactory.GetDefaultProvider<T>());
        }

        /// <summary>
        /// Returns the matrix-scalar product between a dense matrix and a scalar, using the default level-1 BLAS provider. 
        /// </summary>
        /// <param name="A">The dense matrix.</param>
        /// <param name="scalar">The scalar value.</param>
        /// <returns>The matrix-scalar product.</returns>
        public static DenseMatrix<T> operator *(DenseMatrix<T> A, T scalar)
        {
            if (A is null) throw new ArgumentNullException(nameof(A));
            return A.Multiply(scalar, ProviderFactory.GetDefaultBLAS1<T>());
        }

        /// <summary>
        /// Returns the matrix-scalar product between a dense matrix and a scalar value.
        /// </summary>
        /// <param name="scalar">The scalar value.</param>
        /// <param name="A">The dense matrix.</param>
        /// <returns>The matrix-scalar product.</returns>
        public static DenseMatrix<T> operator *(T scalar, DenseMatrix<T> A) => A * scalar;

        /// <summary>
        /// Divides a matrix by a scalar, returning the result.
        /// </summary>
        /// <param name="A">The matrix.</param>
        /// <param name="scalar">The scalar to divide by.</param>
        /// <returns>The result of the matrix-scalar division.</returns>
        public static DenseMatrix<T> operator /(DenseMatrix<T> A, T scalar)
        {
            if (A is null)
            {
                throw new ArgumentNullException(nameof(A));
            }
            return A.Divide(scalar, ProviderFactory.GetDefaultBLAS1<T>());
        }

        /// <summary>
        /// Returns the sum of two dense matrices, using the default naive level-3 BLAS provider.
        /// </summary>
        /// <param name="A">The left matrix.</param>
        /// <param name="B">The right matrix.</param>
        /// <returns>The sum of those two matrices.</returns>
        public static DenseMatrix<T> operator +(DenseMatrix<T> A, DenseMatrix<T> B)
        {
            if (A is null) throw new ArgumentNullException(nameof(A));
            return A.Add(B, ProviderFactory.GetDefaultBLAS2<T>());
        }

        /// <summary>
        /// Returns the matrix difference between two dense matrices, using the default level-2 BLAS provider.
        /// </summary>
        /// <param name="A">The left matrix.</param>
        /// <param name="B">The right matrix.</param>
        /// <returns>The left matrix subtract the right matrix.</returns>
        public static DenseMatrix<T> operator -(DenseMatrix<T> A, DenseMatrix<T> B)
        {
            if (A is null) throw new ArgumentNullException(nameof(A));
            return A.Subtract(B, ProviderFactory.GetDefaultBLAS2<T>());
        }

        /// <summary>
        /// Returns the negation of a matrix (i.e. its additive inverse $-A$), using the default level-2 BLAS provider.
        /// </summary>
        /// <param name="A">The matrix to negate.</param>
        /// <returns>The matrix negation.</returns>
        public static DenseMatrix<T> operator -(DenseMatrix<T> A)
        {
            if (A is null) throw new ArgumentNullException(nameof(A));
            return A.Negate(ProviderFactory.GetDefaultBLAS2<T>());
        }

        /// <summary>
        /// Returns whether two matrices are equal in value, using the default equality comparer 
        /// for type <txt>T</txt>. This method is identical to <txt>Equals</txt>.
        /// </summary>
        /// <param name="A">The left matrix to compare.</param>
        /// <param name="B">The right matrix to compare.</param>
        /// <returns>Whether the two matrices are equal in value.</returns>
        public static bool operator ==(DenseMatrix<T> A, DenseMatrix<T> B)
        {
            if (A is null)
            {
                return false;
            }
            return A.Equals(B);
        }

        /// <summary>
        /// Returns whether two matrices are not equal in value, using the default equality comparer 
        /// for type <txt>T</txt>.
        /// </summary>
        /// <param name="A">The left matrix to compare.</param>
        /// <param name="B">The right matrix to compare.</param>
        /// <returns>Whether the two matrices are not equal in value.</returns>
        public static bool operator !=(DenseMatrix<T> A, DenseMatrix<T> B) => !(A == B);

        #endregion


        #region Casts

        /// <summary>
        /// Cast a jagged matrix into a <txt>DenseMatrix<T></txt>
        /// </summary>
        /// <param name="matrix">A jagged 2-dimensional array.</param>
        public static implicit operator DenseMatrix<T>(T[][] matrix) => new DenseMatrix<T>(matrix);

        /// <summary>
        /// Cast a rectangular 2-dimensional matrix into a <txt>DenseMatrix<T></txt>
        /// </summary>
        /// <param name="matrix">A rectangular 2-dimensional matrix.</param>
        public static implicit operator DenseMatrix<T>(T[,] matrix) => new DenseMatrix<T>(matrix);

        /// <summary>
        /// Cast a diagonal matrix into a dense matrix. Note that as <txt>DenseMatrix<T></txt> requires storage of 
        /// all elements of the matrix (including zeroes), this method is prone to out-of-memory exceptions (hence 
        /// an explicit cast is required).
        /// </summary>
        /// <param name="matrix">A diagonal matrix.</param>
        public static explicit operator DenseMatrix<T>(DiagonalMatrix<T> matrix) => new DenseMatrix<T>(matrix);

        /// <summary>
        /// Cast a band matrix into a dense matrix. Note that as <txt>DenseMatrix<T></txt> requires storage of 
        /// all elements of the matrix (including zeroes), this method is prone to out-of-memory exceptions (hence 
        /// an explicit cast is required).
        /// </summary>
        /// <param name="matrix">A band matrix.</param>
        public static explicit operator DenseMatrix<T>(BandMatrix<T> matrix) => new DenseMatrix<T>(matrix);

        /// <summary>
        /// Cast a sparse matrix in Coordinate List (COO) form into a dense matrix.
        /// </summary>
        /// <param name="matrix">A matrix using COO storage.</param>
        public static explicit operator DenseMatrix<T>(COOSparseMatrix<T> matrix) => new DenseMatrix<T>(matrix);

        /// <summary>
        /// Cast a sparse matrix in Compressed Sparse Row (CSR) form into a dense matrix.
        /// </summary>
        /// <param name="matrix">A matrix using CSR storage.</param>
        public static explicit operator DenseMatrix<T>(CSRSparseMatrix<T> matrix) => new DenseMatrix<T>(matrix);

        /// <summary>
        /// Cast a sparse matrix in Compressed Sparse Column (CSC) form into a dense matrix.
        /// </summary>
        /// <param name="matrix">The sparse matrix.</param>
        public static explicit operator DenseMatrix<T>(CSCSparseMatrix<T> matrix) => new DenseMatrix<T>(matrix);

        /// <summary>
        /// Cast a sparse matrix into a dense matrix. Note that as <txt>DenseMatrix<T></txt> requires storage of 
        /// all elements of the matrix (including zeroes), this method is prone to out-of-memory exceptions (hence 
        /// an explicit cast is required).
        /// </summary>
        /// <param name="matrix">A sparse matrix.</param>
        public static explicit operator DenseMatrix<T>(DOKSparseMatrix<T> matrix) => new DenseMatrix<T>(matrix);

        /// <summary>
        /// Cast a sparse matrix in Skyline form into a dense matrix.
        /// </summary>
        /// <param name="matrix">The sparse matrix.</param>
        public static explicit operator DenseMatrix<T>(SKYSparseMatrix<T> matrix) => new DenseMatrix<T>(matrix);

        /// <summary>
        /// Cast a block-diagonal matrix into a dense matrix.
        /// </summary>
        /// <param name="matrix">The block-diagonal matrix.</param>
        public static explicit operator DenseMatrix<T>(BlockDiagonalMatrix<T> matrix) => new DenseMatrix<T>(matrix);

        #endregion

    }
}
