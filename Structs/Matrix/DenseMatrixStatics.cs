﻿using LinearNet.Global;
using LinearNet.Helpers;
using LinearNet.Matrices;
using LinearNet.Statistics;
using System;
using System.Reflection;

namespace LinearNet.Structs
{
    /// <summary>
    /// A static class containing useful functions for creating special types of dense matrices.
    /// </summary>
    /// <cat>linear-algebra</cat>
    public static class DenseMatrix
    {
        /// <summary>
        /// Create a matrix of dimensions <txt>rows</txt> x <txt>columns</txt> where each entry is <txt>value</txt>.
        /// <!--inputs-->
        /// <!--returns-->
        /// 
        /// <h4>Examples</h4>
        /// <pre><code class="cs">
        /// // Create a dense 10 x 10 matrix of ones
        /// DenseMatrix&lt;double&gt; ones = DenseMatrix.Repeat&lt;double&gt;(10, 10, 0.0); 
        /// 
        /// // Create a sparse 5 by 5 matrix with each entry equal to -3 + 2i
        /// SparseMatrix&lt;Complex&gt; matrix = SparseMatrix.Repeat&lt;Complex&gt;(5, 5, new Complex(-3, 2));
        /// 
        /// // Create a rectangular 15 by 10 matrix with entry equal to -2
        /// float[,] A = RectangularMatrix.Repeat&lt;float&gt;(15, 10, -2.0f);
        /// 
        /// </code></pre>
        /// 
        /// </summary>
        /// <param name="rows">The number of rows in the created matrix.</param>
        /// <param name="columns">The number of columns in the created matrix.</param>
        /// <param name="value">The value of each entry in the created matrix.</param>
        /// <returns>A matrix of dimensions <txt>rows</txt> $\times$ <txt>columns</txt> with all entries equal to <txt>value</txt>.</returns>
        public static DenseMatrix<T> Repeat<T>(int rows, int columns, T value) where T : new()
        {
            T[][] values = MatrixInternalExtensions.JMatrix<T>(rows, columns);
            T[] row;

            int i, j;
            for (i = 0; i < rows; ++i)
            {
                row = values[i];
                for (j = 0; j < columns; ++j)
                {
                    row[j] = value;
                }
            }
            return new DenseMatrix<T>(values);
        }

        /// <summary>
        /// <para>
        /// Create a <txt>rows</txt> $\times$ <txt>columns</txt> matrix where all entries are the additive identity of some 
        /// additive group.
        /// </para>
        /// <para>
        /// Supported matrix types: <txt>DenseMatrix&lt;T&gt;</txt>, <txt>SparseMatrix&lt;T&gt;</txt>, <txt>DiagonalMatrix&lt;T&gt;</txt>, 
        /// <txt>BandMatrix&lt;T&gt;</txt>, <txt>TriangularMatrix&lt;T&gt;</txt> and <txt>T[,]</txt>.
        /// </para>
        /// <para>
        /// Supported data types: <txt>int</txt>, <txt>long</txt>, <txt>float</txt>, <txt>double</txt>, <txt>decimal</txt> and all types 
        /// <txt>T</txt> implementing <txt>AdditiveGroup&lt;T&gt;</txt>.
        /// </para>
        /// <!--inputs-->
        /// <!--returns-->
        /// </summary>
        /// <param name="rows">The number of rows in the created matrix.</param>
        /// <param name="columns">The number of columns in the created matrix.</param>
        /// <returns>A <txt>rows</txt> $\times$ <txt>columns</txt> matrix of zeroes.</returns>
        public static DenseMatrix<T> Zeroes<T>(int rows, int columns) where T : new()
        {
            Type type = typeof(T);
            if (type == typeof(int))
            {
                return new DenseMatrix<int>(rows, columns) as DenseMatrix<T>;
            }
            if (type == typeof(long))
            {
                return new DenseMatrix<long>(rows, columns) as DenseMatrix<T>;
            }
            if (type == typeof(float))
            {
                return new DenseMatrix<float>(rows, columns) as DenseMatrix<T>;
            }
            if (type == typeof(double))
            {
                return new DenseMatrix<double>(rows, columns) as DenseMatrix<T>;
            }
            if (type == typeof(decimal))
            {
                return new DenseMatrix<decimal>(rows, columns) as DenseMatrix<T>;
            }
            if (typeof(IAdditiveGroup<T>).GetTypeInfo().IsAssignableFrom(type.GetTypeInfo()))
            {
                T zero = ((IAdditiveGroup<T>)new T()).AdditiveIdentity;
                return Repeat(rows, columns, zero);
            }

            throw new NotSupportedException(ExceptionMessages.TypeNotSupported);
        }
        /// <summary>
        /// Creates a matrix where each element is 1. 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="rows">The number of rows of the matrix.</param>
        /// <param name="columns">The number of zeroes of the matrix.</param>
        /// <returns>A dense matrix whose elements are one.</returns>
        public static DenseMatrix<T> Ones<T>(int rows, int columns) where T : new()
        {
            Type type = typeof(T);
            if (type == typeof(int))
            {
                return Repeat(rows, columns, 1) as DenseMatrix<T>;
            }
            if (type == typeof(long))
            {
                return Repeat(rows, columns, 1L) as DenseMatrix<T>;
            }
            if (type == typeof(float))
            {
                return Repeat(rows, columns, 1.0f) as DenseMatrix<T>;
            }
            if (type == typeof(double))
            {
                return Repeat(rows, columns, 1.0) as DenseMatrix<T>;
            }
            if (type == typeof(decimal))
            {
                return Repeat(rows, columns, 1.0m) as DenseMatrix<T>;
            }
            if (typeof(IRing<T>).GetTypeInfo().IsAssignableFrom(type.GetTypeInfo()))
            {
                T one = ((IRing<T>)new T()).MultiplicativeIdentity;
                return Repeat(rows, columns, one) as DenseMatrix<T>;
            }
            throw new NotSupportedException(ExceptionMessages.TypeNotSupported);
        }

        #region Identity matrix creation methods 

        /// <summary>
        /// Create a <txt>dim</txt> $\times$ <txt>dim</txt> identity matrix.
        /// 
        /// <h4>Example</h4>
        /// <pre><code class="cs">
        /// // Create a dense 10 x 10 real identity matrix
        /// DenseMatrix&lt;double&gt; matrix1 = DenseMatrix.Identity&lt;double&gt;(10); 
        /// 
        /// // Create a sparse 10 x 10 complex identity matrix
        /// SparseMatrix&lt;Complex&gt; matrix2 = SparseMatrix.Identity&lt;Complex&gt;(10); 
        /// 
        /// </code></pre>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dim">The row/column dimension of the matrix.</param>
        /// <returns>A square identity matrix.</returns>
        public static DenseMatrix<T> Identity<T>(int dim) where T : new()
        {
            Type type = typeof(T);
            if (type == typeof(int))
            {
                return Identity<int>(dim, 1) as DenseMatrix<T>;
            }
            if (type == typeof(long))
            {
                return Identity<long>(dim, 1L) as DenseMatrix<T>;
            }
            if (type == typeof(float))
            {
                return Identity<float>(dim, 1.0F) as DenseMatrix<T>;
            }
            if (type == typeof(double))
            {
                return Identity<double>(dim, 1.0) as DenseMatrix<T>;
            }
            if (type == typeof(decimal))
            {
                return Identity<decimal>(dim, 1.0M) as DenseMatrix<T>;
            }
            if (typeof(IRing<T>).GetTypeInfo().IsAssignableFrom(type.GetTypeInfo()))
            {
                IRing<T> def = new T() as IRing<T>;
                return Identity(dim, def.MultiplicativeIdentity, def.AdditiveIdentity) as DenseMatrix<T>;
            }
            throw new NotSupportedException("Matrices over the type " + type.ToString() + " are not supported yet.");
        }
        /// <summary>
        /// Create an identity matrix, given the additive and multiplicative identity 
        /// </summary>
        /// <param name="dim"></param>
        /// <param name="one"></param>
        /// <param name="zero"></param>
        /// <returns></returns>
        private static DenseMatrix<T> Identity<T>(int dim, T one, T zero) where T : new()
        {
            T[][] matrix = MatrixInternalExtensions.JMatrix<T>(dim, dim);
            T[] row;

            int i, j;
            for (i = 0; i < dim; ++i)
            {
                row = matrix[i];
                for (j = 0; j < dim; ++j)
                {
                    row[j] = zero;
                }
                row[i] = one;
            }
            return new DenseMatrix<T>(matrix);
        }
        /// <summary>
        /// Create an identity matrix, given the multiplicative identity. Used by matrix types 
        /// whose default value is the additive identity (e.g. all primitive types), so we dont 
        /// waste time looping though the entire matrix setting entries to 0.
        /// </summary>
        /// <param name="dim"></param>
        /// <param name="one"></param>
        /// <returns></returns>
        private static DenseMatrix<T> Identity<T>(int dim, T one) where T : new()
        {
            T[][] matrix = MatrixInternalExtensions.JMatrix<T>(dim, dim);
            for (int i = 0; i < dim; ++i)
            {
                matrix[i][i] = one;
            }
            return new DenseMatrix<T>(matrix);
        }

        #endregion

        #region Diagonal matrix creation methods

        /// <summary>
        /// Create a $n \times n$ diagonal matrix from a length-$n$ vector of the diagonal elements.
        /// <!--inputs-->
        /// </summary>
        /// <name>Diag</name>
        /// <proto>IMatrix<T> IMatrix<T>.Diag(params T[] diagonal)</proto>
        /// <cat>la</cat>
        /// <param name="diagonal">The main diagonal terms of the matrix.</param>
        /// <returns>A square diagonal matrix.</returns>
        public static DenseMatrix<T> Diag<T>(params T[] diagonal) where T : IAdditiveGroup<T>, new()
        {
            if (diagonal == null)
            {
                throw new ArgumentNullException();
            }
            int dim = diagonal.Length, i, j;
            if (dim == 0)
            {
                throw new ArgumentOutOfRangeException("Cannot create an empty matrix.");
            }

            T[][] matrix = MatrixInternalExtensions.JMatrix<T>(dim, dim);
            T[] row;

            T zero = new T().AdditiveIdentity;
            for (i = 0; i < dim; ++i)
            {
                row = matrix[i];
                for (j = 0; j < dim; ++j)
                {
                    row[j] = zero;
                }
                row[i] = diagonal[i];
            }
            return new DenseMatrix<T>(matrix);
        }
        private static DenseMatrix<T> diag_inner<T>(params T[] diagonal) where T : new()
        {
            if (diagonal == null) throw new ArgumentNullException();

            int dim = diagonal.Length;
            if (dim == 0) throw new ArgumentOutOfRangeException("Cannot create an empty matrix.");

            T[][] matrix = MatrixInternalExtensions.JMatrix<T>(dim, dim);
            for (int i = 0; i < dim; ++i)
            {
                matrix[i][i] = diagonal[i];
            }
            return new DenseMatrix<T>(matrix);
        }
        public static DenseMatrix<int> Diag(params int[] diagonal) => diag_inner(diagonal);
        public static DenseMatrix<long> Diag(params long[] diagonal) => diag_inner(diagonal);
        public static DenseMatrix<float> Diag(params float[] diagonal) => diag_inner(diagonal);
        public static DenseMatrix<double> Diag(params double[] diagonal) => diag_inner(diagonal);
        public static DenseMatrix<decimal> Diag(params decimal[] diagonal) => diag_inner(diagonal);

        #endregion

        #region Random matrix creation methods

        /// <summary>
        /// <para>
        /// Creates a dense <txt>rows</txt> $\times$ <txt>columns</txt> matrix of type <txt>T</txt> whose entries are random values.
        /// </para>
        /// <para>
        /// The method accepts a random element generator, or uses a default generator if one not specified. Default random generators 
        /// are defined for <txt>int</txt>, <txt>long</txt>, <txt>float</txt>, <txt>double</txt>, <txt>decimal</txt> and <txt>Complex</txt>.
        /// <ul>
        /// <li>For matrices over <txt>float</txt>, <txt>double</txt> and <txt>decimal</txt>, the random elements will be uniformly sampled between $[-1, 1]$</li>
        /// <li>For matrices over <txt>int</txt>, the elements will be uniformly sampled from $[-2^{32}, 2^{32} - 1]$.</li>
        /// <li>For matrices over <txt>long</txt>, the elements will be uniformly sampled from $[-2^{64}, 2^{64} - 1]$.</li>
        /// <li>For matrices over <txt>Complex</txt>, the elements will be uniformly sampled from the unit disk $\{ z: |z| \le 1 \}$</li>
        /// </ul>
        /// </para>
        /// <para>For custom algebras over $G$, the method accepts a input <txt>func</txt> $f: () \to G$</para>
        /// 
        /// <!--inputs-->
        /// <!--returns-->
        /// </summary>
        /// <name>Random</name>
        /// <proto>DenseMatrix<T> DenseMatrix.Random<T>(int rows, int columns, Func<T> random)
        /// </proto>
        /// <cat>la</cat>
        /// <param name="rows">The number of rows in the created matrix.</param>
        /// <param name="columns">The number of columns in the created matrix.</param>
        /// <param name="random">
        /// <b>Optional.</b><br/>
        /// A random generator of type <txt>T</txt>. If not specified, a default generator will be used.
        /// </param>
        /// <returns>A <txt>rows</txt> $\times$ <txt>columns</txt> random matrix.</returns>
        public static DenseMatrix<T> Random<T>(int rows, int columns, Func<T> random = null) where T : new()
        {
            if (random == null)
            {
                random = DefaultGenerators.GetRandomGenerator<T>();
            }

            T[][] matrix = MatrixInternalExtensions.JMatrix<T>(rows, columns);
            T[] row;
            int i, j;
            for (i = 0; i < rows; ++i)
            {
                row = matrix[i];
                for (j = 0; j < columns; ++j)
                {
                    row[j] = random();
                }
            }
            return matrix;
        }

        /// <summary>
        /// Create a dense matrix with random elements independently sampled from a probability distribution.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="rows">The number of rows of the matrix.</param>
        /// <param name="columns">The number of columns of the matrix.</param>
        /// <param name="dist">The univariate probability distribution to sample the matrix elements.</param>
        /// <returns>A random matrix whose elements are sampled from a probability distribution.</returns>
        public static DenseMatrix<T> Random<T>(int rows, int columns, IUnivariateDistribution<T> dist) where T : IComparable<T>, new()
        {
            Random r = new Random();
            return Random(rows, columns, () => dist.Sample(r));
        }

        /// <summary>
        /// <para>Creates a random <txt>dim</txt> $\times$ <txt>dim</txt> positive-definite matrix.</para>
        /// <para>The method generates a random matrix then multiplies it by its own transpose.</para>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dim">the row/column dimension of the matrix.</param>
        /// <returns>A random square positive definite matrix.</returns>
        public static DenseMatrix<T> RandomSPD<T>(int dim, Func<T> random = null) where T : new()
        {
            if (random == null)
            {
                random = DefaultGenerators.GetRandomGenerator<T>();
            }

            // Check if complex...
            if (typeof(T) == typeof(Complex))
            {
                Func<Complex> rc = random as Func<Complex>;
                DenseMatrix<Complex> C = Random(dim, dim, rc);
                return (C * C.ConjugateTranspose()) as DenseMatrix<T>;
            }

            DenseMatrix<T> A = Random(dim, dim, random);

            return A * A.Transpose();
        }

        /// <summary>
        /// <para>Creates a random <txt>dim</txt> $\times$ <txt>dim</txt> Hessenberg matrix.</para>
        /// <!--inputs-->
        /// </summary>
        /// <param name="dim">The dimension of the created matrix.</param>
        /// <param name="upper">
        /// <b>Optional.</b><br/>
        /// Specifies whether the returned matrix will be upper or lower Hessenberg.</param>
        /// <param name="random">
        /// <b>Optional.</b><br/>
        /// A random generator of type <txt>T</txt>. If not specified, a default generator will be used. Please see <a href="#Random"><txt>random</txt></a> for 
        /// documentation on default random generators. 
        /// </param>
        /// <returns>A random matrix is Hessenberg form.</returns>
        public static DenseMatrix<T> RandomHessenberg<T>(int dim, bool upper = true, Func<T> random = null) where T : new()
        {
            DenseMatrix<T> zero = Zeroes<T>(dim, dim);
            if (random == null)
            {
                random = DefaultGenerators.GetRandomGenerator<T>();
            }

            for (int i = 0; i < dim; ++i)
            {
                T[] row = zero[i];

                int start = upper ? Math.Max(0, i - 1) : 0;
                int end = upper ? dim : Math.Min(dim, i + 2);
                for (int j = start; j < end; ++j)
                {
                    row[j] = random();
                }
            }
            return zero;
        }

        /// <summary>
        /// <para>
        /// Creates a random <txt>rows</txt> $\times$ <txt>columns</txt> diagonal matrix.
        /// All off-diagonal entries of the created matrix will be equal to the additive identity of the group <txt>T</txt>.
        /// </para>
        /// <!--inputs-->
        /// </summary>
        /// <param name="rows">The number of rows in the created matrix.</param>
        /// <param name="columns">The number of columns in the created matrix.</param>
        /// <param name="random">
        /// <b>Optional.</b><br/>
        /// A random generator of type <txt>T</txt>. If not specified, a default generator will be used. Please see <a href="#Random"><txt>random</txt></a> for 
        /// documentation on default random generators. 
        /// </param>
        /// <returns>Returns a random diagonal matrix.</returns>
        public static DenseMatrix<T> RandomDiagonal<T>(int rows, int columns, Func<T> random = null) where T : new()
        {

            DenseMatrix<T> zero = Zeroes<T>(rows, columns);
            if (random == null)
            {
                random = DefaultGenerators.GetRandomGenerator<T>();
            }

            int end = Math.Min(rows, columns), i;
            for (i = 0; i < end; ++i)
            {
                zero[i][i] = random();
            }
            return zero;
        }

        /// <summary>
        /// <para>
        /// Creates a random <txt>rows</txt> $\times$ <txt>columns</txt> bidiagonal matrix.
        /// </para>
        /// <!--inputs-->
        /// </summary>
        /// <param name="rows">The number of rows in the created matrix.</param>
        /// <param name="columns">The number of columns in the created matrix.</param>
        /// <param name="upper">
        /// <b>Optional.</b><br/>
        /// Specifies whether the created matrix will be an upper bidiagonal matrix.
        /// </param>
        /// <param name="random">
        /// <b>Optional.</b><br/>
        /// A random generator of type <txt>T</txt>. If not specified, a default generator will be used. Please see <a href="#Random"><txt>random</txt></a> for 
        /// documentation on default random generators. 
        /// </param>
        /// <returns>A random bidiagonal matrix.</returns>
        public static DenseMatrix<T> RandomBidiagonal<T>(int rows, int columns, bool upper = true, Func<T> random = null) where T : new()
        {
            DenseMatrix<T> zero = Zeroes<T>(rows, columns);
            if (random == null)
            {
                random = DefaultGenerators.GetRandomGenerator<T>();
            }

            zero[0][0] = random();
            if (upper)
            {
                int end = Math.Min(rows, columns);
                for (int i = 1; i < end; ++i)
                {
                    zero[i][i] = random();
                    zero[i - 1][i] = random();
                }

                if (columns > rows)
                {
                    zero[end][end + 1] = random();
                }
            }
            else
            {
                int end = Math.Min(rows, columns);
                for (int i = 1; i < end; ++i)
                {
                    zero[i][i] = random();
                    zero[i][i - 1] = random();
                }
                if (rows > columns)
                {
                    zero[end + 1][end] = random();
                }
            }
            return zero;
        }

        /// <summary>
        /// <para>
        /// Creates a random <txt>rows</txt> $\times$ <txt>columns</txt> tridiagonal matrix.
        /// </para>
        /// <!--inputs-->
        /// </summary>
        /// <param name="rows">The number of rows in the created matrix.</param>
        /// <param name="columns">The number of columns in the created matrix.</param>
        /// <param name="random">
        /// <b>Optional.</b><br/>
        /// A random generator of type <txt>T</txt>. If not specified, a default generator will be used. Please see <a href="#Random"><txt>random</txt></a> for 
        /// documentation on default random generators. 
        /// </param>
        /// <returns>A random tridiagonal matrix.</returns>
        public static DenseMatrix<T> RandomTridiagonal<T>(int rows, int columns, Func<T> random = null) where T : new()
        {
            DenseMatrix<T> zero = Zeroes<T>(rows, columns);

            if (random == null)
            {
                random = DefaultGenerators.GetRandomGenerator<T>();
            }

            zero[0][0] = random();
            int end = Math.Min(rows, columns);
            for (int i = 0; i < end; ++i)
            {
                zero[i][i - 1] = random();
                zero[i][i] = random();
                zero[i - 1][i] = random();
            }

            if (rows > columns)
            {
                zero[end + 1][end] = random();
            }
            else if (rows < columns)
            {
                zero[end][end + 1] = random();
            }
            return zero;
        }

        /// <summary>
        /// <para>
        /// Creates a random <txt>rows</txt> $\times$ <txt>columns</txt> triangular matrix.
        /// </para>
        /// <!--inputs-->
        /// </summary>
        /// <param name="rows">The number of rows in the created matrix.</param>
        /// <param name="columns">The number of columns in the created matrix.</param>
        /// <param name="upper">
        /// <b>Optional.</b><br/>
        /// Specifies whether the created matrix will be an upper triangular matrix.
        /// </param>
        /// <param name="random">
        /// <b>Optional.</b><br/>
        /// A random generator of type <txt>T</txt>. If not specified, a default generator will be used. Please see <a href="#Random"><txt>random</txt></a> for 
        /// documentation on default random generators. 
        /// </param>
        /// <returns>A random triangular matrix.</returns>
        public static DenseMatrix<T> RandomTriangular<T>(int rows, int columns, bool upper = true, Func<T> random = null) where T : new()
        {
            DenseMatrix<T> zero = Zeroes<T>(rows, rows);
            if (random == null)
            {
                random = DefaultGenerators.GetRandomGenerator<T>();
            }

            for (int i = 0; i < rows; ++i)
            {
                T[] row = zero[i];

                int start = upper ? i : 0;
                int end = upper ? columns : Math.Min(columns, i + 1);
                for (int j = start; j < end; ++j)
                {
                    row[j] = random();
                }
            }
            return zero;
        }

        /// <summary>
        /// <para>
        /// Creates a random <txt>rows</txt> $\times$ <txt>columns</txt> matrix of rank $r$.
        /// </para>
        /// <!--inputs-->
        /// </summary>
        /// <param name="rows">The number of rows in the created matrix.</param>
        /// <param name="columns">The number of columns in the created matrix.</param>
        /// <param name="rank">The rank of the created matrix, $r$.</param>
        /// <param name="random">
        /// <b>Optional.</b><br/>
        /// A random generator of type <txt>T</txt>. If not specified, a default generator will be used. Please see <a href="#Random"><txt>random</txt></a> for 
        /// documentation on default random generators. 
        /// </param>
        /// <returns>A random low-rank matrix.</returns>
        public static DenseMatrix<T> RandomLowRankMatrix<T>(int rows, int columns, int rank) where T : new()
        {
            Type type = typeof(T);
            if (type == typeof(int))
            {
                return RandomLowRankMatrix<int>(rows, columns, rank, (A, B) => A.Multiply(B)) as DenseMatrix<T>;
            }
            if (type == typeof(long))
            {
                return RandomLowRankMatrix<long>(rows, columns, rank, (A, B) => A.Multiply(B)) as DenseMatrix<T>;
            }
            if (type == typeof(float))
            {
                return RandomLowRankMatrix<float>(rows, columns, rank, (A, B) => A.Multiply(B)) as DenseMatrix<T>;
            }
            if (type == typeof(double))
            {
                return RandomLowRankMatrix<double>(rows, columns, rank, (A, B) => A.Multiply(B)) as DenseMatrix<T>;
            }
            if (type == typeof(decimal))
            {
                return RandomLowRankMatrix<decimal>(rows, columns, rank, (A, B) => A.Multiply(B)) as DenseMatrix<T>;
            }
            if (type == typeof(Complex))
            {
                return RandomLowRankMatrix<Complex>(rows, columns, rank, (A, B) => A.Multiply(B)) as DenseMatrix<T>;
            }
            throw new NotImplementedException();
        }
        private static DenseMatrix<T> RandomLowRankMatrix<T>(int rows, int columns, int rank, Func<DenseMatrix<T>, DenseMatrix<T>, DenseMatrix<T>> Multiply) where T : new()
        {
            if (rows < 0 || columns < 0 || rank < 0 || rank > Math.Min(rows, columns))
            {
                throw new ArgumentOutOfRangeException();
            }
            return Multiply(Random<T>(rows, rank), Random<T>(rank, columns));
        }

        /// <summary>
        /// Create a random sparse matrix with the specified dimensionality and sparsity ratio.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="rows">The number of rows of the matrix.</param>
        /// <param name="columns">The number of columns of the matrix.</param>
        /// <param name="sparsity">The proportion of elements that are zero. Must between 0 and 1.</param>
        /// <param name="random">A random element generator.</param>
        /// <returns>A random sparse matrix.</returns>
        public static DenseMatrix<T> RandomSparse<T>(int rows, int columns, double sparsity, Func<T> random = null) where T : new()
        {
            if (random == null)
            {
                random = DefaultGenerators.GetRandomGenerator<T>();
            }

            DenseMatrix<T> matrix = Random(rows, columns, random);

            int totalElements = rows * columns, zeroElements = (int)(sparsity * totalElements);
            //Debug.WriteLine("Generating " + rows + " x " + columns + " matrix with " + zeroElements + " 0's");
            int[] elements = new int[totalElements];
            for (int i = 0; i < totalElements; ++i)
            {
                elements[i] = i;
            }

            Random r = new Random(0);
            elements.Shuffle(r);

            T zero = new T();
            for (int i = 0; i < zeroElements; ++i)
            {
                int row = elements[i] / columns, column = elements[i] % columns;
                matrix[row, column] = zero;
            }
            return matrix;
        }
        #endregion

        /// <summary>
        /// Load a dense matrix from a file.
        /// <!--inputs-->
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="filename">The path to the file.</param>
        /// <param name="elementDelim">
        /// <b>Optional</b>, defaults to <txt>","</txt><br/>
        /// The string separating each matrix element within the same row.</param>
        /// <param name="rowDelim">
        /// <b>Optional</b>, defaults to <txt>"\n"</txt><br/>
        /// The string separating each row of the matrix.</param>
        /// <param name="Parse">
        /// <b>Optional</b>, defaults to the default generator if <txt>T</txt> is <txt>int</txt>, <txt>long</txt>, 
        /// <txt>float</txt>, <txt>double</txt>, <txt>decimal</txt> and <txt>Complex</txt>.<br/>
        /// A func <txt>string</txt> $\to$ <txt>T</txt> that parses a string.
        /// </param>
        /// <returns>A dense matrix.</returns>
        public static DenseMatrix<T> Load<T>(string filename, string elementDelim = ",", string rowDelim = "\n", Func<string, T> Parse = null) where T : new()
        {
            if (Parse == null)
            {
                Parse = DefaultGenerators.GetParser<T>();
            }

            string[] lines = FileIO.ReadAllText(filename).Split(new string[] { rowDelim }, StringSplitOptions.RemoveEmptyEntries);
            string[] elDelims = { elementDelim };

            int rows = lines.Length, columns = 0, i, j;
            string[][] r = new string[rows][];

            for (i = 0; i < rows; ++i)
            {
                r[i] = lines[i].Split(elDelims, StringSplitOptions.None);
                if (columns < r[i].Length)
                {
                    columns = r[i].Length;
                }
            }

            T[][] matrix = new T[rows][];
            for (i = 0; i < rows; ++i)
            {
                T[] row = new T[columns];
                string[] r_i = r[i];
                int len = r[i].Length;
                for (j = 0; j < len; ++j)
                {
                    row[j] = Parse(r_i[j]);
                }
            }
            return new DenseMatrix<T>(rows, columns, matrix);
        }
    }

}
