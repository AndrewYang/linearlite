﻿using LinearNet.Providers;
using System;
using System.Collections.Generic;

namespace LinearNet.Structs
{
    /// <summary>
    /// Implementation of a diagonal matrix, where only the diagonal elements are stored, and all off-diagonal elements assumed to be zero.
    /// </summary>
    /// <cat>linear-algebra</cat>
    public class DiagonalMatrix<T> : Matrix<T> where T : new()
    {
        private readonly T _zero;
        private T[] _diagonalTerms;

        /// <summary>
        /// An array of the diagonal terms of the matrix, where the $i$-th element represents the $(i,i)$-th 
        /// entry of the matrix.
        /// </summary>
        public T[] DiagonalTerms { get { return _diagonalTerms; } }

        /// <summary>
        /// Gets or sets the <txt>index</txt>-th row of the matrix.
        /// </summary>
        /// <param name="index">The row index</param>
        /// <returns>The <txt>index</txt>-th row of the diagonal matrix.</returns>
        public override T[] this[int index]
        {
            get
            {
                if (index < 0 || index >= Rows)
                {
                    throw new ArgumentOutOfRangeException();
                }

                T[] row = new T[Columns];
                if (index < Math.Min(Rows, Columns))
                {
                    row[index] = _diagonalTerms[index];
                }
                return row;
            }
            set
            {
                if (index < 0 || index >= Rows)
                {
                    throw new ArgumentOutOfRangeException();
                }
                throw new NotImplementedException();
            }
        }
        /// <summary>
        /// Gets or sets the entry at row <txt>rowIndex</txt> and column <txt>columnIndex</txt> of this matrix.
        /// </summary>
        /// <param name="rowIndex">The row index of the entry.</param>
        /// <param name="columnIndex">The column index of the entry.</param>
        /// <returns>The (<txt>rowIndex</txt>, <txt>columnIndex</txt>)-th element of this matrix.</returns>
        public override T this[int rowIndex, int columnIndex]
        {
            get
            {
                if (rowIndex < 0 || columnIndex < 0 || rowIndex >= Rows || columnIndex >= Columns)
                {
                    throw new ArgumentOutOfRangeException();
                }

                if (rowIndex != columnIndex)
                {
                    return default(T);
                }
                return _diagonalTerms[rowIndex];
            }
            set
            {
                if (rowIndex < 0 || columnIndex < 0 || rowIndex >= Rows || columnIndex >= Columns)
                {
                    throw new ArgumentOutOfRangeException();
                }

                if (rowIndex != columnIndex)
                {
                    throw new InvalidOperationException();
                }
                _diagonalTerms[rowIndex] = value;
            }
        }

        public override int SymbolicNonZeroCount => _diagonalTerms.Length;

        public override IEnumerable<MatrixEntry<T>> NonZeroEntries
        {
            get
            {
                for (int i = 0; i < _diagonalTerms.Length; ++i)
                {
                    yield return new MatrixEntry<T>(i, i, _diagonalTerms[i]);
                }
            }
        }


        #region Constructors

        /// <summary>
        /// Initializes a diagonal matrix with <txt>rows</txt> rows and <txt>columns</txt> columns.
        /// </summary>
        /// <param name="rows">The number of rows of the matrix.</param>
        /// <param name="columns">The number of columns of the matrix.</param>
        public DiagonalMatrix(int rows, int columns) : base(MatrixStorageType.DIAGONAL, rows, columns)
        {
            _diagonalTerms = new T[Math.Min(rows, columns)];
            _zero = new T();
        }

        /// <summary>
        /// <para>
        /// Initializes a square diagonal matrix using an array of the diagonal elements.
        /// </para>
        /// <para>
        /// The number of rows and columns of this matrix will be equal to the length of the <txt>diagonalTerms</txt>
        /// array.
        /// </para>
        /// </summary>
        /// <param name="diagonalTerms">The diagonal terms of the matrix.</param>
        public DiagonalMatrix(T[] diagonalTerms) : base(MatrixStorageType.DIAGONAL)
        {
            if (diagonalTerms is null)
            {
                throw new ArgumentNullException(nameof(diagonalTerms));
            }

            Rows = diagonalTerms.Length;
            Columns = diagonalTerms.Length;

            _diagonalTerms = diagonalTerms;
            _zero = new T();
        }

        /// <summary>
        /// <para>
        /// Construct a diagonal matrix given the dimensions of a matrix, and its diagonal elements.
        /// </para>
        /// <para>
        /// The length of the diagonal vector must be at most both the number of rows and columns.
        /// </para>
        /// </summary>
        /// <param name="rows">The number of rows in the matrix.</param>
        /// <param name="columns">The number of columns in the matrix.</param>
        /// <param name="diagonalTerms">The diagonal terms of the matrix</param>
        public DiagonalMatrix(int rows, int columns, T[] diagonalTerms) : base(MatrixStorageType.DIAGONAL, rows, columns)
        {
            if (diagonalTerms is null)
            {
                throw new ArgumentNullException(nameof(diagonalTerms));
            }
            _diagonalTerms = diagonalTerms;
            _zero = new T();
        }

        public DiagonalMatrix(BlockDiagonalMatrix<T> matrix) : base(MatrixStorageType.DIAGONAL)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            if (!matrix.IsDiagonal())
            {
                throw new InvalidOperationException("Matrix cannot be stored as a diagonal matrix because it contains a non-zero off-diagonal entry.");
            }
            Rows = matrix.Rows;
            Columns = matrix.Columns;
            _zero = new T();

            _diagonalTerms = RectangularVector.Zeroes<T>(Math.Min(Rows, Columns));

            int nBlocks = matrix.Blocks.Length;
            for (int b = 0; b < nBlocks; ++b)
            {
                DenseMatrix<T> block = matrix.Blocks[b];
                int dim = block.Rows, offset = matrix.BlockIndices[b];

                for (int i = 0; i < dim; ++i)
                {
                    _diagonalTerms[i + offset] = block.Values[i][i];
                }
            }
        }

        public DiagonalMatrix(COOSparseMatrix<T> matrix) : base(MatrixStorageType.DIAGONAL)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            Rows = matrix.Rows;
            Columns = matrix.Columns;
            _zero = new T();

            _diagonalTerms = RectangularVector.Zeroes<T>(Math.Min(Rows, Columns));

            List<MatrixEntry<T>> values = matrix.Values;
            int count = values.Count;
            for (int i = 0; i < count; ++i)
            {
                MatrixEntry<T> e = values[i];
                if (e.RowIndex != e.ColumnIndex)
                {
                    throw new InvalidOperationException("Matrix cannot be stored as a diagonal matrix because it contains a non-zero off-diagonal entry.");
                }
                _diagonalTerms[e.RowIndex] = e.Value;
            }
        }

        public DiagonalMatrix(Matrix<T> matrix) : base(MatrixStorageType.DIAGONAL)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            Rows = matrix.Rows;
            Columns = matrix.Columns;
            _zero = new T();

            _diagonalTerms = RectangularVector.Zeroes<T>(Math.Min(Rows, Columns));
            foreach (MatrixEntry<T> e in matrix.NonZeroEntries)
            {
                if (e.RowIndex != e.ColumnIndex)
                {
                    throw new InvalidOperationException("Matrix cannot be stored as a diagonal matrix because it contains a non-zero off-diagonal entry.");
                }
                _diagonalTerms[e.RowIndex] = e.Value;
            }
        }

        #endregion


        public override int Count(Predicate<T> predicate)
        {
            if (predicate is null) throw new ArgumentNullException(nameof(predicate));

            int count, i, len = _diagonalTerms.Length;
            if (predicate(_zero))
            {
                count = Rows * Columns - len;
            }
            else
            {
                count = 0;
            }

            for (i = 0; i < len; ++i)
            {
                if (predicate(_diagonalTerms[i]))
                {
                    ++count;
                }
            }
            return count;
        }

        public override T[] LeadingDiagonal()
        {
            return _diagonalTerms;
        }

        public override Matrix<T> Add(Matrix<T> matrix, IProvider<T> provider)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            if (CompareRepresentationPriority(this, matrix) < 0)
            {
                return matrix.Add(this, provider); // return the sum as type(matrix)
            }
            return Add(matrix.ToDiagonalMatrix(), provider);
        }

        /// <summary>
        /// Calculates and returns the diagonal matrix equal to (this) + B
        /// </summary>
        /// <param name="D"></param>
        /// <returns></returns>
        public DiagonalMatrix<T> Add(DiagonalMatrix<T> D, IProvider<T> provider)
        {
            MatrixChecks.AssertMatrixDimensionsEqual(this, D);

            T[] diag1 = _diagonalTerms;
            T[] diag2 = D._diagonalTerms;

            int len1 = diag1.Length, len2 = diag2.Length, i;

            // Add, while remaining agnostic to differing lengths
            int max = Math.Max(len1, len2);
            T[] sum = new T[max];
            for (i = 0; i < len1; ++i)
            {
                sum[i] = diag1[i];
            }
            for (i = len1; i < len2; ++i)
            {
                sum[i] = provider.Zero;
            }
            for (i = 0; i < len2; ++i)
            {
                sum[i] = provider.Add(sum[i], diag2[i]);
            }
            return new DiagonalMatrix<T>(sum);
        }

        /// <summary>
        /// Calculates and returns the diagonal matrix equal to (this) - B
        /// </summary>
        /// <param name="D"></param>
        /// <returns></returns>
        internal DiagonalMatrix<T> Subtract(DiagonalMatrix<T> D, Func<T, T, T> Subtract)
        {
            MatrixChecks.AssertMatrixDimensionsEqual(this, D);

            T[] diag1 = _diagonalTerms;
            T[] diag2 = D._diagonalTerms;

            int len1 = diag1.Length, len2 = diag2.Length, i;

            // Add, while remaining agnostic to differing lengths
            int max = Math.Max(len1, len2);
            T[] sum = new T[max];
            for (i = 0; i < len1; ++i)
            {
                sum[i] = diag1[i];
            }
            for (i = len1; i < len2; ++i)
            {
                sum[i] = new T();
            }
            for (i = 0; i < len2; ++i)
            {
                sum[i] = Subtract(sum[i], diag2[i]);
            }
            return new DiagonalMatrix<T>(sum);
        }

        /// <summary>
        /// Calculates and returns the product of two diagonal matrices 
        /// </summary>
        /// <param name="D"></param>
        /// <param name="Multiply"></param>
        /// <returns></returns>
        internal DiagonalMatrix<T> Multiply(DiagonalMatrix<T> D, Func<T, T, T> Multiply)
        {
            MatrixChecks.AssertMatrixDimensionsForMultiplication(this, D);

            T[] diag = _diagonalTerms;
            T[] B_diag = D._diagonalTerms;

            int min = Math.Min(diag.Length, B_diag.Length), i;
            T[] prod = new T[min];
            for (i = 0; i < min; ++i)
            {
                prod[i] = Multiply(diag[i], B_diag[i]);
            }
            return new DiagonalMatrix<T>(Rows, D.Columns, prod);
        }

        /// <summary>
        /// Multiply a matrix by a vector
        /// </summary>
        /// <param name="vector"></param>
        /// <param name="Multiply"></param>
        /// <returns></returns>
        internal override void Multiply(T[] vector, T[] product, IProvider<T> provider, bool increment)
        {
            if (vector is null)
            {
                throw new ArgumentNullException(nameof(vector));
            }
            if (product is null)
            {
                throw new ArgumentNullException(nameof(product));
            }
            if (vector.Length != Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(vector), $"The number of columns of this matrix does not match the dimension of '{nameof(vector)}'.");
            }
            if (product.Length < Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(product), $"'{nameof(product)}' is not large enough to store the matrix-vector product.");
            }

            int min = Math.Min(Rows, Columns), i;
            if (increment)
            {
                for (i = 0; i < min; ++i)
                {
                    product[i] = provider.Add(product[i], provider.Multiply(_diagonalTerms[i], vector[i]));
                }
            }
            else
            {
                for (i = 0; i < min; ++i)
                {
                    product[i] = provider.Multiply(_diagonalTerms[i], vector[i]);
                }
            }
        }
        internal override void TransposeAndMultiply(T[] vector, T[] product, IProvider<T> provider, bool increment)
        {
            // The exact same
            Multiply(vector, product, provider, increment);
        }

        private static SparseVector<T> Multiply(T[] diagonalTerms, int rows, SparseVector<T> vector, IProvider<T> provider)
        {
            int[] vect_indices = vector.Indices;
            T[] vect_values = vector.Values;

            int len = vect_values.Length;
            int[] prod_indices = vect_indices.Copy();
            T[] prod_values = new T[len];

            for (int i = 0; i < len; ++i)
            {
                prod_values[i] = provider.Multiply(vect_values[i], diagonalTerms[vect_indices[i]]);
            }
            return new SparseVector<T>(rows, prod_indices, prod_values, true);
        }
        public override SparseVector<T> Multiply(SparseVector<T> vector, IProvider<T> provider)
        {
            if (vector is null)
            {
                throw new ArgumentNullException(nameof(vector));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            if (vector.Dimension < Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(vector), "The dimension of the vector is less than the number of columns of the matrix.");
            }

            return Multiply(_diagonalTerms, Rows, vector, provider);
        }

        public override SparseVector<T> TransposeAndMultiply(SparseVector<T> vector, IProvider<T> provider)
        {
            if (vector is null)
            {
                throw new ArgumentNullException(nameof(vector));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            if (vector.Dimension < Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(vector), "The dimension of the vector is less than the number of rows of the matrix.");
            }

            return Multiply(_diagonalTerms, Columns, vector, provider);
        }

        /// <summary>
        /// Calculate and return the product of (this) and a scalar of type T
        /// </summary>
        /// <param name="s"></param>
        /// <param name="Multiply"></param>
        /// <returns></returns>
        internal DiagonalMatrix<T> Multiply(T s, IDenseBLAS1<T> blas)
        {
            DiagonalMatrix<T> result = Copy();
            T[] vec = result._diagonalTerms;
            blas.SCAL(vec, s, 0, vec.Length);
            return result;
        }

        public override void MultiplyInPlace(T scalar, IProvider<T> provider)
        {
            if (scalar is null)
            {
                throw new ArgumentNullException(nameof(scalar));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }

            int dim = _diagonalTerms.Length, i;
            for (i = 0; i < dim; ++i)
            {
                _diagonalTerms[i] = provider.Multiply(_diagonalTerms[i], scalar);
            }
        }

        /// <summary>
        /// Divide this matrix by a scalar, using the specified provider for elementwise division.
        /// A new matrix is returned without modifying this matrix.
        /// </summary>
        /// <param name="scalar">The scalar to divide by.</param>
        /// <param name="provider">The provider used for elementwise division.</param>
        /// <returns>The matrix-scalar division result.</returns>
        public DiagonalMatrix<T> Divide(T scalar, IProvider<T> provider)
        {
            DiagonalMatrix<T> copy = Copy();
            copy.DivideInPlace(scalar, provider);
            return copy;
        }

        /// <summary>
        /// Calculates and returns a matrix raised to a integer power
        /// </summary>
        /// <param name="power"></param>
        /// <param name="Power"></param>
        /// <returns></returns>
        internal DiagonalMatrix<T> Pow(int power, Func<T, int, T> Power)
        {
            int len = _diagonalTerms.Length, i;
            T[] diag = new T[len];
            for (i = 0; i < len; ++i)
            {
                diag[i] = Power(_diagonalTerms[i], power);
            }
            return new DiagonalMatrix<T>(diag);
        }

        /// <summary>
        /// Calculates and returns exp(A) where A is diagonal 
        /// </summary>
        /// <param name="Exp"></param>
        /// <returns></returns>
        internal DiagonalMatrix<F> Exp<F>(F one, Func<T, F> Exp) where F : new()
        {
            if (Rows != Columns)
            {
                throw new InvalidOperationException();
            }

            int len = _diagonalTerms.Length, i;
            F[] diag = new F[Rows];
            for (i = 0; i < len; ++i)
            {
                diag[i] = Exp(_diagonalTerms[i]);
            }
            for (i = len; i < Rows; ++i) 
            {
                diag[i] = one;
            }
            return new DiagonalMatrix<F>(diag);
        }

        /// <summary>
        /// Returns a deep clone of this diagonal matrix, as an object.
        /// </summary>
        /// <returns>A copy of this matrix as an object.</returns>
        public override object Clone()
        {
            return Copy();
        }
        /// <summary>
        /// Returns a deep copy of this diagonal matrix, as a <txt>DiagonalMatrix<T>.</txt>
        /// </summary>
        /// <returns>A copy of this matrix.</returns>
        public DiagonalMatrix<T> Copy()
        {
            return new DiagonalMatrix<T>(Rows, Columns, _diagonalTerms.Copy());
        }

        internal override bool SolveTriangular(T[] b, T[] x, IDenseBLAS1<T> blas, bool lower, bool transpose)
        {
            // Divide and we're done - no difference made in lower/upper or transpose/no transpose

            IProvider<T> provider = blas.Provider;
            int len = Rows;
            for (int i = 0; i < len; ++i)
            {
                T d = _diagonalTerms[i];
                if (provider.Equals(d, _zero))
                {
                    return false;
                }
                x[i] = provider.Divide(b[i], d);
            }
            return true;
        }

        public override T Trace(IProvider<T> provider)
        {
            T tr = provider.Zero;
            for (int d = 0; d < _diagonalTerms.Length; ++d)
            {
                tr = provider.Add(tr, _diagonalTerms[d]);
            }
            return tr;
        }

        public override DenseVector<T> Row(int rowIndex)
        {
            if (rowIndex < 0 || rowIndex >= Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(rowIndex));
            }

            T[] row = RectangularVector.Zeroes<T>(Columns);
            if (rowIndex < _diagonalTerms.Length)
            {
                row[rowIndex] = _diagonalTerms[rowIndex];
            }
            return new DenseVector<T>(row);
        }

        public override SparseVector<T> SparseRow(int rowIndex)
        {
            if (rowIndex < 0 || rowIndex >= Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(rowIndex));
            }
           
            if (rowIndex < _diagonalTerms.Length)
            {
                return new SparseVector<T>(Columns, rowIndex, _diagonalTerms[rowIndex]);
            }
            else
            {
                return new SparseVector<T>(Columns);
            }
        }

        public override DenseVector<T> Column(int columnIndex)
        {
            if (columnIndex < 0 || columnIndex >= Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(columnIndex));
            }

            T[] column = RectangularVector.Zeroes<T>(Rows);
            if (columnIndex < _diagonalTerms.Length)
            {
                column[columnIndex] = _diagonalTerms[columnIndex];
            }
            return new DenseVector<T>(column);
        }

        public override SparseVector<T> SparseColumn(int columnIndex)
        {
            if (columnIndex < 0 || columnIndex >= Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(columnIndex));
            }

            if (columnIndex < _diagonalTerms.Length)
            {
                return new SparseVector<T>(Rows, columnIndex, _diagonalTerms[columnIndex]);
            }
            else
            {
                return new SparseVector<T>(Rows);
            }
        }

        public override Matrix<T> Select(int[] rowIndices, int[] columnIndices)
        {
            if (rowIndices is null)
            {
                throw new ArgumentNullException(nameof(rowIndices));
            }
            if (columnIndices is null)
            {
                throw new ArgumentNullException(nameof(columnIndices));
            }

            ThrowHelper.AssertCollectionInRange(rowIndices, 0, Rows, nameof(rowIndices));
            ThrowHelper.AssertCollectionInRange(columnIndices, 0, Columns, nameof(columnIndices));

            // Returns a COOSparseMatrix
            List<MatrixEntry<T>> values = new List<MatrixEntry<T>>();

            // Surely there is a faster algorithm than this O(mn) rubbish?! Too lazy to improve it 
            // for now...
            int rlen = rowIndices.Length, clen = columnIndices.Length;
            for (int ri = 0; ri < rlen; ++ri)
            {
                int r = rowIndices[ri];
                for (int ci = 0; ci < clen; ++ci)
                {
                    int c = columnIndices[ci];
                    if (c == r)
                    {
                        values.Add(new MatrixEntry<T>(ri, ci, _diagonalTerms[r]));
                    }
                }
            }

            return new COOSparseMatrix<T>(rlen, clen, values, true);
        }

        public override Vector<T> SelectAsVector(int rowIndex, int[] columnIndices)
        {
            if (rowIndex < 0 || rowIndex >= Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(rowIndex));
            }
            if (columnIndices is null)
            {
                throw new ArgumentNullException(nameof(columnIndices));
            }
            ThrowHelper.AssertCollectionInRange(columnIndices, 0, Columns, nameof(columnIndices));

            int clen = columnIndices.Length;
            for (int ci = 0; ci < clen; ++ci)
            {
                if (columnIndices[ci] == rowIndex)
                {
                    return new SparseVector<T>(clen, new int[] { ci }, new T[] { _diagonalTerms[rowIndex] }, true);
                }
            }
            return new SparseVector<T>(clen);
        }

        public override Vector<T> SelectAsVector(int[] rowIndices, int columnIndex)
        {
            if (columnIndex < 0 || columnIndex >= Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(columnIndex));
            }
            if (rowIndices is null)
            {
                throw new ArgumentNullException(nameof(rowIndices));
            }
            ThrowHelper.AssertCollectionInRange(rowIndices, 0, Rows, nameof(rowIndices));

            int rlen = rowIndices.Length;
            for (int ri = 0; ri < rlen; ++ri)
            {
                if (rowIndices[ri] == columnIndex)
                {
                    return new SparseVector<T>(rlen, new int[] { ri }, new T[] { _diagonalTerms[columnIndex] }, true);
                }
            }
            return new SparseVector<T>(rlen);
        }

        public override DenseVector<T> RowSums(IProvider<T> provider)
        {
            if (_diagonalTerms.Length == Rows)
            {
                return new DenseVector<T>(_diagonalTerms.Copy());
            }

            T[] sums = RectangularVector.Zeroes<T>(Rows);
            Array.Copy(_diagonalTerms, sums, _diagonalTerms.Length);
            return new DenseVector<T>(sums);
        }

        public override DenseVector<T> ColumnSums(IProvider<T> provider)
        {
            if (_diagonalTerms.Length == Columns)
            {
                return new DenseVector<T>(_diagonalTerms.Copy());
            }

            T[] sums = RectangularVector.Zeroes<T>(Columns);
            Array.Copy(_diagonalTerms, sums, _diagonalTerms.Length);
            return new DenseVector<T>(sums);
        }

        public override void SwapRows(int rowIndex1, int rowIndex2)
        {
            throw new NotImplementedException("Unable to swap rows because the resulting matrix is not a diagonal matrix.");
        }

        public override void SwapColumns(int columnIndex1, int columnIndex2)
        {
            throw new NotImplementedException("Unable to swap columns because the resulting matrix is not a diagonal matrix.");
        }

        public override void Clear()
        {
            if (_zero.Equals(default))
            {
                Array.Clear(_diagonalTerms, 0, _diagonalTerms.Length);
            }
            else
            {
                int len = _diagonalTerms.Length, i;
                for (i = 0; i < len; ++i)
                {
                    _diagonalTerms[i] = _zero;
                }
            }
        }

        public void Clear(Predicate<MatrixEntry<T>> select)
        {
            if (select is null)
            {
                throw new ArgumentNullException(nameof(select));
            }

            for (int i = 0; i < _diagonalTerms.Length; ++i)
            {
                if (select(new MatrixEntry<T>(i, i, _diagonalTerms[i])))
                {
                    _diagonalTerms[i] = _zero;
                }
            }
        }

        public override void Clear(Predicate<T> select)
        {
            if (select is null)
            {
                throw new ArgumentNullException(nameof(select));
            }

            Clear(e => select(e.Value));
        }

        public override void ClearRow(int rowIndex)
        {
            if (rowIndex < 0 || rowIndex >= Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(rowIndex));
            }

            if (rowIndex < _diagonalTerms.Length)
            {
                _diagonalTerms[rowIndex] = _zero;
            }
        }

        public override void ClearColumn(int columnIndex)
        {
            if (columnIndex < 0 || columnIndex >= Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(columnIndex));
            }

            if (columnIndex < _diagonalTerms.Length)
            {
                _diagonalTerms[columnIndex] = _zero;
            }
        }

        public override void ClearSubmatrix(int rowIndex, int columnIndex, int rows, int columns)
        {
            if (rowIndex < 0 || rowIndex >= Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(rowIndex));
            }
            if (columnIndex < 0 || columnIndex >= Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(columnIndex));
            }

            int bottom = rowIndex + rows, right = columnIndex + columns;
            if (rows < 0 || bottom >= Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(rows));
            }
            if (columns < 0 || right >= Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(columns));
            }

            Clear(e => rowIndex <= e.RowIndex && e.RowIndex < bottom && columnIndex <= e.ColumnIndex && e.ColumnIndex < right);
        }

        public override void ClearLower()
        {
            // Do nothing
        }

        public override void ClearUpper()
        {
            // Do nothing
        }


        #region Convertors

        public override BandMatrix<T> ToBandMatrix()
        {
            return new BandMatrix<T>(this);
        }

        public override BlockDiagonalMatrix<T> ToBlockDiagonalMatrix()
        {
            return new BlockDiagonalMatrix<T>(this);
        }

        public override COOSparseMatrix<T> ToCOOSparseMatrix()
        {
            return new COOSparseMatrix<T>(this);
        }

        public override CSCSparseMatrix<T> ToCSCSparseMatrix()
        {
            return new CSCSparseMatrix<T>(this);
        }

        public override CSRSparseMatrix<T> ToCSRSparseMatrix()
        {
            return new CSRSparseMatrix<T>(this);
        }

        public override DenseMatrix<T> ToDenseMatrix()
        {
            return new DenseMatrix<T>(this);
        }

        public override DiagonalMatrix<T> ToDiagonalMatrix()
        {
            return this;
        }

        public override DOKSparseMatrix<T> ToDOKSparseMatrix()
        {
            return new DOKSparseMatrix<T>(this);
        }

        public override MCSRSparseMatrix<T> ToMCSRSparseMatrix()
        {
            return new MCSRSparseMatrix<T>(this);
        }

        public override SKYSparseMatrix<T> ToSKYSparseMatrix()
        {
            return new SKYSparseMatrix<T>(this);
        }

        #endregion


        #region Boolean methods

        public bool Equals(DiagonalMatrix<T> matrix, Func<T, T, bool> equals)
        {
            if (equals is null)
            {
                throw new ArgumentNullException(nameof(equals));
            }
            if (matrix is null)
            {
                return false;
            }
            if (Rows != matrix.Rows || Columns != matrix.Columns)
            {
                return false;
            }

            int dim = _diagonalTerms.Length;
            if (matrix._diagonalTerms.Length != dim)
            {
                return false;
            }

            for (int d = 0; d < dim; ++d)
            {
                if (!equals(_diagonalTerms[d], matrix._diagonalTerms[d]))
                {
                    return false;
                }
            }
            return true;
        }
        public bool Equals(DiagonalMatrix<T> matrix, IEqualityComparer<T> comparer)
        {
            if (comparer is null)
            {
                throw new ArgumentNullException(nameof(comparer));
            }
            return Equals(matrix, comparer.Equals);
        }
        public bool Equals(DiagonalMatrix<T> matrix)
        {
            return Equals(matrix, EqualityComparer<T>.Default);
        }
        public override bool Equals(object obj)
        {
            if (obj is null)
            {
                return false;
            }
            if (obj is DiagonalMatrix<T>)
            {
                return Equals(obj as DiagonalMatrix<T>);
            }
            return Equals(obj as Matrix<T>);
        }

        public override bool IsLower(Predicate<T> isZero)
        {
            return true;
        }
        public override bool IsUpper(Predicate<T> isZero)
        {
            return true;
        }
        public override bool IsSymmetric(Func<T, T, bool> equals)
        {
            return true;
        }
        public override bool IsSymmetric(IEqualityComparer<T> comparer)
        {
            return true;
        }

        public override bool IsDiagonal(Predicate<T> isZero)
        {
            return true;
        }

        public override bool IsBanded(int lowerBandwidth, int upperBandwidth, Predicate<T> isZero)
        {
            if (lowerBandwidth >= 0 && upperBandwidth >= 0)
            {
                return true;
            }

            // Nothing has negative bandwidth
            return false;
        }

        public override bool IsZero(Predicate<T> isZero)
        {
            if (isZero is null)
            {
                throw new ArgumentNullException(nameof(isZero));
            }

            int len = _diagonalTerms.Length, i;
            for (i = 0; i < len; ++i)
            {
                if (!isZero(_diagonalTerms[i]))
                {
                    return false;
                }
            }
            return true;
        }

        public override bool IsIdentity(Predicate<T> isZero, Predicate<T> isOne)
        {
            if (isOne is null)
            {
                throw new ArgumentNullException(nameof(isOne));
            }

            int len = _diagonalTerms.Length, i;
            for (i = 0; i < len; ++i)
            {
                if (!isOne(_diagonalTerms[i]))
                {
                    return false;
                }
            }
            return true;
        }

        #endregion
    }
}
