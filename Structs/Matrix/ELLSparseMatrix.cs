﻿using LinearNet.Global;
using LinearNet.Providers;
using LinearNet.Providers.Managed;
using System;
using System.Collections;
using System.Collections.Generic;

namespace LinearNet.Structs
{
    public class ELLSparseMatrix<T> : Matrix<T> where T : new()
    {
        private int _width;
        private int[] _columnIndices;
        private T[] _values;
        private readonly T _zero;

        public override T[] this[int index] { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public override T this[int rowIndex, int columnIndex]
        {
            get
            {
                if (rowIndex < 0 || rowIndex >= Rows)
                {
                    throw new ArgumentOutOfRangeException(nameof(rowIndex));
                }
                if (columnIndex < 0 || columnIndex >= Columns)
                {
                    throw new ArgumentOutOfRangeException(nameof(columnIndex));
                }

                // This is O(n) which is good enough for 'thin' matrices
                // Use a separate index i to keep track of which column 
                // we are in, to avoid overflow problems
                for (int i = 0, j = rowIndex; i < _width; ++i, j += Rows)
                {
                    if (_columnIndices[j] == columnIndex)
                    {
                        return _values[j];
                    }
                }

                return _zero;
            }
            set => throw new NotImplementedException(); 
        }

        public override IEnumerable<MatrixEntry<T>> NonZeroEntries
        {
            get
            {
                for (int i = 0, k = 0; i < _width; ++i)
                {
                    for (int r = 0; r < Rows; ++r, ++k)
                    {
                        yield return new MatrixEntry<T>(r, _columnIndices[k], _values[k]);
                    }
                }
            }
        }

        public override int SymbolicNonZeroCount => _values.Length;


        #region Constructors

        public ELLSparseMatrix(CSRSparseMatrix<T> matrix) : base(MatrixStorageType.ELL)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            Rows = matrix.Rows;
            Columns = matrix.Columns;
            _zero = new T();

            int[] rowIndices = matrix.RowIndices,
                colIndices = matrix.ColumnIndices;
            T[] values = matrix.Values;

            int maxWidth = 0;
            for (int r = 0; r < Rows; ++r)
            {
                int width = rowIndices[r + 1] - rowIndices[r];
                if (width > maxWidth)
                {
                    maxWidth = width;
                }
            }

            _width = maxWidth;
            if ((double)maxWidth * Rows > int.MaxValue)
            {
                throw new OverflowException("Matrix is too large to be stored in Ellpack format.");
            }
            int len = maxWidth * Rows;
            _columnIndices = new int[len];
            _values = new T[len];

            for (int r = 0; r < Rows; ++r)
            {
                int start = rowIndices[r], 
                    end = rowIndices[r + 1], 
                    count = end - start,
                    j = r;

                for (int i = start; i < end; ++i, j += Rows)
                {
                    _columnIndices[j] = colIndices[i];
                    _values[j] = values[i];
                }

                // Fill the rest with zeroes
                // We dont use j to avoid overflow
                int c = (end > start) ? colIndices[end - 1] + 1 : 0;
                for (int k = count; k < maxWidth; ++k)
                {
                    int index = k * Rows + r;
                    _columnIndices[index] = c++;
                    _values[index] = _zero;
                }
            }
        }

        /// <summary>
        /// Copy constructor
        /// </summary>
        /// <param name="matrix"></param>
        public ELLSparseMatrix(ELLSparseMatrix<T> matrix) : base(MatrixStorageType.ELL)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            Rows = matrix.Rows;
            Columns = matrix.Columns;
            _zero = new T();

            _width = matrix._width;
            _columnIndices = matrix._columnIndices.Copy();
            _values = matrix._values.Copy();
        }

        public ELLSparseMatrix(Matrix<T> matrix) : base(MatrixStorageType.ELL)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            
            Rows = matrix.Rows;
            Columns = matrix.Columns;
            _zero = new T();

            // Iterate once to get the width
            int[] widths = new int[Rows + 1];
            foreach (MatrixEntry<T> e in matrix.NonZeroEntries)
            {
                widths[e.RowIndex]++;
            }

            // Set _width to the max width
            _width = 0;
            for (int i = 0; i < Rows; ++i)
            {
                if (widths[i] > _width)
                {
                    _width = widths[i];
                }
            }

            if ((double)Rows * _width > int.MaxValue)
            {
                throw new OverflowException("Matrix is too large to be stored in Ellpack format.");
            }

            int len = Rows * _width;
            _columnIndices = new int[len];
            _values = new T[len];

            // Use widths as the 'next' array
            Array.Clear(widths, 0, Rows);

            // Generate temporary arrays that are stored in row-major order
            int[] columnIndices = new int[len];
            T[] values = new T[len];

            foreach (MatrixEntry<T> e in matrix.NonZeroEntries)
            {
                int index = e.RowIndex * _width + widths[e.RowIndex]++;
                columnIndices[index] = e.ColumnIndex;
                values[index] = e.Value;
            }

            // Add in the missing column indexes - note that widths has been 
            // restored because of the second iteration
            for (int r = 0; r < Rows; ++r)
            {
                int w = widths[r];
                int c = w > 0 ? columnIndices[(w - 1) * Rows + r] + 1 : 0;
                for (int i = widths[r]; i < _width; ++i)
                {
                    int index = i * Rows + r;
                    columnIndices[index] = c++;
                    values[index] = _zero;
                }
            }

            // Perform in-place sorting of each row
            for (int r = 0; r < Rows; ++r)
            {
                Array.Sort(columnIndices, values, r * _width, _width);
            }

            // Perform the transposition into the actual storage
            for (int r = 0, i = 0; r < Rows; ++r)
            {
                for (int c = 0; c < _width; ++c, ++i)
                {
                    int j = c * Rows + r;
                    _columnIndices[j] = columnIndices[i];
                    _values[j] = values[i];
                }
            }
        }

        public ELLSparseMatrix(int rows, int columns, int[] columnIndices, T[] values) : base(MatrixStorageType.ELL)
        {
            if (rows <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(rows), ExceptionMessages.NonPositiveNotAllowed);
            }
            if (columns <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(columns), ExceptionMessages.NonPositiveNotAllowed);
            }
            if (columnIndices is null)
            {
                throw new ArgumentNullException(nameof(columnIndices));
            }
            if (values is null)
            {
                throw new ArgumentNullException(nameof(values));
            }
            if (columnIndices.Length != values.Length)
            {
                throw new ArgumentOutOfRangeException($"The lengths of '{nameof(columnIndices)}' and '{nameof(values)}' arrays don't match.");
            }
            if (columnIndices.Length % rows != 0)
            {
                throw new ArgumentOutOfRangeException(nameof(columnIndices), $"The length of '{nameof(columnIndices)}' array is not divisible by the number of rows.");
            }

            Rows = rows;
            Columns = columns;
            _zero = new T();
            _width = columnIndices.Length / rows;

            // no copying!
            _columnIndices = columnIndices;
            _values = values;
        }

        #endregion


        /// <summary>
        /// Calculate the width of the symbolic union between this matrix and another matrix, 
        /// when stored in Ellpack format. 
        /// </summary>
        /// <param name="matrix"></param>
        /// <returns></returns>
        private int CalculateWidthOfUnion(ELLSparseMatrix<T> matrix)
        {
            // Calculate the maximum width
            int width = 0;

            // Use longs to avoid overflow
            long len1 = _values.Length, len2 = matrix._values.Length;

            for (int r = 0; r < Rows; ++r)
            {
                int nnz = 0;
                long i1 = r, i2 = r;

                while (i1 < len1 || i2 < len2)
                {
                    int c1 = i1 < len1 ? _columnIndices[i1] : int.MaxValue,
                        c2 = i2 < len2 ? matrix._columnIndices[i2] : int.MaxValue;

                    if (c1 > c2)
                    {
                        ++i2;
                    }
                    else if (c1 < c2)
                    {
                        ++i1;
                    }
                    else
                    {
                        ++i1;
                        ++i2;
                    }
                    ++nnz;
                }

                if (nnz > width)
                {
                    width = nnz;
                }
            }
            return width;
        }

        public override Matrix<T> Add(Matrix<T> matrix, IProvider<T> provider)
        {
            throw new NotImplementedException();
        }

        public ELLSparseMatrix<T> Add(ELLSparseMatrix<T> matrix, IProvider<T> provider)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }

            MatrixChecks.AssertMatrixDimensionsEqual(this, matrix);

            // Calculate the maximum width
            int width = CalculateWidthOfUnion(matrix);

            if ((double)width * Rows > int.MaxValue)
            {
                throw new OverflowException("Sum is too large to be stored in Ellpack format.");
            }

            // Use longs to avoid overflow
            long len1 = _values.Length, len2 = matrix._values.Length;
            int len = width * Rows;
            int[] columnIndices = new int[len];
            T[] values = new T[len];

            for (int r = 0; r < Rows; ++r)
            {
                long i1 = r, i2 = r;
                int ci = 0;
                while (i1 < len1 || i2 < len2)
                {
                    int c1 = i1 < len1 ? _columnIndices[i1] : int.MaxValue;
                    int c2 = i2 < len2 ? matrix._columnIndices[i2] : int.MaxValue;

                    int index = ci * Rows + r;
                    if (c1 > c2)
                    {
                        if (c2 >= Columns) break; // Range check is required

                        columnIndices[index] = c2;
                        values[index] = matrix._values[i2];
                    }
                    else if (c1 < c2)
                    {
                        if (c1 >= Columns) break; // Range check is required

                        columnIndices[index] = c1;
                        values[index] = _values[i1];
                    }
                    else
                    {
                        if (c1 >= Columns) break; // Range check is required

                        columnIndices[index] = c1;
                        values[index] = provider.Add(_values[i1], matrix._values[i2]);
                    }
                    ++ci;
                }

                // populate the end
                int c = ci == 0 ? 0 : columnIndices[(ci - 1) * Rows + r] + 1;
                for (; ci < width; ++ci)
                {
                    int index = ci * Rows + r;
                    columnIndices[index] = c++;
                    values[index] = _zero;
                }
            }

            return new ELLSparseMatrix<T>(Rows, Columns, columnIndices, values);
        }

        public override void Clear()
        {
            if (_zero.Equals(default(T)))
            {
                Array.Clear(_values, 0, _values.Length);
            }
            else
            {
                for (int i = 0; i < _values.Length; ++i)
                {
                    _values[i] = _zero;
                }
            }
        }

        public override void Clear(Predicate<T> select)
        {
            if (select is null)
            {
                throw new ArgumentNullException(nameof(select));
            }

            for (int i = 0, len = _values.Length; i < len; ++i)
            {
                if (select(_values[i]))
                {
                    _values[i] = _zero;
                }
            }
        }

        public override void ClearColumn(int columnIndex)
        {
            if (columnIndex < 0 || columnIndex >= Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(columnIndex));
            }

            // Should be faster than iterating over the whole _columnIndices array
            // in one go...

            for (int r = 0, k = 0; r < Rows; ++r)
            {
                for (int ci = 0; ci < _width; ++ci, ++k)
                {
                    int c = _columnIndices[k];
                    if (c > columnIndex)
                    {
                        break;
                    }
                    if (c == columnIndex)
                    {
                        _values[k] = _zero;
                    }
                }
            }
        }

        public override void ClearLower()
        {
            for (int r = 0, k = 0; r < Rows; ++r)
            {
                for (int ci = 0; ci < _width; ++ci, ++k)
                {
                    int c = _columnIndices[ci];
                    if (c >= r) break;
                    _values[k] = _zero;
                }
            }
        }

        public override void ClearRow(int rowIndex)
        {
            if (rowIndex < 0 || rowIndex >= Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(rowIndex));
            }

            for (int ci = 0, k = rowIndex; ci < _width; ++ci, k += Rows)
            {
                _values[k] = _zero;
            }
        }

        public override void ClearSubmatrix(int rowIndex, int columnIndex, int rows, int columns)
        {
            if (rowIndex < 0 || rowIndex >= Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(rowIndex));
            }
            if (columnIndex < 0 || columnIndex >= Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(columnIndex));
            }
            int rowEnd = checked(rowIndex + rows);
            if (rows < 0 || rowEnd >= Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(rows));
            }
            int colEnd = checked(columnIndex + columns);
            if (columns < 0 || colEnd >= Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(columns));
            }

            for (int r = rowIndex; r < rowEnd; ++r)
            {
                for (int ci = 0; ci < _width; ++ci)
                {
                    int k = r + ci * Rows,
                        c = _columnIndices[k];

                    if (columnIndex <= c && c < colEnd)
                    {
                        _values[k] = _zero;
                    }
                }
            }
        }

        public override void ClearUpper()
        {
            int dim = Math.Min(Rows, Columns);
            for (int r = 0, k = 0; r < dim; ++r)
            {
                for (int ci = 0; ci < _width; ++ci, ++k)
                {
                    int c = _columnIndices[k];
                    if (c > r)
                    {
                        _values[k] = _zero;
                    }
                }
            }
        }

        public override object Clone()
        {
            return Copy();
        }

        public override DenseVector<T> Column(int columnIndex)
        {
            if (columnIndex < 0 || columnIndex >= Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(columnIndex));
            }

            T[] vector = RectangularVector.Zeroes<T>(Rows);

            // Iterating in column-major order is far more cache friendly that
            // by row-major order
            for (int k = 0, len = _values.Length; k < len; ++k)
            {
                if (_columnIndices[k] == columnIndex)
                {
                    vector[k % Rows] = _values[k];
                }
            }
            return new DenseVector<T>(vector);
        }

        public override DenseVector<T> ColumnSums(IProvider<T> provider)
        {
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }

            T[] sums = RectangularVector.Zeroes<T>(Columns);

            for (int k = 0, len = _values.Length; k < len; ++k)
            {
                int c = _columnIndices[k];
                sums[c] = provider.Add(sums[c], _values[k]);
            }

            return new DenseVector<T>(sums);
        }

        public ELLSparseMatrix<T> Copy()
        {
            return new ELLSparseMatrix<T>(this);
        }

        public override int Count(Predicate<T> predicate)
        {
            int count = 0;
            for (int k = 0, len = _values.Length; k < len; ++k)
            {
                if (predicate(_values[k])) ++count;
            }
            return count;
        }

        public override bool Equals(object obj)
        {
            if (obj is null)
            {
                return false;
            }
            if (obj is ELLSparseMatrix<T>)
            {
                return Equals(obj as ELLSparseMatrix<T>);
            }
            if (obj is Matrix<T>)
            {
                return Equals(obj as Matrix<T>);
            }
            return false;
        }

        public bool Equals(ELLSparseMatrix<T> matrix, Func<T, T, bool> equals)
        {
            if (matrix is null) return false;

            if (Rows != matrix.Rows || Columns != matrix.Columns)
            {
                return false;
            }

            if (equals is null)
            {
                throw new ArgumentNullException(nameof(equals));
            }

            long len1 = _width, len2 = matrix._width;
            for (int r = 0; r < Rows; ++r)
            {
                long i1 = 0, i2 = 0;
                while (i1 < len1 || i2 < len2)
                {
                    int c1 = i1 < len1 ? _columnIndices[i1] : int.MaxValue;
                    int c2 = i2 < len2 ? matrix._columnIndices[i2] : int.MaxValue;

                    if (c1 > c2)
                    {
                        if (!equals(_zero, matrix._values[i2])) return false;
                    }
                    else if (c1 < c2)
                    {
                        if (!equals(_zero, _values[i1])) return false;
                    }
                    else
                    {
                        if (!equals(_values[i1], matrix._values[i2])) return false;
                    }
                }
            }

            return true;
        }

        public bool Equals(ELLSparseMatrix<T> matrix, IEqualityComparer<T> comparer)
        {
            if (comparer is null)
            {
                throw new ArgumentNullException(nameof(comparer));
            }
            return Equals(matrix, comparer.Equals);
        }

        public bool Equals(ELLSparseMatrix<T> matrix)
        {
            return Equals(matrix, EqualityComparer<T>.Default);
        }

        public override bool IsBanded(int lowerBandwidth, int upperBandwidth, Predicate<T> isZero)
        {
            if (lowerBandwidth < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(lowerBandwidth));
            }
            if (upperBandwidth < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(upperBandwidth));
            }
            if (isZero is null)
            {
                throw new ArgumentNullException(nameof(isZero));
            }

            // Cache-friendly iteration
            for (int k = 0, len = _values.Length; k < len; ++k)
            {
                if (!isZero(_values[k]))
                {
                    int c = _columnIndices[k], r = k % Rows;
                    if (r > c)
                    {
                        if (r - c > lowerBandwidth) return false;
                    }
                    else
                    {
                        if (c - r > upperBandwidth) return false;
                    }
                }
            }
            return true;
        }

        public override bool IsDiagonal(Predicate<T> isZero)
        {
            if (isZero is null)
            {
                throw new ArgumentNullException(nameof(isZero));
            }

            // Cache friendly iteration
            for (int k = 0, len = _values.Length; k < len; ++k)
            {
                int c = _columnIndices[k],
                    r = k % Rows;

                if (r != c && !isZero(_values[k]))
                {
                    return false;
                }
            }
            return true;
        }

        public override bool IsIdentity(Predicate<T> isZero, Predicate<T> isOne)
        {
            if (!IsSquare)
            {
                return false;
            }

            if (isZero is null)
            {
                throw new ArgumentNullException(nameof(isZero));
            }
            if (isOne is null)
            {
                throw new ArgumentNullException(nameof(isOne));
            }

            for (int k = 0, len = _values.Length; k < len; ++k)
            {
                int c = _columnIndices[k], r = k % Rows;
                if (r != c)
                {
                    if (!isZero(_values[k]))
                    {
                        return false;
                    }
                }
                else
                {
                    if (!isOne(_values[k]))
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        public override bool IsLower(Predicate<T> isZero)
        {
            if (isZero is null)
            {
                throw new ArgumentNullException(nameof(isZero));
            }

            for (int k = 0, len = _values.Length; k < len; ++k)
            {
                int c = _columnIndices[k], r = k % Rows;
                if (c > r && !isZero(_values[k]))
                {
                    return false;
                }
            }
            return true;
        }

        public override bool IsUpper(Predicate<T> isZero)
        {
            if (isZero is null)
            {
                throw new ArgumentNullException(nameof(isZero));
            }

            for (int k = 0, len = _values.Length; k < len; ++k)
            {
                int c = _columnIndices[k], r = k % Rows;
                if (r > c && !isZero(_values[k]))
                {
                    return false;
                }
            }
            return true;
        }

        public override bool IsZero(Predicate<T> isZero)
        {
            if ( isZero is null)
            {
                throw new ArgumentNullException(nameof(isZero));
            }

            for (int k = 0, len = _values.Length; k < len; ++k)
            {
                if (!isZero(_values[k]))
                {
                    return false;
                }
            }
            return true;
        }

        public override T[] LeadingDiagonal()
        {
            T[] diagonal = RectangularVector.Zeroes<T>(Math.Min(Rows, Columns));
            for (int k = 0, len = _values.Length; k < len; ++k)
            {
                int c = _columnIndices[k], r = k % len;
                if (c == r)
                {
                    diagonal[r] = _values[k];
                }
            }
            return diagonal; 
        }

        public override SparseVector<T> Multiply(SparseVector<T> vector, IProvider<T> provider)
        {
            if (vector is null)
            {
                throw new ArgumentNullException(nameof(vector));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            if (vector.Dimension != Columns)
            {
                throw new InvalidOperationException("Number of columns of matrix does not match the vector dimension.");
            }

            int[] vect_indices = vector.Indices;
            T[] vect_values = vector.Values;

            int vlen = vect_indices.Length;
            long len = _values.Length;

            List<int> indices = new List<int>();
            List<T> values = new List<T>();

            // coiteration - slow!
            for (int r = 0; r < Rows; ++r)
            {
                T dot = provider.Zero;
                long i1 = r;
                int i2 = 0;
                bool any = false;

                while (i1 < len && i2 < vlen)
                {
                    int c1 = _columnIndices[i1],
                        c2 = vect_indices[i2];

                    if (c1 > c2)
                    {
                        ++i2;
                    }
                    else if (c1 < c2) 
                    {
                        i1 += Rows;
                    }
                    else
                    {
                        dot = provider.Add(dot, provider.Multiply(_values[i1], vect_values[i2]));
                        i1 += Rows;
                        ++i2;
                        any = true;
                    }
                }

                if (any)
                {
                    indices.Add(r);
                    values.Add(dot);
                }
            }

            return new SparseVector<T>(Rows, indices.ToArray(), values.ToArray(), true);
        }

        internal override void Multiply(T[] vector, T[] product, IProvider<T> provider, bool increment)
        {
            if (increment)
            {
                for (int k = 0, len = _values.Length; k < len; ++k)
                {
                    int r = k % Rows;
                    product[r] = provider.Add(product[r], provider.Multiply(_values[k], vector[_columnIndices[k]]));
                }
            }
            else
            {
                for (int k = 0; k < Rows; ++k)
                {
                    product[k] = provider.Multiply(_values[k], vector[_columnIndices[k]]);
                }
                for (int k = Rows, len = _values.Length; k < len; ++k)
                {
                    product[k] = provider.Add(product[k], provider.Multiply(_values[k], vector[_columnIndices[k]]));
                }
            }
        }

        public CSRSparseMatrix<T> Multiply(ELLSparseMatrix<T> matrix, IProvider<T> provider)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }

            MatrixChecks.AssertMatrixDimensionsForMultiplication(this, matrix);

            BitArray in_set = new BitArray(Columns);
            T[] workspace = new T[Columns];
            int[] columnIndex = new int[Columns];

            // Overflow checks?
            int initialSize = _values.Length + matrix._values.Length;
            int[] rowIndices = new int[Rows + 1];
            List<int> columnIndices = new List<int>(initialSize);
            List<T> values = new List<T>(initialSize);

            // A column-major ordering will be more efficient, but can't seem to 
            // make it work without complex engineering... TODO for future research
            for (int r = 0; r < Rows; ++r)
            {
                int nz = 0;
                for (int i = 0; i < _width; ++i)
                {
                    int index = i * Rows + r,
                        k = _columnIndices[index];

                    T v = _values[index];

                    // We should in theory be checking for k < Columns
                    // however it is guaranteed that all these phantom entries 
                    // will be equal to zero anyway, so this single check suffices
                    if (!provider.Equals(_zero, v))
                    {
                        // Iterate across row k of matrix 2
                        for (int j = 0; j < matrix._width; ++j)
                        {
                            int index2 = j * Columns + k,
                                c = matrix._columnIndices[index2];

                            if (in_set[c])
                            {
                                workspace[c] = provider.Add(workspace[c], provider.Multiply(v, matrix._values[index2]));
                            }
                            else
                            {
                                workspace[c] = provider.Multiply(v, matrix._values[index2]);
                                columnIndex[nz++] = c;
                                in_set[c] = true;
                            }
                        }
                    }
                }

                // Store
                for (int i = 0; i < nz; ++i)
                {
                    int c = columnIndex[i];
                    columnIndices.Add(c);
                    values.Add(workspace[c]);

                    // preserve invariance
                    in_set[c] = false;
                }

                rowIndices[r + 1] = values.Count;
            }

            int[] cIndices_arr = columnIndices.ToArray();
            T[] values_arr = values.ToArray();

            // Enforce sort order
            for (int r = 0; r < Rows; ++r)
            {
                // There is no reason for the entries to already sorted (due to the set union),
                // so we won't waste time checking to see if it is presorted
                int start = rowIndices[r], end = rowIndices[r + 1];
                Array.Sort(cIndices_arr, values_arr, start, end - start);
            }

            return new CSRSparseMatrix<T>(matrix.Columns, rowIndices, cIndices_arr, values_arr);
        }

        public ELLSparseMatrix<T> Multiply(T scalar, IProvider<T> provider)
        {
            ELLSparseMatrix<T> copy = Copy();
            copy.MultiplyInPlace(scalar, provider);
            return copy;
        }

        public ELLSparseMatrix<T> Multiply(T scalar, IDenseBLAS1Provider<T> blas)
        {
            ELLSparseMatrix<T> copy = Copy();
            copy.MultiplyInPlace(scalar, blas);
            return copy;
        }

        public override void MultiplyInPlace(T scalar, IProvider<T> provider)
        {
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }

            MultiplyInPlace(scalar, new DefaultNativeProvider<T>(provider));
        }

        public void MultiplyInPlace(T scalar, IDenseBLAS1Provider<T> blas)
        {
            if (blas is null)
            {
                throw new ArgumentNullException(nameof(blas));
            }
            if (!(blas is IDenseBLAS1<T> b))
            {
                throw new NotSupportedException(ExceptionMessages.TypeNotSupported);
            }

            b.SCAL(_values, scalar, 0, _values.Length);
        }

        public ELLSparseMatrix<T> Negate(IDenseBLAS1Provider<T> blas)
        {
            ELLSparseMatrix<T> copy = Copy();
            copy.NegateInPlace(blas);
            return copy;
        }

        public void NegateInPlace(IDenseBLAS1Provider<T> blas)
        {
            if (blas is null)
            {
                throw new ArgumentNullException(nameof(blas));
            }
            if (!(blas is IDenseBLAS1<T> b))
            {
                throw new NotSupportedException(ExceptionMessages.TypeNotSupported);
            }

            T negone = b.Provider.Negate(b.Provider.One);
            b.SCAL(_values, negone, 0, _values.Length);
        }

        public override DenseVector<T> Row(int rowIndex)
        {
            if (rowIndex < 0 || rowIndex >= Rows)
            {
                throw new ArgumentNullException(nameof(rowIndex));
            }

            T[] row = RectangularVector.Zeroes<T>(Columns);
            for (int i = 0, k = rowIndex; i < _width; ++i)
            {
                row[_columnIndices[k]] = _values[k];
                k += Rows;
            }

            return new DenseVector<T>(row);
        }

        public override DenseVector<T> RowSums(IProvider<T> provider)
        {
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }

            T[] sums = new T[Rows];

            // Intialize with the first column
            Array.Copy(_values, sums, Rows);

            // BLAS will really speed things up here
            for (int i = Rows, len = _values.Length; i < len; ++i)
            {
                int r = i % Rows;
                sums[r] = provider.Add(sums[r], _values[i]);
            }
            return new DenseVector<T>(sums);
        }

        public override Matrix<T> Select(int[] rowIndices, int[] columnIndices)
        {
            if (rowIndices is null)
            {
                throw new ArgumentNullException(nameof(rowIndices));
            }
            if (columnIndices is null)
            {
                throw new ArgumentNullException(nameof(columnIndices));
            }
            ThrowHelper.AssertCollectionInRange(rowIndices, 0, Rows, nameof(rowIndices));
            ThrowHelper.AssertCollectionInRange(columnIndices, 0, Columns, nameof(columnIndices));

            // Initialize arrays to keep track of the original positions 
            // of the column indices arrays 
            int rlen = rowIndices.Length, clen = columnIndices.Length;
            int[] cPositions = new int[clen];

            for (int c = 0; c < clen; ++c)
            {
                cPositions[c] = c;
            }

            Array.Sort(columnIndices, cPositions);

            DenseMatrix<T> submatrix = new DenseMatrix<T>(rlen, clen);
            for (int ri = 0; ri < rlen; ++ri)
            {
                int r = rowIndices[ri];
                T[] values = submatrix.Values[r];

                // columnIndices is sorted
                long len = _values.Length, index = r;
                for (int i = 0; i < clen; ++i)
                {
                    int c = columnIndices[i];
                    while (index < len && _columnIndices[index] < c)
                    {
                        index += Rows;
                    }

                    if (index >= len) break;

                    if (_columnIndices[index] == c)
                    {
                        values[cPositions[c]] = _values[index];
                    }
                }
            }

            // Restore the original vector
            Array.Sort(cPositions, columnIndices);
            
            return submatrix;
        }

        public override Vector<T> SelectAsVector(int rowIndex, int[] columnIndices)
        {
            if (rowIndex < 0 || rowIndex >= Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(rowIndex));
            }
            if (columnIndices is null)
            {
                throw new ArgumentNullException(nameof(columnIndices));
            }
            ThrowHelper.AssertCollectionInRange(columnIndices, 0, Columns, nameof(columnIndices));

            int clen = columnIndices.Length;
            int[] cIndices = new int[clen];
            for (int c = 0; c < clen; ++c)
            {
                cIndices[c] = c;
            }

            Array.Sort(columnIndices, cIndices);

            T[] vector = RectangularVector.Zeroes<T>(clen);
            long index = rowIndex, len = _values.Length;
            for (int i = 0; i < clen; ++i)
            {
                int c = columnIndices[i];
                while (index < len && _columnIndices[index] < c)
                {
                    index += Rows;
                }
                if (index >= len)
                {
                    break;
                }

                if (_columnIndices[index] == c)
                {
                    vector[cIndices[i]] = _values[index];
                }
            }

            // Restore the indices vector
            Array.Sort(cIndices, columnIndices);

            return new DenseVector<T>(vector);
        }

        public override Vector<T> SelectAsVector(int[] rowIndices, int columnIndex)
        {
            if (rowIndices is null)
            {
                throw new ArgumentNullException(nameof(rowIndices));
            }
            if (columnIndex < 0 || columnIndex >= Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(columnIndex));
            }
            ThrowHelper.AssertCollectionInRange(rowIndices, 0, Rows, nameof(rowIndices));

            int rlen = rowIndices.Length;
            T[] values = RectangularVector.Zeroes<T>(rlen);
            
            long len = _values.Length;
            for (int ri = 0; ri < rlen; ++ri)
            {
                int r = rowIndices[ri];
                long index = r;
                while (index < len)
                {
                    if (_columnIndices[index] == columnIndex)
                    {
                        values[r] = _values[index];
                        break;
                    }
                    index += Rows;
                }
            }

            return new DenseVector<T>(values);
        }

        public override SparseVector<T> SparseColumn(int columnIndex)
        {
            if (columnIndex < 0 || columnIndex >= Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(columnIndex));
            }

            int nz = 0;
            for (int k = 0, len = _values.Length; k < len; ++k)
            {
                if (_columnIndices[k] == columnIndex) ++nz;
            }

            int[] indices = new int[nz];
            T[] values = new T[nz];

            for (int k = 0, len = _values.Length, i = 0; k < len; ++k)
            {
                if (_columnIndices[k] == columnIndex)
                {
                    indices[i] = k % Rows;
                    values[i] = _values[k];
                    ++i;
                }
            }

            // Requires a sort
            Array.Sort(indices, values);

            return new SparseVector<T>(Rows, indices, values, true);
        }

        public override SparseVector<T> SparseRow(int rowIndex)
        {
            if (rowIndex < 0 || rowIndex >= Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(rowIndex));
            }

            int[] indices = new int[_width];
            T[] values = new T[_width];

            for (int i = 0, k = rowIndex; i < _width; ++i)
            {
                indices[i] = _columnIndices[k];
                values[i] = _values[k];
                k += Rows;
            }

            // No sort required!
            return new SparseVector<T>(Columns, indices, values, true);
        }

        public ELLSparseMatrix<T> Subtract(ELLSparseMatrix<T> matrix, IProvider<T> provider)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }

            MatrixChecks.AssertMatrixDimensionsEqual(this, matrix);

            // Calculate the maximum width
            int width = CalculateWidthOfUnion(matrix);

            if ((double)width * Rows > int.MaxValue)
            {
                throw new OverflowException("Sum is too large to be stored in Ellpack format.");
            }

            // Use longs to avoid overflow
            long len1 = _values.Length, len2 = matrix._values.Length;
            int len = width * Rows;
            int[] columnIndices = new int[len];
            T[] values = new T[len];

            for (int r = 0; r < Rows; ++r)
            {
                long i1 = r, i2 = r;
                int ci = 0;
                while (i1 < len1 || i2 < len2)
                {
                    int c1 = i1 < len1 ? _columnIndices[i1] : int.MaxValue;
                    int c2 = i2 < len2 ? matrix._columnIndices[i2] : int.MaxValue;

                    int index = ci * Rows + r;
                    if (c1 > c2)
                    {
                        if (c2 >= Columns) break; // Range check is required

                        columnIndices[index] = c2;
                        values[index] = provider.Negate(matrix._values[i2]);
                    }
                    else if (c1 < c2)
                    {
                        if (c1 >= Columns) break; // Range check is required

                        columnIndices[index] = c1;
                        values[index] =_values[i1];
                    }
                    else
                    {
                        if (c1 >= Columns) break; // Range check is required

                        columnIndices[index] = c1;
                        values[index] = provider.Subtract(_values[i1], matrix._values[i2]);
                    }
                    ++ci;
                }

                // populate the end
                int c = ci == 0 ? 0 : columnIndices[(ci - 1) * Rows + r] + 1;
                for (; ci < width; ++ci)
                {
                    int index = ci * Rows + r;
                    columnIndices[index] = c++;
                    values[index] = _zero;
                }
            }

            return new ELLSparseMatrix<T>(Rows, Columns, columnIndices, values);
        }

        public override void SwapColumns(int columnIndex1, int columnIndex2)
        {
            // This is a nightmare because of the possibility that we're swapping into a zero column 
            // and vice versa... impleemntation needs careful thought...

            throw new NotImplementedException();
        }

        public override void SwapRows(int rowIndex1, int rowIndex2)
        {
            // This is much easier because all rows are indexed
            if (rowIndex1 < 0 || rowIndex1 >= Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(rowIndex1));
            }
            if (rowIndex2 < 0 || rowIndex2 >= Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(rowIndex2));
            }

            int k1 = rowIndex1, k2 = rowIndex2;
            for (int i = 0; i < _width; ++i)
            {
                Util.Swap(ref _values[k1], ref _values[k2]);
                Util.Swap(ref _columnIndices[k1], ref _columnIndices[k2]);

                k1 += Rows;
                k2 += Rows;
            }
        }

        public override BandMatrix<T> ToBandMatrix()
        {
            return new BandMatrix<T>(this);
        }

        public override BlockDiagonalMatrix<T> ToBlockDiagonalMatrix()
        {
            return new BlockDiagonalMatrix<T>(this);
        }

        public override COOSparseMatrix<T> ToCOOSparseMatrix()
        {
            return new COOSparseMatrix<T>(this);
        }

        public override CSCSparseMatrix<T> ToCSCSparseMatrix()
        {
            return new CSCSparseMatrix<T>(this);
        }

        public override CSRSparseMatrix<T> ToCSRSparseMatrix()
        {
            return new CSRSparseMatrix<T>(this);
        }

        public override DenseMatrix<T> ToDenseMatrix()
        {
            return new DenseMatrix<T>(this);
        }

        public override DiagonalMatrix<T> ToDiagonalMatrix()
        {
            return new DiagonalMatrix<T>(this);
        }

        public override DOKSparseMatrix<T> ToDOKSparseMatrix()
        {
            return new DOKSparseMatrix<T>(this);
        }

        public override MCSRSparseMatrix<T> ToMCSRSparseMatrix()
        {
            return new MCSRSparseMatrix<T>(this);
        }

        public override SKYSparseMatrix<T> ToSKYSparseMatrix()
        {
            return new SKYSparseMatrix<T>(this);
        }

        public override T Trace(IProvider<T> provider)
        {
            T trace = provider.Zero;
            for (int k = 0, len = _values.Length; k < len; ++k)
            {
                if (_columnIndices[k] == k % Rows)
                {
                    trace = provider.Add(trace, _values[k]);
                }
            }
            return trace;
        }

        public override SparseVector<T> TransposeAndMultiply(SparseVector<T> vector, IProvider<T> provider)
        {
            if (vector is null)
            {
                throw new ArgumentNullException(nameof(vector));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }

            if (vector.Dimension != Rows)
            {
                throw new InvalidOperationException("The vector dimension does not match the number of rows of this matrix.");
            }

            // For determining the size of the sparse vector
            BitArray inset = new BitArray(Columns);
            T[] workspace = new T[Columns];
            int[] indices = new int[Columns];

            int[] vect_indices = vector.Indices;
            T[] vect_values = vector.Values;
            int vlen = vect_indices.Length;

            int nz = 0;
            for (int j = 0; j < _width; ++j)
            {
                int offset = j * Rows;
                for (int i = 0; i < vlen; ++i)
                {
                    int r = vect_indices[i],
                        index = offset + r, 
                        c = _columnIndices[index];
                    if (c < Columns)
                    {
                        if (!inset[c])
                        {
                            inset[c] = true;
                            workspace[c] = provider.Multiply(_values[index], vect_values[i]);
                            indices[nz++] = c;
                        }
                        else
                        {
                            inset[c] = false;
                            workspace[c] = provider.Add(workspace[c], provider.Multiply(_values[index], vect_values[i]));
                        }
                    }
                }
            }

            Array.Resize(ref indices, nz);
            T[] values = new T[nz];
            for (int i = 0; i < nz; ++i)
            {
                values[i] = workspace[indices[i]];
            }

            Array.Sort(indices, values);

            return new SparseVector<T>(Columns, indices, values, true);
        }

        internal override void TransposeAndMultiply(T[] vector, T[] product, IProvider<T> provider, bool increment)
        {
            if (!increment)
            {
                if (provider.Equals(provider.Zero, default))
                {
                    Array.Clear(product, 0, Columns);
                }
                else
                {
                    for (int c = 0; c < Columns; ++c)
                    {
                        product[c] = _zero;
                    }
                }
            }

            for (int i = 0, k = 0; i < _width; ++i)
            {
                for (int j = 0; j < Rows; ++j, ++k)
                {
                    int c = _columnIndices[k];
                    product[c] = provider.Add(product[c], provider.Multiply(vector[j], _values[k]));
                }
            }
        }

        internal override bool SolveTriangular(T[] b, T[] x, IDenseBLAS1<T> blas, bool lower, bool transpose)
        {
            throw new NotImplementedException();
        }


        #region Operators 

        public static ELLSparseMatrix<T> operator +(ELLSparseMatrix<T> A, ELLSparseMatrix<T> B)
        {
            if (A is null)
            {
                throw new ArgumentNullException(nameof(A));
            }
            return A.Add(B, ProviderFactory.GetDefaultProvider<T>());
        }
        public static ELLSparseMatrix<T> operator -(ELLSparseMatrix<T> A, ELLSparseMatrix<T> B)
        {
            if (A is null)
            {
                throw new ArgumentNullException(nameof(A));
            }
            return A.Subtract(B, ProviderFactory.GetDefaultProvider<T>());
        }
        public static ELLSparseMatrix<T> operator -(ELLSparseMatrix<T> A)
        {
            if (A is null)
            {
                throw new ArgumentNullException(nameof(A));
            }
            return A.Negate(ProviderFactory.GetDefaultBLAS1<T>());
        }
        public static ELLSparseMatrix<T> operator *(ELLSparseMatrix<T> A, T scalar)
        {
            if (A is null)
            {
                throw new ArgumentNullException(nameof(A));
            }
            return A.Multiply(scalar, ProviderFactory.GetDefaultBLAS1<T>());
        }
        public static ELLSparseMatrix<T> operator *(T scalar, ELLSparseMatrix<T> A) => A * scalar;
        public static CSRSparseMatrix<T> operator *(ELLSparseMatrix<T> A, ELLSparseMatrix<T> B)
        {
            if (A is null)
            {
                throw new ArgumentNullException(nameof(A));
            }
            return A.Multiply(B, ProviderFactory.GetDefaultProvider<T>());
        }

        #endregion


        #region Casts 

        public static explicit operator ELLSparseMatrix<T>(CSRSparseMatrix<T> A) => new ELLSparseMatrix<T>(A);

        #endregion
    }
}
