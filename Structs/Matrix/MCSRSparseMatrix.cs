﻿using LinearNet.Global;
using LinearNet.Providers;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

namespace LinearNet.Structs
{
    /// <summary>
    /// <para>
    /// Implements a modified compressed sparse row matrix (MCSR or MSR), used to efficiently represent 
    /// sparse, square matrices that have a dense leading diagonal (frequently encountered in PDE problems).
    /// </para>
    /// 
    /// <para>
    /// This sparse representation stores two arrays. For a matrix of dimension $m \times n$ with $k$ 
    /// non-zero off-diagonal elements, the arrays stored are:
    /// <ol>
    /// <li>
    /// An array of matrix elements of length $m + k + 1$. The first $m$ elements are the entries along the 
    /// leading diagonal, in order. The $(m + 1)$-th element is ignored. Traditionally this was used to store 
    /// the length of the array, but this implementation keeps track of array lengths separately. 
    /// The next $k_1$ entries are off-diagonal elements arranged in row-major order 
    /// (elements from the same row are arranged from left to right). 
    /// </li>
    /// 
    /// <li>
    /// An array containing the column indices, also of length $m + k + 1$. The first $m$ elements keep
    /// track of the first index in the value array, amongst all off-diagonal entries, in each of the $m$ rows of the matrix. 
    /// The next element keeps track of the total number of non-zero off-diagonal entries. The last $k$ elements 
    /// store the column index of each of the $k$ off-diagonal elements.
    /// </li>
    /// </ol>
    /// </para>
    /// </summary>
    public class MCSRSparseMatrix<T> : Matrix<T> where T : new()
    {
        private readonly T _zero;

        private T[] _values;
        private int[] _indices;

        internal T[] Values { get { return _values; } }
        internal int[] Indices { get { return _indices; } }

        public override T[] this[int index] 
        { 
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }
        public override T this[int rowIndex, int columnIndex] 
        { 
            get
            {
                if (rowIndex < 0 || rowIndex >= Rows)
                {
                    throw new ArgumentOutOfRangeException(nameof(rowIndex));
                }
                if (columnIndex < 0 || columnIndex >= Columns)
                {
                    throw new ArgumentOutOfRangeException(nameof(columnIndex));
                }

                // Diagonal values are stored first
                if (rowIndex == columnIndex)
                {
                    return _values[rowIndex];
                }

                // search for columnIndex in the appropriate range
                int row_start = _indices[rowIndex], row_end = _indices[rowIndex + 1];
                for (int i = row_start; i < row_end; ++i)
                {
                    if (_indices[i] == columnIndex)
                    {
                        return _values[i];
                    }
                }

                return _zero;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public override int SymbolicNonZeroCount => _indices[Rows] - 1;

        public override IEnumerable<MatrixEntry<T>> NonZeroEntries
        {
            get
            {
                // Diagonal entries first
                for (int i = 0; i < Rows; ++i)
                {
                    yield return new MatrixEntry<T>(i, i, _values[i]);
                }

                // Off diagonal entries
                for (int r = 0; r < Rows; ++r)
                {
                    int end = _indices[r + 1];
                    for (int i = _indices[r]; i < end; ++i)
                    {
                        yield return new MatrixEntry<T>(r, _indices[i], _values[i]);
                    }
                }
            }
        }


        #region Constructors

        /// <summary>
        /// Create an empty square matrix of the specified dimension.
        /// </summary>
        /// <param name="dimension">The row and column dimension of the matrix.</param>
        public MCSRSparseMatrix(int dimension) : base(MatrixStorageType.MCSR, dimension, dimension)
        {
            _zero = new T();
            _values = RectangularVector.Repeat(_zero, dimension + 1);
            _indices = RectangularVector.Repeat(dimension + 1, dimension + 1);
        }
        /// <summary>
        /// Copy constructor.
        /// </summary>
        /// <param name="matrix"></param>
        public MCSRSparseMatrix(BandMatrix<T> matrix) : base(MatrixStorageType.MCSR)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            if (!matrix.IsSquare)
            {
                throw new ArgumentOutOfRangeException(nameof(matrix), ExceptionMessages.MatrixNotSquare);
            }

            int dim = matrix.Rows;
            Rows = dim;
            Columns = dim;
            _zero = new T();

            T[][] upperBands = matrix.UpperBands, lowerBands = matrix.LowerBands;
            int ubw = matrix.UpperBandwidth, lbw = matrix.LowerBandwidth;

            // Count number of off-diagonal non-zero elements
            int count = 0;
            for (int b = 0; b < ubw; ++b)
            {
                T[] band = upperBands[b];
                int len = band.Length;
                for (int i = 0; i < len; ++i)
                {
                    if (!band[i].Equals(_zero))
                    {
                        count++;
                    }
                }
            }
            for (int b = 0; b < lbw; ++b)
            {
                T[] band = lowerBands[b];
                int len = band.Length;
                for (int i = 0; i < len; ++i)
                {
                    if (!band[i].Equals(_zero))
                    {
                        count++;
                    }
                }
            }

            int offset = dim + 1;
            _values = new T[offset + count];
            _indices = new int[offset + count];

            Array.Copy(matrix.Diagonal, 0, _values, 0, dim);

            int k = offset;
            for (int r = 0; r < dim; ++r)
            {
                // Lower bands on row r
                for (int b = lbw - 1; b >= 0; --b)
                {
                    int index = r - b - 1;
                    if (0 <= index)
                    {
                        T e = lowerBands[b][index];
                        if (!e.Equals(_zero))
                        {
                            _values[k] = e;
                            _indices[k] = index;
                            ++k;
                            _indices[r + 1]++;
                        }
                    }
                }

                // upper bands on row r
                for (int b = 0; b < ubw; ++b)
                {
                    int c = r + b + 1;
                    if (c < dim)
                    {
                        T e = upperBands[b][r];
                        if (!e.Equals(_zero))
                        {
                            _values[k] = e;
                            _indices[k] = c;
                            ++k;
                            _indices[r + 1]++;
                        }
                    }
                }
            }

            // Accumulate row indices
            _indices[0] = offset;
            for (int r = 1; r <= dim; ++r)
            {
                _indices[r] += _indices[r - 1];
            }
        }
        /// <summary>
        /// Create a MCSR matrix using the values of a <txt>BlockDiagonalMatrix</txt>.
        /// </summary>
        /// <param name="matrix"></param>
        public MCSRSparseMatrix(BlockDiagonalMatrix<T> matrix) : base(MatrixStorageType.MCSR)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            if (!matrix.IsSquare)
            {
                throw new InvalidOperationException("Matrix cannot be represented as a Modified Compressed Sparse Row matrix because it is not square.");
            }
            Rows = matrix.Rows;
            Columns = matrix.Columns;
            _zero = new T();

            int offDiagCount = 0, nBlocks = matrix.Blocks.Length;
            for (int b = 0; b < nBlocks; ++b)
            {
                int dim = matrix.Blocks[b].Rows;
                offDiagCount += (dim * dim - dim);
            }

            _indices = new int[offDiagCount + 1 + Rows];
            _values = new T[_indices.Length];

            // First pass - count the number of non-zeroes per row
            for (int b = 0; b < nBlocks; ++b)
            {
                DenseMatrix<T> block = matrix.Blocks[b];
                int start = matrix.BlockIndices[b] + 1, // include shift offset
                    end = start + block.Rows + 1, // include shift offset
                    nz = block.Rows - 1;
                for (int r = start; r < end; ++r)
                {
                    _indices[r] = nz;
                }
            }

            // Accumulate (already shifted)
            for (int r = 0; r < Rows; ++r)
            {
                _indices[r + 1] += _indices[r];
            }

            int[] next = new int[Rows];
            Array.Copy(_indices, next, Rows);

            // 2nd pass - fill in numeric values
            for (int b = 0; b < nBlocks; ++b)
            {
                DenseMatrix<T> block = matrix.Blocks[b];
                int offset = matrix.BlockIndices[b],
                    dim = block.Rows;

                for (int r = 0; r < dim; ++r)
                {
                    T[] row = block.Values[r];
                    int rowIndex = r + offset;
                    for (int c = 0; c < dim; ++c)
                    {
                        if (r == c)
                        {
                            _values[rowIndex] = row[c];
                        }
                        else
                        {
                            int index = next[rowIndex]++;
                            _indices[index] = c + offset;
                            _values[index] = row[c];
                        }
                    }
                }
            }
        }
        public MCSRSparseMatrix(COOSparseMatrix<T> matrix) : base(MatrixStorageType.MCSR)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            if (!matrix.IsSquare)
            {
                throw new ArgumentOutOfRangeException(nameof(matrix), ExceptionMessages.MatrixNotSquare);
            }

            Rows = matrix.Rows;
            Columns = matrix.Columns;
            _zero = new T();

            int dim = Rows;
            List<MatrixEntry<T>> elements = matrix.Values;

            // Count the number of off-diagonal terms
            int k = elements.Count;
            int count = 0;
            for (int i = 0; i < k; ++i)
            {
                MatrixEntry<T> e = elements[i];
                if (e.RowIndex != e.ColumnIndex)
                {
                    count++;
                }
            }

            _values = new T[dim + count + 1];
            _indices = new int[dim + count + 1];

            if (!matrix.IsSorted)
            {
                // Still conflicted as to the best course of action
                // this copy operation requires both computational effort and
                // memory, which can be avoided by modifying the matrix value
                // list itself. However that would mean having to modify the 
                // matrix from within another classes' constructor.
                List<MatrixEntry<T>> copy = new List<MatrixEntry<T>>(elements);
                copy.Sort();
                elements = copy;
            }

            // From here onwards, can treat the elements in sorted order
            int offset = dim + 1;
            for (int i = 0, j = offset; i < k; ++i)
            {
                MatrixEntry<T> e = elements[i];
                if (e.RowIndex == e.ColumnIndex)
                {
                    _values[e.RowIndex] = e.Value;
                }
                else
                {
                    _values[j] = e.Value;
                    _indices[j] = e.ColumnIndex;
                    _indices[e.RowIndex + 1]++; // record the number of elements in each row
                    ++j;
                }
            }

            // Accumulate the number of values in each row
            _indices[0] = offset;
            for (int i = 1; i <= dim; ++i)
            {
                _indices[i] += _indices[i - 1];
            }

            // Final step - in case the default value is not equal to _zero, set diagonal
            // default values to _zero
            if (!_zero.Equals(default(T)))
            {
                T def = default;
                for (int i = 0; i < dim; ++i)
                {
                    if (_values[i].Equals(def))
                    {
                        _values[i] = _zero;
                    }
                }
            }
        }
        public MCSRSparseMatrix(CSRSparseMatrix<T> matrix) : base(MatrixStorageType.MCSR)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            if (!matrix.IsSquare)
            {
                throw new ArgumentOutOfRangeException(nameof(matrix), ExceptionMessages.MatrixNotSquare);
            }

            Rows = matrix.Rows;
            Columns = matrix.Columns;

            int dim = matrix.Rows;

            int[] rowIndices = matrix.RowIndices, colIndices = matrix.ColumnIndices;
            T[] values = matrix.Values;

            // Count the number of off-diagonal non-zero terms
            int count = 0;
            for (int r = 0; r < dim; ++r)
            {
                int row_end = rowIndices[r + 1];
                for (int i = rowIndices[r]; i < row_end; ++i)
                {
                    if (colIndices[i] != r)
                    {
                        ++count;
                    }
                }
            }

            int offset = dim + 1;
            _values = new T[offset + count];
            _indices = new int[offset + count];

            // Loop through a 2nd time to fill in the values and indices
            int k = offset;
            for (int r = 0; r < dim; ++r)
            {
                int row_end = rowIndices[r + 1];
                for (int i = rowIndices[r]; i < row_end; ++i)
                {
                    int c = colIndices[i];
                    if (r == c)
                    {
                        _values[r] = values[i];
                    }
                    else
                    {
                        _values[k] = values[i];
                        _indices[k] = c;
                        ++k;

                        _indices[r + 1]++;
                    }
                }
            }

            // Accumulate the row counts
            _indices[0] = offset;
            for (int r = 1; r <= dim; ++r)
            {
                _indices[r] += _indices[r - 1];
            }

            // Set all default values to zero along the main diagonal
            T def = default;
            if (!def.Equals(_zero))
            {
                for (int r = 0; r < dim; ++r)
                {
                    if (_values[r].Equals(def))
                    {
                        _values[r] = _zero;
                    }
                }
            }
        }
        public MCSRSparseMatrix(CSCSparseMatrix<T> matrix) : base(MatrixStorageType.MCSR)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            if (!matrix.IsSquare)
            {
                throw new ArgumentOutOfRangeException(nameof(matrix), ExceptionMessages.MatrixNotSquare);
            }

            int dim = matrix.Rows;
            Rows = dim;
            Columns = dim;
            _zero = new T();

            int[] colIndices = matrix.ColumnIndices;
            int[] rowIndices = matrix.RowIndices;
            T[] values = matrix.Values;

            int[] off_rowCounts = new int[Rows];
            int off_nnz = 0;
            for (int c = 0; c < dim; ++c)
            {
                int col_end = colIndices[c + 1];
                for (int i = colIndices[c]; i < col_end; ++i)
                {
                    int r = rowIndices[i];
                    if (r != c)
                    {
                        ++off_nnz;
                        off_rowCounts[r]++;
                    }
                }
            }

            _indices = new int[Rows + 1 + off_nnz];
            _values = new T[Rows + 1 + off_nnz];

            // Check if zero-initialization is required
            if (!_zero.Equals(default))
            {
                for (int i = 0; i <= Rows; ++i)
                {
                    _values[i] = _zero;
                }
            }

            _indices[0] = dim + 1;
            for (int r = 0; r < dim; ++r)
            {
                _indices[r + 1] = _indices[r] + off_rowCounts[r];
            }

            int[] counts = new int[Rows];
            for (int c = 0; c < dim; ++c)
            {
                int col_end = colIndices[c + 1];
                for (int i = colIndices[c]; i < col_end; ++i)
                {
                    int r = rowIndices[i];
                    if (r != c)
                    {
                        int index = _indices[r] + counts[r];
                        _values[index] = values[i];
                        _indices[index] = c;
                        counts[r]++;
                    }
                    else
                    {
                        _values[r] = values[i];
                    }
                }
            }
        }
        public MCSRSparseMatrix(DenseMatrix<T> matrix) : base(MatrixStorageType.MCSR)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            if (!matrix.IsSquare)
            {
                throw new ArgumentOutOfRangeException(nameof(matrix), ExceptionMessages.MatrixNotSquare);
            }

            Rows = matrix.Rows;
            Columns = matrix.Columns;
            _zero = new T();

            int dim = Rows;

            // Count the number of off-diagonal non-zero elements
            int count = 0;
            for (int r = 0; r < dim; ++r)
            {
                T[] row = matrix[r];
                for (int c = 0; c < dim; ++c)
                {
                    if (r != c && !row[c].Equals(_zero))
                    {
                        ++count;
                    }
                }
            }

            int offset = dim + 1;
            _values = new T[offset + count];
            _indices = new int[offset + count];

            int k = offset;
            for (int r = 0; r < dim; ++r)
            {
                T[] row = matrix[r];
                for (int c = 0; c < dim; ++c)
                {
                    if (r == c)
                    {
                        _values[r] = row[c];
                    }
                    else
                    {
                        if (!row[c].Equals(_zero))
                        {
                            _values[k] = row[c];
                            _indices[k] = c;
                            ++k;

                            _indices[r + 1]++;
                        }
                    }
                }
            }

            // Accumulate row indices
            _indices[0] = offset;
            for (int r = 1; r <= dim; ++r)
            {
                _indices[r] += _indices[r - 1];
            }

            // No need to replace default values with zero, since all diagonals are already filled in
        }
        public MCSRSparseMatrix(DiagonalMatrix<T> matrix) : base(MatrixStorageType.MCSR)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            if (!matrix.IsSquare)
            {
                throw new ArgumentOutOfRangeException(nameof(matrix), ExceptionMessages.MatrixNotSquare);
            }

            int dim = matrix.Rows;
            Rows = dim;
            Columns = dim;

            // Keep as zero - no off-diagonal terms
            _indices = new int[dim + 1];
            _values = new T[dim + 1];
            _zero = new T();

            Array.Copy(matrix.DiagonalTerms, 0, _values, 0, dim);
        }
        public MCSRSparseMatrix(DOKSparseMatrix<T> matrix) : base(MatrixStorageType.MCSR)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            if (!matrix.IsSquare)
            {
                throw new ArgumentOutOfRangeException(nameof(matrix), ExceptionMessages.MatrixNotSquare);
            }

            Rows = matrix.Rows;
            Columns = matrix.Columns;
            _zero = new T();

            // Count the number of off-diagonal terms 
            int count = 0;
            long lc = Columns;
            Dictionary<long, T> entries = matrix.Values;
            foreach (long e in entries.Keys)
            {
                long r = e / lc, c = e % lc;
                if (r != c)
                {
                    ++count;
                }
            }

            int dim = Rows;
            _values = new T[dim + count + 1];
            _indices = new int[dim + count + 1];

            // Store diagonal elements first
            for (int r = 0; r < Rows; ++r)
            {
                long key = r * lc + r;
                if (entries.ContainsKey(key))
                {
                    _values[r] = entries[key];
                }
                else
                {
                    _values[r] = _zero;
                }
            }

            // Store off-diagonal elements
            List<long> keys = new List<long>(entries.Keys);
            keys.Sort();

            int k = keys.Count;
            int offset = dim + 1;
            for (int i = 0, j = 0; i < k; ++i)
            {
                long key = keys[i];
                int r = (int)(key / lc), c = (int)(key % lc);

                // Skip diagonal terms, they have already been recorded
                if (r == c)
                {
                    continue;
                }

                // Record the column index and value
                int index = offset + j++;
                _indices[index] = c;
                _values[index] = entries[key];

                // Record the number of elements in this row
                _indices[r + 1]++;
            }

            // Accumulate the number of elements in each row
            _indices[0] = offset;
            for (int r = 1; r <= dim; ++r)
            {
                _indices[r] += _indices[r - 1];
            }
        }
        /// <summary>
        /// Copy constructor.
        /// </summary>
        /// <param name="matrix"></param>
        public MCSRSparseMatrix(MCSRSparseMatrix<T> matrix) : base(MatrixStorageType.MCSR)
        {
            Rows = matrix.Rows;
            Columns = matrix.Columns;
            _zero = matrix._zero;
            _values = matrix._values.Copy();
            _indices = matrix._indices.Copy();
        }
        public MCSRSparseMatrix(Matrix<T> matrix) : base(MatrixStorageType.MCSR)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            Rows = matrix.Rows;
            Columns = matrix.Columns;
            _zero = new T();

            IEnumerable<MatrixEntry<T>> values = matrix.NonZeroEntries;
            int[] next = new int[Rows + 1];

            // 1st pass to count the number of off-diagonal terms 
            // in each row (store in next)
            foreach (MatrixEntry<T> e in values)
            {
                if (e.RowIndex != e.ColumnIndex)
                {
                    next[e.RowIndex]++;
                }
            }

            // Shift and accumulate
            for (int r = Rows - 1; r >= 0; --r)
            {
                next[r + 1] = next[r];
            }
            next[0] = 0;
            for (int r = 0; r < Rows; ++r)
            {
                next[r + 1] += next[r];
            }

            int len = Rows + 1 + next[Rows];
            _indices = new int[len];
            _values = RectangularVector.Zeroes<T>(len);

            Array.Copy(next, _indices, Rows);

            // 2nd pass is to insert the values into storage
            foreach (MatrixEntry<T> e in values)
            {
                if (e.RowIndex == e.ColumnIndex)
                {
                    _values[e.RowIndex] = e.Value;
                }
                else
                {
                    int index = next[e.RowIndex]++;
                    _indices[index] = e.ColumnIndex;
                    _values[index] = e.Value;
                }
            }
        }
        /// <summary>
        /// Create a MCSR matrix given two arrays of size $m + k + 1$ where $m$ is the matrix size and
        /// $k$ is the number of off-diagonal symbolic non-zeroes. 
        /// 
        /// <para>
        /// For the first $m$ entries of the <txt>indices</txt>,
        /// the $i$-th value represents the index of the first entry in the <txt>indices</txt> array that belongs to 
        /// row $i$ of the matrix. The $(m + 1)$-th entry of <txt>indices</txt> is the total number of non-zeroes in 
        /// the matrix. The remaining $k$ entries of <txt>indices</txt> represent the column indexes for each of the $k$
        /// off-diagonal non-zeroes (the column index of diagonal non-zeroes are not stored).  
        /// </para>
        /// 
        /// <para>
        /// The first $m$ entries of <txt>values</txt> represent the diagonal terms of the matrix, in order. Zeroes 
        /// along the main diagonal are explicitly stored. The $(m + 1)$-th entry is not used. The remaining $k$ entries
        /// represent the numeric value of each of the $k$ off-diagonal non-zeroes, respectively. 
        /// </para>
        /// </summary>
        /// <param name="rows">The number of rows of this matrix, $m$.</param>
        /// <param name="indices">The array of indexes of size $m + k + 1$.</param>
        /// <param name="values">The array of values of size $m + k + 1$.</param>
        public MCSRSparseMatrix(int rows, int[] indices, T[] values) : base(MatrixStorageType.MCSR)
        {
            // TODO argument checks
            Rows = rows;
            Columns = rows;
            _zero = new T();
            _indices = indices;
            _values = values;
        }

        #endregion


        #region Boolean methods

        public override bool Equals(object obj)
        {
            if (obj is null)
            {
                return false;
            }
            if (obj is MCSRSparseMatrix<T>)
            {
                return Equals(obj as MCSRSparseMatrix<T>);
            }
            if (obj is CSRSparseMatrix<T>)
            {
                return Equals(obj as CSRSparseMatrix<T>);
            }
            return Equals(obj as Matrix<T>);
        }

        public bool Equals(MCSRSparseMatrix<T> matrix, Func<T, T, bool> equals)
        {
            if (matrix is null)
            {
                return false;
            }
            if (Rows != matrix.Rows || Columns != matrix.Columns)
            {
                return false;
            }

            // Compare diagonal elements first
            for (int r = 0; r < Rows; ++r)
            {
                if (!equals(_values[r], matrix._values[r]))
                {
                    return false;
                }
            }

            // Compare off-diagonal terms
            for (int r = 0; r < Rows; ++r)
            {
                int r1 = _indices[r], row_end_1 = _indices[r + 1],
                    r2 = matrix._indices[r], row_end_2 = matrix._indices[r + 1];

                while (r1 < row_end_1 || r2 < row_end_2)
                {
                    int c1 = r1 < row_end_1 ? _indices[r1] : int.MaxValue;
                    int c2 = r2 < row_end_2 ? matrix._indices[r2] : int.MaxValue;

                    if (c1 > c2)
                    {
                        if (!equals(matrix._values[r2], _zero))
                        {
                            return false;
                        }
                        ++r2;
                    }
                    else if (c1 < c2)
                    {
                        if (!equals(_values[r1], _zero))
                        {
                            return false;
                        }
                        ++r1;
                    }
                    else
                    {
                        if (!equals(_values[r1], matrix._values[r2]))
                        {
                            return false;
                        }
                        ++r1;
                        ++r2;
                    }
                }
            }

            return true;
        }
        public bool Equals(MCSRSparseMatrix<T> matrix, IEqualityComparer<T> comparer)
        {
            if (comparer is null)
            {
                throw new ArgumentNullException(nameof(comparer));
            }
            return Equals(matrix, comparer.Equals);
        }
        public bool Equals(MCSRSparseMatrix<T> matrix)
        {
            return Equals(matrix, EqualityComparer<T>.Default);
        }

        public bool Equals(CSRSparseMatrix<T> matrix, Func<T, T, bool> equals)
        {
            if (equals is null)
            {
                throw new ArgumentNullException(nameof(equals));
            }

            if (matrix is null)
            {
                return false;
            }
            if (Rows != matrix.Rows || Columns != matrix.Columns)
            {
                return false;
            }

            int[] rowIndices = matrix.RowIndices, colIndices = matrix.ColumnIndices;
            T[] values = matrix.Values;

            // Iterate through the rows 
            for (int r = 0; r < Rows; ++r)
            {
                int r1 = _indices[r], row_end_1 = _indices[r + 1],
                    r2 = rowIndices[r], row_end_2 = rowIndices[r + 1];

                while (r1 < row_end_1 || r2 < row_end_2)
                {
                    int c1 = r1 < row_end_1 ? _indices[r1] : int.MaxValue;
                    int c2 = r2 < row_end_2 ? rowIndices[r2] : int.MaxValue;

                    // c1 will never be equal to r, but c2 can be - handle this case first
                    if (c2 == r)
                    {
                        if (!equals(_values[r], values[r2]))
                        {
                            return false;
                        }
                        ++r2;
                        continue;
                    }

                    if (c1 > c2)
                    {
                        if (!equals(values[r2], _zero))
                        {
                            return false;
                        }
                        ++r2;
                    }
                    else if (c1 < c2)
                    {
                        if (!equals(_values[r1], _zero))
                        {
                            return false;
                        }
                        ++r1;
                    }
                    else
                    {
                        if (!equals(_values[r1], values[r2]))
                        {
                            return false;
                        }
                        ++r1;
                        ++r2;
                    }
                }
            }
            return true;
        }
        public bool Equals(CSRSparseMatrix<T> matrix, IEqualityComparer<T> comparer)
        {
            if (comparer is null)
            {
                throw new ArgumentNullException(nameof(comparer));
            }
            return Equals(matrix, comparer.Equals);
        }
        public bool Equals(CSRSparseMatrix<T> matrix)
        {
            return Equals(matrix, EqualityComparer<T>.Default);
        }

        public override bool IsLower(Predicate<T> isZero)
        {
            if (isZero is null)
            {
                throw new ArgumentNullException(nameof(isZero));
            }

            // Ignore diagonal terms
            int r, i, c, row_end;
            for (r = 0; r < Rows; ++r)
            {
                row_end = _indices[r + 1];
                for (i = _indices[r]; i < row_end; ++i)
                {
                    c = _indices[i];
                    if (r < c && !isZero(_values[i]))
                    {
                        return false;
                    }
                }
            }
            return false;
        }

        public override bool IsUpper(Predicate<T> isZero)
        {
            if (isZero is null)
            {
                throw new ArgumentNullException(nameof(isZero));
            }

            int r, i, c, row_end;
            for (r = 0; r < Rows; ++r)
            {
                row_end = _indices[r + 1];
                for (i = _indices[r]; i < row_end; ++i)
                {
                    c = _indices[i];
                    if (r > c && !isZero(_values[i]))
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        public override bool IsDiagonal(Predicate<T> isZero)
        {
            if (_values.Length <= Rows + 1)
            {
                return true;
            }

            if (isZero is null)
            {
                throw new ArgumentNullException(nameof(isZero));
            }

            for (int i = Rows + 1; i < _values.Length; ++i)
            {
                if (!isZero(_values[i]))
                {
                    return false;
                }
            }

            return true;
        }

        public override bool IsBanded(int lowerBandwidth, int upperBandwidth, Predicate<T> isZero)
        {
            if (isZero is null)
            {
                throw new ArgumentNullException(nameof(isZero));
            }

            for (int r = 0; r < Rows; ++r)
            {
                int end = _indices[r + 1];
                for (int i = _indices[r]; i < end; ++i)
                {
                    int c = _indices[i];
                    if (c > r)
                    {
                        if (c - r > upperBandwidth && !isZero(_values[i]))
                        {
                            return false;
                        }
                    }
                    else
                    {
                        if (r - c > lowerBandwidth && !isZero(_values[i]))
                        {
                            return false;
                        }
                    }
                }
            }
            return true;
        }

        public override bool IsZero(Predicate<T> isZero)
        {
            if (isZero is null)
            {
                throw new ArgumentNullException(nameof(isZero));
            }

            for (int i = 0; i < _values.Length; ++i)
            {
                if (!isZero(_values[i]))
                {
                    return false;
                }
            }
            return true;
        }

        public override bool IsIdentity(Predicate<T> isZero, Predicate<T> isOne)
        {
            // Rredundant check
            if (!IsSquare)
            {
                return false;
            }

            if (isZero is null)
            {
                throw new ArgumentNullException(nameof(isZero));
            }
            if (isOne is null)
            {
                throw new ArgumentNullException(nameof(isOne));
            }

            if (!IsDiagonal(isZero))
            {
                return false;
            }

            for (int r = 0; r < Rows; ++r)
            {
                if (!isOne(_values[r]))
                {
                    return false;
                }
            }
            return true;
        }

        #endregion


        #region Public methods

        public override void Clear()
        {
            int len = Rows + 1;
            _values = RectangularVector.Repeat(_zero, len);
            _indices = RectangularVector.Repeat(len, len);
        }

        public override void Clear(Predicate<T> select)
        {
            if (select is null)
            {
                throw new ArgumentNullException(nameof(select));
            }

            Clear(e => select(e.Value), true);
        }

        public override void ClearRow(int rowIndex)
        {
            if (rowIndex < 0 || rowIndex >= Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(rowIndex));
            }

            // defrag check?
            int row_start = _indices[rowIndex], row_end = _indices[rowIndex + 1],
                count = row_end - row_start;

            int[] indices = new int[_indices.Length - count];
            T[] values = new T[_values.Length - count];

            Array.Copy(_indices, indices, row_start);
            Array.Copy(_indices, row_end, indices, row_start, indices.Length - row_start);

            Array.Copy(_values, values, row_start);
            Array.Copy(_values, row_end, values, row_start, values.Length - row_start);

            // Update row indices
            for (int r = rowIndex + 1; r <= Rows; ++r)
            {
                indices[r] -= count;
            }

            _indices = indices;
            _values = values;
        }

        public override void ClearColumn(int columnIndex)
        {
            if (columnIndex < 0 || columnIndex >= Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(columnIndex));
            }

            Clear(e => e.ColumnIndex == columnIndex, true);
        }

        public override void ClearSubmatrix(int rowIndex, int columnIndex, int rows, int columns)
        {
            if (rowIndex < 0 || rowIndex >= Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(rowIndex));
            }
            if (columnIndex < 0 || columnIndex >= Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(columnIndex));
            }

            int bottom = rowIndex + rows, right = columnIndex + columns;
            if (rows < 0 || bottom >= Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(rows));
            }
            if (columns < 0 || right >= Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(columns));
            }

            Clear(e => rowIndex <= e.RowIndex && e.RowIndex < bottom && columnIndex <= e.ColumnIndex && e.ColumnIndex < right, true);
        }

        public override void ClearLower()
        {
            Clear(e => e.RowIndex > e.ColumnIndex, true);
        }

        public override void ClearUpper()
        {
            Clear(e => e.RowIndex < e.ColumnIndex, true);
        }

        public void Clear(Predicate<MatrixEntry<T>> predicate, bool defrag)
        {
            // Clearning diagonal terms are the same regardless of whether defrag is used
            for (int r = 0; r < Rows; ++r)
            {
                if (predicate(new MatrixEntry<T>(r, r, _values[r])))
                {
                    _values[r] = _zero;
                }
            }

            if (defrag)
            {
                // Keep track of which entries to keep, since evaluation of the 
                // predicate is potentially expensive and we want to avoid re-evaluations
                BitArray keep = new BitArray(_values.Length);

                int nnz = Rows + 1;
                for (int r = 0; r < Rows; ++r)
                {
                    int row_end = _indices[r + 1];
                    for (int i = _indices[r]; i < row_end; ++i)
                    {
                        if (!predicate(new MatrixEntry<T>(r, _indices[i], _values[i])))
                        {
                            keep[i] = true;
                            ++nnz;
                        }
                    }
                }

                // Create new storage
                int[] indices = new int[nnz];
                T[] values = new T[nnz];

                int k = Rows + 1, start, end = 0;
                for (int r = 0; r < Rows; ++r)
                {
                    start = end;
                    end = _indices[r + 1];

                    for (int i = start; i < end; ++i)
                    {
                        if (keep[i])
                        {
                            indices[k] = _indices[i];
                            values[k] = _values[i];
                            ++k;
                        }
                    }
                    indices[r + 1] = k;
                }

                _indices = indices;
                _values = values;
            }
            else
            {
                for (int r = 0; r < Rows; ++r)
                {
                    int row_end = _indices[r + 1];
                    for (int i = _indices[r]; i < row_end; ++i)
                    {
                        if (predicate(new MatrixEntry<T>(r, _indices[i], _values[i])))
                        {
                            _values[i] = _zero;
                        }
                    }
                }
            }

        }

        public override object Clone()
        {
            return Copy();
        }
        public MCSRSparseMatrix<T> Copy()
        {
            return new MCSRSparseMatrix<T>(Rows, _indices.Copy(), _values.Copy());
        }

        public override int Count(Predicate<T> predicate)
        {
            if (predicate is null)
            {
                throw new ArgumentNullException(nameof(predicate));
            }

            int count, i, len = _values.Length, skip = Rows;
            if (predicate(_zero))
            {
                count = Rows * Columns - len + 1; // there is a redundant entry in the values array
            }
            else
            {
                count = 0;
            }

            for (i = 0; i < len; ++i)
            {
                if (i == skip) continue;
                if (predicate(_values[i]))
                {
                    ++count;
                }
            }
            return count;
        }

        public override T[] LeadingDiagonal()
        {
            T[] diag = new T[Math.Min(Rows, Columns)];
            Array.Copy(_values, 0, diag, 0, diag.Length);
            return diag;
        }

        public override DenseVector<T> RowSums(IProvider<T> provider)
        {
            // Initialize to diagonal
            T[] sums = new T[Rows];
            for (int r = 0; r < Rows; ++r)
            {
                // Initialize to the diagonal term
                T sum = _values[r];

                int row_end = _indices[r + 1];
                for (int i = _indices[r]; i < row_end; ++i)
                {
                    sum = provider.Add(sum, _values[i]);
                }
                sums[r] = sum;
            }

            return new DenseVector<T>(sums);
        }
        public override DenseVector<T> ColumnSums(IProvider<T> provider)
        {
            int dim = Columns;

            T[] sums = new T[dim];
            Array.Copy(_values, 0, sums, 0, dim);

            for (int r = 0; r < dim; ++r)
            {
                int row_end = _indices[r + 1];
                for (int i = _indices[r]; i < row_end; ++i)
                {
                    int c = _indices[i];
                    sums[c] = provider.Add(sums[c], _values[i]);
                }
            }

            return new DenseVector<T>(sums);
        }

        internal override bool SolveTriangular(T[] b, T[] x, IDenseBLAS1<T> blas, bool lower, bool transpose)
        {
            // Matrix is guaranteed to be square
            int dim = Rows;
            IProvider<T> provider = blas.Provider;

            if (lower)
            {
                if (transpose)
                {
                    Array.Copy(b, 0, x, 0, dim);

                    int col_start = _indices[dim], col_end;
                    for (int c = dim - 1; c >= 0; --c)
                    {
                        col_end = col_start;
                        col_start = _indices[c];

                        T diag = _values[c];
                        if (provider.Equals(diag, _zero)) return false;

                        T xc = provider.Divide(x[c], diag);
                        x[c] = xc;

                        for (int i = col_start; i < col_end; ++i)
                        {
                            int r = _indices[i];
                            if (r >= c) break;

                            x[r] = provider.Subtract(x[r], provider.Multiply(xc, _values[i]));
                        }
                    }
                    return true;
                }
                else
                {
                    int row_start, row_end = _indices[0];
                    for (int r = 0; r < dim; ++r)
                    {
                        T xr = b[r];

                        row_start = row_end;
                        row_end = _indices[r + 1];

                        for (int i = row_start; i < row_end; ++i)
                        {
                            int c = _indices[i];
                            if (c > r)
                            {
                                break;
                            }
                            xr = provider.Subtract(xr, provider.Multiply(_values[i], x[c]));
                        }

                        T diag = _values[r];
                        if (provider.Equals(diag, _zero))
                        {
                            return false;
                        }
                        x[r] = provider.Divide(xr, diag);
                    }
                    return true;
                }
            }
            else
            {
                if (transpose)
                {
                    Array.Copy(b, 0, x, 0, dim);

                    int col_start, col_end = _indices[0];
                    for (int c = 0; c < dim; ++c)
                    {
                        col_start = col_end;
                        col_end = _indices[c + 1];

                        T diag = _values[c];
                        if (provider.Equals(diag, _zero))
                        {
                            return false;
                        }

                        T xc = provider.Divide(x[c], diag);
                        x[c] = xc;

                        for (int i = col_end - 1; i >= col_start; --i)
                        {
                            int r = _indices[i];
                            if (r <= c)
                            {
                                break;
                            }

                            x[r] = provider.Subtract(x[r], provider.Multiply(xc, _values[i]));
                        }
                    }
                    return true;
                }
                else
                {
                    int row_start = _indices[dim], row_end;
                    for (int r = dim - 1; r >= 0; --r)
                    {
                        T xr = b[r];

                        row_end = row_start;
                        row_start = _indices[r];

                        // Iterate backwards until c < r
                        for (int i = row_end - 1; i >= row_start; --i)
                        {
                            int c = _indices[i];
                            if (c < r)
                            {
                                break;
                            }
                            xr = provider.Subtract(xr, provider.Multiply(_values[i], x[c]));
                        }

                        T diag = _values[r];
                        if (provider.Equals(diag, _zero))
                        {
                            return false;
                        }
                        x[r] = provider.Divide(xr, diag);
                    }
                    return true;
                }
            }
        }

        public override void SwapColumns(int columnIndex1, int columnIndex2)
        {
            if (columnIndex1 == columnIndex2)
            {
                return;
            }

            // Enforce columnIndex1 < columnIndex2
            if (columnIndex1 > columnIndex2)
            {
                SwapColumns(columnIndex2, columnIndex1);
                return;
            }

            // Lazy man's implementation - can be improved by first checking if a resize is 
            // required...its on the list of things todo

            throw new NotImplementedException();
        }

        public override void SwapRows(int rowIndex1, int rowIndex2)
        {
            throw new NotImplementedException();
        }

        public override T Trace(IProvider<T> provider)
        {
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }

            T sum = provider.Zero;
            for (int r = 0; r < Rows; ++r)
            {
                sum = provider.Add(sum, _values[r]);
            }
            return sum;
        }

        public MCSRSparseMatrix<T> Transpose()
        {
            // Assumes matrix is a square
            if (Rows != Columns)
            {
                throw new NotImplementedException();
            }

            // lengths should be preserved
            int[] indices = new int[_indices.Length];
            T[] values = new T[_values.Length];

            // The diagonal terms are preserved
            Array.Copy(_values, 0, values, 0, Rows);

            // Record the off-diagonal column count
            // Store count of column r in indices[r]
            for (int r = 0; r < Rows; ++r)
            {
                int row_start = _indices[r], row_end = _indices[r + 1];
                for (int i = row_start; i < row_end; ++i)
                {
                    indices[_indices[i]]++;
                }
            }

            // Shift indices, and calculate the accumulation
            for (int c = Columns - 1; c >= 0; --c)
            {
                indices[c + 1] = indices[c];
            }
            indices[0] = Rows + 1;
            for (int c = 0; c < Columns; ++c)
            {
                indices[c + 1] += indices[c];
            }

            // Store the number of off-diagonal elements encountered so far 
            // in each column
            int[] counts = new int[Columns];

            // Insert into values and indices
            for (int r = 0; r < Rows; ++r)
            {
                int row_start = _indices[r], row_end = _indices[r + 1];
                for (int i = row_start; i < row_end; ++i)
                {
                    int c = _indices[i];
                    int index = indices[c] + counts[c];
                    indices[index] = r;
                    values[index] = _values[i];
                    counts[c]++;
                }
            }

            return new MCSRSparseMatrix<T>(Columns, indices, values);
        }

        public override DenseVector<T> Row(int rowIndex)
        {
            if (rowIndex < 0 || rowIndex >= Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(rowIndex));
            }

            T[] row = RectangularVector.Zeroes<T>(Rows);

            // Insert diagonal term first
            row[rowIndex] = _values[rowIndex];

            int row_end = _indices[rowIndex + 1];
            for (int i = _indices[rowIndex]; i < row_end; ++i)
            {
                row[_indices[i]] = _values[i];
            }

            return new DenseVector<T>(row);
        }

        public override SparseVector<T> SparseRow(int rowIndex)
        {
            if (rowIndex < 0 || rowIndex >= Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(rowIndex));
            }

            int start = _indices[rowIndex], end = _indices[rowIndex + 1],
                nz = end - start + 1;

            int[] indices = new int[nz];
            T[] values = new T[nz];

            int k = 0, i = start;
            for ( ; i < end; ++i)
            {
                int c = _indices[i];
                if (c > rowIndex)
                {
                    break;
                }
                indices[k] = c;
                values[k++] = _values[i];
            }

            // Add diagonal term
            indices[k] = rowIndex;
            values[k++] = _values[rowIndex];

            for (; i < end; ++i)
            {
                indices[k] = _indices[i];
                values[k++] = _values[i];
            }

            return new SparseVector<T>(Columns, indices, values, true);
        }

        public override DenseVector<T> Column(int columnIndex)
        {
            if (columnIndex < 0 || columnIndex >= Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(columnIndex));
            }

            T[] column = RectangularVector.Zeroes<T>(Columns);

            // Insert diagonal entry
            column[columnIndex] = _values[columnIndex];

            for (int r = 0; r < Rows; ++r)
            {
                int row_start = _indices[r],
                    row_end = _indices[r + 1];

                int index = Array.BinarySearch(_indices, row_start, row_end - row_start, columnIndex);
                if (index >= 0)
                {
                    column[r] = _values[index];
                }
            }

            return new DenseVector<T>(column);
        }

        public override SparseVector<T> SparseColumn(int columnIndex)
        {
            if (columnIndex < 0 || columnIndex >= Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(columnIndex));
            }

            List<int> indices = new List<int>();
            List<T> values = new List<T>();

            for (int r = 0; r < Rows; ++r)
            {
                if (r == columnIndex)
                {
                    indices.Add(r);
                    values.Add(_values[r]);
                }
                else
                {
                    int start = _indices[r],
                        end = _indices[r + 1],
                        index = Array.BinarySearch(_indices, start, end - start, columnIndex);

                    if (index >= 0)
                    {
                        indices.Add(r);
                        values.Add(_values[index]);
                    }
                }
            }

            return new SparseVector<T>(Rows, indices.ToArray(), values.ToArray(), true);
        }

        public override Matrix<T> Select(int[] rowSelection, int[] colSelection)
        {
            if (rowSelection is null)
            {
                throw new ArgumentNullException(nameof(rowSelection));
            }
            if (colSelection is null)
            {
                throw new ArgumentNullException(nameof(colSelection));
            }
            ThrowHelper.AssertCollectionInRange(rowSelection, 0, Rows, nameof(rowSelection));
            ThrowHelper.AssertCollectionInRange(colSelection, 0, Columns, nameof(colSelection));

            // Set up lookup array for the index of the column for each row
            int[] cIndex_lookup = new int[Columns];
            BitArray in_row = new BitArray(Columns);

            // Keep track of the number of nonzeroes in each row
            int rlen = rowSelection.Length,
                clen = colSelection.Length;
            int[] row_counts = new int[rlen + 1];

            // Creating a MCSRSparseMatrix will require two loops - once to extract the diagonals
            // and another to extract the non-diagonals in order
            int off_diag_nz = 0;
            for (int ri = 0; ri < rlen; ++ri)
            {
                int r = rowSelection[ri],
                    start = _indices[r],
                    end = _indices[r + 1];

                // Mark entries in this row
                for (int i = start; i < end; ++i)
                {
                    in_row[_indices[i]] = true;
                }

                int row_nz = 0;
                for (int ci = 0; ci < clen; ++ci)
                {
                    int c = colSelection[ci];
                    if (r == c || in_row[c])
                    {
                        if (ri != ci)
                        {
                            ++off_diag_nz;
                            ++row_nz;
                        }
                    }
                }
                row_counts[ri + 1] = row_nz;

                // Unmark entries in this row
                for (int i = start; i < end; ++i)
                {
                    in_row[_indices[i]] = false;
                }
            }

            int[] indices = new int[rlen + off_diag_nz + 1];
            T[] values = new T[indices.Length];

            // Accumulate and store the row counts in indices
            // row_counts will now serve the purpose of keeping
            // track of the "next" index in each row
            row_counts[0] = rlen + 1;
            for (int r = 0; r < rlen; ++r)
            {
                row_counts[r + 1] += row_counts[r];
            }
            Array.Copy(row_counts, indices, rlen + 1);

            // Fill the diagonal with zeroes
            if (!_zero.Equals(default))
            {
                for (int i = 0; i < rlen; ++i)
                {
                    values[i] = _zero;
                }
            }

            // Numeric step
            for (int ri = 0; ri < rlen; ++ri)
            {
                int r = rowSelection[ri],
                    start = _indices[r],
                    end = _indices[r + 1];

                // Mark entries in this row
                for (int i = start; i < end; ++i)
                {
                    int c = _indices[i];
                    in_row[c] = true;
                    cIndex_lookup[c] = i;
                }

                for (int ci = 0; ci < clen; ++ci)
                {
                    int c = colSelection[ci];

                    // Diagonal term in large matrix
                    if (r == c)
                    {
                        // Diagonal term in submatrix
                        if (ri == ci)
                        {
                            values[ri] = _values[r];
                        }
                        // Off-diagonal term in submatrix
                        else
                        {
                            int index = row_counts[ri]++;
                            indices[index] = ci;
                            values[index] = _values[r];
                        }
                    }
                    // Off diagonal term in large matrix
                    else if (in_row[c])
                    {
                        // Diagonal term in submatrix
                        if (ri == ci)
                        {
                            values[ri] = _values[cIndex_lookup[c]];
                        }
                        // Off-diagonal term in submatrix
                        else
                        {
                            int index = row_counts[ri]++;
                            indices[index] = ci;
                            values[index] = _values[cIndex_lookup[c]];
                        }
                    }
                }

                // Unmark entries in this row
                for (int i = start; i < end; ++i)
                {
                    in_row[_indices[i]] = false;
                }
            }

            return new MCSRSparseMatrix<T>(rlen, indices, values);
        }

        public override Vector<T> SelectAsVector(int rowIndex, int[] columnIndices)
        {
            if (rowIndex < 0 || rowIndex >= Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(rowIndex));
            }
            if (columnIndices is null)
            {
                throw new ArgumentNullException(nameof(columnIndices));
            }
            ThrowHelper.AssertCollectionInRange(columnIndices, 0, Columns, nameof(columnIndices));

            // Store row in fixed-size dictionary
            int start = _indices[rowIndex], end = _indices[rowIndex + 1];
            Dictionary<int, T> cIndex_lookup = new Dictionary<int, T>(end - start + 1)
            {
                [rowIndex] = _values[rowIndex] // diagonal term
            };
            for (int i = start; i < end; ++i)
            {
                cIndex_lookup[_indices[i]] = _values[i];
            }

            // Count non-zeroes 
            int nz = 0, clen = columnIndices.Length;
            for (int ci = 0; ci < clen; ++ci)
            {
                if (cIndex_lookup.ContainsKey(columnIndices[ci])) ++nz;
            }

            int[] indices = new int[nz];
            T[] values = new T[nz];
            for (int ci = 0, k = 0; ci < clen; ++ci)
            {
                int c = columnIndices[ci];
                if (cIndex_lookup.ContainsKey(c))
                {
                    indices[k] = ci;
                    values[k++] = cIndex_lookup[c];
                }
            }
            return new SparseVector<T>(clen, indices, values, true);
        }

        public override Vector<T> SelectAsVector(int[] rowIndices, int columnIndex)
        {
            if (rowIndices is null)
            {
                throw new ArgumentNullException(nameof(rowIndices));
            }
            ThrowHelper.AssertCollectionInRange(rowIndices, 0, Rows, nameof(rowIndices));

            if (columnIndex < 0 || columnIndex >= Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(columnIndex));
            }

            int rlen = rowIndices.Length;

            // Might as well use a dense vector because the complexity is going to 
            // be at least O(rlen)
            T[] vector = RectangularVector.Zeroes<T>(rlen);
            for (int ri = 0; ri < rlen; ++ri)
            {
                int r = rowIndices[ri], start = _indices[r], end = _indices[r + 1];
                int index = Array.BinarySearch(_indices, start, end - start, columnIndex);
                if (index >= 0)
                {
                    vector[ri] = _values[index];
                }
            }
            return new DenseVector<T>(vector);
        }

        #endregion


        #region Operations

        private static int CountNnzInUnion(int[] indices1, int[] indices2, int rows)
        {
            int nnz = 0;
            for (int r = 0; r < rows; ++r)
            {
                int r_ = r + 1,
                    r1 = indices1[r], row_end_1 = indices1[r_],
                    r2 = indices2[r], row_end_2 = indices2[r_];

                while (r1 < row_end_1 || r2 < row_end_2)
                {
                    int c1 = r1 < row_end_1 ? indices1[r1] : int.MaxValue;
                    int c2 = r2 < row_end_2 ? indices2[r2] : int.MaxValue;

                    if (c1 > c2)
                    {
                        ++r2;
                    }
                    else if (c1 < c2)
                    {
                        ++r1;
                    }
                    else
                    {
                        ++r1;
                        ++r2;
                    }
                    ++nnz;
                }
            }
            return nnz;
        }

        public override Matrix<T> Add(Matrix<T> matrix, IProvider<T> provider)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            if (CompareRepresentationPriority(this, matrix) < 0)
            {
                return matrix.Add(this, provider); // return the sum as type(matrix)
            }
            return Add(matrix.ToMCSRSparseMatrix(), provider);
        }

        public MCSRSparseMatrix<T> Add(MCSRSparseMatrix<T> matrix, IProvider<T> provider)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            if (matrix.Rows != Rows || matrix.Columns != Columns)
            {
                throw new InvalidOperationException(ExceptionMessages.MatrixSize);
            }

            int[] indices2 = matrix._indices;
            T[] values2 = matrix._values;

            int dim = Rows;
            int off_nnz = CountNnzInUnion(_indices, matrix._indices, dim), 
                len = off_nnz + dim + 1;

            int[] sum_indices = new int[len];
            T[] sum_values = new T[len];

            // Add all off-diagonal terms
            int offset = dim + 1;
            for (int r = 0, k = offset; r < dim; ++r)
            {
                int r1 = _indices[r], row_end_1 = _indices[r + 1],
                    r2 = indices2[r], row_end_2 = indices2[r + 1];

                while (r1 < row_end_1 || r2 < row_end_2)
                {
                    int c1 = r1 < row_end_1 ? _indices[r1] : int.MaxValue;
                    int c2 = r2 < row_end_2 ? indices2[r2] : int.MaxValue;

                    if (c1 > c2)
                    {
                        sum_indices[k] = c2;
                        sum_values[k] = values2[r2++];
                    }
                    else if (c1 < c2)
                    {
                        sum_indices[k] = c1;
                        sum_values[k] = _values[r1++];
                    }

                    // c1 = c2, requires addition
                    else
                    {
                        sum_indices[k] = c1;
                        sum_values[k] = provider.Add(_values[r1++], values2[r2++]);
                    }

                    ++k;
                }
                sum_indices[r + 1] = k;
            }
            sum_indices[0] = offset;

            // Calculate the diagonal values
            for (int r = 0; r < dim; ++r)
            {
                sum_values[r] = provider.Add(_values[r], values2[r]);
            }
            return new MCSRSparseMatrix<T>(dim, sum_indices, sum_values);
        }
        public MCSRSparseMatrix<T> Subtract(MCSRSparseMatrix<T> matrix, IProvider<T> provider)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            if (matrix.Rows != Rows || matrix.Columns != Columns)
            {
                throw new InvalidOperationException(ExceptionMessages.MatrixSize);
            }

            int[] indices2 = matrix._indices;
            T[] values2 = matrix._values;

            int dim = Rows;
            int off_nnz = CountNnzInUnion(_indices, matrix._indices, dim),
                len = off_nnz + dim + 1;

            int[] sum_indices = new int[len];
            T[] sum_values = new T[len];

            // Add all off-diagonal terms
            int offset = dim + 1;
            for (int r = 0, k = offset; r < dim; ++r)
            {
                int r1 = _indices[r], row_end_1 = _indices[r + 1],
                    r2 = indices2[r], row_end_2 = indices2[r + 1];

                while (r1 < row_end_1 || r2 < row_end_2)
                {
                    int c1 = r1 < row_end_1 ? _indices[r1] : int.MaxValue;
                    int c2 = r2 < row_end_2 ? indices2[r2] : int.MaxValue;

                    if (c1 > c2)
                    {
                        sum_indices[k] = c2;
                        sum_values[k] = provider.Negate(values2[r2++]);
                    }
                    else if (c1 < c2)
                    {
                        sum_indices[k] = c1;
                        sum_values[k] = _values[r1++];
                    }

                    // c1 = c2, requires addition
                    else
                    {
                        sum_indices[k] = c1;
                        sum_values[k] = provider.Subtract(_values[r1++], values2[r2++]);
                    }

                    ++k;
                }
                sum_indices[r + 1] = k;
            }
            sum_indices[0] = offset;

            // Calculate the diagonal values
            for (int r = 0; r < dim; ++r)
            {
                sum_values[r] = provider.Subtract(_values[r], values2[r]);
            }
            return new MCSRSparseMatrix<T>(dim, sum_indices, sum_values);
        }

        public MCSRSparseMatrix<T> Multiply(MCSRSparseMatrix<T> matrix, IProvider<T> provider)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            MatrixChecks.AssertMatrixDimensionsForMultiplication(this, matrix);

            T[] values2 = matrix.Values;
            int[] indices2 = matrix.Indices;

            // workspace for storing a row of the computed product
            T[] prod_row = new T[Columns];

            // workspace for holding the index of non-zero entries of the computed row of the product
            int[] prod_row_indices = new int[Columns];

            // The number of non-zero elements in this row
            int nz;

            // workspace for tracking whether each index is contained in the non-zero pattern of this row
            bool[] in_row = new bool[Columns];

            // Storage for the product matrix
            List<int> prod_indices = new List<int>(Rows + 1);
            List<T> prod_values = new List<T>(Rows + 1);

            // Add rows + 1 placeholder entries
            for (int r = 0; r <= Rows; ++r)
            {
                prod_indices.Add(0);
                prod_values.Add(_zero);
            }

            for (int r = 0; r < Rows; ++r)
            {
                nz = 0;
                int row_end = _indices[r + 1];
                for (int i = _indices[r]; i < row_end; ++i)
                {
                    T value = _values[i];

                    // For each non-zero element of this row in matrix A, get its column index
                    // and iterate through the corresponding row in matrix B
                    int k = _indices[i], row_end_B = indices2[k + 1];
                    for (int j = indices2[k]; j < row_end_B; ++j)
                    {
                        int c2 = indices2[j];
                        if (in_row[c2])
                        {
                            prod_row[c2] = provider.Add(prod_row[c2], provider.Multiply(value, values2[j]));
                        }
                        else
                        {
                            prod_row[c2] = provider.Multiply(value, values2[j]);
                            in_row[c2] = true;
                            prod_row_indices[nz++] = c2;
                        }
                    }

                    // Non-diagonal term from A * diagonal term from B
                    if (in_row[k])
                    {
                        prod_row[k] = provider.Add(prod_row[k], provider.Multiply(value, values2[k]));
                    }
                    else
                    {
                        prod_row[k] = provider.Multiply(value, values2[k]);
                        in_row[k] = true;
                        prod_row_indices[nz++] = k;
                    }
                }

                // Diagonal term from A * non-diagonal term from B
                T diag = _values[r];
                int B_row_end = indices2[r + 1];
                for (int j = indices2[r]; j < B_row_end; ++j)
                {
                    int c2 = indices2[j];
                    if (in_row[c2])
                    {
                        prod_row[c2] = provider.Add(prod_row[c2], provider.Multiply(diag, values2[j]));
                    }
                    else
                    {
                        prod_row[c2] = provider.Multiply(diag, values2[j]);
                        in_row[c2] = true;
                        prod_row_indices[nz++] = c2;
                    }
                }

                // Diagonal term from A * diagonal term from B
                if (in_row[r])
                {
                    prod_row[r] = provider.Add(prod_row[r], provider.Multiply(diag, values2[r]));
                }
                else
                {
                    prod_row[r] = provider.Multiply(diag, values2[r]);
                    in_row[r] = true;
                    prod_row_indices[nz++] = r;
                }

                // Sort the column indices
                Array.Sort(prod_row_indices, 0, nz);

                for (int j = 0; j < nz; ++j)
                {
                    int c = prod_row_indices[j];

                    // Diagonal term
                    if (c == r)
                    {
                        prod_values[c] = prod_row[c];
                    }
                    else
                    {
                        prod_values.Add(prod_row[c]);
                        prod_indices.Add(c);
                    }

                    // Reset
                    in_row[c] = false;
                }
                prod_indices[r + 1] = prod_values.Count;
            }

            return new MCSRSparseMatrix<T>(Rows, prod_indices.ToArray(), prod_values.ToArray());
        }

        internal override void Multiply(T[] vector, T[] result, IProvider<T> provider, bool increment = false)
        {
            if (vector is null)
            {
                throw new ArgumentNullException(nameof(vector));
            }
            if (result is null)
            {
                throw new ArgumentNullException(nameof(result));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            if (vector.Length != Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(vector), "The number of columns do not match the dimension of the vector.");
            }
            if (result.Length < Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(result), "The result vector is not large enough to store the matrix-vector product.");
            }

            int dim = Rows;

            // increment all off-diagonal terms
            for (int r = 0; r < dim; ++r)
            {
                int row_end = _indices[r + 1];

                // Set sum to diagonal product
                T sum = provider.Multiply(_values[r], vector[r]);

                // Append the off-diagonal terms
                for (int i = _indices[r]; i < row_end; ++i)
                {
                    sum = provider.Add(sum, provider.Multiply(_values[i], vector[_indices[i]]));
                }

                if (increment)
                {
                    result[r] = provider.Add(result[r], sum);
                }
                else
                {
                    result[r] = sum;
                }
            }
        }

        internal override void TransposeAndMultiply(T[] vector, T[] product, IProvider<T> provider, bool increment)
        {
            // Set the product vector to diagonal * vector
            int dim = Rows;
            if (increment)
            {
                for (int r = 0; r < dim; ++r)
                {
                    product[r] = provider.Add(product[r], provider.Multiply(_values[r], vector[r]));
                }
            }
            else
            {
                for (int r = 0; r < dim; ++r)
                {
                    product[r] = provider.Multiply(_values[r], vector[r]);
                }
            }

            // Increment by all off-diagonal products
            for (int c = 0; c < dim; ++c)
            {
                int row_end = _indices[c + 1];
                T vect_value = vector[c];

                // AXPY operation
                for (int i = _indices[c]; i < row_end; ++i)
                {
                    int r = _indices[i];
                    product[r] = provider.Add(product[r], provider.Multiply(_values[i], vect_value));
                }
            }
        }

        public override SparseVector<T> Multiply(SparseVector<T> vector, IProvider<T> provider)
        {
            if (vector is null)
            {
                throw new ArgumentNullException(nameof(vector));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            if (Columns != vector.Dimension)
            {
                throw new InvalidOperationException("The number of columns of the matrix does not match the dimension of the vector.");
            }

            int[] indices = new int[Rows];
            T[] values = new T[Rows];

            int nz = Multiply(vector, indices, values, provider);

            int[] prod_indices = new int[nz];
            T[] prod_values = new T[nz];

            Array.Copy(indices, 0, prod_indices, 0, nz);
            Array.Copy(values, 0, prod_values, 0, nz);

            return new SparseVector<T>(Rows, indices, values, true);
        }
        
        /// <summary>
        /// Postmultiply this matrix by the sparse vector, recording the product vector implicitly in the arrays
        /// 'productIndices' and 'productValues'. This method returns an integer specifying the number of non-zero elements
        /// recorded in 'productIndices'
        /// </summary>
        internal int Multiply(SparseVector<T> vector, int[] productIndices, T[] productValues, IProvider<T> provider)
        {
            int[] vect_indices = vector.Indices;
            T[] vect_values = vector.Values;

            int k = 0;
            for (int r = 0; r < Rows; ++r)
            {
                int i1 = _indices[r], row_end = _indices[r + 1];
                int i2 = 0, vect_end = vect_indices.Length;

                T dot = provider.Zero;
                bool any = false;

                // Set to product of diagonal term, if both are non-zero
                if (!_values[r].Equals(_zero))
                {
                    T vr = vector[r];
                    if (!vr.Equals(provider.Zero))
                    {
                        dot = provider.Multiply(vr, _values[r]);
                        any = true;
                    }
                }
                
                while (i1 < row_end && i2 < vect_end)
                {
                    int a = _indices[i1];
                    int b = vect_indices[i2];

                    if (a > b)
                    {
                        ++i2;
                    }
                    else if (a < b)
                    {
                        ++i1;
                    }
                    else
                    {
                        dot = provider.Add(dot, provider.Multiply(_values[i1], vect_values[i2]));
                        any = true;
                        ++i1;
                        ++i2;
                    }
                }

                if (any)
                {
                    productIndices[k] = r;
                    productValues[k] = dot;
                    ++k;
                }
            }

            return k;
        }

        public override SparseVector<T> TransposeAndMultiply(SparseVector<T> vector, IProvider<T> provider)
        {
            if (vector is null)
            {
                throw new ArgumentNullException(nameof(vector));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            if (vector.Dimension < Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(vector), "The dimension of the vector is less than the number of rows in the matrix.");
            }

            T[] prod = new T[Columns];

            // Use a bitarray to keep track of the non-zero pattern of 
            // the product - this takes a [[bit]] more memory but evaluating whether 
            // an entry is zero is potentially very expensive - tradeoff
            BitArray nz = new BitArray(Columns);
            int count = 0;

            // Initialize with the diagonal terms
            Array.Copy(_values, prod, Columns);

            int[] vect_indices = vector.Indices;
            T[] vect_values = vector.Values;
            int len = vect_indices.Length;

            for (int i = 0; i < len; ++i)
            {
                T v = vect_values[i];
                int c = vect_indices[i], col_end = _indices[c + 1];
                for (int j = _indices[c]; j < col_end; ++j)
                {
                    int r = _indices[j];
                    prod[r] = provider.Add(prod[r], provider.Multiply(_values[j], v));
                }
                nz[c] = true;
                ++count;
            }

            // Shrink - count 
            T zero = provider.Zero;
            for (int i = 0; i < Columns; ++i)
            {
                if (!nz[i])
                {
                    if (!provider.Equals(prod[i], zero))
                    {
                        nz[i] = true;
                        ++count;
                    }
                }
            }

            int[] prod_indices = new int[count];
            T[] prod_values = new T[count];

            int k = 0;
            for (int i = 0; i < Columns; ++i)
            {
                if (nz[i])
                {
                    prod_indices[k] = i;
                    prod_values[k] = prod[i];
                }
            }

            return new SparseVector<T>(Columns, prod_indices, prod_values, true);
        }

        public MCSRSparseMatrix<T> Multiply(T scalar, IProvider<T> provider)
        {
            MCSRSparseMatrix<T> copy = Copy();
            copy.MultiplyInPlace(scalar, provider);
            return copy;
        }

        /// <summary>
        /// Multiply this matrix by a scalar, and rewrite this matrix with the product.
        /// </summary>
        /// <param name="scalar">The scalar to multiply by.</param>
        /// <param name="provider">The provider used for elementwise multiplication.</param>
        public override void MultiplyInPlace(T scalar, IProvider<T> provider)
        {
            if (scalar is null)
            {
                throw new ArgumentNullException(nameof(scalar));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }

            int len = _values.Length;
            for (int i = 0; i < len; ++i)
            {
                _values[i] = provider.Multiply(_values[i], scalar);
            }
        }

        public void MultiplyInPlace(T scalar, IDenseBLAS1Provider<T> blas1)
        {
            if (scalar is null)
            {
                throw new ArgumentNullException(nameof(scalar));
            }
            if (blas1 is null)
            {
                throw new ArgumentNullException(nameof(blas1));
            }
            if (!(blas1 is IDenseBLAS1<T> blas))
            {
                throw new NotSupportedException(ExceptionMessages.TypeNotSupported);
            }

            blas.SCAL(_values, scalar, 0, _values.Length);
        }

        /// <summary>
        /// Divide this matrix by a scalar, using the specified provider for elementwise division.
        /// A new matrix is returned without modifying this matrix.
        /// </summary>
        /// <param name="scalar">The scalar to divide by.</param>
        /// <param name="provider">The provider used for elementwise division.</param>
        /// <returns>The matrix-scalar division result.</returns>
        public MCSRSparseMatrix<T> Divide(T scalar, IProvider<T> provider)
        {
            MCSRSparseMatrix<T> copy = Copy();
            copy.DivideInPlace(scalar, provider);
            return copy;
        }

        #endregion


        #region Convertors

        public override BandMatrix<T> ToBandMatrix()
        {
            return new BandMatrix<T>(this);
        }

        public override BlockDiagonalMatrix<T> ToBlockDiagonalMatrix()
        {
            return new BlockDiagonalMatrix<T>(this);
        }

        public override COOSparseMatrix<T> ToCOOSparseMatrix()
        {
            return new COOSparseMatrix<T>(this);
        }

        public override CSCSparseMatrix<T> ToCSCSparseMatrix()
        {
            return new CSCSparseMatrix<T>(this);
        }

        public override CSRSparseMatrix<T> ToCSRSparseMatrix()
        {
            return new CSRSparseMatrix<T>(this);
        }

        public override DenseMatrix<T> ToDenseMatrix()
        {
            return new DenseMatrix<T>(this);
        }

        public override DiagonalMatrix<T> ToDiagonalMatrix()
        {
            return new DiagonalMatrix<T>(this);
        }

        public override DOKSparseMatrix<T> ToDOKSparseMatrix()
        {
            return new DOKSparseMatrix<T>(this);
        }

        public override MCSRSparseMatrix<T> ToMCSRSparseMatrix()
        {
            return this;
        }

        public override SKYSparseMatrix<T> ToSKYSparseMatrix()
        {
            return new SKYSparseMatrix<T>(this);
        }
        #endregion

        #region Operators

        public static MCSRSparseMatrix<T> operator +(MCSRSparseMatrix<T> matrix1, MCSRSparseMatrix<T> matrix2)
        {
            if (matrix1 is null)
            {
                throw new ArgumentNullException(nameof(matrix1));
            }
            return matrix1.Add(matrix2, ProviderFactory.GetDefaultProvider<T>());
        }
        public static MCSRSparseMatrix<T> operator -(MCSRSparseMatrix<T> matrix1, MCSRSparseMatrix<T> matrix2)
        {
            if (matrix1 is null)
            {
                throw new ArgumentNullException(nameof(matrix1));
            }
            return matrix1.Subtract(matrix2, ProviderFactory.GetDefaultProvider<T>());
        }
        public static MCSRSparseMatrix<T> operator *(MCSRSparseMatrix<T> A, MCSRSparseMatrix<T> B)
        {
            if (A is null)
            {
                throw new ArgumentNullException(nameof(A));
            }
            return A.Multiply(B, ProviderFactory.GetDefaultProvider<T>());
        }
        public static DenseVector<T> operator *(MCSRSparseMatrix<T> matrix, DenseVector<T> vector)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            return matrix.Multiply(vector, ProviderFactory.GetDefaultProvider<T>());
        }
        public static SparseVector<T> operator *(MCSRSparseMatrix<T> matrix, SparseVector<T> vector)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            return matrix.Multiply(vector, ProviderFactory.GetDefaultProvider<T>());
        }
        public static MCSRSparseMatrix<T> operator *(MCSRSparseMatrix<T> matrix, T scalar)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            return matrix.Multiply(scalar, ProviderFactory.GetDefaultProvider<T>());
        }
        public static MCSRSparseMatrix<T> operator *(T scalar, MCSRSparseMatrix<T> matrix) => matrix * scalar;
        public static MCSRSparseMatrix<T> operator /(MCSRSparseMatrix<T> matrix, T scalar)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            return matrix.Divide(scalar, ProviderFactory.GetDefaultProvider<T>());
        }
        public static bool operator ==(MCSRSparseMatrix<T> A, MCSRSparseMatrix<T> B)
        {
            if (A is null || B is null)
            {
                return false;
            }
            return A.Equals(B);
        }
        public static bool operator !=(MCSRSparseMatrix<T> A, MCSRSparseMatrix<T> B) => !(A == B);
        #endregion

        #region Casts
        public static explicit operator MCSRSparseMatrix<T>(BandMatrix<T> matrix) => new MCSRSparseMatrix<T>(matrix);
        public static explicit operator MCSRSparseMatrix<T>(DiagonalMatrix<T> matrix) => new MCSRSparseMatrix<T>(matrix);
        public static explicit operator MCSRSparseMatrix<T>(DenseMatrix<T> matrix) => new MCSRSparseMatrix<T>(matrix);
        public static explicit operator MCSRSparseMatrix<T>(COOSparseMatrix<T> matrix) => new MCSRSparseMatrix<T>(matrix);
        public static explicit operator MCSRSparseMatrix<T>(CSCSparseMatrix<T> matrix) => new MCSRSparseMatrix<T>(matrix);
        public static explicit operator MCSRSparseMatrix<T>(CSRSparseMatrix<T> matrix) => new MCSRSparseMatrix<T>(matrix);
        public static explicit operator MCSRSparseMatrix<T>(DOKSparseMatrix<T> matrix) => new MCSRSparseMatrix<T>(matrix);
        #endregion
    }
}
