﻿using LinearNet.Global;
using LinearNet.Helpers;
using System;
using System.Collections.Generic;

namespace LinearNet.Structs
{
    public static class MCSRSparseMatrix
    {
        public static MCSRSparseMatrix<T> Random<T>(int dim, double diagonalDensity, double offDiagonalDensity, Func<T> random = null) where T : new()
        {
            if (dim <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(dim), ExceptionMessages.NonPositiveNotAllowed);
            }
            if (double.IsNaN(diagonalDensity))
            {
                throw new ArgumentOutOfRangeException(nameof(diagonalDensity), ExceptionMessages.NaNNotAllowed);
            }
            if (double.IsInfinity(diagonalDensity))
            {
                throw new ArgumentOutOfRangeException(nameof(diagonalDensity), ExceptionMessages.InfinityNotAllowed);
            }
            if (diagonalDensity < 0 || diagonalDensity > 1)
            {
                throw new ArgumentOutOfRangeException(nameof(diagonalDensity), ExceptionMessages.NotBetween0And1);
            }
            if (double.IsNaN(offDiagonalDensity))
            {
                throw new ArgumentOutOfRangeException(nameof(offDiagonalDensity), ExceptionMessages.NaNNotAllowed);
            }
            if (double.IsInfinity(offDiagonalDensity))
            {
                throw new ArgumentOutOfRangeException(nameof(offDiagonalDensity), ExceptionMessages.InfinityNotAllowed);
            }
            if (offDiagonalDensity < 0 || offDiagonalDensity > 1)
            {
                throw new ArgumentOutOfRangeException(nameof(offDiagonalDensity), ExceptionMessages.NotBetween0And1);
            }

            if (random is null)
            {
                random = DefaultGenerators.GetRandomGenerator<T>();
            }

            int capacity = (int)(1.5 * offDiagonalDensity * dim * dim);
            List<T> values = new List<T>(capacity);
            List<int> indices = new List<int>(capacity);

            T zero = new T();
            Random r = new Random();

            // Add the diagonals first
            for (int i = 0; i < dim; ++i)
            {
                if (r.NextDouble() < diagonalDensity)
                {
                    values.Add(random());
                }
                else
                {
                    values.Add(zero);
                }
                indices.Add(0); // placeholder for row indices
            }

            // Extra entry
            values.Add(default);
            indices.Add(0);

            // Add non-zero off-diagonal terms
            int offset = dim + 1;
            indices[0] = values.Count;
            for (int i = 0; i < dim; ++i)
            {
                for (int j = 0; j < dim; ++j)
                {
                    if (i == j) continue;

                    if (r.NextDouble() < offDiagonalDensity)
                    {
                        values.Add(random());
                        indices.Add(j);
                    }
                }
                indices[i + 1] = values.Count;
            }

            return new MCSRSparseMatrix<T>(dim, indices.ToArray(), values.ToArray());
        }
    }
}
