﻿using LinearNet.Global;
using LinearNet.Providers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace LinearNet.Structs
{
    /// <summary>
    /// <p>An abstract class representing a matrix object. All matrix implementations inherit from this class.</p>
    /// <p>Most matrix operations are implemented via <a href="Extensions.html">extension methods</a> instead of instance methods from this class.</p>
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <cat>linear-algebra</cat>
    public abstract class Matrix<T> : ICloneable where T : new()
    {
        /// <summary>
        /// The storage format of this matrix.
        /// </summary>
        public MatrixStorageType StorageType { get; private set; }

        /// <summary>
        /// Returns the number of rows of the matrix.
        /// </summary>
        public int Rows { get; protected set; }

        /// <summary>
        /// Returns the number of columns of the matrix.
        /// </summary>
        public int Columns { get; protected set; }

        /// <summary>
        /// Returns whether the matrix is a square matrix.
        /// </summary>
        public bool IsSquare { get { return Rows == Columns; } }

        /// <summary>
        /// Returns an enumeration of all entries of this matrix, in row-major order.
        /// </summary>
        public IEnumerable<T> Entries
        {
            get
            {
                for (int r = 0; r < Rows; ++r)
                {
                    for (int c = 0; c < Columns; ++c)
                    {
                        yield return this[r, c];
                    }
                }
            }
        }

        /// <summary>
        /// Returns an enumeration of all symbolic non-zero entries of this matrix. 
        /// </summary>
        public abstract IEnumerable<MatrixEntry<T>> NonZeroEntries { get; }

        /// <summary>
        /// Returns the number of structural (symbolic) non-zero entries in this matrix.
        /// </summary>
        public abstract int SymbolicNonZeroCount { get; }

        /// <summary>
        /// Returns the <txt>index</txt>-th row of this matrix as a dense array.
        /// <para>This method can incur large performance penalties for sparse matrices.</para>
        /// </summary>
        /// <param name="index">The index of the row to extract.</param>
        /// <returns>The <txt>index</txt>-th row of this matrix.</returns>
        public abstract T[] this[int index] { get; set; }

        /// <summary>
        /// Gets or sets the $($<txt>rowIndex</txt>, <txt>columnIndex</txt>$)$-th entry from this matrix.
        /// </summary>
        /// <param name="rowIndex">The row index of the entry to get or set.</param>
        /// <param name="columnIndex">The column index of the entry to get or set.</param>
        /// <returns>The entry in row "<txt>rowIndex</txt>" and column "<txt>columnIndex</txt>".</returns>
        public abstract T this[int rowIndex, int columnIndex] { get; set; }

        public Matrix<T> this[string rowSelector, string columnSelector]
        {
            get
            {
                return Select(rowSelector, columnSelector, false);
            }
        }
        public Vector<T> this[int rowIndex, string columnSelector]
        {
            get
            {
                return SelectAsVector(rowIndex, columnSelector, false);
            }
        }
        public Vector<T> this[string rowSelector, int columnIndex]
        {
            get
            {
                return SelectAsVector(rowSelector, columnIndex, false);
            }
        }


        #region Constructors

        protected Matrix(MatrixStorageType type, int rows, int columns)
        {
            if (rows <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(rows), ExceptionMessages.NonPositiveNotAllowed);
            }
            if (columns <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(columns), ExceptionMessages.NonPositiveNotAllowed);
            }

            StorageType = type;
            Rows = rows;
            Columns = columns;
        }
        protected Matrix(MatrixStorageType type)
        {
            StorageType = type;
            Rows = 0;
            Columns = 0;
        }

        #endregion


        /// <summary>
        /// Returns the hashcode of this matrix's values.
        /// <para>
        /// It is too expensive to generate a hashcode of the matrix values O(mn), so we generate
        /// a hashcode from only the leading diagonal elements, which is O(n).
        /// For rectangular matrices, we take the hashcode of the largest square submatrix that fits
        /// inside the matrix, starting from the top-left corner.
        /// </para>
        /// </summary>
        /// <returns>The hash code of this matrix.</returns>
        public override int GetHashCode()
        {
            int hash = 7;
            int dim = Math.Min(Rows, Columns);
            for (int d = 0; d < dim; ++d)
            {
                hash = hash * 11 + this[d, d].GetHashCode();
            }
            return hash ^ ((101 + Rows) * 103 + Columns);
        }

        /// <summary>
        /// Returns the leading diagonal of this matrix, as an array.
        /// </summary>
        /// <returns>The leading diagonal of this matrix.</returns>
        public abstract T[] LeadingDiagonal();

        /// <summary>
        /// Returns the trace of this matrix. If the matrix is not square, it will return the 
        /// sum of all diagonal entries.
        /// </summary>
        /// <param name="provider">The provider used for elementwise operations.</param>
        /// <returns>The matrix trace.</returns>
        public abstract T Trace(IProvider<T> provider);

        /// <summary>
        /// Returns the number of entries that satisfy a predicate
        /// </summary>
        /// <param name="predicate">The predicate that returns true if the entry should be counted.</param>
        /// <returns>The number of entries satisfying a predicate.</returns>
        public abstract int Count(Predicate<T> predicate);

        /// <summary>
        /// Returns the row sums of this matrix, as a dense vector. The $i$-th element of the 
        /// vector is equal to the sum of all elements in the $i$-th row of this matrix.
        /// </summary>
        /// <param name="provider">The provider used to sum elements of the matrix.</param>
        /// <returns>The row sums of this matrix.</returns>
        public abstract DenseVector<T> RowSums(IProvider<T> provider);

        /// <summary>
        /// Returns the column sums of this matrix, as a dense vector. The $i$-th element of the 
        /// vector is equal to the sum of all elements in the $i$-th column of this matrix. 
        /// </summary>
        /// <param name="provider">The provider used to sum elements of the matrix.</param>
        /// <returns>The column sums of this matrix.</returns>
        public abstract DenseVector<T> ColumnSums(IProvider<T> provider);

        /// <summary>
        /// Returns a row of this matrix as a dense vector.
        /// </summary>
        /// <param name="rowIndex">The index of the row to return.</param>
        /// <returns>A row of this matrix.</returns>
        public abstract DenseVector<T> Row(int rowIndex);

        /// <summary>
        /// Returns a row of this matrix as a sparse vector.
        /// </summary>
        /// <param name="rowIndex">The index of the row to extract (0-based indexing).</param>
        /// <returns>A row of this matrix.</returns>
        public abstract SparseVector<T> SparseRow(int rowIndex);

        /// <summary>
        /// Returns a column of this matrix as a dense vector.
        /// </summary>
        /// <param name="columnIndex">The index of the column to return.</param>
        /// <returns>A column of this matrix.</returns>
        public abstract DenseVector<T> Column(int columnIndex);

        /// <summary>
        /// Returns a column of this matrix as a sparse vector.
        /// </summary>
        /// <param name="columnIndex">The index of the column to extract (0-based indexing).</param>
        /// <returns>A column of this matrix.</returns>
        public abstract SparseVector<T> SparseColumn(int columnIndex);

        /// <summary>
        /// Returns a new matrix formed by selecting a set of rows and columns of this matrix.
        /// </summary>
        /// <param name="rowSelector">The row index selector expression.</param>
        /// <param name="colSelector">The column index selector expression.</param>
        /// <param name="oneBased">Specifies whether the row/column selector expressions are 0-based 
        /// or 1-based.</param>
        /// <returns></returns>
        public Matrix<T> Select(string rowSelector, string colSelector, bool oneBased = false)
        {
            if (!Selector.TryParse(rowSelector, oneBased, Rows, out Selector rowSel))
            {
                throw new FormatException(nameof(rowSelector));
            }
            if (!Selector.TryParse(colSelector, oneBased, Columns, out Selector colSel))
            {
                throw new FormatException(nameof(colSelector));
            }
            return Select(rowSel.Selection, colSel.Selection);
        }

        public abstract Matrix<T> Select(int[] rowIndices, int[] columnIndices);

        /// <summary>
        /// Returns a vector formed by extracting a row from this matrix, and selecting a subset of columns.
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <param name="colSelector"></param>
        /// <param name="oneBased"></param>
        /// <returns></returns>
        public Vector<T> SelectAsVector(int rowIndex, string colSelector, bool oneBased = false)
        {
            if (oneBased)
            {
                rowIndex--;
            }
            if (rowIndex < 0 || rowIndex >= Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(rowIndex));
            }
            if (!Selector.TryParse(colSelector, oneBased, Columns, out Selector colSel))
            {
                throw new FormatException(nameof(colSelector));
            }
            return SelectAsVector(rowIndex, colSel.Selection);
        }

        public abstract Vector<T> SelectAsVector(int rowIndex, int[] columnIndices);

        public Vector<T> SelectAsVector(string rowSelector, int columnIndex, bool oneBased = false)
        {
            if (oneBased)
            {
                columnIndex--;
            }
            if (!Selector.TryParse(rowSelector, oneBased, Rows, out Selector rowSel))
            {
                throw new FormatException(nameof(rowSelector));
            }
            if (columnIndex < 0 || columnIndex >= Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(columnIndex));
            }
            return SelectAsVector(rowSel.Selection, columnIndex);
        }

        public abstract Vector<T> SelectAsVector(int[] rowIndices, int columnIndex);

        public COOSparseTensor<T> TensorProduct(Vector<T> vector, IDenseBLAS1Provider<T> blas)
        {
            if (vector is null)
            {
                throw new ArgumentNullException(nameof(vector));
            }
            if (blas is null)
            {
                throw new ArgumentNullException(nameof(blas));
            }
            if (!(blas is IDenseBLAS1<T> b))
            {
                throw new NotSupportedException(ExceptionMessages.TypeNotSupported);
            }

            long len = (long)SymbolicNonZeroCount * vector.NonZeroCount;
            if (len * 3 > int.MaxValue)
            {
                throw new OverflowException("Tensor product is too large to be stored as in Coordinate format.");
            }

            int[] indices = new int[len * 3];
            T[] values = new T[len];

            bool sorted = false;
            int prev_row = -1, 
                prev_col = -1, 
                prev_vect;

            int i = 0, k = 0;
            foreach (MatrixEntry<T> me in NonZeroEntries)
            {
                int r = me.RowIndex, 
                    c = me.ColumnIndex,
                    start = k;

                // Check sort order
                if (sorted && 
                    (r < prev_row || (r == prev_row && c < prev_col)))
                {
                    sorted = false;
                }
                prev_row = r;
                prev_col = c;
                prev_vect = -1;

                foreach (VectorEntry<T> ve in vector.NonZeroEntries)
                {
                    indices[i++] = r;
                    indices[i++] = c;
                    indices[i++] = ve.Index;
                    values[k++] = ve.Value;

                    // Check sort order
                    if (sorted && prev_vect > ve.Index)
                    {
                        sorted = false;
                    }
                    prev_vect = ve.Index;
                }

                // Multiply by the matrix entry using dense BLAS
                b.SCAL(values, me.Value, start, k);
            }

            return new COOSparseTensor<T>(new int[] { Rows, Columns, vector.Dimension }, indices, values, (int)len, sorted);
        }

        #region Matrix modifiers

        /// <summary>
        /// Swaps two rows of this matrix, in place.
        /// </summary>
        /// <param name="rowIndex1">The index of the first row to swap.</param>
        /// <param name="rowIndex2">The index of the second row to swap.</param>
        public abstract void SwapRows(int rowIndex1, int rowIndex2);

        /// <summary>
        /// Swaps two columns of this matrix, in place.
        /// </summary>
        /// <param name="columnIndex1">The index of the first column to swap.</param>
        /// <param name="columnIndex2">The index of the second column to swap.</param>
        public abstract void SwapColumns(int columnIndex1, int columnIndex2);

        /// <summary>
        /// <p>Sets each value of this matrix to zero. For custom types, all entries will be set 
        /// to the value of <txt>new T()</txt>.</p>
        /// </summary>
        public abstract void Clear();

        /// <summary>
        /// Sets all entries that match a predicate to zero. For custom types, the entries will 
        /// be set to the value of <txt>new T()</txt>.
        /// </summary>
        /// <param name="select">The predicate to match</param>
        public abstract void Clear(Predicate<T> select);

        /// <summary>
        /// <p>Sets all elements of a row to zero. For custom types, the entries will be set to 
        /// the value of <txt>new T()</txt>.</p>
        /// </summary>
        /// <param name="rowIndex">The index of the row to clear.</param>
        public abstract void ClearRow(int rowIndex);

        /// <summary>
        /// <p>Sets all elements of a column to zero. For custom types, the entries will be set to 
        /// the value of <txt>new T()</txt>.</p>
        /// </summary>
        /// <param name="columnIndex">The index of the column to clear.</param>
        public abstract void ClearColumn(int columnIndex);

        /// <summary>
        /// Sets all elements in a submatrix to zero.
        /// </summary>
        /// <param name="rowIndex">The index of the first row to clear.</param>
        /// <param name="columnIndex">The index of the first column to clear.</param>
        /// <param name="rows">The number of rows in the submatrix block.</param>
        /// <param name="columns">The number of columns in the submatrix block.</param>
        public abstract void ClearSubmatrix(int rowIndex, int columnIndex, int rows, int columns);

        /// <summary>
        /// Sets all entries under the main diagonal to zero. For custom types, the entries will 
        /// be set to the value of <txt>new T()</txt>.
        /// </summary>
        public abstract void ClearLower();

        /// <summary>
        /// Sets all entries above the main diagonal to zero. For custom types, the entries will 
        /// be set to the value of <txt>new T()</txt>.
        /// </summary>
        public abstract void ClearUpper();

        #endregion


        #region Boolean methods

        /// <summary>
        /// <para>
        /// Returns whether this matrix is equal in value to another matrix, using a predicate 
        /// to decide whether two matrix elements of type <txt>T</txt> should be considered equal.
        /// </para>
        /// 
        /// <para>
        /// This method handles general matrix comparisons; there are faster implementations 
        /// that only handle specific matrix types. 
        /// </para>
        /// </summary>
        /// <param name="matrix">The matrix to compare to.</param>
        /// <param name="equals">A predicate that takes 2 arguments of type <txt>T</txt> and returns 
        /// whether they should be considered equal.</param>
        /// <returns>Whether the two matrices are equal in value.</returns>
        public bool Equals(Matrix<T> matrix, Func<T, T, bool> equals)
        {
            if (equals is null)
            {
                throw new ArgumentNullException(nameof(equals));
            }
            if (matrix is null)
            {
                return false;
            }
            if (Rows != matrix.Rows || Columns != matrix.Columns)
            {
                return false;
            }

            for (int r = 0; r < Rows; ++r)
            {
                for (int c = 0; c < Columns; ++c)
                {
                    if (!equals(this[r, c], matrix[r, c]))
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// Returns whether this matrix is equal in value to another matrix, using a equality comparer
        /// to determine whether two matrix elements should be considered equal.
        /// </summary>
        /// <param name="matrix">The matrix to compare to.</param>
        /// <param name="comparer">A equality comparer implemention used to decide whether two matrix 
        /// elements of type <txt>T</txt> should be considered equal.</param>
        /// <returns>Whether the two matrices are equal in value.</returns>
        public bool Equals(Matrix<T> matrix, IEqualityComparer<T> comparer)
        {
            if (comparer is null)
            {
                throw new ArgumentNullException(nameof(comparer));
            }
            return Equals(matrix, comparer.Equals);
        }

        /// <summary>
        /// Returns whether this matrix is equal in value to another matrix using the default equality 
        /// comparer of type <txt>T</txt>.
        /// </summary>
        /// <param name="matrix">The matrix to compare to.</param>
        /// <returns>Whether the two matrices are equal in value.</returns>
        public bool Equals(Matrix<T> matrix)
        {
            return Equals(matrix, EqualityComparer<T>.Default);
        }

        /// <summary>
        /// Returns whether this matrix is equal in value to an object.
        /// </summary>
        /// <param name="obj">The object to compare to.</param>
        /// <returns>Whether the matrix and object are equal in value.</returns>
        public override abstract bool Equals(object obj);

        /// <summary>
        /// Returns whether this matrix is symmetric, using a delegate to determine whether two 
        /// elements of type <txt>T</txt> should be considered equal.
        /// </summary>
        /// <param name="equals">A delegate function that returns true if its two arguments should
        /// be considered equal.</param>
        /// <returns>Whether this matrix is symmetric.</returns>
        public virtual bool IsSymmetric(Func<T, T, bool> equals)
        {
            if (!IsSquare)
            {
                return false;
            }

            for (int i = 0; i < Rows; ++i)
            {
                for (int j = 0; j < i; ++j)
                {
                    if (!equals(this[i, j], this[j, i]))
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        /// <summary>
        /// Returns whether this matrix is symmetric, using a equality comparer to determine whether 
        /// two matrix entries should be considered equal.
        /// </summary>
        /// <param name="comparer">The equality comparer.</param>
        /// <returns>Whether this matrix is symmetric.</returns>
        public virtual bool IsSymmetric(IEqualityComparer<T> comparer)
        {
            if (comparer is null)
            {
                throw new ArgumentNullException(nameof(comparer));
            }
            return IsSymmetric(comparer.Equals);
        }

        /// <summary>
        /// Returns whether this matrix is a lower triangular (or lower trapezoidal if the matrix is 
        /// rectangular), using the default value comparator for type <txt>T</txt> to determine whether
        /// an entry is zero.
        /// </summary>
        /// <returns>Whether this matrix is lower triangular (or lower trapezoidal if it is rectangular).</returns>
        public bool IsLower()
        {
            T zero = new T();
            return IsLower(x => x.Equals(zero));
        }

        /// <summary>
        /// Returns whether this matrix is a lower triangular matrix. For non-square matrices,
        /// this method will return <txt>true</txt> if all entries above the main diagonal are zero.
        /// </summary>
        /// <param name="isZero">A predicate that returns <txt>true</txt> if an entry is equal to zero.</param>
        /// <returns>Whether this matrix is lower-triangular (or lower trapezoidal for rectangular matrices).</returns>
        public abstract bool IsLower(Predicate<T> isZero);

        /// <summary>
        /// Returns whether this matrix is upper triangular (or upper trapezoidal for rectangular 
        /// matrices), using the default value comparator for type <txt>T</txt> to determine whether
        /// an entry is zero.
        /// </summary>
        /// <returns>Whether this matrix is upper triangular (or upper trapezoidal if it is rectangular).</returns>
        public bool IsUpper()
        {
            T zero = new T();
            return IsUpper(x => x.Equals(zero));
        }

        /// <summary>
        /// Returns whether this matrix is a upper triangular matrix. For non-square matrices,
        /// this method returns <txt>true</txt> if all entries below the main diagonal are zero (i.e. 
        /// the matrix is upper trapezoidal). 
        /// </summary>
        /// <param name="isZero">A predicate that returns <txt>true</txt> if an entry is equal to zero.</param>
        /// <returns>Whether this matrix is upper triangular (or upper trapezoidal for rectangular matrices).</returns>
        public abstract bool IsUpper(Predicate<T> isZero);

        /// <summary>
        /// Returns whether this matrix is a diagonal matrix, using the default equality comparer of type 
        /// <txt>T</txt> to determine whether an entry equals zero.
        /// </summary>
        /// <returns>Whether this matrix is a diagonal matrix.</returns>
        public bool IsDiagonal()
        {
            T zero = new T();
            return IsDiagonal(x => x.Equals(zero));
        }

        /// <summary>
        /// Returns whether this matrix is a diagonal matrix. For rectangular matrices, this 
        /// method returns true if all non-zero entries lie along the main diagonal.
        /// </summary>
        /// <param name="isZero">A predicate the returns <txt>true</txt> if an entry is equal to zero.</param>
        /// <returns>Whether this matrix is diagonal.</returns>
        public abstract bool IsDiagonal(Predicate<T> isZero);

        /// <summary>
        /// Returns whether this matrix is a banded matrix with bandwidths less than or equal to 
        /// the specified maximum band widths. This method uses the default equality comparer to 
        /// determine whether an entry of type <txt>T</txt> is equal in value to zero.
        /// </summary>
        /// <param name="lowerBandwidth">The maximum lower bandwidth.</param>
        /// <param name="upperBandwidth">The maximum upper bandwidth.</param>
        /// <returns>Whether this matrix is banded with the specified maximum lower and upper bandwidths.</returns>
        public bool IsBanded(int lowerBandwidth, int upperBandwidth)
        {
            T zero = new T();
            return IsBanded(lowerBandwidth, upperBandwidth, x => x.Equals(zero));
        }

        /// <summary>
        /// Returns whether this matrix is a banded matrix with bandwidths less than or equal to 
        /// the specified maximum band widths.
        /// </summary>
        /// <param name="lowerBandwidth">The maximum lower bandwidth.</param>
        /// <param name="upperBandwidth">The maximum upper bandwidth.</param>
        /// <param name="isZero">A predicate returning <txt>true</txt> if an element is equal to zero.</param>
        /// <returns>Whether this matrix is banded with the specified maximum bandwidths.</returns>
        public abstract bool IsBanded(int lowerBandwidth, int upperBandwidth, Predicate<T> isZero);

        /// <summary>
        /// Returns whether all entries of this matrix is equal to zero, using default equality 
        /// comparer for type <txt>T</txt> to determine whether an entry is zero or not.
        /// </summary>
        /// <returns>Whether this matrix is a zero matrix.</returns>
        public bool IsZero()
        {
            T zero = new T();
            return IsZero(x => x.Equals(zero));
        }

        /// <summary>
        /// Returns whether all entries of this matrix is equal to zero, using a predicate to 
        /// determine whether an entry is zero or not.
        /// </summary>
        /// <param name="isZero">The predicate used compare matrix entries with zero.</param>
        /// <returns>Whether this matrix is a zero matrix.</returns>
        public abstract bool IsZero(Predicate<T> isZero);

        /// <summary>
        /// Returns whether this matrix is an identity matrix, using a provider to determine 
        /// whether a matrix entry equals zero or one.
        /// </summary>
        /// <param name="provider">The provider used for elementwise comparisons.</param>
        /// <returns>Whether this matrix is an identity matrix.</returns>
        public bool IsIdentity(IProvider<T> provider)
        {
            T zero = provider.Zero, one = provider.One;
            return IsIdentity(x => provider.Equals(x, zero), x => provider.Equals(x, one));
        }

        /// <summary>
        /// Returns whether this matrix is an identity matrix, using two predicates that 
        /// return <txt>true</txt> if an entry is equal in value to 0 and 1, respectively.
        /// </summary>
        /// <param name="isZero">A predicate returning <txt>true</txt> if an entry is zero.</param>
        /// <param name="isOne">A predicate returning <txt>true</txt> if an entry is one.</param>
        /// <returns>Whether this matrix is equal in value to an identity matrix.</returns>
        public abstract bool IsIdentity(Predicate<T> isZero, Predicate<T> isOne);

        #endregion


        #region Public operations

        /// <summary>
        /// Returns an integer representing the relative importance of each storage type (MatrixStorageType)
        /// A matrix computation involving two matrices of type t1, t2, with importance i1, i2, will return a matrix of 
        /// type t1 if i1 > i2 and t2 if i1 < i2.
        /// </summary>
        /// <returns></returns>
        protected static int GetRepresentationPriority(MatrixStorageType type)
        {
            return type switch
            {
                // Diagonal matrices can be represented in all other forms, 
                // so it has the lowest priority.
                MatrixStorageType.DIAGONAL => 0,

                // Block-diagonal can be represented as both Skyline and band,
                // but not vice versa - 2nd lowest representation priority
                MatrixStorageType.BLOCK_DIAGONAL => 1,
                MatrixStorageType.SKYLINE => 2,
                MatrixStorageType.BAND => 3,

                // Slow random-access types like COO, DOK should be de-prioritised
                // relative to efficient types
                MatrixStorageType.DOK => 4,
                MatrixStorageType.COO => 5,

                // Prioritise CSC over CSR since many order-3 operations are faster
                // Deprioritise MCSR because it only supports square matrices
                MatrixStorageType.MCSR => 6,
                MatrixStorageType.CSR => 7,
                MatrixStorageType.CSC => 8,

                // Prioritise dense since dense-sparse operations typically returns
                // dense structures
                MatrixStorageType.DENSE => 100,

                _ => 1,
            };
        }
        protected int CompareRepresentationPriority(Matrix<T> A, Matrix<T> B)
        {
            return GetRepresentationPriority(A.StorageType) - GetRepresentationPriority(B.StorageType);
        }

        /// <summary>
        /// Adds another matrix to this matrix. The storage type of the sum will depend on the 
        /// representation priority of the storage types of the two summands. 
        /// </summary>
        /// <param name="matrix">The matrix to add.</param>
        /// <param name="provider">The provider used for elementwise operations.</param>
        /// <returns>The matrix sum.</returns>
        public abstract Matrix<T> Add(Matrix<T> matrix, IProvider<T> provider);

        internal abstract void Multiply(T[] vector, T[] product, IProvider<T> provider, bool increment);
        internal abstract void TransposeAndMultiply(T[] vector, T[] product, IProvider<T> provider, bool increment);

        /// <summary>
        /// Postmultiply this matrix by a sparse vector, returning the product as a sparse vector.
        /// </summary>
        /// <param name="vector">The vector to postmultiply by.</param>
        /// <param name="provider">The provider used for elementwise operations.</param>
        /// <returns>The matrix-vector product.</returns>
        public abstract SparseVector<T> Multiply(SparseVector<T> vector, IProvider<T> provider);

        /// <summary>
        /// Postmultiply the transpose of this matrix by a sparse vector, returning the product as a sparse vector.
        /// </summary>
        /// <param name="vector">The vector to postmultiply by.</param>
        /// <param name="provider">The provider used for elementwise operations.</param>
        /// <returns>The matrix-vector product.</returns>
        public abstract SparseVector<T> TransposeAndMultiply(SparseVector<T> vector, IProvider<T> provider);

        /// <summary>
        /// Postmultiply this matrix by a dense vector, then store the product in <txt>result</txt>.
        /// <p>
        /// The <txt>result</txt> vector must have dimension $\ge$ the number of rows of this matrix.
        /// If the dimension of <txt>result</txt> is larger than the dimension of the product, 
        /// only the first elements of <txt>result</txt> will be altered.
        /// </p>
        /// </summary>
        /// <param name="vector">The vector to postmultiply by.</param>
        /// <param name="result">The vector that will store the computed matrix-vector product.</param>
        /// <param name="provider">The provider used for elementwise multiplication and addition.</param>
        /// <param name="increment">If <txt>true</txt>, <txt>result</txt> will be incremented by the 
        /// matrix-vector product. Otherwise, it will be set to the value of the matrix-vector product.
        /// </param>
        public void Multiply(DenseVector<T> vector, DenseVector<T> result, IProvider<T> provider, bool increment = false)
        {
            if (vector is null)
            {
                throw new ArgumentNullException(nameof(vector));
            }
            if (result is null)
            {
                throw new ArgumentNullException(nameof(result));
            }
            Multiply(vector.Values, result.Values, provider, increment);
        }

        /// <summary>
        /// Postmultiply this matrix by a vector, returning the product as a dense vector. The original matrix 
        /// and vector are unchanged.
        /// </summary>
        /// <param name="vector">The vector to postmultiply by.</param>
        /// <param name="provider">The provider used for elementwise multiplication and addition.</param>
        /// <returns>The matrix-vector product.</returns>
        public DenseVector<T> Multiply(DenseVector<T> vector, IProvider<T> provider)
        {
            if (vector is null)
            {
                throw new ArgumentNullException(nameof(vector));
            }

            T[] result = new T[Rows];
            Multiply(vector.Values, result, provider, false);
            return new DenseVector<T>(result);
        }

        /// <summary>
        /// Postmultiply this matrix by a vector represented as an array, returning the product also as an array.
        /// The original matrix and vector are unchanged.
        /// </summary>
        /// <param name="vector">The vector to postmultiply by.</param>
        /// <param name="provider">The provider used for elementwise multiplication and addition.</param>
        /// <returns>The matrix-vector product.</returns>
        public T[] Multiply(T[] vector, IProvider<T> provider)
        {
            T[] product = new T[Rows];
            Multiply(vector, product, provider, false);
            return product;
        }

        /// <summary>
        /// Postmultiply the transpose of this matrix by a dense vector, storing the product in <txt>result</txt>.
        /// The original matrix and vector are unchanged.
        /// </summary>
        /// <param name="vector">The vector to postmultiply by.</param>
        /// <param name="result">The vector which will hold the matrix-vector product.</param>
        /// <param name="provider">The provider used for elementwise multiplication and addition.</param>
        /// <param name="increment">If <txt>true</txt>, <txt>result</txt> will be incremented by the product $v = v + A^Tx$. Otherwise
        /// <txt>result</txt> will be set to the product $v = A^Tx.$</param>
        public void TransposeAndMultiply(DenseVector<T> vector, DenseVector<T> result, IProvider<T> provider, bool increment = false) 
        {
            if (vector is null)
            {
                throw new ArgumentNullException(nameof(vector));
            }
            if (result is null)
            {
                throw new ArgumentNullException(nameof(result));
            }

            // Rest of the arguments are checked internally
            TransposeAndMultiply(vector.Values, result.Values, provider, increment);
        }

        /// <summary>
        /// Postmultiply the transpose of this matrix by a dense vector, return the product as another dense vector.
        /// The original matrix and vector are unchanged.
        /// </summary>
        /// <param name="vector">The vector to postmultiply by.</param>
        /// <param name="provider">The provider used for elementwise multiplication and addition.</param>
        /// <returns>The matrix-vector product.</returns>
        public DenseVector<T> TransposeAndMultiply(DenseVector<T> vector, IProvider<T> provider)
        {
            if (vector is null)
            {
                throw new ArgumentNullException(nameof(vector));
            }
            T[] product = new T[Columns];
            if (provider.Equals(default, provider.Zero))
            {
                TransposeAndMultiply(vector.Values, product, provider, true); // increment is faster
            }
            else
            {
                TransposeAndMultiply(vector.Values, product, provider, false);
            }
            return new DenseVector<T>(product);
        }

        /// <summary>
        /// Multiply this matrix by a scalar, and overwrite this matrix with the product.
        /// </summary>
        /// <param name="scalar">The scalar to multiply by.</param>
        /// <param name="provider">The provider to use for elementwise multiplication.</param>
        public abstract void MultiplyInPlace(T scalar, IProvider<T> provider);

        /// <summary>
        /// Divides this matrix by a scalar, then overwrite this matrix with the result. 
        /// </summary>
        /// <param name="scalar">The scalar value to divide by.</param>
        /// <param name="provider">The provider to use for elementwise division.</param>
        public void DivideInPlace(T scalar, IProvider<T> provider)
        {
            if (scalar is null)
            {
                throw new ArgumentNullException(nameof(scalar));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }

            MultiplyInPlace(provider.Divide(provider.One, scalar), provider);
        }

        #endregion


        #region Convertors

        public Matrix<T> ToMatrix(MatrixStorageType type)
        {
            return type switch
            {
                MatrixStorageType.BAND => ToBandMatrix(),
                MatrixStorageType.BLOCK_DIAGONAL => ToBlockDiagonalMatrix(),
                MatrixStorageType.COO => ToCOOSparseMatrix(),
                MatrixStorageType.CSC => ToCSCSparseMatrix(),
                MatrixStorageType.CSR => ToCSRSparseMatrix(),
                MatrixStorageType.DENSE => ToDenseMatrix(),
                MatrixStorageType.DIAGONAL => ToDiagonalMatrix(),
                MatrixStorageType.DOK => ToDOKSparseMatrix(),
                MatrixStorageType.MCSR => ToMCSRSparseMatrix(),
                MatrixStorageType.SKYLINE => ToSKYSparseMatrix(),
                _ => throw new NotImplementedException()
            };
        }

        /// <summary>
        /// Returns this matrix stored in band matrix format.
        /// </summary>
        /// <returns>A band matrix.</returns>
        public abstract BandMatrix<T> ToBandMatrix();

        /// <summary>
        /// Returns this matrix as a block-diagonal matrix.
        /// </summary>
        /// <returns>A block-diagonal matrix.</returns>
        public abstract BlockDiagonalMatrix<T> ToBlockDiagonalMatrix();

        /// <summary>
        /// Returns this matrix as a sparse matrix stored using Coordinate List (COO) format.
        /// </summary>
        /// <returns>A COO sparse matrix.</returns>
        public abstract COOSparseMatrix<T> ToCOOSparseMatrix();

        /// <summary>
        /// Returns this matrix as a sparse matrix stored using Compressed Sparse Column (CSC) format.
        /// </summary>
        /// <returns>A CSC sparse matrix.</returns>
        public abstract CSCSparseMatrix<T> ToCSCSparseMatrix();

        /// <summary>
        /// Returns this matrix as a sparse matrix stored using Compressed Sparse Row (CSR) format.
        /// </summary>
        /// <returns>A CSR sparse matrix.</returns>
        public abstract CSRSparseMatrix<T> ToCSRSparseMatrix();

        /// <summary>
        /// Returns this matrix as a dense matrix.
        /// </summary>
        /// <returns>A dense matrix.</returns>
        public abstract DenseMatrix<T> ToDenseMatrix();

        /// <summary>
        /// Returns this matrix as a diagonal matrix.
        /// </summary>
        /// <returns>A diagonal matrix.</returns>
        public abstract DiagonalMatrix<T> ToDiagonalMatrix();

        /// <summary>
        /// Returns this matrix as a sparse matrix stored in Dictionary-of-Keys (DOK) format.
        /// </summary>
        /// <returns>A DOK sparse matrix.</returns>
        public abstract DOKSparseMatrix<T> ToDOKSparseMatrix();

        /// <summary>
        /// Returns this matrix as a sparse matrix stored in Modified Compressed Sparse Row (MCSR) format.
        /// </summary>
        /// <returns>A MCSR sparse matrix.</returns>
        public abstract MCSRSparseMatrix<T> ToMCSRSparseMatrix();

        /// <summary>
        /// Returns this matrix as a sparse matrix stored in Skyline format.
        /// </summary>
        /// <returns>A Skyline matrix.</returns>
        public abstract SKYSparseMatrix<T> ToSKYSparseMatrix();

        #endregion


        #region Solvers

        /// <summary>
        /// Solve the triangular system $Ax = b$, where this matrix $A$ is either lower triangular or upper triangular, and 
        /// $x, b$ are dense vectors. Returns whether the solve was successful.
        /// </summary>
        /// <param name="b">The dense right-hand-side vector $b$.</param>
        /// <param name="x">The dense solution vector $x$.</param>
        /// <param name="blas">The level-1 BLAS implementation used.</param>
        /// <param name="lower">Specifies whether this matrix is a lower matrix.</param>
        /// <param name="transpose">Specifies whether this matrix should be transposed.</param>
        /// <returns><txt>true</txt> if the solve was successful.</returns>
        internal abstract bool SolveTriangular(T[] b, T[] x, IDenseBLAS1<T> blas, bool lower, bool transpose);

        public bool SolveTriangular(DenseVector<T> b, DenseVector<T> x, IDenseBLAS1Provider<T> blas, bool lower = true, bool transpose = false)
        {
            if (b is null)
            {
                throw new ArgumentNullException(nameof(b));
            }
            if (x is null)
            {
                throw new ArgumentNullException(nameof(x));
            }
            if (blas is null)
            {
                throw new ArgumentNullException(nameof(blas));
            }
            if (!(blas is IDenseBLAS1<T> blas1))
            {
                throw new NotSupportedException(ExceptionMessages.TypeNotSupported);
            }
            if (transpose)
            {
                if (b.Dimension < Columns)
                {
                    throw new ArgumentOutOfRangeException(nameof(b), $"The number of columns of the coefficient matrix does not match the dimension of '{nameof(b)}'.");
                }
                if (x.Dimension < Rows)
                {
                    throw new ArgumentOutOfRangeException(nameof(x), $"The number of rows of the coefficient matrix does not match the dimension of '{nameof(x)}'.");
                }
            }
            else
            {
                if (b.Dimension < Rows)
                {
                    throw new ArgumentOutOfRangeException(nameof(b), $"The number of rows of the coefficient matrix does not match the dimension of '{nameof(b)}'.");
                }
                if (x.Dimension < Columns)
                {
                    throw new ArgumentOutOfRangeException(nameof(x), $"The number of columns of the coefficient matrix does not match the dimension of '{nameof(x)}'.");
                }
            }
            return SolveTriangular(b.Values, x.Values, blas1, lower, transpose);
        }

        #endregion


        /// <summary>
        /// Prints this matrix to the Debug console, using a string formatter, up to a maximum number of rows and columns, and using 
        /// the specified row and column delimiters.
        /// </summary>
        /// <param name="ToString">A custom object string formatter, will use to default ToString() method if null.</param>
        /// <param name="maxRows">The maximum number of rows to print.</param>
        /// <param name="maxCols">The maximum number of columns to print.</param>
        /// <param name="columnDelimiter">The string delimiter to separate elements in adjacent columns.</param>
        /// <param name="rowDelimiter">The string delimiter to separate two adjacent rows.</param>
        public void Print(Func<T, string> ToString = null, int maxRows = -1, int maxCols = -1, string columnDelimiter = "\t", string rowDelimiter = "\r\n")
        {
            if (ToString == null)
            {
                ToString = obj => obj.ToString();
            }

            int r = maxRows < 0 ? Rows : Math.Min(Rows, maxRows);
            int c = maxCols < 0 ? Columns : Math.Min(Columns, maxCols);

            StringBuilder sb = new StringBuilder();
            int i, j;
            for (i = 0; i < r; ++i)
            {
                for (j = 0; j < c; ++j)
                {
                    sb.Append(ToString(this[i, j]) + columnDelimiter);
                }
                sb.Append(rowDelimiter);
            }
            Debug.WriteLine(sb.ToString());
        }

        /// <summary>
        /// Returns a deep clone of this matrix as an object.
        /// </summary>
        /// <returns>A deep clone of this matrix.</returns>
        public abstract object Clone();


        #region Operators

        /// <summary>
        /// Returns whether two matrices are equal in value, using the default equality comparer 
        /// of type <txt>T</txt> to compare matrix elements.
        /// </summary>
        /// <param name="A">The left matrix to compare.</param>
        /// <param name="B">The right matrix to compare.</param>
        /// <returns>Whether the two matrices are equal in value.</returns>
        public static bool operator ==(Matrix<T> A, Matrix<T> B)
        {
            if (A is null)
            {
                return false;
            }
            return A.Equals(B);
        }

        /// <summary>
        /// Returns whether two matrices are not equal in value, using the default equality comparer
        /// of type <txt>T</txt> to compare matrix elements.
        /// </summary>
        /// <param name="A">The left matrix to compare.</param>
        /// <param name="B">The right matrix to compare.</param>
        /// <returns>Whether the two matrices are not equal in value.</returns>
        public static bool operator !=(Matrix<T> A, Matrix<T> B) => !(A == B);

        public static Matrix<T> operator +(Matrix<T> A, Matrix<T> B)
        {
            if (A is null)
            {
                throw new ArgumentNullException(nameof(A));
            }
            return A.Add(B, ProviderFactory.GetDefaultProvider<T>());
        }

        public static DenseVector<T> operator *(Matrix<T> A, DenseVector<T> v)
        {
            if (A is null)
            {
                throw new ArgumentNullException(nameof(A));
            }
            return A.Multiply(v, ProviderFactory.GetDefaultProvider<T>());
        }

        public static SparseVector<T> operator *(Matrix<T> A, SparseVector<T> v)
        {
            if (A is null)
            {
                throw new ArgumentNullException(nameof(A));
            }
            return A.Multiply(v, ProviderFactory.GetDefaultProvider<T>());
        }

        #endregion

    }
}
