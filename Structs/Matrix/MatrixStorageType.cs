﻿
namespace LinearNet.Structs
{
    public enum MatrixStorageType
    {
        /// <summary>
        /// Represents a band matrix storage type for banded matrices
        /// </summary>
        BAND,

        /// <summary>
        /// Represents a block-Compressed Sparse Row storage type for blocked-sparse matrices
        /// </summary>
        BCSR,

        /// <summary>
        /// Represents a block-diagonal storage type for block-diagonal sparse matrices
        /// </summary>
        BLOCK_DIAGONAL,

        /// <summary>
        /// Represents a Coordinate List storage type for general sparse matrices
        /// </summary>
        COO,

        /// <summary>
        /// Represents a Compressed Sparse Column storage type for general sparse matrices
        /// </summary>
        CSC,

        /// <summary>
        /// Represents a Compressed Sparse Row storage type for general sparse matrices
        /// </summary>
        CSR,

        /// <summary>
        /// Represents a dense storage type for general dense matrices
        /// </summary>
        DENSE,

        /// <summary>
        /// Represents a diagonal storage type for diagonal matrices
        /// </summary>
        DIAGONAL,

        /// <summary>
        /// Represents a Dictionary-of-Keys storage type for general sparse matrices
        /// </summary>
        DOK,

        /// <summary>
        /// Represents a Ellpack storage type for general sparse matrices
        /// </summary>
        ELL,

        /// <summary>
        /// Represents a Modified Compressed Sparse Row storage type for square sparse matrices with a dense diagonal
        /// </summary>
        MCSR,

        /// <summary>
        /// Represents a Skyline storage type for low-bandwidth square sparse matrices that are either lower triangular,
        /// upper triangular, or symmetric
        /// </summary>
        SKYLINE
    }
}
