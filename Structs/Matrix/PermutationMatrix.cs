﻿using LinearNet.Global;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace LinearNet.Structs
{
    /// <summary>
    /// Implements a permutation matrix, which is a square binary matrix that
    /// <ul>
    /// <li>Permute the rows of a matrix when premultiplied by this matrix.</li>
    /// <li>Permute the columns of a matrix when postmultiplied by this matrix.</li>
    /// </ul>
    /// </summary>
    /// <cat>linear-algebra</cat>
    public class PermutationMatrix : IEquatable<PermutationMatrix>, ICloneable
    {
        private readonly int[] _permutationVector;

        internal int[] PermutationVector { get { return _permutationVector; } }

        /// <summary>
        /// Returns the dimension of this permutation matrix (all permutation matrices are square).
        /// </summary>
        public int Dimension { get; private set; }

        /// <summary>
        /// Returns whether this permutation matrix is the identity matrix.
        /// </summary>
        public bool IsIdentity 
        { 
            get
            {
                for (int i = 0; i < Dimension; ++i)
                {
                    if (_permutationVector[i] != i) return false;
                }
                return true;
            } 
        }

        /// <summary>
        /// Returns whether this permutation matrix is an exchange matrix.
        /// </summary>
        public bool IsExchange
        {
            get
            {
                int dim = Dimension, i, j;
                for (i = 0, j = dim - 1; i < dim; ++i, --j)
                {
                    if (_permutationVector[i] != j)
                    {
                        return false;
                    }
                }
                return true;
            }
        }

        /// <summary>
        /// Gets or sets the row of this matrix at row index <txt>index</txt>.
        /// </summary>
        /// <param name="rowIndex">The index of the row to return.</param>
        /// <returns>An array representing a row of the matrix.</returns>
        public int[] this[int rowIndex] 
        { 
            get 
            {
                if (rowIndex < 0 || rowIndex >= Dimension)
                {
                    throw new ArgumentOutOfRangeException(nameof(rowIndex));
                }

                int[] row = new int[Dimension];
                row[_permutationVector[rowIndex]] = 1;
                return row;
            }
            set 
            {
                throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Gets or sets an element of this matrix at row <txt>rowIndex</txt> 
        /// and column <txt>columnIndex</txt>.
        /// </summary>
        /// <param name="rowIndex">The row index of the element.</param>
        /// <param name="columnIndex">The column index of the element.</param>
        /// <returns>The element at (<txt>rowIndex</txt>, <txt>columnnIndex</txt>).</returns>
        public int this[int rowIndex, int columnIndex]
        {
            get
            {
                if (rowIndex < 0 || rowIndex >= Dimension)
                {
                    throw new ArgumentOutOfRangeException(nameof(rowIndex));
                }
                if (columnIndex < 0 || columnIndex >= Dimension)
                {
                    throw new ArgumentOutOfRangeException(nameof(rowIndex));
                }

                if (_permutationVector[rowIndex] == columnIndex)
                {
                    return 1;
                }
                return 0;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        #region Constructors

        /// <summary>
        /// Initializes a permutation matrix from a vector of permutations, where the $i$-th element 
        /// is equal to $\pi(i)$, the image of $i$ under the permutation.
        /// <para>
        /// A permutation array of length $n$ must contain each of the integers $1, 2, ..., n - 1$ in 
        /// some order.
        /// </para>
        /// </summary>
        /// <param name="permutations">An array containing a permutation of $1, 2, ..., n - 1$.</param>
        public PermutationMatrix(int[] permutations)
        {
            if (permutations == null)
            {
                throw new ArgumentNullException(nameof(permutations));
            }

            // Check to see if its an actual permutation matrix...
            int dim = permutations.Length;
            Dimension = dim;

            bool[] present = new bool[dim];
            foreach (int index in permutations)
            {
                present[index] = true;
            }
            foreach (bool b in present)
            {
                if (!b) throw new InvalidOperationException();
            }

            _permutationVector = permutations;
        }

        /// <summary>
        /// Initializes an identity permutation matrix of the specified dimensions.
        /// </summary>
        /// <param name="dimension">The dimension of the permutation matrix.</param>
        public PermutationMatrix(int dimension)
        {
            if (dimension <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(dimension), ExceptionMessages.NonPositiveNotAllowed);
            }

            _permutationVector = new int[dimension];
            for (int d = 0; d < dimension; ++d)
            {
                _permutationVector[d] = d;
            }
        }

        /// <summary>
        /// Create a permutation matrix using a dense matrix.
        /// <para>Throws <txt>ArgumentOutOfRangeException</txt> if the matrix does not represent a permutation matrix.</para>
        /// </summary>
        /// <param name="matrix">The dense matrix.</param>
        public PermutationMatrix(DenseMatrix<int> matrix)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            if (!matrix.IsSquare)
            {
                throw new ArgumentOutOfRangeException(nameof(matrix), ExceptionMessages.MatrixNotSquare);
            }

            Dimension = matrix.Rows;

            // Extract the permutation vector
            _permutationVector = new int[Dimension];
            bool[] mappedTo = new bool[Dimension];

            for (int i = 0; i < Dimension; ++i)
            {
                int[] row = matrix[i];
                for (int j = 0; j < Dimension; ++j)
                {
                    int e = row[j];
                    if (e != 0)
                    {
                        if (e != 1) throw new ArgumentOutOfRangeException($"{nameof(matrix)} is not a permutation matrix.");
                        _permutationVector[i] = j;
                        mappedTo[j] = true;
                        break;
                    }
                }
            }

            // Check that every index is mapped to
            foreach (bool map in mappedTo)
            {
                if (!map)
                {
                    throw new ArgumentOutOfRangeException($"{nameof(matrix)} is not a permutation matrix.");
                }
            }
        }

        /// <summary>
        /// Create a permutation matrix using a Dictionary-of-Keys sparse matrix.
        /// <para>Throws <txt>ArgumentOutOfRangeException</txt> if the matrix is not a permutation matrix.</para>
        /// </summary>
        /// <param name="matrix">A sparse matrix.</param>
        public PermutationMatrix(DOKSparseMatrix<int> matrix)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            if (!matrix.IsSquare)
            {
                throw new ArgumentOutOfRangeException(nameof(matrix), ExceptionMessages.MatrixNotSquare);
            }

            Dimension = matrix.Rows;
            long lc = Dimension;
            _permutationVector = new int[Dimension];
            bool[] mapped = new bool[Dimension];

            // Currently, this will allow cases for many-to-1 maps... without throwing an exception
            foreach (KeyValuePair<long, int> entry in matrix.Values)
            {
                if (entry.Value == 0)
                {
                    continue;
                }
                if (entry.Value != 1)
                {
                    throw new ArgumentOutOfRangeException($"'{nameof(matrix)}' is not a permutation matrix.");
                }

                int row = (int)(entry.Key / lc), col = (int)(entry.Key % lc);
                if (mapped[col])
                {
                    // Already mapped 
                    throw new ArgumentOutOfRangeException($"'{nameof(matrix)}' is not a permutation matrix.");
                }

                _permutationVector[row] = col;

                mapped[col] = true;
            }

            foreach (bool m in mapped)
            {
                if (!m)
                {
                    throw new ArgumentOutOfRangeException($"'{nameof(matrix)}' is not a permutation matrix.");
                }
            }
        }

        /// <summary>
        /// Create a permutation matrix using a Coordinate List (COO) sparse matrix.
        /// <para>Throws <txt>ArgumentOutOfRangeException</txt> if the matrix is not a permutation matrix.</para>
        /// </summary>
        /// <param name="matrix">The sparse matrix</param>
        public PermutationMatrix(COOSparseMatrix<int> matrix)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            if (!matrix.IsSquare)
            {
                throw new ArgumentOutOfRangeException(nameof(matrix), ExceptionMessages.MatrixNotSquare);
            }

            Dimension = matrix.Rows;
            _permutationVector = new int[Dimension];
            bool[] mapped = new bool[Dimension];

            // Currently allows many-to-1 mappings without throwing exception
            foreach (MatrixEntry<int> entry in matrix.Values)
            {
                if (entry.Value == 0)
                {
                    continue;
                }
                if (entry.Value != 1 || mapped[entry.ColumnIndex])
                {
                    throw new ArgumentOutOfRangeException($"'{nameof(matrix)}' is not a permutation matrix.");
                }
                _permutationVector[entry.RowIndex] = entry.ColumnIndex;
                mapped[entry.ColumnIndex] = true;
            }

            // Check that all indices are mapped to
            foreach (bool map in mapped)
            {
                if (!map)
                {
                    throw new ArgumentOutOfRangeException($"'{nameof(matrix)}' is not a permutation matrix.");
                }
            }
        }

        /// <summary>
        /// Create a permutation matrix using a Compressed Sparse Row (CSR) sparse matrix.
        /// </summary>
        /// <param name="matrix">The sparse matrix.</param>
        public PermutationMatrix(CSRSparseMatrix<int> matrix)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            if (!matrix.IsSquare)
            {
                throw new ArgumentOutOfRangeException(nameof(matrix), ExceptionMessages.MatrixNotSquare);
            }

            Dimension = matrix.Rows;
            _permutationVector = new int[Dimension];
            bool[] mapped = new bool[Dimension];

            for (int r = 0; r < Dimension; ++r)
            {
                int row_end = matrix.RowIndices[r + 1];
                for (int i = matrix.RowIndices[r]; i < row_end; ++i)
                {
                    int value = matrix.Values[i];
                    if (value == 0) continue;

                    int columnIndex = matrix.ColumnIndices[i];
                    if (value != 1 || !mapped[columnIndex])
                    {
                        throw new ArgumentOutOfRangeException($"'{nameof(matrix)}' is not a permutation matrix.");
                    }

                    _permutationVector[r] = columnIndex;
                    mapped[columnIndex] = true;
                    break; // stop iterating over this row - could potentially miss some violations?
                }
            }

            foreach (bool m in mapped)
            {
                if (!m)
                {
                    throw new ArgumentOutOfRangeException($"'{nameof(matrix)}' is not a permutation matrix.");
                }
            }
        }
        #endregion


        #region Static methods

        /// <summary>
        /// Creates the identity permutation matrix that maps all matrices to itself.
        /// </summary>
        /// <param name="dimension">The row/column dimension of the matrix.</param>
        /// <returns>The identity permutation matrix.</returns>
        public static PermutationMatrix Identity(int dimension)
        {
            int[] permutations = new int[dimension];
            for (int d = 0; d < dimension; ++d)
            {
                permutations[d] = d;
            }
            return new PermutationMatrix(permutations);
        }

        /// <summary>
        /// Creates an exchange matrix of the specified dimension, which reverses the rows of a matrix
        /// that it premultiplies. All 1's of this permutation matrix reside on the main counter-diagonal.
        /// </summary>
        /// <param name="dimension"></param>
        /// <returns></returns>
        public static PermutationMatrix Exchange(int dimension)
        {
            int[] permutations = new int[dimension];
            for (int d = 0, p = dimension - 1; d < dimension; ++d, --p)
            {
                permutations[d] = p;
            }
            return new PermutationMatrix(permutations);
        }

        /// <summary>
        /// Creates a random permutation matrix.
        /// </summary>
        /// <param name="dimension">The row/column dimension of the matrix.</param>
        /// <param name="seed">
        /// The seed for the <txt>Random</txt> object.
        /// <para>If negative, the random object will not be seeded.</para>
        /// </param>
        /// <returns>A random permutation matrix.</returns>
        public static PermutationMatrix Random(int dimension, int seed = -1)
        {
            int[] pvector = new int[dimension];
            for (int i = 0; i < dimension; ++i)
            {
                pvector[i] = i;
            }

            Random r;
            if (seed < 0)
            {
                r = new Random();
            }
            else
            {
                r = new Random(seed);
            }
            pvector.Shuffle(r);

            return new PermutationMatrix(pvector);
        }

        /// <summary>
        /// Creates a random permutation matrix subject to a set of fixed permutations.
        /// <para>
        /// The fixed permutations are in the form of key-value pairs such that each key
        /// must map to the corresponding value in the generated permutation matrix.
        /// </para>
        /// <para>
        /// Throws <txt>InvalidOperationException</txt> if <txt>fixedPermutations</txt> contains
        /// two (distinct) keys that map to the same value.
        /// </para>
        /// </summary>
        /// <param name="dimension">The row/column dimension of the permutation matrix.</param>
        /// <param name="fixedPermutations">
        /// A set of pre-determined permutations that must be satisfied by the 
        /// resulting permutation matrix. The set of fixed permutations is stored as a dictionary of 
        /// pairs of integers $(k, v)$ such that $v = \pi(k)$.
        /// </param>
        /// <param name="seed">The seed for the <txt>Random</txt> object used to randomize the 
        /// matrix. If negative, a seed will not be used. 
        /// </param>
        /// <returns></returns>
        public static PermutationMatrix Random(int dimension, Dictionary<int, int> fixedPermutations, int seed = -1)
        {
            if (dimension <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(dimension), ExceptionMessages.NonPositiveNotAllowed);
            }
            if (fixedPermutations == null)
            {
                throw new ArgumentNullException(nameof(fixedPermutations));
            }

            // Check dimensionality and uniqueness
            HashSet<int> valueSet = new HashSet<int>();
            foreach (KeyValuePair<int, int> p in fixedPermutations)
            {
                if (p.Key < 0 || p.Key >= dimension || p.Value < 0 || p.Value >= dimension)
                {
                    throw new ArgumentOutOfRangeException(nameof(fixedPermutations));
                }
                if (valueSet.Contains(p.Value))
                {
                    throw new InvalidOperationException("The parameter 'fixedPermutations' contains duplicate values.");
                }
                valueSet.Add(p.Value);
            }

            // Generate a permutation vector for the unknown indices.
            int degFreedom = dimension - fixedPermutations.Count, i, j;
            int[] pvect = new int[degFreedom];
            for (i = 0, j = 0; i < dimension; ++i)
            {
                if (valueSet.Contains(i)) continue;
                pvect[j++] = i;
            }

            Random r;
            if (seed < 0)
            {
                r = new Random();
            }
            else
            {
                r = new Random(seed);
            }
            pvect.Shuffle(r);

            int[] permutation = new int[dimension];
            for (i = 0, j = 0; i < dimension; ++i)
            {
                if (fixedPermutations.ContainsKey(i))
                {
                    permutation[i] = fixedPermutations[i];
                }
                else
                {
                    permutation[i] = pvect[j++];
                }
            }
            return new PermutationMatrix(permutation);
        }

        #endregion


        /// <summary>
        /// Returns the image of an index $i$ under this permutation, $\pi(i)$.
        /// </summary>
        /// <param name="index">An index less than the dimension of this permutation matrix.</param>
        /// <returns>The image of <txt>index</txt> under this permutation.</returns>
        public int GetMapping(int index)
        {
            if (index < 0 || index >= Dimension)
            {
                throw new ArgumentOutOfRangeException(nameof(index));
            }

            return _permutationVector[index];
        }

        /// <summary>
        /// Returns the pre-image of an index $i$ under this permutation, i.e. returns a integer $j$ such that 
        /// $\pi(j) = i$.
        /// </summary>
        /// <param name="index">An index less than the dimension of this permutation matrix.</param>
        /// <returns>The preimage of <txt>index</txt> under this permutation.</returns>
        public int MapsFrom(int index)
        {
            if (index < 0 || index >= Dimension)
            {
                throw new ArgumentOutOfRangeException(nameof(index));
            }

            for (int i = 0; i < Dimension; ++i)
            {
                if (_permutationVector[i] == index)
                {
                    return i;
                }
            }

            throw new InvalidOperationException(nameof(index));
        }

        /// <summary>
        /// Returns the inverse of this permutation matrix, $P^{-1}$, which is another 
        /// permutation matrix such that the product of this matrix and its inverse is the identity matrix.
        /// </summary>
        /// <returns>The inverse permutation matrix.</returns>
        public PermutationMatrix Inverse()
        {
            return Transpose();
        }

        /// <summary>
        /// Returns the transpose of this permutation matrix, $P^T$.
        /// </summary>
        /// <returns>The transposed matrix.</returns>
        public PermutationMatrix Transpose()
        {
            int[] transpose = new int[Dimension];
            for (int i = 0; i < Dimension; ++i)
            {
                transpose[_permutationVector[i]] = i;
            }
            return new PermutationMatrix(transpose);
        }

        /// <summary>
        /// Swaps the rows at indices <txt>rowIndex1</txt> and <txt>rowIndex2</txt>.
        /// </summary>
        /// <param name="rowIndex1">The index of the first row to swap.</param>
        /// <param name="rowIndex2">The index of the second row to swap.</param>
        public void SwapRows(int rowIndex1, int rowIndex2)
        {
            if (rowIndex1 < 0 || rowIndex1 >= Dimension) throw new ArgumentOutOfRangeException(nameof(rowIndex1));
            if (rowIndex2 < 0 || rowIndex2 >= Dimension) throw new ArgumentOutOfRangeException(nameof(rowIndex2));

            int tmp = _permutationVector[rowIndex1];
            _permutationVector[rowIndex1] = _permutationVector[rowIndex2];
            _permutationVector[rowIndex2] = tmp;
        }

        /// <summary>
        /// Swaps the columns at indices <txt>colIndex1</txt> and <txt>colIndex2</txt>.
        /// </summary>
        /// <param name="colIndex1">The index of the first column to swap.</param>
        /// <param name="colIndex2">The index of the second column to swap.</param>
        public void SwapColumns(int colIndex1, int colIndex2)
        {
            if (colIndex1 < 0 || colIndex1 >= Dimension) throw new ArgumentOutOfRangeException(nameof(colIndex1));
            if (colIndex2 < 0 || colIndex2 >= Dimension) throw new ArgumentOutOfRangeException(nameof(colIndex2));

            int i1 = -1, i2 = -1;
            for (int d = 0; d < Dimension; ++d)
            {
                if (_permutationVector[d] == colIndex1)
                {
                    i1 = d;
                }
                if (_permutationVector[d] == colIndex2)
                {
                    i2 = d;
                }
            }

            _permutationVector[i1] = colIndex2;
            _permutationVector[i2] = colIndex1;
        }

        /// <summary>
        /// Returns a new dense vector with its elements permutated using this permutation matrix.
        /// </summary>
        /// <param name="vector">A dense vector of the same length as the dimension of this matrix.</param>
        /// <returrns>A dense vector.</returrns>
        public DenseVector<T> Multiply<T>(DenseVector<T> vector) where T : new() => new DenseVector<T>(Multiply(vector.Values));

        /// <summary>
        /// Returns a new array with its elements permutated using this permuation matrix.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="vector">An array of the same length as the dimension of this permutation matrix.</param>
        /// <returns>An array of the same length as the dimension of this permutation matrix.</returns>
        public T[] Multiply<T>(T[] vector)
        {
            if (vector == null)
            {
                throw new ArgumentNullException(nameof(vector));
            }
            if (vector.Length != Dimension)
            {
                throw new InvalidOperationException(nameof(vector));
            }

            int dim = vector.Length, i;
            T[] product = new T[dim];
            for (i = 0; i < dim; ++i)
            {
                product[i] = vector[_permutationVector[i]];
            }
            return product;
        }

        /// <summary>
        /// Postmultiply this matrix $P$ by a sparse vector $v$, returning the product $Pv$ as a sparse vector.
        /// This is equivalent to permutating the vector with this permutation matrix.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="vector">The vector to postmultiply by.</param>
        /// <returns>The matrix-vector product.</returns>
        public BigSparseVector<T> Multiply<T>(BigSparseVector<T> vector) where T : new()
        {
            if (vector is null)
            {
                throw new ArgumentNullException(nameof(vector));
            }
            if (vector.Dimension != Dimension)
            {
                throw new InvalidOperationException($"The dimension of '{nameof(vector)}' does not match the dimension of the permutation matrix.");
            }

            Dictionary<long, T> product = new Dictionary<long, T>(vector.Values.Count);

            // temporary store the transpose
            int[] transpose = new int[Dimension];
            for (int i = 0; i < Dimension; ++i)
            {
                transpose[_permutationVector[i]] = i;
            }

            foreach (KeyValuePair<long, T> entry in vector.Values)
            {
                int index = (int)entry.Key;
                product[transpose[index]] = entry.Value;
            }

            return new BigSparseVector<T>(Dimension, product);
        }

        /// <summary>
        /// Returns a new dense matrix whose rows have been permuted using this permutation matrix,
        /// i.e. returns the matrix product $PA$.
        /// </summary>
        /// <param name="matrix">A dense matrix with the same number of rows as this permutation matrix.</param>
        /// <returms>A dense matrix under the row permutation.</returms>
        public DenseMatrix<T> Multiply<T>(DenseMatrix<T> matrix) where T : new()
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            if (matrix.Rows != Dimension)
            {
                throw new InvalidOperationException($"The number of rows of '{nameof(matrix)}' does not match the dimension of the permutation matrix.");
            }

            T[][] values = matrix.Values;
            T[][] product = new T[Dimension][];
            for (int i = 0; i < Dimension; ++i)
            {
                product[i] = values[_permutationVector[i]].Copy();
            }
            return new DenseMatrix<T>(product.Length, product[0].Length, product);
        }

        /// <summary>
        /// <para>
        /// Returns the product of this permutation matrix with another permutation matrix (the product itself
        /// is also a permutation matrix).
        /// </para>
        /// <para>
        /// The product matrix represents the composition of the two permutations.
        /// </para>
        /// </summary>
        /// <param name="matrix">A permutation matrix of the same dimensions as this permutation matrix.</param>
        /// <returns>The permutation matrix product.</returns>
        public PermutationMatrix Multiply(PermutationMatrix matrix)
        {
            if (matrix == null) throw new ArgumentNullException(nameof(matrix));
            if (Dimension != matrix.Dimension)
            {
                throw new InvalidOperationException(ExceptionMessages.MatrixSize);
            }

            int[] product = new int[Dimension], pvector = matrix._permutationVector;
            for (int i = 0; i < Dimension; ++i)
            {
                product[i] = _permutationVector[pvector[i]];
            }
            return new PermutationMatrix(product);
        }


        #region Conversion methods

        /// <summary>
        /// Convert the matrix into a dense matrix over 32-bit integers.
        /// </summary>
        /// <returns>The dense matrix equivalent of this permutation matrix.</returns>
        public DenseMatrix<int> ToDenseMatrix()
        {
            DenseMatrix<int> dense = new DenseMatrix<int>(Dimension, Dimension);
            int[][] values = dense.Values;
            for (int i = 0; i < Dimension; ++i)
            {
                values[i][_permutationVector[i]] = 1;
            }
            return dense;
        }

        /// <summary>
        /// Convert this matrix into a sparse matrix in Dictionary-of-Keys (DOK) storage format.
        /// </summary>
        /// <returns>The sparse matrix equivalent of this permutation matrix.</returns>
        public DOKSparseMatrix<int> ToDOKSparseMatrix()
        {
            Dictionary<long, int> values = new Dictionary<long, int>();
            int dim = Dimension, d;
            for (d = 0; d < dim; ++d)
            {
                values[d * dim + _permutationVector[d]] = 1;
            }
            return new DOKSparseMatrix<int>(dim, dim, values);
        }

        /// <summary>
        /// Convert this matrix into a sparse matrix in Coordinate List (COO) storage format.
        /// </summary>
        /// <returns></returns>
        public COOSparseMatrix<int> ToCOOSparseMatrix()
        {
            List<MatrixEntry<int>> values = new List<MatrixEntry<int>>(Dimension);
            for (int i = 0; i < Dimension; ++i)
            {
                values.Add(new MatrixEntry<int>(i, _permutationVector[i], 1));
            }
            return new COOSparseMatrix<int>(Dimension, Dimension, values, true);
        }

        /// <summary>
        /// Convert this matrix into a sparse matrix in Compressed Sparse Row (CSR) storage format.
        /// </summary>
        /// <returns></returns>
        public CSRSparseMatrix<int> ToCSRSparseMatrix()
        {
            int dim = Dimension;
            int[] rowIndices = new int[dim + 1];
            int[] columnIndices = _permutationVector.Copy();
            int[] values = RectangularVector.Repeat(1, dim);

            for (int i = 1; i <= dim; ++i)
            {
                rowIndices[i] = i;
            }
            return new CSRSparseMatrix<int>(dim, rowIndices, columnIndices, values);
        }

        #endregion


        /// <summary>
        /// Returns whether this permutation matrix is equal to another.
        /// </summary>
        /// <param name="other">A permutation matrix.</param>
        /// <returns>Whether the two permutation matrices are equal.</returns>
        public bool Equals(PermutationMatrix other)
        {
            if (other == null) return false;
            if (Dimension != other.Dimension) return false;

            int[] pvector = other._permutationVector;
            for (int i = 0; i < Dimension; ++i)
            {
                if (_permutationVector[i] != pvector[i])
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Creates a deep copy of this matrix, returning it as an object.
        /// </summary>
        /// <returns>A copy of this matrix as an object.</returns>
        public object Clone()
        {
            return Copy();
        }

        /// <summary>
        /// Creates a deep copy of this matrix, returning it as a <txt>PermutationMatrix</txt>
        /// </summary>
        /// <returns>A copy of this matrix as a <txt>PermutationMatrix</txt>.</returns>
        public PermutationMatrix Copy()
        {
            int[] permutationVector = _permutationVector.Copy();
            return new PermutationMatrix(permutationVector);
        }

        public void Print(Func<int, string> ToString = null, int maxRows = -1, int maxCols = -1, string columnDelimiter = "\t", string rowDelimiter = "\r\n")
        {
            if (ToString == null)
            {
                ToString = obj => obj.ToString();
            }

            int r = maxRows < 0 ? Dimension : Math.Min(Dimension, maxRows);
            int c = maxCols < 0 ? Dimension : Math.Min(Dimension, maxCols);

            StringBuilder sb = new StringBuilder();
            int i, j;
            for (i = 0; i < r; ++i)
            {
                for (j = 0; j < c; ++j)
                {
                    sb.Append(ToString(this[i, j]) + columnDelimiter);
                }
                sb.Append(rowDelimiter);
            }
            Debug.WriteLine(sb.ToString());
        }


        #region Operators 

        public static PermutationMatrix operator *(PermutationMatrix p, PermutationMatrix q)
        {
            if (p is null)
            {
                throw new ArgumentNullException(nameof(p));
            }
            return p.Multiply(q);
        }

        #endregion


        #region Casts 
        public static explicit operator PermutationMatrix(DenseMatrix<int> matrix) => new PermutationMatrix(matrix);
        public static explicit operator PermutationMatrix(DOKSparseMatrix<int> matrix) => new PermutationMatrix(matrix);
        public static explicit operator PermutationMatrix(COOSparseMatrix<int> matrix) => new PermutationMatrix(matrix);
        public static explicit operator PermutationMatrix(CSRSparseMatrix<int> matrix) => new PermutationMatrix(matrix);
        #endregion
    }
}
