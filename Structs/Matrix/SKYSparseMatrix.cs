﻿using LinearNet.Global;
using LinearNet.Matrices;
using LinearNet.Matrices.Sparse.Decompositions;
using LinearNet.Providers;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace LinearNet.Structs
{
    /// <summary>
    /// Skyline storage format, used to represent a square matrix, which can either be:
    /// (i) an upper matrix, (ii) a lower matrix or (iii) a symmetric matrix.
    /// 
    /// Suitable for almost-banded sparse matrices.
    /// </summary>
    /// <cat>linear-algebra</cat>
    public sealed class SKYSparseMatrix<T> : Matrix<T> where T : new()
    {
        private SKYType _type;
        private readonly T _zero;
        private T[] _values;
        private int[] _indices;

        internal T[] Values { get { return _values; } }
        internal int[] Indices { get { return _indices; } }

        public SKYType Type { get { return _type; } }

        public override T[] this[int index]
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }
        public override T this[int rowIndex, int columnIndex] 
        {
            get
            {
                // symmetric matrices are stored in lower form
                if (_type == SKYType.SYMMETRIC && rowIndex < columnIndex)
                {
                    return this[columnIndex, rowIndex];
                }

                if (rowIndex < 0 || rowIndex >= Rows)
                {
                    throw new ArgumentOutOfRangeException(nameof(rowIndex));
                }
                if (columnIndex < 0 || columnIndex >= Columns)
                {
                    throw new ArgumentOutOfRangeException(nameof(columnIndex));
                }

                if (_type == SKYType.LOWER || _type == SKYType.SYMMETRIC)
                {
                    if (rowIndex < columnIndex)
                    {
                        return _zero;
                    }

                    // _indices is arranged by row index
                    int row_start = _indices[rowIndex], row_end = _indices[rowIndex + 1];
                    int index = columnIndex - rowIndex + row_end - 1;
                    if (index >= row_start)
                    {
                        return _values[index];
                    }

                    // Out of range!
                    return _zero;
                }
                else
                {
                    // Upper
                    if (rowIndex > columnIndex)
                    {
                        return _zero;
                    }

                    int col_start = _indices[columnIndex], col_end = _indices[columnIndex + 1];
                    int index = rowIndex - columnIndex + col_end - 1;
                    if (index >= col_start)
                    {
                        return _values[index];
                    }

                    return _zero;
                }
                
            }
            set
            {
                // symmetric matrices are stored in lower form
                if (_type == SKYType.SYMMETRIC && columnIndex > rowIndex)
                {
                    this[columnIndex, rowIndex] = value;
                    return;
                }

                if (rowIndex < 0 || rowIndex >= Rows)
                {
                    throw new ArgumentOutOfRangeException(nameof(rowIndex));
                }
                if (columnIndex < 0 || columnIndex > Columns)
                {
                    throw new ArgumentOutOfRangeException(nameof(columnIndex));
                }

                if (_type == SKYType.LOWER || _type == SKYType.SYMMETRIC)
                {
                    if (columnIndex > rowIndex)
                    {
                        return;
                    }

                    int row_start = _indices[rowIndex], row_end = _indices[rowIndex + 1];
                    int index = columnIndex - rowIndex + row_end - 1;
                    if (index >= row_start)
                    {
                        _values[index] = value;
                    }

                    // Out of bounds - ignore
                    return;
                }
                
                else if (_type == SKYType.UPPER)
                {
                    if (rowIndex > columnIndex)
                    {
                        return;
                    }

                    int col_start = _indices[columnIndex], col_end = _indices[columnIndex + 1];
                    int index = rowIndex - columnIndex + col_end - 1;
                    if (index >= col_start)
                    {
                        _values[index] = value;
                    }

                    // Is there a better scheme than to silently fail?
                    return;
                }
            }
        }

        public override int SymbolicNonZeroCount => _indices[Rows];

        public override IEnumerable<MatrixEntry<T>> NonZeroEntries
        {
            get
            {
                // Row-major order
                if (_type == SKYType.LOWER)
                {
                    for (int r = 0; r < Rows; ++r)
                    {
                        int start = _indices[r], end = _indices[r + 1], c = r - (end - start) + 1;
                        for (int i = start; c <= r; ++c, ++i)
                        {
                            yield return new MatrixEntry<T>(r, c, _values[i]);
                        }
                    }
                }

                // Column-major order
                else if (_type == SKYType.UPPER)
                {
                    for (int c = 0; c < Columns; ++c)
                    {
                        int start = _indices[c], end = _indices[c + 1], r = c - (end - start) + 1;
                        for (int i = start; r <= c; ++r, ++i)
                        {
                            yield return new MatrixEntry<T>(r, c, _values[i]);
                        }
                    }
                }

                // Undefined order
                else if (_type == SKYType.SYMMETRIC)
                {
                    for (int r = 0; r < Rows; ++r)
                    {
                        int start = _indices[r], end = _indices[r + 1], c = r - (end - start) + 1;
                        for (int i = start; c <= r; ++c, ++i)
                        {
                            yield return new MatrixEntry<T>(r, c, _values[i]);
                        }
                    }
                    for (int c = 0; c < Columns; ++c)
                    {
                        int start = _indices[c], end = _indices[c + 1], r = c - (end - start) + 1;
                        // Not inclusive of diagonal
                        for (int i = start; r < c; ++r, ++i)
                        {
                            yield return new MatrixEntry<T>(r, c, _values[i]);
                        }
                    }
                }
            }
        }


        #region Constructors

        /// <summary>
        /// Create an empty (zero) sparse Skyline matrix with the specified number of rows/columns.
        /// </summary>
        /// <param name="dimension">The number of rows and columns in this matrix (matrix is always square).</param>
        public SKYSparseMatrix(int dimension) : base(MatrixStorageType.SKYLINE, dimension, dimension)
        {
            _zero = new T();
            _values = new T[0];
            _indices = new int[dimension + 1];
        }

        /// <summary>
        /// Create a Skyline matrix given an array of indices and the values of the non-zeroes in the 
        /// matrix.
        /// </summary>
        /// <param name="indices"></param>
        /// <param name="values"></param>
        /// <param name="type"></param>
        /// <param name="skipChecks"></param>
        public SKYSparseMatrix(int[] indices, T[] values, SKYType type, bool skipChecks) : base(MatrixStorageType.SKYLINE)
        {
            if (indices is null)
            {
                throw new ArgumentNullException(nameof(indices));
            }
            if (values is null)
            {
                throw new ArgumentNullException(nameof(values));
            }

            if (!skipChecks)
            {
                int len = indices.Length;
                if (len == 0)
                {
                    throw new ArgumentOutOfRangeException(nameof(indices), "Length of index array cannot be zero.");
                }
                if (indices[0] != 0)
                {
                    throw new ArgumentOutOfRangeException(nameof(indices), "The first entry of the index array must be zero.");
                }
                if (indices[len - 1] != values.Length)
                {
                    throw new ArgumentOutOfRangeException(nameof(indices), "The last entry of the index array must equal the length of the values array.");
                }
                
                for (int i = 1; i <= len; ++i)
                {
                    // indices must be monotonic increasing
                    if (indices[i - 1] > indices[i])
                    {
                        throw new ArgumentOutOfRangeException(nameof(indices), "The index array must be monotonically increasing.");
                    }

                    // row lengths cannot exceed the boundary of the matrix
                    int length = indices[i] - indices[i - 1];
                    if (length > i)
                    {
                        throw new ArgumentOutOfRangeException(nameof(indices), "The index array contains a row/column that extends outside the bounds of the matrix.");
                    }
                }
            }

            Rows = indices.Length - 1;
            Columns = Rows;

            _indices = indices;
            _values = values;
            _type = type;
        }

        /// <summary>
        /// Create a sparse Skyline matrix from a band matrix.
        /// </summary>
        /// <param name="matrix">The band matrix</param>
        /// <param name="type"></param>
        public SKYSparseMatrix(BandMatrix<T> matrix, SKYType type) : base(MatrixStorageType.SKYLINE)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            if (!matrix.IsSquare)
            {
                throw new InvalidOperationException(ExceptionMessages.MatrixNotSquare);
            }

            Rows = matrix.Rows;
            Columns = matrix.Columns;
            _zero = new T();
            _type = type;

            int dim = Rows,
                upperBandwidth = matrix.UpperBandwidth,
                lowerBandwidth = matrix.LowerBandwidth;
            T[][] upper = matrix.UpperBands,
                lower = matrix.LowerBands;
            T[] diagonal = matrix.Diagonal;

            _indices = new int[dim + 1];

            if (type == SKYType.LOWER || type == SKYType.SYMMETRIC)
            {
                // make the simplifying assumption that the each individual band contains non-zeroes

                // Accumulate the number of non-zeroes in each row
                for (int r = 0; r < dim; ++r)
                {
                    // Find the starting index for that row
                    // offset = the number of leading zeroes
                    int b = Math.Min(r, lowerBandwidth) - 1, k = Math.Max(0, r - lowerBandwidth), offset = 0;
                    while (b >= 0 && k < lower[b].Length && lower[b][k].Equals(_zero))
                    {
                        --b;
                        ++k;
                        ++offset;
                    }
                    _indices[r + 1] = _indices[r] + Math.Min(r, lowerBandwidth) + 1 - offset;
                }

                // Initialize the values array
                _values = Zeroes(_indices[dim]);

                // Insert values from the diagonal
                for (int i = 0; i < dim; ++i)
                {
                    _values[IndexLowerSymmetric(i, i)] = diagonal[i];
                }

                // Insert values from the lower bands
                for (int b = 0; b < lowerBandwidth; ++b)
                {
                    T[] band = lower[b];
                    int len = band.Length, offset = b + 1;
                    for (int i = 0; i < len; ++i)
                    {
                        if (!band[i].Equals(_zero))
                        {
                            _values[IndexLowerSymmetric(i + offset, i)] = band[i];
                        }
                    }
                }
            }
            else if (type == SKYType.UPPER)
            {
                // Accumulate the number of non-zeroes in each column
                for (int r = 0; r < dim; ++r)
                {
                    // Find the starting index for that row
                    // offset = the number of leading zeroes
                    int b = Math.Min(r, upperBandwidth) - 1, k = Math.Max(0, r - upperBandwidth), offset = 0;
                    while (b >= 0 && k < upper[b].Length && upper[b][k].Equals(_zero))
                    {
                        --b;
                        ++k;
                        ++offset;
                    }
                    _indices[r + 1] = _indices[r] + Math.Max(r, upperBandwidth) + 1 - offset;
                }

                // Init the values array
                _values = Zeroes(_indices[dim]);

                // Insert values from the diagonal
                for (int i = 0; i < dim; ++i)
                {
                    _values[IndexUpper(i, i)] = diagonal[i];
                }

                // Insert values from the upper bands
                for (int b = 0; b < upperBandwidth; ++b)
                {
                    T[] band = upper[b];
                    int len = band.Length, offset = b + 1;
                    for (int i = 0; i < len; ++i)
                    {
                        if (!band[i].Equals(_zero))
                        {
                            _values[IndexUpper(i, i + offset)] = band[i];
                        }
                    }
                }
            }
            else
            {
                throw new NotSupportedException(nameof(type));
            }
        }

        public SKYSparseMatrix(BlockDiagonalMatrix<T> matrix) : base(MatrixStorageType.SKYLINE)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            if (!matrix.IsSquare)
            {
                throw new InvalidOperationException("Matrix cannot be stored in Skyline format because it is not square.");
            }

            int nBlocks = matrix.Blocks.Length;

            if (matrix.IsUpper())
            {
                _type = SKYType.UPPER;

                _indices = new int[Columns + 1];
                for (int b = 0; b < nBlocks; ++b)
                {
                    DenseMatrix<T> block = matrix.Blocks[b];
                    int offset = matrix.BlockIndices[b], dim = block.Rows;
                    for (int c = 1; c <= dim; ++c)
                    {
                        _indices[offset + c] = c;
                    }
                }

                // Accumulate
                for (int c = 0; c < Columns; ++c)
                {
                    _indices[c + 1] += _indices[c];
                }

                _values = new T[_indices[Columns]];
                for (int b = 0, k = 0; b < nBlocks; ++b)
                {
                    DenseMatrix<T> block = matrix.Blocks[b];
                    T[][] values = block.Values;
                    int offset = matrix.BlockIndices[b], dim = block.Rows;
                    for (int c = 0; c < dim; ++c)
                    {
                        for (int r = 0; r <= c; ++r)
                        {
                            _values[k++] = values[r][c];
                        }
                    }
                }
                return;
            }

            // Lower or symmetric case
            if (matrix.IsLower())
            {
                _type = SKYType.LOWER;
            }
            else
            {
                if (!matrix.IsSymmetric(EqualityComparer<T>.Default))
                {
                    throw new InvalidOperationException("Matrix cannot be stored in Skyline format because it is not lower triangular, upper " +
                        "triangular, or symmetric.");
                }
                _type = SKYType.SYMMETRIC;
            }

            _indices = new int[Rows + 1];

            // 1st pass - count the number of non-zeroes in each row
            for (int b = 0; b < nBlocks; ++b)
            {
                DenseMatrix<T> block = matrix.Blocks[b];
                int offset = matrix.BlockIndices[b], dim = block.Rows;
                for (int r = 1; r <= dim; ++r)
                {
                    //_indices[offset + r + 1] = r + 1;
                    _indices[offset + r] = r;
                }
            }

            // Accumulate
            for (int r = 0; r < Rows; ++r)
            {
                _indices[r + 1] += _indices[r];
            }

            // 2nd pass - fill in the numeric values
            _values = new T[_indices[Rows]];

            for (int b = 0, k = 0; b < nBlocks; ++b)
            {
                DenseMatrix<T> block = matrix.Blocks[b];
                int dim = block.Rows;
                for (int r = 0; r < dim; ++r)
                {
                    T[] row = block.Values[r];
                    for (int c = 0; c <= r; ++c)
                    {
                        _values[k++] = row[c];
                    }
                }
            }
            
            
        }

        /// <summary>
        /// Create a sparse Skyline matrix from a sparse COO (coordinate list) matrix.
        /// </summary>
        /// <param name="matrix">The sparse COO matrix.</param>
        /// <param name="type">The type of Skyline matrix to be constructed.</param>
        public SKYSparseMatrix(COOSparseMatrix<T> matrix) : base(MatrixStorageType.SKYLINE)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            if (!matrix.IsSquare)
            {
                throw new InvalidOperationException("Matrix is not square.");
            }

            int dim = matrix.Rows;
            Rows = dim;
            Columns = dim;
            _zero = new T();

            if (matrix.IsUpper())
            {
                _type = SKYType.UPPER;

                _indices = new int[dim + 1];

                // Calculate the number of terms per column, store in _indices
                foreach (MatrixEntry<T> e in matrix.Values)
                {
                    if (!e.Equals(_zero))
                    {
                        int columnLength = Math.Max(0, e.RowIndex - e.ColumnIndex + 1);
                        int c = e.ColumnIndex + 1;
                        if (_indices[c] < columnLength)
                        {
                            _indices[c] = columnLength;
                        }
                    }
                }

                // Calculate the cumulative number of elements at the start of each column
                for (int c = 1; c <= dim; ++c)
                {
                    _indices[c] += _indices[c - 1];
                }

                // Allocate the values array
                _values = RectangularVector.Zeroes<T>(_indices[dim]);

                // Iterate through the nz elements again, and allocate
                foreach (MatrixEntry<T> e in matrix.Values)
                {
                    if (e.RowIndex <= e.ColumnIndex && !e.Equals(_zero))
                    {
                        _values[IndexUpper(e.RowIndex, e.ColumnIndex)] = e.Value;
                    }
                }
            }

            else
            {
                if (matrix.IsLower())
                {
                    _type = SKYType.LOWER;
                }
                else
                {
                    // No checks for actual numerical symmetry, just 
                    // reflect the lower triangular part upwards
                    _type = SKYType.SYMMETRIC; 
                }

                _indices = new int[dim + 1];

                // Store the number of elements in each row in _indices
                if (matrix.IsSorted)
                {
                    int rowIndex = -1;
                    foreach (MatrixEntry<T> e in matrix.Values)
                    {
                        // Is left-most element
                        if (e.RowIndex != rowIndex && !e.Equals(_zero))
                        {
                            _indices[e.RowIndex + 1] = e.RowIndex - e.ColumnIndex + 1;
                            rowIndex = e.RowIndex;
                        }
                    }
                }
                else
                {
                    foreach (MatrixEntry<T> e in matrix.Values)
                    {
                        if (!e.Equals(_zero))
                        {
                            int rowLength = e.RowIndex - e.ColumnIndex + 1;
                            int r = e.RowIndex + 1;

                            if (_indices[r] < rowLength)
                            {
                                _indices[r] = rowLength;
                            }
                        }
                    }
                }

                // Calculate the cumulative number of elements in each row
                for (int r = 1; r <= dim; ++r)
                {
                    _indices[r] += _indices[r - 1];
                }

                // Allocate the values array
                _values = RectangularVector.Zeroes<T>(_indices[dim]);

                // Iterate through the values for the second time
                foreach (MatrixEntry<T> e in matrix.Values)
                {
                    if (e.RowIndex >= e.ColumnIndex && !e.Equals(_zero))
                    {
                        _values[IndexLowerSymmetric(e.RowIndex, e.ColumnIndex)] = e.Value;
                    }
                }
            }
        }

        public SKYSparseMatrix(CSCSparseMatrix<T> matrix) : base(MatrixStorageType.SKYLINE)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            if (!matrix.IsSquare)
            {
                throw new InvalidOperationException("Matrix cannot be stored in Skyline format because it is not square.");
            }

            Rows = matrix.Rows;
            Columns = matrix.Columns;
            _zero = new T();

            int[] columnIndices = matrix.ColumnIndices,
                rowIndices = matrix.RowIndices;
            T[] values = matrix.Values;

            if (matrix.IsUpper())
            {
                _type = SKYType.UPPER;
                _indices = new int[Columns + 1];

                // Count the number of non-zeroes in each column
                for (int c = 0; c < Columns; ++c)
                {
                    _indices[c + 1] = c - rowIndices[columnIndices[c]] + 1;
                }

                // Accumulate
                for (int c = 0; c < Columns; ++c)
                {
                    _indices[c + 1] += _indices[c];
                }

                _values = RectangularVector.Zeroes<T>(_indices[Columns]);

                for (int c = 0; c < Columns; ++c)
                {
                    int start = columnIndices[c], 
                        end = columnIndices[c + 1], 
                        begin = _indices[c],
                        firstRow = rowIndices[start];
                    for (int i = start; i < end; ++i)
                    {
                        int r = rowIndices[i];
                        _values[begin + r - firstRow] = values[i];
                    }
                }
            }

            else
            {
                if (matrix.IsLower())
                {
                    _type = SKYType.LOWER;
                }
                else
                {
                    _type = SKYType.SYMMETRIC;
                }

                // Only iterate over the lower triangular half
                _indices = new int[Rows + 1];
                for (int c = 0; c < Columns; ++c)
                {
                    // Record the smallest column index for each row
                    // This requires iterating over all non-zero entries
                    int end = columnIndices[c + 1];
                    for (int i = columnIndices[c]; i < end; ++i)
                    {
                        int r = rowIndices[i];
                        if (r < c) continue;
                        if (_indices[r] > c)
                        {
                            _indices[r] = c;
                        }
                    }
                }

                // Convert into counts in each row
                for (int r = 0; r < Rows; ++r)
                {
                    _indices[r] = r - _indices[r] + 1;
                }
                // Shift and accumulate
                for (int r = Rows; r >= 1; --r)
                {
                    _indices[r] = _indices[r - 1];
                }
                _indices[0] = 0;
                for (int r = 0; r < Rows; ++r)
                {
                    _indices[r + 1] += _indices[r];
                }

                for (int c = 0; c < Columns; ++c)
                {
                    int end = columnIndices[c + 1];
                    for (int i = columnIndices[c]; i < end; ++i)
                    {
                        int r = rowIndices[i];
                        if (r < c)
                        {
                            continue;
                        }
                        _values[_indices[r + 1] - r + c - 1] = values[i]; 
                    }
                }
            }
        }

        /// <summary>
        /// Create a sparse Skyline matrix from a sparse CSR (compressed sparse row) matrix.
        /// </summary>
        /// <param name="matrix">The sparse matrix.</param>
        /// <param name="type">The type of Skyline matrix to create.</param>
        public SKYSparseMatrix(CSRSparseMatrix<T> matrix) : base(MatrixStorageType.SKYLINE)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            if (!matrix.IsSquare)
            {
                throw new ArgumentOutOfRangeException(nameof(matrix), ExceptionMessages.MatrixNotSquare);
            }

            int dim = matrix.Rows;
            Rows = dim;
            Columns = dim;
            _zero = new T();
            _indices = new int[dim + 1];

            int[] rowIndices = matrix.RowIndices,
                colIndices = matrix.ColumnIndices;
            T[] values = matrix.Values;

            if (matrix.IsUpper())
            {
                _type = SKYType.UPPER;

                // Set to > largest possible value
                for (int i = 0; i < dim; ++i)
                {
                    _indices[i] = dim;
                }

                // Store in indices[c] the min row index of non-zero elements in column c.
                for (int r = 0; r < dim; ++r)
                {
                    int row_end = rowIndices[r + 1];
                    for (int i = rowIndices[r]; i < row_end; ++i)
                    {
                        int c = colIndices[i];
                        if (c < r) continue;

                        if (r < _indices[c])
                        {
                            _indices[c] = r;
                        }
                    }
                }

                // Set indices[c + 1] = #nz in column c 
                for (int c = dim - 1; c >= 0; --c)
                {
                    _indices[c + 1] = Math.Max(_indices[c] - c, 0) + 1;
                }

                // Accumulate indices
                _indices[0] = 0;
                for (int c = 0; c < dim; ++c)
                {
                    _indices[c + 1] += _indices[c];
                }

                // Set all values
                for (int r = 0; r < dim; ++r)
                {
                    int row_end = _indices[r + 1];
                    for (int i = _indices[r]; i < row_end; ++i)
                    {
                        int c = colIndices[i];
                        if (c < r) continue;

                        _values[IndexUpper(r, c)] = _values[i];
                    }
                }
            }

            else
            {
                if (matrix.IsLower())
                {
                    _type = SKYType.LOWER;
                }
                else
                {
                    _type = SKYType.SYMMETRIC;
                }

                // Assume that all elements of the CSR matrix is non-zero
                for (int r = 0; r < dim; ++r)
                {
                    // Assumes ordering
                    int row_start = rowIndices[r];
                    if (row_start < colIndices.Length)
                    {
                        _indices[r + 1] = _indices[r] + Math.Max(0, r - colIndices[row_start]) + 1;
                    }
                    else
                    {
                        _indices[r + 1] = _indices[r];
                    }
                }
                _values = Zeroes(_indices[dim]);

                // Set all values under or on the main diagonal
                for (int r = 0; r < dim; ++r)
                {
                    int row_end = rowIndices[r + 1];
                    for (int i = rowIndices[r]; i < row_end; ++i)
                    {
                        int c = colIndices[i];
                        if (c > r) break;

                        _values[IndexLowerSymmetric(r, c)] = values[i];
                    }
                }
            }
        }

        /// <summary>
        /// Create a sparse Skyline matrix from a sparse DOK (dictionary of keys) matrix.
        /// </summary>
        /// <param name="matrix">The sparse DOK matrix.</param>
        /// <param name="type">The type of Skyline matrix to be constructed.</param>
        public SKYSparseMatrix(DOKSparseMatrix<T> matrix) : base(MatrixStorageType.SKYLINE)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            if (!matrix.IsSquare)
            {
                throw new InvalidOperationException("Matrix is not square.");
            }

            int dim = matrix.Rows;
            Rows = dim;
            Columns = dim;

            if (matrix.IsUpper())
            {
                _type = SKYType.UPPER;
                _indices = new int[dim + 1];

                // Temporarily store the minimum row index of non-zero elements of each column
                foreach (long key in matrix.Values.Keys)
                {
                    // use shifted column values 
                    int row = (int)(key / dim), column = (int)(key % dim) + 1;
                    if (_indices[column] > row)
                    {
                        _indices[column] = row;
                    }
                }

                // Convert into cumulative list
                for (int i = 1; i <= dim; ++i)
                {
                    // The number of entries in column (i - 1)
                    int count = Math.Max(0, i - _indices[i]);
                    _indices[i] = _indices[i - 1] + count;
                }

                // Allocate the value array
                _values = RectangularVector.Zeroes<T>(_indices[dim]);

                // Iterate through collection a second time
                foreach (KeyValuePair<long, T> e in matrix.Values)
                {
                    int row = (int)(e.Key / dim), column = (int)(e.Key % dim);
                    if (row <= column)
                    {
                        _values[IndexUpper(row, column)] = e.Value;
                    }
                }
            }
            else
            {
                if (matrix.IsLower())
                {
                    _type = SKYType.LOWER;
                }
                else
                {
                    _type = SKYType.SYMMETRIC;
                }

                _indices = new int[dim + 1];

                // Temporarily store the minimum column index of non-zero elements of each row
                foreach (long key in matrix.Values.Keys)
                {
                    // Use shifted row values
                    int row = (int)(key / dim) + 1, column = (int)(key % dim);
                    if (_indices[row] > column)
                    {
                        _indices[row] = column;
                    }
                }

                // convert the _indices array into actual indices
                for (int i = 1; i <= dim; ++i)
                {
                    // The number of elements in row @ index (i - 1)
                    int count = Math.Max(0, i - _indices[i]);
                    _indices[i] = _indices[i - 1] + count;
                }

                // Allocate the value array
                _values = new T[_indices[dim]];

                // Iterate though collection a second time
                foreach (KeyValuePair<long, T> e in matrix.Values)
                {
                    int row = (int)(e.Key / dim), column = (int)(e.Key % dim);
                    if (column <= row)
                    {
                        _values[IndexLowerSymmetric(row, column)] = e.Value;
                    }
                }
            }
        }

        public SKYSparseMatrix(Matrix<T> matrix) : base(MatrixStorageType.SKYLINE)
        {
            // Copied from COOSparseMatrix implementation
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            if (!matrix.IsSquare)
            {
                throw new InvalidOperationException("Matrix is not square.");
            }

            int dim = matrix.Rows;
            Rows = dim;
            Columns = dim;
            _zero = new T();

            bool isLower = true, isUpper = true;
            IEnumerable<MatrixEntry<T>> values = matrix.NonZeroEntries;
            foreach (MatrixEntry<T> e in values)
            {
                if (e.RowIndex > e.ColumnIndex) isUpper = false;
                else if (e.RowIndex < e.ColumnIndex) isLower = false;

                if (!isLower && !isUpper)
                {
                    break;
                }
            }

            if (isUpper)
            {
                _type = SKYType.UPPER;

                _indices = new int[dim + 1];

                // Calculate the number of terms per column, store in _indices
                foreach (MatrixEntry<T> e in values)
                {
                    if (!e.Equals(_zero))
                    {
                        int columnLength = Math.Max(0, e.RowIndex - e.ColumnIndex + 1);
                        int c = e.ColumnIndex + 1;
                        if (_indices[c] < columnLength)
                        {
                            _indices[c] = columnLength;
                        }
                    }
                }

                // Calculate the cumulative number of elements at the start of each column
                for (int c = 1; c <= dim; ++c)
                {
                    _indices[c] += _indices[c - 1];
                }

                // Allocate the values array
                _values = RectangularVector.Zeroes<T>(_indices[dim]);

                // Iterate through the nz elements again, and allocate
                foreach (MatrixEntry<T> e in values)
                {
                    if (e.RowIndex <= e.ColumnIndex && !e.Equals(_zero))
                    {
                        _values[IndexUpper(e.RowIndex, e.ColumnIndex)] = e.Value;
                    }
                }
            }

            else
            {
                if (isLower)
                {
                    _type = SKYType.LOWER;
                }
                else
                {
                    // No checks for actual numerical symmetry, just 
                    // reflect the lower triangular part upwards
                    _type = SKYType.SYMMETRIC;
                }

                _indices = new int[dim + 1];

                // Store the number of elements in each row in _indices
                foreach (MatrixEntry<T> e in values)
                {
                    if (!e.Equals(_zero))
                    {
                        int rowLength = e.RowIndex - e.ColumnIndex + 1;
                        int r = e.RowIndex + 1;

                        if (_indices[r] < rowLength)
                        {
                            _indices[r] = rowLength;
                        }
                    }
                }

                // Calculate the cumulative number of elements in each row
                for (int r = 1; r <= dim; ++r)
                {
                    _indices[r] += _indices[r - 1];
                }

                // Allocate the values array
                _values = RectangularVector.Zeroes<T>(_indices[dim]);

                // Iterate through the values for the second time
                foreach (MatrixEntry<T> e in values)
                {
                    if (e.RowIndex >= e.ColumnIndex && !e.Equals(_zero))
                    {
                        _values[IndexLowerSymmetric(e.RowIndex, e.ColumnIndex)] = e.Value;
                    }
                }
            }
        }

        /// <summary>
        /// Create a sparse Skyline matrix from a dense matrix. The dense matrix must be square. Note Skyline 
        /// sparse matrices can only hold either a triangular matrix or a symmetric matrix.
        /// 
        /// <p>
        /// If <txt>type</txt> is <txt>UPPER</txt>, the upper triangular half of the dense matrix will be used 
        /// to construct the sparse matrix. The elements below the main diagonal are ignored (assumed to be zero).
        /// </p>
        /// 
        /// <p>
        /// If <txt>type</txt> is <txt>LOWER</txt>, the lower triangular half of the dense matrix will be used 
        /// to construct the sparse matrix. The elements above the main diagonal are ignored (assumed to be zero).
        /// </p>
        /// 
        /// <p>
        /// If <txt>type</txt> is <txt>SYMMETRIC</txt>, the dense matrix must be symmetric. The entire matrix 
        /// will be copied.
        /// </p>
        /// </summary>
        /// <param name="matrix">The dense matrix.</param>
        /// <param name="type">The type of Skyline matrix to be constructed.</param>
        public SKYSparseMatrix(DenseMatrix<T> matrix, SKYType type) : base(MatrixStorageType.SKYLINE)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            if (!matrix.IsSquare)
            {
                throw new InvalidOperationException("The matrix is not square.");
            }

            int dim = matrix.Rows;
            Rows = dim;
            Columns = dim;
            _type = type;
            _zero = new T();

            if (type == SKYType.UPPER)
            {
                // Arranged by column if format is upper
                _indices = new int[dim + 1];

                List<T> values = new List<T>();

                T[][] dense = matrix.Values;
                for (int c = 0; c < dim; ++c)
                {
                    int r_start = 0;
                    while (r_start <= c && dense[r_start][c].Equals(_zero))
                    {
                        r_start++;
                    }

                    for (int r = r_start; r <= c; ++r)
                    {
                        values.Add(dense[r][c]);
                    }
                    _indices[c + 1] = values.Count;
                }

                _values = values.ToArray();
            }
            else if (type == SKYType.LOWER || type == SKYType.SYMMETRIC)
            {
                _indices = new int[dim + 1];

                // Store the start indices
                T[][] dense = matrix.Values;
                for (int r = 0; r < dim; ++r)
                {
                    T[] row = dense[r];
                    int c = 0;
                    while (c <= r)
                    {
                        if (!row[c].Equals(_zero))
                        {
                            break;
                        }
                        c++;
                    }
                    _indices[r + 1] = r - c + 1;
                }

                // calculate the accumuation
                for (int r = 1; r <= dim; ++r)
                {
                    _indices[r] += _indices[r - 1];
                }

                // initialize values array
                _values = RectangularVector.Zeroes<T>(_indices[dim]);

                for (int r = 0, k = 0; r < dim; ++r)
                {
                    T[] row = dense[r];

                    int c_start = Math.Max(0, r - (_indices[r + 1] - _indices[r]) + 1);
                    for (int c = c_start; c <= r; ++c)
                    {
                        _values[k++] = row[c];
                    }
                }
            }
            else
            {
                throw new NotSupportedException(nameof(type));
            }
        }

        /// <summary>
        /// Copy constructor.
        /// </summary>
        /// <param name="matrix">Another Skyline matrix.</param>
        public SKYSparseMatrix(SKYSparseMatrix<T> matrix) : base(MatrixStorageType.SKYLINE)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            Rows = matrix.Rows;
            Columns = matrix.Columns;
            _type = matrix._type;
            _zero = matrix._zero;
            _values = matrix._values.Copy();
            _indices = matrix._indices.Copy();
        }

        #endregion


        /// <summary>
        /// Returns the index given (rowIndex, columnIndex) for a lower or symmetric matrix
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private int IndexLowerSymmetric(int rowIndex, int columnIndex)
        {
            return columnIndex - rowIndex + _indices[rowIndex + 1] - 1;
        }
        /// <summary>
        /// Returns the index given (rowIndex, columnIndex) coordinates for a upper matrix
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private int IndexUpper(int rowIndex, int columnIndex)
        {
            return rowIndex - columnIndex + _indices[columnIndex + 1] - 1;
        }
        private static T[] Zeroes(int dimension)
        {
            if (dimension < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(dimension));
            }

            T[] row = new T[dimension];
            T zero = new T();
            if (!zero.Equals(default))
            {
                for (int i = 0; i < dimension; ++i)
                {
                    row[i] = zero;
                }
            }
            return row;
        }
        private static void Clear(T[] array, int start, int len)
        {
            T zero = new T();
            if (zero.Equals(default))
            {
                Array.Clear(array, start, len);
            }
            else
            {
                int end = start + len;
                for (int i = start; i < end; ++i)
                {
                    array[i] = zero;
                }
            }
        }


        #region Boolean methods

        public override bool Equals(object obj)
        {
            if (obj is null)
            {
                return false;
            }
            if (obj is SKYSparseMatrix<T>)
            {
                return Equals(obj as SKYSparseMatrix<T>);
            }
            if (obj is Matrix<T>)
            {
                return Equals(obj as Matrix<T>);
            }
            return false;
        }
        public bool Equals(SKYSparseMatrix<T> matrix)
        {
            return Equals(matrix, EqualityComparer<T>.Default);
        }
        public bool Equals(SKYSparseMatrix<T> matrix, IEqualityComparer<T> comparer)
        {
            if (comparer is null)
            {
                throw new ArgumentNullException(nameof(comparer));
            }
            return Equals(matrix, comparer.Equals);
        }
        public bool Equals(SKYSparseMatrix<T> matrix, Func<T, T, bool> equals)
        {
            if (matrix is null)
            {
                return false;
            }
            if (equals is null)
            {
                throw new ArgumentNullException(nameof(equals));
            }

            if (_type == matrix._type)
            {
                return EqualsLike(_indices, _values, matrix._indices, matrix._values, Rows, equals, _zero);
            }

            // If matrices are not the same type, then the only way they can be equal is if both 
            // matrices are diagonal, and their diagonal elements coincide
            bool isZero(T x) => equals(x, _zero);
            if (!matrix.IsDiagonal(isZero) || !IsDiagonal(isZero))
            {
                return false;
            }

            // Since both matrices are zero, we just check equality of their diagonal terms
            for (int i = 0; i < Rows; ++i)
            {
                int start1 = _indices[i], end1 = _indices[i + 1],
                    start2 = matrix._indices[i], end2 = matrix._indices[i + 1];

                T d1 = end1 > start1 ? _values[end1 - 1] : _zero;
                T d2 = end2 > start2 ? _values[end2 - 1] : _zero;
                if (!equals(d1, d2))
                {
                    return false;
                }
            }
            return true;
        }
        private static bool EqualsLike(int[] indices1, T[] values1, int[] indices2, T[] values2, int dim, Func<T, T, bool> equals, T zero)
        {
            for (int i = 0; i < dim; ++i)
            {
                int start1 = indices1[i], end1 = indices1[i + 1], len1 = end1 - start1,
                    start2 = indices2[i], end2 = indices2[i + 1], len2 = end2 - start2;

                // Compare the common section
                int len = Math.Min(len1, len2);
                for (int a = end1 - len, b = end2 - len; a < end1; ++a, ++b)
                {
                    if (!equals(values1[a], values2[b]))
                    {
                        return false;
                    }
                }

                // Compare the leftover parts against 0
                if (len1 > len)
                {
                    int end = start1 + (len1 - len);
                    for (int a = start1; a < end; ++a)
                    {
                        if (!equals(values1[a], zero))
                        {
                            return false;
                        }
                    }
                }
                else
                {
                    int end = start2 + (len2 - len);
                    for (int b = start2; b < end; ++b)
                    {
                        if (!equals(values2[b], zero))
                        {
                            return false;
                        }
                    }
                }
            }
            return true;
        }

        public override bool IsDiagonal(Predicate<T> isZero)
        {
            if (isZero is null)
            {
                throw new ArgumentNullException(nameof(isZero));
            }
            for (int i = 0; i < Rows; ++i)
            {
                int start = _indices[i], end = _indices[i + 1];

                // Ensure that all terms off the main diagonal @[end - 1] are zero
                for (int j = end - 2; j >= start; --j)
                {
                    if (!isZero(_values[j]))
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        public override bool IsZero(Predicate<T> isZero)
        {
            for (int i = 0; i < _values.Length; ++i)
            {
                if (!isZero(_values[i]))
                {
                    return false;
                }
            }
            return true;
        }

        public override bool IsLower(Predicate<T> isZero)
        {
            if (_type == SKYType.LOWER)
            {
                return true;
            }

            for (int r = 0; r < Rows; ++r)
            {
                int start = _indices[r], end = _indices[r + 1];
                for (int i = end - 2; i >= start; --i)
                {
                    if (!isZero(_values[i]))
                    {
                        return false;
                    }
                }
            }
            return true;
        }
        public override bool IsUpper(Predicate<T> isZero)
        {
            if (_type == SKYType.UPPER)
            {
                return true;
            }

            return IsDiagonal(isZero);
        }

        public override bool IsBanded(int lowerBandwidth, int upperBandwidth, Predicate<T> isZero)
        {
            if (isZero is null)
            {
                throw new ArgumentNullException(nameof(isZero));
            }

            if (_type == SKYType.LOWER || _type == SKYType.SYMMETRIC)
            {
                for (int r = 0; r < Rows; ++r)
                {
                    int end = _indices[r + 1], start = _indices[r], c = r - end + start + 1;
                    
                    while (c < r && r - c > lowerBandwidth)
                    {
                        if (!isZero(_values[start++]))
                        {
                            return false;
                        }
                        ++c;
                    }
                }
            }

            if (_type == SKYType.UPPER || _type == SKYType.SYMMETRIC)
            {
                for (int c = 0; c < Columns; ++c)
                {
                    int end = _indices[c + 1], start = _indices[c], r = c - end + start + 1;

                    while (r < c && c - r > upperBandwidth)
                    {
                        if (!isZero(_values[start++]))
                        {
                            return false;
                        }
                        ++r;
                    }
                }
            }

            return true;
        }

        public override bool IsIdentity(Predicate<T> isZero, Predicate<T> isOne)
        {
            // Redundant
            if (!IsSquare)
            {
                return false;
            }

            if (isZero is null)
            {
                throw new ArgumentNullException(nameof(isZero));
            }
            if (isOne is null)
            {
                throw new ArgumentNullException(nameof(isOne));
            }

            if (!IsDiagonal(isZero))
            {
                return false;
            }

            for (int r = 0; r < Rows; ++r)
            {
                int start = _indices[r], end = _indices[r + 1];
                if (start >= end || !isOne(_values[end - 1]))
                {
                    return false;
                }
            }
            return true;
        }

        #endregion


        #region Misc public methods

        public override int Count(Predicate<T> predicate)
        {
            if (predicate is null)
            {
                throw new ArgumentNullException(nameof(predicate));
            }

            int diagonalCount = 0, offDiagonalCount = 0, triangularTerms = 0;
            for (int r = 0; r < Rows; ++r)
            {
                int start = _indices[r], end = _indices[r + 1];
                if (end > start)
                {
                    if (predicate(_values[end - 1]))
                    {
                        ++diagonalCount;
                    }

                    for (int i = end - 2; i >= start; --i)
                    {
                        if (predicate(_values[i]))
                        {
                            ++offDiagonalCount;
                        }
                    }

                    triangularTerms += (end - start - 1);
                }
            }

            if (predicate(_zero))
            {
                offDiagonalCount += (Rows * (Rows - 1)) / 2 - triangularTerms;
            }

            if (_type == SKYType.LOWER || _type == SKYType.UPPER)
            {
                return diagonalCount + offDiagonalCount;
            }

            return diagonalCount + 2 * offDiagonalCount;
        }

        public override object Clone()
        {
            return Copy();
        }
        public SKYSparseMatrix<T> Copy()
        {
            return new SKYSparseMatrix<T>(_indices.Copy(), _values.Copy(), _type, true);
        }

        public override T[] LeadingDiagonal()
        {
            T[] diag = Zeroes(Rows);
            for (int i = 0; i < Rows; ++i)
            {
                int start = _indices[i], end = _indices[i + 1];
                if (end > start)
                {
                    diag[i] = _values[end - 1];
                }
            }
            return diag;
        }

        public override void Clear()
        {
            Array.Clear(_indices, 0, _indices.Length);
            _values = new T[0];
        }
        public override void Clear(Predicate<T> select)
        {
            Clear(select, true);
        }
        /// <summary>
        /// Removes all elements which satisfy a predicate from the matrix storage. 
        /// If element $A_{ij}$ is removed from a symmetric matrix, its conjugate entry, 
        /// $A_{ji}$, will also be removed.
        /// </summary>
        /// <param name="remove">
        /// A predicate that returns <txt>true</txt> if an element should be removed.
        /// </param>
        /// <param name="defrag">
        /// If <txt>true</txt>, the matrix storage will be shrunk after discarding the 
        /// removed entries. Makes the matrix more memory-efficient for future methods,
        /// but this method will take longer to complete. 
        /// </param>
        public void Clear(Predicate<T> remove, bool defrag)
        {
            if (remove is null)
            {
                throw new ArgumentNullException(nameof(remove));
            }

            if (defrag)
            {
                // Count the number of items to remove
                int count = 0;
                for (int i = 0; i < _values.Length; ++i)
                {
                    if (remove(_values[i]))
                    {
                        ++count;
                    }
                }

                if (count > 0)
                {
                    // Loop through in row (column) order to update indices as we go
                    // the _values array is borrowed to hold the new values, then trimmed at the end
                    int start, end = 0, k = 0;
                    for (int r = 0; r < Rows; ++r)
                    {
                        start = end;
                        end = _indices[r + 1];

                        // Find the start
                        int i = start;
                        while (i < _values.Length && (remove(_values[i]) || _values[i].Equals(_zero)))
                        {
                            ++i;
                        }

                        for (; i < end; ++i, ++k)
                        {
                            if (!remove(_values[i]))
                            {
                                _values[k] = _values[i];
                            }
                            else
                            {
                                _values[k] = _zero;
                            }
                        }
                        _indices[r + 1] = k;
                    }

                    // Resize the array to k
                    Array.Resize(ref _values, k);
                }
            }
            else
            {
                for (int i = 0; i < _values.Length; ++i)
                {
                    if (remove(_values[i]))
                    {
                        _values[i] = _zero;
                    }
                }
            }
        }
        public override void ClearRow(int rowIndex)
        {
            ClearRow(rowIndex, true);
        }
        public void ClearRow(int rowIndex, bool defrag)
        {
            if (rowIndex < 0 || rowIndex >= Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(rowIndex));
            }

            if (_type == SKYType.LOWER)
            {
                ClearRowLower(_indices, ref _values, rowIndex, Rows, defrag);
            }
            else if (_type == SKYType.UPPER)
            {
                ClearRowUpper(_indices, ref _values, rowIndex, Rows, _zero, defrag);
            }
            else
            {
                throw new NotSupportedException("Cannot clear a row from a symmetric matrix.");
            }
        }
        private static void ClearRowLower(int[] indices, ref T[] values, int rowIndex, int dim, bool defrag)
        {
            int start = indices[rowIndex], end = indices[rowIndex + 1];
            if (defrag)
            {
                int len = values.Length, interval = end - start;

                // Remove [start, end) from _values
                T[] newValues = new T[len - interval];
                Array.Copy(values, 0, newValues, 0, start);
                Array.Copy(values, end, newValues, start, len - end);
                values = newValues;

                // Update the indices
                for (int i = rowIndex + 1; i <= dim; ++i)
                {
                    indices[i] -= interval;
                }
            }
            else
            {
                Clear(values, start, end - start);
            }
        }
        private static void ClearRowUpper(int[] indices, ref T[] values, int rowIndex, int dim, T zero, bool defrag)
        {
            // Since values are stored by column, defragging is ony of benefit if 
            // the top element of some column is being removed.
            // We perform a 'useful'-ness check to see if defragging is of any benefit
            if (defrag)
            {
                bool change = false; // records whether there are any removals at all
                int top_removed = 0; // the number of cases where the topmost element of a column is removed

                for (int c = 0; c < dim; ++c)
                {
                    int start = indices[c], end = indices[c + 1], top = c - (end - start) + 1;
                    if (c >= rowIndex && rowIndex >= top)
                    {
                        change = true;

                        if (rowIndex == top)
                        {
                            top_removed++;
                        }
                    }
                }

                // No changes made to the matrix.
                if (!change)
                {
                    return;
                }

                // Check for usefulness of defragging
                if (top_removed > 0)
                {
                    T[] newValues = new T[values.Length - top_removed];
                    int k = 0, start, end = 0;
                    for (int c = 0; c < dim; ++c)
                    {
                        start = end;
                        end = indices[c + 1];

                        int top = c - (end - start) + 1;
                        if (top == rowIndex)
                        {
                            ++top; // skip the topmost element, if it is in the row to remove
                            ++start;
                        }

                        for (int r = top, i = start; r <= c; ++r, ++i)
                        {
                            if (r != rowIndex)
                            {
                                newValues[k++] = values[i];
                            }
                        }

                        // This can be editted since we use 'end' instead of looking up _indices[c + 1] in the next itr
                        indices[c + 1] = k;
                    }
                    values = newValues;

                    return;
                }

                // Not useful to defrag - drop onto the next case
            }

            for (int c = 0; c < dim; ++c)
            {
                int start = indices[c], end = indices[c + 1], row_start = c - (end - start) + 1;
                if (c >= rowIndex && rowIndex >= row_start)
                {
                    values[start + (rowIndex - row_start)] = zero;
                }
            }
        }
        public override void ClearColumn(int columnIndex)
        {
            ClearColumn(columnIndex, true);
        }
        public void ClearColumn(int columnIndex, bool defrag)
        {
            if (columnIndex < 0 || columnIndex >= Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(columnIndex));
            }

            if (_type == SKYType.LOWER)
            {
                ClearRowUpper(_indices, ref _values, columnIndex, Columns, _zero, defrag);
            }
            else if (_type == SKYType.UPPER)
            {
                ClearRowLower(_indices, ref _values, columnIndex, Columns, defrag);
            }
            else
            {
                throw new NotSupportedException("Cannot clear a column from a symmetric matrix.");
            }
        }
        public override void ClearSubmatrix(int rowIndex, int columnIndex, int rows, int columns)
        {
            if (rowIndex < 0 || rowIndex >= Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(rowIndex));
            }
            if (columnIndex < 0 || columnIndex >= Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(columnIndex));
            }

            int bottom = rowIndex + rows, right = columnIndex + columns;
            if (rows < 0 || bottom >= Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(rows));
            }
            if (columns < 0 || right >= Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(columns));
            }

            Clear(e => rowIndex <= e.RowIndex && e.RowIndex < bottom && columnIndex <= e.ColumnIndex && e.ColumnIndex < right, true);
        }
        public override void ClearLower()
        {
            // Only applies to lower and symmetric matrices
            if (_type == SKYType.UPPER)
            {
                return;
            }

            Clear(e => e.RowIndex > e.ColumnIndex, true);
        }
        public override void ClearUpper()
        {
            if (_type == SKYType.LOWER || _type == SKYType.SYMMETRIC)
            {
                return;
            }

            Clear(e => e.RowIndex < e.ColumnIndex, true);
        }
        public void Clear(Predicate<MatrixEntry<T>> remove, bool defrag)
        {
            if (remove is null)
            {
                throw new ArgumentNullException(nameof(remove));
            }

            if (defrag)
            {
                BitArray keep = new BitArray(_values.Length);

                int nnz = 0, start, end = 0;
                for (int r = 0; r < Rows; ++r)
                {
                    start = end;
                    end = _indices[r + 1];

                    int c = r - end + start + 1;
                    for (int i = start; i < end; ++i, ++c)
                    {
                        if (_type == SKYType.UPPER)
                        {
                            if (!remove(new MatrixEntry<T>(c, r, _values[i])))
                            {
                                keep[i] = true;
                                ++nnz;
                            }
                        }
                        else
                        {
                            if (!remove(new MatrixEntry<T>(r, c, _values[i])))
                            {
                                keep[i] = true;
                                ++nnz;
                            }
                        }
                    }
                }

                T[] values = new T[nnz];
                int k = 0;
                end = 0; // reset end
                for (int r = 0; r < Rows; ++r)
                {
                    start = end;
                    end = _indices[r + 1];

                    int c = r - end + start + 1;
                    for (int i = start; i < end; ++i, ++c)
                    {
                        if (keep[i])
                        {
                            values[k++] = _values[i];
                        }
                    }
                    _indices[r + 1] = k;
                }
                _values = values;
            }
            else
            {
                for (int r = 0; r < Rows; ++r)
                {
                    int start = _indices[r], end = _indices[r + 1], c = r - end + start + 1;
                    for (int i = start; i < end; ++i, ++c)
                    {
                        if (_type == SKYType.UPPER)
                        {
                            if (remove(new MatrixEntry<T>(c, r, _values[i])))
                            {
                                _values[i] = _zero;
                            }
                        }

                        // LOWER and SYM
                        else
                        {
                            if (remove(new MatrixEntry<T>(r, c, _values[i])))
                            {
                                _values[i] = _zero;
                            }
                        }
                    }
                }
            }
        }

        private static SparseVector<T> GetRowLower(int[] _indices, T[] _values, int rowIndex, int dim)
        {
            int row_start = _indices[rowIndex],
                    row_end = _indices[rowIndex + 1],
                    len = row_end - row_start;
            int c = rowIndex - len + 1;

            T[] values = new T[len];
            int[] indices = new int[len];
            for (int k = row_start; k < row_end; ++k)
            {
                int j = k - row_start;
                values[j] = _values[k];
                indices[j] = c++;
            }
            return new SparseVector<T>(dim, indices, values, true);
        }
        private static SparseVector<T> GetRowUpper(int[] _indices, T[] _values, int rowIndex, int dim)
        {
            List<int> indices = new List<int>();
            List<T> values = new List<T>();
            for (int c = rowIndex; c < dim; ++c)
            {
                int col_start = _indices[c],
                    col_end = _indices[c + 1],
                    len = col_end - col_start;

                if (c - len + 1 <= rowIndex)
                {
                    indices.Add(c);
                    values.Add(_values[col_end - (c - rowIndex) - 1]);
                }
            }
            return new SparseVector<T>(dim, indices.ToArray(), values.ToArray());
        }
        private static SparseVector<T> GetRowSym(int[] _indices, T[] _values, int rowIndex, int dim)
        {
            // Count the number of nz elements in the row
            int nz = _indices[rowIndex + 1] - _indices[rowIndex];
            for (int r = 0; r < dim; ++r)
            {
                int start = _indices[r], end = _indices[r + 1], row_start = r - (end - start) + 1;
                if (row_start <= rowIndex && rowIndex < r)
                {
                    ++nz;
                }
            }

            int[] indices = new int[nz];
            T[] values = new T[nz];

            // Lower triangular values populated first
            int s = _indices[rowIndex], e = _indices[rowIndex + 1], k = 0;
            for (int c = rowIndex - (e - s) + 1, i = s; c <= rowIndex; ++c, ++i, ++k)
            {
                indices[k] = c;
                values[k] = _values[i];
            }

            // Upper triangular values 
            for (int r = 0; r < dim; ++r)
            {
                int start = _indices[r], end = _indices[r + 1], row_start = r - (end - start) + 1;
                if (row_start <= rowIndex && rowIndex < r)
                {
                    indices[k] = r;
                    values[k] = _values[rowIndex - row_start + start];
                    ++k;
                }
            }
            return new SparseVector<T>(dim, indices, values, true);
        }

        public override SparseVector<T> SparseRow(int rowIndex)
        {
            if (rowIndex < 0 || rowIndex >= Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(rowIndex));
            }

            if (_type == SKYType.LOWER)
            {
                return GetRowLower(_indices, _values, rowIndex, Columns);
            }
            else if (_type == SKYType.UPPER)
            {
                return GetRowUpper(_indices, _values, rowIndex, Columns);
            }
            else if (_type == SKYType.SYMMETRIC)
            {
                return GetRowSym(_indices, _values, rowIndex, Columns);
            }
            throw new NotImplementedException();
        }

        public override SparseVector<T> SparseColumn(int columnIndex)
        {
            if (columnIndex < 0 || columnIndex >= Columns)
            {
                throw new ArgumentNullException(nameof(columnIndex));
            }

            if (_type == SKYType.LOWER)
            {
                return GetRowUpper(_indices, _values, columnIndex, Rows);
            }
            if (_type == SKYType.UPPER)
            {
                return GetRowLower(_indices, _values, columnIndex, Rows);
            }
            if (_type == SKYType.SYMMETRIC)
            {
                return GetRowSym(_indices, _values, columnIndex, Rows);
            }

            throw new NotSupportedException(nameof(Type));
        }

        public override DenseVector<T> Row(int rowIndex)
        {
            if (rowIndex < 0 || rowIndex >= Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(rowIndex));
            }

            T[] row = RectangularVector.Zeroes<T>(Columns);
            if (_type == SKYType.LOWER || _type == SKYType.SYMMETRIC)
            {
                int row_start = _indices[rowIndex], row_end = _indices[rowIndex + 1],
                    i_start = rowIndex - row_end + row_start + 1;
                for (int k = row_start, i = i_start; k < row_end; ++k, ++i)
                {
                    row[i] = _values[k];
                }
            }
            if (_type == SKYType.UPPER || _type == SKYType.SYMMETRIC)
            {
                for (int c = rowIndex; c < Columns; ++c)
                {
                    int col_start = _indices[c],
                        col_end = _indices[c + 1];
                    if (c - col_end + col_start + 1 <= rowIndex)
                    {
                        row[c] = _values[col_end - c + rowIndex - 1];
                    }
                }
            }
            return new DenseVector<T>(row);
        }

        public override DenseVector<T> Column(int columnIndex)
        {
            if (columnIndex < 0 || columnIndex >= Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(columnIndex));
            }

            T[] column = RectangularVector.Zeroes<T>(Rows);
            if (_type == SKYType.LOWER || _type == SKYType.SYMMETRIC)
            {
                for (int r = columnIndex; r < Rows; ++r)
                {
                    int row_start = _indices[r],
                        row_end = _indices[r + 1];
                    if (r - row_end + row_start + 1 <= columnIndex)
                    {
                        column[r] = _values[row_end - r + columnIndex - 1];
                    }
                }
            }
            if (_type == SKYType.UPPER || _type == SKYType.SYMMETRIC)
            {
                int col_start = _indices[columnIndex], col_end = _indices[columnIndex + 1],
                   i_start = columnIndex - col_end + col_start + 1;
                for (int k = col_start, i = i_start; k < col_end; ++k, ++i)
                {
                    column[i] = _values[k];
                }
            }
            return new DenseVector<T>(column);
        }

        public override Matrix<T> Select(int[] rowIndices, int[] columnIndices)
        {
            if (rowIndices is null)
            {
                throw new ArgumentNullException(nameof(rowIndices));
            }
            if (columnIndices is null)
            {
                throw new ArgumentNullException(nameof(columnIndices));
            }
            ThrowHelper.AssertCollectionInRange(rowIndices, 0, Rows, nameof(rowIndices));
            ThrowHelper.AssertCollectionInRange(columnIndices, 0, Columns, nameof(columnIndices));

            // Treat as lower 
            int[] rowSelection = _type == SKYType.LOWER || _type == SKYType.SYMMETRIC ? rowIndices : columnIndices;
            int[] colSelection = _type == SKYType.UPPER ? columnIndices : rowIndices;
            int rlen = rowSelection.Length,
                clen = colSelection.Length;

            // Since resulting matrix is not necessarily skyline, we use a COOSparseMatrix
            List<MatrixEntry<T>> values = new List<MatrixEntry<T>>();
            for (int ri = 0; ri < rlen; ++ri)
            {
                int r = rowSelection[ri],
                    start = rowIndices[r],
                    end = rowIndices[r + 1],
                    begin = r - end + start + 1;

                for (int ci = 0; ci < clen; ++ci)
                {
                    int c = colSelection[ci];
                    if (begin <= c && c <= r)
                    {
                        T value = _values[c - begin + start];
                        if (_type == SKYType.UPPER)
                        {
                            values.Add(new MatrixEntry<T>(ci, ri, value));
                        }
                        else if (_type == SKYType.LOWER)
                        {
                            values.Add(new MatrixEntry<T>(ri, ci, value));
                        }
                        else
                        {
                            values.Add(new MatrixEntry<T>(ri, ci, value));
                            values.Add(new MatrixEntry<T>(ci, ri, value));
                        }
                    }
                }
            }

            // Only sorted if lower
            return new COOSparseMatrix<T>(rlen, clen, values, _type == SKYType.LOWER);
        }

        private static Vector<T> SelectVector(int rowIndex, int[] columnIndices, int[] _indices, T[] _values, T _zero, SKYType _type)
        {
            int clen = columnIndices.Length;
            if (_type == SKYType.LOWER)
            {
                int nz = 0,
                    start = _indices[rowIndex],
                    end = _indices[rowIndex + 1],
                    begin = rowIndex - end + start + 1;

                for (int ci = 0; ci < clen; ++ci)
                {
                    int c = columnIndices[ci];
                    if (begin <= c && c <= rowIndex && !_values[c - begin + start].Equals(_zero)) ++nz;
                }

                int[] indices = new int[nz];
                T[] values = new T[nz];

                int k = 0;
                for (int ci = 0; ci < clen; ++ci)
                {
                    int c = columnIndices[ci];
                    if (begin <= c && c <= rowIndex)
                    {
                        T value = _values[c - begin + start];
                        if (!value.Equals(_zero))
                        {
                            indices[k] = ci;
                            values[k++] = value;
                        }
                    }
                }
                return new SparseVector<T>(clen, indices, values, true);
            }

            if (_type == SKYType.UPPER)
            {
                int nz = 0;
                for (int ci = 0; ci < clen; ++ci)
                {
                    int c = columnIndices[ci],
                        start = _indices[c],
                        end = _indices[c + 1],
                        begin = c - end + start + 1;

                    if (begin <= rowIndex && rowIndex <= c && !_values[rowIndex - begin + start].Equals(_zero))
                    {
                        ++nz;
                    }
                }

                int[] indices = new int[nz];
                T[] values = new T[nz];

                for (int ci = 0, k = 0; ci < clen; ++ci)
                {
                    int c = columnIndices[ci],
                        start = _indices[c],
                        end = _indices[c + 1],
                        begin = c - end + start + 1;

                    if (begin <= rowIndex && rowIndex <= c)
                    {
                        T value = _values[rowIndex - begin + start];
                        if (!value.Equals(_zero))
                        {
                            indices[k] = ci;
                            values[k++] = value;
                        }
                    }
                }
                return new SparseVector<T>(clen, indices, values, true);
            }

            if (_type == SKYType.SYMMETRIC)
            {
                int row_start = _indices[rowIndex],
                    row_end = _indices[rowIndex + 1],
                    row_begin = rowIndex - row_end + row_start + 1,
                    nz = 0;

                for (int ci = 0; ci < clen; ++ci)
                {
                    int c = columnIndices[ci];
                    if (c <= rowIndex)
                    {
                        // Lower
                        if (row_begin <= c && !_values[c - row_begin + row_start].Equals(_zero))
                        {
                            ++nz;
                        }
                    }
                    else
                    {
                        // Upper
                        int col_start = _indices[c],
                            col_end = _indices[c + 1],
                            col_begin = c - col_end + col_start + 1;
                        if (col_begin <= rowIndex && !_values[rowIndex - col_begin + col_start].Equals(_zero))
                        {
                            ++nz;
                        }
                    }
                }

                int[] indices = new int[nz];
                T[] values = new T[nz];
                for (int ci = 0, k = 0; ci < clen; ++ci)
                {
                    int c = columnIndices[ci];
                    if (c <= rowIndex)
                    {
                        // Lower
                        if (row_begin <= c)
                        {
                            T value = _values[c - row_begin + row_start];
                            if (!value.Equals(_zero))
                            {
                                indices[k] = ci;
                                values[k++] = value;
                            }
                        }
                    }
                    else
                    {
                        // Upper
                        int col_start = _indices[c],
                            col_end = _indices[c + 1],
                            col_begin = c - col_end + col_start + 1;
                        if (col_begin <= rowIndex)
                        {
                            T value = _values[rowIndex - col_begin + col_start];
                            if (!value.Equals(_zero))
                            {
                                indices[k] = ci;
                                values[k++] = value;
                            }
                        }
                    }
                }

                return new SparseVector<T>(clen, indices, values, true);
            }

            throw new NotSupportedException();
        }
        public override Vector<T> SelectAsVector(int rowIndex, int[] columnIndices)
        {
            if (rowIndex < 0 || rowIndex >= Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(rowIndex));
            }
            if (columnIndices is null)
            {
                throw new ArgumentNullException(nameof(columnIndices));
            }
            ThrowHelper.AssertCollectionInRange(columnIndices, 0, Columns, nameof(columnIndices));
            return SelectVector(rowIndex, columnIndices, _indices, _values, _zero, _type);
        }
        public override Vector<T> SelectAsVector(int[] rowIndices, int columnIndex)
        {
            if (rowIndices is null)
            {
                throw new ArgumentNullException(nameof(rowIndices));
            }
            ThrowHelper.AssertCollectionInRange(rowIndices, 0, Rows, nameof(rowIndices));
            if (columnIndex < 0 || columnIndex >= Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(columnIndex));
            }

            SKYType type = SKYType.SYMMETRIC;
            if (_type == SKYType.LOWER) type = SKYType.UPPER;
            else if (_type == SKYType.UPPER) type = SKYType.LOWER;

            // Apply to the transpose
            return SelectVector(columnIndex, rowIndices, _indices, _values, _zero, type);
        }

        /// <summary>
        /// Increment sums[r] by the sum of sum[f(A[r, c]), c], if the matrix is lower triangular
        /// </summary>
        /// <param name="includeDiagonalEntry">
        /// If false, the diagonal entry of each row is excluded
        /// (this is required to ensure we're not double-counting the diagonal entries in symmetric case).
        /// </param>
        private void IncrementByRowSumLower(T[] sums, Func<T, T> f, IProvider<T> provider, bool includeDiagonalEntry)
        {
            T fz = f(_zero);
            bool f_zero_is_zero = provider.Equals(fz, _zero);

            for (int r = 0; r < Rows; ++r)
            {
                int start = _indices[r], end = _indices[r + 1];

                // Add all the zeroes first
                T sum;
                if (f_zero_is_zero)
                {
                    sum = provider.Zero;
                }
                else
                {
                    sum = provider.MultiplyInteger(fz, Columns - (end - start));
                }

                // Stop 1 early if diagonal entry is not to be included
                if (!includeDiagonalEntry)
                {
                    --end;
                }

                for (int i = start; i < end; ++i)
                {
                    sum = provider.Add(sum, f(_values[i]));
                }
                sums[r] = provider.Add(sums[r], sum);
            }
        }
        private void IncrementByRowSumUpper(T[] sums, Func<T, T> f, IProvider<T> provider)
        {
            T fz = f(_zero);
            if (!provider.Equals(fz, _zero))
            {
                // Add the zeroes in the lower triangular portion of the matrix
                for (int r = 1; r < Rows; ++r)
                {
                    sums[r] = provider.Add(sums[r], provider.MultiplyInteger(fz, r));
                }

                // Add the zeroes in the upper triangular portion of the matrix
                for (int c = 0; c < Columns; ++c)
                {
                    int start = _indices[c], end = _indices[c + 1], row_start = c - (end - start) + 1;
                    for (int r = 0; r < row_start; ++r)
                    {
                        sums[r] = provider.Add(sums[r], fz);
                    }
                }
            }

            for (int c = 0; c < Columns; ++c)
            {
                int start = _indices[c], end = _indices[c + 1], r = c - (end - start) + 1;
                for (int i = start; r <= c; ++r, ++i)
                {
                    sums[r] = provider.Add(sums[r], _values[i]);
                }
            }
        }
        /// <summary>
        /// Returns the sum of all entries in each row, as a dense vector. Optionally, a function can 
        /// be specified which will be applied to each entry prior to summing them together. If not 
        /// specified, the entries will be simply added together. 
        /// </summary>
        /// <param name="provider">The provider used for elementwise summation.</param>
        /// <param name="function">The function to be applied to each entry prior to summing them.</param>
        /// <returns>The row sums of this matrix.</returns>
        public DenseVector<T> RowSums(IProvider<T> provider, Func<T, T> function = null)
        {
            if (function is null)
            {
                function = x => x;
            }

            if (_type == SKYType.LOWER)
            {
                T[] sums = Zeroes(Rows);
                IncrementByRowSumLower(sums, function, provider, true);
                return new DenseVector<T>(sums);
            }
            if (_type == SKYType.UPPER)
            {
                T[] sums = Zeroes(Rows);
                IncrementByRowSumUpper(sums, function, provider);
                return new DenseVector<T>(sums);
            }
            if (_type == SKYType.SYMMETRIC)
            {
                T[] sums = Zeroes(Rows);
                IncrementByRowSumLower(sums, function, provider, false);
                IncrementByRowSumUpper(sums, function, provider);
                return new DenseVector<T>(sums);
            }

            throw new NotSupportedException(nameof(Type));
        }
        /// <summary>
        /// Returns the sum of all entries in each column, as a dense vector. Optionally, a function can
        /// be specified which will be applied to each entry prior to summing them together. If not 
        /// specified, the entries will be simply added together. 
        /// </summary>
        /// <param name="provider">The provider used for elementwise summation.</param>
        /// <param name="function">The function to be applied to each entry prior to summing them.</param>
        /// <returns>The column sums of this matrix.</returns>
        public DenseVector<T> ColumnSums(IProvider<T> provider, Func<T, T> function = null)
        {
            if (function is null)
            {
                function = x => x;
            }

            T[] sums = Zeroes(Columns);
            if (_type == SKYType.LOWER)
            {
                IncrementByRowSumUpper(sums, function, provider);
            }
            else if (_type == SKYType.UPPER)
            {
                IncrementByRowSumLower(sums, function, provider, true);
            }
            else if (_type == SKYType.SYMMETRIC)
            {
                IncrementByRowSumUpper(sums, function, provider);
                IncrementByRowSumLower(sums, function, provider, false);
            }
            else
            {
                throw new NotSupportedException(nameof(Type));
            }
            return new DenseVector<T>(sums);
        }
        public override DenseVector<T> RowSums(IProvider<T> provider)
        {
            return RowSums(provider, x => x);
        }
        public override DenseVector<T> ColumnSums(IProvider<T> provider)
        {
            return ColumnSums(provider, x => x);
        }

        /// <summary>
        /// Returns the sums of the absolute value of all entries in each row.
        /// </summary>
        /// <param name="provider">The provider used for elementwise addition.</param>
        /// <returns>The absolute row sums, as a dense vector.</returns>
        public DenseVector<T> RowAbsSums(IProvider<T> provider)
        {
            return RowSums(provider, x => provider.IsRealAndNegative(x) ? provider.Negate(x) : x);
        }
        /// <summary>
        /// Returns the sums of the absolute value of all entries in each column.
        /// </summary>
        /// <param name="provider">The provider used for elementwise addition.</param>
        /// <returns>The absolute column sums, as a dense vector.</returns>
        public DenseVector<T> ColumnsAbsSums(IProvider<T> provider)
        {
            return ColumnSums(provider, x => provider.IsRealAndNegative(x) ? provider.Negate(x) : x);
        }

        private void RowCountsLower(int[] counts, int dim, Predicate<T> isZero)
        {
            for (int r = 0; r < dim; ++r)
            {
                int start = _indices[r], end = _indices[r + 1];

                int count = 0;
                for (int i = start; i < end; ++i)
                {
                    if (!isZero(_values[i]))
                    {
                        ++count;
                    }
                }
                counts[r] += count;
            }
        }
        private void RowCountsLower(int[] counts, int dim)
        {
            for (int r = 0; r < dim; ++r)
            {
                counts[r] += _indices[r + 1] - _indices[r];
            }
        }
        private void RowCountsUpper(int[] counts, int dim, Predicate<T> isZero, bool includeDiagonal)
        {
            for (int c = 0; c < dim; ++c)
            {
                int start = _indices[c], end = _indices[c + 1];

                int r_last = includeDiagonal ? c : c - 1;
                for (int r = c - (end - start) + 1, i = start; r <= r_last; ++r, ++i)
                {
                    if (!isZero(_values[i]))
                    {
                        counts[r]++;
                    }
                }
            }
        }
        private void RowCountsUpper(int[] counts, int dim, bool includeDiagonal)
        {
            for (int c = 0; c < dim; ++c)
            {
                int start = _indices[c], end = _indices[c + 1], r_last = includeDiagonal ? c : c - 1;
                for (int r = c - (end - start) + 1, i = start; r <= r_last; ++r, ++i)
                {
                    counts[r]++;
                }
            }
        }
        /// <summary>
        /// Returns the number of non-zero entries of each row, using the specified predicate to 
        /// decide whether an entry should be treated as zero.
        /// </summary>
        /// <param name="isZero"></param>
        /// <returns></returns>
        public DenseVector<int> RowCounts(Predicate<T> isZero)
        {
            int[] counts = new int[Rows];
            if (_type == SKYType.LOWER)
            {
                RowCountsLower(counts, Rows, isZero);
            }
            else if (_type == SKYType.UPPER)
            {
                RowCountsUpper(counts, Columns, isZero, true);
            }
            else if (_type == SKYType.SYMMETRIC)
            {
                RowCountsLower(counts, Rows, isZero);
                RowCountsUpper(counts, Columns, isZero, false);
            }
            else
            {
                throw new NotSupportedException(nameof(Type));
            }
            return new DenseVector<int>(counts);
        }
        public DenseVector<int> ColumnCounts(Predicate<T> isZero)
        {
            int[] counts = new int[Columns];
            if (_type == SKYType.UPPER)
            {
                RowCountsUpper(counts, Columns, isZero, true);
            }
            else if (_type == SKYType.LOWER)
            {
                RowCountsLower(counts, Rows, isZero);
            }
            else if (_type == SKYType.SYMMETRIC)
            {
                RowCountsUpper(counts, Columns, isZero, false);
                RowCountsLower(counts, Rows, isZero);
            }
            else
            {
                throw new NotSupportedException(nameof(Type));
            }
            return new DenseVector<int>(counts);
        }

        /// <summary>
        /// Returns the symbolic row count of this matrix. Note that this may differ from the numerical 
        /// row count of the matrix, since values can be stored that are numerically zero but symbolically 
        /// non-zero.
        /// </summary>
        /// <returns></returns>
        public DenseVector<int> RowCounts()
        {
            int[] counts = new int[Rows];
            if (_type == SKYType.LOWER)
            {
                RowCountsLower(counts, Rows);
            }
            else if (_type == SKYType.UPPER)
            {
                RowCountsUpper(counts, Columns, true);
            }
            else if (_type == SKYType.SYMMETRIC)
            {
                RowCountsLower(counts, Rows);
                RowCountsUpper(counts, Columns, false);
            }
            else
            {
                throw new NotSupportedException(nameof(Type));
            }
            return new DenseVector<int>(counts);
        }
        public DenseVector<int> ColumnCounts()
        {
            int[] counts = new int[Columns];
            if (_type == SKYType.UPPER)
            {
                RowCountsUpper(counts, Columns, true);
            }
            else if (_type == SKYType.LOWER)
            {
                RowCountsLower(counts, Rows);
            }
            else if (_type == SKYType.SYMMETRIC)
            {
                RowCountsUpper(counts, Columns, false);
                RowCountsLower(counts, Rows);
            }
            else
            {
                throw new NotSupportedException(nameof(Type));
            }
            return new DenseVector<int>(counts);
        }

        public SKYSparseMatrix<T> DirectSum(params SKYSparseMatrix<T>[] matrices)
        {
            if (matrices is null)
            {
                throw new ArgumentNullException(nameof(matrices));
            }

            // All matrices must be the same type in order to be represented
            int count = matrices.Length, rows = Rows, nz = _values.Length;
            for (int i = 0; i < count; ++i)
            {
                SKYSparseMatrix<T> matrix = matrices[i];
                if (matrix is null)
                {
                    throw new ArgumentNullException(nameof(matrices));
                }
                if (matrix._type != _type)
                {
                    throw new InvalidOperationException("Direct sum is not lower triangular, upper triangular or symmetric, and cannot be stored as a Skyline matrix.");
                }

                rows += matrix.Rows;
                nz += matrix.Values.Length;
            }

            // Initialize sum arrays 
            int[] indices = new int[rows + 1];
            T[] values = new T[nz];

            // Insert the first matrix
            Array.Copy(_indices, 0, indices, 0, _indices.Length);
            Array.Copy(_values, 0, values, 0, _values.Length);

            // Insert all other matrices, in order
            int row_offset = Rows, nz_offset = _values.Length;
            for (int i = 1; i <= count; ++i)
            {
                SKYSparseMatrix<T> matrix = matrices[i - 1];
                int dim = matrix.Rows;
                for (int r = 0; r < dim; ++r)
                {
                    indices[row_offset + r] = matrix.Indices[r] + nz_offset;
                }
                row_offset += dim;

                Array.Copy(matrix._values, 0, values, nz_offset, matrix._values.Length);
                nz_offset += matrix._values.Length;
            }
            indices[rows] = nz_offset;

            return new SKYSparseMatrix<T>(indices, values, _type, true);
        }

        /// <summary>
        /// Apply a function $f:$ <txt>T</txt> $\to$ <txt>F</txt> to each non-zero entry of 
        /// this matrix. The zeroes are unchanged.
        /// </summary>
        /// <typeparam name="F"></typeparam>
        /// <param name="function">The function to apply to each non-zero entry of the matrix.</param>
        /// <returns>The resulting matrix.</returns>
        public SKYSparseMatrix<F> Elementwise<F>(Func<T, F> function) where F : new()
        {
            // Check if f maps zero (in T) to zero (in F)
            if (function(_zero).Equals(new F()))
            {
                F[] result = _values.Elementwise(function);
                return new SKYSparseMatrix<F>(_indices.Copy(), result, _type, true);
            }

            // If f does not map 0 to 0, then there could be catastrophic fill-in - 
            // requires reshaping of the matrix, storage required is O(n^2)
            // Also, the matrix cannot be represented unless it is symmetric
            if (_type != SKYType.SYMMETRIC)
            {
                throw new InvalidOperationException("The resultant matrix cannot be represented as a triangular or symmetric matrix.");
            }

            int dim = Rows, len = (dim * (dim + 1)) / 2;
            F[] values = new F[len];
            F fz = function(_zero);
            if (!fz.Equals(default))
            {
                for (int i = 0; i < len; ++i)
                {
                    values[i] = fz;
                }
            }

            int[] indices = new int[dim + 1];
            int k = 0;
            for (int i = 0; i < dim; ++i)
            {
                k += (i + 1);
                int start = _indices[i], end = _indices[i + 1], j = k - (end - start);
                for (int v = start; j < k; ++j, ++v)
                {
                    values[j] = function(_values[v]);
                }
                indices[i + 1] = k;
            }

            return new SKYSparseMatrix<F>(indices, values, _type, true);
        }

        /// <summary>
        /// Returns a new Skyline matrix whose entries are outputs of a two-argument function applied pointwise
        /// to entries of this matrix and another matrix.
        /// </summary>
        /// <typeparam name="T2"></typeparam>
        /// <typeparam name="TOut"></typeparam>
        /// <param name="matrix"></param>
        /// <param name="function"></param>
        /// <returns></returns>
        public SKYSparseMatrix<TOut> Pointwise<T2, TOut>(SKYSparseMatrix<T2> matrix, Func<T, T2, TOut> function) where T2 : new() where TOut : new()
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            if (function is null)
            {
                throw new ArgumentNullException(nameof(function));
            }
            if (Rows != matrix.Rows || Columns != matrix.Columns)
            {
                throw new InvalidOperationException(ExceptionMessages.MatrixSize);
            }
            if (_type != matrix._type)
            {
                throw new InvalidOperationException("Matrix types do not match - cannot evaluate because resultant matrix might be neither triangular nor symmetric.");
            }

            // Simple case - where f(0, 0) = 0
            T2 zero2 = new T2(); 
            TOut zeroOut = new TOut();
            TOut fzero = function(_zero, zero2);
            if (fzero.Equals(zeroOut))
            {
                // Compute the indices of the resultant matrix
                int[] indices = new int[Rows + 1];
                for (int i = 0; i < Rows; ++i)
                {
                    int i1 = i + 1;
                    indices[i1] = indices[i] + Math.Max(_indices[i1] - _indices[i], matrix._indices[i1] - matrix._indices[i]);
                }

                TOut[] values = new TOut[indices[Rows]];
                int k = 0;
                for (int i = 0; i < Rows; ++i)
                {
                    int start1 = _indices[i], end1 = _indices[i + 1],
                        start2 = matrix._indices[i], end2 = matrix._indices[i + 1],
                        len = Math.Min(end1 - start1, end2 - start2),
                        begin1 = end1 - len,
                        begin2 = end2 - len;

                    // f(x, 0)
                    if (begin1 > start1)
                    {
                        for (int j = start1; j < begin1; )
                        {
                            values[k++] = function(_values[j++], zero2);
                        }
                    }

                    // f(0, y)
                    else if (begin2 > start2)
                    {
                        for (int j = start2; j < begin2; )
                        {
                            values[k++] = function(_zero, matrix._values[j++]);
                        }
                    }

                    // overlap: f(x, y)
                    for (int j = begin1, j2 = begin2; j < end1; )
                    {
                        values[k++] = function(_values[j++], matrix._values[j2++]);
                    }

                    indices[i + 1] = k;
                }

                return new SKYSparseMatrix<TOut>(indices, values, _type, true);
            }
            else
            {
                // Catastrophic fill in - not recommended for performance reasons. Only implemented here 
                // for completeness

                if (_type != SKYType.SYMMETRIC)
                {
                    throw new InvalidOperationException("The resultant matrix cannot be represented as a triangular or symmetric matrix.");
                }

                int dim = Rows, k = 0;
                int[] indices = new int[dim + 1];
                TOut[] values = new TOut[(dim * (dim + 1)) / 2];
                for (int r = 0; r < dim; ++r)
                {
                    int start1 = _indices[r], end1 = _indices[r + 1],
                        start2 = matrix._indices[r], end2 = matrix._indices[r + 1],
                        len = Math.Min(end1 - start1, end2 - start2),
                        begin1 = end1 - len,
                        begin2 = end2 - len;

                    // f(x, 0)
                    if (begin1 > start1)
                    {
                        int end = r - (end1 - start1) + 1;
                        for (int j = 0; j < end; ++j)
                        {
                            values[k++] = fzero;
                        }
                        for (int j = start1; j < begin1; )
                        {
                            values[k++] = function(_values[j++], zero2);
                        }
                    }

                    // f(0, y)
                    else if (begin2 > start2)
                    {
                        int end = r - (end2 - start2) + 1;
                        for (int j = 0; j < end; ++j)
                        {
                            values[k++] = fzero;
                        }
                        for (int j = start2; j < begin2; )
                        {
                            values[k++] = function(_zero, matrix._values[j++]);
                        }
                    }
                    
                    // f(x, y)
                    for (int j = begin1, j2 = begin2; j < end1; )
                    {
                        values[k++] = function(_values[j++], matrix._values[j2++]);
                    }

                    indices[r + 1] = indices[r] + r + 1;
                }

                return new SKYSparseMatrix<TOut>(indices, values, _type, true);
            }
        }
        public SKYSparseMatrix<T> HadamardProduct(SKYSparseMatrix<T> matrix, IProvider<T> provider)
        {
            return PointwiseMultiply(matrix, provider);
        }
        public SKYSparseMatrix<T> HadamardPower(int power, IProvider<T> provider)
        {
            return PointwisePower(power, provider);
        }
        public SKYSparseMatrix<T> PointwiseMultiply(SKYSparseMatrix<T> matrix, IProvider<T> provider)
        {
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            return Pointwise(matrix, provider.Multiply);
        }
        public SKYSparseMatrix<T> PointwiseDivide(SKYSparseMatrix<T> matrix, IProvider<T> provider)
        {
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            return Pointwise(matrix, provider.Divide);
        }
        public SKYSparseMatrix<T> PointwiseInverse(IProvider<T> provider)
        {
            T one = provider.One;
            return Elementwise(x => provider.Divide(one, x));
        }
        public SKYSparseMatrix<T> PointwisePower(int power, IProvider<T> provider)
        {
            return Elementwise(x => provider.Pow(x, power));
        }

        public override void SwapRows(int rowIndex1, int rowIndex2)
        {
            throw new NotImplementedException("Unable to swap rows because the resulting matrix cannot be stored as a Skyline matrix.");
        }

        public override void SwapColumns(int columnIndex1, int columnIndex2)
        {
            throw new NotImplementedException("Unable to swap rows because the resulting matrix cannot be stored as a Skyline matrix.");
        }

        /// <summary>
        /// Replaces all entries which satisfy a predicate with a specified value.
        /// </summary>
        /// <param name="replace">
        /// The predicate returning true if the entry should be replaced.
        /// </param>
        /// <param name="replacement">The replacement value.</param>
        public void ReplaceAll(Predicate<T> replace, T replacement)
        {
            if (replace is null)
            {
                throw new ArgumentNullException(nameof(replace));
            }
            if (replacement is null)
            {
                throw new ArgumentNullException(nameof(replacement));
            }

            for (int i = 0; i < _values.Length; ++i)
            {
                if (replace(_values[i]))
                {
                    _values[i] = replacement;
                }
            }
        }

        /// <summary>
        /// Overwrite this matrix with its transpose.
        /// </summary>
        public void TransposeInPlace()
        {
            if (_type == SKYType.LOWER)
            {
                _type = SKYType.UPPER;
            }
            else if (_type == SKYType.UPPER)
            {
                _type = SKYType.LOWER;
            }
        }
        /// <summary>
        /// Transpose this matrix, returning a new matrix without changing this matrix.
        /// </summary>
        /// <returns>The transpose of this matrix.</returns>
        public SKYSparseMatrix<T> Transpose()
        {
            SKYSparseMatrix<T> matrix = Copy();
            matrix.TransposeInPlace();
            return matrix;
        }

        public override T Trace(IProvider<T> provider)
        {
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }

            T sum = provider.Zero;
            for (int r = 0; r < Rows; ++r)
            {
                int start = _indices[r], end = _indices[r + 1];
                if (end > start)
                {
                    // The last element of each row (column) will be the diagonal element
                    sum = provider.Add(sum, _values[end - 1]);
                }
            }
            return sum;
        }

        /// <summary>
        /// Returns the product of diagonal elements of this matrix.
        /// </summary>
        /// <param name="provider"></param>
        /// <returns></returns>
        public T DiagonalProduct(IProvider<T> provider)
        {
            int dim = Rows, start, end;

            T[] diagonal = new T[dim];
            for (int i = 0; i < dim; ++i)
            {
                start = _indices[i]; end = _indices[i + 1];
                if (end <= start)
                {
                    return provider.Zero;
                }
                diagonal[i] = _values[end - 1];
            }
            return provider.Product(diagonal);
        }

        /// <summary>
        /// Returns the max absolute row sum, for a lower matrix
        /// </summary>
        private static T MaxAbsRowSumLower(int[] indices, T[] values, int dim, IProvider<T> provider, IComparer<T> comparer)
        {
            T max = provider.Zero;
            for (int r = 0; r < dim; ++r)
            {
                int start = indices[r], end = indices[r + 1];
                T absSum = provider.Zero;
                for (int i = start; i < end; ++i)
                {
                    absSum = provider.Add(absSum, provider.IsRealAndNegative(values[i]) ? provider.Negate(values[i]) : values[i]);
                }
                if (comparer.Compare(max, absSum) < 0)
                {
                    max = absSum;
                }
            }
            return max;
        }
        private static T MaxAbsRowSumUpper(int[] indices, T[] values, int dim, IProvider<T> provider, IComparer<T> comparer)
        {
            T[] absSums = Zeroes(dim);
            for (int r = 0; r < dim; ++r)
            {
                int start = indices[r], end = indices[r + 1], c = r - (end - start) + 1;
                for (int i = start; c <= r; ++i, ++c)
                {
                    T v = values[i];
                    absSums[c] = provider.Add(absSums[c], provider.IsRealAndNegative(v) ? provider.Negate(v) : v);
                }
            }

            T max = absSums[0];
            for (int r = 1; r < dim; ++r)
            {
                if (comparer.Compare(max, absSums[r]) < 0)
                {
                    max = absSums[r];
                }
            }
            return max;
        }

        /// <summary>
        /// Vectorize a lower triangular matrix in row-major order
        /// </summary>
        private static SparseVector<T> VectorizeLowerRowMajor(int[] indices, T[] values, int dim)
        {
            int len = values.Length;
            int[] vect_indices = new int[len];
            T[] vect_values = new T[len];

            int k = 0;
            for (int r = 0; r < dim; ++r)
            {
                int start = indices[r], end = indices[r + 1], c = r - (end - start) + 1, index_offset = r * dim;
                for (int i = start; c <= r; ++c, ++i)
                {
                    vect_indices[k] = index_offset + c;
                    vect_values[k] = values[i];
                    ++k;
                }
            }
            return new SparseVector<T>(dim * dim, vect_indices, vect_values, true);
        }
        private static SparseVector<T> VectorizeLowerColumnMajor(int[] indices, T[] values, int dim)
        {
            int len = values.Length;
            int[] vect_indices = new int[len];
            T[] vect_values = new T[len];

            int k = 0;
            for (int r = 0; r < dim; ++r)
            {
                int start = indices[r], end = indices[r + 1], c = r - (end - start) + 1;
                for (int i = start; c <= r; ++c, ++i)
                {
                    vect_indices[k] = c * dim + r;
                    vect_values[k] = values[i];
                    ++k;
                }
            }

            // Requires an additional sort since we were traversing in row_major order
            Array.Sort(vect_indices, vect_values, 0, len);

            return new SparseVector<T>(dim * dim, vect_indices, vect_values, true);
        }
        private static SparseVector<T> VectorizeSymmetric(int[] indices, T[] values, int dim)
        {
            // Count the number of diagonal terms to work out nnz
            int diag = 0;
            for (int i = 0; i < dim; ++i)
            {
                if (indices[i + 1] > indices[i]) ++diag;
            }

            int nz = values.Length * 2 - diag;
            int[] vect_indices = new int[nz];
            T[] vect_values = new T[nz];

            // Lets try to keep things O(n) - initialize a workspace array of length dim, 
            // to keep track of the index of the first element in each row.
            int[] offsets = new int[dim];
            // populate counts with the number of elements in lower half of matrix (including diagonal)
            for (int r = 1; r < dim; ++r)
            {
                offsets[r] = indices[r] - indices[r - 1];
            }
            // Add the terms in the upper half of the matrix (excluding the diagonal)
            for (int r = 0; r < dim; ++r)
            {
                int start = indices[r], end = indices[r + 1], c = r - (end - start) + 1;
                for (; c < r;)
                {
                    offsets[++c]++;
                }
            }
            // Accumulate the counts
            for (int r = 1; r < dim; ++r)
            {
                offsets[r] += offsets[r - 1];
            }

            // Initialize another array to keep track of the number of elements in each row 
            // visited so far
            int[] visited = new int[dim];

            // Fill lower-diagonal terms
            for (int r = 0; r < dim; ++r)
            {
                // These will be in correct order - just need to find the index within the vector to start (k)
                int start = indices[r], 
                    end = indices[r + 1], 
                    length = end - start,
                    c = r - length + 1, 
                    k = offsets[r], 
                    offset = r * dim;

                for (int i = start; c <= r; ++c, ++i)
                {
                    vect_indices[k] = offset + c;
                    vect_values[k] = values[i];
                    ++k;
                }

                // Update visited
                visited[r] += length;
            }

            // Fill in the upper-diagonal terms (excluding the diagonal)
            for (int c = 0; c < dim; ++c)
            {
                int start = indices[c], end = indices[c + 1], r = c - (end - start) + 1;
                for (int i = start; r < c; ++r, ++i)
                {
                    int index = offsets[r] + visited[r];
                    vect_indices[index] = r * dim + c;
                    vect_values[index] = values[i];
                    visited[r]++;
                }
            }

            return new SparseVector<T>(dim * dim, vect_indices, vect_values, true);
        }
        public SparseVector<T> Vectorize(bool rowMajor = true)
        {
            if (rowMajor)
            {
                if (_type == SKYType.LOWER)
                {
                    return VectorizeLowerRowMajor(_indices, _values, Rows);
                }
                if (_type == SKYType.UPPER)
                {
                    return VectorizeLowerColumnMajor(_indices, _values, Rows);
                }
            }
            else
            {
                if (_type == SKYType.LOWER)
                {
                    return VectorizeLowerColumnMajor(_indices, _values, Columns);
                }
                if (_type == SKYType.UPPER)
                {
                    return VectorizeLowerRowMajor(_indices, _values, Columns);
                }
            }

            // Symmetric matrices generate the same vectorization regardless of order
            if (_type == SKYType.SYMMETRIC)
            {
                return VectorizeSymmetric(_indices, _values, Columns);
            }

            throw new NotSupportedException(nameof(Type));
        }

        public void SolveTriangular(DenseVector<T> rhsVector, DenseVector<T> solution, IProvider<T> provider)
        {
            if (rhsVector is null)
            {
                throw new ArgumentNullException(nameof(rhsVector));
            }
            if (solution is null)
            {
                throw new ArgumentNullException(nameof(solution));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }

            if (rhsVector.Dimension != Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(rhsVector), $"The number of columns of the coefficient matrix does not match the dimension of '{nameof(rhsVector)}'.");
            }
            if (solution.Dimension != Rows)
            {
                throw new ArgumentOutOfRangeException(nameof(solution), $"The number of rows of the coefficient matrix does not match the dimension of '{nameof(solution)}'.");
            }

            SolveTriangular(rhsVector.Values, solution.Values, provider);
        }
        internal void SolveTriangular(T[] b, T[] x, IProvider<T> provider, bool transpose = false)
        {
            if (_type == SKYType.LOWER && !transpose || _type == SKYType.UPPER && transpose)
            {
                for (int r = 0; r < Rows; ++r)
                {
                    int start = _indices[r], end = _indices[r + 1], start_col = r - (end - start) + 1;

                    T xr = b[r];
                    for (int c = start_col, i = start; c < r; ++c, ++i)
                    {
                        xr = provider.Subtract(xr, provider.Multiply(_values[i], x[c]));
                    }

                    // Divide by diagonal element
                    x[r] = provider.Divide(xr, _values[end - 1]);
                }
                return;
            }
            if (_type == SKYType.UPPER && !transpose || _type == SKYType.LOWER && transpose)
            {
                // Initialize x <- b
                Array.Copy(b, 0, x, 0, Rows);

                for (int c = Columns - 1; c >= 0; --c)
                {
                    int start = _indices[c], end = _indices[c + 1], r = c - (end - start) + 1;

                    T xc = provider.Divide(x[c], _values[end - 1]);
                    for (int i = start; r < c; ++r, ++i)
                    {
                        x[r] = provider.Subtract(x[r], provider.Multiply(_values[i], xc));
                    }
                    x[c] = xc;
                }
                return;
            }
            throw new InvalidOperationException("Triangular solve not implemented for symmetric matrices.");
        }
        internal override bool SolveTriangular(T[] b, T[] x, IDenseBLAS1<T> blas1, bool lower, bool transpose)
        {
            IProvider<T> provider = blas1.Provider;

            int dim = Rows;
            if (lower)
            {
                if (_type == SKYType.LOWER && !transpose || _type == SKYType.UPPER && transpose || _type == SKYType.SYMMETRIC)
                {
                    for (int r = 0; r < dim; ++r)
                    {
                        int start = _indices[r], end = _indices[r + 1], start_col = r - (end - start) + 1;

                        T dot = blas1.DOT(_values, x, start, end - 1, start_col - start);

                        // Divide by diagonal element
                        if (start >= end)
                        {
                            return false;
                        }
                        T diag = _values[end - 1];
                        if (provider.Equals(diag, _zero))
                        {
                            return false;
                        }
                        x[r] = provider.Divide(provider.Subtract(b[r], dot), _values[end - 1]);
                    }

                    return true;
                }
            }
            else
            {
                if (_type == SKYType.UPPER && !transpose || _type == SKYType.LOWER && transpose || _type == SKYType.SYMMETRIC)
                {
                    // Initialize x <- b
                    Array.Copy(b, 0, x, 0, Rows);

                    for (int c = Columns - 1; c >= 0; --c)
                    {
                        int start = _indices[c], last = _indices[c + 1] - 1, last_c = last - c;

                        if (start > last)
                        {
                            return false;
                        }
                        T diag = _values[last];
                        if (provider.Equals(diag, _zero))
                        {
                            return false;
                        }
                        T xc = provider.Divide(x[c], diag);
                        blas1.AXPY(x, _values, provider.Negate(xc), start - last_c, c, last_c);
                        x[c] = xc;
                    }
                    return true;
                }
            }

            // Edge cases here either: lower type is tranposed into upper, or upper type is untransposed, but 
            // we are attempting to do a lower-triangular solve ... in either case, its a diagonal solve

            for (int i = 0; i < dim; ++i)
            {
                int start = _indices[i], end = _indices[i + 1];

                // Catch division by zero exceptions
                if (end <= start)
                {
                    return false;
                }
                T diag = _values[end - 1];
                if (provider.Equals(diag, _zero))
                {
                    return false;
                }
                x[i] = provider.Divide(b[i], diag);
            }

            return true;
        }

        /// <summary>
        /// Returns the lower-triangular Cholesky factor $L$ such that $LL^* = A$ where $A$ is this matrix. 
        /// This matrix must be symmetric and positive definite. This method uses the default provider for type 
        /// <txt>T</txt>. It can be accelerated by using a level-1 dense BLAS implementation instead. 
        /// </summary>
        /// <returns>The lower-triangular sparse Cholesky factor $L$.</returns>
        public SKYSparseMatrix<T> CholeskyFactor()
        {
            return CholeskyFactor(ProviderFactory.GetDefaultProvider<T>());
        }
        public SKYSparseMatrix<T> CholeskyFactor(IProvider<T> provider)
        {
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }

            SKYCholesky<T> chol = new SKYCholesky<T>();
            return chol.CholUp(this, provider);
        }
        public SKYSparseMatrix<T> CholeskyFactor(IDenseBLAS1Provider<T> blas1)
        {
            if (blas1 is null)
            {
                throw new ArgumentNullException(nameof(blas1));
            }
            if (!(blas1 is IDenseBLAS1<T> b))
            {
                throw new NotSupportedException(ExceptionMessages.TypeNotSupported);
            }

            SKYCholesky<T> chol = new SKYCholesky<T>();
            return chol.CholUp(this, b);
        }

        /// <summary>
        /// Returns the Cholesky decomposition of this matrix, if it exists. 
        /// </summary>
        /// <param name="blas1">The level-1 dense BLAS use to compute the factorization.</param>
        /// <returns>The Cholesky decomposition of this matrix.</returns>
        public Cholesky<T> Cholesky(IDenseBLAS1Provider<T> blas1)
        {
            if (blas1 is null)
            {
                throw new ArgumentNullException(nameof(blas1));
            }
            if (!(blas1 is IDenseBLAS1<T> b))
            {
                throw new NotSupportedException(ExceptionMessages.TypeNotSupported);
            }

            SKYCholesky<T> chol = new SKYCholesky<T>();
            return new Cholesky<T>(chol.CholUp(this, b), b);
        }

        #endregion


        #region Operations

        /// <summary>
        /// Returns the indices of the matrix with non-zero pattern equal to the union of 
        /// non-zeroes pattern of this matrix and that of another matrix.
        /// </summary>
        private int[] GetUnionIndices(SKYSparseMatrix<T> matrix)
        {
            int dim = Rows;
            int[] indices = new int[dim + 1];

            // Store the lengths of row/column i in [i + 1]
            for (int i = 0; i < dim; ++i)
            {
                int i1 = i + 1;
                indices[i1] = Math.Max(_indices[i1] - _indices[i], matrix._indices[i1] - matrix._indices[i]);
            }

            // Accumulate the indices
            for (int i = 0; i < dim; ++i)
            {
                indices[i + 1] += indices[i];
            }
            return indices;
        }

        public override Matrix<T> Add(Matrix<T> matrix, IProvider<T> provider)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            if (CompareRepresentationPriority(this, matrix) < 0)
            {
                return matrix.Add(this, provider); // return the sum as type(matrix)
            }

            // Require special checks here because matrix may not be of compatible type
            return Add(matrix.ToSKYSparseMatrix(), provider);
        }

        public SKYSparseMatrix<T> Add(SKYSparseMatrix<T> matrix, IProvider<T> provider)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            if (_type != matrix._type)
            {
                throw new InvalidOperationException("Cannot add two Skyline matrices of different type.");
            }
            MatrixChecks.AssertMatrixDimensionsEqual(this, matrix);

            int dim = Rows;
            int[] indices = GetUnionIndices(matrix);
            T[] values = Zeroes(indices[dim]);

            // Iterate over row (column)
            for (int i = 0; i < dim; ++i)
            {
                // Add values from first matrix
                int sumEnd = indices[i + 1], 
                    end1 = _indices[i + 1],
                    offset = sumEnd - end1;
                for (int k = _indices[i]; k < end1; ++k)
                {
                    int index = offset + k;
                    values[index] = provider.Add(values[index], _values[k]);
                }

                int end2 = matrix._indices[i + 1],
                    offset2 = sumEnd - end2;
                for (int k = matrix._indices[i]; k < end2; ++k)
                {
                    int index = offset2 + k;
                    values[index] = provider.Add(values[index], matrix._values[k]);
                }
            }

            return new SKYSparseMatrix<T>(indices, values, _type, true);
        }
        public SKYSparseMatrix<T> Subtract(SKYSparseMatrix<T> matrix, IProvider<T> provider)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            if (_type != matrix._type)
            {
                throw new InvalidOperationException("Cannot subtract two Skyline matrices of different type.");
            }
            MatrixChecks.AssertMatrixDimensionsEqual(this, matrix);

            int dim = Rows;
            int[] indices = GetUnionIndices(matrix);
            T[] values = Zeroes(indices[dim]);

            // Iterate over row (column)
            for (int i = 0; i < dim; ++i)
            {
                // Add values from first matrix
                int sumEnd = indices[i + 1],
                    end1 = _indices[i + 1],
                    offset1 = sumEnd - end1;
                for (int k = _indices[i]; k < end1; ++k)
                {
                    int index = offset1 + k;
                    values[index] = provider.Add(values[index], _values[k]);
                }

                int end2 = matrix._indices[i + 1],
                    offset2 = sumEnd - end2;
                for (int k = matrix._indices[i]; k < end2; ++k)
                {
                    int index = offset2 + k;
                    values[index] = provider.Subtract(values[index], matrix._values[k]);
                }
            }

            return new SKYSparseMatrix<T>(indices, values, _type, true);
        }

        /// <summary>
        /// For lower-lower multiplication - find the non-zero pattern of row r of the product of this matrix
        /// and another matrix 'matrix', represented as a contigous block [col_start, r] (both inclusive), 
        /// where col_start is returned.
        /// 
        /// This is a perfect estimate if the 
        /// diagonal of both matrices are non-zero (which occurs most of the time), however will be an overestimate
        /// if the diagonals are not all non-zero. 
        /// 
        /// If there are no non-zeroes in the row of the product, then int.MaxValue is returned.
        /// </summary>
        private int MultiplyLowerLower_RowNonZeroStart(int[] indices, int r)
        {
            int start = _indices[r], end = _indices[r + 1];
            int row_start = r - (end - start) + 1, row_end = r;

            // Find the boundaries of the columns 
            int col_start = int.MaxValue;
            for (int c = row_start; c <= row_end; ++c)
            {
                int b_start = indices[c], b_end = indices[c + 1];

                // If there are any nz elements in row c of matrix B
                if (b_end > b_start)
                {
                    int b_col_start = c - (b_end - b_start) + 1;
                    if (b_col_start < col_start)
                    {
                        col_start = b_col_start;
                    }
                }
            }
            return col_start;
        }
        /// <summary>
        /// Multiplies two lower matrices in implicit form
        /// </summary>
        private SKYSparseMatrix<T> MultiplyLowerLower(int[] indices1, T[] values1, int[] indices2, T[] values2, IProvider<T> provider)
        {
            // Each row if the first matrix is only going to affect a subrow
            // of the product, defined by its start and end column indices (inclusive)
            // Furthermore, unless if the diagonal term is zero, the end column index 
            // is going to equal the row index. 

            // First - calculate the number of non-zeroes
            int nz = 0;
            for (int r = 0; r < Rows; ++r)
            {
                // Find the boundaries of the columns 
                int col_start = MultiplyLowerLower_RowNonZeroStart(indices2, r);

                // If there are any non-zeroes
                if (col_start <= r)
                {
                    nz += (r - col_start + 1);
                }
            }

            // Allocate the arrays 
            int[] prod_indices = new int[Rows + 1];
            T[] prod_values = new T[nz];
            // Temporary storage for a row of the product matrix
            T[] row_workspace = new T[Columns];

            // Second iteration for numerical calculations
            int n = 0;
            for (int r = 0; r < Rows; ++r)
            {
                // Find the start of the non-zero pattern of row r of the product
                int prod_col_start = MultiplyLowerLower_RowNonZeroStart(indices2, r);
                if (prod_col_start > r)
                {
                    // No entries in this row
                    prod_indices[r + 1] = n;
                    continue;
                }

                // Clear the workspace in [prod_col_start, r]
                int prod_len = r - prod_col_start + 1;
                Clear(row_workspace, prod_col_start, prod_len);

                // Get the set of row indices of B we are iterating over
                int start = indices1[r], end = indices1[r + 1];
                int b_col_start = r - (end - start) + 1;
                for (int k = b_col_start; k <= r; ++k)
                {
                    // Get the boundaries of row k of matrix B
                    int b_row_k_start = indices2[k],
                        b_row_k_end = indices2[k + 1],

                        // The column of the element at b_row_k_start
                        c = k - (b_row_k_end - b_row_k_start) + 1;

                    // Cache A[r, k]
                    T A_rk = values1[k - b_col_start + start];

                    // Iterate over row k of matrix B - this is just a modified AXPY
                    // with two iterates (i, c). Maybe will be supported by dense BLAS in future
                    for (int i = b_row_k_start; i < b_row_k_end; ++i, ++c)
                    {
                        row_workspace[c] = provider.Add(row_workspace[c], provider.Multiply(A_rk, values2[i]));
                    }
                }

                // Store into product matrices
                Array.Copy(row_workspace, prod_col_start, prod_values, n, prod_len);
                n += prod_len;
                prod_indices[r + 1] = n;
            }

            return new SKYSparseMatrix<T>(prod_indices, prod_values, SKYType.LOWER, true);
        }

        /// <summary>
        /// Postmultiplies this matrix by anther matrix in Skyline storage form. 
        /// 
        /// If the matrices are both lower triangular or both upper triangular, then another Skyline matrix 
        /// is returned.
        /// 
        /// If one of the matrices is lower triangular and the other is upper triangular, or if either matrix is symmetric, 
        /// then a band matrix is returned. This is because the product is not necessarily symmetric, nor lower/upper triangular
        /// and hence cannot be stored in a Skyline storage format. 
        /// 
        /// </summary>
        /// <param name="matrix"></param>
        /// <param name="provider"></param>
        /// <returns></returns>
        public Matrix<T> Multiply(SKYSparseMatrix<T> matrix, IProvider<T> provider)
        {
            if (_type == SKYType.UPPER && matrix._type == SKYType.UPPER)
            {
                // U1 * U2 = (U2' * U1')'
                SKYSparseMatrix<T> prod = MultiplyLowerLower(matrix._indices, matrix._values, _indices, _values, provider);
                prod._type = SKYType.UPPER;
                return prod;
            }
            if (_type == SKYType.LOWER && matrix._type == SKYType.LOWER)
            {
                return MultiplyLowerLower(_indices, _values, matrix._indices, matrix._values, provider);
            }

            // May require a more efficient implementation but this will do for now
            return new BandMatrix<T>(this).Multiply2(new BandMatrix<T>(matrix), provider);
        }

        private void MultiplyUpper(T[] vector, T[] product, IProvider<T> provider, bool increment, bool includeDiag)
        {
            if (!increment)
            {
                Clear(product, 0, Rows);
            }

            for (int c = 0; c < Columns; ++c)
            {
                int col_start = _indices[c],
                    col_end = _indices[c + 1],
                    row_offset = c - col_end + 1;

                if (!includeDiag)
                {
                    --col_end;
                }

                T vect_c = vector[c];
                for (int i = col_start; i < col_end; ++i)
                {
                    int r = row_offset + i;
                    product[r] = provider.Add(product[r], provider.Multiply(_values[i], vect_c));
                }
            }
        }
        private void MultiplyLower(T[] vector, T[] product, IProvider<T> provider, bool increment)
        {
            for (int r = 0; r < Rows; ++r)
            {
                int row_start = _indices[r],
                    row_end = _indices[r + 1],
                    col_offset = r - row_end + 1;

                T dot = increment ? product[r] : provider.Zero;
                for (int i = row_start; i < row_end; ++i)
                {
                    int c = col_offset + i;
                    dot = provider.Add(dot, provider.Multiply(_values[i], vector[c]));
                }
                product[r] = dot;
            }
        }
        internal override void Multiply(T[] vector, T[] product, IProvider<T> provider, bool increment)
        {
            if (_type == SKYType.UPPER)
            {
                MultiplyUpper(vector, product, provider, increment, true);
            }
            else if (_type == SKYType.LOWER)
            {
                MultiplyLower(vector, product, provider, increment);
            }
            else if (_type == SKYType.SYMMETRIC)
            {
                MultiplyLower(vector, product, provider, increment);
                MultiplyUpper(vector, product, provider, true, false);
            }
            else
            {
                throw new NotSupportedException();
            }
        }
        internal override void TransposeAndMultiply(T[] vector, T[] product, IProvider<T> provider, bool increment)
        {
            if (_type == SKYType.UPPER)
            {
                MultiplyLower(vector, product, provider, increment);
            }
            else if (_type == SKYType.LOWER)
            {
                MultiplyUpper(vector, product, provider, increment, true);
            }
            else if (_type == SKYType.SYMMETRIC)
            {
                MultiplyUpper(vector, product, provider, increment, false);
                MultiplyLower(vector, product, provider, true);
            }
            else
            {
                throw new NotSupportedException();
            }
        }

        public override SparseVector<T> Multiply(SparseVector<T> vector, IProvider<T> provider)
        {
            if (vector is null)
            {
                throw new ArgumentNullException(nameof(vector));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            if (vector.Dimension < Columns)
            {
                throw new ArgumentOutOfRangeException(nameof(vector), "The dimension of the vector is less than the number of columns in the matrix.");
            }

            // initializing workspaces is O(n), but everything else is O(flops)
            int[] nz_indices = new int[Rows];
            BitArray is_nz = new BitArray(Rows);
            T[] values = new T[Rows];

            int[] vect_indices = vector.Indices;
            T[] vect_values = vector.Values;

            if (_type == SKYType.LOWER)
            {
                int len = vect_indices.Length;
                for (int k = 0; k < len; ++k)
                {
                    int c = vect_indices[k];
                    for (int r = c; r < Rows; ++r)
                    {
                        int start = _indices[r + 1] - _indices[r] + r + 1;
                        if (start <= c && c <= r)
                        {

                        }
                    }
                }
            }
            throw new NotImplementedException();
        }

        public override SparseVector<T> TransposeAndMultiply(SparseVector<T> vector, IProvider<T> provider)
        {
            throw new NotImplementedException();
        }

        public override void MultiplyInPlace(T scalar, IProvider<T> provider)
        {
            if (scalar is null)
            {
                throw new ArgumentNullException(nameof(scalar));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }

            int len = _values.Length, i;
            for (i = 0; i < len; ++i)
            {
                _values[i] = provider.Multiply(_values[i], scalar);
            }
        }
        public SKYSparseMatrix<T> Multiply(T scalar, IProvider<T> provider)
        {
            SKYSparseMatrix<T> copy = Copy();
            copy.MultiplyInPlace(scalar, provider);
            return copy;
        }
        public void MultiplyInPlace(T scalar, IDenseBLAS1Provider<T> blas1)
        {
            if (scalar is null)
            {
                throw new ArgumentNullException(nameof(scalar));
            }
            if (blas1 is null)
            {
                throw new ArgumentNullException(nameof(blas1));
            }
            if (!(blas1 is IDenseBLAS1<T> b))
            {
                throw new NotSupportedException(ExceptionMessages.TypeNotSupported);
            }

            b.SCAL(_values, scalar, 0, _values.Length);
        }
        public SKYSparseMatrix<T> Multiply(T scalar, IDenseBLAS1Provider<T> blas1)
        {
            SKYSparseMatrix<T> copy = Copy();
            copy.Multiply(scalar, blas1);
            return copy;
        }

        /// <summary>
        /// Divide this matrix by a scalar, using the specified provider for elementwise division.
        /// A new matrix is returned without modifying this matrix.
        /// </summary>
        /// <param name="scalar">The scalar to divide by.</param>
        /// <param name="provider">The provider used for elementwise division.</param>
        /// <returns>The matrix-scalar division result.</returns>
        public SKYSparseMatrix<T> Divide(T scalar, IProvider<T> provider)
        {
            SKYSparseMatrix<T> copy = Copy();
            copy.DivideInPlace(scalar, provider);
            return copy;
        }

        #endregion


        #region Convertors

        public override BandMatrix<T> ToBandMatrix()
        {
            return new BandMatrix<T>(this);
        }
        public override BlockDiagonalMatrix<T> ToBlockDiagonalMatrix()
        {
            return new BlockDiagonalMatrix<T>(this);
        }
        public override COOSparseMatrix<T> ToCOOSparseMatrix()
        {
            return new COOSparseMatrix<T>(this);
        }
        public override CSCSparseMatrix<T> ToCSCSparseMatrix()
        {
            return new CSCSparseMatrix<T>(this);
        }
        public override CSRSparseMatrix<T> ToCSRSparseMatrix()
        {
            return new CSRSparseMatrix<T>(this);
        }
        public override DenseMatrix<T> ToDenseMatrix()
        {
            return new DenseMatrix<T>(this);
        }
        public override DiagonalMatrix<T> ToDiagonalMatrix()
        {
            return new DiagonalMatrix<T>(this);
        }
        public override DOKSparseMatrix<T> ToDOKSparseMatrix()
        {
            return new DOKSparseMatrix<T>(this);
        }
        public override MCSRSparseMatrix<T> ToMCSRSparseMatrix()
        {
            return new MCSRSparseMatrix<T>(this);
        }
        public override SKYSparseMatrix<T> ToSKYSparseMatrix()
        {
            return this;
        }

        #endregion


        #region Operators

        public static SKYSparseMatrix<T> operator +(SKYSparseMatrix<T> A, SKYSparseMatrix<T> B)
        {
            if (A is null)
            {
                throw new ArgumentNullException(nameof(A));
            }
            return A.Add(B, ProviderFactory.GetDefaultProvider<T>());
        }
        public static SKYSparseMatrix<T> operator -(SKYSparseMatrix<T> A, SKYSparseMatrix<T> B)
        {
            if (A is null)
            {
                throw new ArgumentNullException(nameof(A));
            }
            return A.Subtract(B, ProviderFactory.GetDefaultProvider<T>());
        }
        public static DenseVector<T> operator *(SKYSparseMatrix<T> matrix, DenseVector<T> vector)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            return matrix.Multiply(vector, ProviderFactory.GetDefaultProvider<T>());
        }
        public static Matrix<T> operator *(SKYSparseMatrix<T> A, SKYSparseMatrix<T> B)
        {
            if (A is null)
            {
                throw new ArgumentNullException(nameof(A));
            }
            return A.Multiply(B, ProviderFactory.GetDefaultProvider<T>());
        }
        public static SKYSparseMatrix<T> operator *(SKYSparseMatrix<T> matrix, T scalar)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            return matrix.Multiply(scalar, ProviderFactory.GetDefaultBLAS1<T>());
        }
        public static SKYSparseMatrix<T> operator *(T scalar, SKYSparseMatrix<T> matrix) => matrix * scalar;
        public static SKYSparseMatrix<T> operator /(SKYSparseMatrix<T> matrix, T scalar)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            return matrix.Divide(scalar, ProviderFactory.GetDefaultProvider<T>());
        }
        public static bool operator ==(SKYSparseMatrix<T> A, SKYSparseMatrix<T> B)
        {
            if (A is null || B is null)
            {
                return false;
            }
            return A.Equals(B);
        }
        public static bool operator !=(SKYSparseMatrix<T> A, SKYSparseMatrix<T> B) => !(A == B);

        #endregion

    }
    public enum SKYType
    {
        UPPER, LOWER, SYMMETRIC
    }
}
