﻿using LinearNet.Global;
using LinearNet.Providers;
using System;
using System.Text;

namespace LinearNet.Structs
{
    public static class SKYSparseMatrix
    {
        /// <summary>
        /// Create an identity sparse matrix in Skyline storage format, using the defined unity
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dim"></param>
        /// <returns></returns>
        public static SKYSparseMatrix<T> Identity<T>(int dim, T one, SKYType type = SKYType.SYMMETRIC) where T : new()
        {
            if (dim <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(dim), ExceptionMessages.NonPositiveNotAllowed);
            }
            if (one is null)
            {
                throw new ArgumentNullException(nameof(one));
            }

            int[] indices = new int[dim + 1];
            T[] values = RectangularVector.Repeat(one, dim);
            for (int r = 1; r <= dim; ++r)
            {
                indices[r] = r;
            }
            return new SKYSparseMatrix<T>(indices, values, type, true);
        }

        /// <summary>
        /// Create an identity matrix in Skyline storage format, using the default unity of type <txt>T</txt>.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dim"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public static SKYSparseMatrix<T> Identity<T>(int dim, SKYType type = SKYType.SYMMETRIC) where T : new()
        {
            IProvider<T> provider = ProviderFactory.GetDefaultProvider<T>();
            return Identity(dim, provider.One, type);
        }

        /// <summary>
        /// Try to save the Skyline sparse matrix into a readable format. The written file will contain 4 items:
        /// 1) The dimension of the matrix, as an integer.
        /// 2) The type of the matrix, as a string.
        /// 3) A list of all integer values forming the indexes of the matrix, separated by the delimiter 'delim'.
        /// 4) A list of all recorded entries of the matrix, separated by the delimiter 'delim'.
        /// The items will be separated by the <txt>eolDelim</txt>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="path"></param>
        /// <param name="matrix"></param>
        /// <returns></returns>
        public static void Save<T>(string path, SKYSparseMatrix<T> matrix, string delim = "|", string eolDelim = "\n") where T : new()
        {
            if (string.IsNullOrEmpty(path))
            {
                throw new ArgumentNullException(nameof(path));
            }
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            StringBuilder sb = new StringBuilder(matrix.Rows);
            sb.Append(eolDelim);
            sb.Append(matrix.Type.ToString());
            sb.Append(eolDelim);

            // 3) all indexes of the matrix
            int[] indices = matrix.Indices;
            if (indices.Length > 0)
            {
                sb.Append(indices[0]);
                for (int i = 1; i < indices.Length; ++i)
                {
                    sb.Append(delim);
                    sb.Append(indices[i]);
                }
            }
            sb.Append(eolDelim);

            // 4) all values of the matrix
            T[] values = matrix.Values;
            if (values.Length > 0)
            {
                sb.Append(values[0]);
                for (int i = 1; i < values.Length; ++i)
                {
                    sb.Append(delim);
                    sb.Append(values[i].ToString());
                }
            }

            System.IO.File.WriteAllText(path, sb.ToString());
        }
        public static bool TryRead<T>(string path, out SKYSparseMatrix<T> matrix) where T : new()
        {
            matrix = default;
            return false;
        }
    }
}
