﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Structs
{
    /// <summary>
    /// An interface for a commutative ring, i.e. a ring
    /// whose multiplicative operation commutes. 
    /// 
    /// IField<T> inherits from both CommutativeRing<T> and DivisionRing<T>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface ICommutativeRing<T> : IRing<T> where T : new()
    {

    }
}
