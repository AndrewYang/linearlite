﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Structs
{
    /// <summary>
    /// A ring over the set of type T that is also equipped with 
    /// a multiplicative inverse. Division rings differ from Fields 
    /// (see interface IField<T>) in that multiplication is not necessarily 
    /// commutative. 
    /// 
    /// Examples of implementations: Quaternion
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <cat>linear-algebra</cat>
    public interface IDivisionRing<T> : IRing<T> where T : new()
    {
        /// <summary>
        /// Calculates and returns the multiplicative inverse of this element.
        /// </summary>
        /// <returns>The multiplicative inverse.</returns>
        T MultiplicativeInverse();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <cat>linear-algebra</cat>
    public static class DivisionRingExtensions
    {
        public static T Divide<T>(this T f, T g) where T : IDivisionRing<T>, new()
        {
            return f.Multiply(g.MultiplicativeInverse());
        }
    }
}
