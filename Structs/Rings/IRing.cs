﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Structs
{
    /// <summary>
    /// Implements a Ring structure.
    /// 
    /// The following methods are inherited from Group<T>
    /// 
    /// T AdditiveIdentity { get; }
    /// T Add(T e);
    /// T AdditiveInverse();
    /// 
    /// The following methods are inherited from SemiRing<T>
    /// 
    /// T AdditiveIdentity { get; }
    /// T Add(T e);
    /// T MultiplicativeIdentity { get; }
    /// T Multiply(T e);
    /// 
    /// Note that IRing<T> has the additional requirement over Group<T>
    /// in that the Add(e) method must be commutative, i.e. e.Add(f) = f.Add(e)
    /// for any two elements e, f in IRing<T>. 
    /// Non-commutativity will lead to unexpected results when computing
    /// with Rings.
    /// 
    /// IRing<T> is also an extension over SemiRing<T> since it has an 
    /// additive inverse 
    /// 
    /// </summary>
    /// <cat>linear-algebra</cat>
    public interface IRing<T> : IAdditiveGroup<T>, ISemiring<T> where T : new()
    {

    }
}
