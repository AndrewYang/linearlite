﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Structs
{
    /// <summary>
    /// <p>
    /// Represents the algebraic structure of a semiring over the set $T$, which differs from 
    /// a ring in that elements do not necessarily have an additive inverse.
    /// </p>
    /// <p>
    /// The <txt>IRing<T></txt> interface extends this interface.
    /// </p>
    /// </summary>
    /// <remarks>
    /// This interface inherits: 
    /// - T Add(T e);                   from AdditiveSemigroup<T>
    /// - T AdditiveIdentity { get; }   from AdditiveMonoid<T>
    /// </remarks>
    /// <typeparam name="T"></typeparam>
    /// <cat>linear-algebra</cat>
    public interface ISemiring<T> : IAdditiveMonoid<T> where T : new()
    {
        /// <summary>
        /// The binary multiplicative operator, returns (this) * b without altering (this) or b
        /// Note that multiplication does not need to commute for Semirings.
        /// </summary>
        /// <param name="b"></param>
        /// <returns></returns>
        T Multiply(T b);

        /// <summary>
        /// The element e (of type T) in IRing<T> such that 
        /// e * a = a * e = e for any element a in IRing<T>, 
        /// where * is the multiplication operator. 
        /// </summary>
        T MultiplicativeIdentity { get; }
    }
}
