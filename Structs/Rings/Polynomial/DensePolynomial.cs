﻿using LinearNet.Global;
using LinearNet.Providers;
using LinearNet.Signal.Convolution;
using LinearNet.Solvers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Structs
{
    /// <summary>
    /// Implements a polynomial ring over a single indeterminate <txt>T</txt>.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <cat>linear-algebra</cat>
    public partial class DensePolynomial<T> : IRing<DensePolynomial<T>>, IEquatable<DensePolynomial<T>> where T : new()
    {
        private readonly IProvider<T> _provider;
        protected readonly int _degree;
        protected readonly T[] _coefficients;

        public int Degree { get { return _degree; } }
        public T[] Coefficients { get { return _coefficients; } }
        internal IProvider<T> Provider { get { return _provider; } }

        public DensePolynomial<T> Zero => new DensePolynomial<T>(int.MinValue, new T[] { _provider.Zero }, _provider);
        public DensePolynomial<T> One => new DensePolynomial<T>(0, new T[] { _provider.One }, _provider);

        public DensePolynomial<T> MultiplicativeIdentity => One;
        public DensePolynomial<T> AdditiveIdentity => Zero;

        #region Constructors
        /// <summary>
        /// Zero constructor
        /// </summary>
        public DensePolynomial()
        {
            _provider = ProviderFactory.GetDefaultProvider<T>();
            _degree = int.MinValue;
            _coefficients = new T[0];
        }

        /// <summary>
        /// <p>Create a dense polynomial given its coefficients arranged with largest powers first.</p>
        /// <p>E.g. $-x^6 + 2x^3 - 5x^2 - x + 1$ is created from the array [-1, 0, 0, 2, -5, -1, 1].</p>
        /// </summary>
        /// <param name="coefficients">An array of the polynomial's coefficients</param>
        public DensePolynomial(params T[] coefficients) : this(coefficients, ProviderFactory.GetDefaultProvider<T>()) { }

        /// <summary>
        /// <p>
        /// Create a dense polynomial given a continuous collection of its coefficients, arranged either 
        /// in ascending power order or descending power order (default).
        /// </p>
        /// </summary>
        /// <param name="coefficients">An ordered collection of its coefficients.</param>
        /// <param name="ascending">Specified whether the coefficients are arranged in ascending power order
        /// or descending power order. For example, [1, 2, 3] will be interpreted as $x^2 + 2x + 3$ is <txt>ascending</txt>
        /// is <txt>false</txt>, and $1 + 2x + 3x^2$ otherwise.</param>
        public DensePolynomial(IEnumerable<T> coefficients, bool ascending = false) : this(GetCoefficientArray(coefficients, ascending), ProviderFactory.GetDefaultProvider<T>()) { }
        private static T[] GetCoefficientArray(IEnumerable<T> coefficients, bool ascending)
        {
            // By default coefficients are descending
            if (ascending)
            {
                return coefficients.Reverse().ToArray();
            }
            return coefficients.ToArray();
        }

        /// <summary>
        /// Copy constructor
        /// </summary>
        /// <param name="polynomial"></param>
        public DensePolynomial(DensePolynomial<T> polynomial)
        {
            _degree = polynomial._degree;
            _coefficients = polynomial._coefficients.Copy();
            _provider = polynomial._provider;
        }

        internal DensePolynomial(T[] coeffs, IProvider<T> provider)
        {
            _provider = provider;

            // Find the first non-zero coefficient
            int start = 0;
            T zero = provider.Zero;
            while (start < coeffs.Length && provider.ApproximatelyEquals(coeffs[start], zero, Precision.DOUBLE_PRECISION)) start++;

            if (start == coeffs.Length)
            {
                _degree = int.MinValue;
                _coefficients = new T[1] { zero };  // 0 constant term
            }
            else
            {
                _degree = coeffs.Length - start - 1;
                _coefficients = new T[coeffs.Length - start];
                for (int d = 0; d < Coefficients.Length; ++d)
                {
                    _coefficients[d] = coeffs[d + start];
                }
            }
        }
        internal DensePolynomial(int degree, T[] coefficients, IProvider<T> provider)
        {
            _degree = degree;
            _coefficients = coefficients;
            _provider = provider;
        }
        #endregion 

        /// <summary>
        /// <para>
        /// Evaluates the polynomial at point $x\in$ <txt>T</txt>, $P(x)$.
        /// </para>
        /// </summary>
        /// <param name="x">The point $x$ at which to evaluate the polynomial.</param>
        /// <returns>$P(x)$</returns>
        public T Evaluate(T x)
        {
            if (_degree == int.MinValue)
            {
                // avoid using _provider.Zero because _provider might be null
                return new T(); 
            }

            T px = _coefficients[0];
            int len = _coefficients.Length, d;
            for (d = 1; d < len; ++d)
            {
                // value <- value * x
                px = _provider.Multiply(px, x);
                px = _provider.Add(px, _coefficients[d]);
            }
            return px;
        }

        /// <summary>
        /// Evaluates the natural logarithm of P(x) given some $x\in$ <txt>T</txt>.
        /// Not implemented for polynomials over integer fields
        /// </summary>
        /// <param name="x">The point $x$ at which to evaluate the polynomial.</param>
        /// <returns>$\log(P(x))$</returns>
        public void LogEvaluate(T x, out T logAbs, out int sign)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Evaluate the derivative of polynomial at a point c
        /// This method is more efficient that finding a polynomial's derivative 
        /// then evaluating it at a point x.
        /// </summary>
        /// <param name="x">The point where we evaluate the polynomial's derivative</param>
        /// <returns>P'(x) where P(x) is the polynomial</returns>
        public T Derivative(T x)
        {
            // If polynomial is constant, derivative will be 0.
            if (_degree <= 0)
            {
                // avoid using _provider.Zero because _provider might be null
                return new T();
            }

            T one = _provider.One;
            T xPow = one, derivative = _provider.Zero, n = _provider.Zero;
            for (int d = _coefficients.Length - 2; d >= 0; d--)
            {
                n = _provider.Add(n, one);
                derivative = _provider.Add(derivative, _provider.Multiply(_provider.Multiply(xPow, _coefficients[d]), n));
                xPow = _provider.Multiply(xPow, x);
            }
            return derivative;
        }

        /// <summary>
        /// <para>
        /// For a univariate polynomial $P(x):$ <txt>T</txt> -> <txt>T</txt> and a point $x$,
        /// calculates $\log(|P(x)|)$ and $\sign(P(x))$.
        /// </para>
        /// <para>
        /// This method is more efficient than finding a polynomial's derivative than
        /// calculating the derivative polynomial at a point $x$.
        /// </para>
        /// </summary>
        /// <param name="x">The point at which to evaluate the derivative of the polynomial.</param>
        /// <returns>The polynomial's derivative at point $x$.</returns>
        public void LogDerivative(T x, out T logAbsDerivative, out int sign)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Returns the derivative polynomial 
        /// </summary>
        /// <returns></returns>
        public DensePolynomial<T> Derivative()
        {
            // if constant, derivative is 0
            if (_degree <= 0)
            {
                // avoid using _provider.Zero because _provider might be null
                return Zero;
            }

            // p(x) = 0, p(x) = c => p'(x) = 0

            T[] derivative = new T[_coefficients.Length - 1];
            T one = _provider.One, degree = _provider.Zero;

            // Must traverse backwards since n must be incremented
            for (int i = derivative.Length - 1; i >= 0; --i)
            {
                degree = _provider.Add(degree, one);
                derivative[i] = _provider.Multiply(_coefficients[i], degree);
            }
            return new DensePolynomial<T>(_degree - 1, derivative, _provider);
        }

        /// <summary>
        /// Evaluates the composition of this polynomial with another polynomial. 
        /// </summary>
        /// <param name="q">Any polynomial over <txt>T</txt>.</param>
        /// <returns>The polynomial composition $q(x)$</returns>
        public DensePolynomial<T> Of(DensePolynomial<T> q)
        {
            if (q is null) throw new ArgumentNullException(nameof(q));

            if (_coefficients.Length == 0)
            {
                return new DensePolynomial<T>();
            }

            DensePolynomial<T> sum = q.Pow(_coefficients.Length - 1);
            sum.MultiplyInPlace(_coefficients[0]);

            for (int i = 1; i < _coefficients.Length; ++i)
            {
                DensePolynomial<T> term = q.Pow(_coefficients.Length - 1 - i);
                term.MultiplyInPlace(_coefficients[i]);

                sum = sum.Add(term);
            }

            return sum;
        }

        public DensePolynomial<T> Pow(int power)
        {
            return new DensePolynomial<T>(PowArray(power), _provider);
        }
        private T[] PowArray(int power)
        {
            if (power < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(power), ExceptionMessages.NegativeNotAllowed);
            }

            // Return the multiplicative identity
            if (power == 0)
            {
                return new T[] { _provider.One };
            }

            // If deg(p(x)) = n, then deg(p(x)^m) = mn
            T[] pow = new T[(Coefficients.Length - 1) * power + 1];
            T[] workspace = new T[pow.Length];

            PowInner(_coefficients, pow, workspace, new NaiveDiscreteConvolution<T>(_provider), power);

            return pow;
        }
        private void PowInner(T[] x, T[] pow, T[] workspace, IDiscreteConvolution1D<T> conv, int power)
        {
            // Guaranteed that power > 0.
            if (power == 1)
            {
                Array.Copy(x, 0, pow, 0, x.Length);
                return;
            }

            // Note that the convolution operator is 0-indexed, so all elements must be 
            // in a contingous block starting at index 0.

            // pow <- x^(power / 2)
            int half_pow = power / 2;
            PowInner(x, pow, workspace, conv, half_pow);

            // workspace <- pow * pow
            int nTerms = (x.Length - 1) * half_pow + 1;
            conv.Transform(pow, pow, workspace, nTerms, nTerms);

            if (power % 2 == 0)
            {
                // pow <- workspace
                Array.Copy(workspace, 0, pow, 0, pow.Length);
            }
            else
            {
                // pow <- workspace * x
                int wsTerms = (x.Length - 1) * (half_pow * 2) + 1;
                conv.Transform(workspace, x, pow, wsTerms, x.Length);
            }
        }


        #region Ring methods
        public DensePolynomial<T> AdditiveInverse()
        {
            if (_degree == int.MinValue)
            {
                return new DensePolynomial<T>();
            }

            T[] coeffs = Coefficients.Copy();
            for (int i = 0; i < coeffs.Length; ++i)
            {
                coeffs[i] = _provider.Negate(coeffs[i]);
            }
            return new DensePolynomial<T>(coeffs, _provider);
        }
        public DensePolynomial<T> Multiply(DensePolynomial<T> q)
        {
            // Multiplying by 0
            if (_degree == int.MinValue || q._degree == int.MinValue)
            {
                return new DensePolynomial<T>();
            }

            T[] coeffs = new T[Coefficients.Length + q.Coefficients.Length - 1];

            // For larger arrays, it may be better to swap this for a O(n log n) solution
            IDiscreteConvolution1D<T> conv = new NaiveDiscreteConvolution<T>(_provider);
            conv.Transform(_coefficients, q._coefficients, coeffs, _coefficients.Length, q._coefficients.Length);

            // Borrow the constructor for simplification
            return new DensePolynomial<T>(coeffs, _provider);
        }
        public DensePolynomial<T> Multiply(T scalar)
        {
            T[] coeffs = new T[_coefficients.Length];
            for (int i = 0; i < _coefficients.Length; ++i)
            {
                coeffs[i] = _provider.Multiply(coeffs[i], scalar);
            }
            return new DensePolynomial<T>(coeffs, _provider);
        }
        public void MultiplyInPlace(T scalar)
        {
            for (int i = 0; i < _coefficients.Length; ++i)
            {
                _coefficients[i] = _provider.Multiply(_coefficients[i], scalar);
            }
        }
        public DensePolynomial<T> Add(DensePolynomial<T> q)
        {
            if (q == null)
            {
                throw new ArgumentNullException(nameof(q));
            }

            if (_degree == int.MinValue && q._degree == int.MinValue)
            {
                // Return the 0 polynomial
                return new DensePolynomial<T>();
            }
            if (_degree == int.MinValue)
            {
                return q.Copy();
            }
            if (q._degree == int.MinValue)
            {
                return Copy();
            }

            DensePolynomial<T> min, max;
            if (Degree > q.Degree)
            {
                min = q;
                max = this;
            }
            else
            {
                min = this;
                max = q;
            }

            T[] coeffs = max.Coefficients.Copy();
            int offset = coeffs.Length - min.Coefficients.Length;
            for (int i = 0; i < min.Coefficients.Length; ++i)
            {
                int j = i + offset;
                coeffs[j] = _provider.Add(coeffs[j], min.Coefficients[i]);
            }

            // Borrow the constructor for simplification
            return new DensePolynomial<T>(coeffs, _provider);
        }
        public bool Equals(DensePolynomial<T> e)
        {
            if (this is null || e is null) return false;
            if (_degree != e.Degree) return false;

            int len = _coefficients.Length, i;
            for (i = 0; i < len; ++i)
            {
                if (!_provider.Equals(_coefficients[i], e.Coefficients[i])) return false;
            }
            return true;
        }
        public bool ApproximatelyEquals(DensePolynomial<T> e, double eps)
        {
            if (this == null || e == null) return false;
            if (_degree != e.Degree) return false;

            int len = _coefficients.Length, i;
            for (i = 0; i < len; ++i)
            {
                if (!_provider.ApproximatelyEquals(_coefficients[i], e.Coefficients[i], eps)) return false;
            }
            return true;
        }
        #endregion

        #region Operator overrides 
        public static DensePolynomial<T> operator +(DensePolynomial<T> p, DensePolynomial<T> q) => p.Add(q);
        public static DensePolynomial<T> operator -(DensePolynomial<T> p, DensePolynomial<T> q) => p.Subtract(q);
        public static DensePolynomial<T> operator -(DensePolynomial<T> p) => p.AdditiveInverse();
        public static DensePolynomial<T> operator *(DensePolynomial<T> p, DensePolynomial<T> q) => p.Multiply(q);
        public static bool operator ==(DensePolynomial<T> p, DensePolynomial<T> q) => p.Equals(q);
        public static bool operator !=(DensePolynomial<T> p, DensePolynomial<T> q) => !p.Equals(q);
        #endregion

        public override bool Equals(object e)
        {
            if (e == null || !(e is DensePolynomial<T>))
            {
                return false;
            }
            return Equals(e as DensePolynomial<T>);
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        public override string ToString()
        {
            if (Coefficients == null) return "null";
            if (Coefficients.Length == 0) return "0";

            StringBuilder s = new StringBuilder();
            for (int i = 0; i < Coefficients.Length; ++i)
            {
                int power = Degree - i;

                string coeff = Coefficients[i].ToString();
                if (i > 0)
                {
                    s.Append(" + ");
                }

                string x = "";
                if (power == 1) x = "x";
                else if (power > 1) x = "x^" + power;

                s.Append(coeff + x);
            }
            return s.ToString();
        }
        public DensePolynomial<T> Copy()
        {
            return new DensePolynomial<T>(this);
        }
    }
}
