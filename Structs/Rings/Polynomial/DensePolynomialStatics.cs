﻿using LinearNet.Global;
using LinearNet.Providers;
using System;
using System.Collections.Generic;
using System.Text;

namespace LinearNet.Structs
{
    /// <summary>
    /// Static methods associated with Polynomial<T> object
    /// </summary>
    public static class DensePolynomial
    {
        /// <summary>
        /// Returns the univariate polynomial raised to a power. The original polynomial will be unchanged.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="polynomial">The base polynomial.</param>
        /// <param name="power">The power to raise the polynomial to. Must be >= 0.</param>
        public static DensePolynomial<T> Pow<T>(DensePolynomial<T> polynomial, int power) where T : new()
        {
            if (power < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(power), ExceptionMessages.NegativeNotAllowed);
            }

            if (power == 0)
            {
                return polynomial.AdditiveIdentity;
            }

            if (power == 1)
            {
                return polynomial.Copy();
            }

            IProvider<T> provider = polynomial.Provider;
            //List<int[]> powers = Partitioner.GetAllPartitions(power, summands.Length);

            throw new NotImplementedException();
        }
    }
}
