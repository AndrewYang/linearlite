﻿using LinearNet.Global;
using LinearNet.Helpers;
using LinearNet.Providers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace LinearNet.Structs
{
    /// <summary>
    /// Implements a multivariate polynomial class.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <cat>linear-algebra</cat>
    public class MultivariatePolynomial<T> : IRing<MultivariatePolynomial<T>>, ICloneable where T : new()
    {
        /// <summary>
        /// If the number of indeterminates is < 0, then it is not set and the polynomial must be identically 0.
        /// </summary>
        private readonly int _numIndeterminates;

        /// <summary>
        /// Unique identifiers of the indeterminates (does not have to be single letters)
        /// </summary>
        private readonly string[] _indeterminates;

        /// <summary>
        /// The hash function takes care of efficient int[] -> string and string -> int[] conversions 
        /// </summary>
        private readonly ArrayHash<int> _hashFunction;

        /// <summary>
        /// The coefficients dictionary: efficient string -> coefficient storage.
        /// </summary>
        private readonly Dictionary<string, T> _coefficients;
        private readonly IProvider<T> _provider;

        /// <summary>
        /// Returns the multiplicative identity of this polynomial type
        /// </summary>
        public MultivariatePolynomial<T> MultiplicativeIdentity => new MultivariatePolynomial<T>(_provider.One);
        public MultivariatePolynomial<T> AdditiveIdentity => new MultivariatePolynomial<T>(_provider.Zero);

        /// <summary>
        /// Returns an array of unique strings representing the names of the indeterminates of this polynomial.
        /// </summary>
        public string[] Indeterminates { get { return _indeterminates; } }

        /// <summary>
        /// Returns whether the multivariate polynomial is a constant.
        /// </summary>
        public bool IsConstant 
        { 
            get
            {
                // Degenerate cases
                if (_numIndeterminates <= 0 || _coefficients.Count == 0) return true;

                // Calculate the constant hash, if any other hash exists in the coefficients 
                // dictionary, then the polynomial is not constant.
                string const_hash = _hashFunction.Hash(new int[_numIndeterminates]);
                return !_coefficients.Keys.Any(k => k != const_hash);
            } 
        }

        /// <summary>
        /// Returns the constant value that the polynomial is equal to. 
        /// Throws <txt>InvalidOperationException</txt> if the polynomial is not a constant.
        /// </summary>
        public T ConstantValue
        {
            get
            {
                if (!IsConstant)
                {
                    throw new InvalidOperationException("Polynomial is not a constant.");
                }

                if (_numIndeterminates < 0 || _coefficients.Count == 0)
                {
                    return _provider.Zero;
                }

                int[] index = new int[_numIndeterminates];
                string const_hash = _hashFunction.Hash(index);
                
                if (!_coefficients.ContainsKey(const_hash))
                {
                    return _provider.Zero;
                }

                return _coefficients[const_hash];
            }
        }

        #region Constructors

        /// <summary>
        /// Create a degenerate polynomial equal to 0.
        /// </summary>
        public MultivariatePolynomial()
        {
            _numIndeterminates = int.MinValue;
            _indeterminates = null;
            _hashFunction = null;
            _coefficients = new Dictionary<string, T>();

            if (!ProviderFactory.TryGetDefaultProvider(out IProvider<T> provider))
            {
                throw new NotSupportedException(nameof(provider) + " is not a supported type.");
            }
            _provider = provider;
        }

        /// <summary>
        /// Create a new multivariate polynomial object with the specified number of indeterminates.
        /// </summary>
        public MultivariatePolynomial(params string[] indeterminates)
        {
            if (indeterminates is null)
            {
                throw new ArgumentNullException(nameof(indeterminates));
            }

            _numIndeterminates = indeterminates.Length;
            _indeterminates = indeterminates;
            _coefficients = new Dictionary<string, T>();
            _hashFunction = new ArrayHash<int>(_numIndeterminates, sizeof(int));

            if (!ProviderFactory.TryGetDefaultProvider(out IProvider<T> provider))
            {
                throw new NotSupportedException(nameof(provider) + " is not a supported type.");
            }
            _provider = provider;
        }

        /// <summary>
        /// Copy constructor
        /// </summary>
        /// <param name="polynomial"></param>
        public MultivariatePolynomial(MultivariatePolynomial<T> polynomial)
        {
            if (polynomial is null) throw new ArgumentNullException(nameof(polynomial));

            _numIndeterminates = polynomial._numIndeterminates;
            _indeterminates = polynomial._indeterminates.Copy();

            // Assume that T is struct-like and hence does not require deep-cloning.
            _coefficients = new Dictionary<string, T>(polynomial._coefficients);
            _hashFunction = new ArrayHash<int>(_numIndeterminates, sizeof(int));

            // No need to clone providers as it is not stateful. Thread safety is a potential future concern
            _provider = polynomial._provider;
        }

        /// <summary>
        /// Initialize a multivariate polynomial from a univariate dense polynomial
        /// </summary>
        /// <param name="polynomial"></param>
        public MultivariatePolynomial(DensePolynomial<T> polynomial) : this(new string[] { "x" })
        {
            if (polynomial is null) throw new ArgumentNullException(nameof(polynomial));

            int[] index_buff = new int[1];
            for (int i = 0; i < polynomial.Coefficients.Length; ++i)
            {
                index_buff[0] = polynomial.Coefficients.Length - i - 1;
                _coefficients[_hashFunction.Hash(index_buff)] = polynomial.Coefficients[i];
            }
        }

        /// <summary>
        /// Create a multivariate polynomial from a univarate sparse polynomial.
        /// </summary>
        /// <param name="polynomial">A univariate polynomial.</param>
        /// <param name="indeterminate">
        /// The indeterminate of the univariate polynomial (can be anything, is used to 
        /// differentiate between indeterminates that can be added later).
        /// </param>
        public MultivariatePolynomial(Polynomial<T> polynomial, string indeterminate = "x")
        {
            if (polynomial is null)
            {
                throw new ArgumentNullException(nameof(polynomial));
            }
            if (string.IsNullOrWhiteSpace(indeterminate))
            {
                throw new ArgumentNullException(nameof(indeterminate));
            }

            int[] index_buff = new int[1];
            foreach (KeyValuePair<long, T> pair in polynomial.Coefficients)
            {
                if (pair.Key > int.MaxValue)
                {
                    throw new ArgumentOutOfRangeException(nameof(polynomial), 
                        "Multivariate polynomial only supports powers up to int.MaxValue.");
                }
                index_buff[0] = (int)pair.Key;
                _coefficients[_hashFunction.Hash(index_buff)] = pair.Value;
            }
        }
        /// <summary>
        /// Initialize a polynomial as a constant
        /// </summary>
        /// <param name="constant"></param>
        public MultivariatePolynomial(T constant) : this(new string[0])
        {
            // Pattern is retained here for readability
            _coefficients[_hashFunction.Hash(new int[0])] = constant;
        }

        /// <summary>
        /// <p>Initialize a multivariate polynomial over T, using its coefficients. All other coefficients are 
        /// assumed zero.</p>
        /// <p>
        /// The polynomial is assumed to reside in $T[x_1, x_2, ..., x_d]$ where the $x_i$ are the indeterminates.</p>
        /// </summary>
        /// <param name="indeterminates">The indeterminates of this polynomial.</param>
        /// <param name="coefficients"></param>
        public MultivariatePolynomial(string[] indeterminates, IEnumerable<Tuple<int[], T>> coefficients) : this(indeterminates)
        {
            if (coefficients is null)
            {
                throw new ArgumentNullException(nameof(coefficients));
            }

            foreach (Tuple<int[], T> coeff in coefficients)
            {
                _coefficients[_hashFunction.Hash(coeff.Item1)] = coeff.Item2;
            }
        }
        internal MultivariatePolynomial(string[] indeterminates, Dictionary<string, T> coefficientsByHash) : this(indeterminates)
        {
            _coefficients = coefficientsByHash;
        }

        /// <summary>
        /// Re-index the polynomial using the new set of indeterminates. There must be no term in the polynomial that 
        /// depends on any indeterminate not present in <txt>indeterminates</txt>.
        /// </summary>
        /// <param name="polynomial">Any multivariate polyomial</param>
        /// <param name="indeterminates">A new set of indeterminates that is a superset of the existing set of </param>
        internal MultivariatePolynomial(MultivariatePolynomial<T> polynomial, string[] indeterminates) : this(indeterminates)
        {
            if (polynomial is null) throw new ArgumentNullException(nameof(polynomial));
            if (indeterminates is null) throw new ArgumentNullException(nameof(indeterminates));

            if (indeterminates.Distinct().Count() != indeterminates.Length)
            {
                throw new ArgumentException("Array of indeterminates contain duplicate elements.");
            }

            // convertTable[old index] = new index
            int[] convertTable = new int[polynomial._numIndeterminates];
            for (int i = 0; i < polynomial._numIndeterminates; ++i)
            {
                string variable = polynomial._indeterminates[i];

                // If not present in the new set of indeterminates, converted index is -1.
                convertTable[i] = -1;
                for (int j = 0; j < indeterminates.Length; ++j)
                {
                    if (indeterminates[j] == variable)
                    {
                        convertTable[i] = j;
                        break;
                    }
                }
            }

            // Start the index conversion
            int[] oldIndex = new int[polynomial._numIndeterminates];
            int[] newIndex = new int[_numIndeterminates];
            Dictionary<string, T> coefficients = polynomial._coefficients;
            foreach (KeyValuePair<string, T> pair in coefficients)
            {
                polynomial._hashFunction.Unhash(pair.Key, oldIndex);

                // Clear buffer
                Array.Clear(newIndex, 0, _numIndeterminates);

                // Convert old index array to new form
                for (int i = 0; i < polynomial._numIndeterminates; ++i)
                {
                    // if power > 0
                    int old_ind = oldIndex[i];
                    if (old_ind > 0) 
                    {
                        int new_ind = convertTable[i];
                        if (new_ind < 0)
                        {
                            throw new ArgumentOutOfRangeException(
                                $"Polynomial contains a term that depends on the indeterminate '{ polynomial._indeterminates[i] }' which is not present in the new indeterminate array.");
                        }
                        newIndex[new_ind] = old_ind;
                    }
                }

                _coefficients.Add(_hashFunction.Hash(newIndex), pair.Value);
            }
        }

        #endregion

        /// <summary>
        /// Returns the coefficient associated with the specified monomial.
        /// </summary>
        /// <param name="monomial"></param>
        /// <returns></returns>
        public T GetCoefficient(int[] monomial)
        {
            if (monomial is null)
            {
                throw new ArgumentNullException(nameof(monomial));
            }

            if (monomial.Length != _numIndeterminates)
            {
                throw new ArgumentOutOfRangeException(nameof(monomial), "Length of monomial index does not match the number of indeterminates.");
            }

            string hash = _hashFunction.Hash(monomial);
            if (_coefficients.ContainsKey(hash))
            {
                return _coefficients[hash];
            }
            return new T();
        }

        /// <summary>
        /// Returns all the terms of this polynomial as pairs of coefficient-monomial pairs.
        /// </summary>
        /// <returns>A list of tuples containing coefficient/monomial pairs.</returns>
        public List<Tuple<T, int[]>> GetTerms()
        {
            List<Tuple<T, int[]>> terms = new List<Tuple<T, int[]>>();
            foreach (KeyValuePair<string, T> term in _coefficients)
            {
                int[] buff = new int[_numIndeterminates];
                _hashFunction.Unhash(term.Key, buff);

                terms.Add(new Tuple<T, int[]>(term.Value, buff));
            }
            return terms;
        }

        /// <summary>
        /// Adds two multivariate polynomials together
        /// </summary>
        /// <param name="q"></param>
        /// <returns></returns>
        public MultivariatePolynomial<T> Add(MultivariatePolynomial<T> q)
        {
            if (q is null)
            {
                throw new ArgumentNullException(nameof(q));
            }

            MultivariatePolynomial<T> p = this;
            StandardizeIndices(ref p, ref q);
            return AddUnsafe(p, q, _provider);
        }
        /// <summary>
        /// Add, assuming that p and q have matching indices
        /// </summary>
        private static MultivariatePolynomial<T> AddUnsafe(MultivariatePolynomial<T> p, MultivariatePolynomial<T> q, IProvider<T> provider)
        {
            Dictionary<string, T> coefficients = new Dictionary<string, T>();
            foreach (KeyValuePair<string, T> pair in p._coefficients)
            {
                coefficients.Add(pair.Key, pair.Value);
            }
            foreach (KeyValuePair<string, T> pair in q._coefficients)
            {
                if (coefficients.ContainsKey(pair.Key))
                {
                    coefficients[pair.Key] = provider.Add(coefficients[pair.Key], pair.Value);
                }
                else
                {
                    coefficients.Add(pair.Key, pair.Value);
                }
            }
            return new MultivariatePolynomial<T>(p._indeterminates, coefficients);
        }

        /// <summary>
        /// Returns the negation of this polynomial, $-P(x)$.
        /// </summary>
        /// <returns>The opposite of this polynomial.</returns>
        public MultivariatePolynomial<T> AdditiveInverse()
        {
            Dictionary<string, T> coeffs = new Dictionary<string, T>();
            foreach (KeyValuePair<string, T> pair in _coefficients)
            {
                coeffs[pair.Key] = _provider.Negate(pair.Value);
            }
            return new MultivariatePolynomial<T>(_indeterminates.Copy(), coeffs);
        }

        /// <summary>
        /// Returns whether this polynomial is equal to another, within a relative error of <txt>eps</txt>.
        /// </summary>
        /// <param name="q">Another multivariate polynomial.</param>
        /// <param name="eps">The maximum tolerated relative error.</param>
        /// <returns></returns>
        public bool ApproximatelyEquals(MultivariatePolynomial<T> q, double eps)
        {
            MultivariatePolynomial<T> p = this;
            StandardizeIndices(ref p, ref q);
            return ApproximatelyEqualsUnsafe(p, q, eps, _provider);
        }
        // Assumes that indices match
        private static bool ApproximatelyEqualsUnsafe(MultivariatePolynomial<T> p, MultivariatePolynomial<T> q, double eps, IProvider<T> provider)
        {
            if (p._coefficients.Count != q._coefficients.Count)
            {
                return false;
            }

            T zero = provider.Zero;
            foreach (KeyValuePair<string, T> pair in p._coefficients)
            {
                bool p_zero = provider.Equals(pair.Value, zero);
                bool q_zero = !q._coefficients.ContainsKey(pair.Key) || provider.Equals(q._coefficients[pair.Key], zero);

                if (p_zero || q_zero)
                {
                    if (p_zero != q_zero)
                    {
                        return false;
                    }
                }
                else
                {
                    // p and q are guaranteed to not be zero
                    if (!provider.ApproximatelyEquals(q._coefficients[pair.Key], pair.Value, eps))
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        /// <summary>
        /// Returns whether this polynomial is equal to another exactly. Most suited for exact types.
        /// </summary>
        /// <param name="q">A multivariate polynomial.</param>
        /// <returns>Whether the two polynomials are exactly equal.</returns>
        public bool Equals(MultivariatePolynomial<T> q)
        {
            MultivariatePolynomial<T> p = this;
            StandardizeIndices(ref p, ref q);
            return EqualsUnsafe(p, q, _provider);
        }
        // Assume that all indices match
        private static bool EqualsUnsafe(MultivariatePolynomial<T> p, MultivariatePolynomial<T> q, IProvider<T> provider)
        {
            if (p._coefficients.Count != q._coefficients.Count)
            {
                return false;
            }

            T zero = provider.Zero;
            foreach (KeyValuePair<string, T> pair in p._coefficients)
            {
                bool p_zero = provider.Equals(pair.Value, zero);
                bool q_zero = !q._coefficients.ContainsKey(pair.Key) || provider.Equals(q._coefficients[pair.Key], zero);

                if (p_zero || q_zero)
                {
                    if (p_zero != q_zero)
                    {
                        return false;
                    }
                }
                else
                {
                    // p and q are guaranteed to not be zero
                    if (!provider.Equals(q._coefficients[pair.Key], pair.Value))
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        /// <summary>
        /// Returns the product of this polynomial with another.
        /// </summary>
        /// <param name="q"></param>
        /// <returns></returns>
        public MultivariatePolynomial<T> Multiply(MultivariatePolynomial<T> q)
        {
            MultivariatePolynomial<T> p = this;
            StandardizeIndices(ref p, ref q);
            return MultiplyUnsafe(p, q, _provider);
        }
        private static void StandardizeIndices(ref MultivariatePolynomial<T> p, ref MultivariatePolynomial<T> q)
        {
            string[] p_ind = p._indeterminates, q_ind = q._indeterminates;

            // Lucky case, if parameterization is exactly the same, no re-indexing
            if (p_ind.Equals(q_ind, (x, y) => x == y))
            {
                return;
            }

            // The union parameterization
            string[] ind = p_ind.Union(q_ind).ToArray();

            // Because of presumed uniqueness of p_ind and q_ind, it is sufficient to just 
            // check their lengths to establish exact equality.
            if (ind.Length == p_ind.Length)
            {
                // Leave the first unchanged
                q = new MultivariatePolynomial<T>(q, p_ind);
                return;
            }
            else if (ind.Length == q_ind.Length)
            {
                // Leave the second unchanged
                p = new MultivariatePolynomial<T>(p, q_ind);
                return;
            }

            // Requires both to be changed
            p = new MultivariatePolynomial<T>(p, ind);
            q = new MultivariatePolynomial<T>(q, ind);
        }
        private static MultivariatePolynomial<T> MultiplyUnsafe(MultivariatePolynomial<T> p, MultivariatePolynomial<T> q, IProvider<T> provider)
        {
            // O(n^2) algorithm used here can be improved

            // Spend O(n) time converting into int[] indexes
            Dictionary<int[], T> pCoeffs = new Dictionary<int[], T>(p._coefficients.Count),
                qCoeffs = new Dictionary<int[], T>(q._coefficients.Count);

            foreach (KeyValuePair<string, T> c in p._coefficients)
            {
                int[] index = new int[p._numIndeterminates];
                p._hashFunction.Unhash(c.Key, index);
                pCoeffs.Add(index, c.Value);
            }

            foreach (KeyValuePair<string, T> c in q._coefficients)
            {
                int[] index = new int[q._numIndeterminates];
                q._hashFunction.Unhash(c.Key, index);
                qCoeffs.Add(index, c.Value);
            }

            // O(n^2) time multiplying the indexes together
            Dictionary<string, T> prodCoeffs = new Dictionary<string, T>();

            int dim = p._numIndeterminates;
            int[] index_buff = new int[dim];
            foreach (KeyValuePair<int[], T> pc in pCoeffs)
            {
                foreach (KeyValuePair<int[], T> qc in qCoeffs)
                {
                    // Calculate the index
                    for (int i = 0; i < dim; ++i)
                    {
                        index_buff[i] = pc.Key[i] + qc.Key[i];
                    }

                    T prod = provider.Multiply(pc.Value, qc.Value);
                    string hash = p._hashFunction.Hash(index_buff);
                    if (prodCoeffs.ContainsKey(hash))
                    {
                        prodCoeffs[hash] = provider.Add(prod, prodCoeffs[hash]);
                    }
                    else
                    {
                        prodCoeffs[hash] = prod;
                    }
                }
            }

            // Remove all zero entries
            T zero = provider.Zero;
            List<string> keys = prodCoeffs.Keys.ToList();
            foreach (string hash in keys)
            {
                if (provider.Equals(zero, prodCoeffs[hash]))
                {
                    prodCoeffs.Remove(hash);
                }
            }
            return new MultivariatePolynomial<T>(p._indeterminates.Copy(), prodCoeffs);
        }

        /// <summary>
        /// Substitute some of the indeterminates with their substituted values
        /// </summary>
        /// <param name="substitutions"></param>
        /// <returns></returns>
        public MultivariatePolynomial<T> Substitute(Dictionary<string, T> substitutions)
        {
            if (substitutions is null)
            {
                throw new ArgumentNullException(nameof(substitutions));
            }

            int i = 0, j;
            string[] variables = new string[substitutions.Count];
            int[] lookupTable = new int[substitutions.Count];
            foreach (KeyValuePair<string, T> pair in substitutions)
            {
                variables[i] = pair.Key;
                lookupTable[i] = IndexOf(pair.Key);
                ++i;
            }

            // table[old index] = new index... or -1 if it is a substituted variable...
            int[] indexConversionTable = new int[_numIndeterminates];
            for (i = 0, j = 0; i < _numIndeterminates; ++i)
            {
                indexConversionTable[i] = substitutions.ContainsKey(_indeterminates[i]) ? -1 : j++;
            }

            int nSubstitutions = lookupTable.Count(i => i >= 0);
            if (nSubstitutions == 0)
            {
                return new MultivariatePolynomial<T>(this);
            }

            // The number of variables remaining after the substitution 
            int count = _numIndeterminates - nSubstitutions;
            int[] old_buff = new int[_numIndeterminates];
            int[] new_buff = new int[count];
            Dictionary<string, T> new_coeffs = new Dictionary<string, T>();
            ArrayHash<int> new_hash_fn = new ArrayHash<int>(count, sizeof(int));

            foreach (KeyValuePair<string, T> term in _coefficients)
            {
                _hashFunction.Unhash(term.Key, old_buff);
                T coeff = term.Value;

                for (i = 0; i < variables.Length; ++i)
                {
                    int index = lookupTable[i];
                    if (index >= 0)
                    {
                        T bse = substitutions[variables[i]];
                        coeff = _provider.Multiply(coeff, _provider.Pow(bse, old_buff[index]));
                    }
                }

                // Copy old index into new index
                for (i = 0, j = 0; i < _numIndeterminates; ++i)
                {
                    int new_index = indexConversionTable[i];
                    if (new_index >= 0)
                    {
                        new_buff[new_index] = old_buff[i];
                    }
                }

                // Increment the monomial by coeff.
                string new_hash = new_hash_fn.Hash(new_buff);
                if (new_coeffs.ContainsKey(new_hash))
                {
                    new_coeffs[new_hash] = _provider.Add(new_coeffs[new_hash], coeff);
                }
                else
                {
                    new_coeffs[new_hash] = coeff;
                }
            }

            // Calculate the new indeterminates
            string[] new_indeterminates = new string[count];
            for (i = 0, j = 0; i < _numIndeterminates; ++i)
            {
                if (!substitutions.ContainsKey(_indeterminates[i]))
                {
                    new_indeterminates[j++] = _indeterminates[i];
                }
            }
            return new MultivariatePolynomial<T>(new_indeterminates, new_coeffs);
        }
        private int IndexOf(string variable)
        {
            for (int i = 0; i < _indeterminates.Length; ++i)
            {
                if (_indeterminates[i] == variable)
                {
                    return i;
                }
            }
            return -1;
        }

        /// <summary>
        /// Returns the partial derivative of this polynomial with respect to the specified variable.
        /// </summary>
        /// <param name="variable"></param>
        /// <returns></returns>
        public MultivariatePolynomial<T> Differentiate(string variable)
        {
            if (!DependsOn(variable))
            {
                return AdditiveIdentity; // 0
            }

            int var_index = IndexOf(variable);
            int[] buff = new int[_numIndeterminates];

            Dictionary<string, T> new_coeffs = new Dictionary<string, T>(); 
            foreach (KeyValuePair<string, T> pair in _coefficients)
            {
                _hashFunction.Unhash(pair.Key, buff);

                // Decrement the old index by 1
                int power = buff[var_index];
                if (power == 0) continue; // constant term, disappears after differentiation

                buff[var_index]--;

                string hash = _hashFunction.Hash(buff);
                T new_coeff = _provider.MultiplyInteger(pair.Value, power);

                // Increment
                if (new_coeffs.ContainsKey(hash))
                {
                    new_coeffs[hash] = _provider.Add(new_coeffs[hash], new_coeff);
                }
                else
                {
                    new_coeffs[hash] = new_coeff;
                }
            }

            MultivariatePolynomial<T> derivative = new MultivariatePolynomial<T>(_indeterminates.Copy(), new_coeffs);

            // Simplify dependencies
            if (!derivative.DependsOn(variable))
            {
                return new MultivariatePolynomial<T>(derivative, _indeterminates.RemoveAt(var_index));
            }

            return derivative;
        }

        /// <summary>
        /// Returns the integral with respect to the specified variable, and the specified constant term
        /// </summary>
        /// <param name="variable">The variable to integrate with respect to.</param>
        /// <param name="constant">The constant of integration.</param>
        /// <returns>The integral</returns>
        public MultivariatePolynomial<T> Integrate(string variable, T constant)
        {
            if (string.IsNullOrEmpty(variable))
            {
                throw new ArgumentNullException(nameof(variable));
            }

            int index = IndexOf(variable);

            // Case 1: variable is not on current list of indeterminates
            if (index < 0)
            {
                string[] new_indeterminates = _indeterminates.InsertAt(_numIndeterminates, variable);
                int[] new_buff = new int[_numIndeterminates + 1];
                int[] old_buff = new int[_numIndeterminates];

                Dictionary<string, T> new_coeffs = new Dictionary<string, T>();
                ArrayHash<int> new_hash = new ArrayHash<int>(_numIndeterminates + 1, sizeof(int));

                foreach (KeyValuePair<string, T> pair in _coefficients)
                {
                    _hashFunction.Unhash(pair.Key, old_buff);
                    Array.Copy(old_buff, 0, new_buff, 0, _numIndeterminates);
                    new_buff[_numIndeterminates] = 1; // c -> cx

                    string hash = new_hash.Hash(new_buff);
                    new_coeffs.Add(hash, pair.Value);
                }

                // Add the costant term if not 0
                if (!_provider.Equals(constant, _provider.Zero))
                {
                    Array.Clear(new_buff, 0, new_buff.Length);
                    new_coeffs[new_hash.Hash(new_buff)] = constant;
                }
                return new MultivariatePolynomial<T>(new_indeterminates, new_coeffs);
            }

            // Case 2: variable is already included in list of indeterminates, no need to re-index
            else
            {
                Dictionary<string, T> new_coeffs = new Dictionary<string, T>();
                int[] buff = new int[_numIndeterminates];
                foreach (KeyValuePair<string, T> pair in _coefficients)
                {
                    _hashFunction.Unhash(pair.Key, buff);
                    buff[index]++; // increase power
                    T coeff = _provider.Divide(pair.Value, _provider.FromInt(buff[index])); // divide by new power
                    new_coeffs[_hashFunction.Hash(buff)] = coeff;
                }

                // Add the costant term if not 0
                if (!_provider.Equals(constant, _provider.Zero))
                {
                    Array.Clear(buff, 0, buff.Length);
                    new_coeffs[_hashFunction.Hash(buff)] = constant;
                }
                return new MultivariatePolynomial<T>(_indeterminates.Copy(), new_coeffs);
            }
        }

        /// <summary>
        /// Returns whether this polynomial is dependent on the specified variable.
        /// </summary>
        /// <param name="variable"></param>
        /// <returns></returns>
        public bool DependsOn(string variable)
        {
            if (string.IsNullOrEmpty(variable))
            {
                return false;
            }

            int index = IndexOf(variable);
            if (index < 0)
            {
                return false;
            }

            // Iterate through all the coefficients and check to see 
            // if there are any non-zero powers of that variable
            int[] buff = new int[_numIndeterminates];
            T zero = _provider.Zero;
            foreach (KeyValuePair<string, T> pair in _coefficients)
            {
                _hashFunction.Unhash(pair.Key, buff);

                // If power > 0 and coefficient is not 0, then there is a dependency
                if (buff[index] > 0 && !_provider.Equals(zero, pair.Value))
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Returns the multidegree of this polynomial, $\operatorname{mdeg} p$, the largest
        /// exponent under lexicographical monomial ordering.
        /// </summary>
        /// <returns></returns>
        public int[] Multidegree()
        {
            if (IsConstant)
            {
                if (_provider.Equals(ConstantValue, _provider.Zero))
                {
                    // Negative infinity
                    return RectangularVector.Repeat(int.MinValue, _numIndeterminates);
                }

                // Zero
                return new int[_numIndeterminates];
            }

            // Initialize to a random term's exponent
            string bestHash = _coefficients.Keys.First();
            foreach (string hash in _coefficients.Keys)
            {
                if (_hashFunction.CompareAsInt32(bestHash, hash) < 0)
                {
                    bestHash = hash;
                }
            }

            int[] monomial = new int[_numIndeterminates];
            _hashFunction.Unhash(bestHash, monomial);
            return monomial;
        }
        /// <summary>
        /// Returns the multidegree of this polynomial under the specified monomial ordering
        /// </summary>
        /// <param name="monomialOrder"></param>
        /// <returns></returns>
        public int[] Multidegree(IComparer<int[]> monomialOrder)
        {
            if (monomialOrder is null)
            {
                throw new ArgumentNullException(nameof(monomialOrder));
            }

            if (IsConstant)
            {
                if (_provider.Equals(ConstantValue, _provider.Zero))
                {
                    // Negative infinity
                    return RectangularVector.Repeat(int.MinValue, _numIndeterminates);
                }

                // Zero
                return new int[_numIndeterminates];
            }

            int[] leading = new int[_numIndeterminates];
            int[] buff = new int[_numIndeterminates];

            // populate with an arbitrary term
            _hashFunction.Unhash(_coefficients.Keys.First(), leading); 
            foreach (string hash in _coefficients.Keys)
            {
                _hashFunction.Unhash(hash, buff);
                if (monomialOrder.Compare(leading, buff) < 0)
                {
                    Array.Copy(buff, 0, leading, 0, _numIndeterminates);
                }
            }

            return leading;
        }

        /// <summary>
        /// Returns this polynomial raised to a non-negative integer power.
        /// </summary>
        /// <param name="power"></param>
        /// <returns></returns>
        public MultivariatePolynomial<T> Pow(int power)
        {
            if (power < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(power), ExceptionMessages.NegativeNotAllowed);
            }

            if (power == 0)
            {
                return MultiplicativeIdentity;
            }
            if (power == 1)
            {
                return Copy();
            }

            MultivariatePolynomial<T> sqrt = Pow(power / 2);
            if (power % 2 == 0)
            {
                return sqrt.Multiply(sqrt);
            }
            return sqrt.Multiply(sqrt).Multiply(this);
        }

        /// <summary>
        /// Creates a deep copy of this polynomial object.
        /// </summary>
        /// <returns>The deep copy.</returns>
        public MultivariatePolynomial<T> Copy()
        {
            return new MultivariatePolynomial<T>(this);
        }

        /// <summary>
        /// Creates a clone of this object.
        /// </summary>
        /// <returns></returns>
        public object Clone()
        {
            return Copy();
        }

        public override string ToString()
        {
            List<string> ordered = _coefficients.Keys.ToList();
            ordered.Sort((a, b) => -_hashFunction.CompareAsInt32(a, b));

            List<string> terms = new List<string>();
            int[] index = new int[_numIndeterminates];
            foreach (string hash in ordered)
            {
                _hashFunction.Unhash(hash, index);
                T coeff = _coefficients[hash];
                terms.Add(TermToString(coeff, index));
            }
            return string.Join(" + ", terms);
        }
        private string TermToString(T coeff, int[] index)
        {
            StringBuilder sb = new StringBuilder();

            // negative sign if coefficient is -1
            T minus_one = _provider.Negate(_provider.One);
            if (_provider.Equals(coeff, minus_one))
            {
                sb.Append("-");
            }

            // else - check if not 1, if so add the coefficient
            else if (!_provider.Equals(coeff, _provider.One))
            {
                sb.Append(coeff);
            }

            // Append all factors of the monomial
            bool zero = true;
            for (int i = 0; i < index.Length; ++i)
            {
                int ind = index[i];
                if (ind > 1)
                {
                    sb.Append(_indeterminates[i] + "^" + ind);
                    zero = false;
                }
                else if (ind == 1)
                {
                    sb.Append(_indeterminates[i]);
                    zero = false;
                }
            }

            // If all powers = 0, return 1 or -1
            if (zero)
            {
                if (_provider.Equals(coeff, minus_one))
                {
                    return "-" + _provider.One.ToString();
                }
                else
                {
                    return _provider.One.ToString();
                }
            }
            return sb.ToString();
        }

    }
}
