﻿using LinearNet.Structs.Fields;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Text.RegularExpressions;

namespace LinearNet.Structs
{
    /// <summary>
    /// Static methods for MultivariatePolynomial
    /// </summary>
    public static class MultivariatePolynomial
    {
        public static MultivariatePolynomial<BigRational> ParseBigRational(string s)
        {
            return new MultivariatePolynomialParser().Parse(s);
        }
        /*
        public static MultivariatePolynomial<BigRational> ParseBigRational(string s)
        {
            // Return the zero multivariate polynomial if string is empty
            if (string.IsNullOrEmpty(s))
            {
                return new MultivariatePolynomial<BigRational>();
            }

            // Multivariate polynomials over the rationals have limited expressivity:
            // The only allowed symbols are [0-9], [a-zA-Z], +, *, ^ and / (for fractions)
            if (!Regex.IsMatch(s, "[0-9a-zA-Z\\+\\-\\*\\^\\/\\s]*"))
            {
                throw new ArgumentOutOfRangeException("Invalid characters in string");
            }

            // Get all the indeterminates 
            string[] indeterminates = GetAllIndeterminates(s);

            // Split by addition or subtraction
            List<string> terms = SplitByAddSubtract(s);

            List<Tuple<int[], BigRational>> coeffs = new List<Tuple<int[], BigRational>>();
            List<string> list = indeterminates.ToList();
            foreach (string term in terms)
            {
                if (!TryParseTerm(term, list, out BigRational coeff, out int[] monomial))
                {
                    throw new FormatException($"Unparsable term: '{term}'");
                }
                coeffs.Add(new Tuple<int[], BigRational>(monomial, coeff));
            }

            return new MultivariatePolynomial<BigRational>(indeterminates, coeffs);
        }

        private static string[] GetAllIndeterminates(string s)
        {
            HashSet<string> variables = new HashSet<string>();

            MatchCollection matches = Regex.Matches(s, "[a-zA-Z]");
            foreach (Match match in matches)
            {
                if (!variables.Contains(match.Value))
                {
                    variables.Add(match.Value);
                }
            }

            string[] array = new string[variables.Count];
            int i = 0;
            foreach (string v in variables)
            {
                array[i++] = v;
            }
            return array;
        }

        // Split by + or -
        private static List<string> SplitByAddSubtract(string s)
        {
            s = s.Trim();

            List<string> summands = new List<string>();

            StringBuilder termBuilder = new StringBuilder();
            foreach (char c in s)
            {
                if (c == '+' || c == '-')
                {
                    if (termBuilder.Length > 0)
                    {
                        string term = termBuilder.ToString();
                        if (!string.IsNullOrWhiteSpace(term))
                        {
                            summands.Add(term.Trim());
                        }
                        termBuilder.Clear();
                    }
                }

                if (c != '+' && c != ' ')
                {
                    termBuilder.Append(c);
                }
            }

            // Collect up anything at the end
            if (termBuilder.Length > 0)
            {
                string term = termBuilder.ToString();
                if (!string.IsNullOrWhiteSpace(term))
                {
                    summands.Add(term.Trim());
                }
            }

            return summands;
        }

        private static bool TryParseTerm(string s, List<string> indeterminates, out BigRational coefficient, out int[] monomial)
        {
            coefficient = default;
            monomial = new int[indeterminates.Count];

            if (string.IsNullOrWhiteSpace(s))
            {
                return false;
            }

            // Remove all spaces
            s = Regex.Replace(s, "\\s+", "");

            // coefficient must appear at the start
            if (Regex.IsMatch(s, "^[\\-0-9]+\\/[0-9]+"))
            {
                // Parse coefficient as a rational p/q
                Match match = Regex.Match(s, "^[\\-0-9]+\\/[0-9]+");
                s = s.Substring(match.Value.Length);

                string[] parts = match.Value.Split('/');

                if (parts.Length != 2) return false;
                if (!BigInteger.TryParse(parts[0], out BigInteger num)) return false;
                if (!BigInteger.TryParse(parts[1], out BigInteger den)) return false;
                coefficient = new BigRational(num, den);
            }
            else if (Regex.IsMatch(s, "^[\\-0-9]+"))
            {
                // Parse coefficient as integer
                Match match = Regex.Match(s, "^[\\-0-9]+");
                s = s.Substring(match.Value.Length);

                if (!BigInteger.TryParse(match.Value, out BigInteger num)) return false;
                coefficient = new BigRational(num);
            }
            else
            {
                coefficient = BigRational.One;
            }

            while (s.Length > 0)
            {
                Match match = Regex.Match(s, "^[a-zA-Z]\\^[0-9]+");
                if (match.Success)
                {
                    s = s.Substring(match.Value.Length);

                    string[] parts = match.Value.Split('^');
                    if (parts.Length != 2) return false;

                    string variable = parts[0];
                    int index = indeterminates.IndexOf(variable);
                    if (index < 0) return false;

                    if (!int.TryParse(parts[1], out int power)) return false;
                    monomial[index] = power;
                }

                // Otherwise - check for singular powers e.g. x = x^1
                else
                {
                    match = Regex.Match(s, "^[a-zA-Z]");
                    if (match.Success)
                    {
                        string variable = match.Value;
                        s = s.Substring(variable.Length);

                        int index = indeterminates.IndexOf(variable);
                        if (index < 0) return false;

                        monomial[index] = 1;
                    }
                    else
                    {
                        // Unparsable term
                        return false;
                    }
                }
            }

            return true;
        }*/
    }
}
