﻿using LinearNet.Functions;
using LinearNet.Global;
using LinearNet.Helpers;
using LinearNet.Providers;
using LinearNet.Structs.Fields;
using LinearNet.Symbolic;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace LinearNet.Structs
{
    /// <summary>
    /// Implements a univariate sparse polynomial.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <cat>linear-algebra</cat>
    public class Polynomial<T> : IRing<Polynomial<T>>, ICloneable where T : new()
    {
        private readonly IProvider<T> _provider;
        private readonly SortedList<long, T> _coefficients; // key = exponent, value = coefficient
        
        // Fields used for establishing equality - the hashcode is cached 
        // the first time equality is checked, and used from then on. If 
        // the polynomial is modified (e.g. via in-place methods), _hashCodeValid
        // will be set to false and the hash code will need to be recalculated 
        // the next time equality is checked.
        private bool _hashCodeValid;
        private int _hashCode;

        internal IProvider<T> Provider { get { return _provider; } }
        internal SortedList<long, T> Coefficients { get { return _coefficients; } }

        #region Public properties

        public Polynomial<T> MultiplicativeIdentity => new Polynomial<T>(_provider.One);
        public Polynomial<T> AdditiveIdentity => new Polynomial<T>();

        /// <summary>
        /// Returns whether this polynomial is equal to zero.
        /// </summary>
        public bool IsZero
        {
            get
            {
                if (_coefficients.Count == 0)
                {
                    return true;
                }

                T zero = _provider.Zero;
                foreach (T coeff in _coefficients.Values)
                {
                    if (!_provider.Equals(zero, coeff))
                    {
                        return false;
                    }
                }
                return true;
            }
        }

        /// <summary>
        /// Returns whether this polynomial is a constant polynomial.
        /// </summary>
        public bool IsConstant
        {
            get
            {
                if (_coefficients.Count == 0)
                {
                    return true;
                }
                if (_coefficients.Count > 1)
                {
                    return false;
                }
                // Exactly 1 coefficient
                return _coefficients.ContainsKey(0L);
            }
        }

        /// <summary>
        /// Returns the value of this polynomial if it is a constant. Throws <txt>InvalidOperationException</txt>
        /// if the polynomial is not constant.
        /// </summary>
        public T ConstantValue
        {
            get
            {
                if (!IsConstant)
                {
                    throw new InvalidOperationException("Polynomial is not constant.");
                }

                if (_coefficients.Count == 0)
                {
                    return _provider.Zero;
                }

                return _coefficients[0L];
            }
        }

        /// <summary>
        /// Gets the degree of this polynomial
        /// </summary>
        /// <returns></returns>
        public long Degree
        {
            get
            {
                int n = _coefficients.Count;
                if (n == 0)
                {
                    return long.MinValue;
                }
                return _coefficients.Keys[n - 1];
            }
        }

        /// <summary>
        /// Returns the leading coefficient of this polynomial
        /// </summary>
        /// <returns></returns>
        public T LeadingCoefficient
        {
            get
            {
                int n = _coefficients.Count;
                if (n == 0)
                {
                    return _provider.Zero;
                }
                return _coefficients.Values[n - 1];
            }
        }

        /// <summary>
        /// Returns whether this polynomial is a monic (i.e. having a leading coefficient 
        /// of 1).
        /// </summary>
        public bool IsMonic
        {
            get
            {
                return _provider.Equals(LeadingCoefficient, _provider.One);
            }
        }

        /// <summary>
        /// Returns the number of terms of this polynomial.
        /// </summary>
        public int Count
        {
            get
            {
                return _coefficients.Count;
            }
        }
        #endregion


        #region Constructors

        /// <summary>
        /// Default constructor for a zero polynomial.
        /// </summary>
        public Polynomial()
        {
            if (!ProviderFactory.TryGetDefaultProvider(out IProvider<T> provider))
            {
                throw new NotSupportedException(
                    $"The type '{ typeof(T) }' does not have a default provider. Please provide an explicit provider via the constructor.");
            }
            _provider = provider;
            _coefficients = new SortedList<long, T>();
            _hashCodeValid = false;
        }

        /// <summary>
        /// Create a new constant polynomial
        /// </summary>
        /// <param name="constant"></param>
        public Polynomial(T constant) : this()
        {
            _coefficients.Add(0, constant);
        }

        /// <summary>
        /// Create a polynomial given its coefficients as a dense array.
        /// </summary>
        /// <param name="coefficients"></param>
        public Polynomial(params T[] coefficients) : this()
        {
            if (coefficients is null)
            {
                throw new ArgumentNullException(nameof(coefficients));
            }

            int i, j = 0;
            for (i = coefficients.Length - 1; i >= 0; --i)
            {
                // Since collection is already ordered, addition to SortedList is O(1)
                // so long as resize is not required.
                _coefficients.Add(j++, coefficients[i]);
            }
        }

        /// <summary>
        /// Create a polynomial given its coefficients as a dictionary
        /// </summary>
        /// <param name="coefficients"></param>
        public Polynomial(Dictionary<long, T> coefficients) : this()
        {
            if (coefficients is null)
            {
                throw new ArgumentNullException(nameof(coefficients));
            }

            // Shallow copy, suited for struct-like T.
            _coefficients = new SortedList<long, T>(coefficients);
        }
        
        /// <summary>
        /// Create a polynomial given a dense polynomial object.
        /// </summary>
        /// <param name="polynomial"></param>
        public Polynomial(DensePolynomial<T> polynomial) : this()
        {
            if (polynomial is null)
            {
                throw new ArgumentNullException(nameof(polynomial));
            }

            // coeffs are arranged from highest order to lowest order
            // hence requires a reversal 
            T[] coeffs = polynomial.Coefficients;

            // SortedList is O(1) if we are inserting in natural order
            for (int i = coeffs.Length - 1, j = 0; i >= 0; --i)
            {
                _coefficients[j++] = coeffs[i];
            }
        }

        /// <summary>
        /// Copy constructor
        /// </summary>
        /// <param name="polynomial"></param>
        public Polynomial(Polynomial<T> polynomial) : this()
        {
            if (polynomial is null)
            {
                throw new ArgumentNullException(nameof(polynomial));
            }

            // Shallow copy
            _coefficients = new SortedList<long, T>(polynomial._coefficients);

            // Copy hashcode - saves computation later.
            if (polynomial._hashCodeValid)
            {
                _hashCode = polynomial._hashCode;
                _hashCodeValid = true;
            }
        }

        #endregion


        /// <summary>
        /// Returns the coefficient of the monomial of the specified power.
        /// </summary>
        /// <param name="power"></param>
        /// <returns></returns>
        public T GetCoefficient(long power)
        {
            if (_coefficients.ContainsKey(power))
            {
                return _coefficients[power];
            }
            return _provider.Zero;
        }

        private void RecalculateHashCode()
        {
            _hashCode = GetHashCode();
            _hashCodeValid = true;
        }
        public override int GetHashCode()
        {
            int hash = 7;
            foreach (KeyValuePair<long, T> pair in _coefficients)
            {
                hash += ((int)(pair.Key % int.MaxValue) * 37) + pair.Value.GetHashCode();
            }
            return hash;
        }
        public override bool Equals(object obj)
        {
            if (obj is null)
            {
                return false;
            }
            return Equals(obj as Polynomial<T>);
        }

        public override string ToString()
        {
            return ToString("x");
        }
        public string ToString(string variableName)
        {
            if (_coefficients.Count == 0)
            {
                return "0";
            }

            List<long> powers = _coefficients.Keys.ToList();
            List<string> terms = new List<string>();
            foreach (long power in powers)
            {
                if (_provider.Equals(_provider.Zero, _coefficients[power]))
                {
                    continue;
                }

                string term = "";
                if (!_provider.Equals(_provider.One, _coefficients[power]))
                {
                    term += _coefficients[power].ToString();
                }
                
                if (power > 1)
                {
                    terms.Add(term + variableName + "^" + power);
                }
                else if (power == 1)
                {
                    terms.Add(term + variableName);
                }
                else if (power == 0)
                {
                    if (string.IsNullOrEmpty(term))
                    {
                        terms.Add("1");
                    }
                    else
                    {
                        terms.Add(term);
                    }
                }
            }

            if (terms.Count == 0)
            {
                return "0";
            }
            return string.Join(" + ", terms);
        }

        public Polynomial<T> Copy()
        {
            return new Polynomial<T>(this);
        }
        public object Clone()
        {
            return Copy();
        }

        /// <summary>
        /// Returns this polynomial evaluated 
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public T Evaluate(T x)
        {
            T sum = _provider.Zero;
            T pow = _provider.One;
            long curr_power = 0;

            foreach (KeyValuePair<long, T> pair in _coefficients)
            {
                pow = _provider.Multiply(pow, _provider.Pow(x, pair.Key - curr_power));
                sum = _provider.Add(sum, _provider.Multiply(pair.Value, pow));
                curr_power = pair.Key;
            }
            return sum;
        }

        /// <summary>
        /// Returns this polynomial raised to a non-negative 64-bit integer power, $p(x)^n$.
        /// </summary>
        /// <param name="power">The power to raise this polynomial to.</param>
        /// <returns>This polynomial to the specified power.</returns>
        public Polynomial<T> Pow(long power)
        {
            if (power < 0L)
            {
                throw new ArgumentOutOfRangeException(nameof(power), ExceptionMessages.NegativeNotAllowed);
            }
            if (power == 0L)
            {
                return MultiplicativeIdentity;
            }
            return PowInner(this, power);
        }
        private static Polynomial<T> PowInner(Polynomial<T> p, long power)
        {
            if (power == 1L)
            {
                return p.Copy();
            }

            Polynomial<T> sqrt = PowInner(p, power / 2L);
            if (power % 2L == 0)
            {
                return sqrt.Multiply(sqrt);
            }
            return sqrt.Multiply(sqrt).Multiply(p);
        }
        
        /// <summary>
        /// Divide this polynomial by another polynomial, returning the result and the remainder.
        /// </summary>
        /// <param name="divisor">The polynomial to divide by.</param>
        /// <param name="remainder">
        /// The remainder of the polynomial. Guaranteed to have degree less than
        /// the degree of <txt>divisor</txt>.
        /// </param>
        /// <returns>The result of the polynomial division.</returns>
        public Polynomial<T> Divide(Polynomial<T> divisor, out Polynomial<T> remainder)
        {
            if (divisor is null)
            {
                throw new ArgumentNullException(nameof(divisor));
            }

            if (divisor.IsZero)
            {
                throw new DivideByZeroException(nameof(divisor));
            }

            // Sorted in ascending order
            SortedList<long, T> num = new SortedList<long, T>(_coefficients);
            SortedList<long, T> den = divisor._coefficients; // never modified - no need to shallow copy

            Dictionary<long, T> result = new Dictionary<long, T>();
            T zero = _provider.Zero;

            long lt_den = den.Keys.Last();
            T lt_den_coeff = den[lt_den];

            // While leading term of divisor <= divisee,
            while (num.Count > 0 && num.Keys[num.Count - 1] >= lt_den)
            {
                long lt_num = num.Keys[num.Count - 1];
                T lt_num_coeff = num[lt_num];

                // lt(n) = 0, skip
                if (_provider.Equals(lt_num_coeff, zero))
                {
                    num.RemoveAt(num.Count - 1);
                    continue;
                }

                // Calculate [lt(n) / lt(d)]
                T coeff = _provider.Divide(lt_num_coeff, lt_den_coeff);
                long power = lt_num - lt_den;
                num.RemoveAt(num.Count - 1);

                // n(x) = n(x) - [lt(n) / lt(d)] d(x)
                foreach (KeyValuePair<long, T> pair in den)
                {
                    // Skip the last term of den, since we have already removed it from num
                    if (pair.Key == lt_den) continue;

                    long p = pair.Key + power;
                    T c = _provider.Multiply(coeff, pair.Value);

                    if (num.ContainsKey(p))
                    {
                        num[p] = _provider.Add(num[p], _provider.Negate(c));
                    }
                    else
                    {
                        num.Add(p, _provider.Negate(c));
                    }
                }

                // Divide 
                result.Add(power, coeff);
            }

            remainder = new Polynomial<T>(new Dictionary<long, T>(num));
            return new Polynomial<T>(result);
        }
        public Polynomial<T> Divide(Polynomial<T> divisor)
        {
            if (divisor is null)
            {
                throw new ArgumentNullException(nameof(divisor));
            }

            if (divisor.IsZero)
            {
                throw new DivideByZeroException(nameof(divisor));
            }

            // Sorted in ascending order
            SortedList<long, T> num = new SortedList<long, T>(_coefficients);
            SortedList<long, T> den = divisor._coefficients; // never modified - no need to shallow copy

            Dictionary<long, T> result = new Dictionary<long, T>();
            T zero = _provider.Zero;

            long lt_den = den.Keys.Last();
            T lt_den_coeff = den[lt_den];

            // While leading term of divisor <= divisee,
            while (num.Count > 0 && num.Keys[num.Count - 1] >= lt_den)
            {
                long lt_num = num.Keys[num.Count - 1];
                T lt_num_coeff = num[lt_num];

                // lt(n) = 0, skip
                if (_provider.Equals(lt_num_coeff, zero))
                {
                    num.RemoveAt(num.Count - 1);
                    continue;
                }

                // Calculate [lt(n) / lt(d)]
                T coeff = _provider.Divide(lt_num_coeff, lt_den_coeff);
                long power = lt_num - lt_den;
                num.RemoveAt(num.Count - 1);

                // n(x) = n(x) - [lt(n) / lt(d)] d(x)
                foreach (KeyValuePair<long, T> pair in den)
                {
                    // Skip the last term of den, since we have already removed it from num
                    if (pair.Key == lt_den) continue;

                    long p = pair.Key + power;
                    T c = _provider.Multiply(coeff, pair.Value);

                    if (num.ContainsKey(p))
                    {
                        num[p] = _provider.Add(num[p], _provider.Negate(c));
                    }
                    else
                    {
                        num.Add(p, _provider.Negate(c));
                    }
                }

                // Divide 
                result.Add(power, coeff);
            }
            return new Polynomial<T>(result);
        }

        /// <summary>
        /// Returns the remainder upon division by another polynomial
        /// </summary>
        /// <param name="divisor"></param>
        /// <returns></returns>
        public Polynomial<T> Mod(Polynomial<T> divisor)
        {
            // Currently, implementation is very similar to the divide method...
            // except it doesnt keep track of the quotient
            // See if these two methods can be combined for easier maintainability

            if (divisor is null)
            {
                throw new ArgumentNullException(nameof(divisor));
            }
            if (divisor.IsZero)
            {
                throw new DivideByZeroException(nameof(divisor));
            }

            // Sorted in ascending order
            SortedList<long, T> num = new SortedList<long, T>(_coefficients);
            SortedList<long, T> den = divisor._coefficients; // never modified - no need to shallow copy

            T zero = _provider.Zero;

            long lt_den = den.Keys.Last();
            T lt_den_coeff = den[lt_den];

            // While leading term of divisor <= divisee,
            while (num.Count > 0 && num.Keys[num.Count - 1] >= lt_den)
            {
                long lt_num = num.Keys[num.Count - 1];
                T lt_num_coeff = num[lt_num];

                // lt(n) = 0, skip
                if (_provider.Equals(lt_num_coeff, zero))
                {
                    num.RemoveAt(num.Count - 1);
                    continue;
                }

                // Calculate [lt(n) / lt(d)]
                T coeff = _provider.Divide(lt_num_coeff, lt_den_coeff);
                long power = lt_num - lt_den;
                num.RemoveAt(num.Count - 1);

                // n(x) = n(x) - [lt(n) / lt(d)] d(x)
                foreach (KeyValuePair<long, T> pair in den)
                {
                    // Skip the last term of den, since we have already removed it from num
                    if (pair.Key == lt_den) continue;

                    long p = pair.Key + power;
                    T c = _provider.Multiply(coeff, pair.Value);
                    if (num.ContainsKey(p))
                    {
                        num[p] = _provider.Add(num[p], _provider.Negate(c));
                    }
                    else
                    {
                        num.Add(p, _provider.Negate(c));
                    }
                }
            }
            return new Polynomial<T>(new Dictionary<long, T>(num));
        }

        /// <summary>
        /// Returns the composition of this polynomial $p$ with another polynomial $q$, i.e. $p(q(x))$.
        /// </summary>
        /// <param name="q">A polynomial.</param>
        /// <returns>The composition of the two polynomials.</returns>
        public Polynomial<T> Of(Polynomial<T> q)
        {
            if (q is null)
            {
                throw new ArgumentNullException(nameof(q));
            }

            Polynomial<T> comp = new Polynomial<T>();

            // Substitute q into each term of p.
            foreach (KeyValuePair<long, T> term in _coefficients)
            {
                comp = comp.Add(q.Pow(term.Key) * term.Value);
            }
            return comp;
        }


        /// <summary>
        /// Returns whether this polynomial is square-free.
        /// </summary>
        /// <returns></returns>
        public bool IsSquareFree()
        {
            return Polynomial.GCDInner(this, Differentiate()).Degree > 0;
        }

        #region Ring implementations

        public Polynomial<T> Add(Polynomial<T> g)
        {
            // Sparse vector-like addition - move to a sparse BLAS?
            if (g is null)
            {
                throw new ArgumentNullException(nameof(g));
            }

            Dictionary<long, T> coefficients = new Dictionary<long, T>(_coefficients);
            T zero = _provider.Zero;

            foreach (KeyValuePair<long, T> coeff in g._coefficients)
            {
                if (coefficients.ContainsKey(coeff.Key))
                {
                    T val = _provider.Add(coefficients[coeff.Key], coeff.Value);
                    if (_provider.Equals(val, zero))
                    {
                        coefficients.Remove(coeff.Key);
                    }
                    else
                    {
                        coefficients[coeff.Key] = val;
                    }
                }
                else
                {
                    coefficients[coeff.Key] = coeff.Value;
                }
            }
            return new Polynomial<T>(coefficients);
        }

        public Polynomial<T> AdditiveInverse()
        {
            Dictionary<long, T> coefficients = new Dictionary<long, T>();
            foreach (KeyValuePair<long, T> c in _coefficients)
            {
                coefficients.Add(c.Key, _provider.Negate(c.Value));
            }
            return new Polynomial<T>(coefficients);
        }

        public bool ApproximatelyEquals(Polynomial<T> e, double eps)
        {
            if (e is null)
            {
                return false;
            }

            if (eps < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(eps), ExceptionMessages.NegativeNotAllowed);
            }

            // Use hash codes for equality comparison whenever possible
            if (_hashCodeValid && e._hashCodeValid)
            {
                if (_hashCode != e._hashCode)
                {
                    return false;
                }
            }

            // Simplify before equality check
            if (CanBeSimplified()) Simplify();
            if (e.CanBeSimplified()) e.Simplify();

            // Recalculate hash codes if any are not available
            if (!_hashCodeValid) RecalculateHashCode();
            if (!e._hashCodeValid) e.RecalculateHashCode();

            if (_coefficients.Count != e._coefficients.Count)
            {
                return false;
            }

            // Since counts are the same, we just need to check for 1-sided membership
            foreach (KeyValuePair<long, T> c in _coefficients)
            {
                if (!e._coefficients.ContainsKey(c.Key) || 
                    !_provider.ApproximatelyEquals(e._coefficients[c.Key], c.Value, eps))
                {
                    return false;
                }
            }
            return true;
        }

        public bool Equals(Polynomial<T> e)
        {
            if (e is null)
            {
                return false;
            }

            // Use hash codes for equality comparison whenever possible
            if (_hashCodeValid && e._hashCodeValid)
            {
                if (_hashCode != e._hashCode)
                {
                    return false;
                }
            }

            // Simplify before equality check
            if (CanBeSimplified()) Simplify();
            if (e.CanBeSimplified()) e.Simplify();

            // Recalculate hash codes if any are not available
            if (!_hashCodeValid) RecalculateHashCode();
            if (!e._hashCodeValid) e.RecalculateHashCode();

            // Free of zeroes - count check is sufficient
            if (_coefficients.Count != e._coefficients.Count)
            {
                return false;
            }

            // Since counts are the same, we just need to check for 1-sided membership
            foreach (KeyValuePair<long, T> c in _coefficients)
            {
                if (!e._coefficients.ContainsKey(c.Key) ||
                    !_provider.Equals(e._coefficients[c.Key], c.Value))
                {
                    return false;
                }
            }
            return true;
        }

        private bool CanBeSimplified()
        {
            T zero = _provider.Zero;
            foreach (T coeff in _coefficients.Values)
            {
                if (_provider.Equals(coeff, zero))
                {
                    return true;
                }
            }
            return true;
        }
        private void Simplify()
        {
            T zero = _provider.Zero;
            for (int i = _coefficients.Count - 1; i >= 0; --i)
            {
                if (_provider.Equals(_coefficients.Values[i], zero))
                {
                    _coefficients.RemoveAt(i);
                }
            }
        }

        public Polynomial<T> Multiply(Polynomial<T> b)
        {
            // O(n^2) !! Make this better!

            Dictionary<long, T> product = new Dictionary<long, T>();
            T zero = _provider.Zero;
            foreach (KeyValuePair<long, T> c in _coefficients)
            {
                long power1 = c.Key;
                T coeff1 = c.Value;

                foreach (KeyValuePair<long, T> c2 in b._coefficients)
                {
                    T prod = _provider.Multiply(coeff1, c2.Value);
                    long power = power1 + c2.Key;

                    if (product.ContainsKey(power))
                    {
                        T sum = _provider.Add(product[power], prod);
                        if (_provider.Equals(sum, zero))
                        {
                            product.Remove(power);
                        }
                        else
                        {
                            product[power] = sum;
                        }
                    }
                    else
                    {
                        product[power] = prod;
                    }
                }
            }

            return new Polynomial<T>(product);
        }

        /// <summary>
        /// Multiply this polynomial by a scalar, returning a new polynomial object.
        /// </summary>
        /// <param name="scalar">The scalar to multiply by.</param>
        /// <returns>The multiplication result.</returns>
        public Polynomial<T> Multiply(T scalar)
        {
            Polynomial<T> copy = Copy();
            copy.MultiplyInPlace(scalar);
            return copy;
        }

        /// <summary>
        /// Divides this polynomial by a scalar, returning a new polynomial object.
        /// </summary>
        /// <param name="scalar">The scalar to divide by.</param>
        /// <returns>The division result</returns>
        public Polynomial<T> Divide(T scalar)
        {
            Polynomial<T> copy = Copy();
            copy.DivideInPlace(scalar);
            return copy;
        }

        #endregion

        #region In place methods - remember to invalid hash codes!

        /// <summary>
        /// Overwrite this polynomial with this polynomial multiplied by a scalar.
        /// </summary>
        /// <param name="scalar"></param>
        public void MultiplyInPlace(T scalar)
        {
            foreach (long power in _coefficients.Keys.ToList())
            {
                _coefficients[power] = _provider.Multiply(_coefficients[power], scalar);
            }
            _hashCodeValid = false;
        }
        /// <summary>
        /// Overwrite this polynomial with the polynomial divided by a scalar.
        /// </summary>
        /// <param name="scalar"></param>
        public void DivideInPlace(T scalar)
        {
            foreach (long power in _coefficients.Keys.ToList())
            {
                _coefficients[power] = _provider.Divide(_coefficients[power], scalar);
            }
            _hashCodeValid = false;
        }

        #endregion

        #region Calculus

        /// <summary>
        /// Calculate the first derivative of this polynomial, with respect to the indeterminate variable.
        /// </summary>
        /// <returns>The derivative.</returns>
        public Polynomial<T> Differentiate()
        {
            Dictionary<long, T> derivative = new Dictionary<long, T>();
            foreach (KeyValuePair<long, T> coeff in _coefficients)
            {
                if (coeff.Key > 0)
                {
                    derivative.Add(coeff.Key - 1, _provider.Multiply(coeff.Value, _provider.FromLong(coeff.Key)));
                }
            }
            return new Polynomial<T>(derivative);
        }
        /// <summary>
        /// Calculate the integral of this polynomial, with respect to the indeterminate variable, and using
        /// the specified constant of integration.
        /// </summary>
        /// <param name="constant">The constant of integration.</param>
        /// <returns>The integral.</returns>
        public Polynomial<T> Integrate(T constant)
        {
            Dictionary<long, T> integral = new Dictionary<long, T>();
            foreach (KeyValuePair<long, T> coeff in _coefficients)
            {
                long new_power = coeff.Key + 1;
                integral.Add(new_power, _provider.Divide(coeff.Value, _provider.FromLong(new_power)));
            }

            if (!_provider.Equals(constant, _provider.Zero))
            {
                integral[0L] = constant;
            }
            return new Polynomial<T>(integral);
        }

        #endregion


        #region Operators

        /// <summary>
        /// Returns the sum of two polynomials over the same type.
        /// </summary>
        /// <param name="p">The left polynomial.</param>
        /// <param name="q">The right polynomial.</param>
        /// <returns></returns>
        public static Polynomial<T> operator +(Polynomial<T> p, Polynomial<T> q) => p.Add(q);

        /// <summary>
        /// Returns the difference between two polynomials of the same type.
        /// </summary>
        /// <param name="p">The first polynomial.</param>
        /// <param name="q">The second polynomial.</param>
        /// <returns>The first polynomial subtract the second polynomial.</returns>
        public static Polynomial<T> operator -(Polynomial<T> p, Polynomial<T> q) => p.Subtract(q);

        /// <summary>
        /// Returns the opposite polynomial $-p(x)$.
        /// </summary>
        /// <param name="p">A polynomial.</param>
        /// <returns>The opposite polynomial</returns>
        public static Polynomial<T> operator -(Polynomial<T> p) => p.AdditiveInverse();

        /// <summary>
        /// Returns the product of two polynomials of the same type.
        /// </summary>
        /// <param name="p">The first polynomial.</param>
        /// <param name="q">The second polynomial.</param>
        /// <returns>The polynomial product.</returns>
        public static Polynomial<T> operator *(Polynomial<T> p, Polynomial<T> q) => p.Multiply(q);

        /// <summary>
        /// Returns the product between a polynomial and a scalar.
        /// </summary>
        /// <param name="p">The polynomial.</param>
        /// <param name="scalar">A scalar.</param>
        /// <returns>The polynomial-scalar product</returns>
        public static Polynomial<T> operator *(Polynomial<T> p, T scalar) => p.Multiply(scalar);

        /// <summary>
        /// Returns the product between a scalar and a polynomial.
        /// </summary>
        /// <param name="scalar">A scalar.</param>
        /// <param name="p">A polynomial</param>
        /// <returns>The polynomial-scalar product.</returns>
        public static Polynomial<T> operator *(T scalar, Polynomial<T> p) => p.Multiply(scalar);

        /// <summary>
        /// Returns the remainder upon dividing $p(x)$ by $q(x)$.
        /// </summary>
        /// <param name="p">The first polynomial.</param>
        /// <param name="q">The second polynomial.</param>
        /// <returns>The remainder polynomial.</returns>
        public static Polynomial<T> operator %(Polynomial<T> p, Polynomial<T> q) => p.Mod(q);

        /// <summary>
        /// Returns whether the two polynomials are exactly equal.
        /// </summary>
        /// <param name="p">The first polynomial.</param>
        /// <param name="q">The second polynomial.</param>
        /// <returns>Whether the two polynomials are exactly equal.</returns>
        public static bool operator ==(Polynomial<T> p, Polynomial<T> q) => p.Equals(q);

        /// <summary>
        /// Returns true if the two polynomials are not exactly equal, false otherwise.
        /// </summary>
        /// <param name="p">The first polynomial.</param>
        /// <param name="q">The second polynomial.</param>
        /// <returns>Whether the two polynomials are unequal.</returns>
        public static bool operator !=(Polynomial<T> p, Polynomial<T> q) => !p.Equals(q);

        #endregion

        #region Conversions and casts

        /// <summary>
        /// Convert this polynomial into a multivariate polynomial structure.
        /// </summary>
        /// <returns></returns>
        public MultivariatePolynomial<T> ToMultivariate()
        {
            return new MultivariatePolynomial<T>(this);
        }

        #endregion
    }

    /// <summary>
    /// Static methods for sparse polynomials.
    /// </summary>
    /// <cat>linear-algebra</cat>
    public static class Polynomial
    {
        /// <summary>
        /// <p>Parse a univariate polynomial over the <txt>BigRational</txt> field from a string.</p>
        /// <p>Throws <txt>FormatException</txt> if string is not in a valid format.</p>
        /// </summary>
        /// <param name="s">A valid string.</param>
        /// <returns>A polynomial object over <txt>BigRational</txt>.</returns>
        public static Polynomial<BigRational> ParseBigRational(string s)
        {
            return new SparsePolynomialParser().Parse(s);
        }

        /// <summary>
        /// Calculates the greatest common divisor (GCD) of two univariate polynomials 
        /// up to the constant factor, returning the GCD as a monic polynomial.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="p">The first polynomial.</param>
        /// <param name="q">The second polynomial.</param>
        /// <returns>The greatest common divisor (highest common factor) of the two polynomials.</returns>
        public static Polynomial<T> GCD<T>(Polynomial<T> p, Polynomial<T> q) where T : new()
        {
            Polynomial<T> gcd = GCDInner(p, q);

            if (gcd.IsConstant)
            {
                return gcd.MultiplicativeIdentity; // 1
            }

            // Ensure that the returned polynomial is a monic
            IProvider<T> provider = p.Provider;

            T lc = gcd.LeadingCoefficient;
            if (!provider.Equals(lc, provider.One))
            {
                return gcd.Divide(lc);
            }
            return gcd.Copy(); // ensure copy.
        }

        /// <summary>
        /// Returns the greatest common divisor of a collection of polynomials.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="polynomials">The collection of polynomials (must contain at least 1 polynomial).</param>
        /// <returns>The greatest common divisor of those polynomials.</returns>
        public static Polynomial<T> GCD<T>(params Polynomial<T>[] polynomials) where T : new()
        {
            if (polynomials is null)
            {
                throw new ArgumentNullException(nameof(polynomials));
            }

            if (polynomials.Length == 0)
            {
                throw new InvalidOperationException("Polynomial GCD requires at least 1 polynomial.");
            }

            if (polynomials.Length == 1)
            {
                return polynomials[0].Copy();
            }

            Polynomial<T> gcd = GCD(polynomials[0], polynomials[1]);
            for (int i = 2; i < polynomials.Length; ++i)
            {
                if (gcd.IsConstant)
                {
                    return gcd;
                }
                gcd = GCD(gcd, polynomials[i]);
            }
            return gcd;
        }

        /// <summary>
        /// Computes the GCD 
        /// - without ensuring that the leading monomial has positive coefficient
        /// - without ensuring that the polynomials are copied.
        /// </summary>
        internal static Polynomial<T> GCDInner<T>(Polynomial<T> p, Polynomial<T> q) where T : new()
        {
            if (p.IsZero)
            {
                return q;
            }
            if (q.IsZero)
            {
                return p;
            }

            long pdeg = p.Degree, qdeg = q.Degree;
            if (pdeg > qdeg)
            {
                // deg(P) > deg(Q)
                return GCDInner(p.Mod(q), q);
            }
            else
            {
                // deg(P) <= deg(Q)
                return GCDInner(p, q.Mod(p));
            }
        }

    }
}
