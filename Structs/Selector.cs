﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LinearNet.Structs
{
    /// <summary>
    /// Provides Matlab-like selector functionality for a collection
    /// Supports the following usage patterns:
    /// - 1
    /// - 1:9
    /// - 2, 4:5
    /// </summary>
    internal class Selector
    {
        internal int[] Selection { get; private set; }

        private Selector(int[] selection)
        {
            Selection = selection;
        }

        internal static bool TryParse(string q, bool oneBased, int dimension, out Selector s)
        {
            // Default
            s = null;

            if (string.IsNullOrEmpty(q))
            {
                return false;
            }

            List<int> selection = new List<int>();
            if (!TryParse(q, dimension, selection))
            {
                return false;
            }

            int[] array = selection.ToArray();

            // Convert 1-based notation
            if (oneBased)
            {
                for (int i = 0; i < array.Length; ++i)
                {
                    array[i]--;
                }
            }

            s = new Selector(array);
            return true;
        }
        private static bool TryParse(string q, int dimension, List<int> selection)
        {
            q = q.Trim();

            if (q.Contains(","))
            {
                string[] parts = q.Split(',');
                foreach (string part in parts)
                {
                    if (!TryParse(part, dimension, selection))
                    {
                        return false;
                    }
                }
                return true;
            }

            if (q.Contains(":"))
            {
                string[] parts = q.Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries);

                int begin = -1, end = -1;
                if (parts.Length == 0) // E.g. :
                {
                    begin = 0;
                    end = dimension - 1;
                }
                else if (parts.Length == 1) // E.g. 6: , :10
                {
                    if (q.EndsWith(":"))
                    {
                        if (!int.TryParse(q.Substring(0, q.Length - 1), out begin))
                        {
                            return false;
                        }
                        end = dimension - 1;
                    }
                    else
                    {
                        if (!int.TryParse(q.Substring(1), out end))
                        {
                            return false;
                        }
                        begin = 0;
                    }
                }
                else if (parts.Length == 2) // E.g. 3:5
                {
                    if (!int.TryParse(parts[0], out begin) || !int.TryParse(parts[1], out end))
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
                
                if (begin > end || begin < 0 || end < 0)
                {
                    return false;
                }

                for (int i = begin; i <= end; ++i)
                {
                    selection.Add(i);
                }
                return true;
            }

            // Only other supported type is a single integer
            if (!int.TryParse(q, out int index))
            {
                return false;
            }
            selection.Add(index);
            return true;
        }
    }
}
