﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Structs.Spaces
{
    public interface MetricSpace<T>
    {
        /// <summary>
        /// For a and b in set T, calculates and returns the distance between a and b
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        double Distance(T a, T b);
    }
}
