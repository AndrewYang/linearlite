﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Structs.Spaces
{
    interface NormedVectorSpace<T> : MetricSpace<T>
    {
        /// <summary>
        /// Calculates and returns the norm of an element a of type T
        /// </summary>
        /// <param name="a">An element of type T</param>
        /// <returns>The norm of a, |a|</returns>
        double Norm(T a);
    }
}
