﻿using LinearNet.Global;
using LinearNet.Providers;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace LinearNet.Structs
{
    public class COOSparseTensor<T> : Tensor<T> where T : new()
    {
        private readonly int[] _dimensions;
        private readonly int _order;
        private readonly T _zero;
        private bool _sorted;

        private int _nnz;
        /// indices are stored in continuous blocks of size _order. 
        /// The length is >= _order * _nnz
        /// </summary>
        private int[] _indices;
        /// <summary>
        /// values is of length >= _nnz
        /// </summary>
        private T[] _values;

        internal int[] Indices { get { return _indices; } }
        internal T[] Values { get { return _values; } }

        public override int Order => _order;
        public override long NonZeroCount => _nnz;
        public override IEnumerable<TensorEntry<T>> NonZeroEntries
        {
            get
            {
                if (_order == 0)
                {
                    yield return new TensorEntry<T>(new int[0], _values[0]);
                }
                else
                {
                    for (int i = 0; i < _nnz; ++i)
                    {
                        int[] index = new int[_order];
                        Array.Copy(_indices, i * _order, index, 0, _order);
                        yield return new TensorEntry<T>(index, _values[i]);
                    }
                }
            }
        }


        #region Constructor 

        public COOSparseTensor(params int[] dimensions)
        {
            if (dimensions is null)
            {
                throw new ArgumentNullException(nameof(dimensions));
            }
            for (int d = 0; d < dimensions.Length; ++d)
            {
                if (dimensions[d] < 0)
                {
                    throw new ArgumentOutOfRangeException(nameof(dimensions));
                }
            }

            _order = dimensions.Length;
            _dimensions = dimensions;
            _nnz = 0;
            _indices = new int[0];
            _values = new T[0];
            _zero = new T();
            _sorted = true;
        }
        public COOSparseTensor(T scalar)
        {
            if (scalar is null)
            {
                throw new ArgumentNullException(nameof(scalar));
            }
            _order = 0;
            _dimensions = new int[0];
            _zero = new T();
            _sorted = true;
            _nnz = 1;
            _indices = new int[0];
            _values = new T[] { scalar };
        }
        public COOSparseTensor(SparseVector<T> vector)
        {
            if (vector is null)
            {
                throw new ArgumentNullException(nameof(vector));
            }

            _order = 1;
            _dimensions = new int[] { vector.Dimension };
            _zero = new T();
            _indices = vector.Indices.Copy();
            _values = vector.Values.Copy();
            _nnz = _indices.Length;
            _sorted = true;
        }
        public COOSparseTensor(Vector<T> vector)
        {
            if (vector is null)
            {
                throw new ArgumentNullException(nameof(vector));
            }

            _order = 1;
            _dimensions = new int[] { vector.Dimension };
            _zero = new T();

            _nnz = vector.NonZeroCount;
            _indices = new int[_nnz];
            _values = new T[_nnz];

            _sorted = true;

            int k = 0, prevIndex = -1;
            foreach (VectorEntry<T> entry in vector.NonZeroEntries)
            {
                if (_sorted && prevIndex > entry.Index) _sorted = false;
                _indices[k] = entry.Index;
                _values[k++] = entry.Value;
            }
        }
        public COOSparseTensor(Matrix<T> matrix)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            _order = 2;
            _dimensions = new int[] { matrix.Rows, matrix.Columns };
            _zero = new T();

            IEnumerable<MatrixEntry<T>> values = matrix.NonZeroEntries;

            
            TrySetSize(matrix.SymbolicNonZeroCount);

            // Check sort as we iterate
            bool sorted = true, hasLast = false;
            int i = 0, j = 0;
            foreach (MatrixEntry<T> e in values)
            {
                _indices[i++] = e.RowIndex;
                _indices[i++] = e.ColumnIndex;
                _values[j++] = e.Value;

                if (sorted && hasLast)
                {
                    if (Compare(_indices, i - 4, i - 2, 2) > 0)
                    {
                        sorted = false;
                    }
                }
                hasLast = true;
            }
            _nnz = j;
            _sorted = sorted;
        }
        public COOSparseTensor(Tensor<T> tensor)
        {
            if (tensor is null)
            {
                throw new ArgumentNullException(nameof(tensor));
            }

            _order = tensor.Order;
            _dimensions = tensor.GetDimensions().Copy();
            _zero = new T();

            long nnz = tensor.NonZeroCount;
            if (nnz > int.MaxValue)
            {
                throw new OverflowException("Tensor is too large to be stored as in coordinate format");
            }

            TrySetSize((int)nnz);
            PopulateValues(tensor.NonZeroEntries);
        }
        public COOSparseTensor(int[] dimensions, int[] indices, T[] values, int nnz, bool sorted)
        {
            if (dimensions is null)
            {
                throw new ArgumentNullException(nameof(dimensions));
            }
            if (indices is null)
            {
                throw new ArgumentNullException(nameof(indices));
            }
            if (values is null)
            {
                throw new ArgumentNullException(nameof(values));
            }

            _order = dimensions.Length;
            _dimensions = dimensions.Copy();
            _zero = new T();

            if (nnz < 0)
            {
                _nnz = Math.Min(indices.Length / _order, values.Length);
            }
            else
            {
                _nnz = nnz;

                if (indices.Length < nnz * _order)
                {
                    throw new ArgumentOutOfRangeException(nameof(indices));
                }
                if (values.Length < nnz)
                {
                    throw new ArgumentOutOfRangeException(nameof(values));
                }
            }

            _values = values;
            _indices = indices;
            _sorted = sorted;
        }
        public COOSparseTensor(int[] dimensions, IEnumerable<TensorEntry<T>> values)
        {
            if (dimensions is null)
            {
                throw new ArgumentNullException(nameof(dimensions));
            }
            if (values is null)
            {
                throw new ArgumentNullException(nameof(values));
            }

            _order = dimensions.Length;
            for (int d = 0; d < _order; ++d)
            {
                if (dimensions[d] <= 0)
                {
                    throw new ArgumentOutOfRangeException(nameof(dimensions) + $"[{d}]");
                }
            }
            _dimensions = dimensions.Copy();
            _zero = new T();

            long nnz = values.LongCount();
            if (nnz > int.MaxValue)
            {
                throw new OverflowException("Tensor is too large to be stored as in coordinate format");
            }

            TrySetSize((int)nnz);
            PopulateValues(values);
        }

        #endregion


        #region Private methods 

        private void PopulateValues(IEnumerable<TensorEntry<T>> values)
        {

            // Check sort as we iterate
            bool sorted = true, hasLast = false;
            int i = 0, j = 0;
            foreach (TensorEntry<T> e in values)
            {
                Array.Copy(e.Index, 0, _indices, i, _order);
                _values[j++] = e.Value;

                if (sorted && hasLast)
                {
                    if (Compare(_indices, i - _order, i, _order) > 0)
                    {
                        sorted = false;
                    }
                }

                hasLast = true;
                i += _order;
            }

            _nnz = j;
            _sorted = sorted;
        }

        private void GuaranteeSize(int size)
        {
            if (_values.Length < size)
            {
                long len = _values.LongLength,
                    newlen = Math.Min(int.MaxValue / _order, len * 2);

                if (newlen <= size)
                {
                    throw new OverflowException("The tensor is too large to be stored in coordinate format.");
                }

                T[] values = new T[newlen];
                Array.Copy(_values, values, len);
                _values = values;

                int[] indices = new int[newlen * _order];
                Array.Copy(_indices, indices, len * _order);
                _indices = indices;
            }
        }
        private void TrySetSize(int size)
        {
            if ((double)size * _order > int.MaxValue)
            {
                throw new OverflowException("The tensor is too large to be stored in coordinate format.");
            }

            if (_nnz > 0)
            {
                T[] values = new T[size];
                Array.Copy(_values, values, _nnz);
                _values = values;

                int[] indices = new int[size * _order];
                Array.Copy(_indices, indices, _nnz * _order);
                _indices = indices;
            }
            else
            {
                _values = new T[size];
                _indices = new int[size * _order];
            }
        }
        private static int Compare(int[] index, int start1, int start2, int order)
        {
            for (int d = 0; d < order; ++d)
            {
                int i1 = index[start1 + d],
                    i2 = index[start2 + d];

                if (i1 > i2) return 1;
                if (i1 < i2) return -1;
            }
            return 0;
        }
        private static int Compare(int[] index, int start1, int[] index2, int order)
        {
            for (int d = 0; d < order; ++d)
            {
                int i1 = index[d + start1], i2 = index2[d];
                if (i1 > i2) return 1;
                if (i1 < i2) return -1;
            }
            return 0;
        }
        private static int Compare(int[] index, int start1, int[] index2, int start2, int order)
        {
            for (int d = 0; d < order; ++d)
            {
                int i1 = index[d + start1], i2 = index2[d + start2];
                if (i1 > i2) return 1;
                if (i1 < i2) return -1;
            }
            return 0;
        }
        /// <summary>
        /// Compares two indices, by first de-prioritizing an index in [0, order), keeping all other 
        /// dimensions in their existing order.
        /// </summary>
        /// <param name="index"></param>
        /// <param name="start1"></param>
        /// <param name="start2"></param>
        /// <param name="deprioritizedIndex"></param>
        /// <param name="order"></param>
        /// <returns></returns>
        private static int CompareDeprioritized(int[] index, int start1, int start2, int deprioritizedIndex, int order)
        {
            for (int d = 0; d < order; ++d)
            {
                if (d == deprioritizedIndex) continue;
                int i1 = index[start1 + d], i2 = index[start2 + d];
                if (i1 > i2) return 1;
                if (i1 < i2) return -1;
            }

            int k1 = index[start1 + deprioritizedIndex],
                k2 = index[start2 + deprioritizedIndex];
            if (k1 > k2) return 1;
            if (k1 < k2) return -1;
            return 0;
        }
        /// <summary>
        /// Compares two indices, and returns 0 if all indices except for the excluded index are equal (the excluded index
        /// can be equal, but it will not be checked).
        /// </summary>
        /// <param name="index"></param>
        /// <param name="start1"></param>
        /// <param name="start2"></param>
        /// <param name="excludedIndex"></param>
        /// <param name="order"></param>
        /// <returns></returns>
        private static int CompareExcluded(int[] index, int start1, int start2, int excludedIndex, int order)
        {
            for (int d = 0; d < order; ++d)
            {
                if (d != excludedIndex)
                {
                    int i1 = index[start1 + d], i2 = index[start2 + d];
                    if (i1 > i2) return 1;
                    if (i1 < i2) return -1;
                }
            }
            return 0;
        }
        private void Add(int[] index, T value)
        {
            GuaranteeSize(_nnz + 1);
            Array.Copy(index, 0, _indices, _nnz * _order, _order);
            _values[_nnz] = value;
            ++_nnz;
        }
        private void Insert(int index, int[] coord, T value)
        {
            GuaranteeSize(_nnz + 1);

            // Shift indices in lots of block size 
            int size = _order * sizeof(int),
                i1 = _nnz * _order,
                i2 = i1 - _order;

            for (int i = _nnz - 1; i > index; --i)
            {
                Buffer.BlockCopy(_indices, i2, _indices, i1, size);
                _values[i + 1] = _values[i];
                i2 -= _order;
                i1 -= _order;
            }

            Array.Copy(coord, 0, _indices, index * _order, _order);
            _values[index] = value;
        }
        private void AssertValid(int[] index)
        {
            if (index.Length != _order)
            {
                throw new ArgumentOutOfRangeException(nameof(index));
            }
            for (int d = 0; d < _order; ++d)
            {
                int ind = index[d];
                if (ind < 0 || ind >= _dimensions[d])
                {
                    throw new ArgumentOutOfRangeException(nameof(index) + $"[{d}]");
                }
            }
        }
        /// <summary>
        /// Sorts the values within this tensor by index, using in-place mergesort
        /// </summary>
        /// <param name="index">The whole index array of size nnz * dim</param>
        /// <param name="values">The whole value array of size nnz</param>
        /// <param name="start">The start index within 'values' to sort (inclusive)</param>
        /// <param name="end">The end index within 'values' to sort (inclusive)</param>
        /// <param name="dim">The order of the tensor</param>
        /// <param name="buffer">Temporarily holds an index (of size dim)</param>
        private static void MergeSort(int[] index, T[] values, int start, int end, int dim, int[] buffer)
        {
            if (end - start < 1) return;
            
            int mid = start + (end - start) / 2;

            // Divide and conquer
            MergeSort(index, values, start, mid, dim, buffer);
            MergeSort(index, values, mid + 1, end, dim, buffer);
            Merge(index, values, start, mid, end, dim, buffer);
        }
        private static void Merge(int[] index, T[] values, int start, int mid, int end, int dim, int[] buffer)
        {
            // Pointers to the currently compared index within the left and right halves
            int pi = start, 
                qi = (mid + 1),
                p = pi * dim, 
                q = qi * dim;

            for (int i = start; i <= end; ++i)
            {
                // L < R: L is already in the correct place
                if (qi > end || (pi <= mid && Compare(index, p, q, dim) < 0))
                {
                    p += dim;
                    ++pi;
                }

                // R < L: R needs to be inserted into Q
                else
                {
                    Array.Copy(index, q, buffer, 0, dim);
                    T value = values[qi];

                    // Shift all values in [p, q) -> [p + 1, q + 1) = [p + 1, q]
                    for (int k = q; k > p; k -= dim)
                    {
                        Array.Copy(index, k - dim, index, k, dim);
                    }
                    for (int k = qi; k > pi; --k)
                    {
                        values[k] = values[k - 1];
                    }

                    // Copy value into location i
                    Array.Copy(buffer, 0, index, p, dim);
                    values[pi] = value;

                    // Also need to shift the boundaries
                    ++pi;
                    p += dim;
                    ++mid;

                    // Update the q variables
                    ++qi;
                    q += dim;
                }
            }
        }
        /// <summary>
        /// Sorts the values of this tensor (with their indices), using a custom comparator
        /// </summary>
        /// <param name="index"></param>
        /// <param name="values"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <param name="dim"></param>
        /// <param name="buffer"></param>
        /// <param name="Compare"></param>
        private static void MergeSort(int[] index, T[] values, int start, int end, int dim, int[] buffer, Func<int[], int, int, int, int> Compare)
        {
            if (end - start < 1) return;

            int mid = start + (end - start) / 2;

            // Divide and conquer
            MergeSort(index, values, start, mid, dim, buffer, Compare);
            MergeSort(index, values, mid + 1, end, dim, buffer, Compare);
            Merge(index, values, start, mid, end, dim, buffer, Compare);
        }
        private static void Merge(int[] index, T[] values, int start, int mid, int end, int dim, int[] buffer, Func<int[], int, int, int, int> Compare)
        {
            // Pointers to the currently compared index within the left and right halves
            int pi = start,
                qi = (mid + 1),
                p = pi * dim,
                q = qi * dim;

            for (int i = start; i <= end; ++i)
            {
                // L < R: L is already in the correct place
                if (qi > end || (pi <= mid && Compare(index, p, q, dim) < 0))
                {
                    p += dim;
                    ++pi;
                }

                // R < L: R needs to be inserted into Q
                else
                {
                    Array.Copy(index, q, buffer, 0, dim);
                    T value = values[qi];

                    // Shift all values in [p, q) -> [p + 1, q + 1) = [p + 1, q]
                    for (int k = q; k > p; k -= dim)
                    {
                        Array.Copy(index, k - dim, index, k, dim);
                    }
                    for (int k = qi; k > pi; --k)
                    {
                        values[k] = values[k - 1];
                    }

                    // Copy value into location i
                    Array.Copy(buffer, 0, index, p, dim);
                    values[pi] = value;

                    // Also need to shift the boundaries
                    ++pi;
                    p += dim;
                    ++mid;

                    // Update the q variables
                    ++qi;
                    q += dim;
                }
            }
        }

        private static int BinarySearch(int[] indices, int[] index, int start, int end)
        {
            int dim = index.Length;
            if (end - start < 5)
            {
                for (int i = start; i < end; ++i)
                {
                    if (Compare(indices, i * dim, index, dim) == 0)
                    {
                        return i;
                    }
                }
                return -1;
            }

            int mid = (start + end) / 2;
            if (Compare(indices, mid * dim, index, dim) <= 0)
            {
                return BinarySearch(indices, index, mid, end); 
            }
            return BinarySearch(indices, index, start, mid);
        }
        private static int LinearSearch(int[] indices, int[] index, int start, int end)
        {
            int order = index.Length;
            for (int i = start; i < end; ++i)
            {
                if (Compare(indices, i * order, index, order) == 0)
                {
                    return i;
                }
            }
            return -1;
        }
        private static int BinarySearchInsertIndex(int[] indices, int[] index, int start, int end)
        {
            int dim = index.Length;
            if (end - start < 5)
            {
                for (int i = start; i < end; ++i)
                {
                    if (Compare(indices, i * dim, index, dim) < 0)
                    {
                        return i;
                    }
                }
                return end;
            }

            int mid = (start + end) / 2;
            if (Compare(indices, mid, index, dim) > 0)
            {
                return BinarySearchInsertIndex(indices, index, start, mid);
            }
            else
            {
                return BinarySearchInsertIndex(indices, index, mid, end);
            }
        }

        private static int GetNnzInUnion(int[] indices1, int[] indices2, int order, int nnz1, int nnz2)
        {
            // Use a 64-bit integer to avoid overflow
            long nnz = 0;
            int i = 0, j = 0;
            while (i < nnz1 || j < nnz2)
            {
                if (i >= nnz1)
                {
                    // j is in range, i is not in range
                    nnz += nnz2 - j;
                    break;
                }
                else if (j >= nnz2)
                {
                    // i is in range, j is not in range
                    nnz += nnz1 - i;
                    break;
                }
                else
                {
                    // Both i and j are in range
                    int comp = Compare(indices1, i * order, indices2, j * order, order);
                    if (comp > 0)
                    {
                        ++j;
                    }
                    else if (comp < 0)
                    {
                        ++i;
                    }
                    else
                    {
                        ++i;
                        ++j;
                    }
                    ++nnz;
                }
            }

            if (nnz > int.MaxValue)
            {
                throw new OverflowException("The tensor is too large to be stored in a Coordinate format.");
            }
            return (int)nnz;
        }

        #endregion


        #region Public instance methods

        public COOSparseTensor<T> Add(COOSparseTensor<T> tensor, IProvider<T> provider)
        {
            if (tensor is null)
            {
                throw new ArgumentNullException(nameof(tensor));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            AssertDimensionsMatch(_dimensions, tensor._dimensions);

            if (!_sorted)
            {
                Sort();
            }
            if (!tensor._sorted)
            {
                tensor.Sort();
            }

            // Count the number of non-zeroes in the result
            int nnz = GetNnzInUnion(_indices, tensor._indices, _order, _nnz, tensor._nnz);

            int[] indices = new int[nnz * _order];
            T[] values = new T[nnz];

            int i = 0, j = 0, k = 0;
            while (i < _nnz || j < tensor._nnz)
            {
                if (i >= _nnz)
                {
                    // j is in range, i is not in range
                    int len = tensor._nnz - j;
                    Array.Copy(tensor._indices, j * _order, indices, k * _order, len * _order);
                    Array.Copy(tensor._values, j, values, k, len);
                    break;
                }
                else if (j >= tensor._nnz)
                {
                    // i is in range, j is not in range
                    int len = _nnz - i;
                    Array.Copy(_indices, i * _order, indices, k * _order, len * _order);
                    Array.Copy(_values, i, values, k, len);
                    break;
                }
                else
                {
                    int ii = i * _order, jj = j * _order;

                    // Both i and j are in range
                    int comp = Compare(_indices, ii, tensor._indices, jj, _order);
                    if (comp > 0)
                    {
                        Array.Copy(tensor._indices, jj, indices, k * _order, _order);
                        values[k] = tensor._values[j];
                        ++j;
                    }
                    else if (comp < 0)
                    {
                        Array.Copy(_indices, ii, indices, k * _order, _order);
                        values[k] = _values[i];
                        ++i;
                    }
                    else
                    {
                        Array.Copy(_indices, ii, indices, k * _order, _order);
                        values[k] = provider.Add(_values[i], tensor._values[j]);
                        ++i;
                        ++j;
                    }
                    ++k;
                }
            }

            return new COOSparseTensor<T>(_dimensions.Copy(), indices, values, nnz, true);
        }
        public COOSparseTensor<T> Copy()
        {
            int[] indices = new int[_nnz * _order];
            Array.Copy(_indices, indices, indices.Length);

            T[] values = new T[_nnz];
            Array.Copy(_values, values, _nnz);

            return new COOSparseTensor<T>(_dimensions.Copy(), indices, values, _nnz, _sorted);
        }
        /// <summary>
        /// Apply a function to all symbolically non-zero entries of this tensor.
        /// </summary>
        /// <typeparam name="F"></typeparam>
        /// <param name="function"></param>
        /// <returns></returns>
        public COOSparseTensor<F> Elementwise<F>(Func<T, F> function) where F : new()
        {
            if (function is null)
            {
                throw new ArgumentNullException(nameof(function));
            }

            F[] values = new F[_nnz];
            for (int i = 0; i < _nnz; ++i)
            {
                values[i] = function(_values[i]);
            }

            int[] indices = new int[_nnz * _order];
            Array.Copy(_indices, indices, indices.Length);

            return new COOSparseTensor<F>(_dimensions.Copy(), _indices, values, _nnz, _sorted);
        }
        public override T Get(params int[] index)
        {
            AssertValid(index);

            if (_order == 0)
            {
                return _values[0];
            }

            int i;
            if (_sorted)
            {
                i = BinarySearch(_indices, index, 0, _nnz); // O(log(n))
            }
            else
            {
                i = LinearSearch(_indices, index, 0, _nnz); // O(n)
            }

            if (i < 0)
            {
                return _zero;
            }
            return _values[i];
        }
        public override int[] GetDimensions()
        {
            return _dimensions;
        }
        public COOSparseTensor<T> Multiply(DenseVector<T> vector, int mode, IProvider<T> provider)
        {
            // Sort by deprioritizing the mode index - this is O(n log(n))
            MergeSort(_indices, _values, 0, _nnz - 1, _order, new int[_order], (index, i1, i2, order) => CompareDeprioritized(index, i1, i2, mode, order));
            
            // Unfortunately, this destroys the original sort order
            // if the values were pre-sorted, it may be better to waste O(n) 
            // memory to avoid sorting in-place?
            _sorted = false;

            // Calculate the size of the product
            int nnz = 1; // Initialize to 1 to account for the last group
            for (int i = 1; i < _nnz; ++i)
            {
                // Changed product indices
                if (CompareExcluded(_indices, (i - 1) * _order, i * _order, mode, _order) != 0)
                {
                    ++nnz;
                }
            }

            int newOrder = _order - 1;
            int[] indices = new int[nnz * newOrder];
            T[] values = new T[nnz],
                vect_values = vector.Values;

            T sum = provider.Multiply(_values[0], vect_values[_indices[mode]]);
            int prev, curr = 0, k = 0;
            for (int i = 1; i < _nnz; ++i)
            {
                prev = curr;
                curr += _order;

                if (CompareExcluded(_indices, prev, curr, mode, _order) != 0)
                {
                    // Copy to the new index, reset the current cumulative sum
                    Array.Copy(_indices, prev, indices, k * newOrder, mode);
                    Array.Copy(_indices, prev + mode + 1, indices, k * newOrder + mode, newOrder - mode);
                    values[k++] = sum;
                    sum = provider.Zero;
                }

                sum = provider.Add(sum, provider.Multiply(_values[i], vect_values[_indices[curr + mode]]));
            }

            Array.Copy(_indices, curr, indices, k * newOrder, mode);
            Array.Copy(_indices, curr + mode + 1, indices, k * newOrder + mode, newOrder - mode);
            values[k] = sum;

            // Calculate the resulting dimensions
            int[] dimensions = new int[newOrder];
            Array.Copy(_dimensions, dimensions, mode);
            Array.Copy(_dimensions, mode + 1, dimensions, mode, newOrder - mode);

            return new COOSparseTensor<T>(dimensions, indices, values, nnz, true);
        }
        public COOSparseTensor<T> Multiply(T scalar, IDenseBLAS1Provider<T> blas1)
        {
            COOSparseTensor<T> copy = Copy();
            copy.Multiply(scalar, blas1);
            return copy;
        }
        public void MultiplyInPlace(T scalar, IDenseBLAS1Provider<T> blas1)
        {
            if (blas1 is null)
            {
                throw new ArgumentNullException(nameof(blas1));
            }
            if (!(blas1 is IDenseBLAS1<T> b))
            {
                throw new NotSupportedException(ExceptionMessages.TypeNotSupported);
            }
            b.SCAL(_values, scalar, 0, _nnz);
        }
        public COOSparseTensor<T> Negate(IDenseBLAS1Provider<T> blas1)
        {
            COOSparseTensor<T> copy = Copy();
            copy.NegateInPlace(blas1);
            return copy;
        }
        public void NegateInPlace(IDenseBLAS1Provider<T> blas1)
        {
            if (blas1 is null)
            {
                throw new ArgumentNullException(nameof(blas1));
            }
            if (!(blas1 is IDenseBLAS1<T> b))
            {
                throw new NotSupportedException(ExceptionMessages.TypeNotSupported);
            }
            T negone = b.Provider.Negate(b.Provider.One);
            b.SCAL(_values, negone, 0, _nnz);
        }
        public COOSparseTensor<T> Select(Dictionary<int, int> fixedIndices)
        {
            if (fixedIndices is null)
            {
                throw new ArgumentNullException(nameof(fixedIndices));
            }

            // Holds the fixed indices
            int[] index = new int[_order];
            bool[] isFixed = new bool[_order];
            foreach (KeyValuePair<int, int> e in fixedIndices)
            {
                if (e.Key < 0 || e.Key >= _order)
                {
                    throw new ArgumentOutOfRangeException(nameof(fixedIndices));
                }
                if (e.Value < 0 || e.Value >= _dimensions[e.Key])
                {
                    throw new ArgumentOutOfRangeException(nameof(fixedIndices));
                }

                isFixed[e.Key] = true;
                index[e.Key] = e.Value;
            }

            // Calculate the new order, new dimensions
            int newOrder = 0;
            for (int d = 0; d < _order; ++d)
            {
                if (!isFixed[d])
                {
                    ++newOrder;
                }
            }
            int[] newDimensions = new int[newOrder];
            for (int d = 0, k = 0; d < _order; ++d)
            {
                if (!isFixed[d])
                {
                    newDimensions[k++] = _dimensions[d];
                }
            }

            BitArray selected = new BitArray(_nnz);
            int nnz = 0;
            for (int i = 0; i < _nnz; ++i)
            {
                bool sel = true;
                for (int d = 0; d < _order; ++d)
                {
                    if (isFixed[d] && _indices[i + d] != index[d])
                    {
                        sel = false;
                        break;
                    }
                }

                if (sel)
                {
                    selected[i] = true;
                    ++nnz;
                }
            }

            int[] indices = new int[nnz * newOrder];
            T[] values = new T[nnz];

            for (int i = 0, k = 0; i < _nnz; ++i)
            {
                if (selected[i])
                {
                    int start = i * _order,
                        start1 = k * newOrder;

                    for (int d = 0; d < _order; ++d)
                    {
                        if (!isFixed[d])
                        {
                            indices[start1++] = _indices[start + d];
                        }
                    }
                    values[k++] = _values[i];
                }
            }
            return new COOSparseTensor<T>(newDimensions, indices, values, nnz, _sorted);
        }
        public override void Set(T value, params int[] index)
        {
            AssertValid(index);

            if (_order == 0)
            {
                _values[0] = value;
                return;
            }

            int i;
            if (_sorted)
            {
                i = BinarySearch(_indices, index, 0, _nnz);
            }
            else
            {
                i = LinearSearch(_indices, index, 0, _nnz);
            }

            if (i < 0)
            {
                if (_sorted)
                {
                    // Unfortunately, this is still O(n) average case since the list insertion
                    // requires shifting all entries coming after the inserted element. :(
                    int insert = BinarySearchInsertIndex(_indices, index, 0, _nnz);

                    // Since we are only copying, no need to copy the index array
                    Insert(insert, index, value);
                }
                else
                {
                    // Ironically, this is O(1) amortized time (we will pay for this later via 
                    // more expensive operations involving an unsorted tensor).
                    Add(index, value);
                }
            }
            else
            {
                _values[i] = value;
            }
        }
        public void Sort()
        {
            if (_sorted)
            {
                return;
            }
            else
            {
                MergeSort(_indices, _values, 0, _nnz - 1, _order, new int[_order]);
                _sorted = true;
            }
        }
        public COOSparseTensor<T> Subtract(COOSparseTensor<T> tensor, IProvider<T> provider)
        {
            if (tensor is null)
            {
                throw new ArgumentNullException(nameof(tensor));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            AssertDimensionsMatch(_dimensions, tensor._dimensions);

            if (!_sorted)
            {
                Sort();
            }
            if (!tensor._sorted)
            {
                tensor.Sort();
            }

            // Count the number of non-zeroes in the result
            int nnz = GetNnzInUnion(_indices, tensor._indices, _order, _nnz, tensor._nnz);

            int[] indices = new int[nnz * _order];
            T[] values = new T[nnz];

            int i = 0, j = 0, k = 0;
            while (i < _nnz || j < tensor._nnz)
            {
                if (i >= _nnz)
                {
                    // j is in range, i is not in range
                    int len = tensor._nnz - j;
                    Array.Copy(tensor._indices, j * _order, indices, k * _order, len * _order);
                    for ( ; j < tensor._nnz; ++j)
                    {
                        values[k++] = provider.Negate(tensor._values[j]);
                    }
                    break;
                }
                else if (j >= tensor._nnz)
                {
                    // i is in range, j is not in range
                    int len = _nnz - i;
                    Array.Copy(_indices, i * _order, indices, k * _order, len * _order);
                    Array.Copy(_values, i, values, k, len);
                    break;
                }
                else
                {
                    int ii = i * _order, jj = j * _order;

                    // Both i and j are in range
                    int comp = Compare(_indices, ii, tensor._indices, jj, _order);
                    if (comp > 0)
                    {
                        Array.Copy(tensor._indices, jj, indices, k * _order, _order);
                        values[k] = provider.Negate(tensor._values[j]);
                        ++j;
                    }
                    else if (comp < 0)
                    {
                        Array.Copy(_indices, ii, indices, k * _order, _order);
                        values[k] = _values[i];
                        ++i;
                    }
                    else
                    {
                        Array.Copy(_indices, ii, indices, k * _order, _order);
                        values[k] = provider.Subtract(_values[i], tensor._values[j]);
                        ++i;
                        ++j;
                    }
                    ++k;
                }
            }

            return new COOSparseTensor<T>(_dimensions.Copy(), indices, values, nnz, true);
        }
        public override T ToScalar()
        {
            if (_order != 0)
            {
                throw new InvalidOperationException("Tensor is not of order 0.");
            }
            return _values[0];
        }
        public override Vector<T> ToVector()
        {
            return ToSparseVector();
        }
        public SparseVector<T> ToSparseVector()
        {
            if (_order != 1)
            {
                throw new InvalidOperationException("Tensor is not of order 1.");
            }

            // Index arrays are of length 1 - can use default array sort 
            if (!_sorted)
            {
                Array.Sort(_indices, _values, 0, _nnz);
                _sorted = true;
            }

            // Copy into new memory
            int[] indices = new int[_nnz];
            T[] values = new T[_nnz];

            Array.Copy(_indices, indices, _nnz);
            Array.Copy(_values, values, _nnz);

            return new SparseVector<T>(_dimensions[0], indices, values, true);
        }
        public override Matrix<T> ToMatrix()
        {
            if (_order != 2)
            {
                throw new InvalidOperationException("Tensor is not of order 2.");
            }

            // Return a COO sparse matrix
            int len = _values.Length;
            List<MatrixEntry<T>> values = new List<MatrixEntry<T>>(len);
            for (int i = 0, j = 0; i < len; ++i)
            {
                values.Add(new MatrixEntry<T>(_indices[j++], _indices[j++], _values[i]));
            }
            return new COOSparseMatrix<T>(_dimensions[0], _dimensions[1], values, _sorted);
        }

        #endregion


        #region Operator overrides

        public static COOSparseTensor<T> operator +(COOSparseTensor<T> A, COOSparseTensor<T> B)
        {
            if (A is null)
            {
                throw new ArgumentNullException(nameof(A));
            }
            return A.Add(B, ProviderFactory.GetDefaultProvider<T>());
        }
        public static COOSparseTensor<T> operator -(COOSparseTensor<T> A, COOSparseTensor<T> B)
        {
            if (A is null)
            {
                throw new ArgumentNullException(nameof(A));
            }
            return A.Subtract(B, ProviderFactory.GetDefaultProvider<T>());
        }
        public static COOSparseTensor<T> operator -(COOSparseTensor<T> A)
        {
            if (A is null)
            {
                throw new ArgumentNullException(nameof(A));
            }
            return A.Negate(ProviderFactory.GetDefaultBLAS1<T>());
        }
        public static COOSparseTensor<T> operator *(COOSparseTensor<T> A, T scalar)
        {
            if (A is null)
            {
                throw new ArgumentNullException(nameof(A));
            }
            return A.Multiply(scalar, ProviderFactory.GetDefaultBLAS1<T>());
        }
        public static COOSparseTensor<T> operator *(T scalar, COOSparseTensor<T> A) => A * scalar;

        #endregion


        #region Casts

        public static explicit operator COOSparseTensor<T>(SparseVector<T> vector) => new COOSparseTensor<T>(vector);
        public static explicit operator COOSparseTensor<T>(Vector<T> vector) => new COOSparseTensor<T>(vector);
        public static explicit operator COOSparseTensor<T>(Matrix<T> matrix) => new COOSparseTensor<T>(matrix);
        public static explicit operator COOSparseTensor<T>(DenseTensor<T> tensor) => new COOSparseTensor<T>(tensor);
        public static explicit operator COOSparseTensor<T>(CSFSparseTensor<T> tensor) => new COOSparseTensor<T>(tensor);
        public static explicit operator COOSparseTensor<T>(SparseTensor<T> tensor) => new COOSparseTensor<T>(tensor);
        public static explicit operator COOSparseTensor<T>(KruskalTensor<T> tensor) => new COOSparseTensor<T>(tensor);

        #endregion
    }
}
