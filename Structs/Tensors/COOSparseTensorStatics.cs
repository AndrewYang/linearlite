﻿using LinearNet.Helpers;
using System;
using System.Collections.Generic;

namespace LinearNet.Structs
{
    public static class COOSparseTensor
    {
        public static COOSparseTensor<T> Random<T>(int[] dimensions, double density, Func<T> random = null) where T : new()
        {
            if (dimensions is null)
            {
                throw new ArgumentNullException(nameof(dimensions));
            }
            if (random is null)
            {
                random = DefaultGenerators.GetRandomGenerator<T>();
            }
            int order = dimensions.Length;
            for (int d = 0; d < order; ++d)
            {
                if (dimensions[d] < 0)
                {
                    throw new ArgumentOutOfRangeException(nameof(dimensions) + $"[{d}]");
                }
            }

            Random r = new Random();

            // This is only efficient if nnz = O(capacity)

            int incrementIndex = order - 1,
                incrementIndexDim = dimensions[incrementIndex];

            int[] index = new int[order];

            List<TensorEntry<T>> values = new List<TensorEntry<T>>();

            int i = 0;
            while (true)
            {
                if (i == incrementIndexDim)
                {
                    int j = incrementIndex;
                    while (index[j] == dimensions[j])
                    {
                        index[j--] = 0;

                        if (j == 0) goto end;

                        index[j]++;
                    }
                }

                if (r.NextDouble() <= density)
                {
                    values.Add(new TensorEntry<T>(index.Copy(), random()));
                }

                i = ++index[incrementIndex];
            }

        end:
            return new COOSparseTensor<T>(dimensions, values);
        }
    }
}
