﻿using LinearNet.Global;
using LinearNet.Matrices.Sparse.Decompositions;
using LinearNet.Providers;
using LinearNet.Tensors;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading;
using System.Windows.Markup;

namespace LinearNet.Structs
{
    /// <summary>
    /// Third-order dense tensor that can only handle up to int.MaxValue symbolic non-zero entries.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <cat>linear-algebra</cat>
    public class CSFSparseTensor<T> : Tensor<T> where T : new()
    {
        private readonly T _zero;
        private readonly int _rows, _columns, _depth; // Only order-3 tensors are supported

        private int[]
            _ri, // The row index of size (rows + 1), points to _ci
            _nzc, // The non-zero columns, in order and grouped by row
            _ci, // The column index of those non-zero columns, of size |_nzc| + 1
            _di; // The depth index for each row index and column index, of size nnz.

        private T[] _values; // The values array, of size nnz.
        internal T[] Values { get { return _values; } }


        #region Properties

        public int Rows { get { return _rows; } }
        public int Columns { get { return _columns; } }
        public int Depth { get { return _depth; } }
        public override int Order => 3;

        public override long NonZeroCount => _values.Length;

        public override IEnumerable<TensorEntry<T>> NonZeroEntries
        {
            get
            {
                for (int r = 0; r < _rows; ++r)
                {
                    int row_start = _ri[r],
                        row_end = _ri[r + 1];

                    for (int ci = row_start; ci < row_end; ++ci)
                    {
                        int c = _nzc[ci],
                            col_start = _ci[ci],
                            col_end = _ci[ci + 1];

                        for (int j = col_start; j < col_end; ++j)
                        {
                            yield return new TensorEntry<T>(new int[] { r, c, _di[j] }, _values[j]);
                        }
                    }
                }
            }
        }

        public CSRSparseMatrix<T> this[int rowIndex]
        {
            get
            {
                return Select(rowIndex);
            }
        }
        public SparseVector<T> this[int rowIndex, int columnIndex]
        {
            get
            {
                return Select(rowIndex, columnIndex);
            }
        }
        public T this[int rowIndex, int columnIndex, int depthIndex]
        {
            get
            {
                return Get(rowIndex, columnIndex, depthIndex);
            }
            set
            {
                Set(value, rowIndex, columnIndex, depthIndex);
            }
        }

        public CSFSparseTensor<T> this[string rowSelection]
        {
            get
            {
                return Select(rowSelection, oneBased: false);
            }
        }
        public CSFSparseTensor<T> this[string rowSelection, string columnSelection]
        {
            get
            {
                return Select(rowSelection, columnSelection, oneBased: false);
            }
        }
        public CSFSparseTensor<T> this[string rowSelection, string columnSelection, string depthSelection]
        {
            get
            {
                return Select(rowSelection, columnSelection, depthSelection, oneBased: false);
            }
        }

        #endregion


        #region Constructors

        /// <summary>
        /// Create a 1 x 1 x 1 sparse tensor using CSF storage format from a scalar value. 
        /// </summary>
        /// <param name="scalar"></param>
        public CSFSparseTensor(T scalar)
        {
            if (scalar is null) throw new ArgumentNullException(nameof(scalar));

            _rows = 1;
            _columns = 1;
            _depth = 1;
            _ri = new int[] { 0, 1 };
            _nzc = new int[] { 0 };
            _ci = new int[] { 0, 1 };
            _di = new int[] { 0 };
            _values = new T[] { scalar };
        }

        /// <summary>
        /// Create a sparse tensor using CSF storage format from a sparse vector. 
        /// Given a vector of length n,
        /// if mode = 1, a tensor of dimension n x 1 x 1 will be created
        /// if mode = 2, a tensor of dimension 1 x n x 1 will be created 
        /// if mode = 3, a tensor of dimension 1 x 1 x n will be created
        /// </summary>
        /// <param name="vector">The vector</param>
        /// <param name="mode">The embedding dimension of the vector in the tensor.</param>
        public CSFSparseTensor(SparseVector<T> vector, int mode = 0)
        {
            if (vector is null) throw new ArgumentNullException(nameof(vector));
            if (mode < 0 || mode > 2) throw new ArgumentOutOfRangeException(nameof(mode), "Value must be between 0 and 2 inclusive.");

            int[] vect_index = vector.Indices;
            T[] vect_values = vector.Values;
            int nnz = vect_values.Length;

            if (mode == 0)
            {
                // Create a tensor of dimension n x 1 x 1
                _rows = vector.Dimension;
                _columns = 1;
                _depth = 1;

                _ri = new int[_rows + 1];
                _nzc = new int[nnz];    // Keep as 0
                _ci = new int[nnz + 1];
                _di = new int[nnz];     // Keep as 0
                _values = vect_values.Copy();

                for (int i = 0; i < nnz; ++i)
                {
                    _ri[vect_index[i] + 1] = 1;
                }
                for (int i = 0; i < _rows; ++i)
                {
                    _ri[i + 1] += _ri[i];
                }
                for (int i = 0; i <= nnz; ++i)
                {
                    _ci[i] = i;
                }
            }
            else if (mode == 1)
            {
                // Create a tensor of dimension 1 x n x 1
                _rows = 1;
                _columns = vector.Dimension;
                _depth = 1;
                _ri = new int[] { 0, nnz };
                _nzc = vect_index.Copy();
                _ci = new int[nnz + 1];
                _di = new int[nnz]; // Keep as 0
                _values = vect_values.Copy();

                for (int i = 0; i <= nnz; ++i)
                {
                    _ci[i] = i;
                }
            }
            else
            {
                // Create a tensor of dimension 1 x 1 x n
                _rows = 1;
                _columns = 1;
                _depth = vector.Dimension;
                _ri = new int[] { 0, 1 };
                _nzc = new int[] { 0 };
                _ci = new int[] { 0, nnz };
                _di = vect_index.Copy();
                _values = vect_values.Copy();
            }
        }

        /// <summary>
        /// Create a sparse tensor using CSF storage format from a matrix
        /// </summary>
        /// <param name="matrix"></param>
        /// <param name="mode1"></param>
        /// <param name="mode2"></param>
        public CSFSparseTensor(Matrix<T> matrix, int mode1 = 0, int mode2 = 1)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            if (mode1 < 0 || mode1 > 2)
            {
                throw new ArgumentOutOfRangeException(nameof(mode1));
            }
            if (mode2 < 0 || mode2 > 2)
            {
                throw new ArgumentOutOfRangeException(nameof(mode2));
            }
            if (mode1 == mode2)
            {
                throw new ArgumentOutOfRangeException(nameof(mode2));
            }

            Tuple<int, int, int, T>[] values = new Tuple<int, int, int, T>[matrix.SymbolicNonZeroCount];
            int k = 0;
            int[] index = new int[3];

            foreach (MatrixEntry<T> e in matrix.NonZeroEntries)
            {
                index[mode1] = e.RowIndex;
                index[mode2] = e.ColumnIndex;
                values[k++] = new Tuple<int, int, int, T>(index[0], index[1], index[2], e.Value);
            }

            static int compare(Tuple<int, int, int, T> a, Tuple<int, int, int, T> b)
            {
                if (a.Item1 != b.Item1) return a.Item1 - b.Item1;
                if (a.Item2 != b.Item2) return a.Item2 - b.Item2;
                if (a.Item3 != b.Item3) return a.Item3 - b.Item3;
                return 0;
            }
            Array.Sort(values, compare);

            // Count the number of columns containing non-zero entries
            int count = 1,
                nnz = values.Length;
            for (int i = 1; i < nnz; ++i)
            {
                // If different row or column, increment
                Tuple<int, int, int, T> e1 = values[i - 1], e2 = values[i];
                if (e2.Item1 != e1.Item1 || e2.Item2 != e1.Item2)
                {
                    ++count;
                }
            }

            int[] dims = { 1, 1, 1 };
            dims[mode1] = matrix.Rows;
            dims[mode2] = matrix.Columns;

            _rows = dims[0];
            _columns = dims[1];
            _depth = dims[2];
            _ri = new int[_rows + 1];
            _nzc = new int[count];
            _ci = new int[count + 1];
            _di = new int[values.Length];
            _values = new T[values.Length];

            int ci = 0;
            for (int i = 0; i < nnz; ++i)
            {
                // Different columns - add the new column 
                if (i == 0 || values[i].Item1 != values[i - 1].Item1 || values[i].Item2 != values[i - 1].Item2)
                {
                    _nzc[ci] = values[i].Item2;
                    _ci[ci] = i;
                    ++ci;
                    _ri[values[i].Item1]++; // Increment rows on new column
                }

                Tuple<int, int, int, T> t = values[i];
                _di[i] = t.Item3;
                _values[i] = t.Item4;
            }
            _ci[ci] = nnz; // Fill in the last bound

            // Shift and accumulate rows
            for (int i = _rows; i > 0; --i)
            {
                _ri[i] = _ri[i - 1];
            }
            _ri[0] = 0;
            for (int i = 0; i < _rows; ++i)
            {
                _ri[i + 1] += _ri[i];
            }
        }

        /// <summary>
        /// Create a sparse tensor using CSF storage from a sparse tensor in COO storage.
        /// </summary>
        /// <param name="tensor">A tensor in COO storage format.</param>
        public CSFSparseTensor(COOSparseTensor<T> tensor)
        {
            if (tensor is null)
            {
                throw new ArgumentNullException(nameof(tensor));
            }
            if (tensor.Order != 3)
            {
                throw new NotSupportedException("Only order-3 tensors are supported.");
            }

            int[] dimensions = tensor.GetDimensions();
            _rows = dimensions[0];
            _columns = dimensions[1];
            _depth = dimensions[2];
            _zero = new T();

            if (tensor.NonZeroCount > int.MaxValue)
            {
                throw new OverflowException("Tensor is too large to be stored in CSF format.");
            }

            int nnz = (int)tensor.NonZeroCount;

            // Sort the tensor here
            tensor.Sort();

            int[] indices = tensor.Indices;

            // Record the depth array first (last index)
            _di = new int[nnz];
            _ri = new int[_rows + 1]; // The row indices array
            int lastCol = -1; // the previous column index (to detect a change)
            for (int i = 0, z = 0; i < nnz; ++i)
            {
                int row = indices[z++],
                    col = indices[z++],
                    depth = indices[z++];

                // Record column if column index is different to previous, 
                // or if we are on the last index
                if (col != lastCol || i == nnz - 1)
                {
                    lastCol = col;
                    _ri[row]++;
                }
                _di[i] = depth;
            }

            // Shift and accumulate the column count across the rows
            for (int r = _rows; r > 0; --r)
            {
                _ri[r] = _ri[r - 1];
            }
            _ri[0] = 0;
            for (int r = 0; r < _rows; ++r)
            {
                _ri[r + 1] += _ri[r];
            }
            int nCols = _ri[_rows];

            // Initialize column index array, and column index's index array 
            _nzc = new int[nCols];
            _ci = new int[nCols + 1];

            // Iterate again to fill in the columns array
            lastCol = -1;
            int k = 0;
            for (int i = 0, z = 1; i < nnz; ++i, z += 3)
            {
                int col = indices[z];
                if (col != lastCol)
                {
                    _nzc[k] = col;
                    _ci[k++] = i;
                    lastCol = col;
                }
            }

            // The final entry in _ci is the end
            _ci[k] = nnz;

            // Copy the values array
            _values = tensor.Values.Copy();

        }

        /// <summary>
        /// Creates a tensor using CSF storage 
        /// </summary>
        /// <param name="columns">The number of columns of the tensor (Dimension 2).</param>
        /// <param name="depth">The depth of the tensor (Dimension 3).</param>
        /// <param name="rowIndices">An array of size (rows + 1) containing the start and end indices of the rows.</param>
        /// <param name="nzColumns">An array of size (non-zero columns) containing the indices of the non-zero columns in each row.</param>
        /// <param name="columnIndices">An array of size (non-zero columns + 1) containing the start end end indices within the depthIndices array.</param>
        /// <param name="depthIndices">An array of size (nnz) containing the depth indices of all non-zero entries of the tensor.</param>
        /// <param name="values">An array of size (nnz) containing the non-zero values of the tensor.</param>
        public CSFSparseTensor(int columns, int depth, int[] rowIndices, int[] nzColumns, int[] columnIndices, int[] depthIndices, T[] values)
        {
            if (columns <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(columns), ExceptionMessages.NonPositiveNotAllowed);
            }
            if (depth <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(depth), ExceptionMessages.NonPositiveNotAllowed);
            }
            if (rowIndices is null)
            {
                throw new ArgumentNullException(nameof(rowIndices));
            }
            if (nzColumns is null)
            {
                throw new ArgumentNullException(nameof(nzColumns));
            }
            if (columnIndices is null)
            {
                throw new ArgumentNullException(nameof(columnIndices));
            }
            if (depthIndices is null)
            {
                throw new ArgumentNullException(nameof(depthIndices));
            }
            if (values is null)
            {
                throw new ArgumentNullException(nameof(values));
            }

            // Each entry in rowIndices must be in [0, |nzColumns|]
            ThrowHelper.AssertCollectionInRange(rowIndices, 0, nzColumns.Length + 1, nameof(rowIndices));

            // rowIndices must be increasing
            ThrowHelper.AssertMonotonicIncreasing(rowIndices, nameof(rowIndices));

            // nzColumns are the actual column indices - they must lie in [0, columns)
            ThrowHelper.AssertCollectionInRange(nzColumns, 0, columns, nameof(nzColumns));

            // nzColumns must be arranged in increasing order within each row
            ThrowHelper.AssertStrictlyIncreasingWithinRange(nzColumns, rowIndices, nameof(nzColumns));

            // columnIndices map into depthIndices and values array - must be a valid index in [0, |values|]
            ThrowHelper.AssertCollectionInRange(columnIndices, 0, values.Length + 1, nameof(columnIndices));

            // columnIndices must be monotonic increasing
            ThrowHelper.AssertMonotonicIncreasing(columnIndices, nameof(columnIndices));

            // columnIndices must be 1 more than nzColumns
            if (columnIndices.Length != nzColumns.Length + 1)
            {
                throw new ArgumentOutOfRangeException($"'{nameof(columnIndices)}' is not 1 longer than '{nameof(nzColumns)}'.");
            }

            // depthIndices must be strictly increasing within each column
            ThrowHelper.AssertStrictlyIncreasingWithinRange(depthIndices, columnIndices, nameof(depthIndices));

            // depthIndices and values must be the same length
            if (depthIndices.Length != values.Length)
            {
                throw new ArgumentOutOfRangeException($"Length of '{nameof(depthIndices)}' and '{nameof(values)}' do not match.");
            }

            _zero = new T();
            _rows = rowIndices.Length - 1;
            _columns = columns;
            _depth = depth;
            _ri = rowIndices;
            _nzc = nzColumns;
            _ci = columnIndices;
            _di = depthIndices;
            _values = values;
        }

        /// <summary>
        /// Creates a sparse tensor from a dense tensor, using a predicate to determine which entries to keep.
        /// </summary>
        /// <param name="tensor">The tensor.</param>
        /// <param name="include">If this predicate is true for an entry, it is included in the 
        /// constructed sparse tensor. Otherwise, the entry is discarded.</param>
        public CSFSparseTensor(DenseTensor<T> tensor, Predicate<T> include)
        {
            if (tensor is null) throw new ArgumentNullException(nameof(tensor));
            if (include is null) throw new ArgumentNullException(nameof(include));

            if (tensor.Order != 3)
            {
                throw new NotSupportedException("Only order-3 tensors can be stored in CSF format.");
            }

            int[] dimensions = tensor.GetDimensions();

            _rows = dimensions[0];
            _columns = dimensions[1];
            _depth = dimensions[1];

            // Init storage
            _ri = new int[_rows + 1];
            List<int> nzc = new List<int>(),
                ci = new List<int>() { 0 },
                di = new List<int>();
            List<T> values = new List<T>();

            for (int r = 0; r < _rows; ++r)
            {
                for (int c = 0; c < _columns; ++c)
                {
                    bool any = false;
                    for (int d = 0; d < _depth; ++d)
                    {
                        T e = tensor.Get(r, c, d);
                        if (include(e))
                        {
                            values.Add(e);
                            di.Add(d);
                            any = true;
                        }
                    }

                    if (any)
                    {
                        nzc.Add(c);
                        ci.Add(values.Count);
                    }
                }
                _ri[r + 1] = nzc.Count;
            }

            _nzc = nzc.ToArray();
            _ci = ci.ToArray();
            _di = di.ToArray();
            _values = values.ToArray();
        }

        /// <summary>
        /// Create a sparse tensor given the non-zero row, column and depth indexes, and the non-zero values stored in a dense tensor.
        /// The arrays must be sorted.
        /// </summary>
        /// <param name="rowIndices"></param>
        /// <param name="columnIndices"></param>
        /// <param name="depthIndices"></param>
        /// <param name="values"></param>
        public CSFSparseTensor(int rows, int columns, int depth, int[] rowIndices, int[] columnIndices, int[] depthIndices, DenseTensor<T> values)
        {
            if (rows < 0) throw new ArgumentOutOfRangeException(nameof(rows));
            if (columns < 0) throw new ArgumentOutOfRangeException(nameof(columns));
            if (depth < 0) throw new ArgumentOutOfRangeException(nameof(depth));
            if (rowIndices is null) throw new ArgumentNullException(nameof(rowIndices));
            if (columnIndices is null) throw new ArgumentNullException(nameof(columnIndices));
            if (depthIndices is null) throw new ArgumentNullException(nameof(depthIndices));
            if (values is null) throw new ArgumentNullException(nameof(values));

            if (values.Order != 3)
            {
                throw new InvalidOperationException("Tensor is not of order 3.");
            }

            int[] dims = values.GetDimensions();

            // Take the minimum of the dimensions
            int R = Math.Min(dims[0], rowIndices.Length),
                C = Math.Min(dims[1], columnIndices.Length),
                D = Math.Min(dims[2], depthIndices.Length),
                nzc = R * C,
                nnz = R * C * D;

            // Check the indices
            for (int i = 0; i < R; ++i)
            {
                int r = rowIndices[i];
                if (r < 0 || r >= rows) throw new ArgumentOutOfRangeException($"{nameof(rowIndices)}[{i}] is not a valid row index.");
                if (i > 0 && rowIndices[i] <= rowIndices[i - 1]) throw new ArgumentException(nameof(rowIndices), "Array is not sorted.");
            }
            for (int i = 0; i < C; ++i)
            {
                int c = columnIndices[i];
                if (c < 0 || c >= columns) throw new ArgumentOutOfRangeException($"{nameof(columnIndices)}[{i}] is not a valid column index.");
                if (i > 0 && columnIndices[i] <= columnIndices[i - 1]) throw new ArgumentException(nameof(columnIndices), "Array is not sorted.");
            }
            for (int i = 0; i < D; ++i)
            {
                int d = depthIndices[i];
                if (d < 0 || d >= depth) throw new ArgumentOutOfRangeException($"{nameof(depthIndices)}[{i}] is not a valid depth index.");
                if (i > 0 && depthIndices[i] <= depthIndices[i - 1]) throw new ArgumentException(nameof(depthIndices), "Array is not sorted.");
            }

            _rows = rows;
            _columns = columns;
            _depth = depth;
            _ri = new int[rows + 1];
            _nzc = new int[nzc];
            _ci = new int[nzc + 1];
            _di = new int[nnz];
            _values = new T[nnz];
            int[] index = new int[3];

            for (int r = 0, i = 0, j = 0; r < R; ++r)
            {
                index[0] = r;
                for (int c = 0; c < C; ++c)
                {
                    index[1] = c;
                    _nzc[i++] = columnIndices[c];
                    _ci[i] = i * D;

                    for (int d = 0; d < D; ++d)
                    {
                        index[2] = d;
                        _di[j] = depthIndices[d];
                        _values[j++] = values.Get(index);
                    }
                }
                _ri[rowIndices[r] + 1] = C;
            }

            // Fill in the rest of _ri
            for (int i = 0; i < rows; ++i)
            {
                _ri[i + 1] += _ri[i];
            }
        }

        /// <summary>
        /// Creates a sparse tensor from the outer product of three sparse vectors.
        /// </summary>
        /// <param name="v1">The first sparse vector to take the outer product of.</param>
        /// <param name="v2">The second sparse vector.</param>
        /// <param name="v3">The third sparse vector.</param>
        public CSFSparseTensor(SparseVector<T> v1, SparseVector<T> v2, SparseVector<T> v3, IDenseBLAS1<T> blas)
        {
            if (v1 is null) throw new ArgumentNullException(nameof(v1));
            if (v2 is null) throw new ArgumentNullException(nameof(v2));
            if (v3 is null) throw new ArgumentNullException(nameof(v3));
            if (blas is null) throw new ArgumentNullException(nameof(blas));

            _rows = v1.Dimension;
            _columns = v2.Dimension;
            _depth = v3.Dimension;

            IProvider<T> provider = blas.Provider;

            T[] values1 = v1.Values, values2 = v2.Values, values3 = v3.Values;
            int nnz1 = values1.Length, nnz2 = values2.Length, nnz3 = values3.Length;

            _ri = new int[_rows + 1];
            _nzc = new int[nnz1 * nnz2];
            _ci = new int[_nzc.Length + 1];
            _di = new int[_nzc.Length * nnz3];
            _values = new T[_di.Length];

            // Insert the row count
            for (int i = 0; i < nnz1; ++i)
            {
                _ri[v1.Indices[i] + 1] += nnz2;
            }
            for (int i = 0; i < _rows; ++i)
            {
                _ri[i + 1] += _ri[i];
            }

            for (int r = 0, i = 0, j = 0; r < nnz1; ++r)
            {
                T x = values1[r];
                for (int c = 0; c < nnz2; ++c, ++i)
                {
                    _nzc[i] = v2.Indices[c];
                    _ci[i + 1] = _ci[i] + nnz3;

                    T xy = provider.Multiply(x, values2[c]);
                    for (int d = 0; d < nnz3; ++d, ++j)
                    {
                        _di[j] = v3.Indices[d];
                        _values[j] = values3[d];
                    }
                    blas.SCAL(_values, xy, j - nnz3, j);
                }
            }
        }

        /// <summary>
        /// Creates a sparse tensor from the outer product of a matrix and a vector.
        /// </summary>
        /// <param name="matrix">A matrix</param>
        /// <param name="vector">A vector</param>
        /// <param name="blas">A dense level-1 BLAS provider.</param>
        public CSFSparseTensor(CSRSparseMatrix<T> matrix, SparseVector<T> vector, IDenseBLAS1<T> blas)
        {
            if (matrix is null) throw new ArgumentNullException(nameof(matrix));
            if (vector is null) throw new ArgumentNullException(nameof(vector));
            if (blas is null) throw new ArgumentNullException(nameof(blas));

            _rows = matrix.Rows;
            _columns = matrix.Columns;
            _depth = vector.Dimension;

            int[] vector_indices = vector.Indices;
            T[] matrix_values = matrix.Values,
                vector_values = vector.Values;
            int nnz12 = matrix.Values.Length,
                nnz3 = vector_values.Length;

            _ri = matrix.RowIndices.Copy();
            _nzc = matrix.ColumnIndices.Copy();
            _ci = new int[matrix.ColumnIndices.Length + 1];
            _di = new int[nnz12 * nnz3];
            _values = new T[nnz12 * nnz3];

            for (int i = 1; i < _ci.Length; ++i)
            {
                _ci[i] = i * nnz3;
            }

            for (int r = 0, k = 0; r < _rows; ++r)
            {
                int row_start = matrix.RowIndices[r],
                    row_end = matrix.RowIndices[r + 1];
                for (int i = row_start; i < row_end; ++i)
                {
                    T x = matrix_values[i];
                    Array.Copy(vector_values, 0, _values, k, nnz3);
                    blas.SCAL(_values, x, k, k + nnz3);
                    for (int t = 0; t < nnz3; ++t)
                    {
                        _di[k + t] = vector_indices[t];
                    }
                    k += nnz3;
                }
            }
        }

        /// <summary>
        /// Creates a sparse tensor from the outer product of a vector and a matrix.
        /// </summary>
        /// <param name="vector"></param>
        /// <param name="matrix"></param>
        /// <param name="blas"></param>
        public CSFSparseTensor(SparseVector<T> vector, CSRSparseMatrix<T> matrix, IDenseBLAS1<T> blas)
        {
            if (vector is null)
            {
                throw new ArgumentNullException(nameof(vector));
            }
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            if (blas is null)
            {
                throw new ArgumentNullException(nameof(blas));
            }

            _rows = vector.Dimension;
            _columns = matrix.Rows;
            _depth = matrix.Columns;

            _ri = new int[_rows + 1];

            int nzc = 0;
            throw new NotImplementedException();
        }

        /// <summary>
        /// Copy constructor
        /// </summary>
        /// <param name="tensor"></param>
        public CSFSparseTensor(CSFSparseTensor<T> tensor)
        {
            if (tensor is null)
            {
                throw new ArgumentNullException(nameof(tensor));
            }

            _rows = tensor._rows;
            _columns = tensor._columns;
            _depth = tensor._depth;

            _ri = tensor._ri.Copy();
            _nzc = tensor._nzc.Copy();
            _ci = tensor._ci.Copy();
            _di = tensor._di.Copy();
            _values = tensor._values.Copy();
        }

        #endregion


        #region Private methods

        /// <summary>
        /// Calculates the non-zero pattern of this tensor with another tensor, producing two integers:
        /// 1) the number of non-zero entries of the resulting union tensor
        /// 2) the number of columns in the resulting union tensor.
        /// </summary>
        /// <param name="tensor">The other tensor to union with.</param>
        /// <param name="_nnz">(out parameter) the number of non-zero entries in the union</param>
        /// <param name="_nz_col">(out parameter) the number of columns containing non-zero entries in the union.</param>
        private void CalculateNonZeroPatternOfUnionWith(CSFSparseTensor<T> tensor, out int _nnz, out int _nz_col)
        {
            // Counting operations - determine the size of required vectors
            int nnz = 0;
            int nz_col = 0; // The number of non-zero columns

            for (int r = 0; r < _rows; ++r)
            {
                int r1 = _ri[r], r2 = tensor._ri[r],
                    row_end1 = _ri[r + 1], row_end2 = tensor._ri[r + 1];

                while (r1 < row_end1 || r2 < row_end2)
                {
                    int c1 = r1 < row_end1 ? _nzc[r1] : int.MaxValue,
                        c2 = r2 < row_end2 ? tensor._nzc[r2] : int.MaxValue;

                    if (c1 > c2)
                    {
                        nnz += (tensor._ci[r2 + 1] - tensor._ci[r2]);
                        ++r2;
                    }
                    else if (c1 < c2)
                    {
                        nnz += (_ci[r1 + 1] - _ci[r1]);
                        ++r1;
                    }
                    else
                    {
                        // Both tensors contain this column - merge 
                        int i1 = _ci[r1], i2 = tensor._ci[r2],
                            col_end1 = _ci[r1 + 1], col_end2 = tensor._ci[r2 + 1];

                        //while (true)
                        //{
                        //    if (i1 < col_end1)
                        //    {
                        //        if (i2 < col_end2)
                        //        {
                        //            int d1 = _di[i1], d2 = tensor._di[i2];
                        //            if (d1 > d2) ++i2;
                        //            else if (d1 < d2) ++i1;
                        //            else
                        //            {
                        //                ++i1; ++i2;
                        //            }
                        //        }
                        //        else
                        //        {
                        //            ++i1;
                        //        }
                        //        ++nnz;
                        //    }
                        //    else
                        //    {
                        //        if (i2 < col_end2)
                        //        {
                        //            ++i2;
                        //            ++nnz;
                        //        }
                        //        else
                        //        {
                        //            break;
                        //        }
                        //    }
                        //}

                        bool b1;
                        while ((b1 = i1 < col_end1) || i2 < col_end2)
                        {

                            int d1 = b1 ? _di[i1] : int.MaxValue,
                                d2 = i2 < col_end2 ? tensor._di[i2] : int.MaxValue;
                            if (d1 > d2) ++i2;
                            else if (d1 < d2) ++i1;
                            else
                            {
                                ++i1;
                                ++i2;
                            }
                            ++nnz;
                        }
                        ++r1;
                        ++r2;
                    }
                    ++nz_col;
                }
            }

            _nnz = nnz;
            _nz_col = nz_col;
        }

        private void ValidateIndex(int[] index)
        {
            if (index is null)
            {
                throw new ArgumentNullException(nameof(index));
            }
            if (index.Length < 3)
            {
                throw new ArgumentOutOfRangeException(nameof(index));
            }

            int r = index[0],
                c = index[1],
                d = index[2];

            if (r < 0 || r >= _rows) throw new ArgumentOutOfRangeException(nameof(index) + "[0]");
            if (c < 0 || c >= _columns) throw new ArgumentOutOfRangeException(nameof(index) + "[1]");
            if (d < 0 || d >= _depth) throw new ArgumentOutOfRangeException(nameof(index) + "[2]");
        }

        /// <summary>
        /// For an array sorted in [start, end), return the index in which we should insert value.
        /// Behaviour is undefined if the value already exists in the array.
        /// Possible values are [0, end].
        /// Complexity is O(log(n))
        /// </summary>
        /// <param name="sortedArray"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        private int GetInsertionIndex(int[] sortedArray, int start, int end, int value)
        {
            if (end - start < 4)
            {
                for (int i = start; i < end; ++i)
                {
                    if (sortedArray[i] > value)
                    {
                        return i;
                    }
                }
                return end;
            }

            int mid = (start + end) / 2;
            if (value < sortedArray[mid])
            {
                return GetInsertionIndex(sortedArray, start, mid, value);
            }
            return GetInsertionIndex(sortedArray, mid, end, value);
        }

        private bool IsPermutationVector(int[] permutationVector)
        {
            if (permutationVector.Length != 3)
            {
                return false;
            }

            bool[] present = new bool[3];
            for (int i = 0; i < permutationVector.Length; ++i)
            {
                present[permutationVector[i]] = true;
            }

            for (int i = 0; i < present.Length; ++i)
            {
                if (!present[i])
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Mode-0 matrix multiplication implementation using dictionaries
        /// No argument checks in this method as it is handled in the public methods
        /// </summary>
        /// <param name="matrix"></param>
        /// <param name="provider"></param>
        /// <returns></returns>
        private CSFSparseTensor<T> Multiply_Mode0(CSRSparseMatrix<T> matrix, IProvider<T> provider)
        {
            int M = matrix.Rows;
            // [R, C, D] x [M, R] -> [M, C, D]
            int[] ri = new int[M + 1];
            FastList<int> nzc = new FastList<int>(M),
                ci = new FastList<int>(M),
                di = new FastList<int>(M);
            FastList<T> values = new FastList<T>(M);

            // Used to keep track of the nz pattern of each slice [r, :, :]
            Dictionary<long, T> slice = new Dictionary<long, T>();
            long ld = _depth;
            for (int r = 0; r < M; ++r) // iterate through the rows of the matrix = rows of the new tensor
            {
                // Calculate the nz pattern of this row in the resulting product tensor
                // The nz pattern of this row is simply the set union of the nz pattern of all slices [k, :, :] from 
                // the original tensor where k is in the nz column pattern of the row of the matrix.
                // Unfortunately this nz union is not easy to compute, because its potential max size is _columns * _depth
                // so an array solution is not possible. We are using a dictionary in this case which is very slow but at 
                // least linear in nnz. Unclear if there exists a better solution - this is a future research direction.

                int rstart = matrix.RowIndices[r], rend = matrix.RowIndices[r + 1];
                for (int i = rstart; i < rend; ++i)
                {
                    int c = matrix.ColumnIndices[i]; // column index of the matrix element v = row index of old tensor
                    T v = matrix.Values[i];

                    int t_rstart = _ri[c], t_rend = _ri[c + 1]; // bounds of the rows of the old tensor
                    for (int j = t_rstart; j < t_rend; ++j)
                    {
                        int t_c = _nzc[j]; // the old tensor column index
                        int t_cstart = _ci[j], t_cend = _ci[j + 1]; // the old tensor column bounds

                        // There are (t_cend - t_cstart) nz entries in this fibre [r, c, :]
                        long key_offset = t_c * ld;
                        for (int k = t_cstart; k < t_cend; ++k)
                        {
                            int t_d = _di[k]; // old tensor depth index
                            T tv = provider.Multiply(v, _values[k]); // old tensor value * v

                            long key = key_offset + t_d;
                            if (slice.ContainsKey(key))
                            {
                                slice[key] = provider.Add(slice[key], tv);
                            }
                            else
                            {
                                slice[key] = tv;
                            }
                        }
                    }
                }

                // Re-pack the dictionary into a slice
                long[] keys = new long[slice.Count];
                T[] slice_values = new T[slice.Count];
                int l = 0;
                foreach (KeyValuePair<long, T> e in slice)
                {
                    keys[l] = e.Key;
                    slice_values[l++] = e.Value;
                }

                // Sort to arrange into column/depth order - there is a faster way 
                // to do this
                Array.Sort(keys, slice_values);

                // Pack into the storage structure
                int nnz_so_far = values.Count;
                for (int i = 0; i < keys.Length; ++i)
                {
                    // New column - we don't consider zeroes
                    if (i == 0 || (keys[i] / ld) != (keys[i - 1] / ld))
                    {
                        nzc.Add((int)(keys[i] / ld)); // Add the column index
                        ci.Add(nnz_so_far + i);
                    }

                    // Add the depth index and value
                    di.Add((int)(keys[i] % ld));
                    values.Add(slice_values[i]);
                }
                ri[r + 1] = nzc.Count;

                // reset dictionary
                slice.Clear();
            }
            // Add the last ci
            ci.Add(values.Count);

            // Create arrays 
            int[] nzc_arr = nzc.ToArray(),
                ci_arr = ci.ToArray(),
                di_arr = di.ToArray();
            T[] values_arr = values.ToArray();

            // Create the tensor itself.
            return new CSFSparseTensor<T>(_columns, _depth, ri, nzc_arr, ci_arr, di_arr, values_arr);
        }

        /// <summary>
        /// Mode-0 matrix multiplication implementation using linked lists 
        /// Again, no argument checks here...
        /// This method currently fails some edge cases and is only slightly more performant than the other method
        /// </summary>
        /// <param name="matrix"></param>
        /// <param name="provider"></param>
        /// <returns></returns>
        private CSFSparseTensor<T> Multiply_Mode0_1(CSRSparseMatrix<T> matrix, IProvider<T> provider)
        {
            int M = matrix.Rows;

            int[] ri = new int[M + 1];

            /// Workspace keeping track of all the non-zero columns of a particular slice. (i.e. all 
            /// the column indexes that contain nz entries @ some depth. This array will be cleared 
            /// at the end of each slice to maintain invariance. Column indexes will be filled left 
            /// to right @[0, ..., count_nzc)
            int[] nz_cols = new int[_columns];
            /// The number of non-zero columns in this slice
            int count_nzc = 0;
            /// If is_nzc[i] = true then column i contains a non-zero in this slice
            bool[] is_nzc = new bool[_columns];
            /// nzc_head[i] = the starting index (index of first entry) of column i. 
            int[] nzc_head = new int[_columns];
            /// nzc_tail[i] = the ending index (index of last entry) of column i.
            int[] nzc_tail = new int[_columns];
            /// Contains the depth index
            FastList<int> depth_index = new FastList<int>(_columns);
            /// Contains the next index (this is the linked list)
            FastList<int> next_index = new FastList<int>(_columns);
            // 1:1 correspondence with depth_index
            FastList<T> values = new FastList<T>(_columns);

            // The depth workspace for computing set unions across depth
            int[] nz_depth_index = new int[_depth];
            int count_nz_depth_index = 0;
            bool[] is_nz_depth_index = new bool[_depth];
            T[] sums = new T[_depth];

            // Storage for the actual product tensor
            FastList<int> nzc = new FastList<int>(_columns);
            FastList<int> ci = new FastList<int>(_columns); ci.Add(0);
            FastList<int> di = new FastList<int>(_columns);
            FastList<T> _values_ = new FastList<T>(_columns);

            for (int r = 0; r < M; ++r)
            {
                int m_rstart = matrix.RowIndices[r],
                    m_rend = matrix.RowIndices[r + 1];
                for (int i = m_rstart; i < m_rend; ++i)
                {
                    int c = matrix.ColumnIndices[i];
                    T e = matrix.Values[i];

                    // iterate through slice [c,:,:]
                    int t_rstart = _ri[c],
                        t_rend = _ri[c + 1];

                    for (int j = t_rstart; j < t_rend; ++j)
                    {
                        int tc = _nzc[j];

                        // Initialize if new column
                        if (!is_nzc[tc])
                        {
                            nz_cols[count_nzc++] = tc;
                            is_nzc[tc] = true;
                            nzc_head[tc] = -1;
                            nzc_tail[tc] = -1;
                        }

                        int parent_index = nzc_tail[tc];

                        // Iterate across the fibre [r, c, :]
                        int t_cstart = _ci[j], t_cend = _ci[j + 1];
                        for (int k = t_cstart; k < t_cend; ++k)
                        {
                            int td = _di[k];
                            T te = _values[k];

                            //Debug.WriteLine(tc + "\t" + td);

                            // insert (td, value) into linked list corresponding to column tc
                            int self_index = depth_index.Count;
                            if (parent_index >= 0)
                            {
                                next_index.Values[parent_index] = self_index; // Link to parent
                            }
                            depth_index.Add(td);                    // Add the depth index
                            values.Add(provider.Multiply(te, e));   // Add the product value
                            next_index.Add(-1);                     // No next element yet
                            nzc_tail[tc] = self_index;              // Update the tail element
                            parent_index = self_index;              // Update the parent for next itr
                            if (nzc_head[tc] < 0)
                            {
                                nzc_head[tc] = self_index;          // check if it's the first element in this column
                            }
                        }
                    }
                }

                // Sort the column indexes so they get iterated in the correct order 
                Array.Sort(nz_cols, 0, count_nzc);

                // Calculate the set union of all the affected columns,
                // clearing them from memory as we iterate
                for (int j = 0; j < count_nzc; ++j)
                {
                    int tc = nz_cols[j];

                    // Iterate through the linkedlist
                    int index = nzc_head[tc];
                    while (index >= 0)
                    {
                        int d = depth_index[index];
                        T value = values[index];
                        if (is_nz_depth_index[d])
                        {
                            sums[d] = provider.Add(sums[d], value);
                        }
                        else
                        {
                            sums[d] = value;
                            is_nz_depth_index[d] = true;
                            nz_depth_index[count_nz_depth_index++] = d;
                        }
                        index = next_index[index];
                    }

                    // Add values 
                    for (int k = 0; k < count_nz_depth_index; ++k)
                    {
                        int d = nz_depth_index[k];
                        T value = sums[d];
                        di.Add(d);
                        _values_.Add(value);
                        is_nz_depth_index[d] = false; // Reset
                    }
                    nzc.Add(tc);
                    ci.Add(_values_.Count);
                    count_nz_depth_index = 0; // Reset

                    is_nzc[j] = false; // Reset
                }
                count_nzc = 0; // Reset

                ri[r + 1] = nzc.Count;
            }

            int[] nzc_arr = nzc.ToArray(),
                ci_arr = ci.ToArray(),
                di_arr = di.ToArray();
            T[] values_arr = _values_.ToArray();

            // Sort - only the depth values 
            for (int i = 0; i < nzc_arr.Length; ++i)
            {
                int cstart = ci_arr[i], cend = ci_arr[i + 1];
                Array.Sort(di_arr, values_arr, cstart, cend - cstart);
            }

            //Debug.WriteLine("ri");
            //ri.Print();
            //Debug.WriteLine("nzc_arr");
            //nzc_arr.Print();
            //Debug.WriteLine("ci_arr");
            //ci_arr.Print();
            //Debug.WriteLine("di_arr");
            //di_arr.Print();
            //Debug.WriteLine("values_arr");
            //values_arr.Print();

            return new CSFSparseTensor<T>(_columns, _depth, ri, nzc_arr, ci_arr, di_arr, values_arr);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <notes>
        /// Approximately the same performance of Multiply_Mode0 on 100, 200 and 300 dimension tensors.
        /// This method is unstable - breaks down under very rare edge cases. Requires further research
        /// </notes>
        /// <param name="matrix"></param>
        /// <param name="provider"></param>
        /// <returns></returns>
        private CSFSparseTensor<T> Multiply_Mode0_2(CSRSparseMatrix<T> matrix, IProvider<T> provider)
        {
            int M = matrix.Rows;
            // [R, C, D] x [M, R] -> [M, C, D]
            int[] ri = new int[M + 1];
            FastList<int> nzc = new FastList<int>(M),
                ci = new FastList<int>(M),
                di = new FastList<int>(M);
            FastList<T> values = new FastList<T>(M);

            ci.Add(0);

            // Keep track of the number of nz in each column, within a slice
            int[] count_nzc_slice = new int[_columns];
            // [0,..., nz_count) contains the nz column indexes for this slice
            int[] nzc_slice = new int[_columns];
            // [i] specifies whether column i contains a nz entry
            bool[] is_nzc = new bool[_columns];
            // The number of nz columns within a slice
            int nz_count = 0;

            // [i] is the index to insert the next element from column i
            int[] next_index = new int[_columns];

            // Resizable workspace used for sorting di/values
            int[] di_workspace = new int[0];
            T[] values_workspace = new T[0];

            // Used to keep track of the nz pattern of each slice [r, :, :]
            Dictionary<long, T> slice = new Dictionary<long, T>();
            long ld = _depth;
            for (int r = 0; r < M; ++r) // iterate through the rows of the matrix = rows of the new tensor
            {
                int rstart = matrix.RowIndices[r], rend = matrix.RowIndices[r + 1];
                for (int i = rstart; i < rend; ++i)
                {
                    int c = matrix.ColumnIndices[i]; // column index of the matrix element = row index of old tensor
                    T v = matrix.Values[i];
                    for (int j = _ri[c], t_rend = _ri[c + 1]; j < t_rend; ++j)
                    {
                        int t_c = _nzc[j]; // the old tensor column index

                        // Record the column as non-zero
                        if (!is_nzc[t_c])
                        {
                            nzc_slice[nz_count++] = t_c;
                            is_nzc[t_c] = true;
                            count_nzc_slice[t_c] = 0; // Reset
                        }

                        // There are (t_cend - t_cstart) nz entries in this fibre [r, c, :]
                        long key_offset = t_c * ld;
                        for (int k = _ci[j], t_cend = _ci[j + 1]; k < t_cend; ++k)
                        {
                            int t_d = _di[k]; // old tensor depth index
                            T tv = provider.Multiply(v, _values[k]); // old tensor value * v

                            long key = key_offset + t_d;
                            if (slice.ContainsKey(key))
                            {
                                slice[key] = provider.Add(slice[key], tv);
                            }
                            else
                            {
                                slice[key] = tv;
                                count_nzc_slice[t_c]++;
                            }
                        }
                    }
                }

                // Early return if entire row is empty
                if (nz_count == 0)
                {
                    ri[r + 1] = nzc.Count;
                    slice.Clear();
                    continue;
                }

                // Create the nz structure of the slice
                Array.Sort(nzc_slice, 0, nz_count);

                // Add all the non-zero columns to the array 
                nzc.AddRange(nzc_slice, 0, nz_count);

                // Increment the values in count_nzc_slice, and add them to ci list
                // Also, use this opportunity to copy into the next_index array which 
                // will be used later.
                int prev_count = ci[ci.Count - 1], cum = 0;
                next_index[nzc_slice[0]] = 0;
                for (int i = 0; i < nz_count; ++i)
                {
                    int c = nzc_slice[i];
                    cum += count_nzc_slice[c];
                    count_nzc_slice[c] = cum;
                    if (c < nz_count - 1)
                    {
                        next_index[nzc_slice[c + 1]] = cum; // shifted 
                    }
                    ci.Add(prev_count + cum);
                }

                // Prepare to insert into workspace arrays - ensure sizing
                if (di_workspace.Length < slice.Count)
                {
                    Array.Resize(ref di_workspace, slice.Count);
                    Array.Resize(ref values_workspace, slice.Count);
                }

                foreach (KeyValuePair<long, T> e in slice)
                {
                    int c = (int)(e.Key / ld),
                        d = (int)(e.Key % ld);

                    int index = next_index[c]++;
                    di_workspace[index] = d;
                    values_workspace[index] = e.Value;
                }

                // Sort within each column
                for (int i = 0; i < nz_count; ++i)
                {
                    int start = i > 0 ? count_nzc_slice[nzc_slice[i - 1]] : 0,
                        end = count_nzc_slice[nzc_slice[i]];
                    Array.Sort(di_workspace, values_workspace, start, end - start);
                }
                di.AddRange(di_workspace, 0, slice.Count);
                values.AddRange(values_workspace, 0, slice.Count);

                // Clear/reset
                for (int i = 0; i < nz_count; ++i)
                {
                    is_nzc[nzc_slice[i]] = false;
                }
                nz_count = 0;

                // Set the row counts
                ri[r + 1] = nzc.Count;

                // reset dictionary
                slice.Clear();
            }

            // Create arrays 
            int[] nzc_arr = nzc.ToArray(),
                ci_arr = ci.ToArray(),
                di_arr = di.ToArray();
            T[] values_arr = values.ToArray();

            // Create the tensor itself.
            return new CSFSparseTensor<T>(_columns, _depth, ri, nzc_arr, ci_arr, di_arr, values_arr);
        }

        /// <summary>
        /// Mode-1 matrix multiplication implementation
        /// </summary>
        /// <param name="matrix"></param>
        /// <param name="provider"></param>
        /// <returns></returns>
        private CSFSparseTensor<T> Multiply_Mode1(CSRSparseMatrix<T> matrix, IProvider<T> provider)
        {
            // [R, C, D] x [M, C] -> [R, M, D]
            int[] ri = new int[_rows + 1];
            FastList<int> nzc = new FastList<int>(_columns),
                ci = new FastList<int>(_columns),
                di = new FastList<int>(_columns);
            FastList<T> values = new FastList<T>(_columns);

            ci.Add(0);

            // For computing the set union of depth fibres
            T[] sums = new T[_depth];
            int[] nz_index = new int[_depth];
            bool[] is_nz = new bool[_depth];
            int nz_count = 0;

            // This will be overwritten for each row. 
            // column_inverseLookup[nzc[i]] = i
            int[] column_inverseLookup = new int[_columns];
            for (int i = 0; i < _columns; ++i)
            {
                column_inverseLookup[i] = -1;
            }

            for (int r = 0; r < _rows; ++r)
            {
                int t_rstart = _ri[r], t_rend = _ri[r + 1];

                // Clear previous column_inverseLookup
                if (r > 0)
                {
                    int begin = _ri[r - 1];
                    for (int i = begin; i < t_rstart; ++i)
                    {
                        column_inverseLookup[_nzc[i]] = -1;
                    }
                }

                // Populate the inverse column lookup table
                for (int i = t_rstart; i < t_rend; ++i)
                {
                    column_inverseLookup[_nzc[i]] = i;
                }

                // This is essentially a CSC x CSC matrix multiplication
                for (int mr = 0; mr < matrix.Rows; ++mr)
                {
                    int m_rstart = matrix.RowIndices[mr], m_rend = matrix.RowIndices[mr + 1];
                    for (int i = m_rstart; i < m_rend; ++i)
                    {
                        int mc = matrix.ColumnIndices[i];
                        T e = matrix.Values[i];

                        int nzci = column_inverseLookup[mc];
                        if (nzci >= 0)
                        {
                            int cstart = _ci[nzci], cend = _ci[nzci + 1];
                            for (int j = cstart; j < cend; ++j)
                            {
                                int d = _di[j];
                                if (is_nz[d])
                                {
                                    sums[d] = provider.Add(sums[d], provider.Multiply(e, _values[j]));
                                }
                                else
                                {
                                    sums[d] = provider.Multiply(e, _values[j]);
                                    is_nz[d] = true;
                                    nz_index[nz_count++] = d;
                                }
                            }
                        }
                    }

                    // Add the fibre 
                    if (nz_count > 0)
                    {
                        nzc.Add(mr);
                        for (int i = 0; i < nz_count; ++i)
                        {
                            int d = nz_index[i];
                            di.Add(d);
                            values.Add(sums[d]);
                            is_nz[d] = false;   // Reset
                        }
                        nz_count = 0; // Reset
                        ci.Add(values.Count);
                    }
                }
                ri[r + 1] = nzc.Count;
            }

            int[] nzc_arr = nzc.ToArray(),
                ci_arr = ci.ToArray(),
                di_arr = di.ToArray();
            T[] values_arr = values.ToArray();

            // Sort the di's/values within each ci
            for (int i = 0; i < nzc_arr.Length; ++i)
            {
                int start = ci_arr[i], end = ci_arr[i + 1];
                Array.Sort(di_arr, values_arr, start, end - start);
            }

            return new CSFSparseTensor<T>(matrix.Rows, _depth, ri, nzc_arr, ci_arr, di_arr, values_arr);
        }

        private CSFSparseTensor<T> Multiply_Mode2(CSRSparseMatrix<T> matrix, IProvider<T> provider)
        {
            // [R, C, D] x [M, D] -> [R, C, M]

            int[] ri = new int[_rows + 1];
            // since this implementation is at least O(RC) anyway, we are not afraid to completely 
            // initialize the column arrays. They will be compressed later. This has the advantage that 
            // _nzc is implicitly represented by the index within the array, also, fast access which we 
            // need.
            int[] ci = new int[_rows * _columns + 1]; 
            FastList<int> di = new FastList<int>(_rows);
            FastList<T> values = new FastList<T>(_rows);
            int nnz = 0; // need to externally store the nz count, since we are not using the FastList .Count member
            

            // Used for converting slices from CSC into CSR format 
            // This array is overwritten for each slice (total |_rows| times)
            int[] depthIndex = new int[_depth + 1];
            int[] nextIndex = new int[_depth];

            // Used to temporarily hold the transpose - the total resize complexity is no more 
            // than O(nnz)
            int[] tempRowIndex = new int[_depth];
            T[] tempValues = new T[_depth];

            // Used to perform the set union during matrix multiplication
            bool[] is_nz = new bool[_columns];
            int[] nz_index = new int[_columns];
            int nz_count = 0;
            T[] sum = new T[_columns];

            // Used to temporarily hold the transpose of the result - the total resize complexity 
            // is also no more than O(nnz(result))
            int[] productRowCounts = new int[_columns + 1];

            // Iterate through the slices
            for (int r = 0; r < _rows; ++r)
            {
                int rstart = _ri[r],
                    rend = _ri[r + 1];

                // Convert C x D slice from CSR format into CSC format for fast sparse multiplication
                // First, count the depth index - directly iterate across all depth indices of this slice
                for (int j = _ci[rstart], cend = _ci[rend]; j < cend; ++j)
                {
                    depthIndex[_di[j]]++;
                }
                // Shift, accumulate depth indices
                for (int i = _depth; i > 0; --i)
                {
                    depthIndex[i] = depthIndex[i - 1];
                }
                depthIndex[0] = 0;
                for (int i = 0; i < _depth; ++i)
                {
                    depthIndex[i + 1] += depthIndex[i];
                }

                Array.Copy(depthIndex, nextIndex, _depth);

                // Insert values into CSC order into tempRowIndex
                if (tempRowIndex.Length < depthIndex[_depth])
                {
                    Array.Resize(ref tempRowIndex, depthIndex[_depth]);
                    Array.Resize(ref tempValues, depthIndex[_depth]);
                }

                for (int i = rstart; i < rend; ++i)
                {
                    int c = _nzc[i];
                    for (int j = _ci[i], cend = _ci[i + 1]; j < cend; ++j)
                    {
                        int index = nextIndex[_di[j]]++;
                        tempRowIndex[index] = c;
                        tempValues[index] = _values[j];
                    }
                }

                // First, compute the non-zero row counts of the resulting product slice, storing it 
                // inside tempInvColumnIndex. This symbolic step allows us to directly store 
                // the computed numerical values into the product tensor storage
                Array.Clear(productRowCounts, 0, _columns + 1);

                for (int mr = 0; mr < matrix.Rows; ++mr)
                {
                    // For each nz column within row mr in the matrix...
                    for (int mi = matrix.RowIndices[mr], cend = matrix.RowIndices[mr + 1]; mi < cend; ++mi)
                    {
                        int mc = matrix.ColumnIndices[mi];

                        // iterate down the corresponding column of the tensor slice to compute
                        // the set union of the nz pattern
                        for (int i = depthIndex[mc], end = depthIndex[mc + 1]; i < end; ++i)
                        {
                            int c = tempRowIndex[i];
                            if (!is_nz[c])
                            {
                                is_nz[c] = true;
                                nz_index[nz_count++] = c;
                            }
                        }

                        // Increment the nz column counts of the resulting product, and clear the 
                        // set union data structures as we go
                        for (int i = 0; i < nz_count; ++i)
                        {
                            int c = nz_index[i];
                            productRowCounts[c]++;
                            is_nz[c] = false;
                        }
                        nz_count = 0;
                    }
                }

                // Shift and accumulate the nz row counts
                for (int i = _columns - 1; i >= 0; --i)
                {
                    productRowCounts[i + 1] = productRowCounts[i];
                }
                productRowCounts[0] = 0;
                for (int i = 0; i < _columns; ++i)
                {
                    productRowCounts[i + 1] += productRowCounts[i];
                }

                nnz += productRowCounts[_columns];

                Debug.Write("product row counts before:\t");
                productRowCounts.Print();

                // Ensure that both di and values have the necessary capacity to store the new nz entries
                di.EnsureCapacity(nnz);
                values.EnsureCapacity(nnz);

                // Begin the multiplication
                for (int mr = 0; mr < matrix.Rows; ++mr)
                {
                    for (int mi = matrix.RowIndices[mr], cend = matrix.RowIndices[mr + 1]; mi < cend; ++mi)
                    {
                        int mc = matrix.ColumnIndices[mi];
                        T e = matrix.Values[mi];

                        // Dense BLAS can be used here...
                        for (int i = depthIndex[mc], end = depthIndex[mc + 1]; i < end; ++i)
                        {
                            int c = tempRowIndex[i];
                            if (is_nz[c])
                            {
                                sum[c] = provider.Add(sum[c], provider.Multiply(e, tempValues[i]));
                            }
                            else
                            {
                                sum[c] = provider.Multiply(e, tempValues[i]);
                                is_nz[c] = true;
                                nz_index[nz_count++] = c;
                            }
                        }
                    }

                    // We need to convert this back into CSC format
                    // which, since we have the starting row indices of the product matrix slice, 
                    // just means we need to insert in careful order.
                    // One big upside is we no longer need to sort the nz_index array since we are 
                    // already iterating in the correct row order.
                    int ci_offset = r * _columns;
                    for (int i = 0; i < nz_count; ++i)
                    {
                        int d = nz_index[i];
                        // Use the precomputed row indexes to insert directly into di, values
                        // The capacities of di, values have been ensured beforehand. 
                        // This implicitly inserts in the correct transposed order.
                        int index = productRowCounts[d]++;
                        di.Values[index] = d;
                        values.Values[index] = sum[d];

                        // Update the ci values (these will be accumulated later)
                        ci[ci_offset + d]++;

                        is_nz[d] = false;   // Reset
                    }
                    nz_count = 0;   // Reset
                }
                Array.Clear(depthIndex, 0, _depth);


                Debug.Write("product row counts after:\t");
                productRowCounts.Print();
            }

            Debug.WriteLine("ci before");
            ci.Print();

            // Compress the ci array, compute the nzc index, and update the ri array
            int count_nzc = 0;
            for (int i = 0; i < ci.Length; ++i)
            {
                if (ci[i] != 0)
                {
                    ++count_nzc;
                }
            }

            int[] nzc = new int[count_nzc];
            int ci_sum = 0, k2 = 0;
            for (int r = 0, k1 = 0; r < _rows; ++r)
            {
                for (int c = 0; c < _columns; ++c, ++k1)
                {
                    if (ci[k1] != 0)
                    {
                        nzc[k2] = c;
                        ci_sum += ci[k1];
                        ci[k2++] = ci_sum;
                    }
                }
                ri[r + 1] = k2;
            }

            // Shift ci
            for (int i = k2; i > 0; --i)
            {
                ci[i] = ci[i - 1];
            }
            ci[0] = 0;

            // Resize the ci array
            Array.Resize(ref ci, k2 + 1);


            int[] di_array = new int[nnz];
            T[] values_array = new T[nnz];
            Array.Copy(di.Values, di_array, nnz);
            Array.Copy(values.Values, values_array, nnz);

            // Requires manual copying
            Debug.Assert(nnz == ci[k2]);

            ri.Print();
            nzc.Print();
            ci.Print();
            di_array.Print();
            values_array.Print();

            return new CSFSparseTensor<T>(_columns, matrix.Rows, ri, nzc, ci, di_array, values_array);
        }

        private CSRSparseMatrix<T> Unfold_Mode0()
        {
            int nnz = _values.Length;
            int[] rowIndex = new int[_rows + 1];
            int[] columnIndex = new int[nnz];
            T[] values = new T[nnz];

            int start = 0, end = 0;
            for (int r = 0; r < _rows; ++r)
            {
                for (int i = _ri[r], rend = _ri[r + 1]; i < rend; ++i)
                {
                    int c = _nzc[i];
                    for (int j = _ci[i], cend = _ci[i + 1]; j < cend; ++j)
                    {
                        int d = _di[j];
                        columnIndex[end] = d * _columns + c;
                        values[end++] = _values[j];
                    }
                }

                // Sort
                Array.Sort(columnIndex, values, start, end - start);
                start = end;
                rowIndex[r + 1] = end;
            }
            return new CSRSparseMatrix<T>(_columns * _depth, rowIndex, columnIndex, values);
        }

        private CSRSparseMatrix<T> Unfold_Mode1()
        {
            int nnz = _values.Length;
            int[] rowIndex = new int[_columns + 1];
            int[] columnIndex = new int[nnz];
            T[] values = new T[nnz];

            // Calculate the column counts
            for (int r = 0; r < _rows; ++r)
            {
                for (int i = _ri[r], rend = _ri[r + 1]; i < rend; ++i)
                {
                    rowIndex[_nzc[i]] += (_ci[i + 1] - _ci[i]);
                }
            }

            // Shift and accumulate
            for (int i = _columns - 1; i >= 0; --i)
            {
                rowIndex[i + 1] = rowIndex[i];
            }
            rowIndex[0] = 0;
            for (int i = 0; i < _columns; ++i)
            {
                rowIndex[i + 1] += rowIndex[i];
            }

            int[] nextIndex = rowIndex.Copy();
            for (int r = 0; r < _rows; ++r)
            {
                for (int i = _ri[r], rend = _ri[r + 1]; i < rend; ++i)
                {
                    int c = _nzc[i];
                    for (int j = _ci[i], cend = _ci[i + 1]; j < cend; ++j)
                    {
                        int index = nextIndex[c]++;
                        columnIndex[index] = _di[j] * _rows + r;
                        values[index] = _values[j];
                    }
                }
            }

            for (int r = 0; r < _columns; ++r)
            {
                int start = rowIndex[r], end = rowIndex[r + 1];
                Array.Sort(columnIndex, values, start, end - start);
            }

            return new CSRSparseMatrix<T>(_rows * _depth, rowIndex, columnIndex, values);
        }

        /// <summary>
        /// O(nnz + CD)
        /// On most tensors of sufficiently high density, this implementation outperforms the equivalent 
        /// method Unfold()
        /// However, because of the potential quadratic complexity due to the term CD, this method stops 
        /// outperforming for size >= 800^3 (any density)
        /// </summary>
        /// <returns></returns>
        private CSRSparseMatrix<T> Unfold_Mode0_Variant1()
        {

            int nnz = _values.Length;

            // Compute the number of entries per column
            int CD = _columns * _depth; // Potentially dangerous for large tensors
            int[] temp_colIndex = new int[CD + 1];
            for (int r = 0; r < _rows; ++r)
            {
                for (int i = _ri[r], rend = _ri[r + 1]; i < rend; ++i)
                {
                    int c = _nzc[i];
                    for (int j = _ci[i], cend = _ci[i + 1]; j < cend; ++j)
                    {
                        ++temp_colIndex[_di[j] * _columns + c];
                    }
                }
            }

            // Shift and accumulate
            for (int i = CD; i > 0; --i)
            {
                temp_colIndex[i] = temp_colIndex[i - 1];
            }
            temp_colIndex[0] = 0;
            for (int i = 0; i < CD; ++i)
            {
                temp_colIndex[i + 1] += temp_colIndex[i];
            }

            // Create a temporary CSC representation
            int[] temp_rowIndex = new int[nnz];
            T[] temp_values = new T[nnz];
            for (int r = 0; r < _rows; ++r)
            {
                for (int i = _ri[r], rend = _ri[r + 1]; i < rend; ++i)
                {
                    int c = _nzc[i];
                    for (int j = _ci[i], cend = _ci[i + 1]; j < cend; ++j)
                    {
                        int col = _di[j] * _columns + c,
                            index = temp_colIndex[col]++;

                        temp_rowIndex[index] = r;
                        temp_values[index] = _values[j];
                    }
                }
            }

            // Restore column counts
            for (int i = CD; i > 0; --i)
            {
                temp_colIndex[i] = temp_colIndex[i - 1];
            }
            temp_colIndex[0] = 0;

            // Calculate the transpose to obtain a CSR representation
            CSRHelper<T>.Transpose(temp_colIndex, temp_rowIndex, temp_values, out int[] rowIndex, out int[] colIndex, out T[] values, CD, _rows);
            return new CSRSparseMatrix<T>(CD, colIndex, rowIndex, values);
        }

        /// <summary>
        /// Mode-0 matrix unfolding implementation which is also quadratic O(nnz + RC)
        /// This method is approx 6 times faster than Unfold_Mode1_Variant0 (across all densities and n <= 800)
        /// Some further optimizations should be possible here too
        /// </summary>
        /// <returns></returns>
        private CSRSparseMatrix<T> Unfold_Mode0_Variant2()
        {
            int nnz = _values.Length;
            int[] rowIndex = new int[_rows + 1],
                columnIndex = new int[nnz];
            T[] values = new T[nnz];

            // slice_start_index[d] specifies the starting index of slice [:,:,d]
            int[] slice_start_index = new int[_depth];
            int offset = 0;
            for (int r = 0; r < _rows; ++r)
            {
                int rstart = _ri[r], rend = _ri[r + 1];

                // slice_start_index[d] <- |tensor[r,:, d]|
                for (int i = rstart; i < rend; ++i)
                {
                    for (int j = _ci[i], cend = _ci[i + 1]; j < cend; ++j)
                    {
                        int d = _di[j];
                        slice_start_index[d]++;
                    }
                }

                // shift and accumulate start indexes
                for (int d = _depth - 1; d > 0; --d)
                {
                    slice_start_index[d] = slice_start_index[d - 1];
                }
                slice_start_index[0] = 0;
                for (int d = 1; d < _depth; ++d)
                {
                    slice_start_index[d] += slice_start_index[d - 1];
                }

                // Insert values 
                for (int i = rstart; i < rend; ++i)
                {
                    int c = _nzc[i];
                    for (int j = _ci[i], cend = _ci[i + 1]; j < cend; ++j)
                    {
                        int d = _di[j];
                        int index = offset + slice_start_index[d]++;
                        columnIndex[index] = d * _columns + c;
                        values[index] = _values[j];
                    }
                }

                rowIndex[r + 1] = rowIndex[r] + slice_start_index[_depth - 1];
                offset = rowIndex[r + 1];

                // Reset 
                Array.Clear(slice_start_index, 0, _depth);
            }

            //rowIndex.Print();
            //columnIndex.Print();
            //values.Print();

            return new CSRSparseMatrix<T>(_columns * _depth, rowIndex, columnIndex, values);
        }

        /// <summary>
        /// This method is ~ 3x faster than Unfold_Mode1() but performance advantage degrades for larger
        /// matrices.
        /// </summary>
        /// <returns></returns>
        private CSRSparseMatrix<T> Unfold_Mode1_Variant1()
        {
            // This is a O(nnz) solution... but we need to perform a transpose

            int nnz = _values.Length;
            int[] rowIndex = new int[_rows * _depth + 1];
            int[] columnIndex = new int[nnz];
            T[] values = new T[nnz];

            // Calculate the nz pattern
            for (int r = 0; r < _rows; ++r)
            {
                int r_1 = r + 1;
                for (int i = _ri[r], rend = _ri[r_1]; i < rend; ++i)
                {
                    for (int j = _ci[i], cend = _ci[i + 1]; j < cend; ++j)
                    {
                        int d = _di[j];
                        rowIndex[d * _rows + r_1]++;
                    }
                }
            }

            int R = rowIndex.Length;
            for (int r = 1; r < R; ++r)
            {
                rowIndex[r] += rowIndex[r - 1];
            }

            int[] startIndex = rowIndex.Copy();
            for (int r = 0; r < _rows; ++r)
            {
                for (int i = _ri[r], rend = _ri[r + 1]; i < rend; ++i)
                {
                    int c = _nzc[i];
                    for (int j = _ci[i], cend = _ci[i + 1]; j < cend; ++j)
                    {
                        int index = startIndex[_di[j] * _rows + r]++;
                        values[index] = _values[j];
                        columnIndex[index] = c;
                    }
                }
            }

            return new CSRSparseMatrix<T>(_columns, rowIndex, columnIndex, values).Transpose();
        }

        /// <summary>
        /// Sets a mode-1 fibre of the tensor to be equal in value to a vector.
        /// The dimension of the vector must match the number of rows of this tensor.
        /// The values in the fibre [:, columnIndex, depthIndex] will be set to the value of the vector.
        /// </summary>
        /// <note>This method is experimental - for the production version use SetMode1Fibre() instead</note>
        /// <param name="columnIndex">The column index of the fibre to set. Must be a valid index within the columns of this tensor.</param>
        /// <param name="depthIndex">The depth index of the fibre to set. Must be a valid index within the depth of this tensor.</param>
        /// <param name="fibre">A dense vector containing values to copy into this tensor.</param>
        private void SetMode1Fibre_1(int columnIndex, int depthIndex, DenseVector<T> fibre)
        {
            if (columnIndex < 0 || columnIndex >= _columns)
            {
                throw new ArgumentOutOfRangeException(nameof(columnIndex));
            }
            if (depthIndex < 0 || depthIndex >= _depth)
            {
                throw new ArgumentOutOfRangeException(nameof(depthIndex));
            }
            if (fibre is null)
            {
                throw new ArgumentNullException(nameof(fibre));
            }
            if (fibre.Dimension != _rows)
            {
                throw new InvalidOperationException("Dimension of vector does not match the number of rows of the tensor.");
            }


            // Store the indexes of the column, depth dimensions
            int[] c_indexes = new int[_rows],
                d_indexes = new int[_rows];

            for (int r = 0; r < _rows; ++r)
            {
                int cindex = Array.BinarySearch(_nzc, _ri[r], _ri[r + 1], columnIndex);
                c_indexes[r] = cindex;

                if (cindex >= 0)
                {
                    d_indexes[r] = Array.BinarySearch(_di, _ci[cindex], _ci[cindex + 1], depthIndex);
                }
                else
                {
                    d_indexes[r] = -1;
                }
            }

            int missing_c = 0,
                missing_d = 0;

            for (int r = 0; r < _rows; ++r)
            {
                if (c_indexes[r] < 0) ++missing_c;
                if (d_indexes[r] < 0) ++missing_d;
            }

            T[] vect = fibre.Values;
            if (missing_c == 0)
            {
                if (missing_d == 0)
                {
                    // No additional columns or depths added - just replace the values
                    for (int r = 0; r < _rows; ++r)
                    {
                        _values[d_indexes[r]] = vect[r];
                    }
                    return;
                }
                else
                {
                    // No additional columns added but new depths added
                    int offset = 0;
                    int[] di = new int[missing_d + _di.Length];
                    T[] values = new T[di.Length];

                    for (int r = 0; r < _rows; ++r)
                    {
                        // start and end of slice [r,:,:] respectively (within _di, _value arrays respectively)
                        int rstart = _ri[r], rend = _ri[r + 1];
                        int start = _ci[rstart], end = _ci[rend];

                        if (d_indexes[r] < 0)
                        {
                            int insertion_index = -d_indexes[r];

                            // Copy _di[start, insertion_index) -> di[start + offset, insertion_index + offset)
                            Array.Copy(_di, start, di, offset + start, insertion_index - start);
                            Array.Copy(_values, start, di, offset + start, insertion_index - start);

                            // Increment column boundaries with the number of inserted elements
                            int c_index = c_indexes[r];
                            Util.Increment(_ci, rstart, c_index + 1, offset);

                            // Copy vector[r] -> di[insertion_index + offset]
                            di[insertion_index + offset] = depthIndex;
                            values[insertion_index + offset] = vect[r];

                            ++offset;

                            // Copy _di[insertion_index, end) -> di[insertion_index + offset, end + offset)
                            Array.Copy(_di, insertion_index, di, offset + insertion_index, end - insertion_index);
                            Array.Copy(_values, insertion_index, values, offset + insertion_index, end - insertion_index);

                            // Increment column index boundaries with the number of inserted elements
                            Util.Increment(_ci, c_index + 1, rend, offset);
                        }
                        else
                        {
                            // Nothing to do except update value and update column boundaries
                            Util.Increment(_ci, rstart, rend, offset);

                            Array.Copy(_di, start, di, offset + start, end - start);
                            Array.Copy(_values, start, values, offset + start, end - start);

                            // Update value 
                            values[offset + d_indexes[r]] = vect[r];
                        }
                    }

                    _di = di;
                    _values = values;

                    // Update the last ci
                    _ci[_nzc.Length] = _values.Length;

                    return;
                }
            }
            else
            {
                // Create new storage
                int[] nzc = new int[_nzc.Length + missing_c];
                int[] ci = new int[_ci.Length + missing_c];
                int[] di = new int[_di.Length + missing_d];
                T[] values = new T[_values.Length + missing_d];

                int c_offset = 0,
                    d_offset = 0;

                for (int r = 0; r < _rows; ++r)
                {
                    int rstart = _ri[r], rend = _ri[r + 1],
                        cstart = _ci[rstart], cend = _ci[rend];
                    if (c_indexes[r] < 0)
                    {
                        int c_insert_index = -c_indexes[r];

                        // Update nzc, ci prior to newly inserted column
                        Array.Copy(_nzc, rstart, nzc, rstart + c_offset, c_insert_index - rstart);
                        for (int i = rstart; i <= c_insert_index; ++i)
                        {
                            ci[i + c_offset] = _ci[i] + d_offset;
                        }

                        // Insert new column
                        nzc[c_insert_index + c_offset] = columnIndex;
                        ci[c_insert_index + c_offset + 1] = ci[c_insert_index + c_offset] + 1;
                        ++c_offset;

                        // Insert di, value
                        int d_insert_index = _ci[c_insert_index - 1];
                        Array.Copy(_di, cstart, di, cstart + d_offset, d_insert_index - cstart);
                        Array.Copy(_values, cstart, values, cstart + d_offset, d_insert_index - cstart);
                        di[d_insert_index] = depthIndex;
                        values[d_insert_index] = vect[r];
                        ++d_offset;
                        Array.Copy(_di, d_insert_index, di, d_insert_index + d_offset, cend - d_insert_index);
                        Array.Copy(_values, d_insert_index, values, d_insert_index + d_offset, cend - d_insert_index);

                        // Update nzc, ci after newly inserted column
                        Array.Copy(_nzc, c_insert_index, nzc, c_insert_index + c_offset, rend - c_insert_index);
                        for (int i = c_insert_index; i < rend; ++i)
                        {
                            ci[i + c_offset + 1] = _ci[i] + d_offset;
                        }
                    }
                    else
                    {
                        throw new NotImplementedException(); // TODO
                    }
                }
            }
        }

        private void SetMode1Fibre_2(int columnIndex, int depthIndex, DenseVector<T> fibre)
        {
            if (columnIndex < 0 || columnIndex >= _columns)
            {
                throw new ArgumentOutOfRangeException(nameof(columnIndex));
            }
            if (depthIndex < 0 || depthIndex >= _depth)
            {
                throw new ArgumentOutOfRangeException(nameof(depthIndex));
            }
            if (fibre is null)
            {
                throw new ArgumentNullException(nameof(fibre));
            }
            if (fibre.Dimension != _rows)
            {
                throw new InvalidOperationException("Dimension of vector does not match the number of rows of the tensor.");
            }

            // The numbe of columns, depths we need to add
            int new_c = 0, new_d = 0;

            // Iterate through the nz entries to count the number of entries to add 
            for (int r = 0; r < _rows; ++r)
            {
                int rstart = _ri[r], rend = _ri[r + 1];
                int cindex = Array.BinarySearch(_nzc, rstart, rend - rstart, columnIndex);
                if (cindex >= 0)
                {
                    int cstart = _ci[cindex], cend = _ci[cindex + 1];
                    if (Array.BinarySearch(_di, cstart, cend - cstart, depthIndex) < 0)
                    {
                        ++new_d;
                    }
                }
                else
                {
                    ++new_c;
                    ++new_d;
                }
            }

            // Create new storage
            int[] nzc = new int[new_c + _nzc.Length],
                ci = new int[new_c + _ci.Length],
                di = new int[new_d + _di.Length];
            T[] values = new T[new_d + _values.Length],
                vect_values = fibre.Values;

            int nnz = 0;
            for (int r = 0, i1 = 0; r < _rows; ++r)
            {
                T v = vect_values[r];

                // Keep track of whether the vector for this row has already been added
                bool inserted_value = false;

                // Iterate through the columns of this row
                for (int i = _ri[r], rend = _ri[r + 1]; i < rend; ++i)
                {
                    int c = _nzc[i];

                    if (c > columnIndex && !inserted_value)
                    {
                        // insert new column @columnIndex
                        nzc[i1] = columnIndex;
                        ci[i1++] = nnz;

                        // insert new value @depthIndex
                        di[nnz] = depthIndex;
                        values[nnz++] = v;
                        inserted_value = true;
                    }

                    nzc[i1] = c;
                    ci[i1++] = nnz;

                    for (int j = _ci[i], cend = _ci[i + 1]; j < cend; ++j)
                    {
                        int d = _di[j];

                        if (c == columnIndex && d > depthIndex && !inserted_value)
                        {
                            // insert new value @depthIndex
                            di[nnz] = depthIndex;
                            values[nnz++] = v;
                            inserted_value = true;
                        }

                        if (c == columnIndex && d == depthIndex)
                        {
                            // Copy over the values
                            di[nnz] = d;
                            values[nnz++] = v;
                            inserted_value = true;
                        }
                        else
                        {
                            // Copy over the values
                            di[nnz] = d;
                            values[nnz++] = _values[j];
                        }
                    }

                    // If value was not already added, add it at the end
                    if (c == columnIndex && !inserted_value)
                    {
                        di[nnz] = depthIndex;
                        values[nnz++] = v;
                        inserted_value = true;
                    }
                }

                // If column was not already added, add it at the end
                if (!inserted_value)
                {
                    // insert new column @columnIndex
                    nzc[i1] = columnIndex;
                    ci[i1] = nnz;

                    // insert new value @depthIndex
                    di[nnz] = depthIndex;
                    values[nnz] = v;

                    ++nnz;
                    ++i1;
                }

                // Remember to update the row indices
                _ri[r + 1] = i1;


                //Debug.WriteLine("row: " + r + " " + new string('=', 100));

                //_ri.Print();
                //nzc.Print();
                //ci.Print();
                //di.Print();
                //values.Print();
            }
            ci[nzc.Length] = nnz;

            // Replace values
            _nzc = nzc;
            _ci = ci;
            _di = di;
            _values = values;
        }

        /// <summary>
        /// The other SumOver method is approximately 3x as fast as this implementation. 
        /// </summary>
        /// <param name="mode"></param>
        /// <param name="provider"></param>
        /// <returns></returns>
        private CSRSparseMatrix<T> SumOver1(int mode, IProvider<T> provider)
        {
            if (mode < 0 || mode > 2) throw new ArgumentOutOfRangeException(nameof(mode));
            if (provider is null) throw new ArgumentNullException(nameof(provider));

            if (mode == 0)
            {
                // Count the slice with the highest number of nz
                int max_nz = 0;
                for (int r = 0; r < _rows; ++r)
                {
                    int nz = _ci[_ri[r + 1]] - _ci[_ri[r]];
                    if (nz > max_nz)
                    {
                        max_nz = nz;
                    }
                }

                Dictionary<long, T> sums = new Dictionary<long, T>(max_nz);
                long ld = _depth;
                for (int r = 0; r < _rows; ++r)
                {
                    for (int i = _ri[r], rend = _ri[r + 1]; i < rend; ++i)
                    {
                        long c = _nzc[i] * ld;
                        for (int j = _ci[i], cend = _ci[i + 1]; j < cend; ++j)
                        {
                            long key = c + _di[j];
                            if (sums.ContainsKey(key))
                            {
                                sums[key] = provider.Add(sums[key], _values[j]);
                            }
                            else
                            {
                                sums[key] = _values[j];
                            }
                        }
                    }
                }

                int nnz = sums.Count;
                long[] keys = new long[nnz];
                T[] values = new T[nnz];
                int k = 0;
                foreach (KeyValuePair<long, T> e in sums)
                {
                    keys[k] = e.Key;
                    values[k++] = e.Value;
                }

                Array.Sort(keys, values);

                int[] rowIndices = new int[_columns + 1],
                    columnIndices = new int[nnz];

                for (int i = 0; i < nnz; ++i)
                {
                    long key = keys[i];
                    int r = (int)(key / _depth),
                        c = (int)(key % _depth);
                    ++rowIndices[r];
                    columnIndices[i] = c;
                }

                // Shift and accumulate
                for (int i = _columns; i > 0; --i)
                {
                    rowIndices[i] = rowIndices[i - 1];
                }
                rowIndices[0] = 0;
                for (int i = 0; i < _columns; ++i)
                {
                    rowIndices[i + 1] += rowIndices[i];
                }

                return new CSRSparseMatrix<T>(_depth, rowIndices, columnIndices, values);
            }
            else
            {
                return SumOver(mode, provider);
            }
        }

        #endregion


        #region Public methods 

        public CSFSparseTensor<T> Add2(CSFSparseTensor<T> tensor, IProvider<T> provider)
        {
            if (tensor is null)
            {
                throw new ArgumentNullException(nameof(tensor));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            AssertDimensionsMatch(GetDimensions(), tensor.GetDimensions());

            int[] rowIndices = new int[_rows + 1];
            List<int> nzc = new List<int>();
            List<int> ci = new List<int>() { 0 };
            List<int> depthIndices = new List<int>();
            List<T> values = new List<T>();

            for (int r = 0; r < _rows; ++r)
            {
                int r_end1 = _ri[r + 1],
                    r_end2 = tensor._ri[r + 1];

                // Calculate the number of columns in the sum of this row
                // Since both column indices are sorted, we can simply 
                // co-iterate without having to use a set union operation
                int i = _ri[r], j = tensor._ri[r];
                while (i < r_end1 || j < r_end2)
                {
                    int c1 = i < r_end1 ? _nzc[i] : int.MaxValue;
                    int c2 = j < r_end2 ? tensor._nzc[j] : int.MaxValue;

                    if (c1 > c2)
                    {
                        // Tensor 2 contains a column that is not present in tensor1
                        nzc.Add(c2);

                        int c_end = tensor._ci[j + 1];
                        for (int k = tensor._ci[j]; k < c_end; ++k)
                        {
                            depthIndices.Add(tensor._di[k]);
                            values.Add(tensor._values[k]);
                        }
                        ci.Add(values.Count);

                        ++j;
                    }
                    else if (c1 < c2)
                    {
                        // Tensor 1 contains a column that is not present in tensor2
                        nzc.Add(c1);

                        int c_end = _ci[i + 1];
                        for (int k = _ci[i]; k < c_end; ++k)
                        {
                            depthIndices.Add(_di[k]);
                            values.Add(_values[k]);
                        }
                        ci.Add(values.Count);

                        ++i;
                    }
                    else
                    {
                        nzc.Add(c1);

                        // To calculate the number of elements in a joint column, need to co-iterate
                        int c_end1 = _ci[i + 1],
                            c_end2 = tensor._ci[j + 1],
                            d1 = _ci[i],
                            d2 = tensor._ci[j];

                        while (d1 < c_end1 || d2 < c_end2)
                        {
                            int i1 = d1 < c_end1 ? _di[d1] : int.MaxValue;
                            int i2 = d2 < c_end2 ? tensor._di[d2] : int.MaxValue;

                            if (i1 < i2)
                            {
                                depthIndices.Add(i1);
                                values.Add(_values[d1]);
                                ++d1;
                            }
                            else if (i1 > i2)
                            {
                                depthIndices.Add(i2);
                                values.Add(tensor._values[d2]);
                                ++d2;
                            }
                            else
                            {
                                depthIndices.Add(i1);
                                values.Add(provider.Add(_values[d1], tensor._values[d2]));
                                ++d1;
                                ++d2;
                            }
                        }

                        ci.Add(values.Count);
                        ++i; ++j;
                    }
                }

                rowIndices[r + 1] = nzc.Count;
            }

            return new CSFSparseTensor<T>(_columns, _depth, rowIndices, nzc.ToArray(), ci.ToArray(), depthIndices.ToArray(), values.ToArray());
        }

        /// <summary>
        /// Return the sum of this tensor with another tensor, using the specified provider. 
        /// A new tensor is returned, and neither tensor is altered by this method
        /// </summary>
        /// <note>Approximately 25% faster than Add2 and uses less memory</note>
        /// <param name="tensor"></param>
        /// <param name="provider"></param>
        /// <returns></returns>
        public CSFSparseTensor<T> Add(CSFSparseTensor<T> tensor, IProvider<T> provider)
        {
            if (tensor is null)
            {
                throw new ArgumentNullException(nameof(tensor));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            AssertDimensionsMatch(GetDimensions(), tensor.GetDimensions());

            // Counting operations - determine the size of required vectors
            // nnz: the number of non-zero entries in the sum
            // nz_col: the number of columns in the sum containing at least 1 non-zero entry
            CalculateNonZeroPatternOfUnionWith(tensor, out int nnz, out int nz_col);

            // Create storage
            int[] rowIndices = new int[_rows + 1],
                nzc = new int[nz_col],
                ci = new int[nz_col + 1],
                di = new int[nnz];
            T[] values = new T[nnz];

            int kc = 0, ki = 0;
            for (int r = 0; r < _rows; ++r)
            {
                int r_end1 = _ri[r + 1],
                    r_end2 = tensor._ri[r + 1];

                // Calculate the number of columns in the sum of this row
                // Since both column indices are sorted, we can simply 
                // co-iterate without having to use a set union operation
                int i = _ri[r], j = tensor._ri[r];
                while (i < r_end1 || j < r_end2)
                {
                    int c1 = i < r_end1 ? _nzc[i] : int.MaxValue;
                    int c2 = j < r_end2 ? tensor._nzc[j] : int.MaxValue;

                    if (c1 > c2)
                    {
                        // Tensor 2 contains a column that is not present in tensor1
                        nzc[kc] = c2;

                        int c_end = tensor._ci[j + 1];
                        for (int k = tensor._ci[j]; k < c_end; ++k, ++ki)
                        {
                            di[ki] = tensor._di[k];
                            values[ki] = tensor._values[k];
                        }
                        ++j;
                    }
                    else if (c1 < c2)
                    {
                        // Tensor 1 contains a column that is not present in tensor2
                        nzc[kc] = c1;

                        int c_end = _ci[i + 1];
                        for (int k = _ci[i]; k < c_end; ++k, ++ki)
                        {
                            di[ki] = _di[k];
                            values[ki] = _values[k];
                        }
                        ++i;
                    }
                    else
                    {
                        nzc[kc] = c1;

                        // To calculate the number of elements in a joint column, need to co-iterate
                        int c_end1 = _ci[i + 1],
                            c_end2 = tensor._ci[j + 1],
                            i1 = _ci[i],
                            i2 = tensor._ci[j];

                        while (i1 < c_end1 || i2 < c_end2)
                        {
                            int d1 = i1 < c_end1 ? _di[i1] : int.MaxValue;
                            int d2 = i2 < c_end2 ? tensor._di[i2] : int.MaxValue;

                            if (d1 < d2)
                            {
                                di[ki] = d1;
                                values[ki] = _values[i1];
                                ++i1;
                            }
                            else if (d1 > d2)
                            {
                                di[ki] = d2;
                                values[ki] = tensor._values[i2];
                                ++i2;
                            }
                            else
                            {
                                di[ki] = d1;
                                values[ki] = provider.Add(_values[i1], tensor._values[i2]);
                                ++i1;
                                ++i2;
                            }
                            ++ki;
                        }
                        ++i; ++j;
                    }
                    ci[++kc] = ki;
                }
                rowIndices[r + 1] = kc;
            }
            return new CSFSparseTensor<T>(_columns, _depth, rowIndices, nzc, ci, di, values);
        }

        /// <summary>
        /// Adds this tensor to another tensor using the specified level-1 BLAS provider.
        /// This method is approximately 10% faster than other equivalent Add method. 
        /// </summary>
        /// <param name="tensor"></param>
        /// <param name="blas"></param>
        /// <returns></returns>
        public CSFSparseTensor<T> Add(CSFSparseTensor<T> tensor, ISparseBLAS1<T> blas)
        {
            if (tensor is null)
            {
                throw new ArgumentNullException(nameof(tensor));
            }
            if (blas is null)
            {
                throw new ArgumentNullException(nameof(blas));
            }
            AssertDimensionsMatch(GetDimensions(), tensor.GetDimensions());

            CalculateNonZeroPatternOfUnionWith(tensor, out int nnz, out int nz_col);

            // Create storage
            int[] rowIndices = new int[_rows + 1],
                nzc = new int[nz_col],
                ci = new int[nz_col + 1],
                di = new int[nnz];
            T[] values = new T[nnz];

            int kc = 0, ki = 0;
            for (int r = 0; r < _rows; ++r)
            {
                int r_end1 = _ri[r + 1],
                    r_end2 = tensor._ri[r + 1];

                // Calculate the number of columns in the sum of this row
                // Since both column indices are sorted, we can simply 
                // co-iterate without having to use a set union operation
                int i = _ri[r], j = tensor._ri[r];
                while (i < r_end1 || j < r_end2)
                {
                    int c1 = i < r_end1 ? _nzc[i] : int.MaxValue;
                    int c2 = j < r_end2 ? tensor._nzc[j] : int.MaxValue;

                    if (c1 > c2)
                    {
                        // Tensor 2 contains a column that is not present in tensor1
                        nzc[kc] = c2;

                        int c_end = tensor._ci[j + 1];
                        for (int k = tensor._ci[j]; k < c_end; ++k, ++ki)
                        {
                            di[ki] = tensor._di[k];
                            values[ki] = tensor._values[k];
                        }
                        ++j;
                    }
                    else if (c1 < c2)
                    {
                        // Tensor 1 contains a column that is not present in tensor2
                        nzc[kc] = c1;

                        int c_end = _ci[i + 1];
                        for (int k = _ci[i]; k < c_end; ++k, ++ki)
                        {
                            di[ki] = _di[k];
                            values[ki] = _values[k];
                        }
                        ++i;
                    }
                    else
                    {
                        nzc[kc] = c1;
                        ki = blas.ADD(_di, _values, tensor._di, tensor._values, di, values, _ci[i], tensor._ci[j], ki, _ci[i + 1], tensor._ci[j + 1]);
                        ++i; ++j;
                    }
                    ci[++kc] = ki;
                }
                rowIndices[r + 1] = kc;
            }
            return new CSFSparseTensor<T>(_columns, _depth, rowIndices, nzc, ci, di, values);
        }

        /// <summary>
        /// Clear a subtensor block from this tensor
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <param name="columnIndex"></param>
        /// <param name="depthIndex"></param>
        /// <param name="rows"></param>
        /// <param name="columns"></param>
        /// <param name="depth"></param>
        public void Clear(int rowIndex, int columnIndex, int depthIndex, int rows, int columns, int depth)
        {
            if (rowIndex < 0 || rowIndex >= _rows) throw new ArgumentOutOfRangeException(nameof(rowIndex));
            if (columnIndex < 0 || columnIndex >= _columns) throw new ArgumentOutOfRangeException(nameof(columnIndex));
            if (depthIndex < 0 || depthIndex >= _depth) throw new ArgumentOutOfRangeException(nameof(depthIndex));

            int rowEnd = rowIndex + rows,
                colEnd = columnIndex + columns,
                depthEnd = depthIndex + depth;

            if (rows < 0 || rowEnd >= _rows) throw new ArgumentOutOfRangeException(nameof(rows));
            if (columns < 0 || colEnd >= _columns) throw new ArgumentOutOfRangeException(nameof(columns));
            if (depth < 0 || depthEnd >= _depth) throw new ArgumentOutOfRangeException(nameof(depth));

            int ci = 0, di = 0, prev_cnz = 0;
            for (int r = 0; r < _rows; ++r)
            {
                bool skipRow = rowIndex <= r && r < rowEnd;
                for (int i = _ri[r], rend = _ri[r + 1]; i < rend; ++i)
                {
                    int c = _nzc[i];
                    bool skipCol = columnIndex <= c && c < colEnd, any = false;

                    for (int j = _ci[i], cend = _ci[i + 1]; j < cend; ++j)
                    {
                        int d = _di[j];
                        bool skipDepth = depthIndex <= d && d < depthEnd;

                        if (!(skipRow && skipCol && skipDepth))
                        {
                            _di[di] = d;
                            _values[di] = _values[j];
                            ++di;
                            any = true;
                        }
                    }

                    if (any)
                    {
                        _nzc[ci] = c;
                        _ci[ci] = prev_cnz;
                        ++ci;
                        prev_cnz = di;
                    }
                }
                _ri[r + 1] = ci;
            }
            _ci[ci] = prev_cnz; // the last column
        }

        /// <summary>
        /// Remove all entries from this tensor that match a predicate 
        /// </summary>
        /// <param name="predicate">The predicate that returns true if an entry is to be removed.</param>
        public void Clear(Predicate<T> predicate)
        {
            if (predicate is null)
            {
                throw new ArgumentNullException(nameof(predicate));
            }

            int ci = 0, di = 0, prev_cnz = 0;
            for (int r = 0; r < _rows; ++r)
            {
                for (int i = _ri[r], rend = _ri[r + 1]; i < rend; ++i)
                {
                    bool any = false;
                    for (int j = _ci[i], cend = _ci[i + 1]; j < cend; ++j)
                    {
                        if (!predicate(_values[j]))
                        {
                            _di[di] = _di[j];
                            _values[di++] = _values[j];
                            any = true;
                        }
                    }

                    if (any)
                    {
                        _nzc[ci] = _nzc[i];
                        _ci[ci] = prev_cnz;
                        ++ci;
                        prev_cnz = di;
                    }
                }
                _ri[r + 1] = ci;
            }
            _ci[ci] = prev_cnz; // the last column
        }

        /// <summary>
        /// Calculate the tensor contraction along the two specified dimensions.
        /// The size of the tensor along <txt>dimension1</txt> and <txt>dimension2</txt> must be equal.
        /// </summary>
        /// <param name="dimension1">An integer between 0 and 2 inclusive, indicating the 1st dimension
        /// to contract along.</param>
        /// <param name="dimension2">An integer between 0 and 2 inclusive, indicating the 2nd dimension
        /// to contract along. Must be different to <txt>dimension1</txt>.</param>
        /// <param name="provider"></param>
        /// <returns>The tensor contraction as a dense vector.</returns>
        public DenseVector<T> Contract(int dimension1, int dimension2, IProvider<T> provider)
        {
            if (dimension1 < 0 || dimension1 > 2)
            {
                throw new ArgumentOutOfRangeException(nameof(dimension1));
            }
            if (dimension2 < 0 || dimension2 > 2)
            {
                throw new ArgumentOutOfRangeException(nameof(dimension2));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            if (GetDimension(dimension1) != GetDimension(dimension2))
            {
                throw new InvalidOperationException("Sizes of the two dimensions must match.");
            }

            // Determine the index and size of the remaining dimension
            int dim_index = -1, dim_size = -1;
            bool[] contracted_dim = new bool[3];
            contracted_dim[dimension1] = true;
            contracted_dim[dimension2] = true;
            int[] dim_sizes = { _rows, _columns, _depth };
            for (int i = 0; i < 3; ++i)
            {
                if (!contracted_dim[i])
                {
                    dim_index = i;
                    dim_size = dim_sizes[i];
                    break;
                }
            }

            DenseVector<T> contraction = new DenseVector<T>(dim_size);
            T[] values = contraction.Values;

            // Single iteration method - slightly less than O(nnz) because of the log factor 
            // of the binary search
            if (dim_index == 0)
            {
                for (int r = 0; r < _rows; ++r)
                {
                    T sum = provider.Zero;
                    for (int i = _ri[r], rend = _ri[r + 1]; i < rend; ++i)
                    {
                        int c = _nzc[i],
                            cstart = _ci[i],
                            cend = _ci[i + 1],
                            di = Array.BinarySearch(_di, cstart, cend - cstart, c);

                        if (di >= 0)
                        {
                            sum = provider.Add(sum, _values[di]);
                        }
                    }
                    values[r] = sum;
                }
            }
            else if (dim_index == 1)
            {
                for (int r = 0; r < _rows; ++r)
                {
                    for (int i = _ri[r], rend = _ri[r + 1]; i < rend; ++i)
                    {
                        int c = _nzc[i],
                            cstart = _ci[i],
                            cend = _ci[i + 1],
                            di = Array.BinarySearch(_di, cstart, cend - cstart, r);

                        if (di >= 0)
                        {
                            values[c] = provider.Add(values[c], _values[di]);
                        }
                    }
                }
            }
            else
            {
                for (int r = 0; r < _rows; ++r)
                {
                    int r_start = _ri[r],
                        r_end = _ri[r + 1];

                    int ci = Array.BinarySearch(_nzc, r_start, r_end - r_start, r);
                    if (ci >= 0)
                    {
                        int c_start = _ci[ci], c_end = _ci[ci + 1];
                        for (int i = c_start; i < c_end; ++i)
                        {
                            int d = _di[i];
                            values[d] = provider.Add(values[d], _values[i]);
                        }
                    }
                }
            }

            return contraction;
        }

        /// <summary>
        /// Returns the number of entries of this tensor for which the predicate is true.
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public int Count(Predicate<T> predicate)
        {
            if (predicate is null)
            {


                throw new ArgumentNullException(nameof(predicate));
            }

            T zero = new T();
            int count = 0;
            if (predicate(zero))
            {
                count = (_rows * _columns * _depth - _values.Length);
            }

            // Add the symbolic non-zero count
            for (int i = 0; i < _values.Length; ++i)
            {
                if (predicate(_values[i]))
                {
                    ++count;
                }
            }

            return count;
        }

        /// <summary>
        /// Returns the direct sum of this tensor with another tensor.
        /// </summary>
        /// <param name="tensor"></param>
        /// <returns></returns>
        public CSFSparseTensor<T> DirectSum(CSFSparseTensor<T> tensor)
        {
            // Calculate the row indexes
            int[] ri = new int[_rows + tensor._rows + 1];
            Array.Copy(_ri, ri, _rows + 1);
            int r_offset = _ri[_rows], len = tensor._rows;
            for (int i = 0; i <= len; ++i)
            {
                ri[_rows + i] = r_offset + tensor._ri[i];
            }

            int[] nzc = new int[_nzc.Length + tensor._nzc.Length];
            Array.Copy(_nzc, nzc, _nzc.Length);
            len = tensor._nzc.Length;
            for (int i = 0; i < len; ++i)
            {
                nzc[_nzc.Length + i] = _columns + tensor._nzc[i];
            }

            int[] ci = new int[nzc.Length + 1];
            Array.Copy(_ci, ci, _ci.Length);
            int c_offset = _ci[_ci.Length - 1], ci_offset = _ci.Length - 1;
            len = tensor._ci.Length;
            for (int i = 0; i < len; ++i)
            {
                ci[ci_offset + i] = tensor._ci[i] + c_offset;
            }

            int[] di = new int[_di.Length + tensor._di.Length];
            Array.Copy(_di, di, _di.Length);
            len = tensor._di.Length;
            for (int i = 0; i < len; ++i)
            {
                di[_di.Length + i] = tensor._di[i] + _depth;
            }

            T[] values = new T[di.Length];
            Array.Copy(_values, values, _values.Length);
            Array.Copy(tensor._values, 0, values, _values.Length, tensor._values.Length);

            return new CSFSparseTensor<T>(_columns + tensor._columns, _depth + tensor._depth, ri, nzc, ci, di, values);
        }

        /// <summary>
        /// Apply a function to each symbolic non-zero value, returning a tensor whose non-zero values are 
        /// equal to the respective outputs of the function. This method does not alter this matrix.
        /// </summary>
        /// <typeparam name="F"></typeparam>
        /// <param name="function"></param>
        /// <returns></returns>
        public CSFSparseTensor<F> Elementwise<F>(Func<T, F> function) where F : new()
        {
            if (function is null)
            {
                throw new ArgumentNullException(nameof(function));
            }

            F[] fvalues = new F[_values.Length];
            for (int i = 0; i < _values.Length; ++i)
            {
                fvalues[i] = function(_values[i]);
            }

            return new CSFSparseTensor<F>(_columns, _depth, _ri.Copy(), _nzc.Copy(), _ci.Copy(), _di.Copy(), fvalues);
        }

        /// <summary>
        /// Returns whether this tensor is equal in value to another tensor. 
        /// In the elementwise comparison, symbolically zero entries are assumed to take the value <txt>new T()</txt>.
        /// If the tensor is <txt>null</txt>, this method returns false.
        /// </summary>
        /// <param name="tensor"></param>
        /// <param name="equals"></param>
        /// <returns></returns>
        public bool Equals(CSFSparseTensor<T> tensor, Func<T, T, bool> equals)
        {
            if (tensor is null)
            {
                return false;
            }
            if (equals is null)
            {
                throw new ArgumentNullException(nameof(equals));
            }

            if (_rows != tensor._rows || _columns != tensor._columns || _depth != tensor._depth)
            {
                return false;
            }

            // Initialize the zero element for elementwise comparison
            T zero = new T();

            // Co-iterate
            for (int r = 0; r < _rows; ++r)
            {
                int r1 = _ri[r], r_end1 = _ri[r + 1],
                    r2 = tensor._ri[r], r_end2 = tensor._ri[r + 1];

                while (r1 < r_end1 || r2 < r_end2)
                {
                    int c1 = r1 < r_end1 ? _nzc[r1] : int.MaxValue,
                        c2 = r2 < r_end2 ? tensor._nzc[r2] : int.MaxValue;

                    if (c1 > c2)
                    {
                        // Check if all entries of tensor 2 is 0
                        for (int i = tensor._ci[r2], cend = tensor._ci[r2 + 1]; i < cend; ++i)
                        {
                            if (!equals(zero, tensor._values[i]))
                            {
                                return false;
                            }
                        }
                        ++r2;
                    }
                    else if (c1 < c2)
                    {
                        // Check if all entries of tensor 1 is 0
                        for (int i = _ci[r1], cend = _ci[r1 + 1]; i < cend; ++i)
                        {
                            if (!equals(zero, _values[i]))
                            {
                                return false;
                            }
                        }
                        ++r1;
                    }
                    else
                    {
                        // c1 == c2
                        int i1 = _ci[r1], cend1 = _ci[r1 + 1],
                            i2 = tensor._ci[r2], cend2 = tensor._ci[r2 + 1];

                        while (i1 < cend1 || i2 < cend2)
                        {
                            int d1 = i1 < cend1 ? _di[i1] : int.MaxValue,
                                d2 = i2 < cend2 ? tensor._di[i2] : int.MaxValue;

                            if (d1 > d2)
                            {
                                if (!equals(zero, tensor._values[i2]))
                                {
                                    return false;
                                }
                                ++i2;
                            }
                            else if (d1 < d2)
                            {
                                if (!equals(zero, _values[i1]))
                                {
                                    return false;
                                }
                                ++i1;
                            }
                            else
                            {
                                if (!equals(_values[i1], tensor._values[i2]))
                                {
                                    return false;
                                }
                                ++i1; ++i2;
                            }
                        }

                        ++r1; ++r2;
                    }
                }
            }

            return true;
        }

        public override T Get(params int[] index)
        {
            ValidateIndex(index);

            int r = index[0],
                c = index[1],
                d = index[2];

            int rstart = _ri[r], rend = _ri[r + 1];
            int ci = Array.BinarySearch(_nzc, rstart, rend - rstart, c);
            if (ci < 0)
            {
                return _zero;
            }
            int cstart = _ci[ci], cend = _ci[ci + 1];
            int di = Array.BinarySearch(_di, cstart, cend - cstart, d);
            if (di < 0)
            {
                return _zero;
            }
            return _values[di];
        }
        public override int[] GetDimensions()
        {
            return new int[] { _rows, _columns, _depth };
        }
        public int GetDimension(int index)
        {
            if (index < 0 || index > 2)
            {
                throw new ArgumentOutOfRangeException(nameof(index));
            }
            switch (index)
            {
                case 0: return _rows;
                case 1: return _columns;
                case 2: return _depth;
                default:
                    break;
            }
            return -1; // should never get here
        }

        /// <summary>
        /// Returns a column fibre (mode-2 fibre) of this tensor.
        /// </summary>
        /// <param name="rowIndex">The row index of the fibre.</param>
        /// <param name="depthIndex">The column index of the fibre.</param>
        /// <returns>A vector of size <txt>Columns</txt></returns>
        public SparseVector<T> GetColumnFibre(int rowIndex, int depthIndex)
        {
            if (rowIndex < 0 || rowIndex >= _rows)
            {
                throw new ArgumentOutOfRangeException(nameof(rowIndex));
            }
            if (depthIndex < 0 || depthIndex >= _depth)
            {
                throw new ArgumentOutOfRangeException(nameof(depthIndex));
            }

            // Select vector [r, :, d]
            int averageRowNnz = Math.Max(1, _values.Length / (_rows * _depth));
            FastList<int> vi = new FastList<int>(averageRowNnz);
            FastList<T> v = new FastList<T>(averageRowNnz);

            int rstart = _ri[rowIndex], rend = _ri[rowIndex + 1];
            for (int i = rstart; i < rend; ++i)
            {
                int cstart = _ci[i], cend = _ci[i + 1];
                int index = Array.BinarySearch(_di, cstart, cend - cstart, depthIndex);
                if (index >= 0)
                {
                    vi.Add(_nzc[i]);
                    v.Add(_values[index]);
                }
            }

            return new SparseVector<T>(_columns, vi.ToArray(), v.ToArray(), skipChecking: true);
        }

        /// <summary>
        /// Returns a row fibre (mode-1 fibre) of this tensor.
        /// </summary>
        /// <param name="columnIndex">The column index of the fibre.</param>
        /// <param name="depthIndex">The depth index of the fibre.</param>
        /// <returns>A vector of size <txt>Rows</txt></returns>
        public SparseVector<T> GetRowFibre(int columnIndex, int depthIndex)
        {
            if (columnIndex < 0 || columnIndex >= _columns)
            {
                throw new ArgumentOutOfRangeException(nameof(columnIndex));
            }
            if (depthIndex < 0 || depthIndex >= _depth)
            {
                throw new ArgumentOutOfRangeException(nameof(depthIndex));
            }

            // Select vector [:, c, d]
            int averageRowNnz = Math.Max(1, _values.Length / (_columns * _depth));
            FastList<int> vi = new FastList<int>(averageRowNnz);
            FastList<T> v = new FastList<T>(averageRowNnz);

            for (int r = 0; r < _rows; ++r)
            {
                int rstart = _ri[r], rend = _ri[r + 1];
                int cindex = Array.BinarySearch(_nzc, rstart, rend - rstart, columnIndex);
                if (cindex >= 0)
                {
                    int cstart = _ci[cindex], cend = _ci[cindex + 1];
                    int dindex = Array.BinarySearch(_di, cstart, cend - cstart, depthIndex);
                    if (dindex >= 0)
                    {
                        vi.Add(r);
                        v.Add(_values[dindex]);
                    }
                }
            }

            return new SparseVector<T>(_rows, vi.ToArray(), v.ToArray(), skipChecking: true);
        }

        /// <summary>
        /// Returns a tube fibre (mode-3 fibre) of this tensor.
        /// </summary>
        /// <param name="columnIndex"></param>
        /// <param name="depthIndex"></param>
        /// <returns>A vector of size <txt>Rows</txt></returns>
        public SparseVector<T> GetTubeFibre(int rowIndex, int columnIndex)
        {
            if (rowIndex < 0 || rowIndex >= _rows)
            {
                throw new ArgumentOutOfRangeException(nameof(rowIndex));
            }
            if (columnIndex < 0 || columnIndex >= _columns)
            {
                throw new ArgumentOutOfRangeException(nameof(columnIndex));
            }

            // Select vector [r, c, :]
            int rstart = _ri[rowIndex], rend = _ri[rowIndex + 1];
            int cindex = Array.BinarySearch(_nzc, rstart, rend - rstart, columnIndex);
            if (cindex < 0)
            {
                return new SparseVector<T>(_depth); // zero vector
            }

            int cstart = _ci[cindex], cend = _ci[cindex + 1];
            int[] vi = new int[cend - cstart];
            T[] v = new T[vi.Length];
            Array.Copy(_di, cstart, vi, 0, vi.Length);
            Array.Copy(_values, cstart, v, 0, v.Length);
            return new SparseVector<T>(_depth, vi, v, skipChecking: true);
        }

        /// <summary>
        /// Returns the inner product between this tensor and another tensor of the same dimension. 
        /// </summary>
        /// <param name="tensor">The second tensor.</param>
        /// <param name="blas">A sparse BLAS-1 provider.</param>
        /// <returns></returns>
        public T InnerProduct(CSFSparseTensor<T> tensor, ISparseBLAS1<T> blas)
        {
            if (tensor is null)
            {
                throw new ArgumentNullException(nameof(tensor));
            }
            if (blas is null)
            {
                throw new ArgumentNullException(nameof(blas));
            }

            AssertDimensionsMatch(GetDimensions(), tensor.GetDimensions());

            IProvider<T> provider = blas.Provider;
            T prod = provider.Zero;

            for (int r = 0; r < _rows; ++r)
            {
                int r1 = _ri[r], r2 = tensor._ri[r],
                    r_end1 = _ri[r + 1], r_end2 = tensor._ri[r + 1];

                while (r1 < r_end1 || r2 < r_end2)
                {
                    int c1 = r1 < r_end1 ? _nzc[r1] : int.MaxValue,
                        c2 = r2 < r_end2 ? tensor._nzc[r2] : int.MaxValue;
                    if (c1 > c2)
                    {
                        ++r2;
                    }
                    else if (c1 < c2)
                    {
                        ++r1;
                    }
                    else
                    {
                        T dot = blas.DOT(_di, _values, tensor._di, tensor._values, _ci[r1], tensor._ci[r2], _ci[r1 + 1], tensor._ci[r2 + 1]);
                        prod = provider.Add(prod, dot);
                        ++r1;
                        ++r2;
                    }
                }
            }
            return prod;
        }

        /// <summary>
        /// n-mode matrix multiplication with a sparse matrix.
        /// </summary>
        /// <param name="matrix"></param>
        /// <param name="mode"></param>
        /// <param name="provider"></param>
        /// <returns></returns>
        public CSFSparseTensor<T> Multiply(CSRSparseMatrix<T> matrix, int mode, IProvider<T> provider)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            if (mode < 0 || mode > 2)
            {
                throw new ArgumentOutOfRangeException(nameof(mode));
            }
            if (GetDimension(mode) != matrix.Columns)
            {
                throw new InvalidOperationException("Dimensions do not match.");
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }

            if (mode == 0)
            {
                return Multiply_Mode0(matrix, provider);
                //return Multiply_Mode0_1(matrix, provider);
                //return Multiply_Mode0_2(matrix, provider);
            }
            else if (mode == 1)
            {
                return Multiply_Mode1(matrix, provider);
            }
            else if (mode == 2)
            {
                return Multiply_Mode2(matrix, provider);
            }

            throw new NotImplementedException();
        }

        /// <summary>
        /// n-mode fibre multiplication with a dense vector.
        /// </summary>
        /// <param name="vector"></param>
        /// <param name="mode"></param>
        /// <param name="provider"></param>
        /// <returns></returns>
        public CSRSparseMatrix<T> Multiply(DenseVector<T> vector, int mode, IProvider<T> provider)
        {
            if (vector is null)
            {
                throw new ArgumentNullException(nameof(vector));
            }
            if (mode < 0 || mode > 2)
            {
                throw new ArgumentOutOfRangeException(nameof(mode));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            if (GetDimension(mode) != vector.Dimension)
            {
                throw new InvalidOperationException("Dimensions do not match.");
            }

            T[] vect_values = vector.Values;

            // [r, c] - summing out depth
            if (mode == 2)
            {
                int[] rowIndices = _ri.Copy();
                int[] columnIndices = _nzc.Copy();
                T[] values = new T[_nzc.Length];

                T zero = provider.Zero;
                for (int r = 0, k = 0; r < _rows; ++r)
                {
                    int start = _ri[r], end = _ri[r + 1];
                    for (int i = start; i < end; ++i)
                    {
                        int c_start = _ci[i], c_end = _ci[i + 1];
                        T sum = zero;
                        for (int j = c_start; j < c_end; ++j)
                        {
                            sum = provider.Add(sum, provider.Multiply(_values[j], vect_values[_di[j]]));
                        }
                        values[k++] = sum;
                    }
                }

                return new CSRSparseMatrix<T>(_columns, rowIndices, columnIndices, values);
            }

            // [r, d] - summing out columns
            if (mode == 1)
            {
                // Count the required size
                int[] set_union = new int[_depth];
                int threshold = 0;

                // Calculate the non-zero counts of rows first
                int[] rowIndices = new int[_rows + 1];
                int nnz = 0;
                for (int r = 0; r < _rows; ++r)
                {
                    int start = _ri[r],
                        end = _ri[r + 1];

                    ++threshold;

                    for (int ci = start; ci < end; ++ci)
                    {
                        int c_start = _ci[ci],
                            c_end = _ci[ci + 1];
                        for (int i = c_start; i < c_end; ++i)
                        {
                            int d = _di[i];
                            if (set_union[d] < threshold)
                            {
                                ++nnz;
                                set_union[d] = threshold;
                            }
                        }
                    }
                    rowIndices[r + 1] = nnz;
                }

                int[] depthIndices = new int[nnz];
                T[] values = new T[nnz];
                T[] workspace = new T[_depth];

                for (int r = 0, k = 0; r < _rows; ++r)
                {
                    int kend = k;
                    ++threshold;

                    for (int i = _ri[r], rend = _ri[r + 1]; i < rend; ++i)
                    {
                        T v = vect_values[_nzc[i]];
                        for (int j = _ci[i], cend = _ci[i + 1]; j < cend; ++j)
                        {
                            int d = _di[j];
                            T vw = provider.Multiply(v, _values[j]);
                            if (set_union[d] < threshold)
                            {
                                // First time addition
                                workspace[d] = vw;
                                // Directly store index into depthIndices
                                depthIndices[kend++] = d;
                                set_union[d] = threshold;
                            }
                            else
                            {
                                workspace[d] = provider.Add(workspace[d], vw);
                            }
                        }
                    }

                    // Gather depth values
                    for (int i = k; i < kend; ++i)
                    {
                        values[i] = workspace[depthIndices[i]];
                    }

                    // perform a sort
                    Array.Sort(depthIndices, values, k, kend - k);

                    // Reset k -> kend
                    k = kend;

                    // Prepare set union for next iteration
                    if (threshold == int.MaxValue)
                    {
                        threshold = 1;
                        Array.Clear(set_union, 0, _depth);
                    }
                }

                return new CSRSparseMatrix<T>(_depth, rowIndices, depthIndices, values);
            }

            // [c, d] - summing out rows
            if (mode == 0)
            {
                // Lazy solution - slow
                int[] columnIndex = new int[_columns + 1];
                Dictionary<long, T> dict = new Dictionary<long, T>();
                for (int r = 0; r < _rows; ++r)
                {
                    T ve = vect_values[r];
                    for (int i = _ri[r], r_end = _ri[r + 1]; i < r_end; ++i)
                    {
                        int c = _nzc[i];
                        for (int j = _ci[i], c_end = _ci[i + 1]; j < c_end; ++j)
                        {
                            int d = _di[j];
                            long key = c * _depth + d;
                            if (dict.ContainsKey(key))
                            {
                                dict[key] = provider.Add(dict[key], provider.Multiply(ve, _values[j]));
                            }
                            else
                            {
                                dict[key] = provider.Multiply(ve, _values[j]);
                                columnIndex[c]++;
                            }
                        }
                    }
                }

                // Calculate column index
                for (int c = _columns; c > 0; --c)
                {
                    columnIndex[c] = columnIndex[c - 1];
                }
                columnIndex[0] = 0;
                for (int c = 0; c < _columns; ++c)
                {
                    columnIndex[c + 1] += columnIndex[c];
                }

                int nnz = dict.Count;
                int[] depthIndices = new int[nnz];
                T[] values = new T[nnz];

                int[] columnStart = new int[_columns];
                Array.Copy(columnIndex, columnStart, _columns);
                foreach (KeyValuePair<long, T> e in dict)
                {
                    long key = e.Key;
                    int c = (int)(key / _depth), d = (int)(key % _depth);
                    int index = columnStart[c]++;
                    depthIndices[index] = d;
                    values[index] = e.Value;
                }

                for (int c = 0; c < _columns; ++c)
                {
                    int start = columnIndex[c], end = columnIndex[c + 1];
                    Array.Sort(depthIndices, values, start, end - start);
                }

                return new CSRSparseMatrix<T>(_depth, columnIndex, depthIndices, values);
            }

            throw new NotImplementedException();
        }

        /// <summary>
        /// Multiply this tensor by a scalar, returning a new tensor without altering this tensor.
        /// </summary>
        /// <param name="scalar"></param>
        /// <param name="blas1"></param>
        /// <returns></returns>
        public CSFSparseTensor<T> Multiply(T scalar, IDenseBLAS1<T> blas1)
        {
            if (scalar is null) throw new ArgumentNullException(nameof(scalar));
            if (blas1 is null) throw new ArgumentNullException(nameof(blas1));

            T[] values = _values.Copy();
            blas1.SCAL(values, scalar, 0, values.Length);

            return new CSFSparseTensor<T>(_columns, _depth, _ri, _nzc, _ci, _di, values);
        }

        /// <summary>
        /// Multiply this sparse tensor by a scalar, in place. This tensor will be modified and replaced with 
        /// the result.
        /// </summary>
        /// <param name="scalar"></param>
        /// <param name="blas1"></param>
        public void MultiplyInPlace(T scalar, IDenseBLAS1<T> blas1)
        {
            if (scalar is null)
            {
                throw new ArgumentNullException(nameof(scalar));
            }
            if (blas1 is null)
            {
                throw new ArgumentNullException(nameof(blas1));
            }
            blas1.SCAL(_values, scalar, 0, _values.Length);
        }

        /// <summary>
        /// Returns the negative of this tensor.
        /// </summary>
        /// <param name="blas1">The dense level-1 BLAS implementation to use.</param>
        /// <returns>The negation of this tensor.</returns>
        public CSFSparseTensor<T> Negate(IDenseBLAS1<T> blas1)
        {
            CSFSparseTensor<T> copy = new CSFSparseTensor<T>(this);
            copy.NegateInPlace(blas1);
            return copy;
        }

        public void NegateInPlace(IDenseBLAS1<T> blas1)
        {
            if (blas1 is null)
            {
                throw new ArgumentNullException(nameof(blas1));
            }

            IProvider<T> provider = blas1.Provider;
            MultiplyInPlace(provider.Negate(provider.One), blas1);
        }

        /// <summary>
        /// Permutes the specified dimension of the tensor by the inverse permutation vector.
        /// The permutation is represented by a vector whose value at index i is the preimage of i.
        /// </summary>
        /// <param name="dimension"></param>
        /// <param name="inversePermutation"></param>
        public void Permute(int dimension, int[] inversePermutation)
        {
            if (dimension < 0 || dimension >= GetDimension(dimension))
            {
                throw new ArgumentOutOfRangeException(nameof(dimension));
            }
            if (inversePermutation is null)
            {
                throw new ArgumentNullException(nameof(inversePermutation));
            }
            if (inversePermutation.Length != GetDimension(dimension))
            {
                throw new ArgumentOutOfRangeException(nameof(inversePermutation), $"Dimension mismatch: {inversePermutation.Length} != {GetDimension(dimension)}");
            }
            if (!inversePermutation.IsPermutation())
            {
                throw new ArgumentException(nameof(inversePermutation), $"{nameof(inversePermutation)} is not a permutation vector.");
            }

            if (dimension == 0)
            {
                // There is probably a way to do these permutations in-place, however the algorithm
                // would be much more complex. Future research direction?
                int nzc_count = _nzc.Length, nnz = _values.Length;
                int[] ri = new int[_rows + 1];
                int[] nzc = new int[nzc_count];
                int[] ci = new int[nzc_count + 1];
                int[] di = new int[nnz];
                T[] values = new T[nnz];

                // Populate ci[1], ..., ci[nzc.Length] with the nz counts within each column
                // prior to permutation
                for (int i = 1; i <= nzc_count; ++i)
                {
                    ci[i] = _ci[i] - _ci[i - 1];
                }

                for (int r = 0, k = 0; r < _rows; ++r)
                {
                    int rstart = _ri[inversePermutation[r]], // the start of the original
                        rend = _ri[inversePermutation[r] + 1], // the end of the original
                        count_nzc = rstart - rend,
                        count_nnz = _ci[rend] - _ci[rstart];

                    // Accumulate row counts
                    ri[r + 1] = ri[r] + count_nzc;
                    Array.Copy(_nzc, rstart, nzc, ri[r], count_nzc);
                    Array.Copy(_ci, rstart, ci, ri[r], count_nzc); // Requires reset
                    Array.Copy(_di, _ci[rstart], di, k, count_nnz);
                    Array.Copy(_values, _ci[rstart], values, k, count_nnz);
                    k += count_nnz;
                }

                // the total nnz does not change
                ci[nzc_count] = _ci[nzc_count];

                // Reset values 
                _ri = ri;
                _nzc = nzc;
                _ci = ci;
                _di = di;
                _values = values;
            }
            else if (dimension == 1)
            {
                // Compute inv permutation
                int[] invPermutation = new int[_columns];
                for (int c = 0; c < _columns; ++c)
                {
                    invPermutation[invPermutation[c]] = c;
                }

                // Permute columns
                int[] nzc = new int[_nzc.Length],
                    ci = new int[_ci.Length],
                    di = new int[_di.Length];
                T[] values = new T[_values.Length];

                for (int r = 0; r < _rows; ++r)
                {
                    int rstart = _ri[r], rend = _ri[r + 1];
                    for (int i = rstart; i < rend; ++i)
                    {
                        int c = _nzc[i], pi = invPermutation[i];
                        nzc[pi] = _nzc[i];
                    }
                }
            }
            throw new NotImplementedException();
        }

        /// <summary>
        /// Returns a matrix formed by selecting the index from the dimension
        /// </summary>
        /// <param name="dimension"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        public CSRSparseMatrix<T> Slice(int dimension, int index)
        {
            if (dimension < 0 || dimension > 2)
            {
                throw new ArgumentOutOfRangeException(nameof(dimension));
            }
            if (index < 0 || index > GetDimension(dimension))
            {
                throw new ArgumentOutOfRangeException(nameof(index));
            }

            if (dimension == 0)
            {
                // Select a row
                int[] rowIndex = new int[_columns + 1];
                int rstart = _ri[index], rend = _ri[index + 1];
                for (int i = rstart; i < rend; ++i)
                {
                    rowIndex[_nzc[i] + 1] += _ci[i + 1] - _ci[i];
                }

                // Accumulate
                for (int i = 0; i < _columns; ++i)
                {
                    rowIndex[i + 1] += rowIndex[i];
                }

                // Fill in the values
                int nnz = _ci[rend] - _ci[rstart];
                int[] columnIndex = new int[nnz];
                T[] values = new T[nnz];
                Array.Copy(_di, _ci[rstart], columnIndex, 0, nnz);
                Array.Copy(_values, _ci[rstart], values, 0, nnz);
                return new CSRSparseMatrix<T>(_depth, rowIndex, columnIndex, values);
            }
            else if (dimension == 1)
            {
                int[] rowIndex = new int[_rows + 1];
                FastList<int> columnIndex = new FastList<int>(_rows);
                FastList<T> values = new FastList<T>(_rows);

                for (int r = 0; r < _rows; ++r)
                {
                    int rstart = _ri[r], rend = _ri[r + 1];
                    int i = Array.BinarySearch(_nzc, rstart, rend - rstart, index);
                    if (i >= 0)
                    {
                        int cstart = _ci[i], cend = _ci[i + 1];
                        columnIndex.AddRange(_di, cstart, cend);
                        values.AddRange(_values, cstart, cend);
                    }
                    rowIndex[r + 1] = columnIndex.Count;
                }

                return new CSRSparseMatrix<T>(_depth, rowIndex, columnIndex.ToArray(), values.ToArray());
            }
            else if (dimension == 2)
            {
                int[] rowIndex = new int[_rows + 1];
                FastList<int> columnIndex = new FastList<int>(_rows);
                FastList<T> values = new FastList<T>(_rows);

                for (int r = 0; r < _rows; ++r)
                {
                    for (int i = _ri[r], rend = _ri[r + 1]; i < rend; ++i)
                    {
                        int cstart = _ci[i], cend = _ci[i + 1];
                        int idx = Array.BinarySearch(_di, cstart, cend - cstart, index);
                        if (idx >= 0)
                        {
                            columnIndex.Add(_nzc[i]);
                            values.Add(_values[idx]);
                        }
                    }
                    rowIndex[r + 1] = values.Count;
                }

                return new CSRSparseMatrix<T>(_columns, rowIndex, columnIndex.ToArray(), values.ToArray());
            }

            throw new NotImplementedException();
        }

        public CSRSparseMatrix<T> Select(int rowIndex)
        {
            return Slice(0, rowIndex);
        }

        public SparseVector<T> Select(int rowIndex, int columnIndex)
        {
            return GetTubeFibre(rowIndex, columnIndex);
        }

        /// <summary>
        /// Untested
        /// </summary>
        /// <param name="rowSelection"></param>
        /// <param name="columnSelection"></param>
        /// <param name="oneBased"></param>
        /// <returns></returns>
        public CSFSparseTensor<T> Select(string rowSelection, bool oneBased = false)
        {
            if (!Selector.TryParse(rowSelection, oneBased, _rows, out Selector rs))
            {
                throw new FormatException($"Invalid syntax near: {rowSelection}");
            }

            int[] R = rs.Selection;
            int rows = R.Length;

            int[] ri = new int[rows + 1];
            int nzc_count = 0, nnz = 0;
            for (int r = 0; r < rows; ++r)
            {
                int Rr = R[r],
                    rstart = _ri[Rr],
                    rend = _ri[Rr + 1];

                nzc_count += (rend - rstart);
                nnz += (_ci[rend] - _ci[rstart]);
                ri[r + 1] = nzc_count;
            }

            int[] nzc = new int[nzc_count],
                ci = new int[nzc_count + 1],
                di = new int[nnz];
            T[] values = new T[nnz];

            for (int r = 0, k = 0; r < rows; ++r)
            {
                int Rr = R[r];
                for (int i = _ri[Rr], rend = _ri[Rr + 1]; i < rend; ++i, ++k)
                {
                    int cstart = _ci[i],
                        cend = _ci[i + 1],
                        cnz = cend - cstart;

                    nzc[k] = _nzc[i];
                    ci[k + 1] = ci[k] + cnz;
                    Array.Copy(_di, cstart, di, ci[k], cnz);
                    Array.Copy(_values, cstart, values, ci[k], cnz);
                }
            }
            return new CSFSparseTensor<T>(_columns, _depth, ri, nzc, ci, di, values);
        }

        /// <summary>
        /// Untested
        /// </summary>
        /// <param name="rowSelection"></param>
        /// <param name="columnSelection"></param>
        /// <param name="oneBased"></param>
        /// <returns></returns>
        public CSFSparseTensor<T> Select(string rowSelection, string columnSelection, bool oneBased = false)
        {
            if (!Selector.TryParse(rowSelection, oneBased, _rows, out Selector rs))
            {
                throw new FormatException($"Invalid syntax near: {rowSelection}");
            }
            if (!Selector.TryParse(columnSelection, oneBased, _columns, out Selector cs))
            {
                throw new FormatException($"Invalid syntax near: {columnSelection}");
            }

            int[] R = rs.Selection, C = cs.Selection;
            int rows = R.Length, cols = C.Length;

            int[] ri = new int[rows + 1];
            Dictionary<int, int> cLookup = new Dictionary<int, int>(cols);
            for (int c = 0; c < cols; ++c)
            {
                cLookup[C[c]] = c;
            }

            FastList<int> nzc = new FastList<int>(rows),
                ci = new FastList<int>(rows),
                di = new FastList<int>(rows);
            FastList<T> values = new FastList<T>(rows);

            ci.Add(0);

            for (int r = 0; r < rows; ++r)
            {
                int Rr = R[r];
                for (int i = _ri[Rr], rend = _ri[Rr + 1]; i < rend; ++i)
                {
                    if (cLookup.ContainsKey(_nzc[i]))
                    {
                        int cstart = _ci[i], cend = _ci[i + 1];

                        nzc.Add(cLookup[_nzc[i]]);
                        di.AddRange(_di, cstart, cend);
                        values.AddRange(_values, cstart, cend);
                        ci.Add(values.Count);
                    }
                }
                ri[r + 1] = values.Count;
            }
            return new CSFSparseTensor<T>(cols, _depth, ri, nzc.ToArray(), ci.ToArray(), di.ToArray(), values.ToArray());
        }

        /// <summary>
        /// Select a subtensor from this tensor, using Matlab/Python notation.
        /// </summary>
        /// <param name="rowSelection"></param>
        /// <param name="columnSelection"></param>
        /// <param name="depthSelection"></param>
        /// <param name="oneBased"></param>
        /// <returns></returns>
        public CSFSparseTensor<T> Select(string rowSelection, string columnSelection, string depthSelection, bool oneBased = false)
        {
            if (!Selector.TryParse(rowSelection, oneBased, _rows, out Selector rs))
            {
                throw new FormatException($"Invalid syntax near: {rowSelection}");
            }
            if (!Selector.TryParse(columnSelection, oneBased, _columns, out Selector cs))
            {
                throw new FormatException($"Invalid syntax near: {columnSelection}");
            }
            if (!Selector.TryParse(depthSelection, oneBased, _depth, out Selector ds))
            {
                throw new FormatException($"Invalid syntax near: {depthSelection}");
            }

            int[] R = rs.Selection, C = cs.Selection, D = ds.Selection;
            int rows = R.Length, cols = C.Length, depth = D.Length;

            // Build lookup tables - using dictionaries here to keep things O(flops), but 
            // in reality its probably faster to use a simple flat array
            Dictionary<int, int> cLookup = new Dictionary<int, int>(cols),
                dLookup = new Dictionary<int, int>(depth);
            for (int c = 0; c < cols; ++c)
            {
                cLookup[C[c]] = c;
            }
            for (int d = 0; d < depth; ++d)
            {
                dLookup[D[d]] = d;
            }

            int[] ri = new int[rows + 1];
            FastList<int> nzc = new FastList<int>(rows),
                ci = new FastList<int>(rows),
                di = new FastList<int>(rows);
            FastList<T> values = new FastList<T>(rows);

            ci.Add(0);

            for (int r = 0; r < rows; ++r)
            {
                // Co-iterate down the columns
                int i1 = _ri[r], rend = _ri[r + 1], i2 = 0;
                while (i1 < rend || i2 < cols)
                {
                    int c1 = i1 < rend ? _nzc[i1] : int.MaxValue,
                        c2 = i2 < cols ? C[i2] : int.MaxValue;

                    if (c1 > c2)
                    {
                        ++i2;
                    }
                    else if (c1 < c2)
                    {
                        ++i1;
                    }
                    else
                    {
                        // Coiterate down the depths
                        int j1 = _ci[i1], cend = _ci[i1 + 1], j2 = 0;
                        bool any = false;
                        while (j1 < cend || j2 < depth)
                        {
                            int d1 = j1 < cend ? _di[j1] : int.MaxValue,
                                d2 = j2 < depth ? D[j2] : int.MaxValue;

                            if (d1 > d2)
                            {
                                ++j2;
                            }
                            else if (d1 < d2)
                            {
                                ++j1;
                            }
                            else
                            {
                                // Record the depth index, and the value
                                di.Add(dLookup[d2]);
                                values.Add(_values[j1]);
                                ++j1; ++j2;
                                any = true;
                            }
                        }

                        // Add the column if there is any
                        if (any)
                        {
                            nzc.Add(cLookup[_nzc[i1]]);
                            ci.Add(values.Count);
                        }

                        ++i1; ++i2;
                    }
                }

                ri[r + 1] = nzc.Count;
            }

            Debug.WriteLine(new string('=', 100));
            ri.Print();
            nzc.Print();
            ci.Print();
            di.Print();
            values.Print();
            Debug.WriteLine(new string('=', 100));

            return new CSFSparseTensor<T>(cols, depth, ri, nzc.ToArray(), ci.ToArray(), di.ToArray(), values.ToArray());
        }

        /// <summary>
        /// Sets the value at the specified index to a value. 
        /// This method should be used with care, as it can be very slow if the set value does not belong 
        /// to the symbolic non-zero pattern of the tensor, as this requires restructuring of the tensor. 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="index"></param>
        public override void Set(T value, params int[] index)
        {
            if (value is null)
            {
                throw new ArgumentNullException(nameof(value));
            }

            ValidateIndex(index);

            int r = index[0],
                c = index[1],
                d = index[2];

            int rstart = _ri[r], rend = _ri[r + 1];
            int cindex = Array.BinarySearch(_nzc, rstart, rend - rstart, c);
            if (cindex < 0)
            {
                // Insert column, column index and depth index
                int nzc_count = _nzc.Length;
                int insertion_index = GetInsertionIndex(_nzc, rstart, rend, c);
                int di_insertion_index = _ci[insertion_index];

                // Create the new nzc array
                _nzc = _nzc.InsertAt(insertion_index, c);

                // Create the new ci array
                int[] new_ci = new int[_ci.Length + 1];
                Array.Copy(_ci, 0, new_ci, 0, insertion_index + 1);
                for (int i = insertion_index; i <= nzc_count; ++i)
                {
                    new_ci[i + 1] = _ci[i] + 1; // need to increment because of the inserted value
                }
                _ci = new_ci;

                // Create the new di array
                _di = _di.InsertAt(di_insertion_index, d);

                // Create the new values array
                _values = _values.InsertAt(di_insertion_index, value);

                // Update the ri array to reflect the added column
                for (int i = r + 1; i <= _rows; ++i)
                {
                    ++_ri[i];
                }
                return;
            }

            int c_start = _ci[cindex], c_end = _ci[cindex + 1];
            int dindex = Array.BinarySearch(_di, c_start, c_end - c_start, d);
            if (dindex < 0)
            {
                // Insert into column "cindex"
                // Insert the depth index d
                int di_insertion_index = GetInsertionIndex(_di, c_start, c_end, d);
                _di = _di.InsertAt(di_insertion_index, d);

                // Insert the value 
                _values = _values.InsertAt(di_insertion_index, value);

                // Increment all ci's [c_end, )
                int count = _ci.Length;
                for (int i = cindex + 1; i < count; ++i)
                {
                    ++_ci[i];
                }
                return;
            }

            // Best possible case - value is already part of the non-zero pattern - simply update
            _values[dindex] = value;
        }

        /// <summary>
        /// Sets a mode-1 fibre of the tensor to be equal in value to a vector.
        /// The dimension of the vector must match the number of rows of this tensor.
        /// The values in the fibre [:, columnIndex, depthIndex] will be replaced by the value of the vector.
        /// </summary>
        /// <note>This implementation is ~2x as fast as SetMode1Fibre_2 and supercedes it as the default implementation</note>
        /// <param name="columnIndex">The column index of the fibre to set. Must be a valid column index within this tensor.</param>
        /// <param name="depthIndex">The depth index of the fibre to set. Must be a valid tube index within this tensor.</param>
        /// <param name="fibre">A dense vector containing values to copy into this tensor.</param>
        public void SetRowFibre(int columnIndex, int depthIndex, DenseVector<T> fibre)
        {
            if (columnIndex < 0 || columnIndex >= _columns)
            {
                throw new ArgumentOutOfRangeException(nameof(columnIndex));
            }
            if (depthIndex < 0 || depthIndex >= _depth)
            {
                throw new ArgumentOutOfRangeException(nameof(depthIndex));
            }
            if (fibre is null)
            {
                throw new ArgumentNullException(nameof(fibre));
            }
            if (fibre.Dimension != _rows)
            {
                throw new InvalidOperationException("Dimension of vector does not match the number of rows of the tensor.");
            }

            // The numbe of columns, depths we need to add
            int new_c = 0, new_d = 0;

            // Iterate through the nz entries to count the number of entries to add 
            for (int r = 0; r < _rows; ++r)
            {
                int rstart = _ri[r], rend = _ri[r + 1];
                int cindex = Array.BinarySearch(_nzc, rstart, rend - rstart, columnIndex);
                if (cindex >= 0)
                {
                    int cstart = _ci[cindex], cend = _ci[cindex + 1];
                    if (Array.BinarySearch(_di, cstart, cend - cstart, depthIndex) < 0)
                    {
                        ++new_d;
                    }
                }
                else
                {
                    ++new_c;
                    ++new_d;
                }
            }

            // Create new storage
            int[]
                ri = new int[_rows + 1],
                nzc = new int[new_c + _nzc.Length],
                ci = new int[new_c + _ci.Length],
                di = new int[new_d + _di.Length];
            T[] values = new T[new_d + _values.Length],
                vect_values = fibre.Values;

            int c_offset = 0, d_offset = 0,
                ci_shift = 0; // the number of columns added (need to track this to alter ci)
            for (int r = 0; r < _rows; ++r)
            {
                // This kind of access prevents us from altering _ri
                int rstart = _ri[r], rend = _ri[r + 1];
                int c_insert_index = Array.BinarySearch(_nzc, rstart, rend - rstart, columnIndex);
                if (c_insert_index >= 0)
                {
                    int cstart = _ci[rstart], cend = _ci[rend],
                        ci_start = _ci[c_insert_index], ci_end = _ci[c_insert_index + 1];
                    int d_insert_index = Array.BinarySearch(_di, ci_start, ci_end - ci_start, depthIndex);
                    if (d_insert_index >= 0)
                    {
                        //Debug.WriteLine("R1\t" + new string('=', 100));
                        //ri.Print();
                        //nzc.Print();
                        //ci.Print();
                        //di.Print();
                        //values.Print();

                        // Both c, d indices exists - this is a simple replacement
                        _values[d_insert_index] = vect_values[r];
                        // Copy entire slice into new arrays; no change to c_offset or d_offset
                        Array.Copy(_nzc, rstart, nzc, c_offset + rstart, rend - rstart);
                        Array.Copy(_ci, rstart, ci, c_offset + rstart, rend - rstart);
                        Util.Increment(ci, c_offset + rstart, c_offset + rend, ci_shift);
                        Array.Copy(_di, cstart, di, d_offset + cstart, cend - cstart);
                        Array.Copy(_values, cstart, values, d_offset + cstart, cend - cstart);
                    }
                    else
                    {
                        //Debug.WriteLine("R2" + new string('=', 100));
                        //ri.Print();
                        //nzc.Print();
                        //ci.Print();
                        //di.Print();
                        //values.Print();

                        // c index present but d index missing
                        // The nzc array is copied in full, since no column indices are changed
                        Array.Copy(_nzc, rstart, nzc, c_offset + rstart, rend - rstart);

                        // The ci array is copied in full but incremented differently to accomodate the 
                        // increased column
                        Array.Copy(_ci, rstart, ci, c_offset + rstart, rend - rstart);
                        Util.Increment(ci, c_offset + rstart, c_offset + c_insert_index + 1, ci_shift);
                        Util.Increment(ci, c_offset + c_insert_index + 1, c_offset + rend, ++ci_shift);

                        // The di/values array is copied in 3 parts. Part 1: [cstart, insert_index)
                        int insert_index = ~d_insert_index;
                        Array.Copy(_di, cstart, di, d_offset + cstart, insert_index - cstart);
                        Array.Copy(_values, cstart, values, d_offset + cstart, insert_index - cstart);

                        // Part 2: [insert_index]
                        di[d_offset + insert_index] = depthIndex;
                        values[d_offset + insert_index] = vect_values[r];
                        ++d_offset;

                        // Part 3: (insert_index, cend)
                        Array.Copy(_di, insert_index, di, d_offset + insert_index, cend - insert_index);
                        Array.Copy(_values, insert_index, values, d_offset + insert_index, cend - insert_index);
                    }
                }
                else
                {
                    //Debug.WriteLine("R3" + new string('=', 100));
                    //ri.Print();
                    //nzc.Print();
                    //ci.Print();
                    //di.Print();
                    //values.Print();

                    // Both c and d indices are missing
                    // The nzc array is copied in 3 parts. 
                    c_insert_index = ~c_insert_index; // covert into the index @ which we insert into

                    // The nzc/ci array is also copied in 3/2 parts, with different increments
                    Array.Copy(_nzc, rstart, nzc, c_offset + rstart, c_insert_index - rstart); // Part 1: [rstart, c_insert_index)
                    Array.Copy(_ci, rstart, ci, c_offset + rstart, c_insert_index - rstart + 1); // Part 1: [rstart, c_insert_index]
                    Util.Increment(ci, c_offset + rstart, c_offset + c_insert_index + 1, ci_shift); // Shift

                    nzc[c_insert_index + c_offset] = columnIndex; // Part 2: insert new column
                    ++c_offset;
                    ++ci_shift;

                    Array.Copy(_nzc, c_insert_index, nzc, c_offset + c_insert_index, rend - c_insert_index); // Part 3: [c_insert_index, rend)
                    Array.Copy(_ci, c_insert_index, ci, c_offset + c_insert_index, rend - c_insert_index); // Part 2: [c_insert_index, rend) -> [off + c_insert_index + 1,...)
                    Util.Increment(ci, c_offset + c_insert_index, c_offset + rend, ci_shift); // Shift (+1)

                    // The di/values array added in 3 parts 
                    int insert_index = _ci[c_insert_index]; // Insertion location in the original di, values array
                    int cstart = _ci[rstart], // The first index of the first column of the current row slice
                        cend = _ci[rend]; // The first index of the first column of the next row slice
                    Array.Copy(_di, cstart, di, d_offset + cstart, insert_index - cstart); // Part 1: [cstart, insert_index) -> [cstart, insert_index)
                    Array.Copy(_values, cstart, values, d_offset + cstart, insert_index - cstart);
                    //Debug.WriteLine("insert_index: " + insert_index);
                    //Debug.WriteLine("c_insert_index: " + c_insert_index);
                    //Debug.WriteLine("cstart: " + cstart);
                    //Debug.WriteLine("cend: " + cend);
                    //Debug.WriteLine("d_offset: " + d_offset);
                    //Debug.Write("values:\t"); values.Print();

                    // Part 2: [] -> [insert_index]
                    di[d_offset + insert_index] = depthIndex;
                    values[d_offset + insert_index] = vect_values[r];
                    //Debug.WriteLine("d_offset: " + d_offset);
                    //Debug.WriteLine("rstart: " + rstart + "\trend: " + rend);
                    //Debug.WriteLine("nzc:\t"); _nzc.Print();
                    //Debug.WriteLine("c_insert_index: " + c_insert_index);
                    //Debug.WriteLine("cstart: " + cstart);
                    //Debug.Write("_ci:\t"); _ci.Print();
                    //Debug.WriteLine("insert_index: " + insert_index);
                    //Debug.WriteLine("vect_values[r]: " + vect_values[r]);
                    ++d_offset;

                    // Part 3: [insert_index, cend) -> [insert_index + 1, cend + 1) (+1 handled by the d_offset inc.)
                    Array.Copy(_di, insert_index, di, d_offset + insert_index, cend - insert_index);
                    Array.Copy(_values, insert_index, values, d_offset + insert_index, cend - insert_index);
                }

                ri[r + 1] = c_offset + _ri[r + 1];
            }

            // Set the last ci value 
            ci[nzc.Length] = values.Length;

            _ri = ri;
            _nzc = nzc;
            _ci = ci;
            _di = di;
            _values = values;
        }

        /// <summary>
        /// Sets a column (mode-2) fibre of the tensor to be equal in value to a vector. The dimension of the vector
        /// must match the number of columns of this tensor.
        /// <p>The values in the fibre [rowIndex, :, depthIndex] will be replaced by the value of the vector.</p>
        /// </summary>
        /// <param name="rowIndex">The row index of the fibre to set. Must be a valid row index within this tensor.</param>
        /// <param name="depthIndex">The tube index of the fibre to set. Must be a valid tube index within this tensor.</param>
        /// <param name="fibre"></param>
        public void SetColumnFibre(int rowIndex, int depthIndex, DenseVector<T> fibre)
        {
            if (rowIndex < 0 || rowIndex >= _rows)
            {
                throw new ArgumentOutOfRangeException(nameof(rowIndex));
            }
            if (depthIndex < 0 || depthIndex > _depth)
            {
                throw new ArgumentOutOfRangeException(nameof(depthIndex));
            }
            if (fibre is null)
            {
                throw new ArgumentNullException(nameof(fibre));
            }
            if (fibre.Dimension != _columns)
            {
                throw new InvalidOperationException("Dimension of the vector does not match the number of columns.");
            }

            int rstart = _ri[rowIndex],
                rend = _ri[rowIndex + 1],
                new_c = _columns - (rend - rstart), // since this is a dense vector, the entire column will be filled
                new_d = new_c;

            for (int i = rstart; i < rend; ++i)
            {
                int cstart = _ci[i], cend = _ci[i + 1];
                int dindex = Array.BinarySearch(_di, cstart, cend - cstart, depthIndex);
                if (dindex < 0)
                {
                    ++new_d;
                }
            }

            int[] nzc = new int[new_c + _nzc.Length],
                ci = new int[new_c + _ci.Length],
                di = new int[new_d + _di.Length];
            T[] values = new T[new_d + _values.Length];

            /// Copy everything coming prior to the inserted column
            Array.Copy(_nzc, nzc, rstart);
            Array.Copy(_ci, ci, rstart);
            Array.Copy(_di, di, _ci[rstart]);
            Array.Copy(_values, values, _ci[rstart]);

            // Insert the column
            int k = rstart, dk = _ci[rstart], nzci = rstart;
            for (int c = 0; c < _columns; ++c)
            {
                T e = fibre[c];
                nzc[k] = c;
                ci[k++] = dk; // From the previous column

                // Case 1: if the column is present already in the correct row
                if (rstart <= nzci && nzci < rend && _nzc[nzci] == c)
                {
                    // Check if the depth is already present
                    int cstart = _ci[nzci], cend = _ci[nzci + 1];
                    int dindex = Array.BinarySearch(_di, cstart, cend - cstart, depthIndex);
                    if (dindex >= 0)
                    {
                        // Update value with the column value
                        _values[dindex] = e;

                        // Straight copy
                        Array.Copy(_di, cstart, di, dk, cend - cstart);
                        Array.Copy(_values, cstart, values, dk, cend - cstart);
                        dk += (cend - cstart);
                    }
                    else
                    {
                        int insertion_index = ~dindex;
                        // Copy
                        Array.Copy(_di, cstart, di, dk, insertion_index - cstart);
                        Array.Copy(_values, cstart, values, dk, insertion_index - cstart);
                        dk += (insertion_index - cstart);

                        // Copy new value
                        di[dk] = depthIndex;
                        values[dk++] = e;

                        // Copy after
                        Array.Copy(_di, insertion_index, di, dk, cend - insertion_index);
                        Array.Copy(_values, insertion_index, values, dk, cend - insertion_index);
                        dk += (cend - insertion_index);
                    }
                    ++nzci;
                }

                // Case 2: if this is a newly introduced column
                else
                {
                    di[dk] = depthIndex;
                    values[dk++] = e;
                }
            }

            // Copy everything coming after the inserted column
            Util.Increment(_ri, rowIndex + 1, _rows + 1, new_c);
            Array.Copy(_nzc, rend, nzc, k, _nzc.Length - rend);
            Array.Copy(_ci, rend, ci, k, _ci.Length - rend);
            Util.Increment(ci, k, ci.Length, dk - _ci[rend]);

            Array.Copy(_di, _ci[rend], di, dk, _di.Length - _ci[rend]);
            Array.Copy(_values, _ci[rend], values, dk, _values.Length - _ci[rend]);

            // Update the tensor storage
            _nzc = nzc;
            _ci = ci;
            _di = di;
            _values = values;
        }

        /// <summary>
        /// Sets a tube (mode-3) fibre of the tensor to be equal in value to a vector.
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <param name="columnIndex"></param>
        /// <param name="fibre"></param>
        public void SetTubeFibre(int rowIndex, int columnIndex, DenseVector<T> fibre)
        {
            if (rowIndex < 0 || rowIndex >= _rows)
            {
                throw new ArgumentOutOfRangeException(nameof(rowIndex));
            }
            if (columnIndex < 0 || columnIndex >= _columns)
            {
                throw new ArgumentOutOfRangeException(nameof(columnIndex));
            }
            if (fibre is null)
            {
                throw new ArgumentNullException(nameof(fibre));
            }
            if (fibre.Dimension != _depth)
            {
                throw new InvalidOperationException("Dimension of the vector does not match the tube dimension.");
            }

            int rstart = _ri[rowIndex], rend = _ri[rowIndex + 1];
            int cindex = Array.BinarySearch(_nzc, rstart, rend - rstart, columnIndex);
            if (cindex >= 0)
            {
                // If the column is already present
                // This method can be made more efficient by not duplicating the array

                int cstart = _ci[cindex], cend = _ci[cindex + 1];
                int additions = _depth - (cend - cstart), // the number of nz entries added
                    nnz = _values.Length;

                int[] di = new int[_di.Length + additions];
                T[] values = new T[di.Length];

                // Copy everything prior to the tube
                Array.Copy(_di, di, _ci[cindex]);
                Array.Copy(_values, values, _ci[cindex]);

                // Copy the tube 
                int k = _ci[cindex];
                for (int i = 0; i < _depth; ++i, ++k)
                {
                    di[k] = i;
                    values[k] = fibre[i];
                }

                // Copy everything after the tube
                Array.Copy(_di, cend, di, k, nnz - cend);
                Array.Copy(_values, cend, values, k, nnz - cend);
                Util.Increment(_ci, cindex + 1, _ci.Length, k - _ci[cindex + 1]); // we want _ci[cindex + 1] to equal k

                _di = di;
                _values = values;
            }
            else
            {
                // Insert the entire vector as a new column
                int[] nzc = new int[_nzc.Length + 1],
                    ci = new int[_ci.Length + 1],
                    di = new int[_di.Length + _depth];
                T[] values = new T[di.Length];

                int insert_index = ~cindex,
                    cstart = _ci[insert_index];

                // Copy everything prior to the tube
                Array.Copy(_nzc, nzc, insert_index);
                Array.Copy(_ci, ci, insert_index + 1);
                Array.Copy(_di, di, cstart);
                Array.Copy(_values, values, cstart);

                // Insert the tube - no need to update _ci
                nzc[insert_index] = columnIndex;
                for (int i = 0, k = cstart; i < _depth; ++i, ++k)
                {
                    di[k] = i;
                    values[k] = fibre[i];
                }

                // Copy everything after the tube
                Array.Copy(_nzc, insert_index, nzc, insert_index + 1, _nzc.Length - insert_index);
                Array.Copy(_ci, insert_index, ci, insert_index + 1, _ci.Length - insert_index);
                Util.Increment(ci, insert_index + 1, ci.Length, _depth);
                int len = _di.Length - cstart;
                Array.Copy(_di, cstart, di, cstart + _depth, len);
                Array.Copy(_values, cstart, values, cstart + _depth, len);
                Util.Increment(_ri, rowIndex + 1, _rows + 1, 1); // Inserted column

                // Copy the storage arrays
                _nzc = nzc;
                _ci = ci;
                _di = di;
                _values = values;
            }
        }

        public CSFSparseTensor<T> Subtract(CSFSparseTensor<T> tensor, ISparseBLAS1<T> blas)
        {
            if (tensor is null)
            {
                throw new ArgumentNullException(nameof(tensor));
            }
            if (blas is null)
            {
                throw new ArgumentNullException(nameof(blas));
            }
            AssertDimensionsMatch(GetDimensions(), tensor.GetDimensions());

            CalculateNonZeroPatternOfUnionWith(tensor, out int nnz, out int nz_col);

            IProvider<T> provider = blas.Provider;

            // Create storage
            int[] rowIndices = new int[_rows + 1],
                nzc = new int[nz_col],
                ci = new int[nz_col + 1],
                di = new int[nnz];
            T[] values = new T[nnz];

            int kc = 0, ki = 0;
            for (int r = 0; r < _rows; ++r)
            {
                int r_end1 = _ri[r + 1],
                    r_end2 = tensor._ri[r + 1];

                // Calculate the number of columns in the sum of this row
                // Since both column indices are sorted, we can simply 
                // co-iterate without having to use a set union operation
                int i = _ri[r], j = tensor._ri[r];
                while (i < r_end1 || j < r_end2)
                {
                    int c1 = i < r_end1 ? _nzc[i] : int.MaxValue;
                    int c2 = j < r_end2 ? tensor._nzc[j] : int.MaxValue;

                    if (c1 > c2)
                    {
                        // Tensor 2 contains a column that is not present in tensor1
                        nzc[kc] = c2;

                        int c_end = tensor._ci[j + 1];
                        for (int k = tensor._ci[j]; k < c_end; ++k, ++ki)
                        {
                            di[ki] = tensor._di[k];
                            values[ki] = provider.Negate(tensor._values[k]);
                        }
                        ++j;
                    }
                    else if (c1 < c2)
                    {
                        // Tensor 1 contains a column that is not present in tensor2
                        nzc[kc] = c1;

                        int c_end = _ci[i + 1];
                        for (int k = _ci[i]; k < c_end; ++k, ++ki)
                        {
                            di[ki] = _di[k];
                            values[ki] = _values[k];
                        }
                        ++i;
                    }
                    else
                    {
                        nzc[kc] = c1;
                        ki = blas.SUB(_di, _values, tensor._di, tensor._values, di, values, _ci[i], tensor._ci[j], ki, _ci[i + 1], tensor._ci[j + 1]);
                        ++i; ++j;
                    }
                    ci[++kc] = ki;
                }
                rowIndices[r + 1] = kc;
            }
            return new CSFSparseTensor<T>(_columns, _depth, rowIndices, nzc, ci, di, values);
        }

        /// <summary>
        /// Returns the matrix obtained by summing over one type of fibre of this tensor, specified by the mode.
        /// </summary>
        /// <param name="mode">A integer (0, 1 or 2) indicating the dimension to sum over.</param>
        /// <param name="provider">The provider to use for summation.</param>
        /// <returns></returns>
        public CSRSparseMatrix<T> SumOver(int mode, IProvider<T> provider)
        {
            if (mode < 0 || mode > 2) throw new ArgumentOutOfRangeException(nameof(mode));
            if (provider is null) throw new ArgumentNullException(nameof(provider));

            if (mode == 0)
            {
                // Create sorted columns list, with inverse lookup table
                int count = _nzc.Length;
                int[] sorted_columns = _nzc.Copy();
                int[] inv_lookup = new int[count];
                for (int i = 0; i < count; ++i)
                {
                    inv_lookup[i] = i;
                }
                Array.Sort(sorted_columns, inv_lookup); // This is the most expensive operation of the method O(|nzc| log |nzc|)

                int[] columnIndex = new int[_columns + 1]; // Array used to hold the column indexes
                FastList<int> depthIndex = new FastList<int>(_depth); // List used to hold the depth indexes
                FastList<T> values = new FastList<T>(_depth); // List used to hold the values (sums)

                bool[] is_nz = new bool[_depth]; // holds whether an index is in the set union
                T[] sum = new T[_depth]; // holds the sum of elements in the set union
                int[] nz_indices = new int[_depth]; // Holds the nz indices of the union (starting from the front)
                int nz_count = 0; // Holds the number of elements in the set union

                int ci = 0;
                for (int i = 0; i < count; ++i)
                {
                    if (i > 0 && sorted_columns[i] != sorted_columns[i - 1])
                    {
                        // Add set, clear set union
                        for (int k = 0; k < nz_count; ++k)
                        {
                            int j = nz_indices[k];
                            depthIndex.Add(j);
                            values.Add(sum[j]);
                            is_nz[j] = false; // Reset
                        }
                        columnIndex[++ci] = values.Count;
                        nz_count = 0; // Reset
                    }

                    // The index of the column we are traversing over (and adding to the set union)
                    int idx = inv_lookup[i];
                    for (int j = _ci[idx], cend = _ci[idx + 1]; j < cend; ++j)
                    {
                        int d = _di[j];
                        if (is_nz[d])
                        {
                            sum[d] = provider.Add(sum[d], _values[j]);
                        }
                        else
                        {
                            sum[d] = _values[j];
                            is_nz[d] = true;
                            nz_indices[nz_count++] = d;
                        }
                    }
                }

                // Add the last column
                for (int k = 0; k < nz_count; ++k)
                {
                    int j = nz_indices[k];
                    depthIndex.Add(j);
                    values.Add(sum[j]);
                }
                columnIndex[++ci] = values.Count;

                // Extract and resize the arrays
                int[] depthIndex_array = depthIndex.Values;
                Array.Resize(ref depthIndex_array, values.Count);
                T[] values_array = values.Values;
                Array.Resize(ref values_array, values.Count);

                // Sort the arrays within each column
                for (int c = 0; c < _columns; ++c)
                {
                    int c_start = columnIndex[c], c_end = columnIndex[c + 1];
                    Array.Sort(depthIndex_array, values_array, c_start, c_end - c_start);
                }

                // Return the matrix
                return new CSRSparseMatrix<T>(_depth, columnIndex, depthIndex_array, values_array);
            }
            else if (mode == 1)
            {
                // Matrix of size [r, d]

                int[] rowIndex = new int[_rows + 1]; // The row indexes of the matrix
                FastList<int> columnIndex = new FastList<int>(_depth); // Keeps track of the column indexes of the matrix
                FastList<T> values = new FastList<T>(_depth); // Keeps track of the values of the sum matrix

                bool[] is_nz = new bool[_depth]; // [i] indicates whether the i-th entry is non-zero 
                int[] nz_indices = new int[_depth]; // [i] represents the index of the i-th non-zero entry
                int count_nz = 0; // the number of non-zero indices
                T[] sums = new T[_depth]; // keeps track of the partial sums 

                for (int r = 0; r < _rows; ++r)
                {
                    for (int i = _ri[r], r_end = _ri[r + 1]; i < r_end; ++i)
                    {
                        for (int j = _ci[i], c_end = _ci[i + 1]; j < c_end; ++j)
                        {
                            int d = _di[j];
                            if (is_nz[d])
                            {
                                sums[d] = provider.Add(sums[d], _values[j]);
                            }
                            else
                            {
                                sums[d] = _values[j];
                                is_nz[d] = true;
                                nz_indices[count_nz++] = d;
                            }
                        }
                    }

                    rowIndex[r + 1] = rowIndex[r] + count_nz;

                    // Reset the set union and insert into values list
                    for (int i = 0; i < count_nz; ++i)
                    {
                        int c = nz_indices[i];
                        columnIndex.Add(c);
                        values.Add(sums[c]);
                        is_nz[c] = false;
                    }
                    count_nz = 0;
                }

                int[] columnIndex_arr = columnIndex.Values;
                Array.Resize(ref columnIndex_arr, columnIndex.Count);

                T[] values_arr = values.Values;
                Array.Resize(ref values_arr, values.Count);

                // Array sort
                for (int r = 0; r < _rows; ++r)
                {
                    int rstart = rowIndex[r], rend = rowIndex[r + 1];
                    Array.Sort(columnIndex_arr, values_arr, rstart, rend - rstart);
                }

                return new CSRSparseMatrix<T>(_depth, rowIndex, columnIndex_arr, values_arr);
            }
            else if (mode == 2)
            {
                // Matrix of size [r, c]
                int[] rowIndex = _ri.Copy();
                int[] columnIndex = _nzc.Copy();
                int nnz = _nzc.Length;
                T[] values = new T[nnz];

                for (int i = 0; i < nnz; ++i)
                {
                    int c_start = _ci[i], c_end = _ci[i + 1];
                    T sum = provider.Zero;
                    for (int j = c_start; j < c_end; ++j)
                    {
                        sum = provider.Add(sum, _values[j]);
                    }
                    values[i] = sum;
                }
                return new CSRSparseMatrix<T>(_columns, rowIndex, columnIndex, values);
            }
            throw new NotImplementedException();
        }

        /// <summary>
        /// Transpose this tensor (without altering it).
        /// </summary>
        /// <param name="permutation">An array whose k-th element is the transposed order of the current k-th dimension.</param>
        /// <returns></returns>
        public CSFSparseTensor<T> Transpose(params int[] permutation)
        {
            if (permutation is null)
            {
                throw new ArgumentNullException(nameof(permutation));
            }
            if (!IsPermutationVector(permutation))
            {
                throw new ArgumentException(nameof(permutation), "Vector does not represent a permutation.");
            }

            int p1 = permutation[0],
                p2 = permutation[1],
                p3 = permutation[2];

            // Slow method - lots of room for speedups here
            int[] dims = new int[3], index = new int[3];
            dims[p1] = _rows;
            dims[p2] = _columns;
            dims[p3] = _depth;

            int d23 = dims[1] * dims[2],
                d3 = dims[2],
                nnz = _values.Length;

            // Arrange into COO format
            T[] values = new T[nnz];
            long[] indices = new long[nnz];
            for (int r = 0, k = 0; r < _rows; ++r)
            {
                index[p1] = r;
                for (int i = _ri[r], r_end = _ri[r + 1]; i < r_end; ++i)
                {
                    index[p2] = _nzc[i];
                    for (int j = _ci[i], c_end = _ci[i + 1]; j < c_end; ++j)
                    {
                        index[p3] = _di[j];
                        indices[k] = index[0] * d23 + index[1] * d3 + index[2];
                        values[k++] = _values[j];
                    }
                }
            }

            Array.Sort(indices, values);

            // Count the number of columns
            int[] set_union = new int[dims[1]];
            int count_col = 0,
                set_union_threshold = 0;
            for (int i = 0; i < nnz; ++i)
            {
                // These belong to different rows
                if (i == 0 || (indices[i] / d23) != (indices[i - 1] / d23))
                {
                    ++set_union_threshold;

                    if (set_union_threshold == int.MaxValue)
                    {
                        Array.Clear(set_union, 0, dims[1]);
                        set_union_threshold = 1;
                    }
                }

                int column = (int)(indices[i] % d23) / d3;
                if (set_union[column] < set_union_threshold)
                {
                    set_union[column] = set_union_threshold;
                    ++count_col;
                }
            }

            // Insert values
            int[] ri = new int[dims[0] + 1],
                nzc = new int[count_col],
                ci = new int[count_col + 1],
                di = new int[nnz];

            long last_r = -1,
                last_c = -1;
            int ci_index = 0,
                ri_index = 0;

            for (int i = 0; i < nnz; ++i)
            {
                long ind = indices[i];

                long r = ind / d23,
                    c = (ind % d23) / d3;
                int d = (int)(ind / d3);

                if (last_r != r)
                {
                    last_r = r;
                    ri[++ri_index] = ci_index;
                }

                if (last_c != c)
                {
                    last_c = c;
                    nzc[ci_index] = (int)c;
                    ci[++ci_index] = i;
                }

                di[i] = d;
            }

            return new CSFSparseTensor<T>(dims[1], dims[2], ri, nzc, ci, di, values);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <note>This method is 33% faster than the other Transpose method, for the implemented permutations.</note>
        /// <param name="permutation"></param>
        /// <returns></returns>
        public CSFSparseTensor<T> Transpose2(params int[] permutation)
        {
            if (permutation is null)
            {
                throw new ArgumentNullException(nameof(permutation));
            }
            if (!IsPermutationVector(permutation))
            {
                throw new ArgumentException(nameof(permutation), "Vector does not represent a permutation.");
            }

            if (permutation[0] == 0)
            {
                if (permutation[1] == 1)
                {
                    // Return a copy of this tensor - identity permutation [0, 1, 2]
                    return new CSFSparseTensor<T>(this);
                }
            }
            else if (permutation[0] == 2)
            {
                if (permutation[1] == 1)
                {
                    // [2, 1, 0]

                    // Calculate the depth counts
                    int[] depth_start = new int[_depth + 1];
                    int nnz = _di.Length;
                    for (int i = 0; i < nnz; ++i)
                    {
                        ++depth_start[_di[i]];
                    }
                    for (int i = _depth; i > 0; --i)
                    {
                        depth_start[i] = depth_start[i - 1];
                    }
                    depth_start[0] = 0;
                    for (int i = 0; i < _depth; ++i)
                    {
                        depth_start[i + 1] += depth_start[i];
                    }

                    // Calculate the (c, r) indexes within each depth
                    long[] cr_index = new long[nnz];
                    T[] values = new T[nnz];

                    for (int r = 0; r < _rows; ++r)
                    {
                        for (int i = _ri[r], r_end = _ri[r + 1]; i < r_end; ++i)
                        {
                            int c = _nzc[i];
                            long cr = c * _columns + r;
                            for (int j = _ci[i], c_end = _ci[i + 1]; j < c_end; ++j)
                            {
                                int d = _di[j];
                                int index = depth_start[d]++;
                                cr_index[index] = cr;
                                values[index] = _values[j];
                            }
                        }
                    }

                    // Sort within each depth index, and count the number of nz elements
                    int col_count = 0;
                    for (int d = 0; d < _depth; ++d)
                    {
                        int start = depth_start[d], end = depth_start[d + 1];
                        Array.Sort(cr_index, values, start, end - start);

                        // Count the number of unique columns in this depth
                        if (end > start)
                        {
                            ++col_count;
                        }
                        for (int i = start + 1; i < end; ++i)
                        {
                            if ((cr_index[i] / _columns) != (cr_index[i - 1] / _columns)) // different columns
                            {
                                ++col_count;
                            }
                        }
                    }

                    int[] ri = new int[_depth + 1],
                        nzc = new int[col_count],
                        ci = new int[col_count + 1],
                        di = new int[nnz];

                    // populate values
                    int k = 0;
                    for (int d = 0; d < _depth; ++d)
                    {
                        int start = depth_start[d],
                            end = depth_start[d + 1],
                            nz = 0;

                        for (int i = start; i < end; ++i)
                        {
                            if (i == start || ((cr_index[i] / _columns) != (cr_index[i - 1] / _columns)))
                            {
                                nzc[k++] = (int)(cr_index[i] / _columns);
                                ci[k] = i;
                                ++nz;
                            }
                            di[i] = (int)(cr_index[i] % _columns);
                        }
                        ri[d + 1] = ri[d] + nz;
                    }

                    return new CSFSparseTensor<T>(_columns, _rows, ri, nzc, ci, di, values);
                }
            }

            // Not implemented yet - fallback to the original transpose implementation
            return Transpose(permutation);
        }

        public override T ToScalar()
        {
            if (_rows == 1 && _columns == 1 && _depth == 1)
            {
                if (_values.Length > 0)
                {
                    return _values[0];
                }
                return _zero;
            }

            throw new InvalidOperationException("Tensor is not of order 0.");
        }
        public override Vector<T> ToVector()
        {
            if (_rows == 1 && _columns == 1)
            {
                // Depth fibres
                return new SparseVector<T>(_depth, _di.Copy(), _values.Copy(), true);
            }
            if (_rows == 1 && _depth == 1)
            {
                // Column fibres
                return new SparseVector<T>(_columns, _nzc.Copy(), _values.Copy(), true);
            }
            if (_columns == 1 && _depth == 1)
            {
                // Row fibres
                int[] indices = new int[_values.Length];
                T[] values = _values.Copy();
                for (int r = 0, k = 0; r < _rows; ++r)
                {
                    if (_ri[r + 1] > _ri[r])
                    {
                        indices[k++] = r;
                    }
                }

                return new SparseVector<T>(_rows, indices, values, true);
            }

            throw new InvalidOperationException("Tensor is not of order 1.");
        }
        public override Matrix<T> ToMatrix()
        {
            // row x column
            if (_depth == 1)
            {
                return new CSRSparseMatrix<T>(_columns, _ri.Copy(), _nzc.Copy(), _values.Copy());
            }

            // row x depth
            if (_columns == 1)
            {
                return new CSRSparseMatrix<T>(_depth, _ri.Copy(), _di.Copy(), _values.Copy());
            }

            // column x depth
            if (_rows == 1)
            {
                // We would like to just copy over the columnIndex array ci, however it may be missing
                // some columns (e.g. when there are simply no nz entries in that entire column.
                int[] columnIndex = new int[_columns + 1];
                for (int i = 0; i < _nzc.Length; ++i)
                {
                    columnIndex[_nzc[i] + 1] += (_ci[i + 1] - _ci[i]);
                }
                for (int i = 0; i < _columns; ++i)
                {
                    columnIndex[i + 1] += columnIndex[i];
                }

                return new CSRSparseMatrix<T>(_depth, columnIndex, _di.Copy(), _values.Copy());
            }

            throw new InvalidOperationException("Tensor is not of order 2.");
        }

        /// <summary>
        /// Returns the n-th mode matrix unfolding of this tensor. 
        /// </summary>
        /// <param name="mode">The mode of the unfolding. Must be between 0 and 2 inclusive.</param>
        /// <returns>The matrix unfolding of this tensor.</returns>
        public CSRSparseMatrix<T> Unfold(int mode)
        {
            if (mode < 0 || mode > 2)
            {
                throw new ArgumentOutOfRangeException(nameof(mode));
            }

            if (mode == 0)
            {
                //return Unfold_Mode0();
                //return Unfold_Mode0_Variant1();
                return Unfold_Mode0_Variant2();
            }
            else if (mode == 1)
            {
                //return Unfold_Mode1();
                return Unfold_Mode1_Variant1();
            }
            else if (mode == 2)
            {
                int nnz = _values.Length;
                int[] rowIndex = new int[_depth + 1];
                int[] columnIndex = new int[nnz];
                T[] values = new T[nnz];

                // Calculate the nz pattern
                for (int i = 0; i < nnz; ++i)
                {
                    rowIndex[_di[i]]++;
                }
                // Shift and accumulate
                for (int i = _depth - 1; i >= 0; --i)
                {
                    rowIndex[i + 1] = rowIndex[i];
                }
                rowIndex[0] = 0;
                for (int i = 0; i < _depth; ++i)
                {
                    rowIndex[i + 1] += rowIndex[i];
                }

                int[] nextIndex = rowIndex.Copy();
                for (int r = 0; r < _rows; ++r)
                {
                    for (int i = _ri[r], rend = _ri[r + 1]; i < rend; ++i)
                    {
                        int index = _nzc[i] * _rows + r;
                        for (int j = _ci[i], cend = _ci[i + 1]; j < cend; ++j)
                        {
                            int k = nextIndex[_di[j]]++;
                            columnIndex[k] = index;
                            values[k] = _values[j];
                        }
                    }
                }

                return new CSRSparseMatrix<T>(_rows * _columns, rowIndex, columnIndex, values);
            }

            throw new NotImplementedException();
        }

        /// <summary>
        /// Returns the vectorized version of this tensor, in the order specified by the specified permutation vector
        /// </summary>
        /// <param name="permutation">The order of dimensions to traverse through. Defaults to [0, 1, 2]</param>
        /// <returns></returns>
        public SparseVector<T> Vectorize(params int[] permutation)
        {
            if (!(permutation is null || permutation.Length == 0))
            {
                if (!IsPermutationVector(permutation))
                {
                    throw new ArgumentNullException(nameof(permutation), "Vector does not represent a permutation.");
                }
            }
            else
            {
                permutation = new int[] { 0, 1, 2 };
            }

            // Vectorize
            int[] vect_index = new int[_values.Length],
                index = new int[3];
            T[] values = _values.Copy();

            int[] dims = GetDimensions();
            int[] permutedDims = new int[dims.Length];
            for (int d = 0; d < dims.Length; ++d)
            {
                permutedDims[permutation[d]] = dims[d];
            }
            int d12 = permutedDims[1] * permutedDims[2],
                d2 = permutedDims[2];

            for (int r = 0, k = 0; r < _rows; ++r)
            {
                int row_start = _ri[r], row_end = _ri[r + 1];
                index[permutation[0]] = r;

                for (int ci = row_start; ci < row_end; ++ci)
                {
                    int c = _nzc[ci],
                        col_start = _ci[ci],
                        col_end = _ci[ci + 1];
                    index[permutation[1]] = c;

                    for (int i = col_start; i < col_end; ++i, ++k)
                    {
                        index[permutation[2]] = _di[i];
                        vect_index[k] = index[0] * d12 + index[1] * d2 + index[2];
                    }
                }
            }

            return new SparseVector<T>(_rows * _columns * _depth, vect_index, values);
        }

        #endregion


        #region Debugging methods

        internal void PrintStructure()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("ri\t");
            foreach (int d in _ri) sb.Append(d + "\t");
            sb.AppendLine();

            sb.Append("nzc\t");
            foreach (int d in _nzc) sb.Append(d + "\t");
            sb.AppendLine();

            sb.Append("ci\t");
            foreach (int d in _ci) sb.Append(d + "\t");
            sb.AppendLine();

            sb.Append("di\t");
            foreach (int d in _di) sb.Append(d + "\t");
            sb.AppendLine();

            sb.Append("value\t");
            foreach (T d in _values) sb.Append(d + "\t");
            sb.AppendLine();

            Debug.WriteLine(sb.ToString());
        }

        #endregion
        

        #region Operators 

        public static CSFSparseTensor<T> operator +(CSFSparseTensor<T> A, CSFSparseTensor<T> B)
        {
            if (A is null) throw new ArgumentNullException(nameof(A));
            if (B is null) throw new ArgumentNullException(nameof(B));
            return A.Add(B, ProviderFactory.GetDefaultProvider<T>());
        }
        public static CSFSparseTensor<T> operator *(CSFSparseTensor<T> A, T scalar)
        {
            if (A is null) throw new ArgumentNullException(nameof(A));
            return A.Multiply(scalar, ProviderFactory.GetDefaultBLAS1<T>());
        }
        public static CSFSparseTensor<T> operator *(T scalar, CSFSparseTensor<T> A)
        {
            if (A is null) throw new ArgumentNullException(nameof(A));
            return A.Multiply(scalar, ProviderFactory.GetDefaultBLAS1<T>());
        }
        public static CSFSparseTensor<T> operator -(CSFSparseTensor<T> A, CSFSparseTensor<T> B)
        {
            if (A is null) throw new ArgumentNullException(nameof(A));
            return A.Subtract(B, ProviderFactory.GetDefaultSparseBLAS1<T>());
        }
        public static CSFSparseTensor<T> operator -(CSFSparseTensor<T> A)
        {
            if (A is null)
            {
                throw new ArgumentNullException(nameof(A));
            }
            return A.Negate(ProviderFactory.GetDefaultBLAS1<T>());
        }

        #endregion


        #region Casts 

        public static explicit operator CSFSparseTensor<T>(COOSparseTensor<T> tensor) => new CSFSparseTensor<T>(tensor);

        #endregion

    }
}
