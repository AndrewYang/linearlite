﻿using LinearNet.Global;
using LinearNet.Helpers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace LinearNet.Structs.Tensors
{
    public static class CSFSparseTensor
    {
        /// <summary>
        /// Create a random CSFSparseTensor object, of dimension rows x columns x depth, with an approximate 
        /// density and where each entry is independently and identically distributed according to a random 
        /// generator. 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="rows"></param>
        /// <param name="columns"></param>
        /// <param name="depth"></param>
        /// <param name="random"></param>
        /// <param name="density"></param>
        /// <returns></returns>
        public static CSFSparseTensor<T> Random<T>(int rows, int columns, int depth, Func<T> random = null, double density = 0.1, int seed = int.MinValue) where T : new()
        {
            if (rows <= 0) throw new ArgumentOutOfRangeException(nameof(rows), ExceptionMessages.NonPositiveNotAllowed);
            if (columns <= 0) throw new ArgumentOutOfRangeException(nameof(columns), ExceptionMessages.NonPositiveNotAllowed);
            if (depth <= 0) throw new ArgumentOutOfRangeException(nameof(depth), ExceptionMessages.NonPositiveNotAllowed);
            if (density < 0 || density > 1) throw new ArgumentOutOfRangeException(nameof(density), ExceptionMessages.NotBetween0And1);

            if (random is null)
            {
                random = DefaultGenerators.GetRandomGenerator<T>();
            }

            // Initialize to approximate size, with leeway
            int expected_nz_count = (int)(1.1 * rows * columns * depth * density);

            int[] rowIndices = new int[rows + 1];
            List<int> nzColumns = new List<int>();
            List<int> columnIndices = new List<int>() { 0 };
            List<int> depthIndices = new List<int>(expected_nz_count);
            List<T> values = new List<T>(expected_nz_count);

            // Slow method for small matrices
            Random rand;
            if (seed == int.MinValue)
            {
                rand = new Random();
            }
            else
            {
                rand = new Random(seed);
            }

            for (int r = 0; r < rows; ++r)
            {
                for (int c = 0; c < columns; ++c)
                {
                    bool any = false;
                    for (int d = 0; d < depth; ++d)
                    {
                        if (rand.NextDouble() < density)
                        {
                            depthIndices.Add(d);
                            values.Add(random());
                            any = true;
                        }
                    }

                    if (any)
                    {
                        nzColumns.Add(c);
                        columnIndices.Add(values.Count);
                    }
                }
                rowIndices[r + 1] = nzColumns.Count;
            }
            return new CSFSparseTensor<T>(columns, depth, rowIndices, nzColumns.ToArray(), columnIndices.ToArray(), depthIndices.ToArray(), values.ToArray());
        }
    }
}
