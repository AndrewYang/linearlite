﻿using LinearNet.Global;
using LinearNet.Matrices.Multiplication;
using LinearNet.Providers;
using LinearNet.Providers.Basic.Interfaces;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace LinearNet.Structs
{
    /// <summary>
    /// <p>Implementation of an arbitrary-order dense tensor.</p>
    /// <p>
    /// Although this class supports tensors of all non-negative orders, it is best suited for tensors of order 3 and above, for performance reasons.
    /// Use the <txt>IMatrix<T></txt> interface for order 2 tensors, and the <txt>IVector</txt> interface for order-1 tensors.
    /// </p>
    /// </summary>
    /// <cat>linear-algebra</cat>
    public class DenseTensor<T> : Tensor<T> where T : new()
    {
        private int _order;
        private int[] _dimensions;
        private readonly T[] _values;

        /// <summary>
        /// Indicates whether this tensor requires a 64-bit integer
        /// to address its values array.
        /// </summary>
        private readonly bool _requiresLongIndex;

        public override int Order => _order;

        public override long NonZeroCount
        {
            get
            {
                long count = 1;
                for (int d = 0; d < _order; ++d)
                {
                    count *= _dimensions[d];
                }
                return count;
            }
        }

        public override IEnumerable<TensorEntry<T>> NonZeroEntries
        {
            get
            {
                if (_order == 0)
                {
                    yield return new TensorEntry<T>(new int[0], _values[0]);
                }
                else
                {
                    int[] index = new int[_order];
                    int incrementIndex = _order - 1,
                        incrementIndexDim = _dimensions[incrementIndex];

                    long len = _values.LongLength;
                    for (long i = 0; i < len; ++i)
                    {
                        if (index[incrementIndex] == incrementIndexDim)
                        {
                            int j = incrementIndex;
                            while (index[j] == _dimensions[j])
                            {
                                index[j] = 0;
                                --j;
                                index[j]++;
                            }
                        }

                        yield return new TensorEntry<T>(index.Copy(), _values[i]);

                        index[incrementIndex]++;
                    }
                }
            }
        }
        internal T[] Values { get { return _values; } }

        #region Constructors 

        /// <summary>
        /// Create an empty dense tensor of the specified dimensions, with 
        /// all entries equal to the default value of type $T$.
        /// </summary>
        /// <param name="dimensions"></param>
        public DenseTensor(params int[] dimensions)
        {
            if (dimensions is null)
            {
                throw new ArgumentNullException(nameof(dimensions));
            }
            _order = dimensions.Length;
            _dimensions = dimensions;
            _values = InitStorage(dimensions);
            _requiresLongIndex = _values.LongLength > int.MaxValue;
        }

        /// <summary>
        /// Create a 0-th order tensor (a scalar).
        /// </summary>
        /// <param name="scalar">The scalar value.</param>
        public DenseTensor(T scalar)
        {
            _order = 0;
            _values = new T[] { scalar };
            _requiresLongIndex = false;
        }

        /// <summary>
        /// Create a first-order tensor (a vector), using an array of values 
        /// representing a dense vector.
        /// </summary>
        /// <param name="vector">An array of values representing the vector.</param>
        public DenseTensor(T[] vector)
        {
            if (vector is null)
            {
                throw new ArgumentNullException(nameof(vector));
            }
            _order = 1;
            _dimensions = new int[] { vector.Length };
            _values = vector; // no copy!
            _requiresLongIndex = vector.LongLength > int.MaxValue;
        }

        /// <summary>
        /// Create a first-order tensor (a vector), using a <txt>Vector</txt> object.
        /// </summary>
        /// <param name="vector">A vector.</param>
        public DenseTensor(Vector<T> vector)
        {
            if (vector is null)
            {
                throw new ArgumentNullException(nameof(vector));
            }
            _order = 1;
            _dimensions = new int[] { vector.Dimension };
            _values = vector.ToArray();
            _requiresLongIndex = vector.LongDimension > int.MaxValue;
        }

        /// <summary>
        /// Create a second-order tensor (a matrix), using a rectangular array 
        /// of two dimensions.
        /// </summary>
        /// <param name="matrix">The 2-dimensional rectangular array.</param>
        public DenseTensor(T[,] matrix)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            int rows = matrix.GetLength(0), cols = matrix.GetLength(1);
            
            _order = 2;
            _dimensions = new int[] { rows, cols };
            _values = new T[rows * cols];

            // Store in row-major order.
            for (int r = 0, k = 0; r < rows; ++r)
            {
                for (int c = 0; c < cols; ++c)
                {
                    _values[k++] = matrix[r, c];
                }
            }
            _requiresLongIndex = _values.LongLength > int.MaxValue;
        }

        /// <summary>
        /// Create a second-order tensor (a matrix), using a <txt>Matrix</txt>
        /// object.
        /// </summary>
        /// <param name="matrix">The matrix object.</param>
        public DenseTensor(Matrix<T> matrix)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            _order = 2;
            _dimensions = new int[] { matrix.Rows, matrix.Columns };
            _values = RectangularVector.Zeroes<T>(matrix.Rows * matrix.Columns);

            int cols = matrix.Columns;
            foreach (MatrixEntry<T> e in matrix.NonZeroEntries)
            {
                _values[e.RowIndex * cols + e.ColumnIndex] = e.Value;
            }
            _requiresLongIndex = _values.LongLength > int.MaxValue;
        }

        /// <summary>
        /// Create a second-order tensor from a dense matrix.
        /// </summary>
        /// <param name="matrix">The dense matrix object.</param>
        public DenseTensor(DenseMatrix<T> matrix)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            int r = matrix.Rows, c = matrix.Columns; 

            _order = 2;
            _dimensions = new int[] { r, c };
            _values = new T[r * c];

            for (int i = 0; i < r; ++i)
            {
                Array.Copy(matrix.Values[i], 0, _values, i * c, c);
            }
            _requiresLongIndex = _values.LongLength > int.MaxValue;
        }

        /// <summary>
        /// Create a third-order tensor from a 3-dimensional rectangular array.
        /// </summary>
        /// <param name="tensor">The 3-dimensional rectangular array.</param>
        public DenseTensor(T[,,] tensor)
        {
            if (tensor is null)
            {
                throw new ArgumentNullException(nameof(tensor));
            }

            int m = tensor.GetLength(0), n = tensor.GetLength(1), p = tensor.GetLength(2);
            _order = 3;
            _dimensions = new int[] { m, n, p };
            _values = new T[(long)m * n * p];

            for (int i = 0, z = 0; i < m; ++i)
            {
                for (int j = 0; j < n; ++j)
                {
                    for (int k = 0; k < p; ++k)
                    {
                        _values[z++] = tensor[i, j, k];
                    }
                }
            }
            _requiresLongIndex = _values.LongLength > int.MaxValue;
        }

        /// <summary>
        /// Create a fourth-order tensor from a 4-dimensional rectangular array.
        /// </summary>
        /// <param name="tensor">A 4-dimensional rectangular array.</param>
        public DenseTensor(T[,,,] tensor)
        {
            if (tensor is null)
            {
                throw new ArgumentNullException(nameof(tensor));
            }

            int m = tensor.GetLength(0),
                n = tensor.GetLength(1),
                p = tensor.GetLength(2),
                q = tensor.GetLength(3);

            _order = 4;
            _dimensions = new int[] { m, n, p, q };
            _values = new T[(long)m * n * p * q];
            _requiresLongIndex = _values.LongLength > int.MaxValue;

            if (_requiresLongIndex)
            {
                long z = 0;
                for (int i = 0; i < m; ++i)
                {
                    for (int j = 0; j < n; ++j)
                    {
                        for (int k = 0; k < p; ++k)
                        {
                            for (int l = 0; l < q; ++l)
                            {
                                _values[z++] = tensor[i, j, k, l];
                            }
                        }
                    }
                }
            }
            else
            {
                int z = 0;
                for (int i = 0; i < m; ++i)
                {
                    for (int j = 0; j < n; ++j)
                    {
                        for (int k = 0; k < p; ++k)
                        {
                            for (int l = 0; l < q; ++l)
                            {
                                _values[z++] = tensor[i, j, k, l];
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Create a arbitrary-order tensor given its dimensions, and an array
        /// of values. The length of the values array must match the product 
        /// of the dimensions.
        /// </summary>
        /// <param name="dimensions"></param>
        /// <param name="values"></param>
        public DenseTensor(int[] dimensions, T[] values)
        {
            if (dimensions is null)
            {
                throw new ArgumentNullException(nameof(dimensions));
            }
            if (values is null)
            {
                throw new ArgumentNullException(nameof(values));
            }
            _order = dimensions.Length;
            _dimensions = dimensions;
            _values = values;
            _requiresLongIndex = values.LongLength > int.MaxValue;
        }

        /// <summary>
        /// Copy constructor
        /// </summary>
        /// <param name="tensor"></param>
        public DenseTensor(DenseTensor<T> tensor)
        {
            if (tensor is null)
            {
                throw new ArgumentNullException(nameof(tensor));
            }
            _order = tensor._order;
            _dimensions = tensor._dimensions.Copy();
            _values = tensor._values.Copy();
            _requiresLongIndex = tensor._requiresLongIndex;
        }

        /// <summary>
        /// Create a dense tensor equal to the outer product of a set of dense vectors.
        /// </summary>
        /// <param name="vectors">The vectors.</param>
        /// <param name="provider">The provider used for elementwise products.</param>
        public DenseTensor(DenseVector<T>[] vectors, IProvider<T> provider)
        {
            if (vectors is null)
            {
                throw new ArgumentNullException(nameof(vectors));
            }

            _order = vectors.Length;
            _dimensions = new int[_order];
            for (int d = 0; d < _order; ++d)
            {
                if (vectors[d] is null)
                {
                    throw new ArgumentNullException($"{nameof(vectors)}[{d}]");
                }
                _dimensions[d] = vectors[d].Dimension;
            }
            _values = InitStorage(_dimensions);
            _requiresLongIndex = _values.LongLength > int.MaxValue;

            // Cache the product of all dimensions up to an index
            T[] semiProducts = new T[_order];
            int[] index = new int[_order];
            int last = _order - 1, lastDim = _dimensions[last];
            DenseVector<T> lastVect = vectors[last];

            // Precalculate all semiproduct as (0, 0, ..., 0)
            semiProducts[0] = vectors[0][0];
            for (int d = 1; d < last; ++d)
            {
                semiProducts[d] = provider.Multiply(semiProducts[d - 1], vectors[d][0]);
            }

            if (_requiresLongIndex)
            {
                long len = _values.LongLength;
                for (long i = 0L; i < len; ++i)
                {
                    if (index[last] == lastDim)
                    {
                        int j = last;
                        while (index[j] == _dimensions[j])
                        {
                            index[j] = 0; // reset the current dimension
                            --j;        // no need to check j < 0 since the len should be exactly correct
                            index[j]++; // carry over to the next dimension
                        }

                        // Update the semi product up to index j
                        // The semi product represents the product of all vector 0, ..., j
                        // inclusive, at their selected indexes
                        if (j == 0)
                        {
                            // Separate case for j = 0, guarantee that j > 0 going forward
                            semiProducts[0] = vectors[0][index[0]];
                            ++j;
                        }
                        for (; j < last; ++j)
                        {
                            semiProducts[j] = provider.Multiply(semiProducts[j - 1], vectors[j][index[j]]);
                        }
                    }

                    // increment the last index, and use
                    _values[i] = provider.Multiply(semiProducts[last - 1], lastVect[index[last]++]);
                }
            }
            else
            {
                int len = _values.Length;
                for (int i = 0; i < len; ++i)
                {
                    if (index[last] == lastDim)
                    {
                        int j = last;
                        while (index[j] == _dimensions[j])
                        {
                            index[j] = 0; // reset the current dimension
                            --j;        // no need to check j < 0 since the len should be exactly correct
                            index[j]++; // carry over to the next dimension
                        }

                        // Update the semi product up to index j
                        // The semi product represents the product of all vector 0, ..., j
                        // inclusive, at their selected indexes
                        if (j == 0)
                        {
                            // Separate case for j = 0, guarantee that j > 0 going forward
                            semiProducts[0] = vectors[0][index[0]];
                            ++j;
                        }
                        for (; j < last; ++j)
                        {
                            semiProducts[j] = provider.Multiply(semiProducts[j - 1], vectors[j][index[j]]);
                        }
                    }

                    // increment the last index, and use
                    _values[i] = provider.Multiply(semiProducts[last - 1], lastVect[index[last]++]);
                }
            }
        }

        /// <summary>
        /// Create a dense tensor using a Kruskal-format tensor.
        /// </summary>
        /// <param name="ktensor">A tensor in Kruskal format.</param>
        public DenseTensor(KruskalTensor<T> ktensor)
        {
            if (ktensor is null)
            {
                throw new ArgumentNullException(nameof(ktensor));
            }

            IProvider<T> provider = ktensor.Provider;
            _order = ktensor.Order;
            _dimensions = ktensor.GetDimensions().Copy();
            _values = InitStorage(_dimensions);
            _requiresLongIndex = _values.LongLength > int.MaxValue;

            // Requires proper initialization
            T zero = provider.Zero;
            if (!zero.Equals(default))
            {
                if (_requiresLongIndex)
                {
                    long len = _values.LongLength;
                    for (long i = 0; i < len; ++i)
                    {
                        _values[i] = zero;
                    }
                }
                else
                {
                    int len = _values.Length;
                    for (int i = 0; i < len; ++i)
                    {
                        _values[i] = zero;
                    }
                }
            }

            T[][][] values = ktensor.Values;
            T[] lambda = ktensor.Lambda;

            // partial products[d] = product of v[0] * v[1] * ... * v[d] from rank slice 
            // i, in the current iteration
            T[] partialProducts = new T[_order];
            int[] index = new int[_order];

            int rank = values.Length,
                incrementIndex = _order - 1,
                incrementIndexDim = _dimensions[incrementIndex];

            for (int r = 0; r < rank; ++r)
            {
                Array.Clear(index, 0, _order);
                T[][] slice = values[r];
                T[] lastSlice = slice[incrementIndex];
                T factor = lambda[r];

                // Precalculate all partial products except for the last term (unneeded)
                partialProducts[0] = slice[0][0];
                for (int d = 1; d < incrementIndex; ++d)
                {
                    partialProducts[d] = provider.Multiply(partialProducts[d - 1], slice[d][0]);
                }

                long len = _values.LongLength;
                for (long i = 0; i < len; ++i)
                {
                    if (index[incrementIndex] == incrementIndexDim)
                    {
                        int j = incrementIndex;
                        while (index[j] == _dimensions[j])
                        {
                            index[j] = 0;
                            index[--j]++;
                        }

                        if (j == 0)
                        {
                            partialProducts[0] = slice[0][index[0]];
                            ++j;
                        }

                        // Go up to _order - 1
                        for (; j < incrementIndex; ++j)
                        {
                            partialProducts[j] = provider.Multiply(partialProducts[j - 1], slice[j][index[j]]);
                        }
                    }

                    int ind = index[incrementIndex]++;
                    _values[i] = provider.Add(_values[i], 
                        provider.Multiply(factor,
                        provider.Multiply(partialProducts[incrementIndex - 1], lastSlice[ind])));
                }
            }
        }

        /// <summary>
        /// Create a dense tensor from a sparse tensor.
        /// </summary>
        /// <param name="stensor">A sparse tensor.</param>
        public DenseTensor(SparseTensor<T> stensor)
        {
            if (stensor is null)
            {
                throw new ArgumentNullException(nameof(stensor));
            }

            _order = stensor.Order;
            _dimensions = stensor.GetDimensions().Copy();
            _values = InitStorage(_dimensions);
            _requiresLongIndex = _values.Length > int.MaxValue;

            T zero = new T();
            if (_requiresLongIndex)
            {
                long len = _values.LongLength;
                for (long i = 0; i < len; ++i)
                {
                    _values[i] = zero; 
                }
            }
            else
            {
                int len = _values.Length;
                for (int i = 0; i < len; ++i)
                {
                    _values[i] = zero;
                }
            }

            // The annoying thing is that sparse tensors are 
            // encoded in a different way to dense tensor indexes
            // so we need to do an expensive translation here
            int[] index = new int[_order];

            Dictionary<long, T> values = stensor.Values;
            foreach (KeyValuePair<long, T> e in values)
            {
                stensor.get_index(e.Key, index);
                _values[GetKeyInt64(index)] = e.Value;
            }
        }

        /// <summary>
        /// Create a dense tensor from a arbitrary tensor object.
        /// <p>
        /// Note: this constructor can be much slower than one of the 
        /// more specialised constructors targetting a specific tensor type.
        /// </p>
        /// </summary>
        /// <param name="tensor">The tensor object.</param>
        public DenseTensor(Tensor<T> tensor)
        {
            if (tensor is null)
            {
                throw new ArgumentNullException(nameof(tensor));
            }

            _order = tensor.Order;
            _dimensions = tensor.GetDimensions().Copy();
            _values = InitStorage(_dimensions);

            T zero = new T();
            if (!zero.Equals(default))
            {
                long len = _values.LongLength;
                for (long i = 0; i < len; ++i)
                {
                    _values[i] = zero;
                }
            }

            foreach (TensorEntry<T> e in tensor.NonZeroEntries)
            {
                _values[GetKeyInt64(e.Index)] = e.Value;
            }
        }

        private static T[] InitStorage(int[] dimensions)
        {
            long size = 1L;
            for (int i = 0; i < dimensions.Length; ++i)
            {
                checked
                {
                    size *= dimensions[i];
                }
            }

            if (size > int.MaxValue)
            {
                return new T[size];
            }
            else
            {
                return new T[(int)size];
            }
        }

        #endregion


        /// <summary>
        /// Get the index within the _values array. This method is unchecked
        /// </summary>
        internal int GetKeyInt32(int[] index)
        {
            int i = 0;
            for (int d = 0; d < _order; ++d)
            {
                i *= _dimensions[d];
                i += index[d];
            }
            return i;
        }
        internal long GetKeyInt64(int[] index)
        {
            long i = 0L;
            for (int d = 0; d < _order; ++d)
            {
                i *= _dimensions[d];
                i += index[d];
            }
            return i;
        }
        private void GetIndex(int key, int[] index)
        {
            for (int d = _order - 1; d >= 0; --d)
            {
                int dim = _dimensions[d],
                    i = key % dim;
                key = (key - i) / dim;

                index[d] = i;
            }
        }
        private void GetIndex(long key, int[] index)
        {
            for (int d = _order - 1; d >= 0; --d)
            {
                long dim = _dimensions[d],
                    i = key % dim;
                key = (key - i) / dim;

                index[d] = (int)i;
            }
        }
        private void AssertValid(int[] index)
        {
            if (index is null)
            {
                throw new ArgumentNullException(nameof(index));
            }
            if (index.Length < _order)
            {
                throw new ArgumentOutOfRangeException(nameof(index));
            }

            for (int i = 0; i < _order; ++i)
            {
                int ind = index[i];
                if (ind < 0 || ind >= _dimensions[i])
                {
                    throw new ArgumentOutOfRangeException(nameof(index));
                }
            }
        }

        /// <summary>
        /// Calculate the tensor contraction by prioritizing speed over memory usage.
        /// This method will create a temporary workspace of size O(k) where k is the capacity of 
        /// <txt>tensor</txt>.
        /// </summary>
        private DenseTensor<T> Contract_PrioritizeSpeed(DenseTensor<T> tensor, int dimension1, int dimension2, IDenseBLAS1<T> b)
        {
            // Perform a cyclic transposition so that dimension2 is the last 
            // dimension of tensor2
            int[] permutation = new int[_order];
            for (int i = 0, j = 0; i < tensor._order; ++i)
            {
                if (i == dimension2)
                {
                    permutation[i] = tensor._order - 1;
                }
                else
                {
                    permutation[i] = j++;
                }
            }

            // Transpose the tensor 
            tensor = tensor.Transpose(permutation);
            dimension2 = tensor._order - 1;

            // Array holding the dimensions of the product tensor
            int[] dimensions = new int[_order + tensor._order - 2];

            // Initialize two lookup arrays that convert tensor1, tensor2 indices
            // into product indices.
            int[] index1Lookup = new int[_order],
                index2Lookup = new int[tensor._order];

            // Calculate the dimension of the tensor product
            int k = 0, d;
            for (d = 0; d < _order; ++d)
            {
                if (d != dimension1)
                {
                    index1Lookup[d] = k;
                    dimensions[k++] = _dimensions[d];
                }
            }
            // It is known that the 2nd tensor contraction dimension is the last dimension
            for (d = 0; d < dimension2; ++d)
            {
                index2Lookup[d] = k;
                dimensions[k++] = tensor._dimensions[d];
            }

            int dim = _dimensions[dimension1];

            // Indexes for accessing entries in tensor1, tensor2 and the product tensor
            int[] index1 = new int[_order],
                index2 = new int[tensor._order];

            // Workspaces for temporarily holding the two vectors from tensor1, tensor2
            // along the contracted dimensions, prior to computing their dot products.
            T[] workspace = new T[dim];

            // The indexes from tensor1 and tensor2 along which we increment
            int incrementIndex1 = dimension1 == _order - 1 ? _order - 2 : _order - 1,
                incrementIndex2 = tensor._order - 2, // the second tensor is known to contract along its last dimension
                incrementIndexDim1 = _dimensions[incrementIndex1],
                incrementIndexDim2 = tensor._dimensions[incrementIndex2];

            int len1 = _values.Length / _dimensions[dimension1],
                len2 = tensor._values.Length / tensor._dimensions[dimension2];

            DenseTensor<T> product = new DenseTensor<T>(dimensions);
            T[] values = product._values;
            for (int i = 0, pi = 0; i < len1; ++i)
            {
                if (index1[incrementIndex1] == incrementIndexDim1)
                {
                    int j = incrementIndex1;
                    while (index1[j] == _dimensions[j])
                    {
                        index1[j--] = 0;
                        if (j == dimension1) --j; // skip the incremented dimension
                        index1[j]++;
                    }
                }

                // Fill workspace 1 with the selected fibre
                index1[dimension1] = 0;
                int i1 = GetKeyInt32(index1);
                workspace[0] = _values[i1];

                index1[dimension1] = 1;
                int i2 = GetKeyInt32(index1), step = i2 - i1;
                for (int z = 1; z < dim; ++z, i2 += step)
                {
                    workspace[z] = _values[i2];
                }

                // Clear index2 array to restart the inner loop
                Array.Clear(index2, 0, tensor._order);

                for (int j = 0; j < len2; ++j)
                {
                    if (index2[incrementIndex2] == incrementIndexDim2)
                    {
                        int p = incrementIndex2;
                        while (index2[p] == tensor._dimensions[p])
                        {
                            index2[p--] = 0;
                            if (p == dimension2) --p;
                            index2[p]++;
                        }
                    }

                    // Since the contracted dimension is the last from tensor2,
                    // the entries along this fibre are contingous
                    // Also, the value along dimension 'dimension2' will always
                    // be zero so we don't need to set it every time
                    int j1 = tensor.GetKeyInt32(index2);

                    // Set the dot product into the product tensor
                    values[pi++] = b.DOT(workspace, tensor._values, 0, dim, j1);

                    index2[incrementIndex2]++;
                }

                index1[incrementIndex1]++;
            }

            return product;
        }
        private DenseTensor<T> Contract_PrioritizeMemory(DenseTensor<T> tensor, int dimension1, int dimension2, IDenseBLAS1<T> b)
        {
            // Array holding the dimensions of the product tensor
            int[] dimensions = new int[_order + tensor._order - 2];

            // Initialize two lookup arrays that convert tensor1, tensor2 indices
            // into product indices.
            int[] index1Lookup = new int[_order],
                index2Lookup = new int[tensor._order];

            // Calculate the dimension of the tensor product
            int k = 0, d;
            for (d = 0; d < _order; ++d)
            {
                if (d != dimension1)
                {
                    index1Lookup[d] = k;
                    dimensions[k++] = _dimensions[d];
                }
            }
            for (d = 0; d < tensor._order; ++d)
            {
                if (d != dimension2)
                {
                    index2Lookup[d] = k;
                    dimensions[k++] = tensor._dimensions[d];
                }
            }

            int dim = _dimensions[dimension1];

            // Indexes for accessing entries in tensor1, tensor2 and the product tensor
            int[] index1 = new int[_order],
                index2 = new int[tensor._order]
                /*,
                index = new int[dimensions.Length]*/;

            // Workspaces for temporarily holding the two vectors from tensor1, tensor2
            // along the contracted dimensions, prior to computing their dot products.
            T[] workspace1 = new T[dim],
                workspace2 = new T[dim];

            // The indexes from tensor1 and tensor2 along which we increment
            int incrementIndex1 = dimension1 == _order - 1 ? _order - 2 : _order - 1,
                incrementIndex2 = dimension2 == tensor._order - 1 ? tensor._order - 2 : tensor._order - 1,
                incrementIndexDim1 = _dimensions[incrementIndex1],
                incrementIndexDim2 = tensor._dimensions[incrementIndex2];

            long len1 = _values.LongLength / _dimensions[dimension1],
                len2 = tensor._values.LongLength / tensor._dimensions[dimension2];

            DenseTensor<T> product = new DenseTensor<T>(dimensions);
            T[] values = product._values;
            for (long i = 0, pi = 0; i < len1; ++i)
            {
                if (index1[incrementIndex1] == incrementIndexDim1)
                {
                    int j = incrementIndex1;
                    while (index1[j] == _dimensions[j])
                    {
                        index1[j--] = 0;
                        if (j == dimension1) --j; // skip the incremented dimension
                        index1[j]++;
                    }

                    // Update all changed indices in the product index array
                    /*for (; j < _order; ++j)
                    {
                        index[index1Lookup[j]] = index1[j];
                    }*/
                }

                // Update the product index array (only the last index is changed)
                //index[index1Lookup[incrementIndex1]] = index1[incrementIndex1];

                // Fill workspace 1 with the selected fibre
                index1[dimension1] = 0;
                long i1 = GetKeyInt64(index1);
                workspace1[0] = _values[i1];

                index1[dimension1] = 1;
                long i2 = GetKeyInt64(index1), step = i2 - i1;
                for (int z = 1; z < dim; ++z, i2 += step)
                {
                    workspace1[z] = _values[i2];
                }

                // Clear index2 array to restart the inner loop
                Array.Clear(index2, 0, tensor._order);
                //Array.Clear(index, _order - 1, tensor._order - 1);

                for (long j = 0; j < len2; ++j)
                {
                    if (index2[incrementIndex2] == incrementIndexDim2)
                    {
                        int p = incrementIndex2;
                        while (index2[p] == tensor._dimensions[p])
                        {
                            index2[p--] = 0;
                            if (p == dimension2) --p;
                            index2[p]++;
                        }

                        // Update all changed indices in the product index array
                        /*for (; p < tensor._order; ++p)
                        {
                            index[index2Lookup[p]] = index2[p];
                        }*/
                    }

                    // Fill the workspace for tensor2 for the dot product
                    index2[dimension2] = 0;
                    long j1 = tensor.GetKeyInt64(index2);
                    workspace2[0] = tensor._values[j1];

                    index2[dimension2] = 1;
                    long j2 = tensor.GetKeyInt64(index2),
                        step_j2 = j2 - j1;
                    for (int z = 1; z < dim; ++z, j2 += step_j2)
                    {
                        workspace2[z] = tensor._values[j2];
                    }

                    // Set the dot product into the product tensor
                    values[pi++] = b.DOT(workspace1, workspace2, 0, dim);

                    index2[incrementIndex2]++;
                }

                index1[incrementIndex1]++;
            }

            return product;
        }


        #region Public methods

        public DenseTensor<T> Add(DenseTensor<T> tensor, IDenseBLAS1<T> blas)
        {
            if (tensor is null)
            {
                throw new ArgumentNullException(nameof(tensor));
            }
            if (blas is null)
            {
                throw new ArgumentNullException(nameof(blas));
            }
            AssertDimensionsMatch(_dimensions, tensor._dimensions);

            T[] sum = new T[_values.LongLength];

            // blas does not support > int.MaxValue lengths
            if (_requiresLongIndex)
            {
                blas.ADD(_values, tensor._values, sum, 0, int.MaxValue);

                IProvider<T> provider = blas.Provider;
                long len = _values.LongLength;
                for (long i = int.MaxValue; i < len; ++i)
                {
                    sum[i] = provider.Add(_values[i], tensor._values[i]);
                }
            }
            else
            {
                blas.ADD(_values, tensor._values, sum, 0, sum.Length);
            }

            return new DenseTensor<T>(_dimensions.Copy(), sum);
        }
        /// <summary>
        /// Calculates the tensor contraction between this tensor and itself, along the two specified dimensions.
        /// </summary>
        /// <param name="dimension1">The first dimension to contact along.</param>
        /// <param name="dimension2">The second dimension to contract along.</param>
        /// <param name="provider">The provider used for elementwise operations.</param>
        /// <returns>The contracted tensor, which has an order 2 less than this tensor.</returns>
        public DenseTensor<T> Contract(int dimension1, int dimension2, IProvider<T> provider)
        {
            if (dimension1 < 0 || dimension1 >= _order)
            {
                throw new ArgumentOutOfRangeException(nameof(dimension1));
            }
            if (dimension2 < 0 || dimension2 >= _order)
            {
                throw new ArgumentOutOfRangeException(nameof(dimension2));
            }
            if (_dimensions[dimension1] != _dimensions[dimension2])
            {
                throw new InvalidOperationException($"Dimensions do not match: {_dimensions[dimension1]} != {_dimensions[dimension2]}.");
            }
            if (_order < 2)
            {
                throw new InvalidOperationException("Tensor contractions are only defined for tensors with order >= 2.");
            }

            // Need to handle the scalar case differently
            if (_order == 2)
            {
                T scalar = provider.Zero;
                int size = _dimensions[0];
                long k = 0, step = _dimensions[1] + 1;
                for (int j = 0; j < size; ++j, k += step)
                {
                    scalar = provider.Add(scalar, _values[k]);
                }
                return new DenseTensor<T>(scalar);
            }

            int[] index = new int[_order];
            int incrementIndex = _order - 1;
            while (incrementIndex == dimension1 || incrementIndex == dimension2) --incrementIndex;
            int incrementIndexDim = _dimensions[incrementIndex];

            int dim = _dimensions[dimension1];
            long len = _values.LongLength;
            int[] newDimensions = new int[_order - 2];
            for (int d = 0, j = 0; d < _order; ++d)
            {
                if (d != dimension1 && d != dimension2)
                {
                    newDimensions[j++] = _dimensions[d];
                }
            }

            T[] values = RectangularVector.Zeroes<T>(len / dim / dim);
            for (int d = 0; d < dim; ++d)
            {
                index[dimension1] = d;
                index[dimension2] = d;

                for (int k = 0; ; ++k)
                {
                    if (index[incrementIndex] == incrementIndexDim)
                    {
                        int j = incrementIndex;
                        while (index[j] == _dimensions[j])
                        {
                            index[j] = 0;
                            do
                            {
                                --j;
                            } 
                            while (j == dimension1 || j == dimension2);

                            if (j < 0)
                            {
                                goto end;
                            }

                            index[j]++;
                        }
                    }

                    values[k] = provider.Add(values[k], _values[GetKeyInt64(index)]);
                    index[incrementIndex]++;
                }
            end:
                continue;
            }

            return new DenseTensor<T>(newDimensions, values);
        }
        /// <summary>
        /// Calculates the contracted tensor product between this tensor and another dense tensor, along dimension1 and dimension2
        /// respectively. The product tensor will be of order $o_A + o_B - 2$, where $o_A$ and $o_B$ are the orders of the 
        /// two tensors respectively. 
        /// </summary>
        /// <param name="tensor">The tensor to contract with.</param>
        /// <param name="dimension1">The dimension from this tensor to contract along.</param>
        /// <param name="dimension2">The dimension from the second tensor to contract along.</param>
        /// <param name="blas">The level-1 BLAS implementation to use.</param>
        /// <returns>The contracted tensor product.</returns>
        public DenseTensor<T> Contract(DenseTensor<T> tensor, int dimension1, int dimension2, IDenseBLAS1<T> blas, MultiplicationMode mode = MultiplicationMode.OPTIMIZE_SPEED)
        {
            if (tensor is null)
            {
                throw new ArgumentNullException(nameof(tensor));
            }
            if (dimension1 < 0 || dimension1 >= _order)
            {
                throw new ArgumentOutOfRangeException(nameof(dimension1));
            }
            if (dimension2 < 0 || dimension2 >= tensor._order)
            {
                throw new ArgumentOutOfRangeException(nameof(dimension2));
            }
            if (blas is null)
            {
                throw new ArgumentNullException(nameof(blas));
            }
            if (_dimensions[dimension1] != tensor._dimensions[dimension2])
            {
                throw new InvalidOperationException("Tensor dimension's don't match.");
            }

            if (mode == MultiplicationMode.OPTIMIZE_SPEED)
            {
                return Contract_PrioritizeSpeed(tensor, dimension1, dimension2, blas);
            }

            return Contract_PrioritizeMemory(tensor, dimension1, dimension2, blas);
        }
        public DenseTensor<T> Contract(DenseTensor<T> tensor, IEnumerable<int> contractedDimensions1, IEnumerable<int> contractedDimensions2, IDenseBLAS1<T> blas)
        {
            if (tensor is null)
            {
                throw new ArgumentNullException(nameof(tensor));
            }
            if (contractedDimensions1 is null)
            {
                throw new ArgumentNullException(nameof(contractedDimensions1));
            }
            if (contractedDimensions2 is null)
            {
                throw new ArgumentNullException(nameof(contractedDimensions2));
            }
            if (blas is null)
            {
                throw new ArgumentNullException(nameof(blas));
            }

            bool[] contracted1 = new bool[_order],
                contracted2 = new bool[tensor._order];

            foreach (int d in contractedDimensions1)
            {
                if (d < 0 || d >= _order)
                {
                    throw new ArgumentOutOfRangeException(nameof(contractedDimensions1));
                }
                contracted1[d] = true;
            }
            foreach (int d in contractedDimensions2)
            {
                if (d < 0 || d >= tensor._order)
                {
                    throw new ArgumentOutOfRangeException(nameof(contractedDimensions2));
                }
                contracted2[d] = true;
            }

            int newOrder = 0;
            for (int d = 0; d < _order; ++d)
            {
                if (!contracted1[d]) newOrder++;
            }
            for (int d = 0; d < tensor._order; ++d)
            {
                if (!contracted2[d]) newOrder++;
            }

            // The dimensions of the product tensor
            int[] dimensions = new int[newOrder];
            // The lookup table that converts tensor1, tensor2 indexes into product indexes
            int[] index1Lookup = new int[_order], index2Lookup = new int[tensor._order];
            // Calculate the lengths of tensor1, tensor2 without 
            long len1 = 1, len2 = 1;
            int k = 0;
            for (int d = 0; d < _order; ++d)
            {
                if (!contracted1[d])
                {
                    index1Lookup[d] = k;
                    int dim = _dimensions[d];
                    dimensions[k++] = dim;
                    len1 *= dim;
                }
            }
            for (int d = 0; d < tensor._order; ++d)
            {
                if (!contracted2[d])
                {
                    index2Lookup[d] = k;
                    int dim = tensor._dimensions[d];
                    dimensions[k++] = dim;
                    len2 *= dim;
                }
            }

            DenseTensor<T> product = new DenseTensor<T>(dimensions);
            T[] values = product._values;

            int[] index1 = new int[_order],
                index2 = new int[tensor._order],
                index = new int[product._order];

            // Find the index within tensor1, tensor2 which we should be 
            // incrementing over - this requires that we're not doing a 
            // full contraction (will need to handle that case above)
            int incrementIndex1 = _order - 1,
                incrementIndex2 = tensor._order - 1;
            while (contracted1[incrementIndex1]) --incrementIndex1;
            while (contracted2[incrementIndex2]) --incrementIndex2;
            int incrementIndex1Dim = _dimensions[incrementIndex1],
                incrementIndex2Dim = tensor._dimensions[incrementIndex2];
            for (int i = 0; i < len1; ++i)
            {
                if (index1[incrementIndex1] == incrementIndex1Dim)
                {
                    int j = incrementIndex1;
                    while (index1[j] == _dimensions[j])
                    {
                        index1[j--] = 0;
                        index1[j]++;
                    }
                }
                index1[incrementIndex1]++;
            }


            throw new NotImplementedException();
        }
        /// <summary>
        /// Returns a deep clone of this tensor.
        /// </summary>
        /// <returns>A deep copy of this tensor.</returns>
        public DenseTensor<T> Copy()
        {
            return new DenseTensor<T>(this);
        }
        /// <summary>
        /// Returns the direct sum of this tensor with another tensor of the same order.
        /// </summary>
        /// <param name="tensor">The tensor to direct sum with.</param>
        /// <returns>The direct sum of two dense tensors.</returns>
        public DenseTensor<T> DirectSum(DenseTensor<T> tensor)
        {
            if (tensor is null)
            {
                throw new ArgumentNullException(nameof(tensor));
            }
            if (tensor._order != _order)
            {
                throw new InvalidOperationException(ExceptionMessages.TensorOrderMismatch);
            }

            int[] dimension = new int[_order];
            for (int d = 0; d < _order; ++d)
            {
                long dim = (long)_dimensions[d] + tensor._dimensions[d];
                if (dim > int.MaxValue)
                {
                    throw new ArgumentOutOfRangeException($"Resulting tensor is too large to be represented as a {typeof(DenseTensor<T>)}");
                }
                dimension[d] = (int)dim;
            }

            // Overflow checks in constructor
            DenseTensor<T> sum = new DenseTensor<T>(dimension);

            // Insert values from this tensor
            int[] index = new int[_order];
            int last = index.Length - 1, lastDim = _dimensions[last], j = 0;
            if (_requiresLongIndex || sum._requiresLongIndex)
            {
                long len = _values.LongLength;
                for (long i = 0; i < len; ++i)
                {
                    if (index[last] == lastDim)
                    {
                        j = last;
                        while (index[j] == _dimensions[j]) // no checks for j >= 0 since we should never get there
                        {
                            index[j] = 0;
                            index[--j]++;
                        }
                    }

                    // insert the index
                    sum._values[sum.GetKeyInt64(index)] = _values[i];

                    // increment for the next cycle
                    index[last]++;
                }
            }
            else
            {
                int len = _values.Length;
                for (int i = 0; i < len; ++i)
                {
                    if (index[last] == lastDim)
                    {
                        j = last;
                        while (index[j] == _dimensions[j]) // no checks for j >= 0 since we should never get there
                        {
                            index[j] = 0;
                            index[--j]++;
                        }
                    }

                    // insert the index
                    sum._values[sum.GetKeyInt32(index)] = _values[i];

                    // increment for the next cycle
                    index[last]++;
                }
            }

            // init index to the corner
            Array.Copy(_dimensions, index, _order);
            // Update boundaries
            lastDim = sum._dimensions[_order - 1];

            // Insert values from the second tensor
            if (tensor._requiresLongIndex || sum._requiresLongIndex)
            {
                long len = tensor._values.LongLength;
                for (long i = 0; i < len; ++i)
                {
                    if (index[last] == lastDim)
                    {
                        j = last;
                        while (index[j] == sum._dimensions[j]) // no checks for j >= 0 since we should never get there
                        {
                            index[j] = _dimensions[j]; // reset to the corner
                            index[--j]++;
                        }
                    }

                    // insert the index
                    sum._values[sum.GetKeyInt64(index)] = tensor._values[i];

                    // increment for the next cycle
                    index[last]++;
                }
            }
            else
            {
                int len = tensor._values.Length;
                for (int i = 0; i < len; ++i)
                {
                    if (index[last] == lastDim)
                    {
                        j = last;
                        while (index[j] == sum._dimensions[j]) // no checks for j >= 0 since we should never get there
                        {
                            index[j] = _dimensions[j]; // reset to the corner
                            index[--j]++;
                        }
                    }

                    // insert the index
                    sum._values[sum.GetKeyInt32(index)] = tensor._values[i];

                    // increment for the next cycle
                    index[last]++;
                }
            }
            return sum;
        }
        /// <summary>
        /// Apply a function to each entry of this tensor, returning a new dense tensor without 
        /// modifying this one.
        /// </summary>
        /// <typeparam name="F"></typeparam>
        /// <param name="function">The function to apply elementwise to this tensor.</param>
        /// <returns>A tensor of function outputs, of the same dimensions as this tensor.</returns>
        public DenseTensor<F> Elementwise<F>(Func<T, F> function) where F : new()
        {
            long len = _values.LongLength;
            F[] values = new F[len];
            for (long i = 0; i < len; ++i)
            {
                values[i] = function(_values[i]);
            }
            return new DenseTensor<F>(_dimensions.Copy(), values);
        }
        /// <summary>
        /// Apply a two-argument function to each corresponding pair of entries from this tensor and another 
        /// tensor of the same dimensions, returning the result as another dense tensor.
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <typeparam name="T2"></typeparam>
        /// <param name="tensor">The tensor whose entries are used as a second argument in each function evaluation.</param>
        /// <param name="function">The two-argument function to apply to each pair of entries from two tensors.</param>
        /// <returns>A tensor whose entries are the outputs of the tensors, of the same size as each of the two input tensors.</returns>
        public DenseTensor<T2> Elementwise<T1, T2>(DenseTensor<T1> tensor, Func<T, T1, T2> function) where T1 : new() where T2 : new()
        {
            if (tensor is null)
            {
                throw new ArgumentNullException(nameof(tensor));
            }
            if (function is null)
            {
                throw new ArgumentNullException(nameof(function));
            }
            AssertDimensionsMatch(_dimensions, tensor._dimensions);

            if (_requiresLongIndex)
            {
                long len = _values.LongLength;
                T2[] values = new T2[len];
                for (long i = 0; i < len; ++i)
                {
                    values[i] = function(_values[i], tensor._values[i]);
                }
                return new DenseTensor<T2>(_dimensions.Copy(), values);
            }
            else
            {
                int len = _values.Length;
                T2[] values = new T2[len];
                for (int i = 0; i < len; ++i)
                {
                    values[i] = function(_values[i], tensor._values[i]);
                }
                return new DenseTensor<T2>(_dimensions.Copy(), values);
            }
        }
        public override bool Equals(Tensor<T> tensor, Func<T, T, bool> equals)
        {
            if (tensor is null)
            {
                throw new ArgumentNullException(nameof(tensor));
            }
            if (equals is null)
            {
                throw new ArgumentNullException(nameof(equals));
            }

            if (_order != tensor.Order)
            {
                return false;
            }

            int[] dimensions2 = tensor.GetDimensions();
            for (int d = 0; d < _order; ++d)
            {
                if (_dimensions[d] != dimensions2[d])
                {
                    return false;
                }
            }

            //BitArray found = new BitArray(_values.LongLength); // are you kidding me
            long len = _values.LongLength;
            bool[] found = new bool[len];
            foreach (TensorEntry<T> e in tensor.NonZeroEntries)
            {
                long index = GetKeyInt64(e.Index);
                if (!equals(_values[index], e.Value))
                {
                    return false;
                }
                found[index] = true;
            }

            // Loop through the dense array and make sure all unfound indices are 0 in value
            T zero = new T();
            for (long i = 0; i < len; ++i)
            {
                if (!found[i])
                {
                    if (!equals(zero, _values[i]))
                    {
                        return false;
                    }
                }
            }

            return true;
        }
        /// <summary>
        /// Returns whether this tensor is equal to another tensor in value, using a custom
        /// equality condition.
        /// </summary>
        /// <param name="tensor">The other tensor to compare to.</param>
        /// <param name="equals">A function that returns true if its two arguments should be 
        /// considered equal in value.</param>
        /// <returns>Whether the two tensors are equal in value.</returns>
        public bool Equals(DenseTensor<T> tensor, Func<T, T, bool> equals)
        {
            if (tensor is null)
            {
                throw new ArgumentNullException(nameof(tensor));
            }
            if (equals is null)
            {
                throw new ArgumentNullException(nameof(equals));
            }

            if (_order != tensor._order)
            {
                return false;
            }
            for (int d = 0; d < _order; ++d)
            {
                if (_dimensions[d] != tensor._dimensions[d])
                {
                    return false;
                }
            }

            long len = _values.Length;
            for (long i = 0; i < len; ++i)
            {
                if (!equals(_values[i], tensor._values[i]))
                {
                    return false;
                }
            }
            return true;
        }
        public bool Equals(DenseTensor<T> tensor, IEqualityComparer<T> comparer)
        {
            if (comparer is null)
            {
                throw new ArgumentNullException(nameof(comparer));
            }
            return Equals(tensor, comparer.Equals);
        }
        public bool Equals(DenseTensor<T> tensor)
        {
            return Equals(tensor, EqualityComparer<T>.Default);
        }
        public T FrobeniusNorm(IRealProvider<T> provider)
        {
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }

            T norm = provider.Zero;
            long len = _values.LongLength;
            for (long i = 0; i < len; ++i)
            {
                T d = _values[i];
                norm = provider.Add(norm, provider.Multiply(d, d));
            }
            return provider.Sqrt(norm);
        }
        public T FrobeniusDistance(DenseTensor<T> tensor, IRealProvider<T> provider)
        {
            if (tensor is null)
            {
                throw new ArgumentNullException(nameof(tensor));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            AssertDimensionsMatch(_dimensions, tensor._dimensions);
            
            T norm = provider.Zero;
            long len = _values.LongLength;
            for (long i = 0; i < len; ++i)
            {
                T d = provider.Subtract(_values[i], tensor._values[i]);
                norm = provider.Add(norm, provider.Multiply(d, d));
            }
            return provider.Sqrt(norm);
        }
        public override T Get(params int[] index)
        {
            AssertValid(index);

            if (_requiresLongIndex)
            {
                return _values[GetKeyInt64(index)];
            }
            return _values[GetKeyInt32(index)];
        }
        public override int[] GetDimensions()
        {
            return _dimensions;
        }
        public T InnerProduct(DenseTensor<T> tensor, IDenseBLAS1<T> blas)
        {
            if (blas is null)
            {
                throw new ArgumentNullException(nameof(blas));
            }
            if (tensor is null)
            {
                throw new ArgumentNullException(nameof(tensor));
            }
            AssertDimensionsMatch(_dimensions, tensor._dimensions);

            if (_requiresLongIndex)
            {
                T dot = blas.DOT(_values, tensor._values, 0, int.MaxValue);
                for (long i = int.MaxValue; i < _values.LongLength; ++i)
                {
                    dot = blas.Provider.Add(dot, blas.Provider.Multiply(_values[i], tensor._values[i]));
                }
                return dot;
            }
            else
            {
                return blas.DOT(_values, tensor._values, 0, _values.Length);
            }
        }
        public DenseTensor<T> Multiply(T scalar, IDenseBLAS1<T> blas)
        {
            DenseTensor<T> copy = Copy();
            copy.MultiplyInPlace(scalar, blas);
            return copy;
        }
        public DenseTensor<T> Multiply(DenseVector<T> vector, int mode, IDenseBLAS1<T> blas)
        {
            if (vector is null)
            {
                throw new ArgumentNullException(nameof(vector));
            }
            if (mode < 0 || mode >= _order)
            {
                throw new ArgumentOutOfRangeException(nameof(mode));
            }
            if (vector.Dimension != _dimensions[mode])
            {
                throw new ArgumentOutOfRangeException("Mismatched dimensions.");
            }
            if (blas is null)
            {
                throw new ArgumentNullException(nameof(blas));
            }

            // For temporary storage of the index
            int[] index = new int[_order];

            // The product tensor
            T[] product = new T[_values.LongLength / vector.Dimension];

            // The dimensions of the product tensor
            int[] dimensions = new int[_order - 1];
            Array.Copy(_dimensions, dimensions, mode);
            Array.Copy(_dimensions, mode + 1, dimensions, mode, _order - mode - 1);

            // The index we increment (cannot be the mode)
            int incrementIndex = mode == _order - 1 ? _order - 2 : _order - 1,
                incrementIndexDim = _dimensions[incrementIndex],
                modeDim = _dimensions[mode];

            // For workspace for storing a fibre of the tensor
            T[] workspace = new T[modeDim];

            if (product.LongLength > int.MaxValue)
            {
                // The product of dimensions coming before and after 'mode',
                // respectively
                long postMultiplier = 1;
                for (int d = mode + 1; d < _order; ++d)
                {
                    postMultiplier *= _dimensions[d];
                }

                long len = product.LongLength;
                for (long i = 0, z = 0; i < len; ++i)
                {
                    if (index[incrementIndex] == incrementIndexDim)
                    {
                        // Since incrementIndex is either the last index
                        // or the 2nd last if the last index is the mode, 
                        // we only need to iterate downwards
                        int j = incrementIndex;
                        while (index[j] == _dimensions[j])
                        {
                            index[j] = 0;

                            // Find the next non-mode index
                            --j;
                            if (j == mode) --j;

                            index[j]++;
                        }
                    }

                    // Calculate the first index in the fibre
                    index[mode] = 0;
                    long key = GetKeyInt64(index);

                    // Iterate across the fibre and store in the workspace
                    for (int k = 0; k < modeDim; ++k)
                    {
                        workspace[k] = _values[key];

                        // increment key by the postmultiplier, which will 
                        // step over the subtensor represented by dimensions
                        // mode + 1, ..., _order - 1 inclusive.
                        key += postMultiplier;
                    }

                    // store the dot product
                    product[z++] = blas.DOT(workspace, vector.Values, 0, modeDim);

                    index[incrementIndex]++;
                }
            }
            else
            {
                // The product of dimensions coming before and after 'mode',
                // respectively
                int postMultiplier = 1;
                for (int d = mode + 1; d < _order; ++d)
                {
                    postMultiplier *= _dimensions[d];
                }

                int len = product.Length;
                for (int i = 0, z = 0; i < len; ++i)
                {
                    if (index[incrementIndex] == incrementIndexDim)
                    {
                        // Since incrementIndex is either the last index
                        // or the 2nd last if the last index is the mode, 
                        // we only need to iterate downwards
                        int j = incrementIndex;
                        while (index[j] == _dimensions[j])
                        {
                            index[j] = 0;

                            // Find the next non-mode index
                            --j;
                            if (j == mode) --j;

                            index[j]++;
                        }
                    }

                    // Calculate the first index in the fibre
                    index[mode] = 0;
                    int key = GetKeyInt32(index);

                    // Iterate across the fibre and store in the workspace
                    for (int k = 0; k < modeDim; ++k)
                    {
                        workspace[k] = _values[key];

                        // increment key by the postmultiplier, which will 
                        // step over the subtensor represented by dimensions
                        // mode + 1, ..., _order - 1 inclusive.
                        key += postMultiplier;
                    }

                    // store the dot product
                    product[z++] = blas.DOT(workspace, vector.Values, 0, modeDim);

                    index[incrementIndex]++;
                }
            }

            return new DenseTensor<T>(dimensions, product);
        }
        public DenseTensor<T> Multiply(DenseMatrix<T> matrix, int mode, IDenseBLAS1<T> blas)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            if (mode < 0 || mode >= _order)
            {
                throw new ArgumentOutOfRangeException(nameof(mode));
            }
            if (matrix.Columns != _dimensions[mode])
            {
                throw new ArgumentOutOfRangeException("Mismatched dimensions.");
            }
            if (blas is null)
            {
                throw new ArgumentNullException(nameof(blas));
            }

            int rows = matrix.Rows;

            // For temporary storage of the index
            int[] index = new int[_order];

            // The dimensions of the product tensor
            int[] dimensions = _dimensions.Copy();
            dimensions[mode] = rows;

            // The product tensor
            DenseTensor<T> result = new DenseTensor<T>(dimensions);
            T[] product = result._values;

            // The index we increment (cannot be the mode)
            int incrementIndex = mode == _order - 1 ? _order - 2 : _order - 1,
                incrementIndexDim = _dimensions[incrementIndex],
                modeDim = _dimensions[mode];

            // For workspace for storing a fibre of the tensor
            T[] workspace = new T[modeDim];

            // Use the transpose of the matrix
            T[][] values = matrix.Values;

            if (product.LongLength > int.MaxValue)
            {
                // The product of dimensions coming before and after 'mode',
                // respectively
                long postMultiplier = 1;
                for (int d = mode + 1; d < _order; ++d)
                {
                    postMultiplier *= _dimensions[d];
                }

                long len = product.LongLength / rows;
                for (long i = 0; i < len; ++i)
                {
                    if (index[incrementIndex] == incrementIndexDim)
                    {
                        // Since incrementIndex is either the last index
                        // or the 2nd last if the last index is the mode, 
                        // we only need to iterate downwards
                        int j = incrementIndex;
                        while (index[j] == _dimensions[j])
                        {
                            index[j] = 0;

                            // Find the next non-mode index
                            --j;
                            if (j == mode) --j;

                            index[j]++;
                        }
                    }

                    // Calculate the first index in the fibre
                    index[mode] = 0;
                    long key = GetKeyInt64(index);

                    // Iterate across the fibre and store in the workspace
                    for (int k = 0; k < modeDim; ++k)
                    {
                        workspace[k] = _values[key];

                        // increment key by the postmultiplier, which will 
                        // step over the subtensor represented by dimensions
                        // mode + 1, ..., _order - 1 inclusive.
                        key += postMultiplier;
                    }

                    // store the matrix product
                    for (int r = 0; r < rows; ++r)
                    {
                        // This inner loop can be simplified if we calculate
                        // the steps taken in advance
                        index[mode] = r;
                        product[result.GetKeyInt64(index)] = blas.DOT(workspace, values[r], 0, modeDim);
                    }

                    index[incrementIndex]++;
                }
            }
            else
            {
                // The product of dimensions coming before and after 'mode',
                // respectively
                int postMultiplier = 1;
                for (int d = mode + 1; d < _order; ++d)
                {
                    postMultiplier *= _dimensions[d];
                }

                int len = product.Length / rows;
                for (int i = 0; i < len; ++i)
                {
                    if (index[incrementIndex] == incrementIndexDim)
                    {
                        // Since incrementIndex is either the last index
                        // or the 2nd last if the last index is the mode, 
                        // we only need to iterate downwards
                        int j = incrementIndex;
                        while (index[j] == _dimensions[j])
                        {
                            index[j] = 0;

                            // Find the next non-mode index
                            --j;
                            if (j == mode) --j;

                            index[j]++;
                        }
                    }

                    // Calculate the first index in the fibre
                    index[mode] = 0;
                    int key = GetKeyInt32(index);

                    // Iterate across the fibre and store in the workspace
                    for (int k = 0; k < modeDim; ++k)
                    {
                        workspace[k] = _values[key];

                        // increment key by the postmultiplier, which will 
                        // step over the subtensor represented by dimensions
                        // mode + 1, ..., _order - 1 inclusive.
                        key += postMultiplier;
                    }

                    // store the matrix product
                    for (int r = 0; r < rows; ++r)
                    {
                        index[mode] = r;
                        product[result.GetKeyInt32(index)] = blas.DOT(workspace, values[r], 0, modeDim);
                    }
                    index[incrementIndex]++;
                }
            }

            return new DenseTensor<T>(dimensions, product);
        }
        /// <summary>
        /// Returns the pointwise product between this tensor and another tensor. The dimensions 
        /// of the two tensors must match.
        /// </summary>
        /// <param name="tensor">The tensor to pointwise-multiply by.</param>
        /// <param name="blas1">The level-1 BLAS implementation to use.</param>
        /// <returns>The pointwise tensor product.</returns>
        public DenseTensor<T> MultiplyElementwise(DenseTensor<T> tensor, IDenseBLAS1<T> blas)
        {
            if (tensor is null)
            {
                throw new ArgumentNullException(nameof(tensor));
            }
            if (blas is null)
            {
                throw new ArgumentNullException(nameof(blas));
            }

            if (_requiresLongIndex)
            {
                long len = _values.LongLength;
                T[] product = new T[len];
                blas.PMULT(_values, tensor._values, product, 0, int.MaxValue, 0, 0);

                IProvider<T> provider = blas.Provider;
                for (long i = int.MaxValue; i < len; ++i)
                {
                    product[i] = provider.Multiply(_values[i], tensor._values[i]);
                }
                return new DenseTensor<T>(_dimensions.Copy(), product);
            }
            else
            {
                T[] product = new T[_values.Length];
                blas.PMULT(_values, tensor._values, product, 0, product.Length, 0, 0);
                return new DenseTensor<T>(_dimensions.Copy(), product);
            }
        }
        public void MultiplyInPlace(T scalar, IDenseBLAS1<T> blas)
        {
            if (scalar is null) throw new ArgumentNullException(nameof(scalar));
            if (blas is null) throw new ArgumentNullException(nameof(blas));

            if (_requiresLongIndex)
            {
                IProvider<T> provider = blas.Provider;
                blas.SCAL(_values, scalar, 0, int.MaxValue);

                for (long i = int.MaxValue, len = _values.LongLength; i < len; ++i)
                {
                    _values[i] = provider.Multiply(_values[i], scalar);
                }
            }
            else
            {
                blas.SCAL(_values, scalar, 0, _values.Length);
            }
        }
        public DenseTensor<T> Negate(IDenseBLAS1<T> blas)
        {
            DenseTensor<T> copy = Copy();
            copy.NegateInPlace(blas);
            return copy;
        }
        public void NegateInPlace(IDenseBLAS1<T> blas)
        {
            if (blas is null)
            {
                throw new ArgumentNullException(nameof(blas));
            }

            T negone = blas.Provider.Negate(blas.Provider.One);
            if (_requiresLongIndex)
            {
                blas.SCAL(_values, negone, 0, int.MaxValue);

                IProvider<T> provider = blas.Provider;
                long len = _values.LongLength;
                for (long i = int.MaxValue; i < len; ++i)
                {
                    _values[i] = provider.Negate(_values[i]);
                }
            }
            else
            {
                blas.SCAL(_values, negone, 0, _values.Length);
            }
        }
        /// <summary>
        /// Changes the shape (i.e. order and dimensions) of this tensor, in place, without changing its values.
        /// </summary>
        /// <param name="dimensions">The dimensions of the altered tensor.</param>
        public void Reshape(params int[] dimensions)
        {
            if (dimensions is null)
            {
                throw new ArgumentNullException(nameof(dimensions));
            }

            long prod = 1L;
            for (int i = 0; i < dimensions.Length; ++i)
            {
                prod *= dimensions[i];
            }
            if (prod != _values.LongLength)
            {
                throw new ArgumentOutOfRangeException(nameof(dimensions), "Dimensions does not match the size of the array.");
            }

            _order = dimensions.Length;
            _dimensions = dimensions;
        }
        /// <summary>
        /// Returns a tensor formed by selecting a set of dimensions to hold constant.
        /// </summary>
        /// <param name="fixedIndices">A dictionary containing pairs of integers representing
        /// the dimension and the selected index along that dimension, respectively. </param>
        /// <returns></returns>
        public DenseTensor<T> Select(Dictionary<int, int> fixedIndices)
        {
            if (fixedIndices is null)
            {
                throw new ArgumentNullException(nameof(fixedIndices));
            }

            int[] index = new int[_order];
            bool[] isFixed = new bool[_order];
            foreach (KeyValuePair<int, int> e in fixedIndices)
            {
                if (e.Key < 0 || e.Key >= _order)
                {
                    throw new ArgumentOutOfRangeException(nameof(fixedIndices));
                }
                if (e.Value < 0 || e.Value >= _dimensions[e.Key])
                {
                    throw new ArgumentOutOfRangeException(nameof(fixedIndices));
                }

                isFixed[e.Key] = true;
                index[e.Key] = e.Value;
            }

            int freeDim = _order - fixedIndices.Count;
            if (freeDim == 0)
            {
                // Handle the scalar case, from here onwards there is 
                // at least 1 free dimension
                return new DenseTensor<T>(Get(index)); 
            }

            int[] dimensions = new int[freeDim];
            for (int d = 0, k = 0; d < _order; ++d)
            {
                if (!isFixed[d])
                {
                    dimensions[k++] = _dimensions[d];
                }
            }

            DenseTensor<T> subtensor = new DenseTensor<T>(dimensions);
            T[] values = subtensor._values;

            // Find the dimension we increment - since there is at least
            // 1 free dimension after handling the scalar case, this index
            // will be well-defined
            int incrementIndex = _order - 1;
            while (isFixed[incrementIndex]) --incrementIndex;
            int incrementIndexDim = _dimensions[incrementIndex];

            if (_requiresLongIndex)
            {
                long len = values.LongLength;
                bool recalculate = true, lastIndexFixed = isFixed[incrementIndex];
                long cachedIndex = 0;
                for (long i = 0, z = 0; i < len; ++i)
                {
                    // Start indexing from the right-most dimension
                    if (index[incrementIndex] == incrementIndexDim)
                    {
                        int j = incrementIndex;
                        while (index[j] == _dimensions[j])
                        {
                            index[j] = 0;
                            do
                            {
                                --j;
                            } while (isFixed[j]);
                            index[j]++;
                        }

                        // requires recalculation
                        recalculate = true;
                    }

                    // Want to avoid constant re-evaluation of the index
                    // if we can, which is possible to mitigate so long as 
                    // the last index is not fixed
                    if (lastIndexFixed || recalculate)
                    {
                        cachedIndex = GetKeyInt64(index);
                        recalculate = false;
                    }
                    else
                    {
                        ++cachedIndex;
                    }

                    values[z++] = _values[cachedIndex];
                    index[incrementIndex]++;
                }
            }
            else
            {
                int len = values.Length, cachedIndex = 0;
                bool recalculate = true, lastIndexFixed = isFixed[incrementIndex];
                for (int i = 0, z = 0; i < len; ++i)
                {
                    // Start indexing from the right-most dimension
                    if (index[incrementIndex] == incrementIndexDim)
                    {
                        int j = incrementIndex;
                        while (index[j] == _dimensions[j])
                        {
                            index[j] = 0;
                            do
                            {
                                --j;
                            } while (isFixed[j]);
                            index[j]++;
                        }

                        // requires recalculation
                        recalculate = true;
                    }

                    // Want to avoid constant re-evaluation of the index
                    // if we can, which is possible to mitigate so long as 
                    // the last index is not fixed
                    if (lastIndexFixed || recalculate)
                    {
                        cachedIndex = GetKeyInt32(index);
                        recalculate = false;
                    }
                    else
                    {
                        ++cachedIndex;
                    }

                    values[z++] = _values[cachedIndex];
                    index[incrementIndex]++;
                }
            }

            return subtensor;
        }
        public DenseTensor<T> Select(Dictionary<int, string> selections, bool oneBased = false)
        {
            if (selections is null)
            {
                throw new ArgumentNullException(nameof(selections));
            }

            bool[] isSelected = new bool[_order];
            int[][] indices = new int[_order][];
            foreach (KeyValuePair<int, string> q in selections)
            {
                if (q.Key < 0 || q.Key >= _order)
                {
                    throw new ArgumentOutOfRangeException(nameof(selections), "Dimension index is out of bounds.");
                }
                if (!Selector.TryParse(q.Value, oneBased, _dimensions[q.Key], out Selector s))
                {
                    throw new FormatException($"Invalid syntax near: {q.Value}");
                }
                indices[q.Key] = s.Selection;
                isSelected[q.Key] = true;
            }

            // Calculate the new dimensions
            int[] dimensions = new int[_order], 
                index = new int[_order],
                oldIndex = new int[_order];
            for (int d = 0; d < _order; ++d)
            {
                if (isSelected[d])
                {
                    dimensions[d] = indices[d].Length;
                    oldIndex[d] = indices[d][0];
                }
                else
                {
                    dimensions[d] = _dimensions[d];
                }
            }

            DenseTensor<T> subtensor = new DenseTensor<T>(dimensions);
            
            int incrementIndex = _order - 1,
                incrementIndexDim = dimensions[incrementIndex];
            bool incrementIndexSelected = isSelected[incrementIndex];
            if (_requiresLongIndex)
            {
                long len = subtensor._values.LongLength;
                for (long i = 0; i < len; ++i)
                {
                    if (index[incrementIndex] == incrementIndexDim)
                    {
                        int j = incrementIndex;
                        while (index[j] == dimensions[j])
                        {
                            index[j] = 0;
                            if (isSelected[j])
                            {
                                oldIndex[j] = indices[j][index[j]];
                            }
                            --j;
                            index[j]++;
                            if (isSelected[j])
                            {
                                oldIndex[j] = indices[j][index[j]];
                            }
                        }
                    }

                    subtensor._values[subtensor.GetKeyInt64(index)] = _values[GetKeyInt64(oldIndex)];

                    index[incrementIndex]++;
                    if (incrementIndexSelected)
                    {
                        oldIndex[incrementIndex] = indices[incrementIndex][index[incrementIndex]];
                    }
                }
            }
            else
            {
                int len = subtensor._values.Length;
                for (int i = 0; i < len; ++i)
                {
                    if (index[incrementIndex] == incrementIndexDim)
                    {
                        int j = incrementIndex;
                        while (index[j] == dimensions[j])
                        {
                            index[j] = 0;
                            if (isSelected[j])
                            {
                                oldIndex[j] = indices[j][0];
                            }
                            --j;
                            index[j]++;
                            if (isSelected[j])
                            {
                                oldIndex[j] = indices[j][index[j]];
                            }
                        }
                    }
                    else
                    {
                        if (incrementIndexSelected)
                        {
                            oldIndex[incrementIndex] = indices[incrementIndex][index[incrementIndex]];
                        }
                    }

                    subtensor._values[subtensor.GetKeyInt32(index)] = _values[GetKeyInt32(oldIndex)];

                    index[incrementIndex]++;
                }
            }

            return subtensor;
        }
        /// <summary>
        /// Extract a fibre from this tensor by fixing all but one of the dimensions. 
        /// </summary>
        /// <param name="fixedIndices">A dictionary containing key-value pairs representing the 
        /// dimensions to fix, and the indexes that they should be fixed to.</param>
        /// <returns>A fibre of the tensor.</returns>
        public DenseVector<T> SelectAsVector(Dictionary<int, int> fixedIndices)
        {
            if (fixedIndices is null)
            {
                throw new ArgumentNullException(nameof(fixedIndices));
            }
            if (fixedIndices.Count != _order - 1)
            {
                throw new ArgumentOutOfRangeException(nameof(fixedIndices), $"Incorrect number of fixed dimensions. Expected {_order - 1}.");
            }

            bool[] selected = new bool[_order];
            foreach (KeyValuePair<int, int> e in fixedIndices)
            {
                int i = e.Key;
                if (i < 0 || i >= _order)
                {
                    throw new ArgumentOutOfRangeException(nameof(fixedIndices) + $"[{i}]");
                }
                selected[i] = true;
            }

            // Find the missing index and populate the index array
            int vIndex = -1;
            int[] index = new int[_order];
            for (int d = 0; d < _order; ++d)
            {
                if (!selected[d])
                {
                    vIndex = d;
                }
                else
                {
                    index[d] = fixedIndices[d];
                }
            }

            int len = _dimensions[vIndex];
            T[] values = new T[len];
            long i0 = GetKeyInt64(index);
            values[0] = _values[i0];

            if (len > 1)
            {
                index[vIndex] = 1;
                long i1 = GetKeyInt64(index);
                values[1] = _values[i1];

                long step = i1 - i0, 
                    j = i1 + step;
                for (int i = 2; i < len; ++i, j += step)
                {
                    values[i] = _values[j];
                }
            }

            return new DenseVector<T>(values);
        }
        public override void Set(T value, params int[] index)
        {
            AssertValid(index);
            if (_requiresLongIndex)
            {
                _values[GetKeyInt64(index)] = value;
            }
            else
            {
                _values[GetKeyInt32(index)] = value;
            }
        }
        public DenseTensor<T> SumOver(int dimension, IProvider<T> provider)
        {
            return SumOver(new int[] { dimension }, provider);
        }
        public DenseTensor<T> SumOver(IEnumerable<int> dimensions, IProvider<T> provider)
        {
            if (dimensions is null)
            {
                throw new ArgumentNullException(nameof(dimensions));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }

            bool[] sumOver = new bool[_order];
            foreach (int d in dimensions)
            {
                if (d < 0 || d >= _order)
                {
                    throw new ArgumentOutOfRangeException(nameof(dimensions));
                }
                sumOver[d] = true;
            }

            // Calculate the order of the new tensor
            int newOrder = 0;
            for (int i = 0; i < _order; ++i)
            {
                if (!sumOver[i])
                {
                    ++newOrder;
                }
            }

            // Check for redundancy here 
            if (newOrder == _order)
            {
                return Copy();
            }
            // Need to handle this separately because the incrementation
            // index will be undefined for full contractions
            if (newOrder == 0)
            {
                T sum = provider.Zero;
                long len = _values.LongLength;
                for (long i = 0; i < len; ++i)
                {
                    sum = provider.Add(sum, _values[i]);
                }
                return new DenseTensor<T>(sum);
            }

            // The dimensions along which we sum over, in order.
            int[] summedDimensions = new int[_order - newOrder];

            // The dimensionality of the remaining dimensions, in order.
            int[] newDimensions = new int[newOrder];
            for (int i = 0, k = 0, p = 0; i < _order; ++i)
            {
                if (!sumOver[i])
                {
                    newDimensions[k++] = _dimensions[i];

                }
                else
                {
                    summedDimensions[p++] = i;
                }
            }

            DenseTensor<T> subtensor = new DenseTensor<T>(newDimensions);
            T[] values = subtensor._values;

            // Used to temporarily hold the index within this larger tensor
            int[] index = new int[_order];

            // Increment the last remaining dimension - this will be guaranteed
            // to exist since we handle the scalar case separately.
            int incrementIndex = _order - 1;
            while (sumOver[incrementIndex]) --incrementIndex;
            int incrementIndexDim = _dimensions[incrementIndex];

            // The index of the last dimension over which we sum - also guaranteed
            // to exist since we check for non-summing at the start.
            int incrementIndex2 = _order - 1;
            while (!sumOver[incrementIndex2]) --incrementIndex2;
            int incrementIndex2Dim = _dimensions[incrementIndex2];

            if (_requiresLongIndex)
            {
                long len = values.LongLength, terms = _values.LongLength / len;
                for (long i = 0; i < len; ++i)
                {
                    // In this loop we only increment over the summed 
                    // tensor indexes 
                    if (index[incrementIndex] == incrementIndexDim)
                    {
                        int j = incrementIndex;
                        while (index[j] == _dimensions[j])
                        {
                            index[j] = 0;
                            do
                            {
                                --j;
                            } while (sumOver[j]);
                            index[j]++;
                        }
                    }

                    // Reset all summation indices to 0
                    for (int d = 0; d < _order; ++d)
                    {
                        if (sumOver[d]) index[d] = 0;
                    }

                    T sum = provider.Zero;
                    for (long k = 0; k < terms; ++k)
                    {
                        if (index[incrementIndex2] == incrementIndex2Dim)
                        {
                            int j = incrementIndex2;
                            while (index[j] == _dimensions[j])
                            {
                                index[j] = 0;
                                do
                                {
                                    --j;
                                } while (!sumOver[j]);
                                index[j]++;
                            }
                        }
                        sum = provider.Add(sum, _values[GetKeyInt64(index)]);
                        index[incrementIndex2]++;
                    }

                    values[i] = sum;

                    index[incrementIndex]++;
                }
            }
            else
            {
                int len = values.Length, terms = _values.Length / len;
                for (int i = 0; i < len; ++i)
                {
                    // In this loop we only increment over the summed 
                    // tensor indexes 
                    if (index[incrementIndex] == incrementIndexDim)
                    {
                        int j = incrementIndex;
                        while (index[j] == _dimensions[j])
                        {
                            index[j] = 0;
                            do
                            {
                                --j;
                            } while (sumOver[j]);
                            index[j]++;
                        }
                    }

                    // Reset all summation indices to 0
                    for (int d = 0; d < _order; ++d)
                    {
                        if (sumOver[d]) index[d] = 0;
                    }

                    T sum = provider.Zero;
                    for (int k = 0; k < terms; ++k)
                    {
                        if (index[incrementIndex2] == incrementIndex2Dim)
                        {
                            int j = incrementIndex2;
                            while (index[j] == _dimensions[j])
                            {
                                index[j] = 0;
                                do
                                {
                                    --j;
                                } while (!sumOver[j]);
                                index[j]++;
                            }
                        }
                        sum = provider.Add(sum, _values[GetKeyInt32(index)]);
                        index[incrementIndex2]++;
                    }

                    values[i] = sum;

                    index[incrementIndex]++;
                }
            }

            return subtensor;
        }
        public DenseVector<T> SumOverAllExcept(int dimension, IProvider<T> provider)
        {
            if (dimension < 0 || dimension > _order)
            {
                throw new ArgumentOutOfRangeException(nameof(dimension));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }

            int dim = _dimensions[dimension];
            int incrementIndex = dimension == _order - 1 ? _order - 2 : _order - 1;
            int incrementIndexDim = _dimensions[incrementIndex];
            int[] index = new int[_order];
            bool isNotSequential = dimension == _order - 1;

            T[] vector = new T[dim];
            if (_requiresLongIndex)
            {
                long len = _values.LongLength / dim;
                for (int d = 0; d < dim; ++d)
                {
                    Array.Clear(index, 0, _order);
                    index[dimension] = d;

                    T sum = provider.Zero;
                    bool recalculate = true;
                    for (long i = 0, k = 0; i < len; ++i)
                    {
                        if (index[incrementIndex] == incrementIndexDim)
                        {
                            int j = incrementIndex;
                            while (index[j] == _dimensions[j])
                            {
                                index[j] = 0;
                                --j;
                                if (j == dimension)
                                {
                                    --j;
                                    // crossing the boundary - requires recalculate
                                    recalculate = true;
                                }
                                index[j]++;
                            }
                        }

                        if (isNotSequential || recalculate)
                        {
                            k = GetKeyInt64(index);
                            recalculate = false;
                        }
                        else
                        {
                            ++k;
                        }
                        sum = provider.Add(sum, _values[k]);
                        index[incrementIndex]++;
                    }

                    vector[d] = sum;
                }
            }
            else
            {
                int len = _values.Length / dim;
                for (int d = 0; d < dim; ++d)
                {
                    Array.Clear(index, 0, _order);
                    index[dimension] = d;

                    T sum = provider.Zero;
                    bool recalculate = true;
                    for (int i = 0, k = 0; i < len; ++i)
                    {
                        if (index[incrementIndex] == incrementIndexDim)
                        {
                            int j = incrementIndex;
                            while (index[j] == _dimensions[j])
                            {
                                index[j] = 0;
                                --j;
                                if (j == dimension)
                                {
                                    --j;
                                    // crossing the boundary - requires recalculate
                                    recalculate = true;
                                }
                                index[j]++;
                            }
                        }

                        if (isNotSequential || recalculate)
                        {
                            k = GetKeyInt32(index);
                            recalculate = false;
                        }
                        else
                        {
                            ++k;
                        }
                        sum = provider.Add(sum, _values[k]);
                        index[incrementIndex]++;
                    }

                    vector[d] = sum;
                }
            }

            return new DenseVector<T>(vector);
        }
        public DenseMatrix<T> SumOverAllExcept(int rowDimension, int columnDimension, IProvider<T> provider)
        {
            if (rowDimension < 0 || rowDimension >= _order)
            {
                throw new ArgumentOutOfRangeException(nameof(rowDimension));
            }
            if (columnDimension < 0 || columnDimension >= _order)
            {
                throw new ArgumentOutOfRangeException(nameof(columnDimension));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }

            int rows = _dimensions[rowDimension],
                cols = _dimensions[columnDimension];

            T[][] values = MatrixInternalExtensions.JMatrix<T>(rows, cols);

            int incrementIndex = _order - 1;
            while (incrementIndex == rowDimension || incrementIndex == columnDimension) --incrementIndex;
            int incrementIndexDim = _dimensions[incrementIndex];
            int[] index = new int[_order];
            bool isNotSequential = rowDimension == _order - 1 || columnDimension == _order - 1;

            for (int r = 0; r < rows; ++r)
            {
                T[] row = values[r];
                for (int c = 0; c < cols; ++c)
                {
                    Array.Clear(index, 0, _order);
                    index[rowDimension] = r;
                    index[columnDimension] = c;

                    bool recalculate = true;
                    T sum = provider.Zero;
                    if (_requiresLongIndex)
                    {
                        long len = _values.LongLength / ((long)rows * cols);
                        for (long i = 0, k = 0; i < len; ++i)
                        {
                            if (index[incrementIndex] == incrementIndexDim)
                            {
                                int j = incrementIndex;
                                while (index[j] == _dimensions[j])
                                {
                                    index[j] = 0;
                                    --j;
                                    while (j == rowDimension || j == columnDimension)
                                    {
                                        recalculate = true; // crossed the border
                                        --j;
                                    }
                                    index[j]++;
                                }
                            }
                            if (isNotSequential || recalculate)
                            {
                                k = GetKeyInt64(index);
                                recalculate = false;
                            }
                            else
                            {
                                ++k;
                            }
                            sum = provider.Add(sum, _values[k]);
                            index[incrementIndex]++;
                        }
                    }
                    else
                    {
                        int len = _values.Length / (rows * cols);
                        for (int i = 0, k = 0; i < len; ++i)
                        {
                            if (index[incrementIndex] == incrementIndexDim)
                            {
                                int j = incrementIndex;
                                while (index[j] == _dimensions[j])
                                {
                                    index[j] = 0;
                                    --j;
                                    while (j == rowDimension || j == columnDimension)
                                    {
                                        recalculate = true; // crossed the border
                                        --j;
                                    }
                                    index[j]++;
                                }
                            }
                            if (isNotSequential || recalculate)
                            {
                                k = GetKeyInt32(index);
                                recalculate = false;
                            }
                            else
                            {
                                ++k;
                            }
                            sum = provider.Add(sum, _values[k]);
                            index[incrementIndex]++;
                        }
                    }

                    row[c] = sum;
                }
            }

            return new DenseMatrix<T>(rows, cols, values);
        }
        public DenseTensor<T> Subtract(DenseTensor<T> tensor, IDenseBLAS1<T> blas)
        {
            if (tensor is null)
            {
                throw new ArgumentNullException(nameof(tensor));
            }
            if (blas is null)
            {
                throw new ArgumentNullException(nameof(blas));
            }
            AssertDimensionsMatch(_dimensions, tensor._dimensions);

            T[] sum = new T[_values.LongLength];

            // blas does not support > int.MaxValue lengths
            if (_requiresLongIndex)
            {
                blas.SUB(_values, tensor._values, sum, 0, int.MaxValue);

                IProvider<T> provider = blas.Provider;
                long len = _values.LongLength;
                for (long i = int.MaxValue; i < len; ++i)
                {
                    sum[i] = provider.Subtract(_values[i], tensor._values[i]);
                }
            }
            else
            {
                blas.SUB(_values, tensor._values, sum, 0, sum.Length);
            }

            return new DenseTensor<T>(_dimensions.Copy(), sum);
        }
        /// <summary>
        /// Returns the tensor product between this tensor and another. Supports 2 types of 
        /// tensor product - type 1: the order of the tensor is the sum of the orders of the 
        /// two constituent tensors, and type 2: each dimension of the product is equal to the 
        /// product of the respective dimensions of the two constituent tensors. 
        /// </summary>
        /// <param name="tensor"></param>
        /// <param name="blas"></param>
        /// <returns></returns>
        public DenseTensor<T> TensorProduct(DenseTensor<T> tensor, IDenseBLAS1<T> b, bool type1)
        {
            if (tensor is null)
            {
                throw new ArgumentNullException(nameof(tensor));
            }
            if (b is null)
            {
                throw new ArgumentNullException(nameof(b));
            }

            if (type1)
            {
                // Copy over the new dimensions of the tensor product
                int[] dimensions = new int[_order + tensor._order];
                Array.Copy(dimensions, _dimensions, _order);
                Array.Copy(tensor._dimensions, 0, dimensions, _order, tensor._order);

                T[] values = new T[checked(_values.LongLength * tensor._values.LongLength)];
                if (values.LongLength > int.MaxValue)
                {
                    long len1 = _values.LongLength,
                        len2 = tensor._values.LongLength;

                    IProvider<T> provider = b.Provider;
                    T[] values2 = tensor._values;

                    for (long i = 0; i < len1; ++i)
                    {
                        long start = i * len2, end = start + len2;
                        T v = _values[i];
                        for (long j = start; j < end; ++j)
                        {
                            values[j] = provider.Multiply(v, values2[j - start]);
                        }
                    }
                }
                else
                {
                    int len1 = _values.Length, len2 = tensor._values.Length;
                    T[] values2 = tensor._values;

                    for (int i = 0; i < len1; ++i)
                    {
                        // The alternative way of doing this is to AXPY directly
                        // into values, however this will rely on the proper initialization
                        // of values as zero... not worth the risk.
                        int start = i * len2;
                        Array.Copy(values2, 0, values, start, len2);
                        b.SCAL(values, _values[i], start, start + len2);
                    }
                }
                return new DenseTensor<T>(dimensions, values);
            }
            else
            {
                if (_order != tensor._order)
                {
                    throw new InvalidOperationException("A type 2 tensor product can only be performed on two tensors of the same order.");
                }
                int[] dimensions = new int[_order];
                for (int i = 0; i < _order; ++i)
                {
                    dimensions[i] = checked(_dimensions[i] * tensor._dimensions[i]);
                }

                DenseTensor<T> prod = new DenseTensor<T>(dimensions);
                T[] values = prod._values;
                long len1 = _values.LongLength, 
                    len2 = tensor._values.LongLength;
                int[] index1 = new int[_order],
                    index2 = new int[_order],
                    index = new int[_order];

                IProvider<T> provider = b.Provider;

                int incrementIndex = _order - 1;
                for (long i = 0; i < len1; ++i)
                {
                    while (index1[incrementIndex] == dimensions[incrementIndex])
                    {
                        int j = incrementIndex;
                        while (index1[j] == dimensions[j])
                        {
                            index1[j--] = 0;
                            index1[j] += tensor._dimensions[j];
                        }
                    }

                    T v = _values[i];
                    Array.Clear(index2, 0, _order);
                    Array.Copy(index1, index, _order);

                    long z = prod.GetKeyInt64(index);
                    for (long j = 0; j < len2; ++j)
                    {
                        if (index2[incrementIndex] == tensor._dimensions[incrementIndex])
                        {
                            int k = incrementIndex;
                            while (index2[k] == tensor._dimensions[k])
                            {
                                index2[k--] = 0;
                                index2[k]++;
                            }

                            for (; k < incrementIndex; ++k)
                            {
                                index[k] = index1[k] + index2[k];
                            }

                            // Only the last index needs to be updated since we handle carries
                            // within the while loop above
                            index[incrementIndex] = index1[incrementIndex] + index2[incrementIndex];
                            z = prod.GetKeyInt64(index); // recalculate
                        }

                        values[z] = provider.Multiply(v, tensor._values[j]);
                        index2[incrementIndex]++;
                        ++z;
                    }

                    index1[incrementIndex] += tensor._dimensions[incrementIndex];
                }

                return prod;
            }
        }
        /// <summary>
        /// Returns the outer product between this tensor and a dense vector
        /// </summary>
        /// <param name="vector"></param>
        /// <param name="blas"></param>
        public DenseTensor<T> TensorProduct(DenseVector<T> vector, int insertionIndex, IDenseBLAS1<T> b)
        {
            if (vector is null)
            {
                throw new ArgumentNullException(nameof(vector));
            }
            if (b is null)
            {
                throw new ArgumentNullException(nameof(b));
            }

            int newOrder = _order + 1,
                dim = vector.Dimension,
                k = 0;

            // Calculate the new dimensions
            int[] dimensions = new int[newOrder];
            for (int d = 0; d < _order; ++d)
            {
                if (insertionIndex == d)
                {
                    dimensions[k++] = dim;
                }
                dimensions[k++] = _dimensions[d];
            }
            if (insertionIndex == _order)
            {
                dimensions[k++] = dim;
            }

            DenseTensor<T> prod = new DenseTensor<T>(dimensions);

            T[] values = prod._values,
                vect_values = vector.Values,
                workspace = new T[dim];
            int[] index = new int[newOrder];

            int incrementIndexDim = _dimensions[_order];

            long len = _values.LongLength;
            for (long i = 0; i < len; ++i)
            {
                if (index[_order] == incrementIndexDim)
                {
                    int j = _order;
                    while (index[j] == _dimensions[j])
                    {
                        index[j] = 0;
                        --j;
                        if (j == insertionIndex) --j;
                        index[j]++;
                    }
                }

                // Perform all multiplications in 1 step
                Array.Copy(vect_values, workspace, dim);
                b.SCAL(workspace, _values[i], 0, dim);

                index[insertionIndex] = 0;
                long i1 = prod.GetKeyInt64(index);
                values[i1] = workspace[0];

                // Calculate the first 2, then extrapolate the 
                // step size for all over indices.
                // This requires us to handle the 1-D vector 
                // separately
                if (dim > 1)
                {
                    index[insertionIndex] = 1;
                    long i2 = prod.GetKeyInt64(index),
                        step = i2 - i1;
                    values[i2] = workspace[1];

                    long ip = i2 + step;
                    for (int p = 2; p < dim; ++p, ip += step)
                    {
                        values[ip] = workspace[p];
                    }
                }

                index[_order]++;
            }

            return prod;
        }
        public override Matrix<T> ToMatrix()
        {
            return ToDenseMatrix();
        }
        public DenseMatrix<T> ToDenseMatrix()
        {
            if (_order != 2)
            {
                throw new InvalidOperationException("Tensor is not of order 2.");
            }

            int rows = _dimensions[0], cols = _dimensions[1];
            DenseMatrix<T> matrix = new DenseMatrix<T>(rows, cols);

            long lc = cols;
            for (int r = 0; r < rows; ++r)
            {
                // Use long to avoid overflow
                Array.Copy(_values, r * lc, matrix.Values[r], 0, lc);
            }
            return matrix;
        }
        /// <summary>
        /// Returns this tensor as an array. Only defined if this tensor is of order 1.
        /// </summary>
        /// <returns>This tensor as a 1-dimensional array.</returns>
        public T[] ToRect1()
        {
            if (_order != 1)
            {
                throw new InvalidOperationException("Tensor is not of order 1.");
            }
            return _values.Copy();
        }
        /// <summary>
        /// Returns this tensor as a 2-dimensional array. Only defined if this tensor is order-2.
        /// </summary>
        /// <returns>This tensor as a 2-dimensional array.</returns>
        public T[,] ToRect2()
        {
            if (_order != 2)
            {
                throw new InvalidOperationException("Tensor is not of order 2.");
            }

            int rows = _dimensions[0], columns = _dimensions[1];
            T[,] matrix = new T[rows, columns];
            
            if (_requiresLongIndex)
            {
                long len = _values.LongLength;
                int r = 0, c = 0;
                for (long i = 0; i < len; ++i, ++c)
                {
                    if (c == columns)
                    {
                        c = 0;
                        ++r;
                    }
                    matrix[r, c] = _values[i];
                }
            }
            else
            {
                int len = _values.Length;
                for (int i = 0, r = 0, c = 0; i < len; ++i, ++c)
                {
                    if (c == columns)
                    {
                        c = 0;
                        ++r;
                    }
                    matrix[r, c] = _values[i];
                }
            }
            return matrix;
        }
        /// <summary>
        /// Returns this tensor as a 3-dimensional array. Only defined if this tensor is order-3.
        /// </summary>
        /// <returns>This tensor as a 3-dimensional array.</returns>
        public T[,,] ToRect3()
        {
            if (_order != 3)
            {
                throw new InvalidOperationException("Tensor is not of order 3.");
            }

            int m = _dimensions[0], n = _dimensions[1], p = _dimensions[2];
            T[,,] tensor = new T[m, n, p];
            if (_requiresLongIndex)
            {
                long len = _values.Length;
                int i = 0, j = 0, k = 0;
                for (long z = 0; z < len; ++z, ++k)
                {
                    if (k == p)
                    {
                        k = 0;
                        ++j;
                    }
                    if (j == n)
                    {
                        j = 0;
                        ++i;
                    }
                    tensor[i, j, k] = _values[z];
                }
            }
            else
            {
                int len = _values.Length, i = 0, j = 0, k = 0;
                for (int z = 0; z < len; ++z, ++k)
                {
                    if (k == p)
                    {
                        k = 0;
                        ++j;
                    }
                    if (j == n)
                    {
                        j = 0;
                        ++i;
                    }
                    tensor[i, j, k] = _values[z];
                }
            }
            return tensor;
        }
        /// <summary>
        /// Returns this tensor as a 4-dimensional array. Only defined if this tensor is order-4.
        /// </summary>
        /// <returns>This tensor as an 4-dimensional array.</returns>
        public T[,,,] ToRect4()
        {
            if (_order != 4)
            {
                throw new InvalidOperationException("Tensor is not of order 4.");
            }

            int m = _dimensions[0], n = _dimensions[1], p = _dimensions[2], q = _dimensions[3];
            T[,,,] tensor = new T[m, n, p, q];
            if (_requiresLongIndex)
            {
                long len = _values.LongLength;
                int i = 0, j = 0, k = 0, l = 0;
                for (long z = 0; z < len; ++z, ++l)
                {
                    if (l == q) { l = 0; ++k; }
                    if (k == p) { k = 0; ++j; }
                    if (j == n) { j = 0; ++i; }
                    tensor[i, j, k, l] = _values[z];
                }
            }
            else
            {
                int len = _values.Length;
                for (int z = 0, i = 0, j = 0, k = 0, l = 0; z < len; ++z, ++l)
                {

                    if (l == q) { l = 0; ++k; }
                    if (k == p) { k = 0; ++j; }
                    if (j == n) { j = 0; ++i; }
                    tensor[i, j, k, l] = _values[z];
                }
            }
            return tensor;
        }
        /// <summary>
        /// Returns this tensor as a scalar. Only defined if every dimension of this tensor is of size 1.
        /// </summary>
        /// <returns>This tensor as a scalar value.</returns>
        public override T ToScalar()
        {
            if (_order != 0)
            {
                // Also support tensors that are actually a fibre in disguise, i.e. 
                // even though its order > 0, all dimensions are 1 in size

                for (int d = 0; d < _order; ++d)
                {
                    if (_dimensions[d] > 1)
                    {
                        throw new InvalidOperationException("Tensor is not of order 0.");
                    }
                }
            }
            return _values[0];
        }
        /// <summary>
        /// Returns this tensor as a vector. Only defined if this tensor has only 1 dimension having size 
        /// greater than 1. 
        /// </summary>
        /// <returns>This tensor as a vector.</returns>
        public override Vector<T> ToVector()
        {
            return ToDenseVector();
        }
        /// <summary>
        /// Returns this tensor as a dense vector. Only defined for tensors with only 1 dimension of size greater than 1.
        /// </summary>
        /// <returns>This tensor as a dense vector.</returns>
        public DenseVector<T> ToDenseVector()
        {
            if (_order != 1)
            {
                // Also support tensors that are actually vectors in disguise, i.e. 
                // there is only 1 dimension of size > 1

                int count = 0;
                for (int d = 0; d < _order; ++d)
                {
                    if (_dimensions[d] > 1) ++count;
                }

                if (count == 1)
                {
                    for (int d = 0; d < _order; ++d)
                    {
                        if (_dimensions[d] > 1)
                        {
                            int dim = _dimensions[d];
                            T[] vect = new T[dim];
                            int[] index = new int[_order];
                            for (int i = 0; i < dim; ++i)
                            {
                                index[d] = i;
                                vect[i] = _values[GetKeyInt64(index)];
                            }

                            return new DenseVector<T>(vect);
                        }
                    }
                    // Should never get here
                }
                else if (count == 0)
                {
                    if (_values.Length > 0)
                    {
                        return new DenseVector<T>(_values.Copy()); // should only be of length 1
                    }
                }
                throw new InvalidOperationException("Tensor is not of order 1.");
            }
            return new DenseVector<T>(_values.Copy());
        }
        /// <summary>
        /// Calculates a transposition of this tensor where the dimensions have been permuted according
        /// to a permutation vector. A new tensor is returned, without changing this tensor.
        /// </summary>
        /// <param name="permutationVector">The vector of permutations that rearrange the order 
        /// of this tensor's dimensions.</param>
        /// <returns>The transposed tensor.</returns>
        public DenseTensor<T> Transpose(params int[] permutationVector)
        {
            if (permutationVector is null)
            {
                throw new ArgumentNullException(nameof(permutationVector));
            }
            if (permutationVector.Length != _order || !permutationVector.IsPermutation())
            {
                throw new ArgumentOutOfRangeException(nameof(permutationVector), "Not a permutation vector.");
            }

            int[] dimensions = new int[_order];
            for (int d = 0; d < _order; ++d)
            {
                dimensions[permutationVector[d]] = _dimensions[d]; 
            }
            DenseTensor<T> tensor = new DenseTensor<T>(dimensions);

            int[] index = new int[_order], newIndex = new int[_order];
            int incrementIndex = _order - 1,
                incrementIndexDim = _dimensions[incrementIndex],
                pIncrementIndex = permutationVector[incrementIndex];

            long len = _values.LongLength;
            for (long i = 0; i < len; ++i)
            {
                if (index[incrementIndex] == incrementIndexDim)
                {
                    int j = incrementIndex;
                    while (index[j] == _dimensions[j])
                    {
                        index[j--] = 0;
                        index[j]++;
                    }

                    // Update the new index at p[j]
                    for (; j < incrementIndex; ++j)
                    {
                        newIndex[permutationVector[j]] = index[j];
                    }
                }

                // Only the last index needs to be updated
                newIndex[pIncrementIndex] = index[incrementIndex]++;

                tensor._values[tensor.GetKeyInt64(newIndex)] = _values[i];
            }
            return tensor;
        }
        /// <summary>
        /// Returns the $n$-mode unfolding (matricization) of a tensor with order $> 2$.
        /// </summary>
        /// <param name="mode">The mode of the unfolding, $n$.</param>
        /// <returns>The unfolded tensor.</returns>
        public DenseMatrix<T> Unfold(int mode)
        {
            if (mode < 0 || mode >= _order)
            {
                throw new ArgumentOutOfRangeException(nameof(mode));
            }
            if (_order <= 2)
            {
                throw new InvalidOperationException("Tensor unfolding only applies for tensors with order > 2.");
            }

            int rows = _dimensions[mode];

            if (_values.LongLength / rows > int.MaxValue)
            {
                throw new OverflowException("Unfolded matrix is too large to be stored as a dense matrix.");
            }
            int cols = (int)(_values.LongLength / rows);

            DenseMatrix<T> unfolded = new DenseMatrix<T>(rows, cols);
            int[] index = new int[_order];

            // The index which we increment - note that unfoldings
            // start from the first dimension 
            int incrementIndex = mode == 0 ? 1 : 0, 
                incrementIndexDim = _dimensions[incrementIndex];

            if (_requiresLongIndex)
            {
                for (int r = 0; r < rows; ++r)
                {
                    T[] row = unfolded[r];

                    Array.Clear(index, 0, _order);
                    index[mode] = r; // set the index of the mode dimension

                    for (int c = 0; c < cols; ++c)
                    {
                        if (index[incrementIndex] == incrementIndexDim)
                        {
                            int j = incrementIndex;
                            while (index[j] == _dimensions[j])
                            {
                                index[j] = 0;
                                ++j;
                                if (j == mode) ++j; // Skip the mode
                                index[j]++;
                            }
                        }

                        row[c] = _values[GetKeyInt64(index)];
                        index[incrementIndex]++;
                    }
                }
            }
            else
            {
                for (int r = 0; r < rows; ++r)
                {
                    T[] row = unfolded[r];

                    Array.Clear(index, 0, _order);
                    index[mode] = r; // set the index of the mode dimension

                    for (int c = 0; c < cols; ++c)
                    {
                        if (index[incrementIndex] == incrementIndexDim)
                        {
                            int j = incrementIndex;
                            while (index[j] == _dimensions[j])
                            {
                                index[j] = 0;
                                ++j;
                                if (j == mode) ++j; // Skip the mode
                                index[j]++;
                            }
                        }
                        row[c] = _values[GetKeyInt32(index)];
                        index[incrementIndex]++;
                    }
                }
            }

            return unfolded;
        }
        /// <summary>
        /// Returns this tensor expressed as a dense vector. The returned vector does 
        /// not share memory with this tensor; altering the vector will not change this 
        /// tensor.
        /// </summary>
        /// <returns>The vectorized version of this tensor.</returns>
        public DenseVector<T> Vectorize()
        {
            return new DenseVector<T>(_values.Copy());
        }
        /// <summary>
        /// Returns the vectorized tensor traversed in the specified order.
        /// </summary>
        /// <param name="traversalOrder"></param>
        /// <returns></returns>
        public DenseVector<T> Vectorize(params int[] traversalOrder)
        {
            if (traversalOrder is null)
            {
                throw new ArgumentNullException(nameof(traversalOrder));
            }
            if (traversalOrder.Length != _order)
            {
                throw new ArgumentOutOfRangeException($"Length of '{traversalOrder}' does not match the order of the tensor.");
            }
            if (!traversalOrder.IsPermutation())
            {
                throw new ArgumentOutOfRangeException(nameof(traversalOrder), "Not a permutation matrix.");
            }
            if (_values.LongLength > int.MaxValue)
            {
                throw new OverflowException("The tensor is too large to be vectorized as a dense vector.");
            }

            T[] values = new T[_values.Length];
            int[] index = new int[_order];

            int last = _order - 1,
                incrementIndex = traversalOrder[last],
                incrementIndexDim = _dimensions[incrementIndex];

            int len = _values.Length;
            for (int i = 0; i < len; ++i)
            {
                if (index[incrementIndex] == incrementIndexDim)
                {
                    int ji = last, j = incrementIndex;
                    while (index[j] == _dimensions[j])
                    {
                        index[j] = 0;
                        j = traversalOrder[--ji];
                        index[j]++;
                    }
                }
                values[i] = _values[GetKeyInt32(index)];
                index[incrementIndex]++;
            }

            return new DenseVector<T>(values);
        }
        #endregion

        #region Operators
        public static DenseTensor<T> operator +(DenseTensor<T> A, DenseTensor<T> B)
        {
            if (A is null)
            {
                throw new ArgumentNullException(nameof(A));
            }
            return A.Add(B, ProviderFactory.GetDefaultBLAS1<T>());
        }
        public static DenseTensor<T> operator -(DenseTensor<T> A, DenseTensor<T> B)
        {
            if (A is null)
            {
                throw new ArgumentNullException(nameof(A));
            }
            return A.Subtract(B, ProviderFactory.GetDefaultBLAS1<T>());
        }
        public static DenseTensor<T> operator -(DenseTensor<T> A)
        {
            if (A is null)
            {
                throw new ArgumentNullException(nameof(A));
            }
            return A.Negate(ProviderFactory.GetDefaultBLAS1<T>());
        }
        public static DenseTensor<T> operator *(DenseTensor<T> A, T scalar)
        {
            if (A is null)
            {
                throw new ArgumentNullException(nameof(A));
            }
            return A.Multiply(scalar, ProviderFactory.GetDefaultBLAS1<T>());
        }
        public static DenseTensor<T> operator *(T scalar, DenseTensor<T> A)
        {
            return A * scalar;
        }
        #endregion

        #region Casts
        public static explicit operator DenseTensor<T>(T scalar) => new DenseTensor<T>(scalar);
        public static explicit operator DenseTensor<T>(T[] vector) => new DenseTensor<T>(vector);
        public static explicit operator DenseTensor<T>(T[,] matrix) => new DenseTensor<T>(matrix);
        public static explicit operator DenseTensor<T>(T[,,] tensor) => new DenseTensor<T>(tensor);
        public static explicit operator DenseTensor<T>(T[,,,] tensor) => new DenseTensor<T>(tensor);
        public static explicit operator DenseTensor<T>(Vector<T> vector) => new DenseTensor<T>(vector);
        public static explicit operator DenseTensor<T>(Matrix<T> matrix) => new DenseTensor<T>(matrix);
        public static explicit operator DenseTensor<T>(DenseMatrix<T> matrix) => new DenseTensor<T>(matrix);
        public static explicit operator DenseTensor<T>(KruskalTensor<T> tensor) => new DenseTensor<T>(tensor);
        public static explicit operator DenseTensor<T>(SparseTensor<T> tensor) => new DenseTensor<T>(tensor);

        public static explicit operator T[](DenseTensor<T> tensor)
        {
            if (tensor is null) throw new ArgumentNullException(nameof(tensor));
            return tensor.ToRect1();
        }
        public static explicit operator T[,](DenseTensor<T> tensor)
        {
            if (tensor is null) throw new ArgumentNullException(nameof(tensor));
            return tensor.ToRect2();
        }
        public static explicit operator T[,,](DenseTensor<T> tensor)
        {
            if (tensor is null) throw new ArgumentNullException(nameof(tensor));
            return tensor.ToRect3();
        }
        public static explicit operator T[,,,](DenseTensor<T> tensor)
        {
            if (tensor is null) throw new ArgumentNullException(nameof(tensor));
            return tensor.ToRect4();
        }
        #endregion
    }
}
