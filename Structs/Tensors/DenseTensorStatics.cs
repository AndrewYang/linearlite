﻿using LinearNet.Helpers;
using System;

namespace LinearNet.Structs
{
    /// <summary>
    /// A static class containing useful functions for creating special types of dense tensors.
    /// </summary>
    /// <cat>linear-algebra</cat>
    public static class DenseTensor
    {
        internal static DenseTensor<T> Random<T>(Func<T> Sample, params int[] dimensions) where T : new()
        {
            if (dimensions == null) throw new ArgumentNullException();

            long len = 1;
            checked
            {
                for (int d = 0; d < dimensions.Length; ++d)
                {
                    if (dimensions[d] < 0)
                    {
                        throw new ArgumentOutOfRangeException(nameof(dimensions) + $"[{d}]");
                    }
                    len *= dimensions[d];
                }
            }

            T[] values = new T[len];
            for (long i = 0; i < len; ++i)
            {
                values[i] = Sample();
            }
            return new DenseTensor<T>(dimensions, values);
        }

        /// <summary>
        /// Create a random dense tensor given its dimensions.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dimensions">The dimensions of the tensor.</param>
        /// <returns>The random dense tensor.</returns>
        public static DenseTensor<T> Random<T>(params int[] dimensions) where T : new()
        {
            return Random(DefaultGenerators.GetRandomGenerator<T>(), dimensions);
        }
            
        /// <summary>
        /// Creates a tensor of dimensionality <txt>dimensions</txt> where each element is equal to <txt>value</txt>.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value">The repeated value.</param>
        /// <param name="dimensions">The dimensions of the tensor.</param>
        /// <returns>The dense tensor of repeated values.</returns>
        public static DenseTensor<T> Repeat<T>(T value, params int[] dimensions) where T : new()
        {
            if (dimensions is null)
            {
                throw new ArgumentNullException(nameof(dimensions));
            }

            long len = 1;
            checked
            {
                for (int d = 0; d < dimensions.Length; ++d)
                {
                    if (dimensions[d] < 0)
                    {
                        throw new ArgumentOutOfRangeException(nameof(dimensions) + $"[{d}]");
                    }
                    len *= dimensions[d];
                }
            }
            return new DenseTensor<T>(dimensions, RectangularVector.Repeat(value, len));
        }

        /// <summary>
        /// Creates a dense tensor of 1's (i.e. every element of the tensor is 1), given its dimensions.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dimensions">The dimensions of the tensor.</param>
        /// <returns>A dense tensor of 1's.</returns>
        public static DenseTensor<T> Ones<T>(params int[] dimensions) where T : new()
        {
            return Repeat(DefaultGenerators.GetOne<T>(), dimensions);
        }

        /// <summary>
        /// Create a tensor of dimension <txt>dimensions</txt>, where each element is zero.
        /// <para>Supported for <txt>DenseTensor&lt;T&gt;</txt> and <txt>SparseTensor&lt;T&gt;</txt>.</para>
        /// <para>Supported for data types <txt>int</txt>, <txt>long</txt>, <txt>float</txt>, <txt>double</txt>, <txt>decimal</txt> and all types 
        /// <txt>T</txt> implementing <txt>AdditiveGroup&lt;T&gt;</txt>.</para>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dimensions">The dimensions of the tensor.</param>
        /// <returns>A dense tensor of 0's.</returns>
        public static DenseTensor<T> Zeroes<T>(params int[] dimensions) where T : new()
        {
            T zero = new T();
            if (zero.Equals(default))
            {
                return new DenseTensor<T>(dimensions);
            }
            return Repeat(zero, dimensions);
        }
    }
}
