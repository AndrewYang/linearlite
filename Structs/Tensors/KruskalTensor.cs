﻿using LinearNet.Global;
using LinearNet.Providers;
using LinearNet.Providers.Managed;
using System;
using System.Collections.Generic;

namespace LinearNet.Structs
{
    public class KruskalTensor<T> : Tensor<T> where T : new()
    {
        /// <summary>
        /// Values of size [rank][dimensions][dimension i] 
        /// </summary>
        private readonly T[][][] _values;
        private readonly T[] _lambda;
        private readonly int[] _dimensions;
        private readonly int _order, _rank;
        private readonly IProvider<T> _provider;

        internal T[][][] Values { get { return _values; } }
        internal IProvider<T> Provider { get { return _provider; } }
        internal T[] Lambda { get { return _lambda; } }

        public override int Order => _order;

        public override long NonZeroCount
        {
            get
            {
                long count = 1;
                T[][] slice = _values[0];
                for (int d = 0; d < _order; ++d)
                {
                    count *= slice[d].Length;
                }
                return count;
            }
        }

        public override IEnumerable<TensorEntry<T>> NonZeroEntries
        {
            get
            {
                long len = 1;
                for (int d = 0; d < _order; ++d)
                {
                    len *= _dimensions[d];
                }

                int[] index = new int[_order];
                for (long i = 0; i < len; ++i)
                {
                    if (index[0] == _dimensions[0])
                    {
                        int j = 0;
                        while (index[j] == _dimensions[j])
                        {
                            index[j] = 0;
                            index[++j]++;
                        }
                    }

                    T sum = _provider.Zero;
                    for (int r = 0; r < _rank; ++r)
                    {
                        T[][] slice = _values[r];

                        T temp = _lambda[r];
                        for (int d = 0; d < _order; ++d)
                        {
                            temp = _provider.Multiply(temp, slice[d][index[d]]);
                        }
                        sum = _provider.Add(sum, temp);
                    }
                    yield return new TensorEntry<T>(index.Copy(), sum);

                    index[0]++;
                }
            }
        }

        #region Constructors

        public KruskalTensor(int[] dimensions, int rank, IProvider<T> provider)
        {
            if (dimensions is null)
            {
                throw new ArgumentNullException(nameof(dimensions));
            }

            _order = dimensions.Length;
            _dimensions = dimensions;
            _rank = rank;
            _lambda = RectangularVector.Repeat(provider.One, rank);
            _values = InitStorage(dimensions, rank);
            _provider = provider;
        }
        /// <summary>
        /// Create a rank-1 tensor formed by taking the outer product of a set of vectors.
        /// </summary>
        /// <param name="vectors">The vectors forming the outer product.</param>
        /// <param name="provider">The provider used for elementwise operations.</param>
        public KruskalTensor(DenseVector<T>[] vectors, IProvider<T> provider)
        {
            if (vectors is null)
            {
                throw new ArgumentNullException(nameof(vectors));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }

            _provider = provider;
            _order = vectors.Length;
            _rank = 1;
            _lambda = RectangularVector.Repeat(provider.One, 1);
            _dimensions = new int[_order];
            for (int d = 0; d < _order; ++d)
            {
                _dimensions[d] = vectors[d].Dimension;
            }

            _values = InitStorage(_dimensions, 1);
            T[][] values = _values[0];
            for (int d = 0; d < _order; ++d)
            {
                values[d] = vectors[d].Values.Copy();   // copied - no shared memory
            }
        }
        public KruskalTensor(T[] lambda, T[][][] values, IProvider<T> provider)
        {
            if (lambda is null)
            {
                throw new ArgumentNullException(nameof(lambda));
            }
            if (values is null)
            {
                throw new ArgumentNullException(nameof(values));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            if (values.Length != lambda.Length)
            {
                throw new ArgumentOutOfRangeException($"The length of '{nameof(values)}' does not match the length of '{nameof(lambda)}'.");
            }

            _rank = lambda.Length;
            if (_rank > 0)
            {
                _order = values[0].Length;
                _dimensions = new int[_order];
                for (int d = 0; d < _order; ++d)
                {
                    _dimensions[d] = values[0][d].Length;
                }

                for (int r = 1; r < _rank; ++r)
                {
                    T[][] slice = values[r];
                    for (int d = 0; d < _order; ++d)
                    {
                        if (slice[d].Length != _dimensions[d])
                        {
                            throw new ArgumentOutOfRangeException("Inconsistent vector lengths.");
                        }
                    }
                }
            }
            _lambda = lambda;
            _values = values;
        }
        public KruskalTensor(KruskalTensor<T> tensor)
        {
            if (tensor is null)
            {
                throw new ArgumentNullException(nameof(tensor));
            }

            _rank = tensor._rank;
            _order = tensor._order;
            _dimensions = tensor._dimensions.Copy();
            _values = new T[_rank][][];
            for (int r = 0; r < _rank; ++r)
            {
                _values[r] = CopyJagged(tensor._values[r], _dimensions);
            }
            _provider = tensor._provider;
            _lambda = tensor._lambda.Copy();
        }

        private static T[][][] InitStorage(int[] dimensions, int rank)
        {
            T[][][] storage = new T[rank][][];
            for (int r = 0; r < rank; ++r)
            {
                storage[r] = InitStorage(dimensions);
            }
            return storage;
        }
        private static T[][] InitStorage(int[] dimensions)
        {
            int order = dimensions.Length, i;
            T[][] vectors = new T[order][];
            for (i = 0; i < order; ++i)
            {
                vectors[i] = RectangularVector.Zeroes<T>(dimensions[i]);
            }
            return vectors;
        }
        private static T[][] CopyJagged(T[][] array, int[] lengths)
        {
            int order = array.Length;
            T[][] copy = new T[order][];

            for (int i = 0; i < order; ++i)
            {
                int len = lengths[i];
                T[] cpy = new T[len];
                Array.Copy(array[i], cpy, len);
                copy[i] = cpy;
            }
            return copy;
        }

        #endregion

        private void AssertValid(int[] index)
        {
            if (index is null)
            {
                throw new ArgumentNullException(nameof(index));
            }
            if (index.Length < _order)
            {
                throw new ArgumentOutOfRangeException(nameof(index));
            }

            for (int i = 0; i < _order; ++i)
            {
                int ind = index[i];
                if (ind < 0 || ind >= _dimensions[i])
                {
                    throw new ArgumentOutOfRangeException(nameof(index));
                }
            }
        }

        public KruskalTensor<T> Add(KruskalTensor<T> tensor)
        {
            if (tensor is null)
            {
                throw new ArgumentNullException(nameof(tensor));
            }
            if (tensor._order != _order)
            {
                throw new InvalidOperationException(ExceptionMessages.TensorOrderMismatch);
            }
            AssertDimensionsMatch(_dimensions, tensor._dimensions);

            T[][][] values = new T[_rank + tensor._rank][][];
            for (int r = 0; r < _rank; ++r)
            {
                values[r] = CopyJagged(_values[r], _dimensions);
            }
            for (int r = 0; r < tensor._rank; ++r)
            {
                values[r + _rank] = CopyJagged(tensor._values[r], _dimensions);
            }
            T[] lambda = new T[values.Length];
            Array.Copy(_lambda, lambda, _rank);
            Array.Copy(tensor._lambda, 0, lambda, _rank, tensor._rank);

            return new KruskalTensor<T>(lambda, values, _provider);
        }
        public KruskalTensor<T> Copy()
        {
            return new KruskalTensor<T>(this);
        }
        public override T Get(params int[] index)
        {
            AssertValid(index);

            T value = _provider.Zero;
            for (int r = 0; r < _rank; ++r)
            {
                T[][] vectors = _values[r];
                T product = _lambda[r];
                for (int d = 0; d < _order; ++d)
                {
                    product = _provider.Multiply(product, vectors[d][index[d]]);
                }
                value = _provider.Add(value, product);
            }
            return value;
        }
        public override int[] GetDimensions()
        {
            return _dimensions;
        }
        public KruskalTensor<T> Multiply(T scalar)
        {
            KruskalTensor<T> copy = Copy();
            copy.MultiplyInPlace(scalar);
            return copy;
        }
        public KruskalTensor<T> Multiply(T scalar, IDenseBLAS1Provider<T> blas)
        {
            KruskalTensor<T> copy = Copy();
            copy.MultiplyInPlace(scalar, blas);
            return copy;
        }
        public void MultiplyInPlace(T scalar)
        {
            if (ProviderFactory.TryGetDefaultBLAS1Provider(out IDenseBLAS1<T> blas))
            {
                MultiplyInPlace(scalar, blas);
            }
            else
            {
                for (int d = 0; d < _order; ++d)
                {
                    _lambda[d] = _provider.Multiply(_lambda[d], scalar);
                }
            }
        }
        public void MultiplyInPlace(T scalar, IDenseBLAS1Provider<T> blas)
        {
            if (blas is null)
            {
                throw new ArgumentNullException(nameof(blas));
            }
            if (!(blas is IDenseBLAS1<T> b))
            {
                throw new NotSupportedException(ExceptionMessages.TypeNotSupported);
            }
            b.SCAL(_lambda, scalar, 0, _order);
        }
        public KruskalTensor<T> Negate()
        {
            KruskalTensor<T> copy = Copy();
            copy.NegateInPlace();
            return copy;
        }
        public void NegateInPlace()
        {
            for (int d = 0; d < _order; ++d)
            {
                _lambda[d] = _provider.Negate(_lambda[d]);
            }
        }
        public override void Set(T value, params int[] index)
        {
            throw new NotImplementedException();
        }
        public KruskalTensor<T> Subtract(KruskalTensor<T> tensor)
        {
            if (tensor is null)
            {
                throw new ArgumentNullException(nameof(tensor));
            }
            if (tensor._order != _order)
            {
                throw new InvalidOperationException(ExceptionMessages.TensorOrderMismatch);
            }
            AssertDimensionsMatch(_dimensions, tensor._dimensions);

            T[][][] values = new T[_rank + tensor._rank][][];
            for (int r = 0; r < _rank; ++r)
            {
                values[r] = CopyJagged(_values[r], _dimensions);
            }
            for (int r = 0; r < tensor._rank; ++r)
            {
                values[r + _rank] = CopyJagged(tensor._values[r], _dimensions);
            }
            T[] lambda = new T[values.Length];
            Array.Copy(_lambda, lambda, _rank);
            for (int r = 0; r < tensor._rank; ++r)
            {
                lambda[r + _rank] = _provider.Negate(tensor._lambda[r]);
            }

            return new KruskalTensor<T>(lambda, values, _provider);
        }
        public override T ToScalar()
        {
            if (_rank == 0)
            {
                return _provider.Zero;
            }

            if (_order == 0)
            {
                T sum = _provider.Zero;
                for (int r = 0; r < _rank; ++r)
                {
                    sum = _provider.Add(sum, _lambda[r]);
                }
                return sum;
            }

            // Also check for the case where all dimensions are size 1
            for (int d = 0; d < _order; ++d)
            {
                if (_dimensions[d] > 1)
                {
                    throw new InvalidOperationException("Tensor is not of order 0.");
                }
            }

            // a BLAS would be great here
            T dot = _provider.Zero;
            T[] workspace = new T[_order + 1];
            for (int r = 0; r < _rank; ++r)
            {
                T[][] vectors = _values[r];
                for (int d = 0; d < _order; ++d)
                {
                    workspace[d] = vectors[d][0];
                }
                workspace[_order] = _lambda[r];

                dot = _provider.Add(dot, _provider.Product(workspace));
            }
            return dot;
        }
        public override Vector<T> ToVector()
        {
            if (_order == 1)
            {
                int dim = _dimensions[0];
                T[] values = new T[dim];
                for (int r = 0; r < _rank; ++r)
                {
                    // BLAS AXPY would be great here...
                    T[] vect = _values[r][0];
                    T lambda = _lambda[r];
                    for (int i = 0; i < dim; ++i)
                    {
                        values[i] = _provider.Add(values[i], _provider.Multiply(lambda, vect[i]));
                    }
                }
                return new DenseVector<T>(values);
            }

            // Consider the case where all but 1 dimension is of length 1
            int index = 0, count = 0;
            for (int d = 0; d < _order; ++d)
            {
                if (_dimensions[d] > 1)
                {
                    index = d;
                    ++count;
                }
            }

            if (count > 1)
            {
                throw new InvalidOperationException("Tensor is not of order 1.");
            }

            else
            {
                int dim = _dimensions[index];
                T[] values = RectangularVector.Zeroes<T>(dim);
                for (int r = 0; r < _rank; ++r)
                {
                    // Dense BLAS AXPY would be great here
                    T[] vect = _values[r][index];
                    T lambda = _lambda[r];
                    for (int i = 0; i < dim; ++i)
                    {
                        values[i] = _provider.Add(values[i], _provider.Multiply(lambda, vect[i]));
                    }
                }

                return new DenseVector<T>(values);
            }
        }
        public override Matrix<T> ToMatrix()
        {
            // Consider the case where there are only 2 dimensions > 1
            int rowIndex = -1, 
                columnIndex = -1;
            
            if (_order == 2)
            {
                rowIndex = 0;
                columnIndex = 1;
            }
            else
            {
                int count = 0;
                for (int d = 0; d < _order; ++d)
                {
                    if (_dimensions[d] > 1)
                    {
                        if (count >= 2)
                        {
                            throw new InvalidOperationException("Tensor is not of order 2.");
                        }
                        if (count == 1)
                        {
                            columnIndex = d;
                        }
                        else
                        {
                            rowIndex = d;
                        }
                        ++count;
                    }
                }
                if (count != 2)
                {
                    throw new InvalidOperationException("Tensor is not of order 2.");
                }
            }

            DenseMatrix<T> matrix = new DenseMatrix<T>(_dimensions[rowIndex], _dimensions[columnIndex]);
            var blas = new DefaultNativeProvider<T>(_provider);

            for (int r = 0; r < _rank; ++r)
            {
                T[][] slice = _values[r];
                T scalar = _lambda[r];
                for (int d = 0; d < _order; ++d)
                {
                    if (d != rowIndex && d != columnIndex) 
                    {
                        scalar = _provider.Multiply(scalar, slice[d][0]);
                    }
                }
                matrix.Rank1Update(scalar, new DenseVector<T>(slice[rowIndex]), new DenseVector<T>(slice[columnIndex]), blas);
            }

            return matrix;
        }

        public static KruskalTensor<T> operator +(KruskalTensor<T> A, KruskalTensor<T> B)
        {
            if (A is null)
            {
                throw new ArgumentNullException(nameof(A));
            }
            return A.Add(B);
        }
        public static KruskalTensor<T> operator -(KruskalTensor<T> A, KruskalTensor<T> B)
        {
            if (A is null)
            {
                throw new ArgumentNullException(nameof(A));
            }
            return A.Subtract(B);
        }
        public static KruskalTensor<T> operator -(KruskalTensor<T> A)
        {
            if (A is null)
            {
                throw new ArgumentNullException(nameof(A));
            }
            return A.Negate();
        }
        public static KruskalTensor<T> operator *(KruskalTensor<T> A, T scalar)
        {
            if (A is null)
            {
                throw new ArgumentNullException(nameof(A));
            }

            if (ProviderFactory.TryGetDefaultBLAS1Provider<T>(out IDenseBLAS1<T> blas))
            {
                return A.Multiply(scalar, blas);
            }
            return A.Multiply(scalar);
        }
        public static KruskalTensor<T> operator *(T scalar, KruskalTensor<T> A)
        {
            return A * scalar;
        }
    }
}
