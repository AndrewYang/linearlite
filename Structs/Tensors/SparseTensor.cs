﻿using LinearNet.Global;
using LinearNet.Helpers;
using LinearNet.Providers;
using LinearNet.Providers.Basic.Interfaces;
using LinearNet.Tensors;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace LinearNet.Structs
{
    /// <summary>
    /// Implementation of a DOK (dictionary of keys) sparse tensor object.
    /// </summary>
    /// <cat>linear-algebra</cat>
    public class SparseTensor<T> : Tensor<T> where T : new()
    {
        private readonly T _zero;
        private readonly int _order;
        private readonly List<int> _dimensions;
        private readonly Dictionary<long, T> _values;

        internal Dictionary<long, T> Values { get { return _values; } }
        public override int Order => _order;

        public override long NonZeroCount
        {
            get
            {
                return _values.LongCount(); // long count is much slower
            }
        }

        public override IEnumerable<TensorEntry<T>> NonZeroEntries
        {
            get
            {
                int[] index = new int[_order];
                foreach (KeyValuePair<long, T> entry in _values)
                {
                    get_index(entry.Key, index);
                    yield return new TensorEntry<T>(index.Copy(), entry.Value);
                }
            }
        }

        #region Constructors 

        /// <summary>
        /// Constructor for a sparse tensor by copying another tensor
        /// </summary>
        /// <param name="tensor"></param>
        public SparseTensor(SparseTensor<T> tensor)
        {
            if (tensor == null)
            {
                throw new ArgumentNullException();
            }

            _zero = new T();
            _order = tensor.Order;

            // Copy over the dimensions list
            _dimensions = new List<int>();
            foreach (int dim in tensor._dimensions)
            {
                _dimensions.Add(dim);
            }

            // Copy over the values dictionary
            _values = InitializeDictionary(_dimensions);
            foreach (KeyValuePair<long, T> pair in _values)
            {
                _values[pair.Key] = pair.Value;
            }
        }
        public SparseTensor(params int[] dimensions)
        {
            if (dimensions == null)
            {
                throw new ArgumentNullException();
            }

            _dimensions = new List<int>(dimensions.Length);
            _dimensions.AddRange(dimensions);

            _order = dimensions.Length;
            _zero = new T();

            _values = InitializeDictionary(dimensions);
        }
        internal SparseTensor(int[] dimensions, Dictionary<long, T> values) : this(dimensions)
        {
            _values = values;
        }

        /// <summary>
        /// Create an order 0 tensor from a scalar value of type T
        /// </summary>
        /// <param name="value"></param>
        public SparseTensor(T value) : this()
        {
            _values[0] = value;
        }

        /// <summary>
        /// Create a order 1 tensor from a dense vector of values
        /// </summary>
        /// <param name="values"></param>
        public SparseTensor(T[] values) : this(values.Length)
        {
            int len = values.Length, i;
            for (i = 0; i < len; ++i)
            {
                _values[i] = values[i];
            }
        }
        public SparseTensor(DenseVector<T> vector) : this(vector.Values) { }
        public SparseTensor(SparseVector<T> vector)
        {
            if (vector is null)
            {
                throw new ArgumentNullException(nameof(vector));
            }

            _dimensions = new List<int>(1) { vector.Dimension };
            _order = 1;
            _zero = new T();
            _values = new Dictionary<long, T>(vector.Values.Length);

            int len = vector.Values.Length, i;
            for (i = 0; i < len; ++i)
            {
                _values[vector.Indices[i]] = vector.Values[i];
            }
        }

        /// <summary>
        /// Create a order 2 sparse tensor from a dense 2D rectangular array of values
        /// </summary>
        /// <param name="values"></param>
        public SparseTensor(T[,] values) : this(values.GetLength(0), values.GetLength(1))
        {
            int rows = values.GetLength(0), cols = values.GetLength(1), i, j, index = 0;
            for (i = 0; i < rows; ++i)
            {
                for (j = 0; j < cols; ++j)
                {
                    _values[index++] = values[i, j];
                }
            }
        }
        /// <summary>
        /// Create an order 2 sparse tensor from a dense 2D jagged array of values
        /// </summary>
        /// <param name="values"></param>
        public SparseTensor(T[][] values) : this(values, GetBoundingDimensions(values)) { }
        public SparseTensor(T[][] values, int[] dimensions) : this(dimensions)
        {
            if (values == null)
            {
                throw new ArgumentNullException("Matrix is null or empty.");
            }
            if (dimensions.Length != 2)
            {
                throw new ArgumentException("Order must be 2.");
            }

            int rows = values.Length, i, j, index = 0, outerIndex = 0;
            for (i = 0; i < rows; ++i)
            {
                T[] row = values[i];
                int cols = row.Length;
                for (j = 0; j < cols; ++j)
                {
                    _values[index++] = row[j];
                }

                outerIndex += dimensions[0];
                index = outerIndex;
            }
        }
        public SparseTensor(DenseMatrix<T> matrix) : this(matrix.Values) { }
        /// <summary>
        /// Create an order 2 sparse tensor from a matrix.
        /// </summary>
        /// <param name="matrix">The matrix used to create the tensor.</param>
        public SparseTensor(Matrix<T> matrix)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            _dimensions = new List<int>(2) { matrix.Rows, matrix.Columns };
            _order = 2;
            _zero = new T();
            _values = new Dictionary<long, T>(matrix.SymbolicNonZeroCount);

            IEnumerable<MatrixEntry<T>> nzEntries = matrix.NonZeroEntries;
            foreach (MatrixEntry<T> e in nzEntries)
            {
                _values[e.RowIndex * matrix.Columns + e.ColumnIndex] = e.Value;
            }
        }

        /// <summary>
        /// Create an order 3 tensor from a 3D rectangular array
        /// </summary>
        /// <param name="values"></param>
        public SparseTensor(T[,,] values) : this(values.GetLength(0), values.GetLength(1), values.GetLength(2))
        {
            if (values == null)
            {
                throw new ArgumentNullException("Tensor is null or empty.");
            }

            int m = values.GetLength(0), n = values.GetLength(1), p = values.GetLength(2), i, j, k, index = 0;
            for (i = 0; i < m; ++i)
            {
                for (j = 0; j < n; ++j)
                {
                    for (k = 0; k < p; ++k)
                    {
                        _values[index++] = values[i, j, k];
                    }
                }
            }
        }
        public SparseTensor(T[][][] values) : this(values, GetBoundingDimensions(values)) { }
        public SparseTensor(T[][][] S, int[] dimensions)
        {
            if (S == null)
            {
                throw new ArgumentNullException("Tensor is null or empty.");
            }

            int m = S.Length, i, j, k, index = 0, boundingIndex = 0, d2 = dimensions[1], d3 = dimensions[2];
            for (i = 0; i < m; ++i)
            {
                T[][] S_i = S[i];
                int n = S_i.Length;
                for (j = 0; j < n; ++j)
                {
                    T[] S_ij = S_i[j];
                    int p = S_ij.Length;
                    for (k = 0; k < p; ++k)
                    {
                        _values[index++] = S_ij[k];
                    }

                    boundingIndex += d3;
                    index = boundingIndex;
                }
                boundingIndex += d2;
                index = boundingIndex;
            }
        }

        /// <summary>
        /// Create a rank 4 tensor from a 4D rectangular array
        /// </summary>
        /// <param name="S"></param>
        public SparseTensor(T[,,,] S) : this(S.GetLength(0), S.GetLength(1), S.GetLength(2), S.GetLength(3))
        {
            if (S == null) throw new ArgumentNullException("Tensor is null or empty.");

            int m = S.GetLength(0), n = S.GetLength(1), p = S.GetLength(2), q = S.GetLength(3), i, j, k, l, index = 0;

            for (i = 0; i < m; ++i)
            {
                for (j = 0; j < n; ++j)
                {
                    for (k = 0; k < p; ++k)
                    {
                        for (l = 0; l < q; ++l)
                        {
                            _values[index++] = S[i, j, k, l];
                        }
                    }
                }
            }
        }
        public SparseTensor(T[][][][] S) : this(S, GetBoundingDimensions(S)) { }
        private SparseTensor(T[][][][] S, int[] dimensions) : this(dimensions)
        {
            if (S == null) throw new ArgumentNullException("Tensor is null or empty.");

            int m = S.Length, i, j, k, l, index = 0, boundingIndex = 0, d2 = dimensions[1], d3 = dimensions[2], d4 = dimensions[3];
            for (i = 0; i < m; ++i)
            {
                T[][][] S_i = S[i];
                int n = S_i.Length;
                for (j = 0; j < n; ++j)
                {
                    T[][] S_ij = S_i[j];
                    int p = S_ij.Length;
                    for (k = 0; k < p; ++k)
                    {
                        T[] S_ijk = S_ij[k];
                        int q = S_ijk.Length;
                        for (l = 0; l < q; ++l)
                        {
                            _values[index++] = S_ijk[l];
                        }
                        boundingIndex += q;
                        index = boundingIndex;
                    }
                    boundingIndex += p;
                    index = boundingIndex;
                }
                boundingIndex += n;
                index = boundingIndex;
            }
        }

        public SparseTensor(DenseTensor<T> tensor)
        {
            if (tensor is null)
            {
                throw new ArgumentNullException(nameof(tensor));
            }

            _order = tensor.Order;
            _dimensions = tensor.GetDimensions().ToList();
            _zero = new T();

            T[] values = tensor.Values;
            if (values.LongLength > int.MaxValue)
            {
                throw new OverflowException("Tensor is too large to be stored in sparse format.");
            }

            int len = values.Length,
                incrementIndex = _order - 1,
                incrementIndexDim = _dimensions[incrementIndex];

            _values = new Dictionary<long, T>(len);
            int[] index = new int[_order];

            for (int i = 0; i < len; ++i)
            {
                if (index[incrementIndex] == incrementIndexDim)
                {
                    int j = incrementIndex;
                    while (index[j] == _dimensions[j])
                    {
                        index[j--] = 0;
                        index[j]++;
                    }
                }

                _values[get_key(index)] = values[i];

                index[incrementIndex]++;
            }        
        }
        public SparseTensor(Tensor<T> tensor)
        {
            if (tensor is null)
            {
                throw new ArgumentNullException(nameof(tensor));
            }
            _order = tensor.Order;
            _dimensions = tensor.GetDimensions().ToList();
            _zero = new T();

            long nnz = tensor.NonZeroCount;
            if (nnz > int.MaxValue)
            {
                throw new OverflowException("Tensor is too large to be stored as a sparse tensor.");
            }

            _values = new Dictionary<long, T>((int)nnz);
            foreach (TensorEntry<T> e in tensor.NonZeroEntries)
            {
                _values[get_key(e.Index)] = e.Value;
            }
        }

        private Dictionary<long, T> InitializeDictionary(IEnumerable<int> dimensions)
        {
            long size = TryCalculateSize(dimensions);

            if (size > int.MaxValue)
            {
                // Give up, accept the crush in performance
                return new Dictionary<long, T>();
            }
            else
            {
                // Maximise performance by setting a key size
                return new Dictionary<long, T>((int)size);
            }
        }
        private long TryCalculateSize(IEnumerable<int> dimensions)
        {
            long size = 1L;
            foreach (int dim in dimensions)
            {
                size = checked(size * dim);
            }
            return size;
        }

        #endregion


        #region Private methods

        /// <summary>
        /// Get the maximum (bounding) dimensionality along a particular dimension.
        /// Similar to GetLength(int i)
        /// </summary>
        /// <param name="A"></param>
        /// <param name="dimension"></param>
        /// <returns></returns>
        private static int[] GetBoundingDimensions(T[][] A)
        {
            if (A == null)
            {
                throw new ArgumentNullException();
            }

            int max = A[0].Length, i;
            for (i = 0; i < max; ++i)
            {
                int len = A[i].Length;
                if (len > max) max = len;
            }
            return new int[] { A.Length, max };
        }
        private static int[] GetBoundingDimensions(T[][][] A)
        {
            if (A == null)
            {
                throw new ArgumentNullException();
            }

            int max1 = 0, max2 = 0, m = A.Length, i, j;
            for (i = 0; i < m; ++i)
            {
                T[][] A_i = A[i];
                int n = A_i.Length;
                if (n > max1) max1 = n;

                for (j = 0; j < n; ++j)
                {
                    if (A_i[j].Length > max2) max2 = A_i[j].Length;
                }
            }
            return new int[] { A.Length, max1, max2 };
        }
        private static int[] GetBoundingDimensions(T[][][][] A)
        {
            if (A == null)
            {
                throw new ArgumentNullException();
            }

            int max1 = 0, max2 = 0, max3 = 0, i, j, k, m = A.Length;
            for (i = 0; i < m; ++i)
            {
                T[][][] A_i = A[i];
                int n = A_i.Length;
                if (n > max1) max1 = n;

                for (j = 0; j < n; ++j)
                {
                    T[][] A_ij = A_i[j];
                    int p = A_ij.Length;
                    if (p > max2) max2 = p;

                    for (k = 0; k < p; ++k)
                    {
                        if (A_ij[k].Length > max3)
                        {
                            max3 = A_ij[k].Length;
                        }
                    }
                }
            }
            return new int[] { A.Length, max1, max2, max3 };
        }

        private void check_index_valid(int[] index)
        {
            if (index == null)
            {
                throw new ArgumentNullException();
            }

            int len = index.Length;
            if (len < _order)
            {
                throw new ArgumentOutOfRangeException();
            }

            for (int i = len - _order; i < len; ++i)
            {
                int ix = index[i];
                if (ix < 0 || ix >= _dimensions[i])
                {
                    throw new ArgumentOutOfRangeException();
                }
            }
        }

        /// <summary>
        /// Get the key using the index array
        /// </summary>
        /// <param name="index"></param>
        /// <param name="big_endian">
        /// If true, the [0] index will have largest weight. If false, the last 
        /// index will have largest weight.
        /// </param>
        /// <returns></returns>
        private long get_key(int[] index, bool big_endian = true)
        {
            if (_order == 0)
            {
                return 0L;
            }

            int len = index.Length;
            long ix = 0L;

            if (big_endian)
            {
                for (int i = len - _order, j = 0; i < len; ++i, ++j)
                {
                    ix *= _dimensions[j];
                    ix += index[i];
                }
            }
            else
            {
                int last = _order - len;
                for (int i = len - 1, j = _order - 1; i >= last; --i, --j)
                {
                    ix *= _dimensions[j];
                    ix += index[i];
                }
            }

            return ix;
        }
        /// <summary>
        /// Given a valid key, fills in the index array
        /// The index array must be at least the order of the tensor.
        /// If the index array is longer than the tensor's order, then the 
        /// indices will be filled starting at the last element of the array.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="index"></param>
        internal void get_index(long key, int[] index, bool big_endian = true)
        {
            if (index.Length < _order)
            {
                throw new ArgumentOutOfRangeException();
            }

            if (big_endian)
            {
                int i, j;
                for (i = index.Length - 1, j = _order - 1; j >= 0; --i, --j)
                {
                    int dim = _dimensions[j];
                    long ix = key % dim;
                    index[i] = (int)ix;
                    key = (key - ix) / dim;
                }
            }
            else
            {
                int len = index.Length;
                for (int i = len - _order - 1, j = 0; i < len; ++i, ++j)
                {
                    int dim = _dimensions[j];
                    long ix = key % dim;
                    index[i] = (int)ix;
                    key = (key - ix) / dim;
                }
            }
        }

        #endregion


        #region Internal methods 

        internal bool Equals(SparseTensor<T> tensor, Func<T, T, bool> Equals)
        {
            if (!DimensionsEquals(tensor))
            {
                return false;
            }

            Dictionary<long, T> vals = tensor.Values;

            // Dual looping is less efficient but conserves memory
            foreach (KeyValuePair<long, T> pair in _values)
            {
                long k = pair.Key;
                if (!vals.ContainsKey(k) || !Equals(pair.Value, vals[k])) return false;
            }

            // No need to check for equality, as it should already be handled 
            // in the previous case.
            foreach (long key in vals.Keys)
            {
                if (!_values.ContainsKey(key)) return false;
            }
            return true;
        }

        #endregion


        #region Public methods 

        public SparseTensor<T> AddElementwise(SparseTensor<T> tensor)
        {
            if (!ProviderFactory.TryGetDefaultProvider<T>(out IProvider<T> provider))
            {
                throw new NotSupportedException(ExceptionMessages.TypeNotSupported);
            }
            return AddElementwise(tensor, provider);
        }

        /// <summary>
        /// Add another tensor to this tensor, elementwise.
        /// </summary>
        /// <param name="tensor"></param>
        /// <param name="provider"></param>
        /// <returns></returns>
        public SparseTensor<T> AddElementwise(SparseTensor<T> tensor, IProvider<T> provider)
        {
            if (tensor is null)
            {
                throw new ArgumentNullException(nameof(tensor));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            TensorChecks.CheckDimensionsMatch(this, tensor, out int[] dimensions);

            SparseTensor<T> result = new SparseTensor<T>(dimensions.ToArray());
            Dictionary<long, T> v = result._values, s = _values, t = tensor._values;

            foreach (KeyValuePair<long, T> pair in s)
            {
                v.Add(pair.Key, pair.Value);
            }
            foreach (KeyValuePair<long, T> pair in t)
            {
                long key = pair.Key;
                if (v.ContainsKey(key))
                {
                    v[key] = provider.Add(v[key], pair.Value);
                }
                else
                {
                    v[key] = pair.Value;
                }
            }
            return result;
        }

        public bool DimensionsEquals(SparseTensor<T> tensor)
        {
            if (tensor == null || tensor._order != _order)
            {
                return false;
            }

            List<int> dimensions = tensor._dimensions;
            for (int i = 0; i < _order; ++i)
            {
                if (_dimensions[i] != dimensions[i])
                {
                    return false;
                }
            }
            return true;
        }
        public bool DimensionsEquals(DenseTensor<T> tensor)
        {
            if (tensor == null || tensor.Order != _order)
            {
                return false;
            }

            int[] dimensions = tensor.GetDimensions();
            for (int i = 0; i < _order; ++i)
            {
                if (_dimensions[i] != dimensions[i])
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Returns the direct sum between this tensor and another tensor.
        /// </summary>
        /// <param name="tensor"></param>
        /// <returns></returns>
        public SparseTensor<T> DirectSum(SparseTensor<T> tensor)
        {
            if (tensor is null)
            {
                throw new ArgumentNullException(nameof(tensor));
            }
            if (tensor._order != _order)
            {
                throw new InvalidOperationException(ExceptionMessages.TensorOrderMismatch);
            }

            int[] dimensions = new int[_order];
            for (int i = 0; i < _order; ++i)
            {
                dimensions[i] = checked(_dimensions[i] + tensor._dimensions[i]);
            }

            SparseTensor<T> sum = new SparseTensor<T>(dimensions);
            int[] index = new int[_order];
            foreach (KeyValuePair<long, T> pair in _values)
            {
                get_index(pair.Key, index, true);
                sum.Set(pair.Value, index);
            }
            foreach (KeyValuePair<long, T> pair in tensor._values)
            {
                get_index(pair.Key, index, true);
                for (int i = 0; i < _order; ++i) index[i] += _dimensions[i];
                sum.Set(pair.Value, index);
            }
            return sum;
        }

        /// <summary>
        /// Returns a new tensor created by applying a function to each non-zero entry of 
        /// this tensor. Zero entries are ignored.
        /// </summary>
        /// <typeparam name="F"></typeparam>
        /// <param name="function">The function to apply elementwise</param>
        /// <returns></returns>
        public SparseTensor<F> Elementwise<F>(Func<T, F> function) where F : new()
        {
            if (function is null)
            {
                throw new ArgumentNullException(nameof(function));
            }

            Dictionary<long, F> values = new Dictionary<long, F>(_values.Count);
            foreach (KeyValuePair<long, T> e in _values)
            {
                values[e.Key] = function(e.Value);
            }
            return new SparseTensor<F>(_dimensions.ToArray(), values);
        }

        /// <summary>
        /// Returns the Frobenius distance from this tensor to another sparse tensor.
        /// </summary>
        /// <param name="tensor"></param>
        /// <param name="provider"></param>
        /// <returns></returns>
        public T FrobeniusDistance(SparseTensor<T> tensor, IRealProvider<T> provider)
        {
            if (tensor is null)
            {
                throw new ArgumentNullException(nameof(tensor));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            TensorChecks.CheckDimensionsMatch(this, tensor, out int[] _);

            T dist = provider.Zero;
            foreach (KeyValuePair<long, T> e in _values)
            {
                if (tensor._values.ContainsKey(e.Key))
                {
                    T d = provider.Subtract(e.Value, tensor._values[e.Key]);
                    dist = provider.Add(dist, provider.Multiply(d, d));
                }
                else
                {
                    dist = provider.Add(dist, provider.Multiply(e.Value, e.Value));
                }
            }
            return provider.Sqrt(dist);
        }

        /// <summary>
        /// Returns the inner product between this tensor and another sparse tensor.
        /// </summary>
        /// <param name="tensor">A sparse tensor.</param>
        /// <returns>The inner product.</returns>
        public T InnerProduct(SparseTensor<T> tensor, IProvider<T> provider)
        {
            if (tensor is null)
            {
                throw new ArgumentNullException(nameof(tensor));
            }
            TensorChecks.CheckDimensionsMatch(this, tensor, out int[] dimensions);

            T sum = provider.Zero;
            foreach (KeyValuePair<long, T> e in _values)
            {
                if (tensor._values.ContainsKey(e.Key))
                {
                    sum = provider.Add(sum, provider.Multiply(e.Value, tensor._values[e.Key]));
                }
            }
            return sum;
        }

        public SparseTensor<T> Multiply(T scalar)
        {
            if (!ProviderFactory.TryGetDefaultProvider(out IProvider<T> provider))
            {
                throw new NotSupportedException(ExceptionMessages.TypeNotSupported);
            }
            return Multiply(scalar, provider);
        }

        /// <summary>
        /// Multiply this tensor by a scalar, using the specified provider.
        /// </summary>
        /// <param name="scalar"></param>
        /// <param name="provider"></param>
        /// <returns></returns>
        public SparseTensor<T> Multiply(T scalar, IProvider<T> provider)
        {
            if (scalar is null)
            {
                throw new ArgumentNullException(nameof(scalar));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }

            Dictionary<long, T> values = new Dictionary<long, T>(_values.Count);
            foreach (KeyValuePair<long, T> e in _values)
            {
                values.Add(e.Key, provider.Multiply(scalar, e.Value));
            }
            return new SparseTensor<T>(_dimensions.ToArray(), values);
        }

        public SparseTensor<T> Multiply(SparseVector<T> vector, int mode)
        {
            if (!ProviderFactory.TryGetDefaultProvider(out IProvider<T> provider))
            {
                throw new NotSupportedException(ExceptionMessages.TypeNotSupported);
            }
            return Multiply(vector, mode, provider);
        }

        /// <summary>
        /// Calculates a sparse tensor-vector n-mode product, returning a new sparse tensor of dimension - 1.
        /// </summary>
        /// <param name="vector"></param>
        /// <param name="mode">The dimension along which the vector will be postmultiplied.</param>
        /// <returns></returns>
        public SparseTensor<T> Multiply(SparseVector<T> vector, int mode, IProvider<T> provider)
        {
            if (vector is null)
            {
                throw new ArgumentNullException(nameof(vector));
            }
            if (mode < 0 || mode >= Order)
            {
                throw new ArgumentOutOfRangeException(nameof(mode));
            }
            if (vector.Dimension != _dimensions[mode])
            {
                throw new InvalidOperationException("Dimensions don't match.");
            }

            int[] dim = new int[Order - 1];
            for (int i = 0, k = 0; i < Order; ++i)
            {
                if (i != mode)
                {
                    dim[k++] = _dimensions[i];
                }
            }

            SparseTensor<T> product = new SparseTensor<T>(dim);
            Dictionary<long, T> new_dict = new Dictionary<long, T>();
            T zero = provider.Zero;

            int[] index = new int[Order],
                new_index = new int[Order - 1];
            foreach (KeyValuePair<long, T> e in _values)
            {
                get_index(e.Key, index);

                // Check if vector is non-zero at the index, this is a O(log(n)) operation, 
                // check performance!
                T v = vector[index[mode]];
                if (provider.Equals(v, zero)) continue;

                // Copy index into new coordinates
                Array.Copy(index, new_index, mode);
                Array.Copy(index, mode + 1, new_index, mode, Order - mode - 1);

                // Get the key under the new coordinates
                long new_key = product.get_key(new_index);
                if (new_dict.ContainsKey(new_key))
                {
                    new_dict[new_key] = provider.Add(new_dict[new_key], provider.Multiply(e.Value, v));
                }
                else
                {
                    new_dict[new_key] = provider.Multiply(e.Value, v);
                }
            }

            return product;
        }

        /// <summary>
        /// Calculates the sparse tensor-matrix n-mode product.
        /// </summary>
        /// <param name="matrix"></param>
        /// <param name="mode"></param>
        /// <param name="provider"></param>
        /// <returns></returns>
        public SparseTensor<T> Multiply(CSCSparseMatrix<T> matrix, int mode, IProvider<T> provider)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            if (mode < 0 || mode >= Order)
            {
                throw new ArgumentOutOfRangeException(nameof(mode));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            if (matrix.Columns != _dimensions[mode])
            {
                throw new InvalidOperationException("Dimensions don't match");
            }

            Dictionary<long, T> newValues = new Dictionary<long, T>(_values.Count);
            int[] index = new int[Order], 
                columnIndices = matrix.ColumnIndices, 
                rowIndices = matrix.RowIndices;
            T[] values = matrix.Values;

            foreach (KeyValuePair<long, T> e in _values)
            {
                get_index(e.Key, index);
                int c = index[mode], 
                    col_start = columnIndices[c], 
                    col_end = columnIndices[c + 1];

                T v = e.Value;
                for (int i = col_start; i < col_end; ++i)
                {
                    // Change index[mode] = r 
                    index[mode] = rowIndices[i];
                    long key = get_key(index);

                    if (newValues.ContainsKey(key))
                    {
                        newValues[key] = provider.Add(newValues[key], provider.Multiply(values[i], v));
                    }
                    else
                    {
                        newValues[key] = provider.Multiply(values[i], v);
                    }
                }
            }

            return new SparseTensor<T>(_dimensions.ToArray(), newValues);
        }

        public SparseTensor<T> MultiplyElementwise(SparseTensor<T> tensor)
        {
            if (!ProviderFactory.TryGetDefaultProvider(out IProvider<T> provider))
            {
                throw new NotSupportedException(ExceptionMessages.TypeNotSupported);
            }
            return MultiplyElementwise(tensor, provider);
        }

        public SparseTensor<T> MultiplyElementwise(SparseTensor<T> tensor, IProvider<T> provider)
        {
            TensorChecks.CheckDimensionsMatch(this, tensor, out int[] dimensions);

            SparseTensor<T> result = new SparseTensor<T>(dimensions.Copy());
            Dictionary<long, T> v = result._values, s = _values, t = tensor._values;
            foreach (KeyValuePair<long, T> pair in s)
            {
                if (t.ContainsKey(pair.Key))
                {
                    v.Add(pair.Key, provider.Multiply(pair.Value, t[pair.Key]));
                }
            }
            return result;
        }

        public void NegateInPlace()
        {
            if (!ProviderFactory.TryGetDefaultProvider<T>(out IProvider<T> provider))
            {
                throw new NotSupportedException(ExceptionMessages.TypeNotSupported);
            }
            NegateInPlace(provider);
        }

        public void NegateInPlace(IProvider<T> provider)
        {
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }

            long[] keys = new long[_values.Count];
            int k = 0;
            foreach (long key in _values.Keys)
            {
                keys[k++] = key;
            }

            for (int i = 0; i < keys.Length; ++i)
            {
                long key = keys[i];
                _values[key] = provider.Negate(_values[key]);
            }
        }

        public SparseTensor<T> Negate()
        {
            if (!ProviderFactory.TryGetDefaultProvider<T>(out IProvider<T> provider))
            {
                throw new NotSupportedException(ExceptionMessages.TypeNotSupported);
            }
            return Negate(provider);
        }

        public SparseTensor<T> Negate(IProvider<T> provider)
        {
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }

            Dictionary<long, T> values = new Dictionary<long, T>(_values.Count);
            foreach (KeyValuePair<long, T> e in _values)
            {
                values.Add(e.Key, provider.Negate(e.Value));
            }
            return new SparseTensor<T>(_dimensions.ToArray(), values);
        }

        /// <summary>
        /// Permutes a dimension of this tensor using the specified permutation vector, 
        /// so that index $i$ along that dimension is replaced by $\pi(i)$.
        /// </summary>
        /// <param name="dimension">The dimension to permute.</param>
        /// <param name="permutationVector">The permutation vector to use.</param>
        public SparseTensor<T> Permute(int dimension, int[] permutationVector)
        {
            if (dimension < 0 || dimension >= _order)
            {
                throw new ArgumentOutOfRangeException(nameof(dimension));
            }

            if (permutationVector is null)
            {
                throw new ArgumentNullException(nameof(permutationVector));
            }
            if (permutationVector.Length != _dimensions[dimension])
            {
                throw new ArgumentOutOfRangeException("Dimension does not match length of permutation vector.");
            }
            if (!permutationVector.IsPermutation())
            {
                throw new ArgumentOutOfRangeException($"'{nameof(permutationVector)}' is not a permutation vector.");
            }

            Dictionary<long, T> newValues = new Dictionary<long, T>(_values.Count);
            int[] index = new int[_order], pindex = new int[_order];
            foreach (KeyValuePair<long, T> e in _values)
            {
                get_index(e.Key, index);

                for (int i = 0; i < dimension; ++i)
                {
                    pindex[permutationVector[i]] = index[i];
                }

                newValues[get_key(pindex)] = e.Value;
            }
            return new SparseTensor<T>(_dimensions.ToArray(), newValues);
        }

        /// <summary>
        /// Returns a tensor formed by selecting a set of dimensions to hold constant.
        /// </summary>
        /// <param name="selections">A dictionary containing pairs of integers representing
        /// the dimension and the selected index along that dimension, respectively. </param>
        /// <returns></returns>
        public SparseTensor<T> Select(Dictionary<int, int> selections)
        {
            if (selections is null)
            {
                throw new ArgumentNullException(nameof(selections));
            }

            int[] fixedIndices = RectangularVector.Repeat(-1, Order);
            foreach (KeyValuePair<int, int> s in selections)
            {
                if (s.Key < 0 || s.Key >= Order)
                {
                    throw new ArgumentOutOfRangeException(nameof(selections));
                }
                if (s.Value < 0 || s.Value >= _dimensions[s.Key])
                {
                    throw new ArgumentOutOfRangeException(nameof(selections));
                }
                fixedIndices[s.Key] = s.Value;
            }

            int newOrder = Order - selections.Count, k = 0;
            int[] newIndexIndices = new int[newOrder], 
                newIndex = new int[newOrder];
            for (int d = 0; d < Order; ++d)
            {
                if (!selections.ContainsKey(d))
                {
                    // Temporarily store the dimensions
                    newIndex[k] = _dimensions[d];
                    newIndexIndices[k++] = d;
                }
            }

            // Copying the dimension array is unnecessary, however 
            // we do it as a precaution
            SparseTensor<T> result = new SparseTensor<T>(newIndex.Copy());

            int[] index = new int[Order];
            foreach (KeyValuePair<long, T> e in _values)
            {
                get_index(e.Key, index);

                // Determine whether this entry is included in the selection
                bool match = true;
                for (int d = 0; d < Order; ++d)
                {
                    int fi = fixedIndices[d];
                    if (fi >= 0 && fi != index[d])
                    {
                        match = false;
                        break;
                    }
                }

                if (match)
                {
                    // Copy relevant indices into new index buffer
                    for (int d = 0; d < newOrder; ++d)
                    {
                        newIndex[d] = index[newIndexIndices[d]];
                    }
                    long key = result.get_key(newIndex);
                    result._values[key] = e.Value;
                }
            }

            return result;
        }

        /// <summary>
        /// Returns a tensor formed by selecting a subset of dimensions
        /// </summary>
        /// <param name="selections">A dictionary consisting of key-value pairs of 
        /// the dimension to select, and the selection expression along that dimension.</param>
        /// <returns>The subtensor formed by selecting a subset of dimensions.</returns>
        public SparseTensor<T> Select(Dictionary<int, string> selections, bool oneBased = false)
        {
            if (selections is null)
            {
                throw new ArgumentNullException(nameof(selections));
            }

            Dictionary<int, int[]> indices = new Dictionary<int, int[]>();
            foreach (KeyValuePair<int, string> q in selections)
            {
                if (q.Key < 0 || q.Key >= _order)
                {
                    throw new ArgumentOutOfRangeException(nameof(selections), "Dimension index is out of bounds.");
                }
                if (!Selector.TryParse(q.Value, oneBased, _dimensions[q.Key], out Selector s))
                {
                    throw new FormatException($"Invalid syntax near: {q.Value}");
                }
                indices.Add(q.Key, s.Selection);
            }

            // Calculate the new dimensions
            int[] dimensions = new int[_order];
            for (int d = 0; d < _order; ++d)
            {
                dimensions[d] = indices.ContainsKey(d) ? indices[d].Length : _dimensions[d];
            }

            SparseTensor<T> subtensor = new SparseTensor<T>(dimensions);

            int[] index = new int[_order];
            foreach (KeyValuePair<long, T> e in _values)
            {
                get_index(e.Key, index);

                bool in_selection = true;
                foreach (KeyValuePair<int, int[]> s in indices)
                {
                    int i = Array.IndexOf(s.Value, index[e.Key]);
                    if (i >= 0)
                    {
                        index[e.Key] = i; // replace with selection index
                    }
                    else
                    {
                        in_selection = false;
                        break;
                    }
                }

                if (in_selection)
                {
                    long key = subtensor.get_key(index);
                    subtensor._values.Add(key, e.Value);
                }
            }

            return subtensor;
        }

        /// <summary>
        /// Returns a sparse vector formed by fixing all dimensions except for one dimension.
        /// </summary>
        /// <param name="selections"></param>
        /// <returns></returns>
        public SparseVector<T> SelectAsVector(Dictionary<int, int> selections)
        {
            if (selections is null)
            {
                throw new ArgumentNullException(nameof(selections));
            }
            if (selections.Count != _order - 1)
            {
                throw new InvalidOperationException("Incorrect number of dimensions are fixed.");
            }

            int[] fixedIndices = RectangularVector.Repeat(-1, Order);
            foreach (KeyValuePair<int, int> s in selections)
            {
                if (s.Key < 0 || s.Key >= _order)
                {
                    throw new ArgumentOutOfRangeException(nameof(selections));
                }
                if (s.Value < 0 || s.Value >= _dimensions[s.Key])
                {
                    throw new ArgumentOutOfRangeException(nameof(selections));
                }
                fixedIndices[s.Key] = s.Value;
            }

            int unfixedIndex = 0;
            for (int d = 0; d < _order; ++d)
            {
                if (!selections.ContainsKey(d))
                {
                    unfixedIndex = d;
                    break;
                }
            }

            int[] index = new int[_order];

            List<int> indices = new List<int>();
            List<T> values = new List<T>();
            foreach (KeyValuePair<long, T> e in _values)
            {
                get_index(e.Key, index);
                indices.Add(index[unfixedIndex]);
                values.Add(e.Value);
            }

            // Packing 
            int[] index_arr = indices.ToArray();
            T[] value_arr = values.ToArray();
            Array.Sort(index_arr, value_arr);

            return new SparseVector<T>(_dimensions[unfixedIndex], index_arr, value_arr, false);
        }

        /// <summary>
        /// Returns a sparse matrix formed by fixing all dimensions except for 2 dimensions.
        /// </summary>
        /// <param name="fixedDimensions"></param>
        /// <returns></returns>
        public COOSparseMatrix<T> Slice(Dictionary<int, int> fixedDimensions)
        {
            if (fixedDimensions is null)
            {
                throw new ArgumentNullException(nameof(fixedDimensions));
            }

            if (fixedDimensions.Count != _order - 2)
            {
                throw new InvalidOperationException("Invalid number of fixed dimensions.");
            }

            int[] fixedIndices = RectangularVector.Repeat(-1, Order);
            foreach (KeyValuePair<int, int> s in fixedDimensions)
            {
                if (s.Key < 0 || s.Key >= _order)
                {
                    throw new ArgumentOutOfRangeException(nameof(fixedDimensions));
                }
                if (s.Value < 0 || s.Value >= _dimensions[s.Key])
                {
                    throw new ArgumentOutOfRangeException(nameof(fixedDimensions));
                }
                fixedIndices[s.Key] = s.Value;
            }

            int freeIndex1 = -1, freeIndex2 = 0;
            for (int d = 0; d < _order; ++d)
            {
                if (!fixedDimensions.ContainsKey(d))
                {
                    if (freeIndex1 < 0)
                    {
                        freeIndex1 = d;
                    }
                    else
                    {
                        freeIndex2 = d;
                        break;
                    }
                }
            }

            if (freeIndex2 == freeIndex1 || freeIndex1 < 0) 
            {
                throw new ArgumentOutOfRangeException(nameof(fixedDimensions));
            }

            List<MatrixEntry<T>> entries = new List<MatrixEntry<T>>();
            int[] index = new int[_order];
            foreach (KeyValuePair<long, T> e in _values)
            {
                get_index(e.Key, index);
                entries.Add(new MatrixEntry<T>(index[freeIndex1], index[freeIndex2], e.Value));
            }

            return new COOSparseMatrix<T>(_dimensions[freeIndex1], _dimensions[freeIndex2], entries, false);
        }

        public SparseTensor<T> SubtractElementwise(SparseTensor<T> tensor)
        {
            if (!ProviderFactory.TryGetDefaultProvider<T>(out IProvider<T> provider))
            {
                throw new NotSupportedException(ExceptionMessages.TypeNotSupported);
            }
            return SubtractElementwise(tensor, provider);
        }

        /// <summary>
        /// Subtract a tensor from this tensor, elementwise, using the specified provider.
        /// </summary>
        /// <param name="tensor"></param>
        /// <param name="provider"></param>
        /// <returns></returns>
        public SparseTensor<T> SubtractElementwise(SparseTensor<T> tensor, IProvider<T> provider)
        {
            TensorChecks.CheckDimensionsMatch(this, tensor, out int[] dimensions);

            SparseTensor<T> result = new SparseTensor<T>(dimensions.Copy());
            Dictionary<long, T> v = result._values, s = _values, t = tensor._values;

            foreach (KeyValuePair<long, T> pair in s)
            {
                v.Add(pair.Key, pair.Value);
            }
            foreach (KeyValuePair<long, T> pair in t)
            {
                long key = pair.Key;
                if (v.ContainsKey(key))
                {
                    v[key] = provider.Subtract(v[key], pair.Value);
                }
                else
                {
                    v[key] = provider.Negate(pair.Value);
                }
            }
            return result;
        }

        /// <summary>
        /// Returns a new tensor formed by summing over a subset of dimensions
        /// </summary>
        /// <param name="dimensions">The set of dimensions to sum over. Must be unique</param>
        /// <returns></returns>
        public SparseTensor<T> SumOver(IEnumerable<int> dimensions, IProvider<T> provider)
        {
            if (dimensions is null)
            {
                throw new ArgumentNullException(nameof(dimensions));
            }

            // Count the number of fixed dimensions (remove duplicates)
            int nFixed = 0;
            BitArray isFixed = new BitArray(_order);
            foreach (int d in dimensions)
            {
                if (d < 0 || d >= _order)
                {
                    throw new ArgumentOutOfRangeException(nameof(dimensions));
                }
                if (!isFixed[d])
                {
                    nFixed++;
                }
                isFixed[d] = true;
            }

            // Record the free dimensions in ascending order
            int[] freeDimensions = new int[_order - nFixed];
            for (int d = 0, k = 0; d < _order; ++d)
            {
                if (!isFixed[d])
                {
                    freeDimensions[k++] = d;
                }
            }

            // Workspaces
            int[] index = new int[_order], newIndex = new int[freeDimensions.Length];

            // Temporarily store the dimensionality of the new tensor inside newIndex
            for (int d = 0; d < freeDimensions.Length; ++d)
            {
                newIndex[d] = _dimensions[freeDimensions[d]];
            }

            SparseTensor<T> sum = new SparseTensor<T>(newIndex.Copy());

            foreach (KeyValuePair<long, T> e in _values)
            {
                get_index(e.Key, index);

                // Copy the new index from old
                for (int i = 0; i < freeDimensions.Length; ++i)
                {
                    newIndex[i] = index[freeDimensions[i]];
                }

                // Get new key
                long key = sum.get_key(newIndex);
                if (sum._values.ContainsKey(key))
                {
                    sum._values[key] = provider.Add(sum._values[key], e.Value);
                }
                else
                {
                    sum._values[key] = e.Value;
                }
            }

            return sum;
        }

        /// <summary>
        /// Returns a sparse vector formed by summing over all dimensions except for one dimension.
        /// </summary>
        /// <param name="dimension"></param>
        /// <param name="provider"></param>
        /// <returns></returns>
        public SparseVector<T> SumOverAllExcept(int dimension, IProvider<T> provider)
        {
            if (dimension < 0 || dimension >= _order)
            {
                throw new ArgumentOutOfRangeException(nameof(dimension));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }

            Dictionary<int, T> sum = new Dictionary<int, T>();

            int[] index = new int[_order];
            foreach (KeyValuePair<long, T> e in _values)
            {
                get_index(e.Key, index);

                int i = index[dimension];
                if (sum.ContainsKey(i))
                {
                    sum[i] = provider.Add(sum[i], e.Value);
                }
                else
                {
                    sum[i] = e.Value;
                }
            }

            // Pack 
            int[] indices = new int[sum.Count];
            T[] values = new T[sum.Count];
            int k = 0;
            foreach (KeyValuePair<int, T> entry in sum)
            {
                indices[k] = entry.Key;
                values[k++] = entry.Value;
            }

            Array.Sort(indices, values);
            return new SparseVector<T>(_dimensions[dimension], indices, values, true);
        }

        public SparseTensor<T> TensorProduct(SparseTensor<T> tensor)
        {
            if (!ProviderFactory.TryGetDefaultProvider(out IProvider<T> provider))
            {
                throw new NotSupportedException(ExceptionMessages.TypeNotSupported);
            }
            return TensorProduct(tensor, provider);
        }

        /// <summary>
        /// Calculates and returns the tensor product of (this) (x) tensor.
        /// The order of the tensor product is equal to the order of the tensors.
        /// The dimension of the tensor product is equal to the product of the dimensions 
        /// of the original tensors.
        /// 
        /// The orders of the two tensors being multiplied must be equal.
        /// </summary>
        /// <param name="tensor"></param>
        /// <returns></returns>
        public SparseTensor<T> TensorProduct(SparseTensor<T> tensor, IProvider<T> provider)
        {
            if (tensor is null)
            {
                throw new ArgumentNullException(nameof(tensor));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            if (tensor._order != _order)
            {
                throw new InvalidOperationException(ExceptionMessages.TensorOrderMismatch);
            }

            int[] dimensions = new int[_order];
            for (int i = 0; i < _order; ++i)
            {
                dimensions[i] = checked(_dimensions[i] * tensor._dimensions[i]);
            }
            SparseTensor<T> product = new SparseTensor<T>(dimensions);
            Dictionary<long, T> dict = product.Values;

            int[] index1 = new int[_order], index2 = new int[_order], indexProd = new int[_order];
            foreach (KeyValuePair<long, T> p1 in _values)
            {
                get_index(p1.Key, index1, true);
                T x = p1.Value;

                foreach (KeyValuePair<long, T> p2 in tensor._values)
                {
                    tensor.get_index(p2.Key, index2, true);

                    for (int i = 0; i < _order; ++i)
                    {
                        indexProd[i] = index2[i] + tensor._dimensions[i] * index1[i];
                    }

                    dict[product.get_key(indexProd)] = provider.Multiply(x, p2.Value);
                }
            }
            return product;
        }

        public override T ToScalar()
        {
            if (_order == 0)
            {
                return _values.ContainsKey(0L) ? _values[0L] : _zero;
            }

            for (int d = 0; d < _order; ++d)
            {
                if (_dimensions[d] > 1)
                {
                    throw new InvalidOperationException("Tensor is not of order 0.");
                }
            }
            return _values.ContainsKey(0L) ? _values[0L] : _zero;
        }

        public override Vector<T> ToVector()
        {
            if (_order == 0)
            {
                return new DenseVector<T>(new T[] { ToScalar() });
            }

            int dimIndex = -1;
            if (_order == 1)
            {
                dimIndex = 0;
            }
            else
            {
                int count = 0;
                for (int d = 0; d < _order; ++d)
                {
                    if (_dimensions[d] > 1)
                    {
                        dimIndex = d;
                        ++count;
                    }
                }
                if (count > 1)
                {
                    throw new InvalidOperationException("Tensor is not of order 1.");
                }
                if (count == 0)
                {
                    // any dimension will do, guaranteed to be defined because
                    // _order = 0 case is handled
                    dimIndex = 0; 
                }
            }

            int[] index = new int[_order];
            int nz = _values.Count, k = 0;
            int[] indices = new int[nz];
            T[] values = new T[nz];

            foreach (KeyValuePair<long, T> e in _values)
            {
                get_index(e.Key, index);
                indices[k] = index[dimIndex];
                values[k++] = e.Value;
            }

            Array.Sort(indices, values, 0, nz);

            return new SparseVector<T>(_dimensions[dimIndex], indices, values, true);
        }

        public override Matrix<T> ToMatrix()
        {
            if (_order == 0)
            {
                return new DOKSparseMatrix<T>(1, 1, new Dictionary<long, T>() { [0] = ToScalar() });
            }
            else if (_order == 1)
            {
                int rows = _dimensions[0];
                Vector<T> vector = ToVector();
                Dictionary<long, T> values = new Dictionary<long, T>(vector.NonZeroCount);
                foreach (VectorEntry<T> e in vector.NonZeroEntries)
                {
                    values[e.Index] = e.Value;
                }
                return new DOKSparseMatrix<T>(rows, 1, values);
            }
            else
            {
                // From this point onwards, guaranteed to have order >= 2
                int rowIndex = -1,
                    columnIndex = -1;

                if (_order == 2)
                {
                    rowIndex = 0;
                    columnIndex = 1;
                }
                else
                {
                    for (int d = 0; d < _order; ++d)
                    {
                        if (_dimensions[d] > 1)
                        {
                            if (columnIndex >= 0)
                            {
                                throw new InvalidOperationException("Tensor is not of order 2.");
                            }
                            if (rowIndex >= 0)
                            {
                                columnIndex = d;
                            }
                            else
                            {
                                rowIndex = d;
                            }
                        }
                    }

                    if (rowIndex < 0)
                    {
                        rowIndex = 0;
                    }
                    if (columnIndex < 0)
                    {
                        columnIndex = rowIndex + 1;
                    }
                }

                int rows = _dimensions[rowIndex],
                    cols = _dimensions[columnIndex];
                long lc = cols;

                int[] index = new int[_order];
                Dictionary<long, T> values = new Dictionary<long, T>(_values.Count);
                foreach (KeyValuePair<long, T> e in _values)
                {
                    get_index(e.Key, index);

                    // Actually this should equate to a straight copy of 
                    // _values because rowIndex < columnIndex...however 
                    // this code is a lot more clear 
                    values[index[rowIndex] * lc + index[columnIndex]] = e.Value;
                }

                return new DOKSparseMatrix<T>(rows, cols, values);
            }
        }

        /// <summary>
        /// Returns a vectorized version of this tensor.
        /// </summary>
        /// <returns></returns>
        public BigSparseVector<T> Vectorize()
        {
            long len = 1L;
            foreach (int dim in _dimensions)
            {
                len *= dim;
            }

            Dictionary<long, T> dict = new Dictionary<long, T>(_values.Count);
            foreach (KeyValuePair<long, T> pair in _values)
            {
                int[] index = new int[_order];
                get_index(pair.Key, index, true);

                long key = get_key(index, false);
                dict[key] = pair.Value;
            }
            return new BigSparseVector<T>(len, dict);
        }

        /// <summary>
        /// Returns a vectorized version of this order, using the specified 
        /// traversal order. The traversal order is a permutation matrix  
        /// that contains, in order, the indexes of the dimensions in which it 
        /// should traverse first. 
        /// </summary>
        /// <param name="traversalOrder">The traversal order array.</param>
        /// <returns>The vectorized tensor.</returns>
        public BigSparseVector<T> Vectorize(int[] traversalOrder)
        {
            if (traversalOrder is null)
            {
                throw new ArgumentNullException(nameof(traversalOrder));
            }
            if (traversalOrder.Length != _order)
            {
                throw new ArgumentOutOfRangeException($"Length of '{traversalOrder}' does not match the order of the tensor.");
            }
            if (!traversalOrder.IsPermutation())
            {
                throw new ArgumentOutOfRangeException(nameof(traversalOrder), "Not a permutation matrix.");
            }

            long len = 1L;
            checked
            {
                foreach (int dim in _dimensions)
                {
                    len *= dim;
                }
            }

            int[] dimensions = new int[_order];
            for (int d = 0; d < _order; ++d)
            {
                dimensions[d] = _dimensions[traversalOrder[d]];
            }
            SparseTensor<T> tensor = new SparseTensor<T>(dimensions);

            Dictionary<long, T> dict = new Dictionary<long, T>(_values.Count);
            int[] index = new int[_order], 
                pindex = new int[_order];

            foreach (KeyValuePair<long, T> e in _values)
            {
                get_index(e.Key, index);

                for (int i = 0; i < _order; ++i)
                {
                    pindex[i] = index[traversalOrder[i]];
                }

                // Get the permuted index
                dict[tensor.get_key(pindex)] = e.Value;
            }

            return new BigSparseVector<T>(len, dict);
        }

        #endregion


        #region Operations

        public static SparseTensor<T> operator +(SparseTensor<T> A, SparseTensor<T> B)
        {
            if (A is null) throw new ArgumentNullException(nameof(A));
            return A.AddElementwise(B);
        }

        public static SparseTensor<T> operator -(SparseTensor<T> A, SparseTensor<T> B)
        {
            if (A is null) throw new ArgumentNullException(nameof(A));
            return A.SubtractElementwise(B);
        }

        public static SparseTensor<T> operator -(SparseTensor<T> A)
        {
            if (A is null)
            {
                throw new ArgumentNullException(nameof(A));
            }
            return A.Negate();
        }

        public static SparseTensor<T> operator *(SparseTensor<T> A, SparseTensor<T> B)
        {
            if (A is null)
            {
                throw new ArgumentNullException(nameof(A));
            }
            return A.MultiplyElementwise(B);
        }

        public static SparseTensor<T> operator *(SparseTensor<T> A, T scalar)
        {
            if (A is null)
            {
                throw new ArgumentNullException(nameof(A));
            }
            return A.Multiply(scalar);
        }

        public static SparseTensor<T> operator *(T scalar, SparseTensor<T> A)
        {
            return A * scalar;
        }

        #endregion


        #region Casts

        public static explicit operator SparseTensor<T>(T scalar) => new SparseTensor<T>(scalar);
        public static explicit operator SparseTensor<T>(T[] vector) => new SparseTensor<T>(vector);
        public static explicit operator SparseTensor<T>(T[][] matrix) => new SparseTensor<T>(matrix);
        public static explicit operator SparseTensor<T>(T[,] matrix) => new SparseTensor<T>(matrix);
        public static explicit operator SparseTensor<T>(T[][][] tensor) => new SparseTensor<T>(tensor);
        public static explicit operator SparseTensor<T>(T[,,] tensor) => new SparseTensor<T>(tensor);
        public static explicit operator SparseTensor<T>(T[][][][] tensor) => new SparseTensor<T>(tensor);
        public static explicit operator SparseTensor<T>(T[,,,] tensor) => new SparseTensor<T>(tensor);

        public static explicit operator SparseTensor<T>(DenseVector<T> tensor) => new SparseTensor<T>(tensor);
        public static explicit operator SparseTensor<T>(DenseMatrix<T> matrix) => new SparseTensor<T>(matrix);
        public static explicit operator SparseTensor<T>(DenseTensor<T> tensor) => new SparseTensor<T>(tensor);

        #endregion

        #region Interface implementations

        public override int[] GetDimensions()
        {
            return _dimensions.ToArray();
        }
        public override T Get(params int[] index)
        {
            check_index_valid(index);

            long key = get_key(index, true);
            if (_values.ContainsKey(key))
            {
                return _values[key];
            }
            return _zero;
        }
        public override void Set(T value, params int[] index)
        {
            check_index_valid(index);
            _values[get_key(index, true)] = value;
        }

        #endregion
    }

    /// <summary>
    /// A static class containing useful functions for creating special types of sparse tensors.
    /// </summary>
    /// <cat>linear-algebra</cat>
    public static class SparseTensor
    {
        public static SparseTensor<T> Random<T>(params int[] dimensions) where T : new()
        {
            // Generate sampler
            Func<T> sample = DefaultGenerators.GetRandomGenerator<T>();

            // Generate and return a Sparse tensor
            SparseTensor<T> tensor = new SparseTensor<T>(dimensions);

            // Calculate number of samples to take
            long totalSize = 1;
            foreach (int dim in dimensions)
            {
                // overflow check
                long size = totalSize * dim;
                if (size < totalSize)
                {
                    totalSize = long.MaxValue;
                    break;
                }
                totalSize = size;
            }

            // Fill sparsely up to 1e6 entries
            Random r = new Random();
            int entries = (int)Math.Min(totalSize, 1e6);
            int[] index = new int[dimensions.Length];
            for (int i = 0; i < entries; ++i)
            {
                // Sample a point inside the tensor
                for (int j = 0; j < dimensions.Length; ++j)
                {
                    index[j] = r.Next(dimensions[j]);
                }
                tensor.Set(sample(), index);
            }
            return tensor;
        }
    }
}
