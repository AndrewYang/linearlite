﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace LinearNet.Structs
{
    /// <summary>
    /// An abstract class representing an arbitrary-order tensor. All tensor implementations inherit this class.
    /// </summary>
    /// <cat>linear-algebra</cat>
    public abstract class Tensor<T> where T : new()
    {
        /// <summary>
        /// The number of dimensions of the tensor (note this may differ from the rank).
        /// </summary>
        public abstract int Order { get; }

        /// <summary>
        /// The number of symbolic (structural) non-zeroes in this tensor.
        /// </summary>
        public abstract long NonZeroCount { get; }

        /// <summary>
        /// Returns an enumeration of the non-zero entries in this tensor
        /// </summary>
        public abstract IEnumerable<TensorEntry<T>> NonZeroEntries { get; }


        protected static bool DimensionsMatch(int[] dim1, int[] dim2)
        {
            if (dim1.Length != dim2.Length)
            {
                return false;
            }
            for (int i = 0; i < dim2.Length; ++i)
            {
                if (dim1[i] != dim2[i])
                {
                    return false;
                }
            }
            return true;
        }
        protected static void AssertDimensionsMatch(int[] dim1, int[] dim2)
        {
            if (!DimensionsMatch(dim1, dim2))
            {
                throw new InvalidOperationException("Tensor dimensions don't match.");
            }
        }


        /// <summary>
        /// Gets the value of the tensor at the specified index. 
        /// The index array length must be at least the tensor's order
        /// If the array length greater than the tensor's order, the first 
        /// 'order' indices are used.
        /// </summary>
        /// <param name="index">An array representing an index within the tensor.</param>
        /// <returns>The value of the tensor at the index.</returns>
        public abstract T Get(params int[] index);

        /// <summary>
        /// Returns a list of dimensions of the tensor
        /// The number of elements of this list will be equal to the order of the tensor.
        /// </summary>
        /// <returns>A list of the dimensionality of the tensor</returns>
        public abstract int[] GetDimensions();

        /// <summary>
        /// Returns whether this tensor is equal to another tensor in value.
        /// </summary>
        /// <param name="tensor">The tensor to compare.</param>
        /// <param name="equals">A function that returns true if its two arguments 
        /// of type <txt>T</txt> should be considered equal.</param>
        /// <returns>Whether the two tensors are equal in value.</returns>
        public virtual bool Equals(Tensor<T> tensor, Func<T, T, bool> equals)
        {
            if (tensor is null)
            {
                return false;
            }

            int[] dimensions = GetDimensions();
            if (!DimensionsMatch(dimensions, tensor.GetDimensions()))
            {
                return false;
            }

            if (equals is null)
            {
                throw new ArgumentNullException(nameof(equals));
            }

            if (Order == 0)
            {
                return equals(Get(), tensor.Get());
            }

            int[] index = new int[Order];
            int firstDim = dimensions[0];

            while (true)
            {
                if (index[0] == firstDim)
                {
                    int j = 0;
                    while (index[j] == dimensions[j])
                    {
                        index[j++] = 0;
                        if (j == Order)
                        {
                            return true;
                        }
                        index[j]++;
                    }
                }

                if (!equals(Get(index), tensor.Get(index)))
                {
                    return false;
                }

                index[0]++;
            }
        }

        /// <summary>
        /// Returns whether this tensor is equal in value to a matrix. Returns <txt>false</txt>
        /// for non-second order tensors.
        /// </summary>
        /// <param name="matrix">The matrix to compare to.</param>
        /// <param name="equals">A function that returns true if its two arguments should be 
        /// considered equal. Used for elementwise comparisons.</param>
        /// <returns>Whether this tensor is equal in value to a matrix.</returns>
        public virtual bool Equals(Matrix<T> matrix, Func<T, T, bool> equals)
        {
            if (matrix is null)
            {
                return false;
            }

            if (Order != 2)
            {
                return false;
            }

            int[] dimensions = GetDimensions();
            if (dimensions[0] != matrix.Rows || dimensions[1] != matrix.Columns)
            {
                return false;
            }

            if (equals is null)
            {
                throw new ArgumentNullException(nameof(equals));
            }

            int[] index = new int[2];
            for (int r = 0; r < matrix.Rows; ++r)
            {
                index[0] = r;
                for (int c = 0; c < matrix.Columns; ++c)
                {
                    index[1] = c;
                    if (!equals(Get(index), matrix[r, c]))
                    {
                        return false;
                    }
                }
            }
            
            return true;
        }

        /// <summary>
        /// Returns whether this tensor is equal in value to a vector. Returns <txt>false</txt>
        /// for non-order 1 tensors.
        /// </summary>
        /// <param name="vector">The vector to compare to.</param>
        /// <param name="equals">A function that returns <txt>true</txt> if its two arguments 
        /// should be considered equal. Used for elementwise comparisons.</param>
        /// <returns>Whether this tensor is equal in value to a vector.</returns>
        public virtual bool Equals(Vector<T> vector, Func<T, T, bool> equals)
        {
            if (Order != 1)
            {
                return false;
            }

            if (vector is null)
            {
                throw new ArgumentNullException(nameof(vector));
            }

            int[] dimensions = GetDimensions();
            if (dimensions[0] != vector.LongDimension)
            {
                return false;
            }

            if (equals is null)
            {
                throw new ArgumentNullException(nameof(equals));
            }

            int dim = dimensions[0];
            for (int i = 0; i < dim; ++i)
            {
                if (!equals(Get(i), vector[i]))
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Returns whether this tensor is equal in value to a scalar. Returns <txt>false</txt>
        /// if this tensor is not of order 0.
        /// </summary>
        /// <param name="scalar">The scalar value to compare to.</param>
        /// <param name="equals">A function that returns <txt>true</txt> if its two arguments 
        /// should be considered equal.</param>
        /// <returns></returns>
        public virtual bool Equals(T scalar, Func<T, T, bool> equals)
        {
            if (Order != 0)
            {
                return false;
            }

            if (equals is null)
            {
                throw new ArgumentNullException(nameof(equals));
            }

            return equals(Get(), scalar);
        }

        public override bool Equals(object obj)
        {
            if (obj is null)
            {
                return false;
            }
            if (obj is Tensor<T> tensor)
            {
                return Equals(tensor, (u, v) => u.Equals(v));
            }
            if (obj is Matrix<T> matrix)
            {
                return Equals(matrix, (u, v) => u.Equals(v));
            }
            if (obj is Vector<T> vector)
            {
                return Equals(vector, (u, v) => u.Equals(v));
            }
            if (obj is T scalar)
            {
                return Equals(scalar, (u, v) => u.Equals(v));
            }
            return false;
        }

        /// <summary>
        /// Print the tensor to the Debug console given an element formatter.
        /// </summary>
        /// <param name="Format">The string formatting function</param>
        public void Print(Func<T, string> Format)
        {
            if (Order == 0)
            {
                Debug.WriteLine(Format(Get()));
            }
            else if (Order == 1)
            {
                int dimension = GetDimensions()[0];
                var s = new StringBuilder();
                for (int i = 0; i < dimension; ++i)
                {
                    s.Append(Format(Get(i)) + "\t");
                }
                Debug.WriteLine(s.ToString());
            }
            else if (Order == 2)
            {
                StringBuilder s = new StringBuilder();

                int[] dimensions = GetDimensions();
                int rows = dimensions[0], columns = dimensions[1], i, j;
                for (i = 0; i < rows; ++i)
                {
                    for (j = 0; j < columns; ++j)
                    {
                        s.Append(Format(Get(i, j)) + "\t");
                    }
                    s.AppendLine();
                }
                Debug.WriteLine(s.ToString());
            }
            else if (Order == 3)
            {
                StringBuilder s = new StringBuilder();
                int[] dimensions = GetDimensions();

                int m = dimensions[0], rows = dimensions[1], columns = dimensions[2], i, j, k;
                for (i = 0; i < m; ++i)
                {
                    for (j = 0; j < rows; ++j)
                    {
                        for (k = 0; k < columns; ++k)
                        {
                            s.Append(Format(Get(i, j, k)) + "\t");
                        }
                        s.AppendLine();
                    }
                    s.AppendLine();
                }
                Debug.WriteLine(s.ToString());
            }
            else
            {
                Debug.WriteLine($"Dense tensor [{string.Join(", ", GetDimensions())}]");
            }
        }

        /// <summary>
        /// Print the tensor's dimensions to the debug console.
        /// </summary>
        public void PrintDimensions()
        {
            Debug.WriteLine($"[{string.Join(", ", GetDimensions())}]");
        }

        /// <summary>
        /// Sets the value of the tensor at the specified index.
        /// The index array must be >= the tensor's order
        /// If the array length > the tensor's order, the first 'order'
        /// indices are used.
        /// </summary>
        /// <param name="value">The value to set the element to.</param>
        /// <param name="index">The index within the tensor to set the value</param>
        public abstract void Set(T value, params int[] index);

        /// <summary>
        /// Returns this tensor as a scalar. Only valid for tensors of order 0.
        /// </summary>
        /// <returns>This tensor represented as a scalar.</returns>
        public abstract T ToScalar();

        /// <summary>
        /// Returns this tensor as a vector. Only valid for tensors of order 1.
        /// </summary>
        /// <returns>This tensor represented as a vector.</returns>
        public abstract Vector<T> ToVector();

        /// <summary>
        /// Returns this tensor as a matrix. Only valid for tensors of order 2.
        /// </summary>
        /// <returns>This tensor represented as a matrix.</returns>
        public abstract Matrix<T> ToMatrix();
    }
}
