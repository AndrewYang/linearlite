﻿using System;

namespace LinearNet.Structs
{
    public readonly struct TensorEntry<T> : IComparable<TensorEntry<T>>
    {
        public readonly int[] Index { get; }
        public readonly T Value { get; }

        public readonly int Order { get { return Index.Length; } }
        public readonly bool IsDiagonal
        {
            get
            {
                int dim = Index.Length;
                if (dim < 1)
                {
                    return true;
                }

                int first = Index[0];
                for (int d = 1; d < dim; ++d)
                {
                    if (Index[d] != first)
                    {
                        return false;
                    }
                }
                return true;
            }
        }

        public TensorEntry(int[] index, T value)
        {
            Index = index;
            Value = value;
        }
        public TensorEntry(MatrixEntry<T> entry)
        {
            Index = new int[] { entry.RowIndex, entry.ColumnIndex };
            Value = entry.Value;
        }

        public int CompareTo(TensorEntry<T> other)
        {
            for (int i = 0; i < Index.Length; ++i)
            {
                int i1 = Index[i], i2 = other.Index[i];
                if (i1 < i2)
                {
                    return -1;
                }
                if (i1 > i2)
                {
                    return 1;
                }
            }
            return 0;
        }
    }
}
