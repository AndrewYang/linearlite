﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LinearNet.Structs
{
    internal static class ThrowHelper
    {
        /// <summary>
        /// Checks that each element in a collection is within [min, max)
        /// </summary>
        /// <param name="enumerable"></param>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <param name="name"></param>
        internal static void AssertCollectionInRange(IEnumerable<int> enumerable, int min, int max, string name)
        {
            foreach (int v in enumerable)
            {
                if (v < min || v >= max)
                {
                    throw new ArgumentOutOfRangeException(name);
                }
            }
        }
        internal static void AssertCollectionInRange<T>(IEnumerable<T> enumerable, T min, T max, string name) where T : IComparable<T>
        {
            foreach (T v in enumerable)
            {
                if (v.CompareTo(min) < 0 || v.CompareTo(max) > 0)
                {
                    throw new ArgumentOutOfRangeException(name);
                }
            }
        }
        internal static void AssertMonotonicIncreasing<T>(IList<T> collection, string name) where T : IComparable<T>
        {
            int len = collection.Count;
            for (int i = 1; i < len; ++i)
            {
                if (collection[i - 1].CompareTo(collection[i]) > 0)
                {
                    throw new ArgumentOutOfRangeException(name);
                }
            }
        }
        internal static void AssertStrictlyIncreasingWithinRange(int[] values, int[] indices, string name)
        {
            for (int i = 1; i < indices.Length; ++i)
            {
                int start = indices[i - 1],
                    end = indices[i];

                for (int j = start + 1; j < end; ++j)
                {
                    if (values[j] < values[j - 1])
                    {
                        throw new ArgumentOutOfRangeException(name, $"Value is not increasing in the range [{start}, {end})");
                    }
                }
            }
        }
    }
}
