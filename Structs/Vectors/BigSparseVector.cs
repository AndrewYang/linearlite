﻿using LinearNet.Global;
using LinearNet.Helpers;
using LinearNet.Providers;
using System;
using System.Collections.Generic;

namespace LinearNet.Structs
{
    /// <summary>
    /// <a>Implements an arbitrary-dimensional sparse vector over <txt>T</txt>.</a>
    /// <a>This class supports vectors of dimension up to <txt>long.MaxValue</txt> = 9,223,372,036,854,775,807.</a>
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <cat>linear-algebra</cat>
    public class BigSparseVector<T> : Vector<T>, ICloneable where T : new()
    {
        private readonly T _zero;
        private Dictionary<long, T> _values;

        internal Dictionary<long, T> Values { get { return _values; } }

        /// <summary>
        /// Gets or sets the element at the specified 32-bit integer index.
        /// </summary>
        /// <param name="index">The index of the vector.</param>
        /// <returns>The element at the specified index.</returns>
        public override T this[int index]
        {
            get
            {
                return _values.ContainsKey(index) ? _values[index] : default(T);
            }
            set
            {
                _values[index] = value;
            }
        }

        /// <summary>
        /// Gets or sets the element at the specified 64-bit integer index.
        /// </summary>
        /// <param name="index">The index of the vector.</param>
        /// <returns>The element at the specified index.</returns>
        public T this[long index]
        {
            get
            {
                return _values.ContainsKey(index) ? _values[index] : _zero;
            }
            set
            {
                if (!value.Equals(_zero))
                {
                    _values[index] = value;
                }
            }
        }

        public override int NonZeroCount => _values.Count;

        public override IEnumerable<VectorEntry<T>> NonZeroEntries
        {
            get
            {
                foreach (KeyValuePair<long, T> e in _values)
                {
                    if (e.Key > int.MaxValue)
                    {
                        throw new InvalidOperationException("Vector is too large to be enumerated over.");
                    }
                    yield return new VectorEntry<T>((int)e.Key, e.Value);
                }
            }
        } 

        #region Constructors

        /// <summary>
        /// Create a sparse vector of dimension <txt>dim</txt>. The components of the vector 
        /// will all hold the default value of type <txt>T</txt>.
        /// </summary>
        /// <param name="dim">The dimension of the vector.</param>
        public BigSparseVector(long dim) : base(dim)
        {
            if (dim <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(dim), ExceptionMessages.NonPositiveNotAllowed);
            }
            _values = new Dictionary<long, T>();
            _zero = new T();
        }

        /// <summary>
        /// Create a sparse vector using an array of its values.
        /// </summary>
        /// <param name="vector">An array containing all components of the vector.</param>
        public BigSparseVector(params T[] vector) : base(vector.Length)
        {
            if (vector == null)
            {
                throw new ArgumentNullException();
            }

            _values = new Dictionary<long, T>();
            _zero = new T();

            long len = vector.Length;
            for (long i = 0; i < len; ++i)
            {
                T value = vector[i];
                if (!value.Equals(_zero))
                {
                    _values[i] = value;
                }
            }
        }

        /// <summary>
        /// Create a sparse vector from a dense vector.
        /// </summary>
        /// <param name="vector">The dense vector.</param>
        public BigSparseVector(DenseVector<T> vector) : base(vector.Dimension)
        {
            if (vector is null)
            {
                throw new ArgumentException(nameof(vector));
            }

            _values = new Dictionary<long, T>();
            _zero = new T();

            T[] values = vector.Values;
            for (int i = 0; i < values.Length; ++i)
            {
                if (!_zero.Equals(values[i]))
                {
                    _values[i] = values[i];
                }
            }
        }

        public BigSparseVector(SparseVector<T> vector) : this(vector.Dimension)
        {
            if (vector is null)
            {
                throw new ArgumentNullException(nameof(vector));
            }

            int[] indices = vector.Indices;
            T[] values = vector.Values;

            int len = values.Length;
            for (int i = 0; i < len; ++i)
            {
                _values[indices[i]] = values[i];
            }
        }
        /// <summary>
        /// Create a sparse vector using its dimensionality and a dictionary of its non-zero values. All other 
        /// values are assumed to equal the default value of type <txt>T</txt>.
        /// </summary>
        /// <param name="dim">The dimension of the vector.</param>
        /// <param name="values">
        /// A dictionary of key-value pairs containing the index of each non-zero component, as well
        /// as its value.
        /// </param>
        public BigSparseVector(long dim, Dictionary<long, T> values) : base(dim)
        {
            if (dim <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(dim), ExceptionMessages.NonPositiveNotAllowed);
            }

            foreach (int key in values.Keys)
            {
                if (key < 0 || key >= dim)
                {
                    throw new ArgumentOutOfRangeException(nameof(values), "values contains an index that lies outside the range of the vector.");
                }
            }
            _values = values;
            _zero = new T();
        }

        #endregion

        #region Reflection/Meta

        public override double GetRandomAccessCost()
        {
            return 1;
        }

        public override double GetIterationCost()
        {
            return _values.Count;
        }

        #endregion

        public void Clear()
        {
            _values.Clear();
        }

        internal void AddInPlace(BigSparseVector<T> v, IProvider<T> provider)
        {
            if (v is null)
            {
                throw new ArgumentNullException(nameof(v));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            if (LongDimension != v.LongDimension)
            {
                throw new InvalidOperationException("Vector dimensions do not match.");
            }

            foreach (KeyValuePair<long, T> e in v._values)
            {
                if (_values.ContainsKey(e.Key))
                {
                    _values[e.Key] = provider.Add(_values[e.Key], e.Value);
                }
                else
                {
                    _values.Add(e.Key, e.Value);
                }
            }
        }
        internal BigSparseVector<T> Add(BigSparseVector<T> v, IProvider<T> provider)
        {
            BigSparseVector<T> sum = Copy();
            sum.AddInPlace(v, provider);
            return sum;
        }

        internal void SubtractInPlace(BigSparseVector<T> v, IProvider<T> provider)
        {
            if (v is null) throw new ArgumentNullException(nameof(v));
            if (provider is null) throw new ArgumentNullException(nameof(provider));

            if (v.LongDimension != LongDimension)
            {
                throw new InvalidOperationException("Vector dimensions don't match.");
            }

            foreach (KeyValuePair<long, T> e in v._values)
            {
                if (_values.ContainsKey(e.Key))
                {
                    _values[e.Key] = provider.Subtract(_values[e.Key], e.Value);
                }
                else
                {
                    _values[e.Key] = provider.Negate(e.Value);
                }
            }
        }
        internal BigSparseVector<T> Subtract(BigSparseVector<T> v, IProvider<T> provider)
        {
            if (v.LongDimension != LongDimension)
            {
                throw new InvalidOperationException("Vector dimensions don't match.");
            }

            BigSparseVector<T> diff = Copy();
            diff.SubtractInPlace(v, provider);
            return diff;
        }


        #region Public methods

        /// <summary>
        /// Returns the direct sum between this sparse vector and another, $u \oplus v$.
        /// </summary>
        /// <param name="v">The other sparse vector.</param>
        /// <returns>The direct sum.</returns>
        public BigSparseVector<T> DirectSum(BigSparseVector<T> v)
        {
            if (v is null)
            {
                throw new ArgumentNullException(nameof(v));
            }

            BigSparseVector<T> sum = Copy();
            sum.Append(v);
            return sum;
        }

        /// <summary>
        /// Appends a sparse vector to the end of this sparse vector.
        /// </summary>
        /// <param name="v">A sparse vector.</param>
        public void Append(BigSparseVector<T> v)
        {
            if (v is null)
            {
                throw new ArgumentNullException(nameof(v));
            }

            long dim = LongDimension;
            foreach (KeyValuePair<long, T> e in v._values)
            {
                _values.Add(dim + e.Key, e.Value);
            }
        }

        /// <summary>
        /// Returns the dot product between this sparse vector and another sparse vector, using 
        /// the specified sparse level-1 BLAS provider.
        /// </summary>
        /// <param name="v">A sparse vector.</param>
        /// <param name="provider">A sparse level-1 BLAS provider.</param>
        /// <returns>The dot product.</returns>
        public T Dot(BigSparseVector<T> v, ISparseBLAS1Provider<T> provider)
        {
            if (v is null)
            {
                throw new ArgumentNullException(nameof(v));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            if (v.LongDimension != LongDimension)
            {
                throw new InvalidOperationException("Vector dimensions don't match.");
            }
            if (!(provider is ISparseBLAS1<T> blas))
            {
                throw new NotSupportedException($"The type { typeof(T).ToString() } is not supported for this method");
            }

            return blas.SUMPROD(_values, v.Values, 0L, LongDimension);
        }

        /// <summary>
        /// Multiply this sparse vector by a scalar, overwriting this vector with the result.
        /// </summary>
        /// <param name="scalar">The scalar value</param>
        /// <param name="provider">The level-1 sparse BLAS provider.</param>
        public void MultiplyInPlace(T scalar, ISparseBLAS1Provider<T> provider)
        {
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            if (!(provider is ISparseBLAS1<T> blas))
            {
                throw new NotSupportedException($"The type { typeof(T).ToString() } is not supported for this method");
            }

            blas.SCAL(_values, scalar, 0, LongDimension);
        }

        /// <summary>
        /// Multiply this vector by a scalar, without changing this vector.
        /// </summary>
        /// <param name="scalar">A scalar.</param>
        /// <param name="provider">A sparse level-1 BLAS provider.</param>
        /// <returns>The vector-scalar product.</returns>
        public BigSparseVector<T> Multiply(T scalar, ISparseBLAS1Provider<T> provider)
        {
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            if (!(provider is ISparseBLAS1<T> blas))
            {
                throw new NotSupportedException($"The type { typeof(T).ToString() } is not supported for this method");
            }

            BigSparseVector<T> prod = Copy();
            prod.MultiplyInPlace(scalar, provider);
            return prod;
        }

        /// <summary>
        /// Returns a deep copy of the sparse vector.
        /// </summary>
        /// <returns>A copy of the sparse vector.</returns>
        public BigSparseVector<T> Copy()
        {
            Dictionary<long, T> cpy = new Dictionary<long, T>();
            foreach (KeyValuePair<long, T> pair in _values)
            {
                cpy[pair.Key] = pair.Value;
            }
            return new BigSparseVector<T>(Dimension, cpy);
        }

        public object Clone()
        {
            return Copy();
        }

        public override Vector<T> Select(int[] indices)
        {
            if (indices is null)
            {
                throw new ArgumentNullException(nameof(indices));
            }
            ThrowHelper.AssertCollectionInRange(indices, 0, LongDimension > int.MaxValue ? int.MaxValue : Dimension, nameof(indices));

            int nz = 0;
            // Since indices <= int.MaxValue, return a SparseVector
            int len = indices.Length;
            for (int i = 0; i < len; ++i)
            {
                if (_values.ContainsKey(indices[i]))
                {
                    ++nz;
                }
            }

            int[] sel_indices = new int[nz];
            T[] sel_values = new T[nz];

            for (int i = 0, k = 0; i < len; ++i)
            {
                if (_values.ContainsKey(indices[i]))
                {
                    sel_indices[k] = i;
                    sel_values[k++] = _values[indices[i]];
                }
            }
            return new SparseVector<T>(len, sel_indices, sel_values, true);
        }

        public override T[] ToArray()
        {
            T[] array = RectangularVector.Zeroes<T>(LongDimension);
            foreach (KeyValuePair<long, T> e in _values)
            {
                array[e.Key] = e.Value;
            }
            return array;
        }

        public override DenseVector<T> ToDenseVector()
        {
            return new DenseVector<T>(this);
        }

        public override SparseVector<T> ToSparseVector()
        {
            return new SparseVector<T>(this);
        }

        #endregion
    }

    /// <summary>
    /// A static class containing useful methods for creating special types of sparse vectors.
    /// </summary>
    /// <cat>linear-algebra</cat>
    public static class BigSparseVector
    {
        public static BigSparseVector<T> Random<T>(int dimension, int maxNonZeroElements) where T : new()
        {
            Func<T> random = DefaultGenerators.GetRandomGenerator<T>();

            BigSparseVector<T> vector = new BigSparseVector<T>(dimension);
            Dictionary<long, T> values = new Dictionary<long, T>();
            Random rand = new Random();

            for (int i = 0; i < maxNonZeroElements; ++i)
            {
                int r = rand.Next(dimension);
                values[r] = random();
            }
            return new BigSparseVector<T>(dimension, values);
        }
    }
}
