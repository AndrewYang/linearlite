﻿using LinearNet.Providers;
using System;
using System.Collections.Generic;
using LinearNet.Global;
using LinearNet.Structs.Vectors;
using LinearNet.Providers.Basic.Interfaces;

namespace LinearNet.Structs
{
    /// <summary>
    /// Implements a $n$-dimensional dense vector over the vector space <txt>T^$n$</txt>.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <cat>linear-algebra</cat>
    public class DenseVector<T> : Vector<T> where T : new()
    {
        private readonly T[] _values;

        /// <summary>
        /// Gets or sets the element at an index of the vector.
        /// </summary>
        /// <param name="index">The index of the element to get or set.</param>
        /// <returns>The element at that index.</returns>
        public override T this[int index]
        {
            get
            {
                return _values[index];
            }
            set 
            {
                _values[index] = value;
            } 
        }

        /// <summary>
        /// Gets the values of the vector as a single array.
        /// </summary>
        public T[] Values { get { return _values; } }

        public override int NonZeroCount => _values.Length;

        public override IEnumerable<VectorEntry<T>> NonZeroEntries
        {
            get
            {
                for (int i = 0; i < _values.Length; ++i)
                {
                    yield return new VectorEntry<T>(i, _values[i]);
                }
            }
        }


        #region Constructors

        /// <summary>
        /// Initialize a dense vector of the specified dimension.
        /// </summary>
        /// <param name="length">The dimension/length of this vector.</param>
        public DenseVector(int length) : base(VectorStorageType.DENSE, length)
        {
            _values = new T[length];
        }

        /// <summary>
        /// Initialize a dense vector using an array of values.
        /// </summary>
        /// <param name="values">An array containing the vector's components.</param>
        public DenseVector(T[] values) : base(VectorStorageType.DENSE, values.Length)
        {
            _values = values ?? throw new ArgumentNullException();
        }

        /// <summary>
        /// Create a dense vector from a big sparse vector.
        /// </summary>
        /// <param name="vector">A big sparse vector.</param>
        public DenseVector(BigSparseVector<T> vector) : base(VectorStorageType.DENSE, vector.Dimension)
        {
            if (vector is null)
            {
                throw new ArgumentNullException(nameof(vector));
            }

            _values = RectangularVector.Zeroes<T>(vector.Dimension);
            foreach (KeyValuePair<long, T> e in vector.Values)
            {
                _values[(int)e.Key] = e.Value;
            }
        }

        /// <summary>
        /// Create a dense vector from a sparse vector (the 'scatter' operation).
        /// </summary>
        /// <param name="vector"></param>
        public DenseVector(SparseVector<T> vector) : base(VectorStorageType.DENSE, vector.Dimension)
        {
            if (vector is null)
            {
                throw new ArgumentNullException(nameof(vector));
            }
            _values = RectangularVector.Zeroes<T>(vector.Dimension);

            T[] vals = vector.Values;
            int[] indices = vector.Indices;

            int len = vals.Length, i;
            for (i = 0; i < len; ++i)
            {
                _values[indices[i]] = vals[i];
            }
        }

        /// <summary>
        /// Copy constructor
        /// </summary>
        /// <param name="vector">The dense vector to copy.</param>
        public DenseVector(DenseVector<T> vector) : base(VectorStorageType.DENSE, vector.Dimension)
        {
            if (vector is null)
            {
                throw new ArgumentNullException(nameof(vector));
            }

            _values = vector._values.Copy();
        }

        #endregion


        /// <summary>
        /// Returns a deep copy of this vector.
        /// </summary>
        /// <returns>A copy of this vector.</returns>
        public DenseVector<T> Copy()
        {
            return new DenseVector<T>(_values.Copy());
        }

        public override Vector<T> Select(int[] indices)
        {
            if (indices is null)
            {
                throw new ArgumentNullException(nameof(indices));
            }

            int len = indices.Length, i;
            T[] selection = new T[len];
            for (i = 0; i < len; ++i)
            {
                selection[i] = _values[indices[i]];
            }
            return new DenseVector<T>(selection);
        }

        public override T[] ToArray()
        {
            return _values.Copy();
        }

        #region Reflection/Meta

        public override double GetRandomAccessCost()
        {
            return 1;
        }
        public override double GetIterationCost()
        {
            return _values.Length;
        }

        #endregion


        #region Operations 

        public override Vector<T> Add(Vector<T> vector)
        {
            if (vector.Type == VectorStorageType.DENSE)
            {
                return Add((DenseVector<T>)vector, ProviderFactory.GetDefaultBLAS1<T>());
            }
            else if (vector.Type == VectorStorageType.SPARSE)
            {
                return Add((SparseVector<T>)vector, ProviderFactory.GetDefaultProvider<T>());
            }
            else if (vector.Type == VectorStorageType.DOK)
            {
                return Add((BigSparseVector<T>)vector, ProviderFactory.GetDefaultProvider<T>());
            }
            throw new NotImplementedException();
        }

        /// <summary>
        /// Add a sparse vector to this vector, returning the sum as a new vector. Neither of the original 
        /// vectors are modified.
        /// </summary>
        /// <param name="vector"></param>
        /// <param name="provider"></param>
        /// <returns></returns>
        public DenseVector<T> Add(SparseVector<T> vector, IProvider<T> provider)
        {
            if (vector is null)
            {
                throw new ArgumentNullException(nameof(vector));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            if (vector.Dimension != Dimension)
            {
                throw new InvalidOperationException(ExceptionMessages.VectorSizeMismatch);
            }

            int[] vi = vector.Indices;
            T[] values = _values.Copy(),
                v = vector.Values;

            int nnz = vi.Length;
            for (int i = 0; i < nnz; ++i)
            {
                int index = vi[i];
                values[index] = provider.Add(values[index], v[i]);
            }
            return new DenseVector<T>(values);
        }

        public void AddInPlace(SparseVector<T> vector, IProvider<T> provider)
        {
            if (vector is null)
            {
                throw new ArgumentNullException(nameof(vector));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            if (vector.Dimension != Dimension)
            {
                throw new InvalidOperationException(ExceptionMessages.VectorSizeMismatch);
            }

            int[] vi = vector.Indices;
            T[] v = vector.Values;

            int nnz = vi.Length;
            for (int i = 0; i < nnz; ++i)
            {
                int index = vi[i];
                _values[index] = provider.Add(_values[index], v[i]);
            }
        }

        /// <summary>
        /// Returns the sum of this vector with another, using the specified level-1 BLAS implementation.
        /// </summary>
        /// <param name="vector">The other vector to add.</param>
        /// <param name="blas1">A level-1 BLAS implementation.</param>
        /// <returns>The vector sum.</returns>
        public DenseVector<T> Add(DenseVector<T> vector, IDenseBLAS1<T> blas1)
        {
            if (vector is null)
            {
                throw new ArgumentNullException(nameof(vector));
            }

            if (blas1 is null)
            {
                throw new ArgumentNullException(nameof(blas1));
            }

            if (vector.Dimension != Dimension)
            {
                throw new InvalidOperationException(ExceptionMessages.VectorSizeMismatch);
            }

            if (!(blas1 is IDenseBLAS1<T> blas))
            {
                throw new NotSupportedException(ExceptionMessages.TypeNotSupported);
            }

            T[] sum = new T[Dimension];
            blas.ADD(_values, vector._values, sum, 0, Dimension);
            return new DenseVector<T>(sum);
        }

        /// <summary>
        /// Calculates the sum of this vector and another, overwriting this vector with the sum.
        /// </summary>
        /// <param name="vector">The vector to increment by.</param>
        /// <param name="blas1">A level-1 BLAS implementation.</param>
        public void AddInPlace(DenseVector<T> vector, IDenseBLAS1Provider<T> blas1)
        {
            if (vector is null)
            {
                throw new ArgumentNullException(nameof(vector));
            }

            if (blas1 is null)
            {
                throw new ArgumentNullException(nameof(blas1));
            }

            if (vector.Dimension != Dimension)
            {
                throw new InvalidOperationException(ExceptionMessages.VectorSizeMismatch);
            }

            if (!(blas1 is IDenseBLAS1<T> blas))
            {
                throw new NotSupportedException(ExceptionMessages.TypeNotSupported);
            }

            blas.ADD(_values, vector._values, _values, 0, Dimension);
        }

        public DenseVector<T> Add(BigSparseVector<T> vector, IProvider<T> provider)
        {
            if (vector is null)
            {
                throw new ArgumentNullException(nameof(vector));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            if (LongDimension != vector.LongDimension)
            {
                throw new InvalidOperationException(ExceptionMessages.VectorSizeMismatch);
            }

            T[] values = _values.Copy();
            Dictionary<long, T> dict = vector.Values;
            foreach (KeyValuePair<long, T> e in dict)
            {
                values[e.Key] = provider.Add(values[e.Key], e.Value);
            }
            return new DenseVector<T>(values);
        }

        /// <summary>
        /// Clears this vector (sets all components to zero).
        /// </summary>
        public void Clear()
        {
            T zero = new T();
            if (zero.Equals(default))
            {
                Array.Clear(_values, 0, Dimension);
            }
            else
            {
                for (int i = 0, len = _values.Length; i < len; ++i)
                {
                    _values[i] = zero;
                }
            }
        }

        /// <summary>
        /// Given two dense vectors $u\in\mathbb{F}^m$ and $v\in\mathbb{F}^n$, returns the (tensorial) direct sum $u \oplus v\in\mathbb{F}^{m + n}$.
        /// </summary>
        /// <param name="u">The first vector.</param>
        /// <param name="vector">The second vector.</param>
        /// <returns>The direct sum of the two vectors.</returns>
        public DenseVector<T> DirectSum(DenseVector<T> vector)
        {
            if (vector is null)
            {
                throw new ArgumentNullException(nameof(vector));
            }
            return new DenseVector<T>(_values.DirectSum(vector._values));
        }

        public Vector<T> DirectSum(SparseVector<T> vector)
        {
            if (vector is null)
            {
                throw new ArgumentNullException(nameof(vector));
            }
            
            // If the second vector is longer than this vector, return a sparse vector
            if (Dimension < vector.Dimension)
            {
                int[] vect_indices = vector.Indices;
                T[] vect_values = vector.Values;

                int nnz1 = Dimension, nnz2 = vect_indices.Length;
                int[] vi = new int[nnz1 + nnz2];
                for (int i = 0; i < nnz1; ++i) vi[i] = i;
                for (int i = 0; i < nnz2; ++i) vi[i + nnz1] = vect_indices[i] + nnz1; 

                T[] values = new T[vi.Length];
                Array.Copy(_values, values, nnz1);
                Array.Copy(vect_values, 0, values, nnz1, nnz2);

                return new SparseVector<T>(Dimension + vector.Dimension, vi, values, true);
            }

            // Otherwise, return a dense vector
            else
            {
                T[] values = new T[Dimension + vector.Dimension];
                Array.Copy(_values, values, Dimension);

                // Set to zero
                T zero = new T();
                for (int i = Dimension, end = values.Length; i < end; ++i)
                {
                    values[i] = zero;
                }
                // Set with sparse values
                int[] vi = vector.Indices;
                T[] v = vector.Values;
                for (int i = 0, end = v.Length; i < end; ++i)
                {
                    int index = vi[i];
                    values[vi[i] + Dimension] = v[i];
                }

                return new DenseVector<T>(values);
            }
        }

        public Vector<T> DirectSum(BigSparseVector<T> vector)
        {
            if (vector is null) throw new ArgumentNullException(nameof(vector));

            // If the result dimension can fit inside a sparse vector, use a sparse vector
            long dim = LongDimension + vector.LongDimension;
            if (dim <= int.MaxValue)
            {
                int dim1 = Dimension, dim2 = vector.Values.Count;
                int[] vi = new int[dim1 + dim2];
                T[] v = new T[vi.Length];

                int i = 0;
                for (; i < dim1; ++i)
                {
                    vi[i] = i;
                    v[i] = _values[i];
                }
                foreach (KeyValuePair<long, T> e in vector.Values)
                {
                    vi[i] = dim1 + (int)e.Key;
                    v[i] = e.Value;
                }

                // Sort the second part
                Array.Sort(vi, v, dim1, dim2);

                return new SparseVector<T>(dim1 + dim2, vi, v);
            }
            else
            {
                // Otherwise - use the BigSparseVector storage format
                Dictionary<long, T> values = new Dictionary<long, T>(Dimension + vector.Values.Count);
                for (int i = 0; i < Dimension; ++i)
                {
                    values[i] = _values[i];
                }
                foreach (KeyValuePair<long, T> e in vector.Values)
                {
                    values[e.Key + Dimension] = e.Value;
                }
                return new BigSparseVector<T>(Dimension + vector.Dimension, values);
            }
        }

        public override Vector<T> DirectSum(Vector<T> vector)
        {
            if (vector is null) throw new ArgumentNullException(nameof(vector));

            if (vector.Type == VectorStorageType.DENSE)
            {
                return DirectSum(vector as DenseVector<T>);
            }
            else if (vector.Type == VectorStorageType.SPARSE)
            {
                return DirectSum(vector as SparseVector<T>);
            }
            else if (vector.Type == VectorStorageType.DOK)
            {
                return DirectSum(vector as BigSparseVector<T>);
            }
            throw new NotImplementedException();
        }

        /// <summary>
        /// Returns the dot product between this vector and another, using the specified level-1 BLAS implementation.
        /// </summary>
        /// <param name="vector">A dense vector.</param>
        /// <param name="provider">A level-1 BLAS implementation.</param>
        /// <returns>The dot product.</returns>
        public T Dot(DenseVector<T> vector, IDenseBLAS1<T> provider)
        {
            if (vector == null)
            {
                throw new ArgumentNullException(nameof(vector));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            if (Dimension != vector.Dimension)
            {
                throw new InvalidOperationException(ExceptionMessages.VectorSizeMismatch);
            }
            return provider.DOT(_values, vector._values, 0, Dimension);
        }

        /// <summary>
        /// Returns the dot product between this vector and a sparse real vector, using the specified provider.
        /// </summary>
        /// <note>This is only for real providers</note>
        /// <param name="vector">The sparse vector to take the dot product with.</param>
        /// <param name="provider"></param>
        /// <returns></returns>
        public T Dot(SparseVector<T> vector, IRealProvider<T> provider)
        {
            if (vector is null)
            {
                throw new ArgumentNullException(nameof(vector));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            if (Dimension != vector.Dimension)
            {
                throw new InvalidOperationException(ExceptionMessages.VectorSizeMismatch);
            }

            T dot = provider.Zero;
            int[] vi = vector.Indices;
            T[] v = vector.Values;
            int nnz = v.Length;
            for (int i = 0; i < nnz; ++i)
            {
                dot = provider.Add(dot, provider.Multiply(v[i], _values[vi[i]]));
            }
            return dot;
        }

        public T Dot(BigSparseVector<T> vector, IRealProvider<T> provider)
        {
            if (vector is null)
            {
                throw new ArgumentNullException(nameof(vector));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            if (vector.LongDimension != LongDimension)
            {
                throw new InvalidOperationException(ExceptionMessages.VectorSizeMismatch);
            }

            T dot = provider.Zero;
            foreach (KeyValuePair<long, T> e in vector.Values)
            {
                dot = provider.Add(dot, provider.Multiply(e.Value, _values[e.Key]));
            }
            return dot;
        }

        public bool Equals(DenseVector<T> vector, Func<T, T, bool> equals)
        {
            if (vector is null)
            {
                return false;
            }
            if (equals is null)
            {
                throw new ArgumentNullException(nameof(equals));
            }

            if (Dimension != vector.Dimension)
            {
                return false;
            }

            for (int i = 0; i < Dimension; ++i)
            {
                if (!equals(_values[i], vector._values[i]))
                {
                    return false;
                }
            }

            return true; 
        }

        /// <summary>
        /// Apply a function independently to each element of this vector, storing the results in a new 
        /// dense vector of the same dimension.
        /// </summary>
        /// <param name="predicate">The function to apply.</param>
        /// <returns>A vector of values.</returns>
        public DenseVector<U> Elementwise<U>(Func<T, U> predicate) where U : new()
        {
            if (predicate is null)
            {
                throw new ArgumentNullException(nameof(predicate));
            }
            return new DenseVector<U>(_values.Elementwise(predicate));
        }

        /// <summary>
        /// Apply a two-argument function pointwise to each pair of components in this vector and another vector respectively,
        /// returning a new vector containing the outputs of the function. Neither of the original vector is altered.
        /// </summary>
        /// <typeparam name="U"></typeparam>
        /// <typeparam name="V"></typeparam>
        /// <param name="vector">The other vector.</param>
        /// <param name="binaryPredicate">A two-argument function $f:$ <txt>T</txt> $\times$ <txt>U</txt> $\to$ <txt>V</txt>.</param>
        /// <returns>A vector of output values.</returns>
        public DenseVector<V> Pointwise<U, V>(DenseVector<U> vector, Func<T, U, V> binaryPredicate) where U : new() where V : new()
        {
            if (vector is null)
            {
                throw new ArgumentNullException(nameof(vector));
            }
            if (vector.Dimension != Dimension)
            {
                throw new InvalidOperationException(ExceptionMessages.VectorSizeMismatch);
            }
            if (binaryPredicate is null)
            {
                throw new ArgumentNullException(nameof(binaryPredicate));
            }

            int dim = Dimension, i;
            V[] result = new V[dim];
            for (i = 0; i < dim; ++i)
            {
                result[i] = binaryPredicate(_values[i], vector._values[i]);
            }
            return new DenseVector<V>(result);
        }

        /// <summary>
        /// Multiply the vector by a scalar without changing the original vector,
        /// using the specified level-1 BLAS implementation.
        /// </summary>
        /// <param name="scalar">The scalar value to multiply by.</param>
        /// <param name="blas1">The level-1 BLAS implementation.</param>
        public DenseVector<T> Multiply(T scalar, IDenseBLAS1Provider<T> blas1)
        {
            DenseVector<T> copy = Copy();
            copy.MultiplyInPlace(scalar, blas1);
            return copy;
        }

        /// <summary>
        /// Returns the transpose of this vector postmultiplied by a matrix, i.e. $v^TA$.
        /// </summary>
        /// <param name="matrix">The matrix to postmultiplied by.</param>
        /// <param name="provider">The provider used to elementwise operations.</param>
        /// <returns>The vector-matrix product.</returns>
        public DenseVector<T> Multiply(Matrix<T> matrix, IProvider<T> provider)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            return matrix.TransposeAndMultiply(this, provider);
        }

        /// <summary>
        /// Multiply this vector by a scalar using the specified level-1 BLAS implementation, and overwrite this vector with the result.
        /// </summary>
        /// <param name="scalar">The scalar value to multiply by.</param>
        /// <param name="blas1">The level-1 BLAS implementation.</param>
        public void MultiplyInPlace(T scalar, IDenseBLAS1Provider<T> blas1)
        {
            if (blas1 is null)
            {
                throw new ArgumentNullException(nameof(blas1));
            }

            if (!(blas1 is IDenseBLAS1<T> blas))
            {
                throw new NotSupportedException(ExceptionMessages.TypeNotSupported);
            }

            blas.SCAL(_values, scalar, 0, Dimension);
        }

        public DenseVector<T> Negate(IDenseBLAS1Provider<T> blas1)
        {
            DenseVector<T> copy = Copy();
            copy.NegateInPlace(blas1);
            return copy;
        }

        public void NegateInPlace(IDenseBLAS1Provider<T> blas1)
        {
            if (blas1 is null)
            {
                throw new ArgumentNullException(nameof(blas1));
            }
            if (!(blas1 is IDenseBLAS1<T> b))
            {
                throw new NotSupportedException(ExceptionMessages.TypeNotSupported);
            }
            T neg_one = b.Provider.Negate(b.Provider.One);
            b.SCAL(_values, neg_one, 0, Dimension);
        }

        /// <summary>
        /// Calculates the outer product between this vector and another dense vector, returning a dense matrix.
        /// </summary>
        /// <param name="vector"></param>
        /// <param name="blas1"></param>
        /// <returns></returns>
        public DenseMatrix<T> OuterProduct(DenseVector<T> vector, IDenseBLAS1Provider<T> blas1)
        {
            if (vector is null)
            {
                throw new ArgumentNullException(nameof(vector));
            }
            if (blas1 is null)
            {
                throw new ArgumentNullException(nameof(blas1));
            }
            if (!(blas1 is IDenseBLAS1<T> b))
            {
                throw new NotSupportedException(ExceptionMessages.TypeNotSupported);
            }

            int rows = Dimension, cols = vector.Dimension;
            T[][] values = MatrixInternalExtensions.JMatrix<T>(rows, cols);
            for (int r = 0; r < rows; ++r)
            {
                T[] row = values[r];
                Array.Copy(vector._values, row, cols);
                b.SCAL(row, _values[r], 0, cols);
            }
            return new DenseMatrix<T>(rows, cols, values);
        }

        /// <summary>
        /// Returns this vector subtract another, using the specified level-1 BLAS implementation.
        /// </summary>
        /// <param name="blas1">The vector to subtract.</param>
        /// <param name="blas1">A level-1 BLAS implementation.</param>
        /// <returns>The vector difference.</returns>
        public DenseVector<T> Subtract(DenseVector<T> vector, IDenseBLAS1Provider<T> blas1)
        {
            if (vector is null)
            {
                throw new ArgumentNullException(nameof(vector));
            }

            if (blas1 is null)
            {
                throw new ArgumentNullException(nameof(blas1));
            }

            if (vector.Dimension != Dimension)
            {
                throw new InvalidOperationException(ExceptionMessages.VectorSizeMismatch);
            }

            if (!(blas1 is IDenseBLAS1<T> blas))
            {
                throw new NotSupportedException(ExceptionMessages.TypeNotSupported);
            }

            T[] diff = new T[Dimension];
            blas.SUB(_values, vector._values, diff, 0, Dimension);
            return new DenseVector<T>(diff);
        }

        /// <summary>
        /// Subtract a vector from this vector, and overwrite this vector with the result, using the specified level-1 BLAS implementation.
        /// </summary>
        /// <param name="vector">The vector to subtract.</param>
        /// <param name="blas1">A level-1 BLAS implementation.</param>
        public void SubtractInPlace(DenseVector<T> vector, IDenseBLAS1Provider<T> blas1)
        {
            if (vector is null)
            {
                throw new ArgumentNullException(nameof(vector));
            }

            if (blas1 is null)
            {
                throw new ArgumentNullException(nameof(blas1));
            }

            if (vector.Dimension != Dimension)
            {
                throw new InvalidOperationException(ExceptionMessages.VectorSizeMismatch);
            }

            if (!(blas1 is IDenseBLAS1<T> blas))
            {
                throw new NotSupportedException(ExceptionMessages.TypeNotSupported);
            }

            blas.SUB(_values, vector._values, _values, 0, Dimension);
        }

        /// <summary>
        /// Returns the tensor product between this vector $v$ and a dense matrix $A$, i.e. $v \otimes A$.
        /// </summary>
        /// <param name="matrix"></param>
        /// <param name="blas1"></param>
        /// <returns></returns>
        public DenseTensor<T> TensorProduct(DenseMatrix<T> matrix, IDenseBLAS1Provider<T> blas1)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            if (blas1 is null)
            {
                throw new ArgumentNullException(nameof(blas1));
            }
            if (!(blas1 is IDenseBLAS1<T> b))
            {
                throw new NotSupportedException(ExceptionMessages.TypeNotSupported);
            }

            T[][] matrix_values = matrix.Values;
            T[] values = new T[(long)Dimension * matrix.Rows * matrix.Columns];

            if (values.LongLength > int.MaxValue)
            {
                IProvider<T> provider = b.Provider;
                long offset = 0;
                for (int d = 0; d < Dimension; ++d)
                {
                    T v = _values[d];
                    for (int r = 0; r < matrix.Rows; ++r)
                    {
                        T[] row = matrix_values[r];
                        for (int c = 0; c < matrix.Columns; ++c)
                        {
                            values[offset++] = provider.Multiply(v, row[c]);
                        }
                    }
                }
            }
            else
            {
                int cols = matrix.Columns, offset = 0;
                for (int d = 0; d < Dimension; ++d)
                {
                    T v = _values[d];
                    for (int r = 0; r < matrix.Rows; ++r)
                    {
                        Array.Copy(matrix_values[r], 0, values, offset, cols);
                        b.SCAL(values, v, offset, offset + cols);
                        offset += cols;
                    }
                }
            }

            int[] dimensions = { Dimension, matrix.Rows, matrix.Columns };
            return new DenseTensor<T>(dimensions, values);
        }

        /// <summary>
        /// Returns the tensor product between this vector $v$ and a dense tensor $T$, i.e. $v \otimes T$.
        /// </summary>
        /// <param name="tensor"></param>
        /// <param name="blas1"></param>
        /// <returns></returns>
        public DenseTensor<T> TensorProduct(DenseTensor<T> tensor, IDenseBLAS1Provider<T> blas1)
        {
            if (tensor is null)
            {
                throw new ArgumentNullException(nameof(tensor));
            }
            if (blas1 is null)
            {
                throw new ArgumentNullException(nameof(blas1));
            }
            if (!(blas1 is IDenseBLAS1<T> b))
            {
                throw new NotSupportedException(ExceptionMessages.TypeNotSupported);
            }

            // Although it is possible to use the tensor \otimes vector method with index = 0, 
            // this implementation is more efficient for this particular case

            T[] tensor_values = tensor.Values;

            long tlen = tensor_values.LongLength, 
                len = Dimension * tlen;

            T[] values = new T[len];
            if (len > int.MaxValue)
            {
                IProvider<T> provider = b.Provider;
                long k = 0;
                for (int d = 0; d < Dimension; ++d)
                {
                    T v = _values[d];
                    for (long t = 0; t < tlen; ++t)
                    {
                        values[k++] = provider.Multiply(v, tensor_values[t]);
                    }
                }
            }
            else
            {
                int int_tlen = (int)tlen;
                for (int d = 0, k = 0; d < Dimension; ++d)
                {
                    Array.Copy(tensor_values, 0, values, k, int_tlen);
                    b.SCAL(values, _values[d], k, k + int_tlen);
                    k += int_tlen;
                }
            }

            int[] dimensions = new int[tensor.Order + 1];
            dimensions[0] = Dimension;
            Array.Copy(tensor.GetDimensions(), 0, dimensions, 1, tensor.Order);

            return new DenseTensor<T>(dimensions, values);
        }

        public override DenseVector<T> ToDenseVector()
        {
            return new DenseVector<T>(this);
        }

        public override SparseVector<T> ToSparseVector()
        {
            return new SparseVector<T>(this);
        }

        #endregion


        #region Operators

        /// <summary>
        /// Returns the sum of two vectors.
        /// </summary>
        /// <param name="u">The left vector.</param>
        /// <param name="v">The right vector.</param>
        /// <returns>The vector sum.</returns>
        public static DenseVector<T> operator +(DenseVector<T> u, DenseVector<T> v)
        {
            if (u is null)
            {
                throw new ArgumentNullException(nameof(u));
            }
            return u.Add(v, ProviderFactory.GetDefaultBLAS1<T>());
        }

        /// <summary>
        /// Returns the vector difference between two vectors.
        /// </summary>
        /// <param name="u">The left vector.</param>
        /// <param name="v">The right vector.</param>
        /// <returns>The left vector subtract the right vector.</returns>
        public static DenseVector<T> operator -(DenseVector<T> u, DenseVector<T> v)
        {
            if (u is null)
            {
                throw new ArgumentNullException(nameof(u));
            }
            return u.Subtract(v, ProviderFactory.GetDefaultBLAS1<T>());
        }

        /// <summary>
        /// Returns the opposite of a vector.
        /// </summary>
        /// <param name="u">A dense vector.</param>
        /// <returns>The negation of the vector.</returns>
        public static DenseVector<T> operator -(DenseVector<T> u)
        {
            if (u is null)
            {
                throw new ArgumentNullException(nameof(u));
            }
            return u.Negate(ProviderFactory.GetDefaultBLAS1<T>());
        }

        /// <summary>
        /// Returns the product between a vector and a scalar.
        /// </summary>
        /// <param name="u">The vector.</param>
        /// <param name="scalar">The scalar.</param>
        /// <returns>The vector-scalar product.</returns>
        public static DenseVector<T> operator *(DenseVector<T> u, T scalar)
        {
            if (u is null)
            {
                throw new ArgumentNullException(nameof(u));
            }
            return u.Multiply(scalar, ProviderFactory.GetDefaultBLAS1<T>());
        }

        /// <summary>
        /// Returns the product between a vector and scalar.
        /// </summary>
        /// <param name="scalar">The scalar.</param>
        /// <param name="u">The vector.</param>
        /// <returns>The vector-scalar product.</returns>
        public static DenseVector<T> operator *(T scalar, DenseVector<T> u) => u * scalar;

        /// <summary>
        /// Returns the product between a vector $v$ and a matrix $A$ of compatible dimension, i.e. $v^TA$.
        /// </summary>
        /// <param name="v">The vector $v$.</param>
        /// <param name="A">The matrix $A$.</param>
        /// <returns>The vector-matrix product.</returns>
        public static DenseVector<T> operator *(DenseVector<T> v, Matrix<T> A)
        {
            if (v is null)
            {
                throw new ArgumentNullException(nameof(v));
            }
            return v.Multiply(A, ProviderFactory.GetDefaultProvider<T>());
        }
        
        #endregion


        #region Casts

        /// <summary>
        /// Cast an array into a dense vector.
        /// </summary>
        /// <param name="vector">An array representing a vector.</param>
        public static explicit operator DenseVector<T>(T[] vector) => new DenseVector<T>(vector);

        /// <summary>
        /// Cast a <txt>DenseVector</txt> into an array.
        /// </summary>
        /// <param name="vector">A vector.</param>
        public static explicit operator T[](DenseVector<T> vector)
        {
            if (vector is null)
            {
                throw new ArgumentNullException(nameof(vector));
            }

            return vector._values;
        }

        #endregion
    }

}
