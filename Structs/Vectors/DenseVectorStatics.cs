﻿using LinearNet.Helpers;
using LinearNet.Statistics;
using System;

namespace LinearNet.Structs
{
    /// <summary>
    /// A static class with useful functions for creating special types of dense vectors.
    /// </summary>
    /// <cat>linear-algebra</cat>
    public static class DenseVector
    {
        /// <summary>
        /// Create a <txt>dim</txt>-dimensional vector where each entry is equal to <txt>value</txt>.
        /// <!--inputs-->
        /// <!--returns-->
        /// 
        /// <h4>Examples</h4>
        /// <pre><code class="cs">
        /// // Create a 10-dimensional vector
        /// DenseVector&lt;double&gt; ones = DenseVector.Repeat&lt;double&gt;(10, 1.0); 
        /// 
        /// // Create a sparse 5-dimensional vector with each entry equal to -3 + 2i
        /// SparseVector&lt;Complex&gt; vector = SparseVector.Repeat&lt;Complex&gt;(5, new Complex(-3, 2));
        /// 
        /// int[] intVector = RectangularVector.Repeat&lt;T&gt;(10, 1); 
        /// </code></pre>
        /// </summary>
        /// <param name="dim">The dimensionality of the created vector.</param>
        /// <param name="value">The value of each entry in the created vector.</param>
        /// <returns></returns>
        public static DenseVector<T> Repeat<T>(int dim, T value) where T : new()
        {
            if (dim < 0)
            {
                throw new ArgumentOutOfRangeException();
            }

            T[] vector = new T[dim];
            for (int i = 0; i < dim; ++i)
            {
                vector[i] = value;
            }
            return new DenseVector<T>(vector);
        }

        /// <summary>
        /// Create a <txt>dim</txt>-dimensional vector of 0's.
        /// <!--inputs-->
        /// <!--returns-->
        /// </summary>
        /// <param name="dim">The dimensionality of the created vector.</param>
        /// <returns>A <txt>dim</txt>-dimensional vector of zeroes.</returns>
        public static DenseVector<T> Zeroes<T>(int dim) where T : new() => Repeat(dim, DefaultGenerators.GetZero<T>());

        /// <summary>
        /// Create a <txt>dim</txt>-dimensional vector of 1's (i.e. every component of the vector is 1).
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dim">The dimensionality of the vector.</param>
        /// <returns>A vector of ones.</returns>
        public static DenseVector<T> Ones<T>(int dim) where T : new() => Repeat(dim, DefaultGenerators.GetOne<T>());

        /// <summary>
        /// Create a random <txt>dim</txt>-dimensional vector where each element is sampled randomly from 
        /// a random element generator.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dim">The dimensionality of the vector.</param>
        /// <param name="random">
        /// The random element generator.
        /// If null, it will default to a default random generator of type <txt>T</txt>.
        /// </param>
        /// <returns>A random vector.</returns>
        public static DenseVector<T> Random<T>(int dim, Func<T> random = null) where T : new()
        {
            if (random == null)
            {
                random = DefaultGenerators.GetRandomGenerator<T>();
            }

            T[] vect = new T[dim];
            for (int i = 0; i < dim; ++i)
            {
                vect[i] = random();
            }
            return new DenseVector<T>(vect);
        }

        /// <summary>
        /// Create a random <txt>dim</txt>-dimensional vector where each element is sampled independently from a 
        /// univariate distribution.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dim">The dimensionality of the vector.</param>
        /// <param name="distribution">The univariate distribution from which to sample the elements of the vector.</param>
        /// <returns>A random vector.</returns>
        public static DenseVector<T> Random<T>(int dim, IUnivariateDistribution<T> distribution) where T : IComparable<T>, new()
        {
            Random random = new Random();
            T sampler() => distribution.Sample(random);
            return Random(dim, sampler);
        }
    }
}
