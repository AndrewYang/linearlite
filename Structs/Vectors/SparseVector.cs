﻿using LinearNet.Global;
using LinearNet.Matrices.Sparse.Decompositions;
using LinearNet.Providers;
using LinearNet.Providers.Basic.Interfaces;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

namespace LinearNet.Structs
{
    /// <summary>
    /// Implements a sparse vector of up to dimension $2^31 - 1$ (<txt>int.MaxValue</txt>).
    /// 
    /// <para>
    /// The values of this sparse vector implementation are kept in pairs of (index, value)
    /// tupes, ordered by index. Value access is $O(log(n))$, and most other operations are 
    /// $O(n)$, where $n$ is the number of non-zero elements of the vector (nnz).
    /// </para>
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class SparseVector<T> : Vector<T> where T : new()
    {
        private int[] _indices;
        private T[] _values;
        private readonly T _zero;

        public override T this[int index] 
        { 
            get 
            {
                if (index < 0 || index >= Dimension)
                {
                    throw new ArgumentOutOfRangeException(nameof(index));
                }

                int i = FindIndexUnsafe(index, 0, _indices.Length);
                if (i < 0)
                {
                    return _zero;
                }
                return _values[i];
            }
            set 
            {
                if (index < 0 || index >= Dimension)
                {
                    throw new ArgumentOutOfRangeException(nameof(index));
                }

                // Fast case - O(log(n)), if we are replacing an index that already exists
                int i = FindIndexUnsafe(index, 0, _indices.Length);
                if (i >= 0)
                {
                    _values[i] = value;
                    return;
                }

                // Slow case - need to shift the entire vector, which is O(n) on average anyway
                // so we don't bother speeding this up
                for (int j = 0; j < _indices.Length; ++j)
                {
                    if (_indices[j] > index)
                    {
                        _indices = _indices.InsertAt(j, index);
                        _values = _values.InsertAt(j, value);
                        return;
                    }
                }

                // Insert at end
                _indices = _indices.InsertAt(_indices.Length, index);
                _values = _values.InsertAt(_values.Length, value);
            } 
        }

        internal int[] Indices { get { return _indices; } set { _indices = value; } }
        internal T[] Values { get { return _values; } set { _values = value; } }

        public override int NonZeroCount => _values.Length;

        public override IEnumerable<VectorEntry<T>> NonZeroEntries
        {
            get
            {
                for (int i = 0; i < _values.Length; ++i)
                {
                    yield return new VectorEntry<T>(_indices[i], _values[i]);
                }
            }
        }

        #region Constructor

        /// <summary>
        /// Create a sparse vector from a <txt>BigSparseVector</txt>.
        /// </summary>
        /// <param name="vector">The sparse vector.</param>
        public SparseVector(BigSparseVector<T> vector) : base(vector.Dimension)
        {
            if (vector is null)
            {
                throw new ArgumentNullException(nameof(vector));
            }
            if (vector.LongDimension > int.MaxValue)
            {
                throw new ArgumentOutOfRangeException("The vector's dimension is too large to be stored in a SparseVector<T>.");
            }

            int count = vector.Values.Count;
            _indices = new int[count];

            int i = 0;
            foreach (long key in vector.Values.Keys)
            {
                _indices[i++] = (int)key;
            }

            Array.Sort(_indices);

            _values = new T[count];
            for (i = 0; i < count; ++i)
            {
                _values[i] = vector.Values[_indices[i]];
            }
            _zero = new T();
        }

        /// <summary>
        /// Create a sparse vector from a 1-D array representing a dense vector.
        /// </summary>
        /// <param name="denseVector">A dense vector.</param>
        public SparseVector(T[] denseVector) : base(denseVector.Length)
        {
            if (denseVector is null)
            {
                throw new ArgumentNullException(nameof(denseVector));
            }

            int len = denseVector.Length;
            _indices = new int[len];
            for (int i = 0; i < len; ++i)
            {
                _indices[i] = i;
            }
            _values = denseVector.Copy();
            _zero = new T();
        }

        /// <summary>
        /// Create a sparse vector from a dense vector.
        /// </summary>
        /// <param name="denseVector">A dense vector.</param>
        public SparseVector(DenseVector<T> denseVector) : this(denseVector?.Values)
        {
            _zero = new T();
        }

        /// <summary>
        /// Create a sparse vector by applying a filter to a dense vector. 
        /// Only entries for which the predicate returns true will be included in the sparse vector.
        /// </summary>
        /// <param name="denseVector"></param>
        /// <param name="keep"></param>
        public SparseVector(DenseVector<T> denseVector, Predicate<T> keep) : base(denseVector.Dimension)
        {
            if (denseVector is null)
            {
                throw new ArgumentNullException(nameof(denseVector));
            }
            if (keep is null)
            {
                throw new ArgumentNullException(nameof(keep));
            }

            FastList<int> index = new FastList<int>(1);
            FastList<T> values = new FastList<T>(1);

            T[] dvalues = denseVector.Values;

            for (int i = 0; i < dvalues.Length; ++i)
            {
                if (keep(dvalues[i]))
                {
                    index.Add(i);
                    values.Add(dvalues[i]);
                }
            }

            _indices = index.ToArray();
            _values = values.ToArray();
        }

        /// <summary>
        /// Copy constructor
        /// </summary>
        /// <param name="vector">A sparse vector.</param>
        public SparseVector(SparseVector<T> vector) : base(vector.Dimension)
        {
            if (vector is null)
            {
                throw new ArgumentNullException(nameof(vector));
            }

            _indices = vector._indices.Copy();
            _values = vector._values.Copy();
            _zero = new T();
        }

        /// <summary>
        /// Create a zero vector, of the specified dimension.
        /// </summary>
        /// <param name="dimension">The dimension of the vector.</param>
        public SparseVector(int dimension) : base(dimension)
        {
            _indices = new int[0];
            _values = new T[0];
            _zero = new T();
        }

        /// <summary>
        /// Create a sparse vector from two arrays containing the non-zero indices and their 
        /// respective values. 
        /// </summary>
        /// <param name="dimension">The dimension of the sparse vector.</param>
        /// <param name="indices">The indexes of non-zero elements of the vector, in ascending order.</param>
        /// <param name="values">The non-zero elements of this vector corresponding to <txt>indices</txt>.</param>
        /// <param name="skipChecking">
        /// If <txt>true</txt>, the parameters will not be checked for correctness. This improves performance slightly.
        /// </param>
        public SparseVector(int dimension, int[] indices, T[] values, bool skipChecking = false) : base(dimension)
        { 
            if (!skipChecking)
            {
                if (indices.Length != values.Length)
                {
                    throw new ArgumentOutOfRangeException($"The dimensions of '{nameof(indices)}' and '{nameof(values)}' do not match.");
                }

                // check that indices are ascending and that none exceed 'dimensions'
                for (int i = 1; i < indices.Length; ++i)
                {
                    if (indices[i] <= indices[i - 1])
                    {
                        throw new ArgumentOutOfRangeException(nameof(indices), $"'{nameof(indices)}' is not arranged in strictly ascending order.");
                    }
                }

                // Since the indices are in ascending order, we only need to check the first and last index
                if (indices.Length > 0 && indices[0] < 0)
                {
                    throw new ArgumentOutOfRangeException(nameof(indices),
                        $"'{nameof(indices)}' contains one or more elements that lie outside the range of the vector.");
                }
                if (indices.Length > 0 && indices[indices.Length - 1] >= dimension)
                {
                    throw new ArgumentOutOfRangeException(nameof(indices),
                        $"'{nameof(indices)}' contains one or more elements that lie outside the range of the vector.");
                }
            }

            _indices = indices;
            _values = values;
            _zero = new T();
        }

        /// <summary>
        /// Create a sparse vector with a single non-zero entry, at the specified index and with 
        /// the specified value.
        /// </summary>
        /// <param name="dimension">The dimension of the vector.</param>
        /// <param name="index">The index of the non-zero entry.</param>
        /// <param name="value">The value of the non-zero entry.</param>
        public SparseVector(int dimension, int index, T value) : base(dimension)
        {
            if (index < 0 || index >= dimension)
            {
                throw new ArgumentOutOfRangeException(nameof(index));
            }

            _indices = new int[] { index };
            _values = new T[] { value };
        }

        #endregion


        #region Private methods 

        /// <summary>
        /// Returns the number of non-zero elements in the sum or difference of two vectors of the same size.
        /// </summary>
        private static int CountSizeOfUnion(SparseVector<T> vect1, SparseVector<T> vect2)
        {
            int[] indices1 = vect1._indices, indices2 = vect2._indices;
            T[] values1 = vect1._values, values2 = vect2._values;

            int count = 0,
                i1 = 0, i2 = 0, // the index of each vector
                nnz_1 = values1.Length, nnz_2 = values2.Length; // the number of non-zero elements in each vector

            while (i1 < nnz_1 || i2 < nnz_2)
            {
                int index1 = i1 >= nnz_1 ? int.MaxValue : indices1[i1],
                    index2 = i2 >= nnz_2 ? int.MaxValue : indices2[i2];

                if (index1 > index2)
                {
                    count++;
                    i2++;
                }
                else if (index1 < index2)
                {
                    count++;
                    i1++;
                }
                else
                {
                    count++;
                    i1++;
                    i2++;
                }
            }

            return count;
        }

        /// <summary>
        /// Find the index of the specified index, within the _indices array, in the interval [start, end)
        /// </summary>
        private int FindIndexUnsafe(int index, int start, int end)
        {
            if (start == end)
            {
                if (0 <= start && start < _indices.Length && _indices[start] == index)
                {
                    return start;
                }
                return -1;
            }

            int mid = (start + end) / 2;
            int ind = _indices[mid];

            if (ind == index)
            {
                return mid;
            }
            if (index < ind)
            {
                return FindIndexUnsafe(index, start, mid);
            }
            return FindIndexUnsafe(index, mid + 1, end);
        }

        #endregion


        #region Reflection/Meta

        public override double GetRandomAccessCost()
        {
            return _values.Length / Constants.Ln2;
        }
        public override double GetIterationCost()
        {
            return _values.Length;
        }

        #endregion


        #region Public methods

        /// <summary>
        /// Append another vector either at the end or at the start of this vector.
        /// </summary>
        /// <param name="vector">The vector to append.</param>
        /// <param name="after">
        /// If <txt>true</txt>, the vector will be append after the end of this one; otherwise it 
        /// will be prepended to this vector.
        /// </param>
        /// <returns></returns>
        public SparseVector<T> Append(SparseVector<T> vector, bool after = true)
        {
            int len = _values.Length + vector._values.Length;
            T[] values = new T[len];
            int[] indices = new int[len];

            if (after)
            {
                Array.Copy(_values, 0, values, 0, _values.Length);
                Array.Copy(_indices, 0, indices, 0, _indices.Length);

                Array.Copy(vector._values, 0, values, _values.Length, vector._values.Length);
                Array.Copy(vector._indices, 0, indices, _indices.Length, vector._indices.Length);
            }
            else
            {
                Array.Copy(vector._values, 0, values, 0, vector._values.Length);
                Array.Copy(vector._indices, 0, indices, 0, vector._indices.Length);

                Array.Copy(_values, 0, values, vector._values.Length, _values.Length);
                Array.Copy(_indices, 0, indices, vector._indices.Length, _indices.Length);
            }

            return new SparseVector<T>(Dimension + vector.Dimension, indices, values, true);
        }

        public void Clear()
        {
            _indices = new int[0];
            _values = new T[0];
        }

        /// <summary>
        /// Clear every <txt>step</txt>-th element from this vector, starting
        /// at <txt>startIndex</txt>, and ending at <txt>startIndex + length</txt>.
        /// </summary>
        /// <param name="startIndex"></param>
        /// <param name="length"></param>
        /// <param name="step"></param>
        public void Clear(int startIndex, int length, int step = 1)
        {
            if (startIndex < 0 || startIndex >= Dimension)
            {
                throw new ArgumentOutOfRangeException(nameof(startIndex));
            }
            if (length < 0 || startIndex + length >= Dimension)
            {
                throw new ArgumentOutOfRangeException(nameof(length));
            }
            if (step <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(step));
            }

            int len = _values.Length, end = startIndex + length, i;
            for (i = 0; i < len; ++i)
            {
                int index = _indices[i];
                if (index < startIndex) continue;
                if (index >= end) return;
                if ((index - startIndex) % step == 0)
                {
                    _values[i] = _zero;
                }
            }

        }

        public SparseVector<T> Copy()
        {
            return new SparseVector<T>(this);
        }

        /// <summary>
        /// Returns the number of entries that satisfy a predicate.
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public int Count(Predicate<T> predicate)
        {
            if (predicate is null)
            {
                throw new ArgumentNullException(nameof(predicate));
            }

            int count = 0;
            if (predicate(_zero))
            {
                count += Dimension - _values.Length;
            }

            for (int i = 0; i < _values.Length; ++i)
            {
                if (predicate(_values[i]))
                {
                    ++count;
                }
            }
            return count;
        }

        /// <summary>
        /// Returns the dot product between this vector and another vector.
        /// </summary>
        /// <param name="vector"></param>
        /// <param name="blas"></param>
        /// <returns></returns>
        public T Dot(SparseVector<T> vector, ISparseBLAS1<T> blas)
        {
            if (vector is null)
            {
                throw new ArgumentNullException(nameof(vector));
            }
            if (vector.Dimension != Dimension)
            {
                throw new InvalidOperationException("Vector dimensions don't match.");
            }
            if (blas is null)
            {
                throw new ArgumentNullException(nameof(blas));
            }

            return blas.DOT(_indices, _values, vector._indices, vector._values, 0, 0, _indices.Length, vector._indices.Length);
        }
        
        /// <summary>
        /// Apply a function to each entry of the vector, returning a new vector with the outputs.
        /// </summary>
        /// <typeparam name="F"></typeparam>
        /// <param name="function">The function to apply to each entry.</param>
        /// <param name="skipSymbolicZeroes">If <txt>true</txt>, the function will not be evaluated for 
        /// structural (symbolic) zeroes of this sparse vector. In the resulting vector, these components
        /// will be symbolically zero.
        /// </param>
        /// <param name="skipNumericZeroes">If <txt>true</txt>, the function will not be evaluated for 
        /// numeric zeroes of this sparse vector. In the resulting vector, these components will be 
        /// symbolically zero.</param>
        /// <returns></returns>
        public SparseVector<F> Elementwise<F>(Func<T, F> function, bool skipSymbolicZeroes = true, bool skipNumericZeroes = true) where F  :new()
        {
            if (function is null)
            {
                throw new ArgumentNullException(nameof(function));
            }

            int len = _values.Length;
            F zero = new F();
            if (skipNumericZeroes)
            {
                int nz = 0;

                // Store the non-zero pattern of vector
                BitArray isnz = new BitArray(len);
                for (int i = 0; i < len; ++i)
                {
                    if (!_values[i].Equals(_zero))
                    {
                        isnz[i] = true;
                        ++nz;
                    }
                }

                if (skipSymbolicZeroes || function(_zero).Equals(zero))
                {
                    // Skip both symbolic and numeric zeroes
                    int[] indices = new int[nz];
                    F[] values = new F[nz];
                    for (int i = 0, k = 0; i < len; ++i)
                    {
                        if (isnz[i])
                        {
                            indices[k] = _indices[i];
                            values[k] = function(_values[i]);
                            ++k;
                        }
                    }
                    return new SparseVector<F>(Dimension, indices, values, true);
                }
                else
                {
                    F fzero = function(_zero);
                    nz += Dimension - len;
                    int[] indices = new int[nz];
                    F[] values = new F[nz];

                    for (int i = 0, p = 0, k = 0; i < Dimension; ++i)
                    {
                        if (_indices[p] == i)
                        {
                            // Skip numeric zeroes
                            if (isnz[p])
                            {
                                indices[k] = _indices[p];
                                values[k] = function(_values[p]);
                                ++k;
                            }
                            ++p;
                        }
                        else
                        {
                            indices[k] = i;
                            values[k] = fzero;
                            ++k;
                        }
                    }
                    return new SparseVector<F>(Dimension, indices, values, true);
                }
            }
            else
            {
                if (skipSymbolicZeroes)
                {
                    // Skip symbolic zeroes but not numeric zeroes
                    F[] values = new F[len];
                    for (int i = 0; i < len; ++i)
                    {
                        values[i] = function(_values[i]);
                    }
                    return new SparseVector<F>(Dimension, _indices.Copy(), values, true);
                }
                else
                {
                    // Skip neither numeric nor symbolic zeroes - dense vector
                    int[] indices = new int[Dimension];
                    F[] values = new F[Dimension];
                    F fzero = function(_zero);
                    for (int i = 0, p = 0; i < Dimension; ++i)
                    {
                        if (_indices[p] == i)
                        {
                            values[i] = function(_values[p]);
                            ++p;
                        }
                        else
                        {
                            values[i] = fzero;
                        }
                        indices[i] = i;
                    }
                    return new SparseVector<F>(Dimension, indices, values, true);
                }
            }
        }

        /// <summary>
        /// Returns whether this vector is equal in value to another vector in value, given an elementwise comparer.
        /// Zero is assumed to be identically equal in value to <txt>new T()</txt>
        /// </summary>
        /// <param name="vector"></param>
        /// <param name="equals"></param>
        /// <returns></returns>
        public bool Equals(SparseVector<T> vector, Func<T, T, bool> equals)
        {
            if (vector is null)
            {
                return false;
            }
            if (equals is null)
            {
                throw new ArgumentNullException(nameof(equals));
            }

            if (vector.Dimension != Dimension) return false;
            T zero = new T();

            int end1 = _indices.Length, end2 = vector._indices.Length;
            int i1 = 0, i2 = 0;
            while (i1 < end1 || i2 < end2)
            {
                int j1 = i1 < end1 ? _indices[i1] : int.MaxValue,
                    j2 = i2 < end2 ? vector._indices[i2] : int.MaxValue;

                if (j1 < j2)
                {
                    if (!equals(_values[i1], zero)) return false;
                    ++i1;
                }
                else if (j1 > j2)
                {
                    if (!equals(vector._values[i2], zero)) return false;
                    ++i2;
                }
                else
                {
                    if (!equals(_values[i1], vector._values[i2])) return false;
                    ++i1; ++i2;
                }
            }

            return true;
        }

        /// <summary>
        /// Returns whether this vector is equal in value to a dense vector. 
        /// </summary>
        /// <param name="vector"></param>
        /// <param name="equals"></param>
        /// <returns></returns>
        public bool Equals(DenseVector<T> vector, Func<T, T, bool> equals)
        {
            if (vector is null || vector.Dimension != Dimension)
            {
                return false;
            }
            if (equals is null)
            {
                throw new ArgumentNullException(nameof(equals));
            }

            T zero = new T();
            T[] dvalues = vector.Values;

            int nnz = _values.Length,
                start,
                end = -1;

            // Check [0, _indices[nnz - 1]]
            for (int vi = 0; vi < nnz; ++vi)
            {
                start = end;
                end = _indices[vi];

                for (int i = start + 1; i < end; ++i)
                {
                    if (!equals(zero, dvalues[i]))
                    {
                        return false;
                    }
                }

                if (!equals(_values[vi], dvalues[end]))
                {
                    return false;
                }
            }

            // Check (_indices[nnz - 1], dim)
            for (int i = end + 1; i < Dimension; ++i)
            {
                if (!equals(zero, dvalues[i]))
                {
                    return false;
                }
            }

            return true;
        }

        public SparseVector<T> DirectSum(SparseVector<T> vector)
        {
            return Append(vector, true);
        }

        /// <summary>
        /// Calculates the Hadamard product between this sparse vector and a dense vector, returning the product
        /// as a sparse vector.
        /// </summary>
        /// <param name="vector">The dense vector to pointwise-multiply by.</param>
        /// <param name="provider">The provider used for elementwise multiplication.</param>
        /// <returns>The pointwise product.</returns>
        public SparseVector<T> PointwiseMultiply(DenseVector<T> vector, IProvider<T> provider)
        {
            if (vector is null) throw new ArgumentNullException(nameof(vector));
            if (provider is null) throw new ArgumentNullException(nameof(provider));

            if (Dimension != vector.Dimension)
            {
                throw new InvalidOperationException("Vector dimensions don't match");
            }

            int len = _values.Length;
            T[] values = new T[len], vect_values = vector.Values;
            for (int i = 0; i < len; ++i)
            {
                values[i] = provider.Multiply(_values[i], vect_values[_indices[i]]);
            }
            return new SparseVector<T>(Dimension, _indices.Copy(), values, true);
        }

        /// <summary>
        /// Calculates the Hadamard product between two sparse vectors, returning the product as a sparse vector.
        /// </summary>
        /// <param name="vector">The sparse vector to multiply by.</param>
        /// <param name="provider">The provider used for elementwise multiplications.</param>
        /// <returns>The sparse vector product.</returns>
        public SparseVector<T> PointwiseMultiply(SparseVector<T> vector, IProvider<T> provider)
        {
            if (vector is null)
            {
                throw new ArgumentNullException(nameof(vector));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }

            if (vector.Dimension != Dimension)
            {
                throw new InvalidOperationException("Vector dimensions don't match");
            }

            // Count the number of nz in the product
            int nz = 0;
            int len1 = _values.Length, len2 = vector._values.Length, i1 = 0, i2 = 0;
            while (i1 < len1 && i2 < len2)
            {
                int a = _indices[i1], b = vector._indices[i2];
                if (a < b)
                {
                    ++i1;
                }
                else if (a > b)
                {
                    ++i2;
                }
                else
                {
                    ++i1;
                    ++i2;
                    ++nz;
                }
            }

            int[] indices = new int[nz];
            T[] values = new T[nz];

            int k = 0;
            while (i1 < len1 && i2 < len2)
            {
                int a = _indices[i1], b = vector._indices[i2];
                if (a < b)
                {
                    ++i1;
                }
                else if (a > b)
                {
                    ++i2;
                }
                else
                {
                    indices[k] = i1;
                    values[k++] = provider.Multiply(_values[i1++], vector._values[i2++]);
                }
            }
            return new SparseVector<T>(Dimension, indices, values, true);
        }

        /// <summary>
        /// Returns the outer product between this vector and another vector, as a CSC sparse matrix.
        /// </summary>
        /// <param name="vector">A sparse vector.</param>
        /// <returns>The outer product expressed as a sparse CSC matrix.</returns>
        public CSCSparseMatrix<T> OuterProduct(SparseVector<T> vector, IDenseBLAS1<T> blas1)
        {
            if (vector is null)
            {
                throw new ArgumentNullException(nameof(vector));
            }
            if (blas1 is null)
            {
                throw new ArgumentNullException(nameof(blas1));
            }
            if (!(blas1 is IDenseBLAS1<T> b))
            {
                throw new NotSupportedException(nameof(blas1));
            }

            int rows = Dimension, cols = vector.Dimension, 
                len1 = _values.Length, len2 = vector._values.Length,
                nnz1 = 0, nnz2 = 0;

            IProvider<T> provider = b.Provider;
            T zero = provider.Zero;

            // count the number of numeric non-zeroes
            // numeric evaluation may be expensive, so store in a bitarray
            BitArray b1 = new BitArray(len1), b2 = new BitArray(len2);
            for (int i = 0; i < len1; ++i)
            {
                if (provider.Equals(zero, _values[i]))
                {
                    b1[i] = true;
                    ++nnz1;
                }
            }
            for (int i = 0; i < len2; ++i)
            {
                if (provider.Equals(zero, vector._values[i]))
                {
                    b2[i] = true;
                    ++nnz2;
                }
            }

            int[] columnIndices = new int[cols + 1],
                rowIndices = new int[nnz1 * nnz2];
            T[] values = new T[nnz1 * nnz2];

            int k = 0;
            for (int i = 0; i < len2; ++i)
            {
                if (b2[i])
                {
                    int c = vector._indices[i];
                    T v = vector._values[i];

                    int d = k;
                    for (int j = 0; j < len1; ++j)
                    {
                        if (b1[j])
                        {
                            rowIndices[d++] = _indices[j];
                            values[d++] = _values[j];
                        }
                    }

                    b.SCAL(values, v, k, d);
                    k = d;

                    columnIndices[c + 1] = k;
                }
            }

            // Fill in the missing column indices
            for (int c = 1; c <= cols; ++c)
            {
                if (columnIndices[c] == 0)
                {
                    columnIndices[c] = columnIndices[c - 1];
                }
            }

            return new CSCSparseMatrix<T>(rows, columnIndices, rowIndices, values);
        }

        /// <summary>
        /// Returns the outer product between this vector and a matrix, using the specified dense level-1 BLAS implementation.
        /// </summary>
        /// <param name="matrix"></param>
        /// <param name="blas"></param>
        /// <returns></returns>
        public CSFSparseTensor<T> OuterProduct(CSRSparseMatrix<T> matrix, IDenseBLAS1<T> blas)
        {
            if (matrix is null) throw new ArgumentNullException(nameof(matrix));
            if (blas is null) throw new ArgumentNullException(nameof(blas));
            return new CSFSparseTensor<T>(this, matrix, blas);
        }

        /// <summary>
        /// Replace all non-zero elements that match a predicate with a replacement value.
        /// </summary>
        /// <param name="select"></param>
        /// <param name="replacement"></param>
        public void Replace(Predicate<T> select, T replacement)
        {
            if (select is null)
            {
                throw new ArgumentNullException(nameof(select));
            }
            if (replacement is null)
            {
                throw new ArgumentNullException(nameof(replacement));
            }

            int len = _values.Length;
            for (int i = 0; i < len; ++i)
            {
                if (select(_values[i]))
                {
                    _values[i] = replacement;
                }
            }
        }

        /// <summary>
        /// Reshape this sparse vector into a matrix
        /// </summary>
        /// <param name="rows"></param>
        /// <param name="columns"></param>
        /// <param name="rowMajorOrder"></param>
        /// <returns></returns>
        public CSRSparseMatrix<T> Reshape(int rows, int columns, bool rowMajorOrder = true)
        {
            if (rows <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(rows), ExceptionMessages.NonPositiveNotAllowed);
            }
            if (columns <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(columns), ExceptionMessages.NonPositiveNotAllowed);
            }
            if (rows * columns != Dimension)
            {
                throw new InvalidOperationException($"Invalid dimension sizes: {rows} x {columns} != {Dimension}");
            }

            return new CSRSparseMatrix<T>(rows, columns, this, rowMajorOrder);
        }

        public override Vector<T> Select(int[] indices)
        {
            if (indices is null)
            {
                throw new ArgumentNullException(nameof(indices));
            }
            ThrowHelper.AssertCollectionInRange(indices, 0, Dimension, nameof(indices));

            int len = indices.Length;
            // indices is not necessarily unique or sorted. to reduce 
            // complexity we create a inverse mapping array that points
            // indices[i] to _indices[i]
            int[] workspace = new int[len];
            Array.Copy(indices, workspace, len);

            // sort and make workspace unique
            Array.Sort(workspace);
            int k = 0;
            workspace[k] = workspace[0];
            for (int i = 1; i < len; ++i)
            {
                if (workspace[i] > workspace[k])
                {
                    workspace[++k] = workspace[i];
                }
            }
            ++k;

            // Dictionary used to inverse-map the required indices
            Dictionary<int, int> inverse_indices = new Dictionary<int, int>(k);

            // Since both workspace and _indices are sorted, we can 
            // co-iterate in linear time
            int nz = _indices.Length;
            int a = 0, b = 0;
            while (a < nz && b < k)
            {
                int i_vect = _indices[a], i_sel = workspace[b];
                if (i_vect < i_sel)
                {
                    ++a;
                }
                else if (i_vect > i_sel)
                {
                    ++b;
                }
                else
                {
                    inverse_indices.Add(i_sel, a);
                    ++a;
                    ++b;
                }
            }

            // Count the number of non-zeroes
            nz = 0;
            for (int i = 0; i < len; ++i)
            {
                if (inverse_indices.ContainsKey(indices[i]))
                {
                    ++nz;
                }
            }

            // Return a sparse vector
            int[] sel_indices = new int[nz];
            T[] sel_values = new T[nz];
            k = 0;
            for (int i = 0; i < len; ++i)
            {
                int index = indices[i];
                if (inverse_indices.ContainsKey(index))
                {
                    sel_indices[k] = i;
                    sel_values[k++] = _values[inverse_indices[index]];
                }
            }

            return new SparseVector<T>(len, sel_indices, sel_values, true);
        }

        /// <summary>
        /// Returns the sum of this vector's elements.
        /// </summary>
        /// <param name="provider">The provider used for elementwise operations.</param>
        /// <returns>The sum of this vector's elements.</returns>
        public T Sum(IProvider<T> provider)
        {
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }

            T sum = provider.Zero;
            int len = _values.Length, i;
            for (i = 0; i < len; ++i)
            {
                sum = provider.Add(sum, _values[i]);
            }
            return sum;
        }

        /// <summary>
        /// Returns the sum of absolute values of this vector's elements.
        /// </summary>
        /// <param name="provider"></param>
        /// <returns></returns>
        public T SumAbs(IRealProvider<T> provider)
        {
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            int len = _values.Length, i;
            T sum = provider.Zero;
            for (i = 0; i < len; ++i)
            {
                sum = provider.Add(sum, provider.Abs(_values[i]));
            }
            return sum;
        }

        public override T[] ToArray()
        {
            T[] values = RectangularVector.Zeroes<T>(Dimension);
            int len = _values.Length;
            for (int i = 0; i < len; ++i)
            {
                values[_indices[i]] = _values[i];
            }
            return values;
        }

        public override DenseVector<T> ToDenseVector()
        {
            return new DenseVector<T>(this);
        }

        public override SparseVector<T> ToSparseVector()
        {
            return new SparseVector<T>(this);
        }

        public BigSparseVector<T> ToBigSparseVector()
        {
            return new BigSparseVector<T>(this);
        }

        public CSCSparseMatrix<T> ToColumnMatrix()
        {
            int[] columnIndices = { 0, _values.Length };
            int[] rowIndices = _indices.Copy();
            T[] values = _values.Copy();
            return new CSCSparseMatrix<T>(Dimension, columnIndices, rowIndices, values);
        }

        public CSRSparseMatrix<T> ToRowMatrix()
        {
            int[] columnIndices = { 0, _values.Length };
            int[] rowIndices = _indices.Copy();
            T[] values = _values.Copy();
            return new CSRSparseMatrix<T>(Dimension, columnIndices, rowIndices, values);
        }

        #endregion


        #region Operations

        public SparseVector<T> Add(SparseVector<T> vector, IProvider<T> provider)
        {
            if (vector is null)
            {
                throw new ArgumentNullException(nameof(vector));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            if (vector.Dimension != Dimension)
            {
                throw new InvalidOperationException(ExceptionMessages.VectorSizeMismatch);
            }

            // Want to avoid using any lists - loop through values twice, once to count 
            // the number of non-zero elements, and again to insert the values.
            // Total complexity: O(nnz)

            int count = CountSizeOfUnion(this, vector),
                nnz_1 = _values.Length, nnz_2 = vector._values.Length; // the number of non-zero elements in each vector

            // Loop through a second time, filling in the values
            T[] sum_values = new T[count];
            int[] sum_indices = new int[count];

            int i1 = 0, i2 = 0, k = 0;
            while (i1 < nnz_1 || i2 < nnz_2)
            {
                int index1 = i1 >= nnz_1 ? int.MaxValue : _indices[i1],
                    index2 = i2 >= nnz_2 ? int.MaxValue : vector._indices[i2];

                if (index1 > index2)
                {
                    sum_values[k] = vector._values[i2];
                    sum_indices[k] = index2;
                    ++i2;
                }
                else if (index1 < index2)
                {
                    sum_values[k] = _values[i1];
                    sum_indices[k] = index1;
                    ++i1;
                }
                else
                {
                    sum_values[k] = provider.Add(_values[i1], vector._values[i2]);
                    sum_indices[k] = index1;
                    i1++;
                    i2++;
                }
                ++k;
            }

            return new SparseVector<T>(Dimension, sum_indices, sum_values, true);
        }

        public SparseVector<T> Subtract(SparseVector<T> vector, IProvider<T> provider)
        {
            if (vector is null)
            {
                throw new ArgumentNullException(nameof(vector));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            if (vector.Dimension != Dimension)
            {
                throw new InvalidOperationException(ExceptionMessages.VectorSizeMismatch);
            }

            // Want to avoid using any lists - loop through values twice, once to count 
            // the number of non-zero elements, and again to insert the values.
            // Total complexity: O(nnz)

            int count = CountSizeOfUnion(this, vector),
                nnz_1 = _values.Length, nnz_2 = vector._values.Length; // the number of non-zero elements in each vector

            // Loop through a second time, filling in the values
            T[] sum_values = new T[count];
            int[] sum_indices = new int[count];

            int i1 = 0, i2 = 0, k = 0;
            while (i1 < nnz_1 || i2 < nnz_2)
            {
                int index1 = i1 >= nnz_1 ? int.MaxValue : _indices[i1],
                    index2 = i2 >= nnz_2 ? int.MaxValue : vector._indices[i2];

                if (index1 > index2)
                {
                    sum_values[k] = provider.Negate(vector._values[i2]);
                    sum_indices[k] = index2;
                    ++i2;
                }
                else if (index1 < index2)
                {
                    sum_values[k] = _values[i1];
                    sum_indices[k] = index1;
                    ++i1;
                }
                else
                {
                    sum_values[k] = provider.Subtract(_values[i1], vector._values[i2]);
                    sum_indices[k] = index1;
                    i1++;
                    i2++;
                }
                ++k;
            }

            return new SparseVector<T>(Dimension, sum_indices, sum_values, true);
        }

        public void NegateInPlace(IProvider<T> provider)
        {
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }

            int len = _values.Length; 
            for (int i = 0; i < len; ++i)
            {
                _values[i] = provider.Negate(_values[i]);
            }
        }
        public SparseVector<T> Negate(IProvider<T> provider)
        {
            SparseVector<T> copy = Copy();
            copy.NegateInPlace(provider);
            return copy;
        }

        public void MultiplyInPlace(T scalar, IProvider<T> provider)
        {
            if (scalar is null)
            {
                throw new ArgumentNullException(nameof(scalar));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }

            int len = _values.Length;
            for (int i = 0; i < len; ++i)
            {
                _values[i] = provider.Multiply(_values[i], scalar);
            }
        }
        public void MultiplyInPlace(T scalar, IDenseBLAS1Provider<T> blas1)
        {
            if (scalar is null)
            {
                throw new ArgumentNullException(nameof(scalar));
            }
            if (blas1 is null)
            {
                throw new ArgumentNullException(nameof(blas1));
            }
            if (!(blas1 is IDenseBLAS1<T> b))
            {
                throw new NotSupportedException(ExceptionMessages.TypeNotSupported);
            }

            b.SCAL(_values, scalar, 0, _values.Length);
        }

        public SparseVector<T> Multiply(T scalar, IProvider<T> provider)
        {
            SparseVector<T> copy = Copy();
            MultiplyInPlace(scalar, provider);
            return copy;
        }
        public SparseVector<T> Multiply(T scalar, IDenseBLAS1Provider<T> blas1)
        {
            SparseVector<T> copy = Copy();
            MultiplyInPlace(scalar, blas1);
            return copy;
        }

        /// <summary>
        /// Postmultiply the transpose of this vector by a matrix, $v^TA$, returning the product 
        /// as a sparse vector.
        /// </summary>
        /// <param name="matrix">The matrix to postmultiply by.</param>
        /// <param name="provider">The provider used for elementwise operations.</param>
        /// <returns>The sparse vector-matrix product.</returns>
        public SparseVector<T> Multiply(Matrix<T> matrix, IProvider<T> provider)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            if (Dimension != matrix.Rows)
            {
                throw new InvalidOperationException("Dimensions don't match");
            }
            return matrix.TransposeAndMultiply(this, provider);
        }

        public void DivideInPlace(T scalar, IProvider<T> provider)
        {
            if (scalar is null)
            {
                throw new ArgumentNullException(nameof(scalar));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            MultiplyInPlace(provider.Divide(provider.One, scalar), provider);
        }
        public void DivideInPlace(T scalar, IDenseBLAS1Provider<T> blas1)
        {
            if (scalar is null)
            {
                throw new ArgumentNullException(nameof(scalar));
            }
            if (blas1 is null)
            {
                throw new ArgumentNullException(nameof(blas1));
            }
            if (!(blas1 is IDenseBLAS1<T> b))
            {
                throw new NotSupportedException(ExceptionMessages.TypeNotSupported);
            }
            b.SCAL(_values, b.Provider.Divide(b.Provider.One, scalar), 0, _values.Length);
        }

        public SparseVector<T> Divide(T scalar, IProvider<T> provider)
        {
            SparseVector<T> copy = Copy();
            copy.DivideInPlace(scalar, provider);
            return copy;
        }
        public SparseVector<T> Divide(T scalar, IDenseBLAS1Provider<T> blas1)
        {
            SparseVector<T> copy = Copy();
            copy.DivideInPlace(scalar, blas1);
            return copy;
        }

        public T SumProduct(SparseVector<T> vector, IProvider<T> provider)
        {
            if (vector is null)
            {
                throw new ArgumentNullException(nameof(vector));
            }
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }

            int nnz_1 = _values.Length, nnz_2 = vector._values.Length; // the number of non-zero elements in each vector

            T dot = provider.Zero;
            int i1 = 0, i2 = 0;
            while (i1 < nnz_1 || i2 < nnz_2)
            {
                int index1 = i1 >= nnz_1 ? int.MaxValue : _indices[i1],
                    index2 = i2 >= nnz_2 ? int.MaxValue : vector._indices[i2];

                if (index1 > index2)
                {
                    ++i2;
                }
                else if (index1 < index2)
                {
                    ++i1;
                }
                else
                {
                    dot = provider.Add(dot, provider.Multiply(_values[i1], vector._values[i2]));
                    ++i1;
                    ++i2;
                }
            }

            return dot;
        }

        #endregion


        #region Operators

        public static SparseVector<T> operator +(SparseVector<T> u, SparseVector<T> v)
        {
            if (u is null)
            {
                throw new ArgumentNullException(nameof(u));
            }
            return u.Add(v, ProviderFactory.GetDefaultProvider<T>());
        }

        public static SparseVector<T> operator -(SparseVector<T> u, SparseVector<T> v)
        {
            if (u is null)
            {
                throw new ArgumentNullException(nameof(u));
            }
            return u.Subtract(v, ProviderFactory.GetDefaultProvider<T>());
        }

        public static SparseVector<T> operator -(SparseVector<T> u)
        {
            if (u is null)
            {
                throw new ArgumentNullException(nameof(u));
            }
            return u.Negate(ProviderFactory.GetDefaultProvider<T>());
        }

        public static SparseVector<T> operator *(SparseVector<T> u, T scalar)
        {
            if (u is null)
            {
                throw new ArgumentNullException(nameof(u));
            }
            return u.Multiply(scalar, ProviderFactory.GetDefaultBLAS1<T>());
        }

        public static SparseVector<T> operator *(T scalar, SparseVector<T> u) => u * scalar;

        public static SparseVector<T> operator *(SparseVector<T> v, Matrix<T> A)
        {
            if (v is null) throw new ArgumentNullException(nameof(v));
            return v.Multiply(A, ProviderFactory.GetDefaultProvider<T>());
        }

        public static SparseVector<T> operator /(SparseVector<T> u, T scalar)
        {
            if (u is null)
            {
                throw new ArgumentNullException(nameof(u));
            }
            return u.Divide(scalar, ProviderFactory.GetDefaultBLAS1<T>());
        }

        #endregion


        #region Casts

        public static explicit operator DenseVector<T>(SparseVector<T> u) => new DenseVector<T>(u);
        public static explicit operator BigSparseVector<T>(SparseVector<T> u) => new BigSparseVector<T>(u);

        #endregion
    }
}
