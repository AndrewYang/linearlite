﻿using LinearNet.Global;
using LinearNet.Helpers;
using System;
using System.Collections.Generic;

namespace LinearNet.Structs
{
    public static class SparseVector
    {
        public static SparseVector<T> Random<T>(int dimension, double approxDensity, Func<T> random = null) where T : new()
        {
            if (dimension <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(dimension), ExceptionMessages.NonPositiveNotAllowed);
            }
            if (double.IsNaN(approxDensity))
            {
                throw new ArgumentOutOfRangeException(nameof(approxDensity), ExceptionMessages.NaNNotAllowed);
            }
            if (approxDensity < 0.0 || approxDensity > 1.0)
            {
                throw new ArgumentOutOfRangeException(nameof(approxDensity), ExceptionMessages.NotBetween0And1);
            }

            if (random is null)
            {
                random = DefaultGenerators.GetRandomGenerator<T>();
            }

            int capacity = Math.Max(1, (int)(dimension * approxDensity));
            List<int> indices = new List<int>(capacity);
            List<T> values = new List<T>(capacity);

            Random r = new Random();

            // Naive implementation here
            for (int d = 0; d < dimension; ++d)
            {
                if (r.NextDouble() < approxDensity)
                {
                    indices.Add(d);
                    values.Add(random());
                }
            }

            return new SparseVector<T>(dimension, indices.ToArray(), values.ToArray(), true);
        }
    }
}
