﻿using LinearNet.Structs.Vectors;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace LinearNet.Structs
{
    /// <summary>
    /// An abstract class representing an arbitrary dimensional vector over <txt>T</txt>. All vector implementations 
    /// inherit this class.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <cat>linear-algebra</cat>
    public abstract class Vector<T> where T : new()
    {
        public VectorStorageType Type { get; private set; }

        public int Dimension { get; private set; }
        public long LongDimension { get; private set; }
        
        public abstract T this[int index] { get; set; }
        public Vector<T> this[string selector] 
        { 
            get
            {
                return Select(selector, false);
            } 
        }

        public abstract int NonZeroCount { get; }
        public abstract IEnumerable<VectorEntry<T>> NonZeroEntries { get; }

        protected Vector(VectorStorageType type, int dimension)
        {
            Type = type;
            Dimension = dimension;
            LongDimension = dimension;
        }
        protected Vector(VectorStorageType type, long dimension)
        {
            Type = type;
            Dimension = (int)dimension;
            LongDimension = dimension;
        }

        internal void Print(Func<T, string> ToString)
        {
            StringBuilder s = new StringBuilder();
            for (int i = 0; i < Dimension; ++i)
            {
                s.Append(ToString(this[i]) + "\t");
            }
            Debug.WriteLine(s.ToString());
        }

        #region Meta 

        /// <summary>
        /// Returns an estimate for the cost of randomly accessing an element 
        /// from this vector.
        /// </summary>
        /// <returns>The random access cost (in operations).</returns>
        public abstract double GetRandomAccessCost();

        /// <summary>
        /// Returns an estimate for the cost of iterating through the non-zeroes
        /// of this vector.
        /// </summary>
        /// <returns>The iteration cost (in operations)</returns>
        public abstract double GetIterationCost();

        #endregion

        public abstract Vector<T> Add(Vector<T> vector);

        public Vector<T> Append(Vector<T> vector)
        {
            return DirectSum(vector);
        }

        public abstract Vector<T> DirectSum(Vector<T> vector);

        public Vector<T> Select(string selector, bool oneBased = false)
        {
            if (string.IsNullOrEmpty(selector))
            {
                throw new ArgumentNullException(nameof(selector));
            }
            if (!Selector.TryParse(selector, oneBased, Dimension, out Selector s))
            {
                throw new FormatException(nameof(selector));
            }
            return Select(s.Selection);
        }

        public abstract Vector<T> Select(int[] indices);

        /// <summary>
        /// Returns this vector represented as an array of values.
        /// The array is guaranteed to not share any memory with this 
        /// vector object; altering the array will not alter the vector.
        /// </summary>
        /// <returns>An array representing this vector.</returns>
        public abstract T[] ToArray();

        /// <summary>
        /// Returns this vector as a dense vector. The returned vector is guaranteed to not share any memory with 
        /// this vector.
        /// </summary>
        /// <returns></returns>
        public abstract DenseVector<T> ToDenseVector();

        /// <summary>
        /// Returns this vector as a sparse vector. The returned vector is guaranteed to not share any memory with 
        /// this vector.
        /// </summary>
        /// <returns></returns>
        public abstract SparseVector<T> ToSparseVector();
    }
}
