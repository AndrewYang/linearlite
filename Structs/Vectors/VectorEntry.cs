﻿using System;

namespace LinearNet.Structs
{
    public readonly struct VectorEntry<T> : IComparable<VectorEntry<T>>
    {
        public readonly int Index { get; }
        public readonly T Value { get; }

        public VectorEntry(int index, T value)
        {
            Index = index;
            Value = value;
        }

        public int CompareTo(VectorEntry<T> other)
        {
            return Index.CompareTo(other.Index);
        }
    }
}
