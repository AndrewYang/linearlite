﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LinearNet.Structs.Vectors
{
    public enum VectorStorageType
    {
        /// <summary>
        /// Dictionary-of-keys storage format
        /// </summary>
        DOK,

        /// <summary>
        /// Dense vector storage format
        /// </summary>
        DENSE,

        /// <summary>
        /// Sparse vector storage format
        /// </summary>
        SPARSE
    }
}
