﻿using LinearNet.Symbolic.Structs;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;

namespace LinearNet.Symbolic
{
    /// <summary>
    /// Implements comparison methods for two expressions.
    /// </summary>
    internal class ExprCompare : EqualityComparer<Expr>
    {
        public override bool Equals(Expr x, Expr y)
        {
            return ExactlyEqual(x, y);
        }

        /// <summary>
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override int GetHashCode(Expr obj)
        {
            /// TODO: implement
            throw new NotImplementedException();
        }

        /// <summary>
        /// Sort this expression (no copy is made)
        /// </summary>
        /// <param name="expr"></param>
        internal static void ToCanonicalSortOrder(Expr expr)
        {
            // Already sorted
            if (InCanonicalSortOrder(expr))
            {
                return;
            }

            if (expr.IsLeaf)
            {
                return;
            }

            Expr[] args = expr.Children;

            // depth-first sort by children 
            foreach (Expr e in args)
            {
                ToCanonicalSortOrder(e);
            }

            // Must use this to "pass through" non-associative functions
            if (expr.Function.IsAssociative)
            {
                // Then arrange root using CompareTo
                expr.Children = args.OrderBy(e => e).ToArray();
            }
        }

        /// <summary>
        /// Returns whether this expression is in canonical sort order.
        /// This method is used before every attempt at sorting, since 
        /// it has O(n) complexity (vs O(nlogn) for sort), and most of the 
        /// time our expressions will have already been sorted.
        /// </summary>
        /// <returns></returns>
        internal static bool InCanonicalSortOrder(Expr expr)
        {
            if (expr.IsLeaf)
            {
                return true;
            }

            foreach (Expr child in expr.Children)
            {
                if (!InCanonicalSortOrder(child))
                {
                    return false;
                }
            }

            // Comparing root-level sort order only applies if this expr is associative 
            if (expr.Function.IsAssociative)
            {
                int dary = expr.NumArguments;
                for (int i = 1; i < dary; ++i)
                {
                    //Debug.WriteLine(expr.Children[i - 1] + " vs " + expr.Children[i] + " = " + expr.Children[i].CompareTo(expr.Children[i - 1]));

                    // Invalid sort order
                    if (expr.Children[i].CompareTo(expr.Children[i - 1]) < 0)
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// Returns whether this expression is a and b are like terms.
        /// a and b will be changed by this operation
        /// </summary>
        internal static bool Like(Expr a, Expr b)
        {
            if (a is null || b is null)
            {
                return false;
            }

            // Before checking multiply, add etc, 
            // make sure to flatten, this will also put a, b into canonical sort order
            ExprSimplify.FlattenAssociativeOperations(a);
            ExprSimplify.FlattenAssociativeOperations(b);

            return LikeSorted(a, b);
        }
        private static bool LikeSorted(Expr a, Expr b)
        {
            // Easiest case to handle is the constant case
            if (a.IsConstant || b.IsConstant)
            {
                return a.IsConstant && b.IsConstant;
            }
            if (a.IsVar && b.IsVar)
            {
                return (a.Function as FVariable).VariableName == (b.Function as FVariable).VariableName;
            }

            // Handle negations 
            if (a.Function.Type == FType.Negate) return LikeSorted(a.FirstArg, b);
            if (b.Function.Type == FType.Negate) return LikeSorted(a, b.FirstArg);

            // Remove constants in front of terms
            if (a.Function.Type == FType.Multiply)
            {
                Expr ac = a.WithoutCoefficient();
                if (ac is null)
                {
                    return b.IsConstant;
                }
                if (ac.NumArguments < a.NumArguments)
                {
                    return LikeSorted(ac, b);
                }
            }
            if (b.Function.Type == FType.Multiply)
            {
                Expr bc = b.WithoutCoefficient();
                if (bc is null)
                {
                    return a.IsConstant;
                }
                if (bc.NumArguments < b.NumArguments)
                {
                    return LikeSorted(a, bc);
                }
            }

            // End of recursive calls - from here on out a, b must have the same function type
            if (a.Function.Type != b.Function.Type)
            {
                return false;
            }

            // If not multiplication, children must be exact (since everything is sorted in canonical order)
            if (a.Function.Type != FType.Multiply)
            {
                return a.EqualsExactly(b);
            }

            int a_ary = a.Children.Length, b_ary = b.Children.Length;
            int index = 0, other_index = 0;
            while (index < a_ary && other_index < b_ary)
            {
                if (a.Children[index].CompareTo(b.Children[other_index]) != 0)
                {
                    return false;
                }

                index++;
                other_index++;

                // Skip all constant terms
                if (index < a_ary && a.Children[index].IsConstant)
                {
                    index++;
                }
                if (other_index < b_ary && b.Children[other_index].IsConstant)
                {
                    other_index++;
                }
            }

            // check finished
            if (index == a_ary && other_index == b_ary)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Returns whether expressions x, y are equal up to permutation of associative operations
        /// like multiplication and addition.
        /// 
        /// x, y are not changed by this operation
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        internal static bool EqualUpToAssociativity(Expr x, Expr y)
        {
            bool x_null = x is null,
                y_null = y is null;

            if (x_null && y_null)
            {
                return true;
            }
            if (x_null || y_null)
            {
                return false;
            }

            Expr x_sorted = x.Copy();
            ExprSimplify.FlattenAssociativeOperations(x_sorted);

            Expr y_sorted = y.Copy();
            ExprSimplify.FlattenAssociativeOperations(y_sorted);

            return x_sorted.CompareTo(y_sorted) == 0;
        }

        /// <summary>
        /// Returns whether x, y are exactly equal, including precise ordering
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        internal static bool ExactlyEqual(Expr x, Expr y)
        {
            bool x_null = x is null,
                y_null = y is null;

            if (x_null && y_null)
            {
                return true;
            }
            if (x_null || y_null)
            {
                return false;
            }

            return x.CompareTo(y) == 0;
        }

        /// <summary>
        /// Returns whether x, y are equal after simplification
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        internal static bool EqualUptoSimplification(Expr x, Expr y)
        {
            bool x_null = x is null,
                y_null = y is null;

            if (x_null && y_null)
            {
                return true;
            }
            if (x_null || y_null)
            {
                return false;
            }

            Expr simplified_x = x.Simplify(),
                simplified_y = y.Simplify();

            return simplified_x.CompareTo(simplified_y) == 0;
        }
    }
}
