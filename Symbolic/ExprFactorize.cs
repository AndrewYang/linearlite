﻿using LinearNet.Symbolic.Structs;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Numerics;
using System.Text;

namespace LinearNet.Symbolic
{
    internal static class ExprFactorize
    {
        /// <summary>
        /// Returns: whether a change was made.
        /// </summary>
        internal static bool Factorize(Expr expr, bool simplify)
        {
            ExprSimplify.FlattenAssociativeOperations(expr);

            bool success = FactorInner(expr);

            if (simplify)
            {
                ExprSimplify.Simplify(expr, false);
            }

            return success;
        }

        internal static bool FactorInner(Expr expr)
        {
            if (expr == null)
            {
                return false;
            }

            if (expr.IsLeaf)
            {
                return false;
            }

            // Factorization only applies to expressions whose root function is (+). 
            // Iterate into child elements if it is not a (+) function.
            if (expr.Function.Type != FType.Add)
            {
                bool changed = false;
                foreach (Expr child in expr.Children)
                {
                    if (FactorInner(child))
                    {
                        changed = true;
                    }
                }
                return changed;
            }

            // Must be in proper sorted order for deep comparisons
            ExprCompare.ToCanonicalSortOrder(expr);

            // Try to factorize according to its form
            if (TryFactorizeNaive(expr))
            {
                return true;
            }

            return false;
        }

        private static bool TryFactorizeNaive(Expr expr)
        {
            if (expr.Function.Type != FType.Add)
            {
                return false;
            }

            if (expr.NumArguments <= 1)
            {
                return false;
            }

            Factorization[] summands = new Factorization[expr.NumArguments];
            for (int i = 0; i < summands.Length; ++i)
            {
                summands[i] = new Factorization(expr.Children[i]);
            }

            List<Expr> hcf = HCF(summands);
            if (hcf.Count == 0)
            {
                return false;
            }

            Expr divisor = new Expr(new FMultiply(hcf.Count), hcf.ToArray());
            Expr[] terms = new Expr[expr.NumArguments];
            for (int i = 0; i < summands.Length; ++i)
            {
                Factorization factor = summands[i];
                if (factor.Negate)
                {
                    terms[i] = -Divide(summands[i].Factors, hcf);
                }
                else
                {
                    terms[i] = Divide(summands[i].Factors, hcf);
                }
            }

            expr.Function = new FMultiply(2);
            expr.Children = new Expr[]
            {
                divisor,
                new Expr(new FAdd(terms.Length), terms)
            };

            return true;
        }

        private static List<Expr> HCF(Factorization[] terms)
        {
            List<Expr> hcf = Copy(terms[0].Factors);
            for (int i = 1; i < terms.Length; ++i)
            {
                Intersect(hcf, Copy(terms[i].Factors));
            }
            return hcf;
        }
        private static List<Expr> Copy(List<Expr> list)
        {
            List<Expr> copy = new List<Expr>();
            copy.AddRange(list);
            return copy;
        }

        /// <summary>
        /// set list1 <- intersection(list1, list2)
        /// </summary>
        private static void Intersect(List<Expr> list1, List<Expr> list2)
        {
            for (int i = list1.Count - 1; i >= 0; --i)
            {
                int index = IndexOf(list2, list1[i]);
                if (index < 0)
                {
                    // not found in list2 - remove from list1
                    list1.RemoveAt(i);
                }
                else
                {
                    // found in list2, remove from list2 to avoid counting it again
                    list2.RemoveAt(index);
                }
            }
        }
        private static int IndexOf(List<Expr> list, Expr e)
        {
            for (int i = 0; i < list.Count; ++i)
            {
                if (list[i].CompareTo(e) == 0)
                {
                    return i;
                }
            }
            return -1;
        }

        private static Expr Divide(List<Expr> a, List<Expr> b)
        {
            foreach (Expr bFact in b)
            {
                // Must loop through backwards to avoid indexing issues
                bool found = false;
                for (int i = a.Count - 1; i >= 0; --i)
                {
                    Expr aFact = a[i];
                    if (aFact.CompareTo(bFact) == 0)
                    {
                        a.RemoveAt(i);
                        found = true;
                        break;
                    }
                }

                if (!found)
                {
                    throw new NotImplementedException();
                }
            }

            // Exactly divisible
            if (a.Count == 0)
            {
                return Expr.One;
            }
            return new Expr(new FMultiply(a.Count), a.ToArray());
        }
    }
    internal class Factorization
    {
        internal bool Negate { get; set; }
        internal List<Expr> Factors { get; set; }

        internal Factorization(Expr expr)
        {
            List<Expr> factors = new List<Expr>();
            if (expr.Function.Type == FType.Negate)
            {
                BreakUpIntoFactors(expr.FirstArg, factors);
                Negate = true;
            }
            else
            {
                BreakUpIntoFactors(expr, factors);
                Negate = false;
            }
            Factors = factors;
        }

        private static void BreakUpIntoFactors(Expr expr, List<Expr> factors)
        {
            if (expr.Function.Type == FType.Multiply)
            {
                foreach (Expr fact in expr.Children)
                {
                    BreakUpIntoFactors(fact, factors);
                }
                return;
            }

            if (expr.Function.Type == FType.Pow)
            {
                Expr bse = expr.FirstArg;
                Expr pow = expr.LastArg;

                if (pow.IsConstant)
                {
                    FConstant power = pow.Function as FConstant;
                    if (power.IsInteger)
                    {
                        BigInteger bint = power.BigIntegerValue;
                        if (int.MinValue <= bint && bint <= int.MaxValue)
                        {
                            int int_power = (int)bint;
                            for (int i = 0; i < int_power; ++i)
                            {
                                factors.Add(bse);
                            }
                        }
                        else
                        {
                            // This shows the weakness of the method - better to group by variable type 
                            // as the list of variables can get very large 
                            throw new NotImplementedException();
                        }
                    }
                    else
                    {
                        factors.Add(expr);
                    }
                    return;
                }

                if (pow.Function.Type == FType.Add)
                {
                    foreach (Expr exponent in pow.Children)
                    {
                        BreakUpIntoFactors(bse ^ exponent, factors);
                    }
                    return;
                }
            }

            factors.Add(expr);
        }
    }
}
