﻿using LinearNet.Structs;
using LinearNet.Structs.Fields;
using LinearNet.Symbolic.Structs;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Numerics;
using System.Text;

namespace LinearNet.Symbolic
{
    internal static class ExprPolynomial
    {
        /// <summary>
        /// This method only looks at the expression 'as is', without 
        /// bothering to simplify
        /// </summary>
        /// <param name="expr"></param>
        /// <returns></returns>
        internal static bool IsPolynomial(Expr expr)
        {
            if (expr.IsConstant || expr.IsVar)
            {
                return true;
            }

            // These functions pass the polynomial test
            if (expr.Function.Type == FType.Add || 
                expr.Function.Type == FType.Multiply || 
                expr.Function.Type == FType.Negate)
            {
                foreach (Expr child in expr.Children)
                {
                    if (!IsPolynomial(child))
                    {
                        return false;
                    }
                }
                return true;
            }

            // Pow() is only a polynomial if the power is >= 0 and integer
            if (expr.Function.Type == FType.Pow)
            {
                if (expr.LastArg.IsConstant)
                {
                    FConstant power = expr.LastArg.Function as FConstant;
                    if (power.IsInteger)
                    {
                        // Satisfies the polynomial requirement for power - 
                        // check if base is a polynomial.
                        return IsPolynomial(expr.FirstArg);
                    }
                }
                return false;
            }

            // All other functions are not supported
            return false;
        }

        /// <summary>
        /// Assumes that the expression is actually a polynomial
        /// For inexact polynomials
        /// </summary>
        /// <param name="expr"></param>
        /// <param name="polynomial"></param>
        /// <returns></returns>
        internal static bool TryConvertUnipolynomial(Expr expr, out Polynomial<BigDecimal> polynomial)
        {
            if (expr == null)
            {
                throw new ArgumentNullException(nameof(expr));
            }

            // Check that the polynomial is univariate
            HashSet<string> variables = expr.Variables();
            if (variables.Count > 1)
            {
                polynomial = default;
                return false;
            }

            // Expand and simplify
            expr = expr.Expand(true);

            return TryConvertUnipolynomialInner(expr, out polynomial);
        }
        private static bool TryConvertUnipolynomialInner(Expr expr, out Polynomial<BigDecimal> polynomial)
        {
            polynomial = default;

            if (expr is null || expr.Function is null)
            {
                return false;
            }

            // Handles
            // 4, x
            if (expr.IsLeaf)
            {
                if (expr.Function.Type == FType.Const)
                {
                    polynomial = new Polynomial<BigDecimal>((expr.Function as FConstant).BigDecimalValue);
                    return true;
                }
                if (expr.Function.Type == FType.Var)
                {
                    polynomial = new Polynomial<BigDecimal>(new Dictionary<long, BigDecimal>() { [1L] = BigDecimal.One });
                    return true;
                }
                return false;
            }

            // Handles additive negations
            // E.g. -x
            if (expr.Function.Type == FType.Negate)
            {
                if (!TryConvertUnipolynomialInner(expr.FirstArg, out polynomial))
                {
                    return false;
                }
                polynomial = polynomial.AdditiveInverse();
                return true;
            }

            // Handles addition of different polynomial terms
            // E.g. x + x^2 + 1
            if (expr.Function.Type == FType.Add)
            {
                polynomial = new Polynomial<BigDecimal>();
                foreach (Expr summand in expr.Children)
                {
                    if (!TryConvertUnipolynomialInner(summand, out Polynomial<BigDecimal> term))
                    {
                        polynomial = default;
                        return false;
                    }
                    polynomial += term;
                }
                return true;
            }

            // Handles:
            // x^3, 2^10
            if (expr.Function.Type == FType.Pow)
            {
                Expr bse = expr.FirstArg;
                Expr pow = expr.LastArg;

                if (!pow.IsConstant)
                {
                    return false;
                }

                // E.g. 3^5
                if (bse.IsConstant)
                {
                    polynomial = new Polynomial<BigDecimal>(BigDecimal.Pow((bse.Function as FConstant).BigDecimalValue, (pow.Function as FConstant).BigDecimalValue));
                    return true;
                }

                // E.g. x^5
                else if (bse.IsVar)
                {
                    FConstant c = pow.Function as FConstant;
                    if (!c.IsInteger)
                    {
                        return false;
                    }

                    BigInteger bint = c.BigIntegerValue;
                    if (bint < 0 || bint > long.MaxValue)
                    {
                        return false;
                    }

                    var coeffs = new Dictionary<long, BigDecimal>
                    {
                        [(long)bint] = BigDecimal.One
                    };
                    polynomial = new Polynomial<BigDecimal>(coeffs);
                    return true;
                }
                return false;
            }

            // Handles:
            // 2x^5, 3x^0, 2x(3^5)(x^6)(x^2), 5x^10000x^(-9999)
            if (expr.Function.Type == FType.Multiply)
            {
                polynomial = new Polynomial<BigDecimal>(BigDecimal.One);

                // Loop through all factors
                foreach (Expr factor in expr.Children)
                {
                    if (!TryConvertUnipolynomialInner(factor, out Polynomial<BigDecimal> p))
                    {
                        return false;
                    }
                    polynomial *= p;
                }

                return true;
            }

            return false;
        }

        internal static bool TryConvertExactUnipolynomial(Expr expr, out Polynomial<BigRational> polynomial)
        {
            if (expr is null)
            {
                throw new ArgumentNullException(nameof(expr));
            }

            if (expr.Variables().Count > 1)
            {
                polynomial = default;
                return false;
            }

            // Expand and simplify
            expr = expr.Expand(true);

            return TryConvertExactUnipolynomialInner(expr, out polynomial);
        }
        private static bool TryConvertExactUnipolynomialInner(Expr expr, out Polynomial<BigRational> polynomial)
        {
            polynomial = default;

            if (expr is null || expr.Function is null)
            {
                return false;
            }

            // Handles
            // 4, x
            if (expr.IsLeaf)
            {
                if (expr.Function.Type == FType.Const)
                {
                    polynomial = new Polynomial<BigRational>((expr.Function as FConstant).BigRationalValue);
                    return true;
                }
                if (expr.Function.Type == FType.Var)
                {
                    polynomial = new Polynomial<BigRational>(new Dictionary<long, BigRational>() { [1L] = BigRational.One });
                    return true;
                }
                return false;
            }

            // Handles additive negations
            // E.g. -x
            if (expr.Function.Type == FType.Negate)
            {
                if (!TryConvertExactUnipolynomialInner(expr.FirstArg, out polynomial))
                {
                    return false;
                }
                polynomial = polynomial.AdditiveInverse();
                return true;
            }

            // Handles addition of different polynomial terms
            // E.g. x + x^2 + 1
            if (expr.Function.Type == FType.Add)
            {
                polynomial = new Polynomial<BigRational>();
                foreach (Expr summand in expr.Children)
                {
                    if (!TryConvertExactUnipolynomialInner(summand, out Polynomial<BigRational> term))
                    {
                        polynomial = default;
                        return false;
                    }
                    polynomial += term;
                }
                return true;
            }

            // Handles:
            // x^3, 2^10
            if (expr.Function.Type == FType.Pow)
            {
                Expr bse = expr.FirstArg;
                Expr pow = expr.LastArg;

                // Require power to be an integer
                if (!pow.IsConstant || !pow.IsInteger)
                {
                    return false;
                }

                // E.g. 3^5
                if (bse.IsConstant)
                {
                    polynomial = new Polynomial<BigRational>(BigRational.Pow((bse.Function as FConstant).BigRationalValue, (pow.Function as FConstant).BigIntegerValue));
                    return true;
                }

                // E.g. x^5
                else if (bse.IsVar)
                {
                    FConstant c = pow.Function as FConstant;
                    if (!c.IsInteger)
                    {
                        return false;
                    }

                    BigInteger bint = c.BigIntegerValue;
                    if (bint < 0 || bint > long.MaxValue)
                    {
                        return false;
                    }

                    var coeffs = new Dictionary<long, BigRational>
                    {
                        [(long)bint] = BigRational.One
                    };
                    polynomial = new Polynomial<BigRational>(coeffs);
                    return true;
                }
                return false;
            }

            // Handles:
            // 2x^5, 3x^0, 2x(3^5)(x^6)(x^2), 5x^10000x^(-9999)
            if (expr.Function.Type == FType.Multiply)
            {
                polynomial = new Polynomial<BigRational>(BigRational.One);

                // Loop through all factors
                foreach (Expr factor in expr.Children)
                {
                    if (!TryConvertExactUnipolynomialInner(factor, out Polynomial<BigRational> p))
                    {
                        return false;
                    }
                    polynomial *= p;
                }
                return true;
            }

            return false;
        }

        internal static bool IsRationalFraction(Expr expr)
        {
            if (expr is null) return false;

            // inverse of a polynomial 
            if (expr.Function.Type == FType.Inv)
            {
                return IsRationalFraction(expr.FirstArg);
            }
            if (expr.Function.Type == FType.Negate)
            {
                return IsRationalFraction(expr.FirstArg);
            }

            // Sum of rational functions is rational
            if (expr.Function.Type == FType.Add)
            {
                foreach (Expr summand in expr.Children)
                {
                    if (!IsRationalFraction(summand))
                    {
                        return false;
                    }
                }
                return true;
            }

            // Product of rational fractions is rational
            if (expr.Function.Type == FType.Multiply) 
            { 
                foreach (Expr factor in expr.Children)
                {
                    if (!IsRationalFraction(factor))
                    {
                        return false;
                    }
                }
                return true;
            }

            // Return whether this is a polynomial
            return IsPolynomial(expr);
        }

        // Only perform the division if expr root 
        internal static bool PolynomialDivide(Expr numerator, Expr denominator, out Expr result)
        {
            if (numerator == null || denominator == null)
            {
                throw new ArgumentNullException();
            }

            throw new NotImplementedException();
        }
    }
}
