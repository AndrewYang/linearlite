﻿using LinearNet.Structs;
using LinearNet.Structs.Fields;
using LinearNet.Symbolic.Structs;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace LinearNet.Symbolic
{
    internal static class ExprRational
    {
        internal static bool TrySimplifyRational(Expr expr)
        {
            // Check that this is a rational function
            if (!IsRational(expr))
            {
                return false;
            }

            SplitIntoFraction(expr, out Expr numerator, out Expr denominator);

            // Univariate polynomials are handled separately
            if (ExprPolynomial.TryConvertExactUnipolynomial(numerator, out Polynomial<BigRational> p) &&
                ExprPolynomial.TryConvertExactUnipolynomial(denominator, out Polynomial<BigRational> q))
            {
                Polynomial<BigRational> quotient = p.Divide(q, out Polynomial<BigRational> remainder);

                Expr result = new Expr(quotient) + (Expr.One / new Expr(remainder));
                expr.Function = result.Function;
                expr.Children = result.Children;
                return true;
            }

            // General rational conversions
            if (TryCancelRational(ref numerator, ref denominator))
            {
                expr.Function = new FMultiply(2);
                expr.Children = new Expr[]
                {
                        numerator,
                        Expr.Inv(denominator)
                };
                return true;
            }

            return false;
        }
        private static bool IsRational(Expr expr)
        {
            return expr.Function.Type == FType.Multiply && expr.Children.Any(e => e.Function.Type == FType.Inv);
        }
        private static void SplitIntoFraction(Expr expr, out Expr numerator, out Expr denominator)
        {
            List<Expr> numFactors = new List<Expr>(), denFactors = new List<Expr>();
            foreach (Expr child in expr.Children)
            {
                if (child.Function.Type == FType.Inv)
                {
                    denFactors.Add(child.FirstArg);
                }
                else
                {
                    numFactors.Add(child);
                }
            }

            numerator = (numFactors.Count == 0) ? Expr.One : Expr.Multiply(numFactors);
            denominator = (denFactors.Count == 0) ? Expr.One : Expr.Multiply(denFactors);
        }

        internal static bool TryCancelRational(ref Expr numerator, ref Expr denominator)
        {
            //Debug.WriteLine("Before numerator:\t" + numerator);
            //Debug.WriteLine("Before denominator:\t" + denominator);

            Dictionary<Expr, Expr> numeratorFactors = ToAtomicFactors(numerator), denominatorFactors = ToAtomicFactors(denominator);

            //Debug.WriteLine("Numerator:\t" + string.Join(", ", numeratorFactors.Select(o => "(" + o.Key.ToString() + ")^" + o.Value.ToString())));
            //Debug.WriteLine("Denominator:\t" + string.Join(", ", denominatorFactors.Select(o => "(" + o.Key.ToString() + ")^" + o.Value.ToString())));

            // Cancel factors
            bool cancelled = false;
            foreach (Expr factor in denominatorFactors.Keys.ToList())
            {
                if (numeratorFactors.Contains(factor, out Expr term))
                {
                    numeratorFactors[term] = (numeratorFactors[term] - denominatorFactors[factor]).Simplify();
                    denominatorFactors.Remove(factor);
                    cancelled = true;
                }
            }

            if (!cancelled)
            {
                return false;
            }

            CollectFactors(numeratorFactors, out FConstant constant, out List<Expr> newNumerator);
            CollectFactors(denominatorFactors, out FConstant denConstant, out List<Expr> newDenominator);

            FConstant coeff = (constant.IsFinitePrecision || denConstant.IsFinitePrecision) ?
                                    (FConstant)new FConstantDecimal(constant.BigDecimalValue / denConstant.BigDecimalValue) :
                                    (FConstant)new FConstantRational(constant.BigRationalValue / denConstant.BigRationalValue);

            if (!coeff.IsOne)
            {
                newNumerator.Add(new Expr(coeff));
            }

            if (newNumerator.Count == 0) numerator = Expr.One;
            else if (newNumerator.Count == 1) numerator = newNumerator[0];
            else numerator = Expr.Multiply(newNumerator);

            if (newDenominator.Count == 0) denominator = Expr.One;
            else if (newDenominator.Count == 1) denominator = newDenominator[0];
            else denominator = Expr.Multiply(newDenominator);

            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="expr"></param>
        /// <returns>A collection of factors and their powers.</returns>
        private static Dictionary<Expr, Expr> ToAtomicFactors(Expr expr)
        {
            if (expr.Function.Type == FType.Pow)
            {
                Dictionary<Expr, Expr> baseFactors = ToAtomicFactors(expr.FirstArg);
                foreach (Expr bse in baseFactors.Keys.ToList())
                {
                    baseFactors[bse] = baseFactors[bse].Multiply(expr.LastArg);
                }
                return baseFactors;
            }

            if (expr.Function.Type == FType.Multiply)
            {
                Dictionary<Expr, Expr> factors = new Dictionary<Expr, Expr>();
                foreach (Expr factor in expr.Children)
                {
                    Dictionary<Expr, Expr> childFactors = ToAtomicFactors(factor);
                    foreach (Expr key in childFactors.Keys)
                    {
                        // Check to see if 'key' is in 'factors' - without using the equality comparer 
                        // since it is extremely expensive
                        if (factors.Contains(key, out Expr k))
                        {
                            factors[k] = factors[k].Add(childFactors[key]);
                        }
                        else
                        {
                            factors.Add(key, childFactors[key]);
                        }
                    }
                }
                return factors;
            }

            return new Dictionary<Expr, Expr>() { [expr] = Expr.One };
        }

        private static bool Contains(this Dictionary<Expr, Expr> dict, Expr key, out Expr term)
        {
            foreach (Expr e in dict.Keys)
            {
                // No need to cater for associativity since e is already sorted
                if (e.EqualsExactly(key))
                {
                    term = e;
                    return true;
                }
            }

            term = default;
            return false;
        }

        private static void CollectFactors(Dictionary<Expr, Expr> factors, out FConstant constant, out List<Expr> newNumerator)
        {
            // Set the numerator
            constant = new FConstantRational(1);
            bool const_is_exact = true;

            newNumerator = new List<Expr>();
            foreach (KeyValuePair<Expr, Expr> num in factors)
            {
                // Take care of constant terms first
                if (num.Key.IsConstant && num.Value.IsConstant)
                {
                    FConstant b = num.Key.Function as FConstant;
                    FConstant p = num.Value.Function as FConstant;

                    // Inexact arithmetic
                    if (!const_is_exact || b.IsFinitePrecision || p.IsFinitePrecision || !p.IsInteger)
                    {
                        constant = new FConstantDecimal(constant.BigDecimalValue * BigDecimal.Pow(b.BigDecimalValue, p.BigDecimalValue));
                        const_is_exact = false;
                    }

                    // Exact arithmetic
                    else
                    {
                        constant = new FConstantRational(constant.BigRationalValue * BigRational.Pow(b.BigRationalValue, p.BigIntegerValue));
                    }
                }
                else
                {
                    if (num.Value.IsZero)
                    {
                        // Don't add
                    }
                    else if (num.Value.IsOne)
                    {
                        newNumerator.Add(num.Key);
                    }
                    else
                    {
                        newNumerator.Add(Expr.Pow(num.Key, num.Value));
                    }
                }
            }
        }
    }
}
