﻿using LinearNet.Structs.Fields;
using LinearNet.Symbolic.Structs;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Symbolic
{
    internal class ExprSimplify
    {
        /// <summary>
        /// To improve efficiency, there are certain simplifications which are only carried out 
        /// once (at the beginning, such as replacing exp(x) -> e^x) and others are carried out 
        /// repeatedly.
        /// </summary>
        /// <param name="expr"></param>
        /// <param name="expand"></param>
        /// <returns></returns>
        internal static bool Simplify(Expr expr, bool expand = true)
        {
            bool changed = false;

            // Initial simplification - only called once
            changed = PreSimplify(expr) || changed;

            // Before any expansion, take care of any rational simplifications
            ExprCompare.ToCanonicalSortOrder(expr);
            changed = ExprRational.TrySimplifyRational(expr) || changed;

            if (expand)
            {
                if (ExpressionExpand.Expand(expr, false))
                {
                    ExprCompare.ToCanonicalSortOrder(expr);
                    changed = true;
                }
            }
            
            // Main simplification - called recursively until 
            // no change is made.
            while (TrySimplify(expr))
            {
                ExprCompare.ToCanonicalSortOrder(expr);
                changed = true;
            }

            ExprCompare.ToCanonicalSortOrder(expr);

            // Final simplification e.g. -1 -> negate, only called once
            changed = PostSimplify(expr) || changed;

            return changed;
        }

        /// <summary>
        /// The simplifications that only need to be done once
        /// </summary>
        /// <param name="expr"></param>
        /// <returns></returns>
        private static bool PreSimplify(Expr expr)
        {
            bool changed = false;

            if (!expr.IsLeaf)
            {
                foreach (Expr child in expr.Children)
                {
                    bool ch = PreSimplify(child);
                    changed = ch || changed;
                }
            }

            // Replace exp(x) -> e^x (as a power)
            if (expr.Function.Type == FType.Exp)
            {
                Expr power = expr.FirstArg;
                expr.Function = new FPow();
                expr.Children = new Expr[]
                {
                    new Expr(new FConstantSpecial(ConstantName.E)),
                    power
                };

                return true;
            }

            // Replace negations with multiplications of -1. This allows us to handle 
            // like term collection more effectively
            if (expr.Function.Type == FType.Negate)
            {
                expr.Function = new FMultiply(2);
                expr.Children = new Expr[]
                {
                    new Expr(new FConstantRational(-1)),
                    expr.FirstArg
                };

                return true;
            }

            return changed;
        }

        /// <summary>
        /// The simplifications that take place after the main simplification loop
        /// </summary>
        /// <param name="expr"></param>
        /// <returns></returns>
        private static bool PostSimplify(Expr expr)
        {
            bool modified = false;

            if (!expr.IsLeaf)
            {
                foreach (Expr child in expr.Children)
                {
                    modified = PostSimplify(child) || modified;
                }
            }

            // Replace -1 with inverses
            if (expr.Function.Type == FType.Multiply)
            {
                for (int i = 0; i < expr.Children.Length; ++i)
                {
                    Expr child = expr.Children[i];
                    if (child.Function.Type == FType.Const)
                    {
                        FConstant c = child.Function as FConstant;
                        if (c.IsInteger && c.BigIntegerValue == -1)
                        {
                            expr.Children = expr.Children.RemoveAt(i);
                            if (expr.Children.Length == 1)
                            {
                                expr.Function = new FNegate();
                                // leaving the children unchanged
                            }
                            else
                            {
                                expr.Function = new FNegate();
                                expr.Children = new Expr[] { Expr.Multiply(expr.Children) };
                            }
                            modified = true;
                            break;
                        }
                    }
                }
            }

            return modified;
        }
        /// <summary>
        /// Simplifies the expression using a rule set
        /// </summary>
        /// <returns>Whether a change was made.</returns>
        private static bool TrySimplify(Expr expr)
        {
            if (!expr.IsLeaf)
            {
                bool changeMade = false;

                if (FlattenAssociativeOperations(expr)) changeMade = true;

                if (TryEvaluateConstants(expr)) changeMade = true;
                
                if (TryCollectLikeTerms(expr)) changeMade = true;

                if (TryConvertToIndices(expr)) changeMade = true;
                if (TryApplyPowerLaws(expr)) changeMade = true;
                
                if (TrySimplifyNegations(expr)) changeMade = true;
                if (TryCancelFunctionInverses(expr)) changeMade = true;

                if (TryReduceArbitraryLengthFunctions(expr)) changeMade = true;

                // Otherwise, try all child elements
                if (expr.Children != null)
                {
                    for (int i = 0; i < expr.Children.Length; ++i)
                    {
                        if (TrySimplify(expr.Children[i]))
                        {
                            changeMade = true;
                        }
                    }
                }
                if (changeMade) return true;
            }
            return false;
        }

        #region Constants
        /// <summary>
        /// 1 + 2 -> 3
        /// sin(5) -> ??
        /// </summary>
        private static bool TryEvaluateConstants(Expr expr)
        {
            if (TryCompleteEvaluation(expr))
            {
                return true;
            }

            if (TryPartialEvaluation(expr))
            {
                return true;
            }

            if (TrySimplifyConstantMultiplications(expr))
            {
                return true;
            }

            if (TrySimplifyConstantAdditions(expr))
            {
                return true;
            }
            return false;
        }
        private static bool TryCompleteEvaluation(Expr expr)
        {
            // All child expressions are leaves, and all are known values.
            // Then -> replace this with known value.
            if (expr.Children.All(e => e.IsLeaf && e.Function.Type == FType.Const))
            {
                // The default operation is: if any of the arguments of the function 
                // is a decimal, then the entire operation will be treated as a finite
                // precision operation, i.e. the function will be evaluated in decimal
                // form. However, if all arguments are rationals, then an exact answer 
                // will be attempted to be calculated. If this is not possible, then 
                // no change will be made.
                if (expr.Children.Any(e => (e.Function as FConstant).IsFinitePrecision))
                {
                    // evaluate as a decimal
                    BigDecimal[] values = expr.Children.Select(e => (e.Function as FConstant).BigDecimalValue).ToArray();
                    if (expr.Function.TryEvaluate(values, out FConstantDecimal result))
                    {
                        expr.Function = result;
                        expr.Children = null;
                        return true;
                    }
                }

                if (expr.Children.All(e => (e.Function as FConstant).IsRational))
                {
                    // attempt to evaluate as a rational
                    BigRational[] values = expr.Children.Select(e => (e.Function as FConstantRational).Value).ToArray();
                    if (expr.Function.TryEvaluate(values, out FConstantRational result))
                    {
                        expr.Function = result;
                        expr.Children = null;
                        return true;
                    }
                }
            }
            return false;
        }
        private static bool TryPartialEvaluation(Expr expr)
        {
            // Partial evaluation is possible for special functions, e.g. 1 + x + 2 = 3 + x
            if (expr.Function.Type == FType.Add)
            {
                if (expr.Children.Count(e => e.IsConstant) > 1)
                {
                    // the remaining expressions
                    List<Expr> exprs = new List<Expr>();

                    // Determine whether we should treat it as a BigDecimal or BigRational sum
                    IEnumerable<FConstant> summands = expr.Children.Where(e => e.IsConstant).Select(f => f.Function as FConstant);

                    // Use decimal summation
                    if (summands.Any(s => s.ConstantType == ConstantType.DECIMAL))
                    {
                        separate_based_on_cond(expr.Children, e => e.IsConstant, out List<Expr> constants, out exprs);
                        BigDecimal sum = BigDecimal.Sum(constants.Select(c => c.BigDecimalValue));
                        if (!sum.IsZero)
                        {
                            exprs.Insert(0, new Expr(new FConstantDecimal(sum)));
                        }
                    }

                    // Use rational summation
                    else
                    {
                        separate_based_on_cond(expr.Children, e => e.IsRational, out List<Expr> rationals, out exprs);
                        BigRational sum = BigRational.Sum(rationals.Select(c => c.BigRationalValue));
                        if (!sum.IsZero)
                        {
                            exprs.Insert(0, new Expr(new FConstantRational(sum)));
                        }
                    }

                    // Check if an improvement has been made
                    if (exprs.Count < expr.Children.Length)
                    {
                        expr.Function = new FAdd(exprs.Count);
                        expr.Children = exprs.ToArray();
                        return true;
                    }
                }
                return false;
            }

            if (expr.Function.Type == FType.Multiply)
            {
                if (expr.Children.Count(e => e.IsConstant) > 1)
                {
                    // the remaining expressions
                    List<Expr> exprs = new List<Expr>();

                    // Determine whether we should treat it as a BigDecimal or BigRational sum
                    IEnumerable<FConstant> factors = expr.Children.Where(e => e.IsConstant).Select(f => f.Function as FConstant);

                    // Using BigDecimal products
                    if (factors.Any(s => s.ConstantType == ConstantType.DECIMAL))
                    {
                        separate_based_on_cond(expr.Children, x => x.IsConstant, out List<Expr> constants, out exprs);
                        BigDecimal product = BigDecimal.Product(constants.Select(c => c.BigDecimalValue));
                        if (!product.Equals(BigDecimal.One))
                        {
                            exprs.Insert(0, new Expr(new FConstantDecimal(product)));
                        }
                    }

                    // Using BigRational products
                    else
                    {
                        separate_based_on_cond(expr.Children, x => x.IsRational, out List<Expr> rationals, out exprs);
                        BigRational product = BigRational.Product(rationals.Select(r => r.BigRationalValue));
                        if (!product.Equals(BigRational.One))
                        {
                            exprs.Insert(0, new Expr(new FConstantRational(product)));
                        }
                    }

                    // Check to see if improvement has been made
                    if (exprs.Count < expr.Children.Length)
                    {
                        expr.Function = new FMultiply(exprs.Count);
                        expr.Children = exprs.ToArray();
                        return true;
                    }
                }
                return false;
            }

            return false;
        }
        /// <summary>
        /// Simplifies multiplication with 2 arguments, where 1 is a constant and 1 is a variable.
        /// 0 * x -> 0, x * 0 -> 0
        /// 1 * x -> x
        /// The case where both arguments are constants is handled by the method above.
        /// </summary>
        private static bool TrySimplifyConstantMultiplications(Expr expr)
        {
            if (expr.Function.Type == FType.Multiply)
            {
                bool changed = false;

                // If there are any 0's, the entire product is 0.
                if (expr.Children.Any(e => e.IsZero))
                {
                    // By default, make the 0 exact
                    expr.Function = new FConstantRational(0);
                    expr.Children = new Expr[0];
                    changed = true;
                }

                // If there are any 1's remove them
                if (expr.Children.Any(e => e.IsOne))
                {
                    IEnumerable<Expr> remaining = expr.Children.Where(e => !e.IsOne).ToArray();

                    int count = remaining.Count();
                    if (count == 0)
                    {
                        // no factors - product is exactly 1.
                        expr.Function = new FConstantRational(1);
                        expr.Children = new Expr[0];
                    }
                    else if (count == 1) // 1 factor - product is just that factor (remove the product)
                    {
                        Expr replace = remaining.First();
                        expr.Function = replace.Function;
                        expr.Children = replace.Children;
                    }
                    else
                    {
                        expr.Children = remaining.ToArray();
                    }
                    changed = true;
                }
                return changed;
            }
            return false;
        }
        /// <summary>
        /// Simplifies addition with 2 or more arguments by removing 0's
        /// 0 + f(x) -> f(x)
        /// </summary>
        private static bool TrySimplifyConstantAdditions(Expr expr)
        {
            if (expr.Function.Type == FType.Add)
            {
                // Check for any of 0's
                if (expr.Children.Any(e => e.IsZero))
                {
                    IEnumerable<Expr> simplified = expr.Children.Where(e => !e.IsZero);

                    // no summands left - make base = 0
                    if (!simplified.Any())
                    {
                        // Default to using exact form (i.e. as a BigRational)
                        expr.Function = new FConstantRational();
                        expr.Children = new Expr[0];
                    }
                    // only 1 summand - made the base this summand
                    else if (simplified.Count() == 1)
                    {
                        Expr e = simplified.First();
                        expr.Function = e.Function;
                        expr.Children = e.Children;
                    }
                    else
                    {
                        expr.Children = simplified.ToArray();
                    }

                    return true;
                }
            }
            return false;
        }
        /// <summary>
        /// Separates the elements of 'terms' into two lists - first list contains all terms
        /// that satisfy a conditional, second list contains all terms that do not satisfy a 
        /// conditional.
        /// </summary>
        /// <param name="terms"></param>
        /// <param name="condition"></param>
        /// <param name="matches"></param>
        /// <param name="notMatches"></param>
        private static void separate_based_on_cond(Expr[] terms, Func<Expr, bool> condition, out List<Expr> matches, out List<Expr> notMatches)
        {
            matches = new List<Expr>();
            notMatches = new List<Expr>();

            foreach (Expr e in terms)
            {
                if (condition(e))
                {
                    matches.Add(e);
                }
                else
                {
                    notMatches.Add(e);
                }
            }
        }

        #endregion

        #region Index laws
        /// <summary>
        /// 
        /// Part 1) constant laws:
        /// 0^x -> 0
        /// 1^x -> 1
        /// x^0 -> 1 unless if x = 0, in which case -> NaN
        /// x^1 -> x
        /// 
        /// Part 2) dynamic laws:
        /// 2a) x^a * x^b -> x^(a+b)
        /// 2b) (x^a)^b -> x^(ab)
        /// 2c) (xy)^n -> x^n * y^n ... also implemented in expansion, ... (x / y)^n -> x^n / y^n
        /// 2d) inv(x^n) -> x^(-n)
        /// </summary>
        /// <returns></returns>
        private static bool TryApplyPowerLaws(Expr expr)
        {
            if (TryApplyConstantPowerLaws(expr))
            {
                return true;
            }

            if (TryApplyPowerLaw2a(expr))
            {
                return true;
            }

            if (TryApplyPowerLaw2b(expr))
            {
                return true;
            }

            if (TryApplyPowerLaw2c(expr))
            {
                return true;
            }

            if (TryApplyPowerLaw2d(expr))
            {
                return true;
            }

            return false;
        }
        private static bool TryApplyConstantPowerLaws(Expr expr)
        {
            if (expr.Function.Type == FType.Pow)
            {
                Expr bse = expr.Children[0];
                Expr power = expr.Children[1];

                // Use 'b' as a template for creation of replacement constants
                // e.g. if b in Q, then expr will be in Q too. 
                if (bse.IsConstant)
                {
                    FConstant b = bse.Function as FConstant;

                    // 0^x -> 0 for all x
                    if (b.IsZero)
                    {
                        expr.Function = b.Zero;
                        expr.Children = null;
                        return true;
                    }

                    // 1^x -> 1 for all x
                    if (b.IsOne)
                    {
                        expr.Function = b.One;
                        expr.Children = null;
                        return true;
                    }
                }

                if (power.IsConstant)
                {
                    FConstant v = power.Function as FConstant;

                    // x^0 -> 1 for all x
                    if (v.IsZero)
                    {
                        if (bse.Function is FConstant && (bse.Function as FConstant).IsZero)
                        {
                            expr.Function = v.NaN;
                            expr.Children = null;
                            return true;
                        }
                        else
                        {
                            expr.Function = v.One;
                            expr.Children = null;
                            return true;
                        }
                    }

                    // x^1 -> x for all x
                    if (v.IsOne)
                    {
                        expr.Function = bse.Function;
                        expr.Children = bse.Children;
                        return true;
                    }
                }
            }

            return false;
        }
        // 2a) x^a * x^b -> x^(a+b)
        private static bool TryApplyPowerLaw2a(Expr expr)
        {
            if (expr.Function.Type != FType.Multiply)
            {
                return false;
            }

            // x^a * x^b -> x^(a + b)

            // powers are stored as hash strings, and a lookup dictionary
            // that maps hash strings -> expression objects
            Dictionary<string, List<Expr>> powers = new Dictionary<string, List<Expr>>();
            Dictionary<string, Expr> hashLookup = new Dictionary<string, Expr>();
            foreach (Expr e in expr.Children)
            {
                Expr bse, power;
                if (e.Function.Type == FType.Pow)
                {
                    bse = e.FirstArg;
                    power = e.LastArg;
                }
                else
                {
                    bse = e;
                    power = 1;
                }

                string hash = bse.ToHashString();
                if (powers.ContainsKey(hash))
                {
                    powers[hash].Add(power);
                }
                else
                {
                    powers[hash] = new List<Expr>() { power };
                    hashLookup[hash] = bse;
                }
            }

            // Contains a^n, b^n in above example
            List<Expr> factors = new List<Expr>();
            foreach (KeyValuePair<string, List<Expr>> factor in powers)
            {
                Expr bse = hashLookup[factor.Key];

                if (factor.Value.Count == 1)
                {
                    Expr power = factor.Value[0];

                    if (power.Function.Type == FType.Const)
                    {
                        FConstant c = power.Function as FConstant;
                        if (c.IsZero)
                        {
                            continue;
                        }
                        else if (c.IsOne)
                        {
                            factors.Add(bse);
                        }
                        else
                        {
                            factors.Add(bse ^ power);
                        }
                    }
                    else
                    {
                        factors.Add(bse ^ power);
                    }
                }

                // Multiple exponents added together
                else
                {
                    factors.Add(bse ^ new Expr(new FAdd(factor.Value.Count), factor.Value.ToArray()));
                }
            }

            // Define a change if the original factor count != the current factor count
            bool changed = expr.Children.Length != factors.Count;
            if (changed)
            {
                if (factors.Count == 0)
                {
                    expr.Function = new FConstantRational(1);
                    expr.Children = null;
                }
                else if (factors.Count == 1)
                {
                    Expr factor = factors[0];
                    expr.Function = factor.Function;
                    expr.Children = factor.Children;
                }
                else
                {
                    expr.Function = new FMultiply(factors.Count);
                    expr.Children = factors.ToArray();
                }
                return true;
            }

            return false;
        }
        // 2b) (x^a)^b -> x^(ab)
        private static bool TryApplyPowerLaw2b(Expr expr)
        {
            if (expr.Function.Type == FType.Pow)
            {
                Expr inner = expr.FirstArg;
                if (inner.Function.Type == FType.Pow)
                {
                    expr.Children[0] = inner.FirstArg;
                    expr.Children[1] = expr.LastArg * inner.LastArg;
                    return true;
                }
            }

            return false;
        }
        // 2c) (xy)^n -> x^n * y^n
        private static bool TryApplyPowerLaw2c(Expr expr)
        {
            if (expr.Function.Type != FType.Pow)
            {
                return false;
            }

            Expr bse = expr.FirstArg;
            if (bse.Function.Type != FType.Multiply)
            {
                return false;
            }

            Expr power = expr.LastArg;

            // Replace each base factor with their exponentiated version
            for (int i = 0; i < bse.Children.Length; ++i)
            {
                bse.Children[i] = bse.Children[i] ^ power;
            }

            // Set the root function to multiply
            expr.Function = new FMultiply(bse.Children.Length);
            expr.Children = bse.Children;

            return true;
        }
        // 2d) inv(x^n) -> x^(neg(n))
        private static bool TryApplyPowerLaw2d(Expr expr)
        {
            if (expr.Function.Type != FType.Inv)
            {
                return false;
            }

            Expr inv = expr.FirstArg;
            if (inv.Function.Type != FType.Pow)
            {
                return false;
            }

            Expr bse = inv.FirstArg,
                pow = inv.LastArg;

            expr.Function = new FPow();
            expr.Children = new Expr[]
            {
                bse,
                -pow
            };

            return true;
        }

        /// <summary>
        /// x * x -> x^2
        /// x * x * y -> x^2 * y
        /// Note: this method is currently O(n^2), whereas a O(n log n) method exists
        /// </summary>
        /// <returns></returns>
        private static bool TryConvertToIndices(Expr expr)
        {
            if (TryConvertMultiplicationToExponents(expr))
            {
                return true;
            }

            if (TryConvertInverseToIndices(expr))
            {
                return true;
            }
            
            return false;
        }
        // x * x * y -> x^2 * y
        private static bool TryConvertMultiplicationToExponents(Expr expr)
        {
            if (expr.Function.Type != FType.Multiply)
            {
                return false;
            }

            int factorCount = expr.Children.Length;
            if (factorCount == 0)
            {
                throw new NotImplementedException();
            }

            // Track the powers of each term
            var powers = new Dictionary<string, int>();
            var hashLookup = new Dictionary<string, Expr>();
            foreach (Expr child in expr.Children)
            {
                string hash = child.ToHashString();
                if (powers.ContainsKey(hash))
                {
                    powers[hash]++;
                }
                else
                {
                    powers[hash] = 1;
                    hashLookup[hash] = child;
                }
            }

            if (powers.Count == 1)
            {
                Expr bse = expr.Children[0];
                int power = powers[bse.ToHashString()];

                if (power == 0)
                {
                    // x^0 = 1
                    expr.Function = new FConstantRational(1);
                    expr.Children = null;
                }
                else if (power == 1)
                {
                    // x^1 = x, copy from bse
                    expr.Function = bse.Function;
                    expr.Children = bse.Children;
                }
                else
                {
                    expr.Function = new FPow();
                    expr.Children = new Expr[2]
                    {
                         bse,
                         new Expr(new FConstantRational(power))
                    };
                }
            }
            else
            {
                List<Expr> uniqFactors = new List<Expr>();
                foreach (KeyValuePair<string, int> pair in powers)
                {
                    if (pair.Value == 0) continue;

                    if (pair.Value == 1)
                    {
                        uniqFactors.Add(hashLookup[pair.Key]);
                    }
                    else
                    {
                        uniqFactors.Add(new Expr(new FPow(), hashLookup[pair.Key], pair.Value));
                    }
                }

                // rare but its possible that all unique factors have power 0 -> result is 1
                if (uniqFactors.Count == 0)
                {
                    expr.Function = new FConstantRational(1);
                    expr.Children = new Expr[0];
                }
                else
                {
                    expr.Function = new FMultiply(uniqFactors.Count);
                    expr.Children = uniqFactors.ToArray();
                }
            }

            // We count it as a change if the number of factors have changed.
            if (expr.IsLeaf)
            {
                return factorCount == 1;
            }
            return factorCount != expr.Children.Length;
        }
        // inv(x) -> x^-1
        private static bool TryConvertInverseToIndices(Expr expr)
        {
            if (expr.Function.Type != FType.Inv)
            {
                return false;
            }

            expr.Function = new FPow();
            expr.Children = new Expr[]
            {
                expr.FirstArg, 
                -1
            };

            return true;
        }
        #endregion

        #region Negations
        /// <summary>
        /// -(-x) -> x
        /// *(-x)(y) -> -(x*y)
        /// </summary>
        /// <returns></returns>
        private static bool TrySimplifyNegations(Expr expr)
        {
            // Take care of double negations -(-x) -> x
            if (expr.Function.Type == FType.Negate && expr.Children[0].Function.Type == FType.Negate)
            {
                Expr replacement = expr.Children[0].Children[0];
                expr.Function = replacement.Function;
                expr.Children = replacement.Children;
                return true;
            }

            if (TrySimplifyMultipliedNegations(expr))
            {
                return true;
            }

            if (TrySimplifyNegationPowers(expr))
            {
                return true;
            }
            return false;
        }
        // (-x) * y -> -(x * y)
        private static bool TrySimplifyMultipliedNegations(Expr expr)
        {
            if (expr.Function.Type != FType.Multiply)
            {
                return false;
            }

            if (!expr.Children.Any(e => e.Function.Type == FType.Negate))
            {
                return false;
            }

            // Collect all negations into a single sign, simplifying as we go
            int sign = 1;
            for (int i = 0; i < expr.Children.Length; ++i)
            {
                Expr e = expr.Children[i];
                if (e.Function.Type == FType.Negate)
                {
                    // remove the negation and save it
                    sign *= -1;
                    expr.Children[i] = e.FirstArg;
                }
            }

            if (sign < 0)
            {
                expr.Function = new FNegate();
                expr.Children = new Expr[] { new Expr(new FMultiply(expr.Children.Length), expr.Children) };
            }
            else
            {
                expr.Function = new FMultiply(expr.Children.Length);
                // ChildExpressions already set...
            }
            return true;
        }
        // (-x)^(2k) -> x^(2k), (-x)^(2k + 1) -> -(x^(2k + 1))
        private static bool TrySimplifyNegationPowers(Expr expr)
        {
            if (expr.Function.Type != FType.Pow)
            {
                return false;
            }

            Expr bse = expr.FirstArg,
                pow = expr.LastArg;

            if (bse.Function.Type != FType.Negate)
            {
                return false;
            }

            // Only handle integer powers for simplification purposes
            if (!pow.IsConstant)
            {
                return false;
            }

            FConstant exponent = pow.Function as FConstant;
            if (!exponent.IsInteger)
            {
                return false;
            }

            if (exponent.BigIntegerValue % 2 == 0)
            {
                // Set the base: (-x)^n -> x^n
                expr.Children[0] = bse.FirstArg;
            }
            else
            {
                // Set: (-x)^n -> -(x^n)
                expr.Function = new FNegate();
                expr.Children = new Expr[]
                {
                    new Expr(new FPow(), new Expr[]
                    {
                        bse.FirstArg,
                        pow
                    })
                };
            }
            return true;
        }
        #endregion

        #region Associative operators
        /// <summary>
        /// Flatten all associative operations (+, *) into a single level
        /// </summary>
        /// <param name="expr"></param>
        /// <returns></returns>
        public static bool FlattenAssociativeOperations(Expr expr)
        {
            bool changed = FlattenAssociativeOperationsWithoutSort(expr);
            ExprCompare.ToCanonicalSortOrder(expr);
            return changed;
        }
        /// <summary>
        /// Sorting is expensive, we only do it once 
        /// </summary>
        private static bool FlattenAssociativeOperationsWithoutSort(Expr expr)
        {
            if (expr.IsLeaf)
            {
                return false;
            }

            bool changed = false;

            // Must re-order sub-expressions first, since their ordering can influence 
            // upper levels
            // Also note that to guarantee we are looping through all the children, we must 
            // perform this loop regardless of whether the function itself is associative...
            foreach (Expr child in expr.Children)
            {
                changed = FlattenAssociativeOperationsWithoutSort(child) || changed;
            }

            while (TryFlattenAddition(expr)) changed = true;
            while (TryFlattenMultiplications(expr)) changed = true;
            return changed;
        }
        /// <summary>
        /// 1 + (2 + x) -> 1 + 2 + x (single level)
        /// </summary>
        /// <returns></returns>
        private static bool TryFlattenAddition(Expr expr)
        {
            if (expr.Function.Type == FType.Add)
            {
                if (expr.Children.Any(e => e.Function.Type == FType.Add))
                {
                    List<Expr> summands = new List<Expr>();
                    foreach (Expr child in expr.Children)
                    {
                        if (child.Function.Type == FType.Add)
                        {
                            summands.AddRange(child.Children);
                        }
                        else
                        {
                            summands.Add(child);
                        }
                    }

                    expr.Function = new FAdd(summands.Count);
                    expr.Children = summands.ToArray();
                    return true;
                }
            }
            return false;
        }
        /// <summary>
        /// 1 * (2 * x) -> 1 * 2 * x (single level)
        /// </summary>
        /// <returns></returns>
        private static bool TryFlattenMultiplications(Expr expr)
        {
            if (expr.Function.Type != FType.Multiply)
            {
                return false;
            }

            if (!expr.Children.Any(e => e.Function.Type == FType.Multiply))
            {
                return false;
            }

            List<Expr> factors = new List<Expr>();
            foreach (Expr child in expr.Children)
            {
                if (child.Function.Type == FType.Multiply)
                {
                    factors.AddRange(child.Children);
                }
                else
                {
                    factors.Add(child);
                }
            }

            expr.Function = new FMultiply(factors.Count);
            expr.Children = factors.ToArray();

            return true;
        }
        #endregion

        #region Arbitrary-dimensional functions
        // (+) f -> f
        private static bool TryReduceArbitraryLengthFunctions(Expr expr)
        {
            if (expr.Function.Type == FType.Add)
            {
                if (expr.NumArguments == 1)
                {
                    Expr child = expr.FirstArg;
                    expr.Function = child.Function;
                    expr.Children = child.Children;
                    return true;
                }
                if (expr.NumArguments == 0)
                {
                    expr.Function = new FConstantRational(0);
                    expr.Children = null;
                    return true;
                }
            }

            else if (expr.Function.Type == FType.Multiply)
            {
                if (expr.NumArguments == 1)
                {
                    Expr child = expr.FirstArg;
                    expr.Function = child.Function;
                    expr.Children = child.Children;
                    return true;
                }

                if (expr.NumArguments == 0)
                {
                    expr.Function = new FConstantRational(1);
                    expr.Children = null;
                    return true;
                }
            }

            return false;
        }
        #endregion

        /// <summary>
        /// f^-1(f(x)) -> x.
        /// x * (1 / x) -> 1
        /// Requires us to keep a up-to-date list of functions and their inverses
        /// </summary>
        /// <returns></returns>
        private static bool TryCancelFunctionInverses(Expr expr)
        {
            if (expr.IsLeaf) return false;
            
            if (expr.Function.Type.IsInvertible() &&
                expr.Function.Type.Invert() == expr.Children[0].Function.Type)
            {
                Expr replace = expr.Children[0].Children[0];
                expr.Function = replace.Function;
                expr.Children = replace.Children;

                return true;
            }
            
            if (expr.Function.Type == FType.Multiply)
            {
                // this only handles direct cancellations for now
                // also - this is O(n^2)... - go for O(n log n)

                for (int i = 0; i < expr.Children.Length; ++i)
                {
                    if (expr.Children[i].Function.Type == FType.Inv)
                    {
                        // Find another element equal to this
                        Expr e = expr.Children[i].Children[0];
                        for (int j = 0; j < expr.Children.Length; ++j)
                        {
                            if (i == j) continue;

                            // direct equality case
                            if (e == expr.Children[j])
                            {
                                // Remove at both i and j
                            }
                        }
                    }
                }
            }
            return false;
        }
        
        // xy + 2xy -> 3xy
        private static bool TryCollectLikeTerms(Expr expr)
        {
            if (expr.Function.Type != FType.Add)
            {
                return false;
            }

            var collectedTerms = new Dictionary<Expr, List<Expr>>();
            foreach (Expr child in expr.Children)
            {
                bool added = false;
                foreach (Expr key in collectedTerms.Keys)
                {
                    if (child.Like(key))
                    {
                        collectedTerms[key].Add(child);
                        added = true;
                        break;
                    }
                }

                if (!added)
                {
                    collectedTerms[child] = new List<Expr>() { child };
                }
            }

            Expr[] summands = new Expr[collectedTerms.Count];
            int i = 0;
            foreach (KeyValuePair<Expr, List<Expr>> terms in collectedTerms)
            {
                if (terms.Value.Count == 0)
                {
                    throw new InvalidOperationException();
                }

                // Determine whether the computation can be carried out in exact terms
                bool useExact = true;
                foreach (Expr t in terms.Value)
                {
                    if (t.Function.Type == FType.Multiply)
                    {
                        foreach (Expr factor in t.Children)
                        {
                            if (factor.IsConstant && (factor.Function as FConstant).IsFinitePrecision)
                            {
                                useExact = false;
                                goto start;
                            }
                        }
                    }
                }

            start:

                Expr coeff;
                if (useExact)
                {
                    BigRational val = BigRational.Zero;
                    foreach (Expr t in terms.Value)
                    {
                        if (t.Function.Type == FType.Multiply)
                        {
                            BigRational c = BigRational.One;
                            foreach (Expr factor in t.Children)
                            {
                                if (factor.IsRational)
                                {
                                    c *= (factor.Function as FConstantRational).Value;
                                }
                            }
                            val += c;
                        }
                        else
                        {
                            val += 1;
                        }
                    }
                    coeff = new Expr(new FConstantRational(val));
                }
                else
                {
                    BigDecimal val = new BigDecimal(0, Expr.DefaultSignificantFigures);
                    foreach (Expr t in terms.Value)
                    {
                        if (t.Function.Type == FType.Multiply)
                        {
                            BigDecimal c = new BigDecimal(1, Expr.DefaultSignificantFigures);
                            foreach (Expr factor in t.Children)
                            {
                                if (factor.IsConstant)
                                {
                                    c *= (factor.Function as FConstant).BigDecimalValue;
                                }
                            }
                            val += c;
                        }
                        else
                        {
                            val += new BigDecimal(1, Expr.DefaultSignificantFigures);
                        }
                    }
                    coeff = new Expr(new FConstantDecimal(val));
                }

                // Get the non-coefficient part of the 1st term
                Expr first = terms.Value[0].WithoutCoefficient();
                if (first is null)
                {
                    summands[i++] = coeff;
                }
                else
                {
                    summands[i++] = coeff * first;
                }
            }


            if (summands.Length != expr.Children.Length)
            {
                expr.Children = summands;
                return true;
            }

            return false;
        }
    }
}
