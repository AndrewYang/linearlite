﻿using LinearNet.Global;
using LinearNet.Structs.Fields;
using LinearNet.Symbolic.Structs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;

namespace LinearNet.Symbolic
{
    /// <summary>
    /// All the static methods go in this file
    /// </summary>
    /// <cat>symbolic-algebra</cat>
    public partial class Expr
    {
        internal static int SignificantFigures = BigDecimal.DEFAULT_SIG_FIGS;

        /// <summary>
        /// <p>
        /// Gets or sets the default number of significant figures in which finite-precision
        /// arithmetic involving constants should be performed and stored in.
        /// Does not affect infinite-precision structs such as <txt>int</txt> or 
        /// <txt>BigRational</txt>. 
        /// </p>
        /// 
        /// <p>
        /// Once set, all <txt>Expr</txt> objects created afterwards 
        /// will carry the specified number of significant figures, and all operations involving
        /// <txt>BigDecimal</txt>, such as addition/subtraction, trignometric functions, exponentiation, etc
        /// will occur also with that degree of precision. The <txt>Expr</txt> objects 
        /// created beforehand are not affected. 
        /// </p>
        /// 
        /// <p>By default, this value is initialized to the constant <txt>BigDecimal.DEFAULT_SIG_FIGS</txt>.</p>
        /// 
        /// <h4>Example:</h4>
        /// <pre><code class="cs">
        /// Expr numerator = 5, denominator = 7;
        ///
        /// // Store the fraction in exact form
        /// Expr fraction = (numerator / denominator).Simplify();
        ///
        /// Expr.DefaultSignificantFigures = 5;
        /// Debug.WriteLine(fraction.BigDecimalValue); // 0.71428
        ///
        /// Expr.DefaultSignificantFigures = 20;
        /// Debug.WriteLine(fraction.BigDecimalValue); // 0.71428571428571428571
        /// 
        /// // BigDecimal constants are rounded to the correct number of significant 
        /// // figures at construction time:
        /// BigDecimal large_pi = BigDecimal.Pi(1000); // calculate pi to 1,000 decimal places
        /// Expr pi = large_pi;
        /// Debug.WriteLine(pi); // 3.1415926535897932385 (20 decimal places)
        /// 
        /// </code></pre>
        /// </summary>
        public static int DefaultSignificantFigures
        {
            get
            {
                return SignificantFigures;
            }
            set
            {
                if (value <= 0)
                {
                    throw new ArgumentOutOfRangeException(nameof(value), ExceptionMessages.NonPositiveNotAllowed);
                }
                SignificantFigures = value;
            }
        }

        /// <summary>
        /// Returns a new expression object representing the sum of an array of expressions.
        /// </summary>
        /// <param name="summands">An array of summands.</param>
        /// <returns>The sum</returns>
        public static Expr Add(params Expr[] summands)
        {
            if (summands.Length == 0)
            {
                return new Expr(new FConstantRational(0));
            }
            if (summands.Length == 1)
            {
                return summands[0];
            }
            return new Expr(new FAdd(summands.Length), summands);
        }
        public static Expr Add(IEnumerable<Expr> summands) => Add(summands.ToArray());
        public static Expr Multiply(params Expr[] factors)
        {
            if (factors.Length == 0)
            {
                return new Expr(new FConstantRational(1));
            }
            if (factors.Length == 1)
            {
                return factors[0];
            }
            return new Expr(new FMultiply(factors.Length), factors);
        }
        public static Expr Multiply(IEnumerable<Expr> factors) => Multiply(factors.ToArray());
        public static Expr Negate(Expr e)
        {
            return new Expr(new FNegate(), e);
        }
        public static Expr Inv(Expr e)
        {
            return new Expr(new FInv(), e);
        }
        public static Expr Log(Expr e)
        {
            return new Expr(new FLog(), e);
        }
        public static Expr Exp(Expr e)
        {
            return new Expr(new FExp(), e);
        }
        public static Expr Pow(Expr bse, Expr exponent)
        {
            if (exponent.IsOne)
            {
                return bse;
            }
            if (exponent.IsZero)
            {
                return One;
            }
            if (bse.IsZero)
            {
                return Zero;
            }
            if (bse.IsOne)
            {
                return One;
            }
            return new Expr(new FPow(), bse, exponent);
        }
        public static Expr Sqrt(Expr e)
        {
            return new Expr(new FSqrt(), e);
        }
        public static Expr Sin(Expr e)
        {
            return new Expr(new FSin(), e);
        }
        public static Expr Cos(Expr e)
        {
            return new Expr(new FCos(), e);
        }
        public static Expr Tan(Expr e)
        {
            return new Expr(new FTan(), e);
        }
        public static Expr Asin(Expr e)
        {
            return new Expr(new FAsin(), e);
        }
        public static Expr Acos(Expr e)
        {
            return new Expr(new FAcos(), e);
        }
        public static Expr Atan(Expr e)
        {
            return new Expr(new FAtan(), e);
        }
        public static Expr Sinh(Expr e)
        {
            return new Expr(new FSinh(), e);
        }
        public static Expr Cosh(Expr e)
        {
            return new Expr(new FCosh(), e);
        }
        public static Expr Tanh(Expr e)
        {
            return new Expr(new FTanh(), e);
        }
        public static Expr Gamma(Expr e)
        {
            return new Expr(new FGamma(), e);
        }
        public static Expr Beta(Expr a, Expr b)
        {
            return new Expr(new FBeta(), a, b);
        }
        public static Expr Zeta(Expr a)
        {
            return new Expr(new FZeta(), a);
        }
        public static Expr Logit(Expr e)
        {
            return new Expr(new FLogit(), e);
        }
        public static Expr Logistic(Expr e)
        {
            return new Expr(new FLogistic(), e);
        }
        public static Expr Erf(Expr e)
        {
            return new Expr(new FErf(), e);
        }
        public static Expr Erfc(Expr e)
        {
            return new Expr(new FErfc(), e);
        }
    }
}
