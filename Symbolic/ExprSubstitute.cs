﻿using LinearNet.Structs.Fields;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace LinearNet.Symbolic
{
    internal static class ExprSubstitute
    {
        internal static bool Substitute(ref Expr expr, Expr sub, Expr replacement)
        {
            // We have the freedom to simplify
            expr = expr.Simplify();
            sub = sub.Simplify();
            replacement = replacement.Simplify();

            if (SubstituteInner(expr, sub, replacement))
            {
                expr = expr.Simplify();
                return true;
            }
            return false;
        }
        private static bool SubstituteInner(Expr expr, Expr sub, Expr replacement)
        {
            // all expressions have been simplified - exact equality is sufficient
            if (expr.EqualsExactly(sub))
            {
                // replace expr with replacement
                expr.Function = replacement.Function;
                expr.Children = replacement.Children;
                return true;
            }

            // Otherwise, check for substitutes in the children
            if (expr.IsLeaf)
            {
                return false;
            }

            bool replaced = false;
            foreach (Expr child in expr.Children)
            {
                if (SubstituteInner(child, sub, replacement))
                {
                    replaced = true;
                }
            }
            return replaced;
        }
    }
}
