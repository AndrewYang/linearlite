﻿using LinearNet.Functions;
using LinearNet.Structs.Fields;
using LinearNet.Symbolic.Structs;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Numerics;

namespace LinearNet.Symbolic
{
    /// <summary>
    /// Evaluate trig constants
    /// </summary>
    internal class ExprTrigonometryConstants
    {
        internal static readonly BigRational _half = new BigRational(1, 2, false);
        internal static readonly BigRational _third = new BigRational(1, 3, false);

        internal static readonly Expr _pi = new Expr(ConstantName.Pi),
            _sqrt3 = Expr.Sqrt(3),
            _sqrt2 = Expr.Sqrt(2),
            _sqrt3_over_2 = _half * _sqrt3,
            _sqrt2_over_2 = _half * _sqrt2,
            _sqrt3_over_3 = _third * _sqrt3;
        
        internal static bool Evaluate(ref Expr expr)
        {
            if (EvaluateTrigConstants(ref expr)) return true;
            if (EvaluateInverseTrigConstants(ref expr)) return true;

            return false;
        }
        private static bool EvaluateTrigConstants(ref Expr expr)
        {
            bool sin = expr.Function.Type == FType.Sin,
                cos = expr.Function.Type == FType.Cos,
                tan = expr.Function.Type == FType.Tan;


            if (!sin && !cos && !tan)
            {
                return false;
            }

            if (is_rational_pi_multiple(expr.FirstArg, out BigRational multiple))
            {
                // Integer multiples
                if (multiple.IsInteger)
                {
                    if (sin || tan)
                    {
                        expr = 0;
                        return true;
                    }
                    if (cos)
                    {
                        expr = 1 - 2 * (multiple.IntegerValue % 2L);
                        return true;
                    }
                }

                // Integer multiples of pi / 2
                BigRational m = multiple.Divide(new BigRational(1, 2));
                if (m.IsInteger)
                {
                    if (sin)
                    {
                        BigInteger i = m.IntegerValue % 4L;
                        if (i == 1L)
                        {
                            expr = 1;
                            return true;
                        }
                        if (i == 3L)
                        {
                            expr = -1;
                            return true;
                        }
                    }
                    if (cos)
                    {
                        expr = 0;
                        return true;
                    }
                    if (tan)
                    {
                        expr = BigRational.NaN;
                        return true;
                    }
                }

                // Integer multiples of pi / 3
                m = multiple.Divide(new BigRational(1, 3));
                if (m.IsInteger)
                {
                    if (sin)
                    {
                        BigInteger i = m.IntegerValue % 6L;
                        if (i < 3) expr = Expr.Sqrt(3) / 2;
                        else expr = -(Expr.Sqrt(3) / 2);
                        return true;
                    }
                    if (cos)
                    {
                        BigInteger i = m.IntegerValue % 6L;
                        if (i == 1 || i == 5) expr = new BigRational(1, 2);
                        else expr = new BigRational(-1, 2);
                        return true;
                    }
                    if (tan)
                    {
                        BigInteger i = m.IntegerValue % 3L;
                        if (i == 1) expr = Expr.Sqrt(3);
                        else expr = -Expr.Sqrt(3);
                        return true;
                    }
                }

                // Integer multiples pi / 4
                m = multiple.Divide(new BigRational(1, 4));
                if (m.IsInteger)
                {
                    if (sin)
                    {
                        BigInteger i = m.IntegerValue % 8L;
                        if (i < 4) expr = Expr.Sqrt(2) / 2;
                        else expr = -Expr.Sqrt(2) / 2;
                        return true;
                    }
                    if (cos)
                    {
                        BigInteger i = m.IntegerValue % 8L;
                        if (i == 1 || i == 7) expr = Expr.Sqrt(2) / 2;
                        else expr = -Expr.Sqrt(2) / 2;
                        return true;
                    }
                    if (tan)
                    {
                        BigInteger i = m.IntegerValue % 4L;
                        expr = (i == 1) ? 1 : -1;
                        return true;
                    }
                }
            }


            return false;
        }

        private static bool EvaluateInverseTrigConstants(ref Expr expr)
        {
            if (EvaluateInverseSine(ref expr)) return true;
            if (EvaluateInverseCosine(ref expr)) return true;
            if (EvaluateInverseTan(ref expr)) return true;
            return false;
        }
        /// <summary>
        /// Angle -> [-pi / 2, pi / 2]
        /// </summary>
        private static bool EvaluateInverseSine(ref Expr e)
        {
            if (e.Function.Type != FType.Asin)
            {
                return false;
            }

            if (try_get_rational_value(e.FirstArg, out BigRational value))
            {
                if (EvaluateInverseSineFromRational(value, out Expr angle))
                {
                    e = angle;
                    return true;
                }
                return false;
            }

            // If not rational value - try parse exact values
            Expr canonical = e.FirstArg.Simplify();
            if (canonical.EqualsExactly(-_sqrt3_over_2))
            {
                e = -_pi / 3;
                return true;
            }
            if (canonical.EqualsExactly(-_sqrt2_over_2))
            {
                e = -_pi / 4;
                return true;
            }
            if (canonical.EqualsExactly(_sqrt2_over_2))
            {
                e = _pi / 4;
                return true;
            }
            if (canonical.EqualsExactly(_sqrt3_over_2))
            {
                e = _pi / 3;
                return true;
            }

            return false;
        }
        private static bool EvaluateInverseSineFromRational(BigRational argument, out Expr angle)
        {
            if (argument < 0)
            {
                if (EvaluateInverseSineFromRational(-argument, out angle))
                {
                    angle = -angle;
                    return true;
                }
                return false;
            }

            // asin(0) = 0
            if (argument.IsZero)
            {
                angle = 0;
                return true;
            }

            // asin(1/2) = pi / 6
            if (argument.Equals(_half))
            {
                angle = _pi / 6;
                return true;
            }

            // asin(1) = pi / 2
            if (argument.IsOne)
            {
                angle = _pi / 2;
                return true;
            }

            angle = default;
            return false;
        }
        /// <summary>
        /// Angle -> [0, pi]
        /// </summary>
        private static bool EvaluateInverseCosine(ref Expr e)
        {
            if (e.Function.Type == FType.Acos)
            {
                return false;
            }

            if (try_get_rational_value(e.FirstArg, out BigRational value))
            {
                if (EvaluateInverseCosineFromRational(value, out Expr angle))
                {
                    e = angle;
                    return true;
                }
                return false;
            }

            // If not rational value - try parse exact values
            Expr canonical = e.FirstArg.Simplify();
            if (canonical.EqualsExactly(-_sqrt3_over_2))
            {
                e = _pi / 6;
                return true;
            }
            if (canonical.EqualsExactly(-_sqrt2_over_2))
            {
                e = _pi / 4;
                return true;
            }
            if (canonical.EqualsExactly(_sqrt2_over_2))
            {
                e = _pi / 4;
                return true;
            }
            if (canonical.EqualsExactly(_sqrt3_over_2))
            {
                e = _pi / 6;
                return true;
            }

            return false;
        }
        private static bool EvaluateInverseCosineFromRational(BigRational argument, out Expr angle)
        {
            if (argument < 0)
            {
                // Cosine is even i.e. cos(-x) = cos(x)
                return EvaluateInverseCosineFromRational(-argument, out angle);
            }

            if (argument.IsZero)
            {
                angle = _pi / 2;
                return true;
            }
            if (argument.Equals(_half))
            {
                angle = _pi / 3;
                return true;
            }
            if (argument.IsOne)
            {
                angle = 0;
                return true;
            }

            angle = default;
            return false;
        }
        /// <summary>
        /// Angle -> [-pi / 2, pi / 2]
        /// </summary>
        private static bool EvaluateInverseTan(ref Expr e)
        {
            if (e.Function.Type != FType.Atan)
            {
                return false;
            }

            if (try_get_rational_value(e.FirstArg, out BigRational value))
            {
                if (EvaluateInverseTanFromRational(value, out Expr angle))
                {
                    e = angle;
                    return true;
                }
                return false;
            }

            // If not rational value - try parse exact values
            Expr canonical = e.FirstArg.Simplify();
            if (canonical.EqualsExactly(-_sqrt3))
            {
                e = -(_pi / 3);
                return true;
            }
            if (canonical.EqualsExactly(-_sqrt3_over_3))
            {
                e = -(_pi / 6);
                return true;
            }
            if (canonical.EqualsExactly(_sqrt3_over_3))
            {
                e = _pi / 6;
                return true;
            }
            if (canonical.EqualsExactly(_sqrt3))
            {
                e = _pi / 3;
                return true;
            }
            return false;
        }
        private static bool EvaluateInverseTanFromRational(BigRational argument, out Expr angle)
        {
            if (argument < 0)
            {
                // Tan is odd i.e. tan(-x) = -tan(x)
                if (EvaluateInverseTanFromRational(-argument, out angle))
                {
                    angle = -angle;
                    return true;
                }
                return false;
            }

            if (argument.IsZero)
            {
                angle = 0;
                return true;
            }
            if (argument.IsOne)
            {
                angle = Expr.Sqrt(2) / 2;
                return true;
            }

            angle = default;
            return false;
        }

        /// <summary>
        /// TR2: tan(x) -> sin(x) / cos(x)
        /// </summary>
        internal static bool tan_to_sincos(ref Expr e, bool recursive)
        {
            bool changed = false;

            // Simplify children first
            if (!e.IsLeaf && recursive)
            {
                for (int i = 0; i < e.NumArguments; ++i)
                {
                    changed = tan_to_sincos(ref e.Children[i], true) || changed;
                }
            }

            // Handle the function itself
            if (e.Type == FType.Tan)
            {
                e = Expr.Sin(e.FirstArg) / Expr.Cos(e.FirstArg);
                return true;
            }

            if (e.Type == FType.Cot)
            {
                e = Expr.Cos(e.FirstArg) / Expr.Sin(e.FirstArg);
                return true;
            }

            return changed;
        }
        /// <summary>
        /// TR2a: sin(x) / cos(x) -> tan(x)
        /// </summary>
        internal static bool sincos_to_tan(ref Expr e, bool recursive)
        {
            bool changed = false;

            // Simplify children first 
            if (!e.IsLeaf && recursive)
            {
                for (int i = 0; i < e.NumArguments; ++i)
                {
                    changed = sincos_to_tan(ref e.Children[i], true) || changed;
                }
            }

            // sin(f(x)) * (cos(f(x)))^{-1}
            while (replace_sincos_with_tan_once(ref e))
            {
                changed = true;
            }
            return changed;
        }
        internal static bool replace_sincos_with_tan_once(ref Expr e)
        {
            if (e.Type != FType.Multiply)
            {
                return false;
            }

            IEnumerable<Expr> sinFactors = e.Children.Where(f => f.Type == FType.Sin);
            IEnumerable<Expr> cosFactors = e.Children.Where(f => f.Type == FType.Inv && f.FirstArg.Type == FType.Cos)
                                            .Select(g => g.FirstArg);

            foreach (Expr sin in sinFactors)
            {
                Expr arg1 = sin.FirstArg;
                foreach (Expr cos in cosFactors)
                {
                    Expr arg2 = cos.FirstArg;
                    if (arg1.EqualsExactly(arg2))
                    {
                        // Perform the cancellation
                        bool sinFound = false, cosFound = false;

                        List<Expr> factors = e.Children.ToList();
                        for (int i = factors.Count - 1; i >= 0; --i)
                        {
                            Expr child = factors[i];
                            if (!sinFound && child.Type == FType.Sin && child.FirstArg == arg1)
                            {
                                factors.RemoveAt(i);
                                sinFound = true;
                            }

                            if (!cosFound && child.Type == FType.Inv && child.FirstArg.Type == FType.Cos && child.FirstArg.FirstArg == arg2)
                            {
                                factors.RemoveAt(i);
                                cosFound = true;
                            }
                        }

                        factors.Add(Expr.Tan(arg1));

                        if (factors.Count == 0)
                        {
                            e = Expr.Tan(arg1);
                        }
                        else
                        {
                            e = Expr.Multiply(factors);
                        }

                        return true;
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// TR3: 
        /// sin(2n*pi + x) = sin(x), cos(2n*pi + x) + cos(x)
        /// sin((2n - 1)*pi + x) = -sin(x), cos((2n - 1)*pi + x) = -cos(x)
        /// </summary>
        internal static bool mod_arguments_by_period(ref Expr e, bool recursive)
        {
            bool changed = false;

            // Depth first 
            if (!e.IsLeaf && recursive)
            {
                for (int i = 0; i < e.NumArguments; ++i)
                {
                    changed = mod_arguments_by_period(ref e.Children[i], recursive) || changed;
                }
            }

            if (e.Function.Type == FType.Sin)
            {
                Expr angle = e.FirstArg;

                // sin(-x) = -sin(x)
                if (angle.Function.Type == FType.Negate)
                {
                    e = -Expr.Sin(angle.FirstArg);
                    return true;
                }

                return mod_arguments_by_period(ref e, exp => Expr.Sin(exp));
            }

            if (e.Function.Type == FType.Cos)
            {
                Expr angle = e.FirstArg;

                // cos(-x) = cos(x)
                if (angle.Function.Type == FType.Negate)
                {
                    e = Expr.Cos(angle.FirstArg);
                    return true;
                }

                return mod_arguments_by_period(ref e, exp => Expr.Cos(exp));
            }

            return changed;
        }
        private static bool mod_arguments_by_period(ref Expr e, Func<Expr, Expr> Create)
        {
            Expr angle = e.FirstArg;
            if (angle.Function.Type == FType.Add)
            {
                List<Expr> aux_terms = new List<Expr>();
                BigRational total_pi_multiple = 0;
                foreach (Expr child in angle.Children)
                {
                    if (is_rational_pi_multiple(child, out BigRational multiple))
                    {
                        total_pi_multiple += multiple;
                    }
                    else
                    {
                        aux_terms.Add(child);
                    }
                }

                // Check if a simplification is admitted
                // The length of the resultant expression is equal to the 
                // number of auxillary terms plus an additional term for the 
                // multiple of pi, if it is non-zero.
                // To be on the conservative side, we ignore the possible 
                // additional term for the multiple of pi.
                if (aux_terms.Count < angle.NumArguments)
                {
                    if (total_pi_multiple.IsInteger)
                    {
                        BigInteger intvalue = total_pi_multiple.IntegerValue;

                        // Enforce the remainder to be [0, 2] 
                        BigRational rem_pi_multiple = total_pi_multiple - intvalue;
                        if (!rem_pi_multiple.IsZero)
                        {
                            if (rem_pi_multiple < 0)
                            {
                                rem_pi_multiple += 2;
                            }
                            aux_terms.Add(rem_pi_multiple);
                        }

                        Expr arg = Expr.Add(aux_terms);
                        if (intvalue % 2L == 0)
                        {
                            // sin(2n * pi + x) -> sin(x)
                            // cos(2n * pi + x) -> cos(x)
                            e = Create(arg);
                        }
                        else
                        {
                            // sin((2n - 1) * pi + x) = -sin(x)
                            // cos((2n - 1) * pi + x) = -cos(x)
                            e = -Create(arg);
                        }
                        return true;
                    }

                    else
                    {
                        BigRational mod = total_pi_multiple % 2;
                        aux_terms.Add(mod * _pi);
                        e = Create(Expr.Add(aux_terms));

                        return true;
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// TR5: sin(x)^2 -> 1 - cos(x)^2
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        internal static bool sin_square(ref Expr e, bool recursive)
        {
            bool changed = false;
            if (!e.IsLeaf && recursive)
            {
                for (int i = 0; i < e.NumArguments; ++i)
                {
                    changed = sin_square(ref e.Children[i], true) || changed;
                }
            }

            if (is_squared_via_pow(e, FType.Sin))
            {
                Expr angle = e.FirstArg.FirstArg;
                e = Expr.One - Expr.Pow(Expr.Cos(angle), 2);
                return true;
            }

            if (is_squared_via_multiply(e, FType.Sin))
            {
                Expr angle = e.FirstArg.FirstArg;
                e = Expr.One - Expr.Pow(Expr.Cos(angle), 2);
                return true;
            }

            return changed;
        }

        /// <summary>
        /// TR6: cos(x)^2 -> 1 - sin(x)^2
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        internal static bool cos_square(ref Expr e, bool recursive)
        {
            bool changed = false;
            if (!e.IsLeaf && recursive)
            {
                for (int i = 0; i < e.NumArguments; ++i)
                {
                    changed = cos_square(ref e.Children[i], true) || changed;
                }
            }

            if (is_squared_via_pow(e, FType.Cos))
            {
                Expr angle = e.FirstArg.FirstArg;
                e = Expr.One - Expr.Pow(Expr.Sin(angle), 2);
                return true;
            }

            if (is_squared_via_multiply(e, FType.Cos))
            {
                Expr angle = e.FirstArg.FirstArg;
                e = Expr.One - Expr.Pow(Expr.Sin(angle), 2);
                return true;
            }
            return changed;
        }

        /// <summary>
        /// TR7: cos(x)^2 -> (1 + cos(2x)) / 2
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        internal static bool lower_cos_degree(ref Expr e, bool recursive)
        {
            bool changed = false;
            if (!e.IsLeaf && recursive)
            {
                for (int i = 0; i < e.NumArguments; ++i)
                {
                    changed = lower_cos_degree(ref e.Children[i], true);
                }
            }

            if (is_squared_via_pow(e, FType.Cos) ||
                is_squared_via_multiply(e, FType.Cos))
            {
                Expr angle = e.FirstArg.FirstArg;
                e = _half + _half * Expr.Cos(2 * angle);
                return true;
            }

            return changed;
        }
        /// <summary>
        /// Returns whether the expression is f(x)^2 for some function
        /// </summary>
        /// <returns></returns>
        private static bool is_squared_via_pow(Expr e, FType function)
        {
            if (e.Function.Type == FType.Pow && e.FirstArg.Function.Type == function)
            {
                if (e.LastArg.IsInteger && e.LastArg.BigIntegerValue == 2L)
                {
                    return true;
                }
            }
            return false;
        }
        private static bool is_squared_via_multiply(Expr e, FType function)
        {
            return e.Function.Type == FType.Multiply &&
                e.NumArguments == 2 &&
                e.Children[0].Function.Type == function &&
                e.Children[1].Function.Type == function;
        }

        /// <summary>
        /// TR8: product -> sum 
        /// sin(a)cos(b) -> (1/2) * (sin(a + b) + sin(a - b))
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        internal static bool product_to_sum(ref Expr e, bool recursive)
        {
            bool changed = false;
            if (!e.IsLeaf && recursive)
            {
                for (int i = 0; i < e.NumArguments; ++i)
                {
                    changed = product_to_sum(ref e.Children[i], true);
                }
            }

            // We're defaulting to the first 2 factors here - other choices can
            // lead to better simplifications

            // sin(a)cos(b) -> 0.5(sin(a + b) + sin(a - b))
            if (e.Function.Type == FType.Multiply &&
                e.Children.Any(c => is_sin(c)) &&
                e.Children.Any(c => is_cos(c)))
            {
                split_into_sin_cos_and_other(e.Children, out Expr sin, out Expr cos, out List<Expr> otherFactors);

                Expr a = sin.FirstArg, b = cos.FirstArg;
                otherFactors.Add(_half);
                otherFactors.Add(Expr.Sin((a + b).Simplify()) + Expr.Sin((a - b).Simplify()));

                e = Expr.Multiply(otherFactors);
                return true;
            }

            // cos(a)cos(b) -> 0.5(cos(a + b) + cos(a - b))
            if (e.Function.Type == FType.Multiply &&
                e.Children.Count(c => is_cos(c)) >= 2)
            {
                split_into_cos_and_other(e.Children, 2, out List<Expr> cos, out List<Expr> otherFactors);

                Expr a = cos[0].FirstArg, b = cos[1].FirstArg;
                otherFactors.Add(_half);
                otherFactors.Add(Expr.Cos((a + b).Simplify()) + Expr.Cos((a - b).Simplify()));

                e = Expr.Multiply(otherFactors);
                return true;
            }

            // sin(a)sin(b) -> 0.5(cos(a - b) - cos(a + b))
            if (e.Function.Type == FType.Multiply &&
                e.Children.Count(c => is_sin(c)) >= 2)
            {
                split_into_sin_and_other(e.Children, 2, out List<Expr> sin, out List<Expr> otherFactors);

                Expr a = sin[0].FirstArg, b = sin[1].FirstArg;
                otherFactors.Add(_half);
                otherFactors.Add(Expr.Cos((a - b).Simplify()) - Expr.Cos((a + b).Simplify()));

                e = Expr.Multiply(otherFactors);
                return true;
            }

            return changed;
        }

        /// <summary>
        /// TR9: sum -> product
        /// sin(a) + sin(b) -> 2 sin((a + b) / 2) cos((a - b) / 2)
        /// sin(a) - sin(b) -> 2 cos((a + b) / 2) sin((a - b) / 2)
        /// cos(a) + cos(b) -> 2 cos((a + b) / 2) cos((a - b) / 2)
        /// cos(a) - cos(b) -> -2 sin((a + b) / 2) sin((a - b) / 2)
        /// </summary>
        internal static bool sum_to_product(ref Expr e, bool recursive)
        {
            bool changed = false;
            if (!e.IsLeaf && recursive)
            {
                for (int i = 0; i < e.NumArguments; ++i)
                {
                    changed = sum_to_product(ref e.Children[i], true);
                }
            }

            if (e.Function.Type != FType.Add)
            {
                return false;
            }

            // sin(a) + sin(b) -> 2 sin((a + b) / 2) cos((a - b) / 2)
            if (e.Children.Count(c => is_sin(c)) >= 2)
            {
                split_into_sin_and_other(e.Children, 2, out List<Expr> sin, out List<Expr> otherFactors);

                Expr a = sin[0].FirstArg, b = sin[1].FirstArg;
                Expr p = 2 * Expr.Sin((_half * (a + b)).Simplify()) * Expr.Cos((_half * (a - b)).Simplify());

                if (otherFactors.Count > 0)
                {
                    otherFactors.Add(p);
                    e = Expr.Add(otherFactors);
                }
                else
                {
                    e = p;
                }
                return true;
            }

            // sin(a) - sin(b) -> 2 cos((a + b) / 2) sin((a - b) / 2)
            if (e.Children.Any(c => is_sin(c)) && e.Children.Any(c => is_neg_sin(c)))
            {
                Expr sin = null, negsin = null;
                List<Expr> otherFactors = new List<Expr>();
                foreach (Expr c in e.Children)
                {
                    if (is_sin(c) && sin is null)
                    {
                        sin = c;
                    }
                    else if (is_neg_sin(c) && negsin is null)
                    {
                        negsin = c;
                    }
                    else
                    {
                        otherFactors.Add(c);
                    }
                }

                Expr a = sin.FirstArg, b = negsin.FirstArg;
                Expr p = 2 * Expr.Cos((_half * (a + b)).Simplify()) * Expr.Sin((_half * (a - b)).Simplify());
                if (otherFactors.Count > 0)
                {
                    otherFactors.Add(p);
                    e = Expr.Add(otherFactors);
                }
                else
                {
                    e = p;
                }
                return true;
            }

            // cos(a) + cos(b) -> 2 cos((a + b) / 2) cos((a - b) / 2)
            if (e.Children.Count(c => is_cos(c)) >= 2)
            {
                split_into_cos_and_other(e.Children, 2, out List<Expr> cos, out List<Expr> otherFactors);

                Expr a = cos[0].FirstArg, b = cos[1].FirstArg;
                Expr p = 2 * Expr.Cos((_half * (a + b)).Simplify()) * Expr.Cos((_half * (a - b)).Simplify());

                if (otherFactors.Count > 0)
                {
                    otherFactors.Add(p);
                    e = Expr.Add(otherFactors);
                }
                else
                {
                    e = p;
                }
                return true;
            }

            // cos(a) - cos(b) -> -2 sin((a + b) / 2) sin((a - b) / 2)
            if (e.Children.Any(c => is_cos(c)) && e.Children.Any(c => is_neg_cos(c)))
            {
                Expr cos = null, negcos = null;
                List<Expr> otherFactors = new List<Expr>();
                foreach (Expr child in e.Children)
                {
                    if (is_cos(child) && cos is null)
                    {
                        cos = child;
                    }
                    else if (is_neg_cos(child) && negcos is null)
                    {
                        negcos = child;
                    }
                    else
                    {
                        otherFactors.Add(child);
                    }
                }

                Expr a = cos.FirstArg, b = negcos.FirstArg.FirstArg;
                Expr p = -(2 * Expr.Sin((_half * (a + b)).Simplify()) * Expr.Sin((_half * (a - b)).Simplify()));

                if (otherFactors.Count > 0)
                {
                    otherFactors.Add(p);
                    e = Expr.Add(otherFactors);
                }
                else
                {
                    e = p;
                }
                return true;
            }

            return changed;
        }

        /// <summary>
        /// TR10: 
        /// sin(a + b) -> sin(a)cos(b) + cos(a)sin(b)
        /// cos(a + b) -> cos(a)cos(b) - sin(a)sin(b)
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        internal static bool expand_angle_sum(ref Expr e, bool recursive)
        {
            bool changed = false;
            if (!e.IsLeaf && recursive)
            {
                for (int i = 0; i < e.NumArguments; ++i)
                {
                    changed = expand_angle_sum(ref e.Children[i], true);
                }
            }

            if (is_sin(e) && e.FirstArg.Function.Type == FType.Add && e.FirstArg.NumArguments == 2)
            {
                Expr sum = e.FirstArg, a = sum.FirstArg, b = sum.LastArg;
                e = Expr.Sin(a) * Expr.Cos(b) + Expr.Cos(a) * Expr.Sin(b);
                return true;
            }

            if (is_cos(e) && e.FirstArg.Function.Type == FType.Add && e.FirstArg.NumArguments == 2)
            {
                Expr sum = e.FirstArg, a = sum.FirstArg, b = sum.LastArg;
                e = Expr.Cos(a) * Expr.Cos(b) - Expr.Sin(a) * Expr.Sin(b);
                return true;
            }

            return changed;
        }

        /// <summary>
        /// TR11: sin(2a) -> 2sin(a)cos(b)
        /// </summary>
        internal static bool expand_sin_double_angle(ref Expr e, bool recursive)
        {
            bool changed = false;
            if (!e.IsLeaf && recursive)
            {
                for (int i = 0; i < e.NumArguments; ++i)
                {
                    changed = expand_sin_double_angle(ref e.Children[i], true);
                }
            }

            // sin(2a) -> 2sin(a)cos(b)
            if (is_sin(e) && 
                e.FirstArg.Function.Type == FType.Multiply && 
                e.FirstArg.NumArguments >= 2 &&
                e.FirstArg.Children.Any(c => c.IsInteger && c.BigIntegerValue == 2L))
            {
                List<Expr> otherFactors = new List<Expr>();
                bool found2 = false;
                foreach (Expr factor in e.FirstArg.Children)
                {
                    if (!found2 && factor.IsInteger && factor.BigIntegerValue == 2L)
                    {
                        found2 = true;
                    }
                    else
                    {
                        otherFactors.Add(factor);
                    }
                }

                Expr angle = otherFactors.Count > 1 ? Expr.Multiply(otherFactors) : otherFactors[0];
                e = Expr.Multiply(2, Expr.Sin(angle), Expr.Cos(angle));
                return true;
            }

            return changed;
        }
        /// <summary>
        /// TR11a: sin(na) -> 
        /// </summary>
        /// <param name="e"></param>
        /// <param name="recursive"></param>
        /// <returns></returns>
        internal static bool expand_sin_n_angle(ref Expr e, bool recursive)
        {
            bool changed = false;
            if (!e.IsLeaf && recursive)
            {
                for (int i = 0; i < e.Children.Length; ++i)
                {
                    changed = expand_sin_n_angle(ref e.Children[i], true) || changed;
                }
            }

            if (e.Type == FType.Sin && e.FirstArg.Type == FType.Multiply)
            {
                Expr[] factors = e.FirstArg.Children;
                if (factors.Any(f => f.IsInteger))
                {
                    split_based_on_cond(factors, x => x.IsInteger, out List<Expr> integers, out List<Expr> nonIntegers);

                    Expr angle = Expr.Multiply(nonIntegers);

                    BigInteger n = BigIntegerExtensions.Product(integers.Select(i => i.BigIntegerValue));
                    if (n.IsZero)
                    {
                        e = Expr.Zero;
                        return true;
                    }

                    // Ensure sign +ve
                    bool flip_sign = false;
                    if (n < 0)
                    {
                        flip_sign = true;
                        n = -n;
                    }

                    BigInteger two = 2;
                    List<Expr> summands = new List<Expr>();
                    for (BigInteger k = 1; k < n; k += two)
                    {
                        Expr term = MathFunctions.Combinations(n, k) * Expr.Pow(Expr.Cos(angle), n - k) * Expr.Pow(Expr.Sin(angle), k);

                        // (-1)^((k - 1) / 2)
                        if (((k - 1) / 2) % 2 == 0)
                        {
                            summands.Add(term);
                        }
                        else
                        {
                            summands.Add(-term);
                        }
                    }

                    if (flip_sign)
                    {
                        e = -Expr.Add(summands);
                    }
                    else
                    {
                        e = Expr.Add(summands);
                    }
                    return true;
                }
            }

            return false;
        }
        /// <summary>
        /// TR11: cos(2a) -> cos(a)^2 - sin(a)^2 or 2cos(a)^2 - 1 or 1 - 2sin(a)^2.
        /// </summary>
        internal static bool expand_cos_double_angle(ref Expr e, int mode, bool recursive)
        {
            bool changed = false;
            if (!e.IsLeaf && recursive)
            {
                for (int i = 0; i < e.NumArguments; ++i)
                {
                    changed = expand_cos_double_angle(ref e.Children[i], mode, true);
                }
            }

            if (is_cos(e) && 
                e.FirstArg.Function.Type == FType.Multiply &&
                e.FirstArg.NumArguments >= 2 &&
                e.FirstArg.Children.Any(c => c.IsInteger && c.BigIntegerValue == 2L))
            {
                List<Expr> otherFactors = new List<Expr>();
                bool found2 = false;
                foreach (Expr factor in e.FirstArg.Children)
                {
                    if (!found2 && factor.IsInteger && factor.BigIntegerValue == 2L)
                    {
                        found2 = true;
                    }
                    else
                    {
                        otherFactors.Add(factor);
                    }
                }

                Expr angle = otherFactors.Count > 1 ? Expr.Multiply(otherFactors) : otherFactors[0];
                
                // cos(2x) -> cos(x)^2 - sin(x)^2
                if (mode < 0)
                {
                    e = Expr.Pow(Expr.Cos(angle), 2) - Expr.Pow(Expr.Sin(angle), 2);
                }

                // 2 cos(x)^2 - 1
                else if (mode == 0)
                {
                    e = 2 * Expr.Pow(Expr.Cos(angle), 2) - Expr.One;
                }

                // 1 - 2 sin(x)^2
                else
                {
                    e = Expr.One - (2 * Expr.Pow(Expr.Sin(angle), 2));
                }
                return true;
            }

            return changed;
        }

        /// <summary>
        /// TR12: 
        /// tan(a + b) -> [tan(a) + tan(b)] / [1 - tan(a) tan(b)],
        /// //tan(a - b) -> [tan(a) - tan(b)] / [1 + tan(a) tan(b)]
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        internal static bool expand_tan_angle_sum(ref Expr e, bool recursive)
        {
            bool changed = false;
            if (!e.IsLeaf && recursive)
            {
                for (int i = 0; i < e.NumArguments; ++i)
                {
                    changed = expand_tan_angle_sum(ref e.Children[i], true);
                }
            }

            if (e.Function.Type == FType.Tan && 
                e.FirstArg.Function.Type == FType.Add &&
                e.FirstArg.NumArguments >= 2)
            {
                // Split into first and others
                split_sum_into_two(e.FirstArg.Children, out Expr a, out Expr b);

                Expr tan_a = Expr.Tan(a), tan_b = Expr.Tan(b);
                e = (tan_a + tan_b) / (Expr.One - (tan_a * tan_b));
                return true;
            }

            return changed;
        }

        /// <summary>
        /// TR13: tan(a)tan(b) -> 1 - [tan(a) + tan(b)] / tan(a + b)
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        internal static bool expand_tan_product(ref Expr e, bool recursive)
        {
            bool changed = false;
            if (!e.IsLeaf && recursive) 
            { 
                for (int i = 0; i < e.NumArguments; ++i)
                {
                    changed = expand_tan_product(ref e.Children[i], true);
                }
            }

            // Product of two tangents
            if (e.Function.Type == FType.Multiply && 
                e.NumArguments == 2 &&
                e.Children.All(c => c.Function.Type == FType.Tan))
            {
                Expr a = e.FirstArg.FirstArg, b = e.LastArg.FirstArg;
                e = 1 - ((Expr.Tan(a) + Expr.Tan(b)) / Expr.Tan(a + b));
                return true;
            }
            return changed;
        }

        private static bool is_rational_pi_multiple(Expr e, out BigRational multiple)
        {
            if (e.IsZero)
            {
                multiple = BigRational.Zero;
                return true;
            }

            if (e.Function.Type == FType.Negate)
            {
                bool success = is_rational_pi_multiple(e.FirstArg, out multiple);
                if (success)
                {
                    multiple = -multiple;
                    return true;
                }
                return false;
            }

            if (e.Function.Type == FType.Multiply)
            {
                bool containsPi = false;
                multiple = BigRational.One;

                foreach (Expr factor in e.Children)
                {
                    // Factor might be a rational value
                    if (try_get_rational_value(factor, out BigRational v))
                    {
                        multiple *= v;
                    }

                    // Otherwise, factor might be pi
                    else if (IsPi(factor))
                    {
                        // Pi is already found
                        if (containsPi)
                        {
                            multiple = default;
                            return false;
                        }
                        containsPi = true;
                    }

                    // Not a rational or pi
                    else
                    {
                        return false;
                    }
                }

                return true;
            }

            multiple = default;
            return false;
        }
        private static bool try_get_rational_value(Expr e, out BigRational value)
        {
            if (e.IsConstant)
            {
                FConstant constant = e.Function as FConstant;
                if (constant.ConstantType == ConstantType.RATIONAL)
                {
                    value = constant.BigRationalValue;
                    return true;
                }

                // Do not support non-exact values
                value = default;
                return false;
            }

            if (e.Function.Type == FType.Negate)
            {
                if (try_get_rational_value(e, out value))
                {
                    value = -value;
                    return true;
                }

                value = default;
                return false;
            }

            if (e.Function.Type == FType.Inv)
            {
                if (try_get_rational_value(e, out value))
                {
                    value = value.MultiplicativeInverse();
                    return true;
                }

                value = default;
                return false;
            }

            if (e.Function.Type == FType.Add)
            {
                value = BigRational.Zero;
                foreach (Expr child in e.Children)
                {
                    if (try_get_rational_value(child, out BigRational v))
                    {
                        value += v;
                    }
                    else
                    {
                        return false;
                    }
                }
                return true;
            }

            if (e.Function.Type == FType.Multiply)
            {
                value = BigRational.One;
                foreach (Expr child in e.Children)
                {
                    if (try_get_rational_value(child, out BigRational v))
                    {
                        value *= v;
                    }
                    else
                    {
                        return false;
                    }
                }
                return true;
            }

            if (e.Function.Type == FType.Pow)
            {
                // Power must be an integer
                if (!e.LastArg.IsInteger)
                {
                    value = default;
                    return false;
                }

                BigInteger pow = e.LastArg.BigIntegerValue;
                if (pow.Sign < 0)
                {
                    value = default;
                    return false;
                }

                // Base must be a rational
                if (try_get_rational_value(e.FirstArg, out BigRational bse))
                {
                    value = BigRational.Pow(bse, e.LastArg.BigIntegerValue);
                    return true;
                }

                value = default;
                return false;
            }

            value = default;
            return false;
        }
        private static bool IsPi(Expr e)
        {
            if (e.Function.Type == FType.Const)
            {
                FConstant constant = e.Function as FConstant;
                return constant.ConstantType == ConstantType.EXACT_DECIMAL && (constant as FConstantSpecial).Name == ConstantName.Pi;
            }
            return false;
        }

        private static bool is_sin(Expr e) => e.Function.Type == FType.Sin;
        private static bool is_cos(Expr e) => e.Function.Type == FType.Cos;
        private static bool is_neg_sin(Expr e) => e.Function.Type == FType.Negate && is_sin(e.FirstArg);
        private static bool is_neg_cos(Expr e) => e.Function.Type == FType.Negate && is_cos(e.FirstArg);

        private static void split_into_sin_and_other(Expr[] factors, int maxSinFactors, out List<Expr> sinFactors, out List<Expr> otherFactors)
        {
            otherFactors = new List<Expr>();
            sinFactors = new List<Expr>();

            foreach (Expr factor in factors)
            {
                if (is_sin(factor) && sinFactors.Count < maxSinFactors)
                {
                    sinFactors.Add(factor);
                }
                else
                {
                    otherFactors.Add(factor);
                }
            }
        }
        private static void split_into_cos_and_other(Expr[] factors, int maxCosFactors, out List<Expr> cosFactors, out List<Expr> otherFactors)
        {
            otherFactors = new List<Expr>();
            cosFactors = new List<Expr>();

            foreach (Expr factor in factors)
            {
                if (is_cos(factor) && cosFactors.Count < maxCosFactors)
                {
                    cosFactors.Add(factor);
                }
                else
                {
                    otherFactors.Add(factor);
                }
            }
        }
        private static void split_into_sin_cos_and_other(Expr[] factors, out Expr sinFactor, out Expr cosFactor, out List<Expr> otherFactors)
        {
            sinFactor = default;
            cosFactor = default;
            otherFactors = new List<Expr>();

            foreach (Expr factor in factors)
            {
                if (is_sin(factor) && sinFactor is null)
                {
                    sinFactor = factor;
                }
                else if (is_cos(factor) && cosFactor is null)
                {
                    cosFactor = factor;
                }
                else
                {
                    otherFactors.Add(factor);
                }
            }
        }
        
        private static void split_sum_into_two(Expr[] summands, out Expr first, out Expr second)
        {
            first = summands[0];

            int len = summands.Length;
            if (len > 2)
            {
                List<Expr> terms = new List<Expr>();
                for (int i = 1; i < len; ++i)
                {
                    terms.Add(summands[i]);
                }
                second = Expr.Add(terms);
            }
            else
            {
                second = summands[1];
            }
        }
        private static void split_based_on_cond(Expr[] factors, Func<Expr, bool> conditional, out List<Expr> matches, out List<Expr> nonMatches)
        {
            matches = new List<Expr>();
            nonMatches = new List<Expr>();

            foreach (Expr e in factors)
            {
                if (conditional(e))
                {
                    matches.Add(e);
                }
                else
                {
                    nonMatches.Add(e);
                }
            }
        }

        /// <summary>
        /// Measurement of the complexity of the expression.
        /// </summary>
        private static int length(Expr expr)
        {
            if (expr.IsLeaf)
            {
                return 1;
            }

            int len = 1;
            foreach (Expr child in expr.Children)
            {
                // Additional penalty of trig functions
                if (is_trig(child.Function.Type))
                {
                    len += 5;
                }

                // Add length of children
                len += length(child);
            }
            return len;
        }
        private static bool is_trig(FType type)
        {
            return type == FType.Sin ||
                type == FType.Cos ||
                type == FType.Tan ||
                type == FType.Cot ||
                type == FType.Asin ||
                type == FType.Acos ||
                type == FType.Atan; 
        }
    }
}
