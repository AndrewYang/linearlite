﻿using LinearNet.Structs.Fields;
using LinearNet.Symbolic.Structs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Symbolic
{
    public static class ExpressionDerivative
    {
        internal static Expr Differentiate(Expr expr, string variable)
        {
            // will require some manual handling here
            if (!expr.DependsOn(variable))
            {
                return Expr.Zero;
            }

            // x' = 1
            if (expr.Function.Type == FType.Var)
            {
                return Expr.One;
            }

            // Function will depend on the root node
            if (expr.Function.Type == FType.Add)
            {
                List<Expr> derivatives = new List<Expr>();
                for (int i = 0; i < expr.Children.Length; ++i)
                {
                    Expr child = expr.Children[i];

                    // Only include those terms depending on the variable - otherwise the partial derivative 
                    // is zero.
                    if (child.DependsOn(variable))
                    {
                        derivatives.Add(Differentiate(expr.Children[i], variable));
                    }
                }
                return new Expr(new FAdd(derivatives.Count), derivatives.ToArray()); // derivative of sum = sum of derivatives
            }

            if (expr.Function.Type == FType.Negate)
            {
                // (-f(x))' = -f'(x)
                return -Differentiate(expr.Children[0], variable);
            }

            if (expr.Function.Type == FType.Multiply)
            {
                // Using extended product rule for n-way products.
                int len = expr.Children.Length, i, j;
                Expr sum = Expr.Zero;
                for (i = 0; i < len; ++i)
                {
                    if (!expr.Children[i].DependsOn(variable))
                    {
                        continue; // derivative of this summand = 0
                    }

                    Expr prod = Differentiate(expr.Children[i], variable);
                    for (j = 0; j < len; ++j)
                    {
                        if (j == i) continue;
                        prod *= expr.Children[j];
                    }
                    sum += prod;
                }

                return sum;
            }

            if (expr.Function.Type == FType.Inv)
            {
                // (1 / a)' = -a' / a^2
                Expr a = expr.Children[0], aPrime = Differentiate(a, variable);
                return -aPrime / (a * a);
            }

            if (expr.Function.Type ==  FType.Pow)
            {
                Expr f = expr.Children[0],
                    g = expr.Children[1];

                // f(x)^a where a is constant
                if (!g.DependsOn(variable))
                {
                    // Chain rule: [f(x)^n]' = n * f(x)^(n - 1) * f'(x)
                    return g * (f ^ (g - 1)) * Differentiate(f, variable);
                }

                // a^f(x) where a is constant
                if (!f.DependsOn(variable))
                {
                    // Chain rule: a^(f(x))' = f'(x) * a^(f(x)) * ln(a)
                    return Differentiate(g, variable) * expr * Expr.Log(f);
                }

                // general case: f(x)^g(x)
                // (f(x)^g(x))' = g(x) * f(x)^(g(x)- 1) * f'(x) + f(x)^g(x) * ln f(x) * g'(x)
                Expr fPrime = Differentiate(f, variable);
                Expr gPrime = Differentiate(g, variable);
                return g * (f ^ (g - 1)) * fPrime + expr * Expr.Log(f) * gPrime;
            }
            if (expr.Function.Type == FType.Exp)
            {
                // Chain rule: [exp(f(x))]' = exp(f(x)) * f'(x)
                Expr a = expr.Children[0], aPrime = Differentiate(a, variable);
                return expr * aPrime;
            }

            if (expr.Function.Type == FType.Log)
            {
                // Chain rule: log(f(x))' = f'(x) / f(x)
                Expr a = expr.Children[0], aPrime = Differentiate(a, variable);
                return aPrime / a;
            }

            if (expr.Function.Type == FType.Sin)
            {
                // Chain rule: sin(f(x))' = cos(f(x)) * f'(x)
                Expr a = expr.Children[0], aPrime = Differentiate(a, variable);
                return Expr.Cos(a) * aPrime; // no need for copying a because of the * operator
            }
            if (expr.Function.Type == FType.Cos)
            {
                // Chain rule: cos(f(x))' = -sin(f(x)) * f'(x)
                Expr a = expr.Children[0], aPrime = Differentiate(a, variable);
                return - Expr.Sin(a) * aPrime; // no need for copying a because of the * operator
            }
            if (expr.Function.Type == FType.Tan)
            {
                // Chain rule: tan(f(x))' = f'(x) / cos(f(x))^2 
                Expr a = expr.Children[0], aPrime = Differentiate(a, variable);
                return aPrime / (Expr.Cos(a) ^ 2);
            }
            if (expr.Function.Type == FType.Asin)
            {
                // Chain rule: asin(f(x))' = f'(x) / sqrt(1 - f(x)^2)
                Expr f = expr.Children[0], fPrime = Differentiate(f, variable);
                return fPrime / Expr.Pow(Expr.One - f * f, new BigRational(1, 2));
            }
            if (expr.Function.Type == FType.Acos)
            {
                // Chain rule: acos(f(x))' = -f'(x) / sqrt(1 - f(x)^2)
                Expr f = expr.Children[0], fPrime = Differentiate(f, variable);
                return -fPrime / Expr.Pow(Expr.One - f * f, new BigRational(1, 2));
            }
            if (expr.Function.Type == FType.Atan)
            {
                // Chain rule: atan(f(x))' = f'(x) / (1 + f(x)^2)
                Expr f = expr.Children[0], fPrime = Differentiate(f, variable);
                return fPrime / (Expr.One + f * f);
            }
            if (expr.Function.Type == FType.Sinh)
            {
                // Chain rule: sinh(f(x))' = f'(x) cosh(f(x))
                Expr f = expr.Children[0], fPrime = Differentiate(f, variable);
                return fPrime * Expr.Cosh(f);
            }
            if (expr.Function.Type == FType.Cosh)
            {
                // Chain rule: cosh(f(x))' = f'(x) sinh(f(x))
                Expr f = expr.Children[0], fPrime = Differentiate(f, variable);
                return fPrime * Expr.Sinh(f);
            }
            if (expr.Function.Type == FType.Tanh)
            {
                // Chain rule: tanh(f(x))' = f'(x) (1 - tanh^2(f(x)))
                Expr f = expr.Children[0], fPrime = Differentiate(f, variable);
                return fPrime * (Expr.One - expr * expr);
            }

            if (expr.Function.Type == FType.Gamma)
            {
                // Chain rule: G(f(x))' = G(f(x)) * D(f(x)) * f'(x)
                Expr f = expr.Children[0], fPrime = Differentiate(f, variable);
                return expr * new Expr(new FDigamma(), f) * fPrime;
            }
            throw new NotImplementedException();
        }
    }
}
