﻿using LinearNet.Functions;
using LinearNet.Helpers;
using LinearNet.Symbolic.Structs;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Numerics;
using System.Text;

namespace LinearNet.Symbolic
{
    internal static class ExpressionExpand
    {
        /// <summary>
        /// Expand an expression until it is no longer changed
        /// </summary>
        /// <param name="expr"></param>
        /// <param name="simplify"></param>
        /// <returns></returns>
        internal static bool Expand(Expr expr, bool simplify)
        {
            bool changed = false;

            while (ExpandIteration(expr))
            {
                changed = true;
            }

            // Simplify without expanding, only if expansion was successful
            if (simplify && changed)
            {
                ExprSimplify.Simplify(expr, false);
            }
            return changed;
        }

        /// <summary>
        /// Expand an expression over 1 iteration
        /// </summary>
        /// <param name="expr"></param>
        /// <returns>Whether a change was made</returns>
        internal static bool ExpandIteration(Expr expr)
        {
            if (expr == null) throw new ArgumentNullException(nameof(expr));
            if (expr.IsLeaf) return false;

            bool changed = false;

            // Recursively expand all children (if any)
            foreach (Expr child in expr.Children)
            {
                changed = ExpandIteration(child) || changed;
            }

            changed = TryExpandBrackets(expr) || changed;
            changed = TryExpandPowers(expr) || changed;
            changed = TryExpandPowerProducts(expr) || changed;

            return changed;
        }
        /// <summary>
        /// a * (b + c) -> a * b + a * c
        /// (a + b) * (c + d) -> a * (c + d) + a * (c + d) (i.e. only performs single step each time)
        /// </summary>
        /// <param name="expr"></param>
        /// <returns></returns>
        internal static bool TryExpandBrackets(Expr expr)
        {
            if (expr.Function.Type != FType.Multiply)
            {
                return false;
            }

            // This is detected by a (*) containing a (+) child
            // Note that in general there could be more than one factor 
            // containing a (+). We only take care of the first one, then loop through
            // and take care of the others. 

            Expr[] factors = expr.Children;
            for (int i = 0; i < factors.Length; ++i)
            {
                Expr child = factors[i];
                if (child.Function.Type == FType.Add)
                {
                    // Get the summands (e.g. b + c in the above example)
                    Expr[] summands = child.Children;

                    // This will hold all the expanded terms, e.g. a * b, a * c in the above example 
                    Expr[] expandedTerms = new Expr[summands.Length];
                    for (int s = 0; s < summands.Length; ++s)
                    {
                        Expr summand = summands[s];

                        // Each expanded term (e.g. a * b, a * c in the above example)
                        // still contains 'factors' terms, its just that the term containing (+)
                        // is replaced with a single summand.
                        Expr[] termFactors = new Expr[factors.Length];
                        for (int j = 0; j < factors.Length; ++j)
                        {
                            // Check if we are at the factor that is to be expanded
                            if (i == j)
                            {
                                termFactors[i] = summand;
                            }
                            else
                            {
                                termFactors[j] = factors[j];
                            }
                        }

                        expandedTerms[s] = new Expr(new FMultiply(factors.Length), termFactors);
                    }

                    // Alter the expression so that the top level function is (+)
                    expr.Function = new FAdd(expandedTerms.Length);
                    expr.Children = expandedTerms;

                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Expands (a + b)^n, (a + b + c)^n, etc for integer n > 1 using multinomial coefficients
        /// </summary>
        /// <param name="expr"></param>
        /// <returns></returns>
        internal static bool TryExpandPowers(Expr expr)
        {
            if (expr.Function.Type != FType.Pow) return false;

            Expr bse = expr.Children[0];
            Expr power = expr.Children[1];

            // Base function must be add 
            if (bse.Function.Type != FType.Add)
            {
                return false;
            }

            // Only support constant powers for now
            if (power.Function.Type != FType.Const)
            {
                return false;
            }

            // Only support integer powers for now
            FConstant c = power.Function as FConstant;
            if (!c.IsInteger)
            {
                return false;
            }

            // Leave edge cases (e.g. 0, 1) for the simplification engine,
            // also, > int.MaxValue -> out of range
            BigInteger bint = c.BigIntegerValue;
            if (bint <= 1L || c.BigIntegerValue > int.MaxValue)
            {
                return false;
            }

            Expr[] summands = bse.Children;
            int n = (int)bint;
            List<int[]> powers = PartitionUtil.GetAllPartitions(n, summands.Length);

            expr.Function = new FAdd(summands.Length);
            expr.Children = new Expr[powers.Count];
            for (int i = 0; i < powers.Count; ++i)
            {
                int[] p = powers[i];

                List<Expr> termFactors = new List<Expr>();
                for (int j = 0; j < summands.Length; ++j)
                {
                    // 0-powers are just 1: ignore
                    if (p[j] > 0)
                    {
                        termFactors.Add(summands[j] ^ p[j]);
                    }
                }

                if (termFactors.Count > 0)
                {
                    expr.Children[i] = MathFunctions.BigCombinations(n, p) * new Expr(new FMultiply(termFactors.Count), termFactors.ToArray());
                }
                else
                {
                    expr.Children[i] = MathFunctions.BigCombinations(n, p);
                }
            }

            return true;
        }

        /// <summary>
        /// Expands (ab)^n -> a^n * b^n for general n
        /// </summary>
        /// <param name="expr"></param>
        /// <returns></returns>
        internal static bool TryExpandPowerProducts(Expr expr)
        {
            if (expr.Function.Type != FType.Pow)
            {
                return false;
            }

            Expr bse = expr.Children[0];

            if (bse.Function.Type != FType.Multiply)
            {
                return false;
            }

            Expr power = expr.Children[1];

            if (bse.Children.Length == 0)
            {
                // Default to the multiplicative identity = 1
                expr.Function = new FConstantRational(1);
                expr.Children = null;
            }

            else if (bse.Children.Length == 1)
            {
                // Only 1 factor - i.e. ^(*(a))(n) -> ^(a)(n)
                Expr newBase = bse.Children[0];
                bse.Function = newBase.Function;
                bse.Children = newBase.Children;
            }

            else
            {
                // More than 1 factor - change the root function to *
                for (int i = 0; i < bse.Children.Length; ++i)
                {
                    bse.Children[i] = bse.Children[i] ^ power;
                }

                expr.Function = new FMultiply(bse.Children.Length);
                expr.Children = bse.Children;
            }

            return true;
        }

        
    }
}
