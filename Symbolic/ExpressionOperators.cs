﻿using LinearNet.Structs;
using LinearNet.Structs.Fields;
using LinearNet.Symbolic.Structs;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Symbolic
{
    /// <summary>
    /// 
    /// </summary>
    /// <cat>symbolic-algebra</cat>
    public partial class Expr : IField<Expr>
    {
        public Expr MultiplicativeIdentity => One;
        public Expr AdditiveIdentity => Zero;
        public BigInteger Characteristic => BigInteger.Zero;

        public Expr Add(Expr g)
        {
            return new Expr(new FAdd(2), Copy(), g.Copy());
        }
        public Expr AdditiveInverse()
        {
            return new Expr(new FNegate(), Copy());
        }
        public Expr Subtract(Expr g)
        {
            return new Expr(new FAdd(2), Copy(), g.AdditiveInverse());
        }
        public Expr MultiplicativeInverse()
        {
            return new Expr(new FInv(), Copy());
        }
        public Expr Multiply(Expr b)
        {
            return new Expr(new FMultiply(2), Copy(), b.Copy());
        }
        public Expr Divide(Expr e)
        {
            return new Expr(new FMultiply(2), Copy(), e.MultiplicativeInverse());
        }

        public bool ApproximatelyEquals(Expr e, double eps)
        {
            return Equals(e);
        }

        /// <summary>
        /// See EqualsExactly(object obj).
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        public bool Equals(Expr e)
        {
            return EqualsExactly(e);
        }

        /// <summary>
        /// See EqualsExactly(object obj).
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            if (obj is null) return false;
            return Equals(obj as Expr);
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        /// <summary>
        /// Returns whether two expressions are equal up to permutation of associative operations like 
        /// addition and multiplication. 
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        public bool EqualsUptoAssociativity(Expr e)
        {
            return ExprCompare.EqualUpToAssociativity(this, e);
        }

        /// <summary>
        /// Returns whether this expression is identical to another expression.
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        public bool EqualsExactly(Expr e)
        {
            return ExprCompare.ExactlyEqual(this, e);
        }

        /// <summary>
        /// <p>Returns whether this expression is equal to another expression object, after simplification.</p>
        /// <p>
        /// There are 2 other types of equality operators. <txt>EqualsUptoPermutation</txt> and <txt>==</txt>
        /// return <txt>true</txt> if two expressions are exactly equal, up to permutation of associative 
        /// operations such as addition and multiplication.
        /// </p>
        /// <p>
        /// Finally, <txt>EqualsExactly</txt> will only return <txt>true</txt> if two expressions are exactly 
        /// identical in form, i.e. different permutations of associative operations will be treated as different
        /// operations. 
        /// </p>
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        public bool EqualsAfterSimplification(Expr e)
        {
            return ExprCompare.EqualUptoSimplification(this, e);
        }
        #region Operator overrides

        public static Expr operator +(Expr a, Expr b) => a.Add(b);
        public static Expr operator -(Expr a, Expr b) => a.Subtract(b);
        public static Expr operator *(Expr a, Expr b) => a.Multiply(b);
        public static Expr operator /(Expr a, Expr b) => a.Divide(b);
        public static Expr operator -(Expr e) => e.AdditiveInverse();
        /// <summary>
        /// Raises one expression to the power of another expression.
        /// </summary>
        /// <param name="a">The base expression.</param>
        /// <param name="b">The power expression.</param>
        /// <returns></returns>
        public static Expr operator ^(Expr a, Expr b)
        {
            return new Expr(new FPow(), a.Copy(), b.Copy());
        }
        public static bool operator ==(Expr a, Expr b) => ExprCompare.EqualUpToAssociativity(a, b);
        public static bool operator !=(Expr a, Expr b) => !ExprCompare.EqualUpToAssociativity(a, b);

        public static implicit operator Expr(int x) => new Expr(new FConstantRational(x));
        public static implicit operator Expr(long x) => new Expr(new FConstantRational(x));
        public static implicit operator Expr(BigInteger x) => new Expr(new FConstantRational(x));
        public static implicit operator Expr(BigRational x) => new Expr(new FConstantRational(x));

        public static implicit operator Expr(float x) => new Expr(new FConstantDecimal(x));
        public static implicit operator Expr(double x) => new Expr(new FConstantDecimal(x));
        public static implicit operator Expr(BigDecimal x) => new Expr(new FConstantDecimal(x));

        #endregion
    }
}
