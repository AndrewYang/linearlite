﻿using LinearNet.Structs.Fields;
using LinearNet.Symbolic.Structs;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace LinearNet.Symbolic
{
    internal class ExpressionParser
    {
        private readonly char[] _supportedInstructionSet = { '+', '-', '*', '/', '^', '!', '(', '{', '[', ')', '}', ']' };

        internal Expr Parse(string s)
        {
            ValidateString(s);
            CheckBrackets(s);

            s = InsertImplicitMultiplication(s);

            Expr e = ParseInner(s);
            return e;
        }

        /// <summary>
        /// Check to make sure the string conforms to our supported instruction sets
        /// </summary>
        /// <param name="s"></param>
        private void ValidateString(string s)
        {
            foreach (char c in s)
            {
                if (char.IsWhiteSpace(c)) continue;

                // numeric digits are allowed
                if ('0' <= c && c <= '9') continue;

                // decimal points and placeholders are allowed
                if (c == '.' || c == ',') continue;

                // variables are allowed
                if ('a' <= c && c <= 'z') continue;
                if ('A' <= c && c <= 'Z') continue;

                // instructions are allowed
                if (_supportedInstructionSet.Contains(c)) continue;

                throw new ArgumentException();
            }
        }

        /// <summary>
        /// Check brackets
        /// </summary>
        /// <param name="s"></param>
        private void CheckBrackets(string s)
        {
            List<char> currentOpenBrackets = new List<char>();
            foreach (char c in s)
            {
                if (c.IsOpenBracket())
                {
                    currentOpenBrackets.Add(c);
                }
                else if (c.IsCloseBracket())
                {
                    if (currentOpenBrackets.Count == 0)
                    {
                        throw new FormatException(nameof(s) + " contains mismatched brackets");
                    }

                    char last = currentOpenBrackets[currentOpenBrackets.Count - 1];
                    if (!c.Closes(last))
                    {
                        throw new FormatException(nameof(s) + " contains mismatched brackets");
                    }
                    currentOpenBrackets.RemoveAt(currentOpenBrackets.Count - 1);
                }
            }

            if (currentOpenBrackets.Count > 0)
            {
                throw new FormatException(nameof(s) + " contains mismatched brackets");
            }
        }

        /// <summary>
        /// 3a -> 3 * a
        /// 4(b + c) -> 4 * (b + c)
        /// (a + b) (c + d) -> (a + b) * (c + d)
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        private string InsertImplicitMultiplication(string s)
        {
            int index;

            // 3a -> 3 * a
            while (Regex.IsMatch(s, "[0-9]\\s*[a-zA-Z]"))
            {
                Match match = Regex.Match(s, "[0-9]\\s*[a-zA-Z]");
                string val = Regex.Replace(match.Value, "\\s+", ""); // remove the space
                string digit = val.Substring(0, 1);
                string character = val.Substring(1, 1);

                // Perform the replacement
                s = s.Substring(0, match.Index) + digit + " * " + character + s.Substring(match.Index + match.Value.Length);
            }

            // 5 (a + b) -> 5 * (a + b)
            while (Regex.IsMatch(s, "[0-9]\\s*\\("))
            {
                Match match = Regex.Match(s, "[0-9]\\s*\\(");
                string val = Regex.Replace(match.Value, "\\s+", ""); // remove the space
                string digit = val.Substring(0, 1);
                string bracket = val.Substring(1, 1);

                // Perform the replacement
                s = s.Substring(0, match.Index) + digit + " * " + bracket + s.Substring(match.Index + match.Value.Length);
            }

            // a (a + b) -> a * (a + b)
            index = 0;
            Regex r1 = new Regex("[a-zA-Z]+\\s*\\(");
            while (r1.IsMatch(s, index))
            {
                Match match = r1.Match(s, index);
                string val = match.Value.Replace("(", "").Trim();

                if (!IsSpecialFunction(val))
                {
                    // Perform the replacement
                    s = s.Substring(0, match.Index) + val + " * (" + s.Substring(match.Index + match.Value.Length);
                }
                index = match.Index + match.Value.Length;
            }

            // (a + b) a -> (a + b) * a
            index = 0;
            Regex r2 = new Regex("\\)\\s*[a-zA-Z]+");
            while (r2.IsMatch(s, index))
            {
                Match match = r2.Match(s, index);
                string val = match.Value.Replace(")", "").Trim();

                // no need to filter out special functions
                s = s.Substring(0, match.Index) + ") * " + val + s.Substring(match.Index + match.Value.Length);
                index = match.Index + match.Value.Length;
            }

            // (a + b) (x + y) -> (a + b) * (x + y)
            while (Regex.IsMatch(s, "(\\)|\\]|\\})\\s*(\\(|\\[|\\{)"))
            {
                Match match = Regex.Match(s, "(\\)|\\]|\\})\\s*(\\(|\\[|\\{)");
                string val = Regex.Replace(match.Value, "\\s+", ""); // remove the space
                string close = val.Substring(0, 1);
                string open = val.Substring(1, 1);

                // Perform the replacement
                s = s.Substring(0, match.Index) + close + " * " + open + s.Substring(match.Index + match.Value.Length);
            }

            // ab -> a * b
            // log -> log
            Regex rgx3 = new Regex("[a-zA-Z]{2,}");
            index = 0;
            while (rgx3.IsMatch(s, index)) // look for >= 2 continuous characters
            {
                Match match = rgx3.Match(s, index);
                string val = match.Value;
                
                // Filter out special words
                if (!IsSpecialFunction(val))
                {
                    string replace = string.Join(" * ", val.ToCharArray());
                    s = s.Substring(0, match.Index) + replace + s.Substring(match.Index + val.Length);
                }
                // update the start index
                index = match.Index + val.Length;
            }
            return s;
        }
        private bool IsSpecialFunction(string val)
        {
            return val == "exp" ||
                    val == "log" || val == "ln" ||
                    val == "sqrt" ||
                    val == "sin" || val == "cos" || val == "tan" ||
                    val == "asin" || val == "acos" || val == "atan" ||
                    val == "arcsin" || val == "arccos" || val == "arctan" ||
                    
                    val == "pi" || val == "e";
        }

        private Expr ParseInner(string s)
        {
            // Find the top level function 
            s = s.Trim();
            
            if (string.IsNullOrEmpty(s))
            {
                return Expr.Zero;
            }

            // Remove outer brackets
            if (HasOuterBrackets(s))
            {
                s = RemoveOuterBrackets(s);
            }
            
            // Split by operators - if any
            List<Operator> operators = FindRootLevelOperators(s);
            if (operators.Count > 0)
            {
                // Split the sequence by + and - first, if they exist. Since they are the lowest
                // priority operators, they will always be split first. 
                if (operators.Any(o => o.Symbol == "+" || o.Symbol == "-"))
                {
                    // We find the last occuring +/- operator and start evaluating from there
                    Operator split = operators.Last(o => o.Symbol == "+" || o.Symbol == "-");

                    // Deal with negations e.g. -3x
                    string left_str = s.Substring(0, split.Index);
                    string right_str = s.Substring(split.Index + 1);
                    if (string.IsNullOrWhiteSpace(left_str) && split.Symbol == "-")
                    {
                        return -ParseInner(right_str);
                    }

                    // Ordinary + or - operations
                    else
                    {
                        Expr left = ParseInner(left_str);
                        Expr right = ParseInner(right_str);
                        if (split.Symbol == "+")
                        {
                            return left + right;
                        }
                        else
                        {
                            return left - right;
                        }
                    }
                }

                // Next, split by either * or /, whichever comes first
                if (operators.Any(o => o.Symbol == "*" || o.Symbol == "/"))
                { 
                    // Split by the last occuring instance of the * or / operator
                    Operator split = operators.Last(o => o.Symbol == "*" || o.Symbol == "/");

                    Expr left = ParseInner(s.Substring(0, split.Index));
                    Expr right = ParseInner(s.Substring(split.Index + 1));
                    if (split.Symbol == "*")
                    {
                        return left * right;
                    }
                    else
                    {
                        return left / right;
                    }
                }

                // Next, split by ^, of which there should only be one
                if (operators.Any(o => o.Symbol == "^"))
                {
                    if (operators.Count != 1)
                    {
                        throw new ArgumentException(nameof(s) + " is not formatted correctly.");
                    }

                    Operator split = operators[0];
                    Expr left = ParseInner(s.Substring(0, split.Index));
                    Expr right = ParseInner(s.Substring(split.Index + 1));
                    return new Expr(new FPow(), left, right);
                }

                throw new FormatException("Unknown operators contained in: " + string.Join(",", operators.Select(o => o.Symbol)));
            }

            // No BIMDAS operators - check for special operators
            Expr e = null;
            if (TryParseFunction(s, "log", new FLog(), out e)) return e;
            if (TryParseFunction(s, "ln", new FLog(), out e)) return e;
            if (TryParseFunction(s, "exp", new FExp(), out e)) return e;
            if (TryParseFunction(s, "sqrt", new FSqrt(), out e)) return e;

            // Hyperbolics go first - potential conflicts with sin, cos, tan...
            if (TryParseFunction(s, "sinh", new FSinh(), out e)) return e;
            if (TryParseFunction(s, "cosh", new FCosh(), out e)) return e;
            if (TryParseFunction(s, "tanh", new FTanh(), out e)) return e;

            // Trig functions
            if (TryParseFunction(s, "sin", new FSin(), out e)) return e;
            if (TryParseFunction(s, "cos", new FCos(), out e)) return e;
            if (TryParseFunction(s, "tan", new FTan(), out e)) return e;
            if (TryParseFunction(s, "asin", new FAsin(), out e)) return e;
            if (TryParseFunction(s, "arcsin", new FAsin(), out e)) return e;
            if (TryParseFunction(s, "acos", new FAcos(), out e)) return e;
            if (TryParseFunction(s, "arccos", new FAcos(), out e)) return e;
            if (TryParseFunction(s, "atan", new FAtan(), out e)) return e;
            if (TryParseFunction(s, "arctan", new FAtan(), out e)) return e;

            // Special functions
            if (TryParseFunction(s, "beta", new FBeta(), out e, 2)) return e;
            if (TryParseFunction(s, "Beta", new FBeta(), out e, 2)) return e;
            if (TryParseFunction(s, "gamma", new FGamma(), out e)) return e;
            if (TryParseFunction(s, "Gamma", new FGamma(), out e)) return e;
            if (TryParseFunction(s, "zeta", new FZeta(), out e)) return e;
            if (TryParseFunction(s, "Zeta", new FZeta(), out e)) return e;
            if (TryParseFunction(s, "erf", new FErf(), out e)) return e;
            if (TryParseFunction(s, "Erf", new FErf(), out e)) return e;
            if (TryParseFunction(s, "erfc", new FErfc(), out e)) return e;
            if (TryParseFunction(s, "Erfc", new FErfc(), out e)) return e;
            if (TryParseFunction(s, "logit", new FLogit(), out e)) return e;
            if (TryParseFunction(s, "Logit", new FLogit(), out e)) return e;
            if (TryParseFunction(s, "logistic", new FLogistic(), out e)) return e;
            if (TryParseFunction(s, "Logistic", new FLogistic(), out e)) return e;

            // Special constants
            //if (TryParseFunction(s, "pi", new FConstantSpecial(ConstantName.Pi), out e)) return e;
            //if (TryParseFunction(s, "e", new FConstantSpecial(ConstantName.E), out e)) return e;

            // No operators - check for variables and constants
            if (TryParse(s, out FConstant constant))
            {
                return new Expr(constant);
            }

            // Everything else should be a variable
            return new Expr(new FVariable(s));
        }
        
        /// <summary>
        /// Find the operators from the root level, e.g.
        /// (3 + 4) * 2 -> [*]
        /// 1 ^ 2 + 5 -> [^,+]
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        private List<Operator> FindRootLevelOperators(string s)
        {
            List<Operator> operators = new List<Operator>();
            int depth = 0;
            for (int i = 0; i < s.Length; ++i)
            {
                char c = s[i];
                if (c.IsOpenBracket())
                {
                    depth++;
                }
                else if (c.IsCloseBracket())
                {
                    depth--;
                }
                else if (depth == 0 && c.IsOperator())
                {
                    operators.Add(new Operator(i, c.ToString()));
                }
            }

            return operators;
        }
        private List<BracketSet> FindRootLevelBrackets(string s)
        {
            List<BracketSet> topLevelBrackets = new List<BracketSet>();

            int startIndex = 0;
            while (true)
            {
                BracketSet bracket = s.OutermostBracketSet(startIndex);
                if (bracket.OpenIndex < 0 || bracket.CloseIndex < 0)
                {
                    break;
                }
                topLevelBrackets.Add(bracket);

                startIndex = bracket.CloseIndex + 1;

                // Finished
                if (startIndex >= s.Length)
                {
                    break;
                }
            }
            return topLevelBrackets;
        }
        /// Checks whether the entire string is contained inside a 
        /// root-level bracket.
        /// This is harder than checking the first the last characters,
        /// which will fail in examples like (1 + 2) * (3 + 4), which
        /// contain a root level multiplication *.
        private bool HasOuterBrackets(string s)
        {
            List<char> openBrackets = new List<char>();
            foreach (char c in s)
            {
                if (c.IsCloseBracket())
                {
                    if (!c.Closes(openBrackets.Last()))
                    {
                        throw new FormatException(nameof(s) + " is not of a valid format: " + s);
                    }
                    openBrackets.RemoveAt(openBrackets.Count - 1);
                }
                else if (c.IsOpenBracket())
                {
                    openBrackets.Add(c);
                }
                else
                {
                    // Here we have root-level operators
                    if (openBrackets.Count == 0)
                    {
                        return false;
                    }
                }
            }
            return true;
        }
        /// <summary>
        /// (a + b) -> a + b
        /// (a (b + c)) -> a (b + c)
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        private string RemoveOuterBrackets(string s)
        {
            if (!(s[0].IsOpenBracket() && s.Last().Closes(s[0])))
            {
                throw new FormatException(nameof(s) + " is not of a valid format: " + s);
            }
            return s.Substring(1, s.Length - 2).Trim();
        }
        
        private bool TryParse(string s, out FConstant constant)
        {
            if (s == "pi")
            {
                constant = new FConstantSpecial(ConstantName.Pi);
                return true;
            }
            if (s == "e")
            {
                constant = new FConstantSpecial(ConstantName.E);
                return true;
            }

            if (int.TryParse(s, out int ivalue))
            {
                // at the moment this is still interpreted 
                // as a double, may change in the future.
                constant = new FConstantRational(ivalue); 
                return true;
            }

            if (double.TryParse(s, out double dvalue))
            {
                BigDecimal d = new BigDecimal(dvalue, Expr.DefaultSignificantFigures);
                constant = new FConstantDecimal(d);
                return true;
            }

            constant = null;
            return false;
        }
        private bool TryParseFunction(string s, string funcName, IFunction func, out Expr expr, int expectedArguments = 1)
        {
            Regex r1 = new Regex(funcName + "\\s*\\(.*\\)");
            if (r1.IsMatch(s) && r1.Match(s).Index == 0)
            {
                // find index of the first '('
                BracketSet bracket = s.OutermostBracketSet(0);
                if (bracket.OpenIndex < 0 || bracket.CloseIndex < 0)
                {
                    throw new FormatException(nameof(s) + " is not formatted correctly: " + s);
                }

                string[] args = bracket.ExtractInternal(s).Split(',');
                if (args.Length != expectedArguments)
                {
                    throw new FormatException("Expected " + expectedArguments + " for " + funcName);
                }

                Expr[] argExprs = new Expr[expectedArguments];
                for (int i = 0; i < expectedArguments; ++i)
                {
                    argExprs[i] = ParseInner(args[i]);
                }
                expr = new Expr(func, argExprs);
                return true;
            }

            // This kind of loose formatting is not supported for functions with more than 1 argument
            if (expectedArguments == 1)
            {
                Regex r2 = new Regex(funcName + "\\s*");
                if (r2.IsMatch(s) && r1.Match(s).Index == 0)
                {
                    s = s.Substring(funcName.Length).Trim();

                    // Stop at the first space
                    s = s.Split(' ')[0];
                    expr = new Expr(func, ParseInner(s));
                    return true;
                }
            }
           

            expr = null;
            return false;
        }
    }
    internal class Operator
    {
        internal int Index { get; set; }
        internal string Symbol { get; set; }

        public Operator(int index, string symbol)
        {
            Index = index;
            Symbol = symbol;
        }
    }
}
