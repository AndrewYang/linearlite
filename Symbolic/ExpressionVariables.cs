﻿using LinearNet.Symbolic.Structs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Symbolic
{
    /// <cat>symbolic-algebra</cat>
    public partial class Expr
    {
        /// <summary>
        /// Returns a set of indeterminates (unknown variables) referenced by this expression
        /// </summary>
        /// <returns>A set of variables.</returns>
        public HashSet<string> Variables()
        {
            HashSet<string> variables = new HashSet<string>();
            GetVariables(variables);
            return variables;
        }
        private void GetVariables(HashSet<string> variableNames)
        {
            if (IsLeaf)
            {
                if (Function.Type == FType.Var)
                {
                    FVariable v = Function as FVariable;
                    if (!variableNames.Contains(v.VariableName))
                    {
                        variableNames.Add(v.VariableName);
                    }
                }
            }
            else
            {
                foreach (Expr expr in Children)
                {
                    expr.GetVariables(variableNames);
                }
            }
        }

        /// <summary>
        /// Returns whether this expression contains the specified variable.
        /// </summary>
        /// <param name="variableName">The variable name to check.</param>
        /// <returns>Whether this expression contains the variable.</returns>
        public bool DependsOn(string variableName)
        {
            // Check self
            if (Function.Type == FType.Var)
            {
                FVariable v = Function as FVariable;
                if (v.VariableName == variableName)
                {
                    return true;
                }
            }

            if (Children != null)
            {
                // Check children
                foreach (Expr expr in Children)
                {
                    if (expr.DependsOn(variableName))
                    {
                        return true;
                    }
                }
            }
            return false;
        }
    }
}
