﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LinearNet.Symbolic
{
    /// <summary>
    /// These are functions that may lead to incorrect results due to finite precision arithmetic
    /// At the moment the only constant types that are supported are doubles...in the future, rationals 
    /// and integers will also be supported, but need to figure out how to implement them properly first...
    /// hence the need for this class for the time being
    /// </summary>
    internal static class RiskyFunctions
    {
        internal static bool IsExactlyTwo(double x)
        {
            return Math.Abs(x - 2.0) < 1e-6;
        }
        internal static int IntValue(double x)
        {
            return (int)Math.Round(x);
        }
    }
}
