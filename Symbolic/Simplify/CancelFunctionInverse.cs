﻿using LinearNet.Symbolic.Structs;
using System;
using System.Collections.Generic;
using System.Text;

namespace LinearNet.Symbolic.Simplify
{
    /// <summary>
    /// Handles function inverses only
    /// Arithmetic inverses are handled via the path 
    /// CollectLikeTerms -> NumericSimplify
    /// </summary>
    internal class CancelFunctionInverse : ISimplification
    {
        public bool Apply(ref Expr expr)
        {
            if (expr.IsLeaf) return false;

            // Handle function inverses f^-1 o f (x) -> x
            if (expr.Function.IsInvertible && expr.Function.Type.Invert() == expr.Children[0].Function.Type)
            {
                expr = expr.Children[0].Children[0];
                return true;
            }
            return false;
        }
    }
}
