﻿using LinearNet.Helpers;
using LinearNet.Symbolic.Structs;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace LinearNet.Symbolic.Simplify
{
    /// <summary>
    /// The template is currently just parsed as an expression, however this may be different in the future.
    /// </summary>
    public class ExprTemplate
    {
        private readonly Expr _template;

        public Expr Expression { get { return _template; } }

        public ExprTemplate(string template)
        {
            // Potentially replate this in the future
            _template = Expr.Parse(template);
        }


        /// <summary>
        /// Shallow matching
        /// </summary>
        /// <param name="expression"></param>
        /// <param name="template"></param>
        /// <returns></returns>
        public bool Matches(Expr expression)
        {
            return Matches(expression, _template, new Dictionary<string, Expr>());
        }

        /// <summary>
        /// Shallow matching (up to associativity, no other simplification), returning the matches variables as a dictionary
        /// </summary>
        /// <param name="expression"></param>
        /// <param name="variables"></param>
        /// <returns></returns>
        public bool Matches(Expr expression, out Dictionary<string, Expr> variables)
        {
            variables = new Dictionary<string, Expr>();
            return Matches(expression, _template, variables);
        }

        private bool Matches(Expr expression, Expr template, Dictionary<string, Expr> knownExpressions)
        {
            // Special matching logic here
            if (template.IsVar)
            {
                string varName = (template.Function as FVariable).VariableName;
                if (knownExpressions.ContainsKey(varName))
                {
                    // Check for equality up to permutation 
                    //Expr e = knownExpressions[varName];
                    //if (e.Function.IsAssociative)
                    //{
                    //    return knownExpressions[varName].EqualsUptoAssociativity(expression);
                    //}
                    return knownExpressions[varName].Equals(expression);
                }

                // Not already present - insert then return true
                knownExpressions[varName] = expression;
                return true;
            }

            // Regular matching starts here
            if (expression.Function.Type != template.Function.Type) return false;

            // Same type, both are leaves
            if (expression.IsLeaf)
            {
                return expression.Equals(template);
            }

            // Check children length - (remember this only exists if these are not leaves)
            if (expression.Children.Length != template.Children.Length) return false;

            // Recursively check child expressions - being careful with associative operations
            if (expression.Function.IsAssociative)
            {
                // Current: exhaustive search of all permutations? 
                // Consider instead:
                // some kind of hashing?
                // hashing groups and then within each group checking all permutations?
                // ML state-based approach without considering all permutations?

                Expr[] templateChildren = new Expr[template.Children.Length];
                Array.Copy(template.Children, templateChildren, templateChildren.Length);
                PermutationIterator<Expr> iterator = new PermutationIterator<Expr>(templateChildren);

                foreach (Expr[] permutation in iterator.GetEnumerator())
                {
                    Dictionary<string, Expr> tempDict = new Dictionary<string, Expr>(knownExpressions);
                    if (Matches(expression.Children, permutation, tempDict))
                    {
                        // Copy expression list to known expressions
                        foreach (KeyValuePair<string, Expr> e in tempDict)
                        {
                            if (!knownExpressions.ContainsKey(e.Key))
                            {
                                knownExpressions[e.Key] = e.Value;
                            }
                        }
                        return true;
                    }
                }
                return false;
            }
            else
            {
                return Matches(expression.Children, template.Children, knownExpressions);
            }
        }

        private bool Matches(Expr[] array1, Expr[] array2, Dictionary<string, Expr> knownExpressions)
        {
            for (int i = 0; i < array1.Length; ++i)
            {
                if (!Matches(array1[i], array2[i], knownExpressions))
                {
                    return false;
                }
            }
            return true;
        }
    }
}
