﻿using LinearNet.Symbolic.Structs;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LinearNet.Symbolic.Simplify
{
    /// <summary>
    /// Flatten all associative operations (e.g. +, *) into a single level
    /// </summary>
    /// <param name="expr"></param>
    /// <returns></returns>
    internal class FlattenAssociativeOperations : ISimplification
    {
        public bool Apply(ref Expr expr)
        {
            bool changed = FlattenAssociativeOperationsWithoutSort(expr);
            if (changed)
            {
                ExprCompare.ToCanonicalSortOrder(expr);
            }
            return changed;
        }

        /// <summary>
        /// Sorting is expensive, we only do it once 
        /// </summary>
        private static bool FlattenAssociativeOperationsWithoutSort(Expr expr)
        {
            if (expr.IsLeaf)
            {
                return false;
            }

            bool changed = false;

            // Must re-order sub-expressions first, since their ordering can influence 
            // upper levels
            // Also note that to guarantee we are looping through all the children, we must 
            // perform this loop regardless of whether the function itself is associative...
            foreach (Expr child in expr.Children)
            {
                changed = FlattenAssociativeOperationsWithoutSort(child) || changed;
            }

            // 1 + (2 + x) -> 1 + 2 + x
            while (TryFlatten(expr, FType.Add)) changed = true;

            // 1 * (2 * x) -> 1 * 2 * x
            while (TryFlatten(expr, FType.Multiply)) changed = true;

            return changed;
        }

        /// <summary>
        /// Only applied to functions that can be flatterned.
        /// </summary>
        /// <param name="expr"></param>
        /// <param name="operation"></param>
        /// <returns>The child factors.</returns>
        private static bool TryFlatten(Expr expr, FType operation)
        {
            if (expr.Function.Type != operation ||
                expr.Children.Any(e => e.Function.Type == operation)) 
            {
                return false;
            }

            // Count the number of factors 
            int count = 0;
            foreach (Expr e in expr.Children)
            {
                if (e.Function.Type == operation)
                {
                    count += e.Children.Length;
                }
                else
                {
                    count++;
                }
            }

            // The array of factors
            Expr[] factors = new Expr[count];
            int k = 0;
            foreach (Expr child in expr.Children)
            {
                if (child.Function.Type == operation)
                {
                    foreach (Expr e in child.Children)
                    {
                        factors[k++] = e;
                    }
                }
                else
                {
                    factors[k++] = child;
                }
            }

            // Overwrite function
            if (operation == FType.Add)
            {
                expr.Function = new FAdd(factors.Length);
            }
            else if (operation == FType.Multiply)
            {
                expr.Function = new FMultiply(factors.Length);
            }
            else
            {
                throw new NotSupportedException();
            }
            expr.Children = factors;

            return true;
        }
    }
}
