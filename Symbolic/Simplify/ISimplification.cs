﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LinearNet.Symbolic.Simplify
{
    /// <summary>
    /// Interface for all simplification routines
    /// </summary>
    interface ISimplification
    {
        /// <summary>
        /// Returns whether a simplification was made. If so, the expr 
        /// object will be altered.
        /// </summary>
        /// <param name="expr"></param>
        /// <returns></returns>
        bool Apply(ref Expr expr);
    }
}
