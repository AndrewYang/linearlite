﻿using LinearNet.Symbolic.Structs;
using System;
using System.Collections.Generic;
using System.Text;

namespace LinearNet.Symbolic.Simplify
{
    /// <summary>
    /// An alternative road - string expression -> string expression
    /// using template matching
    /// </summary>
    internal class TemplateSimplification : ISimplification
    {
        private readonly ExprTemplate _inputExpr, _outputExpr;

        /// <summary>
        /// </summary>
        /// <param name="input"></param>
        /// <param name="output"></param>
        internal TemplateSimplification(string input, string output)
        {
            _inputExpr = new ExprTemplate(input);
            _outputExpr = new ExprTemplate(output);
        }

        public bool Apply(ref Expr expr)
        {
            if (_inputExpr.Matches(expr, out Dictionary<string, Expr> es))
            {
                expr = new Expr(_outputExpr.Expression);
                Substitute(ref expr, es);
                return true;
            }
            return false;
        }

        // Faster than using individual substitution
        private void Substitute(ref Expr e, Dictionary<string, Expr> variables)
        {
            if (e.IsLeaf)
            {
                if (e.IsVar)
                {
                    string varName = (e.Function as FVariable).VariableName;
                    if (variables.ContainsKey(varName))
                    {
                        e = new Expr(variables[varName]);
                        return;
                    }
                }
                return;
            }

            // Iterate over children
            for (int i = 0; i < e.Children.Length; ++i)
            {
                Substitute(ref e.Children[i], variables);
            }
        }
    }
}
