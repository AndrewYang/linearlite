﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Symbolic
{
    internal static class StringUtils
    {
        internal static bool IsOpenBracket(this char c) => c == '(' || c == '[' || c == '{';
        internal static bool IsCloseBracket(this char c) => c == ')' || c == ']' || c == '}';
        internal static bool IsDigit(this char c) => '0' <= c && c <= '9';
        internal static bool IsOperator(this char c)
        {
            return c == '+' || c == '-' || c == '*' || c == '/' || c == '^';
        }

        internal static char GetComplementingBracket(this char c)
        {
            switch (c)
            {
                case '(': return ')';
                case '{': return '}';
                case '[': return ']';
                case ')': return '(';
                case '}': return '{';
                case ']': return '[';
            }
            throw new NotSupportedException();
        }
        internal static bool Complements(this char a, char b) 
        {
            return
                (a == '(' && b == ')') ||
                (a == ')' && b == '(') ||
                (a == '[' && b == ']') ||
                (a == ']' && b == '[') ||
                (a == '{' && b == '}') ||
                (a == '}' && b == '{');
        }
        /// <summary>
        /// Returns whether char a closes char b, e.g. a = ']' and b = '['
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        internal static bool Closes(this char a, char b)
        {
            return 
                (a == ')' && b == '(') ||
                (a == ']' && b == '[') ||
                (a == '}' && b == '{');
        }

        /// <summary>
        /// Returns the outermost bracket set of a string s
        /// E.g. (a + b) -> a + b
        /// E.g. (a + b(c + d)) -> a + b(c + d)
        /// E.g. a + c(d + e) -> a + c(d + e)
        /// </summary>
        /// <param name="s"></param>
        /// <param name="openBracketIndex"></param>
        /// <param name="closeBracketIndex"></param>
        internal static BracketSet OutermostBracketSet(this string s, int startIndex)
        {
            int len = s.Length, i;
            for (i = startIndex; i < len; ++i)
            {
                if (s[i].IsOpenBracket())
                {
                    break;
                }
            }

            if (i == len)
            {
                return new BracketSet(-1, -1);
            }
            else
            {
                return new BracketSet(i, CloseBracketIndex(s, i));
            }
        }

        /// <summary>
        /// Returns the index of the corresponding close bracket, given the opening bracket's index
        /// </summary>
        /// <param name="s"></param>
        /// <param name="openingBracketIndex"></param>
        /// <returns></returns>
        internal static int CloseBracketIndex(this string s, int openingBracketIndex)
        {
            char c = s[openingBracketIndex];
            if (!c.IsOpenBracket())
            {
                return -1;
            }

            List<char> open_brackets = new List<char>() { c };
            int len = s.Length, i;
            for (i = openingBracketIndex + 1; i < len; ++i)
            {
                if (s[i].IsOpenBracket())
                {
                    open_brackets.Add(s[i]);
                }
                else if (s[i].IsCloseBracket())
                {
                    int last = open_brackets.Count - 1;
                    if (last >= 0 && s[i].Closes(open_brackets[last]))
                    {
                        open_brackets.RemoveAt(last);

                        if (last == 0)
                        {
                            return i;
                        }
                    }
                }
            }
            return -1;
        }
        
        /// <summary>
        /// Returns the first instance of any one of the alternatives, or, if none are present, return -1
        /// </summary>
        /// <param name="s"></param>
        /// <param name="alternatives"></param>
        /// <returns></returns>
        internal static int IndexOf(this string s, params string[] alternatives)
        {
            int minIndex = int.MaxValue;
            foreach (string alt in alternatives)
            {
                int i = s.IndexOf(alt);
                if (i >= 0 && i < minIndex)
                {
                    minIndex = i;
                }
            }

            if (minIndex == int.MaxValue)
            {
                return -1;
            }
            return minIndex;
        }

    }
    internal class BracketSet
    {
        internal int OpenIndex { get; set; }
        internal int CloseIndex { get; set; }
        
        internal BracketSet(int open, int close)
        {
            OpenIndex = open;
            CloseIndex = close;
        }
        
        /// <summary>
        /// Extract the internal contents from a given string, without the brackets themselves.
        /// E.g. (a + b) -> a + b
        /// E.g. (a + b(c + d)) -> a + b(c + d)
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        internal string ExtractInternal(string s)
        {
            if (OpenIndex < 0 || CloseIndex < 0 || CloseIndex <= OpenIndex)
            {
                return string.Empty;
            }
            return s.Substring(OpenIndex + 1, CloseIndex - OpenIndex - 1);
        }
        /// <summary>
        /// Extract the external contents from a string (including the brackets)
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        internal string ExtractExternal(string s)
        {
            if (OpenIndex < 0 || CloseIndex < 0 || CloseIndex <= OpenIndex)
            {
                return string.Empty;
            }
            return s.Substring(OpenIndex, CloseIndex - OpenIndex + 1);  // need to check this
        }

        public override string ToString()
        {
            return "(" + OpenIndex + ", " + CloseIndex + ")";
        }
    }
}
