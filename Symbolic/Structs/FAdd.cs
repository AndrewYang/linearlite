﻿using LinearNet.Structs.Fields;

namespace LinearNet.Symbolic.Structs
{
    internal class FAdd : IFunction
    {
        internal FAdd(int count) : base(FType.Add, count, true) { }

        public override string ToString()
        {
            return "+";
        }

        public override bool Equals(IFunction other)
        {
            if (other is null) return false;
            return other.Type == FType.Add;
        }

        protected override bool TryEvaluateInner(BigRational[] param, out BigRational result)
        {
            BigRational sum = BigRational.Zero;
            for (int i = 0; i < ParameterCount; ++i)
            {
                sum = sum.Add(param[i]);
            }
            result = sum;
            return true;
        }

        protected override bool TryEvaluateInner(BigDecimal[] param, out BigDecimal result)
        {
            int min_sf = int.MaxValue;
            for (int i = 0; i < ParameterCount; ++i)
            {
                if (min_sf > param[i].SignificantFigures)
                {
                    min_sf = param[i].SignificantFigures;
                }
            }

            BigDecimal sum = new BigDecimal(0, min_sf);
            for (int i = 0; i < ParameterCount; ++i)
            {
                sum = sum.Add(param[i]);
            }
            sum.Round(min_sf);

            result = sum;
            return true;
        }
    }
}
