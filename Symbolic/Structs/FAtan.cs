﻿using LinearNet.Structs;
using LinearNet.Structs.Fields;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Symbolic.Structs
{
    internal class FAtan : IFunction
    {
        internal FAtan() : base(FType.Atan, 1, false)
        {

        }

        public override string ToString()
        {
            return "atan";
        }

        public override bool Equals(IFunction other)
        {
            if (other is null) return false;
            return other.Type == Type;
        }

        protected override bool TryEvaluateInner(BigRational[] param, out BigRational result)
        {
            // special values only
            BigRational p = param[0];
            if (p.IsZero)
            {
                result = BigRational.Zero;
                return true;
            }

            result = default;
            return true;
        }

        protected override bool TryEvaluateInner(BigDecimal[] param, out BigDecimal result)
        {
            result = BigDecimal.Atan(param[0]);
            return true;
        }
    }
}
