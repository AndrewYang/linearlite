﻿using LinearNet.Functions;
using LinearNet.Structs;
using LinearNet.Structs.Fields;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Symbolic.Structs
{
    internal class FBeta : IFunction
    {
        internal FBeta() : base(FType.Beta, 1, false) { }

        public override string ToString()
        {
            return "Beta";
        }

        public override bool Equals(IFunction other)
        {
            if (other is null) return false;
            return other.Type == Type;
        }

        protected override bool TryEvaluateInner(BigRational[] param, out BigRational result)
        {
            // defined for integers thanks for Gamma(n) = (n - 1)! for n in Z.

            BigRational a = param[0], b = param[1];
            if (a.IsInteger && b.IsInteger && !a.IsNegative && !b.IsNegative)
            {
                // Get integer values
                BigInteger _a = a.Numerator / a.Denominator,
                    _b = b.Numerator / b.Denominator,
                    sum = _a + _b;

                // currently only supported for < int.MaxValue
                if (sum <= int.MaxValue)
                {
                    result = new BigRational(
                        MathFunctions.BigFactorial((int)_a - 1) * MathFunctions.BigFactorial((int)_b - 1),
                        MathFunctions.BigFactorial((int)sum - 1));
                    return true;
                }
            }

            // General case - not defined
            result = default;
            return false;
        }

        protected override bool TryEvaluateInner(BigDecimal[] param, out BigDecimal result)
        {
            result = MathFunctions.Beta(param[0], param[1]);
            return true;
        }
    }
}
