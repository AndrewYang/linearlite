﻿using LinearNet.Structs;
using LinearNet.Structs.Fields;
using System;
using System.Diagnostics.CodeAnalysis;
using System.Numerics;

namespace LinearNet.Symbolic.Structs
{
    public abstract class FConstant : IFunction, IComparable<FConstant>
    {
        public ConstantType ConstantType { get; private set; }
        public abstract BigDecimal BigDecimalValue { get; }
        public abstract BigInteger BigIntegerValue { get; }
        public abstract BigRational BigRationalValue { get; }
        public abstract double DoubleValue { get; }

        public abstract bool IsInteger { get; }
        public abstract bool IsZero { get; }
        public abstract bool IsOne { get; }
        public abstract bool IsNaN { get; }
        public abstract bool IsRational { get; }

        public abstract FConstant Zero { get; }
        public abstract FConstant One { get; }
        public abstract FConstant NaN { get; }

        /// <summary>
        /// When we are combining two constants together, if they are finite precision
        /// they will be combined using BigDecimal methods...
        /// </summary>
        internal bool IsFinitePrecision
        {
            get
            {
                return ConstantType == ConstantType.DECIMAL;
            }
        }

        internal FConstant(ConstantType type) : base(FType.Const, 0, false) 
        {
            ConstantType = type;
        }

        public abstract int CompareTo(FConstant other);
    }
    public enum ConstantType
    {
        DECIMAL, 
        RATIONAL, 
        EXACT_DECIMAL, // exact constants e.g. pi, e
        COMPLEX
    }
}
