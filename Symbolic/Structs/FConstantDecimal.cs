﻿using LinearNet.Structs;
using LinearNet.Structs.Fields;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Numerics;
using System.Text;

namespace LinearNet.Symbolic.Structs
{
    /// <summary>
    /// Finite but arbitrary-precision decimal representation.
    /// </summary>
    public class FConstantDecimal : FConstant, IField<FConstantDecimal>
    {
        public FConstantDecimal MultiplicativeIdentity => new FConstantDecimal(1);
        public FConstantDecimal AdditiveIdentity => new FConstantDecimal(0);
        public BigInteger Characteristic => BigInteger.Zero;


        public BigDecimal Value { get; protected set; }
        public override BigDecimal BigDecimalValue => Value;
        public override BigInteger BigIntegerValue => throw new NotImplementedException();
        public override BigRational BigRationalValue => throw new NotImplementedException();
        public override double DoubleValue => (double)Value;

        public override bool IsInteger => Value.IsInteger;
        public override bool IsZero => Value.IsZero;
        public override bool IsOne => Value.Equals(BigDecimal.One);
        public override FConstant Zero => AdditiveIdentity;
        public override FConstant One => MultiplicativeIdentity;

        public override bool IsNaN => throw new NotImplementedException();

        public override FConstant NaN => throw new NotImplementedException();

        public override bool IsRational => false;



        /// <summary>
        /// Default constructor - initializes to 0.
        /// </summary>
        public FConstantDecimal() : base(ConstantType.DECIMAL)
        {
            Value = new BigDecimal(BigInteger.Zero, Expr.DefaultSignificantFigures);
        }

        /// <summary>
        /// Initialize a constant object from a <txt>BigDecimal</txt>
        /// </summary>
        /// <param name="d"></param>
        public FConstantDecimal(BigDecimal d) : base(ConstantType.DECIMAL) 
        {
            if (d.SignificantFigures != Expr.DefaultSignificantFigures)
            {
                d.Round(Expr.DefaultSignificantFigures);
            }
            Value = d;
        }

        /// <summary>
        /// Create a new constant object from a <txt>int</txt>.
        /// </summary>
        /// <param name="constant">The value of the constant</param>
        public FConstantDecimal(int constant) : this(new BigDecimal(constant, Expr.DefaultSignificantFigures)) { }

        /// <summary>
        /// Create a new constant object from a <txt>long</txt>.
        /// </summary>
        /// <param name="constant">The value of the constant</param>
        public FConstantDecimal(long constant) : this(new BigDecimal(constant, Expr.DefaultSignificantFigures)) { }

        /// <summary>
        /// Create a new constant object from a <txt>System.Numerics.BigInteger</txt>.
        /// </summary>
        /// <param name="constant">The value of the constant</param>
        public FConstantDecimal(BigInteger constant) : this(new BigDecimal(constant, Expr.DefaultSignificantFigures)) { }

        /// <summary>
        /// Create a new constant object from a <txt>float</txt>.
        /// </summary>
        /// <param name="constant">The value of the constant</param>
        public FConstantDecimal(float constant) : this(new BigDecimal(constant, Expr.DefaultSignificantFigures)) { }

        /// <summary>
        /// Create a new constant object from a <txt>double</txt>.
        /// </summary>
        /// <param name="constant">The value of the constant</param>
        public FConstantDecimal(double constant) : this(new BigDecimal(constant, Expr.DefaultSignificantFigures)) { }

        /// <summary>
        /// Create a new constant object from a <txt>decimal</txt>.
        /// </summary>
        /// <param name="constant">The value of the constant</param>
        public FConstantDecimal(decimal constant) : this(new BigDecimal(constant, Expr.DefaultSignificantFigures)) { }


        public FConstantDecimal Add(FConstantDecimal g)
        {
            return new FConstantDecimal(Value.Add(g.Value));
        }
        public FConstantDecimal AdditiveInverse()
        {
            return new FConstantDecimal(Value.AdditiveInverse());
        }

        public bool ApproximatelyEquals(FConstantDecimal e, double eps)
        {
            throw new NotImplementedException();
        }

        public bool Equals(FConstantDecimal e)
        {
            throw new NotImplementedException();
        }

        public override bool Equals(IFunction other)
        {
            throw new NotImplementedException();
        }

        public FConstantDecimal MultiplicativeInverse()
        {
            return new FConstantDecimal(Value.MultiplicativeInverse());
        }

        public FConstantDecimal Multiply(FConstantDecimal b)
        {
            return new FConstantDecimal(Value.Multiply(b.Value));
        }

        public override string ToString()
        {
            return Value.ToString();
        }

        protected override bool TryEvaluateInner(BigRational[] param, out BigRational result)
        {
            result = default;
            return false;
        }

        protected override bool TryEvaluateInner(BigDecimal[] param, out BigDecimal result)
        {
            result = Value;
            return true;
        }

        public override int CompareTo(FConstant other)
        {
            if (other is null)
            {
                return 1;
            }

            // Irrespective of type, use big decimal value as comparison
            if (ConstantType == ConstantType.DECIMAL ||
                ConstantType == ConstantType.RATIONAL)
            {
                return Value.CompareTo(other.BigDecimalValue);
            }

            throw new NotImplementedException();
        }

        public override int GetHashCode()
        {
            return base.GetHashCode() ^ Value.GetHashCode();
        }
    }
}
