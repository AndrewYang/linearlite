﻿using LinearNet.Structs;
using LinearNet.Structs.Fields;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Numerics;
using System.Text;

namespace LinearNet.Symbolic.Structs
{
    public class FConstantRational : FConstant, IField<FConstantRational>
    {
        public BigRational Value { get; private set; }

        public override BigDecimal BigDecimalValue => Value.ToBigDecimal(Expr.DefaultSignificantFigures);
        public override BigInteger BigIntegerValue => Value.Numerator / Value.Denominator;
        public override BigRational BigRationalValue => Value;
        public override double DoubleValue => (double)Value;

        public FConstantRational MultiplicativeIdentity => new FConstantRational(1);
        public FConstantRational AdditiveIdentity => new FConstantRational(0);
        public BigInteger Characteristic => BigInteger.Zero;


        public override bool IsInteger => Value.IsInteger;
        public override bool IsZero => Value.IsZero;
        public override bool IsOne => Value.IsOne;
        public override bool IsRational => true;
        public override FConstant Zero => AdditiveIdentity;
        public override FConstant One => MultiplicativeIdentity;
        public override bool IsNaN => Value.IsNaN;
        public override FConstant NaN => new FConstantRational(BigRational.NaN);



        // Default constructor, initializes to 0.
        public FConstantRational() : base(ConstantType.RATIONAL)
        {
            Value = 0;
        }
        public FConstantRational(BigRational bigRational) : base(ConstantType.RATIONAL)
        {
            Value = bigRational;
        }

        public override bool Equals(IFunction other)
        {
            if (other is null) return false;

            if (other.Type != FType.Const) return false;

            FConstant c = other as FConstant;
            if (c.ConstantType != ConstantType.RATIONAL)
            {
                return false;
            }

            FConstantRational r = c as FConstantRational;
            return r.Value == Value;
        }

        public override string ToString()
        {
            return Value.ToString();
        }

        public override int GetHashCode()
        {
            return base.GetHashCode() ^ Value.GetHashCode();
        }

        public FConstantRational MultiplicativeInverse()
        {
            return new FConstantRational(Value.MultiplicativeInverse());
        }

        public FConstantRational AdditiveInverse()
        {
            return new FConstantRational(Value.MultiplicativeInverse());
        }

        public FConstantRational Multiply(FConstantRational b)
        {
            return new FConstantRational(Value.Multiply(b.Value));
        }

        public FConstantRational Add(FConstantRational g)
        {
            return new FConstantRational(Value.Add(g.Value));
        }

        public bool Equals(FConstantRational e)
        {
            throw new NotImplementedException();
        }

        public bool ApproximatelyEquals(FConstantRational e, double eps)
        {
            throw new NotImplementedException();
        }

        protected override bool TryEvaluateInner(BigRational[] param, out BigRational result)
        {
            result = Value;
            return true;
        }

        protected override bool TryEvaluateInner(BigDecimal[] param, out BigDecimal result)
        {
            result = BigDecimalValue;
            return true;
        }

        public override int CompareTo(FConstant other)
        {
            if (other is null)
            {
                return 1;
            }

            if (other.ConstantType == ConstantType.RATIONAL)
            {
                BigRational comp = (other as FConstantRational).Value;
                return Value.CompareTo(comp);
            }

            if (other.ConstantType == ConstantType.DECIMAL ||
                other.ConstantType == ConstantType.EXACT_DECIMAL)
            {
                return BigDecimalValue.CompareTo(other.BigDecimalValue);
            }

            throw new NotImplementedException();
        }
    }
}
