﻿using LinearNet.Structs.Fields;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Numerics;
using System.Text;

namespace LinearNet.Symbolic.Structs
{
    /// <summary>
    /// Special constants
    /// </summary>
    internal class FConstantSpecial : FConstant
    {
        private readonly ConstantName _name;
        internal ConstantName Name { get { return _name; } }

        public override BigDecimal BigDecimalValue => GetValue();
        public override BigInteger BigIntegerValue => throw new NotImplementedException();
        public override BigRational BigRationalValue => throw new NotImplementedException();

        public override double DoubleValue => (double)BigDecimalValue;

        public override bool IsInteger => false;
        public override bool IsZero => false;
        public override bool IsOne => false;
        public override bool IsNaN => false;
        public override bool IsRational => false;

        public override FConstant Zero => new FConstantDecimal(BigDecimal.Zero);
        public override FConstant One => new FConstantDecimal(BigDecimal.One);
        public override FConstant NaN => throw new NotImplementedException();


        public FConstantSpecial(ConstantName name) : base(ConstantType.EXACT_DECIMAL)
        {
            _name = name;
        }

        private BigDecimal GetValue()
        {
            if (_name == ConstantName.E)
            {
                BigDecimal e = BigDecimal.E;
                if (e.SignificantFigures > Expr.DefaultSignificantFigures)
                {
                    e.Round(Expr.DefaultSignificantFigures);
                    return e;
                }
                else if (e.SignificantFigures == Expr.DefaultSignificantFigures)
                {
                    return e;
                }
                else
                {
                    return BigDecimal.Euler(Expr.DefaultSignificantFigures);
                }
            }

            else if (_name == ConstantName.Pi)
            {
                BigDecimal pi = BigDecimal.PI;
                if (pi.SignificantFigures > Expr.DefaultSignificantFigures)
                {
                    pi.Round(Expr.SignificantFigures);
                    return pi;
                }
                else if (pi.SignificantFigures == Expr.DefaultSignificantFigures)
                {
                    return pi;
                }
                else
                {
                    return BigDecimal.Pi(Expr.DefaultSignificantFigures);
                }
            }

            throw new NotImplementedException();
        }

        public override int CompareTo(FConstant other)
        {
            return BigDecimalValue.CompareTo(other.BigDecimalValue);
        }

        public override bool Equals(IFunction other)
        {
            // Exact equality is only provided by equality of constant name
            if (other.Type != FType.Const)
            {
                return false;
            }

            FConstant c = other as FConstant;
            if (c.ConstantType != ConstantType.EXACT_DECIMAL)
            {
                return false;
            }

            FConstantSpecial s = c as FConstantSpecial;
            return _name == s._name;
        }

        public override string ToString()
        {
            return _name switch
            {
                ConstantName.E => "e",
                ConstantName.Pi => "pi",
                _ => string.Empty,
            };
        }

        protected override bool TryEvaluateInner(BigRational[] param, out BigRational result)
        {
            throw new NotImplementedException();
        }

        protected override bool TryEvaluateInner(BigDecimal[] param, out BigDecimal result)
        {
            result = BigDecimalValue;
            return true;
        }

    }
    public enum ConstantName
    {
        E, Pi
    }
}
