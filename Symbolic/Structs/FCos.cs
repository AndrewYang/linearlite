﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LinearNet.Structs;
using LinearNet.Structs.Fields;

namespace LinearNet.Symbolic.Structs
{
    internal class FCos : IFunction
    {
        internal FCos() : base(FType.Cos, 1, false) { }

        public override string ToString()
        {
            return "cos";
        }

        public override bool Equals(IFunction other)
        {
            if (other is null) return false;
            return other.Type == FType.Cos;
        }

        protected override bool TryEvaluateInner(BigRational[] param, out BigRational result)
        {
            // Special values?
            if (param[0].IsZero)
            {
                result = BigRational.One;
                return true;
            }

            // General case is unsolvable
            result = default;
            return false;
        }

        protected override bool TryEvaluateInner(BigDecimal[] param, out BigDecimal result)
        {
            result = BigDecimal.Cos(param[0]);
            return true;
        }
    }
}
