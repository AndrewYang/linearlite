﻿using LinearNet.Structs;
using LinearNet.Structs.Fields;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Symbolic.Structs
{
    internal class FCosh : IFunction
    {
        internal FCosh() : base(FType.Cosh, 1, false) { }

        public override string ToString()
        {
            return "cosh";
        }

        public override bool Equals(IFunction other)
        {
            if (other is null) return false;
            return other.Type == Type;
        }

        protected override bool TryEvaluateInner(BigRational[] param, out BigRational result)
        {
            // Special values
            if (param[0].IsZero)
            {
                result = BigRational.One;
                return true;
            }

            result = default;
            return false;
        }

        protected override bool TryEvaluateInner(BigDecimal[] param, out BigDecimal result)
        {
            result = BigDecimal.Cosh(param[0]);
            return true;
        }
    }
}
