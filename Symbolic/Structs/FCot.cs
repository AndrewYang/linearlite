﻿using LinearNet.Structs.Fields;
using System;
using System.Collections.Generic;
using System.Text;

namespace LinearNet.Symbolic.Structs
{
    internal class FCot : IFunction
    {
        public FCot() : base(FType.Cot, 1, false) { }

        public override bool Equals(IFunction other)
        {
            return other.Type == FType.Cot;
        }

        public override string ToString()
        {
            return "cot";
        }

        protected override bool TryEvaluateInner(BigRational[] param, out BigRational result)
        {
            throw new NotImplementedException();
        }

        protected override bool TryEvaluateInner(BigDecimal[] param, out BigDecimal result)
        {
            result = BigDecimal.Cot(param[0]);
            return true;
        }
    }
}
