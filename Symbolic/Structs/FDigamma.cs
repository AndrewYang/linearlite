﻿using LinearNet.Functions;
using LinearNet.Structs;
using LinearNet.Structs.Fields;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Symbolic.Structs
{
    internal class FDigamma : IFunction
    {
        internal FDigamma() : base(FType.Digamma, 1, false)
        {

        }

        public override string ToString()
        {
            return "Digamma";
        }

        public override bool Equals(IFunction other)
        {
            if (other is null) return false;
            return other.Type == Type;
        }

        protected override bool TryEvaluateInner(BigRational[] param, out BigRational result)
        {
            result = default;
            return false;
        }

        protected override bool TryEvaluateInner(BigDecimal[] param, out BigDecimal result)
        {
            result = MathFunctions.Digamma(param[0]);
            return true;
        }
    }
}
