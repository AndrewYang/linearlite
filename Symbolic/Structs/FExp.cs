﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LinearNet.Structs;
using LinearNet.Structs.Fields;

namespace LinearNet.Symbolic.Structs
{
    internal class FExp : IFunction
    {
        internal FExp() : base(FType.Exp, 1, false) { }

        public override string ToString()
        {
            return "exp";
        }
        
        public override bool Equals(IFunction other)
        {
            if (other is null) return false;
            return other.Type == Type;
        }

        protected override bool TryEvaluateInner(BigRational[] param, out BigRational result)
        {
            // Special values?
            if (param[0].IsZero)
            {
                result = BigRational.One;
                return true;
            }

            result = default;
            return false;
        }

        protected override bool TryEvaluateInner(BigDecimal[] param, out BigDecimal result)
        {
            result = BigDecimal.Exp(param[0]);
            return true;
        }
    }
}
