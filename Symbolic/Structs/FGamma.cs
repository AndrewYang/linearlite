﻿using LinearNet.Functions;
using LinearNet.Structs;
using LinearNet.Structs.Fields;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Symbolic.Structs
{
    internal class FGamma : IFunction
    {
        internal FGamma() : base(FType.Gamma, 1, false) { }

        public override string ToString()
        {
            return "Gamma";
        }

        public override bool Equals(IFunction other)
        {
            if (other is null) return false;
            return other.Type == Type;
        }

        protected override bool TryEvaluateInner(BigRational[] param, out BigRational result)
        {
            // Supported for integers
            BigRational p = param[0];
            if (p.IsInteger && !p.IsNegative && p < int.MaxValue)
            {
                result = MathFunctions.BigFactorial((int)p - 1);
                return true;
            }

            result = default;
            return false;
        }

        protected override bool TryEvaluateInner(BigDecimal[] param, out BigDecimal result)
        {
            result = MathFunctions.Gamma(param[0]);
            return true;
        }
    }
}
