﻿using LinearNet.Structs;
using LinearNet.Structs.Fields;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Symbolic.Structs
{
    /// <summary>
    /// Implements a multiplicative inverse
    /// </summary>
    internal class FInv : IFunction
    {
        internal FInv() : base(FType.Inv, 1, false)
        {

        }

        public override string ToString()
        {
            return "1 / ";
        }

        public override bool Equals(IFunction other)
        {
            if (other is null) return false;
            return other.Type == Type;
        }

        protected override bool TryEvaluateInner(BigRational[] param, out BigRational result)
        {
            result = param[0].MultiplicativeInverse();
            return true;
        }

        protected override bool TryEvaluateInner(BigDecimal[] param, out BigDecimal result)
        {
            result = param[0].MultiplicativeInverse();
            return true;
        }
    }
}
