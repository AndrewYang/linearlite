﻿using LinearNet.Functions;
using LinearNet.Structs;
using LinearNet.Structs.Fields;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Symbolic.Structs
{
    internal class FLogistic : IFunction
    {
        internal FLogistic() : base(FType.Logistic, 1, false) { }

        public override string ToString()
        {
            return "logistic";
        }

        public override bool Equals(IFunction other)
        {
            if (other is null) return false;
            return other.Type == Type;
        }

        protected override bool TryEvaluateInner(BigRational[] param, out BigRational result)
        {
            // Special values only
            if (param[0].IsZero)
            {
                result = new BigRational(1, 2);
                return true;
            }

            result = default;
            return false;
        }

        protected override bool TryEvaluateInner(BigDecimal[] param, out BigDecimal result)
        {
            result = MathFunctions.Logistic(param[0]);
            return true;
        }
    }
}
