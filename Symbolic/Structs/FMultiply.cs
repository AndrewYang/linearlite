﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LinearNet.Structs;
using LinearNet.Structs.Fields;

namespace LinearNet.Symbolic.Structs
{
    internal class FMultiply : IFunction
    {
        public FMultiply(int count) : base(FType.Multiply, count, true) { }

        public override string ToString()
        {
            return "*";
        }
        
        public override bool Equals(IFunction other)
        {
            if (other is null) return false;
            return other.Type == Type;
        }

        protected override bool TryEvaluateInner(BigRational[] param, out BigRational result)
        {
            BigRational prod = param[0];
            for (int i = 1; i < ParameterCount; ++i)
            {
                prod *= param[i];
            }
            result = prod;
            return true;
        }

        protected override bool TryEvaluateInner(BigDecimal[] param, out BigDecimal result)
        {
            // This will automatically take care of the significant figures 
            BigDecimal prod = param[0];
            for (int i = 1; i < ParameterCount; ++i)
            {
                prod *= param[i];
            }
            prod.Round(prod.SignificantFigures);

            result = prod;
            return true;
        }
    }
}
