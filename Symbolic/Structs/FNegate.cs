﻿using LinearNet.Structs;
using LinearNet.Structs.Fields;

namespace LinearNet.Symbolic.Structs
{
    internal class FNegate : IFunction
    {
        internal FNegate() : base(FType.Negate, 1, false)
        {

        }

        public override string ToString()
        {
            return "-";
        }

        public override bool Equals(IFunction other)
        {
            if (other is null) return false;
            return other.Type == Type;
        }

        protected override bool TryEvaluateInner(BigRational[] param, out BigRational result)
        {
            result = -param[0];
            return true;
        }

        protected override bool TryEvaluateInner(BigDecimal[] param, out BigDecimal result)
        {
            result = -param[0];
            return true;
        }
    }
}
