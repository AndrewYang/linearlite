﻿using LinearNet.Structs;
using LinearNet.Structs.Fields;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Symbolic.Structs
{
    internal class FPow : IFunction
    {
        internal FPow() : base(FType.Pow, 2, false) { }

        public override string ToString()
        {
            return "^";
        }
        
        public override bool Equals(IFunction other)
        {
            if (other is null) return false;
            return other.Type == Type;
        }

        protected override bool TryEvaluateInner(BigRational[] param, out BigRational result)
        {
            // Defined for special values
            BigRational bse = param[0], pow = param[1];

            if (bse.IsZero)
            {
                result = BigRational.Zero;
                return true;
            }
            if (bse.IsOne)
            {
                result = BigRational.One;
                return true;
            }
            if (pow.IsInteger)
            {
                BigInteger bint = pow.Numerator / pow.Denominator;

                if (bint < 0)
                {
                    if (bint >= int.MinValue)
                    {
                        int intpow = Math.Abs((int)bint);
                        result = new BigRational(BigInteger.Pow(bse.Denominator, intpow), BigInteger.Pow(bse.Numerator, intpow)); // inversed, for negative power

                        // no need to simplify - if the original param was simplified the power will also be in lowest terms
                        return true;
                    }
                }
                else
                {
                    if (bint <= int.MaxValue)
                    {
                        int intpow = (int)bint;
                        result = new BigRational(BigInteger.Pow(bse.Numerator, intpow), BigInteger.Pow(bse.Denominator, intpow));

                        // no need to simplify - if the original param was simplified the power will also be in lowest terms
                        return true;
                    }
                }
            }

            // Not supported otherwise
            result = default;
            return false;
        }

        protected override bool TryEvaluateInner(BigDecimal[] param, out BigDecimal result)
        {
            result = BigDecimal.Pow(param[0], param[1]);
            return true;
        }
    }
}
