﻿using LinearNet.Structs;
using LinearNet.Structs.Fields;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Symbolic.Structs
{
    internal class FSqrt : IFunction
    {
        internal FSqrt() : base(FType.Sqrt, 1, false)
        {

        }

        public override string ToString()
        {
            return "sqrt";
        }
        public override bool Equals(IFunction other)
        {
            if (other is null) return false;
            return other.Type == Type;
        }

        protected override bool TryEvaluateInner(BigRational[] param, out BigRational result)
        {
            // Exact values for perfect squares

            BigRational p = param[0];
            if (IsSquare(p.Numerator) && IsSquare(p.Denominator))
            {
                result = new BigRational(Sqrt(p.Numerator), Sqrt(p.Denominator));
                return true;
            }

            result = default;
            return false;
        }

        protected override bool TryEvaluateInner(BigDecimal[] param, out BigDecimal result)
        {
            result = BigDecimal.Sqrt(param[0]);
            return true;
        }

        private static BigInteger Sqrt(BigInteger num)
        {
            if (0 == num) 
            { 
                return 0; 
            }
            
            BigInteger upper = (num / 2) + 1, lower = (upper + (num / upper)) / 2;
            while (lower < upper)
            {
                upper = lower;
                lower = (upper + (num / upper)) / 2;
            }
            return upper;
        }
        private static bool IsSquare(BigInteger num)
        {
            BigInteger sqrt = Sqrt(num);
            return sqrt * sqrt == num;
        }
    }
}
