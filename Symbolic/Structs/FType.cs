﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Symbolic.Structs
{
    public enum FType
    {
        Add, 
        Asin,
        Acos,
        Atan,
        Beta,
        Const,
        Cos,
        Cosh,
        Cot,
        Digamma,
        Erf,
        Erfc,
        Exp,
        Gamma,
        Inv,
        Log,
        Logistic,
        Logit,
        Multiply,
        Negate,
        Pow,
        Sin,
        Sinh,
        Sqrt,
        Tan,
        Tanh,
        Var,
        Zeta
    }
}
