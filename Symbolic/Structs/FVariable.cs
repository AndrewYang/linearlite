﻿using LinearNet.Structs;
using LinearNet.Structs.Fields;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Symbolic.Structs
{
    internal class FVariable : IFunction
    {
        internal string VariableName { get; private set; }

        public FVariable(string name) : base(FType.Var, 0, false)
        {
            VariableName = name;
        }

        public override string ToString()
        {
            return VariableName;
        }

        public override bool Equals(IFunction other)
        {
            if (other is null) return false;
            return other.Type == Type && VariableName == (other as FVariable).VariableName;
        }

        protected override bool TryEvaluateInner(BigRational[] param, out BigRational result)
        {
            throw new NotImplementedException();
        }

        protected override bool TryEvaluateInner(BigDecimal[] param, out BigDecimal result)
        {
            throw new NotImplementedException();
        }
    }
}
