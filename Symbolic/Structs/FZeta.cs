﻿using LinearNet.Functions;
using LinearNet.Structs;
using LinearNet.Structs.Fields;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Symbolic.Structs
{
    internal class FZeta : IFunction
    {
        internal FZeta() : base(FType.Zeta, 1, false) { }

        public override string ToString()
        {
            return "zeta";
        }

        public override bool Equals(IFunction other)
        {
            if (other is null) return false;
            return other.Type == Type;
        }

        protected override bool TryEvaluateInner(BigRational[] param, out BigRational result)
        {
            // Some selected special values
            BigRational p = param[0];
            if (p == -1) 
            {
                result = new BigRational(-1, 12);
                return true;
            }
            if (p.IsZero)
            {
                result = new BigRational(-1, 2);
                return true;
            }
            if (p.IsOne)
            {
                result = BigRational.NaN; // infinity
                return true;
            }

            result = default;
            return false;
        }

        protected override bool TryEvaluateInner(BigDecimal[] param, out BigDecimal result)
        {
            result = MathFunctions.Zeta(param[0]);
            return true;
        }
    }
}
