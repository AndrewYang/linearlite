﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Symbolic.Structs
{
    public static class FunctionExtensions
    {
        private static readonly HashSet<FType> _invertibleFns = new HashSet<FType>()
        {
            FType.Asin, FType.Sin,
            FType.Acos, FType.Cos,
            FType.Atan, FType.Tan,
            FType.Log, FType.Exp,
            FType.Logistic, FType.Logit
        };

        internal static bool IsInvertible(this FType type)
        {
            return _invertibleFns.Contains(type);
        }
        internal static FType Invert(this FType type)
        {
            switch (type)
            {
                case FType.Asin: return FType.Sin;
                case FType.Sin: return FType.Asin;

                case FType.Acos: return FType.Cos;
                case FType.Cos: return FType.Acos;

                case FType.Atan: return FType.Tan;
                case FType.Tan: return FType.Atan;

                case FType.Log: return FType.Exp;
                case FType.Exp: return FType.Log;

                case FType.Logistic: return FType.Logit;
                case FType.Logit: return FType.Logistic;
            }

            throw new NotImplementedException();
        }
    }
}
