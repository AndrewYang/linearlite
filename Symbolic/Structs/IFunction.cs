﻿using LinearNet.Structs;
using LinearNet.Structs.Fields;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Symbolic.Structs
{
    public abstract class IFunction : IEquatable<IFunction>
    {
        /// <summary>
        /// True if the function is associative, e.g. + and *
        /// </summary>
        public bool IsAssociative { get; private set; }
        public FType Type { get; private set; }
        public int ParameterCount { get; private set; }

        /// <summary>
        /// Returns whether this function is invertible, based on its type.
        /// </summary>
        internal bool IsInvertible { get { return Type.IsInvertible(); } }

        protected IFunction(FType name, int parameters, bool isAssociative)
        {
            Type = name;
            ParameterCount = parameters;
            IsAssociative = isAssociative;
        }
        
        public bool TryEvaluate(BigRational param, out FConstantRational result)
        {
            return TryEvaluate(new BigRational[] { param }, out result);
        }
        public bool TryEvaluate(BigRational[] param, out FConstantRational result)
        {
            if (param.Length != ParameterCount)
            {
                throw new InvalidOperationException();
            }
            if (TryEvaluateInner(param, out BigRational output))
            {
                result = new FConstantRational(output);
                return true;
            }
            result = null;
            return false;
        }
        public bool TryEvaluate(BigDecimal param, out FConstantDecimal result)
        {
            return TryEvaluate(new BigDecimal[] { param }, out result);
        }
        public bool TryEvaluate(BigDecimal[] param, out FConstantDecimal result)
        {
            if (param.Length != ParameterCount)
            {
                throw new InvalidOperationException();
            }
            if (TryEvaluateInner(param, out BigDecimal output)) 
            {
                result = new FConstantDecimal(output);
                return true;
            }
            result = null;
            return false;
        }

        protected abstract bool TryEvaluateInner(BigRational[] param, out BigRational result);
        protected abstract bool TryEvaluateInner(BigDecimal[] param, out BigDecimal result);

        public new abstract string ToString();
        public abstract bool Equals(IFunction other);
        public override bool Equals(object obj)
        {
            if (obj is null)
            {
                return false;
            }

            return Equals(obj as IFunction);
        }
        public override int GetHashCode()
        {
            return Type.GetHashCode() * 101 + ParameterCount * 7;
        }

        public static bool operator ==(IFunction a, IFunction b) => a.Equals(b);
        public static bool operator !=(IFunction a, IFunction b) => !a.Equals(b);
    }
}
