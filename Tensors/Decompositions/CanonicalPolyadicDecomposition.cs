﻿using LinearNet.Providers;
using LinearNet.Providers.LinearAlgebra;
using LinearNet.Structs;
using System;
using System.Collections.Generic;

namespace LinearNet.Tensors.Decompositions
{
    public static class CanonicalPolyadicDecomposition
    {
        /// <summary>
        /// Calculates the rank-1 CPD of a dense tensor, using regularized ALS. 
        /// </summary>
        /// <param name="T"></param>
        /// <param name="L2RegularisationFactor">The L2 regularization factor, $\lambda$</param>
        /// <param name="maxIterations">The maximum number of iterations the ALS routine is allowed to take.</param>
        /// <param name="absoluteNormTerminationThreshold">
        /// Exit the ALS iterative loop when the absolute scaled norm of | A - uv | / (mn) < threshold
        /// </param>
        /// <param name="deltaNormTerminationThreshold">
        /// Exit the ALS iterative loop when the absolute scaled norm of | A - uv | / mn changes by less
        /// than this amount in between two measures.
        /// </param>
        public static KruskalTensor<double> CanonicalPolyadicDecompose(this DenseTensor<double> T, 
            double L2RegularisationFactor = 0.0,
            int maxIterations = 100,
            double absoluteNormTerminationThreshold = 1e-20,
            double deltaNormTerminationThreshold = 1e-20)
        {
            if (T is null)
            {
                throw new ArgumentNullException(nameof(T));
            }

            int checkLossEvery = 5;

            NativeDoubleProvider blas = new NativeDoubleProvider();
            int order = T.Order;
            int[] dimensions = T.GetDimensions(), 
                orderedDimensions = GetDecreasingOrder(dimensions);

            DenseVector<double>[] u = new DenseVector<double>[order];
            // Cache the L2 norms to avoid re-computation
            double[] vectorL2 = new double[order];
            for (int i = 0; i < order; ++i)
            {
                double[] init = new double[dimensions[i]];
                double x = Math.Pow(1.0 / init.Length, 1.0 / order);
                for (int j = 0; j < init.Length; ++j)
                {
                    init[j] = x;
                }
                u[i] = new DenseVector<double>(init);
                vectorL2[i] = init.Dot(init);
            }

            double lastNormMeasurement = double.MaxValue;

            // Iterate until convergence
            for (int t = 0; t < maxIterations; ++t)
            {
                for (int j = 0; j < order; ++j)
                {
                    int stationaryDimension = orderedDimensions[j];

                    List<int> remainingDimensions = new List<int>(orderedDimensions);

                    DenseTensor<double> U = T;
                    for (int i = 0; i < order; ++i)
                    {
                        int dimension = orderedDimensions[i];
                        if (dimension == stationaryDimension)
                        {
                            continue;
                        }

                        int index = remainingDimensions.IndexOf(dimension);
                        U = U.Multiply(u[dimension], index, blas);
                        remainingDimensions.RemoveAt(index);
                    }

                    // U is now reduced to order 1 
                    double[] num = U.Values;

                    // Calculate the denominator, which is the product over all
                    // sums of squares of L2 norms, over all dimensions except j.
                    double den = 1;
                    for (int i = 0; i < order; ++i)
                    {
                        int dimension = orderedDimensions[i];
                        if (dimension != stationaryDimension)
                        {
                            den *= vectorL2[dimension];
                        }
                    }
                    den += L2RegularisationFactor;

                    // Update the stationary vector
                    double[] vector = u[stationaryDimension].Values;
                    for (int i = 0; i < vector.Length; ++i)
                    {
                        vector[i] = num[i] / den;
                    }
                    // Recalculate the norm of this vector, and cache
                    vectorL2[stationaryDimension] = vector.Dot(vector);
                }

                if (t % checkLossEvery == 0)
                {
                    double norm = CalculateL1Norm(T, dimensions, u);
                    if (norm < absoluteNormTerminationThreshold)
                    {
                        break;
                    }
                    if ((lastNormMeasurement - norm) < deltaNormTerminationThreshold)
                    {
                        break;
                    }
                    lastNormMeasurement = norm;
                }
            }

            return new KruskalTensor<double>(u, DefaultProviders.Double);
        }

        private static double CalculateL1Norm(Tensor<double> T, int[] dimensions, DenseVector<double>[] u)
        {
            double norm = 0.0;

            // Calculate the denominator term
            double den = 1;
            foreach (int dim in dimensions)
            {
                den *= dim;
            }
            
            int order = dimensions.Length;
            int[] index = new int[order];
            while (true)
            {
                double prod = 1;
                for (int i = 0; i < order; ++i)
                {
                    prod *= u[i][index[i]];
                }
                norm += Math.Abs(T.Get(index) - prod);

                index[0]++;
                int j = 0;
                while (j < order && dimensions[j] <= index[j])
                {
                    index[j] = 0;
                    j++;
                    if (j < order)
                    {
                        index[j]++;
                    }
                    else
                    {
                        return norm / den;
                    }
                }
            }
        }
        private static int[] GetDecreasingOrder(int[] dimensions)
        {
            int order = dimensions.Length;

            // Sort the dimension sizes, so that we begin by contracting by the largest dimension
            Tuple<int, int>[] tuples = new Tuple<int, int>[order];
            for (int i = 0; i < order; ++i)
            {
                tuples[i] = new Tuple<int, int>(i, dimensions[i]);
            }
            Array.Sort(tuples, (a, b) => b.Item2.CompareTo(a.Item2));

            int[] orderedDimensions = new int[order];
            for (int i = 0; i < order; ++i)
            {
                orderedDimensions[i] = tuples[i].Item1;
            }
            return orderedDimensions;
        }

        /// <summary>
        /// Calculate the CPD of a tensor T, returning normalized dense vectors u with the property that the p-norm of each vector is 1. 
        /// The tensor will be a product of the scalar factor and each of the vectors u. 
        /// </summary>
        /// <param name="T"></param>
        /// <param name="p"></param>
        public static KruskalTensor<double> NormalizedCanonicalPolyadicDecompose(this DenseTensor<double> T, int p, double L2RegularizationFactor = 0.0)
        {
            if (p < 1)
            {
                throw new ArgumentOutOfRangeException();
            }

            KruskalTensor<double> tensor = CanonicalPolyadicDecompose(T, L2RegularizationFactor);

            NativeDoubleProvider blas = new NativeDoubleProvider();
            double norm = 1.0;
            foreach (DenseVector<double> v in tensor.Values[0])
            {
                double p_norm = v.Values.Norm(p);
                blas.SCAL(v.Values, 1.0 / p_norm, 0, v.Dimension);
                norm *= p_norm;
            }

            tensor.Lambda[0] = norm;
            return tensor;
        }
    }
}
