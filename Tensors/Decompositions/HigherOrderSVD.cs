﻿using LinearNet.Decomposition;
using LinearNet.Providers.LinearAlgebra;
using LinearNet.Structs;
using System.Collections.Generic;

namespace LinearNet.Tensors.Decompositions
{
    public static class HigherOrderSVD
    {

        /// <summary>
        /// For a order-n tensor A, compute the Higher-Order Singular Value Decomposition (HOSVD) 
        /// as A = S x(1) U[0] x(2) U[1] x(3) U[2] ... x(n) U[n - 1]
        /// where 
        /// - S is the core tensor, of the same dimensions and order as A 
        /// - U[i] are orthogonal matrices (unitary in the case of complex tensors) 
        /// - x(i) represents the i-mode product of a tensor and a matrix.
        /// </summary>
        /// <param name="A"></param>
        /// <param name="S"></param>
        /// <param name="U"></param>
        public static void HOSVD(this DenseTensor<double> A, out DenseTensor<double> S, out List<DenseMatrix<double>> U)
        {
            U = new List<DenseMatrix<double>>();
            int order = A.Order, i;
            for (i = 0; i < order; ++i)
            {
                DenseMatrix<double> A_i = A.Unfold(i);
                A_i.SingularValueDecompose(out DenseMatrix<double> U_i, out DenseMatrix<double> S_i, out DenseMatrix<double> V);
                U.Add(U_i);
            }

            NativeDoubleProvider blas = new NativeDoubleProvider();
            for (i = 0; i < order; ++i)
            {
                A = A.Multiply(U[i].Transpose(), i, blas);
            }
            S = A;
        }
    }
}
