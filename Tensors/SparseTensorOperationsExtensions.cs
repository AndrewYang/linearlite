﻿using LinearNet.Providers;
using LinearNet.Structs;
using System;
using System.Collections.Generic;

namespace LinearNet.Tensors
{
    public static class SparseTensorOperationsExtensions
    {
        public static SparseTensor<int> AddElementwise(this SparseTensor<int> S, SparseTensor<int> T) => S.AddElementwise(T, DefaultProviders.Int32);
        public static SparseTensor<long> AddElementwise(this SparseTensor<long> S, SparseTensor<long> T) => S.AddElementwise(T, DefaultProviders.Int64);
        public static SparseTensor<float> AddElementwise(this SparseTensor<float> S, SparseTensor<float> T) => S.AddElementwise(T, DefaultProviders.Float);
        public static SparseTensor<double> AddElementwise(this SparseTensor<double> S, SparseTensor<double> T) => S.AddElementwise(T, DefaultProviders.Double);
        public static SparseTensor<decimal> AddElementwise(this SparseTensor<decimal> S, SparseTensor<decimal> T) => S.AddElementwise(T, DefaultProviders.Decimal);
        public static SparseTensor<G> AddElementwise<G>(this SparseTensor<G> S, SparseTensor<G> T) where G : IAdditiveGroup<G>, new() => S.AddElementwise(T, new AdditiveGroupProvider<G>());

        public static SparseTensor<int> SubtractElementwise(this SparseTensor<int> S, SparseTensor<int> T) => S.SubtractElementwise(T, DefaultProviders.Int32);
        public static SparseTensor<long> SubtractElementwise(this SparseTensor<long> S, SparseTensor<long> T) => S.SubtractElementwise(T, DefaultProviders.Int64);
        public static SparseTensor<float> SubtractElementwise(this SparseTensor<float> S, SparseTensor<float> T) => S.SubtractElementwise(T, DefaultProviders.Float);
        public static SparseTensor<double> SubtractElementwise(this SparseTensor<double> S, SparseTensor<double> T) => S.SubtractElementwise(T, DefaultProviders.Double);
        public static SparseTensor<decimal> SubtractElementwise(this SparseTensor<decimal> S, SparseTensor<decimal> T) => S.SubtractElementwise(T, DefaultProviders.Decimal);
        public static SparseTensor<G> SubtractElementwise<G>(this SparseTensor<G> S, SparseTensor<G> T) where G : IAdditiveGroup<G>, new() => S.SubtractElementwise(T, new AdditiveGroupProvider<G>());

        private static SparseTensor<F> MultiplyElementwise<F>(this SparseTensor<F> S, SparseTensor<F> T, Func<F, F, F> Multiply) where F : new()
        {
            TensorChecks.CheckDimensionsMatch(S, T, out int[] dimensions);

            SparseTensor<F> result = new SparseTensor<F>(dimensions.Copy());
            Dictionary<long, F> v = result.Values, s = S.Values, t = T.Values;
            foreach (KeyValuePair<long, F> pair in s)
            {
                if (t.ContainsKey(pair.Key))
                {
                    v.Add(pair.Key, Multiply(pair.Value, t[pair.Key]));
                }
            }
            return result;
        }
        public static SparseTensor<int> MultiplyElementwise(this SparseTensor<int> S, SparseTensor<int> T) => MultiplyElementwise(S, T, (s, t) => s * t);
        public static SparseTensor<long> MultiplyElementwise(this SparseTensor<long> S, SparseTensor<long> T) => MultiplyElementwise(S, T, (s, t) => s * t);
        public static SparseTensor<float> MultiplyElementwise(this SparseTensor<float> S, SparseTensor<float> T) => MultiplyElementwise(S, T, (s, t) => s * t);
        public static SparseTensor<double> MultiplyElementwise(this SparseTensor<double> S, SparseTensor<double> T) => MultiplyElementwise(S, T, (s, t) => s * t);
        public static SparseTensor<decimal> MultiplyElementwise(this SparseTensor<decimal> S, SparseTensor<decimal> T) => MultiplyElementwise(S, T, (s, t) => s * t);
        public static SparseTensor<G> MultiplyElementwise<G>(this SparseTensor<G> S, SparseTensor<G> T) where G : IRing<G>, new() => MultiplyElementwise(S, T, (s, t) => s.Multiply(t));
    }
}
