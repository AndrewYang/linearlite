﻿using LinearNet.Structs;
using System;

namespace LinearNet.Tensors
{
    public static class TensorChecks
    {
        /// <summary>
        /// Checks if the tensors' dimensions are equal. 
        /// Note that this method only performs shallow checking. Only use for internal methods where 
        /// the tensors are guaranteed to be rectangular (or 'cuboid')
        /// </summary>
        /// <typeparam name="K"></typeparam>
        /// <param name="S"></param>
        /// <param name="T"></param>
        internal static void CheckDimensionsMatch<K>(K[][][] S, K[][][] T)
        {
            if (S.Length != T.Length || S[0].Length != T[0].Length || S[0][0].Length != T[0][0].Length)
            {
                throw new InvalidOperationException(
                    $"Tensor dimensions don't match: {S.Length} x {S[0].Length} x {S[0][0].Length} vs {T.Length} x {T[0].Length} x {T[0][0].Length}");
            }
        }
        internal static void CheckDimensionsMatch<T>(Tensor<T> t1, Tensor<T> t2, out int[] dimensions) where T : new()
        {
            if (t1 == null || t2 == null)
            {
                throw new ArgumentNullException("Tensors cannot be null.");
            }

            int[] dim1 = t1.GetDimensions(), dim2 = t2.GetDimensions();
            if (dim1.Length != dim2.Length)
            {
                throw new InvalidOperationException("Dimensions of tensors don't match.");
            }

            int order = dim1.Length, i;
            for (i = 0; i < order; ++i)
            {
                if (dim1[i] != dim2[i])
                {
                    throw new InvalidOperationException("Dimensions of tensors don't match.");
                }
            }

            dimensions = dim1;
        }
    }
}
