﻿
namespace LinearNet.Tensors
{
    public class TensorInternalExtensions
    {
        internal static T[][][] JTensor<T>(int m, int n, int p)
        {
            T[][][] tensor = new T[m][][];
            for (int i = 0; i < m; ++i)
            {
                tensor[i] = MatrixInternalExtensions.JMatrix<T>(n, p);
            }
            return tensor;
        }
        internal static T[][][][] JTensor<T>(int m, int n, int p, int q)
        {
            T[][][][] tensor = new T[m][][][];
            for (int i = 0; i < m; ++i)
            {
                tensor[i] = JTensor<T>(n, p, q);
            }
            return tensor;
        }
    }
}
