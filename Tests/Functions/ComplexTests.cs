﻿using LinearNet.Structs;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Tests.Functions
{
    public static class ComplexTests
    {
        public static void RunAll()
        {
            ComplexTrigonometryTest();
            ComplexPowerTest();
        }

        private static void ComplexTrigonometryTest()
        {
            // Check that complex trig is consistent with real trigonometry
            Random r = new Random();
            double sin = r.NextDouble() * 2 - 1;
            double angle = Math.Asin(sin);

            Complex complexAngle = Complex.Asin(sin);

            Debug.Assert(complexAngle.ApproximatelyEquals(angle, 1e-8));
        }

        private static void ComplexPowerTest()
        {
            System.Numerics.Complex z = new System.Numerics.Complex(1, 1);
            Complex _z = new Complex(1, 1);
            System.Numerics.Complex w = new System.Numerics.Complex(2, 4);
            Complex _w = new Complex(2, 4);

            System.Numerics.Complex pow = System.Numerics.Complex.Pow(z, w);
            Complex _pow = Complex.Pow(_z, _w);

            Debug.WriteLine(pow);
            Debug.WriteLine(_pow);

            Debug.Assert(Util.ApproximatelyEquals(pow.Real, _pow.Real));
            Debug.Assert(Util.ApproximatelyEquals(pow.Imaginary, _pow.Imaginary));
        }
    }
}
