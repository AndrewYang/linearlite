﻿using LinearNet.Functions;
using LinearNet.Helpers;
using LinearNet.Structs;
using LinearNet.Structs.Fields;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Tests.Functions
{
    public class FunctionsTests
    {
        public static void RunAll()
        {
            bool testAll = false;

            TestFactorial(false || testAll);
            TestCombinations(false || testAll);
            //TestFastLogistic(true || testAll);
            //TestFastLinearInterpolativeLogistic(true);
            TestGammaApproximation(false || testAll);
            TestPartitioningSeries(false || testAll);
            TestErf(false || testAll);
            TestDigammaApproximation(true || testAll);
            TestZetaApproximation(false || testAll);

            BigDecimalDivisionTests(true || testAll);
            BigDecimalSqrtTests(false || testAll);
            BigDecimalNthRootTests(false || testAll);
            BigDecimalExpTests(false || testAll);
            BigDecimalLogTests(false || testAll);
            BigDecimalPowerTests(false || testAll);
            BigDecimalSinTests(false || testAll);
            BigDecimalCosTests(false || testAll);
            BigDecimalPiTests(false || testAll);
            BigDecimalETests(false || testAll);
            BigDecimalErfTests(false || testAll);
            BigDecimalErfcTests(false || testAll);
            BigDecimalAsinTests(false || testAll);
            BigDecimalAtanTests(false || testAll);
            BigDecimalEulerMascheroni(true || testAll);
            BigDecimalGammaTests(true || testAll);
            BigDecimalIncompleteGammaTests(true || testAll);

            BernoulliTests(false || testAll);
            GregoryTests(false || testAll);
            StirlingTests(false || testAll);
            Cauchy2Tests(false || testAll);
            BesselTests(false || testAll);
            IncompleteBetaFunctionTests(false || testAll);
        }
        public static void TestFactorial(bool verbose = false)
        {
            if (verbose)
            {
                for (int i = 0; i < 180; ++i)
                {
                    Debug.WriteLine(MathFunctions.IntFactorial(i) + "," + MathFunctions.Factorial(i) + "\t" + MathFunctions.LogFactorial(i));
                }
            }
        }
        public static void TestCombinations(bool verbose = false)
        {
            if (verbose)
            {
                Debug.WriteLine("C(52, 5):\t" + MathFunctions.Combinations(52, 5));
                Debug.WriteLine("P(10, 4):\t" + MathFunctions.Permutations(10, 4));
            }
            Debug.Assert(Util.ApproximatelyEquals(MathFunctions.Combinations(52, 5), 2598960));
            Debug.Assert(Util.ApproximatelyEquals(MathFunctions.Permutations(10, 4), 5040));
        }
        public static void TestFastFloatLogistic(bool verbose = false)
        {
            int n = 200000000;
            Random r = new Random();
            double[] randomValues = new double[n];
            float[] randomValues1 = new float[n];
            for (int i = 0; i < n; ++i)
            {
                randomValues[i] = r.NextDouble() * 2 - 1;
                randomValues1[i] = (float)randomValues[i];
            }

            // Warm up
            for (int i = 0; i < 10; ++i)
            {
                double x = r.NextDouble() * 2 - 1;
                Debug.WriteLine(MathFunctions.Logistic(x) + "\t" + MathFunctions.FastLogistic((float)x));
            }

            Stopwatch sw = new Stopwatch();
            sw.Start();
            for (int i = 0; i < n; ++i)
            {
                MathFunctions.Logistic(randomValues1[i]);
            }
            sw.Stop();
            Debug.WriteLine("Normal logistic function: " + sw.ElapsedMilliseconds);

            sw.Restart();
            for (int i = 0; i < n; ++i)
            {
                MathFunctions.FastLogistic(randomValues1[i]);
            }
            sw.Stop();
            Debug.WriteLine("Fast logistic function: " + sw.ElapsedMilliseconds);
        }
        public static void TestFastLinearInterpolativeLogistic(bool verbose = false)
        {
            Random r = new Random();
            for (int i = 0; i < 10; ++i)
            {
                double x = MathFunctions.Logit(r.NextDouble());
                Debug.WriteLine(MathFunctions.Logistic(x) + "\t" + MathFunctions.FastLogistic(x));
            }

            int n = 10000;
            double[] randomValues = new double[n];
            double[] savedValues = new double[n];
            double[] savedValues2 = new double[n];
            for (int i = 0; i < n; ++i)
            {
                randomValues[i] = MathFunctions.Logit(r.NextDouble());
            }

            Stopwatch sw = new Stopwatch();
            sw.Start();
            for (int i = 0; i < n; ++i)
            {
                savedValues[i] = MathFunctions.Logistic(randomValues[i]);
            }
            sw.Stop();
            Debug.WriteLine("Normal logistic function: " + sw.ElapsedMilliseconds);

            sw.Restart();
            for (int i = 0; i < n; ++i)
            {
                savedValues2[i] = MathFunctions.FastLogistic(randomValues[i]);
            }
            sw.Stop();
            Debug.WriteLine("Fast logistic function: " + sw.ElapsedMilliseconds);

            // Ensure that all computed values are used to avoid compiler optimizing away the test cases
            for (int j = 0; j < n; ++j)
            {
                Console.WriteLine(savedValues[j] + "\t" + savedValues2[j]);
            }
        }
        public static void TestGammaApproximation(bool verbose = false)
        {
            double factorial8 = MathFunctions.Gamma(9);
            Debug.Assert(Util.ApproximatelyEquals(factorial8, 40320));

            if (verbose)
            {
                for (double x = 0.2; x <= 10.0; x += 0.02)
                {
                    Debug.WriteLine($"Gamma({x}):\tStirling\t{MathFunctions.StirlingsApproximation(x)}\tSpouge:\t{MathFunctions.SpougeApproximation(x)}\tLanczos:\t{MathFunctions.LanczosApproximation(x)}");
                }
            }

            Complex factorial8c = MathFunctions.Gamma(new Complex(9));
            Debug.WriteLine(factorial8c);
        }
        public static void TestPartitioningSeries(bool verbose = false)
        {
            if (verbose)
            {
                for (int i = 1; i < 1000; ++i)
                {
                    Debug.WriteLine(i + "\t" + MathFunctions.PartitionCount(i) + "\t" + MathFunctions.PartitionLongCount(i));
                }
            }
        }
        public static void TestErf(bool verbose = false)
        {
            if (verbose)
            {
                double n = 3;
                for (double x = -n; x <= n; x += 0.2)
                {
                    Debug.WriteLine("Erf:\t" + x + "\t" + MathFunctions.Erf(x) + "\t" + (1.0 - MathFunctions.Erfc(x)));
                }
            }
        }
        public static void TestDigammaApproximation(bool verbose = false)
        {
            if (verbose)
            {
                /*
                for (double x = -10; x < 10; x += 0.01)
                {
                    if (x <= 0 && Math.Abs(x - Math.Round(x)) < 0.01)
                    {
                        continue;
                    }
                    Debug.WriteLine(x.ToString("n8") + "\t" + MathFunctions.Digamma(x));
                }
                */
                Debug.WriteLine(0 + "\t" + MathFunctions.Digamma(1));
            }
        }
        public static void TestZetaApproximation(bool verbose = false)
        {
            if (verbose)
            {
                double[] values = { -1, -0.5, 0.5, 1, 1.5, 2, 3, 4, 1e9 };
                foreach (double x in values)
                {
                    double zeta = MathFunctions.Zeta(x, 100);
                    Debug.WriteLine($"zeta({x}) = " + zeta.ToString("n16"));
                }
            }

            List<Tuple<double, double>> testCases = new List<Tuple<double, double>>()
            {
                new Tuple<double, double>(-1, -1.0 / 12),
                new Tuple<double, double>(0.0, -0.5),
                new Tuple<double, double>(2.0, Math.PI * Math.PI / 6),
                new Tuple<double, double>(4.0, Math.Pow(Math.PI, 4.0) / 90)
            };
            
            foreach (Tuple<double, double> c in testCases)
            {
                double zeta = MathFunctions.Zeta(c.Item1, 100);
                double diff = Math.Abs(zeta - c.Item2);
                Debug.Assert(diff / Math.Abs(c.Item2) < 1e-8, $"zeta({c.Item1}) = " + zeta + " != " + c.Item2 + ", difference = " + diff);
            }
        }

        public static void BigDecimalDivisionTests(bool verbose = false)
        {
            BigDecimal pi_inv = BigDecimal.One / BigDecimal.PI;
            BigDecimal pi_inv_mult_pi = pi_inv * BigDecimal.PI;

            if (verbose)
            {
                Debug.WriteLine("pi^-1:");
                Debug.WriteLine(pi_inv);

                Debug.WriteLine("pi * pi^-1:");
                Debug.WriteLine(pi_inv_mult_pi);
            }

            Debug.Assert(Util.ApproximatelyEquals(new BigDecimal(0.1, 1000), new BigDecimal(2) / new BigDecimal(20), 1e-10));
            Debug.Assert(Util.ApproximatelyEquals(15, new BigDecimal(225) / new BigDecimal(15), 1e-10));
            Debug.Assert(Util.ApproximatelyEquals(pi_inv_mult_pi, 1, 1e-10));
        }
        public static void BigDecimalSqrtTests(bool verbose = false)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            BigDecimal sqrt_pi = BigDecimal.Sqrt(BigDecimal.PI);
            sw.Stop();
            Debug.WriteLine("Sqrt calculated in: " + sw.ElapsedMilliseconds + " ms");

            BigDecimal sqrt_pi_sqrd = sqrt_pi * sqrt_pi;
            if (verbose)
            {
                Debug.WriteLine("sqrt(pi):");
                Debug.WriteLine(sqrt_pi);
                Debug.WriteLine("sqrt(pi)^2:");
                Debug.WriteLine(sqrt_pi_sqrd);
            }

            Debug.Assert(Util.ApproximatelyEquals(sqrt_pi_sqrd, BigDecimal.PI, 1e-10));
        }
        public static void BigDecimalNthRootTests(bool verbose = false)
        {
            int sf = 1000;

            Stopwatch sw = new Stopwatch();
            sw.Start();
            BigDecimal root = BigDecimal.NthRoot(30, 3, sf);
            sw.Stop();

            BigDecimal root_cubed = root * root * root;
            Debug.WriteLine("n-th root calculated in: " + sw.ElapsedMilliseconds + " ms");

            if (verbose)
            {
                Debug.WriteLine("Cube root of 30:");
                Debug.WriteLine(root);
                Debug.WriteLine("Cube root of 30 ^ 3");
                Debug.WriteLine(root_cubed);
            }

            Debug.Assert(Util.ApproximatelyEquals(root_cubed, 30, 1e-10));
        }
        public static void BigDecimalExpTests(bool verbose = false)
        {
            int sf = 1000;
            int power = 15;

            Stopwatch sw = new Stopwatch();
            sw.Restart();
            BigDecimal e = BigDecimal.Exp(power, sf);
            sw.Stop();
            Debug.WriteLine($"Exp({power}) calculated in: " + sw.ElapsedMilliseconds + " ms");

            if (verbose)
            {
                Debug.WriteLine($"e^{power}:");
                Debug.WriteLine(e);
                Debug.WriteLine(Math.Exp(power));
            }
            Debug.Assert(Util.ApproximatelyEquals(e, Math.Exp(power), 1e-5));
        }
        public static void BigDecimalLogTests(bool verbose = false)
        {
            int sf = 300;
            Stopwatch sw = new Stopwatch();
            sw.Restart();
            BigDecimal lne = BigDecimal.Log(BigDecimal.E, sf);
            sw.Stop();
            Debug.WriteLine("Ln(e) calculated in: " + sw.ElapsedMilliseconds + " ms");

            if (verbose)
            {
                Debug.WriteLine(lne);
            }
            Debug.Assert(lne.ApproximatelyEquals(1, new BigDecimal(1, -sf + 1, sf)));

            sw.Restart();
            BigDecimal lne_newton = BigDecimal.LogNewton(BigDecimal.E, sf);
            sw.Stop(); 
            Debug.WriteLine("Ln(e) using newton's method calculated in: " + sw.ElapsedMilliseconds + " ms");

            if (verbose)
            {
                Debug.WriteLine(lne_newton);
            }
            Debug.Assert(lne.ApproximatelyEquals(1, new BigDecimal(1, -sf + 1, sf)));
        }
        public static void BigDecimalPowerTests(bool verbose = false)
        {
            BigDecimal two = new BigDecimal(2);
            BigDecimal two_power_10 = BigDecimal.Pow(two, new BigDecimal(10));

            if (verbose)
            {
                Debug.WriteLine("2^10:\t");
                Debug.WriteLine(two_power_10);
            }
            Debug.Assert(two_power_10.ApproximatelyEquals(1024, new BigDecimal(1, -500, 1000)));
        }
        public static void BigDecimalSinTests(bool verbose = false)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            BigDecimal sin30 = BigDecimal.Sin(-BigDecimal.PI / 6);
            sw.Stop();
            Debug.WriteLine("Sin(30) calculated in " + sw.ElapsedMilliseconds + " ms");

            if (verbose)
            {
                Debug.WriteLine(sin30);
            }
        }
        public static void BigDecimalCosTests(bool verbose = false)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            BigDecimal cos60 = BigDecimal.Cos(BigDecimal.PI / 3);
            sw.Stop();
            Debug.WriteLine("cos(60) calculated in " + sw.ElapsedMilliseconds + " ms");

            if (verbose)
            {
                Debug.WriteLine(cos60);
            }
        }
        public static void BigDecimalPiTests(bool verbose = false)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            BigDecimal pi = BigDecimal.Pi(500);
            sw.Stop();

            Debug.WriteLine("pi to 500 decimal places calculated in " + sw.ElapsedMilliseconds + " ms");
            if (verbose)
            {
                Debug.WriteLine(pi);
            }
        }
        public static void BigDecimalETests(bool verbose = false)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            BigDecimal e = BigDecimal.Euler(500);
            sw.Stop();
            Debug.WriteLine("e to 500 decimal places calculated in " + sw.ElapsedMilliseconds + " ms");

            if (verbose)
            {
                Debug.WriteLine(e);
            }
        }
        public static void BigDecimalErfTests(bool verbose = false)
        {
            // Test against erf(1)
            BigDecimal erf1 = System.Numerics.BigInteger.Parse("8427007929497148693412206350826092592960669979663029084599378978347172540960108412619833253481448884541582615320216943648523390583") /
                                new BigDecimal(System.Numerics.BigInteger.Pow(10, 130));

            Stopwatch sw = new Stopwatch();
            sw.Start();
            BigDecimal erf_1 = MathFunctions.Erf(BigDecimal.One, 1000);
            sw.Stop();
            Debug.WriteLine("erf(1) to 1000 decimal places calculated in " + sw.ElapsedMilliseconds + " ms");

            if (verbose)
            {
                Debug.WriteLine(erf1);
                Debug.WriteLine(erf_1);
            }

            Debug.Assert(erf1.ApproximatelyEquals(erf_1, BigDecimal.Pow(1, -100)));
        }
        public static void BigDecimalErfcTests(bool verbose = false)
        {
            // Test against erfc(1)
            BigDecimal erfc1 = System.Numerics.BigInteger.Parse("1572992070502851306587793649173907407039330020336970915400621021652827459039891587380166746518551115458417384679783056351476609417") /
                                new BigDecimal(System.Numerics.BigInteger.Pow(10, 130));

            Stopwatch sw = new Stopwatch();
            sw.Start();
            BigDecimal erfc_1 = MathFunctions.Erfc(BigDecimal.One, 1000);
            sw.Stop();

            Debug.WriteLine("erfc(1) to 1000 decimal places calculated in " + sw.ElapsedMilliseconds + " ms");

            if (verbose)
            {
                Debug.WriteLine(erfc_1);
            }

            Debug.Assert(erfc1.ApproximatelyEquals(erfc_1, BigDecimal.Pow(1, -100)));
        }
        public static void BigDecimalAsinTests(bool verbose = false)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            BigDecimal asin_half = BigDecimal.Asin(new BigDecimal(0.5, 1000), 1000);
            sw.Stop();
            Debug.WriteLine("asin(0.5) to 2000 decimal places calculated in " + sw.ElapsedMilliseconds + " ms");

            if (verbose)
            {
                Debug.WriteLine(asin_half);
            }
        }
        public static void BigDecimalAtanTests(bool verbose = false)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            BigDecimal atan_half = BigDecimal.Atan(new BigDecimal(1, 1000), 1000);
            sw.Stop();
            Debug.WriteLine("atan(1.0) to 2000 decimal places calculated in " + sw.ElapsedMilliseconds + " ms");

            if (verbose)
            {
                Debug.WriteLine(atan_half);
            }
        }
        public static void BigDecimalEulerMascheroni(bool verbose = false)
        {
            int n = 100;

            Stopwatch sw = new Stopwatch();
            sw.Restart();
            BigDecimal euler = BigDecimal.EulerMascheroniConstant2(n);
            sw.Stop();
            Debug.WriteLine($"Euler–Mascheroni to {n} s.f. calculated in " + sw.ElapsedMilliseconds + " ms");

            if (verbose)
            {
                Debug.WriteLine(euler);
            }
        }
        public static void BigDecimalGammaTests(bool verbose = false)
        {
            BigDecimal g5 = MathFunctions.Gamma(5, 50);
            BigDecimal g8 = MathFunctions.Gamma(8, 50);
            if (verbose)
            {
                Debug.WriteLine("Gamma(5):\t" + g5);
                Debug.WriteLine("Gamma(8):\t" + g8);
            }

            Debug.Assert((int)g5 == 24);
            Debug.Assert((int)g8 == 5040);
        }
        public static void BigDecimalIncompleteGammaTests(bool verbose = false)
        {
            // Test cases 
            Debug.Assert(
                Util.ApproximatelyEquals(
                    BigDecimal.Parse("0.86466471676338730810600050502751559659236845409042"),
                    MathFunctions.IncompleteGamma(1, 2, true, 1000),
                    1e-20));

            BigDecimal g1010 = MathFunctions.IncompleteGamma(10, 10, false, 100);
            if (verbose)
            {
                Debug.WriteLine(g1010);
            }

            Debug.Assert(
                Util.ApproximatelyEquals(
                    BigDecimal.Parse("166173.53478754572935304414332969133302639631942504"),
                    g1010,
                    1e-20));
        }

        public static void BernoulliTests(bool verbose = false)
        {
            Stopwatch sw = new Stopwatch();

            int n = 20;
            Rational[] Bn = new Rational[n];
            MathFunctions.Bernoulli(Bn, n);
            if (verbose)
            {
                for (int i = 0; i < n; ++i)
                {
                    Debug.WriteLine(i + "\t" + Bn[i]);
                }
            }

            int m = 200;
            BigRational[] B_n = new BigRational[m];
            sw.Restart();
            MathFunctions.Bernoulli(B_n, m);
            sw.Stop();
            Debug.WriteLine("First " + m + " BigRational Bernoulli numbers calculated in " + sw.ElapsedMilliseconds + " ms");

            double[] B_n1 = new double[m];
            sw.Restart();
            MathFunctions.Bernoulli(B_n1, m);
            sw.Stop();
            Debug.WriteLine("First " + m + " double Bernoulli numbers calculated in " + sw.ElapsedMilliseconds + " ms");
            
            m /= 2;
            BigRational[] even_Bn = new BigRational[m];
            sw.Restart();
            MathFunctions.EvenBernoulli(even_Bn, 0, m);
            sw.Stop();
            Debug.WriteLine("First " + m + " even Bernoulli numbers calculated in " + sw.ElapsedMilliseconds + " ms");

        }
        public static void GregoryTests(bool verbose = false)
        {
            int m = 100;

            BigRational[] series = new BigRational[m];
            MathFunctions.Gregory(series, m);

            BigDecimal[] series1 = new BigDecimal[m];
            MathFunctions.Gregory(series1, m, 100);

            if (verbose)
            {
                for (int i = 0; i < m; ++i)
                {
                    Debug.WriteLine(i + "\t" + series[i] + "\t" + series1[i]);
                }
            }
        }
        public static void StirlingTests(bool verbose = false)
        {
            /*
            for (int i = 0; i < 10; ++i)
            {
                for (int j = 0; j < 10; ++j)
                {
                    Debug.WriteLine("S(" + i + ", " + j + ")\t" + MathFunctions.BigStirling(i, j) + "\t" + 
                        Math.Exp(MathFunctions.LogStirling(i, j)));
                }
            }*/
            Debug.WriteLine(MathFunctions.BigStirling(9, 3) + "\t" + Math.Exp(MathFunctions.LogStirling(9, 3)));

            Stopwatch sw = new Stopwatch();
            sw.Start();
            MathFunctions.BigStirling(1000, 1000);
            sw.Stop();
            Debug.WriteLine("Stirling(1000, 1000) calculated in " + sw.ElapsedMilliseconds + " ms");
        }
        public static void Cauchy2Tests(bool verbose = false)
        {
            int m = 100;
            Stopwatch sw = new Stopwatch();
            BigRational[] cauchy2 = new BigRational[m];
            sw.Restart();
            MathFunctions.Cauchy2(cauchy2, m);
            sw.Stop();
            Debug.WriteLine($"Calculated {m} Cauchy terms in { sw.ElapsedMilliseconds } ms");
        }
        public static void BesselTests(bool verbose = false)
        {
            if (verbose)
            {
                for (double x = 0; x < 10; x += 0.1)
                {
                    Debug.WriteLine(x + "\t" + MathFunctions.BesselJ(0, x, 100));
                }
            }
        }
        public static void IncompleteBetaFunctionTests(bool verbose = false)
        {
            double p = 2, q = 1;
            Debug.WriteLine($"B({p}, {q}):\t" + MathFunctions.Beta(p, q));

            for (double i = 0; i <= 1.0; i += 0.1)
            {
                Debug.WriteLine(i + "\t" + MathFunctions.IncompleteBeta(i, p, q, 100));
            }
            Debug.WriteLine(1 + "\t" + MathFunctions.IncompleteBeta(1, p, q));
        }
    }
}
