﻿using LinearNet.Providers;
using LinearNet.Structs;
using LinearNet.Structs.Fields;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace LinearNet.Tests
{
    internal static class InstantiationTests
    {
        public static void TestCreateDefaultProvider()
        {
            // Test the provider factory on a non-natively supported type
            ProviderFactory.TryGetDefaultProvider(out IProvider<BigDecimal> provider);
            BigDecimal d1 = new BigDecimal(3), d2 = new BigDecimal(2);
            Debug.Assert(provider.Multiply(d1, d2).Equals(new BigDecimal(6)));

            // Test a complex (nested) type
            ProviderFactory.TryGetDefaultProvider(out IProvider<DensePolynomial<Complex>> provider1);
            DensePolynomial<Complex> p1 = new DensePolynomial<Complex>(1, 2), p2 = new DensePolynomial<Complex>(1, 1, 1);
            Debug.Assert(provider1.Add(p1, p2).ApproximatelyEquals(new DensePolynomial<Complex>(1, 2, 3), Precision.DOUBLE_PRECISION));
        }
    }
}
