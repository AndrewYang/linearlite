﻿using LinearNet.Matrices;
using LinearNet.Matrices.Band;
using LinearNet.Matrices.LU;
using LinearNet.Providers;
using LinearNet.Providers.LinearAlgebra;
using LinearNet.Solvers;
using LinearNet.Solvers.Linear;
using LinearNet.Structs;
using System;
using System.Diagnostics;

namespace LinearNet.Tests
{
    public static class BandMatrixTests
    {
        public static void RunAll(bool verbose = false)
        {
            CreateDOKSparse(verbose);
            CreateCOOSparse(verbose);
            CreateCSRSparse(verbose);
            CreateDiagonal(verbose);
            CreateDense(verbose);
            CreateCSCSparse(verbose);
            CreateSKYSparse(verbose);

            Add(verbose);
            AddInPlace(verbose);
            Subtract(verbose);
            SubtractInPlace(verbose);
            Divide(verbose);
            DivideInPlace(verbose);
            Transpose(verbose);
            Multiply(verbose);
            Multiply2(verbose);
            MultiplyParallel(verbose);
            MultiplyVector(verbose);
            MultiplyScalar(verbose);
            MultiplyScalarInPlace(verbose);
            Row(verbose);
            Column(verbose);
            RowSums(verbose);
            ColumnSums(verbose);
            DirectSum(verbose);
            Vectorize(verbose);
            TriangularSolveBLAS(verbose);
            TriangularSolve(verbose);

            Clear(verbose); 
            ClearRow(verbose);
            ClearColumn(verbose);
            ClearSubmatrix(verbose);

            CholUp(verbose);
            CholUpBLAS(verbose);
            LU(verbose);
            LUSolve(verbose);
            Solve(verbose);
            SolveUnpivoted(verbose);
            SolveTridiagonal(verbose);
            SolveLowerBidiagonal(verbose);
            SolveLowerBidiagonalUnderdetermined(verbose);
        }

        private static bool AssertMatrixEqual<T>(Matrix<T> A, Matrix<T> B, bool verbose) where T : new()
        {
            if (A.Rows != B.Rows)
            {
                Debug.Assert(false, "Matrix rows not equal");
                return false;
            }

            if (A.Columns != B.Columns)
            {
                Debug.Assert(false, "Matrix columns not equal");
                return false;
            }

            for (int i = 0; i < A.Rows; ++i)
            {
                for (int j = 0; j < A.Columns; ++j)
                {
                    if (!A[i, j].Equals(B[i, j]))
                    {
                        Debug.Assert(false, $"Matrix elements @[{i}, {j}] not equal: " + A[i, j] + " vs " + B[i, j]);
                        return false;
                    }
                }
            }

            return true;
        }
        private static bool AssertVectorsEqual<T>(Vector<T> u, Vector<T> v) where T : new()
        {
            if (u.Dimension != v.Dimension)
            {
                Debug.Assert(false, "Vector dimensions not equal");
                return false;
            }

            for (int i = 0; i < u.Dimension; ++i)
            {
                if (!u[i].Equals(v[i]))
                {
                    Debug.Assert(false, $"Vector elements [{i}] not equal: {u[i]} vs {v[i]}");
                    return false;
                }
            }

            return true;
        }
        private static bool AssertVectorsEqual<T>(Vector<T> u, Vector<T> v, Func<T, T, bool> equals) where T : new()
        {
            if (u.Dimension != v.Dimension)
            {
                Debug.Assert(false, "Vector dimensions not equal");
                return false;
            }

            for (int i = 0; i < u.Dimension; ++i)
            {
                if (!equals(u[i], v[i]))
                {
                    Debug.Assert(false, $"Vector elements [{i}] not equal: {u[i]} vs {v[i]}");
                    return false;
                }
            }

            return true;
        }
        private static int Random(int min, int max)
        {
            return new Random().Next(max - min) + min;
        }

        public static void CreateDOKSparse(bool verbose = false)
        {
            DOKSparseMatrix<double> sparse = DOKSparseMatrix.Random<double>(Random(10, 20), Random(10, 20), 0.3);
            BandMatrix<double> band = new BandMatrix<double>(sparse);

            if (AssertMatrixEqual(sparse, band, verbose))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tBandMatrixTests.CreateDOKSparse passed");
                }
            }
        }
        public static void CreateCOOSparse(bool verbose = false)
        {
            COOSparseMatrix<double> sparse = COOSparseMatrix.Random<double>(Random(10, 20), Random(10, 20), 0.3);
            BandMatrix<double> band = new BandMatrix<double>(sparse);
            
            if (AssertMatrixEqual(sparse, band, verbose))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tBandMatrixTests.CreateCOOSparse passed");
                }
            }
        }
        public static void CreateCSRSparse(bool verbose = false)
        {
            CSRSparseMatrix<double> sparse = new CSRSparseMatrix<double>(DOKSparseMatrix.Random<double>(Random(10, 20), Random(10, 20), 0.3));
            BandMatrix<double> band = new BandMatrix<double>(sparse);

            if (AssertMatrixEqual(sparse, band, verbose))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tBandMatrixTests.CreateCSRSparse passed");
                }
            }
        }
        public static void CreateDiagonal(bool verbose = false)
        {
            DiagonalMatrix<double> diag = DiagonalMatrix.Random<double>(Random(10, 20), Random(10, 20));
            BandMatrix<double> band = new BandMatrix<double>(diag);

            if (AssertMatrixEqual(diag, band, verbose))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tBandMatrixTests.CreateDiagonal passed");
                }
            } 
        }
        public static void CreateDense(bool verbose = false)
        {
            DenseMatrix<double> diag = DenseMatrix.Random<double>(Random(10, 20), Random(10, 20));
            BandMatrix<double> band = new BandMatrix<double>(diag);

            if (AssertMatrixEqual(diag, band, verbose))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tBandMatrixTests.CreateDense passed");
                }
            }
        }
        public static void CreateCSCSparse(bool verbose = false)
        {
            CSCSparseMatrix<double> sparse = new CSCSparseMatrix<double>(DOKSparseMatrix.Random<double>(Random(10, 20), Random(10, 20), 0.3));
            BandMatrix<double> band = new BandMatrix<double>(sparse);

            if (AssertMatrixEqual(sparse, band, verbose))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tBandMatrixTests.CreateCSCSparse passed");
                }
            }
        }
        public static void CreateSKYSparse(bool verbose = false)
        {
            int dim = Random(10, 20);
            Random r = new Random();
            SKYSparseMatrix<int> lower = new SKYSparseMatrix<int>(DenseMatrix.RandomTriangular(dim, dim, false, () => r.Next(3) - 1));
            if (AssertMatrixEqual(lower, new BandMatrix<int>(lower), verbose))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tBandMatrixTests.CreateSKYSparse/LOWER passed");
                }
            }
            SKYSparseMatrix<int> upper = new SKYSparseMatrix<int>(DenseMatrix.RandomTriangular(dim, dim, false, () => r.Next(3) - 1));
            if (AssertMatrixEqual(upper, new BandMatrix<int>(upper), verbose))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tBandMatrixTests.CreateSKYSparse/UPPER passed");
                }
            }
            SKYSparseMatrix<double> sym = new SKYSparseMatrix<double>(DenseMatrix.RandomSPD<double>(Random(10, 20)), SKYType.SYMMETRIC);
            if (AssertMatrixEqual(sym, new BandMatrix<double>(sym), verbose))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tBandMatrixTests.CreateSKYSparse/SYMMETRIC passed");
                }
            }
        }

        public static void Add(bool verbose = false)
        {
            BandMatrix<double> A = BandMatrix.Random<double>(10, 10, 2, 2);
            BandMatrix<double> B = BandMatrix.Random<double>(10, 10, 2, 2);

            DenseMatrix<double> _A = A.ToDenseMatrix();
            DenseMatrix<double> _B = B.ToDenseMatrix();

            if (AssertMatrixEqual(_A.Add(_B), A.Add(B), verbose))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tBandMatrixTests.Add passed");
                }
            }
        }
        public static void AddInPlace(bool verbose = false)
        {
            int r = Random(10, 20), c = Random(10, 20);
            BandMatrix<double> A = BandMatrix.Random<double>(r, c, 5, 5);
            BandMatrix<double> B = BandMatrix.Random<double>(r, c, 3, 7);
            DenseMatrix<double> _A = A.ToDenseMatrix(), _B = B.ToDenseMatrix();

            A.AddInPlace(B, BLAS.Double);
            _A.AddInPlace(_B, (IDenseBLAS2<double>)BLAS.Double);

            if (AssertMatrixEqual(A, _A, verbose))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tBandMatrixTests.AddInPlace passed");
                }
            }
        }
        public static void Subtract(bool verbose = false)
        {
            BandMatrix<double> A = BandMatrix.Random<double>(10, 10, 2, 2);
            BandMatrix<double> B = BandMatrix.Random<double>(10, 10, 2, 2);

            DenseMatrix<double> _A = A.ToDenseMatrix();
            DenseMatrix<double> _B = B.ToDenseMatrix();

            if (AssertMatrixEqual(_A.Subtract(_B), A.Subtract(B), verbose))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tBandMatrixTests.Subtract passed");
                }
            }
        }
        public static void SubtractInPlace(bool verbose = false)
        {
            int r = Random(10, 20), c = Random(10, 20);
            BandMatrix<double> A = BandMatrix.Random<double>(r, c, 5, 5);
            BandMatrix<double> B = BandMatrix.Random<double>(r, c, 3, 7);
            DenseMatrix<double> _A = A.ToDenseMatrix(), _B = B.ToDenseMatrix();

            A.SubtractInPlace(B, BLAS.Double);
            _A.SubtractInPlace(_B, (IDenseBLAS2<double>)BLAS.Double);

            if (AssertMatrixEqual(A, _A, verbose))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tBandMatrixTests.SubtractInPlace passed");
                }
            }
        }
        public static void Divide(bool verbose = false)
        {
            BandMatrix<double> sparse = BandMatrix.Random<double>(Random(10, 20), Random(10, 20), 5, 5);
            DenseMatrix<double> dense = new DenseMatrix<double>(sparse);

            double d = Random(1, 10);
            if (AssertMatrixEqual(sparse.Divide(d, BLAS.Double), dense.Divide(d, BLAS.Double), verbose))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tBandMatrixTests.Divide passed");
                }
            }
        }
        public static void DivideInPlace(bool verbose = false)
        {
            BandMatrix<double> sparse = BandMatrix.Random<double>(Random(10, 20), Random(10, 20), 5, 5);
            DenseMatrix<double> dense = new DenseMatrix<double>(sparse);

            double d = Random(1, 10);
            sparse.DivideInPlace(d, BLAS.Double);
            dense.DivideInPlace(d, BLAS.Double);
            if (AssertMatrixEqual(sparse.Divide(d, BLAS.Double), dense.Divide(d, BLAS.Double), verbose))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tBandMatrixTests.DivideInPlace passed");
                }
            }
        }
        public static void Multiply(bool verbose = false)
        {
            BandMatrix<double> A = BandMatrix.Random<double>(10, 10, 3, 2);
            BandMatrix<double> B = BandMatrix.Random<double>(10, 15, 2, 2);

            DenseMatrix<double> _AB = A.ToDenseMatrix().Multiply(B.ToDenseMatrix());
            DenseMatrix<double> AB = A.Multiply(B).ToDenseMatrix();

            if (TestUtil.AssertMatrixEqual(_AB, AB, (u, v) => Util.ApproximatelyEquals(u, v)))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tBandMatrixTests.Multiply passed");
                }
            }
        }
        public static void Multiply2(bool verbose = false)
        {
            int n = Random(10, 20);
            BandMatrix<double> A = BandMatrix.Random<double>(Random(10, 20), n, 3, 5);
            BandMatrix<double> B = BandMatrix.Random<double>(n, Random(10, 20), 2, 4);

            DenseMatrix<double> _A = new DenseMatrix<double>(A);
            DenseMatrix<double> _B = new DenseMatrix<double>(B);

            BandMatrix<double> AB = A.Multiply2(B, BLAS.Double);

            if (TestUtil.AssertMatrixEqual(AB, _A * _B, verbose) && verbose)
            {
                Debug.WriteLine("[success]\tBandMatrixTests.Multiply2 passed");
            }
        }
        public static void MultiplyParallel(bool verbose = false)
        {
            BandMatrix<double> A = BandMatrix.Random<double>(10, 10, 3, 2);
            BandMatrix<double> B = BandMatrix.Random<double>(10, 15, 2, 2);

            BandMatrix<double> pAB = A.MultiplyParallel(B);
            BandMatrix<double> AB = A.Multiply(B);

            if (TestUtil.AssertMatrixEqual(pAB, AB, (u, v) => Util.ApproximatelyEquals(u, v)))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tBandMatrixTests.MultiplyParallel passed");
                }
            } 
            else
            {
                Debug.WriteLine("A");
                A.Print();
                Debug.WriteLine("B");
                B.Print();
                Debug.WriteLine("AB");
                AB.Print();
                Debug.WriteLine("AB");
                pAB.Print();

                Debug.Assert(AB.ApproximatelyEquals(pAB));
            }
        }
        public static void MultiplyVector(bool verbose = false)
        {
            BandMatrix<double> A = BandMatrix.Random<double>(10, 15, 3, 2);
            DenseVector<double> v = DenseVector.Random<double>(15);

            DenseVector<double> Av = A.Multiply(v);
            DenseVector<double> _Av = A.ToDenseMatrix().Multiply(v);

            if (AssertVectorsEqual(Av, _Av))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tBandMatrixTests.MultiplyVector passed");
                }
            }
        }
        public static void MultiplyScalar(bool verbose = false)
        {
            BandMatrix<double> A = BandMatrix.Random<double>(Random(10, 20), Random(10, 20), 5, 5);
            double scalar = Random(-10, 10);

            DenseMatrix<double> _A = A.ToDenseMatrix();

            if (AssertMatrixEqual(_A.Multiply(scalar), A.Multiply(scalar), verbose))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tBandMatrixTests.MultiplyScalar passed");
                }
            }
        }
        public static void MultiplyScalarInPlace(bool verbose = false)
        {
            BandMatrix<double> A = BandMatrix.Random<double>(Random(10, 20), Random(10, 20), 5, 5);
            double scalar = Random(-10, 10);

            DenseMatrix<double> _A = A.ToDenseMatrix();

            // Test using a blas-1 implementation
            _A.MultiplyInPlace(scalar, new NativeDoubleProvider());
            A.MultiplyInPlace(scalar, new NativeDoubleProvider());

            if (AssertMatrixEqual(_A, A, verbose))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tBandMatrixTests.MultiplyScalarInPlace/BLAS1 passed");
                }
            }

            // Test using a provider
            A = BandMatrix.Random<double>(Random(10, 20), Random(10, 20), 5, 5);
            _A = A.ToDenseMatrix();

            A.MultiplyInPlace(scalar, DefaultProviders.Double);
            _A.MultiplyInPlace(scalar, DefaultProviders.Double);
            if (AssertMatrixEqual(_A, A, verbose))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tBandMatrixTests.MultiplyScalarInPlace/Provider passed");
                }
            }
        }
        public static void Transpose(bool verbose = false)
        {
            BandMatrix<double> A = BandMatrix.Random<double>(10, 15, 2, 3);

            DenseMatrix<double> _At = A.ToDenseMatrix().Transpose();
            BandMatrix<double> At = A.Transpose();

            if (AssertMatrixEqual(_At, At, verbose))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tBandMatrixTests.Transpose passed");
                }
            }
        }
        public static void Row(bool verbose = false)
        {
            BandMatrix<double> A = BandMatrix.Random<double>(10, 15, 3, 2);

            int r = Random(0, A.Rows);
            if (AssertVectorsEqual(A.Row(r), A.ToDenseMatrix().Row(r)))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tBandMatrixTests.Row passed");
                }
            }
        }
        public static void Column(bool verbose = false)
        {
            BandMatrix<double> A = BandMatrix.Random<double>(Random(10, 20), Random(10, 20), 5, 5);

            int c = Random(0, A.Columns);
            if (AssertVectorsEqual(A.Column(c), A.ToDenseMatrix().Column(c)))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tBandMatrixTests.Column passed");
                }
            }
        }
        public static void RowSums(bool verbose = false)
        {
            BandMatrix<double> band = BandMatrix.Random<double>(Random(10, 20), Random(10, 20), 5, 5);
            DenseMatrix<double> dense = new DenseMatrix<double>(band);

            DenseVector<double> brs = band.RowSums(DefaultProviders.Double);
            DenseVector<double> drs = dense.RowSums();

            if (AssertVectorsEqual(brs, drs, (a, b) => Util.ApproximatelyEquals(a, b)))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tBandMatrixTests.RowSums passed");
                }
            }
        }
        public static void ColumnSums(bool verbose = false)
        {
            BandMatrix<double> band = BandMatrix.Random<double>(Random(10, 20), Random(10, 20), 5, 5);
            DenseMatrix<double> dense = new DenseMatrix<double>(band);

            DenseVector<double> bs = band.ColumnSums(DefaultProviders.Double);
            DenseVector<double> ds = dense.ColumnSums();

            if (AssertVectorsEqual(bs, ds, (a, b) => Util.ApproximatelyEquals(a, b)))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tBandMatrixTests.ColumnSums passed");
                }
            }
        }
        public static void DirectSum(bool verbose = false)
        {
            BandMatrix<double> A = BandMatrix.Random<double>(20, 10, 4, 5);
            BandMatrix<double> B = BandMatrix.Random<double>(15, 13, 3, 6);

            BandMatrix<double> sum = A.DirectSum(B);
            DenseMatrix<double> _sum = A.ToDenseMatrix().DirectSum(B.ToDenseMatrix());

            if (AssertMatrixEqual(sum, _sum, verbose)) 
            { 
                if (verbose)
                {
                    Debug.WriteLine("[success]\tBandMatrixTests.DirectSum passed");
                }
            }
        }
        public static void Vectorize(bool verbose = false)
        {
            BandMatrix<double> sparse = BandMatrix.Random<double>(Random(10, 20), Random(10, 20), 5, 5);
            DenseMatrix<double> dense = sparse.ToDenseMatrix();

            BigSparseVector<double> rmsv = sparse.Vectorize(true), cmsv = sparse.Vectorize(false);
            DenseVector<double> rmdv = dense.Vectorize(true), cmdv = dense.Vectorize(false);

            if (AssertVectorsEqual(rmsv, rmdv))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tBandMatrixTests.Vectorize(true) passed");
                }
            }
            if (AssertVectorsEqual(cmsv, cmdv))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tBandMatrixTests.Vectorize(false) passed");
                }
            }
        }
        public static void TriangularSolveBLAS(bool verbose = false)
        {
            int dim = Random(10, 10);
            BandMatrix<double> L = BandMatrix.Random<double>(dim, dim, 0, 5);
            BandMatrix<double> U = BandMatrix.Random<double>(dim, dim, 5, 0);
            DenseVector<double> b = DenseVector.Random<double>(dim);
            DenseVector<double> x = DenseVector.Random<double>(dim);

            L.SolveTriangular(b.Values, x.Values, BLAS.Double, true, false);
            if (TestUtil.AssertVectorsEqual(b, L * x, (u, v) => Util.ApproximatelyEquals(u, v)) && verbose)
            {
                Debug.WriteLine("[success]\tBandMatrixTests.TriangularSolveBLAS/lower/untransposed passed");
            }

            U.SolveTriangular(b.Values, x.Values, BLAS.Double, false, false);
            if (TestUtil.AssertVectorsEqual(b, U * x, (u, v) => Util.ApproximatelyEquals(u, v)) && verbose)
            {
                Debug.WriteLine("[success]\tBandMatrixTests.TriangularSolveBLAS/upper/untransposed passed");
            }
        }
        public static void TriangularSolve(bool verbose = false)
        {
            int dim = Random(10, 10);
            BandMatrix<double> L = BandMatrix.Random<double>(dim, dim, 0, 5);
            BandMatrix<double> U = BandMatrix.Random<double>(dim, dim, 5, 0);
            DenseVector<double> b = DenseVector.Random<double>(dim);
            DenseVector<double> x = DenseVector.Random<double>(dim);

            L.SolveTriangular(b.Values, x.Values, DefaultProviders.Double, true, false);
            if (TestUtil.AssertVectorsEqual(b, L * x, (u, v) => Util.ApproximatelyEquals(u, v)) && verbose)
            {
                Debug.WriteLine("[success]\tBandMatrixTests.TriangularSolve/lower/untransposed passed");
            }

            U.SolveTriangular(b.Values, x.Values, DefaultProviders.Double, false, false);
            if (TestUtil.AssertVectorsEqual(b, U * x, (u, v) => Util.ApproximatelyEquals(u, v)) && verbose)
            {
                Debug.WriteLine("[success]\tBandMatrixTests.TriangularSolve/upper/untransposed passed");
            }
        }

        public static void Clear(bool verbose = false)
        {
            BandMatrix<double> sparse = BandMatrix.Random<double>(Random(10, 20), Random(10, 20), 5, 5);
            DenseMatrix<double> dense = sparse.ToDenseMatrix();

            sparse.Clear();
            dense.Clear();

            if (AssertMatrixEqual(sparse, dense, verbose))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tBandMatrixTests.Clear passed");
                }
            }
        }
        public static void ClearRow(bool verbose = false)
        {
            BandMatrix<double> sparse = BandMatrix.Random<double>(Random(10, 20), Random(10, 20), 5, 5);
            DenseMatrix<double> dense = sparse.ToDenseMatrix();

            int r = Random(0, sparse.Rows);
            sparse.ClearRow(r);
            dense.ClearRow(r);

            if (AssertMatrixEqual(sparse, dense, verbose))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tBandMatrixTests.ClearRow passed");
                }
            }
        }
        public static void ClearColumn(bool verbose = false)
        {
            BandMatrix<double> sparse = BandMatrix.Random<double>(Random(10, 20), Random(10, 20), 5, 5);
            DenseMatrix<double> dense = sparse.ToDenseMatrix();

            int c = Random(0, sparse.Columns);
            sparse.ClearColumn(c);
            dense.ClearColumn(c);

            if (AssertMatrixEqual(sparse, dense, verbose))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tBandMatrixTests.ClearColumn passed");
                }
            }
        }
        public static void ClearSubmatrix(bool verbose = false)
        {
            int rows = 12, cols = 14;
            BandMatrix<double> sparse = BandMatrix.Random<double>(rows, cols, 5, 5);
            DenseMatrix<double> dense = sparse.ToDenseMatrix();

            int r = Random(0, 5), c = Random(0, 5);
            sparse.ClearSubmatrix(r, c, 5, 5);
            dense.ClearSubmatrix(r, c, 5, 5);

            if (AssertMatrixEqual(sparse, dense, verbose))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tBandMatrixTests.ClearSubmatrix passed");
                }
            }
        }

        private static void CholUp(bool verbose = false)
        {
            int dim = TestUtil.Random(10, 20);
            Random rand = new Random();
            BandMatrix<double> band = BandMatrix.Random<double>(dim, dim, 3, 3);
            band = band.Transpose() * band;

            BandMatrix<double> L = band.CholeskyFactor(DefaultProviders.Double);
            if (TestUtil.AssertMatrixEqual(L * L.Transpose(), band, (a, b) => Util.ApproximatelyEquals(a, b, 1e-6)) && verbose)
            {
                Debug.WriteLine("[success]\tBandMatrixTests.CholUp passed");
            }
        }
        private static void CholUpBLAS(bool verbose = false)
        {
            int dim = TestUtil.Random(10, 20);
            Random rand = new Random();
            BandMatrix<double> band = BandMatrix.Random<double>(dim, dim, 3, 3);
            band = band.Transpose() * band;

            BandMatrix<double> L = band.CholeskyFactor(BLAS.Double);
            if (TestUtil.AssertMatrixEqual(L * L.Transpose(), band, (a, b) => Util.ApproximatelyEquals(a, b, 1e-6)) && verbose)
            {
                Debug.WriteLine("[success]\tBandMatrixTests.CholUpBLAS passed");
            }
        }
        private static void LU(bool verbose = false)
        {
            int dim = TestUtil.Random(10, 20);
            BandMatrix<double> band = BandMatrix.Random<double>(dim, dim, 3, 4);

            LU<double> lu = band.LU(BLAS.Double, false);
            BandMatrix<double> L = lu.L as BandMatrix<double>,
                U = lu.U as BandMatrix<double>;

            if (TestUtil.AssertMatrixEqual(L * U, band, (u, v) => Util.ApproximatelyEquals(u, v, 1e-6)) && verbose)
            {
                Debug.WriteLine("[success]\tBandMatrixTests.LU passed");
            }
        }
        private static void LUSolve(bool verbose = false)
        {
            int dim = TestUtil.Random(10, 20);
            BandMatrix<double> band = BandMatrix.Random<double>(dim, dim, 4, 3);
            LU<double> lu = band.LU(BLAS.Double, false);

            DenseVector<double> b = DenseVector.Random<double>(dim);
            DenseVector<double> x = new DenseVector<double>(dim);
            lu.Solve(b.Values, x.Values);

            if (TestUtil.AssertVectorsEqual(band * x, b, (u, v) => Util.ApproximatelyEquals(u, v, 1e-6)) && verbose)
            {
                Debug.WriteLine("[success]\tBandMatrixTests.LUSolve passed");
            }
        }
        private static void Solve(bool verbose = false)
        {
            int n = 100;
            BandMatrix<double> A = BandMatrix.Random<double>(n, n, 3, 3);
            DenseVector<double> b = DenseVector.Random<double>(n);
            DenseVector<double> x = new DenseVector<double>(n);

            A.Solve(b.Values, x.Values, BLAS.Double, DefaultProviders.Double, Precision.DOUBLE_PRECISION);

            if (TestUtil.AssertVectorsEqual(A * x, b, (u, v) => Util.ApproximatelyEquals(u, v)) && verbose)
            {
                Debug.WriteLine("[success]\tBandMatrixTests.Solve passed");
            }
        }
        private static void SolveUnpivoted(bool verbose = false)
        {
            int n = 10;
            BandMatrix<double> A = BandMatrix.Random<double>(n, n, 3, 3);
            DenseVector<double> b = DenseVector.Random<double>(n);

            BandSolver<double> bs = new BandSolver<double>(DefaultProviders.Double, BLAS.Double, Precision.DOUBLE_PRECISION, false);
            DenseVector<double> x = bs.Solve(A, b).Result;

            if (TestUtil.AssertVectorsEqual(A * x, b, (u, v) => Util.ApproximatelyEquals(u, v)) && verbose)
            {
                Debug.WriteLine("[success]\tBandMatrixTests.SolveUnpivoted passed");
            }
        }
        private static void SolveTridiagonal(bool verbose = false)
        {
            int n = 10;
            BandMatrix<double> A = BandMatrix.Random<double>(n, n, 1, 1);
            DenseVector<double> b = DenseVector.Random<double>(n);

            BandSolver<double> bs = new BandSolver<double>(DefaultProviders.Double, BLAS.Double, Precision.DOUBLE_PRECISION, false);
            DenseVector<double> x = bs.Solve(A, b).Result;

            if (TestUtil.AssertVectorsEqual(A * x, b, (u, v) => Util.ApproximatelyEquals(u, v)) && verbose)
            {
                Debug.WriteLine("[success]\tBandMatrixTests.SolveTridiagonal passed");
            }
        }
        private static void SolveLowerBidiagonal(bool verbose = false)
        {
            int n = 10;
            BandMatrix<double> A = BandMatrix.Random<double>(n, n, 0, 1);
            DenseVector<double> b = DenseVector.Random<double>(n);

            BandSolver<double> bs = new BandSolver<double>(DefaultProviders.Double, BLAS.Double, Precision.DOUBLE_PRECISION, false);
            DenseVector<double> x = bs.Solve(A, b).Result;

            if (TestUtil.AssertVectorsEqual(A * x, b, (u, v) => Util.ApproximatelyEquals(u, v)) && verbose)
            {
                Debug.WriteLine("[success]\tBandMatrixTests.SolveLowerBidiagonal passed");
            }
        }
        private static void SolveLowerBidiagonalUnderdetermined(bool verbose = false)
        {
            int m = TestUtil.Random(10, 20), n = m + 5;
            BandMatrix<double> A = BandMatrix.Random<double>(m, n, 0, 1);
            DenseVector<double> b = DenseVector.Random<double>(m);

            BandSolver<double> bs = new BandSolver<double>(DefaultProviders.Double, BLAS.Double, Precision.DOUBLE_PRECISION, false);
            Solution<DenseVector<double>> soln = bs.Solve(A, b);
            DenseVector<double> x = soln.Result;

            if (TestUtil.AssertVectorsEqual(A * x, b, (u, v) => Util.ApproximatelyEquals(u, v)) && verbose)
            {
                Debug.WriteLine("[success]\tBandMatrixTests.SolveLowerBidiagonalUnderdetermined passed");
            }
        }
    }
}
