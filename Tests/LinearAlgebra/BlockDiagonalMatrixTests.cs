﻿using LinearNet.Providers.LinearAlgebra;
using LinearNet.Structs;
using System;
using System.Diagnostics;

namespace LinearNet.Tests.LinearAlgebra
{
    public static class BlockDiagonalMatrixTests
    {
        public static void RunAll(bool verbose)
        {
            MultiplyScalar(verbose);
            MultiplyVector(verbose);
            MultiplyVectorTransposed(verbose);
            MultiplySparseVector(verbose);
            MultiplySparseVectorBLAS(verbose);
            MultiplySparseVectorTransposed(verbose);
            MultiplySparseVectorTransposedBLAS(verbose);
        }
        public static void MultiplyScalar(bool verbose = false)
        {
            BlockDiagonalMatrix<double> A = BlockDiagonalMatrix.Random<double>(5, 3, 5);
            DenseMatrix<double> _A = new DenseMatrix<double>(A);

            Random rand = new Random();
            double s = rand.NextDouble();

            if (TestUtil.AssertMatrixEqual(_A * s , A * s, verbose) && verbose)
            {
                Debug.WriteLine("[success]\tBlockDiagonalMatrixTests.MultiplyScalar passed");
            }
        }
        public static void MultiplyVector(bool verbose = false)
        {
            BlockDiagonalMatrix<double> A = BlockDiagonalMatrix.Random<double>(5, 3, 5);
            DenseMatrix<double> _A = new DenseMatrix<double>(A);
            DenseVector<double> v = DenseVector.Random<double>(A.Columns);

            if (TestUtil.AssertVectorsEqual(_A * v, A * v, (u, v) => Util.ApproximatelyEquals(u, v)) && verbose)
            {
                Debug.WriteLine("[success]\tBlockDiagonalMatrixTests.MultiplyVector passed");
            }
        }
        public static void MultiplyVectorTransposed(bool verbose = false)
        {
            BlockDiagonalMatrix<double> A = BlockDiagonalMatrix.Random<double>(5, 3, 5);
            DenseMatrix<double> _A = new DenseMatrix<double>(A);
            DenseVector<double> v = DenseVector.Random<double>(A.Columns);

            DenseVector<double> Atv = A.TransposeAndMultiply(v, DefaultProviders.Double);
            DenseVector<double> _Atv = _A.TransposeAndMultiply(v, DefaultProviders.Double);

            if (TestUtil.AssertVectorsEqual(Atv, _Atv, (u, v) => Util.ApproximatelyEquals(u, v)) && verbose)
            {
                Debug.WriteLine("[success]\tBlockDiagonalMatrixTests.MultiplyVectorTransposed passed");
            }
        }
        public static void MultiplySparseVector(bool verbose = false)
        {
            BlockDiagonalMatrix<double> A = BlockDiagonalMatrix.Random<double>(5, 3, 5);
            DenseMatrix<double> _A = new DenseMatrix<double>(A);
            SparseVector<double> v = SparseVector.Random<double>(A.Columns, 0.3);

            if (TestUtil.AssertVectorsEqual(_A * v, A * v, (u, v) => Util.ApproximatelyEquals(u, v)) && verbose)
            {
                Debug.WriteLine("[success]\tBlockDiagonalMatrixTests.MultiplySparseVector passed");
            }
        }
        public static void MultiplySparseVectorBLAS(bool verbose = false)
        {
            BlockDiagonalMatrix<double> A = BlockDiagonalMatrix.Random<double>(5, 3, 5);
            DenseMatrix<double> _A = new DenseMatrix<double>(A);
            SparseVector<double> v = SparseVector.Random<double>(A.Columns, 0.3);

            SparseVector<double> Atv = A.TransposeAndMultiply(v, BLAS.Double);
            SparseVector<double> _Atv = _A.TransposeAndMultiply(v, DefaultProviders.Double);

            if (TestUtil.AssertVectorsEqual(_A * v, A * v, (u, v) => Util.ApproximatelyEquals(u, v)) && verbose)
            {
                Debug.WriteLine("[success]\tBlockDiagonalMatrixTests.MultiplySparseVectorBLAS passed");
            }
        }
        public static void MultiplySparseVectorTransposed(bool verbose = false)
        {
            BlockDiagonalMatrix<double> A = BlockDiagonalMatrix.Random<double>(5, 3, 5);
            DenseMatrix<double> _A = new DenseMatrix<double>(A);
            SparseVector<double> v = SparseVector.Random<double>(A.Columns, 0.3);

            SparseVector<double> Atv = A.TransposeAndMultiply(v, DefaultProviders.Double);
            SparseVector<double> _Atv = _A.TransposeAndMultiply(v, DefaultProviders.Double);

            if (TestUtil.AssertVectorsEqual(Atv, _Atv, (u, v) => Util.ApproximatelyEquals(u, v)) && verbose)
            {
                Debug.WriteLine("[success]\tBlockDiagonalMatrixTests.MultiplySparseVectorTransposed passed");
            }
        }
        public static void MultiplySparseVectorTransposedBLAS(bool verbose = false)
        {
            BlockDiagonalMatrix<double> A = BlockDiagonalMatrix.Random<double>(5, 3, 5);
            DenseMatrix<double> _A = new DenseMatrix<double>(A);
            SparseVector<double> v = SparseVector.Random<double>(A.Columns, 0.3);

            SparseVector<double> Atv = A.TransposeAndMultiply(v, BLAS.Double);
            SparseVector<double> _Atv = _A.TransposeAndMultiply(v, DefaultProviders.Double);

            if (TestUtil.AssertVectorsEqual(Atv, _Atv, (u, v) => Util.ApproximatelyEquals(u, v)) && verbose)
            {
                Debug.WriteLine("[success]\tBlockDiagonalMatrixTests.MultiplySparseVectorTransposedBLAS passed");
            }
        }
    }
}
