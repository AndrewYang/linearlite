﻿using LinearNet.Matrices;
using LinearNet.Providers.LinearAlgebra;
using LinearNet.Solvers;
using LinearNet.Solvers.Linear;
using LinearNet.Solvers.Preconditioners;
using LinearNet.Structs;
using LinearNet.Vectors;
using System.Diagnostics;

namespace LinearNet.Tests.LinearAlgebra
{
    public static class CGSolverTest
    {
        public static void RunAll(bool verbose = false)
        {
            //Double(verbose);
            JacobiPreconditionedDouble(verbose);
            //SSORPreconditionedDouble(verbose);
        }

        private static void Double(bool verbose)
        {
            RealConjugateGradientSolver<double> solver = new RealConjugateGradientSolver<double>(new NativeDoubleProvider(), 1e-10, 1000);

            // Create a sparse symmetric, positive definite coefficient matrix 
            int dim = 10;
            DOKSparseMatrix<double> L = DOKSparseMatrix.Random<double>(dim, dim, dim * dim);

            CSRSparseMatrix<double> A = new CSRSparseMatrix<double>(L.Transpose() * L);
            DenseVector<double> b = DenseVector.Random<double>(dim);

            Debug.WriteLine("A density: " + A.Density);

            Stopwatch sw = new Stopwatch();
            sw.Start();
            Solution<DenseVector<double>> soln = solver.Solve(A, b);
            sw.Stop();
            Debug.WriteLine(sw.ElapsedMilliseconds + "ms");
        }

        private static void JacobiPreconditionedDouble(bool verbose)
        {
            RealConjugateGradientSolver<double> solver = new RealConjugateGradientSolver<double>(new NativeDoubleProvider(), 1e-20, 1000);

            // Create a sparse symmetric, positive definite coefficient matrix 
            int dim = 100;
            DOKSparseMatrix<double> L = DOKSparseMatrix.Random<double>(dim, dim, (int)(dim * 8));

            CSRSparseMatrix<double> A = new CSRSparseMatrix<double>(L.Transpose() * L);
            DenseVector<double> b = DenseVector.Random<double>(dim);

            Stopwatch sw = new Stopwatch();
            sw.Start();
            Solution<DenseVector<double>> soln = solver.SolveWithPreconditioner(A, b, BLAS.Double);
            sw.Stop();

            var blas = DefaultProviders.Double;
            double norm = A.Multiply(soln.Result, blas).Subtract(b).Norm();
            if (norm < 1e-5)
            {
                Debug.WriteLine($"[success]\tCGSolverTest.JacobiPreconditionedDouble [{dim}x{dim}({A.Density})] passed in {sw.ElapsedMilliseconds} ms with ||Ax - b|| = " + norm);
            }
        }
        private static void SSORPreconditionedDouble(bool verbose)
        {
            RealConjugateGradientSolver<double> solver = new RealConjugateGradientSolver<double>(new NativeDoubleProvider(), 1e-10, 100000);

            // Create a sparse symmetric, positive definite coefficient matrix 
            int dim = 100;
            DOKSparseMatrix<double> L = DOKSparseMatrix.Random<double>(dim, dim, dim * 8);

            CSRSparseMatrix<double> A = new CSRSparseMatrix<double>(L.Transpose() * L);
            DenseVector<double> b = DenseVector.Random<double>(dim);

            SSORPreconditioner ssor = new SSORPreconditioner(A, 1.0);

            Debug.WriteLine("A density: " + A.Density);

            Stopwatch sw = new Stopwatch();
            sw.Start();
            Solution<DenseVector<double>> soln = solver.SolveWithPreconditioner(A, b, BLAS.Double, ssor);
            sw.Stop();
            Debug.WriteLine(sw.ElapsedMilliseconds + "ms");

            // Check solution
            Debug.WriteLine("Exit code: " + soln.ExitCode);

            var blas = DefaultProviders.Double;
            Debug.WriteLine("|Ax - b|_2: " + A.Multiply(soln.Result, blas).Subtract(b).Norm());
        }
    }
}
