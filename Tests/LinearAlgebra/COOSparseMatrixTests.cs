﻿using LinearNet.Matrices;
using LinearNet.Providers;
using LinearNet.Providers.LinearAlgebra;
using LinearNet.Structs;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace LinearNet.Tests.LinearAlgebra
{
    public static class COOSparseMatrixTests
    {
        public static void RunAll(bool verbose = false)
        {
            CastFromDenseMatrix(verbose);
            CastFromSparseMatrix(verbose);
            CastFromBandMatrix(verbose);
            CastFromDiagonalMatrix(verbose);

            Trace(verbose);
            Transpose(verbose);
            RowColumnSums(verbose);
            DirectSum(verbose);
            ClearRowTest(verbose);
            ClearColumnTest(verbose);
            ClearSubmatrixTest(verbose);
            GetRowTest(verbose);
            GetColumnTest(verbose);
            GetSubmatrixTest(verbose);
            AppendMatrixTest(verbose);

            Add(verbose);
            AddInPlace(verbose);
            Subtract(verbose);
            SubtractInPlace(verbose);
            Multiply(verbose);
            MultiplyInPlace(verbose);
            MultiplyVector(verbose);
            MultiplyScalar(verbose); 
            Permute(verbose);
            Vectorize(verbose);
            IsSymmetric(verbose);
        }

        private static bool AssertMatrixEqual<T>(Matrix<T> A, Matrix<T> B, bool verbose) where T : new()
        {
            if (A.Rows != B.Rows)
            {
                Debug.Assert(false, "Matrix rows not equal");
                return false;
            }

            if (A.Columns != B.Columns)
            {
                Debug.Assert(false, "Matrix columns not equal");
                return false;
            }

            for (int i = 0; i < A.Rows; ++i)
            {
                for (int j = 0; j < A.Columns; ++j)
                {
                    if (!A[i, j].Equals(B[i, j]))
                    {
                        Debug.Assert(false, $"Matrix elements @[{i}, {j}] not equal: " + A[i, j] + " vs " + B[i, j]);
                        return false;
                    }
                }
            }

            return true;
        }
        private static bool AssertVectorsEqual<T>(Vector<T> u, Vector<T> v) where T : new()
        {
            if (u.Dimension != v.Dimension)
            {
                Debug.Assert(false, "Vector dimensions not equal");
                return false;
            }

            for (int i = 0; i < u.Dimension; ++i)
            {
                if (!u[i].Equals(v[i]))
                {
                    Debug.Assert(false, $"Vector elements [{i}] not equal: {u[i]} vs {v[i]}");
                    return false;
                }
            }

            return true;
        }

        private static int Random(int min, int max)
        {
            return new Random().Next(max - min) + min;
        }
        public static void CastFromDenseMatrix(bool verbose)
        {
            DenseMatrix<double> dense = DenseMatrix.Random<double>(10, 10);
            COOSparseMatrix<double> sparse = new COOSparseMatrix<double>(dense);
            if (AssertMatrixEqual(sparse, dense, verbose) && verbose)
            {
                Debug.WriteLine("[success]\tCOOSparseMatrixTests.CastFromDenseMatrix passed.");
            }
        }
        public static void CastFromSparseMatrix(bool verbose)
        {
            DOKSparseMatrix<double> sparse = DOKSparseMatrix.Random<double>(10, 10, 30);
            COOSparseMatrix<double> coo = new COOSparseMatrix<double>(sparse);
            if (AssertMatrixEqual(sparse, coo, verbose) && verbose)
            {
                Debug.WriteLine("[success]\tCOOSparseMatrixTests.CastFromSparseMatrix passed.");
            }
        }
        public static void CastFromBandMatrix(bool verbose)
        {
            BandMatrix<double> band = BandMatrix.Random<double>(10, 10, 5, 5);
            COOSparseMatrix<double> coo = new COOSparseMatrix<double>(band);
            if (AssertMatrixEqual(band, coo, verbose) && verbose)
            {
                Debug.WriteLine("[success]\tCOOSparseMatrixTests.CastFromBandMatrix passed.");
            }
        }
        public static void CastFromDiagonalMatrix(bool verbose)
        {
            DiagonalMatrix<double> diag = DiagonalMatrix.Random<double>(10, 10);
            COOSparseMatrix<double> coo = new COOSparseMatrix<double>(diag);
            if (AssertMatrixEqual(diag, coo, verbose) && verbose)
            {
                Debug.WriteLine("[success]\tCOOSparseMatrixTests.CastFromDiagonalMatrix passed.");
            }
        }

        public static void Trace(bool verbose)
        {
            DenseMatrix<double> dense = DenseMatrix.Random<double>(10, 10);
            double tr = dense.Trace();
            double tr1 = new COOSparseMatrix<double>(dense).Trace(new DoubleProvider());

            if (Util.ApproximatelyEquals(tr1, tr))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tCOOSparseMatrixTests.Trace passed.");
                }
            }
            else
            {
                Debug.Assert(false, "COOSparseMatrixTests.Trace failed.");
            }
        }
        public static void Transpose(bool verbose)
        {
            DenseMatrix<double> dense = DenseMatrix.Random<double>(10, 10);
            COOSparseMatrix<double> coo = new COOSparseMatrix<double>(dense);
            if (AssertMatrixEqual(coo.Transpose(), dense.Transpose(), verbose))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tCOOSparseMatrixTests.Transpose passed.");
                }
            }
        }
        public static void RowColumnSums(bool verbose)
        {
            DenseMatrix<double> dense = DenseMatrix.Random<double>(Random(10, 20), Random(10, 20));
            COOSparseMatrix<double> coo = new COOSparseMatrix<double>(dense);

            if (AssertVectorsEqual(dense.RowSums(), coo.RowSums(DefaultProviders.Double)))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tCOOSparseMatrixTests.RowSums passed.");
                }
            }

            if (AssertVectorsEqual(dense.ColumnSums(), coo.ColumnSums(DefaultProviders.Double)))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tCOOSparseMatrixTests.ColumnSums passed.");
                }
            }
        }
        public static void DirectSum(bool verbose)
        {
            DenseMatrix<double> A = DenseMatrix.Random<double>(Random(5, 10), Random(5, 10)),
                                B = DenseMatrix.Random<double>(Random(5, 10), Random(5, 10));

            COOSparseMatrix<double> _A = new COOSparseMatrix<double>(A),
                _B = new COOSparseMatrix<double>(B);

            if (AssertMatrixEqual(A.DirectSum(B), _A.DirectSum(_B), verbose)) 
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tCOOSparseMatrixTests.DirectSum passed.");
                }
            }
        }
        public static void ClearRowTest(bool verbose)
        {
            DenseMatrix<double> dense = DenseMatrix.Random<double>(Random(5, 10), Random(5, 10));
            COOSparseMatrix<double> coo = new COOSparseMatrix<double>(dense);

            int row = Random(0, dense.Rows);
            dense.ClearRow(row);
            coo.ClearRow(row);

            if (AssertMatrixEqual(dense, coo, verbose))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tCOOSparseMatrixTests.ClearRowTest passed.");
                }
            }
        }
        public static void ClearColumnTest(bool verbose)
        {
            DenseMatrix<double> dense = DenseMatrix.Random<double>(Random(5, 10), Random(5, 10));
            COOSparseMatrix<double> coo = new COOSparseMatrix<double>(dense);

            int col = Random(0, dense.Columns);
            dense.ClearColumn(col);
            coo.ClearColumn(col);

            if (AssertMatrixEqual(dense, coo, verbose))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tCOOSparseMatrixTests.ClearColumnTest passed.");
                }
            }
        }
        public static void ClearSubmatrixTest(bool verbose)
        {
            DenseMatrix<double> dense = DenseMatrix.Random<double>(Random(5, 10), Random(5, 10));
            COOSparseMatrix<double> coo = new COOSparseMatrix<double>(dense);

            int rowStart = Random(0, dense.Rows - 2), colStart = Random(0, dense.Columns - 2),
                rows = Random(1, dense.Rows - rowStart), cols = Random(1, dense.Columns - colStart);

            dense.ClearSubmatrix(rowStart, colStart, rows, cols);
            coo.ClearSubmatrix(rowStart, colStart, rows, cols);

            if (AssertMatrixEqual(dense, coo, verbose))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tCOOSparseMatrixTests.ClearSubmatrixTest passed");
                }
            }
        }
        public static void GetRowTest(bool verbose)
        {
            DenseMatrix<double> dense = DenseMatrix.Random<double>(Random(5, 10), Random(5, 10));
            COOSparseMatrix<double> coo = new COOSparseMatrix<double>(dense);

            int row = Random(0, dense.Rows);
            DenseVector<double> denserow = dense.Row(row);
            SparseVector<double> sparserow = coo.SparseRow(row);

            if (AssertVectorsEqual(denserow, sparserow))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tCOOSparseMatrixTests.GetRowTest");
                }
            }
        }
        public static void GetColumnTest(bool verbose)
        {
            DenseMatrix<double> dense = DenseMatrix.Random<double>(Random(5, 10), Random(5, 10));
            COOSparseMatrix<double> coo = new COOSparseMatrix<double>(dense);

            int col = Random(0, dense.Columns);
            DenseVector<double> denserow = dense.Column(col);
            SparseVector<double> sparserow = coo.SparseColumn(col);

            if (AssertVectorsEqual(denserow, sparserow))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tCOOSparseMatrixTests.GetColumnTest");
                }
            }
        }
        public static void GetSubmatrixTest(bool verbose)
        {
            DenseMatrix<double> dense = DenseMatrix.Random<double>(Random(5, 10), Random(5, 10));
            COOSparseMatrix<double> coo = new COOSparseMatrix<double>(dense);

            int rowStart = Random(0, dense.Rows - 2), colStart = Random(0, dense.Columns - 2),
                rows = Random(1, dense.Rows - rowStart), cols = Random(1, dense.Columns - colStart);

            DenseMatrix<double> densesub = dense.Submatrix(rowStart, colStart, rows, cols);
            COOSparseMatrix<double> sparsesub = coo.Submatrix(rowStart, colStart, rows, cols);

            if (AssertMatrixEqual(densesub, sparsesub, verbose))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tCOOSparseMatrixTests.GetSubmatrixTest passed.");
                }
            }
        }
        public static void AppendMatrixTest(bool verbose)
        {
            AppendDirection[] directions = (AppendDirection[])Enum.GetValues(typeof(AppendDirection));

            int n = Random(5, 10);
            DenseMatrix<double> A = DenseMatrix.Random<double>(n, n);
            DenseMatrix<double> B = DenseMatrix.Random<double>(n, n);

            COOSparseMatrix<double> _A = new COOSparseMatrix<double>(A);
            COOSparseMatrix<double> _B = new COOSparseMatrix<double>(B);

            foreach (AppendDirection dir in directions)
            {
                if (AssertMatrixEqual(A.Append(B, dir), _A.Append(_B, dir), verbose))
                {
                    if (verbose)
                    {
                        Debug.WriteLine($"[success]\tCOOSparseMatrixTests.AppendMatrixTest/{dir} passed");
                    }
                }
            }
        }

        public static void Add(bool verbose)
        {
            int r = Random(5, 10), c = Random(5, 10);
            DenseMatrix<double> A = DenseMatrix.Random<double>(r, c),
                                B = DenseMatrix.Random<double>(r, c);

            COOSparseMatrix<double> _A = new COOSparseMatrix<double>(A),
                _B = new COOSparseMatrix<double>(B);

            if (AssertMatrixEqual(_A + _B, A + B, verbose))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tCOOSparseMatrixTests.Add passed");
                }
            }
        }
        public static void AddInPlace(bool verbose)
        {
            int r = Random(5, 10), c = Random(5, 10);
            DenseMatrix<int> A = DenseMatrix.Random<int>(r, c, () => Random(-10, 10)),
                B = DenseMatrix.Random<int>(r, c, () => Random(-10, 10));

            COOSparseMatrix<int> _A = new COOSparseMatrix<int>(A),
                _B = new COOSparseMatrix<int>(B);

            A.AddInPlace(B, (IDenseBLAS1Provider<int>)BLAS.Int32);
            _A.AddInPlace(_B, DefaultProviders.Int32);

            if (AssertMatrixEqual(A, _A, verbose))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tCOOSparseMatrixTests.AddInPlace passed.");
                }
            }
        }
        public static void Subtract(bool verbose)
        {
            int r = Random(5, 10), c = Random(5, 10);
            DenseMatrix<double> A = DenseMatrix.Random<double>(r, c),
                                B = DenseMatrix.Random<double>(r, c);

            COOSparseMatrix<double> _A = new COOSparseMatrix<double>(A),
                _B = new COOSparseMatrix<double>(B);

            if (AssertMatrixEqual(_A - _B, A - B, verbose))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tCOOSparseMatrixTests.Subtract passed");
                }
            }
        }
        public static void SubtractInPlace(bool verbose)
        {
            int r = Random(5, 10), c = Random(5, 10);
            DenseMatrix<int> A = DenseMatrix.Random<int>(r, c, () => Random(-10, 10)),
                B = DenseMatrix.Random<int>(r, c, () => Random(-10, 10));

            COOSparseMatrix<int> _A = new COOSparseMatrix<int>(A),
                _B = new COOSparseMatrix<int>(B);

            A.SubtractInPlace(B, (IDenseBLAS1Provider<int>)BLAS.Int32);
            _A.SubtractInPlace(_B, DefaultProviders.Int32);

            if (AssertMatrixEqual(A, _A, verbose))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tCOOSparseMatrixTests.SubtractInPlace passed.");
                }
            }
        }
        public static void Multiply(bool verbose)
        {
            int m = Random(5, 10), n = Random(5, 10), p = Random(5, 10);
            DenseMatrix<double> A = DenseMatrix.Random<double>(m, n),
                                B = DenseMatrix.Random<double>(n, p);

            COOSparseMatrix<double> _A = new COOSparseMatrix<double>(A),
                _B = new COOSparseMatrix<double>(B);

            if (AssertMatrixEqual(_A * _B, A * B, verbose))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tCOOSparseMatrixTests.Multiply passed");
                }
            }
        }
        public static void MultiplyVector(bool verbose)
        {
            int col = Random(5, 10);
            DenseMatrix<double> dense = DenseMatrix.Random<double>(Random(5, 10), col);
            COOSparseMatrix<double> sparse = new COOSparseMatrix<double>(dense);

            DenseVector<double> densev = DenseVector.Random<double>(col);
            BigSparseVector<double> sparsev = new BigSparseVector<double>(densev);

            if (AssertVectorsEqual(dense * densev, sparse * sparsev))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tCOOSparseMatrixTests.MultiplyVector/SPARSE passed");
                }
            }

            if (AssertVectorsEqual(dense * densev, sparse * densev))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tCOOSparseMatrixTests.MultiplyVector/DENSE passed");
                }
            }
        }
        public static void MultiplyScalar(bool verbose)
        {
            DenseMatrix<double> dense = DenseMatrix.Random<double>(Random(5, 10), Random(5, 10));
            COOSparseMatrix<double> sparse = new COOSparseMatrix<double>(dense);

            double scalar = Random(-10, 10);
            if (AssertMatrixEqual(dense * scalar, sparse * scalar, verbose))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tCOOSparseMatrixTests.MultiplyScalar passed");
                }
            }
        }
        public static void MultiplyInPlace(bool verbose)
        {
            int r = Random(10, 20), c = Random(10, 20);
            DenseMatrix<int> A = DenseMatrix.Random<int>(r, c);
            COOSparseMatrix<int> _A = new COOSparseMatrix<int>(A);

            int scalar = Random(-100, 100);
            A.MultiplyInPlace(scalar, BLAS.Int32);
            _A.MultiplyInPlace(scalar, DefaultProviders.Int32);

            if (AssertMatrixEqual(A, _A, verbose))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tCOOSparseMatrixTests.MultiplyInPlace passed.");
                }
            }
        }

        public static void Permute(bool verbose)
        {
            int cols = Random(10, 15);
            DenseMatrix<double> dense = DenseMatrix.Random<double>(Random(10, 15), cols);
            COOSparseMatrix<double> coo = new COOSparseMatrix<double>(dense);

            PermutationMatrix p = PermutationMatrix.Random(cols);

            dense.MultiplyInPlace(p);
            coo.MultiplyInPlace(p);

            if (AssertMatrixEqual(dense, coo, verbose))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tCOOSparseMatrixTests.Permute passed");
                }
            }
        }
        public static void Vectorize(bool verbose)
        {
            DenseMatrix<double> dense = DenseMatrix.Random<double>(Random(10, 15), Random(10, 15));
            COOSparseMatrix<double> coo = new COOSparseMatrix<double>(dense);

            DenseVector<double> vect = dense.Vectorize();
            BigSparseVector<double> svect = coo.Vectorize();

            if (AssertVectorsEqual(vect, svect))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tCOOSparseMatrixTests.Vectorize passed");
                }
            }
        }
        public static void IsSymmetric(bool verbose)
        {
            int dim = Random(10, 20);

            DenseMatrix<double> L = DenseMatrix.Random<double>(dim, dim);
            DenseMatrix<double> sym = L.Transpose().Multiply(L);

            COOSparseMatrix<double> coo = new COOSparseMatrix<double>(sym);

            bool result = coo.IsSymmetric(EqualityComparer<double>.Default);
            if (!result && verbose)
            {
                Debug.WriteLine("[success]\tCOOSparseMatrixTests.IsSymmetric passed");
            }
        }

    }
}
