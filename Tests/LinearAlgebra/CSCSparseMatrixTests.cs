﻿using LinearNet.Decomposition;
using LinearNet.Graphs;
using LinearNet.Matrices;
using LinearNet.Matrices.Cholesky;
using LinearNet.Matrices.Cholesky.Kernels;
using LinearNet.Matrices.Multiplication;
using LinearNet.Matrices.Sparse;
using LinearNet.Matrices.Sparse.Decompositions;
using LinearNet.Matrices.Sparse.Symbolic;
using LinearNet.Providers.LinearAlgebra;
using LinearNet.Structs;
using System;
using System.Diagnostics;

namespace LinearNet.Tests.LinearAlgebra
{
    public class CSCSparseMatrixTests
    {
        public static void RunAll(bool verbose)
        {
            CreateFromCSRSparse(verbose);
            CreateFromDOKSparse(verbose);
            CreateFromCOOSparse(verbose);
            CreateFromDense(verbose);
            CreateFromDiagonal(verbose);
            CreateFromBand(verbose);

            SetValues(verbose);
            GetColumn(verbose);
            GetBigColumn(verbose);
            GetDenseColumn(verbose);
            GetRow(verbose);
            GetBigRow(verbose);
            GetDenseRow(verbose);
            SetBigColumn(verbose);
            SetColumn(verbose);
            SetDenseColumn(verbose);
            SetBigRow(verbose);
            SetRow(verbose);
            ClearRow(verbose);
            ClearColumn(verbose);
            PermuteRows(verbose);
            PermuteColumns(verbose);
            Transpose(verbose);
            Submatrix(verbose);
            Trace(verbose);
            ReflectLower(verbose);

            MultiplyVector(verbose);
            MultiplySparseVector(verbose);
            TransposeMultiplyVector(verbose);
            MultiplyMatrix(verbose);
            MultiplyMatrixParallel(verbose);
            Add(verbose);
            Subtract(verbose);
            SolveLowerTriangular(verbose);
            SolveTriangularDense(verbose);
            //SolveLowerTriangularSparse(verbose);

            CholeskyUp(verbose);
            CholeskyUp2(verbose);
            CholeskyUpBlock2(verbose);
            CholeskyLeft(verbose);
            CholeskyLeftSuper(verbose);
            EliminationTreeTest(verbose);

            PartitionSortTest(verbose);
        }

        private static void CreateFromCSRSparse(bool verbose)
        {
            CSRSparseMatrix<double> csr = CSRSparseMatrix.Random<double>(TestUtil.Random(10, 20), TestUtil.Random(10, 20), 0.1);
            CSCSparseMatrix<double> csc = new CSCSparseMatrix<double>(csr);

            if (TestUtil.AssertMatrixEqual(csr, csc, verbose))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tCSCSparseMatrixTests.CreateFromCSRSparse passed.");
                }
            }
            else
            {
                Debug.WriteLine("[failure]:\tCSCSparseMatrixTests.CreateFromCSRSparse");
            }
        }
        private static void CreateFromDOKSparse(bool verbose)
        {
            DOKSparseMatrix<double> matrix = DOKSparseMatrix.Random<double>(TestUtil.Random(10, 20), TestUtil.Random(10, 20), 0.1);
            CSCSparseMatrix<double> csc = new CSCSparseMatrix<double>(matrix);

            if (TestUtil.AssertMatrixEqual(matrix, csc, verbose))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tCSCSparseMatrixTests.CreateFromDOKSparse passed.");
                }
            }
            else
            {
                Debug.WriteLine("[failure]:\tCSCSparseMatrixTests.CreateFromDOKSparse");
            }
        }
        private static void CreateFromCOOSparse(bool verbose)
        {
            COOSparseMatrix<double> matrix = COOSparseMatrix.Random<double>(TestUtil.Random(10, 20), TestUtil.Random(10, 20), 0.1);
            CSCSparseMatrix<double> csc = new CSCSparseMatrix<double>(matrix);

            if (TestUtil.AssertMatrixEqual(matrix, csc, verbose))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tCSCSparseMatrixTests.CreateFromCOOSparse passed.");
                }
            }
            else
            {
                Debug.WriteLine("[failure]:\tCSCSparseMatrixTests.CreateFromCOOSparse");
            }
        }
        private static void CreateFromDense(bool verbose)
        {
            DenseMatrix<double> matrix = DenseMatrix.Random<double>(TestUtil.Random(10, 20), TestUtil.Random(10, 20));
            CSCSparseMatrix<double> csc = new CSCSparseMatrix<double>(matrix);

            if (TestUtil.AssertMatrixEqual(matrix, csc, verbose))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tCSCSparseMatrixTests.CreateFromDense passed.");
                }
            }
            else
            {
                Debug.WriteLine("[failure]:\tCSCSparseMatrixTests.CreateFromDense");
            }
        }
        private static void CreateFromDiagonal(bool verbose)
        {
            DiagonalMatrix<double> matrix = DiagonalMatrix.Random<double>(TestUtil.Random(10, 20), TestUtil.Random(10, 20));
            CSCSparseMatrix<double> csc = new CSCSparseMatrix<double>(matrix);

            if (TestUtil.AssertMatrixEqual(matrix, csc, verbose))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tCSCSparseMatrixTests.CreateFromDiagonal passed.");
                }
            }
            else
            {
                Debug.WriteLine("[failure]:\tCSCSparseMatrixTests.CreateFromDiagonal");
            }
        }
        private static void CreateFromBand(bool verbose)
        {
            BandMatrix<double> matrix = BandMatrix.Random<double>(TestUtil.Random(10, 20), TestUtil.Random(10, 20), 5, 5);
            CSCSparseMatrix<double> csc = new CSCSparseMatrix<double>(matrix);

            if (TestUtil.AssertMatrixEqual(matrix, csc, verbose))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tCSCSparseMatrixTests.CreateFromBand passed.");
                }
            }
            else
            {
                Debug.WriteLine("[failure]:\tCSCSparseMatrixTests.CreateFromBand");
            }
        }

        private static void SetValues(bool verbose)
        {
            DenseMatrix<int> dense = new DenseMatrix<int>(TestUtil.Random(10, 20), TestUtil.Random(10, 20));
            CSCSparseMatrix<int> csc = new CSCSparseMatrix<int>(dense);

            Random random = new Random();
            for (int i = 0; i < 1; ++i)
            {
                int r = random.Next(dense.Rows), c = random.Next(dense.Columns), val = random.Next();
                dense[r, c] = val;
                csc[r, c] = val;
            }

            if (TestUtil.AssertMatrixEqual(dense, csc, verbose))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tCSCSparseMatrixTests.SetValues passed.");
                }
            }
        }
        private static void GetColumn(bool verbose)
        {
            DenseMatrix<double> dense = DenseMatrix.Random<double>(TestUtil.Random(10, 20), TestUtil.Random(10, 20));
            CSCSparseMatrix<double> csr = new CSCSparseMatrix<double>(dense);

            int c = TestUtil.Random(0, dense.Columns);
            if (TestUtil.AssertVectorsEqual(dense.Column(c), csr.Column(c)))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tCSCSparseMatrixTests.GetColumn passed");
                }
            }
        }
        private static void GetBigColumn(bool verbose)
        {
            DenseMatrix<double> dense = DenseMatrix.Random<double>(TestUtil.Random(10, 20), TestUtil.Random(10, 20));
            CSCSparseMatrix<double> csr = new CSCSparseMatrix<double>(dense);

            int c = TestUtil.Random(0, dense.Columns);
            if (TestUtil.AssertVectorsEqual(dense.Column(c), csr.BigColumn(c)))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tCSCSparseMatrixTests.GetBigColumn passed");
                }
            }
        }
        private static void GetDenseColumn(bool verbose)
        {
            DenseMatrix<double> dense = DenseMatrix.Random<double>(TestUtil.Random(10, 20), TestUtil.Random(10, 20));
            CSCSparseMatrix<double> csr = new CSCSparseMatrix<double>(dense);

            int c = TestUtil.Random(0, dense.Columns);
            if (TestUtil.AssertVectorsEqual(dense.Column(c), csr.Column(c)))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tCSCSparseMatrixTests.GetDenseColumn passed");
                }
            }
        }
        private static void GetRow(bool verbose)
        {
            DenseMatrix<double> dense = DenseMatrix.Random<double>(TestUtil.Random(10, 20), TestUtil.Random(10, 20));
            CSCSparseMatrix<double> csr = new CSCSparseMatrix<double>(dense);

            int r = TestUtil.Random(0, dense.Rows);
            if (TestUtil.AssertVectorsEqual(dense.Row(r), csr.Row(r)))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tCSCSparseMatrixTests.GetRow passed");
                }
            }
        }
        private static void GetBigRow(bool verbose)
        {
            DenseMatrix<double> dense = DenseMatrix.Random<double>(TestUtil.Random(10, 20), TestUtil.Random(10, 20));
            CSCSparseMatrix<double> csr = new CSCSparseMatrix<double>(dense);

            int r = TestUtil.Random(0, dense.Rows);
            if (TestUtil.AssertVectorsEqual(dense.Row(r), csr.BigRow(r)))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tCSCSparseMatrixTests.GetBigRow passed");
                }
            }
        }
        private static void GetDenseRow(bool verbose)
        {
            DenseMatrix<double> dense = DenseMatrix.Random<double>(TestUtil.Random(10, 20), TestUtil.Random(10, 20));
            CSCSparseMatrix<double> csr = new CSCSparseMatrix<double>(dense);

            int r = TestUtil.Random(0, dense.Rows);
            if (TestUtil.AssertVectorsEqual(dense.Row(r), csr.Row(r)))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tCSCSparseMatrixTests.GetDenseRow passed");
                }
            }
        }
        private static void SetBigColumn(bool verbose)
        {
            DOKSparseMatrix<double> dok = DOKSparseMatrix.Random<double>(TestUtil.Random(10, 20), TestUtil.Random(10, 20), 0.5);
            DenseMatrix<double> dense = new DenseMatrix<double>(dok);
            CSCSparseMatrix<double> csc = new CSCSparseMatrix<double>(dok);
            BigSparseVector<double> vector = BigSparseVector.Random<double>(dense.Rows, 10);

            int c = TestUtil.Random(0, dense.Columns);
            dense.SetColumn(c, new DenseVector<double>(vector));
            csc.SetColumn(c, vector);

            if (TestUtil.AssertMatrixEqual(dense, csc, verbose))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tCSCSparseMatrixTests.SetBigColumn passed");
                }
            }
        }
        private static void SetColumn(bool verbose)
        {
            DOKSparseMatrix<double> dok = DOKSparseMatrix.Random<double>(TestUtil.Random(10, 20), TestUtil.Random(10, 20), 0.5);
            DenseMatrix<double> dense = new DenseMatrix<double>(dok);
            CSCSparseMatrix<double> csc = new CSCSparseMatrix<double>(dok);
            SparseVector<double> vector = SparseVector.Random<double>(dense.Rows, 0.5);

            int c = TestUtil.Random(0, dense.Columns);
            dense.SetColumn(c, new DenseVector<double>(vector));
            csc.SetColumn(c, vector);

            if (TestUtil.AssertMatrixEqual(dense, csc, verbose))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tCSCSparseMatrixTests.SetColumn passed");
                }
            }
        }
        private static void SetDenseColumn(bool verbose)
        {
            CSCSparseMatrix<double> csc = CSCSparseMatrix.Random<double>(TestUtil.Random(10, 20), TestUtil.Random(10, 20), 0.5);
            DenseMatrix<double> dense = new DenseMatrix<double>(csc);
            DenseVector<double> vector = DenseVector.Random<double>(dense.Rows);

            int c = TestUtil.Random(0, dense.Columns);
            dense.SetColumn(c, vector);
            csc.SetColumn(c, vector);

            if (TestUtil.AssertMatrixEqual(dense, csc, verbose) && verbose)
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tCSCSparseMatrixTests.SetDenseColumn passed");
                }
            }
        }
        private static void SetBigRow(bool verbose)
        {
            DOKSparseMatrix<double> dok = DOKSparseMatrix.Random<double>(TestUtil.Random(10, 20), TestUtil.Random(10, 20), 0.5);
            DenseMatrix<double> dense = new DenseMatrix<double>(dok);
            CSCSparseMatrix<double> csc = new CSCSparseMatrix<double>(dok);
            BigSparseVector<double> vector = BigSparseVector.Random<double>(dense.Columns, 10);

            int r = TestUtil.Random(0, dense.Rows);
            dense.SetRow(r, new DenseVector<double>(vector));
            csc.SetRow(r, vector);

            if (TestUtil.AssertMatrixEqual(dense, csc, verbose))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tCSCSparseMatrixTests.SetBigRow passed");
                }
            }
        }
        private static void SetRow(bool verbose)
        {
            DOKSparseMatrix<double> dok = DOKSparseMatrix.Random<double>(TestUtil.Random(10, 20), TestUtil.Random(10, 20), 0.5);
            DenseMatrix<double> dense = new DenseMatrix<double>(dok);
            CSCSparseMatrix<double> csc = new CSCSparseMatrix<double>(dok);
            SparseVector<double> vector = SparseVector.Random<double>(dense.Columns, 0.5);

            int r = TestUtil.Random(0, dense.Rows);
            dense.SetRow(r, new DenseVector<double>(vector));
            csc.SetRow(r, vector);

            if (TestUtil.AssertMatrixEqual(dense, csc, verbose))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tCSCSparseMatrixTests.SetRow passed");
                }
            }
        }
        private static void ClearRow(bool verbose)
        {
            Random rd = new Random();
            int seed = rd.Next(1000);

            Random rand = new Random(seed);
            double density = rand.NextDouble();
            CSCSparseMatrix<double> csc = CSCSparseMatrix.Random(rand.Next(10, 20), rand.Next(10, 20), density, () => rand.NextDouble(), seed: seed);
            DenseMatrix<double> dense = new DenseMatrix<double>(csc);
            DenseMatrix<double> original = new DenseMatrix<double>(dense);

            int r = TestUtil.Random(0, csc.Rows);

            csc.ClearRow(r);
            dense.ClearRow(r);

            if (TestUtil.AssertMatrixEqual(csc, dense, verbose))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tCSCSparseMatrixTests.ClearRow passed");
                }
            }
            else
            {
                Debug.WriteLine("seed: " + seed);
                Debug.WriteLine("original");
                original.Print();
                Debug.WriteLine("CSC structure");
                csc.PrintStructure();
                Debug.WriteLine("Dense");
                dense.Print();
                Debug.WriteLine("CSC");
                csc.Print();
            }
        }
        private static void ClearColumn(bool verbose)
        {
            CSCSparseMatrix<double> csr = CSCSparseMatrix.Random<double>(TestUtil.Random(10, 20), TestUtil.Random(10, 20), 0.5);
            DenseMatrix<double> dense = new DenseMatrix<double>(csr);

            int c = TestUtil.Random(0, csr.Columns);

            csr.ClearColumn(c);
            dense.ClearColumn(c);

            if (TestUtil.AssertMatrixEqual(csr, dense, verbose) && verbose)
            {
                Debug.WriteLine("[success]\tCSCSparseMatrixTests.ClearColumn passed");
            }
        }
        private static void PermuteRows(bool verbose)
        {
            CSCSparseMatrix<double> csc = CSCSparseMatrix.Random<double>(TestUtil.Random(10, 20), TestUtil.Random(10, 20), 0.5);
            DenseMatrix<double> dense = new DenseMatrix<double>(csc);

            PermutationMatrix p = PermutationMatrix.Random(csc.Rows);
            csc.PermuteRows(p);
            dense.PermuteRows(p);

            if (TestUtil.AssertMatrixEqual(dense, csc, verbose))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tCSCSparseMatrixTests.PermuteRows passed");
                }
            }
        }
        private static void PermuteColumns(bool verbose)
        {
            CSCSparseMatrix<double> csc = CSCSparseMatrix.Random<double>(TestUtil.Random(10, 20), TestUtil.Random(10, 20), 0.5);
            DenseMatrix<double> dense = new DenseMatrix<double>(csc);

            PermutationMatrix p = PermutationMatrix.Random(csc.Columns);
            csc.PermuteColumns(p);
            dense.PermuteColumns(p);

            if (TestUtil.AssertMatrixEqual(dense, csc, verbose))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tCSCSparseMatrixTests.PermuteColumns passed");
                }
            }
        }
        private static void Transpose(bool verbose)
        {
            CSCSparseMatrix<double> csc = CSCSparseMatrix.Random<double>(TestUtil.Random(10, 20), TestUtil.Random(10, 20), 0.5);
            DenseMatrix<double> dense = new DenseMatrix<double>(csc);

            if (csc.Transpose() == dense.Transpose())
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tCSCSparseMatrixTests.Transpose passed");
                }
            }
        }
        private static void Submatrix(bool verbose)
        {
            CSCSparseMatrix<double> csc = CSCSparseMatrix.Random<double>(TestUtil.Random(10, 20), TestUtil.Random(10, 20), 0.5);
            DenseMatrix<double> dense = new DenseMatrix<double>(csc);

            int x = TestUtil.Random(0, 5), y = TestUtil.Random(0, 5), rows = 5, cols = 5;

            if (TestUtil.AssertMatrixEqual(csc.Submatrix(x, y, rows, cols), dense.Submatrix(x, y, rows, cols), verbose))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tCSCSparseMatrixTests.Submatrix passed");
                }
            }
        }
        private static void Trace(bool verbose)
        {
            int dim = TestUtil.Random(10, 20);
            CSCSparseMatrix<double> csc = CSCSparseMatrix.Random<double>(dim, dim, 0.5);
            DenseMatrix<double> dense = new DenseMatrix<double>(csc);

            if (csc.Trace(DefaultProviders.Double) != dense.Trace())
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tCSCSparseMatrixTests.Trace passed");
                }
            }
        }
        private static void ReflectLower(bool verbose)
        {
            DenseMatrix<double> dense = DenseMatrix.RandomSPD<double>(10);
            DenseMatrix<double> lower = dense.Lower();

            CSCSparseMatrix<double> csc = new CSCSparseMatrix<double>(lower);
            CSCSparseMatrix<double> reconstructed = csc.ReflectLower();

            if (TestUtil.AssertMatrixEqual(reconstructed, dense, (u, v) => Util.ApproximatelyEquals(u, v)) && verbose)
            {
                Debug.WriteLine("[success]\tCSCSparseMatrixTests.ReflectLower passed");
            }
        }

        private static void MultiplyVector(bool verbose)
        {
            CSCSparseMatrix<double> csc = CSCSparseMatrix.Random<double>(TestUtil.Random(10, 20), TestUtil.Random(10, 20), 0.5);
            DenseMatrix<double> dense = new DenseMatrix<double>(csc);
            DenseVector<double> vector = DenseVector.Random<double>(csc.Columns);

            if (TestUtil.AssertVectorsEqual(csc.Multiply(vector, DefaultProviders.Double), dense.Multiply(vector, BLAS.Double)))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tCSCSparseMatrixTests.MultiplyVector passed");
                }
            }
        }
        private static void MultiplySparseVector(bool verbose)
        {
            CSCSparseMatrix<double> csc = CSCSparseMatrix.Random<double>(TestUtil.Random(10, 20), TestUtil.Random(10, 20), 0.5);
            DenseMatrix<double> dense = new DenseMatrix<double>(csc);
            DenseVector<double> vector = DenseVector.Random<double>(csc.Columns);

            if (TestUtil.AssertVectorsEqual(csc.Multiply(vector, DefaultProviders.Double), dense.Multiply(vector, DefaultProviders.Double)))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tCSCSparseMatrixTests.MultiplySparseVector passed");
                }
            }
        }
        private static void TransposeMultiplyVector(bool verbose)
        {
            CSCSparseMatrix<double> csc = CSCSparseMatrix.Random<double>(TestUtil.Random(10, 20), TestUtil.Random(10, 20), 0.5);
            DenseMatrix<double> dense = new DenseMatrix<double>(csc);
            DenseVector<double> vector = DenseVector.Random<double>(csc.Rows);

            if (TestUtil.AssertVectorsEqual(csc.TransposeAndMultiply(vector, DefaultProviders.Double), dense.TransposeAndMultiply(vector, DefaultProviders.Double)))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tCSCSparseMatrixTests.TransposeMultiplyVector passed");
                }
            }
        }
        private static void MultiplyMatrix(bool verbose)
        {
            int m = TestUtil.Random(10, 20), n = TestUtil.Random(10, 20), p = TestUtil.Random(10, 20);
            CSCSparseMatrix<double> A = CSCSparseMatrix.Random<double>(m, n, 0.5);
            CSCSparseMatrix<double> B = CSCSparseMatrix.Random<double>(n, p, 0.5);

            DenseMatrix<double> _A = (DenseMatrix<double>)A;
            DenseMatrix<double> _B = (DenseMatrix<double>)B;

            if (TestUtil.AssertMatrixEqual(_A * _B, A * B, verbose))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tCSCSparseMatrixTests.MultiplyMatrix passed");
                }
            }
        }
        private static void MultiplyMatrixParallel(bool verbose)
        {
            int m = TestUtil.Random(10, 20), n = TestUtil.Random(10, 20), p = TestUtil.Random(10, 20);
            CSCSparseMatrix<double> A = CSCSparseMatrix.Random<double>(m, n, 0.5);
            CSCSparseMatrix<double> B = CSCSparseMatrix.Random<double>(n, p, 0.5);

            DenseMatrix<double> _A = (DenseMatrix<double>)A;
            DenseMatrix<double> _B = (DenseMatrix<double>)B;

            if (TestUtil.AssertMatrixEqual(_A * _B, A.Multiply(B, BLAS.Double, MultiplicationMode.OPTIMIZE_MEMORY), verbose))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tCSCSparseMatrixTests.MultiplyMatrixParallel passed");
                }
            }
        }
        private static void Add(bool verbose)
        {
            int m = TestUtil.Random(10, 20), n = TestUtil.Random(10, 20);
            CSCSparseMatrix<double> A = CSCSparseMatrix.Random<double>(m, n, 0.5);
            CSCSparseMatrix<double> B = CSCSparseMatrix.Random<double>(m, n, 0.5);

            DenseMatrix<double> C = (DenseMatrix<double>)A,
                                D = (DenseMatrix<double>)B;

            CSCSparseMatrix<double> AB = A + B;
            DenseMatrix<double> CD = C + D;

            if (TestUtil.AssertMatrixEqual(CD, AB, verbose))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tCSCSparseMatrixTests.Add passed");
                }
            }
        }
        private static void Subtract(bool verbose)
        {
            int m = TestUtil.Random(10, 20), n = TestUtil.Random(10, 20);
            CSCSparseMatrix<double> A = CSCSparseMatrix.Random<double>(m, n, 0.5);
            CSCSparseMatrix<double> B = CSCSparseMatrix.Random<double>(m, n, 0.5);

            DenseMatrix<double> C = (DenseMatrix<double>)A,
                                D = (DenseMatrix<double>)B;

            CSCSparseMatrix<double> AB = A - B;
            DenseMatrix<double> CD = C - D;

            if (TestUtil.AssertMatrixEqual(CD, AB, verbose))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tCSCSparseMatrixTests.Subtract passed");
                }
            }
        }
        private static void SolveLowerTriangular(bool verbose)
        {
            int dim = TestUtil.Random(10, 20);
            DenseMatrix<double> L = DenseMatrix.RandomTriangular<double>(dim, dim, false);
            CSCSparseMatrix<double> _L = new CSCSparseMatrix<double>(L);
            SparseVector<double> _b = SparseVector.Random<double>(dim, 1);

            DenseVector<double> _x = new DenseVector<double>(dim);
            _L.SolveTriangular(_b, _x, DefaultProviders.Double);

            DenseVector<double> Lx = _L.Multiply(_x, DefaultProviders.Double);
            if (TestUtil.AssertVectorsEqual(Lx, _b, (a, b) => Util.ApproximatelyEquals(a, b, 1e-3)))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tCSCSparseMatrixTests.SolveLowerTriangular passed");
                }
            }
        }
        private static void SolveTriangularDense(bool verbose)
        {
            int dim = TestUtil.Random(10, 20);
            DenseMatrix<double> dense = DenseMatrix.RandomTriangular<double>(dim, dim, false);
            CSCSparseMatrix<double> L = new CSCSparseMatrix<double>(dense);
            CSCSparseMatrix<double> U = new CSCSparseMatrix<double>(dense.Transpose());
            DenseVector<double> b = DenseVector.Random<double>(dim);

            DenseVector<double> x1 = new DenseVector<double>(dim);
            L.SolveTriangular(b, x1, BLAS.Double, true, false);
            if (TestUtil.AssertVectorsEqual(L * x1, b, (a, b) => Util.ApproximatelyEquals(a, b, 1e-4)) && verbose)
            {
                Debug.WriteLine("[success]\tCSCSparseMatrixTests.SolveTriangularDense/lower passed");
            }

            DenseVector<double> x2 = new DenseVector<double>(dim);
            L.SolveTriangular(b, x2, BLAS.Double, true, true);
            if (TestUtil.AssertVectorsEqual(L.Transpose() * x2, b, (a, b) => Util.ApproximatelyEquals(a, b, 1e-4)) && verbose)
            {
                Debug.WriteLine("[success]\tCSCSparseMatrixTests.SolveTriangularDense/lower/transposed passed");
            }

            DenseVector<double> x3 = new DenseVector<double>(dim);
            U.SolveTriangular(b, x3, BLAS.Double, false, false);
            if (TestUtil.AssertVectorsEqual(U * x3, b, (a, b) => Util.ApproximatelyEquals(a, b, 1e-4)) && verbose)
            {
                Debug.WriteLine("[success]\tCSCSparseMatrixTests.SolveTriangularDense/upper passed");
            }

            DenseVector<double> x4 = new DenseVector<double>(dim);
            U.SolveTriangular(b, x4, BLAS.Double, false, true);
            if (TestUtil.AssertVectorsEqual(U.Transpose() * x4, b, (a, b) => Util.ApproximatelyEquals(a, b, 1e-4)) && verbose)
            {
                Debug.WriteLine("[success]\tCSCSparseMatrixTests.SolveTriangularDense/upper/transposed passed");
            }
        }
        private static void SolveLowerTriangularSparse(bool verbose)
        {
            // This is used to test the Reach function - it passed
            // Construct the sparse matrix
            Random r = new Random();
            COOSparseMatrix<double> L = new COOSparseMatrix<double>(14, 14);
            L[0, 2] = r.NextDouble();
            L[1, 3] = r.NextDouble();
            L[1, 6] = r.NextDouble();
            L[1, 8] = r.NextDouble();
            L[2, 4] = r.NextDouble();
            L[2, 7] = r.NextDouble();
            L[3, 8] = r.NextDouble();
            L[4, 7] = r.NextDouble();
            L[5, 8] = r.NextDouble();
            L[5, 9] = r.NextDouble();
            L[6, 9] = r.NextDouble();
            L[6, 10] = r.NextDouble();
            L[7, 9] = r.NextDouble();
            L[8, 11] = r.NextDouble();
            L[8, 12] = r.NextDouble();
            L[9, 10] = r.NextDouble();
            L[9, 12] = r.NextDouble();
            L[9, 13] = r.NextDouble();
            L[10, 11] = r.NextDouble();
            L[10, 12] = r.NextDouble();
            L[11, 12] = r.NextDouble();
            L[12, 13] = r.NextDouble();

            SparseVector<double> b = new SparseVector<double>(14);
            b[3] = r.NextDouble();
            b[5] = r.NextDouble();

            CSCSparseMatrix<double> _L = new CSCSparseMatrix<double>(L.Transpose());

            _L.Print();

            double[] x = new double[14];
            int[] xi = new int[14];
            int[] pstack = new int[14];

            CSCTriangularSolve<double> solve = new CSCTriangularSolve<double>(DefaultProviders.Double);
            int end = solve.SolveLower(_L, x, xi, pstack, b, 14);

            Debug.WriteLine("x");
            x.Print();
            Debug.WriteLine("xi: " + end);
            xi.Print();


            //_L.SolveTriangular(b, new DenseVector<double>(14), DefaultProviders.Double);
        }

        private static void CholeskyUp(bool verbose)
        {
            int n = 10;
            Random r = new Random();

            // Randomly create a lower-triangular factor L
            COOSparseMatrix<double> L = new COOSparseMatrix<double>(n, n);
            for (int i = 0; i < n; ++i)
            {
                for (int j = 0; j < i; ++j)
                {
                    // 50% sparsity
                    if (r.NextDouble() < 0.5)
                    {
                        L[i, j] = r.NextDouble() - 0.5;
                    }
                }
                // positive diagonal
                L[i, i] = r.NextDouble();
            }

            CSCSparseMatrix<double> A = (CSCSparseMatrix<double>)(L * L.Transpose());
            DenseMatrix<double> Ad = new DenseMatrix<double>(A);

            Ad.CholeskyDecompose(out DenseMatrix<double> Ld);
            CSCCholesky<double> chol = new CSCCholesky<double>();
            CSCSparseMatrix<double> _L = chol.CholUp(A, A.CholeskyAnalysis(), DefaultProviders.Double);

            if (_L.Equals(Ld, (a, b) => Util.ApproximatelyEquals(a, b, 1e-4)))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tCSCSparseMatrixTests.CholeskyUp passed");
                }
            }
            else
            {
                Ld.Print();
                _L.Print();
                _L.Clear(e => Math.Abs(e.Value) < 1e-10);
                _L.Print();
                Debug.Assert(false);
            }
        }
        private static void CholeskyUp2(bool verbose)
        {
            int dim = TestUtil.Random(10, 20);
            CSCSparseMatrix<double> L = CSCSparseMatrix.Random<double>(dim, dim, 0.3);

            // Ensure L has a non-zero diagonal
            Random r = new Random();
            for (int d = 0; d < dim; ++d) L[d, d] = r.NextDouble();

            CSCSparseMatrix<double> A = L.Transpose() * L;

            SymbolicCholesky symChol = A.CholeskyAnalysis();

            CSCCholesky<double> chol = new CSCCholesky<double>();
            CSCSparseMatrix<double> _L = chol.CholUp2(A, A.CholeskyAnalysis(), DefaultProviders.Double, new DoubleCholeskyTriangularSolveKernel());

            DenseMatrix<double> Ad = new DenseMatrix<double>(A);
            Ad.CholeskyDecompose(out DenseMatrix<double> Ld);

            CSCSparseMatrix<double> LL = _L * _L.Transpose();
            if (TestUtil.AssertMatrixEqual(LL, A, (u, v) => Util.ApproximatelyEquals(u, v, 1e-4)) && verbose)
            {
                Debug.WriteLine("[success]\tCSCSparseMatrixTests.CholeskyUp2 passed");
            }
        }
        private static void CholeskyUpBlock2(bool verbose)
        {
            //int dim = TestUtil.Random(10, 20);
            int dim = 16;
            Random r = new Random(0);
            //CSCSparseMatrix<double> L = CSCSparseMatrix.Random<double>(dim, dim, 0.3, () => r.NextDouble() * 2 - 1);
            CSCSparseMatrix<double> L = (CSCSparseMatrix<double>)DenseMatrix.Random<double>(dim, dim, () => r.NextDouble() * 2 - 1);
            // Ensure L has a non-zero diagonal
            for (int d = 0; d < dim; ++d) L[d, d] = r.NextDouble();

            CSCSparseMatrix<double> A = L.Transpose() * L;

            CSCCholesky<double> chol = new CSCCholesky<double>();
            SymbolicCholesky symChol = A.CholeskyAnalysis();

            DenseMatrix<double> Ad = new DenseMatrix<double>(A);
            Ad.CholeskyDecompose(out DenseMatrix<double> Ld);

            //var kernel = new DefaultCSCCholeskyTriangularSolveKernel<double>(DefaultProviders.Double); 
            //var kernel = new DoubleCholeskyTriangularSolveKernel();
            var kernel = new Blas1CSCCholeskyTriangularSolveKernel2<double>(symChol, BLAS.Double);
            CSCSparseMatrix<double> _L = chol.CholUp2(A, symChol, DefaultProviders.Double, kernel);

            CSCSparseMatrix<double> LL = _L * _L.Transpose();
            if (TestUtil.AssertMatrixEqual(LL, A, (u, v) => Util.ApproximatelyEquals(u, v, 1e-4)) && verbose)
            {
                Debug.WriteLine("[success]\tCSCSparseMatrixTests.CholeskyUpBlock2 passed");
            }
        }
        private static void CholeskyLeft(bool verbose)
        {
            int dim = TestUtil.Random(10, 20);
            CSCSparseMatrix<double> L = CSCSparseMatrix.Random<double>(dim, dim, 0.1);

            // Ensure L has a non-zero diagonal
            Random r = new Random();
            for (int d = 0; d < dim; ++d) L[d, d] = r.NextDouble();

            CSCSparseMatrix<double> A = L.Transpose() * L;

            DenseMatrix<double> Ad = new DenseMatrix<double>(A);
            Ad.CholeskyDecompose(out DenseMatrix<double> Ld);
            
            SymbolicCholesky symChol = A.CholeskyAnalysis();

            CSCCholeskyLeft<double> chol = new CSCCholeskyLeft<double>();
            CSCSparseMatrix<double> _L = chol.CholLeft(A, symChol, BLAS.Double);

            CSCSparseMatrix<double> LL = _L * _L.Transpose();
            if (TestUtil.AssertMatrixEqual(LL, A, (u, v) => Util.ApproximatelyEquals(u, v, 1e-4)) && verbose)
            {
                Debug.WriteLine("[success]\tCSCSparseMatrixTests.CholeskyLeft passed");
            }
        }
        private static void CholeskyLeftSuper(bool verbose)
        {
            int seed = 59, dim = 89; //TestUtil.Random(50, 50);
            Random r = new Random(seed);
            //Random r = new Random();
            CSCSparseMatrix<double> L = CSCSparseMatrix.Random(dim, dim, 0.1, () => r.NextDouble() * 2 - 1, seed);

            // Ensure L has a non-zero diagonal
            for (int d = 0; d < dim; ++d) L[d, d] = r.NextDouble();

            CSCSparseMatrix<double> A = L.Transpose() * L;

            DenseMatrix<double> Ad = new DenseMatrix<double>(A);
            Ad.CholeskyDecompose(out DenseMatrix<double> Ld);

            SymbolicCholesky symChol = A.CholeskyAnalysis();

            CSCCholeskyLeftSuper<double> chol = new CSCCholeskyLeftSuper<double>(A, symChol, BLAS.Double);

            CSCSparseMatrix<double> _L = chol.Decompose();
            CSCSparseMatrix<double> LL = _L * _L.Transpose();
            if (TestUtil.AssertMatrixEqual(LL, A, (u, v) => Util.ApproximatelyEquals(u, v, 1e-4)) && verbose)
            {
                Debug.WriteLine("[success]\tCSCSparseMatrixTests.CholeskyLeftSuper passed");
            }
        }
        private static void EliminationTreeTest(bool verbose)
        {
            // Example taken from Davis, T. (2006) "Direct methods for sparse linear systems" pg 39.
            CSCSparseMatrix<double> M = new CSCSparseMatrix<double>(11, 11);

            AddRandomPair(M, 0, 5);
            AddRandomPair(M, 0, 6);
            AddRandomPair(M, 1, 2);
            AddRandomPair(M, 1, 7);
            AddRandomPair(M, 2, 9);
            AddRandomPair(M, 2, 10);
            AddRandomPair(M, 3, 5);
            AddRandomPair(M, 3, 9);
            AddRandomPair(M, 4, 7);
            AddRandomPair(M, 4, 10);
            AddRandomPair(M, 5, 8);
            AddRandomPair(M, 5, 9);
            AddRandomPair(M, 6, 10);
            AddRandomPair(M, 7, 9);
            AddRandomPair(M, 7, 10);
            AddRandomPair(M, 9, 10);

            EliminationTree tree = EliminationTree.Calculate(M);

            int[] parents = { 5, 2, 7, 5, 7, 6, 8, 9, 9, 10, -1 };
            if (TestUtil.AssertVectorsEqual(new DenseVector<int>(tree.Parents), new DenseVector<int>(parents)) && verbose)
            {
                Debug.WriteLine("[success]\tCSCSparseMatrixTests.EliminationTreeTest passed.");
            }

            int[] post = new int[11];
            int[] levels = new int[11];
            tree.CalculatePostOrdering(post, levels);

            int[] firstDesc = new int[11];
            tree.CalculateFirstDescendant(post, firstDesc);

            if (TestUtil.AssertVectorsEqual(firstDesc, new int[] { 4, 0, 0, 5, 2, 4, 4, 0, 4, 0, 0 }) && verbose)
            {
                Debug.WriteLine("[success]\tCSCSparseMatrixTests.FirstDescendantTest passed.");
            }

            CSCSymbolic<double> chol = new CSCSymbolic<double>();
            chol.AnalyseCholesky(M);
        }
        private static void AddRandomPair(CSCSparseMatrix<double> M, int i, int j)
        {
            double v = new Random().NextDouble();
            M[i, j] = v;
            M[j, i] = v;
        }

        private static void PartitionSortTest(bool verbose)
        {
            int n = 5;
            int[] partitionSortedArray = new int[n * n];
            int[] workspace = new int[n * n];
            int[] partitions = new int[n + 1];
            for (int i = 0; i < partitionSortedArray.Length; ++i)
            {
                partitionSortedArray[i] = i;
            }
            for (int i = 0; i <= n; ++i)
            {
                partitions[i] = i * n;
            }
            Random r = new Random(0);
            partitionSortedArray.Shuffle(r);

            // Sort within each of n partitions
            for (int i = 0; i < n; ++i)
            {
                Array.Sort(partitionSortedArray, i * n, n);
            }

            int[] copy = partitionSortedArray.Copy();
            CSCSparseMatrix<int>.PartitionSort(partitionSortedArray, workspace, partitions, n);

            Array.Sort(copy, 0, copy.Length);

            if (TestUtil.AssertVectorsEqual(copy, partitionSortedArray) && verbose)
            {
                Debug.WriteLine("[success]\tCSCSparseMatrixTests.PartitionSortTest passed.");
            }
        }
    }
} 
