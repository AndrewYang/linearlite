﻿using LinearNet.Matrices;
using LinearNet.Providers.LinearAlgebra;
using LinearNet.Structs;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace LinearNet.Tests.LinearAlgebra
{
    public static class CSRSparseMatrixTests
    {
        public static void RunAll(bool verbose = false)
        {
            CastFromDenseMatrix(verbose);
            CastFromDOKSparseMatrix(verbose);
            CastFromCOOSparseMatrix(verbose);
            CastFromDiagonalMatrix(verbose);
            CastFromCSCSparseMatrix(verbose);
            CastFromMCSRSparseMatrix(verbose);
            CastFromSKYSparseMatrixLower(verbose);
            CastFromSKYSparseMatrixUpper(verbose);
            CastFromSKYSparseMatrixSymmetric(verbose);
            CastFromBlockDiagonalMatrix(verbose);
            CreateFromSparseVector();

            SetValues(verbose);

            Add(verbose);
            AddBLAS(verbose);
            Negate(verbose);
            Subtract(verbose);
            MultiplyMatrix(verbose);
            MultiplyBigSparseVector(verbose);
            MultiplySparseVector(verbose);
            MultiplyTransposeSparseVector(verbose);
            MultiplyScalar(verbose);
            MultiplyScalarBlas1(verbose);

            Transpose(verbose);
            GetBigRow(verbose);
            GetRow(verbose);
            GetBigColumn(verbose);
            GetColumn(verbose);
            GetSubmatrix(verbose);
            SetColumn(verbose);
            SetColumnDense(verbose);
            ClearColumn(verbose);
            ClearSubmatrix(verbose);
            ClearDiagonal(verbose);
            PermuteRows(verbose);
            PermuteColumns(verbose);
            SwapRows(verbose);
            SwapColumns(verbose);
            IsSymmetric(verbose);
            Rank1Update(verbose);
            InsertRow(verbose);
            InsertColumn(verbose);
            DirectSum(verbose);
            StructuralSymmetric(verbose);

            Cholesky(verbose);
            SolveTriangular(verbose);
        }

        private static bool AssertMatrixEqual<T>(Matrix<T> A, Matrix<T> B, bool verbose) where T : new()
        {
            if (A.Rows != B.Rows)
            {
                Debug.Assert(false, "Matrix rows not equal");
                return false;
            }

            if (A.Columns != B.Columns)
            {
                Debug.Assert(false, "Matrix columns not equal");
                return false;
            }

            for (int i = 0; i < A.Rows; ++i)
            {
                for (int j = 0; j < A.Columns; ++j)
                {
                    if (!A[i, j].Equals(B[i, j]))
                    {
                        Debug.Assert(false, $"Matrix elements @[{i}, {j}] not equal: " + A[i, j] + " vs " + B[i, j]);
                        return false;
                    }
                }
            }

            return true;
        }
        private static bool AssertVectorsEqual<T>(Vector<T> u, Vector<T> v) where T : new()
        {
            if (u.Dimension != v.Dimension)
            {
                Debug.Assert(false, "Vector dimensions not equal");
                return false;
            }

            for (int i = 0; i < u.Dimension; ++i)
            {
                if (!u[i].Equals(v[i]))
                {
                    Debug.Assert(false, $"Vector elements [{i}] not equal: {u[i]} vs {v[i]}");
                    return false;
                }
            }

            return true;
        }
        private static int Random(int min, int max)
        {
            return new Random().Next(max - min) + min;
        }

        public static void CastFromDenseMatrix(bool verbose)
        {
            Random rand = new Random();
            DenseMatrix<double> dense = DenseMatrix.Random<double>(rand.Next(10, 20), rand.Next(10, 20));
            CSRSparseMatrix<double> sparse = new CSRSparseMatrix<double>(dense);
            if (AssertMatrixEqual(sparse, dense, verbose) && verbose)
            {
                Debug.WriteLine("[success]\tCSRSparseMatrixTests.CastFromDenseMatrix passed.");
            }
        }
        public static void CastFromDOKSparseMatrix(bool verbose)
        {
            Random rand = new Random();
            DOKSparseMatrix<double> sparse = DOKSparseMatrix.Random<double>(rand.Next(10, 20), rand.Next(10, 20), 0.5);
            CSRSparseMatrix<double> coo = new CSRSparseMatrix<double>(sparse);
            if (AssertMatrixEqual(sparse, coo, verbose) && verbose)
            {
                Debug.WriteLine("[success]\tCSRSparseMatrixTests.CastFromSparseMatrix passed.");
            }
        }
        public static void CastFromCOOSparseMatrix(bool verbose)
        {
            Random rand = new Random();
            COOSparseMatrix<double> coo = COOSparseMatrix.Random<double>(rand.Next(10, 20), rand.Next(10, 20), 0.5);
            CSRSparseMatrix<double> csr = new CSRSparseMatrix<double>(coo);

            if (AssertMatrixEqual(coo, csr, verbose) && verbose)
            {
                Debug.WriteLine("[success]\tCSRSparseMatrixTests.CastFromCOOSparseMatrix passed.");
            }
        }
        public static void CastFromDiagonalMatrix(bool verbose)
        {
            DiagonalMatrix<double> diag = DiagonalMatrix.Random<double>(100, 100);
            CSRSparseMatrix<double> coo = new CSRSparseMatrix<double>(diag);
            if (AssertMatrixEqual(diag, coo, verbose) && verbose)
            {
                Debug.WriteLine("[success]\tCSRSparseMatrixTests.CastFromDiagonalMatrix passed.");
            }
        }
        public static void CastFromCSCSparseMatrix(bool verbose)
        {
            CSCSparseMatrix<double> csc = CSCSparseMatrix.Random<double>(Random(10, 20), Random(10, 20), 0.5);
            CSRSparseMatrix<double> csr = (CSRSparseMatrix<double>)csc;
            if (AssertMatrixEqual(csc, csr, verbose) && verbose)
            {
                Debug.WriteLine("[success]\tCSRSparseMatrixTests.CastFromCSCSparseMatrix passed.");
            }
        }
        public static void CastFromMCSRSparseMatrix(bool verbose)
        {
            MCSRSparseMatrix<double> mcsr = MCSRSparseMatrix.Random<double>(Random(10, 20), 0.9, 0.5);
            CSRSparseMatrix<double> csr = (CSRSparseMatrix<double>)mcsr;
            if (AssertMatrixEqual(mcsr, csr, verbose) && verbose)
            {
                Debug.WriteLine("[success]\tCSRSparseMatrixTests.CastFromMCSRSparseMatrix passed.");
            }
        }
        public static void CastFromSKYSparseMatrixLower(bool verbose)
        {
            int dim = TestUtil.Random(10, 20);
            DenseMatrix<double> dense = DenseMatrix.RandomTriangular<double>(dim, dim, false);
            SKYSparseMatrix<double> L = new SKYSparseMatrix<double>(dense, SKYType.LOWER);
            CSRSparseMatrix<double> csr = new CSRSparseMatrix<double>(L);

            if (TestUtil.AssertMatrixEqual(L, csr, verbose) && verbose)
            {
                Debug.WriteLine("[success]\tCSRSparseMatrixTests.CastFromSKYSparseMatrixLower passed");
            }
        }
        public static void CastFromSKYSparseMatrixUpper(bool verbose)
        {
            int dim = TestUtil.Random(10, 20);
            DenseMatrix<double> dense = DenseMatrix.RandomTriangular<double>(dim, dim, true);
            SKYSparseMatrix<double> U = new SKYSparseMatrix<double>(dense, SKYType.UPPER);
            CSRSparseMatrix<double> csr = new CSRSparseMatrix<double>(U);

            if (TestUtil.AssertMatrixEqual(U, csr, verbose) && verbose)
            {
                Debug.WriteLine("[success]\tCSRSparseMatrixTests.CastFromSKYSparseMatrixUpper passed");
            }
        }
        public static void CastFromSKYSparseMatrixSymmetric(bool verbose)
        {
            int dim = TestUtil.Random(10, 20);
            DenseMatrix<double> dense = DenseMatrix.RandomSPD<double>(dim);
            SKYSparseMatrix<double> sym = new SKYSparseMatrix<double>(dense, SKYType.SYMMETRIC);
            CSRSparseMatrix<double> csr = new CSRSparseMatrix<double>(sym);

            if (TestUtil.AssertMatrixEqual(sym, csr, verbose) && verbose)
            {
                Debug.WriteLine("[success]\tCSRSparseMatrixTests.CastFromSKYSparseMatrixSymmetric passed");
            }
        }
        public static void CastFromBlockDiagonalMatrix(bool verbose)
        {
            Random r = new Random();
            BlockDiagonalMatrix<int> bd = BlockDiagonalMatrix.Random(5, 3, 5, () => r.Next(3) - 1);
            CSRSparseMatrix<int> csr = new CSRSparseMatrix<int>(bd, true);

            if (TestUtil.AssertMatrixEqual(bd, csr, verbose) && verbose)
            {
                Debug.WriteLine("[success]\tCSRSparseMatrixTests.CastFromBlockDiagonalMatrix passed");
            }
        }
        public static void CreateFromSparseVector()
        {
            Random rand = new Random();
            int min = 10, max = 20;
            int rows = rand.Next(min, max), cols = rand.Next(10, 20);
            
            SparseVector<double> vect = SparseVector.Random<double>(rows * cols, 0.5);
            CSRSparseMatrix<double> matrix = new CSRSparseMatrix<double>(rows, cols, vect, true);

            for (int r = 0; r < rows; ++r)
            {
                for (int c = 0; c < cols; ++c)
                {
                    Debug.Assert(Util.ApproximatelyEquals(matrix[r, c], vect[r * cols + c]));
                }
            }

            CSRSparseMatrix<double> matrix2 = new CSRSparseMatrix<double>(rows, cols, vect, false);
            for (int r = 0; r < rows; ++r)
            {
                for (int c = 0; c < cols; ++c)
                {
                    Debug.Assert(Util.ApproximatelyEquals(matrix2[r, c], vect[r + c * rows]));
                }
            }
            Debug.WriteLine("[success]\tCSRSparseMatrixTests.CreateFromSparseVector passed");
        }

        public static void SetValues(bool verbose)
        {
            DenseMatrix<int> dense = DenseMatrix.Random<int>(Random(10, 20), Random(10, 20));
            CSRSparseMatrix<int> csr = new CSRSparseMatrix<int>(dense);

            Random random = new Random();
            for (int i = 0; i < 100; ++i)
            {
                int r = random.Next(dense.Rows), c = random.Next(dense.Columns), val = random.Next();
                dense[r, c] = val;
                csr[r, c] = val;
            }

            if (AssertMatrixEqual(dense, csr, verbose))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tCSRSparseMatrixTests.SetValues passed.");
                }
            }
        }
        public static void Add(bool verbose)
        {
            int r = Random(10, 20), c = Random(10, 20);
            DenseMatrix<double> A = DenseMatrix.Random<double>(r, c),
                B = DenseMatrix.Random<double>(r, c);

            CSRSparseMatrix<double> _A = new CSRSparseMatrix<double>(A),
                _B = new CSRSparseMatrix<double>(B);

            if (AssertMatrixEqual(A + B, _A + _B, verbose))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tCSRSparseMatrixTests.Add passed.");
                }
            }
        }
        public static void AddBLAS(bool verbose)
        {
            int r = Random(10, 20), c = Random(10, 20);
            DenseMatrix<double> A = DenseMatrix.Random<double>(r, c),
                B = DenseMatrix.Random<double>(r, c);

            CSRSparseMatrix<double> _A = new CSRSparseMatrix<double>(A),
                _B = new CSRSparseMatrix<double>(B);

            if (AssertMatrixEqual(A + B, _A.Add(_B, new NativeDoubleProvider()), verbose))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tCSRSparseMatrixTests.AddBLAS passed.");
                }
            }
        }
        public static void Negate(bool verbose)
        {
            Random rand = new Random();
            int gen()
            {
                return rand.Next(3) - 1;
            }
            DenseMatrix<int> A = DenseMatrix.Random(Random(10, 20), Random(10, 20), gen);
            CSRSparseMatrix<int> _A = new CSRSparseMatrix<int>(A);

            if (AssertMatrixEqual(-A, _A.Negate(DefaultProviders.Int32), verbose))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tCSRSparseMatrixTests.Negate passed.");
                }
            }
        }
        public static void Subtract(bool verbose)
        {
            int r = Random(10, 20), c = Random(10, 20); 

            Random rand = new Random();
            Func<int> generator = () => rand.Next(5) - 2;

            DenseMatrix<int> A = DenseMatrix.Random(r, c, generator), B = DenseMatrix.Random(r, c, generator);
            CSRSparseMatrix<int> _A = new CSRSparseMatrix<int>(A), _B = new CSRSparseMatrix<int>(B);

            if (AssertMatrixEqual(A - B, _A - _B, verbose))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tCSRSparseMatrixTests.Subtract passed.");
                }
            }
        }
        public static void MultiplyMatrix(bool verbose)
        {
            int m = Random(10, 20), n = Random(10, 20), p = Random(10, 20);

            Random rand = new Random();
            int gen() => rand.Next(3) - 1;

            DenseMatrix<int> A = DenseMatrix.Random(m, n, gen), B = DenseMatrix.Random(n, p, gen);
            CSRSparseMatrix<int> _A = new CSRSparseMatrix<int>(A), _B = new CSRSparseMatrix<int>(B);

            if (AssertMatrixEqual(A * B, _A * _B, verbose))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tCSRSparseMatrixTests.MultiplyMatrix passed");
                }
            }
        }
        public static void MultiplyBigSparseVector(bool verbose)
        {
            int m = Random(10, 20), n = Random(10, 20);

            Random rand = new Random();
            Func<int> gen = () => rand.Next(3) - 1;

            DenseMatrix<int> A = DenseMatrix.Random(m, n, gen);
            CSRSparseMatrix<int> _A = new CSRSparseMatrix<int>(A);

            DenseVector<int> x = DenseVector.Random(n, gen);
            BigSparseVector<int> _x = new BigSparseVector<int>(x);

            if (AssertVectorsEqual(A * x, _A * _x))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tCSRSparseMatrixTests.MultiplyBigSparseVector passed");
                }
            }
        }
        public static void MultiplySparseVector(bool verbose)
        {
            int m = Random(10, 20), n = Random(10, 20);

            Random rand = new Random();
            Func<int> gen = () => rand.Next(3) - 1;

            DenseMatrix<int> A = DenseMatrix.Random(m, n, gen);
            CSRSparseMatrix<int> _A = new CSRSparseMatrix<int>(A);

            DenseVector<int> x = DenseVector.Random(n, gen);
            SparseVector<int> _x = new SparseVector<int>(x);

            if (AssertVectorsEqual(A * x, _A * _x))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tCSRSparseMatrixTests.MultiplySparseVector passed");
                }
            }
        }
        public static void MultiplyTransposeSparseVector(bool verbose)
        {
            int m = Random(10, 20), n = Random(10, 20);

            Random rand = new Random();
            Func<int> gen = () => rand.Next(3) - 1;

            DenseMatrix<int> A = DenseMatrix.Random(m, n, gen);
            CSRSparseMatrix<int> _A = new CSRSparseMatrix<int>(A);

            DenseVector<int> x = DenseVector.Random(m, gen);
            SparseVector<int> _x = new SparseVector<int>(x);

            if (AssertVectorsEqual(A.Transpose() * x, _A.TransposeAndMultiply(_x, DefaultProviders.Int32)))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tCSRSparseMatrixTests.MultiplyTransposeSparseVector passed");
                }
            }
        }
        public static void MultiplyScalar(bool verbose)
        {
            int m = Random(10, 20), n = Random(10, 20);

            Random rand = new Random();
            int gen()
            {
                return rand.Next(3) - 1;
            }

            DenseMatrix<int> A = DenseMatrix.Random(m, n, gen);
            CSRSparseMatrix<int> _A = new CSRSparseMatrix<int>(A);

            int scalar = gen();
            if (AssertMatrixEqual(A * scalar, _A * scalar, verbose))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tCSRSparseMatrixTests.MultiplyScalar passed");
                }
            }
        }
        public static void MultiplyScalarBlas1(bool verbose)
        {
            int m = Random(10, 20), n = Random(10, 20);

            Random rand = new Random();
            int gen()
            {
                return rand.Next(3) - 1;
            }

            DenseMatrix<int> A = DenseMatrix.Random(m, n, gen);
            CSRSparseMatrix<int> _A = new CSRSparseMatrix<int>(A);

            int scalar = gen();
            if (AssertMatrixEqual(A * scalar, _A.Multiply(scalar, new NativeInt32Provider()), verbose))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tCSRSparseMatrixTests.MultiplyScalarBlas1 passed");
                }
            }
        }

        private static void Transpose(bool verbose)
        {
            Random rand = new Random();
            DenseMatrix<int> dense = DenseMatrix.Random(Random(10, 20), Random(10, 20), () => rand.Next(3) - 1);
            CSRSparseMatrix<int> sparse = new CSRSparseMatrix<int>(dense);

            DenseMatrix<int> dtranspose = dense.Transpose();
            CSRSparseMatrix<int> stranspose = sparse.Transpose();

            if (AssertMatrixEqual(dtranspose, stranspose, verbose))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tCSRSparseMatrixTests.Transpose passed");
                }
            }
        }
        private static void GetBigRow(bool verbose)
        {
            DenseMatrix<double> dense = DenseMatrix.Random<double>(Random(10, 20), Random(10, 20));
            CSRSparseMatrix<double> csr = new CSRSparseMatrix<double>(dense);

            int row = Random(0, dense.Rows);
            if (AssertVectorsEqual(dense.Row(row), csr.BigRow(row)))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tCSRSparseMatrixTests.GetBigRow passed");
                }
            }
        }
        private static void GetRow(bool verbose)
        {
            DenseMatrix<double> dense = DenseMatrix.Random<double>(Random(10, 20), Random(10, 20));
            CSRSparseMatrix<double> csr = new CSRSparseMatrix<double>(dense);

            int row = Random(0, dense.Rows);
            if (AssertVectorsEqual(dense.Row(row), csr.Row(row)))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tCSRSparseMatrixTests.GetRow passed");
                }
            }
        }
        private static void GetBigColumn(bool verbose)
        {
            DenseMatrix<double> dense = DenseMatrix.Random<double>(Random(10, 20), Random(10, 20));
            CSRSparseMatrix<double> csr = new CSRSparseMatrix<double>(dense);

            int column = Random(0, dense.Columns);
            if (AssertVectorsEqual(dense.Column(column), csr.BigColumn(column)))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tCSRSparseMatrixTests.GetBigColumn passed");
                }
            }
        }
        private static void GetColumn(bool verbose)
        {
            DenseMatrix<double> dense = DenseMatrix.Random<double>(Random(10, 20), Random(10, 20));
            CSRSparseMatrix<double> csr = new CSRSparseMatrix<double>(dense);

            int c = Random(0, dense.Columns);
            if (AssertVectorsEqual(dense.Column(c), csr.Column(c)))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tCSRSparseMatrixTests.GetColumn passed");
                }
            }
        }
        private static void GetSubmatrix(bool verbose)
        {
            DenseMatrix<double> dense = DenseMatrix.Random<double>(Random(10, 20), Random(10, 20));
            CSRSparseMatrix<double> csr = new CSRSparseMatrix<double>(dense);

            int r = Random(0, 5), c = Random(0, 5), rows = Random(2, dense.Rows - r), columns = Random(2, dense.Columns - c);
            DenseMatrix<double> densesub = dense.Submatrix(r, c, rows, columns);
            CSRSparseMatrix<double> csrsub = csr.Submatrix(r, c, rows, columns);

            if (AssertMatrixEqual(densesub, csrsub, verbose))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tCSRSparseMatrixTests.GetSubmatrix passed");
                }
            }
        }
        private static void SetColumn(bool verbose)
        {
            CSRSparseMatrix<double> csr = CSRSparseMatrix.Random<double>(Random(5, 10), Random(5, 10), 0.7);
            DenseMatrix<double> dense = new DenseMatrix<double>(csr);
            BigSparseVector<double> column = BigSparseVector.Random<double>(dense.Rows, 10);

            int c = Random(0, dense.Columns);
            dense.SetColumn(c, new DenseVector<double>(column));
            csr.SetColumn(c, column);

            if (AssertMatrixEqual(dense, csr, verbose))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tCSRSparseMatrixTests.SetColumn passed");
                }
            }
        }
        private static void SetColumnDense(bool verbose)
        {
            CSRSparseMatrix<double> csr = CSRSparseMatrix.Random<double>(Random(5, 10), Random(5, 10), 0.7);
            DenseMatrix<double> dense = new DenseMatrix<double>(csr);
            DenseVector<double> column = DenseVector.Random<double>(dense.Rows);

            int c = Random(0, dense.Columns);
            dense.SetColumn(c, column);
            csr.SetColumn(c, column);

            if (AssertMatrixEqual(dense, csr, verbose))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tCSRSparseMatrixTests.SetColumnDense passed");
                }
            }
        }
        private static void ClearColumn(bool verbose)
        {
            Random rand = new Random();
            DenseMatrix<int> dense = DenseMatrix.Random(Random(10, 20), Random(10, 20), () => rand.Next(3) - 1);
            CSRSparseMatrix<int> csr = new CSRSparseMatrix<int>(dense);

            int column = Random(0, dense.Columns);
            dense.ClearColumn(column);
            csr.ClearColumn(column);

            if (AssertMatrixEqual(dense, csr, verbose))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tCSRSparseMatrixTests.ClearColumn passed");
                }
            }
        }
        private static void ClearSubmatrix(bool verbose)
        {
            Random rand = new Random();
            DenseMatrix<int> dense = DenseMatrix.Random(Random(10, 20), Random(10, 20), () => rand.Next(3) - 1);
            CSRSparseMatrix<int> csr = new CSRSparseMatrix<int>(dense);

            int r = rand.Next(5), c = rand.Next(5);
            dense.ClearSubmatrix(r, c, 5, 5);
            csr.ClearSubmatrix(r, c, 5, 5, true);

            if (AssertMatrixEqual(dense, csr, verbose) && verbose)
            {
                Debug.WriteLine("[success]\tCSRSparseMatrixTests.ClearSubmatrix passed");
            }
        }
        private static void ClearDiagonal(bool verbose)
        {
            Random rand = new Random();
            DenseMatrix<int> dense = DenseMatrix.Random(Random(10, 20), Random(10, 20), () => rand.Next(3) - 1);
            CSRSparseMatrix<int> csr = new CSRSparseMatrix<int>(dense);

            int d = Random(-dense.Rows + 1, dense.Columns - 1);
            dense.SetDiagonal(0, d);
            csr.ClearDiagonal(d);

            if (AssertMatrixEqual(dense, csr, verbose) && verbose)
            {
                Debug.WriteLine("[success]\tCSRSparseMatrixTests.ClearDiagonal passed");
            }
        }
        private static void PermuteRows(bool verbose)
        {
            Random rand = new Random();
            int gen() => rand.Next(3) - 1;

            DenseMatrix<int> dense = DenseMatrix.Random(Random(10, 20), Random(10, 20), gen);
            CSRSparseMatrix<int> csr = new CSRSparseMatrix<int>(dense);

            PermutationMatrix permute = PermutationMatrix.Random(dense.Rows);
            dense.PermuteRows(permute);
            csr.PermuteRows(permute);

            if (AssertMatrixEqual(dense, csr, verbose))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tCSRSparseMatrixTests.PermuteRows passed");
                }
            }
        }
        private static void PermuteColumns(bool verbose)
        {
            Random rand = new Random();
            int gen() => rand.Next(3) - 1;

            DenseMatrix<int> dense = DenseMatrix.Random(Random(10, 20), Random(10, 20), gen);
            CSRSparseMatrix<int> csr = new CSRSparseMatrix<int>(dense);

            PermutationMatrix permute = PermutationMatrix.Random(dense.Columns);
            dense.PermuteColumns(permute);
            csr.PermuteColumns(permute);

            if (AssertMatrixEqual(dense, csr, verbose))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tCSRSparseMatrixTests.PermuteColumns passed");
                }
            }
        }
        private static void SwapRows(bool verbose)
        {
            DenseMatrix<double> dense = DenseMatrix.Random<double>(Random(10, 20), Random(10, 20));
            CSRSparseMatrix<double> csr = new CSRSparseMatrix<double>(dense);

            // It is possible for r1 = r2, however we'll leave it up to chance.
            int r1 = Random(0, dense.Rows), r2 = Random(0, dense.Rows);

            dense.SwapRows(r1, r2);
            csr.SwapRows(r1, r2);

            if (AssertMatrixEqual(dense, csr, verbose))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tCSRSparseMatrixTests.SwapRows passed");
                }
            }
        }
        private static void SwapColumns(bool verbose)
        {
            DenseMatrix<double> dense = DenseMatrix.Random<double>(Random(10, 20), Random(10, 20));
            CSRSparseMatrix<double> csr = new CSRSparseMatrix<double>(dense);

            int c1 = Random(0, dense.Columns), c2 = Random(0, dense.Columns);

            dense.SwapColumns(c1, c2);
            csr.SwapColumns(c1, c2);

            if (AssertMatrixEqual(dense, csr, verbose))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tCSRSparseMatrixTests.SwapColumns passed");
                }
            }
        }
        private static void IsSymmetric(bool verbose)
        {
            int dim = Random(10, 20);

            DenseMatrix<double> L = DenseMatrix.Random<double>(dim, dim);
            DenseMatrix<double> sym = L.Transpose().Multiply(L);

            CSRSparseMatrix<double> csr = new CSRSparseMatrix<double>(sym);

            bool result = csr.IsSymmetric(EqualityComparer<double>.Default);
            if (!result && verbose)
            {
                Debug.WriteLine("[success]\tCOOSparseMatrixTests.IsSymmetric passed");
            }
        }
        private static void Rank1Update(bool verbose)
        {
            CSRSparseMatrix<double> csr = CSRSparseMatrix.Random<double>(Random(10, 20), Random(10, 20), 0.3);
            DenseMatrix<double> dense = new DenseMatrix<double>(csr);

            SparseVector<double> u = SparseVector.Random<double>(csr.Rows, 0.5);
            SparseVector<double> v = SparseVector.Random<double>(csr.Columns, 0.5);

            Random r = new Random();
            double beta = r.NextDouble();
            csr.Rank1Update(beta, u, v, DefaultProviders.Double);
            dense.Rank1Update(beta, u.ToDenseVector(), v.ToDenseVector(), BLAS.Double);

            if (AssertMatrixEqual(dense, csr, verbose) && verbose)
            {
                Debug.WriteLine("[success]\tCSRSparseMatrixTests.Rank1Update");
            }
        }
        private static void InsertRow(bool verbose)
        {
            CSRSparseMatrix<double> csr = CSRSparseMatrix.Random<double>(Random(10, 20), Random(10, 20), 0.5);
            DenseMatrix<double> dense = new DenseMatrix<double>(csr);

            SparseVector<double> row = SparseVector.Random<double>(csr.Columns, 0.5);

            int rowIndex = Random(0, csr.Rows);
            csr.InsertRow(rowIndex, row);
            dense = dense.InsertRow(rowIndex, row.ToDenseVector());

            if (AssertMatrixEqual(csr, dense, verbose) && verbose)
            {
                Debug.WriteLine("[success]\tCSRSparseMatrixTests.InsertRow passed");
            }
        }
        private static void InsertColumn(bool verbose)
        {
            CSRSparseMatrix<double> csr = CSRSparseMatrix.Random<double>(Random(10, 20), Random(10, 20), 0.5);
            DenseMatrix<double> dense = new DenseMatrix<double>(csr);

            SparseVector<double> column = SparseVector.Random<double>(csr.Rows, 0.5);

            int colIndex = Random(0, csr.Columns);
            csr.InsertColumn(colIndex, column);
            dense = dense.InsertColumn(colIndex, column.ToDenseVector());

            if (AssertMatrixEqual(csr, dense, verbose) && verbose)
            {
                Debug.WriteLine("[success]\tCSRSparseMatrixTests.InsertColumn passed");
            }
        }
        private static void DirectSum(bool verbose)
        {
            CSRSparseMatrix<double> A = CSRSparseMatrix.Random<double>(Random(10, 20), Random(10, 20), 0.5),
                B = CSRSparseMatrix.Random<double>(Random(10, 20), Random(10, 20), 0.5);
            DenseMatrix<double> _A = new DenseMatrix<double>(A),
                _B = new DenseMatrix<double>(B);

            if (AssertMatrixEqual(A.DirectSum(B), _A.DirectSum(_B), verbose) && verbose)
            {
                Debug.WriteLine("[success]\tCSRSparseMatrixTests.DirectSum passed");
            }
        }
        private static void StructuralSymmetric(bool verbose)
        {
            Debug.Assert(!CSRSparseMatrix.Random<double>(10, 11, 0.5).IsSymmetricSymbolic());

            CSRSparseMatrix<double> sym = new CSRSparseMatrix<double>(DenseMatrix.RandomSPD<double>(10));
            Debug.Assert(sym.IsSymmetricSymbolic());

            // Change values to break numerical symmetry
            sym[0, 1] = 1;
            sym[1, 0] = 2;

            Debug.Assert(sym.IsSymmetricSymbolic());
        }

        public static void Cholesky(bool verbose)
        {
            int n = 10;
            DenseMatrix<double> B = DenseMatrix.Random<double>(n, n);
            CSRSparseMatrix<double> C = (CSRSparseMatrix<double>)B.Transpose().Multiply(B);

            CSRSparseMatrix<double> L = C.CholeskyFactor(DefaultProviders.Double);
            if (TestUtil.AssertMatrixEqual(L * L.Transpose(), C, (u, v) => Util.ApproximatelyEquals(u, v, 1e-4)) && verbose)
            {
                Debug.WriteLine("[success]\tCSRSparseMatrixTests.Cholesky passed");
            }
        }

        public static void SolveTriangular(bool verbose)
        {
            int dim = Random(10, 20);
            DenseMatrix<double> dense = DenseMatrix.RandomTriangular<double>(dim, dim, false);
            CSRSparseMatrix<double> csr = new CSRSparseMatrix<double>(dense);

            DenseVector<double> b = DenseVector.Random<double>(dim);
            DenseVector<double> x = new DenseVector<double>(dim);
            csr.SolveTriangular(b.Values, x.Values, BLAS.Double, true, false);

            if (TestUtil.AssertVectorsEqual(csr.Multiply(x, DefaultProviders.Double), b, (a, b) => Util.ApproximatelyEquals(a, b, 1e-4)))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tCSRSparseMatrixTests.SolveTriangular passed");
                }
            }
        }

    }
}
