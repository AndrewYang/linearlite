﻿using LinearNet.Matrices;
using LinearNet.Matrices.Sparse;
using LinearNet.Structs;
using System.Diagnostics;

namespace LinearNet.Tests
{
    public static class DOKSparseMatrixTests
    {
        public static void RunAll()
        {
            SparseMatrixCreationTest(false);
            SparseMatrixTransposeTest(false);
            SparseMatrixDeterminantTest(false);
        }

        public static void SparseMatrixCreationTest(bool verbose = false)
        {
            if (verbose)
            {
                DOKSparseMatrix<double> T0 = DOKSparseMatrix.RandomTriangular<double>(10, 10, 1000, true);
                Debug.WriteLine("Upper triangular matrix");
                T0.Print();

                DOKSparseMatrix<double> T1 = DOKSparseMatrix.RandomTriangular<double>(10, 15, 1000, true);
                Debug.WriteLine("Upper triangular matrix");
                T1.Print();

                DOKSparseMatrix<double> T2 = DOKSparseMatrix.RandomTriangular<double>(15, 10, 1000, true);
                Debug.WriteLine("Upper triangular matrix");
                T2.Print();

                DOKSparseMatrix<double> T3 = DOKSparseMatrix.RandomTriangular<double>(10, 15, 1000, false);
                Debug.WriteLine("Lower triangular matrix");
                T3.Print();

                DOKSparseMatrix<double> T4 = DOKSparseMatrix.RandomTriangular<double>(15, 10, 1000, false);
                Debug.WriteLine("Lower triangular matrix");
                T4.Print();

                DOKSparseMatrix<double> D = DOKSparseMatrix.RandomDiagonal<double>(15, 10, 100);
                Debug.WriteLine("Diagonal matrix");
                D.Print();

                DOKSparseMatrix<double> B1 = DOKSparseMatrix.RandomBidiagonal<double>(15, 10, 1000, true);
                Debug.WriteLine("Bidiagonal matrix");
                B1.Print();

                DOKSparseMatrix<double> B2 = DOKSparseMatrix.RandomBidiagonal<double>(10, 15, 1000, true);
                Debug.WriteLine("Bidiagonal matrix");
                B2.Print();

                DOKSparseMatrix<double> B3 = DOKSparseMatrix.RandomBidiagonal<double>(15, 10, 1000, false);
                Debug.WriteLine("Bidiagonal matrix");
                B3.Print();

                DOKSparseMatrix<double> B4 = DOKSparseMatrix.RandomBidiagonal<double>(10, 15, 1000, false);
                Debug.WriteLine("Bidiagonal matrix");
                B4.Print();

                DOKSparseMatrix<double> TR1 = DOKSparseMatrix.RandomTridiagonal<double>(10, 15, 1000);
                Debug.WriteLine("Tridiagonal matrix");
                TR1.Print();

                DOKSparseMatrix<double> TR2 = DOKSparseMatrix.RandomTridiagonal<double>(10, 10, 1000);
                Debug.WriteLine("Tridiagonal matrix");
                TR2.Print();

                DOKSparseMatrix<double> TR3 = DOKSparseMatrix.RandomTridiagonal<double>(15, 10, 1000);
                Debug.WriteLine("Tridiagonal matrix");
                TR3.Print();

                DOKSparseMatrix<double> H1 = DOKSparseMatrix.RandomHessenberg<double>(10, 1000, true);
                Debug.WriteLine("Hessenberg matrix");
                H1.Print();

                DOKSparseMatrix<double> H2 = DOKSparseMatrix.RandomHessenberg<double>(10, 1000, false);
                Debug.WriteLine("Hessenberg matrix");
                H2.Print();

                DOKSparseMatrix<double> I = DOKSparseMatrix.Identity<double>(10);
                Debug.WriteLine("Identity matrix");
                I.Print();
            }
            
        }
        public static void SparseMatrixTransposeTest(bool verbose = false)
        {
            if (verbose)
            {
                DOKSparseMatrix<double> matrix = DOKSparseMatrix.Random<double>(10, 10, 20);
                Debug.WriteLine("A");
                matrix.Print();

                matrix = matrix.Transpose();
                Debug.WriteLine("A^T");
                matrix.Print();

                DOKSparseMatrix<Complex> B = DOKSparseMatrix.Random<Complex>(10, 10, 20);
                Debug.WriteLine("B");
                B.Print();

                B = B.ConjugateTranspose();
                Debug.WriteLine("B^T");
                B.Print();
            }
        }
        public static void SparseMatrixDeterminantTest(bool verbose = false)
        {
            DenseMatrix<double> A = DenseMatrix.Random<double>(10, 10);
            DOKSparseMatrix<double> _A = (DOKSparseMatrix<double>)A;

            if (verbose)
            {
                Debug.WriteLine("A");
                A.Print();

                Debug.WriteLine("Dense det:\t" + A.Determinant(FinitePrecisionDeterminantMethod.ECHELON_ROW_REDUCTION));
                Debug.WriteLine("Sparse det:\t" + _A.Determinant());
            }
        }
    }
}
