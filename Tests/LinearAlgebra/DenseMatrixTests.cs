﻿using LinearNet.Decomposition;
using LinearNet.Decompositions;
using LinearNet.Matrices;
using LinearNet.Matrices.Cholesky;
using LinearNet.Matrices.Decompositions;
using LinearNet.Matrices.Decompositions.LU;
using LinearNet.Matrices.Decompositions.QR;
using LinearNet.Matrices.Inversion;
using LinearNet.Matrices.Multiplication;
using LinearNet.Matrices.QR;
using LinearNet.Matrices.TriangularSolve;
using LinearNet.Providers;
using LinearNet.Providers.LinearAlgebra;
using LinearNet.Providers.Managed.Parallel;
using LinearNet.Structs;
using LinearNet.Vectors;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Tests
{
    public static class DenseMatrixTests
    {
        private static Random _random = new Random();

        public static void RunAll(bool verbose = false)
        {
            MultiplyOverwriteTest(false);
            DeterminantTest(false);
            FixedSizeStrassenMultiply(false);
            VariableSizeStrassenMultiply(false);
            TransposedFixedSizeStrassenMultiply(false);
            CacheObliviousMultiply(false);
            MatrixIdentityInversionTest(verbose);
            MatrixInversionTest(false);
            PseudoInversionTest(false);
            OneSidedInverseTest(false);
            StrassenInversionTest(false);
            QRDecompositionTest(false);
            QRDecompositionGivensRotationTest(false);
            QRDecompositionBlockHouseholderTest(false);
            QRHessenbergDecompositionTest(false);
            CholeskyDecompositionTest(false);
            LDLTDecompositionTest(false);
            LUDecompositionTest(false);
            SVDecompositionTest(false);
            RankDecompositionTest(false);
            BlockLDUDecompositionTest(false);
            BlockLUDecompositionTest(false);
            BidiagonalizationTest(false);
            TridiagonalizationTest(false);
            ParallelTridiagonalizationTest(false);
            HessenbergFormTest(false);
            InducedNormTest(false);
            VectorizationTest(false);
            OuterProductTest(false);
            CPDecompositionTest(false);
            LUPDecompositionTest(false);
            LUBlockDecompositionTest(false);
            BandReductionTest(false);
            RowAdditionTest(false);
            FastMultiplyTest(false);

            TriangularSolve(verbose);
            QR2_HouseholderTest(verbose);
            QR2_BlockHouseholderTest(verbose);
            QR2_GramSchmidtTest(verbose);
            QR2_GivensRotationTest(verbose);
            QR2_HouseholderSolve(verbose);
            Chol2_BlockTest(verbose);
            TriangularSolve_Strassen(verbose);
            TriangularSolve_Strassen2(verbose);
        }

        private static void MatrixIdentityInversionTest(bool verbose = false)
        {
            DenseMatrix<double> matrix = DenseMatrix.Identity<double>(2);
            if (!matrix.Invert().IsIdentity())
            {
                matrix.Invert().Print();
                Debug.Assert(false, "MatrixIdentityInversionTest");
            }
        }
        public static void MatrixInversionTest(bool verbose = false)
        {
            int n = 20;
            List<Type> types = new List<Type>()
            {
                typeof(int),
                typeof(float),
                typeof(double),
                typeof(decimal),
                typeof(Complex)
            };

            if (types.Contains(typeof(float)))
            {
                DenseMatrix<float> matrix = DenseMatrix.Random<float>(n, n);
                DenseMatrix<float> inverse = matrix.Invert();
                DenseMatrix<float> identity = DenseMatrix.Identity<float>(n);
                DenseMatrix<float> product = matrix.Multiply(inverse);

                if (verbose)
                {
                    Debug.WriteLine("A");
                    matrix.Print();
                    Debug.WriteLine("A^-1");
                    inverse.Print();
                    Debug.WriteLine("AA^-1");
                    product.Print();
                    Debug.WriteLine("float[,]\t|AA^-1 - I|:\t" + product.Subtract(identity).ElementwiseNorm(1, 1));
                }
                Debug.Assert(product.ApproximatelyEquals(identity, 1e-2f));
            }
            if (types.Contains(typeof(double)))
            {
                Random r = new Random();
                DenseMatrix<double> matrix = DenseMatrix.Random(n, n, () => r.NextDouble() * 20 - 10);
                DenseMatrix<double> inverse = matrix.Invert();
                DenseMatrix<double> identity = DenseMatrix.Identity<double>(n);
                DenseMatrix<double> product = matrix.Multiply(inverse);

                if (verbose)
                {
                    Debug.WriteLine("A");
                    matrix.Print();
                    Debug.WriteLine("A^-1");
                    inverse.Print();
                    Debug.WriteLine("AA^-1");
                    product.Print();
                    Debug.WriteLine("double[,]\t|AA^-1 - I|:\t" + product.Subtract(identity).ElementwiseNorm(1, 1));
                }
                Debug.Assert(product.ApproximatelyEquals(identity));
            }
            if (types.Contains(typeof(decimal)))
            {
                Random r = new Random();
                DenseMatrix<decimal> matrix = DenseMatrix.Random(n, n, () => (decimal)(r.NextDouble() * 20 - 10));
                DenseMatrix<decimal> inverse = matrix.Invert();
                DenseMatrix<decimal> identity = DenseMatrix.Identity<decimal>(n);
                DenseMatrix<decimal> product = matrix.Multiply(inverse);

                if (verbose)
                {
                    Debug.WriteLine("A");
                    matrix.Print();
                    Debug.WriteLine("A^-1");
                    inverse.Print();
                    Debug.WriteLine("AA^-1");
                    product.Print();
                    Debug.WriteLine("decimal[,]\t|AA^-1 - I|:\t" + product.Subtract(identity).ElementwiseNorm(1, 1));
                }
                Debug.Assert(product.ApproximatelyEquals(identity, 1e-8m));
            }
            if (types.Contains(typeof(Complex)))
            {
                DenseMatrix<Complex> matrix = DenseMatrix.Random<Complex>(n, n);
                DenseMatrix<Complex> inverse = matrix.Invert();
                DenseMatrix<Complex> identity = DenseMatrix.Identity<Complex>(n);
                DenseMatrix<Complex> product = matrix.Multiply(inverse);

                if (verbose)
                {
                    Debug.WriteLine("|z|\t" + Complex.Zero.Modulus());
                    Debug.WriteLine("A");
                    matrix.Print();
                    Debug.WriteLine("A^-1");
                    inverse.Print();
                    Debug.WriteLine("AA^-1");
                    product.Print();
                    Debug.WriteLine("I");
                    identity.Print();
                    Debug.WriteLine("Complex[,]\t|AA^-1 - I|:\t" + product.Subtract(identity).ElementwiseNorm(1, 1));
                }
                Debug.Assert(product.ApproximatelyEquals(identity, 1e-4));
            }
        }
        public static void PseudoInversionTest(bool verbose = false)
        {
            int n = 10;
            Type[] types = { typeof(double), typeof(float), typeof(decimal), typeof(Complex) };

            if (types.Contains(typeof(double)))
            {
                DenseMatrix<double> A = DenseMatrix.Random<double>(n, n);
                DenseMatrix<double> P = A.PseudoInvert();
                DenseMatrix<double> APA = A.Multiply(P).Multiply(A);
                DenseMatrix<double> PAP = P.Multiply(A).Multiply(P);

                if (verbose)
                {
                    Debug.WriteLine("A");
                    A.Print();

                    Debug.WriteLine("A+");
                    P.Print();

                    Debug.WriteLine("AA+A");
                    APA.Print();

                    Debug.WriteLine("A+AA+");
                    PAP.Print();
                }

                DenseMatrix<double> AP = A.Multiply(P);
                DenseMatrix<double> PA = P.Multiply(A);

                Debug.Assert(APA.ApproximatelyEquals(A, 1e-6));
                Debug.Assert(PAP.ApproximatelyEquals(P, 1e-6));
                Debug.Assert(AP.IsSymmetric(1e-6));
                Debug.Assert(PA.IsSymmetric(1e-6));
            }
            if (types.Contains(typeof(float)))
            {
                DenseMatrix<float> A = DenseMatrix.Random<float>(n, n);
                DenseMatrix<float> P = A.PseudoInvert();
                DenseMatrix<float> APA = A.Multiply(P).Multiply(A);
                DenseMatrix<float> PAP = P.Multiply(A).Multiply(P);

                if (verbose)
                {
                    Debug.WriteLine("A");
                    A.Print();

                    Debug.WriteLine("A+");
                    P.Print();

                    Debug.WriteLine("AA+A");
                    APA.Print();

                    Debug.WriteLine("A+AA+");
                    PAP.Print();
                }

                DenseMatrix<float> AP = A.Multiply(P);
                DenseMatrix<float> PA = P.Multiply(A);

                Debug.Assert(APA.ApproximatelyEquals(A, 1e-1f));
                Debug.Assert(PAP.ApproximatelyEquals(P, 1e-1f));
                Debug.Assert(AP.IsSymmetric(1e-1f));
                Debug.Assert(PA.IsSymmetric(1e-1f));
            }
            if (types.Contains(typeof(decimal)))
            {
                DenseMatrix<decimal> A = DenseMatrix.Random<decimal>(n, n);
                DenseMatrix<decimal> P = A.PseudoInvert();
                DenseMatrix<decimal> APA = A.Multiply(P).Multiply(A);
                DenseMatrix<decimal> PAP = P.Multiply(A).Multiply(P);

                if (verbose)
                {
                    Debug.WriteLine("A");
                    A.Print();

                    Debug.WriteLine("A+");
                    P.Print();

                    Debug.WriteLine("AA+A");
                    APA.Print();

                    Debug.WriteLine("A+AA+");
                    PAP.Print();
                }

                DenseMatrix<decimal> AP = A.Multiply(P);
                DenseMatrix<decimal> PA = P.Multiply(A);

                Debug.Assert(APA.ApproximatelyEquals(A));
                Debug.Assert(PAP.ApproximatelyEquals(P));
                Debug.Assert(AP.IsSymmetric(1e-6m));
                Debug.Assert(PA.IsSymmetric(1e-6m));
            }
            if (types.Contains(typeof(Complex)))
            {
                DenseMatrix<Complex> A = DenseMatrix.Random<Complex>(n, n);
                DenseMatrix<Complex> P = A.PseudoInvert();
                DenseMatrix<Complex> APA = A.Multiply(P).Multiply(A);
                DenseMatrix<Complex> PAP = P.Multiply(A).Multiply(P);

                if (verbose)
                {
                    Debug.WriteLine("A");
                    A.Print();

                    Debug.WriteLine("A+");
                    P.Print();

                    Debug.WriteLine("AA+A");
                    APA.Print();

                    Debug.WriteLine("A+AA+");
                    PAP.Print();
                }

                DenseMatrix<Complex> AP = A.Multiply(P);
                DenseMatrix<Complex> PA = P.Multiply(A);

                Debug.Assert(APA.ApproximatelyEquals(A, 1e-4));
                Debug.Assert(PAP.ApproximatelyEquals(P, 1e-4));
                Debug.Assert(AP.IsHermitian(1e-4));
                Debug.Assert(PA.IsHermitian(1e-4));
            }
        }
        public static void OneSidedInverseTest(bool verbose = false)
        {
            int m = 10, n = 5;
            DenseMatrix<double> A = DenseMatrix.Random<double>(m, n);
            DenseMatrix<double> L = A.LeftInverse();

            DenseMatrix<double> B = DenseMatrix.Random<double>(n, m);
            DenseMatrix<double> R = B.RightInverse();

            if (verbose)
            {
                Debug.WriteLine("A");
                A.Print();

                Debug.WriteLine("L");
                L.Print();

                Debug.WriteLine("LA");
                L.Multiply(A).Print();

                Debug.WriteLine("B");
                B.Print();

                Debug.WriteLine("R");
                R.Print();

                Debug.WriteLine("BR");
                B.Multiply(R).Print();
            }

            Debug.Assert(L.Multiply(A).IsIdentity());
            Debug.Assert(B.Multiply(R).IsIdentity());
        }
        public static void StrassenInversionTest(bool verbose = false)
        {
            int n = 128;
            List<Type> types = new List<Type>()
            {
                typeof(double),
                typeof(Complex)
            };

            if (types.Contains(typeof(double)))
            {
                DenseMatrix<double> A = DenseMatrix.Random<double>(n, n);
                DenseMatrix<double> A_inv_s = A.Invert(new StrassenMatrixInversion<double>(n, 32));

                if (verbose)
                {
                    Debug.WriteLine("A");
                    A.Print();

                    Debug.WriteLine("_A^-1");
                    A_inv_s.Print();

                    Debug.WriteLine("_A * _A^-1");
                    A.Multiply(A_inv_s).Print();
                }

                Debug.Assert(A.Multiply(A_inv_s).IsIdentity(1e-5));
            }
            if (types.Contains(typeof(Complex)))
            {
                DenseMatrix<Complex> A = DenseMatrix.Random<Complex>(n, n);
                DenseMatrix<Complex> A_inv_s = A.Invert(new StrassenMatrixInversion<Complex>(n, 32));

                if (verbose)
                {
                    Debug.WriteLine("A");
                    A.Print();

                    Debug.WriteLine("_A^-1");
                    A_inv_s.Print();

                    Debug.WriteLine("_A * _A^-1");
                    A.Multiply(A_inv_s).Print();
                }

                Debug.Assert(A.Multiply(A_inv_s).IsIdentity());
            }
        }
        public static void DeterminantTest(bool verbose = false)
        {
            int n = 100;
            DenseMatrix<double> matrix = DenseMatrix.Random<double>(n, n);

            double det1 = matrix.Determinant(FinitePrecisionDeterminantMethod.ECHELON_ROW_REDUCTION);
            double det2 = matrix.Determinant(FinitePrecisionDeterminantMethod.LU_DECOMPOSITION);

            if (verbose)
            {
                //matrix.Print();
                Debug.WriteLine("Det(A):\t" + det1 + "\t" + det2);
            }
            Debug.Assert(Util.ApproximatelyEquals(det1, det2));
        }

        private static int Rand(int min, int max)
        {
            return min + _random.Next(max - min);
        }
        private static void MultiplyTest(Func<int, int, int, IMatrixMultiplication<double>> GetAlgo, bool verbose = false)
        {
            Random r = new Random();
            int m = Rand(10, 20), n = Rand(10, 20), p = Rand(10, 20);
            IMatrixMultiplication<double> algo = GetAlgo(m, n, p);
            IMatrixMultiplication<double> native = new NaiveMatrixMultiplication<double>(new NativeDoubleProvider());
            DenseMatrix<double> A = DenseMatrix.Random<double>(m, n);
            DenseMatrix<double> B = DenseMatrix.Random<double>(n, p);

            // Test 1 - aligned positioning
            DenseMatrix<double> C1 = A.Multiply(B, native);
            DenseMatrix<double> C2 = A.Multiply(B, algo);

            if (verbose)
            {
                C1.Print(x => x.ToString("n6"));
                C2.Print(x => x.ToString("n6"));
            }
            Debug.Assert(C1.ApproximatelyEquals(C2), $"<{m}, {n}, {p}> starting at (0, 0) multiplication failed for {algo}");

            int block_m = Rand(4, m),
                block_n = Rand(4, n),
                block_p = Rand(4, p);

            // Test 2 - general positioning
            int ar = r.Next(m - block_m), ac = r.Next(n - block_n),
                br = r.Next(n - block_n), bc = r.Next(p - block_p),
                cr = r.Next(m - block_m), cc = r.Next(p - block_p);

            int _m = r.Next(Math.Min(m / 2, m / 2)),
                _n = r.Next(Math.Min(n / 2, n / 2)),
                _p = r.Next(Math.Min(p / 2, p / 2));

            DenseMatrix<double> D1 = new DenseMatrix<double>(m, p), D2 = new DenseMatrix<double>(m, p);
            
            algo = GetAlgo(5, 4, 4);
            algo.Multiply(A.Values, B.Values, D1.Values, 2, 4, 3, 2, 1, 0, new Size3(5, 4, 4), false);
            native.Multiply(A.Values, B.Values, D2.Values, 2, 4, 3, 2, 1, 0, new Size3(5, 4, 4), false);

            if (verbose)
            {
                D1.Print(x => x.ToString("n6"));
                D2.Print(x => x.ToString("n6"));
            }
            Debug.Assert(D1.ApproximatelyEquals(D2), $"<{m}, {n}, {p}> starting at ({cr}, {cc}) multiplication failed for {algo}");
        }
        public static void MultiplyOverwriteTest(bool verbose = false)
        {
            int n = 10;
            List<Type> types = new List<Type>()
            {
                typeof(float), 
                typeof(double), 
                //typeof(decimal), 
                typeof(Complex)
            };

            if (types.Contains(typeof(float)))
            {
                DenseMatrix<float> A = DenseMatrix.Random<float>(n, n), B = DenseMatrix.Random<float>(n, n), A1 = A.Copy(), B1 = B.Copy();

                double stime, otime1, otime2, potime;
                Stopwatch sw = new Stopwatch();
                sw.Start();
                DenseMatrix<float> C = A.Multiply(B);
                sw.Stop();
                stime = sw.ElapsedMilliseconds;

                sw.Restart();
                A1.MultiplyOverwrite(B, true);
                sw.Stop();
                otime1 = sw.ElapsedMilliseconds;

                sw.Restart();
                A.MultiplyOverwrite(B, false);
                sw.Stop();
                otime2 = sw.ElapsedMilliseconds;

                sw.Restart();
                A.MultiplyOverwriteParallel(B1, false);
                sw.Stop();
                potime = sw.ElapsedMilliseconds;
                
                if (verbose)
                {
                    Debug.WriteLine("[float]\tNaive sequential multiplication:\t" + stime);
                    Debug.WriteLine("[float]\tSequential overwrite multiplication (overwrite A):\t" + otime1);
                    Debug.WriteLine("[float]\tSequential overwrite multiplication (overwrite B):\t" + otime2);
                    Debug.WriteLine("[float]\tParallel overwrite multiplication:\t" + potime);
                }

                Debug.Assert(C.ApproximatelyEquals(A1));
                Debug.Assert(C.ApproximatelyEquals(B));
                Debug.Assert(C.ApproximatelyEquals(B1));
            }
            if (types.Contains(typeof(double)))
            {
                DenseMatrix<double> A = DenseMatrix.Random<double>(n, n), B = DenseMatrix.Random<double>(n, n), A1 = A.Copy(), B1 = B.Copy();

                double stime, otime1, otime2, potime;
                Stopwatch sw = new Stopwatch();
                sw.Start();
                DenseMatrix<double> C = A.Multiply(B);
                sw.Stop();
                stime = sw.ElapsedMilliseconds;

                sw.Restart();
                A1.MultiplyOverwrite(B, true);
                sw.Stop();
                otime1 = sw.ElapsedMilliseconds;

                sw.Restart();
                A.MultiplyOverwrite(B, false);
                sw.Stop();
                otime2 = sw.ElapsedMilliseconds;

                sw.Restart();
                A.MultiplyOverwriteParallel(B1, false);
                sw.Stop();
                potime = sw.ElapsedMilliseconds;

                if (verbose)
                {
                    Debug.WriteLine("[double]\tNaive sequential multiplication:\t" + stime);
                    Debug.WriteLine("[double]\tSequential overwrite multiplication (overwrite A):\t" + otime1);
                    Debug.WriteLine("[double]\tSequential overwrite multiplication (overwrite B):\t" + otime2);
                    Debug.WriteLine("[double]\tParallel overwrite multiplication:\t" + potime);
                }

                Debug.Assert(C.ApproximatelyEquals(A1));
                Debug.Assert(C.ApproximatelyEquals(B));
                Debug.Assert(C.ApproximatelyEquals(B1));
            }
            if (types.Contains(typeof(decimal)))
            {
                DenseMatrix<decimal> A = DenseMatrix.Random<decimal>(n, n), B = DenseMatrix.Random<decimal>(n, n), A1 = A.Copy(), B1 = B.Copy();

                double stime, otime1, otime2, potime;
                Stopwatch sw = new Stopwatch();
                sw.Start();
                DenseMatrix<decimal> C = A.Multiply(B);
                sw.Stop();
                stime = sw.ElapsedMilliseconds;

                sw.Restart();
                A1.MultiplyOverwrite(B, true);
                sw.Stop();
                otime1 = sw.ElapsedMilliseconds;

                sw.Restart();
                A.MultiplyOverwrite(B, false);
                sw.Stop();
                otime2 = sw.ElapsedMilliseconds;

                sw.Restart();
                A.MultiplyOverwriteParallel(B1, false);
                sw.Stop();
                potime = sw.ElapsedMilliseconds;

                if (verbose)
                {
                    Debug.WriteLine("[decimal]\tNaive sequential multiplication:\t" + stime);
                    Debug.WriteLine("[decimal]\tSequential overwrite multiplication (overwrite A):\t" + otime1);
                    Debug.WriteLine("[decimal]\tSequential overwrite multiplication (overwrite B):\t" + otime2);
                    Debug.WriteLine("[decimal]\tParallel overwrite multiplication:\t" + potime);
                }

                Debug.Assert(C.ApproximatelyEquals(A1));
                Debug.Assert(C.ApproximatelyEquals(B));
                Debug.Assert(C.ApproximatelyEquals(B1));
            }
            if (types.Contains(typeof(Complex)))
            {
                DenseMatrix<Complex> A = DenseMatrix.Random<Complex>(n, n), B = DenseMatrix.Random<Complex>(n, n), A1 = A.Copy(), B1 = B.Copy();

                double stime, otime1, otime2, potime;
                Stopwatch sw = new Stopwatch();
                sw.Start();
                DenseMatrix<Complex> C = A.Multiply(B);
                sw.Stop();
                stime = sw.ElapsedMilliseconds;

                sw.Restart();
                A1.MultiplyOverwrite(B, true);
                sw.Stop();
                otime1 = sw.ElapsedMilliseconds;

                sw.Restart();
                A.MultiplyOverwrite(B, false);
                sw.Stop();
                otime2 = sw.ElapsedMilliseconds;

                sw.Restart();
                A.MultiplyOverwriteParallel(B1, false);
                sw.Stop();
                potime = sw.ElapsedMilliseconds;

                if (verbose)
                {
                    Debug.WriteLine("[Complex]\tNaive sequential multiplication:\t" + stime);
                    Debug.WriteLine("[Complex]\tSequential overwrite multiplication (overwrite A):\t" + otime1);
                    Debug.WriteLine("[Complex]\tSequential overwrite multiplication (overwrite B):\t" + otime2);
                    Debug.WriteLine("[Complex]\tParallel overwrite multiplication:\t" + potime);
                }

                Debug.Assert(C.ApproximatelyEquals(A1));
                Debug.Assert(C.ApproximatelyEquals(B));
                Debug.Assert(C.ApproximatelyEquals(B1));
            }
            
        }
        public static void FastMultiplyTest(bool verbose = false)
        {
            int space = 200, n = 40;

            DenseMatrix<double> A = DenseMatrix.Random<double>(space, space),
                                        B = DenseMatrix.Random<double>(space, space),
                                        Bt = new DenseMatrix<double>(n, n),
                                        C1 = new DenseMatrix<double>(space, space);

            Random r = new Random();
            int A_row_start = r.Next(space - n);
            int A_col_start = r.Next(space - n);
            int B_row_start = r.Next(space - n);
            int B_col_start = r.Next(space - n);
            int C_row_start = r.Next(space - n);
            int C_col_start = r.Next(space - n);

            IMatrixMultiplication<double> provider = new NaiveMatrixMultiplication<double>(new NativeDoubleProvider(), MultiplicationMode.OPTIMIZE_SPEED, Bt.Values);
            provider.Multiply(A.Values, B.Values, C1.Values,
                A_row_start, A_col_start,
                B_row_start, B_col_start,
                C_row_start, C_col_start, 
                new Size3(n, n, n), false);

            DenseMatrix<double> C1comp = C1.Submatrix(C_row_start, C_col_start, n, n);
            DenseMatrix<double> C2 = A.Submatrix(A_row_start, A_col_start, n, n).Multiply(B.Submatrix(B_row_start, B_col_start, n, n));

            if (!C1comp.ApproximatelyEquals(C2))
            {
                Debug.WriteLine(C1comp.SubtractParallel(C2).ElementwiseNorm(1, 1) / (n * n));
                throw new Exception();
            }
        }
        public static void FixedSizeStrassenMultiply(bool verbose = false)
        {
            // Sequential ----------------------------------------------------------
            var v = new NativeDoubleProvider();
            var baseAlgo = new NaiveMatrixMultiplication<double>(v);

            // Winograd's variant
            MultiplyTest((m, n, p) => new FixedSizeStrassenMultiplication<double>(v, v, baseAlgo, m, n, p, true, 4), verbose);
            // Strassen's variant
            MultiplyTest((m, n, p) => new FixedSizeStrassenMultiplication<double>(v, v, baseAlgo, m, n, p, false, 4), verbose);


            // Parallel ----------------------------------------------------------
            var b2 = new NativeParallelDoubleProvider();

            // Winograd's variant
            MultiplyTest((m, n, p) => new FixedSizeStrassenMultiplication<double>(v, b2, baseAlgo, m, n, p, true, 4), verbose);
            // Strassen's variant
            MultiplyTest((m, n, p) => new FixedSizeStrassenMultiplication<double>(v, b2, baseAlgo, m, n, p, false, 4), verbose);

        }
        public static void VariableSizeStrassenMultiply(bool verbose = false)
        {
            // sequential ----------------------------------------------------------
            var v = new NativeDoubleProvider();
            var baseAlgo = new NaiveMatrixMultiplication<double>(v);

            // Winograd's variant
            MultiplyTest((m, n, p) => new VariableSizeStrassenMultiplication<double>(v, v, baseAlgo, m, n, p, true, 4), verbose);
            // Strassen's variant
            MultiplyTest((m, n, p) => new VariableSizeStrassenMultiplication<double>(v, v, baseAlgo, m, n, p, false, 4), verbose);
        }
        public static void TransposedFixedSizeStrassenMultiply(bool verbose = false)
        {
            int m = 16, n = 16, p = 16;

            DenseMatrix<double> A = DenseMatrix.Random<double>(m, n),
                B = DenseMatrix.Random<double>(n, p),
                Bt = B.Transpose(),
                C = new DenseMatrix<double>(m, p);

            NativeDoubleProvider pr = new NativeDoubleProvider();
            IMatrixMultiplication<double> algo = new TransposedFixedSizeStrassenMultiplication<double>(pr, pr, m, n, p, 8);

            algo.Multiply(A.Values, Bt.Values, C.Values, new Size3(m, n, p), false);

            if (verbose)
            {
                Debug.WriteLine("Transposed multiply:");
                C.Print(x => x.ToString("n6"));

                Debug.WriteLine("Correct answer:");
                A.Multiply(B).Print(x => x.ToString("n6"));
            }
        }
        public static void CacheObliviousMultiply(bool verbose = false)
        {
            bool testNonTransposedCase = true;
            bool testTransposedCase = true;

            Random r = new Random();
            int m = 100 + r.Next(100), 
                n = 100 + r.Next(100), 
                p = 100 + r.Next(100);

            DenseMatrix<double> A = DenseMatrix.Random<double>(m, n);
            DenseMatrix<double> B = DenseMatrix.Random<double>(n, p);
            DenseMatrix<double> AB = A.Multiply(B, new NaiveMatrixMultiplication<double>(new NativeDoubleProvider()));

            if (testNonTransposedCase)
            {
                DenseMatrix<double> C1 = A.Multiply(B, new DivideAndConquerMatrixMultiplication<double>(new NativeDoubleProvider(), 2));

                if (verbose)
                {
                    Debug.WriteLine("Using cache-oblivious algorithm (no transpose):");
                    C1.Print(x => x.ToString("n6"));

                    Debug.WriteLine("Using naive algorithm:");
                    AB.Print(x => x.ToString("n6"));
                }
                Debug.Assert(C1.ApproximatelyEquals(AB));
            }
            if (testTransposedCase)
            {
                var dac = new DivideAndConquerMatrixMultiplication<double>(new NativeDoubleProvider(), 2, MultiplicationMode.OPTIMIZE_SPEED);
                DenseMatrix<double> result = new DenseMatrix<double>(m, p);
                dac.Multiply(A.Values, B.Values, result.Values, new Size3(m, n, p), false);

                if (verbose)
                {
                    Debug.WriteLine("Using cache-oblivious algorithm (with transpose):");
                    result.Print(x => x.ToString("n6"));

                    Debug.WriteLine("Using naive algorithm:");
                    AB.Print(x => x.ToString("n6"));
                }
                Debug.Assert(result.ApproximatelyEquals(AB));
            }
        }
        public static void QRDecompositionTest(bool verbose = false)
        {
            int m = 15, n = 10;
            List<Type> types = new List<Type>()
            {
                typeof(float),
                typeof(double),
                typeof(decimal),
                //typeof(Complex)
            };
            List<QRDecompositionMethod> methods = new List<QRDecompositionMethod>()
            {
                QRDecompositionMethod.HOUSEHOLDER_TRANSFORM,
                QRDecompositionMethod.GIVENS_ROTATIONS
            };

            foreach (QRDecompositionMethod method in methods)
            {
                if (types.Contains(typeof(float)))
                {
                    DenseMatrix<float> A = DenseMatrix.Random<float>(m, n);
                    A.QRDecompose(out DenseMatrix<float> Q, out DenseMatrix<float> R, positiveDiagonals: true, method: method);

                    DenseMatrix<float> QQ = Q.Transpose().Multiply(Q);
                    DenseMatrix<float> QR = Q.Multiply(R);

                    bool test1 = QQ.IsIdentity(1e-4f), test2 = R.IsUpperTriangular(), test3 = QR.ApproximatelyEquals(A, 1e-3f);
                    if (!test1 || !test2 || !test3 || verbose)
                    {
                        Debug.WriteLine("A");
                        A.Print();
                        Debug.WriteLine("Q^TQ");
                        QQ.Print();
                        Debug.WriteLine("QR");
                        QR.Print();
                        Debug.WriteLine("|Q^TQ - I|\t" + QQ.Subtract(DenseMatrix.Identity<float>(m)).ElementwiseNorm(1, 1));
                        Debug.WriteLine("|QR - A|\t" + QR.Subtract(A).ElementwiseNorm(1, 1));
                    }
                    Debug.Assert(test1, "Q^TQ != I");
                    Debug.Assert(test2, "R is not upper triangular");
                    Debug.Assert(test3, "QR != A");
                }
                if (types.Contains(typeof(float)))
                {
                    DenseMatrix<float> A = DenseMatrix.Random<float>(m, n);
                    A.QLDecompose(out DenseMatrix<float> Q, out DenseMatrix<float> L, positiveDiagonals: true, method: method);

                    DenseMatrix<float> QQ = Q.Transpose().Multiply(Q);
                    DenseMatrix<float> QL = Q.Multiply(L);

                    bool test1 = QQ.IsIdentity(1e-4f), test2 = L.IsLowerTriangular(), test3 = QL.ApproximatelyEquals(A, 1e-3f);
                    if (!test1 || !test2 || !test3 || verbose)
                    {
                        Debug.WriteLine("A");
                        A.Print();
                        Debug.WriteLine("L");
                        L.Print();
                        Debug.WriteLine("Q^TQ");
                        QQ.Print();
                        Debug.WriteLine("QL");
                        QL.Print();
                        Debug.WriteLine("|Q^TQ - I|\t" + QQ.Subtract(DenseMatrix.Identity<float>(m)).ElementwiseNorm(1, 1));
                        Debug.WriteLine("|QL - A|\t" + QL.Subtract(A).ElementwiseNorm(1, 1));
                    }
                    Debug.Assert(test1, "Q^TQ != I");
                    Debug.Assert(test2, "L is not lower triangular");
                    Debug.Assert(test3, "QL != A");
                }
                if (types.Contains(typeof(float)))
                {
                    DenseMatrix<float> A = DenseMatrix.Random<float>(n, m);
                    A.LQDecompose(out DenseMatrix<float> L, out DenseMatrix<float> Q, positiveDiagonals: true, method: method);

                    DenseMatrix<float> QQ = Q.Transpose().Multiply(Q);
                    DenseMatrix<float> LQ = L.Multiply(Q);
                    bool test1 = QQ.IsIdentity(1e-4f), test2 = L.IsLowerTriangular(), test3 = LQ.ApproximatelyEquals(A, 1e-3f);
                    if (!test1 || !test2 || !test3 || verbose)
                    {
                        Debug.WriteLine("A");
                        A.Print();
                        Debug.WriteLine("Q^TQ");
                        QQ.Print();
                        Debug.WriteLine("LQ");
                        LQ.Print();
                        Debug.WriteLine("|Q^TQ - I|\t" + QQ.Subtract(DenseMatrix.Identity<float>(m)).ElementwiseNorm(1, 1));
                        Debug.WriteLine("|LQ - A|\t" + LQ.Subtract(A).ElementwiseNorm(1, 1));
                    }
                    Debug.Assert(test1, "Q^TQ != I");
                    Debug.Assert(test2, "L is not lower triangular");
                    Debug.Assert(test3, "LQ != A");
                }
                if (types.Contains(typeof(float)))
                {
                    DenseMatrix<float> A = DenseMatrix.Random<float>(n, m);
                    A.RQDecompose(out DenseMatrix<float> R, out DenseMatrix<float> Q, positiveDiagonals: true, method: method);

                    DenseMatrix<float> QQ = Q.Transpose().Multiply(Q);
                    DenseMatrix<float> RQ = R.Multiply(Q);
                    bool test1 = QQ.IsIdentity(1e-4f), test2 = R.IsUpperTriangular(), test3 = RQ.ApproximatelyEquals(A, 1e-3f);
                    if (!test1 || !test2 || !test3 || verbose)
                    {
                        Debug.WriteLine("A");
                        A.Print();
                        Debug.WriteLine("Q^TQ");
                        QQ.Print();
                        Debug.WriteLine("RQ");
                        RQ.Print();
                        Debug.WriteLine("|Q^TQ - I|\t" + QQ.Subtract(DenseMatrix.Identity<float>(m)).ElementwiseNorm(1, 1));
                        Debug.WriteLine("|RQ - A|\t" + RQ.Subtract(A).ElementwiseNorm(1, 1));
                    }
                    Debug.Assert(test1, "Q^TQ != I");
                    Debug.Assert(test2, "R is not upper triangular");
                    Debug.Assert(test3, "RQ != A");
                }

                if (types.Contains(typeof(double)))
                {
                    DenseMatrix<double> A = DenseMatrix.Random<double>(m, n);
                    A.QRDecompose(out DenseMatrix<double> Q, out DenseMatrix<double> R, positiveDiagonals: true, method: method);

                    DenseMatrix<double> QQ = Q.Transpose().Multiply(Q);
                    DenseMatrix<double> QR = Q.Multiply(R);
                    if (verbose)
                    {
                        Debug.WriteLine("A");
                        A.Print();
                        Debug.WriteLine("Q^TQ");
                        QQ.Print();
                        Debug.WriteLine("QR");
                        QR.Print();
                        Debug.WriteLine("|Q^TQ - I|\t" + QQ.Subtract(DenseMatrix.Identity<double>(m)).ElementwiseNorm(1, 1));
                        Debug.WriteLine("|QR - A|\t" + QR.Subtract(A).ElementwiseNorm(1, 1));
                    }
                    Debug.Assert(QQ.IsIdentity(), "Q^TQ != I");
                    Debug.Assert(R.IsUpperTriangular(), "R is not upper triangular");
                    Debug.Assert(QR.ApproximatelyEquals(A), "QR != A");
                }
                if (types.Contains(typeof(double)))
                {
                    DenseMatrix<double> A = DenseMatrix.Random<double>(m, n);
                    A.QLDecompose(out DenseMatrix<double> Q, out DenseMatrix<double> L, positiveDiagonals: true, method: method);
                    Debug.Assert(Q.Transpose().Multiply(Q).IsIdentity(), "Q^TQ != I");
                    Debug.Assert(L.IsLowerTriangular(), "L is not lower triangular");
                    Debug.Assert(Q.Multiply(L).ApproximatelyEquals(A), "QL != A");
                }
                if (types.Contains(typeof(double)))
                {
                    DenseMatrix<double> A = DenseMatrix.Random<double>(n, m);
                    A.LQDecompose(out DenseMatrix<double> L, out DenseMatrix<double> Q, positiveDiagonals: true, method: method);
                    Debug.Assert(Q.Transpose().Multiply(Q).IsIdentity(), "Q^TQ != I");
                    Debug.Assert(L.IsLowerTriangular(), "L is not lower triangular");
                    Debug.Assert(L.Multiply(Q).ApproximatelyEquals(A), "LQ != A");
                }
                if (types.Contains(typeof(double)))
                {
                    DenseMatrix<double> A = DenseMatrix.Random<double>(n, m);
                    A.RQDecompose(out DenseMatrix<double> R, out DenseMatrix<double> Q, positiveDiagonals: true, method: method);
                    Debug.Assert(Q.Transpose().Multiply(Q).IsIdentity(), "Q^TQ != I");
                    Debug.Assert(R.IsUpperTriangular(), "R is not upper triangular");
                    Debug.Assert(R.Multiply(Q).ApproximatelyEquals(A), "RQ != A");
                }

                if (types.Contains(typeof(decimal)))
                {
                    DenseMatrix<decimal> A = DenseMatrix.Random<decimal>(m, n);
                    A.QRDecompose(out DenseMatrix<decimal> Q, out DenseMatrix<decimal> R, positiveDiagonals: true, method: method);

                    DenseMatrix<decimal> QQ = Q.Transpose().Multiply(Q);
                    DenseMatrix<decimal> QR = Q.Multiply(R);
                    if (verbose)
                    {
                        Debug.WriteLine("A");
                        A.Print();
                        Debug.WriteLine("Q^TQ");
                        QQ.Print();
                        Debug.WriteLine("QR");
                        QR.Print();
                        Debug.WriteLine("|Q^TQ - I|\t" + QQ.Subtract(DenseMatrix.Identity<decimal>(m)).ElementwiseNorm(1, 1));
                        Debug.WriteLine("|QR - A|\t" + QR.Subtract(A).ElementwiseNorm(1, 1));
                    }
                    Debug.Assert(QQ.IsIdentity(), "Q^TQ != I");
                    Debug.Assert(R.IsUpperTriangular(), "R is not upper triangular");
                    Debug.Assert(QR.ApproximatelyEquals(A), "QR != A");
                }
                if (types.Contains(typeof(decimal)))
                {
                    DenseMatrix<decimal> A = DenseMatrix.Random<decimal>(m, n);
                    A.QLDecompose(out DenseMatrix<decimal> Q, out DenseMatrix<decimal> L, positiveDiagonals: true, method: method);
                    DenseMatrix<decimal> QQ = Q.Transpose().Multiply(Q);
                    DenseMatrix<decimal> QL = Q.Multiply(L);
                    if (verbose)
                    {
                        Debug.WriteLine("A");
                        A.Print();
                        Debug.WriteLine("Q^TQ");
                        QQ.Print();
                        Debug.WriteLine("QL");
                        QL.Print();
                        Debug.WriteLine("|Q^TQ - I|\t" + QQ.Subtract(DenseMatrix.Identity<decimal>(m)).ElementwiseNorm(1, 1));
                        Debug.WriteLine("|QL - A|\t" + QL.Subtract(A).ElementwiseNorm(1, 1));
                    }
                    Debug.Assert(Q.Transpose().Multiply(Q).IsIdentity(), "Q^TQ != I");
                    Debug.Assert(L.IsLowerTriangular(), "L is not lower triangular");
                    Debug.Assert(Q.Multiply(L).ApproximatelyEquals(A), "QL != A");
                }
                if (types.Contains(typeof(decimal)))
                {
                    DenseMatrix<decimal> A = DenseMatrix.Random<decimal>(n, m);
                    A.LQDecompose(out DenseMatrix<decimal> L, out DenseMatrix<decimal> Q, positiveDiagonals: true, method: method);
                    Debug.Assert(Q.Transpose().Multiply(Q).IsIdentity(), "Q^TQ != I");
                    Debug.Assert(L.IsLowerTriangular(), "L is not lower triangular");
                    Debug.Assert(L.Multiply(Q).ApproximatelyEquals(A), "LQ != A");
                }
                if (types.Contains(typeof(decimal)))
                {
                    DenseMatrix<decimal> A = DenseMatrix.Random<decimal>(n, m);
                    A.RQDecompose(out DenseMatrix<decimal> R, out DenseMatrix<decimal> Q, positiveDiagonals: true, method: method);
                    Debug.Assert(Q.Transpose().Multiply(Q).IsIdentity(), "Q^TQ != I");
                    Debug.Assert(R.IsUpperTriangular(), "R is not upper triangular");
                    Debug.Assert(R.Multiply(Q).ApproximatelyEquals(A), "RQ != A");
                }

                if (types.Contains(typeof(Complex)))
                {
                    DenseMatrix<Complex> A = DenseMatrix.Random<Complex>(m, n);
                    A.QRDecompose(out DenseMatrix<Complex> Q, out DenseMatrix<Complex> R, positiveDiagonals: true, method: method);

                    DenseMatrix<Complex> QQ = Q.ConjugateTranspose().Multiply(Q);
                    DenseMatrix<Complex> QR = Q.Multiply(R);
                    if (verbose)
                    {
                        Debug.WriteLine("A");
                        A.Print();
                        Debug.WriteLine("Q^TQ");
                        QQ.Print();
                        Debug.WriteLine("QR");
                        QR.Print();
                        Debug.WriteLine("|Q^TQ - I|\t" + QQ.Subtract(DenseMatrix.Identity<Complex>(m)).ElementwiseNorm(1, 1));
                        Debug.WriteLine("|QR - A|\t" + QR.Subtract(A).ElementwiseNorm(1, 1));
                    }
                    Debug.Assert(QQ.IsIdentity(), "Q*Q != I");
                    Debug.Assert(R.IsUpperTriangular(), "R is not upper triangular");
                    Debug.Assert(QR.ApproximatelyEquals(A), "QR != A");
                }
                if (types.Contains(typeof(Complex)))
                {
                    DenseMatrix<Complex> A = DenseMatrix.Random<Complex>(m, n);
                    A.QLDecompose(out DenseMatrix<Complex> Q, out DenseMatrix<Complex> L, positiveDiagonals: true, method: method);
                    Debug.Assert(Q.ConjugateTranspose().Multiply(Q).IsIdentity(), "Q^TQ != I");
                    Debug.Assert(L.IsLowerTriangular(), "L is not lower triangular");
                    Debug.Assert(Q.Multiply(L).ApproximatelyEquals(A), "QL != A");
                }
                if (types.Contains(typeof(Complex)))
                {
                    DenseMatrix<Complex> A = DenseMatrix.Random<Complex>(n, m);
                    A.LQDecompose(out DenseMatrix<Complex> L, out DenseMatrix<Complex> Q, positiveDiagonals: true, method: method);
                    Debug.Assert(Q.ConjugateTranspose().Multiply(Q).IsIdentity(), "Q^TQ != I");
                    Debug.Assert(L.IsLowerTriangular(), "L is not lower triangular");
                    Debug.Assert(L.Multiply(Q).ApproximatelyEquals(A), "LQ != A");
                }
                if (types.Contains(typeof(Complex)))
                {
                    DenseMatrix<Complex> A = DenseMatrix.Random<Complex>(n, m);
                    A.RQDecompose(out DenseMatrix<Complex> R, out DenseMatrix<Complex> Q, positiveDiagonals: true, method: method);
                    Debug.Assert(Q.ConjugateTranspose().Multiply(Q).IsIdentity(), "Q^TQ != I");
                    Debug.Assert(R.IsUpperTriangular(), "R is not upper triangular");
                    Debug.Assert(R.Multiply(Q).ApproximatelyEquals(A), "RQ != A");
                }
            }
        }
        public static void QRDecompositionGivensRotationTest(bool verbose = false)
        {
            List<Type> types = new List<Type>()
            {
                typeof(double),
                typeof(Complex)
            };

            if (types.Contains(typeof(double)))
            {
                DenseMatrix<double> matrix = DenseMatrix.Random<double>(5, 3);
                matrix.QRDecompose(out DenseMatrix<double> Q, out DenseMatrix<double> R, false, QRDecompositionMethod.GIVENS_ROTATIONS);

                if (verbose)
                {
                    Debug.WriteLine("A");
                    matrix.Print();

                    Debug.WriteLine("R");
                    R.Print();

                    Debug.WriteLine("Q");
                    Q.Print();

                    Debug.WriteLine("QR");
                    Q.Multiply(R).Print();

                    Debug.WriteLine("QQ^T");
                    Q.Transpose().Multiply(Q).Print();
                }
            }
            if (types.Contains(typeof(double)))
            {
                DenseMatrix<double> matrix = DenseMatrix.Random<double>(25, 20);
                matrix.QLDecompose(out DenseMatrix<double> Q, out DenseMatrix<double> L, false, QRDecompositionMethod.GIVENS_ROTATIONS);
                DenseMatrix<double> QL = Q.Multiply(L);

                if (verbose)
                {
                    Debug.WriteLine("A");
                    matrix.Print();

                    Debug.WriteLine("L");
                    L.Print();

                    Debug.WriteLine("Q");
                    Q.Print();

                    Debug.WriteLine("QL");
                    QL.Print();

                    Debug.WriteLine("QQ^T");
                    Q.Transpose().Multiply(Q).Print();
                }
                Debug.Assert(matrix.ApproximatelyEquals(QL));
            }
            if (types.Contains(typeof(double)))
            {
                DenseMatrix<double> matrix = DenseMatrix.Random<double>(25, 20);
                matrix.QRDecomposeParallel(out DenseMatrix<double> Q, out DenseMatrix<double> R, false, QRDecompositionMethod.GIVENS_ROTATIONS);
                DenseMatrix<double> QR = Q.Multiply(R);

                if (verbose)
                {
                    Debug.WriteLine("A");
                    matrix.Print();

                    Debug.WriteLine("R");
                    R.Print();

                    Debug.WriteLine("Q");
                    Q.Print();

                    Debug.WriteLine("QR");
                    QR.Print();

                    Debug.WriteLine("QQ^T");
                    Q.Transpose().Multiply(Q).Print();
                }
                Debug.Assert(matrix.ApproximatelyEquals(QR));
            }
            if (types.Contains(typeof(Complex)))
            {
                DenseMatrix<Complex> matrix = DenseMatrix.Random<Complex>(5, 3);
                matrix.QRDecompose(out DenseMatrix<Complex> Q, out DenseMatrix<Complex> R, false, QRDecompositionMethod.GIVENS_ROTATIONS);

                if (verbose)
                {
                    Debug.WriteLine("A");
                    matrix.Print();

                    Debug.WriteLine("R");
                    R.Print();

                    Debug.WriteLine("Q");
                    Q.Print();

                    Debug.WriteLine("QR");
                    Q.Multiply(R).Print();

                    Debug.WriteLine("QQ^T");
                    Q.ConjugateTranspose().Multiply(Q).Print();
                }
            }
            
        }
        private static void QRDecompositionBlockHouseholderTest(bool verbose = false)
        {
            DenseMatrix<double> matrix = DenseMatrix.Random<double>(32, 24);

            var algo = new BlockHouseholderQRDecompositionAlgorithm(4);
            algo.QRDecompose(matrix, out DenseMatrix<double> Q, out DenseMatrix<double> R, false, false);

            DenseMatrix<double> QR = Q.Multiply(R);
            DenseMatrix<double> QQ = Q.Transpose().Multiply(Q);
            if (verbose)
            {
                Debug.WriteLine("A");
                matrix.Print(x => x.ToString("n6"));

                Debug.WriteLine("R");
                R.Print(x => x.ToString("n6"));

                Debug.WriteLine("Q");
                Q.Print(x => x.ToString("n6"));

                Debug.WriteLine("QR");
                QR.Print(x => x.ToString("n6"));

                Debug.WriteLine("QQ^T");
                Q.Transpose().Multiply(Q).Print(x => x.ToString("n6"));
            }

            Debug.Assert(R.IsUpperTriangular());
            Debug.Assert(QR.ApproximatelyEquals(matrix));
            Debug.Assert(QQ.IsIdentity());
        }
        public static void QRHessenbergDecompositionTest(bool verbose = false)
        {
            Type[] types = { typeof(double), typeof(Complex) };

            IDenseQRDecompositionAlgorithm qr = new HouseholderQRDecompositionAlgorithm();
            if (types.Contains(typeof(double)))
            {
                DenseMatrix<double> hessenberg = DenseMatrix.RandomHessenberg<double>(10);
                DenseMatrix<double> Q = DenseMatrix.Identity<double>(10);

                if (verbose)
                {
                    Debug.WriteLine("H");
                    hessenberg.Print();
                }
                
                qr.QRDecomposeHessenberg(Q.Values, hessenberg.Values);

                if (verbose)
                {
                    Debug.WriteLine("Q");
                    Q.Print(x => x.ToString("n6"));

                    Debug.WriteLine("R");
                    hessenberg.Print(x => x.ToString("n6"));

                    Debug.WriteLine("QR");
                    Q.Multiply(hessenberg).Print(x => x.ToString("n6"));
                }
            }
            if (types.Contains(typeof(Complex)))
            {
                //DenseMatrix<Complex> hessenberg = DenseMatrix.RandomHessenberg<Complex>(10);
                DenseMatrix<Complex> hessenberg = DenseMatrix.RandomBidiagonal<Complex>(10, 10, false);
                DenseMatrix<Complex> Q = DenseMatrix.Identity<Complex>(10);

                if (verbose)
                {
                    Debug.WriteLine("H");
                    hessenberg.Print();
                }

                qr.QRDecomposeHessenberg(Q.Values, hessenberg.Values);

                if (verbose)
                {
                    Debug.WriteLine("Q");
                    Q.Print();

                    Debug.WriteLine("R");
                    hessenberg.Print();

                    Debug.WriteLine("QR");
                    Q.Multiply(hessenberg).Print();
                }
            }
        }
        public static void CholeskyDecompositionTest(bool verbose = false)
        {
            int n = 10;

            List<Type> types = new List<Type>()
            {
                typeof(float),
                typeof(double),
                typeof(decimal),
                typeof(Complex)
            };

            if (types.Contains(typeof(float)))
            {
                DenseMatrix<float> matrix = DenseMatrix.RandomSPD<float>(n);
                matrix.CholeskyDecompose(out DenseMatrix<float> L);

                if (verbose)
                {
                    Debug.WriteLine("Matrix");
                    matrix.Print();

                    Debug.WriteLine("L");
                    L.Print();

                    Debug.WriteLine("LL^T");
                    L.Multiply(L.Transpose()).Print();

                    Debug.WriteLine("|LL^T - A|:\t" + L.Multiply(L.Transpose()).Subtract(matrix).ElementwiseNorm(1, 1));
                }
                Debug.Assert(L.Multiply(L.Transpose()).ApproximatelyEquals(matrix, 1e-4f), "LL^T != A");
                Debug.Assert(L.IsLowerTriangular(), "L is not lower triangular");
            }
            if (types.Contains(typeof(double)))
            {
                DenseMatrix<double> matrix = DenseMatrix.RandomSPD<double>(n);
                matrix.CholeskyDecompose(out DenseMatrix<double> L);

                if (verbose)
                {
                    Debug.WriteLine("Matrix");
                    matrix.Print();

                    Debug.WriteLine("L");
                    L.Print();

                    Debug.WriteLine("LL^T");
                    L.Multiply(L.Transpose()).Print();

                    Debug.WriteLine("|LL^T - A|:\t" + L.Multiply(L.Transpose()).Subtract(matrix).ElementwiseNorm(1, 1));
                }
                Debug.Assert(L.Multiply(L.Transpose()).ApproximatelyEquals(matrix));
                Debug.Assert(L.IsLowerTriangular());
            }
            if (types.Contains(typeof(decimal)))
            {
                DenseMatrix<decimal> matrix = DenseMatrix.RandomSPD<decimal>(n);
                matrix.CholeskyDecompose(out DenseMatrix<decimal> L);

                if (verbose)
                {
                    Debug.WriteLine("Matrix");
                    matrix.Print();

                    Debug.WriteLine("L");
                    L.Print();

                    Debug.WriteLine("LL^T");
                    L.Multiply(L.Transpose()).Print();

                    Debug.WriteLine("|LL^T - A|:\t" + L.Multiply(L.Transpose()).Subtract(matrix).ElementwiseNorm(1, 1));
                }
                Debug.Assert(L.Multiply(L.Transpose()).ApproximatelyEquals(matrix));
                Debug.Assert(L.IsLowerTriangular());
            }
        }
        public static void LDLTDecompositionTest(bool verbose = false)
        {
            int n = 10;
            List<Type> types = new List<Type>()
            {
                typeof(float),
                typeof(double),
                typeof(decimal),
                typeof(Complex),
                typeof(Rational)
            };

            if (types.Contains(typeof(float)))
            {
                DenseMatrix<float> A = DenseMatrix.RandomSPD<float>(n);
                A.LDLTDecompose(out DenseMatrix<float> L, out DenseMatrix<float> D);
                DenseMatrix<float> LDLT = L.Multiply(D).Multiply(L.Transpose());

                if (verbose)
                {
                    Debug.WriteLine("A");
                    A.Print();

                    Debug.WriteLine("L");
                    L.Print();

                    Debug.WriteLine("D");
                    D.Print();

                    Debug.WriteLine("LDL^T");
                    LDLT.Print();

                    Debug.WriteLine("|LDL^T - A|:\t" + LDLT.Subtract(A).ElementwiseNorm(1, 1));
                }
                Debug.Assert(LDLT.ApproximatelyEquals(A, 1e-2f));
                Debug.Assert(L.IsLowerTriangular());
            }
            if (types.Contains(typeof(double)))
            {
                DenseMatrix<double> A = DenseMatrix.RandomSPD<double>(n);
                A.LDLTDecompose(out DenseMatrix<double> L, out DenseMatrix<double> D);
                DenseMatrix<double> LDLT = L.Multiply(D).Multiply(L.Transpose());

                if (verbose)
                {
                    Debug.WriteLine("A");
                    A.Print();

                    Debug.WriteLine("L");
                    L.Print();

                    Debug.WriteLine("D");
                    D.Print();

                    Debug.WriteLine("LDL^T");
                    LDLT.Print();

                    Debug.WriteLine("|LDL^T - A|:\t" + LDLT.Subtract(A).ElementwiseNorm(1, 1));
                }
                Debug.Assert(LDLT.ApproximatelyEquals(A));
                Debug.Assert(L.IsLowerTriangular());
            }
            if (types.Contains(typeof(decimal)))
            {
                DenseMatrix<decimal> A = DenseMatrix.RandomSPD<decimal>(n);
                A.LDLTDecompose(out DenseMatrix<decimal> L, out DenseMatrix<decimal> D);
                DenseMatrix<decimal> LDLT = L.Multiply(D).Multiply(L.Transpose());

                if (verbose)
                {
                    Debug.WriteLine("A");
                    A.Print();

                    Debug.WriteLine("L");
                    L.Print();

                    Debug.WriteLine("D");
                    D.Print();

                    Debug.WriteLine("LDL^T");
                    LDLT.Print();

                    Debug.WriteLine("|LDL^T - A|:\t" + LDLT.Subtract(A).ElementwiseNorm(1, 1));
                }
                Debug.Assert(LDLT.ApproximatelyEquals(A));
                Debug.Assert(L.IsLowerTriangular());
            }
            if (types.Contains(typeof(Complex)))
            {
                DenseMatrix<Complex> A = DenseMatrix.RandomSPD<Complex>(n);
                A.LDLTDecompose(out DenseMatrix<Complex> L, out DenseMatrix<Complex> D);
                DenseMatrix<Complex> LDLT = L.Multiply(D).Multiply(L.ConjugateTranspose());

                if (verbose)
                {
                    Debug.WriteLine("A");
                    A.Print();

                    Debug.WriteLine("L");
                    L.Print();

                    Debug.WriteLine("D");
                    D.Print();

                    Debug.WriteLine("LDL^T");
                    LDLT.Print();

                    Debug.WriteLine("|LDL^T - A|:\t" + LDLT.Subtract(A).ElementwiseNorm(1, 1));
                }
                Debug.Assert(LDLT.ApproximatelyEquals(A));
                Debug.Assert(L.IsLowerTriangular());
            }
        }
        public static void LUDecompositionTest(bool verbose = false) 
        {
            int n = 30;

            List<Type> types = new List<Type>()
            {
                typeof(float),
                typeof(double),
                typeof(decimal),
                typeof(Complex)
            };

            if (types.Contains(typeof(float)))
            {
                DenseMatrix<float> A = DenseMatrix.Random<float>(n, n);
                A.LUDecompose(out DenseMatrix<float> L, out DenseMatrix<float> U);

                DenseMatrix<float> LU = L.Multiply(U);
                if (verbose)
                {
                    Debug.WriteLine("A");
                    A.Print();

                    Debug.WriteLine("L");
                    L.Print();

                    Debug.WriteLine("U");
                    U.Print();

                    Debug.WriteLine("LU");
                    LU.Print();

                    Debug.WriteLine("|LU - A|:\t" + LU.Subtract(A).ElementwiseNorm(1, 1));
                }

                Debug.Assert(L.IsLowerTriangular(1e-1f));
                Debug.Assert(U.IsUpperTriangular(1e-1f));
                Debug.Assert(LU.ApproximatelyEquals(A, 1e-1f));
            }
            if (types.Contains(typeof(double)))
            {
                DenseMatrix<double> A = DenseMatrix.Random<double>(n, n);
                A.LUDecompose(out DenseMatrix<double> L, out DenseMatrix<double> U);

                DenseMatrix<double> LU = L.Multiply(U);
                if (verbose)
                {
                    Debug.WriteLine("A");
                    A.Print();

                    Debug.WriteLine("L");
                    L.Print();

                    Debug.WriteLine("U");
                    U.Print();

                    Debug.WriteLine("LU");
                    LU.Print();

                    Debug.WriteLine("|LU - A|:\t" + LU.Subtract(A).ElementwiseNorm(1, 1));
                }

                Debug.Assert(L.IsLowerTriangular());
                Debug.Assert(U.IsUpperTriangular());
                Debug.Assert(LU.ApproximatelyEquals(A));
            }
            if (types.Contains(typeof(decimal)))
            {
                DenseMatrix<decimal> A = DenseMatrix.Random<decimal>(n, n);
                A.LUDecompose(out DenseMatrix<decimal> L, out DenseMatrix<decimal> U);

                DenseMatrix<decimal> LU = L.Multiply(U);
                if (verbose)
                {
                    Debug.WriteLine("A");
                    A.Print();

                    Debug.WriteLine("L");
                    L.Print();

                    Debug.WriteLine("U");
                    U.Print();

                    Debug.WriteLine("LU");
                    LU.Print();

                    Debug.WriteLine("|LU - A|:\t" + LU.Subtract(A).ElementwiseNorm(1, 1));
                }

                Debug.Assert(L.IsLowerTriangular());
                Debug.Assert(U.IsUpperTriangular());
                Debug.Assert(LU.ApproximatelyEquals(A));
            }
            if (types.Contains(typeof(Complex)))
            {
                DenseMatrix<Complex> A = DenseMatrix.Random<Complex>(n, n);
                A.LUDecompose(out DenseMatrix<Complex> L, out DenseMatrix<Complex> U);

                DenseMatrix<Complex> LU = L.Multiply(U);
                if (verbose)
                {
                    Debug.WriteLine("A");
                    A.Print();

                    Debug.WriteLine("L");
                    L.Print();

                    Debug.WriteLine("U");
                    U.Print();

                    Debug.WriteLine("LU");
                    LU.Print();

                    Debug.WriteLine("|LU - A|:\t" + LU.Subtract(A).ElementwiseNorm(1, 1));
                }

                Debug.Assert(L.IsLowerTriangular());
                Debug.Assert(U.IsUpperTriangular());
                Debug.Assert(LU.ApproximatelyEquals(A));
            }
            
        }
        public static void LUPDecompositionTest(bool verbose = false)
        {
            DenseMatrix<double> A = DenseMatrix.Random<double>(10, 10);

            A.LUPDecompose(out DenseMatrix<double> L, out DenseMatrix<double> U, out int[] P);
            A.PermuteRows(P);

            if (verbose)
            {
                Debug.WriteLine("L");
                L.Print();

                Debug.WriteLine("U");
                U.Print();

                Debug.WriteLine("LU");
                L.Multiply(U).Print();

                Debug.WriteLine("P");
                P.Print();

                Debug.WriteLine("PA");
                A.Print();
            }

            Debug.Assert(A.ApproximatelyEquals(L.Multiply(U)));
        }
        public static void LUBlockDecompositionTest(bool verbose = false)
        {
            if (verbose)
            {
                DenseMatrix<double> matrix = DenseMatrix.Random<double>(8, 8);

                Debug.WriteLine("matrix");
                matrix.Print();

                matrix.LUDecompose(out DenseMatrix<double> L, out DenseMatrix<double> U, false, LUDecompositionMethod.CAMARERO);

                Debug.WriteLine("L");
                L.Print();

                Debug.WriteLine("U");
                U.Print();

                Debug.WriteLine("LU");
                L.Multiply(U).Print();
            }
        }
        public static void SVDecompositionTest(bool verbose = false)
        {
            int m = 10, n = 15;

            List<Type> types = new List<Type>()
            {
                //typeof(float),
                typeof(double),
                //typeof(decimal),
                //typeof(Complex)
            };

            if (types.Contains(typeof(float)))
            {
                DenseMatrix<float> A = DenseMatrix.Random<float>(m, n);
                A.SingularValueDecompose(out DenseMatrix<float> U, out DenseMatrix<float> D, out DenseMatrix<float> V);
                float[] singularValues = A.SingularValues();

                DenseMatrix<float> UDV = U.Multiply(D).Multiply(V.Transpose());
                if (verbose)
                {
                    Debug.WriteLine("A");
                    A.Print();

                    Debug.WriteLine("U");
                    U.Print();

                    Debug.WriteLine("D");
                    D.Print();

                    Debug.WriteLine("V");
                    V.Print();

                    Debug.WriteLine("UDV");
                    UDV.Print();

                    Debug.WriteLine("|U^TDV - A| = " + UDV.Subtract(A).ElementwiseNorm(1, 1));

                    Debug.WriteLine("Singular values");
                    singularValues.Print();

                    Debug.WriteLine("D");
                    D.LeadingDiagonal().Print();

                    Debug.WriteLine("|D - s(A)| = " + D.LeadingDiagonal().Subtract(singularValues).Norm());
                }

                Debug.Assert(U.IsOrthogonal(1e-4f), "U is not orthogonal");
                Debug.Assert(V.IsOrthogonal(1e-4f), "V is not orthogonal");
                Debug.Assert(D.IsDiagonal(1e-4f), "D is not diagonal");
                Debug.Assert(UDV.ApproximatelyEquals(A, 1e-2f), "U^TDV != A");
                Debug.Assert(singularValues.ApproximatelyEquals(D.LeadingDiagonal(), 1e-2f), "Singular values are not equal to the leading diagonal");
            }
            if (types.Contains(typeof(double)))
            {
                DenseMatrix<double> A = DenseMatrix.Random<double>(m, n);
                A.SingularValueDecompose(out DenseMatrix<double> U, out DenseMatrix<double> D, out DenseMatrix<double> V);
                double[] singularValues = A.SingularValues();

                DenseMatrix<double> UDV = U.Multiply(D).Multiply(V.Transpose());
                if (verbose)
                {
                    Debug.WriteLine("A");
                    A.Print();

                    Debug.WriteLine("U");
                    U.Print();

                    Debug.WriteLine("D");
                    D.Print();

                    Debug.WriteLine("V");
                    V.Print();

                    Debug.WriteLine("UDV");
                    UDV.Print();

                    Debug.WriteLine("|U^TDV - A| = " + UDV.Subtract(A).ElementwiseNorm(1, 1));

                    Debug.WriteLine("Singular values");
                    singularValues.Print();

                    Debug.WriteLine("|D - s(A)| = " + D.LeadingDiagonal().Subtract(singularValues).Norm());
                }

                Debug.Assert(U.IsOrthogonal(), "U is not orthogonal");
                Debug.Assert(V.IsOrthogonal(), "V is not orthogonal");
                Debug.Assert(D.IsDiagonal(1e-6), "D is not diagonal");
                Debug.Assert(UDV.ApproximatelyEquals(A), "U^TDV != A");
                Debug.Assert(singularValues.ApproximatelyEquals(D.LeadingDiagonal()), "Singular values are not equal to the leading diagonal");
            }
            if (types.Contains(typeof(decimal)))
            {
                DenseMatrix<decimal> A = DenseMatrix.Random<decimal>(m, n);
                A.SingularValueDecompose(out DenseMatrix<decimal> U, out DenseMatrix<decimal> D, out DenseMatrix<decimal> V);
                decimal[] singularValues = A.SingularValues();

                DenseMatrix<decimal> UDV = U.Multiply(D).Multiply(V.Transpose());
                if (verbose)
                {
                    Debug.WriteLine("A");
                    A.Print();

                    Debug.WriteLine("U");
                    U.Print();

                    Debug.WriteLine("D");
                    D.Print();

                    Debug.WriteLine("V");
                    V.Print();

                    Debug.WriteLine("UDV");
                    UDV.Print();

                    Debug.WriteLine("|U^TDV - A| = " + UDV.Subtract(A).ElementwiseNorm(1, 1));

                    Debug.WriteLine("Singular values");
                    singularValues.Print();

                    Debug.WriteLine("|D - s(A)| = " + D.LeadingDiagonal().Subtract(singularValues).Norm());
                }

                Debug.Assert(U.IsOrthogonal(), "U is not orthogonal");
                Debug.Assert(V.IsOrthogonal(), "V is not orthogonal");
                Debug.Assert(D.IsDiagonal(Precision.DECIMAL_PRECISION), "D is not diagonal");
                Debug.Assert(UDV.ApproximatelyEquals(A), "UDV != A");
                Debug.Assert(singularValues.ApproximatelyEquals(D.LeadingDiagonal()), "Singular values are not equal to the leading diagonal");
            }
            if (types.Contains(typeof(Complex)))
            {
                DenseMatrix<Complex> A = DenseMatrix.Random<Complex>(m, n);
                A.SingularValueDecompose(out DenseMatrix<Complex> U, out DenseMatrix<Complex> D, out DenseMatrix<Complex> V);
                Complex[] singularValues = A.SingularValues();

                DenseMatrix<Complex> UDV = U.Multiply(D).Multiply(V.ConjugateTranspose());
                if (verbose)
                {
                    Debug.WriteLine("A");
                    A.Print();

                    Debug.WriteLine("U");
                    U.Print();

                    Debug.WriteLine("D");
                    D.Print();

                    Debug.WriteLine("V");
                    V.Print();

                    Debug.WriteLine("U*U");
                    U.ConjugateTranspose().Multiply(U).Print();

                    Debug.WriteLine("V*V");
                    V.ConjugateTranspose().Multiply(V).Print();

                    Debug.WriteLine("UDV");
                    UDV.Print();

                    Debug.WriteLine("|U^TDV - A| = " + UDV.Subtract(A).ElementwiseNorm(1, 1));

                    Debug.WriteLine("Singular values");
                    singularValues.Print();

                    Debug.WriteLine("|D - s(A)| = " + D.LeadingDiagonal().Subtract(singularValues).Norm());
                }

                Debug.Assert(U.IsUnitary(), "U is not orthogonal");
                Debug.Assert(V.IsUnitary(), "V is not orthogonal");
                Debug.Assert(D.IsDiagonal(Precision.DOUBLE_PRECISION), "D is not diagonal");
                Debug.Assert(UDV.ApproximatelyEquals(A), "UDV != A");
                Debug.Assert(singularValues.ApproximatelyEquals(D.LeadingDiagonal()), "Singular values are not equal to the leading diagonal");
            }
        }
        public static void RankDecompositionTest(bool verbose = false)
        {
            DenseMatrix<double> lowRankMatrix = DenseMatrix.RandomLowRankMatrix<double>(100, 100, 10);
            lowRankMatrix.RankDecompose(out DenseMatrix<double> C, out DenseMatrix<double> F);
            DenseMatrix<double> CF = C.Multiply(F);

            if (verbose)
            {
                Debug.WriteLine("A");
                lowRankMatrix.Print();

                Debug.WriteLine("Rank");
                Debug.WriteLine(lowRankMatrix.Rank());

                Debug.WriteLine("C");
                C.Print();

                Debug.WriteLine("F");
                F.Print();

                Debug.WriteLine("CF");
                CF.Print();
            }

            Debug.Assert(CF.ApproximatelyEquals(C.Multiply(F)));
        }
        public static void BlockLDUDecompositionTest(bool verbose = false)
        {
            DenseMatrix<double> matrix = DenseMatrix.Random<double>(10, 14);
            matrix.BlockLDUDecompose(out DenseMatrix<double> L, out DenseMatrix<double> D, out DenseMatrix<double> U);
            DenseMatrix<double> LDU = L.Multiply(D).Multiply(U);

            if (verbose)
            {
                Debug.WriteLine("A");
                matrix.Print();

                Debug.WriteLine("L");
                L.Print();

                Debug.WriteLine("D");
                D.Print();

                Debug.WriteLine("U");
                U.Print();

                Debug.WriteLine("LDU");
                LDU.Print();
            }
            Debug.Assert(LDU.ApproximatelyEquals(matrix));
        }
        public static void BlockLUDecompositionTest(bool verbose = false)
        {
            DenseMatrix<double> A = DenseMatrix.RandomSPD<double>(10);
            A.BlockLUDecompose(out DenseMatrix<double> L, out DenseMatrix<double> U);

            DenseMatrix<double> LU = L.Multiply(U);
            if (verbose)
            {
                Debug.WriteLine("A");
                A.Print();

                Debug.WriteLine("L");
                L.Print();

                Debug.WriteLine("U");
                U.Print();

                Debug.WriteLine("LU");
                LU.Print();
            }

            Debug.Assert(LU.ApproximatelyEquals(A));
        }

        public static void BidiagonalizationTest(bool verbose = false)
        {
            int m = 10, n = 15;

            bool[] sides = { 
                true, 
                false 
            };

            bool[] thicknesses =
            {
                true,
                //false
            };

            List<Type> types = new List<Type>()
            {
                typeof(float), 
                typeof(double), 
                typeof(decimal), 
                typeof(Complex)
            };

            List<BidiagonalizationMethod> methods = new List<BidiagonalizationMethod>() 
            { 
                //BidiagonalizationMethod.GOLUB_KAHAN_LANCZOS,
                BidiagonalizationMethod.HOUSEHOLDER_TRANSFORM,
                //BidiagonalizationMethod.GIVENS_ROTATIONS
            };

            foreach (BidiagonalizationMethod method in methods)
            {
                foreach (bool thin in thicknesses)
                {
                    foreach (bool side in sides)
                    {
                        if (types.Contains(typeof(float)))
                        {
                            DenseMatrix<float> A = DenseMatrix.Random<float>(m, n);
                            A.ToBidiagonalForm(out DenseMatrix<float> U, out DenseMatrix<float> B, out DenseMatrix<float> V, upper: side, thin: thin, method: method);

                            DenseMatrix<float> UU = U.Transpose().Multiply(U);
                            DenseMatrix<float> VV = V.Transpose().Multiply(V);
                            DenseMatrix<float> UBV = U.Transpose().Multiply(B).Multiply(V);
                            if (verbose)
                            {
                                Debug.WriteLine("U^TU");
                                UU.Print();
                                Debug.WriteLine("V^TV");
                                VV.Print();
                                Debug.WriteLine("A");
                                A.Print();
                                Debug.WriteLine("U^TBV");
                                UBV.Print();
                                Debug.WriteLine("|U^TU - I| = " + UU.Subtract(DenseMatrix.Identity<float>(m)).ElementwiseNorm(1, 1));
                                Debug.WriteLine("|V^TV - I| = " + VV.Subtract(DenseMatrix.Identity<float>(n)).ElementwiseNorm(1, 1));
                                Debug.WriteLine("|U^TBV - I| = " + UBV.Subtract(A).ElementwiseNorm(1, 1));
                            }
                            Debug.Assert(UU.IsIdentity(1e-1f), "U^TU != I");
                            Debug.Assert(VV.IsIdentity(1e-1f), "V^TV != I");
                            Debug.Assert(UBV.ApproximatelyEquals(A, 1e-1f), "U^TBV != A");
                            Debug.Assert(side ? B.IsUpperBidiagonal(1e-1f) : B.IsLowerBidiagonal(1e-1f), "B is not bidiagonal");
                        }
                        if (types.Contains(typeof(double)))
                        {
                            DenseMatrix<double> A = DenseMatrix.Random<double>(m, n);
                            A.ToBidiagonalForm(out DenseMatrix<double> U, out DenseMatrix<double> B, out DenseMatrix<double> V, upper: side, thin: thin, method: method);

                            DenseMatrix<double> UU = U.Transpose().Multiply(U);
                            DenseMatrix<double> VV = V.Transpose().Multiply(V);
                            DenseMatrix<double> UBV = U.Transpose().Multiply(B).Multiply(V);
                            if (verbose)
                            {
                                Debug.WriteLine("U^TU");
                                UU.Print();
                                Debug.WriteLine("V^TV");
                                VV.Print();
                                Debug.WriteLine("B");
                                B.Print();
                                Debug.WriteLine("A");
                                A.Print();
                                Debug.WriteLine("U^TBV");
                                UBV.Print();
                                Debug.WriteLine("|U^TU - I| = " + UU.Subtract(DenseMatrix.Identity<double>(m)).ElementwiseNorm(1, 1));
                                Debug.WriteLine("|V^TV - I| = " + VV.Subtract(DenseMatrix.Identity<double>(n)).ElementwiseNorm(1, 1));
                                Debug.WriteLine("|U^TBV - I| = " + UBV.Subtract(A).ElementwiseNorm(1, 1));
                            }
                            Debug.Assert(UU.IsIdentity(), "U^TU != I");
                            Debug.Assert(VV.IsIdentity(), "V^TV != I");
                            Debug.Assert(UBV.ApproximatelyEquals(A), "U^TBV != A");
                            Debug.Assert(side ? B.IsUpperBidiagonal() : B.IsLowerBidiagonal(), "B is not bidiagonal");
                        }
                        if (types.Contains(typeof(decimal)))
                        {
                            DenseMatrix<decimal> A = DenseMatrix.Random<decimal>(m, n);
                            A.ToBidiagonalForm(out DenseMatrix<decimal> U, out DenseMatrix<decimal> B, out DenseMatrix<decimal> V, upper: side, thin: thin, method: method);

                            DenseMatrix<decimal> UU = U.Transpose().Multiply(U);
                            DenseMatrix<decimal> VV = V.Transpose().Multiply(V);
                            DenseMatrix<decimal> UBV = U.Transpose().Multiply(B).Multiply(V);
                            if (verbose)
                            {
                                Debug.WriteLine("U^TU");
                                UU.Print();
                                Debug.WriteLine("V^TV");
                                VV.Print();
                                Debug.WriteLine("A");
                                A.Print();
                                Debug.WriteLine("U^TBV");
                                UBV.Print();
                                Debug.WriteLine("|U^TU - I| = " + UU.Subtract(DenseMatrix.Identity<decimal>(m)).ElementwiseNorm(1, 1));
                                Debug.WriteLine("|V^TV - I| = " + VV.Subtract(DenseMatrix.Identity<decimal>(n)).ElementwiseNorm(1, 1));
                                Debug.WriteLine("|U^TBV - I| = " + UBV.Subtract(A).ElementwiseNorm(1, 1));
                            }
                            Debug.Assert(UU.IsIdentity(), "U^TU != I");
                            Debug.Assert(VV.IsIdentity(), "V^TV != I");
                            Debug.Assert(UBV.ApproximatelyEquals(A), "U^TBV != A");
                            Debug.Assert(side ? B.IsUpperBidiagonal() : B.IsLowerBidiagonal(), "B is not bidiagonal");
                        }
                        if (types.Contains(typeof(Complex)))
                        {
                            DenseMatrix<Complex> A = DenseMatrix.Random<Complex>(m, n);
                            A.ToBidiagonalForm(out DenseMatrix<Complex> U, out DenseMatrix<Complex> B, out DenseMatrix<Complex> V, upper: side, thin: thin, method: method);

                            DenseMatrix<Complex> UU = U.ConjugateTranspose().Multiply(U);
                            DenseMatrix<Complex> VV = V.ConjugateTranspose().Multiply(V);
                            DenseMatrix<Complex> UBV = U.ConjugateTranspose().Multiply(B).Multiply(V);
                            if (verbose)
                            {
                                Debug.WriteLine("U^TU");
                                UU.Print();
                                Debug.WriteLine("V^TV");
                                VV.Print();
                                Debug.WriteLine("A");
                                A.Print();
                                Debug.WriteLine("U^TBV");
                                UBV.Print();
                                Debug.WriteLine("|U^TU - I| = " + UU.Subtract(DenseMatrix.Identity<Complex>(m)).ElementwiseNorm(1, 1));
                                Debug.WriteLine("|V^TV - I| = " + VV.Subtract(DenseMatrix.Identity<Complex>(n)).ElementwiseNorm(1, 1));
                                Debug.WriteLine("|U^TBV - I| = " + UBV.Subtract(A).ElementwiseNorm(1, 1));
                            }
                            Debug.Assert(UU.IsIdentity(), "U*U != I");
                            Debug.Assert(VV.IsIdentity(), "V*V != I");
                            Debug.Assert(UBV.ApproximatelyEquals(A), "U*BV != A");
                            Debug.Assert(side ? B.IsUpperBidiagonal() : B.IsLowerBidiagonal(), "B is not bidiagonal");
                        }
                    }
                }
            }
        }
        public static void TridiagonalizationTest(bool verbose = false)
        {
            int m = 20, n = 15;
            List<Type> types = new List<Type>()
            {
                typeof(float), typeof(double), typeof(decimal), typeof(Complex)
            };

            if (types.Contains(typeof(float)))
            {
                DenseMatrix<float> A = DenseMatrix.Random<float>(m, n);
                A.ToTridiagonalForm(out DenseMatrix<float> U, out DenseMatrix<float> D, out DenseMatrix<float> V);

                DenseMatrix<float> UU = U.Transpose().Multiply(U);
                DenseMatrix<float> VV = V.Transpose().Multiply(V);
                DenseMatrix<float> UDV = U.Transpose().Multiply(D).Multiply(V);
                if (verbose)
                {
                    Debug.WriteLine("U^TU");
                    UU.Print();
                    Debug.WriteLine("V^TV");
                    VV.Print();
                    Debug.WriteLine("A");
                    A.Print();
                    Debug.WriteLine("D");
                    D.Print();
                    Debug.WriteLine("U^TDV");
                    UDV.Print();
                    Debug.WriteLine("|U^TU - I| = " + UU.Subtract(DenseMatrix.Identity<float>(m)).ElementwiseNorm(1, 1));
                    Debug.WriteLine("|V^TV - I| = " + VV.Subtract(DenseMatrix.Identity<float>(n)).ElementwiseNorm(1, 1));
                    Debug.WriteLine("|U^TDV - A| = " + UDV.Subtract(A).ElementwiseNorm(1, 1));
                }
                Debug.Assert(UU.IsIdentity(1e-2f), "U^TU != I");
                Debug.Assert(VV.IsIdentity(1e-2f), "V^TV != I");
                Debug.Assert(UDV.ApproximatelyEquals(A, 1e-2f), "U^TDV != A");
                Debug.Assert(D.IsTridiagonal(1e-2f), "D is not tridiagonal");
            }
            if (types.Contains(typeof(double)))
            {
                DenseMatrix<double> A = DenseMatrix.Random<double>(m, n);
                A.ToTridiagonalForm(out DenseMatrix<double> U, out DenseMatrix<double> D, out DenseMatrix<double> V);

                DenseMatrix<double> UU = U.Transpose().Multiply(U);
                DenseMatrix<double> VV = V.Transpose().Multiply(V);
                DenseMatrix<double> UDV = U.Transpose().Multiply(D).Multiply(V);
                if (verbose)
                {
                    Debug.WriteLine("U^TU");
                    UU.Print();
                    Debug.WriteLine("V^TV");
                    VV.Print();
                    Debug.WriteLine("A");
                    A.Print();
                    Debug.WriteLine("D");
                    D.Print();
                    Debug.WriteLine("U^TDV");
                    UDV.Print();
                    Debug.WriteLine("|U^TU - I| = " + UU.Subtract(DenseMatrix.Identity<double>(m)).ElementwiseNorm(1, 1));
                    Debug.WriteLine("|V^TV - I| = " + VV.Subtract(DenseMatrix.Identity<double>(n)).ElementwiseNorm(1, 1));
                    Debug.WriteLine("|U^TDV - A| = " + UDV.Subtract(A).ElementwiseNorm(1, 1));
                }
                Debug.Assert(UU.IsIdentity(), "U^TU != I");
                Debug.Assert(VV.IsIdentity(), "V^TV != I");
                Debug.Assert(UDV.ApproximatelyEquals(A), "U^TDV != A");
                Debug.Assert(D.IsTridiagonal(), "D is not tridiagonal");
            }
            if (types.Contains(typeof(decimal)))
            {
                DenseMatrix<decimal> A = DenseMatrix.Random<decimal>(m, n);
                A.ToTridiagonalForm(out DenseMatrix<decimal> U, out DenseMatrix<decimal> D, out DenseMatrix<decimal> V);

                DenseMatrix<decimal> UU = U.Transpose().Multiply(U);
                DenseMatrix<decimal> VV = V.Transpose().Multiply(V);
                DenseMatrix<decimal> UDV = U.Transpose().Multiply(D).Multiply(V);
                if (verbose)
                {
                    Debug.WriteLine("U^TU");
                    UU.Print();
                    Debug.WriteLine("V^TV");
                    VV.Print();
                    Debug.WriteLine("A");
                    A.Print();
                    Debug.WriteLine("D");
                    D.Print();
                    Debug.WriteLine("U^TDV");
                    UDV.Print();
                    Debug.WriteLine("|U^TU - I| = " + UU.Subtract(DenseMatrix.Identity<decimal>(m)).ElementwiseNorm(1, 1));
                    Debug.WriteLine("|V^TV - I| = " + VV.Subtract(DenseMatrix.Identity<decimal>(n)).ElementwiseNorm(1, 1));
                    Debug.WriteLine("|U^TDV - A| = " + UDV.Subtract(A).ElementwiseNorm(1, 1));
                }
                Debug.Assert(UU.IsIdentity(), "U^TU != I");
                Debug.Assert(VV.IsIdentity(), "V^TV != I");
                Debug.Assert(UDV.ApproximatelyEquals(A), "U^TDV != A");
                Debug.Assert(D.IsTridiagonal(), "D is not tridiagonal");
            }
            if (types.Contains(typeof(Complex)))
            {
                DenseMatrix<Complex> A = DenseMatrix.Random<Complex>(m, n);
                A.ToTridiagonalForm(out DenseMatrix<Complex> U, out DenseMatrix<Complex> D, out DenseMatrix<Complex> V);

                DenseMatrix<Complex> UU = U.ConjugateTranspose().Multiply(U);
                DenseMatrix<Complex> VV = V.ConjugateTranspose().Multiply(V);
                DenseMatrix<Complex> UDV = U.ConjugateTranspose().Multiply(D).Multiply(V);
                if (verbose)
                {
                    Debug.WriteLine("U*U");
                    UU.Print();
                    Debug.WriteLine("V*V");
                    VV.Print();
                    Debug.WriteLine("A");
                    A.Print();
                    Debug.WriteLine("D");
                    D.Print();
                    Debug.WriteLine("U*DV");
                    UDV.Print();
                    Debug.WriteLine("|U*U - I| = " + UU.Subtract(DenseMatrix.Identity<Complex>(m)).ElementwiseNorm(1, 1));
                    Debug.WriteLine("|V*V - I| = " + VV.Subtract(DenseMatrix.Identity<Complex>(n)).ElementwiseNorm(1, 1));
                    Debug.WriteLine("|U*DV - A| = " + UDV.Subtract(A).ElementwiseNorm(1, 1));
                }
                Debug.Assert(UU.IsIdentity(), "U*U != I");
                Debug.Assert(VV.IsIdentity(), "V*V != I");
                Debug.Assert(UDV.ApproximatelyEquals(A), "U*DV != A");
                Debug.Assert(D.IsTridiagonal(), "D is not tridiagonal");
            }
        }
        public static void BandReductionTest(bool verbose = false)
        {
            int m = 20, n = 15;
            List<Type> types = new List<Type>()
            {
                typeof(float), typeof(double), typeof(decimal), typeof(Complex)
            };

            if (types.Contains(typeof(float)))
            {
                DenseMatrix<float> A = DenseMatrix.Random<float>(m, n);
                A.BandReduce(out DenseMatrix<float> U, out DenseMatrix<float> B, out DenseMatrix<float> V, 5, 3);

                DenseMatrix<float> UU = U.Transpose().Multiply(U);
                DenseMatrix<float> VV = V.Transpose().Multiply(V);
                DenseMatrix<float> UBV = U.Transpose().Multiply(B).Multiply(V);

                if (verbose)
                {
                    Debug.WriteLine("A");
                    A.Print();

                    Debug.WriteLine("U");
                    U.Print();

                    Debug.WriteLine("B");
                    B.Print();

                    Debug.WriteLine("V");
                    V.Print();

                    Debug.WriteLine("U^TBV");
                    UBV.Print();
                }

                Debug.Assert(UU.IsIdentity(1e-2f), "U is not orthogonal");
                Debug.Assert(VV.IsIdentity(1e-2f), "V is not orthogonal");
                Debug.Assert(UBV.ApproximatelyEquals(A, 1e-2f), "U^TBV != A");
            }
            if (types.Contains(typeof(double)))
            {
                DenseMatrix<double> A = DenseMatrix.Random<double>(m, n);
                A.BandReduce(out DenseMatrix<double> U, out DenseMatrix<double> B, out DenseMatrix<double> V, 2, 3);

                DenseMatrix<double> UU = U.Transpose().Multiply(U);
                DenseMatrix<double> VV = V.Transpose().Multiply(V);
                DenseMatrix<double> UBV = U.Transpose().Multiply(B).Multiply(V);

                if (verbose)
                {
                    Debug.WriteLine("A");
                    A.Print();

                    Debug.WriteLine("U");
                    U.Print();

                    Debug.WriteLine("B");
                    B.Print();

                    Debug.WriteLine("V");
                    V.Print();

                    Debug.WriteLine("U^TBV");
                    UBV.Print();
                }

                Debug.Assert(UU.IsIdentity(), "U is not orthogonal");
                Debug.Assert(VV.IsIdentity(), "V is not orthogonal");
                Debug.Assert(UBV.ApproximatelyEquals(A), "U^TBV != A");
            }
            if (types.Contains(typeof(decimal)))
            {
                DenseMatrix<decimal> A = DenseMatrix.Random<decimal>(m, n);
                A.BandReduce(out DenseMatrix<decimal> U, out DenseMatrix<decimal> B, out DenseMatrix<decimal> V, 6, 3);

                DenseMatrix<decimal> UU = U.Transpose().Multiply(U);
                DenseMatrix<decimal> VV = V.Transpose().Multiply(V);
                DenseMatrix<decimal> UBV = U.Transpose().Multiply(B).Multiply(V);

                if (verbose)
                {
                    Debug.WriteLine("A");
                    A.Print();

                    Debug.WriteLine("U");
                    U.Print();

                    Debug.WriteLine("B");
                    B.Print();

                    Debug.WriteLine("V");
                    V.Print();

                    Debug.WriteLine("U^TBV");
                    UBV.Print();
                }

                Debug.Assert(UU.IsIdentity(), "U is not orthogonal");
                Debug.Assert(VV.IsIdentity(), "V is not orthogonal");
                Debug.Assert(UBV.ApproximatelyEquals(A), "U^TBV != A");
            }
            if (types.Contains(typeof(Complex)))
            {
                DenseMatrix<Complex> A = DenseMatrix.Random<Complex>(m, n);
                A.BandReduce(out DenseMatrix<Complex> U, out DenseMatrix<Complex> B, out DenseMatrix<Complex> V, 5, 5);

                DenseMatrix<Complex> UU = U.ConjugateTranspose().Multiply(U);
                DenseMatrix<Complex> VV = V.ConjugateTranspose().Multiply(V);
                DenseMatrix<Complex> UBV = U.ConjugateTranspose().Multiply(B).Multiply(V);

                if (verbose)
                {
                    Debug.WriteLine("A");
                    A.Print();

                    Debug.WriteLine("U");
                    U.Print();

                    Debug.WriteLine("B");
                    B.Print();

                    Debug.WriteLine("V");
                    V.Print();

                    Debug.WriteLine("U^TBV");
                    UBV.Print();
                }

                Debug.Assert(UU.IsIdentity(), "U is not orthogonal");
                Debug.Assert(VV.IsIdentity(), "V is not orthogonal");
                Debug.Assert(UBV.ApproximatelyEquals(A), "U^TBV != A");
            }

        }
        public static void ParallelTridiagonalizationTest(bool verbose = false)
        {
            int m = 20, n = 15;
            List<Type> types = new List<Type>()
            {
                typeof(float), typeof(double), typeof(decimal), typeof(Complex)
            };

            if (types.Contains(typeof(float)))
            {
                DenseMatrix<float> A = DenseMatrix.Random<float>(m, n);
                A.ToTridiagonalFormParallel(out DenseMatrix<float> U, out DenseMatrix<float> D, out DenseMatrix<float> V);

                DenseMatrix<float> UU = U.Transpose().Multiply(U);
                DenseMatrix<float> VV = V.Transpose().Multiply(V);
                DenseMatrix<float> UDV = U.Transpose().Multiply(D).Multiply(V);
                if (verbose)
                {
                    Debug.WriteLine("U^TU");
                    UU.Print();
                    Debug.WriteLine("V^TV");
                    VV.Print();
                    Debug.WriteLine("A");
                    A.Print();
                    Debug.WriteLine("D");
                    D.Print();
                    Debug.WriteLine("U^TDV");
                    UDV.Print();
                    Debug.WriteLine("|U^TU - I| = " + UU.Subtract(DenseMatrix.Identity<float>(m)).ElementwiseNorm(1, 1));
                    Debug.WriteLine("|V^TV - I| = " + VV.Subtract(DenseMatrix.Identity<float>(n)).ElementwiseNorm(1, 1));
                    Debug.WriteLine("|U^TDV - A| = " + UDV.Subtract(A).ElementwiseNorm(1, 1));
                }
                Debug.Assert(UU.IsIdentity(1e-2f), "U^TU != I");
                Debug.Assert(VV.IsIdentity(1e-2f), "V^TV != I");
                Debug.Assert(UDV.ApproximatelyEquals(A, 1e-2f), "U^TDV != A");
                Debug.Assert(D.IsTridiagonal(1e-2f), "D is not tridiagonal");
            }
            if (types.Contains(typeof(double)))
            {
                DenseMatrix<double> A = DenseMatrix.Random<double>(m, n);
                A.ToTridiagonalFormParallel(out DenseMatrix<double> U, out DenseMatrix<double> D, out DenseMatrix<double> V);

                DenseMatrix<double> UU = U.Transpose().Multiply(U);
                DenseMatrix<double> VV = V.Transpose().Multiply(V);
                DenseMatrix<double> UDV = U.Transpose().Multiply(D).Multiply(V);
                if (verbose)
                {
                    Debug.WriteLine("U^TU");
                    UU.Print();
                    Debug.WriteLine("V^TV");
                    VV.Print();
                    Debug.WriteLine("A");
                    A.Print();
                    Debug.WriteLine("D");
                    D.Print();
                    Debug.WriteLine("U^TDV");
                    UDV.Print();
                    Debug.WriteLine("|U^TU - I| = " + UU.Subtract(DenseMatrix.Identity<double>(m)).ElementwiseNorm(1, 1));
                    Debug.WriteLine("|V^TV - I| = " + VV.Subtract(DenseMatrix.Identity<double>(n)).ElementwiseNorm(1, 1));
                    Debug.WriteLine("|U^TDV - A| = " + UDV.Subtract(A).ElementwiseNorm(1, 1));
                }
                Debug.Assert(UU.IsIdentity(), "U^TU != I");
                Debug.Assert(VV.IsIdentity(), "V^TV != I");
                Debug.Assert(UDV.ApproximatelyEquals(A), "U^TDV != A");
                Debug.Assert(D.IsTridiagonal(), "D is not tridiagonal");
            }
            if (types.Contains(typeof(decimal)))
            {
                DenseMatrix<decimal> A = DenseMatrix.Random<decimal>(m, n);
                A.ToTridiagonalFormParallel(out DenseMatrix<decimal> U, out DenseMatrix<decimal> D, out DenseMatrix<decimal> V);

                DenseMatrix<decimal> UU = U.Transpose().Multiply(U);
                DenseMatrix<decimal> VV = V.Transpose().Multiply(V);
                DenseMatrix<decimal> UDV = U.Transpose().Multiply(D).Multiply(V);
                if (verbose)
                {
                    Debug.WriteLine("U^TU");
                    UU.Print();
                    Debug.WriteLine("V^TV");
                    VV.Print();
                    Debug.WriteLine("A");
                    A.Print();
                    Debug.WriteLine("D");
                    D.Print();
                    Debug.WriteLine("U^TDV");
                    UDV.Print();
                    Debug.WriteLine("|U^TU - I| = " + UU.Subtract(DenseMatrix.Identity<decimal>(m)).ElementwiseNorm(1, 1));
                    Debug.WriteLine("|V^TV - I| = " + VV.Subtract(DenseMatrix.Identity<decimal>(n)).ElementwiseNorm(1, 1));
                    Debug.WriteLine("|U^TDV - A| = " + UDV.Subtract(A).ElementwiseNorm(1, 1));
                }
                Debug.Assert(UU.IsIdentity(), "U^TU != I");
                Debug.Assert(VV.IsIdentity(), "V^TV != I");
                Debug.Assert(UDV.ApproximatelyEquals(A), "U^TDV != A");
                Debug.Assert(D.IsTridiagonal(), "D is not tridiagonal");
            }
            if (types.Contains(typeof(Complex)))
            {
                DenseMatrix<Complex> A = DenseMatrix.Random<Complex>(m, n);
                A.ToTridiagonalFormParallel(out DenseMatrix<Complex> U, out DenseMatrix<Complex> D, out DenseMatrix<Complex> V);

                DenseMatrix<Complex> UU = U.ConjugateTranspose().Multiply(U);
                DenseMatrix<Complex> VV = V.ConjugateTranspose().Multiply(V);
                DenseMatrix<Complex> UDV = U.ConjugateTranspose().Multiply(D).Multiply(V);
                if (verbose)
                {
                    Debug.WriteLine("U*U");
                    UU.Print();
                    Debug.WriteLine("V*V");
                    VV.Print();
                    Debug.WriteLine("A");
                    A.Print();
                    Debug.WriteLine("D");
                    D.Print();
                    Debug.WriteLine("U*DV");
                    UDV.Print();
                    Debug.WriteLine("|U*U - I| = " + UU.Subtract(DenseMatrix.Identity<Complex>(m)).ElementwiseNorm(1, 1));
                    Debug.WriteLine("|V*V - I| = " + VV.Subtract(DenseMatrix.Identity<Complex>(n)).ElementwiseNorm(1, 1));
                    Debug.WriteLine("|U*DV - A| = " + UDV.Subtract(A).ElementwiseNorm(1, 1));
                }
                Debug.Assert(UU.IsIdentity(), "U*U != I");
                Debug.Assert(VV.IsIdentity(), "V*V != I");
                Debug.Assert(UDV.ApproximatelyEquals(A), "U*DV != A");
                Debug.Assert(D.IsTridiagonal(), "D is not tridiagonal");
            }
        }
        public static void HessenbergFormTest(bool verbose = false)
        {
            int m = 20;
            bool[] upper = { true, false };
            List<Type> types = new List<Type>()
            {
                typeof(float), typeof(double), typeof(decimal), typeof(Complex)
            };

            foreach (bool side in upper)
            {
                if (types.Contains(typeof(float)))
                {
                    DenseMatrix<float> A = DenseMatrix.Random<float>(m, m);
                    A.ToHessenbergForm(out DenseMatrix<float> Q, out DenseMatrix<float> H, upper: side);

                    DenseMatrix<float> QQ = Q.Transpose().Multiply(Q);
                    DenseMatrix<float> QHQ = Q.Transpose().Multiply(H).Multiply(Q);
                    if (verbose)
                    {
                        Debug.WriteLine((side ? "Upper" : "Lower") + " hessenberg decomposition test ---------------------");
                        Debug.WriteLine("Q^TQ");
                        QQ.Print();
                        Debug.WriteLine("A");
                        A.Print();
                        Debug.WriteLine("H");
                        H.Print();
                        Debug.WriteLine("Q^THQ");
                        QHQ.Print();

                        Debug.WriteLine("|Q^TQ - I| = " + QQ.Subtract(DenseMatrix.Identity<float>(m)).ElementwiseNorm(1, 1));
                        Debug.WriteLine("|Q^THQ - A| = " + QHQ.Subtract(A).ElementwiseNorm(1, 1));
                    }
                    Debug.Assert(QQ.IsIdentity(1e-2f), "Q^TQ != I");
                    Debug.Assert(QHQ.ApproximatelyEquals(A, 1e-2f), "Q^THQ != A");
                    Debug.Assert(side ? H.IsUpperHessenberg(1e-2f) : H.IsLowerHessenberg(1e-2f), "H is not in hessenberg form");
                }
                if (types.Contains(typeof(double)))
                {
                    DenseMatrix<double> A = DenseMatrix.Random<double>(m, m);
                    A.ToHessenbergForm(out DenseMatrix<double> Q, out DenseMatrix<double> H, upper: side);

                    DenseMatrix<double> QQ = Q.Transpose().Multiply(Q);
                    DenseMatrix<double> QHQ = Q.Transpose().Multiply(H).Multiply(Q);
                    if (verbose)
                    {
                        Debug.WriteLine((side ? "Upper" : "Lower") + " hessenberg decomposition test ---------------------");
                        Debug.WriteLine("Q^TQ");
                        QQ.Print();
                        Debug.WriteLine("A");
                        A.Print();
                        Debug.WriteLine("H");
                        H.Print();
                        Debug.WriteLine("Q^THQ");
                        QHQ.Print();

                        Debug.WriteLine("|Q^TQ - I| = " + QQ.Subtract(DenseMatrix.Identity<double>(m)).ElementwiseNorm(1, 1));
                        Debug.WriteLine("|Q^THQ - A| = " + QHQ.Subtract(A).ElementwiseNorm(1, 1));
                    }
                    Debug.Assert(QQ.IsIdentity(), "Q^TQ != I");
                    Debug.Assert(QHQ.ApproximatelyEquals(A), "Q^THQ != A");
                    Debug.Assert(side ? H.IsUpperHessenberg() : H.IsLowerHessenberg(), "H is not in hessenberg form");
                }
                if (types.Contains(typeof(decimal)))
                {
                    DenseMatrix<decimal> A = DenseMatrix.Random<decimal>(m, m);
                    A.ToHessenbergForm(out DenseMatrix<decimal> Q, out DenseMatrix<decimal> H, upper: side);

                    DenseMatrix<decimal> QQ = Q.Transpose().Multiply(Q);
                    DenseMatrix<decimal> QHQ = Q.Transpose().Multiply(H).Multiply(Q);
                    if (verbose)
                    {
                        Debug.WriteLine((side ? "Upper" : "Lower") + " hessenberg decomposition test ---------------------");
                        Debug.WriteLine("Q^TQ");
                        QQ.Print();
                        Debug.WriteLine("A");
                        A.Print();
                        Debug.WriteLine("H");
                        H.Print();
                        Debug.WriteLine("Q^THQ");
                        QHQ.Print();

                        Debug.WriteLine("|Q^TQ - I| = " + QQ.Subtract(DenseMatrix.Identity<decimal>(m)).ElementwiseNorm(1, 1));
                        Debug.WriteLine("|Q^THQ - A| = " + QHQ.Subtract(A).ElementwiseNorm(1, 1));
                    }
                    Debug.Assert(QQ.IsIdentity(), "Q^TQ != I");
                    Debug.Assert(QHQ.ApproximatelyEquals(A), "Q^THQ != A");
                    Debug.Assert(side ? H.IsUpperHessenberg() : H.IsLowerHessenberg(), "H is not in hessenberg form");
                }
                if (types.Contains(typeof(Complex)))
                {
                    DenseMatrix<Complex> A = DenseMatrix.Random<Complex>(m, m);
                    A.ToHessenbergForm(out DenseMatrix<Complex> Q, out DenseMatrix<Complex> H, upper: side);

                    DenseMatrix<Complex> QQ = Q.ConjugateTranspose().Multiply(Q);
                    DenseMatrix<Complex> QHQ = Q.ConjugateTranspose().Multiply(H).Multiply(Q);
                    if (verbose)
                    {
                        Debug.WriteLine((side ? "Upper" : "Lower") + " hessenberg decomposition test ---------------------");
                        Debug.WriteLine("Q*Q");
                        QQ.Print();
                        Debug.WriteLine("A");
                        A.Print();
                        Debug.WriteLine("H");
                        H.Print();
                        Debug.WriteLine("Q*HQ");
                        QHQ.Print();

                        Debug.WriteLine("|Q*Q - I| = " + QQ.Subtract(DenseMatrix.Identity<Complex>(m)).ElementwiseNorm(1, 1));
                        Debug.WriteLine("|Q*HQ - A| = " + QHQ.Subtract(A).ElementwiseNorm(1, 1));
                    }
                    Debug.Assert(QQ.IsIdentity(), "Q^TQ != I");
                    Debug.Assert(QHQ.ApproximatelyEquals(A), "Q^THQ != A");
                    Debug.Assert(side ? H.IsUpperHessenberg() : H.IsLowerHessenberg(), "H is not in hessenberg form");
                }
            }
            
        }
        public static void InducedNormTest(bool verbose = false)
        {
            DenseMatrix<double> matrix = DenseMatrix.Random<double>(100, 100);

            if (verbose)
            {
                for (int i = 3; i < 199; i += 10)
                {
                    Debug.WriteLine(i + "-norm:\t" + matrix.InducedNorm(i));
                }
                Debug.WriteLine(200 + "-norm:\t" + matrix.InducedNorm(200));
                Debug.WriteLine(300 + "-norm:\t" + matrix.InducedNorm(300));
                Debug.WriteLine(400 + "-norm:\t" + matrix.InducedNorm(400));
                Debug.WriteLine(500 + "-norm:\t" + matrix.InducedNorm(500));
                Debug.WriteLine(600 + "-norm:\t" + matrix.InducedNorm(600));
                Debug.WriteLine(700 + "-norm:\t" + matrix.InducedNorm(700));
                Debug.WriteLine(800 + "-norm:\t" + matrix.InducedNorm(800));
                Debug.WriteLine(900 + "-norm:\t" + matrix.InducedNorm(900));
                Debug.WriteLine(1000 + "-norm:\t" + matrix.InducedNorm(1000));
                Debug.WriteLine(10000 + "-norm:\t" + matrix.InducedNorm(10000));
                Debug.WriteLine(100000 + "-norm:\t" + matrix.InducedNorm(100000));
                Debug.WriteLine(1000000 + "-norm:\t" + matrix.InducedNorm(1000000));
            }
            
        }
        public static void VectorizationTest(bool verbose = false)
        {
            DenseMatrix<double> matrix = DenseMatrix.Random<double>(3, 3);

            if (verbose)
            {
                Debug.WriteLine("A");
                matrix.Print();

                DenseVector<double> v1 = matrix.Vectorize(false);
                Debug.WriteLine("v1");
                v1.Print();

                DenseVector<double> v2 = matrix.Vectorize(true);
                Debug.WriteLine("v2");
                v2.Print();
            }
        }

        public static void OuterProductTest(bool verbose = false)
        {
            DenseMatrix<double> A = DenseMatrix.Random<double>(3, 3);
            DenseVector<double> v = DenseVector.Random<double>(3);
            DenseTensor<double> T = A.TensorProduct(v);

            if (verbose)
            {
                Debug.WriteLine("A");
                A.Print();

                Debug.WriteLine("v");
                v.Print();

                Debug.WriteLine("T");
                T.Print();
            }
        }
        public static void CPDecompositionTest(bool verbose = false)
        {
            int m = 100, n = 80;
            bool rank1 = false;

            DenseMatrix<double> A;
            if (rank1)
            {
                DenseVector<double> u = DenseVector.Random<double>(m), v = DenseVector.Random<double>(n);
                A = u.OuterProduct(v);
            }
            else
            {
                // Test an "approximately" rank-1 matrix.
                DenseVector<double> u = DenseVector.Random<double>(m), v = DenseVector.Random<double>(n);
                A = u.OuterProduct(v);
                A = A.Add(DenseMatrix.Random<double>(m, n).Multiply(0.01));
            }
            
            A.CanonicalPolyadicDecompose(out DenseVector<double> U, out DenseVector<double> V);

            if (rank1)
            {
                Debug.Assert(A.ApproximatelyEquals(U.OuterProduct(V)));
            }
        }

        public static void RandomSparseMatrixCreationTest(bool verbose = false)
        {
            if (verbose)
            {
                DenseMatrix<double> random = DenseMatrix.RandomSparse<double>(100, 100, 0.7);
                random.Print();
            }
        }
        public static void RowAdditionTest(bool verbose = false)
        {
            if (verbose)
            {
                DenseMatrix<double> A = DenseMatrix.Random<double>(10, 10);

                Debug.WriteLine("Original matrix");
                A.Print();

                double[] vect = DenseVector.Random<double>(10).Values;
                Debug.WriteLine("Adding to row 0");
                A.InsertRow(0, vect).Print();

                Debug.WriteLine("Adding to row 10");
                A.InsertRow(10, vect).Print();

                Debug.WriteLine("Adding to column 0");
                A.InsertColumn(0, vect).Print();

                Debug.WriteLine("Adding to column 10");
                A.InsertColumn(10, vect).Print();
            }
        }

        public static void TriangularSolve(bool verbose = false)
        {
            int dim = Rand(10, 20);
            DenseMatrix<double> L = DenseMatrix.RandomTriangular<double>(dim, dim, false);
            DenseMatrix<double> U = DenseMatrix.RandomTriangular<double>(dim, dim, true);
            DenseVector<double> b = DenseVector.Random<double>(dim);
            DenseVector<double> x = new DenseVector<double>(dim);

            L.SolveTriangular(b.Values, x.Values, BLAS.Double, true, false);
            if (TestUtil.AssertVectorsEqual(b, L * x, (u, v) => Util.ApproximatelyEquals(u, v)) && verbose)
            {
                Debug.WriteLine("[success]\tDenseMatrixTests.TriangularSolve/lower/untransposed passed");
            }

            L.SolveTriangular(b.Values, x.Values, BLAS.Double, true, true);
            if (TestUtil.AssertVectorsEqual(b, L.Transpose() * x, (u, v) => Util.ApproximatelyEquals(u, v)) && verbose)
            {
                Debug.WriteLine("[success]\tDenseMatrixTests.TriangularSolve/lower/transposed passed");
            }

            U.SolveTriangular(b.Values, x.Values, BLAS.Double, false, false);
            if (TestUtil.AssertVectorsEqual(b, U * x, (u, v) => Util.ApproximatelyEquals(u, v)) && verbose)
            {
                Debug.WriteLine("[success]\tDenseMatrixTests.TriangularSolve/upper/untransposed passed");
            }

            U.SolveTriangular(b.Values, x.Values, BLAS.Double, false, true);
            if (TestUtil.AssertVectorsEqual(b, U.Transpose() * x, (u, v) => Util.ApproximatelyEquals(u, v)) && verbose)
            {
                Debug.WriteLine("[success]\tDenseMatrixTests.TriangularSolve/upper/transposed passed");
            }
        }
        private static void QR2_HouseholderTest(bool verbose)
        {
            IDenseExplicitQR<double> algo = new HouseholderDenseQR<double>(DefaultProviders.Double, new NativeDoubleProvider());

            int n = TestUtil.Random(10, 20), m = n + TestUtil.Random(0, 5);
            DenseMatrix<double> A = DenseMatrix.Random<double>(m, n);
            QR<double> qr = A.QR(algo, false);

            DenseMatrix<double> Q = qr.Q as DenseMatrix<double>;
            DenseMatrix<double> R = qr.R as DenseMatrix<double>;

            Debug.Assert(Q.Multiply(R).ApproximatelyEquals(A, 1e-6));
            Debug.Assert(Q.IsOrthogonal(1e-6));
            Debug.Assert(R.IsUpperTriangular(1e-6));

            if (verbose)
            {
                Debug.WriteLine("[success]\tDenseMatrixTests.QR2_HouseholderTest passed");
            }
        }
        private static void QR2_BlockHouseholderTest(bool verbose)
        {
            IDenseExplicitQR<double> algo = new BlockHouseholderDenseQR<double>(4);
            DenseMatrix<double> A = DenseMatrix.Random<double>(32, 24);
            QR<double> qr = A.QR(algo, false);

            DenseMatrix<double> Q = qr.Q as DenseMatrix<double>;
            DenseMatrix<double> R = qr.R as DenseMatrix<double>;

            var algo2 = new BlockHouseholderQRDecompositionAlgorithm(4);
            algo2.QRDecompose(A, out DenseMatrix<double> Q1, out DenseMatrix<double> R1, false, false);

            Debug.Assert(Q.Multiply(R).ApproximatelyEquals(A, 1e-6));
            Debug.Assert(Q.IsOrthogonal(1e-6));
            Debug.Assert(R.IsUpperTriangular(1e-6));

            if (verbose)
            {
                Debug.WriteLine("[success]\tDenseMatrixTests.QR2_BlockHouseholderTest passed");
            }
        }
        private static void QR2_GramSchmidtTest(bool verbose)
        {
            IDenseExplicitQR<double> algo = new GramSchmidtDenseQR<double>();

            int m = TestUtil.Random(10, 15), n = m - TestUtil.Random(0, 5);
            DenseMatrix<double> A = DenseMatrix.Random<double>(m, m);
            QR<double> qr = A.QR(algo, false);

            DenseMatrix<double> Q = qr.Q as DenseMatrix<double>;
            DenseMatrix<double> R = qr.R as DenseMatrix<double>;

            Debug.Assert(Q.Multiply(R).ApproximatelyEquals(A, 1e-3));
            Debug.Assert(Q.IsOrthogonal(1e-3));
            Debug.Assert(R.IsUpperTriangular(1e-3));

            if (verbose)
            {
                Debug.WriteLine("[success]\tDenseMatrixTests.QR2_GramSchmidtTest passed");
            }
        }
        private static void QR2_GivensRotationTest(bool verbose)
        {
            IDenseExplicitQR<double> algo = new ExplicitGivensRotationDenseQR<double>(BLAS.Double);

            int m = TestUtil.Random(10, 20), n = m - TestUtil.Random(0, 5);
            DenseMatrix<double> A = DenseMatrix.Random<double>(m, n);
            QR<double> qr = A.QR(algo, false);

            DenseMatrix<double> Q = qr.Q as DenseMatrix<double>;
            DenseMatrix<double> R = qr.R as DenseMatrix<double>;

            Debug.Assert(Q.Multiply(R).ApproximatelyEquals(A, 1e-3));
            Debug.Assert(Q.IsOrthogonal(1e-3));
            Debug.Assert(R.IsUpperTriangular(1e-3));

            if (verbose)
            {
                Debug.WriteLine("[success]\tDenseMatrixTests.QR2_GivensRotationTest passed");
            }
        }
        private static void QR2_HouseholderSolve(bool verbose)
        {
            IDenseExplicitQR<double> algo = new HouseholderDenseQR<double>(DefaultProviders.Double, new NativeDoubleProvider());

            int m = TestUtil.Random(10, 20);
            DenseMatrix<double> A = DenseMatrix.Random<double>(m, m);
            QR<double> qr = A.QR(algo, false);

            DenseVector<double> b = DenseVector.Random<double>(m);
            DenseVector<double> x = qr.Solve(b);

            if (TestUtil.AssertVectorsEqual(A * x, b, (u, v) => Util.ApproximatelyEquals(u, v, 1e-6)) && verbose)
            {
                Debug.WriteLine("[success]\tDenseMatrixTests.QR2_HouseholderSolve passed");
            }
        }
        private static void Chol2_BlockTest(bool verbose)
        {
            NativeDoubleProvider blas = new NativeDoubleProvider();
            BlockDenseCholesky<double> chol2 = new BlockDenseCholesky<double>(new NaiveMatrixMultiplication<double>(blas), 4, 2);
            DenseMatrix<double> matrix = DenseMatrix.RandomSPD<double>(23);

            Cholesky<double> chol = chol2.Decompose(matrix.Copy());
            DenseMatrix<double> L = (chol.Factor as DenseMatrix<double>).Lower();

            if (TestUtil.AssertMatrixEqual(L * L.Transpose(), matrix, (u, v) => Util.ApproximatelyEquals(u, v, 1e-6)) && verbose)
            {
                Debug.WriteLine("[success]\tDenseMatrixTests.Chol2_BlockTest passed");
            }
        }
        private static void TriangularSolve_Strassen(bool verbose)
        {
            var baseAlgo = new NaiveTriangularSolver<double>(BLAS.Double);

            var strassen = new VariableSizeStrassenMultiplication<double>(
                BLAS.Double, BLAS.Double, new NaiveMatrixMultiplication<double>(BLAS.Double), 4, 4, 4, true, 2);

            var tsolve = new StrassenTriangularSolver<double>(
                baseAlgo, strassen, BLAS.Double, BLAS.Double, 4, 16);

            DenseMatrix<double> L = DenseMatrix.RandomTriangular<double>(16, 16, false);
            DenseMatrix<double> Bt = DenseMatrix.Random<double>(16, 16);
            DenseMatrix<double> Xt = Bt.Copy();

            tsolve.Solve(L.Values, Xt.Values, 0, 0, 0, 0, 16, 16, true, false, true);

            if (TestUtil.AssertMatrixEqual(L * Xt.Transpose(), Bt.Transpose(), (u,v) => Util.ApproximatelyEquals(u, v, 1e-3)) && verbose)
            {
                Debug.WriteLine("[success]\tDenseMatrixTests.TriangularSolve_Strassen passed");
            }
        }
        private static void TriangularSolve_Strassen2(bool verbose)
        {
            var baseAlgo = new NaiveTriangularSolver<double>(BLAS.Double);

            var strassen = new VariableSizeStrassenMultiplication<double>(
                BLAS.Double, BLAS.Double, new NaiveMatrixMultiplication<double>(BLAS.Double), 4, 4, 4, true, 2);

            var tsolve = new StrassenRecursiveTriangularSolver<double>(
                baseAlgo, strassen, BLAS.Double, BLAS.Double, 4, 16);

            DenseMatrix<double> L = DenseMatrix.RandomTriangular<double>(16, 16, false);
            DenseMatrix<double> Bt = DenseMatrix.Random<double>(16, 16);
            DenseMatrix<double> Xt = Bt.Copy();

            tsolve.Solve(L.Values, Xt.Values, 0, 0, 0, 0, 16, 16, true, false, true);

            if (TestUtil.AssertMatrixEqual(L * Xt.Transpose(), Bt.Transpose(), (u, v) => Util.ApproximatelyEquals(u, v, 1e-3)) && verbose)
            {
                Debug.WriteLine("[success]\tDenseMatrixTests.TriangularSolve_Strassen2 passed");
            }
        }
    }
}
