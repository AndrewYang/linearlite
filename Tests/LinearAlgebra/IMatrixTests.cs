﻿using LinearNet.Structs;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace LinearNet.Tests.LinearAlgebra
{
    public static class IMatrixTests
    {
        public static void RunAll(bool verbose)
        {
            List<Matrix<double>> matrices = GetMatrices(TestUtil.Random(10, 20), TestUtil.Random(10, 20));
            foreach (Matrix<double> matrix in matrices)
            {
                TestSelection(matrix, verbose);
                TestSelectionSingle(matrix, verbose);
                TestSelectionRowVectorSingle(matrix, verbose);
            }
        }

        private static List<Matrix<double>> GetMatrices(int rows, int cols)
        {
            return new List<Matrix<double>>()
            {
                BandMatrix.Random<double>(rows, cols, Math.Min(rows, cols) / 2, Math.Min(rows, cols) / 2),
                COOSparseMatrix.Random<double>(rows, cols, 0.5),
                CSCSparseMatrix.Random<double>(rows, cols, 0.5),
                CSRSparseMatrix.Random<double>(rows, cols, 0.5),
                DenseMatrix.Random<double>(rows, cols),
                DiagonalMatrix.Random<double>(rows, cols),
                DOKSparseMatrix.Random<double>(rows, cols, 0.5),
                //MCSRSparseMatrix.Random<double>(rows, 1, 0.5),
                //new SKYSparseMatrix<double>(DenseMatrix.RandomTriangular<double>(rows, rows, false), SKYType.LOWER)
            };
        }

        private static void TestSelection<T>(Matrix<T> matrix, bool verbose) where T : new()
        {
            // Test range
            Random rand = new Random();
            int r1 = rand.Next(matrix.Rows), r2 = rand.Next(matrix.Rows),
                c1 = rand.Next(matrix.Columns), c2 = rand.Next(matrix.Columns);
            if (r1 > r2) Util.Swap(ref r1, ref r2);
            if (c1 > c2) Util.Swap(ref c1, ref c2);
            Matrix<T> m1 = matrix[$"{r1}:{r2}", $"{c1}:{c2}"];

            Debug.Assert(m1.Rows == r2 - r1 + 1);
            Debug.Assert(m1.Columns == c2 - c1 + 1);
            for (int r = 0; r < m1.Rows; ++r)
            {
                for (int c = 0; c < m1.Columns; ++c)
                {
                    Debug.Assert(m1[r, c].Equals(matrix[r + r1, c + c1]), $"{m1[r, c]} != {matrix[r + r1, c + c1]}");
                }
            }

            if (verbose)
            {
                Debug.WriteLine($"[success]\tIMatrixTests.TestSelection/{matrix.GetType().Name} passed");
            }
        }
        private static void TestSelectionSingle<T>(Matrix<T> matrix, bool verbose) where T : new()
        {
            Random r = new Random();
            int rows = matrix.Rows, cols = matrix.Columns;
            int[] rs = { r.Next(rows), r.Next(rows), r.Next(rows) };
            int[] cs = { r.Next(cols), r.Next(cols) };
            Matrix<T> m1 = matrix[string.Join(",", rs), string.Join(",", cs)];

            Debug.Assert(m1.Rows == rs.Length, "Row dimensions don't match");
            Debug.Assert(m1.Columns == cs.Length, "Column dimensions don't match");

            for (int i = 0; i < rs.Length; ++i)
            {
                for (int j = 0; j < cs.Length; ++j)
                {
                    Debug.Assert(m1[i, j].Equals(matrix[rs[i], cs[j]]), $"{m1[i, j]} != {matrix[rs[i], cs[j]]}");
                }
            }

            //if (matrix.StorageType == MatrixStorageType.COO)
            //{
            //    Debug.WriteLine("Selected rs");
            //    rs.Print();
            //    Debug.WriteLine("selected cs");
            //    cs.Print();
            //    Debug.WriteLine("selection?");
            //    m1.Print();
            //    Debug.WriteLine("original");
            //    matrix.Print();
            //}

            if (verbose)
            {
                Debug.WriteLine($"[success]\tIMatrixTests.TestSelectionSingle/{matrix.GetType().Name} passed");
            }
        }
        private static void TestSelectionRowVectorSingle<T>(Matrix<T> matrix, bool verbose) where T : new()
        {
            Random r = new Random();
            int rows = matrix.Rows, cols = matrix.Columns, row = r.Next(rows);
            int[] cs = { r.Next(cols), r.Next(cols), r.Next(cols) };
            Vector<T> m1 = matrix[row, string.Join(",", cs)];

            Debug.Assert(m1.Dimension == cs.Length, "Column dimensions don't match");

            for (int j = 0; j < cs.Length; ++j)
            {
                Debug.Assert(m1[j].Equals(matrix[row, cs[j]]), $"{m1[j]} != {matrix[row, cs[j]]}");
            }

            if (verbose)
            {
                Debug.WriteLine($"[success]\tIMatrixTests.TestSelectionRowVectorSingle/{matrix.GetType().Name} passed");
            }
        }
    }
}
