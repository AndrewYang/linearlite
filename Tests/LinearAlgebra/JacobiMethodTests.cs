﻿using LinearNet.Solvers;
using LinearNet.Solvers.Linear;
using LinearNet.Structs;
using System;
using System.Diagnostics;
using LinearNet.Matrices;
using LinearNet.Vectors;

namespace LinearNet.Tests.LinearAlgebra
{
    public static class JacobiMethodTests
    {
        public static void RunAll(bool verbose)
        {
            Double(verbose);
        }

        private static int Random(int min, int max)
        {
            return new Random().Next(max - min) + min;
        }
        private static void Double(bool verbose = false)
        {
            DenseMatrix<double> A = DenseMatrix.Random<double>(100, 100);

            Random rand = new Random();
            // Multiply diagonal 
            for (int i = 0; i < 100; ++i)
            {
                A[i, i] = (rand.Next(2) == 0 ? -1 : 1) * Random(20, 30);
            }

            DenseVector<double> b = DenseVector.Random<double>(100);
            DenseVector<double> x = new DenseVector<double>(100);

            JacobiSolver solver = new JacobiSolver(10000);
            Solution<DenseVector<double>> soln = solver.Solve(new CSRSparseMatrix<double>(A), x, b);

            double norm = A.Multiply(x).Subtract(b).Norm(2);
            if (norm < 1e-6)
            {
                if (verbose)
                {
                    Debug.WriteLine($"[success]\tJacobiMethodTests.Double passed with ||Ax - b|| = { norm }");
                }
            }
            else
            {
                Debug.WriteLine($"[failure]\tJacobiMethodTests.Double exited with '{ soln.ExitCode }', ||Ax - b|| = { norm }");
            }
        }
    }
}
