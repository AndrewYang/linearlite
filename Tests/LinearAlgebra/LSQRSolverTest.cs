﻿using LinearNet.Providers.LinearAlgebra;
using LinearNet.Solvers;
using LinearNet.Solvers.Linear;
using LinearNet.Structs;
using LinearNet.Vectors;
using System.Diagnostics;

namespace LinearNet.Tests.LinearAlgebra
{
    public static class LSQRSolverTest
    {
        public static void RunAll(bool verbose)
        {
            Double();
        }
        private static void Double()
        {
            LSQRSolver solver = new LSQRSolver(1000);

            // Create a sparse symmetric, positive definite coefficient matrix 
            int m = 100, n = 100;
            CSRSparseMatrix<double> A = CSRSparseMatrix.Random<double>(m, n, 0.5);
            DenseVector<double> b = DenseVector.Random<double>(m);
            DenseVector<double> x = new DenseVector<double>(n);

            Debug.WriteLine("A density: " + A.Density);

            Stopwatch sw = new Stopwatch();
            sw.Start();
            Solution<DenseVector<double>> soln = solver.Solve(A, x, b, BLAS.Double);
            sw.Stop();
            Debug.WriteLine(sw.ElapsedMilliseconds + "ms");

            var blas = DefaultProviders.Double;
            Debug.WriteLine("[success]\tLSQRSolverTest.Double passed with ||Ax - b|| = " + A.Multiply(soln.Result, blas).Subtract(b).Norm());
        }
    }
}
