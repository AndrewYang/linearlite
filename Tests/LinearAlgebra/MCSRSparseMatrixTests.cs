﻿using LinearNet.Matrices;
using LinearNet.Providers.LinearAlgebra;
using LinearNet.Structs;
using System;
using System.Diagnostics;

namespace LinearNet.Tests.LinearAlgebra
{
    public static class MCSRSparseMatrixTests
    {
        public static void RunAll(bool verbose)
        {
            CreateDOK(verbose);
            CreateCOO(verbose);
            CreateCSR(verbose);
            CreateCSC(verbose);
            CreateDense(verbose);
            CreateBand(verbose);

            Add(verbose);
            Subtract(verbose);
            MultiplyScalar(verbose);
            MultiplyMatrix(verbose);
            MultiplyVector(verbose);
            MultiplySparseVector(verbose);

            Transpose(verbose);
            SolveTriangular(verbose);
        }


        private static bool AssertMatrixEqual<T>(Matrix<T> A, Matrix<T> B, bool verbose) where T : new()
        {
            if (A.Rows != B.Rows)
            {
                Debug.Assert(false, "Matrix rows not equal");
                return false;
            }

            if (A.Columns != B.Columns)
            {
                Debug.Assert(false, "Matrix columns not equal");
                return false;
            }

            for (int i = 0; i < A.Rows; ++i)
            {
                for (int j = 0; j < A.Columns; ++j)
                {
                    if (!A[i, j].Equals(B[i, j]))
                    {
                        Debug.Assert(false, $"Matrix elements @[{i}, {j}] not equal: " + A[i, j] + " vs " + B[i, j]);
                        return false;
                    }
                }
            }

            return true;
        }
        private static int Random(int min, int max)
        {
            return new Random().Next(max - min) + min;
        }

        private static void CreateDOK(bool verbose = false)
        {
            DOKSparseMatrix<double> dok = DOKSparseMatrix.Random<double>(10, 10, 0.5);
            MCSRSparseMatrix<double> mcsr = new MCSRSparseMatrix<double>(dok);

            if (AssertMatrixEqual(dok, mcsr, verbose))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tMCSRSparseMatrixTests.CreateDOK passed.");
                }
            }
        }
        private static void CreateCOO(bool verbose = false)
        {
            COOSparseMatrix<double> coo = COOSparseMatrix.Random<double>(10, 10, 0.5);
            MCSRSparseMatrix<double> mcsr = new MCSRSparseMatrix<double>(coo);

            if (AssertMatrixEqual(coo, mcsr, verbose))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tMCSRSparseMatrixTests.CreateCOO passed.");
                }
            }
        }
        private static void CreateCSR(bool verbose = false)
        {
            CSRSparseMatrix<double> csr = CSRSparseMatrix.Random<double>(10, 10, 0.5);
            MCSRSparseMatrix<double> mcsr = new MCSRSparseMatrix<double>(csr);

            if (AssertMatrixEqual(csr, mcsr, verbose))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tMCSRSparseMatrixTests.CreateCSR passed.");
                }
            }
        }
        private static void CreateDense(bool verbose = false)
        {
            Random r = new Random();
            DenseMatrix<int> dense = DenseMatrix.Random(10, 10, () => r.Next(3) - 1);
            MCSRSparseMatrix<int> mcsr = new MCSRSparseMatrix<int>(dense);

            
            if (AssertMatrixEqual(dense, mcsr, verbose))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tMCSRSparseMatrixTests.CreateDense passed.");
                }
            }
        }
        private static void CreateBand(bool verbose = false)
        {
            Random rand = new Random();
            int dim = Random(10, 20);
            BandMatrix<int> band = BandMatrix.Random(dim, dim, 5, 5, () => rand.Next(3) - 1);
            MCSRSparseMatrix<int> mcsr = new MCSRSparseMatrix<int>(band);

            if (AssertMatrixEqual(band, mcsr, verbose))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tMCSRSparseMatrixTests.CreateBand passed.");
                }
            }
        }
        private static void CreateCSC(bool verbose = false)
        {
            CSCSparseMatrix<double> csr = CSCSparseMatrix.Random<double>(10, 10, 0.5);
            MCSRSparseMatrix<double> mcsr = new MCSRSparseMatrix<double>(csr);

            if (AssertMatrixEqual(csr, mcsr, verbose))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tMCSRSparseMatrixTests.CreateCSC passed.");
                }
            }
        }

        private static void Add(bool verbose = false)
        {
            int dim = Random(10, 20);

            Random rand = new Random();
            DenseMatrix<int> A = DenseMatrix.Random(dim, dim, () => rand.Next(3) - 1);
            DenseMatrix<int> B = DenseMatrix.Random(dim, dim, () => rand.Next(3) - 1);

            MCSRSparseMatrix<int> _A = new MCSRSparseMatrix<int>(A);
            MCSRSparseMatrix<int> _B = new MCSRSparseMatrix<int>(B);

            if (AssertMatrixEqual(A.Add(B), _A.Add(_B, DefaultProviders.Int32), verbose))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tMCSRSparseMatrixTests.Add passed.");
                }
            }
        }
        private static void Subtract(bool verbose = false)
        {
            int dim = Random(10, 20);

            Random rand = new Random();
            DenseMatrix<int> A = DenseMatrix.Random(dim, dim, () => rand.Next(3) - 1);
            DenseMatrix<int> B = DenseMatrix.Random(dim, dim, () => rand.Next(3) - 1);

            MCSRSparseMatrix<int> _A = new MCSRSparseMatrix<int>(A);
            MCSRSparseMatrix<int> _B = new MCSRSparseMatrix<int>(B);

            if (AssertMatrixEqual(A.Subtract(B), _A.Subtract(_B, DefaultProviders.Int32), verbose))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tMCSRSparseMatrixTests.Subtract passed.");
                }
            }
        }
        private static void MultiplyScalar(bool verbose = false)
        {
            int dim = Random(10, 20);

            Random rand = new Random();
            MCSRSparseMatrix<double> _A = MCSRSparseMatrix.Random<double>(dim, 1.0, 0.5);
            DenseMatrix<double> A = new DenseMatrix<double>(_A);

            int s = Random(0, 10);
            if (AssertMatrixEqual(A.Multiply(s), _A.Multiply(s, DefaultProviders.Double), verbose))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tMCSRSparseMatrixTests.MultiplyScalar passed.");
                }
            }
        }
        private static void MultiplyMatrix(bool verbose = false)
        {
            int dim = TestUtil.Random(10, 20);
            MCSRSparseMatrix<double> A = MCSRSparseMatrix.Random<double>(dim, 0.9, 0.5),
                B = MCSRSparseMatrix.Random<double>(dim, 0.8, 0.5);

            DenseMatrix<double> _A = new DenseMatrix<double>(A),
                _B = new DenseMatrix<double>(B);

            MCSRSparseMatrix<double> AB = A * B;
            DenseMatrix<double> _AB = _A * _B;

            if (TestUtil.AssertMatrixEqual(AB, _AB, (u, v) => Util.ApproximatelyEquals(u, v)))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tMCSRSparseMatrixTests.MultiplyMatrix passed.");
                }
            }
        }
        private static void MultiplyVector(bool verbose = false)
        {
            int dim = Random(10, 20);

            MCSRSparseMatrix<double> mcsr = MCSRSparseMatrix.Random<double>(dim, 0.9, 0.5);
            DenseMatrix<double> dense = new DenseMatrix<double>(mcsr);

            DenseVector<double> vect = DenseVector.Random<double>(dim);

            if (TestUtil.AssertVectorsEqual(mcsr * vect, dense * vect, (u, v) => Util.ApproximatelyEquals(u, v)) && verbose)
            {
                Debug.WriteLine("[success]\tMCSRSparseMatrixTests.MultiplyVector passed");
            }
        }
        private static void MultiplySparseVector(bool verbose = false)
        {
            int dim = Random(100, 200);
            MCSRSparseMatrix<double> mcsr = MCSRSparseMatrix.Random<double>(dim, 0.9, 0.5);
            DenseMatrix<double> dense = new DenseMatrix<double>(mcsr);

            SparseVector<double> vect = SparseVector.Random<double>(dim, 0.5);
            if (TestUtil.AssertVectorsEqual(mcsr * vect, dense * new DenseVector<double>(vect), (u, v) => Util.ApproximatelyEquals(u, v)) && verbose)
            {
                Debug.WriteLine("[success]\tMCSRSparseMatrixTests.MultiplySparseVector passed");
            }
        }

        private static void Transpose(bool verbose = false)
        {
            int dim = Random(10, 20);
            Random r = new Random();
            DenseMatrix<int> dense = DenseMatrix.Random(dim, dim, () => r.Next(3) - 1);
            MCSRSparseMatrix<int> mcsr = new MCSRSparseMatrix<int>(dense);

            if (TestUtil.AssertMatrixEqual(dense.Transpose(), mcsr.Transpose(), verbose) && verbose)
            {
                Debug.WriteLine("[success]\tMCSRSparseMatrixTests.Transpose passed");
            }
        }
        private static void SolveTriangular(bool verbose = false)
        {
            int dim = Random(10, 20);
            MCSRSparseMatrix<double> L = new MCSRSparseMatrix<double>(DenseMatrix.RandomTriangular<double>(dim, dim, false));
            MCSRSparseMatrix<double> U = new MCSRSparseMatrix<double>(DenseMatrix.RandomTriangular<double>(dim, dim, true));
            DenseVector<double> b = DenseVector.Random<double>(dim);
            DenseVector<double> x = new DenseVector<double>(dim);

            L.SolveTriangular(b.Values, x.Values, BLAS.Double, true, false);
            if (TestUtil.AssertVectorsEqual(b, L * x, (u, v) => Util.ApproximatelyEquals(u, v)) && verbose)
            {
                Debug.WriteLine("[success]\tMCSRSparseMatrixTests.SolveTriangular/lower/untransposed passed");
            }

            L.SolveTriangular(b.Values, x.Values, BLAS.Double, true, true);
            if (TestUtil.AssertVectorsEqual(b, L.Transpose() * x, (u, v) => Util.ApproximatelyEquals(u, v)) && verbose)
            {
                Debug.WriteLine("[success]\tMCSRSparseMatrixTests.SolveTriangular/lower/transposed passed");
            }

            U.SolveTriangular(b.Values, x.Values, BLAS.Double, false, false);
            if (TestUtil.AssertVectorsEqual(b, U * x, (u, v) => Util.ApproximatelyEquals(u, v)) && verbose)
            {
                Debug.WriteLine("[success]\tMCSRSparseMatrixTests.SolveTriangular/upper/untransposed passed");
            }

            U.SolveTriangular(b.Values, x.Values, BLAS.Double, false, true);
            if (TestUtil.AssertVectorsEqual(b, U.Transpose() * x, (u, v) => Util.ApproximatelyEquals(u, v)) && verbose)
            {
                Debug.WriteLine("[success]\tMCSRSparseMatrixTests.SolveTriangular/upper/transposed passed");
            }
        }
    }
}
