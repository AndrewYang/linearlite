﻿using LinearNet.Matrices;
using LinearNet.Providers.LinearAlgebra;
using LinearNet.Solvers;
using LinearNet.Solvers.Linear;
using LinearNet.Structs;
using LinearNet.Vectors;
using System.Diagnostics;

namespace LinearNet.Tests.LinearAlgebra
{
    public static class MinimualResidualSolverTests
    {
        public static void RunAll(bool verbose)
        {
            Double(verbose);
        }

        private static void Double(bool verbose)
        {
            DenseMatrix<double> A = DenseMatrix.RandomSPD<double>(100);
            DenseVector<double> b = DenseVector.Random<double>(100);
            DenseVector<double> x = new DenseVector<double>(100);

            MinimalResidualSolver<double> solver = new MinimalResidualSolver<double>(BLAS.Double, 10000, 1e-20);
            Solution<DenseVector<double>> soln = solver.Solve(new CSRSparseMatrix<double>(A), x, b);

            double norm = A.Multiply(x).Subtract(b).Norm(2);

            // Convergence is so slow with the MR method that the best we can do is test residual norm against 0.5
            if (norm < 5e-1)
            {
                if (verbose)
                {
                    Debug.WriteLine($"[success]\tMinimualResidualSolverTests.Double passed with ||Ax - b|| = { norm }");
                }
            }
            else
            {
                Debug.WriteLine($"[failure]\tMinimualResidualSolverTests.Double exited with '{ soln.ExitCode }', ||Ax - b|| = { norm }");
            }
        }
    }
}
