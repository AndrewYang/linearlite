﻿using LinearNet.Matrices;
using LinearNet.Structs;
using System.Diagnostics;

namespace LinearNet.Tests
{
    public static class PermutationMatrixTests
    {
        public static void PMatrixSwapRowsTest(bool verbose = false)
        {
            PermutationMatrix P = PermutationMatrix.Random(10);

            if (verbose)
            {
                Debug.WriteLine("Permutation matrix");
                P.Print();
                P.ToDenseMatrix().Print();
            }

            P.SwapRows(1, 2);

            if (verbose)
            {
                Debug.WriteLine("Permutation matrix after swapping rows 1, 2");
                P.Print();
            }

            P.SwapColumns(4, 6);

            if (verbose)
            {
                Debug.WriteLine("Permutation matrix after swapping columns 4 and 6");
                P.Print();

                Debug.WriteLine("Permutation matrix[1]");
                P[1].Print();
            }

            DenseMatrix<double> B = DenseMatrix.Random<double>(10, 10);

            if (verbose)
            {
                Debug.WriteLine("B");
                B.Print();

                Debug.WriteLine("PB");
                P.Multiply(B).Print();
            }
            Debug.Assert(P.Multiply(B).ApproximatelyEquals(P.ToDenseMatrix().ToDouble().Multiply(B)));

            if (verbose)
            {
                Debug.WriteLine("P^T");
                P.Transpose().Print();

                Debug.WriteLine("PP^T");
                P.Multiply(P.Transpose()).Print();
            }

            Debug.Assert(P.Multiply(P.Transpose()).IsIdentity);
            Debug.Assert(P.Transpose().Multiply(P).IsIdentity);
        }
    }
}
