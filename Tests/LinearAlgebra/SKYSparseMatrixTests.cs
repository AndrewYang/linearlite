﻿using LinearNet.Matrices;
using LinearNet.Providers.LinearAlgebra;
using LinearNet.Structs;
using System;
using System.Diagnostics;

namespace LinearNet.Tests.LinearAlgebra
{
    public static class SKYSparseMatrixTests
    {
        public static void RunAll(bool verbose = true)
        {
            CreateFromDense(verbose);
            CreateFromDOK(verbose);
            CreateFromCOO(verbose);
            CreateFromBand(verbose);
            CreateFromCSR(verbose);

            RowLower(verbose);
            RowUpper(verbose);
            RowSym(verbose);
            ColumnLower(verbose);
            ColumnUpper(verbose);
            ColumnSym(verbose);
            ClearRowLower(verbose);
            ClearRowUpper(verbose);
            ClearColumnLower(verbose);
            ClearColumnUpper(verbose);
            RowSumLower(verbose);
            RowSumUpper(verbose);
            RowSumSym(verbose);
            ColumnSumLower(verbose);
            ColumnSumUpper(verbose);
            ColumnSumSym(verbose);

            DirectSumLower(verbose);
            DirectSumSym(verbose);
            Elementwise(verbose);
            Pointwise(verbose);
            VectorizeLower(verbose);
            VectorizeUpper(verbose);
            VectorizeSym(verbose);
            RemoveAll(verbose);

            Add(verbose);
            Subtract(verbose);
            MultiplyVectorDenseLower(verbose);
            MultiplyVectorDenseUpper(verbose);
            MultiplyVectorDenseSym(verbose);
            MultiplyTransVectorDenseLower(verbose);
            MultiplyTransVectorDenseUpper(verbose);
            MultiplyTransVectorDenseSym(verbose);
            MultiplyLowerLower(verbose);

            SolveLowerTriangular(verbose);
            SolveUpperTriangular(verbose);
            SolveLowerTriangularBLAS1(verbose);
            SolveUpperTriangularBLAS1(verbose);
            CholUp(verbose);
            CholUpBlAS1(verbose);
        }

        private static int Random(int min, int max)
        {
            return new Random().Next(max - min) + min;
        }
        private static DenseMatrix<int> RandomTriangularInt(int dim, bool upper)
        {
            Random r = new Random();
            return DenseMatrix.RandomTriangular(dim, dim, upper, () => r.Next(3) - 1);
        }
        public static void CreateFromDense(bool verbose)
        {
            Random rand = new Random();

            int n = Random(5, 10);
            DenseMatrix<int> dense = DenseMatrix.Random(n, n, () => rand.Next(3) - 1);
            dense = dense.Transpose() * dense;

            SKYSparseMatrix<int> sky = new SKYSparseMatrix<int>(dense, SKYType.SYMMETRIC);

            if (TestUtil.AssertMatrixEqual(dense, sky, verbose) && verbose)
            {
                Debug.WriteLine("[success]\tSKYSparseMatrixTests.CreateFromDense passed");
            }
        }
        private static void CreateFromDOK(bool verbose)
        {
            DOKSparseMatrix<double> dok = DOKSparseMatrix.Random<double>(TestUtil.Random(10, 20), TestUtil.Random(10, 20), 0.1);
            dok = dok.Transpose() * dok;

            SKYSparseMatrix<double> sky = new SKYSparseMatrix<double>(dok);
            if (TestUtil.AssertMatrixEqual(dok, sky, verbose) && verbose)
            {
                Debug.WriteLine("[success]\tSKYSparseMatrixTests.CreateFromDOK passed");
            }
        }
        private static void CreateFromCOO(bool verbose)
        {
            COOSparseMatrix<double> coo = COOSparseMatrix.Random<double>(TestUtil.Random(10, 20), TestUtil.Random(10, 20), 0.1);
            coo = coo.Transpose() * coo;

            SKYSparseMatrix<double> sky = new SKYSparseMatrix<double>(coo);

            if (TestUtil.AssertMatrixEqual(coo, sky, verbose) && verbose)
            {
                Debug.WriteLine("[success]\tSKYSparseMatrixTests.CreateFromCOO passed");
            }
        }
        private static void CreateFromBand(bool verbose)
        {
            int dim = TestUtil.Random(10, 20);
            Random rand = new Random();
            BandMatrix<int> band = BandMatrix.Random(dim, dim, 3, 3, () => rand.Next(3) - 1);
            band = band.Transpose() * band;

            SKYSparseMatrix<int> sky = new SKYSparseMatrix<int>(band, SKYType.SYMMETRIC);
            if (TestUtil.AssertMatrixEqual(band, sky, verbose) && verbose)
            {
                Debug.WriteLine("[success]\tSKYSparseMatrixTests.CreateFromBand passed");
            }
        }
        private static void CreateFromCSR(bool verbose)
        {
            int dim = TestUtil.Random(10, 20);
            Random rand = new Random();
            BandMatrix<int> band = BandMatrix.Random(dim, dim, 3, 3, () => rand.Next(3) - 1);
            band = band.Transpose() * band;

            CSRSparseMatrix<int> csr = new CSRSparseMatrix<int>(band);
            SKYSparseMatrix<int> sky = new SKYSparseMatrix<int>(csr);

            if (TestUtil.AssertMatrixEqual(csr, sky, verbose) && verbose)
            {
                Debug.WriteLine("[success]\tSKYSparseMatrixTests.CreateFromCSR passed");
            }
        }

        private static void RowLower(bool verbose)
        {
            int dim = Random(10, 20);
            Random r = new Random();
            DenseMatrix<int> dense = DenseMatrix.RandomTriangular(dim, dim, false, () => r.Next(3) - 1);
            SKYSparseMatrix<int> sky = new SKYSparseMatrix<int>(dense, SKYType.LOWER);

            int row = r.Next(dim);
            if (TestUtil.AssertVectorsEqual(dense.Row(row), sky.Row(row)) && verbose)
            {
                Debug.WriteLine("[success]\tSKYSparseMatrixTests.RowLower passed");
            }
        }
        private static void RowUpper(bool verbose)
        {
            int dim = Random(10, 20);
            Random r = new Random();
            DenseMatrix<int> dense = DenseMatrix.RandomTriangular(dim, dim, true, () => r.Next(3) - 1);
            SKYSparseMatrix<int> sky = new SKYSparseMatrix<int>(dense, SKYType.UPPER);

            int row = r.Next(dim);
            if (TestUtil.AssertVectorsEqual(dense.Row(row), sky.Row(row)) && verbose)
            {
                Debug.WriteLine("[success]\tSKYSparseMatrixTests.RowUpper passed");
            }
            else
            {
                dense.Print();
                Debug.WriteLine("Extracting row: " + row);
                dense.Row(row).Print();
                sky.Row(row).Print();
            }
        }
        private static void RowSym(bool verbose)
        {
            int dim = Random(10, 20);
            Random r = new Random();
            DenseMatrix<double> dense = DenseMatrix.RandomSPD<double>(dim);
            SKYSparseMatrix<double> sky = new SKYSparseMatrix<double>(dense, SKYType.SYMMETRIC);

            int row = r.Next(dim);
            if (TestUtil.AssertVectorsEqual(dense.Row(row), sky.Row(row)) && verbose)
            {
                Debug.WriteLine("[success]\tSKYSparseMatrixTests.RowSym passed");
            }
        }
        private static void ColumnLower(bool verbose)
        {
            int dim = Random(10, 20);
            Random r = new Random();
            DenseMatrix<int> dense = DenseMatrix.RandomTriangular(dim, dim, false, () => r.Next(3) - 1);
            SKYSparseMatrix<int> sky = new SKYSparseMatrix<int>(dense, SKYType.LOWER);

            int col = r.Next(dim);

            if (TestUtil.AssertVectorsEqual(dense.Column(col), sky.Column(col)) && verbose)
            {
                Debug.WriteLine("[success]\tSKYSparseMatrixTests.ColumnLower passed");
            }
            else
            {
                dense.Print();
                Debug.WriteLine("Extracting column: " + col);
                dense.Column(col).Print();
                sky.Column(col).Print();
            }
        }
        private static void ColumnUpper(bool verbose)
        {
            int dim = Random(10, 20);
            Random r = new Random();
            DenseMatrix<int> dense = DenseMatrix.RandomTriangular(dim, dim, true, () => r.Next(5) - 2);
            SKYSparseMatrix<int> sky = new SKYSparseMatrix<int>(dense, SKYType.UPPER);

            int col = r.Next(dim);
            if (TestUtil.AssertVectorsEqual(dense.Column(col), sky.Column(col)) && verbose)
            {
                Debug.WriteLine("[success]\tSKYSparseMatrixTests.ColumnUpper passed");
            }
        }
        private static void ColumnSym(bool verbose)
        {
            int dim = Random(10, 20);
            Random r = new Random();
            DenseMatrix<double> dense = DenseMatrix.RandomSPD<double>(dim);
            SKYSparseMatrix<double> sky = new SKYSparseMatrix<double>(dense, SKYType.SYMMETRIC);

            int col = r.Next(dim);
            if (TestUtil.AssertVectorsEqual(dense.Column(col), sky.Column(col)) && verbose)
            {
                Debug.WriteLine("[success]\tSKYSparseMatrixTests.ColumnSym passed");
            }
        }
        private static void ClearRowLower(bool verbose)
        {
            int dim = Random(10, 20);
            DenseMatrix<double> dense = DenseMatrix.RandomTriangular<double>(dim, dim, false);
            SKYSparseMatrix<double> sky = new SKYSparseMatrix<double>(dense, SKYType.LOWER);

            int r = Random(0, dim);
            dense.ClearRow(r);
            sky.ClearRow(r, true);

            if (TestUtil.AssertMatrixEqual(dense, sky, verbose) && verbose)
            {
                Debug.WriteLine("[success]\tSKYSparseMatrixTests.ClearRowLower passed");
            }
        }
        private static void ClearRowUpper(bool verbose)
        {
            int dim = Random(5, 5);
            DenseMatrix<double> dense = DenseMatrix.RandomTriangular<double>(dim, dim, true);
            SKYSparseMatrix<double> sky = new SKYSparseMatrix<double>(dense, SKYType.UPPER);

            int r = Random(0, dim);
            dense.ClearRow(r);
            sky.ClearRow(r, true);

            if (TestUtil.AssertMatrixEqual(dense, sky, verbose) && verbose)
            {
                Debug.WriteLine("[success]\tSKYSparseMatrixTests.ClearRowUpper passed");
            }
        }
        private static void ClearColumnLower(bool verbose)
        {
            int dim = Random(10, 20);
            DenseMatrix<double> dense = DenseMatrix.RandomTriangular<double>(dim, dim, false);
            SKYSparseMatrix<double> sky = new SKYSparseMatrix<double>(dense, SKYType.LOWER);

            int c = Random(0, dim);
            dense.ClearColumn(c);
            sky.ClearColumn(c, true);

            if (TestUtil.AssertMatrixEqual(dense, sky, verbose) && verbose)
            {
                Debug.WriteLine("[success]\tSKYSparseMatrixTests.ClearColumnLower passed");
            }
        }
        private static void ClearColumnUpper(bool verbose)
        {
            int dim = Random(5, 5);
            DenseMatrix<double> dense = DenseMatrix.RandomTriangular<double>(dim, dim, true);
            SKYSparseMatrix<double> sky = new SKYSparseMatrix<double>(dense, SKYType.UPPER);

            int c = Random(0, dim);
            dense.ClearColumn(c);
            sky.ClearColumn(c, true);

            if (TestUtil.AssertMatrixEqual(dense, sky, verbose) && verbose)
            {
                Debug.WriteLine("[success]\tSKYSparseMatrixTests.ClearColumnUpper passed");
            }
        }
        private static void RowSumLower(bool verbose)
        {
            DenseMatrix<int> dense = RandomTriangularInt(Random(10, 20), false);
            SKYSparseMatrix<int> sky = new SKYSparseMatrix<int>(dense, SKYType.LOWER);
            if (TestUtil.AssertVectorsEqual(dense.RowSums(DefaultProviders.Int32), sky.RowSums(DefaultProviders.Int32)) && verbose)
            {
                Debug.WriteLine("[success]\tSKYSparseMatrixTests.RowSumLower passed");
            }
        }
        private static void RowSumUpper(bool verbose)
        {
            DenseMatrix<int> dense = RandomTriangularInt(Random(10, 20), true);
            SKYSparseMatrix<int> sky = new SKYSparseMatrix<int>(dense, SKYType.UPPER);
            if (TestUtil.AssertVectorsEqual(dense.RowSums(DefaultProviders.Int32), sky.RowSums(DefaultProviders.Int32)) && verbose)
            {
                Debug.WriteLine("[success]\tSKYSparseMatrixTests.RowSumUpper passed");
            }
        }
        private static void RowSumSym(bool verbose)
        {
            int dim = Random(10, 20);
            DenseMatrix<int> dense = DenseMatrix.RandomSPD<int>(dim);
            SKYSparseMatrix<int> sky = new SKYSparseMatrix<int>(dense, SKYType.SYMMETRIC);
            if (TestUtil.AssertVectorsEqual(dense.RowSums(DefaultProviders.Int32), sky.RowSums(DefaultProviders.Int32)) && verbose)
            {
                Debug.WriteLine("[success]\tSKYSparseMatrixTests.RowSumSym passed");
            }
        }
        private static void ColumnSumLower(bool verbose)
        {
            DenseMatrix<int> dense = RandomTriangularInt(Random(10, 20), false);
            SKYSparseMatrix<int> sky = new SKYSparseMatrix<int>(dense, SKYType.LOWER);
            if (TestUtil.AssertVectorsEqual(dense.ColumnSums(DefaultProviders.Int32), sky.ColumnSums(DefaultProviders.Int32)) && verbose)
            {
                Debug.WriteLine("[success]\tSKYSparseMatrixTests.ColumnSumLower passed");
            }
        }
        private static void ColumnSumUpper(bool verbose)
        {
            DenseMatrix<int> dense = RandomTriangularInt(Random(10, 20), true);
            SKYSparseMatrix<int> sky = new SKYSparseMatrix<int>(dense, SKYType.UPPER);
            if (TestUtil.AssertVectorsEqual(dense.ColumnSums(DefaultProviders.Int32), sky.ColumnSums(DefaultProviders.Int32)) && verbose)
            {
                Debug.WriteLine("[success]\tSKYSparseMatrixTests.ColumnSumUpper passed");
            }
        }
        private static void ColumnSumSym(bool verbose)
        {
            int dim = Random(10, 20);
            DenseMatrix<int> dense = DenseMatrix.RandomSPD<int>(dim);
            SKYSparseMatrix<int> sky = new SKYSparseMatrix<int>(dense, SKYType.SYMMETRIC);
            if (TestUtil.AssertVectorsEqual(dense.ColumnSums(DefaultProviders.Int32), sky.ColumnSums(DefaultProviders.Int32)) && verbose)
            {
                Debug.WriteLine("[success]\tSKYSparseMatrixTests.ColumnSumSym passed");
            }
        }

        private static void DirectSumLower(bool verbose)
        {
            DenseMatrix<int> A = RandomTriangularInt(Random(10, 20), false);
            DenseMatrix<int> B = RandomTriangularInt(Random(10, 20), false);

            SKYSparseMatrix<int> _A = new SKYSparseMatrix<int>(A, SKYType.LOWER);
            SKYSparseMatrix<int> _B = new SKYSparseMatrix<int>(B, SKYType.LOWER);

            if (TestUtil.AssertMatrixEqual(A.DirectSum(B), _A.DirectSum(_B), verbose) && verbose)
            {
                Debug.WriteLine("[success]\tSKYSparseMatrixTests.DirectSumLower passed");
            }
        }
        private static void DirectSumSym(bool verbose)
        {
            DenseMatrix<int> A = DenseMatrix.RandomSPD<int>(Random(10, 20));
            DenseMatrix<int> B = DenseMatrix.RandomSPD<int>(Random(10, 20));

            SKYSparseMatrix<int> _A = new SKYSparseMatrix<int>(A, SKYType.SYMMETRIC);
            SKYSparseMatrix<int> _B = new SKYSparseMatrix<int>(B, SKYType.SYMMETRIC);

            if (TestUtil.AssertMatrixEqual(A.DirectSum(B), _A.DirectSum(_B), verbose) && verbose)
            {
                Debug.WriteLine("[success]\tSKYSparseMatrixTests.DirectSumSym passed");
            }
        }
        private static void Elementwise(bool verbose)
        {
            Random r = new Random();
            BandMatrix<int> b = BandMatrix.Random(Random(10, 20), Random(10, 20), 3, 3, () => r.Next(3) - 1);
            b = b.Transpose() * b;

            SKYSparseMatrix<int> sky = new SKYSparseMatrix<int>(b, SKYType.SYMMETRIC);

            // Add ones
            DenseMatrix<int> dense = DenseMatrix.Ones<int>(b.Rows, b.Columns);
            dense += new DenseMatrix<int>(b);

            if (TestUtil.AssertMatrixEqual(dense, sky.Elementwise(x => x + 1), verbose) && verbose)
            {
                Debug.WriteLine("[success]\tSKYSparseMatrixTests.Elementwise passed");
            }
        }
        private static void Pointwise(bool verbose)
        {
            int dim = Random(10, 20);

            DenseMatrix<int> _A = RandomTriangularInt(dim, false),
                _B = RandomTriangularInt(dim, false);

            SKYSparseMatrix<int> A = new SKYSparseMatrix<int>(_A, SKYType.LOWER),
                B = new SKYSparseMatrix<int>(_B, SKYType.LOWER);

            if (TestUtil.AssertMatrixEqual(
                _A.Elementwise(_B, (x, y) => x * x + y), A.Pointwise(B, (x, y) => x * x + y), verbose) && verbose)
            {
                Debug.WriteLine("[success]\tSKYSparseMatrixTests.Pointwise passed");
            }
        }
        private static void VectorizeLower(bool verbose)
        {
            Random r = new Random();
            bool rowMajor = r.Next() % 2 == 0;
            DenseMatrix<int> dense = RandomTriangularInt(Random(10, 20), false);
            SKYSparseMatrix<int> sky = new SKYSparseMatrix<int>(dense, SKYType.LOWER);

            if (TestUtil.AssertVectorsEqual(dense.Vectorize(rowMajor), sky.Vectorize(rowMajor)) && verbose)
            {
                Debug.WriteLine("[success]\tSKYSparseMatrixTests.VectorizeLower passed");
            }
        }
        private static void VectorizeUpper(bool verbose)
        {
            Random r = new Random();
            bool rowMajor = r.Next() % 2 == 0;
            DenseMatrix<int> dense = RandomTriangularInt(Random(10, 20), true);
            SKYSparseMatrix<int> sky = new SKYSparseMatrix<int>(dense, SKYType.UPPER);

            if (TestUtil.AssertVectorsEqual(dense.Vectorize(rowMajor), sky.Vectorize(rowMajor)) && verbose)
            {
                Debug.WriteLine("[success]\tSKYSparseMatrixTests.VectorizeUpper passed");
            }
        }
        private static void VectorizeSym(bool verbose)
        {
            Random r = new Random();
            bool rowMajor = r.Next() % 2 == 0;
            BandMatrix<int> b = BandMatrix.Random(Random(10, 20), Random(10, 20), 3, 3, () => r.Next(3) - 1);
            b = b.Transpose() * b;
            DenseMatrix<int> dense = new DenseMatrix<int>(b);
            SKYSparseMatrix<int> sky = new SKYSparseMatrix<int>(b, SKYType.SYMMETRIC);

            if (TestUtil.AssertVectorsEqual(dense.Vectorize(rowMajor), sky.Vectorize(rowMajor)) && verbose)
            {
                Debug.WriteLine("[success]\tSKYSparseMatrixTests.VectorizeSym passed");
            }
        }
        private static void RemoveAll(bool verbose)
        {
            DenseMatrix<int> dense = RandomTriangularInt(Random(10, 20), false);
            SKYSparseMatrix<int> sky = new SKYSparseMatrix<int>(dense, SKYType.LOWER);

            dense.Clear(x => x == 1);
            sky.Clear(x => x == 1, true);

            if (TestUtil.AssertMatrixEqual(dense, sky, verbose) && verbose)
            {
                Debug.WriteLine("[success]\tSKYSparseMatrixTests.RemoveAll passed");
            }
        }

        private static void Add(bool verbose)
        {
            int dim = Random(10, 20);
            Random r = new Random();
            DenseMatrix<int> A = DenseMatrix.RandomTriangular(dim, dim, false, () => r.Next(3) - 1);
            DenseMatrix<int> B = DenseMatrix.RandomTriangular(dim, dim, false, () => r.Next(3) - 1);

            SKYSparseMatrix<int> _A = new SKYSparseMatrix<int>(A, SKYType.LOWER);
            SKYSparseMatrix<int> _B = new SKYSparseMatrix<int>(B, SKYType.LOWER);

            if (TestUtil.AssertMatrixEqual(A + B, _A + _B, verbose) && verbose)
            {
                Debug.WriteLine("[success]\tSKYSparseMatrixTests.Add passed");
            }
        }
        private static void Subtract(bool verbose)
        {
            int dim = Random(10, 20);
            Random r = new Random();
            DenseMatrix<int> A = DenseMatrix.RandomTriangular(dim, dim, false, () => r.Next(3) - 1);
            DenseMatrix<int> B = DenseMatrix.RandomTriangular(dim, dim, false, () => r.Next(3) - 1);

            SKYSparseMatrix<int> _A = new SKYSparseMatrix<int>(A, SKYType.LOWER);
            SKYSparseMatrix<int> _B = new SKYSparseMatrix<int>(B, SKYType.LOWER);

            if (TestUtil.AssertMatrixEqual(A - B, _A - _B, verbose) && verbose)
            {
                Debug.WriteLine("[success]\tSKYSparseMatrixTests.Subtract passed");
            }
        }
        private static void MultiplyVectorDenseLower(bool verbose)
        {
            int dim = Random(10, 20);
            Random r = new Random();
            DenseMatrix<int> dense = DenseMatrix.RandomTriangular(dim, dim, false, () => r.Next(3) - 1);
            SKYSparseMatrix<int> sky = new SKYSparseMatrix<int>(dense, SKYType.LOWER);

            DenseVector<int> vect = DenseVector.Random(dim, () => r.Next(3) - 1);

            if (TestUtil.AssertVectorsEqual(dense * vect, sky * vect) && verbose)
            {
                Debug.WriteLine("[success]\tSKYSparseMatrixTests.MultiplyVectorDenseLower passed");
            }
        }
        private static void MultiplyVectorDenseUpper(bool verbose)
        {
            int dim = Random(10, 20);
            Random r = new Random();
            DenseMatrix<int> dense = DenseMatrix.RandomTriangular(dim, dim, true, () => r.Next(3) - 1);
            SKYSparseMatrix<int> sky = new SKYSparseMatrix<int>(dense, SKYType.UPPER);

            DenseVector<int> vect = DenseVector.Random(dim, () => r.Next(3) - 1);
            if (TestUtil.AssertVectorsEqual(dense * vect, sky * vect) && verbose)
            {
                Debug.WriteLine("[success]\tSKYSparseMatrixTests.MultiplyVectorDenseUpper passed");
            }
        }
        private static void MultiplyVectorDenseSym(bool verbose)
        {
            int dim = Random(10, 20);
            Random r = new Random();
            BandMatrix<int> band = BandMatrix.Random(dim, dim, 3, 3, () => r.Next(3) - 1);
            band = band.Transpose() * band;
            DenseMatrix<int> dense = new DenseMatrix<int>(band);
            SKYSparseMatrix<int> sky = new SKYSparseMatrix<int>(dense, SKYType.SYMMETRIC);

            DenseVector<int> vect = DenseVector.Random(dim, () => r.Next(3) - 1);
            if (TestUtil.AssertVectorsEqual(dense * vect, sky * vect) && verbose)
            {
                Debug.WriteLine("[success]\tSKYSparseMatrixTests.MultiplyVectorDenseSym passed");
            }
        }
        private static void MultiplyTransVectorDenseLower(bool verbose)
        {
            int dim = Random(10, 20);
            Random r = new Random();
            DenseMatrix<int> dense = DenseMatrix.RandomTriangular(dim, dim, false, () => r.Next(3) - 1);
            SKYSparseMatrix<int> sky = new SKYSparseMatrix<int>(dense, SKYType.LOWER);

            DenseVector<int> vect = DenseVector.Random(dim, () => r.Next(3) - 1);

            if (TestUtil.AssertVectorsEqual(dense.Transpose() * vect, sky.TransposeAndMultiply(vect, DefaultProviders.Int32)) && verbose)
            {
                Debug.WriteLine("[success]\tSKYSparseMatrixTests.MultiplyTransVectorDenseLower passed");
            }
        }
        private static void MultiplyTransVectorDenseUpper(bool verbose)
        {
            int dim = Random(10, 20);
            Random r = new Random();
            DenseMatrix<int> dense = DenseMatrix.RandomTriangular(dim, dim, true, () => r.Next(3) - 1);
            SKYSparseMatrix<int> sky = new SKYSparseMatrix<int>(dense, SKYType.UPPER);

            DenseVector<int> vect = DenseVector.Random(dim, () => r.Next(3) - 1);

            if (TestUtil.AssertVectorsEqual(dense.Transpose() * vect, sky.TransposeAndMultiply(vect, DefaultProviders.Int32)) && verbose)
            {
                Debug.WriteLine("[success]\tSKYSparseMatrixTests.MultiplyTransVectorDenseUpper passed");
            }
        }
        private static void MultiplyTransVectorDenseSym(bool verbose)
        {
            int dim = Random(10, 20);
            Random r = new Random();
            BandMatrix<int> band = BandMatrix.Random(dim, dim, 3, 3, () => r.Next(3) - 1);
            band = band.Transpose() * band;
            DenseMatrix<int> dense = new DenseMatrix<int>(band);
            SKYSparseMatrix<int> sky = new SKYSparseMatrix<int>(dense, SKYType.SYMMETRIC);

            DenseVector<int> vect = DenseVector.Random(dim, () => r.Next(3) - 1);
            if (TestUtil.AssertVectorsEqual(dense.Transpose() * vect, sky.TransposeAndMultiply(vect, DefaultProviders.Int32)) && verbose)
            {
                Debug.WriteLine("[success]\tSKYSparseMatrixTests.MultiplyTransVectorDenseSym passed");
            }
        }
        private static void MultiplyLowerLower(bool verbose)
        {
            int dim = Random(10, 20);
            Random r = new Random();
            BandMatrix<int> _A = BandMatrix.Random(dim, dim, 0, 3, () => r.Next(3) - 1);
            BandMatrix<int> _B = BandMatrix.Random(dim, dim, 0, 3, () => r.Next(3) - 1);

            SKYSparseMatrix<int> A = new SKYSparseMatrix<int>(_A, SKYType.LOWER), B = new SKYSparseMatrix<int>(_B, SKYType.LOWER);

            if (TestUtil.AssertMatrixEqual(A * B, _A * _B, verbose) && verbose)
            {
                Debug.WriteLine("[success]\tSKYSparseMatrixTests.MultiplyLowerLower passed");
            }
        }

        private static void SolveLowerTriangular(bool verbose)
        {
            int dim = TestUtil.Random(10, 20);
            DenseMatrix<double> dense = DenseMatrix.RandomTriangular<double>(dim, dim, false);
            SKYSparseMatrix<double> sky = new SKYSparseMatrix<double>(dense, SKYType.LOWER);

            DenseVector<double> b = DenseVector.Random<double>(dim);
            DenseVector<double> x = new DenseVector<double>(dim);

            sky.SolveTriangular(b, x, DefaultProviders.Double);

            if (TestUtil.AssertVectorsEqual(sky.Multiply(x, DefaultProviders.Double), b, (u, v) => Util.ApproximatelyEquals(u, v, 1e-3)) && verbose)
            {
                Debug.WriteLine("[success]\tSKYSparseMatrixTests.SolveLowerTriangular passed");
            }
        }
        private static void SolveUpperTriangular(bool verbose)
        {
            int dim = TestUtil.Random(10, 20);
            DenseMatrix<double> dense = DenseMatrix.RandomTriangular<double>(dim, dim, true);
            SKYSparseMatrix<double> sky = new SKYSparseMatrix<double>(dense, SKYType.UPPER);

            DenseVector<double> b = DenseVector.Random<double>(dim);
            DenseVector<double> x = new DenseVector<double>(dim);

            sky.SolveTriangular(b, x, DefaultProviders.Double);

            if (TestUtil.AssertVectorsEqual(sky.Multiply(x, DefaultProviders.Double), b, (u, v) => Util.ApproximatelyEquals(u, v, 1e-3)) && verbose)
            {
                Debug.WriteLine("[success]\tSKYSparseMatrixTests.SolveUpperTriangular passed");
            }
        }
        private static void SolveLowerTriangularBLAS1(bool verbose)
        {
            int dim = TestUtil.Random(10, 20);
            DenseMatrix<double> dense = DenseMatrix.RandomTriangular<double>(dim, dim, false);
            SKYSparseMatrix<double> sky = new SKYSparseMatrix<double>(dense, SKYType.LOWER);

            DenseVector<double> b = DenseVector.Random<double>(dim);
            DenseVector<double> x = new DenseVector<double>(dim);

            sky.SolveTriangular(b, x, BLAS.Double);

            if (TestUtil.AssertVectorsEqual(sky.Multiply(x, DefaultProviders.Double), b, (u, v) => Util.ApproximatelyEquals(u, v, 1e-3)) && verbose)
            {
                Debug.WriteLine("[success]\tSKYSparseMatrixTests.SolveLowerTriangularBLAS1 passed");
            }
        }
        private static void SolveUpperTriangularBLAS1(bool verbose)
        {
            int dim = TestUtil.Random(10, 20);
            DenseMatrix<double> dense = DenseMatrix.RandomTriangular<double>(dim, dim, true);
            SKYSparseMatrix<double> sky = new SKYSparseMatrix<double>(dense, SKYType.UPPER);

            DenseVector<double> b = DenseVector.Random<double>(dim);
            DenseVector<double> x = new DenseVector<double>(dim);

            sky.SolveTriangular(b, x, BLAS.Double, false, false);

            if (TestUtil.AssertVectorsEqual(sky.Multiply(x, DefaultProviders.Double), b, (u, v) => Util.ApproximatelyEquals(u, v, 1e-3)) && verbose)
            {
                Debug.WriteLine("[success]\tSKYSparseMatrixTests.SolveUpperTriangularBLAS1 passed");
            }
        }
        private static void CholUp(bool verbose)
        {
            int dim = TestUtil.Random(10, 10);
            Random rand = new Random();
            BandMatrix<double> band = BandMatrix.Random<double>(dim, dim, 3, 3);
            band = band.Transpose() * band;

            SKYSparseMatrix<double> sky = new SKYSparseMatrix<double>(band, SKYType.SYMMETRIC);
            SKYSparseMatrix<double> L = sky.CholeskyFactor(DefaultProviders.Double);

            if (TestUtil.AssertMatrixEqual(band, L * L.Transpose(), (a, b) => Util.ApproximatelyEquals(a, b)) && verbose)
            {
                Debug.WriteLine("[success]\tSKYSparseMatrixTests.CholUp passed");
            }
        }
        private static void CholUpBlAS1(bool verbose)
        {
            int dim = TestUtil.Random(10, 10);
            Random rand = new Random();
            BandMatrix<double> band = BandMatrix.Random<double>(dim, dim, 3, 3);
            band = band.Transpose() * band;

            SKYSparseMatrix<double> sky = new SKYSparseMatrix<double>(band, SKYType.SYMMETRIC);
            SKYSparseMatrix<double> L = sky.CholeskyFactor(BLAS.Double);

            if (TestUtil.AssertMatrixEqual(band, L * L.Transpose(), (a, b) => Util.ApproximatelyEquals(a, b)) && verbose)
            {
                Debug.WriteLine("[success]\tSKYSparseMatrixTests.CholUpBlAS1 passed");
            }
        }

        private static void Save(bool verbose)
        {
            string path = @"";
        }
    }
}
