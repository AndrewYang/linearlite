﻿using LinearNet.Solvers;
using LinearNet.Solvers.Linear;
using LinearNet.Structs;
using LinearNet.Vectors;
using System.Diagnostics;

namespace LinearNet.Tests.LinearAlgebra
{
    public static class SORSolverTests
    {
        public static void RunAll(bool verbose)
        {
            TestDouble(verbose);
        }

        private static void TestDouble(bool verbose)
        {
            SORSolver solver = new SORSolver(1.0, 100000);

            // Create a sparse symmetric, positive definite coefficient matrix 
            int dim = 10;
            DOKSparseMatrix<double> L = DOKSparseMatrix.Random<double>(dim, dim, dim * 4);

            CSRSparseMatrix<double> A = new CSRSparseMatrix<double>(L.Transpose() * L);
            DenseVector<double> b = DenseVector.Random<double>(dim);
            DenseVector<double> x = new DenseVector<double>(dim);

            Stopwatch sw = new Stopwatch();
            sw.Start();
            Solution<DenseVector<double>> soln = solver.Solve(A, x, b);
            sw.Stop();

            DenseVector<double> Ax = A.Multiply(x, DefaultProviders.Double);
            double norm = Ax.Subtract(b).Norm();

            if (norm < 1e-6)
            {
                if (verbose)
                {
                    Debug.WriteLine($"[success]\tSORSolverTests.TestDouble passed in {sw.ElapsedMilliseconds} ms with ||Ax - b|| = { norm }");
                }
            }
            else
            {
                Debug.WriteLine($"[failure]\tSORSolverTests.TestDouble exited with '{ soln.ExitCode }' in {sw.ElapsedMilliseconds} ms, ||Ax - b|| = { norm }");
            }
        }
    }
}
