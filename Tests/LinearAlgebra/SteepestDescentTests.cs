﻿using LinearNet.Matrices;
using LinearNet.Solvers;
using LinearNet.Solvers.Linear;
using LinearNet.Structs;
using LinearNet.Vectors;
using System.Diagnostics;

namespace LinearNet.Tests.LinearAlgebra
{
    public static class SteepestDescentTests
    {
        public static void RunAll()
        {
            Double();
        }
        private static void Double()
        {
            SteepestDescentSolver solver = new SteepestDescentSolver(5000);

            // Create a sparse symmetric, positive definite coefficient matrix 
            int dim = 100;
            DenseMatrix<double> L = DenseMatrix.Random<double>(dim, dim);

            CSRSparseMatrix<double> A = new CSRSparseMatrix<double>(L.Transpose().Multiply(L));
            DenseVector<double> b = DenseVector.Random<double>(dim);

            Stopwatch sw = new Stopwatch();
            sw.Start();
            Solution<DenseVector<double>> soln = solver.Solve(A, b);
            sw.Stop();

            var blas = DefaultProviders.Double;
            Debug.WriteLine($"[success]\tSteepestDescentTests.Double [{dim}x{dim}({A.Density})] passed in {sw.ElapsedMilliseconds} ms with ||Ax - b|| = " + A.Multiply(soln.Result, blas).Subtract(b).Norm());
        }
    }
}
