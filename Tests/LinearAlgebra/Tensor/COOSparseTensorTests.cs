﻿using LinearNet.Structs;
using System;
using System.Diagnostics;

namespace LinearNet.Tests.LinearAlgebra.Tensor
{
    public class COOSparseTensorTests
    {
        public static void RunAll(bool verbose)
        {
            CreateFromMatrix(verbose);
            Add(verbose);
            MultiplyVector(verbose);
            Sort1(verbose);
            Sort(verbose);
            Subtract(verbose);
        }

        private static int[] RandomDimensions()
        {
            int order = TestUtil.Random(2, 6);
            int[] dimension = new int[order];
            for (int i = 0; i < order; ++i)
            {
                dimension[i] = TestUtil.Random(4, 6);
            }
            return dimension;
        }

        private static void CreateFromMatrix(bool verbose)
        {
            COOSparseMatrix<double> matrix = COOSparseMatrix.Random<double>(TestUtil.Random(10, 20), TestUtil.Random(10, 20), 0.5);
            COOSparseTensor<double> tensor = new COOSparseTensor<double>(matrix);

            if (TestUtil.AssertEqual(matrix, tensor) && verbose)
            {
                Debug.WriteLine("[success]\tCOOSparseTensorTests.CreateFromMatrix passed");
            }
        }
        private static void Add(bool verbose)
        {
            int[] dimension = RandomDimensions();
            COOSparseTensor<double>
                A = COOSparseTensor.Random<double>(dimension, 0.2),
                B = COOSparseTensor.Random<double>(dimension, 0.2);

            DenseTensor<double>
                _A = new DenseTensor<double>(A),
                _B = new DenseTensor<double>(B);

            if ((_A + _B).Equals(new DenseTensor<double>(A + B), (u, v) => Util.ApproximatelyEquals(u, v)))
            {
                Debug.WriteLine("[success]\tCOOSparseTensorTests.Add passed");
            }
            else
            {
                Debug.Assert(false);
            }
        }
        private static void MultiplyVector(bool verbose)
        {
            COOSparseMatrix<double> A = COOSparseMatrix.Random<double>(TestUtil.Random(10, 20), TestUtil.Random(10, 20), 0.5);
            COOSparseTensor<double> _A = new COOSparseTensor<double>(A);

            DenseVector<double> x = DenseVector.Random<double>(A.Columns);

            DenseVector<double> Ax = A * x;
            Vector<double> _Ax = _A.Multiply(x, 1, DefaultProviders.Double).ToVector();

            if (TestUtil.AssertVectorsEqual(Ax, _Ax, (u, v) => Util.ApproximatelyEquals(u, v)))
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]\tCOOSparseTensorTests.MultiplyVector passed");
                }
            }
        }
        private static void Sort1(bool verbose)
        {
            int n = 5;
            int[] indexes = new int[n];
            double[] values = new double[n];
            for (int i = 0; i < n; ++i)
            {
                indexes[i] = i;
            }
            indexes.Shuffle(new Random(1007));

            COOSparseTensor<double> tensor = new COOSparseTensor<double>(new int[] { n }, indexes, values, n, false);
            tensor.Sort();

        }
        private static void Sort(bool verbose)
        {
            int n = 10;

            // Create a random shuffed index-value array pair
            int[][] indexes = new int[n * n * n][];
            double[] values = new double[indexes.Length];
            for (int i = 0, z = 0; i < n; ++i)
            {
                for (int j = 0; j < n; ++j)
                {
                    for (int k = 0; k < n; ++k)
                    {
                        indexes[z] = new int[] { i, j, k };
                        values[z] = z;
                        ++z;
                    }
                }
            }

            indexes.Shuffle(new Random(1007));

            // Pack into single array
            int[] indices = new int[indexes.Length * 3];
            for (int i = 0; i < indexes.Length; ++i)
            {
                Array.Copy(indexes[i], 0, indices, i * 3, 3);
            }

            COOSparseTensor<double> tensor = new COOSparseTensor<double>(new int[] { n, n, n }, indices, values, indexes.Length, false);
            tensor.Sort();

            //indices.Print();
        }
        private static void Subtract(bool verbose)
        {
            int order = TestUtil.Random(2, 6);
            int[] dimension = new int[order];
            for (int i = 0; i < order; ++i)
            {
                dimension[i] = TestUtil.Random(4, 6);
            }

            COOSparseTensor<double>
                A = COOSparseTensor.Random<double>(dimension, 0.2),
                B = COOSparseTensor.Random<double>(dimension, 0.2);

            DenseTensor<double>
                _A = new DenseTensor<double>(A),
                _B = new DenseTensor<double>(B);

            if ((_A - _B).Equals(A - B, (u, v) => Util.ApproximatelyEquals(u, v)))
            {
                Debug.WriteLine("[success]\tCOOSparseTensorTests.Subtract passed");
            }
            else
            {
                Debug.Assert(false);
            }
        }
    }
}
