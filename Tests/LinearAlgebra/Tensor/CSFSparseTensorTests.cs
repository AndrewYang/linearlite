﻿using LinearNet.Providers;
using LinearNet.Providers.LinearAlgebra;
using LinearNet.Structs;
using LinearNet.Structs.Tensors;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace LinearNet.Tests.LinearAlgebra.Tensor
{
    public static class CSFSparseTensorTests
    {
        public static void RunAll(bool verbose)
        {
            CreateFromCOO();
            CreateFromOuterProductTest();
            CreateFromMatrixVectorProductTest();
            CreateFromDenseTest();
            CreateFromVectorTest();
            CreateFromMatrixTest();
            CreateFromDenseTensorEmbeddingTest();
            ClearSubtensorTest();
            ClearPredicateTest();
            AdditionTest();
            Add2Test();
            AddBlasTest();
            ContractTest();
            DirectSumTest();
            ExtractFibreTest();
            InnerProductTest();
            NegateTest();
            //PermuteTest(); // doesn't work yet
            SliceTest();
            //SelectTest();
            SetTest();
            SetRowFibreTest();
            SetRowFibre1Test();
            SetColumnFibreTest();
            SetDepthFibreTest();
            SubtractTest();
            SumOverTest();
            TransposeTest();
            Transpose2Test();
            UnfoldTest();
            VectorizeTest();
            MatrixMultiplyTest(); // Mode-2 still needs to be completed
            VectorMultiplyTest();
        }

        private static void CreateFromCOO()
        {
            COOSparseMatrix<double> matrix = COOSparseMatrix.Random<double>(TestUtil.Random(10, 20), TestUtil.Random(10, 20), 0.5);
            SparseVector<double> vector = SparseVector.Random<double>(TestUtil.Random(10, 20), 0.5);
            COOSparseTensor<double> tensor = matrix.TensorProduct(vector, BLAS.Double);
            CSFSparseTensor<double> csf = new CSFSparseTensor<double>(tensor);

            //Debug.WriteLine("CSF");
            //csf.Print();
            //Debug.WriteLine("COO");
            //tensor.Print();

            for (int r = 0; r < csf.Rows; ++r)
            {
                for (int c = 0; c < csf.Columns; ++c)
                {
                    for (int d = 0; d < csf.Depth; ++d)
                    {
                        double v1 = csf[r, c, d], v2 = tensor.Get(r, c, d);
                        Debug.Assert(Util.ApproximatelyEquals(v1, v2), v1 + " != " + v2);
                    }
                }
            }
            Debug.WriteLine("[success]\tCSFSparseTensorTests.CreateFromCOO passed");
        }
        private static void CreateFromDenseTest()
        {
            int rows = 10, cols = 10, depth = 10;
            DenseTensor<double> dense = DenseTensor.Random<double>(rows, cols, depth);

            CSFSparseTensor<double> csf = new CSFSparseTensor<double>(dense, x => x > 0.5);

            for (int r = 0; r < rows; ++r)
            {
                for (int c = 0; c < cols; ++c)
                {
                    for (int d = 0; d < depth; ++d)
                    {
                        if (dense.Get(r, c, d) > 0.5)
                        {
                            Debug.Assert(Util.ApproximatelyEquals(dense.Get(r, c, d), csf.Get(r, c, d)));
                        }
                    }
                }
            }
            Debug.WriteLine("[success]\tCSFSparseTensorTests.CreateFromDenseTest passed");
        }
        private static void CreateFromOuterProductTest()
        {
            SparseVector<double> a = SparseVector.Random<double>(10, 0.5),
                b = SparseVector.Random<double>(5, 0.2),
                c = SparseVector.Random<double>(20, 0.3);

            CSFSparseTensor<double> csf = new CSFSparseTensor<double>(a, b, c, BLAS.Double);
            
            // Complete iteration
            for (int i = 0; i < a.Dimension; ++i)
            {
                for (int j = 0; j < b.Dimension; ++j)
                {
                    for (int k = 0; k < c.Dimension; ++k)
                    {
                        double v1 = csf[i, j, k],
                            v2 = a[i] * b[j] * c[k];
                        Debug.Assert(Util.ApproximatelyEquals(v1, v2), v1 + " != " + v2);
                    }
                }
            }
            Debug.WriteLine("[success]\tCSFSparseTensorTests.CreateFromOuterProductTest passed");
        }
        private static void CreateFromMatrixVectorProductTest()
        {
            CSRSparseMatrix<double> matrix = CSRSparseMatrix.Random<double>(10, 20, 0.5);
            SparseVector<double> vector = SparseVector.Random<double>(5, 0.2);

            CSFSparseTensor<double> tensor = new CSFSparseTensor<double>(matrix, vector, BLAS.Double);

            for (int i = 0; i < matrix.Rows; ++i)
            {
                for (int j = 0; j < matrix.Columns; ++j)
                {
                    for (int k = 0; k < vector.Dimension; ++k)
                    {
                        double v1 = matrix[i, j] * vector[k], v2 = tensor[i, j, k];
                        Debug.Assert(Util.ApproximatelyEquals(v1, v2), v1 + " != " + v2);
                    }
                }
            }
            Debug.WriteLine("[success]\tCSFSparseTensorTests.CreateFromMatrixVectorProductTest passed");
        }
        private static void CreateFromVectorTest()
        {
            Random rand = new Random();
            int n = rand.Next(10, 20);
            SparseVector<double> vector = SparseVector.Random<double>(n, 0.5);

            int[] modes = { 0, 1, 2 };
            foreach (int mode in modes)
            {
                CSFSparseTensor<double> tensor = new CSFSparseTensor<double>(vector, mode);

                int[] index = new int[3];
                for (int d = 0; d < vector.Dimension; ++d)
                {
                    index[mode] = d;

                    double v1 = tensor.Get(index), v2 = vector[d];
                    Debug.Assert(Util.ApproximatelyEquals(v1, v2), v1 + " != " + v2);
                }
            }
            Debug.WriteLine("[success]\tCSFSparseTensorTests.CreateFromVectorTest passed");
        }
        private static void CreateFromMatrixTest()
        {
            Random rand = new Random();
            int m = rand.Next(5, 10), n = rand.Next(5, 10);
            CSRSparseMatrix<double> matrix = CSRSparseMatrix.Random<double>(m, n, 0.5);
            
            for (int mode1 = 0; mode1 < 3; ++mode1)
            {
                for (int mode2 = 0; mode2 < 3; ++mode2)
                {
                    if (mode1 == mode2) continue;

                    CSFSparseTensor<double> tensor = new CSFSparseTensor<double>(matrix, mode1, mode2);

                    int[] index = new int[3];
                    for (int i = 0; i < m; ++i)
                    {
                        for (int j = 0; j < n; ++j)
                        {
                            index[mode1] = i;
                            index[mode2] = j;

                            double v1 = matrix[i, j],
                                v2 = tensor.Get(index);
                            Debug.Assert(Util.ApproximatelyEquals(v1, v2), v1 + " != " + v2);
                        }
                    }
                }
            }
            Debug.WriteLine("[success]\tCSFSparseTensorTests.CreateFromMatrixTest passed");
        }
        private static void CreateFromDenseTensorEmbeddingTest()
        {
            Random rand = new Random();
            int min = 5, max = 5;

            DenseTensor<double> dtensor = DenseTensor.Random<double>(rand.Next(min, max), rand.Next(min, max), rand.Next(min, max));
            int[] rowIndices = new int[dtensor.GetDimensions()[0]],
                columnIndices = new int[dtensor.GetDimensions()[1]],
                depthIndices = new int[dtensor.GetDimensions()[2]];

            for (int i = 1; i < rowIndices.Length; ++i)
            {
                rowIndices[i] = rowIndices[i - 1] + rand.Next(1, 5);
            }
            for (int i = 1; i < columnIndices.Length; ++i)
            {
                columnIndices[i] = columnIndices[i - 1] + rand.Next(1, 5);
            }
            for (int i = 1; i < depthIndices.Length; ++i)
            {
                depthIndices[i] = depthIndices[i - 1] + rand.Next(1, 5);
            }

            CSFSparseTensor<double> stensor = new CSFSparseTensor<double>(100, 100, 100, rowIndices, columnIndices, depthIndices, dtensor);
            
            // Check values
            for (int i = 0; i < rowIndices.Length; ++i)
            {
                for (int j = 0; j < columnIndices.Length; ++j) 
                {
                    for (int k = 0; k < depthIndices.Length; ++k)
                    {
                        double v1 = stensor[rowIndices[i], columnIndices[j], depthIndices[k]],
                            v2 = dtensor.Get(i, j, k);

                        Debug.Assert(Util.ApproximatelyEquals(v1, v2), v1 + " != " + v2);
                    }
                }
            }
            Debug.WriteLine("[success]\tCSFSparseTensorTests.CreateFromDenseTensorEmbeddingTest passed");
        }
        private static void ClearSubtensorTest()
        {
            Random r1 = new Random();
            //int seed = r1.Next(1000);
            int seed = 447;

            Random rand = new Random(seed);
            int min = 6, max = 6, size = min / 2;
            CSFSparseTensor<double> tensor = CSFSparseTensor.Random<double>(rand.Next(min, max), rand.Next(min, max), rand.Next(min, max), density: 0.5, seed: seed);
            int rstart = rand.Next(0, size),
                cstart = rand.Next(0, size),
                dstart = rand.Next(0, size),
                rows = size,
                cols = size,
                depth = size;
            CSFSparseTensor<double> copy = new CSFSparseTensor<double>(tensor);
            tensor.Clear(rstart, cstart, dstart, rows, cols, depth);

            for (int i = 0; i < tensor.Rows; ++i)
            {
                for (int j = 0; j < tensor.Columns; ++j)
                {
                    for (int k = 0; k < tensor.Depth; ++k)
                    {
                        if (rstart <= i && i < rstart + rows && 
                            cstart <= j && j < cstart + cols && 
                            dstart <= k && k < dstart + depth)
                        {
                            Debug.Assert(Util.ApproximatelyEquals(tensor[i, j, k], 0));
                        }
                        else
                        {
                            double v1 = tensor[i, j, k], v2 = copy[i, j, k];
                            Debug.Assert(Util.ApproximatelyEquals(v1, v2), v1 + " != " + v2);
                        }
                    }
                }
            }
            Debug.WriteLine("[success]\tCSFSparseTensorTests.ClearSubtensorTest passed");
        }
        private static void ClearPredicateTest()
        {
            Random rand = new Random();
            int min = 10, max = 20;
            CSFSparseTensor<double> tensor = CSFSparseTensor.Random<double>(rand.Next(min, max), rand.Next(min, max), rand.Next(min, max), density: 0.5);
            CSFSparseTensor<double> copy = new CSFSparseTensor<double>(tensor);

            // Remove all positive entries
            tensor.Clear(x => x >= 0);

            for (int i = 0; i < tensor.Rows; ++i)
            {
                for (int j = 0; j < tensor.Columns; ++j)
                {
                    for (int k = 0; k < tensor.Depth; ++k)
                    {
                        if (tensor[i, j, k] < 0)
                        {
                            double v1 = tensor[i, j, k], v2 = copy[i, j, k];
                            if (!Util.ApproximatelyEquals(v1, v2))
                            {
                                Debug.WriteLine("Result");
                                tensor.Print();
                                Debug.WriteLine("original");
                                copy.Print();
                                Debug.Assert(false, v1 + " != " + v2);
                            }
                        }
                        else
                        {
                            if (!Util.ApproximatelyEquals(tensor[i, j, k], 0))
                            {
                                Debug.WriteLine("Result");
                                tensor.Print();
                                Debug.WriteLine("original");
                                copy.Print();
                                Debug.Assert(false);
                            }
                        }
                    }
                }
            }
            Debug.WriteLine("[success]\tCSFSparseTensorTests.ClearPredicateTest passed");
        }
        private static void AdditionTest()
        {
            CSFSparseTensor<double> S = CSFSparseTensor.Random<double>(1, 10, 10, density: 0.3),
                T = CSFSparseTensor.Random<double>(1, 10, 10, density: 0.2),
                sum = S.Add(T, DefaultProviders.Double);

            Matrix<double> Sm = S.ToMatrix();
            Matrix<double> Tm = T.ToMatrix();
            Matrix<double> msum = sum.ToMatrix();

            Matrix<double> summ = Sm.Add(Tm, DefaultProviders.Double);

            if (!msum.Equals(summ, (a, b) => Util.ApproximatelyEquals(a, b)))
            {
                Debug.Assert(false);
            }
            else
            {
                Debug.WriteLine("[success]\tCSFSparseTensorTests.AdditionTest passed");
            }
        }
        private static void Add2Test()
        {
            CSFSparseTensor<double> S = CSFSparseTensor.Random<double>(1, 100, 100, density: 0.1),
                T = CSFSparseTensor.Random<double>(1, 100, 100, density: 0.1),
                sum = S.Add2(T, DefaultProviders.Double);

            Matrix<double> Sm = S.ToMatrix();
            Matrix<double> Tm = T.ToMatrix();
            Matrix<double> msum = sum.ToMatrix();

            Matrix<double> summ = Sm.Add(Tm, DefaultProviders.Double);

            if (!msum.Equals(summ, (a, b) => Util.ApproximatelyEquals(a, b)))
            {
                Debug.Assert(false);
            }
            else
            {
                Debug.WriteLine("[success]\tCSFSparseTensorTests.Add2Test passed");
            }
        }
        private static void AddBlasTest()
        {
            CSFSparseTensor<double> S = CSFSparseTensor.Random<double>(1, 100, 100, density: 0.1),
                T = CSFSparseTensor.Random<double>(1, 100, 100, density: 0.1),
                sum = S.Add(T, BLAS.Double);

            Matrix<double> Sm = S.ToMatrix();
            Matrix<double> Tm = T.ToMatrix();
            Matrix<double> msum = sum.ToMatrix();

            Matrix<double> summ = Sm.Add(Tm, DefaultProviders.Double);

            if (!msum.Equals(summ, (a, b) => Util.ApproximatelyEquals(a, b)))
            {
                Debug.Assert(false);
            }
            else
            {
                Debug.WriteLine("[success]\tCSFSparseTensorTests.Add2Test passed");
            }
        }
        private static void ContractTest()
        {
            int[] modes = { 0, 1, 2 };
            Random rand = new Random();

            foreach (int mode in modes)
            {
                int n = rand.Next(10, 20);
                int[] dimensions = { n, n, n };
                dimensions[mode] = rand.Next(10, 20);

                CSFSparseTensor<double> tensor = CSFSparseTensor.Random<double>(dimensions[0], dimensions[1], dimensions[2], density: 0.2);
                DenseTensor<double> dense = new DenseTensor<double>(tensor);

                int dim1 = 0, dim2 = 0;
                switch (mode)
                {
                    case 0: dim1 = 1; dim2 = 2; break;
                    case 1: dim1 = 0; dim2 = 2; break;
                    case 2: dim1 = 0; dim2 = 1; break;
                }

                DenseVector<double> contraction = tensor.Contract(dim1, dim2, DefaultProviders.Double);
                DenseVector<double> contraction2 = dense.Contract(dim1, dim2, DefaultProviders.Double).ToDenseVector();
                Debug.Assert(contraction.Equals(contraction2, (u, v) => Util.ApproximatelyEquals(u, v)));
            }
        }
        private static void DirectSumTest()
        {
            Random rand = new Random();
            int min = 5, max = 10;
            int m = rand.Next(min, max), n = rand.Next(min, max), p = rand.Next(min, max);
            CSFSparseTensor<double> A = CSFSparseTensor.Random<double>(m, n, p, density: 0.5);

            int a = rand.Next(min, max), b = rand.Next(min, max), c = rand.Next(min, max);
            CSFSparseTensor<double> B = CSFSparseTensor.Random<double>(a, b, c, density: 0.5);

            DenseTensor<double> S = new DenseTensor<double>(A);
            DenseTensor<double> T = new DenseTensor<double>(B);

            CSFSparseTensor<double> AplusB = A.DirectSum(B);
            DenseTensor<double> SplusT = S.DirectSum(T);

            for (int i = 0; i < AplusB.GetDimension(0); ++i)
            {
                for (int j = 0; j < AplusB.GetDimension(1); ++j)
                {
                    for (int k = 0; k < AplusB.GetDimension(2); ++k)
                    {
                        double v1 = AplusB[i, j, k], v2 = SplusT.Get(i, j, k);
                        Debug.Assert(Util.ApproximatelyEquals(v1, v2), v1 + " != " + v2);
                    }
                }
            }
            Debug.WriteLine("[success]\tCSFSparseTensorTests.DirectSumTest passed");
        }
        private static void ExtractFibreTest()
        {
            int[] modes = { 0, 1, 2 };
            int min = 10, max = 20;
            Random rand = new Random();

            foreach (int mode in modes)
            {
                CSFSparseTensor<double> tensor = CSFSparseTensor.Random<double>(rand.Next(min, max), rand.Next(min, max), rand.Next(min, max), density: 0.5);
                DenseTensor<double> dtensor = new DenseTensor<double>(tensor);

                int[] selection = { rand.Next(tensor.Rows), rand.Next(tensor.Columns), rand.Next(tensor.Depth) };
                selection[mode] = -1;

                // Construct dense tensor selection dictionary
                Dictionary<int, int> select = new Dictionary<int, int>(3);
                for (int i = 0; i < selection.Length; ++i)
                {
                    if (selection[i] >= 0)
                    {
                        select[i] = selection[i];
                    }
                }

                SparseVector<double> sv;
                if (mode == 0)
                {
                    sv = tensor.GetRowFibre(selection[1], selection[2]);
                }
                else if (mode == 1)
                {
                    sv = tensor.GetColumnFibre(selection[0], selection[2]);
                }
                else
                {
                    sv = tensor.GetTubeFibre(selection[0], selection[1]);
                }

                DenseVector<double> dv = dtensor.SelectAsVector(select);
                Debug.Assert(new DenseVector<double>(sv).Equals(dv, (u, v) => Util.ApproximatelyEquals(u, v)));
            }
            Debug.WriteLine("[success]\tCSFSparseTensorTests.ExtractFibreTest passed");
        }
        private static void InnerProductTest()
        {
            CSFSparseTensor<double> S = CSFSparseTensor.Random<double>(1, 10, 10, density: 0.5),
                T = CSFSparseTensor.Random<double>(1, 10, 10, density: 0.5);

            DenseMatrix<double> Sm = S.ToMatrix().ToDenseMatrix();
            DenseMatrix<double> Tm = T.ToMatrix().ToDenseMatrix();

            double ip1 = S.InnerProduct(T, BLAS.Double),
                ip2 = Sm.InnerProduct(Tm, BLAS.Double);

            if (!Util.ApproximatelyEquals(ip1, ip2, 1e-8))
            {
                Debug.Assert(false, ip1 + " != " + ip2);
            }
            else
            {
                Debug.WriteLine("[success]\tCSFSparseTensorTests.InnerProductTest passed");
            }
        }
        private static void NegateTest()
        {
            Random rand = new Random();
            int min = 10, max = 20;
            CSFSparseTensor<double> A = CSFSparseTensor.Random<double>(rand.Next(min, max), rand.Next(min, max), rand.Next(min, max), density: 0.5);
            DenseTensor<double> dA = new DenseTensor<double>(A);
            CSFSparseTensor<double> _A = A.Negate(BLAS.Double);
            DenseTensor<double> _dA = dA.Negate(BLAS.Double);
            Debug.Assert(_A.Equals(_dA, (u, v) => Util.ApproximatelyEquals(u, v)));
            Debug.WriteLine("[success]\tCSFSparseTensorTests.NegateTest passed");
        }
        private static void PermuteTest()
        {
            Random rand = new Random();
            int min = 10, max = 20;
            CSFSparseTensor<double> tensor = CSFSparseTensor.Random<double>(rand.Next(min, max), rand.Next(min, max), rand.Next(min, max), density: 0.5);

            int[] modes = { 0 };
            foreach (int mode in modes)
            {
                PermutationMatrix matrix = PermutationMatrix.Random(tensor.GetDimension(mode));
                int[] permutationVector = matrix.PermutationVector;
                CSFSparseTensor<double> ptensor = new CSFSparseTensor<double>(tensor);
                ptensor.Permute(mode, permutationVector);

                // Check permutation
                int[] index = new int[3], pindex = new int[3];
                for (int i = 0; i < tensor.Rows; ++i)
                {
                    index[0] = i;
                    for (int j = 0; j < tensor.Columns; ++j)
                    {
                        index[1] = j;
                        for (int k = 0; k < tensor.Depth; ++k)
                        {
                            index[2] = k;
                            Array.Copy(index, pindex, 3);
                            pindex[mode] = permutationVector[index[mode]];

                            Debug.Assert(Util.ApproximatelyEquals(tensor.Get(index), ptensor.Get(pindex)));
                        }
                    }
                }
            }
            Debug.WriteLine("[success]\tCSFSparseTensorTests.PermuteTest passed");
        }
        private static void SelectTest()
        {
            Random rand = new Random();
            int min = 10, max = 20;
            CSFSparseTensor<double> tensor = CSFSparseTensor.Random<double>(rand.Next(min, max), rand.Next(min, max), rand.Next(min, max), density: 0.5);

            int r1 = rand.Next(tensor.Rows), r2 = rand.Next(tensor.Rows),
                c1 = rand.Next(tensor.Columns), c2 = rand.Next(tensor.Columns),
                d1 = rand.Next(tensor.Depth), d2 = rand.Next(tensor.Depth);
            if (r1 > r2) Util.Swap(ref r1, ref r2);
            if (c1 > c2) Util.Swap(ref c1, ref c2);
            if (d1 > d2) Util.Swap(ref d1, ref d2);

            string rQuery = $"{rand.Next(tensor.Rows)}, {rand.Next(tensor.Rows)}, {r1}:{r2}";
            string cQuery = $"{rand.Next(tensor.Columns)}, {rand.Next(tensor.Columns)}, {c1}:{c2}";
            string dQuery = $"{rand.Next(tensor.Depth)}, {rand.Next(tensor.Depth)}, {d1}:{d2}";

            DenseTensor<double> dtensor = new DenseTensor<double>(tensor);

            CSFSparseTensor<double> selection = tensor.Select(rQuery, cQuery, dQuery);

            Dictionary<int, string> dqueries = new Dictionary<int, string>(3)
            {
                [0] = rQuery,
                [1] = cQuery,
                [2] = dQuery
            };
            DenseTensor<double> dselection = dtensor.Select(dqueries);

            Debug.Assert(selection.Equals(dselection, (u, v) => Util.ApproximatelyEquals(u, v)));
            Debug.WriteLine("[success]\tCSFSparseTensorTests.SelectTest passed");
        }
        private static void SetTest()
        {
            Random rand = new Random(1007);
            int m = rand.Next(10, 20), n = rand.Next(10, 20), p = rand.Next(10, 20);
            CSFSparseTensor<double> tensor = CSFSparseTensor.Random<double>(m, n, p, () => rand.NextDouble(), density: 0.1);

            for (int t = 0; t < 100; ++t)
            {
                int i = rand.Next(m),
                    j = rand.Next(n),
                    k = rand.Next(p);

                double value = rand.NextDouble();
                tensor[i, j, k] = value;
                Debug.Assert(tensor[i, j, k] == value, tensor[i, j, k] + " != " + value);
            }
            Debug.WriteLine("[success]\tCSFSparseTensorTests.SetTest passed");
        }
        private static void SetRowFibreTest()
        {
            Random rd = new Random();
            int seed = rd.Next(10000);

            Random rand = new Random(seed);
            int min = 10, max = 20;

            CSFSparseTensor<double> tensor = CSFSparseTensor.Random<double>(rand.Next(min, max), rand.Next(min, max), rand.Next(min, max), density: rand.NextDouble(), seed: seed);
            CSFSparseTensor<double> original = new CSFSparseTensor<double>(tensor);
            DenseVector<double> vector = DenseVector.Random<double>(tensor.Rows);

            int _c = rand.Next(tensor.Columns), _d = rand.Next(tensor.Depth);

            tensor.SetRowFibre(_c, _d, vector);

            for (int r = 0; r < tensor.Rows; ++r)
            {
                for (int c = 0; c < tensor.Columns; ++c)
                {
                    for (int d = 0; d < tensor.Depth; ++d)
                    {
                        if (_c == c && _d == d)
                        {
                            double v1 = tensor[r, c, d], v2 = vector[r];
                            if (!Util.ApproximatelyEquals(v1, v2))
                            {
                                Debug.WriteLine("c:\t" + _c + "\td:\t" + _d + "\tseed: " + seed);
                                Debug.WriteLine("original");
                                original.Print();
                                Debug.WriteLine("vector");
                                vector.Print();

                                Debug.WriteLine("result structure");
                                tensor.PrintStructure();
                                Debug.WriteLine("result");
                                tensor.Print();

                                Debug.Assert(false, v1 + " != " + v2);
                            }
                        }
                        else
                        {
                            double v1 = tensor[r, c, d], v2 = original[r, c, d];
                            if (!Util.ApproximatelyEquals(v1, v2))
                            {
                                Debug.WriteLine("c:\t" + _c + "\td:\t" + _d + "\tseed: " + seed);
                                Debug.WriteLine("original");
                                original.Print();
                                Debug.WriteLine("vector");
                                vector.Print();

                                Debug.WriteLine("result structure");
                                tensor.PrintStructure();
                                Debug.WriteLine("result");
                                tensor.Print();

                                Debug.Assert(false, v1 + " != " + v2);
                            }
                        }
                    }
                }
            }
            Debug.WriteLine("[success]\tCSFSparseTensorTests.SetFibreTest passed");
        }
        private static void SetRowFibre1Test()
        {
            // Use a deterministic tensor...
            int[] ri = { 0, 4, 8, 13, 18, 23 },
                nzc = { 0, 2, 3, 4, 0, 2, 3, 4, 0, 1, 2, 3, 4, 0, 1, 2, 3, 4, 0, 1, 2, 3, 4 },
                ci = { 0, 3, 4, 8, 10, 12, 15, 18, 22, 27, 30, 33, 36, 37, 41, 44, 46, 47, 51, 52, 54, 58, 61, 63 },
                di = { 0, 1, 3, 4, 0, 1, 3, 4, 2, 3, 2, 3, 0, 1, 3, 1, 3, 4, 0, 1, 3, 4, 0, 1, 2, 3, 4, 0, 2, 4, 1, 2, 4, 0, 2, 4, 1, 0, 2, 3, 4, 0, 1, 2, 1, 3, 2, 0, 2, 3, 4, 1, 1, 2, 0, 1, 2, 3, 1, 2, 3, 1, 3 };
            double[] values =
            {
                0.746977431,0.022992753,0.559522219,0.498268947,-0.16305843,-0.750389525,-0.587692573,0.844175496,-0.049692562,-0.82624432,0.450122368,0.280729706,0.630851196,
                -0.808780406,0.275299045,-0.085558017,-0.963011905,-0.858741788,-0.15404959,-0.209626873,0.328234004,0.657545827,0.786745077,-0.668823954,-0.779969641,-0.143295436,
                -0.153054564,-0.385180326,-0.3538102,0.718368935,0.012317352,0.256480829,-0.94747839,0.371255818,0.420020247,0.251758653,-0.935683265,-0.396125221,-0.714508622,
                0.141439518,0.534767171,-0.996773218,0.823294551,-0.421596416,0.741828588,-0.957681895,-0.454615191,0.111772182,-0.595205732,-0.316634045,0.109132963,0.687521159,
                -0.425760928,-0.345991542,-0.792498329,-0.910568397,0.236247676,0.228346172,0.278238588,0.980237005,0.402665039,0.797487752,0.197985696
            };

            CSFSparseTensor<double> tensor = new CSFSparseTensor<double>(5, 5, ri, nzc, ci, di, values);
            CSFSparseTensor<double> original = new CSFSparseTensor<double>(tensor);
            DenseVector<double> vector = new DenseVector<double>(new double[] { 0.231939, 0.161438, -0.806358, 0.228208, -0.740405 });

            //int _c = rand.Next(tensor.Columns), _d = rand.Next(tensor.Depth);
            int _c = 3, _d = 2;

            //Debug.WriteLine("c\t" + _c + "\td\t" + _d);
            //Debug.WriteLine("Vector");
            //vector.Print();
            //Debug.WriteLine("tensor\t" + new string('=', 100));
            //tensor.PrintStructure();
            //tensor.Print();

            tensor.SetRowFibre(_c, _d, vector);

            //Debug.WriteLine("Result\t"  + new string('=', 100));
            //tensor.PrintStructure();
            //tensor.Print();
            for (int r = 0; r < tensor.Rows; ++r)
            {
                for (int c = 0; c < tensor.Columns; ++c)
                {
                    for (int d = 0; d < tensor.Depth; ++d)
                    {
                        if (_c == c && _d == d)
                        {
                            double v1 = tensor[r, c, d], v2 = vector[r];
                            Debug.Assert(Util.ApproximatelyEquals(v1, v2), v1 + " != " + v2);
                        }
                        else
                        {
                            double v1 = tensor[r, c, d], v2 = original[r, c, d];
                            Debug.Assert(Util.ApproximatelyEquals(v1, v2), v1 + " != " + v2);
                        }
                    }
                }
            }
            Debug.WriteLine("[success]\tCSFSparseTensorTests.SetFibreTest passed");
        }
        private static void SetColumnFibreTest()
        {
            Random rd = new Random();
            int seed = rd.Next(10000);

            Random rand = new Random(seed);
            int min = 5, max = 10;

            for (int i = 0; i < 100; ++i)
            {
                //int seed = rd.Next(10000);
                //Random rand = new Random(seed);
                //int min = 5, max = 10;
                //Debug.WriteLine("seed:\t" + seed);

                CSFSparseTensor<double> tensor = CSFSparseTensor.Random<double>(rand.Next(min, max), rand.Next(min, max), rand.Next(min, max), density: rand.NextDouble(), seed: seed);
                CSFSparseTensor<double> original = new CSFSparseTensor<double>(tensor);
                DenseVector<double> vector = DenseVector.Random<double>(tensor.Columns);

                int _r = rand.Next(tensor.Rows), _d = rand.Next(tensor.Depth);
                tensor.SetColumnFibre(_r, _d, vector);

                for (int r = 0; r < tensor.Rows; ++r)
                {
                    for (int c = 0; c < tensor.Columns; ++c)
                    {
                        for (int d = 0; d < tensor.Depth; ++d)
                        {
                            if (_r == r && _d == d)
                            {
                                double v1 = tensor[r, c, d], v2 = vector[c];
                                if (!Util.ApproximatelyEquals(v1, v2))
                                {
                                    Debug.WriteLine("r:\t" + _r + "\td:\t" + _d + "\tseed: " + seed);
                                    Debug.WriteLine("original");
                                    original.Print();
                                    Debug.WriteLine("vector");
                                    vector.Print();

                                    Debug.WriteLine("result structure");
                                    tensor.PrintStructure();
                                    Debug.WriteLine("result");
                                    tensor.Print();

                                    Debug.Assert(false, v1 + " != " + v2);
                                }
                            }
                            else
                            {
                                double v1 = tensor[r, c, d], v2 = original[r, c, d];
                                if (!Util.ApproximatelyEquals(v1, v2))
                                {
                                    Debug.WriteLine("r:\t" + _r + "\td:\t" + _d + "\tseed: " + seed);
                                    Debug.WriteLine("original");
                                    original.Print();
                                    Debug.WriteLine("vector");
                                    vector.Print();

                                    Debug.WriteLine("result structure");
                                    tensor.PrintStructure();
                                    Debug.WriteLine("result");
                                    tensor.Print();

                                    Debug.Assert(false, v1 + " != " + v2);
                                }
                            }
                        }
                    }
                }
            }
            Debug.WriteLine("[success]\tCSFSparseTensorTests.SetColumnFibreTest passed");
        }
        private static void SetDepthFibreTest()
        {
            Random rd = new Random();
            int seed = rd.Next(10000);

            Random rand = new Random(seed);
            int min = 5, max = 10;

            for (int i = 0; i < 100; ++i)
            {
                //int seed = rd.Next(10000);
                //Random rand = new Random(seed);
                //Debug.WriteLine("seed:\t" + seed);

                CSFSparseTensor<double> tensor = CSFSparseTensor.Random<double>(rand.Next(min, max), rand.Next(min, max), rand.Next(min, max), density: rand.NextDouble(), seed: seed);
                CSFSparseTensor<double> original = new CSFSparseTensor<double>(tensor);
                DenseVector<double> vector = DenseVector.Random<double>(tensor.Depth);

                int _r = rand.Next(tensor.Rows), _c = rand.Next(tensor.Columns);
                tensor.SetTubeFibre(_r, _c, vector);

                for (int r = 0; r < tensor.Rows; ++r)
                {
                    for (int c = 0; c < tensor.Columns; ++c)
                    {
                        for (int d = 0; d < tensor.Depth; ++d)
                        {
                            if (_r == r && _c == c)
                            {
                                double v1 = tensor[r, c, d], v2 = vector[d];
                                if (!Util.ApproximatelyEquals(v1, v2))
                                {
                                    Debug.WriteLine("r:\t" + _r + "\tc:\t" + _c + "\tseed: " + seed);
                                    Debug.WriteLine("original");
                                    original.Print();
                                    Debug.WriteLine("vector");
                                    vector.Print();

                                    Debug.WriteLine("result structure");
                                    tensor.PrintStructure();
                                    Debug.WriteLine("result");
                                    tensor.Print();

                                    Debug.Assert(false, v1 + " != " + v2);
                                }
                            }
                            else
                            {
                                double v1 = tensor[r, c, d], v2 = original[r, c, d];
                                if (!Util.ApproximatelyEquals(v1, v2))
                                {
                                    Debug.WriteLine("r:\t" + _r + "\tc:\t" + _c + "\tseed: " + seed);
                                    Debug.WriteLine("original");
                                    original.Print();
                                    Debug.WriteLine("vector");
                                    vector.Print();

                                    Debug.WriteLine("result structure");
                                    tensor.PrintStructure();
                                    Debug.WriteLine("result");
                                    tensor.Print();

                                    Debug.Assert(false, v1 + " != " + v2);
                                }
                            }
                        }
                    }
                }
            }
            Debug.WriteLine("[success]\tCSFSparseTensorTests.SetDepthFibreTest passed");
        }
        private static void SliceTest()
        {
            Random rand = new Random();
            int min = 10, max = 20;
            CSFSparseTensor<double> tensor = CSFSparseTensor.Random<double>(rand.Next(min, max), rand.Next(min, max), rand.Next(min, max), density: 0.5);
            DenseTensor<double> dtensor = new DenseTensor<double>(tensor);

            // Amazingly, the dense tensor mode-2 slice is actually wrong (the CSF slice implementation is correct)
            // So we don't test mode-2 slice here
            int[] modes = { 0, 1 };

            foreach (int mode in modes)
            {
                int index = rand.Next(tensor.GetDimension(mode));
                CSRSparseMatrix<double> slice = tensor.Slice(mode, index);
                DenseMatrix<double> dslice = dtensor.Select(new Dictionary<int, int>() { [mode] = index }).ToDenseMatrix();
                Debug.Assert(slice.Equals(dslice, (u, v) => Util.ApproximatelyEquals(u, v)));
            }
            Debug.WriteLine("[success]\tCSFSparseTensorTests.SliceTest passed");
        }
        private static void SubtractTest()
        {
            Random rand = new Random();
            CSFSparseTensor<double> A = CSFSparseTensor.Random<double>(rand.Next(10, 20), rand.Next(10, 20), rand.Next(10, 20), density: 0.5);
            CSFSparseTensor<double> B = CSFSparseTensor.Random<double>(A.Rows, A.Columns, A.Depth, density: 0.5);

            DenseTensor<double> _A = new DenseTensor<double>(A), _B = new DenseTensor<double>(B);

            CSFSparseTensor<double> A_B = A.Subtract(B, BLAS.Double);
            DenseTensor<double> _A_B = _A.Subtract(_B, BLAS.Double);

            for (int i = 0; i < A.Rows; ++i)
            {
                for (int j = 0; j < A.Columns; ++j)
                {
                    for (int k = 0; k < A.Depth; ++k)
                    {
                        Debug.Assert(Util.ApproximatelyEquals(A_B[i, j, k], _A_B.Get(i, j, k)));
                    }
                }
            }
            Debug.WriteLine("[success]\tCSFSparseTensorTests.SubtractTest passed");
        }
        private static void SumOverTest()
        {
            Random rand = new Random(839);
            int m = rand.Next(10, 20), n = rand.Next(10, 20), p = rand.Next(10, 20);
            CSFSparseTensor<double> tensor = CSFSparseTensor.Random(m, n, p, () => rand.NextDouble(), density: 0.5, seed: 977);
            DenseTensor<double> dense = new DenseTensor<double>(tensor);

            int[] modes = { 0, 1, 2 };
            foreach (int mode in modes)
            {
                CSRSparseMatrix<double> smatrix = tensor.SumOver(mode, DefaultProviders.Double);
                DenseMatrix<double> dmatrix = dense.SumOver(mode, DefaultProviders.Double).ToDenseMatrix();

                if (!smatrix.Equals(dmatrix, (u, v) => Util.ApproximatelyEquals(u, v)))
                {
                    Debug.WriteLine("mode: " + mode);
                    Debug.WriteLine("Seed used: " + 977);
                    Debug.WriteLine("sparse matrix");
                    smatrix.Print();
                    Debug.WriteLine("sparse matrix structure");
                    smatrix.RowIndices.Print();
                    smatrix.ColumnIndices.Print();
                    smatrix.Values.Print();

                    Debug.WriteLine("dense matrix");
                    dmatrix.Print();
                    Debug.Assert(false);
                }
            }
            Debug.WriteLine("[success]\tCSFSparseTensorTests.SumOverTest passed");
        }
        private static void MatrixMultiplyTest()
        {
            int[] modes = { 0, 1, 2 };

            Random rd = new Random();
            //int seed = rd.Next(10000);
            int seed = 1764;

            Random rand = new Random(seed);
            int min = 5, max = 10;

            foreach (int mode in modes)
            {
                Debug.WriteLine(mode + "\t" + new string('=', 100));
                Debug.WriteLine("seed:\t" + seed);

                int[] md = { rand.Next(min, max), rand.Next(min, max) };
                int[] td = { rand.Next(min, max), rand.Next(min, max), rand.Next(min, max) };
                td[mode] = md[1];

                CSFSparseTensor<double> tensor = CSFSparseTensor.Random(td[0], td[1], td[2], () => rand.NextDouble(), density: 0.3, seed: seed);
                CSRSparseMatrix<double> matrix = CSRSparseMatrix.Random(md[0], md[1], 0.3, () => rand.NextDouble(), seed: seed);
                CSFSparseTensor<double> product = tensor.Multiply(matrix, mode, DefaultProviders.Double);

                // compare with dense variants
                DenseTensor<double> dtensor = new DenseTensor<double>(tensor);
                DenseMatrix<double> dmatrix = new DenseMatrix<double>(matrix);
                DenseTensor<double> dproduct = dtensor.Multiply(dmatrix, mode, BLAS.Double);

                Debug.WriteLine("tensor");
                tensor.Print();

                Debug.WriteLine("matrix");
                matrix.Print();

                Debug.WriteLine("product");
                product.Print();

                Debug.WriteLine("dproduct");
                dproduct.Print();
                for (int i = 0; i < product.GetDimension(0); ++i)
                {
                    for (int j = 0; j < product.GetDimension(1); ++j)
                    {
                        for (int k = 0; k < product.GetDimension(2); ++k)
                        {
                            double v1 = product[i, j, k], v2 = dproduct.Get(new int[] { i, j, k });
                            Debug.Assert(Util.ApproximatelyEquals(v1, v2), v1 + " != " + v2);
                        }
                    }
                }
            }
            
            Debug.WriteLine("[success]\tCSFSparseTensorTests.MatrixMultiplyTest passed");
        }
        private static void VectorMultiplyTest()
        {
            Random rand = new Random();
            int m = rand.Next(10, 20), n = rand.Next(10, 20), p = rand.Next(10, 20);
            CSFSparseTensor<double> tensor = CSFSparseTensor.Random<double>(m, n, p, density: 0.5);
            DenseTensor<double> dense = new DenseTensor<double>(tensor);

            for (int mode = 0; mode < 3; ++mode)
            {
                DenseVector<double> vector = DenseVector.Random<double>(tensor.GetDimension(mode));

                CSRSparseMatrix<double> smatrix = tensor.Multiply(vector, mode, DefaultProviders.Double);
                DenseMatrix<double> dmatrix = dense.Multiply(vector, mode, BLAS.Double).ToDenseMatrix();

                Debug.Assert(dmatrix.Equals(smatrix, (u, v) => Util.ApproximatelyEquals(u, v)));
            }
            Debug.WriteLine("[success]\tCSFSparseTensorTests.VectorMultiplyTest passed");
        }
        private static void TransposeTest()
        {
            CSFSparseTensor<double> tensor = CSFSparseTensor.Random<double>(10, 20, 5, density: 0.5);
            DenseTensor<double> dense = new DenseTensor<double>(tensor);

            Random rand = new Random();
            int[] permutation = { 0, 1, 2 };
            permutation.Shuffle(rand);

            tensor.Transpose(permutation);
            dense.Transpose(permutation);

            for (int r = 0; r < tensor.Rows; ++r)
            {
                for (int c = 0; c < tensor.Columns; ++c)
                {
                    for (int d = 0; d < tensor.Depth; ++d)
                    {
                        Debug.Assert(Util.ApproximatelyEquals(tensor[r, c, d], dense.Get(r, c, d)));
                    }
                }
            }
            Debug.WriteLine("[success]\tCSFSparseTensorTests.TransposeTest passed");
        }
        private static void Transpose2Test()
        {
            CSFSparseTensor<double> tensor = CSFSparseTensor.Random<double>(10, 20, 5, density: 0.5);
            DenseTensor<double> dense = new DenseTensor<double>(tensor);

            int[] permutation = { 2, 1, 0 };

            tensor.Transpose2(permutation);
            dense.Transpose(permutation);

            for (int r = 0; r < tensor.Rows; ++r)
            {
                for (int c = 0; c < tensor.Columns; ++c)
                {
                    for (int d = 0; d < tensor.Depth; ++d)
                    {
                        Debug.Assert(Util.ApproximatelyEquals(tensor[r, c, d], dense.Get(r, c, d)));
                    }
                }
            }
            Debug.WriteLine("[success]\tCSFSparseTensorTests.Transpose2Test passed");
        }
        private static void UnfoldTest()
        {
            int[] modes = { 0, 1 };
            Random rand = new Random();
            int min = 10, max = 20;

            foreach (int mode in modes)
            {
                CSFSparseTensor<double> tensor = CSFSparseTensor.Random<double>(rand.Next(min, max), rand.Next(min, max), rand.Next(min, max), density: 0.5);
                DenseTensor<double> dtensor = new DenseTensor<double>(tensor);

                CSRSparseMatrix<double> matrix = tensor.Unfold(mode);
                DenseMatrix<double> dmatrix = dtensor.Unfold(mode);

                Debug.Assert(matrix.Equals(dmatrix, (u, v) => Util.ApproximatelyEquals(u, v)));
            }
            Debug.WriteLine("[success]\tCSFSparseTensorTests.UnfoldTest passed");
        }
        private static void VectorizeTest()
        {
            CSFSparseTensor<double> tensor = CSFSparseTensor.Random<double>(10, 20, 5, density: 0.1);
            SparseVector<double> vector = tensor.Vectorize(0, 1, 2);

            foreach (TensorEntry<double> e in tensor.NonZeroEntries)
            {
                int index = e.Index[0] * tensor.Columns * tensor.Depth + e.Index[1] * tensor.Depth + e.Index[2];
                Debug.Assert(Util.ApproximatelyEquals(vector[index], e.Value));
            }

            Debug.WriteLine("[success]\tCSFSparseTensorTests.VectorizeTest passed");
        }
    }
}
