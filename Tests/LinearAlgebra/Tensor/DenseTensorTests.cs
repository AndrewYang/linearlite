﻿using LinearNet.Matrices;
using LinearNet.Providers.LinearAlgebra;
using LinearNet.Structs;
using LinearNet.Tensors.Decompositions;
using LinearNet.Vectors;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace LinearNet.Tests.LinearAlgebra.Tensor
{
    public static class DenseTensorTests
    {
        public static void RunAll(bool verbose)
        {
            CreateFromOuterProduct(verbose);
            CreateFromKTensor(verbose);
            Contract(verbose);
            Contract2(verbose);
            DirectSum(verbose);
            MultiplyVector(verbose);
            MultiplyMatrix(verbose);
            Unfold(verbose);
            Select(false);
            Vectorize(verbose);
            SumOver(verbose);
            SumOverAllExcept(verbose);

            CPDecompositionTest(verbose);
        }

        private static void CreateFromOuterProduct(bool verbose)
        {
            DenseVector<double> u = DenseVector.Random<double>(TestUtil.Random(10, 20)),
                v = DenseVector.Random<double>(TestUtil.Random(10, 20));

            DenseVector<double>[] vs = { u, v };
            if (TestUtil.AssertMatrixEqual(u.OuterProduct(v), new DenseTensor<double>(vs, DefaultProviders.Double).ToDenseMatrix(), verbose) && verbose)
            {
                Debug.WriteLine("[success]\tDenseTensorTests.CreateFromOuterProduct passed");
            }
        }
        private static void CreateFromKTensor(bool verbose)
        {
            DenseVector<double> u = DenseVector.Random<double>(TestUtil.Random(10, 20)),
                v = DenseVector.Random<double>(TestUtil.Random(10, 20));

            KruskalTensor<double> kt = new KruskalTensor<double>(new DenseVector<double>[] { u, v }, DefaultProviders.Double);
            DenseTensor<double> T = new DenseTensor<double>(kt);

            if (TestUtil.AssertMatrixEqual(u.OuterProduct(v), T.ToDenseMatrix(), verbose) && verbose)
            {
                Debug.WriteLine("[success]\tDenseTensorTests.CreateFromKTensor passed");
            }
        }
        private static void Contract(bool verbose)
        {
            int dim = TestUtil.Random(10, 20);
            DenseMatrix<double> matrix = DenseMatrix.Random<double>(dim, dim);

            double tr = new DenseTensor<double>(matrix).Contract(0, 1, DefaultProviders.Double).ToScalar();
            if (!Util.ApproximatelyEquals(matrix.Trace(), tr))
            {
                
                Debug.Assert(false, $"{matrix.Trace()} != {tr}");
            }
            else
            {
                Debug.WriteLine("[success]\tDenseTensorTests.Contract passed");
            }
        }
        private static void Contract2(bool verbose)
        {
            int dim = TestUtil.Random(10, 20);
            DenseMatrix<double> A = DenseMatrix.Random<double>(TestUtil.Random(10, 20), dim),
                B = DenseMatrix.Random<double>(dim, TestUtil.Random(10, 20));

            DenseTensor<double> _A = new DenseTensor<double>(A),
                _B = new DenseTensor<double>(B);

            DenseTensor<double> _AB = _A.Contract(_B, 1, 0, BLAS.Double);

            if (TestUtil.AssertMatrixEqual(_AB.ToDenseMatrix(), A * B, (u, v) => Util.ApproximatelyEquals(u, v)) && verbose)
            {
                Debug.WriteLine("[success]\tDenseTensorTests.Contract2 passed");
            }
        }
        private static void DirectSum(bool verbose)
        {
            DenseMatrix<double> A = DenseMatrix.Random<double>(TestUtil.Random(10, 20), TestUtil.Random(10, 20)),
                B = DenseMatrix.Random<double>(TestUtil.Random(10, 20), TestUtil.Random(10, 20));

            DenseTensor<double> _A = new DenseTensor<double>(A), 
                _B = new DenseTensor<double>(B);

            if (TestUtil.AssertMatrixEqual(_A.DirectSum(_B).ToDenseMatrix(), A.DirectSum(B), verbose) && verbose)
            {
                Debug.WriteLine("[success]\tDenseTensorTests.DirectSum passed");
            }
        }
        private static void MultiplyVector(bool verbose)
        {
            DenseMatrix<double> A = DenseMatrix.Random<double>(TestUtil.Random(10, 20), TestUtil.Random(10, 20));
            DenseTensor<double> _A = new DenseTensor<double>(A);

            Random r = new Random();
            int mode = r.Next(2);

            DenseVector<double> x = DenseVector.Random<double>(mode == 1 ? A.Columns : A.Rows);
            DenseVector<double> Ax = mode == 1 ? A * x : A.TransposeAndMultiply(x, DefaultProviders.Double);
            DenseTensor<double> _Ax = _A.Multiply(x, mode, BLAS.Double);

            if (TestUtil.AssertVectorsEqual(Ax, _Ax.ToDenseVector(), (u, v) => Util.ApproximatelyEquals(u, v)) && verbose)
            {
                Debug.WriteLine("[success]\tDenseTensorTests.MultiplyVector passed");
            }
        }
        private static void MultiplyMatrix(bool verbose)
        {
            DenseMatrix<double> A = DenseMatrix.Random<double>(TestUtil.Random(10, 20), TestUtil.Random(10, 20));
            DenseTensor<double> _A = new DenseTensor<double>(A);

            Random r = new Random();
            int mode = r.Next(2);
            if (mode == 1)
            {
                // Standard matrix multiply
                DenseMatrix<double> B = DenseMatrix.Random<double>(A.Columns, TestUtil.Random(10, 20));
                DenseMatrix<double> AB = A * B,
                    _AB = _A.Multiply(B.Transpose(), mode, BLAS.Double).ToDenseMatrix();

                if (TestUtil.AssertMatrixEqual(AB, _AB, (u, v) => Util.ApproximatelyEquals(u, v)) && verbose)
                {
                    Debug.WriteLine("[success]\tDenseTensorTests.MultiplyMatrix(1) passed");
                }
            }
            else
            {
                // transposed multiply
                DenseMatrix<double> B = DenseMatrix.Random<double>(A.Rows, TestUtil.Random(10, 20));
                DenseMatrix<double> AB = B.Transpose() * A,
                    _AB = _A.Multiply(B.Transpose(), mode, BLAS.Double).ToDenseMatrix();

                if (TestUtil.AssertMatrixEqual(AB, _AB, (u, v) => Util.ApproximatelyEquals(u, v)) && verbose)
                {
                    Debug.WriteLine("[success]\tDenseTensorTests.MultiplyMatrix(0) passed");
                }
            }
        }
        private static void Unfold(bool verbose)
        {
            double[,] X0 =
            {
                { 0, 2, 4, 6 },
                { 8, 10, 12, 14 },
                { 16, 18, 20, 22 }
            };

            double[,] X1 =
            {
                { 1, 3, 5, 7 },
                { 9, 11, 13, 15 },
                { 17, 19, 21, 23 }
            };

            double[,,] X = new double[3, 4, 2];
            for (int i = 0; i < 3; ++i)
            {
                for (int j = 0; j < 4; ++j)
                {
                    X[i, j, 0] = X0[i, j];
                    X[i, j, 1] = X1[i, j];
                }
            }

            DenseTensor<double> T = new DenseTensor<double>(X);

            if (verbose)
            {
                T.Unfold(0).ToDenseMatrix().Print();
                T.Unfold(1).ToDenseMatrix().Print();
                T.Unfold(2).ToDenseMatrix().Print();
            }
        }
        private static void Select(bool verbose)
        {
            DenseMatrix<double> matrix = DenseMatrix.Random<double>(TestUtil.Random(10, 20), TestUtil.Random(10, 20));
            Random rand = new Random();
            int r1 = rand.Next(matrix.Rows), r2 = rand.Next(matrix.Rows),
                c1 = rand.Next(matrix.Columns), c2 = rand.Next(matrix.Columns);
            if (r1 > r2) Util.Swap(ref r1, ref r2);
            if (c1 > c2) Util.Swap(ref c1, ref c2);

            string rQuery = $"{rand.Next(matrix.Rows)}, {rand.Next(matrix.Rows)}, {r1}:{r2}";
            string cQuery = $"{rand.Next(matrix.Columns)}, {rand.Next(matrix.Columns)}, {c1}:{c2}";

            DenseMatrix<double> submatrix = matrix[rQuery, cQuery].ToDenseMatrix();

            Dictionary<int, string> sQuery = new Dictionary<int, string>()
            {
                [0] = rQuery, [1] = cQuery
            };
            DenseTensor<double> tensor = new DenseTensor<double>(matrix);
            DenseTensor<double> subtensor = tensor.Select(sQuery, false);

            if (TestUtil.AssertMatrixEqual(submatrix, subtensor.ToDenseMatrix(), verbose) && verbose)
            {
                Debug.WriteLine("[success]\tDenseTensorTests.Select passed");
            }
        }
        private static void Vectorize(bool verbose)
        {
            DenseMatrix<double> A = DenseMatrix.Random<double>(TestUtil.Random(10, 20), TestUtil.Random(10, 20));
            DenseTensor<double> _A = new DenseTensor<double>(A);

            if (TestUtil.AssertVectorsEqual(A.Vectorize(true), _A.Vectorize()) && verbose)
            {
                Debug.WriteLine("[success]\tDenseTensorTests.Vectorize(0, 1) passed");
            }
            if (TestUtil.AssertVectorsEqual(A.Vectorize(false), _A.Vectorize(new int[] { 1, 0 })) && verbose)
            {
                Debug.WriteLine("[success]\tDenseTensorTests.Vectorize(1, 0) passed");
            }
        }
        private static void SumOver(bool verbose)
        {
            DenseMatrix<double> A = DenseMatrix.Random<double>(TestUtil.Random(10, 20), TestUtil.Random(10, 20));
            DenseTensor<double> _A = new DenseTensor<double>(A);

            DenseVector<double> rowSums = A.RowSums(), colSums = A.ColumnSums();
            DenseVector<double> 
                rowSums1 = _A.SumOver(new int[] { 1 }, DefaultProviders.Double).ToDenseVector(),
                colSums1 = _A.SumOver(new int[] { 0 }, DefaultProviders.Double).ToDenseVector();

            if (TestUtil.AssertVectorsEqual(rowSums, rowSums1, (u, v) => Util.ApproximatelyEquals(u, v)) && verbose)
            {
                Debug.WriteLine("[success]\tDenseTensorTests.SumOver(rows) passed");
            }

            if (TestUtil.AssertVectorsEqual(colSums, colSums1, (u, v) => Util.ApproximatelyEquals(u, v)) && verbose)
            {
                Debug.WriteLine("[success]\tDenseTensorTests.SumOver(columns) passed");
            }
        }
        private static void SumOverAllExcept(bool verbose)
        {
            DenseMatrix<double> A = DenseMatrix.Random<double>(TestUtil.Random(10, 20), TestUtil.Random(10, 20));
            DenseTensor<double> _A = new DenseTensor<double>(A);

            DenseVector<double> rowSums = A.RowSums(), colSums = A.ColumnSums();
            DenseVector<double>
                rowSums1 = _A.SumOverAllExcept(0, DefaultProviders.Double),
                colSums1 = _A.SumOverAllExcept(1, DefaultProviders.Double);

            if (TestUtil.AssertVectorsEqual(rowSums, rowSums1, (u, v) => Util.ApproximatelyEquals(u, v)) && verbose)
            {
                Debug.WriteLine("[success]\tDenseTensorTests.SumOverAllExcept(rows) passed");
            }

            if (TestUtil.AssertVectorsEqual(colSums, colSums1, (u, v) => Util.ApproximatelyEquals(u, v)) && verbose)
            {
                Debug.WriteLine("[success]\tDenseTensorTests.SumOverAllExcept(columns) passed");
            }
        }
        private static void CPDecompositionTest(bool verbose = false)
        {
            int order = 6, firstDim = 5;

            DenseVector<double>[] components = new DenseVector<double>[order];
            for (int i = 0; i < order; ++i)
            {
                components[i] = DenseVector.Random<double>(firstDim);
            }

            DenseTensor<double> tensor = new DenseTensor<double>(components, DefaultProviders.Double);
            KruskalTensor<double> ktensor = tensor.NormalizedCanonicalPolyadicDecompose(2);

            DenseTensor<double> reconstruction = new DenseTensor<double>(ktensor);
            Debug.Assert(tensor.Equals(reconstruction, (u, v) => Util.ApproximatelyEquals(u, v)));

            if (verbose)
            {
                double norm = tensor.FrobeniusDistance(reconstruction, DefaultProviders.Double) / tensor.FrobeniusNorm(DefaultProviders.Double);
                Debug.WriteLine($"CPD ||T - T'|| / ||T|| = {norm}");
            }
        }
    }
}
