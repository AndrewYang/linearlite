﻿using LinearNet.Matrices;
using LinearNet.Providers.LinearAlgebra;
using LinearNet.Structs;
using LinearNet.Tensors;
using LinearNet.Tensors.Decompositions;
using System.Collections.Generic;
using System.Diagnostics;
namespace LinearNet.Tests
{
    public static class TensorTests
    {
        public static void RunAll(bool verbose)
        {
            NModeFlattenTest(verbose);
            NModeProductTest(verbose);
            HOSVDTest(verbose);
            TensorVectorizationTest(verbose);
            SparseTensorCreateTest(verbose);
            TensorOverflowTest(verbose);
            SparseTensorDirectSum(verbose);
            SparseTensorProductTest(verbose);
            SparseTensorIndexTest(verbose);
        }

        public static void TensorCreateTest()
        {
            double[,] m1 = RectangularMatrix.Random<double>(5000, 5000);
            double[,] m2 = RectangularMatrix.Random<double>(5000, 5000);
            Stopwatch sw = new Stopwatch();

            sw.Restart();
            double[,] r1 = m1.ElementwiseOperation(m2, (double a, double b) => a + b);
            sw.Stop();
            Debug.WriteLine("Using func:\t" + sw.ElapsedMilliseconds);

            sw.Restart();
            double[,] r2 = m1.Add(m2);
            sw.Stop();
            Debug.WriteLine("Using native double multiplication:\t" + sw.ElapsedMilliseconds);

        }
        public static void SparseTensorCreateTest(bool verbose = false)
        {
            SparseTensor<double> sparse = SparseTensor.Random<double>(10, 10);

            if (verbose)
            {
                sparse.PrintDimensions();
                sparse.Print();
            }
        }
        public static void TensorDimensionTest()
        {
            DenseTensor<double> tensor = new DenseTensor<double>(4, 3, 2, 2, 2, 2);
            int[] dimensions = tensor.GetDimensions();
            dimensions.Print();
        }
        public static void TensorConversionTest(bool verbose = false)
        {
            if (verbose)
            {
                DenseTensor<double> tensor = DenseTensor.Random<double>(10, 10);
                Debug.WriteLine("Dense 2");
                tensor.Print();

                SparseTensor<double> sparse = (SparseTensor<double>)tensor;
                Debug.WriteLine("Sparse 2");
                sparse.Print();


                DenseTensor<double> tensor2 = DenseTensor.Random<double>(5, 10, 10);
                Debug.WriteLine("Dense 3");
                tensor2.Print();

                SparseTensor<double> sparse2 = (SparseTensor<double>)tensor2;
                Debug.WriteLine("Sparse 3");
                sparse2.Print();
            }
        }
        public static void TensorOverflowTest(bool run = false)
        {
            if (run)
            {
                SparseTensor<double> tensor = new SparseTensor<double>(16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16);

            }
        }

        public static void DenseTensorDirectSumTest()
        {
            DenseTensor<double> S = DenseTensor.Random<double>(10, 10), T = DenseTensor.Random<double>(10, 10);

            Debug.WriteLine("S");
            S.Print(a => a.ToString("n6"));

            Debug.WriteLine("T");
            T.Print(a => a.ToString("n6"));

            Debug.WriteLine("S (+) T");
            S.DirectSum(T).Print(a => a.ToString("n6"));
        }
        public static void DenseTensorProductTest()
        {
            DenseTensor<double> S = DenseTensor.Random<double>(4, 5, 6), T = DenseTensor.Random<double>(4, 4, 4);

            Debug.WriteLine("S");
            S.Print(a => a.ToString("n6"));

            Debug.WriteLine("T");
            T.Print(a => a.ToString("n6"));

            Debug.WriteLine("S (x) T");
            S.TensorProduct(T, BLAS.Double, false).Print(a => a.ToString("n6"));

        }
        public static void SparseTensorProductTest(bool verbose = false)
        {
            DenseTensor<double> S = DenseTensor.Random<double>(3, 2, 3), T = DenseTensor.Random<double>(2, 3, 3);
            SparseTensor<double> _S = (SparseTensor<double>)S, _T = (SparseTensor<double>)T;

            DenseTensor<double> ST = S.TensorProduct(T, BLAS.Double, false);
            SparseTensor<double> _ST = _S.TensorProduct(_T);

            if (verbose)
            {
                Debug.WriteLine("S");
                S.Print();

                Debug.WriteLine("_S");
                _S.Print();

                Debug.WriteLine("T");
                T.Print();

                Debug.WriteLine("_T");
                _T.Print();

                Debug.WriteLine("S (x) T (dense)");
                ST.Print();

                Debug.WriteLine("S (x) T (sparse)");
                _ST.Print();
            }

            Debug.Assert(ST.ApproximatelyEquals((DenseTensor<double>)_ST));
        }
        public static void SparseTensorIndexTest(bool verbose = false)
        {
            SparseTensor<double> S = (SparseTensor<double>)DenseTensor.Random<double>(2, 3, 3);

            if (verbose)
            {
                S.PrintDimensions();
                S.Print();

                foreach (long key in S.Values.Keys)
                {
                    int[] index = new int[3];
                    S.get_index(key, index, true);

                    int[] index2 = new int[3];
                    //S.get_index(key, index, false);

                    Debug.WriteLine(key + "\t" + string.Join(",", index) + "\t" + string.Join(",", index2) + "\t" + S.Values[key]);
                }
            }
            
        }

        public static void NModeFlattenTest(bool verbose = false)
        {
            DenseTensor<double> tensor = (DenseTensor<double>)new double[,,]
            {
                { { 1, 13 }, { 4, 16 }, { 7, 19 }, { 10, 22 } },
                { { 2, 14 }, { 5, 17 }, { 8, 20 }, { 11, 23 } },
                { { 3, 15 }, { 6, 18 }, { 9, 21 }, { 12, 24 } }
            };

            if (verbose)
            {
                Debug.WriteLine("Tensor");
                tensor.Print();

                Debug.WriteLine("Mode 0");
                tensor.Unfold(0).Print();

                Debug.WriteLine("Mode 1");
                tensor.Unfold(1).Print();

                Debug.WriteLine("Mode 2");
                tensor.Unfold(2).Print();
            }
        }
        public static void NModeProductTest(bool verbose = false)
        {
            DenseTensor<double> tensor = DenseTensor.Random<double>(3, 4, 5);
            DenseMatrix<double> matrix = DenseMatrix.Random<double>(7, 3);

            if (verbose)
            {
                tensor.Multiply(matrix, 0, BLAS.Double).PrintDimensions();
            }
        }
        public static void HOSVDTest(bool verbose = false)
        {
            DenseTensor<double> A = DenseTensor.Random<double>(4, 4, 4);
            A.HOSVD(out DenseTensor<double> S, out List<DenseMatrix<double>> U);

            DenseTensor<double> product = S;
            for (int i = 0; i < U.Count; ++i)
            {
                product = product.Multiply(U[i], i, BLAS.Double);
            }
            
            if (verbose)
            {
                Debug.WriteLine("A");
                A.Print();

                Debug.WriteLine("S");
                S.Print();

                Debug.WriteLine("Us");
                foreach (DenseMatrix<double> u in U)
                {
                    u.Print();
                }

                Debug.WriteLine("Product");
                product.Print();
            }
            
        }

        public static void TensorVectorizationTest(bool verbose = false)
        {
            if (verbose)
            {
                DenseTensor<double> dense = DenseTensor.Random<double>(3, 3, 3);
                SparseTensor<double> sparse = (SparseTensor<double>)dense;

                DenseVector<double> dvector = dense.Vectorize();
                BigSparseVector<double> svector = sparse.Vectorize();

                Debug.WriteLine("Tensor (dense)");
                dense.Print();

                Debug.WriteLine("Tensor (sparse)");
                sparse.Print();

                Debug.WriteLine("Vector (dense)");
                dvector.Print();

                Debug.WriteLine("Vector (sparse)");
                svector.Print();
            }
        }
        public static void SparseTensorDirectSum(bool verbose = false)
        {
            DenseMatrix<double> A = DenseMatrix.Random<double>(10, 10);
            DenseMatrix<double> B = DenseMatrix.Random<double>(5, 5);

            DenseTensor<double> tensorA = (DenseTensor<double>)A;
            DenseTensor<double> tensorB = (DenseTensor<double>)B;

            DenseMatrix<double> matrixSum = A.DirectSum(B);
            DenseTensor<double> tensorSum = tensorA.DirectSum(tensorB);

            if (verbose)
            {
                Debug.WriteLine("A");
                A.Print();

                Debug.WriteLine("B");
                B.Print();

                Debug.WriteLine("A (+) B");
                matrixSum.Print();

                Debug.WriteLine("A (+) B (version 2)");
                tensorSum.Print();
            }

            Debug.Assert(((DenseTensor<double>)matrixSum).ApproximatelyEquals(tensorSum));
        }

        public static void TensorSummationTest(bool verbose = false)
        {
            DenseTensor<double> tensor = DenseTensor.Random<double>(10, 10);
            DenseVector<double> rowSums = tensor.SumOverAllExcept(0, DefaultProviders.Double);
            DenseVector<double> colSums = tensor.SumOverAllExcept(1, DefaultProviders.Double);

            DenseTensor<double> tensor3 = DenseTensor.Random<double>(10, 10, 10);

            if (verbose)
            {
                Debug.WriteLine("tensor");
                tensor.Print();

                Debug.WriteLine("row sums");
                rowSums.Print();

                Debug.WriteLine("col sums");
                colSums.Print();

                Debug.WriteLine("Tensor 3");
                tensor3.Print();

                Debug.WriteLine("Tensor sum over 0");
                tensor3.SumOver(0, DefaultProviders.Double).Print();

                Debug.WriteLine("Tensor sum over 1");
                tensor3.SumOver(1, DefaultProviders.Double).Print();

                Debug.WriteLine("Tensor sum over 2");
                tensor3.SumOver(2, DefaultProviders.Double).Print();
            }
        }
    }
}
