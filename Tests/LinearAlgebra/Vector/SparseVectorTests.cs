﻿using LinearNet.Providers.LinearAlgebra;
using LinearNet.Structs;
using System;
using System.Diagnostics;

namespace LinearNet.Tests.LinearAlgebra.Vector
{
    public static class SparseVectorTests
    {
        public static void RunAll(bool verbose)
        {
            CSRMatrixMultiply(verbose);
            DotProductTest();
            EqualsDenseTest();
            Select(verbose);
        }
        private static void CSRMatrixMultiply(bool verbose)
        {
            Random r = new Random();

            int dim = 1000;
            CSRSparseMatrix<int> matrix = CSRSparseMatrix.Random(dim, dim, 0.01, () => r.Next(3) - 1);
            SparseVector<int> svect = SparseVector.Random(dim, 0.01, () => r.Next(3) - 1);
            BigSparseVector<int> bsvect = new BigSparseVector<int>(svect);
            DenseVector<int> dvect = new DenseVector<int>(svect);

            Stopwatch sw = new Stopwatch();
            sw.Restart();
            SparseVector<int> v1 = matrix.Multiply(svect, DefaultProviders.Int32);
            sw.Stop();
            double sparsetime = sw.ElapsedMilliseconds;

            sw.Restart();
            DenseVector<int> v2 = matrix.Multiply(dvect, DefaultProviders.Int32);
            sw.Stop();
            double densetime = sw.ElapsedMilliseconds;

            sw.Restart();
            BigSparseVector<int> v3 = matrix.Multiply(bsvect, DefaultProviders.Int32);
            sw.Stop();
            double bigsparsetime = sw.ElapsedMilliseconds;

            if (TestUtil.AssertVectorsEqual(v1, v2))
            {
                if (verbose)
                {
                    Debug.WriteLine($"[success]\tSparseVectorTests.CSRMatrixMultiply passed in {sparsetime} ms vs {densetime} ms for dense variant.");
                }
            }
            else
            {
                Debug.WriteLine("[failure]\tSparseVectorTests.CSRMatrixMultiply failed.");
            }
        }
        private static void DotProductTest()
        {
            Random r = new Random();
            Stopwatch sw = new Stopwatch();
            sw.Start();

            for (int i = 0; i < 100; ++i)
            {
                int dim = r.Next(1, 100);
                SparseVector<double> v1 = SparseVector.Random<double>(dim, r.NextDouble()),
                    v2 = SparseVector.Random<double>(dim, r.NextDouble());
                DenseVector<double> d1 = new DenseVector<double>(v1),
                    d2 = new DenseVector<double>(v2);

                Debug.Assert(Util.ApproximatelyEquals(v1.Dot(v2, BLAS.Double), d1.Dot(d2, BLAS.Double)));
            }
            sw.Stop();
            Debug.WriteLine($"[success]\tSparseVectorTests.DotProductTest passed in {sw.ElapsedMilliseconds} ms");
        }
        private static void EqualsDenseTest()
        {
            Random rand = new Random();
            Stopwatch sw = new Stopwatch();
            sw.Start();

            for (int i = 0; i < 100; ++i)
            {
                SparseVector<double> v = SparseVector.Random<double>(rand.Next(1, 100), 0.5);
                DenseVector<double> dv = new DenseVector<double>(v);

                if (rand.NextDouble() < 0.5)
                {
                    dv[rand.Next(dv.Dimension)] += 0.1 + rand.NextDouble();
                    if (v.Equals(dv, (u, v) => Util.ApproximatelyEquals(u, v)))
                    {
                        Debug.WriteLine("Sparse");
                        v.Print();

                        Debug.WriteLine("Dense");
                        dv.Print();

                        Debug.Assert(false);
                    }
                }
                else
                {
                    if (!v.Equals(dv, (u, v) => Util.ApproximatelyEquals(u, v)))
                    {
                        Debug.WriteLine("Sparse");
                        v.Print();

                        Debug.WriteLine("Dense");
                        dv.Print();

                        Debug.Assert(false);
                    }
                }
            }

            sw.Stop();
            Debug.WriteLine($"[success]\tSparseVectorTests.EqualsDenseTest passed in {sw.ElapsedMilliseconds} ms");
        }
        private static void Select(bool verbose)
        {
            Random r = new Random();
            int dim = 100;
            SparseVector<double> vect = SparseVector.Random<double>(dim, 0.8);
            int[] selection = RectangularVector.Random<int>(10, () => r.Next(dim));

            SparseVector<double> selected = (SparseVector<double>)vect.Select(selection);

            for (int i = 0; i < selected.Dimension; ++i)
            {
                Debug.Assert(selected[i] == vect[selection[i]]);
            }
            Debug.WriteLine($"[success]\tSparseVectorTests.Select passed.");
        }
    }
}
