﻿using LinearNet.LinearAlgebra;
using LinearNet.Solvers;
using LinearNet.Structs;
using LinearNet.Structs.Fields;
using LinearNet.Symbolic;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Tests
{
    public static class PolynomialTests
    {
        public static void RunAll(bool verbose = false)
        {
            //MultivariateInitializationTests();
            //MultivariateParseTest();

            // Sparse multivariate polynomial tests
            MultivariateAddTest(verbose);
            MultivariateMultiplyTest(verbose);
            MultivariateSubstitutionTest(verbose);
            MultivariateDerivativeTest(verbose);
            MultivariateIntegrateTest(verbose);
            MultivariatePowerTest(verbose);

            // Sparse univariate polynomial tests
            SparseAddTest(verbose);
            SparseMultiplyTest(verbose);
            SparseDerivativeTest(verbose);
            SparseIntegralTest(verbose);
            SparsePowerTest(verbose);
            SparseDivisionTest(verbose);
            SparseGCDTest(verbose);
            SquareFreeFactorTest(verbose);
            SparseCompositionTest(verbose);
            ConvertExprTest(verbose);

            // Dense polynomial tests
            DenseAdd(verbose);
            DenseSubtract(verbose);
            DenseMultiply(verbose);
            DensePower(verbose);
            DenseCompose(verbose);
        }


        public static void RootFinderTests()
        {
            int n = 20;

            DensePolynomial<Complex> px = new DensePolynomial<Complex>(1);
            for (double root = -2; root < 2; root += 2.0 / n)
            {
                px = px.Multiply(new DensePolynomial<Complex>(1, root));
            }

            Debug.WriteLine(px);

            AberthMethodSolver solver = new AberthMethodSolver(10000);

            Stopwatch sw = new Stopwatch();
            sw.Start();
            Complex[] roots = solver.Solve(px);
            sw.Stop();

            foreach (Complex root in roots)
            {
                Debug.WriteLine(root);
            }
            Debug.WriteLine("Time:\t" + sw.ElapsedMilliseconds);
        }


        #region Multivariate tests

        public static void MultivariateInitializationTests()
        {
            string[] indeterminates = { "x", "y", "z" };

            List<Tuple<int[], double>> coeffs = new List<Tuple<int[], double>>()
            {
                new Tuple<int[], double>(new int[] { 0, 0, 0 }, 5.0),
                new Tuple<int[], double>(new int[] { 1, 0, 0 }, -6.0),
                new Tuple<int[], double>(new int[] { 0, 1, 0 }, 11.0),
                new Tuple<int[], double>(new int[] { 1, 1, 1 }, 2.5),
                new Tuple<int[], double>(new int[] { 0, 0, 1 }, 1.0),
                new Tuple<int[], double>(new int[] { 2, 1, 3 }, -3.0)
            };

            MultivariatePolynomial<double> p = new MultivariatePolynomial<double>(indeterminates, coeffs);
            Debug.WriteLine(p.ToString());
        }

        private static void MultivariateAddTest<T>(MultivariatePolynomial<T> p, MultivariatePolynomial<T> q, MultivariatePolynomial<T> sum, bool verbose) where T : new()
        {
            MultivariatePolynomial<T> pq = p.Add(q);
            bool equals = pq.ApproximatelyEquals(sum, Precision.DOUBLE_PRECISION);
            if (!equals)
            {
                Debug.Assert(equals, "multivariate addition test failed");
                Debug.WriteLine("p(.):\t" + p);
                Debug.WriteLine("q(.):\t" + q);
                Debug.WriteLine("Correct sum:\t" + sum);
                Debug.WriteLine("Calculated sum:\t" + pq);
            }
            else
            {
                if (verbose)
                {
                    Debug.WriteLine("[success] multivariate addition:\t" + p + " + " + q);
                }
            }
        }
        public static void MultivariateAddTest(bool verbose = false)
        {
            MultivariateAddTest(
                new MultivariatePolynomial<double>(new string[] { "x" },
                    new List<Tuple<int[], double>>()
                    {
                        new Tuple<int[], double>(new int[] { 0 }, 5.0),
                        new Tuple<int[], double>(new int[] { 1 }, -6.0),
                    }),

                new MultivariatePolynomial<double>(new string[] { "x", "y" },
                    new List<Tuple<int[], double>>()
                    {
                        new Tuple<int[], double>(new int[] { 0, 0 }, 1.0),
                        new Tuple<int[], double>(new int[] { 1, 0 }, -1.0),
                        new Tuple<int[], double>(new int[] { 0, 1 }, 2.0),
                        new Tuple<int[], double>(new int[] { 1, 1 }, 1.0),
                        new Tuple<int[], double>(new int[] { 2, 1 }, 1.0),
                        new Tuple<int[], double>(new int[] { 2, 2 }, 1.0),
                    }),

                new MultivariatePolynomial<double>(new string[] { "x", "y" },
                    new List<Tuple<int[], double>>()
                    {
                        new Tuple<int[], double>(new int[] { 0, 0 }, 6.0),
                        new Tuple<int[], double>(new int[] { 1, 0 }, -7.0),
                        new Tuple<int[], double>(new int[] { 0, 1 }, 2.0),
                        new Tuple<int[], double>(new int[] { 1, 1 }, 1.0),
                        new Tuple<int[], double>(new int[] { 2, 1 }, 1.0),
                        new Tuple<int[], double>(new int[] { 2, 2 }, 1.0),
                    }),
                verbose);
        }

        private static void MultivariateMultiplyTest<T>(MultivariatePolynomial<T> p, MultivariatePolynomial<T> q, MultivariatePolynomial<T> prod, bool verbose) where T : new()
        {
            MultivariatePolynomial<T> pq = p.Multiply(q);
            bool equals = pq.ApproximatelyEquals(prod, Precision.DOUBLE_PRECISION);
            if (!equals)
            {
                Debug.WriteLine("p(.):\t" + p);
                Debug.WriteLine("q(.):\t" + q);
                Debug.WriteLine("Correct product:\t" + prod);
                Debug.WriteLine("Calculated product:\t" + pq);
                Debug.Assert(equals, "multivariate multiply test failed");
            }
            else
            {
                if (verbose)
                {
                    Debug.WriteLine($"[success] multivariate multiplication:\t({p})({q})");
                }
            }
        }
        public static void MultivariateMultiplyTest(bool verbose = false)
        {
            MultivariateMultiplyTest(
                MultivariatePolynomial.ParseBigRational("x + 1"),
                MultivariatePolynomial.ParseBigRational("y + 1"),
                MultivariatePolynomial.ParseBigRational("xy + x + y + 1"), verbose);

            MultivariateMultiplyTest(
                MultivariatePolynomial.ParseBigRational("x + 1"),
                MultivariatePolynomial.ParseBigRational("x - 1"),
                MultivariatePolynomial.ParseBigRational("x^2 - 1"), verbose);
        }

        public static void MultivariateParseTest()
        {
            MultivariatePolynomial<BigRational> p = MultivariatePolynomial.ParseBigRational("x^3 + 2x^2y^2 + y + x + 1");
            Debug.WriteLine(p);
        }

        private static void MultivariateSubsTest<T>(MultivariatePolynomial<T> p, Dictionary<string, T> substitutions, MultivariatePolynomial<T> result, bool verbose) where T : new()
        {
            MultivariatePolynomial<T> subbed = p.Substitute(substitutions);
            bool equals = subbed.ApproximatelyEquals(result, Precision.DOUBLE_PRECISION);
            if (!equals)
            {
                Debug.Assert(equals, "multivariate substitution test failed");
                Debug.WriteLine("p(.):\t" + p);
                Debug.WriteLine("substitutions: " + substitutions);
                Debug.WriteLine("Correct result:\t" + result);
                Debug.WriteLine("Calculated result:\t" + subbed);
            }
            else
            {
                if (verbose)
                {
                    Debug.WriteLine($"[success] multivariate substituton:\t{p} -> {result}");
                }
            }
        }
        public static void MultivariateSubstitutionTest(bool verbose = false)
        {
            MultivariateSubsTest(
                    MultivariatePolynomial.ParseBigRational("x^2 + y^2 + x + y + 1"),
                    new Dictionary<string, BigRational>() { { "y", 3 } },
                    MultivariatePolynomial.ParseBigRational("x^2 + x + 13"), verbose);

            MultivariateSubsTest(
                    MultivariatePolynomial.ParseBigRational("x^3 + x^2y^2 + x + y + 1"),
                    new Dictionary<string, BigRational>() { { "x", 2 }, { "y", 3 } },
                    MultivariatePolynomial.ParseBigRational("50"), verbose);
        }

        private static void MultivariateDerivativeTest(string polynomial, string variable, string diff, bool verbose)
        {
            MultivariatePolynomial<BigRational> p = MultivariatePolynomial.ParseBigRational(polynomial);
            MultivariatePolynomial<BigRational> derivative = MultivariatePolynomial.ParseBigRational(diff);
            MultivariatePolynomial<BigRational> d = p.Differentiate(variable);
            bool equals = d.Equals(derivative);
            if (!equals)
            {
                Debug.WriteLine($"[failure] Derivative calculation failed for d/d{variable}({p})");
                Debug.WriteLine("Correct derivative:\t" + derivative);
                Debug.WriteLine("Calculated derivative:\t" + d);
                Debug.Assert(false, "multivariate differentiation test failed");
            }
            else
            {
                if (verbose)
                {
                    Debug.WriteLine("[success] multivariate derivative:\t" + p + " -> " + d);
                }
            }
        }
        public static void MultivariateDerivativeTest(bool verbose = false)
        {
            MultivariateDerivativeTest("x^2 + x + 1", "x", "2x + 1", verbose);
            MultivariateDerivativeTest("x^2y^2 + xy + y + 1", "y", "2x^2y + x + 1", verbose);
        }

        private static void MultivariateIntegrateTest(string polynomial, string variable, BigRational constant, string diff, bool verbose)
        {
            MultivariatePolynomial<BigRational> p = MultivariatePolynomial.ParseBigRational(polynomial);
            MultivariatePolynomial<BigRational> integral = MultivariatePolynomial.ParseBigRational(diff);

            MultivariatePolynomial<BigRational> i = p.Integrate(variable, constant);
            bool equals = i.Equals(integral);
            if (!equals)
            {
                Debug.WriteLine($"[failure] integral calculation failed for int[{variable}({p})]");
                Debug.WriteLine("Correct integral:\t" + integral);
                Debug.WriteLine("Calculated integral:\t" + i);
                Debug.Assert(false, "multivariate integration test failed");
            }
            else
            {
                if (verbose)
                {
                    Debug.WriteLine("[success] multivariate integration:\t" + p + " -> " + i);
                }
            }
        }
        public static void MultivariateIntegrateTest(bool verbose = false)
        {
            MultivariateIntegrateTest("3x^2 + 2x + 1", "y", 5, "3x^2y + 2xy + y + 5", verbose);
            MultivariateIntegrateTest("3x^2y + 2xy + 1", "x", 5, "x^3y + x^2y + x + 5", verbose);
        }

        private static void MultivariatePowerTest(string polynomial, int power, string result, bool verbose)
        {
            MultivariatePolynomial<BigRational> p = MultivariatePolynomial.ParseBigRational(polynomial);
            MultivariatePolynomial<BigRational> pow = p.Pow(power);
            MultivariatePolynomial<BigRational> answer = MultivariatePolynomial.ParseBigRational(result);
            bool equals = pow.Equals(answer);

            if (!equals)
            {
                Debug.WriteLine($"[failure] power calculation failed for ({p})^{power}");
                Debug.WriteLine("Correct power:\t" + answer);
                Debug.WriteLine("Calculated power:\t" + pow);
                Debug.Assert(false, "multivariate power test failed");
            }
            else
            {
                if (verbose)
                {
                    Debug.WriteLine("[success] multivariate power:\t" + p + " -> " + pow);
                }
            }
        }
        public static void MultivariatePowerTest(bool verbose = false)
        {
            MultivariatePowerTest("a + b + c", 0, "1", verbose);
            MultivariatePowerTest("x^2 + xy + y^2", 1, "x^2 + xy + y^2", verbose);
            MultivariatePowerTest("x + 1", 3, "x^3 + 3x^2 + 3x + 1", verbose);
            MultivariatePowerTest("x + y", 4, "x^4 + 4x^3y + 6x^2y^2 + 4xy^3 + y^4", verbose);
        }

        #endregion


        #region Sparse univariate tests

        private static void SparseAddTest(string pstr, string qstr, string sumstr, bool verbose)
        {
            Polynomial<BigRational>
                p = Polynomial.ParseBigRational(pstr),
                q = Polynomial.ParseBigRational(qstr),
                sum = Polynomial.ParseBigRational(sumstr);

            Polynomial<BigRational> pq = p + q;
            bool equals = pq.ApproximatelyEquals(sum, Precision.DOUBLE_PRECISION);
            if (!equals)
            {
                Debug.WriteLine("p(x):\t" + p);
                Debug.WriteLine("q(x):\t" + q);
                Debug.WriteLine("Correct sum:\t" + sum);
                Debug.WriteLine("Calculated sum:\t" + pq);
                Debug.Assert(equals, "sparse univariate addition test failed");
            }
            else
            {
                if (verbose)
                {
                    Debug.WriteLine("[success] sparse univariate addition:\t(" + p + ") + (" + q + ")");
                }
            }
        }
        public static void SparseAddTest(bool verbose = false)
        {
            SparseAddTest("x", "x^2 + x + 1", "x^2 + 2x + 1", verbose);
            SparseAddTest("x - 1", "1", "x", verbose);
            SparseAddTest("-x^2 + x - 1", "1 - x + x^2", "0", verbose);
        }

        private static void SparseMultiplyTest(string pstr, string qstr, string prodstr, bool verbose)
        {
            Polynomial<BigRational>
                p = Polynomial.ParseBigRational(pstr),
                q = Polynomial.ParseBigRational(qstr),
                prod = Polynomial.ParseBigRational(prodstr);

            Polynomial<BigRational> pq = p.Multiply(q);
            bool equals = pq.ApproximatelyEquals(prod, Precision.DOUBLE_PRECISION);
            if (!equals)
            {
                Debug.WriteLine("p(.):\t" + p);
                Debug.WriteLine("q(.):\t" + q);
                Debug.WriteLine("Correct product:\t" + prod);
                Debug.WriteLine("Calculated product:\t" + pq);
                Debug.Assert(equals, "sparse univariate multiply test failed");
            }
            else
            {
                if (verbose)
                {
                    Debug.WriteLine($"[success] sparse univariate multiply:\t({p})({q})");
                }
            }
        }
        public static void SparseMultiplyTest(bool verbose = false)
        {
            SparseMultiplyTest("x + 1", "x + 1", "x^2 + 2x + 1", verbose);
            SparseMultiplyTest("x^2 - 2x + 1", "x - 1", "x^3 - 3x^2 + 3x - 1", verbose);
            SparseMultiplyTest("0", "-x - 1", "0", verbose);
            SparseMultiplyTest("1", "-x - 1", "-x - 1", verbose);
        }

        private static void SparseDerivativeTest(string pstr, string dstr, bool verbose)
        {
            Polynomial<BigRational> p = Polynomial.ParseBigRational(pstr),
                derivative = Polynomial.ParseBigRational(dstr);

            Polynomial<BigRational> d = p.Differentiate();
            bool equals = d.ApproximatelyEquals(derivative, Precision.DOUBLE_PRECISION);
            if (!equals)
            {
                Debug.WriteLine("p(x):\t" + p);
                Debug.WriteLine("Correct derivative:\t" + derivative);
                Debug.WriteLine("Calculated derivative:\t" + d);
                Debug.Assert(equals, "sparse univariate derivative test failed");
            }
            else
            {
                if (verbose)
                {
                    Debug.WriteLine("[success] sparse univariate derivative:\t d(" + p + ")/dx -> " + d + "");
                }
            }
        }
        public static void SparseDerivativeTest(bool verbose = false)
        {
            SparseDerivativeTest("x^3 + x^2 + x + 1", "3x^2 + 2x + 1", verbose);
            SparseDerivativeTest("5", "0", verbose);
        }

        private static void SparseIntegralTest(string pstr, BigRational constant, string istr, bool verbose)
        {
            Polynomial<BigRational> p = Polynomial.ParseBigRational(pstr),
                integral = Polynomial.ParseBigRational(istr);

            Polynomial<BigRational> d = p.Integrate(constant);
            bool equals = d.ApproximatelyEquals(integral, Precision.DOUBLE_PRECISION);
            if (!equals)
            {
                Debug.WriteLine("p(x):\t" + p);
                Debug.WriteLine("Correct integral:\t" + integral);
                Debug.WriteLine("Calculated integral:\t" + d);
                Debug.Assert(equals, "sparse univariate integral test failed");
            }
            else
            {
                if (verbose)
                {
                    Debug.WriteLine("[success] sparse univariate integral:\t int(" + p + ") -> " + d + "");
                }
            }
        }
        public static void SparseIntegralTest(bool verbose = false)
        {
            SparseIntegralTest("3x^2 + 2x + 1", 5, "x^3 + x^2 + x + 5", verbose);
            SparseIntegralTest("0", -1, "-1", verbose);
            SparseIntegralTest("-x + x^4 - 3x^3 - 10", 0, "-1/2x^2 + 1/5x^5 -3/4x^4 - 10x", verbose);
        }

        private static void SparsePowerTest(string basestr, int power, string pstr, bool verbose)
        {
            Polynomial<BigRational> p = Polynomial.ParseBigRational(basestr),
                answer = Polynomial.ParseBigRational(pstr);

            Polynomial<BigRational> pow = p.Pow(power);
            bool equals = pow.ApproximatelyEquals(answer, Precision.DOUBLE_PRECISION);
            if (!equals)
            {
                Debug.WriteLine("p(x):\t" + p);
                Debug.WriteLine("Correct power:\t" + answer);
                Debug.WriteLine("Calculated power:\t" + pow);
                Debug.Assert(equals, "sparse univariate power test failed");
            }
            else
            {
                if (verbose)
                {
                    Debug.WriteLine($"[success] sparse univariate power:\t({p})^{power} -> {pow}");
                }
            }
        }
        public static void SparsePowerTest(bool verbose = false)
        {
            SparsePowerTest("x + 1", 5, "x^5 + 5x^4 + 10x^3 + 10x^2 + 5x + 1", verbose);
            SparsePowerTest("x^2 + x + 1", 1, "x^2 + x + 1", verbose);
            SparsePowerTest("-x^2 + x - 1", 0, "1", verbose);
        }

        private static void SparseDivisionTest(string pstr, string qstr, string quotientstr, string remainstr, bool verbose)
        {
            Polynomial<BigRational> p = Polynomial.ParseBigRational(pstr),
                q = Polynomial.ParseBigRational(qstr),
                quotient = Polynomial.ParseBigRational(quotientstr),
                remain = Polynomial.ParseBigRational(remainstr);

            Polynomial<BigRational> _quotient = p.Divide(q, out Polynomial<BigRational> _remain);
            bool equals = _quotient.ApproximatelyEquals(quotient, Precision.DOUBLE_PRECISION) &&
                _remain.ApproximatelyEquals(remain, Precision.DOUBLE_PRECISION);
            if (!equals)
            {
                Debug.WriteLine("p(x):\t" + p);
                Debug.WriteLine("q(x):\t" + q);
                Debug.WriteLine("Correct qoutient:\t" + quotient);
                Debug.WriteLine("Calculated qoutient:\t" + _quotient);
                Debug.WriteLine("Correct remainder:\t" + remain);
                Debug.WriteLine("Calculated remainder:\t" + _remain);
                Debug.Assert(equals, "sparse univariate divide test failed");
            }
            else
            {
                if (verbose)
                {
                    Debug.WriteLine($"[success] sparse univariate divide:\t({p}) / ({q})");
                }
            }
        }
        public static void SparseDivisionTest(bool verbose)
        {
            SparseDivisionTest("x^2 + 3x + 5", "x + 1", "x + 2", "3", verbose);
            SparseDivisionTest("x^3 - x^2 + x - 1", "x - 1", "x^2 + 1", "0", verbose);
            SparseDivisionTest("-x + 1", "x^2 + x + 1", "0", "-x + 1", verbose);
        }

        private static void SparseGCDTest(string pstr, string qstr, string gcdstr, bool verbose)
        {
            Polynomial<BigRational> p = Polynomial.ParseBigRational(pstr),
                q = Polynomial.ParseBigRational(qstr),
                gcd = Polynomial.ParseBigRational(gcdstr);

            Polynomial<BigRational> _gcd = Polynomial.GCD(p, q);
            if (_gcd != gcd)
            {
                Debug.WriteLine("p(x):\t" + p);
                Debug.WriteLine("q(x):\t" + q);
                Debug.WriteLine("Correct GCD:\t" + gcd);
                Debug.WriteLine("Calculated GCD:\t" + _gcd);
                Debug.Assert(false, "sparse univariate gcd test failed");
            }
            else
            {
                if (verbose)
                {
                    Debug.WriteLine($"[success] sparse univariate gcd:\t({p}, {q})");
                }
            }
        }
        public static void SparseGCDTest(bool verbose)
        {
            SparseGCDTest("x^2 + 3x + 2", "x^2 + 2x + 1", "x + 1", verbose);
            SparseGCDTest("-x^2 + 3x + 2", "x^2 - 2x + 1", "1", verbose);
            SparseGCDTest("-2x^2 + 4x + 8", "2x^2 + 2x", "1", verbose);
            SparseGCDTest("0", "0", "1", verbose);
        }

        private static void SquareFreeFactorTest(string pstr, Dictionary<string, long> factors, bool verbose)
        {
            Polynomial<BigRational> p = Polynomial.ParseBigRational(pstr);
            var answer = new Dictionary<Polynomial<BigRational>, long>();
            foreach (string str in factors.Keys)
            {
                answer.Add(Polynomial.ParseBigRational(str), factors[str]);
            }

            var factor = new TobeyHorowitzFactorization<BigRational>();
            var result = factor.Factor(p);

            bool equals = true;
            if (answer.Count != result.Factors.Count)
            {
                equals = false;
            }
            else
            {
                foreach (Polynomial<BigRational> f in answer.Keys)
                {
                    if (!result.Factors.ContainsKey(f))
                    {
                        equals = false;
                        break;
                    }
                    if (result.Factors[f] != answer[f])
                    {
                        equals = false;
                        break;
                    }
                }
            }

            if (!equals)
            {
                Debug.WriteLine("p(x):\t" + p);
                Debug.Assert(false, "sparse univariate square-free factor test failed");
            }
            else
            {
                if (verbose)
                {
                    Debug.WriteLine($"[success] sparse square-free factor:\t{p}");
                }
            }
        }
        public static void SquareFreeFactorTest(bool verbose = false)
        {
            SquareFreeFactorTest("x^2 + 2x + 1",
                new Dictionary<string, long>() { { "x + 1", 2L } }, 
                verbose);

            SquareFreeFactorTest("x^3 - 3x^2 + 3x - 1",
                new Dictionary<string, long>() { { "x - 1", 3L } },
                verbose);

            SquareFreeFactorTest("x^2  + x - 1",
                new Dictionary<string, long>() { { "x^2  + x - 1", 1L } },
                verbose);
        }

        private static void SparseCompositionTest(string pstr, string qstr, string poqstr, bool verbose)
        {
            Polynomial<BigRational> p = Polynomial.ParseBigRational(pstr),
                q = Polynomial.ParseBigRational(qstr),
                poq = Polynomial.ParseBigRational(poqstr);

            Polynomial<BigRational> _poq = p.Of(q);
            if (_poq != poq)
            {
                Debug.WriteLine("p(x):\t" + p);
                Debug.WriteLine("q(x):\t" + q);
                Debug.WriteLine("Correct p(q(x)):\t" + poq);
                Debug.WriteLine("Calculated p(q(x)):\t" + _poq);
                Debug.Assert(false, "sparse univariate compose test failed");
            }
            else
            {
                if (verbose)
                {
                    Debug.WriteLine($"[success] sparse univariate compose:\t({p}, {q})");
                }
            }
        }
        public static void SparseCompositionTest(bool verbose = false)
        {
            SparseCompositionTest("x^2 + x + 1", "-x^2", "x^4 - x^2 + 1", verbose);
            SparseCompositionTest("3x^2 + 2x + 1", "-1", "2", verbose);
        }

        private static void ConvertExprTest(bool verbose = false)
        {
            if (verbose)
            {
                Debug.WriteLine(new Expr(new Polynomial<BigRational>(1, -3, 3, -1)));
                Debug.WriteLine(new Expr(new Polynomial<BigRational>(0)));
                Debug.WriteLine(new Expr(new Polynomial<BigRational>(2, 0, 0, 0)));
            }
        }

        #endregion


        #region Dense univariate tests

        private static void DenseAdd<T>(DensePolynomial<T> p, DensePolynomial<T> q, DensePolynomial<T> sum, bool verbose) where T : new()
        {
            DensePolynomial<T> _sum = p.Add(q);
            if (_sum != sum)
            {
                Debug.WriteLine("p(x):\t" + p);
                Debug.WriteLine("q(x):\t" + q);
                Debug.WriteLine("Correct sum:\t" + sum);
                Debug.WriteLine("Calculated sum:\t" + _sum);
                Debug.Assert(false, "dense univariate addition test failed");
            }
            else
            {
                if (verbose)
                {
                    Debug.WriteLine("[success] dense univariate addition:\t(" + p + ") + (" + q + ")");
                }
            }
        }
        public static void DenseAdd(bool verbose = false)
        {
            DenseAdd(new DensePolynomial<BigRational>(1, 2, 3), new DensePolynomial<BigRational>(-1, 2, -2), new DensePolynomial<BigRational>(4, 1), verbose);
        }

        private static void DenseSubtract<T>(DensePolynomial<T> p, DensePolynomial<T> q, DensePolynomial<T> sum, bool verbose) where T : new()
        {
            DensePolynomial<T> _sum = p.Subtract(q);
            if (_sum != sum)
            {
                Debug.WriteLine("p(x):\t" + p);
                Debug.WriteLine("q(x):\t" + q);
                Debug.WriteLine("Correct difference:\t" + sum);
                Debug.WriteLine("Calculated difference:\t" + _sum);
                Debug.Assert(false, "dense univariate subtract test failed");
            }
            else
            {
                if (verbose)
                {
                    Debug.WriteLine("[success] dense univariate subtract:\t(" + p + ") - (" + q + ")");
                }
            }
        }
        public static void DenseSubtract(bool verbose = false)
        {
            DenseSubtract(new DensePolynomial<BigRational>(1, -2, 2), new DensePolynomial<BigRational>(1, 1, -2), new DensePolynomial<BigRational>(-3, 4), verbose);
        }

        private static void DenseMultiply<T>(DensePolynomial<T> p, DensePolynomial<T> q, DensePolynomial<T> pq, bool verbose) where T : new()
        {
            //DensePolynomial<T> _pq = p.Multiply(q);
            DensePolynomial<T> _pq = p.Multiply(q);
            if (pq != _pq)
            {
                Debug.WriteLine("p(x):\t" + p);
                Debug.WriteLine("q(x):\t" + q);
                Debug.WriteLine("Correct product:\t" + pq);
                Debug.WriteLine("Calculated product:\t" + _pq);
                Debug.Assert(false, "dense univariate product test failed");
            }
            else
            {
                if (verbose)
                {
                    Debug.WriteLine("[success] dense univariate multiply:\t(" + p + ")(" + q + ")");
                }
            }
        }
        public static void DenseMultiply(bool verbose = false)
        {
            DenseMultiply(new DensePolynomial<BigRational>(1, 1), new DensePolynomial<BigRational>(1, 1), new DensePolynomial<BigRational>(1, 2, 1), verbose);
            DenseMultiply(new DensePolynomial<BigRational>(3, 1), new DensePolynomial<BigRational>(5, -10, 0), new DensePolynomial<BigRational>(15, -25, -10, 0), verbose);
        }

        private static void DensePower<T>(DensePolynomial<T> p, int exp, DensePolynomial<T> pow, bool verbose) where T : new()
        {
            DensePolynomial<T> _pow = p.Pow(exp);
            if (_pow != pow)
            {
                Debug.WriteLine("p(x):\t" + p);
                Debug.WriteLine("Correct power:\t" + pow);
                Debug.WriteLine("Calculated power:\t" + _pow);
                Debug.Assert(false, "dense univariate power test failed");
            }
            else
            {
                if (verbose)
                {
                    Debug.WriteLine($"[success] dense univariate power:\t({p})^{exp}");
                }
            }
        }
        public static void DensePower(bool verbose = false)
        {
            DensePower(new DensePolynomial<BigRational>(1, -2), 0, new DensePolynomial<BigRational>(1), verbose);
            DensePower(new DensePolynomial<BigRational>(1, -2), 1, new DensePolynomial<BigRational>(1, -2), verbose);
            DensePower(new DensePolynomial<BigRational>(1, -1), 4, new DensePolynomial<BigRational>(1, -4, 6, -4, 1), verbose);
            DensePower(new DensePolynomial<BigRational>(3, 2, 1), 3, new DensePolynomial<BigRational>(27, 54, 63, 44, 21, 6, 1), verbose);
            DensePower(
                new DensePolynomial<BigRational>(-1, -5, 0, 2, 0), 4, 
                new DensePolynomial<BigRational>(1, 20, 150, 492, 505, -600, -976, 240, 600, -32, -160, 0, 16, 0, 0, 0, 0), verbose);
        }

        private static void DenseCompose<T>(DensePolynomial<T> p, DensePolynomial<T> q, DensePolynomial<T> poq, bool verbose) where T : new()
        {
            DensePolynomial<T> _poq = p.Of(q);
            if (_poq != poq)
            {
                Debug.WriteLine("p(x):\t" + p);
                Debug.WriteLine("Correct p(q(x)):\t" + poq);
                Debug.WriteLine("Calculated p(q(x)):\t" + _poq);
                Debug.Assert(false, "dense univariate compose test failed");
            }
            else
            {
                if (verbose)
                {
                    Debug.WriteLine($"[success] dense univariate compose:\t({p}, {q})");
                }
            }
        }
        public static void DenseCompose(bool verbose = false)
        {
            DenseCompose(new DensePolynomial<BigRational>(1, 2, 1), new DensePolynomial<BigRational>(2, 0, 0), new DensePolynomial<BigRational>(4, 0, 4, 0, 1), verbose);
        }

        #endregion
    }
}
