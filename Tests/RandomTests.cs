﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Tests
{
    public static class RandomTests
    {
        public static void TestDictionaryAccessPerformance()
        {
            Random r = new Random();
            Dictionary<int, double> values = new Dictionary<int, double>();
            for (int i = 0; i < 4e7; ++i)
            {
                values[i] = r.NextDouble();
            }

            Stopwatch sw = new Stopwatch();
            sw.Start();
            foreach (KeyValuePair<int, double> pair in values)
            {
                int i = pair.Key;
                double s = pair.Value;
            }
            sw.Stop();
            Debug.WriteLine("By key-value pair access:\t" + sw.ElapsedMilliseconds + " ms");

            sw.Restart();
            foreach (int key in values.Keys)
            {
                int i = key;
                double s = values[key];
            }
            sw.Stop();
            Debug.WriteLine("By key access:\t" + sw.ElapsedMilliseconds + " ms");
        }
    }
}
