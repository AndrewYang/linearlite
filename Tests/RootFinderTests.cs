﻿using LinearNet.Optimisation;
using LinearNet.Solvers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Tests
{
    public static class RootFinderTests
    {
        public static void RunDekker(bool verbose = false)
        {
            Function<double[], double> f =
                new Func<double[], double>(
                    x =>
                    {
                        double _x = x[0];
                        return (_x + 3) * Math.Pow(_x - 1, 3);
                    });


            IRootFindingAlgorithm<double[], double> algo = new DekkerRootFindingAlgorithm(0, 2);
            Solution<double[]> root = algo.Solve(f);
            if (verbose)
                Debug.WriteLine("Root(1) using Dekker method:\t" + root.Result[0]);

            IRootFindingAlgorithm<double[], double> algo1 = new DekkerRootFindingAlgorithm(-4, -2);
            Solution<double[]> root3 = algo1.Solve(f);
            if (verbose)
                Debug.WriteLine("Root(3) using Dekker method:\t" + root3.Result[0]);

            Debug.Assert(Util.ApproximatelyEquals(root.Result[0], 1, 1e-6));
            Debug.Assert(Util.ApproximatelyEquals(root3.Result[0], -3, 1e-6));
        }

        internal static void RunBisection(bool verbose = false)
        {
            Function<double[], double> f =
                new Func<double[], double>(
                    x =>
                    {
                        double _x = x[0];
                        return (_x + 3) * Math.Pow(_x - 1, 3);
                    });


            IRootFindingAlgorithm<double[], double> algo = 
                new BisectionRootFindingAlgorithm(new double[] { 0 }, new double[] { 2 });
            Solution<double[]> root = algo.Solve(f);

            if (verbose)
            {
                Debug.WriteLine("Root(1) using bisection method:\t" + root.Result[0]);
            }
            Debug.Assert(Util.ApproximatelyEquals(root.Result[0], 1, 1e-6));
        }
        
        internal static void RunBrentsMethod(bool verbose = false)
        {
            Function<double[], double> f =
                new Func<double[], double>(
                    x =>
                    {
                        double _x = x[0];
                        return (_x + 3) * Math.Pow(_x - 1, 2);
                    });

            IRootFindingAlgorithm<double[], double> algo = new BrentsRootFindingAlgorithm(new double[] { -4 }, new double[] { 4.0 / 3 });
            Solution<double[]> root = algo.Solve(f);

            if (verbose)
            {
                Debug.WriteLine("Root(1) using Brent's method:\t" + root.Result[0]);
            }
            Debug.Assert(Util.ApproximatelyEquals(root.Result[0], -3, 1e-6));
        }

        internal static void RunNewtonsMethod(bool verbose = false)
        {
            Function<double, double> f =
                new Func<double, double>(
                    x =>
                    {
                        double _x = x;
                        return (_x + 3) * Math.Pow(_x - 1, 2);
                    });

            IRootFindingAlgorithm<double, double> algo = new NewtonRootFindingAlgorithm(0.0);
            Solution<double> root = algo.Solve(f);

            if (verbose)
            {
                Debug.WriteLine("Root(1) using Newton's method:\t" + root.Result);
            }
            Debug.Assert(Util.ApproximatelyEquals(root.Result, 1, 1e-6));
        }
    }
}
