﻿#if NETCOREAPP3_0

using LinearNet.Providers.LinearAlgebra;
using LinearNet.Structs;
using System;
using System.Diagnostics;
using System.Reflection;
using LinearNet.Providers;
using LinearNet.Providers.AVX2;

namespace LinearNet.Tests
{
    internal static class SIMDTests
    {
        internal const bool VERBOSE = true;

        public static void RunAll()
        {
            AXPY();
            DOT();
        }

        private static void TestAXPY<T>(T[] y, T[] x, T alpha, Func<T[], T[], bool> Equals, params IDenseBLAS1<T>[] providers)
        {
            if (providers.Length == 0) return;

            Random r = new Random();
            int start = r.Next(x.Length - 1), end = r.Next(x.Length - start) + start + 1;

            T[] y0 = y.Copy();
            providers[0].AXPY(y0, x, alpha, start, end);

            for (int i = 1; i < providers.Length; ++i)
            {
                IDenseBLAS1<T> provider = providers[i];
                T[] _y = y.Copy();
                provider.AXPY(_y, x, alpha, start, end);

                if (!Equals(y0, _y))
                {
                    Debug.WriteLine(i);
                    Debug.WriteLine("y0");
                    y0.Print();
                    Debug.WriteLine("y");
                    _y.Print();
                    Debug.Assert(false);
                }
                else if (VERBOSE)
                {
                    Debug.WriteLine(MethodBase.GetCurrentMethod().Name + "<" + typeof(T).Name + "> passed.");
                }
            }
        }
        internal static void AXPY()
        {
            if (true)
            {
                double[] y = RectangularVector.Random<double>(10);
                double[] x = RectangularVector.Random<double>(10);
                Random r = new Random();
                double alpha = r.NextDouble();

                TestAXPY(y, x, alpha, (_x, _y) => _x.ApproximatelyEquals(_y),
                    new NativeDoubleProvider(), new AVX2DoubleProvider());
            }

            if (true)
            {
                float[] y = RectangularVector.Random<float>(10);
                float[] x = RectangularVector.Random<float>(10);
                Random r = new Random();
                float alpha = (float)r.NextDouble();

                TestAXPY(y, x, alpha, (_x, _y) => _x.ApproximatelyEquals(_y),
                    new NativeFloatProvider(), new AVX2FloatProvider());
            }
            if (true)
            {
                Complex[] y = RectangularVector.Random<Complex>(10);
                Complex[] x = RectangularVector.Random<Complex>(10);
                Random r = new Random();
                Complex alpha = new Complex(r.NextDouble(), r.NextDouble());

                TestAXPY(y, x, alpha, (_x, _y) => _x.ApproximatelyEquals(_y),
                    new NativeComplexProvider(), new AVX2ComplexProvider());
            }
        }

        private static void TestDot<T>(T[] y, T[] x, Func<T, T, bool> Equals, IDenseBLAS1<T> p1, IDenseBLAS1<T> p2)
        {
            T dot1 = p1.DOT(x, y, 0, x.Length);
            T dot2 = p2.DOT(x, y, 0, x.Length);

            if (!Equals(dot1, dot2))
            {
                Debug.WriteLine("x");
                x.Print();
                Debug.WriteLine("y");
                y.Print();
                Debug.WriteLine(dot1 + "\t" + dot2);
                Debug.Assert(false);
            }
            else if (VERBOSE)
            {
                Debug.WriteLine(MethodBase.GetCurrentMethod().Name + "<" + typeof(T).Name + "> passed.");
            }
        }
        internal static void DOT()
        {
            if (true)
            {
                double[] x = RectangularVector.Random<double>(10);
                double[] y = RectangularVector.Random<double>(10);
                TestDot(x, y, (a, b) => a.ApproximatelyEquals(b),
                    new NativeDoubleProvider(), new AVX2DoubleProvider());
            }

            if (true)
            {
                float[] x = RectangularVector.Random<float>(10);
                float[] y = RectangularVector.Random<float>(10);
                TestDot(x, y, (a, b) => a.ApproximatelyEquals(b),
                    new NativeFloatProvider(), new AVX2FloatProvider());
            }

            if (true)
            {
                Complex[] x = RectangularVector.Random<Complex>(10);
                Complex[] y = RectangularVector.Random<Complex>(10);
                TestDot(x, y, (a, b) => a.ApproximatelyEquals(b),
                    new NativeComplexProvider(), new AVX2ComplexProvider());
            }
        }
    }
}

#endif