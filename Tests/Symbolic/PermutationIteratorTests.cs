﻿using LinearNet.Helpers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace LinearNet.Tests.Symbolic
{
    public static class PermutationIteratorTests
    {
        public static void Test()
        {
            int n = 12;
            int[] values = new int[n];
            for (int i = 0; i < n; ++i) values[i] = i + 1;

            PermutationIterator<int> itr = new PermutationIterator<int>(values);
            IEnumerable<int[]> permutations = itr.GetEnumerator();

            Stopwatch sw = new Stopwatch();
            sw.Start();
            foreach (int[] permutation in permutations)
            {

            }
            sw.Stop();
            Debug.WriteLine($"Permutation({n}) " + sw.ElapsedMilliseconds + " ms");
        }
    }
}
