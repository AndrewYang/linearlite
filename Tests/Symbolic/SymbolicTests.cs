﻿using LinearNet.Structs;
using LinearNet.Structs.Fields;
using LinearNet.Symbolic;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Tests
{
    public static class SymbolicTests
    {
        public static void RunAll(bool verbose = false)
        {
            LikeTest(verbose);
            CancelRationalsTest(verbose);
            SimplifyTest(verbose);
            DerivativeTest(verbose);
            ExpandTest(verbose);
            FactorTest(verbose);
            SubstituteTest(verbose);
            RationalTest(verbose);

            SignificantFiguresTest(false);
            ToPolynomialTest(verbose);
        }

        public static void ExpressionParserTest()
        {
            string s = "1 + 5(2 + 3(1 + 5))(1+2) + exp5 / 3 + log(4 * 5) * ln(log(2)) + log2 - x^2 + 5a - 2 xy + a(a+b)";
            Debug.WriteLine("Initial string:\t" + s);
            Expr e = Expr.Parse(s);
            Debug.WriteLine(e);
            HashSet<string> variables = e.Variables();
            Debug.WriteLine(string.Join(", ", variables));
        }

        public static void LikeTest(bool verbose = false)
        {            
            LikeTest("x", "-x", true, verbose);
            LikeTest("x", "-1 * x", true, verbose);
            LikeTest("x", "5", false, verbose);
            LikeTest("x^3", "x^2", false, verbose);
            LikeTest("1", "5", true, verbose);
            LikeTest("3xy", "-xy", true, verbose);
            LikeTest("4x^2y^7z", "-10zx^2y^7", true, verbose);
            LikeTest("4x^2y^7z", "-10z^2x^2y^7", false, verbose);
            LikeTest("3sin(x)", "-sin(x)", true, verbose);
            LikeTest("exp(x^2 + y)", "-10exp(y + x^2)", true, verbose);
            LikeTest("exp(x^2 + log(y - z^2 + exp(x)))", "5exp(log(exp(x) - z^2 + y) + x^2)", true, verbose);
        }
        private static void LikeTest(string f, string g, bool like, bool verbose = false)
        {
            Expr _f = Expr.Parse(f);
            Expr _g = Expr.Parse(g);

            if (_g.Like(_f) != like) 
            {
                Debug.WriteLine("Like terms test failed: expected [" + like + "], got [" + _g.Like(_f) + "]");
                Debug.WriteLine(_f);
                Debug.WriteLine(_g);
            }
            else
            {
                if (verbose)
                {
                    Debug.WriteLine("[success]:\tlike\t" + _f + " vs " + _g);
                }
            }
        }

        public static void SimplifyTest(bool verbose = false)
        {
            SimplifyTest("1 + 4(2 + x)", "4x + 9", verbose);
            SimplifyTest("asin(sin(x^2 + 1))", "1 + x^2", verbose);
            SimplifyTest("((a / b / (c * a)) * (c * d / a) / d)", "1 / (ab)", verbose);
            SimplifyTest("(a + b)^2 / (a + b)", "a + b", verbose);
            SimplifyTest("(x^2 - 1) / (x - 1)", "1 + x", verbose);
            SimplifyTest("(1/(exp(x)*(exp(y)+exp(-x))) - (exp(x+y)-1)/((exp(x+y))^2-1))", "0", verbose);
        }
        private static void SimplifyTest(string expr, string simplifiedExpr, bool verbose = false)
        {
            Expr f = Expr.Parse(expr);
            f = f.Simplify();

            Expr simplified = Expr.Parse(simplifiedExpr);
            if (!f.EqualsAfterSimplification(simplified))
            {
                Debug.WriteLine("[failure] simplify\t" + expr + " -> " + simplifiedExpr);
                Debug.WriteLine("\tExpression was simplified to:\t" + f.ToString());
                Debug.WriteLine("\tCorrect simplification is:\t" + simplified.ToString());
            }
            else if (verbose)
            {
                Debug.WriteLine("[success] simplify\t" + expr + " -> " + simplifiedExpr);
            }
        }

        public static void DerivativeTest(bool verbose = false)
        {
            DerivativeTest("2x^2 + x", "x", "4x + 1", verbose);
            DerivativeTest("(x^2 + 1)^3", "x", "6x(x^2 + 1)^2", verbose);
            DerivativeTest("cos(sin(x))", "x", "-sin(sin(x))cos(x)", verbose);
            DerivativeTest("tan(x + y)", "x", "1 / (cos(x + y))^2", verbose);
            DerivativeTest("ln(x/3 - x^5)", "x", "(1/3 - 5x^4) / (x/3 - x^5)", verbose);
            DerivativeTest("exp(1 - x^2y)", "x", "-2xy * exp(1 - x^2y)", verbose);
        }
        private static void DerivativeTest(string expr, string var, string derivative, bool verbose = false)
        {
            Expr f = Expr.Parse(expr);
            Expr fprime = Expr.Parse(derivative);
            Expr diff = f.Differentiate(var);
            if (!diff.EqualsAfterSimplification(fprime))
            {
                Debug.WriteLine("[failure] derive\t" + expr + " -> " + derivative);
                Debug.WriteLine("\tExpression parsed as:\t" + f);
                Debug.WriteLine("\tDerivative calculated as:\t" + diff.Simplify());
                Debug.WriteLine("\tCorrect derivative:\t" + fprime.Simplify());
                Debug.WriteLine("Derivative structure: ");
                Debug.WriteLine(diff.Simplify().ToIndentedString());
                Debug.WriteLine("fprime structure: ");
                Debug.WriteLine(fprime.Simplify().ToIndentedString());
            }
            else if (verbose)
            {
                Debug.WriteLine("[success] derive\t" + expr + " -> " + derivative);
            }
        }

        public static void ExpandTest(bool verbose = false)
        {
            ExpandTest("(a + b)(b + c)", "ab + b^2 + ac + bc", verbose);
            ExpandTest("(a + b)^2", "a^2 + 2ab + b^2", verbose);
            ExpandTest("(x + 1)^3", "x^3 + 3x^2 + 3x + 1", verbose);
            ExpandTest("(x + 4)^2 * (x - y)^3", "x^5 - 3yx^4 + 8x^4 + 3x^3 * y^2 - 24yx^3 + 16x^3 - x^2*y^3 + 24x^2*y^2 - 48x^2*y - 8xy^3 + 48xy^2 - 16y^3", verbose);
        }
        private static void ExpandTest(string expr, string expandedExpr, bool verbose = false)
        {
            Expr f = Expr.Parse(expr);
            f = f.Expand();
            Expr expanded = Expr.Parse(expandedExpr).Simplify();
            if (!f.EqualsUptoAssociativity(expanded))
            {
                Debug.WriteLine("[failure] expand\t" + expr + " -> " + expandedExpr);
                Debug.WriteLine("\tExpression was expanded to:\t" + f);
                Debug.WriteLine("\tCorrect expansion was:\t" + expanded);
            }
            else if (verbose)
            {
                Debug.WriteLine("[success] expand\t" + expr + " -> " + expandedExpr);
            }
        }

        public static void FactorTest(bool verbose = false)
        {
            FactorTest("x^3 + x", "x(x^2 + 1)", verbose);
            FactorTest("x^3y^4z^2 + x^3y^5z - x^2y^4z^2", "x^2y^4z(xz + xy - z)", verbose);
            FactorTest("x^2ln(x + 1) - xy^2ln(x + 1)", "x(x - y^2)ln(x + 1)", verbose);
        }
        private static void FactorTest(string expr, string factorizedExpr, bool verbose = false)
        {
            Expr f = Expr.Parse(expr);
            f = f.Factorize();
            Expr answer = Expr.Parse(factorizedExpr);

            if (!f.EqualsUptoAssociativity(answer))
            {
                Debug.WriteLine("[failure] factor\t" + expr + " -> " + factorizedExpr);
                ExprCompare.ToCanonicalSortOrder(f);
                Debug.WriteLine("\tExpression was factored to:\t" + f);
                ExprCompare.ToCanonicalSortOrder(answer);
                Debug.WriteLine("\tCorrect factorization was:\t" + answer);
            }
            else if (verbose)
            {
                Debug.WriteLine("[success] factor\t" + expr + " -> " + factorizedExpr);
            }
        }

        public static void RationalTest(bool verbose = false)
        {
            RationalTest("x^2 - 1", true, verbose);
            RationalTest("(x^2 - 1) / (x - 1)", true, verbose);
            RationalTest("-x^3 + (x + 1)(x^2 + 1) / (x - 3) - 1 / x", true, verbose);
            RationalTest("(x + 5)^7 / x + 1", true, verbose);
            RationalTest("x", true, verbose);
            RationalTest("-1", true, verbose);
            RationalTest("sin(x) / 2x", false, verbose);
            RationalTest("exp(x + 1) / log(x)", false, verbose);
        }
        private static void RationalTest(string expr, bool isRational, bool verbose)
        {
            Expr e = Expr.Parse(expr);
            if (ExprPolynomial.IsRationalFraction(e) == isRational)
            {
                if (verbose)
                {
                    Debug.WriteLine("[success] rational\t" + expr);
                }
            }
            else
            {
                Debug.WriteLine("[failure] rational\t" + expr);
            }
        }

        public static void SubstituteTest(bool verbose = false)
        {
            SubstituteTest("x^2 + 2x + 1", "x", "y^2", "y^4 + 2y^2 + 1", verbose);
        }
        private static void SubstituteTest(string expr, string subStr, string replaceStr, string resultStr, bool verbose = false)
        {
            Expr f = Expr.Parse(expr);
            Expr sub = Expr.Parse(subStr);
            Expr replace = Expr.Parse(replaceStr);

            f = f.Substitute(sub, replace);

            Expr answer = Expr.Parse(resultStr);

            if (!f.EqualsUptoAssociativity(answer))
            {
                Debug.WriteLine("[failure] substitute\t" + expr + " -> " + resultStr);
                ExprCompare.ToCanonicalSortOrder(f);
                Debug.WriteLine("\tExpression was substituted to:\t" + f);
                ExprCompare.ToCanonicalSortOrder(answer);
                Debug.WriteLine("\tCorrect substitution was:\t" + answer);
            }
            else if (verbose)
            {
                Debug.WriteLine("[success] factor\t" + expr + " -> " + resultStr);
            }
        }

        private static void SignificantFiguresTest(bool verbose = false)
        {
            if (verbose)
            {
                Expr numerator = 5, denominator = 7;

                // Store the fraction in exact form
                Expr fraction = (numerator / denominator).Simplify();

                Expr.DefaultSignificantFigures = 5;
                Debug.WriteLine(fraction.BigDecimalValue); // 0.71428

                Expr.DefaultSignificantFigures = 20;
                Debug.WriteLine(fraction.BigDecimalValue); // 0.71428571428571428571

                BigDecimal pi = BigDecimal.Pi(1000); // calculate pi to 1,000 decimal places
                Expr expr_pi = pi;
                Debug.WriteLine(expr_pi);
            }
        }

        private static void ToPolynomialTest(string poly, bool isPoly, bool verbose = false)
        {
            Expr expr = Expr.Parse(poly);

            if (expr.ToUnivariatePolynomial(out Polynomial<BigDecimal> p) != isPoly)
            {
                Debug.WriteLine("[failure] polynomial\t" + expr);
            }
            else if (verbose)
            {
                Debug.WriteLine("[success] polynomial\t" + expr + " -> " + p);
            }
        }
        public static void ToPolynomialTest(bool verbose = false)
        {
            ToPolynomialTest("-5", true, verbose);
            ToPolynomialTest("x^3 - x^2 + 6", true, verbose);
            ToPolynomialTest("-(-y^2 + y + 1)^5 - y^10", true, verbose);
            ToPolynomialTest("x + y", false, verbose);
        }

        private static void CancelRationalsTest(string num, string den, string simplifiedNum, string simplifiedDen, bool verbose = false)
        {
            Expr _num = Expr.Parse(num),
                _den = Expr.Parse(den);

            ExprRational.TryCancelRational(ref _num, ref _den);

            Expr answer_num = Expr.Parse(simplifiedNum),
                answer_den = Expr.Parse(simplifiedDen);

            if (_num.EqualsAfterSimplification(answer_num) && _den.EqualsAfterSimplification(answer_den))
            {
                if (verbose)
                {
                    Debug.WriteLine($"[success] cancel\t({num})/({den}) -> ({answer_num})/({answer_den})");
                }
            }
            else
            {
                Debug.WriteLine($"[failure] cancel\t({num})/({den})");
                Debug.WriteLine($"\tExpression was cancelled to:\t({_num})/({_den})");
                Debug.WriteLine($"\tCorrect cancellation was:\t({simplifiedNum})/({simplifiedDen})");
            }
        }
        public static void CancelRationalsTest(bool verbose = false)
        {
            CancelRationalsTest("(a + b)^2", "a + b", "a + b", "1", verbose);
            CancelRationalsTest("6(x - y)(x + 2y)", "3(x + 2y)", "2x - 2y", "1", verbose);
            SimplifyTest("(1/(exp(x)*(exp(y)+exp(-x))) - (exp(x+y)-1)/((exp(x+y))^2-1))", "0", verbose);
        }

    }
}
