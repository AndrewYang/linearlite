﻿using LinearNet.Structs.Fields;
using LinearNet.Symbolic;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace LinearNet.Tests
{
    internal static class SymbolicTrigonometryTests
    {
        public static void RunAll(bool verbose = false)
        {
            TanReplaceSinCos("tan(x)", "sin(x) / cos(x)", true, verbose);
            TanReplaceSinCos("tan(tan(2x))", "sin(sin(2x) / cos(2x)) / cos(sin(2x) / cos(2x))", true, verbose);
            TanReplaceSinCos("tan(tan(2x))", "sin(tan(2x)) / cos(tan(2x))", false, verbose);

            SinCosReplaceTan("sin(x) / cos(x)", "tan(x)", false, verbose);
            SinCosReplaceTan("sin(x) * x / cos(x) * 2", "2x * tan(x)", false, verbose);
            SinCosReplaceTan("sin(x) * sin(y) / cos(x) / cos(y) / cos(z)", "tan(x) * tan(y) / cos(z)", false, verbose);

            ModArguments("cos(5pi + pi / 2 + x)", "cos(3 / 2 * pi + x)", true, verbose);
            ModArguments("sin(2pi + x)", "sin(x)", true, verbose);
            ModArguments("sin(3pi + x)", "-sin(x)", true, verbose);
            ModArguments("cos(-x)", "cos(x)", true, verbose);

            SinPythagoras("2(sin(x))^2", "2(1 - (cos(x))^2)", true, verbose);
            CosPythagoras("(cos(-2x))^2", "1 - (sin(-2x))^2", false, verbose);

            BigRational half = new BigRational(1, 2);
            LowerCosDegree("(cos(x))^2", half + half * Expr.Cos(2 * Expr.Parse("x")), true, verbose);

            ProductToSum("sin(a) cos(b)", half * Expr.Parse("sin(a + b) + sin(a - b)"), false, verbose);
            ProductToSum("cos(b) sin(a)", half * Expr.Parse("sin(a + b) + sin(a - b)"), false, verbose);
            ProductToSum("sin(a) sin(b)", half * Expr.Parse("cos(a - b) - cos(a + b)"), false, verbose);
            ProductToSum("2 (cos(a) cos(b))", 2 * (half * Expr.Parse("cos(a - b) + cos(a + b)")), true, verbose);

            SumToProduct("sin(a) + sin(b)", 2 * Expr.Sin(Expr.Parse("(a + b) / 2").Expand()) * Expr.Cos(Expr.Parse("(a - b) / 2").Expand()), false, verbose);
            SumToProduct("cos(a) + cos(b)", 2 * Expr.Cos(Expr.Parse("(a + b) / 2").Expand()) * Expr.Cos(Expr.Parse("(a - b) / 2").Expand()), false, verbose);
            SumToProduct("cos(a) - cos(b)", -(2 * Expr.Sin(Expr.Parse("(a + b) / 2").Expand()) * Expr.Sin(Expr.Parse("(a - b) / 2").Expand())), false, verbose);

            ExpandAngleSum("sin(a + b)", "sin(a) cos(b) + sin(b) cos(a)", false, verbose);
            //ExpandAngleSum("sin(a - b)", "sin(a) cos(b) - sin(b) cos(a)", false, verbose);
            //ExpandAngleSum("cos(a + b)", "cos(a) cos(b) - sin(b) sin(a)", false, verbose);
            //ExpandAngleSum("cos(a - b)", "cos(a) cos(b) + sin(b) sin(a)", false, verbose);

            ExpandSinDoubleAngle("sin(2a)", "2 sin(a) cos(a)", false, verbose);
            ExpandCosDoubleAngle("cos(2x)", "1 - 2 * (sin(x))^2", false, verbose);

            ExpandSinNAngle("sin(2a)", "2 sin(a) cos(a)", false, verbose);
            ExpandSinNAngle("sin(3a)", "3 sin(a) - 4 (sin(a))^3", false, verbose);
        }

        private static void PrintResults(string name, Expr input, Expr output, Expr answer, bool verbose = false)
        {
            if (output.EqualsUptoAssociativity(answer))
            {
                Debug.WriteLine($"[success] trig\t{name}\t{input} -> {output}");
            }
            else
            {
                if (verbose)
                {
                    Debug.WriteLine($"[failure] trig\t{name}");
                    Debug.WriteLine("\tInput\t" + input);
                    Debug.WriteLine("\tOutput:\t" + output);
                    Debug.WriteLine("\tExpected:\t" + answer);

                    Debug.WriteLine("\tInput structure:");
                    Debug.WriteLine(input.ToIndentedString());

                    Debug.WriteLine("\tOutput structure:");
                    Debug.WriteLine(output.ToIndentedString());

                    Debug.WriteLine("\tExpected structure:");
                    Debug.WriteLine(answer.ToIndentedString());
                }
            }
        }
        private static void TanReplaceSinCos(string _in, string _out, bool recursive, bool verbose)
        {
            Expr input = Expr.Parse(_in), output = input.Copy(), answer = Expr.Parse(_out);
            ExprTrigonometryConstants.tan_to_sincos(ref output, recursive);
            PrintResults("tan -> sin/cos", input, output, answer, verbose);
        }
        private static void SinCosReplaceTan(string _in, string _out, bool recursive, bool verbose)
        {
            Expr sincos = Expr.Parse(_in);
            ExprSimplify.FlattenAssociativeOperations(sincos);

            Expr input = sincos, output = input.Copy(), answer = Expr.Parse(_out);
            ExprTrigonometryConstants.sincos_to_tan(ref output, recursive);
            PrintResults("sin/cos -> tan", input, output, answer, verbose);
        }
        private static void ModArguments(string _in, string _out, bool recursive, bool verbose)
        {
            Expr input = Expr.Parse(_in).Simplify(), output = input.Copy(), answer = Expr.Parse(_out).Simplify();
            ExprTrigonometryConstants.mod_arguments_by_period(ref output, recursive);
            PrintResults("mod arguments", input, output, answer, verbose);
        }
        private static void SinPythagoras(string _in, string _out, bool recursive, bool verbose)
        {
            Expr input = Expr.Parse(_in).Simplify(), output = input.Copy(), answer = Expr.Parse(_out);
            ExprTrigonometryConstants.sin_square(ref output, recursive);
            PrintResults("sin pythagoras", input, output, answer, verbose);
        }
        private static void CosPythagoras(string _in, string _out, bool recursive, bool verbose)
        {
            Expr input = Expr.Parse(_in), output = input.Copy(), answer = Expr.Parse(_out);
            ExprTrigonometryConstants.cos_square(ref output, recursive);
            PrintResults("cos pythagoras", input, output, answer, verbose);
        }
        private static void LowerCosDegree(string _in, Expr answer, bool recursive, bool verbose)
        {
            Expr input = Expr.Parse(_in), output = input.Copy();
            ExprTrigonometryConstants.lower_cos_degree(ref output, recursive);
            PrintResults("lower cos degree", input, output, answer, verbose);
        }
        private static void ProductToSum(string _in, Expr answer, bool recursive, bool verbose)
        {
            Expr input = Expr.Parse(_in), output = input.Copy();
            ExprTrigonometryConstants.product_to_sum(ref output, recursive);
            PrintResults("product to sum", input, output, answer, verbose);
        }
        private static void SumToProduct(string _in, Expr answer, bool recursive, bool verbose)
        {
            Expr input = Expr.Parse(_in), output = input.Copy();
            ExprTrigonometryConstants.sum_to_product(ref output, recursive);
            PrintResults("sum to product", input, output, answer, verbose);
        }
        private static void ExpandAngleSum(string _in, string _out, bool recursive, bool verbose)
        {
            Expr input = Expr.Parse(_in), output = input.Copy(), answer = Expr.Parse(_out);
            ExprTrigonometryConstants.expand_angle_sum(ref output, recursive);
            PrintResults("expand angle sum", input, output, answer, verbose);
        }
        private static void ExpandSinDoubleAngle(string _in, string _out, bool recursive, bool verbose)
        {
            Expr input = Expr.Parse(_in), output = input.Copy(), answer = Expr.Parse(_out);
            ExprTrigonometryConstants.expand_sin_double_angle(ref output, recursive);
            PrintResults("expand sin 2 angle", input, output, answer, verbose);
        }
        private static void ExpandCosDoubleAngle(string _in, string _out, bool recursive, bool verbose)
        {
            Expr input = Expr.Parse(_in), output = input.Copy(), answer = Expr.Parse(_out);
            ExprTrigonometryConstants.expand_cos_double_angle(ref output, 1, recursive);
            PrintResults("expand cos 2 angle", input, output, answer, verbose);
        }
        private static void ExpandSinNAngle(string _in, string _out, bool recursive, bool verbose)
        {
            Expr input = Expr.Parse(_in), output = input.Copy(), answer = Expr.Parse(_out);
            ExprTrigonometryConstants.expand_sin_n_angle(ref output, recursive);
            PrintResults("expand sin n angle", input, output, answer, verbose);
        }
    }
}
