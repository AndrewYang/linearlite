﻿using LinearNet.Symbolic;
using LinearNet.Symbolic.Simplify;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace LinearNet.Tests.Symbolic
{
    internal static class TemplateTests
    {
        internal static void RunAll(bool verbose)
        {
            TestCase("1", "1", true, verbose);
            TestCase("x + y", "a + b", true, verbose);
            TestCase("x + x^2", "y + y", false, verbose);
            TestCase("(1 + y) + 1 / (1 + y)", "x + 1 / x", true, verbose);
            TestCase("(z + a)^2 + (z + a) + 3", "x^2 + x + y", true, verbose);

            TestTemplateSimplify("(x + y) - (x + y)", "z - z", "0", "0", verbose); // Cancellation
            TestTemplateSimplify("(a + 2) + (a + 2)", "x + x", "2x", "2(a + 2)", verbose); // Grouping
            TestTemplateSimplify("(x + 2y) / (x + 2y)", "x / x", "1", "1", verbose); // Cancellation
            TestTemplateSimplify("(5 + y)(x + 2y) / (x + 2y)", "xy / x", "y", "5 + y", verbose); // Cancellation
        }
        private static void TestCase(string expression, string template, bool matches, bool verbose)
        {
            Expr expr = Expr.Parse(expression);
            ExprTemplate exprTemplate = new ExprTemplate(template);

            if (exprTemplate.Matches(expr, out Dictionary<string, Expr> exprs) != matches)
            {
                Debug.WriteLine($"[failure]\tmatch({expression}, {template}) != {matches}");
                if (verbose)
                {
                    Debug.WriteLine("Expression:");
                    Debug.WriteLine(expr.ToIndentedString());
                    Debug.WriteLine("Template:");
                    Debug.WriteLine(exprTemplate.Expression.ToIndentedString());
                    Debug.WriteLine(ToString(exprs));
                }
                Debug.Assert(false);
            }
            else
            {
                Debug.WriteLine($"[success]\tmatch({expression}, {template}) == {matches}\t({ToString(exprs)})");
            }
        }
        private static void TestTemplateSimplify(string expression, string in_template, string out_template, string simplified, bool verbose)
        {
            TemplateSimplification ts = new TemplateSimplification(in_template, out_template);
            Expr expr = Expr.Parse(expression);
            ts.Apply(ref expr);

            if (expr.EqualsUptoAssociativity(Expr.Parse(simplified)))
            {
                Debug.WriteLine($"[success]\t{expression} -> {simplified} using template({in_template}, {out_template})");
            }
            else
            {
                Debug.WriteLine(expr.ToIndentedString());
                Debug.WriteLine(Expr.Parse(in_template).ToIndentedString());

                Debug.WriteLine($"[failure]\t{expression} -> {expr} != {simplified} using template({in_template}, {out_template})");
            }
        }

        internal static string ToString<T, V>(this IEnumerable<KeyValuePair<T, V>> items, string format = null)
        {
            format = string.IsNullOrEmpty(format) ? "{0}='{1}'" : format;

            List<string> strs = new List<string>();
            foreach (var item in items)
            {
                strs.Add(string.Format(format, item.Key, item.Value));
            }
            return string.Join(", ", strs);
        }
    }
}
