﻿using LinearNet.Structs;
using System;
using System.Diagnostics;

namespace LinearNet.Tests
{
    internal static class TestUtil
    {
        internal static bool AssertMatrixEqual<T>(Matrix<T> A, Matrix<T> B, bool verbose) where T : new()
        {
            if (A.Rows != B.Rows)
            {
                Debug.Assert(false, "Matrix rows not equal");
                return false;
            }

            if (A.Columns != B.Columns)
            {
                Debug.Assert(false, "Matrix columns not equal");
                return false;
            }

            for (int i = 0; i < A.Rows; ++i)
            {
                for (int j = 0; j < A.Columns; ++j)
                {
                    if (!A[i, j].Equals(B[i, j]))
                    {
                        Debug.Assert(false, $"Matrix elements @[{i}, {j}] not equal: " + A[i, j] + " vs " + B[i, j]);
                        return false;
                    }
                }
            }

            return true;
        }
        internal static bool AssertMatrixEqual<T>(Matrix<T> A, Matrix<T> B, Func<T, T, bool> equals) where T : new()
        {
            if (A.Rows != B.Rows)
            {
                Debug.Assert(false, "Matrix rows not equal");
                return false;
            }

            if (A.Columns != B.Columns)
            {
                Debug.Assert(false, "Matrix columns not equal");
                return false;
            }

            for (int i = 0; i < A.Rows; ++i)
            {
                for (int j = 0; j < A.Columns; ++j)
                {
                    if (!equals(A[i, j], B[i, j]))
                    {
                        Debug.Assert(false, $"Matrix elements @[{i}, {j}] not equal: " + A[i, j] + " vs " + B[i, j]);
                        return false;
                    }
                }
            }

            return true;
        }
        internal static bool AssertVectorsEqual<T>(Vector<T> u, Vector<T> v) where T : new()
        {
            if (u.Dimension != v.Dimension)
            {
                Debug.Assert(false, "Vector dimensions not equal");
                return false;
            }

            for (int i = 0; i < u.Dimension; ++i)
            {
                if (!u[i].Equals(v[i]))
                {
                    Debug.Assert(false, $"Vector elements [{i}] not equal: {u[i]} vs {v[i]}");
                    return false;
                }
            }

            return true;
        }
        internal static bool AssertVectorsEqual<T>(T[] u, T[] v)
        {
            if (u.Length != v.Length)
            {
                Debug.Assert(false, "Vector dimensions not equal");
                return false;
            }

            for (int i = 0; i < u.Length; ++i)
            {
                if (!u[i].Equals(v[i]))
                {
                    Debug.Assert(false, $"Vector elements [{i}] not equal: {u[i]} vs {v[i]}");
                    return false;
                }
            }

            return true;
        }
        internal static bool AssertVectorsEqual<T>(Vector<T> u, Vector<T> v, Func<T, T, bool> equals) where T : new()
        {
            if (u.Dimension != v.Dimension)
            {
                Debug.Assert(false, "Vector dimensions not equal");
                return false;
            }

            for (int i = 0; i < u.Dimension; ++i)
            {
                if (!equals(u[i], v[i]))
                {
                    Debug.Assert(false, $"Vector elements [{i}] not equal: {u[i]} vs {v[i]}");
                    return false;
                }
            }

            return true;
        }
        internal static bool AssertEqual<T>(Matrix<T> A, Tensor<T> B, Func<T, T, bool> equals) where T : new()
        {
            for (int r = 0; r < A.Rows; ++r)
            {
                for (int c = 0; c < A.Columns; ++c)
                {
                    if (!equals(A[r, c], B.Get(r, c)))
                    {
                        Debug.Assert(false, $"{A[r, c]} != {B.Get(r, c)}");
                        return false;
                    }
                }
            }
            return true;
        }
        internal static bool AssertEqual<T>(Matrix<T> A, Tensor<T> B) where T : new()
        {
            return AssertEqual(A, B, (u, v) => u.Equals(v));
        }
        internal static int Random(int min, int max)
        {
            return new Random().Next(max - min) + min;
        }
    }
}
