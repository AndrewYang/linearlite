﻿using LinearNet.Global;
using LinearNet.Providers;
using LinearNet.Providers.LinearAlgebra;
using LinearNet.Structs;
using System;

namespace LinearNet.Vectors
{
    public static class DenseVectorOperations
    {
        /// <summary>
        /// Returns the opposite of a vector.
        /// </summary>
        /// <param name="u">A vector.</param>
        /// <returns>The opposite vector.</returns>
        /// <cat>la</cat>
        public static DenseVector<int> Negate(this DenseVector<int> u) => new DenseVector<int>(u.Values.Negate());
        public static DenseVector<long> Negate(this DenseVector<long> u) => new DenseVector<long>(u.Values.Negate());
        public static DenseVector<float> Negate(this DenseVector<float> u) => new DenseVector<float>(u.Values.Negate());
        public static DenseVector<double> Negate(this DenseVector<double> u) => new DenseVector<double>(u.Values.Negate());
        public static DenseVector<decimal> Negate(this DenseVector<decimal> u) => new DenseVector<decimal>(u.Values.Negate());
        public static DenseVector<T> Negate<T>(this DenseVector<T> u) where T : IAdditiveGroup<T>, new() => new DenseVector<T>(u.Values.Negate());

        #region Vector sums and differences

        private static DenseVector<T> Op<T>(DenseVector<T> u, DenseVector<T> v, Func<T[], T[], T[]> Operation) where T : new()
        {
            return new DenseVector<T>(Operation(u.Values, v.Values));
        }
        /// <summary>
        /// Returns the sum of two vectors.
        /// <para>Throws <txt>InvalidOperatonException</txt> if the vectors are of different dimensions.</para>
        /// </summary>
        /// <param name="u">The first vector.</param>
        /// <param name="v">The second vector.</param>
        /// <returns>The vector sum.</returns>
        /// <cat>la</cat>
        public static DenseVector<int> Add(this DenseVector<int> u, DenseVector<int> v) => Op(u, v, RectangularVectorOperations.Add);
        public static DenseVector<long> Add(this DenseVector<long> u, DenseVector<long> v) => Op(u, v, RectangularVectorOperations.Add);
        public static DenseVector<float> Add(this DenseVector<float> u, DenseVector<float> v) => Op(u, v, RectangularVectorOperations.Add);
        public static DenseVector<double> Add(this DenseVector<double> u, DenseVector<double> v) => Op(u, v, RectangularVectorOperations.Add);
        public static DenseVector<decimal> Add(this DenseVector<decimal> u, DenseVector<decimal> v) => Op(u, v, RectangularVectorOperations.Add);
        public static DenseVector<T> Add<T>(this DenseVector<T> u, DenseVector<T> v) where T : IAdditiveGroup<T>, new() => Op(u, v, RectangularVectorOperations.Add);

        /// <summary>
        /// Returns the first vector subtract the second vector.
        /// <para>Throws <txt>InvalidOperationException</txt> if the vectors are of different dimensions.</para>
        /// </summary>
        /// <param name="u">The first vector.</param>
        /// <param name="v">The second vector.</param>
        /// <returns>The vector difference.</returns>
        /// <cat>la</cat>
        public static DenseVector<int> Subtract(this DenseVector<int> u, DenseVector<int> v) => Op(u, v, RectangularVectorOperations.Subtract);
        public static DenseVector<long> Subtract(this DenseVector<long> u, DenseVector<long> v) => Op(u, v, RectangularVectorOperations.Subtract);
        public static DenseVector<float> Subtract(this DenseVector<float> u, DenseVector<float> v) => Op(u, v, RectangularVectorOperations.Subtract);
        public static DenseVector<double> Subtract(this DenseVector<double> u, DenseVector<double> v) => Op(u, v, RectangularVectorOperations.Subtract);
        public static DenseVector<decimal> Subtract(this DenseVector<decimal> u, DenseVector<decimal> v) => Op(u, v, RectangularVectorOperations.Subtract);
        public static DenseVector<T> Subtract<T>(this DenseVector<T> u, DenseVector<T> v) where T : IAdditiveGroup<T>, new() => Op(u, v, RectangularVectorOperations.Add);

        #endregion

        #region Vector products

        /// <summary>
        /// <para>Returns the dot product (scalar product) of two dense vectors. The original vectors are unchanged.</para>
        /// <para>Throws <txt>InvalidOperationException</txt> if vectors are not of equal dimension.</para>
        /// </summary>
        /// <param name="u">A dense vector.</param>
        /// <param name="v">A dense vector.</param>
        /// <returns>Dot product of the two vectors.</returns>
        /// <cat>la</cat>
        public static int Dot(this DenseVector<int> u, DenseVector<int> v) => u.Dot(v, BLAS.Int32);
        public static long Dot(this DenseVector<long> u, DenseVector<long> v) => u.Dot(v, BLAS.Int64);
        public static float Dot(this DenseVector<float> u, DenseVector<float> v) => u.Dot(v, BLAS.Float);
        public static double Dot(this DenseVector<double> u, DenseVector<double> v) => u.Dot(v, BLAS.Double);
        public static decimal Dot(this DenseVector<decimal> u, DenseVector<decimal> v) => u.Dot(v, BLAS.Decimal);
        public static Complex Dot(this DenseVector<Complex> u, DenseVector<Complex> v) => u.Dot(v, BLAS.Complex);
        public static T Dot<T>(this DenseVector<T> u, DenseVector<T> v) where T : IRing<T>, new() => u.Dot(v, new NativeRingProvider<T>());

        /// <summary>
        /// <para>Calculates a vector multiplied by a scalar. The original vector is unchanged.</para>
        /// </summary>
        /// <param name="u">A vector.</param>
        /// <param name="s">A scalar.</param>
        /// <returns>A vector representing the vector-scalar product.</returns>
        /// <cat>la</cat>
        public static DenseVector<int> Multiply(this DenseVector<int> u, int s) => u.Multiply(s, BLAS.Int32);
        public static DenseVector<long> Multiply(this DenseVector<long> u, long s) => u.Multiply(s, BLAS.Int64);
        public static DenseVector<float> Multiply(this DenseVector<float> u, float s) => u.Multiply(s, BLAS.Float);
        public static DenseVector<double> Multiply(this DenseVector<double> u, double s) => u.Multiply(s, BLAS.Double);
        public static DenseVector<decimal> Multiply(this DenseVector<decimal> u, decimal s) => u.Multiply(s, BLAS.Decimal);
        public static DenseVector<Complex> Multiply(this DenseVector<Complex> u, Complex s) => u.Multiply(s, BLAS.Complex);
        public static DenseVector<T> Multiply<T>(this DenseVector<T> u, T s) where T : IRing<T>, new() => u.Multiply(s, new NativeRingProvider<T>());

        /// <summary>
        /// <para>Returns the cross product of two three-dimensional vectors, which is another vector orthogonal to both vectors.</para>
        /// </summary>
        /// <param name="u">The first vector.</param>
        /// <param name="v">The second vector.</param>
        /// <returns>A vector orthogonal to both vectors.</returns>
        /// <cat>la</cat>
        public static DenseVector<int> Cross(this DenseVector<int> u, DenseVector<int> v) => Cross(u, v, new Int32Provider());
        public static DenseVector<long> Cross(this DenseVector<long> u, DenseVector<long> v) => Cross(u, v, new Int64Provider());
        public static DenseVector<float> Cross(this DenseVector<float> u, DenseVector<float> v) => Cross(u, v, new FloatProvider());
        public static DenseVector<double> Cross(this DenseVector<double> u, DenseVector<double> v) => Cross(u, v, new DoubleProvider());
        public static DenseVector<decimal> Cross(this DenseVector<decimal> u, DenseVector<decimal> v) => Cross(u, v, new DecimalProvider());
        public static DenseVector<Complex> Cross(this DenseVector<Complex> u, DenseVector<Complex> v) => Cross(u, v, new ComplexProvider());
        public static DenseVector<T> Cross<T>(this DenseVector<T> u, DenseVector<T> v) where T : IRing<T>, new() => Cross(u, v, new RingProvider<T>());
        private static DenseVector<T> Cross<T>(DenseVector<T> u, DenseVector<T> v, IProvider<T> provider) where T : new()
        {
            if (u == null) throw new ArgumentNullException(nameof(u));
            if (v == null) throw new ArgumentNullException(nameof(v));

            if (u.Dimension != 3) throw new InvalidOperationException(nameof(u) + " is not of dimension 3");
            if (v.Dimension != 3) throw new InvalidOperationException(nameof(v) + " is not of dimension 3");

            T[] cross =
            {
                provider.Subtract(provider.Multiply(u[1], v[2]), provider.Multiply(u[2], v[1])),
                provider.Subtract(provider.Multiply(u[2], v[0]), provider.Multiply(u[0], v[2])),
                provider.Subtract(provider.Multiply(u[0], v[1]), provider.Multiply(u[1], v[0]))
            };

            return new DenseVector<T>(cross);
        }

        /// <summary>
        /// <para>Returns the outer product $u \otimes v$ of two vectors $u, v$, which is a matrix whose $(i, j)$-th element is given by 
        /// the $i$-th element of the first vector multiplied by the $j$-th element of the second vector.</para>
        /// </summary>
        /// <param name="u">The first vector.</param>
        /// <param name="v">The second vector.</param>
        /// <returns>The outer product of two vectors.</returns>
        /// <cat>la</cat>
        public static DenseMatrix<int> OuterProduct(this DenseVector<int> u, DenseVector<int> v) => OuterProduct(u, v, (a, b) => a * b);
        public static DenseMatrix<long> OuterProduct(this DenseVector<long> u, DenseVector<long> v) => OuterProduct(u, v, (a, b) => a * b);
        public static DenseMatrix<float> OuterProduct(this DenseVector<float> u, DenseVector<float> v) => OuterProduct(u, v, (a, b) => a * b);
        public static DenseMatrix<double> OuterProduct(this DenseVector<double> u, DenseVector<double> v) => OuterProduct(u, v, (a, b) => a * b);
        public static DenseMatrix<decimal> OuterProduct(this DenseVector<decimal> u, DenseVector<decimal> v) => OuterProduct(u, v, (a, b) => a * b);
        public static DenseMatrix<T> OuterProduct<T>(this DenseVector<T> u, DenseVector<T> v) where T : IRing<T>, new() => OuterProduct(u, v, (a, b) => a.Multiply(b));
        private static DenseMatrix<T> OuterProduct<T>(DenseVector<T> u, DenseVector<T> v, Func<T, T, T> Multiply) where T : new()
        {
            if (u == null || v == null) throw new ArgumentNullException();

            int m = u.Dimension, n = v.Dimension, i, j;

            DenseMatrix<T> matrix = new DenseMatrix<T>(m, n);
            T[][] prod = matrix.Values;
            for (i = 0; i < m; ++i)
            {
                T[] row = prod[i];
                T u_i = u[i];
                for (j = 0; j < n; ++j)
                {
                    row[j] = Multiply(u_i, v[j]);
                }
            }
            return matrix;
        }

        #endregion

        #region Norms

        /// <summary>
        /// <p>Returns the $p$-norm of a dense vector, for $p > 0$. </p>
        /// <p>Type support: <txt>int, long, float, double, decimal, Complex</txt>.</p>
        /// </summary>
        /// <param name="vector">A vector.</param>
        /// <param name="p">The order of the norm, $p$.</param>
        /// <returns>The $p$-norm of the vector.</returns>
        /// <proto>T Norm(this DenseVector<T> vector)</proto>
        /// <cat>la</cat>
        public static double Norm(this DenseVector<int> vector, double p = 2.0) => RectangularVectorOperations.Norm(vector.Values, p);
        public static double Norm(this DenseVector<long> vector, double p = 2.0) => RectangularVectorOperations.Norm(vector.Values, p);
        public static float Norm(this DenseVector<float> vector, float p = 2.0f) => RectangularVectorOperations.Norm(vector.Values, p);
        public static double Norm(this DenseVector<double> vector, double p = 2.0) => RectangularVectorOperations.Norm(vector.Values, p);
        public static decimal Norm(this DenseVector<decimal> vector, decimal p = 2.0m) => RectangularVectorOperations.Norm(vector.Values, p);
        public static double Norm(this DenseVector<Complex> vector) => RectangularVectorOperations.Norm(vector.Values);

        /// <summary>
        /// Returns the infinity-norm of a dense vector, $||x||_{\infty}$.
        /// <p>
        /// Type support: <txt>int, long, float, double, decimal</txt> and all types implementing both <txt>IAdditiveGroup&ltT&gt</txt> 
        /// and <txt>IComparable&ltT&gt</txt>.
        /// </p>
        /// </summary>
        /// <param name="vector">A vector.</param>
        /// <returns>The $\infty$-norm of this vector.</returns>
        /// <proto>T InfinityNorm(this DenseVector<T> vector)</proto>
        /// <cat>la</cat>
        public static int InfinityNorm(this DenseVector<int> vector) => RectangularVectorOperations.InfinityNorm(vector.Values);
        public static long InfinityNorm(this DenseVector<long> vector) => RectangularVectorOperations.InfinityNorm(vector.Values);
        public static float InfinityNorm(this DenseVector<float> vector) => RectangularVectorOperations.InfinityNorm(vector.Values);
        public static double InfinityNorm(this DenseVector<double> vector) => RectangularVectorOperations.InfinityNorm(vector.Values);
        public static decimal InfinityNorm(this DenseVector<decimal> vector) => RectangularVectorOperations.InfinityNorm(vector.Values);
        public static T InfinityNorm<T>(this DenseVector<T> vector) where T : IAdditiveGroup<T>, IComparable<T>, new() => RectangularVectorOperations.InfinityNorm(vector.Values);
        #endregion

        #region Householder matrix

        public static DenseMatrix<double> HouseholderMatrix(this DenseVector<double> z)
        {
            z = z.Multiply(Constants.Sqrt2 / z.Norm(2));

            int length = z.Dimension, r, c;

            DenseMatrix<double> _H = DenseMatrix.Identity<double>(length);
            double[][] H = _H.Values;
            for (c = 0; c < length; c++)
            {
                double z_c = z[c];
                for (r = 0; r < length; r++)
                {
                    H[r][c] -= z[r] * z_c;
                }
            }
            return _H;
        }
        #endregion

        #region boolean methods

        /// <summary>
        /// <p>Returns whether a vector is a zero vector.</p>
        /// <p>Type support: <txt>int, long</txt>. For finite-precision types, see <a href="">IsZero(DenseVector&ltT&gt, T)</a>.</p>
        /// </summary>
        /// <param name="u">A vector.</param>
        /// <returns>Whether this vector is zero.</returns>
        /// <proto>bool IsZero(this DenseVector<T> vector)</proto>
        /// <cat>la</cat>
        public static bool IsZero(this DenseVector<int> u)
        {
            if (u == null) throw new ArgumentNullException(nameof(u));
            return u.Values.IsZero();
        }
        public static bool IsZero(this DenseVector<long> u)
        {
            if (u == null) throw new ArgumentNullException(nameof(u));
            return u.Values.IsZero();
        }
        /// <summary>
        /// Returns whether a vector is zero, up to the specified level of tolerance.
        /// <p>Type support: <txt>float, double, decimal, Complex</txt>.</p>
        /// </summary>
        /// <param name="u">A vector.</param>
        /// <param name="tolerance">
        /// The tolerance level. A vector component is considered to be zero if 
        /// its magnitude is less than the tolerance level.
        /// </param>
        /// <returns>Whether this vector is the zero vector.</returns>
        /// <proto>bool IsZero(this DenseVector<T> vector, T tolerance)</proto>
        /// <cat>la</cat>
        public static bool IsZero(this DenseVector<float> u, float tolerance = Precision.FLOAT_PRECISION)
        {
            if (u == null) throw new ArgumentNullException(nameof(u));
            return u.Values.IsZero(tolerance);
        }
        public static bool IsZero(this DenseVector<double> u, double tolerance = Precision.DOUBLE_PRECISION)
        {
            if (u == null) throw new ArgumentNullException(nameof(u));
            return u.Values.IsZero(tolerance);
        }
        public static bool IsZero(this DenseVector<decimal> u, decimal tolerance = Precision.DECIMAL_PRECISION)
        {
            if (u == null) throw new ArgumentNullException(nameof(u));
            return u.Values.IsZero(tolerance);
        }
        public static bool IsZero(this DenseVector<Complex> u, double tolerance = Precision.DOUBLE_PRECISION)
        {
            if (u == null) throw new ArgumentNullException(nameof(u));
            return u.Values.IsZero(tolerance);
        }

        /// <summary>
        /// Returns whether this vector has unit norm.
        /// </summary>
        /// <param name="u">A vector.</param>
        /// <returns>Whether the vector is a unit vector.</returns>
        /// <cat>la</cat>
        public static bool IsUnit(this DenseVector<int> u) => IsUnit(u, RectangularVectorOperations.IsUnit);
        public static bool IsUnit(this DenseVector<long> u) => IsUnit(u, RectangularVectorOperations.IsUnit);
        public static bool IsUnit(this DenseVector<float> u, float tolerance = Precision.FLOAT_PRECISION) => IsUnit(u, x => RectangularVectorOperations.IsUnit(x, tolerance));
        public static bool IsUnit(this DenseVector<double> u, double tolerance = Precision.DOUBLE_PRECISION) => IsUnit(u, x => RectangularVectorOperations.IsUnit(x, tolerance));
        public static bool IsUnit(this DenseVector<decimal> u, decimal tolerance = Precision.DECIMAL_PRECISION) => IsUnit(u, x => RectangularVectorOperations.IsUnit(x, tolerance));
        private static bool IsUnit<T>(DenseVector<T> u, Func<T[], bool> IsUnit) where T : new()
        {
            if (u == null)
            {
                throw new ArgumentNullException(nameof(u));
            }

            return IsUnit(u.Values);
        }

        #endregion
    }
}
