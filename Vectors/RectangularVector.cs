﻿using LinearNet.Global;
using LinearNet.Helpers;
using LinearNet.Structs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet
{
    public static class RectangularVector
    {
        public static T[] Random<T>(int n, Func<T> Sample)
        {
            T[] rand = new T[n];
            for (int i = 0; i < n; i++)
            {
                rand[i] = Sample();
            }
            return rand;
        }
        public static T[] Random<T>(int n)
        {
            return Random(n, DefaultGenerators.GetRandomGenerator<T>());
        }
        public static T[] Repeat<T>(T value, int n)
        {
            T[] vector = new T[n];
            for (int i = 0; i < n; ++i)
            {
                vector[i] = value;
            }
            return vector;
        }
        public static T[] Repeat<T>(T value, long n)
        {
            T[] vector = new T[n];
            for (long i = 0; i < n; ++i)
            {
                vector[i] = value;
            }
            return vector;
        }
        public static T[] Zeroes<T>(int dim) where T : new()
        {
            if (dim < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(dim));
            }

            T[] vect = new T[dim];

            // have to check to see if the default value concurs with new T()
            if (dim > 0)
            {
                T zero = default;
                if (!vect[0].Equals(zero))
                {
                    for (int i = 0; i < dim; ++i)
                    {
                        vect[i] = new T();
                    }
                }
            }

            return vect;
        }
        public static T[] Zeroes<T>(long dim) where T : new()
        {
            if (dim < 0L)
            {
                throw new ArgumentOutOfRangeException(nameof(dim));
            }

            T[] vect = new T[dim];

            // have to check to see if the default value concurs with new T()
            if (dim > 0)
            {
                T zero = default;
                if (!vect[0].Equals(zero))
                {
                    for (int i = 0; i < dim; ++i)
                    {
                        vect[i] = new T();
                    }
                }
            }

            return vect;
        }
    }
}
