﻿using LinearNet.Providers;
using LinearNet.Structs;
using System;
using System.Collections.Generic;

namespace LinearNet.Vectors
{
    public static class SparseVectorOperations
    {
        /// <summary>
        /// <p>Calculates the sum of two sparse vectors, returning the result as another sparse vector.</p>
        /// <p>
        /// Supported for <txt>int, long, float, double, decimal, Complex</txt> and all custom types <txt>T</txt>
        /// that implement the <txt>IAdditiveGroup&ltT&gt</txt> interface.</p>
        /// </summary>
        /// <param name="u">The first sparse vector.</param>
        /// <param name="v">The second sparse vector.</param>
        /// <returns>The sum.</returns>
        /// <proto>SparseVector<T> Add(this SparseVector<T> u, SparseVector<T> v)</proto>
        /// <cat>la</cat>
        public static BigSparseVector<int> Add(this BigSparseVector<int> u, BigSparseVector<int> v) => u.Add(v, DefaultProviders.Int32);
        public static BigSparseVector<long> Add(this BigSparseVector<long> u, BigSparseVector<long> v) => u.Add(v, DefaultProviders.Int64);
        public static BigSparseVector<float> Add(this BigSparseVector<float> u, BigSparseVector<float> v) => u.Add(v, DefaultProviders.Float);
        public static BigSparseVector<double> Add(this BigSparseVector<double> u, BigSparseVector<double> v) => u.Add(v, DefaultProviders.Double);
        public static BigSparseVector<decimal> Add(this BigSparseVector<decimal> u, BigSparseVector<decimal> v) => u.Add(v, DefaultProviders.Decimal);
        public static BigSparseVector<Complex> Add(this BigSparseVector<Complex> u, BigSparseVector<Complex> v) => u.Add(v, DefaultProviders.Complex);
        public static BigSparseVector<T> Add<T>(this BigSparseVector<T> u, BigSparseVector<T> v) where T : IAdditiveGroup<T>, new() => u.Add(v, new AdditiveGroupProvider<T>());

        /// <summary>
        /// <p>Calculates the first vector subtract the second vector, returning the result as another sparse vector.</p>
        /// <p>
        /// Supported for <txt>int, long, float, double, decimal, Complex</txt> and all custom types <txt>T</txt>
        /// that implement the <txt>IAdditiveGroup&ltT&gt</txt> interface.
        /// </p>
        /// </summary>
        /// <param name="u">The first sparse vector.</param>
        /// <param name="v">The second sparse vector.</param>
        /// <returns>The vector difference.</returns>
        /// <cat>la</cat>
        public static BigSparseVector<int> Subtract(this BigSparseVector<int> u, BigSparseVector<int> v) => u.Subtract(v, DefaultProviders.Int32);
        public static BigSparseVector<long> Subtract(this BigSparseVector<long> u, BigSparseVector<long> v) => u.Subtract(v, DefaultProviders.Int64);
        public static BigSparseVector<float> Subtract(this BigSparseVector<float> u, BigSparseVector<float> v) => u.Subtract(v, DefaultProviders.Float);
        public static BigSparseVector<double> Subtract(this BigSparseVector<double> u, BigSparseVector<double> v) => u.Subtract(v, DefaultProviders.Double);
        public static BigSparseVector<decimal> Subtract(this BigSparseVector<decimal> u, BigSparseVector<decimal> v) => u.Subtract(v, DefaultProviders.Decimal);
        public static BigSparseVector<Complex> Subtract(this BigSparseVector<Complex> u, BigSparseVector<Complex> v) => u.Subtract(v, DefaultProviders.Complex);
        public static BigSparseVector<T> Subtract<T>(this BigSparseVector<T> u, BigSparseVector<T> v) where T : IAdditiveGroup<T>, new() => u.Subtract(v, new AdditiveGroupProvider<T>());

        /*
        /// <summary>
        /// <p>Calculates the dot product between two sparse vectors.</p>
        /// <p>
        /// Supported for <txt>int, long, float, double, decimal, Complex</txt> and all custom types <txt>T</txt>
        /// that implement the <txt>IRing&ltT&gt</txt> interface.
        /// </p>
        /// </summary>
        /// <param name="u">The first sparse vector.</param>
        /// <param name="v">The second sparse vector.</param>
        /// <returns>The dot product.</returns>
        /// <cat>la</cat>
        public static int Dot(this BigSparseVector<int> u, BigSparseVector<int> v) => u.Dot(v, BLAS.Int32);
        public static long Dot(this BigSparseVector<long> u, BigSparseVector<long> v) => u.Dot(v, BLAS.Int64);
        public static float Dot(this BigSparseVector<float> u, BigSparseVector<float> v) => u.Dot(v, BLAS.Float);
        public static double Dot(this BigSparseVector<double> u, BigSparseVector<double> v) => u.Dot(v, BLAS.Double);
        public static decimal Dot(this BigSparseVector<decimal> u, BigSparseVector<decimal> v) => u.Dot(v, BLAS.Decimal);
        public static Complex Dot(this BigSparseVector<Complex> u, BigSparseVector<Complex> v) => u.Dot(v, BLAS.Complex);
        public static T Dot<T>(this BigSparseVector<T> u, BigSparseVector<T> v) where T : IRing<T>, new() => u.Dot(v, new NativeRingProvider<T>());

        /// <summary>
        /// <p>Multiply a sparse vector by a scalar. A new vector is returned and the original vector is unchanged.</p>
        /// <p>
        /// Supported for <txt>int, long, float, double, decimal, Complex</txt> and all custom types <txt>T</txt>
        /// that implement the <txt>IRing&ltT&gt</txt> interface.
        /// </p>
        /// </summary>
        /// <param name="u">A sparse vector.</param>
        /// <param name="s">A scalar.</param>
        /// <returns>The result of the vector-scalar multiplication.</returns>
        /// <cat>la</cat>
        public static BigSparseVector<int> Multiply(this BigSparseVector<int> u, int s) => u.Multiply(s, BLAS.Int32);
        public static BigSparseVector<long> Multiply(this BigSparseVector<long> u, long s) => u.Multiply(s, BLAS.Int64);
        public static BigSparseVector<float> Multiply(this BigSparseVector<float> u, float s) => u.Multiply(s, BLAS.Float);
        public static BigSparseVector<double> Multiply(this BigSparseVector<double> u, double s) => u.Multiply(s, BLAS.Double);
        public static BigSparseVector<decimal> Multiply(this BigSparseVector<decimal> u, decimal s) => u.Multiply(s, BLAS.Decimal);
        public static BigSparseVector<Complex> Multiply(this BigSparseVector<Complex> u, Complex s) => u.Multiply(s, BLAS.Complex);
        public static BigSparseVector<T> Multiply<T>(this BigSparseVector<T> u, T s) where T : IRing<T>, new() => u.Multiply(s, new NativeRingProvider<T>());
        */
        /// <summary>
        /// Calculates the outer product between two sparse vectors, $u \otimes v$. 
        /// </summary>
        /// <param name="u">The left sparse vector.</param>
        /// <param name="v">The right sparse vector.</param>
        /// <returns>A rank 1 sparse matrix equal to the outer product (tensorial product) of two vectors.</returns>
        /// <cat>la</cat>
        public static DOKSparseMatrix<int> OuterProduct(this BigSparseVector<int> u, BigSparseVector<int> v) => OuterProduct(u, v, (x, y) => x * y);
        public static DOKSparseMatrix<long> OuterProduct(this BigSparseVector<long> u, BigSparseVector<long> v) => OuterProduct(u, v, (x, y) => x * y);
        public static DOKSparseMatrix<float> OuterProduct(this BigSparseVector<float> u, BigSparseVector<float> v) => OuterProduct(u, v, (x, y) => x * y);
        public static DOKSparseMatrix<double> OuterProduct(this BigSparseVector<double> u, BigSparseVector<double> v) => OuterProduct(u, v, (x, y) => x * y);
        public static DOKSparseMatrix<decimal> OuterProduct(this BigSparseVector<decimal> u, BigSparseVector<decimal> v) => OuterProduct(u, v, (x, y) => x * y);
        public static DOKSparseMatrix<T> OuterProduct<T>(this BigSparseVector<T> u, BigSparseVector<T> v) where T : IRing<T>, new() => OuterProduct(u, v, (x, y) => x.Multiply(y));
        private static DOKSparseMatrix<T> OuterProduct<T>(this BigSparseVector<T> u, BigSparseVector<T> v, Func<T, T, T> Multiply) where T : new()
        {
            if (u == null || v == null) throw new ArgumentNullException();

            int m = u.Dimension, n = v.Dimension;
            Dictionary<long, T> _u = u.Values, _v = v.Values, product = new Dictionary<long, T>();

            foreach (KeyValuePair<long, T> uPair in _u)
            {
                long x = uPair.Key;
                foreach (KeyValuePair<long, T> vPair in _v)
                {
                    long y = vPair.Key;
                    product[x * n + y] = Multiply(uPair.Value, vPair.Value);
                }
            }
            return new DOKSparseMatrix<T>(m, n, product);
        }
    }
}
