﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearNet.Vectors
{
    public static class VectorChecks
    {
        public static void CheckNotNull(object obj)
        {
            if (obj == null)
            {
                throw new ArgumentNullException("Vector is null or empty.");
            }
        }
    }
}
